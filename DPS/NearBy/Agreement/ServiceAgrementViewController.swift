//
//  ServiceAgrementViewController.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import Kingfisher

struct Signature {
    var customer: UIImage?
    var technician: UIImage?
    var customerNotPresent = false
    var termsCondition = false
    var customerNotes: String?
    var internalNotes: String?
    var sms = true
    var phone = true
    var email = true
    var customerSignature: String?
    var salesSignature: String?
    var amount = 0.0
    var isDecline = false
}
enum UploadType {
    case customer
    case technician
    case address
    case cheque
    case eafSign
}

protocol EditableStatusDelegate {
    func updateEditStatus(_ entityDetail: EntityDetail?, propertyDetail: PropertyDetails?)
}
class ServiceAgrementViewController: BaseViewController {
    
    enum AgreementButtonTypes {
        case initial
        case agreementSent
    }
    
    enum ServiceMonth: String
    {
        case january
        case february
        case march
        case april
        case may
        case june
        case july
        case august
        case september
        case october
        case november
        case december
       
        
        var title: String
        {
            switch self {
            case .january:
                return "January"
            case .february:
                return "February"
            case .march:
                return "March"
            case .april:
                return "April"
            case .may:
                return "May"
            case .june:
                return "June"
            case .july:
                return "July"
            case .august:
                return "August"
            case .september:
                return "September"
            case .october:
                return "October"
            case .november:
                return "November"
            case .december:
                return "December"
            }
        }
    }
    
    enum PaymentMode: Int, CaseIterable {
        case paymentMethod = 0
        case creditCard
        case cash
        case check
        case autoChargeCustomer
        case collectAtTime
        case invoice
        case none
        
        var title: String {
            switch self {
            case .paymentMethod:
                return "Payment Method"
            case .creditCard:
                return "Credit Card"
            case .cash:
                return "Cash"
            case .check:
                return "Check"
            case .autoChargeCustomer:
                return "Auto Charge Customer"
            case .collectAtTime:
                return "Collect at time of Scheduling"
            case .invoice:
                return "Invoice"
            case .none:
                return ""
            }
        }
        var mode: String {
            switch self {
            case .paymentMethod:
                return "Payment Method"
            case .creditCard:
                return "CreditCard"
            case .cash:
                return "Cash"
            case .check:
                return "Check"
            case .autoChargeCustomer:
                return "AutoChargeCustomer"
            case .collectAtTime:
                return "CollectattimeofScheduling"
            case .invoice:
                return "Invoice"
            case .none:
                return ""
            }
        }
    }
    
    private enum Sections: Int, CaseIterable {
        case header = 0
        case property
        case service
        case coupons
        case credit
        case pricing
        // case billingAddress
        case notes
        case serviceMonths
        case payment
        case communication
        case terms
        case signature
        case footer
        var title: String {
            switch self {
            case .service:
                return "Service Detail"
            case .pricing:
                return "Final Price"
            /* case .billingAddress:
             return "Billing Address"*/
            case .notes:
                return "Notes"
            case .serviceMonths:
                return "Service Months"
            case .payment:
                return "Payment Mode"
            case .communication:
                return "Customer's preferred way of comunication"
            case .terms:
                return "Terms & Conditions"
            case .signature:
                return "Signature Section"
            default:
                return ""
            }
        }
    }
    
    // MARK: - Variable
    var services: [SoldServiceStandardDetail] = []
    var entityDetail: EntityDetail?
    var propertyDetails: PropertyDetails?
    var selectedTax: TaxDetail?
    lazy var discounts: [DiscountMaster] = []
    lazy var signature = Signature()
    var paymentMode: PaymentMode = .paymentMethod
    var agreementButtonTypes: AgreementButtonTypes = .initial
    var electronicAuthorizationForm:ElectronicAuthorizationForm?
    private lazy var presenter = {
        return ServiceAgreementPresenter(self)
    }()
    private lazy var presenterEntity = {
        return ServicePresenter(self)
    }()
    var isEditable = true {
        didSet {
            tableView?.reloadData()
        }
    }
    private var isTaxable = false {
        didSet {
            self.reloadTableView()
        }
    }
    var delegateEdit: EditableStatusDelegate?
    var taxPercentage: Double?
    var billingAddress: SendAgreement?
    var customerNotes: String?
    private lazy var presenterTax = {
        return ConfiguralProposalPresenter(self)
    }()
    lazy var couponEntered: String = ""
    var selectedCredit: DiscountMaster?
    
    private var amountToSend = 0.0
    var isCreditCardAdded = false
    var dictCreditCardInfo = NSDictionary()
    var dictPaymentDetails = NSMutableDictionary()
    var billingAmountFinal = 0.0
    var discountsInfoNew: [LeadAppliedDiscount] = []
    var selectedLocation: CLLocation?
    var isPreferredMonth = false
    var arrPreferredMonth = NSMutableArray()
    var strPreferredMonth = ""
    var selectecPaymentType = ""
    var strPaymentMode = ""
    var dictSelectedCustomPaymentMode = NSDictionary()
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Agreement"
        setPreferredMoth()
        setLeadData()
        setEditable()
        setFooterView()
        self.perform(#selector(taxValidation), with: nil, afterDelay: 0.0)
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
        self.navigationController?.setNavigationBarHidden(false, animated: false)

    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
                
        // 1584695 "\(entityDetail?.billToLocationId ?? "")"
        if(nsud.value(forKey: "\(entityDetail?.billToLocationId ?? "")") != nil){
            
            if nsud.value(forKey: "\(entityDetail?.billToLocationId ?? "")") is NSDictionary {
                
                isCreditCardAdded = true
                
                dictCreditCardInfo = nsud.value(forKey: "\(entityDetail?.billToLocationId ?? "")") as! NSDictionary
                
                paymentMode = .creditCard
                
                amountToSend = ("\(dictPaymentDetails.value(forKey: "amount") ?? "")" as NSString).doubleValue
                signature.amount = amountToSend
                
                let indexPath = IndexPath(item: 0, section: 5)
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
                
            }
            
        }

    }
    
    private func setLeadData() {
        if let customerNotes = customerNotes {
            signature.customerNotes = customerNotes
        } else {
            signature.customerNotes = entityDetail?.paymentInfo?.specialInstructions?.trimmed
        }
        
        signature.internalNotes = entityDetail?.leadDetail?.notes?.trimmed
        
        let taxableServicesAmount = services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).compactMap({$0.totalInitialPrice}).sum()
        let initialAmount = services.compactMap({$0.totalInitialPrice}).sum()
        let discount = services.compactMap({$0.discount}).sum()
        let totalAmount = initialAmount - discount
        let totalTaxableAmount = taxableServicesAmount - discount
        var taxAmount = 0.0
        if let taxPercentage = taxPercentage {
            taxAmount = (totalTaxableAmount/100)*(taxPercentage)
        }
        let billingAmount = totalAmount + taxAmount
        signature.amount = Double(billingAmount)
        
        if let previousTaxType = entityDetail?.leadDetail?.taxSysName?.trimmed, !previousTaxType.isEmpty {
            selectedTax = AppUserDefaults.shared.masterDetails?.taxMaster.filter({$0.sysName == previousTaxType}).first
        }
        if let paymentInfo = entityDetail?.paymentInfo, let paymentName = paymentInfo.paymentMode?.trimmed, !paymentName.isEmpty {
            for (index,row) in PaymentMode.allCases.enumerated() {
                if paymentName == row.title.replacingOccurrences(of: " ", with: "") {
                    paymentMode = PaymentMode(rawValue: index) ?? .paymentMethod
                }
            }
            
            if let amount = paymentInfo.amount {
                signature.amount = amount
            }
            
            //Custom Payment Mode
            
            if paymentMode != .cash && paymentMode != .check && paymentMode != .creditCard && paymentMode != .autoChargeCustomer && paymentMode != .invoice && paymentMode != .collectAtTime
            {
                paymentMode = .none
                strPaymentMode = paymentInfo.paymentMode?.trimmed ?? ""
            }
            var paymentModeFound = false
            
            let arrPaymentMode = getPaymentModeFromMaster()
            
            for item in arrPaymentMode
            {
                let dict = item as! NSDictionary
                
                if dict["AddLeadProspect"] == nil
                {
                    if "\(dict.value(forKey: "SysName") ?? "")" == strPaymentMode
                    {
                        self.selectecPaymentType = "\(dict.value(forKey: "Title") ?? "")"
                        paymentModeFound = true
                        dictSelectedCustomPaymentMode = dict
                        break
                    }
                }
                    
            }
            if !paymentModeFound
            {
                self.selectecPaymentType = ""
            }
        
            //End
            
        }
        guard let lead = entityDetail?.leadDetail else {
            return
        }
        if let isCustomerNotPresent = lead.isCustomerNotPresent {
            signature.customerNotPresent = isCustomerNotPresent
        }
        signature.sms = lead.smsReminders ?? true
        signature.phone = lead.phoneReminders ?? true
        signature.email = lead.emailReminders ?? true
        signature.termsCondition = lead.iAgreeTerms ?? false
        if let leadAppliedDiscounts = entityDetail?.leadAppliedDiscounts {
            if let objects = AppUserDefaults.shared.masterDetails?.discountCoupons, !objects.isEmpty {
                for leadAppliedDiscount in leadAppliedDiscounts {
                    if let discount = objects.filter({$0.discountCode == leadAppliedDiscount.discountCode}).first{
                        discounts.append(discount)
                    }
                }
            }
        }
        
        if let strMonth = entityDetail?.leadDetail?.strPreferredMonth?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if strMonth.count > 0
            {
                
                let arrMonth = strMonth.components(separatedBy: ",") //as! NSArray
                
                for item in arrMonth
                {
                    let str = item as String
                    arrPreferredMonth.add(str.trimmingCharacters(in: .whitespacesAndNewlines))
                }
                //arrPreferredMonth.addObjects(from: arrMonth as [Any])
                strPreferredMonth = arrPreferredMonth.componentsJoined(by: ",")

            }
        }

    }
    private func pushToSendTextView() {
        guard let propertyDetails = propertyDetails else {
            return
        }
        let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "SendTextViewController") as? SendTextViewController else {
            return
        }
        switch propertyDetails.propertyType {
        case .lead:
            destination.sendAgreement = getLeadAgreement()
            break
        case .account:
            destination.sendAgreement = getAccountAgreement()
            break
        case .opportunity:
            destination.sendAgreement = getOpportunityAgreement()
            break
        case .empty:
            destination.sendAgreement = getNewSendAggrement()
            break
        }
        var imagesUpload: [ImageToUpload] = []
        
        if let image = propertyDetails.propertyInfo?.imageManual {
            let timeStamp  = Date().currentTimeMillis()
            imagesUpload.append(ImageToUpload(image: image, type: .address, fileName: "\(String.random())-\(timeStamp).jpeg"))
        }
        if let image = signature.technician {
            let timeStamp  = Date().currentTimeMillis()
            imagesUpload.append(ImageToUpload(image: image, type: .technician, fileName: "\(String.random())-\(timeStamp).jpeg"))
        }
        if !signature.customerNotPresent {
            if let image = signature.customer {
                let timeStamp  = Date().currentTimeMillis()
                imagesUpload.append(ImageToUpload(image: image, type: .customer, fileName: "\(String.random())-\(timeStamp).jpeg"))
            }
        }
        destination.delegateEdit = self
        destination.imagesToUpload = imagesUpload
        destination.isEditable = isEditable
        destination.propertyDetails = propertyDetails
        destination.services = services
        destination.entityDetail = entityDetail
        //destination.services = getServiceWithoutCredit()

        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func getServiceWithoutCredit() -> [SoldServiceStandardDetail]
    {
        for (index, service) in services.enumerated()
        {
            var updatedService = service
            
            print(updatedService)
            
            var discountService = service.discount ?? 0 as Double

            let serviceId = service.soldServiceStandardID
            
            for discount in discountsInfoNew
            {
                let discountId = discount.soldServiceID
                
                if serviceId == discountId && DiscountType(rawValue: discount.discountType ?? "") == DiscountType.credit
                {
                    let discountAmount = (discount.discountAmount ?? 0) as Double
                    
                    let totalAmount = (updatedService.totalInitialPrice ?? 0) as Double

                    discountService = discountService - discountAmount
                    
                    updatedService.discount = discountService
                    
                    updatedService.discountPercentage = (discountService * 100)/totalAmount
                }
            }
            
            services[index] = updatedService
        }
        var agreementNew = SendAgreement()
        
        agreementNew.soldServiceStandardDetail = services
        
        return services
    }
    
    private func proceedValidation(_ sender: UIButton) {
        if AppUserDefaults.shared.isTaxRequired {
            guard let selectedTax = selectedTax, let _ = selectedTax.sysName  else {
                showAlert(message: "Please select tax")
                return
            }
        }
        var isProceed = proceedValidationBilling()
        guard isProceed else {
            isProceed = proceedValidationBilling()
            return
        }
        guard paymentMode != .paymentMethod else {
            let indexPath = IndexPath(row: 0, section: Sections.payment.rawValue)
            tableView.scrollToRow(at: indexPath, at: .top, animated: false)
            showAlert(message: "Please select a payment mode")
            return
        }
        guard (!signature.customerNotPresent && signature.termsCondition) || (signature.customerNotPresent) else {
            let indexPath = IndexPath(row: 0, section: Sections.terms.rawValue)
            tableView.scrollToRow(at: indexPath, at: .top, animated: false)
            showAlert(message: "Please accept our Terms and Conditions")
            return
        }
        if !signature.customerNotPresent  {
            if signature.customer == nil && signature.customerSignature == nil {
                let indexPath = IndexPath(row: 0, section: Sections.signature.rawValue)
                tableView.scrollToRow(at: indexPath, at: .top, animated: false)
                showAlert(message: "Please add customer signature")
                return
            }
        }
        
        if sender.tag == 0 {
            if signature.salesSignature?.trimmed != nil || signature.technician != nil {
                if sender.tag == 3 {
                    pushToSendTextView()
                } else {
                    pushToEmailView()
                }
            } else {
                let indexPath = IndexPath(row: 0, section: Sections.signature.rawValue)
                tableView.scrollToRow(at: indexPath, at: .top, animated: false)
                showAlert(message: "Please add technician signature")
            }
        } else {
            if sender.tag == 3 {
                pushToSendTextView()
            } else {
                pushToEmailView(true)
            }
            
        }
    }
    @objc func actionOpenEmail() {
        if let email = propertyDetails?.propertyInfo?.email?.trimmed {
            actionEmail(email: email)
        }
    }
    @objc func actionOpenCall() {
        if let number = propertyDetails?.propertyInfo?.phone?.trimmed {
            actionCall(number: number)
        }
        
    }
    func setPreferredMoth()  {
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        isPreferredMonth = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPreferredMonth") as! Bool
    }
    // MARK: - Actions
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func actionTermsCondition(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        signature.termsCondition = sender.isSelected
    }
    @objc func actionSendText(_ sender: UIButton) {
        proceedValidation(sender)
    }
    
    @objc func actionSendAgreement(_ sender: UIButton) {
        proceedValidation(sender)
    }
    
    private func proceedValidationBilling() -> Bool {
        guard var billingAddress = billingAddress else  {
            return true
        }
        if let billingAddressSameAsService = billingAddress.billingAddressSameAsService, !billingAddressSameAsService {
            guard let address = billingAddress.tempFullBillingAddress, !address.isEmpty else {
                showAlert(message: "Please enter billing Address")
                return false
            }
            let addressComponents =  address.trimmed.replacingOccurrences(of: "  ", with: "").components(separatedBy:",")
            guard addressComponents.count == 4 ||  addressComponents.count == 5  else {
                showAlert(message: "Please enter valid Address")
                return false
            }
            guard let states = AppUtility.fetchStates() else {
                showAlert(message: "Unable to find states")
                return false
            }
            var administrativeArea = ""
            if addressComponents.count == 4 {
                administrativeArea = addressComponents[2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
            } else {
                administrativeArea = addressComponents[3].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
            }
            guard let _ = states.filter({ (stateT) -> Bool in
                return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
            }).first else {
                showAlert(message: "We are not serving in this state")
                return false
            }
            if let address1 =  addressComponents.first?.trimmed {
                billingAddress.billingAddress1 = String(address1.prefix(100))
            }
            var position = 0
            if addressComponents.count == 4 {
                position = 0
            } else {
                position = 1
            }
            if addressComponents.count >= 2 {
                let city = addressComponents[position+1].trimmed
                billingAddress.billingCityName = city
            }
            if addressComponents.count >= 3 {
                let administrativeArea = addressComponents[position+2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
                if let states = AppUtility.fetchStates()  {
                    if let state = states.filter({ (stateT) -> Bool in
                        return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
                    }).first  {
                        billingAddress.billingStateId = state.stateID
                        billingAddress.billingState = state.name
                    } else {
                        showAlert(message: "We are not serving in this state")
                        return false
                    }
                }
            }
            if addressComponents.count >= 4 {
                let zipCode = addressComponents[position+3].trimmed
                billingAddress.billingZipcode = zipCode
            }
            
            guard let firstName = billingAddress.billingFirstName, !firstName.isEmpty else {
                showAlert(message: "Please enter first name")
                return false
            }
            /*guard let lastName = billingAddress.billingLastName, !lastName.isEmpty else {
                showAlert(message: "Please enter last name")
                return false
            }
            if let email = billingAddress.billingPrimaryEmail {
                guard email.isValidEmail()  else {
                    showAlert(message: "Please enter valid Email address")
                    return false
                }
            }*/
        }
        if propertyDetails?.propertyType == .opportunity {
            billingAddress.billingPOCId = propertyDetails?.opportunity?.billingAddress?.billingPOCId
        }
        self.billingAddress = billingAddress
        return true
    }
    @IBAction func actionHome(_ sender: Any)   {
        
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
        NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
        
    }
    @objc func actionDecline(_ sender: UIButton) {
        signature.isDecline = true
        guard let propertyDetails = propertyDetails else {
            return
        }
        let storyboard =  UIStoryboard.init(name: "Proposal", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "DeclinePopupViewController") as! DeclinePopupViewController
        switch propertyDetails.propertyType {
        case .lead:
            destination.sendAgreement = getLeadAgreement()
            break
        case .account:
            destination.sendAgreement = getAccountAgreement()
            break
        case .opportunity:
            destination.sendAgreement = getOpportunityAgreement()
            break
        case .empty:
            destination.sendAgreement = getNewSendAggrement()
            break
        }
        destination.delegate = self
        destination.modalPresentationStyle = .overFullScreen
        self.present(destination, animated: true, completion: nil)
    }
    
    @objc func actionDone(_ sender: UIButton) {
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
    }
    @objc func actionSetupService(_ sender: UIButton) {
        guard !services.isEmpty else {
            return showAlert(message: "you need to have atleast one selected service to setup service")
        }
        let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "ServiceSetupViewController") as? ServiceSetupViewController else {
            return
        }
        destination.entityDetail = entityDetail
        destination.services = services
        destination.propertyDetails = propertyDetails
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    @IBAction func unwindToAgreement(_ sender: UIStoryboardSegue) {
        guard let source = sender.source as? ServiceReportViewController else {
            return
        }
        propertyDetails?.propertyType = .opportunity
        self.entityDetail = source.entityDetail
        propertyDetails?.propertyInfo?.email = entityDetail?.leadDetail?.primaryEmail
        isEditable = AppUtility.setEditable(self.entityDetail)
        agreementButtonTypes = .agreementSent
        delegateEdit?.updateEditStatus(self.entityDetail, propertyDetail: self.propertyDetails)
        setEditable()
        /* if let leadNumber = agreementResponse?.leadNumber?.description {
         presenterEntity.apiCallForServiceList(leadNumber)
         }*/
    }
    @IBAction func actionSms(_ sender: UISwitch) {
        signature.sms = sender.isOn
    }
    @IBAction func actionPhone(_ sender: UISwitch) {
        signature.phone = sender.isOn
    }
    @IBAction func actionEmail(_ sender: UISwitch) {
        signature.email = sender.isOn
    }
    @objc func actionElcetronicAuthorization(_ sender: UIButton) {
        
        let isEaMasterAvailable = Global().isEaMasterAvailable()
        
        if isEaMasterAvailable {
            
            let storyboard = UIStoryboard(name: "ElectronicAuthorization", bundle: nil)
            guard let destination = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorizationViewController") as? ElectronicAuthorizationViewController else { return }
            destination.delegate = self
            destination.isEditable = isEditable
            destination.formDetail = electronicAuthorizationForm
            destination.propertyDetails = propertyDetails
            destination.entityDetail = entityDetail
            self.navigationController?.pushViewController(destination, animated: true)
            
        }else{
            
            showAlert(message: NoDataAvailable)
            
        }

    }
    @IBAction func actionOnRefreshCustomerCheck(_ sender: UISwitch)
    {
        //let strLeadId = entityDetail?.leadDetail?.leadID//"\(String(describing: entityDetail?.leadDetail?.leadID!))"
        let strLeadId = entityDetail?.leadDetail?.leadID
        let strId = "\(strLeadId!)"
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        presenter.apiCallForRefreshSignatureStatus(strId, companyKey: strCompanyKey)
    }
    
    override func showAlert(message: String?) {
        showAlertWithCompletion(message: message, completion: nil)
        tableView.reloadData()
    }
    
    @objc func actionOnJanuary(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("January")
        {
            arrPreferredMonth.remove("January")

        }
        else
        {
            arrPreferredMonth.add("January")

        }
        tableView.reloadData()
    }
    @objc func actionOnFebruary(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("February")
        {
            arrPreferredMonth.remove("February")
        }
        else
        {
            arrPreferredMonth.add("February")
        }
        tableView.reloadData()
        
    }
    @objc func actionOnMarch(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("March")
        {
            arrPreferredMonth.remove("March")
        }
        else
        {
            arrPreferredMonth.add("March")
        }
        tableView.reloadData()
    }
    @objc func actionOnApril(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("April")
        {
            arrPreferredMonth.remove("April")
        }
        else
        {
            arrPreferredMonth.add("April")
        }
        tableView.reloadData()
    }
    @objc func actionOnMay(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("May")
        {
            arrPreferredMonth.remove("May")
        }
        else
        {
            arrPreferredMonth.add("May")
        }
        tableView.reloadData()
    }
    @objc func actionOnJune(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("June")
        {
            arrPreferredMonth.remove("June")
        }
        else
        {
            arrPreferredMonth.add("June")
        }
        tableView.reloadData()
    }
    @objc func actionOnJuly(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("July")
        {
            arrPreferredMonth.remove("July")
        }
        else
        {
            arrPreferredMonth.add("July")
        }
        tableView.reloadData()
    }
    @objc func actionOnAugust(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("August")
        {
            arrPreferredMonth.remove("August")
        }
        else
        {
            arrPreferredMonth.add("August")
        }
        tableView.reloadData()
    }
    @objc func actionOnSeptember(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("September")
        {
            arrPreferredMonth.remove("September")
        }
        else
        {
            arrPreferredMonth.add("September")
        }
        tableView.reloadData()
    }
    @objc func actionOnOctober(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("October")
        {
            arrPreferredMonth.remove("October")
        }
        else
        {
            arrPreferredMonth.add("October")
        }
        tableView.reloadData()
    }
    @objc func actionOnNovember(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("November")
        {
            arrPreferredMonth.remove("November")
        }
        else
        {
            arrPreferredMonth.add("November")
        }
        tableView.reloadData()
    }
    @objc func actionOnDecember(_ sender: UIButton) {
        
        if arrPreferredMonth.contains("December")
        {
            arrPreferredMonth.remove("December")
        }
        else
        {
            arrPreferredMonth.add("December")
        }
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension ServiceAgrementViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allCases.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections(rawValue: section) {
        case .header, .property, .signature, .footer, .communication: //.billingAddress:
            return 1
            
        case .serviceMonths:
            if isPreferredMonth
            {
                return 1
            }
            else
            {
                return 0
            }
        case .service:
            return services.count
        case .pricing, .notes, .terms:
            return 2
        case .payment:
            return 1
        case .some(.coupons):
            return discounts.filter({$0.type == .coupon}).count
        case .some(.credit):
            return discounts.filter({$0.type == .credit}).count
        case .none:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch Sections(rawValue: indexPath.section) {
        case .header:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PropertyDetailHeaderTableViewCell.self), for: indexPath) as! PropertyDetailHeaderTableViewCell
            cell.propertyInfo = propertyDetails?.propertyInfo
            let tap = UITapGestureRecognizer(target: self, action: #selector(actionOpenCall))
            cell.stackPhone.addGestureRecognizer(tap)
            let emailTap = UITapGestureRecognizer(target: self, action: #selector(actionOpenEmail))
            cell.stackEmail.addGestureRecognizer(emailTap)
            return cell
        case .property:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementPropertyTableViewCell.self), for: indexPath) as! AgreementPropertyTableViewCell
            cell.propertyInfo = propertyDetails?.propertyInfo
            return cell
            
        case .service:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ServiceAgreementTableViewCell.self), for: indexPath) as! ServiceAgreementTableViewCell
            cell.service = services[indexPath.row]
            return cell
            
        case .pricing:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementPricingTableViewCell.self), for: indexPath) as! AgreementPricingTableViewCell
            if indexPath.row == 0 {
                cell.lblTitle.text  = "Initial Amount"
                let initialAmount = services.compactMap({$0.totalInitialPrice}).sum()
                let taxableServicesAmount = services.filter({($0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential") || ($0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential")}).compactMap({$0.totalInitialPrice}).sum()
                
                var totalCredit = 0.0
                var totalCoupon = 0.0
                for discountCredit in discounts
                {
                    if discountCredit.type == DiscountType.credit
                    {
                        totalCredit = totalCredit + Double(discountCredit.appliedDiscount)
                    }
                    else
                    {
                        totalCoupon = totalCoupon + Double(discountCredit.appliedDiscount)
                    }
                }
                let discount = totalCredit + totalCoupon//services.compactMap({$0.discount}).sum()
                
                let totalAmount = initialAmount - discount
                let totalTaxableAmount = taxableServicesAmount - discount
                var taxAmount = 0.0
                if let taxPercentage = taxPercentage {
                    taxAmount = (totalTaxableAmount/100)*(taxPercentage)
                }
                let billingAmount = totalAmount + taxAmount
                cell.lblSubTotal.text = initialAmount.currency
                cell.lblTax.text = taxAmount.currency
                cell.lblDiscount.text = discount.currency
                cell.lblBillingAmount.text = billingAmount.currency
                
                signature.amount = billingAmount
                
                cell.viewTax.isHidden = true
                
            } else {
                cell.lblTitle.text  = "Maintenance Amount"
                let initialAmount = services.compactMap({$0.totalMaintPrice}).sum()
                let taxableServicesAmount = services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).compactMap({$0.totalMaintPrice}).sum()
                
                var totalCredit = 0.0

                for discountCredit in discounts
                {
                    if discountCredit.type == DiscountType.credit
                    {
                        totalCredit = totalCredit + Double(discountCredit.appliedMaintanenceDiscount)
                    }
                }

                let discount = totalCredit//services.compactMap({$0.serviceTempMaitenance}).sum()
                let totalAmount = initialAmount - discount
                let totalTaxableAmount = taxableServicesAmount - discount
                var taxAmount = 0.0
                if let taxPercentage = taxPercentage {
                    taxAmount = (totalTaxableAmount/100)*(taxPercentage)
                }
                let billingAmount = totalAmount + taxAmount
                cell.lblSubTotal.text = initialAmount.currency
                cell.lblTax.text = taxAmount.currency
                cell.lblDiscount.text = discount.currency
                cell.lblBillingAmount.text = billingAmount.currency
                cell.viewTax.isHidden = false
            }
            
            cell.txtTax.tag = 1
            cell.txtTax.text = selectedTax?.name
            cell.txtTax.delegate = self
            //cell.viewTax.isHidden = !isTaxable
            cell.txtTax.isUserInteractionEnabled = isEditable
            
            
            return cell
        case .some(.coupons) , .some(.credit):
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalDiscountTableViewCell.self), for: indexPath) as! ProposalDiscountTableViewCell
            if Sections(rawValue: indexPath.section) == .coupons {
                let discount = self.discounts.filter({$0.type == .coupon})[indexPath.row]
                cell.discount = discount
                cell.lblAmount.text = "\(discount.appliedDiscount.currency)"
                
            } else {
                let discount =  self.discounts.filter({$0.type == .credit})[indexPath.row]
                cell.discount = discount
                cell.lblAmount.text = "\(discount.appliedDiscount.currency)/\(discount.appliedMaintanenceDiscount.currency)"
            }
            cell.isUserInteractionEnabled = isEditable
            cell.delegate = self
            return cell
        /*case .billingAddress:
         let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BillingAddressTableViewCell.self), for: indexPath) as! BillingAddressTableViewCell
         cell.delegate = self
         cell.propertyDetails = propertyDetails
         cell.entityDetail = entityDetail
         cell.billingAddress = billingAddress
         cell.setUI()
         cell.setData()
         cell.isUserInteractionEnabled = isEditable
         return cell*/
        case .notes:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementNotesTableViewCell.self), for: indexPath) as! AgreementNotesTableViewCell
            cell.isUserInteractionEnabled = isEditable
            // internalNotes
            if indexPath.row == 0 {
                cell.lblTitle.text = "Customer Notes"
                cell.notesType = .customerN
                cell.txtView.text = signature.customerNotes
            } else {
                cell.lblTitle.text = "Internal Notes"
                cell.notesType = .internalN
                cell.txtView.text = signature.internalNotes
            }
            cell.delegate = self
            return cell
            
        case .serviceMonths:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementServiceMonthTableViewCell.self), for: indexPath) as! AgreementServiceMonthTableViewCell
            
            cell.btnJanuary.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnFebruary.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnMarch.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnApril.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnMay.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnJune.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnJuly.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnAugust.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnSeptember.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnOctober.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnNovember.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            cell.btnDecember.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            
            
            
            if arrPreferredMonth.contains("January")
            {
                cell.btnJanuary.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnJanuary.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            
            if arrPreferredMonth.contains("February")
            {
                cell.btnFebruary.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnFebruary.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            
            if arrPreferredMonth.contains("March")
            {
                cell.btnMarch.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnMarch.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            if arrPreferredMonth.contains("April")
            {
                cell.btnApril.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnApril.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            if arrPreferredMonth.contains("May")
            {
                cell.btnMay.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnMay.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            if arrPreferredMonth.contains("June")
            {
                cell.btnJune.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnJune.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            if arrPreferredMonth.contains("July")
            {
                cell.btnJuly.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnJuly.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            if arrPreferredMonth.contains("August")
            {
                cell.btnAugust.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnAugust.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            if arrPreferredMonth.contains("September")
            {
                cell.btnSeptember.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnSeptember.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            if arrPreferredMonth.contains("October")
            {
                cell.btnOctober.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnOctober.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            
            if arrPreferredMonth.contains("November")
            {
                cell.btnNovember.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnNovember.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            
            if arrPreferredMonth.contains("December")
            {
                cell.btnDecember.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            }
            else
            {
                cell.btnDecember.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            }
            
            cell.btnJanuary.addTarget(self, action: #selector(actionOnJanuary), for: .touchUpInside)
            cell.btnFebruary.addTarget(self, action: #selector(actionOnFebruary), for: .touchUpInside)
            cell.btnMarch.addTarget(self, action: #selector(actionOnMarch), for: .touchUpInside)
            cell.btnApril.addTarget(self, action: #selector(actionOnApril), for: .touchUpInside)
            cell.btnMay.addTarget(self, action: #selector(actionOnMay), for: .touchUpInside)
            cell.btnJune.addTarget(self, action: #selector(actionOnJune), for: .touchUpInside)
            cell.btnJuly.addTarget(self, action: #selector(actionOnJuly), for: .touchUpInside)
            cell.btnAugust.addTarget(self, action: #selector(actionOnAugust), for: .touchUpInside)
            cell.btnSeptember.addTarget(self, action: #selector(actionOnSeptember), for: .touchUpInside)
            cell.btnOctober.addTarget(self, action: #selector(actionOnOctober), for: .touchUpInside)
            cell.btnNovember.addTarget(self, action: #selector(actionOnNovember), for: .touchUpInside)
            cell.btnDecember.addTarget(self, action: #selector(actionOnDecember), for: .touchUpInside)
            
            if arrPreferredMonth.count > 0
            {
                strPreferredMonth = arrPreferredMonth.componentsJoined(by: ",")
            }
            else
            {
                strPreferredMonth = ""
            }

            return cell
            
        case .payment:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementPaymentTableViewCell.self), for: indexPath) as! AgreementPaymentTableViewCell
            cell.delegate = self
            cell.txtPaymentMethod.isUserInteractionEnabled = isEditable
            cell.txtAmount.isUserInteractionEnabled = isEditable
            cell.txtAmount.tag = 100
            cell.txtAmount.delegate = self
            cell.txtPaymentMethod.text = paymentMode.title
            
            if AppUserDefaults.shared.isElectronicAuthorizationForm {
                cell.viewElectronicAuth.isHidden = false
                if let entityDetail = entityDetail, let accountElectronicAuthorizationFormSigned = entityDetail.leadDetail?.accountElectronicAuthorizationFormSigned, accountElectronicAuthorizationFormSigned {
                    cell.lblElectronicAuthStatus.text = "Signed Form Available On Account"
                    cell.lblElectronicAuthStatus.textColor = .black
                } else {
                    cell.lblElectronicAuthStatus.text =  "Signed Form Not Available On Account"
                    cell.lblElectronicAuthStatus.textColor = .systemRed
                }
            } else {
                cell.viewElectronicAuth.isHidden = true
            }
            cell.btnElcetronicAuthorization.addTarget(self, action: #selector(actionElcetronicAuthorization(_:)), for: .touchUpInside)
            if paymentMode == .cash || paymentMode == .check || paymentMode == .creditCard{
                cell.viewAmount.isHidden = false
                cell.txtAmount.text = signature.amount.currencyWithoutSign.replacingOccurrences(of: ",", with: "")
                let strAmount = signature.amount.currencyWithoutSign.replacingOccurrences(of: ",", with: "")
                //String(format:"%.2f", signature.amount)
            }
            else
            {
                cell.viewAmount.isHidden = true
            }
            
            //For Custom Payment Mode
            
            if paymentMode != .cash && paymentMode != .check && paymentMode != .creditCard && paymentMode != .autoChargeCustomer && paymentMode != .invoice && paymentMode != .collectAtTime
            {
                cell.txtPaymentMethod.text = "\(dictSelectedCustomPaymentMode.value(forKey: "Title") ?? "")"
                
                if "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1" || "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                {
                    cell.viewAmount.isHidden = false
                    cell.txtAmount.text = signature.amount.currencyWithoutSign.replacingOccurrences(of: ",", with: "")
                }
                else
                {
                    cell.viewAmount.isHidden = true
                }
            }
            
            
            amountToSend = ((cell.txtAmount.text ?? "") as NSString).doubleValue
            cell.txtAmount.text = String(format:"%.2f", amountToSend)

            
          /*  if paymentMode == .creditCard
            {
                //show button
                //cell.btnChargeCard.ishidden = false
                cell.btnChargeCard.isHidden = false
                
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                
                if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPestPacIntegration") as! Bool) == true
                {
                    if (dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.IsPaymentIntegration") as! Bool) == true
                    {
                        
                        cell.btnChargeCard.isHidden = false

                    }
                    else
                    {
                        // Payment Integration is Off
                        cell.btnChargeCard.isHidden = true

                    }
                }
                else
                {
                    // Payment Integration is Off
                    cell.btnChargeCard.isHidden = true

                }

                if isCreditCardAdded {
                    
                    cell.viewCardInfo.isHidden = false
                    cell.lblCardDetail.isHidden = false
                    cell.lblCardDetail.text = "Card \(dictCreditCardInfo.value(forKey: "accountNumber") ?? "")" + " Added Successfully."
                    
                } else {
                    
                    cell.lblCardDetail.text = ""
                    cell.lblCardDetail.isHidden = true
                    cell.viewCardInfo.isHidden = true

                }
                
            }
            else
            {
                cell.btnChargeCard.isHidden = true
                cell.lblCardDetail.isHidden = true

            }*/
            
            //Nilind - Show Add Card in each payment mode
            
            if strPaymentMode != "" 
            {
                cell.btnChargeCard.isHidden = false
                
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                
                if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPestPacIntegration") as! Bool) == true
                {
                    if (dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.IsPaymentIntegration") as! Bool) == true
                    {
                        
                        cell.btnChargeCard.isHidden = false

                    }
                    else
                    {
                        // Payment Integration is Off
                        cell.btnChargeCard.isHidden = true

                    }
                }
                else
                {
                    // Payment Integration is Off
                    cell.btnChargeCard.isHidden = true

                }

                if isCreditCardAdded {
                    
                    cell.viewCardInfo.isHidden = false
                    cell.lblCardDetail.isHidden = false
                    cell.lblCardDetail.text = "Card \(dictCreditCardInfo.value(forKey: "accountNumber") ?? "")" + " Added Successfully."
                    
                } else {
                    
                    cell.lblCardDetail.text = ""
                    cell.lblCardDetail.isHidden = true
                    cell.viewCardInfo.isHidden = true

                }
            }
            else
            {
                cell.btnChargeCard.isHidden = true
                cell.lblCardDetail.isHidden = true
            }

            
            
            //End
            
            
            
            if entityDetail?.leadDetail?.statusSysName == "Complete" &&  entityDetail?.leadDetail?.stageSysName == "Won"
            {
                
                cell.btnChargeCard.isHidden = true

            }
            
            
            cell.btnChargeCard.tag = indexPath.row
            cell.btnChargeCard.addTarget(self, action: #selector(goToCreditCradView(_:)), for: .touchUpInside)
            
            return cell
        case .communication:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementCommunicationTableViewCell.self), for: indexPath) as! AgreementCommunicationTableViewCell
            cell.signature = signature
            cell.switchSms.isUserInteractionEnabled = isEditable
            cell.switchEmail.isUserInteractionEnabled = isEditable
            cell.switchPhone.isUserInteractionEnabled = isEditable
            cell.switchSms.addTarget(self, action: #selector(actionSms(_:)), for: .touchUpInside)
            cell.switchEmail.addTarget(self, action: #selector(actionEmail(_:)), for: .touchUpInside)
            cell.switchPhone.addTarget(self, action: #selector(actionPhone(_:)), for: .touchUpInside)
            return cell
        case .terms:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementeTermsTableViewCellNew.self), for: indexPath) as! AgreementeTermsTableViewCellNew
            
            cell.btnCheck.isUserInteractionEnabled = isEditable

            if indexPath.row == 0 {
                cell.viewCheckMark.isHidden = true
                cell.lblHeading.text = "Service Terms & Conditions"
               // cell.services = services
                cell.setTermAndCondition(isService: true, services: services)
            } else {
                cell.viewCheckMark.isHidden = signature.customerNotPresent
                cell.lblHeading.text = "General"
               // cell.termsConditions = AppUserDefaults.shared.masterDetails?.generalTermsConditions?.termsConditions?.html2String
                cell.setTermAndCondition(isService: false, services: services)
            }

            if signature.customerNotPresent == true {
                signature.termsCondition = false
                cell.btnCheck.isUserInteractionEnabled = false
            }
            cell.btnCheck.isSelected = signature.termsCondition
            cell.btnCheck.addTarget(self, action: #selector(actionTermsCondition(_:)), for: .touchUpInside)
            
            return cell
        case .signature:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementSignatureTableViewCell.self), for: indexPath) as! AgreementSignatureTableViewCell
            cell.signature = signature
            cell.isUserInteractionEnabled = isEditable
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strEmployeeSignature = "\(dictLoginData.value(forKeyPath: "EmployeeSignature")!)"
            
            //if nsud.bool(forKey: "isPreSetSignSales") || strEmployeeSignature.count > 0
            if nsud.bool(forKey: "isPreSetSignSales") && strEmployeeSignature.count > 0
            {
                signature.salesSignature = strEmployeeSignature.components(separatedBy: "Documents").last
                cell.imgViewTechnicianSignature.isUserInteractionEnabled = false
                if  let urlString = strEmployeeSignature.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)  {
                    cell.imgViewTechnicianSignature.kf.setImage(with: url)
                }
            }
            else if let paymentInfo = entityDetail?.paymentInfo
            {
                cell.imgViewTechnicianSignature.isUserInteractionEnabled = true
               
                if let salesSignature = paymentInfo.salesSignature
                {
                    if paymentInfo.salesSignature == ""
                    {
                        
                    }
                    else
                    {
                        signature.salesSignature = salesSignature.components(separatedBy: "Documents").last
                        
                        if  let urlString = (URL.salesAutoBaseUrl + salesSignature).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)
                        {
                            cell.imgViewTechnicianSignature.kf.setImage(with: url)
                            //cell.imgViewTechnicianSignature.setImageWith(URL(string: urlString), placeholderImage: UIImage(named: ""), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                            
                        }
                    }
                }
                
                if let customerSignature = paymentInfo.customerSignature
                {
                    signature.customerSignature = customerSignature.components(separatedBy: "Documents").last
                    
                    if  let urlString = (URL.salesAutoBaseUrl + customerSignature).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)
                    {
                        cell.imgViewCustomerSignature.kf.setImage(with: url)
                        //cell.imgViewCustomerSignature.setImageWith(URL(string: urlString), placeholderImage: UIImage(named: ""), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                        
                        if customerSignature.contains("Documents")
                        {
                            
                        }
                        else
                        {
                            if  let urlString = (URL.salesAutoBaseUrl + "/Documents/" + customerSignature).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)
                            {
                                cell.imgViewCustomerSignature.kf.setImage(with: url)
                                //cell.imgViewCustomerSignature.setImageWith(URL(string: urlString), placeholderImage: UIImage(named: ""), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                            }
                        }

                    }
                                        
                }
            }
            if propertyDetails?.propertyType == .opportunity
            {
                      
                if entityDetail?.leadDetail?.statusSysName == "Complete" &&  (entityDetail?.leadDetail?.stageSysName == "CompletePending" || entityDetail?.leadDetail?.stageSysName == "Pending" )
                {
                    cell.viewRefreshStatus.isHidden = false
                }else{
                    cell.viewRefreshStatus.isHidden = true
                    
                }
                
            }
            if let paymentInfo = entityDetail?.paymentInfo {
                
                if let customerSignature = paymentInfo.customerSignature {
                    signature.customerSignature = customerSignature.components(separatedBy: "Documents").last
                    if  let urlString = (URL.salesAutoBaseUrl + customerSignature).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)
                    {
                        cell.imgViewCustomerSignature.kf.setImage(with: url)
                        
                        if customerSignature.contains("Documents")
                        {
                            
                        }
                        else
                        {
                            if  let urlString = (URL.salesAutoBaseUrl + "/Documents/" + customerSignature).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)
                            {
                                cell.imgViewCustomerSignature.kf.setImage(with: url)
                                //cell.imgViewCustomerSignature.setImageWith(URL(string: urlString), placeholderImage: UIImage(named: ""), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                            }
                        }
                        //cell.imgViewCustomerSignature.setImageWith(URL(string: urlString), placeholderImage: UIImage(named: ""), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)

                    }
                }
                
                
            }
            cell.delegate = self
            //cell.viewRefreshStatus.isHidden = false
            return cell
        case .footer:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementFooterTableViewCell.self), for: indexPath) as! AgreementFooterTableViewCell
            
            if let leadDetail = self.entityDetail?.leadDetail
            {
                if leadDetail.isInitialSetupCreated == true, !AppUtility.isSetupService(leadDetail)
                {
                    cell.btnSetupService.isHidden = true
                }
                else
                {
                    cell.btnSetupService.isHidden = false
                    cell.btnSetupService.addTarget(self, action: #selector(actionSetupService), for: .touchUpInside)
                    if leadDetail.statusSysName == "Complete" &&  leadDetail.stageSysName == "Won"
                    {
                        cell.btnSetupService.isHidden = false
                        
                        if leadDetail.isInitialSetupCreated == true
                        {
                            cell.btnSetupService.isHidden = true

                        }
                    }
                    else
                    {
                        cell.btnSetupService.isHidden = true
                    }
                }
            }
            else
            {
                cell.btnSetupService.isHidden = true
            }
            /*
             if entityDetail?.leadDetail?.isInitialSetupCreated == true || isEditable {
             cell.btnSetupService.isHidden = true
             } else {
             cell.btnSetupService.isHidden = false
             cell.btnSetupService.addTarget(self, action: #selector(actionSetupService), for: .touchUpInside)
             }*/
            if isEditable {
                cell.btnSendAgreement.tag = 0
                cell.btnResendEmail.isHidden = true
                cell.btnResendText.isHidden = true
                cell.btnSendAgreement.isHidden = false
                cell.btnSendAgreement.setTitle("Send Email", for: .normal)
                cell.btnSendText.isHidden = !AppUserDefaults.shared.isShowSendText
                /*  if signature.customerNotPresent == true {
                 cell.btnSendAgreement.setTitle("Send Agreement Link", for: .normal)
                 } else {
                 cell.btnSendAgreement.setTitle("Send by Email", for: .normal)
                 }*/
            } else {
                cell.btnResendEmail.tag = 1
                cell.btnResendEmail.isHidden = false
                cell.btnSendText.isHidden = !AppUserDefaults.shared.isShowSendText
                cell.btnSendAgreement.isHidden = true
                cell.btnSendText.isHidden = true
                
            }
            cell.btnDecline.isHidden = !isEditable
            cell.btnDecline.addTarget(self, action: #selector(actionDecline), for: .touchUpInside)
            
            cell.btnSendAgreement.addTarget(self, action: #selector(actionSendAgreement), for: .touchUpInside)
            cell.btnResendEmail.addTarget(self, action: #selector(actionSendAgreement), for: .touchUpInside)
            cell.btnSendText.tag = 3
            cell.btnResendText.tag = 3
            cell.btnSendText.addTarget(self, action: #selector(actionSendText), for: .touchUpInside)
            cell.btnResendText.addTarget(self, action: #selector(actionSendText), for: .touchUpInside)
            cell.btnDone.addTarget(self, action: #selector(actionDone), for: .touchUpInside)
            if agreementButtonTypes == .agreementSent {
                cell.btnDone.isHidden = false
            } else {
                cell.btnDone.isHidden = true
            }
            cell.setUI()
            
            return cell
        case .none:
            return UITableViewCell()
        }
    }
}

// MARK: - UITableViewDelegate
extension ServiceAgrementViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderTitleTableViewCell.self)) as! HeaderTitleTableViewCell
        switch Sections(rawValue: section) {
        case .service, .pricing, .notes, .terms, .signature, .payment, .communication, .serviceMonths:
            cell.lblTitle.text = Sections(rawValue: section)?.title
            return cell.contentView
            
//        case  .serviceMonths:
//
//            if isPreferredMonth
//            {
//                cell.lblTitle.text = Sections(rawValue: section)?.title
//                return cell.contentView
//
//            }
//            else
//            {
//                return UIView()
//            }
        case .some(.coupons):
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalCouponsHeaderTableViewCell.self)) as! ProposalCouponsHeaderTableViewCell
            cell.txtCoupon.tag = section
            cell.txtCoupon.text = couponEntered
            cell.txtCoupon.delegate = self
            cell.viewHeadings.isHidden = self.discounts.filter({$0.type == .coupon}).isEmpty
            cell.btnApply.addTarget(self, action: #selector(actionApplyCoupon), for: .touchUpInside)
            return cell.contentView
        case .some(.credit):
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalCreditHeaderTableViewCell.self)) as! ProposalCreditHeaderTableViewCell
            cell.txtCredit.text = selectedCredit?.name
            cell.txtCredit.tag = section
            cell.txtCredit.delegate = self
            cell.viewHeadings.isHidden = self.discounts.filter({$0.type == .credit}).isEmpty
            cell.btnApply.addTarget(self, action: #selector(actionApplyCredit), for: .touchUpInside)
            return cell.contentView
        default:
            return UIView()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch Sections(rawValue: section) {
        case .service, .pricing, .notes, .payment, .signature, .terms, .communication, .coupons, .credit:
            return UITableView.automaticDimension
            
        case .serviceMonths:
            if isPreferredMonth
            {
                return UITableView.automaticDimension
            }
            else
            {
                return 0
            }
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch Sections(rawValue: section) {
        case .credit, .coupons:
            return 10
        default:
            return 1
        }
    }
}
// MARK: - AgreementSignatureDelegate
extension ServiceAgrementViewController: AgreementSignatureDelegate {
    func actioinCustomerNotPresent(_ isSelected: Bool) {
        signature.customerNotPresent = isSelected
        
        //self.tableView.reloadRows(at: [IndexPath(row: 0, section: Sections.signature.rawValue), IndexPath(row: 0, section: Sections.footer.rawValue)], with: .none)
        
        self.tableView.reloadData()
    }
    func openSignatureViewCustomer() {
        openSignatureView(true)
    }
    func openSignatureViewTechnician() {
        openSignatureView(false)
    }
}
// MARK: - Convenience
extension ServiceAgrementViewController {
    
    
    private func  callTaxtApi() {
        if AppUserDefaults.shared.isTaxRequired {
            requiredTax()
        } else {
            nonRequiredTax()
        }
    }
    func requiredTax() {
        if let selectedTax = selectedTax, let sysyName = selectedTax.sysName {
            if isTaxable,(taxPercentage == nil) {
                presenterTax.apiCallForTaxtDetail(taxSysName: sysyName)
                
            }
        }
    }
    func nonRequiredTax() {
        if let selectedTax = selectedTax, let sysyName = selectedTax.sysName, isTaxable {
            if (taxPercentage == nil) {
                presenterTax.apiCallForTaxtDetail(taxSysName: sysyName)
            }
        }
    }
    @objc private func taxValidation() {
        if  services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).first != nil {
            if let isServiceAddrTaxExempt = entityDetail?.leadDetail?.isServiceAddrTaxExempt {
                if isServiceAddrTaxExempt == false {
                    isTaxable = true
                } else {
                    isTaxable = false
                }
            } else {
                isTaxable = true
            }
        } else {
            isTaxable = false
        }
    }
    private func reloadTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    private func openSignatureView(_ isCustomer: Bool) {
        
        guard let vc = UIStoryboard.init(name: "Agreement", bundle: nil).instantiateViewController(withIdentifier: "SignatureViewController") as? SignatureViewController else { return }
        vc.title = isCustomer ? "Customer's Signature" : "Technician's Signature"
        let offset = self.tableView.contentOffset
        vc.signatureActionHandler  =  { [weak self](image) -> () in
            if isCustomer {
                self?.signature.customer  = image
            } else {
                self?.signature.technician = image
            }
            self?.tableView.contentOffset = offset
            self?.tableView.reloadData()
            
            let indexPath = IndexPath(row: 0, section: Sections.signature.rawValue)
            self?.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
            
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    private func pushToEmailView(_ isResend: Bool = false) {
        signature.isDecline = false
        guard let propertyDetails = propertyDetails else {
            return
        }
        let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "ServiceReportViewController") as? ServiceReportViewController else {
            return
        } //Email
        switch propertyDetails.propertyType {
        case .lead:
            destination.sendAgreement = getLeadAgreement()
            break
        case .account:
            destination.sendAgreement = getAccountAgreement()
            break
        case .opportunity:
            destination.sendAgreement = getOpportunityAgreement()
            break
        case .empty:
            destination.sendAgreement = getNewSendAggrement()
            break
        }
        var imagesUpload: [ImageToUpload] = []
        
        if let image = propertyDetails.propertyInfo?.imageManual {
            let timeStamp  = Date().currentTimeMillis()
            imagesUpload.append(ImageToUpload(image: image, type: .address, fileName: "\(String.random())-\(timeStamp).jpeg"))
        }
        if let image = signature.technician {
            let timeStamp  = Date().currentTimeMillis()
            imagesUpload.append(ImageToUpload(image: image, type: .technician, fileName: "\(String.random())-\(timeStamp).jpeg"))
        }
        if !signature.customerNotPresent {
            if let image = signature.customer {
                let timeStamp  = Date().currentTimeMillis()
                imagesUpload.append(ImageToUpload(image: image, type: .customer, fileName: "\(String.random())-\(timeStamp).jpeg"))
            }
        }
        destination.imagesToUpload = imagesUpload
        destination.isResend = isResend
        destination.propertyDetails = propertyDetails
        destination.services = services
        destination.entityDetail = entityDetail
        //destination.services = getServiceWithoutCredit()

        self.navigationController?.pushViewController(destination, animated: true)
    }
    @objc func goToCreditCradView(_ sender: UIButton)
    {
        
        self.view.endEditing(true)
        
        if entityDetail?.leadDetail?.statusSysName == "Complete" &&  entityDetail?.leadDetail?.stageSysName == "Won"
        {
            
        }else{
            
            
            if isInternetAvailable() {
                
                let firstNameL = "\(entityDetail?.leadDetail?.billingFirstName ?? "")"
                let lastNameL = "\(entityDetail?.leadDetail?.billingLastName ?? "")"
                let billingAddress1L = "\(entityDetail?.leadDetail?.billingAddress1 ?? "")"
                let billingZipcodeL = "\(entityDetail?.leadDetail?.billingZipcode ?? "")"
                let billToLocationIdL = "\(entityDetail?.billToLocationId ?? "")"
                let webLeadIdL = "\(entityDetail?.leadDetail?.leadID ?? 0)"
                let contactNameToCheck = "\(firstNameL)" + "\(lastNameL)"

                let contactName = "\(firstNameL)" + " " + "\(lastNameL)"
                
                let ammountL = amountToSend
                            
                if contactNameToCheck.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing contact name.", viewcontrol: self)
                    
                }
                else if contactName.count > 30 {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Billing contact name must be less than 30 characters", viewcontrol: self)
                    
                }
                else if billingAddress1L.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing address.", viewcontrol: self)
                    
                }else if billingZipcodeL.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing zipcode.", viewcontrol: self)
                    
                }else if billToLocationIdL.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billto location id.", viewcontrol: self)
                    
                }else if webLeadIdL.count == 0 || webLeadIdL == "0" {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Lead does not exist.", viewcontrol: self)
                    
                }else if ammountL <= 0 {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide amount to charge.", viewcontrol: self)
                    
                }else{
                    
                    dictPaymentDetails.setValue(webLeadIdL, forKey: "leadId")
                    dictPaymentDetails.setValue(billToLocationIdL, forKey: "billToId")
                    dictPaymentDetails.setValue(ammountL, forKey: "amount")
                    dictPaymentDetails.setValue(contactName, forKey: "nameOnCard")
                    dictPaymentDetails.setValue(billingAddress1L, forKey: "billingAddress")
                    dictPaymentDetails.setValue(billingZipcodeL, forKey: "billingPostalCode")
                    
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestPacPayment" : "PestPacPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "PestPacPaymentVC") as? PestPacPaymentVC
                    vc?.dictPaymentDetails = dictPaymentDetails
                    //self.navigationController?.pushViewController(vc!, animated: false)
                    self.navigationController?.present(vc!, animated: false, completion: nil)

                }
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
                
            }
            
        }
        
    }
    
}

// MARK: - AgreementPaymentCellDelegate
extension ServiceAgrementViewController: AgreementPaymentCellDelegate {
    func amountEntered(_ string: String) {
        signature.amount = Double(string) ?? 0
    }
    
    func paymentMethodPressed() {
        
       /* var rows:[String] = []
        var selectedRow = 0
        for (index,row) in PaymentMode.allCases.enumerated() {
            if index != 0 {
                if paymentMode.title == row.title{
                    selectedRow = index - 1
                }
                rows.append(row.title)
            }
        }
        showStringPicker(sender: view, selectedRow: selectedRow, rows: rows) { (index, value, view) in
            self.paymentMode = PaymentMode(rawValue: index + 1) ?? .paymentMethod
            //self.tableView.reloadRows(at: [IndexPath(row: 0, section: Sections.payment.rawValue)], with: .none)
            self.tableView.reloadData()
        }*/
        paymentMethodCalled()
    }
}

extension ServiceAgrementViewController: AgreementNotesCellDelegate {
    func updatedTextForNotes(_ notes: String, notesType: NotesType) {
        if notesType == .customerN {
            signature.customerNotes = notes
        } else {
            signature.internalNotes = notes
        }
    }
}
// MARK: - ServiceAgreementView
extension ServiceAgrementViewController: ServiceAgreementView
{
   
    func gotSigntureStatus(response: RefreshCustomerSignatureModel) {
        print("Signature Received")
        print(response.leadID)
        entityDetail?.leadDetail?.stageSysName = response.stageSysName
        entityDetail?.leadDetail?.statusSysName = response.statusSysName
        entityDetail?.leadDetail?.isCustomerNotPresent = response.isCustomerNotPresent
        entityDetail?.paymentInfo?.salesSignature = response.leadPaymentDetailDc?.salesSignature
        isEditable = AppUtility.setEditable(self.entityDetail)
        signature.customerNotPresent = response.isCustomerNotPresent!
        signature.customerSignature = response.leadPaymentDetailDc?.customerSignature
        //signature.salesSignature = response.leadPaymentDetailDc?.salesSignature
        entityDetail?.paymentInfo?.customerSignature = response.leadPaymentDetailDc?.customerSignature
        tableView.reloadData()

    }
    
    
    
    func uploadImage(type: UploadType, uploadResponse: UploadResponse) {
        /*if type == .technician {
         signature.salesSignature = uploadResponse.name
         if !signature.customerNotPresent  {
         entityDetail?.paymentInfo?.salesSignature = uploadResponse.name
         if let image = signature.customer {
         let timeStamp  = Date().currentTimeMillis()
         presenter.apiCallForUploadSignature(type: .customer, image: image, fileName: "\(String.random())-\(timeStamp).jpeg")
         } else {
         showAlert(message: "Please add customer signature")
         }
         } else {
         pushToEmailView()
         }
         } else {
         signature.customerSignature = uploadResponse.name
         entityDetail?.paymentInfo?.customerSignature = uploadResponse.name
         pushToEmailView()
         }*/
    }
}
extension ServiceAgrementViewController {
    
    private func getCommonAgreement() -> SendAgreement {
        var agreement = SendAgreement()
        // updateServicesWithDicountVSP()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        agreement.companyKey = companyKey
        agreement.assignedToType = "Individual"
        agreement.addressSubType = "Residential"
        agreement.soldServiceStandardDetail  = services
        
        let date = Date()
        let currentDate = AppUtility.convertDateToString(date, toFormat: "yyyy/MM/dd")
        let currentTime = AppUtility.convertDateToString(date, toFormat: "HH:mm")
        agreement.scheduleDate = currentDate
        agreement.scheduleTime = currentTime
        if let source = AppUserDefaults.shared.leadDetailMaster?.sourceMaster.filter({$0.sourceSysName == "DoorToDoor"}).first {
            agreement.sourceIDS = [source.sourceId]
        }
        agreement.country = "USA"
        agreement.countryID = 1
        let assignedToID = "\(dictLoginData.value(forKeyPath: "EmployeeId") ?? "")"
        agreement.fieldSalesPerson = assignedToID
        agreement.assignedToID = assignedToID
        agreement.addressImagePath = propertyDetails?.propertyInfo?.imageUrl?.components(separatedBy: "\\").last
        if let billingAddress = billingAddress {
            
            agreement.billingAddress1 = billingAddress.billingAddress1
            agreement.billingCityName = billingAddress.billingCityName
            agreement.billingStateId = billingAddress.billingStateId
            agreement.billingZipcode = billingAddress.billingZipcode
            agreement.billingCellPhone1 = billingAddress.billingCellPhone1
            agreement.billingFirstName = billingAddress.billingFirstName
            agreement.billingLastName = billingAddress.billingLastName
            agreement.billingPrimaryEmail = billingAddress.billingPrimaryEmail
            agreement.billingAddressSameAsService = billingAddress.billingAddressSameAsService ?? true
            agreement.billingState = billingAddress.billingState
            agreement.billingCountry = billingAddress.billingCountry ?? "USA"
            agreement.billingCountryId =  billingAddress.billingCountryId ?? "1"
            agreement.billingPOCId = billingAddress.billingPOCId
        }
        agreement.webLeadID = nsud.value(forKey: "webLeadIdD2D") as? Int//entityDetail?.webLeadId
        agreement.leadNumber = "\(nsud.value(forKey: "leadNumberD2D") ?? "")"//entityDetail?.leadNumber
        
       

//        nsud.setValue(0, forKey: "webLeadIdD2D")
//        nsud.setValue("", forKey: "leadNumberD2D")
//        nsud.synchronize()
        agreement.secondCall = true
        agreement.latitude = selectedLocation!.coordinate.latitude
        agreement.longitude = selectedLocation!.coordinate.longitude
        return agreement
        
    }
    private func getNewSendAggrement() -> SendAgreement {
        
        guard let  propertyDetails = propertyDetails, let propertyInfo = propertyDetails.propertyInfo else { return SendAgreement() }
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        
        var agreement = getCommonAgreement()
        agreement.flowType =  "Residential"
        agreement.companyKey = companyKey
        //agreement.soldServiceStandardDetail  = services
        agreement.area = propertyInfo.area
        agreement.noofBathroom = propertyInfo.bathRoom
        agreement.noofBedroom = propertyInfo.bedRoom
        agreement.noofStory = propertyInfo.noOfFloor
        agreement.primaryEmail = propertyInfo.email ?? ""
        agreement.cellPhone1 = propertyInfo.phone ?? ""
        //
        agreement.cityName = propertyInfo.city ?? propertyDetails.propertyInfo?.placeMark?.subLocality
        agreement.zipcode =  propertyInfo.zipCode ?? propertyDetails.propertyInfo?.placeMark?.postalCode
        agreement.firstName =  String(propertyInfo.firstName?.prefix(50) ?? "")
        agreement.lastName =  String(propertyInfo.lastName?.prefix(50) ?? "")
        agreement.address1 = String(propertyInfo.address?.prefix(100) ?? "")
        let array = String(propertyInfo.address?.prefix(100) ?? "").components(separatedBy: ", ") as NSArray
        if array.count > 0
        {
            agreement.address1 = array[0] as? String
        }
        agreement.countryID = 1
        agreement.visitCount = 1
        agreement.stateID = propertyDetails.propertyInfo?.state?.stateID
        agreement.state = propertyDetails.propertyInfo?.state?.stateSysName
        agreement.branchSysName = barnchSysName
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        /*
         if let categorySysName = services.first?.categorySysName, let departmentSysName = AppUserDefaults.shared.masterDetails?.categories.filter({$0.sysName == categorySysName}).first?.departmentSysName {
         agreement.departmentSysName = departmentSysName
         }*/
        agreement.leadDetail = getCommonLead()
        agreement.electronicAuthorizationForm = self.electronicAuthorizationForm
        agreement.paymentInfo = getPaymentInfo(nil)
        agreement.leadAppliedDiscounts = getDiscountInfo(nil)
        return agreement
    }
    private func getLeadAgreement() -> SendAgreement {
        
        var agreement = getCommonAgreement()
        
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        agreement.leadAppliedDiscounts = getDiscountInfo(entityDetail?.leadAppliedDiscounts)
        guard let leadDetail = propertyDetails?.lead else { return agreement }
        // agreement.assignedToID =  leadDetail.assignedToID?.description
        agreement.flowType =  leadDetail.flowType ?? "Residential"
        agreement.companyKey = leadDetail.companyKey ?? ""
        agreement.area = leadDetail.area  ?? ""
        agreement.noofBathroom = leadDetail.noofBathroom ?? ""
        agreement.noofBedroom = leadDetail.noofBedroom ?? ""
        agreement.noofStory = leadDetail.noofStory  ?? ""
        agreement.primaryEmail = leadDetail.primaryEmail  ?? ""
        agreement.cellPhone1 = leadDetail.primaryPhone ?? ""
        agreement.cityName = leadDetail.cityName ?? ""
        agreement.zipcode =  leadDetail.zipcode ?? ""
        agreement.firstName =  String(leadDetail.firstName?.prefix(50) ?? "")
        agreement.lastName = String(leadDetail.lastName?.prefix(50) ?? "")
        agreement.address1 =  String(leadDetail.address1?.prefix(100) ?? "")
        agreement.address2 = String(leadDetail.address2?.prefix(100) ?? "")
        agreement.visitStatusID = leadDetail.visitStatusID
        agreement.emailDetail = entityDetail?.emailDetail
        agreement.documentsDetail = entityDetail?.documentDetail
        agreement.stateID = leadDetail.stateID
        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
            return state.stateID == leadDetail.stateID
        }) {
            agreement.state = state.stateSysName
        }
        
        agreement.visitCount = leadDetail.visitCount
        agreement.objectionIds = leadDetail.objectionIds
        agreement.currentServicesProvider = leadDetail.currentServicesProvider
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        agreement.branchSysName = barnchSysName
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        agreement.electronicAuthorizationForm = self.electronicAuthorizationForm
        agreement.latitude = selectedLocation!.coordinate.latitude
        agreement.longitude = selectedLocation!.coordinate.longitude
        agreement.secondCall = true
        return agreement
    }
    
    
    private func getOpportunityAgreement() -> SendAgreement {
        var agreement = getCommonAgreement()
        
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        agreement.leadAppliedDiscounts = getDiscountInfo(entityDetail?.leadAppliedDiscounts)
        if let opportunityDetails = propertyDetails?.opportunity {
            agreement.firstName =  String(opportunityDetails.firstName?.prefix(50) ?? "")
            agreement.lastName = String(opportunityDetails.lastName?.prefix(50) ?? "")
            //agreement.leadNumber = entityDetail?.leadDetail?.leadNumber?.description
            agreement.primaryEmail = opportunityDetails.primaryEmail ?? ""
            agreement.cellPhone1 = opportunityDetails.primaryPhone ?? ""
            agreement.branchSysName = opportunityDetails.branchSysName ?? ""
            agreement.departmentSysName = opportunityDetails.departmentSysName ?? ""
            agreement.flowType =  opportunityDetails.opportunityType  ?? "Residential"
            if let serviceAddress = opportunityDetails.serviceAddress {
                agreement.area = serviceAddress.area ?? ""
                agreement.noofBathroom = serviceAddress.noofBathroom ?? ""
                agreement.noofBedroom = serviceAddress.noofBedroom ?? ""
                agreement.noofStory = serviceAddress.noofStory ?? ""
                agreement.address1 = serviceAddress.address1 ?? ""
                agreement.address2 = serviceAddress.address2 ?? ""
                agreement.cityName = serviceAddress.cityName ?? ""
                agreement.zipcode =  serviceAddress.zipcode ?? ""
                agreement.stateID = serviceAddress.stateID
                if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                    return state.stateID == serviceAddress.stateID
                }) {
                    agreement.state = state.stateSysName
                }
            }
            
        } else if let opportunityDetails = entityDetail?.leadDetail {
            agreement.firstName =  String(opportunityDetails.firstName?.prefix(50) ?? "")
            agreement.lastName = String(opportunityDetails.lastName?.prefix(50) ?? "")
            agreement.leadNumber = opportunityDetails.leadNumber?.description
            agreement.primaryEmail = opportunityDetails.primaryEmail ?? ""
            agreement.cellPhone1 = opportunityDetails.primaryPhone ?? ""
            agreement.branchSysName = opportunityDetails.branchSysName ?? ""
            agreement.departmentSysName = opportunityDetails.departmentSysName ?? ""
            agreement.flowType =  opportunityDetails.flowType ?? "Residential"
            agreement.area = opportunityDetails.area ?? ""
            agreement.noofBathroom = opportunityDetails.noofBathroom ?? ""
            agreement.noofBedroom = opportunityDetails.noofBedroom ?? ""
            agreement.noofStory = opportunityDetails.noOfStory ?? ""
            agreement.address1 = opportunityDetails.servicesAddress1 ?? ""
            agreement.address2 = opportunityDetails.serviceAddress2 ?? ""
            agreement.cityName = opportunityDetails.serviceCity ?? ""
            agreement.zipcode =  opportunityDetails.serviceZipcode ?? ""
            agreement.state = opportunityDetails.serviceState
        }
        agreement.electronicAuthorizationForm = entityDetail?.electronicAuthorizationForm
        return agreement
    }
    private func getAccountAgreement() -> SendAgreement {
        
        var agreement = getCommonAgreement()
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        agreement.leadAppliedDiscounts = getDiscountInfo(entityDetail?.leadAppliedDiscounts)
        guard let accountDetails = propertyDetails?.account else { return agreement }
        agreement.firstName =  String(accountDetails.firstName?.prefix(50) ?? "")
        agreement.lastName = String(accountDetails.lastName?.prefix(50) ?? "")
        agreement.primaryEmail = accountDetails.primaryEmail
        agreement.cellPhone1 = accountDetails.primaryPhone
        agreement.flowType =  "Residential"
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        agreement.branchSysName = barnchSysName
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        if let serviceAddress = accountDetails.serviceAddress {
            agreement.area = serviceAddress.area ?? ""
            agreement.noofBathroom = serviceAddress.noofBathroom ?? ""
            agreement.noofBedroom = serviceAddress.noofBedroom ?? ""
            agreement.noofStory = serviceAddress.noofStory ?? ""
            agreement.address1 = String(serviceAddress.address1?.prefix(100) ?? "")
            agreement.address2 = String(serviceAddress.address2?.prefix(100) ?? "")
            agreement.cityName = serviceAddress.cityName ?? ""
            agreement.zipcode =  serviceAddress.zipcode ?? ""
            agreement.stateID = serviceAddress.stateID
            agreement.flowType = "Residential"
            if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                return state.stateID == serviceAddress.stateID
            }) {
                agreement.state = state.stateSysName
            }
        }
        agreement.electronicAuthorizationForm = self.electronicAuthorizationForm
        return agreement
    }
    private func getNewLeads() -> LeadDetail? {
        
        var leadDetail = LeadDetail()
        let date = Date()
        let currentDate = AppUtility.convertDateToString(date, toFormat: "yyyy/MM/dd")
        let currentTime = AppUtility.convertDateToString(date, toFormat: "HH:mm")
        leadDetail.flowType = "Residential"
        leadDetail.scheduleTime = currentTime
        leadDetail.scheduleDate = currentDate
        leadDetail.isCustomerNotPresent = signature.customerNotPresent
        leadDetail.latitude = propertyDetails?.propertyInfo?.latitude
        leadDetail.longitude = propertyDetails?.propertyInfo?.longitude
        
        return leadDetail
    }
    private func getCommonLead(_ lead: LeadDetail? = nil) -> LeadDetail? {
        
        var leadDetail = lead
        
        if leadDetail == nil {
            leadDetail = getNewLeads()
        }
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        leadDetail?.notes = signature.internalNotes
        leadDetail?.versionNumber = appVersion
        if signature.isDecline {
            leadDetail?.statusSysName = "Complete"
            leadDetail?.stageSysName = "Lost"
        } else {
            if !signature.customerNotPresent {
                leadDetail?.stageSysName = "Won"
            } else {
                leadDetail?.stageSysName = "CompletePending"
            }
            leadDetail?.statusSysName = "Complete"
        }
        leadDetail?.collectedAmount = signature.amount
        leadDetail?.subTotalAmount = services.compactMap({$0.totalInitialPrice}).sum()
        leadDetail?.subTotalMaintAmount = services.compactMap({$0.totalMaintPrice}).sum()
        leadDetail?.totalPrice = services.compactMap({$0.totalInitialPrice}).sum()
        leadDetail?.totalMaintPrice = services.compactMap({$0.totalMaintPrice}).sum()
        leadDetail?.iAgreeTerms = signature.termsCondition
        leadDetail?.isAgreementSigned = !signature.customerNotPresent
        leadDetail?.isCustomerNotPresent = signature.customerNotPresent
        leadDetail?.smsReminders = signature.sms
        leadDetail?.phoneReminders = signature.phone
        leadDetail?.emailReminders = signature.email
        
        
        leadDetail?.tax = taxPercentage?.description ?? 0.description
        leadDetail?.taxSysName =  selectedTax?.sysName ?? ""
        
        let taxableServicesAmountMaint = services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).compactMap({$0.totalMaintPrice}).sum()
        let discountMaint = services.compactMap({$0.serviceTempMaitenance}).sum()
        
        let totalTaxableAmountMaint = taxableServicesAmountMaint - discountMaint
        var taxAmountMaint = 0.0
        if let taxPercentage = taxPercentage {
            taxAmountMaint = (totalTaxableAmountMaint/100)*(taxPercentage)
        }
        leadDetail?.taxableMaintAmount = totalTaxableAmountMaint
        leadDetail?.taxMaintAmount = taxAmountMaint
        
        var taxAmount = 0.0
        let taxableServicesAmount = services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).compactMap({$0.totalInitialPrice}).sum()
        let discount = services.compactMap({$0.discount}).sum()
        
        let totalTaxableAmount = taxableServicesAmount - discount
        
        if let taxPercentage = taxPercentage {
            taxAmount = (totalTaxableAmount/100)*(taxPercentage)
        }
        
        leadDetail?.taxableAmount = totalTaxableAmount
        leadDetail?.taxAmount = taxAmount
        
        leadDetail?.versionDate = VersionDate
        leadDetail?.versionNumber = VersionNumber
        let deviceVersion = UIDevice.current.systemVersion
        let deviceName = UIDevice.current.name
        leadDetail?.deviceName = deviceName
        leadDetail?.deviceVersion = deviceVersion
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmployeeSignature = "\(dictLoginData.value(forKeyPath: "EmployeeSignature")!)"
        
        if nsud.bool(forKey: "isPreSetSignSales") && strEmployeeSignature.count > 0 {
            if  let urlString = strEmployeeSignature.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let _ = URL(string: urlString)  {
                leadDetail?.isEmployeePresetSignature = true
            }
        }
        
        //Nilind
        
        //let discountInitial = services.compactMap({$0.discount}).sum()
        
        var totalCredit = 0.0
        var totalCoupon = 0.0
        for discountCredit in discounts
        {
            if discountCredit.type == DiscountType.credit
            {
                totalCredit = totalCredit + Double(discountCredit.appliedDiscount)
            }
            else
            {
                totalCoupon = totalCoupon + Double(discountCredit.appliedDiscount)
            }
        }
        let discountInitial = totalCredit + totalCoupon
        
        let totalAmountInitial = services.compactMap({$0.totalInitialPrice}).sum() - discountInitial
        
        let taxableServicesAmountIntial = services.filter({($0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential") || ($0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential")}).compactMap({$0.totalInitialPrice}).sum()
        
        let totalTaxableAmountIntial = taxableServicesAmountIntial - discountInitial
        
        let totalTaxableAmountIntialNew = totalTaxableAmountIntial
        
        var taxAmountInitial = 0.0
        
        if let taxPercentage = taxPercentage {
            taxAmountInitial = (totalTaxableAmountIntialNew/100)*(taxPercentage)
        }
        if taxAmountInitial < 0
        {
            taxAmountInitial = 0
        }
        
        let billingAmountInitial = totalAmountInitial + taxAmountInitial
        
        leadDetail?.totalPrice = totalAmountInitial
        
        //Maintenance Price
 
        let maintAmount = services.compactMap({$0.totalMaintPrice}).sum()
        let taxableServicesMaintAmountNew = services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).compactMap({$0.totalMaintPrice}).sum()
        
        //let discountMaintNew = services.compactMap({$0.serviceTempMaitenance}).sum()
        
        var totalCreditMaint = 0.0

        for discountCredit in discounts
        {
            if discountCredit.type == DiscountType.credit
            {
                totalCreditMaint = totalCreditMaint + Double(discountCredit.appliedMaintanenceDiscount)
            }
        }
        let discountMaintNew = totalCreditMaint//services.compactMap({$0.serviceTempMaitenance}).sum()

        
        let totalAmountMaint = maintAmount - discountMaintNew
        let totalTaxableAmountMaintNew = taxableServicesMaintAmountNew - discountMaintNew
        var taxAmountNew = 0.0
        if let taxPercentage = taxPercentage {
            taxAmountNew = (totalTaxableAmountMaintNew/100)*(taxPercentage)
        }
        
        var billingAmountMaint = totalAmountMaint + taxAmountNew
        
        if billingAmountMaint < 0
        {
            billingAmountMaint = 0
        }
        
        leadDetail?.totalMaintPrice = billingAmountMaint
        
        let statusL = "\(leadDetail?.statusSysName ?? "")"
        let stageL = "\(leadDetail?.stageSysName ?? "")"

        if statusL.caseInsensitiveCompare("complete") == .orderedSame  &&
            stageL.caseInsensitiveCompare("Won") == .orderedSame{
            
            leadDetail?.isAgreementGenerated = true
            
        }
        
        leadDetail?.strPreferredMonth = strPreferredMonth
        
        return leadDetail
    }
    private func getPaymentInfo(_ paymentInfo: PaymentInfo?) -> PaymentInfo? {
        var paymentInfo = paymentInfo
        if paymentInfo == nil {
            paymentInfo = PaymentInfo()
        }
        paymentInfo?.paymentMode = paymentMode.mode
        
        //For Custom Payment Mode
        if paymentInfo?.paymentMode == "" || paymentMode == .none
        {
            paymentInfo?.paymentMode = strPaymentMode
        }
                
        paymentInfo?.amount = signature.amount
        paymentInfo?.salesSignature = signature.salesSignature ?? ""
        paymentInfo?.customerSignature = signature.customerSignature ?? ""
        paymentInfo?.specialInstructions = signature.customerNotes ?? ""
        return paymentInfo
    }
    
    private func getDiscountInfo(_ discountsInfo: [LeadAppliedDiscount]?) -> [LeadAppliedDiscount]? {
        var discountsInfo: [LeadAppliedDiscount] = []
        for discount in discounts {
            var discountInfo = LeadAppliedDiscount()
            
            discountInfo.createdDate = discount.createdDate
            discountInfo.applicableForInitial = discount.applicableForInitial
            discountInfo.applicableForMaintenance =  discount.applicableForMaintenance
            discountInfo.discountAmount = discount.appliedDiscount
            discountInfo.discountCode = discount.discountCode
            discountInfo.discountDescription = discount.description
            discountInfo.discountSysName = discount.sysName
            discountInfo.isActive = discount.isActive
            discountInfo.discountType = discount.type?.rawValue
            discountInfo.isDiscountPercent = discount.isDiscountPercent
            discountInfo.name = discount.name
            
            if discount.isServiceBased == true {
                for (_, service) in services.enumerated() {
                    if service.serviceSysName == discount.serviceSysName {
                        if let soldServiceStandardID = service.soldServiceStandardID {
                            discountInfo.leadAppliedDiscountID = soldServiceStandardID
                            discountInfo.soldServiceID = soldServiceStandardID
                        } else {
                            let randomInt = Int.random(in: 1...100)
                            discountInfo.leadAppliedDiscountID = randomInt
                            discountInfo.soldServiceID = randomInt
                        }
                        discountInfo.serviceSysName = service.serviceSysName
                    }}} else {
                        if let soldServiceStandardID = services[0].soldServiceStandardID {
                            discountInfo.leadAppliedDiscountID = soldServiceStandardID
                            discountInfo.soldServiceID = soldServiceStandardID
                        } else {
                            let randomInt = Int.random(in: 1...100)
                            discountInfo.leadAppliedDiscountID = randomInt
                            discountInfo.soldServiceID = randomInt
                        }
                        discountInfo.serviceSysName = services[0].serviceSysName
                        
                    }
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            let date = Date()
            let currentDate = AppUtility.convertDateToString(date, toFormat: "MM/dd/yyyy")
            discountInfo.serviceType = "Standard"
            discountInfo.modifiedBy = Int(employeeID)
            discountInfo.modifiedDate = currentDate
            discountInfo.createdBy = Int(employeeID)
            discountInfo.isApplied = true
            
            switch propertyDetails?.propertyType {
            case .lead:
                //discountInfo.leadID = propertyDetails?.lead?.leadID
                discountInfo.accountNo = propertyDetails?.lead?.accountNo
            case .opportunity:
                //discountInfo.leadID = entityDetail?.leadDetail?.leadID
                discountInfo.accountNo = entityDetail?.leadDetail?.accountNo
            case .account:
                //discountInfo.leadID = ""
                discountInfo.accountNo = propertyDetails?.account?.accountNumber
            case .empty:
                // discountInfo.leadID = ""
                discountInfo.accountNo = ""
            case .none:
                break
            }
            discountInfo.appliedMaintDiscount = discount.appliedMaintanenceDiscount
            discountInfo.appliedInitialDiscount = discount.appliedDiscount
            
            discountsInfo.append(discountInfo)
        }
        discountsInfoNew = discountsInfo
        return discountsInfo
    }
}
extension ServiceAgrementViewController : ServiceView {
    func gotSeriveList(_ output: EntityDetail) {
        self.entityDetail = output
        delegateEdit?.updateEditStatus(self.entityDetail, propertyDetail: self.propertyDetails)
        setEditable()
    }
    @objc private func setEditable() {
        isEditable = AppUtility.setEditable(entityDetail)
    }
}
// MARK: - EditableStatusDelegate
extension ServiceAgrementViewController: EditableStatusDelegate {
    func updateEditStatus(_ entityDetail: EntityDetail?, propertyDetail: PropertyDetails?) {
        self.entityDetail = entityDetail
        self.propertyDetails = propertyDetail
        isEditable = AppUtility.setEditable(entityDetail)
        delegateEdit?.updateEditStatus(entityDetail, propertyDetail: propertyDetails)
    }
}
// MARK: - EditableStatusDelegate
extension ServiceAgrementViewController: ElectronicFormDelegate {
    func gotFormDetails(_ electronicFromDetails: ElectronicAuthorizationForm){
        if propertyDetails?.propertyType == .opportunity {
            entityDetail?.electronicAuthorizationForm = electronicFromDetails
        } else {
            self.electronicAuthorizationForm = electronicFromDetails
        }
    }
}
// MARK: - AgreementSignatureDelegate
extension ServiceAgrementViewController: BillingAddressDelegate {
    func billingDetailEntered(_ billingAddress: SendAgreement) {
        self.billingAddress = billingAddress
    }
    
    func actionBillingAddress(_ isSelected: Bool) {
        self.billingAddress?.billingAddressSameAsService = isSelected
        tableView.reloadData()
    }
    func addAddressPressed() {
        /*let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
         let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
         vc.tag = 1
         vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
         vc.modalTransitionStyle = .coverVertical
         vc.delegate = self
         self.present(vc, animated: false, completion: {})*/
    }
}

// MARK: - MapViewNavigationDelegate
extension ServiceAgrementViewController: MapViewNavigationDelegate {
    func actionBack() {
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
    }
}
// MARK: - Text field delegate
extension ServiceAgrementViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            var rows:[String] = []
            var selectedRow = 0
            self.view.endEditing(false)
            guard  let objects = AppUserDefaults.shared.masterDetails?.taxMaster, !objects.isEmpty else {
                showAlert(message: "No record found !!")
                return false
            }
            for (index,row) in objects.enumerated() {
                if selectedTax?.taxId == row.taxId{
                    selectedRow = index
                }
                rows.append(row.name ?? "")
            }
            showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                textField.text = value
                let tax = objects[index]
                if tax.taxId != self.selectedTax?.taxId {
                    self.taxPercentage = nil
                }
                self.selectedTax = objects[index]
                self.callTaxtApi()
            }
            return false
        } else  if textField.tag == 4 {
            var rows:[String] = []
            var selectedRow = 0
            
            guard  let objects = AppUserDefaults.shared.masterDetails?.discountCredits, !objects.isEmpty else {
                showAlert(message: "No record found !!")
                return false
            }
            self.view.endEditing(false)
            for (index,row) in objects.enumerated() {
                if textField.text == row.name {
                    selectedRow = index
                }
                rows.append(row.name ?? "")
            }
            showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                textField.text = value
                self.selectedCredit = objects[index]
            }
            
           
            
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            couponEntered = updatedText.trimmed
        }
        if textField.tag == 100
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            return chk
            
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 100 {
            
            amountToSend = ((textField.text ?? "") as NSString).doubleValue
            signature.amount = amountToSend

            
        }
        
    }
}

// MARK: - ProposalView
extension ServiceAgrementViewController: ProposalView {
    func gotTaxDetail(_ output: Double) {
        taxPercentage = output
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
// MARK: - Action
extension ServiceAgrementViewController {
    
    @objc func actionApplyCredit() {
        
        guard let selectedCredits = selectedCredit else {
            showAlert(message: "Please select credit")
            return
        }
        if self.discounts.contains(selectedCredits) {
            showAlert(message: "This credit is already in list")
        } else {
            self.view.endEditing(true)
            self.discounts.append(selectedCredits)
            self.updateServicesWithDiscountVSP()
        }
        selectedCredit = nil
        tableView.reloadData()
    }
    
    @objc func actionApplyCoupon() {
        
        guard  let objects = AppUserDefaults.shared.masterDetails?.discountCoupons, !objects.isEmpty else {
            showAlert(message: "No record found !!")
            return
        }
        guard !couponEntered.isEmpty else {
            showAlert(message: "Please enter coupon")
            return
        }
        guard let coupon = objects.filter({ (discount) -> Bool in
            return discount.discountCode == couponEntered
        }).first else {
            showAlert(message: "Invalid coupon")
            return
        }
        if self.discounts.contains(coupon) {
            showAlert(message: "This coupon is already in list")
        } else {
            /* let amount = services.compactMap({$0.totalInitialPrice}).sum()
             guard
             let discountAmount = coupon.discountAmount , discountAmount < amount else {
             showAlert(message: "Coupon not applicable")
             return
             }*/
            if coupon.isServiceBased == true {
                guard !objects.filter({$0.sysName == coupon.serviceSysName}).isEmpty else {
                    showAlert(message: "This coupon is not valid for any service in the list")
                    return
                }
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            if let date = coupon.validTo, !date.isEmpty {
                guard let validityDate = dateFormatter.date(from: date) else {
                    showAlert(message: "Something went wrong, please try again later")
                    return
                }
                guard Date() <= validityDate  else {
                    showAlert(message: "This coupon is expired !!")
                    return
                }
            }
            if let date = coupon.validFrom, !date.isEmpty {
                guard let validityDate = dateFormatter.date(from: date) else {
                    showAlert(message: "Something went wrong, please try again later")
                    return
                }
                guard Date() >= validityDate else {
                    showAlert(message: "This coupon is not started yet")
                    return
                }
            }
            /*
             if let dateTo = coupon.validTo, !dateTo.isEmpty, let dateFrom = coupon.validFrom, !dateFrom.isEmpty {
             
             guard let validityDateFrom = dateFormatter.date(from: dateFrom) else {
             showAlert(message: "Something went wrong, please try again later")
             return
             }
             guard let validityDateTo = dateFormatter.date(from: dateTo) else {
             showAlert(message: "Something went wrong, please try again later")
             return
             }
             guard validityDateFrom <= Date(), validityDateTo >= Date() else {
             if Date() > validityDateTo {
             showAlert(message: "This coupon is expired !!")
             } else {
             showAlert(message: "This coupon is not started yet")
             }
             return
             }
             }*/
            
            self.view.endEditing(true)
            self.discounts.append(coupon)
            self.updateServicesWithDiscountVSP()
            couponEntered = ""
            tableView.reloadData()
        }
    }
   
}
// MARK: - ProposalDiscountDelegate
extension ServiceAgrementViewController: ProposalDiscountDelegate {
    func actionRemove(_ cell: ProposalDiscountTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        
        if indexPath.section == Sections.credit.rawValue {
            if let index = self.discounts.firstIndex(of: cell.discount!) {
                self.discounts.remove(at: index)
            }
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            /*  guard self.discounts.filter({$0.type == .credit}).isEmpty else {
             return
             }*/
            updateServicesWithDiscountVSP()
            tableView.reloadData()
        } else {
            if let index = self.discounts.firstIndex(of: cell.discount!) {
                self.discounts.remove(at: index)
            }
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            /* guard self.discounts.filter({$0.type == .coupon}).isEmpty else {
             return
             }*/
            updateServicesWithDiscountVSP()
            tableView.reloadData()
        }
    }
}
// MARK: - Convenience
extension ServiceAgrementViewController
{
    private func updateServicesWithDiscountVSP()
    {
        for (index, service) in services.enumerated()
        {
            var updatedService = service
            updatedService.discount = 0
            updatedService.serviceTempMaitenance = 0
            services[index] = updatedService
            
        }
        for (index, discount) in discounts.enumerated()
        {
            if discount.isServiceBased == true
            {
                if let indexService = services.firstIndex(where: {$0.serviceSysName == discount.serviceSysName})
                {
                    let service = services[indexService]
                    
                    let calculatedValue = updateService(service, discount: discount)
                    if discount.type == .credit
                    {
                        
                    }
                    else
                    {
                        services[indexService] = calculatedValue.service
                    }
                    
                    discounts[index].appliedDiscount = calculatedValue.appliedDiscount
                    discounts[index].appliedMaintanenceDiscount = calculatedValue.appiedManitanence
                    
                    if discounts[index].type == DiscountType.credit
                    {
                        if discounts[index].applicableForInitial == false
                        {
                            discounts[index].appliedDiscount = 0.0
                        }
                        if discounts[index].applicableForMaintenance == false
                        {
                            discounts[index].appliedMaintanenceDiscount = 0.0
                            
                        }
                    }
                }
            }
            else
            {
                if let service = services.first
                {
                    let calculatedValue = updateService(service, discount: discount)
                    if discount.type == .credit
                    {
                        
                    }
                    else
                    {
                        services[0] = calculatedValue.service
                    }
                    
                    discounts[index].appliedDiscount = calculatedValue.appliedDiscount
                    
                    discounts[index].appliedMaintanenceDiscount = calculatedValue.appiedManitanence
                    
                    if discounts[index].type == DiscountType.credit
                    {
                        if discounts[index].applicableForInitial == false
                        {
                            discounts[index].appliedDiscount = 0.0
                        }
                        if discounts[index].applicableForMaintenance == false
                        {
                            discounts[index].appliedMaintanenceDiscount = 0.0
                            
                        }
                    }
                    
                    
                }
            }
            
        }
    }
    private func updateService(_ serviceT: SoldServiceStandardDetail, discount: DiscountMaster) -> (service: SoldServiceStandardDetail, appliedDiscount: Double, appiedManitanence: Double)
    {
        var service = serviceT
        let serviceDiscount = service.discount ?? 0
        var appliedDiscount: Double = 0
        var appiedManitanence: Double = 0
        let intialPrice: Double = (service.totalInitialPrice ?? 0.0) //- serviceDiscount
        if intialPrice < 0
        {
            if discount.type == .credit
            {
                
            }
            else
            {
                service.discount = service.totalInitialPrice
            }
        }
        else
        {
            var totalDiscountAmount: Double = 0.0
            var newDiscount: Double = 0
            if discount.isDiscountPercent
            {
                newDiscount = ((intialPrice)/100)*(discount.discountPercent ?? 0)
            }
            else
            {
                newDiscount = (discount.discountAmount ?? 0)
            }
            
            totalDiscountAmount = newDiscount + serviceDiscount
            
            if totalDiscountAmount > (service.totalInitialPrice ?? 0.0)
            {
                appliedDiscount = (service.totalInitialPrice ?? 0) - serviceDiscount
                if discount.type == .credit
                {
                    
                }
                else
                {
                    service.discount = service.totalInitialPrice
                    service.discountPercentage =  (service.totalInitialPrice ?? 0.0)/(service.totalInitialPrice ?? 0.0)*100
                }
            }
            else
            {
                appliedDiscount = newDiscount
                if discount.type == .credit
                {
                    
                }
                else
                {
                    service.discount = totalDiscountAmount
                    service.discountPercentage = (totalDiscountAmount/(service.totalInitialPrice ?? 0.0))*100
                }
            }
        }
        if discount.type == .credit
        {
            let maintPrice: Double = (service.totalMaintPrice ?? 0.0) - service.serviceTempMaitenance
            if maintPrice < 0
            {
                service.serviceTempMaitenance = service.totalMaintPrice ?? 0.0
            }
            else
            {
                var totalDiscountAmount: Double = 0.0
                var newDiscount: Double = 0
                if discount.isDiscountPercent
                {
                    newDiscount = ((maintPrice)/100)*(discount.discountPercent ?? 0)
                }
                else
                {
                    newDiscount = (discount.discountAmount ?? 0)
                }
                totalDiscountAmount = newDiscount + service.serviceTempMaitenance
                if totalDiscountAmount > (service.totalMaintPrice ?? 0.0)
                {
                    appiedManitanence = (service.totalMaintPrice ?? 0) - service.serviceTempMaitenance
                    service.serviceTempMaitenance = service.totalMaintPrice ?? 0.0
                    service.serviceTempMaintDiscPercentage =  (service.totalMaintPrice ?? 0.0)/(service.totalMaintPrice ?? 0.0)*100
                }
                else
                {
                    appiedManitanence = newDiscount
                    service.serviceTempMaitenance = totalDiscountAmount
                    service.serviceTempMaintDiscPercentage = (totalDiscountAmount/(service.totalMaintPrice ?? 0.0))*100
                }
            }
            if discount.applicableForMaintenance == false
            {
                service.serviceTempMaitenance = 0.00
                service.serviceTempMaintDiscPercentage = 0.00
            }
            
        }
        return (service, appliedDiscount, appiedManitanence)
    }
}

//Nilind

extension ServiceAgrementViewController
{
    func getPaymentModeFromMaster() -> NSMutableArray {
        
        var arrPaymentMaster = NSArray()
        
        var  dictLoginData = NSDictionary()
        
        dictLoginData =  nsud.value(forKey: "LoginDetails") as! NSDictionary

        let isSalesPaymentTypeDynamic = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsSalesPaymentTypeDynamic") as! Bool
        //isSalesPaymentTypeDynamic = true
        if isSalesPaymentTypeDynamic
        {
            if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
            {
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "PaymentTypeObj") is NSArray) {
                    
                    arrPaymentMaster = dictMaster.value(forKey: "PaymentTypeObj") as! NSArray
                }
            }
        }

        let arrPaymentNew = NSMutableArray()
        arrPaymentNew.add(["AddLeadProspect": "Cash"])
        arrPaymentNew.add(["AddLeadProspect": "Check"])
        arrPaymentNew.add(["AddLeadProspect": "Credit Card"])
        arrPaymentNew.add(["AddLeadProspect": "Auto Charge Customer"])
        arrPaymentNew.add(["AddLeadProspect": "Collect at time of Scheduling"])
        arrPaymentNew.add(["AddLeadProspect": "Invoice"])
        
        for item in arrPaymentMaster
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1" || ("\(dict.value(forKey: "IsActive") ?? "")" == "0" && "\(dict.value(forKey: "SysName")!)" == strPaymentMode)
            {
                arrPaymentNew.add(dict)
            }
        }
        print(arrPaymentNew)
        
        return arrPaymentNew
    }
    func paymentMethodCalled() {
        
        openTableViewPopUp(tag: 3335, ary: getPaymentModeFromMaster())
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
       // vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
}
extension ServiceAgrementViewController: CustomTableView {
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        print(dictData)
        
        if tag == 3335 {
            
            if dictData["AddLeadProspect"] != nil
            {
                self.selectecPaymentType = dictData.value(forKey: "AddLeadProspect") as! String
                print(self.selectecPaymentType)
                if self.selectecPaymentType == "Cash" {
                    self.strPaymentMode = "Cash"
                    self.paymentMode = .cash
                }else if self.selectecPaymentType == "Check" {
                    self.strPaymentMode = "Check"
                    self.paymentMode = .check
                }else if self.selectecPaymentType == "Credit Card" {
                    self.strPaymentMode = "CreditCard"
                    self.paymentMode = .creditCard
                }else if self.selectecPaymentType == "Auto Charge Customer" {
                    self.strPaymentMode = "AutoChargeCustomer"
                    self.paymentMode = .autoChargeCustomer
                }else if self.selectecPaymentType == "Collect at time of Scheduling" {
                    self.strPaymentMode = "CollectattimeofScheduling"
                    self.paymentMode = .collectAtTime
                }else if self.selectecPaymentType == "Invoice" {
                    self.strPaymentMode = "Invoice"
                    self.paymentMode = .invoice
                }else {
                    self.strPaymentMode = ""
                }
                
            }
            else
            {
                print("\(dictData)")
                self.paymentMode = .none
                dictSelectedCustomPaymentMode = dictData
                self.selectecPaymentType = dictData.value(forKey: "Title") as! String
                self.strPaymentMode = "\(dictData.value(forKey: "SysName") ?? "")"
            }
            
            //self.objPaymentInfo.setValue(self.strPaymentMode, forKey: "paymentMode")
            //let context = getContext()
            //save the object
//            do { try context.save()
//                print("Record Saved Updated.")
//            } catch let error as NSError {
//                debugPrint("Could not save. \(error), \(error.userInfo)")
//            }
            self.tableView.reloadData()
        }else {
            
        }
        
    }
}
