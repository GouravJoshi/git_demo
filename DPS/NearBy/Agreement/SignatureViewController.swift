//
//  SignatureViewController.swift
//  DPS
//
//  Created by Vivek Patel on 22/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class SignatureViewController: UIViewController {

    // MARK: - Outlet
    @IBOutlet weak var viewSign: VMSignatureView!
    
    // MARK: - Variable
    let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
    var signatureActionHandler: SignatureActionHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func actionClear(_ sender: Any) {
        self.viewSign.clear()
    }
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionSave(_ sender: Any) {
           
        guard let image =  self.viewSign.getSignature() else { return }
        self.dismiss(animated: true) {
            self.signatureActionHandler?(image)
        }
    }
    
    // MARK: - For Landscape
    override public var shouldAutorotate: Bool {
        return false
    }
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if deviceIdiom == .phone {
            return .landscapeRight
        } else {
            return .portrait
        }
    }
    override public var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        if deviceIdiom == .phone {
            return .landscapeRight
        } else {
            return .portrait
        }
    }

}
