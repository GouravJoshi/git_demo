//
//  AgreementCommunicationTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 02/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class AgreementCommunicationTableViewCell: UITableViewCell {
    var signature: Signature? {
        didSet {
            guard let signature = self.signature else {
                return
            }
            switchSms.isOn = signature.sms
            switchPhone.isOn = signature.phone
            switchEmail.isOn = signature.email
        }
    }
    @IBOutlet weak var viewFooter: UIStackView!
    @IBOutlet weak var switchSms: UISwitch!
    @IBOutlet weak var switchPhone: UISwitch!
    @IBOutlet weak var switchEmail: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
