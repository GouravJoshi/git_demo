//
//  ServiceAgreementTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class ServiceAgreementTableViewCell: UITableViewCell {
    // MARK: - Variables
    var service : SoldServiceStandardDetail? {
        didSet {
            guard let service = service else { return }
            if let serviceDetails = AppUserDefaults.shared.masterDetails  {
                let name = serviceDetails.categories.flatMap { $0.services}.filter{$0.sysName == service.serviceSysName}.first?.name
                lblName.text = name
            } else {
                lblName.text = service.serviceSysName
            }
            lblFrequency.text = service.serviceFrequency
            lblBillAmount.text = service.totalInitialPrice?.currency
            lblmaintenancePrice.text = service.totalMaintPrice?.currency
            lblDescription.attributedText = service.serviceDescription?.trimmed.html2AttributedString
            lblSeprator.isHidden  =  lblDescription.attributedText?.string == "" || lblDescription.attributedText?.string == nil  ? true : false
        }
    }
    // MARK: - Outlets
    @IBOutlet weak var lblmaintenancePrice: UILabel!
    @IBOutlet weak var lblSeprator: UILabel!
    @IBOutlet weak var lblBillAmount: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblFrequency: UILabel!
    @IBOutlet weak var lblName: UILabel!
    // MARK: - View Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
