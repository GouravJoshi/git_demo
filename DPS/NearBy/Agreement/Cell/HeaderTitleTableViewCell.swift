//
//  HeaderTitleTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 29/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class HeaderTitleTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var lblTitle: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
