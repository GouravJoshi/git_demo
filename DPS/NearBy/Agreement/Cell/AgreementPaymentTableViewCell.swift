//
//  AgreementPaymentTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 29/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol AgreementPaymentCellDelegate: NSObjectProtocol {
    func paymentMethodPressed()
    func amountEntered(_ string: String)
}
class AgreementPaymentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewElectronicAuth: UIStackView!
    @IBOutlet weak var lblElectronicAuthStatus: UILabel!
    @IBOutlet weak var btnElcetronicAuthorization: UIButton!
    @IBOutlet var btnChargeCard: UIButton!
    @IBOutlet var lblCardDetail: UILabel!
    @IBOutlet weak var viewCardInfo: UIStackView!

    weak var delegate: AgreementPaymentCellDelegate?
    @IBOutlet weak var viewAmount: UIStackView!
    @IBOutlet weak var txtPaymentMethod: UITextField! {
        didSet {
            txtPaymentMethod.delegate = self
        }
    }
    @IBOutlet weak var txtAmount: UITextField! {
        didSet {
            txtAmount.delegate = self
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension AgreementPaymentTableViewCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtPaymentMethod {
            delegate?.paymentMethodPressed()
            return false
            
        } else {
            return true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            let characterSet = CharacterSet(charactersIn: string)
            if textField == txtAmount {
                let repeatingDecimalCount = updatedText.filter{$0 == "."}.count
                let allowedCharacters = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "."))
                
                guard allowedCharacters.isSuperset(of: characterSet) && repeatingDecimalCount <= 1 else{
                    return false
                }
                delegate?.amountEntered(updatedText)
            }
        }
        return true
    }
}
