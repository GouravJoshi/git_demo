//
//  AgreementNotesTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 29/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

enum NotesType {
    case customerN
    case internalN
}
protocol AgreementNotesCellDelegate: NSObjectProtocol {
    func updatedTextForNotes(_ notes: String, notesType: NotesType)
}
class AgreementNotesTableViewCell: UITableViewCell {

    var notesType: NotesType = .customerN
    weak var delegate: AgreementNotesCellDelegate?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtView: UITextView!

}
extension AgreementNotesTableViewCell: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        delegate?.updatedTextForNotes(newText, notesType: notesType)
        return true
    }
}
