//
//  AgreementSignatureTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 29/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol AgreementSignatureDelegate: NSObjectProtocol {
    func openSignatureViewCustomer()
    func openSignatureViewTechnician()
    func actioinCustomerNotPresent(_ isSelected: Bool)
}

class AgreementSignatureTableViewCell: UITableViewCell {
    weak var delegate: AgreementSignatureDelegate?
    @IBOutlet weak var viewRefreshStatus: UIView!
    @IBOutlet weak var lblSignatureStatus: UILabel!
    @IBOutlet weak var imageHelp: UIImageView!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var imgViewCustomerSignature: UIImageView! {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapCustomerSignature))
            imgViewCustomerSignature.addGestureRecognizer(tap)
        }
    }
    @IBOutlet weak var imgViewTechnicianSignature: UIImageView! {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapTechnicianSignature))
            imgViewTechnicianSignature.addGestureRecognizer(tap)
        }
    }
    @IBOutlet weak var viewCustomer: UIStackView!
    var signature: Signature? {
        didSet {
            imgViewCustomerSignature.image = signature?.customer
            imgViewTechnicianSignature.image = signature?.technician
            btnCheck.isSelected = signature?.customerNotPresent ?? false
            if signature?.customerNotPresent == false {
                viewCustomer.isHidden = false
            } else {
                viewCustomer.isHidden = true
            }
        }
    }
    
    @objc func tapTechnicianSignature() {
        delegate?.openSignatureViewTechnician()
    }
    @objc func tapCustomerSignature() {
        delegate?.openSignatureViewCustomer()
    }
    @IBAction func actionCustomerNotPresent(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        //signature?.customerNotPresent = sender.isSelected
        delegate?.actioinCustomerNotPresent(sender.isSelected)
    }
    
}
