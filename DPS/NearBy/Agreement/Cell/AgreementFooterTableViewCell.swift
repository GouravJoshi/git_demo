//
//  AgreementFooterTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 29/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class AgreementFooterTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var btnSendText: UIButton!
    @IBOutlet weak var btnResendText: UIButton!
    @IBOutlet weak var btnResendEmail: UIButton!
    @IBOutlet weak var btnSetupService: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnSendAgreement: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setUI() {
        let attrs = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
            NSAttributedString.Key.foregroundColor : UIColor.gray,
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        let buttonTitleStr = NSMutableAttributedString(string:"Decline", attributes:attrs)
        let attributedString = NSMutableAttributedString(string:"")
        attributedString.append(buttonTitleStr)
        btnDecline.setAttributedTitle(attributedString, for: .normal)
        
        let buttonTitleStrText = NSMutableAttributedString(string:"Resend Text", attributes:attrs)
        let attributedStringText = NSMutableAttributedString(string:"")
        attributedStringText.append(buttonTitleStrText)
        btnResendText.setAttributedTitle(attributedStringText, for: .normal)
        
        let buttonTitleStrEmail = NSMutableAttributedString(string:"Resend Email", attributes:attrs)
        let attributedStringEmail = NSMutableAttributedString(string:"")
        attributedStringEmail.append(buttonTitleStrEmail)
        btnResendEmail.setAttributedTitle(attributedStringEmail, for: .normal)
    }
}
