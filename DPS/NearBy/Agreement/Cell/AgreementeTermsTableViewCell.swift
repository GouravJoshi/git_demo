//
//  AgreementeTermsTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 29/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class AgreementeTermsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var txtViewTerms: UITextView!{
        didSet {
            txtViewTerms.text = AppUserDefaults.shared.masterDetails?.generalTermsConditions?.termsConditions?.html2String
        }
    }
    @IBOutlet weak var lblHeading: UILabel!

    @IBOutlet weak var viewCheckMark: UIStackView!
    
    var services: [SoldServiceStandardDetail] = [] {
        didSet {
            var termsConditionsText = ""
            for service in services {
                if let termsConditions = service.serviceTermsConditions?.trimmed, !termsConditions.isEmpty {
                    
                    if let serviceDetails = AppUserDefaults.shared.masterDetails  {
                        let name = serviceDetails.categories.flatMap { $0.services}.filter{$0.sysName == service.serviceSysName}.first?.name
                        termsConditionsText += "\(name ?? ""): \(termsConditions)".trimmed + "\n"
                    } else {
                        termsConditionsText += "\(service.serviceSysName ?? ""): \(termsConditions)".trimmed + "\n"
                    }
                }
            }
            termsConditions = termsConditionsText.trimmed.html2String
        }
    }
    
    var termsConditions: String? {
        didSet {
            txtViewTerms.text = termsConditions
            if let termsConditions = termsConditions , termsConditions.trimmed != "" {
            lblHeading.isHidden = false
            txtViewTerms.isHidden = false
            } else {
                lblHeading.isHidden =  true
                txtViewTerms.isHidden = true
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class AgreementeTermsTableViewCellNew: UITableViewCell {
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var txtViewTerms: UITextView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var viewCheckMark: UIStackView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setTermAndCondition(isService: Bool, services: [SoldServiceStandardDetail]) {
        var strTermsConditions = ""
        if isService {
            var termsConditionsText = ""
            for service in services {
                if let termsConditions = service.serviceTermsConditions?.trimmed, !termsConditions.isEmpty {
                    
                    if let serviceDetails = AppUserDefaults.shared.masterDetails  {
                        let name = serviceDetails.categories.flatMap { $0.services}.filter{$0.sysName == service.serviceSysName}.first?.name
                        termsConditionsText += "\(name ?? ""): \(termsConditions)".trimmed + "\n"
                    } else {
                        termsConditionsText += "\(service.serviceSysName ?? ""): \(termsConditions)".trimmed + "\n"
                    }
                }
            }
            strTermsConditions = termsConditionsText.trimmed.html2String
            txtViewTerms.text = strTermsConditions
            
        }else {
          //  strTermsConditions = termsConditions
            txtViewTerms.text = AppUserDefaults.shared.masterDetails?.generalTermsConditions?.termsConditions?.html2String
        }
        
    }
    
}
