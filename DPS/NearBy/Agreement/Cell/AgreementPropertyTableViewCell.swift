//
//  AgreementPropertyTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 29/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class AgreementPropertyTableViewCell: UITableViewCell {
    
    var propertyInfo: PropertyInfo?  {
        didSet{
            guard let propertyInfo = propertyInfo else  { return }
            if let bed = propertyInfo.bedRoom, !bed.isEmpty {
                lblBeds.text = (bed == "1" || bed == "0" ) ? "\(bed) Bed" : "\(bed) Beds"
                stackBed.isHidden = false
            } else {
                stackBed.isHidden = true
            }
            if let bath = propertyInfo.bathRoom, !bath.isEmpty {
                lblBath.text = (bath == "1" || bath == "0") ? "\(bath) Bath" : "\(bath) Baths"
                stackBath.isHidden = false
            } else {
                stackBath.isHidden = true
            }
            
            if let area = propertyInfo.area , !area.isEmpty  {
                lblSize.text = "\(area) Sq. feet"
                stackSize.isHidden = false
            }
//            if let area = Int(propertyInfo.area ?? "")?.description, !area.isEmpty, area != "0" {
//                lblSize.text = "\(area) Sq. feet"
//                stackSize.isHidden = false
//            }
            else {
                stackSize.isHidden = true
            }
            if let noOfFloor = Int(propertyInfo.noOfFloor ?? "")?.description, noOfFloor != "0", !noOfFloor.isEmpty  {
                lblNoOfFloors.text = noOfFloor
                stackFloor.isHidden = false
            } else {
                stackFloor.isHidden = true
            }
            if let type = propertyInfo.flowType {
                lblType.text = type
            } else {
                lblType.text = "Residential"
            }
        }
    }

    
    // MARK: - Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var stackPropertyType: UIStackView!
    @IBOutlet weak var lblNoOfFloors: UILabel!
    @IBOutlet weak var lblBath: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var stackBath: UIStackView!
    @IBOutlet weak var stackBed: UIStackView!
    @IBOutlet weak var lblBeds: UILabel!
    @IBOutlet weak var stackSize: UIStackView!
    @IBOutlet weak var stackFloor: UIStackView!
    @IBOutlet weak var lblType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
