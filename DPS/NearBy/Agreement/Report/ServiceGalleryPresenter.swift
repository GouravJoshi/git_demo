//
//  ServiceGalleryPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 20/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
protocol GalleryView: CommonView {
    func imageUploadedSuccessfully(uploadResponse: [UploadResponse], indexes: [Int])
    
}

class ServiceGalleryPresenter: NSObject {
    weak private var delegate: GalleryView?
    private lazy var service = {
        return ServiceGalleryService()
    }()
    
    init(_ delegate: GalleryView) {
        self.delegate = delegate
    }
    
    func apiCallForUploadingImages( parameters: [DTDLeadImagesDetailExtSerDc]) {
        
        var images: [UIImage] = []
        var indexes: [Int] = []
        for (index, parameter) in parameters.enumerated() {
            if let image = parameter.tempImage, let compressedImage = image.compressTo(expectedSizeInKb: 100) {
                indexes.append(index)
                images.append(compressedImage)
            }
        }
        service.uploadImage(images) { (object, error) in
            if let object = object {
                self.delegate?.imageUploadedSuccessfully(uploadResponse: object, indexes: indexes)
            } else {
                self.delegate?.hideLoader()
                self.delegate?.showAlert(message: error)
            }
        }
    }
}
