//
//  ServiceReportViewController.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class ServiceReportViewController: BaseViewController {
    // MARK: - Variables
    var services: [SoldServiceStandardDetail] = []
    var propertyDetails: PropertyDetails?
    var isResend = false
    var entityDetail: EntityDetail?
    var emailList:[EmailDetail] = []{
        didSet {
            updateUI()
            tableView.reloadData()
        }
    }
    var typedMail = ""
    var sendAgreement = SendAgreement()
    var sendAgreementNew = SendAgreement.shared

    var uploadResponseAddress: UploadResponse?
    var uploadResponseCustomer: UploadResponse?
    var uploadResponseTechician: UploadResponse?
    lazy var imagesToUpload: [ImageToUpload] = []
    private lazy var presenter = {
        return ServiceReportPresenter(self)
    }()
    private lazy var galleryPresenter = {
        return ServiceGalleryPresenter(self)
    }()
    private lazy var mailPresenter = {
        return WorkOrderMailPresenter(self)
    }()
    var isFromWorkOrder = false
    var scheduleResponse: AppoinmentResponse?
    var rescheduleResponse: RescheduleResponse?

    var fromReschedule = Bool()

    // MARK: - Outlets
    @IBOutlet weak var btnCreatFollow: UIButton!
    @IBOutlet weak var lblSendMailTo: UILabel!
    @IBOutlet weak var switchPrimaryMail: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtEmail: UITextField! {
        didSet {
            txtEmail.delegate = self
        }
    }
    @IBOutlet weak var lblFromMail: UILabel!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Send Sales Report"
        
        sendAgreement.soldServiceStandardDetail = services
        
        setUI()
        setData()
        updateUI()
    }
    
    private func setUI() {
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strServiceReportType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
        
        if strServiceReportType == "CompanyEmail" {
            lblFromMail.text = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
        }
        else {
            lblFromMail.text  = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        }
        
        if let emailDetail = entityDetail?.emailDetail
        {
            self.emailList = emailDetail
        }
        else if let emailList = propertyDetails?.propertyInfo?.email, !emailList.trimmed.isEmpty {
            let emails = emailList.trimmed.components(separatedBy: ",")
            for email in emails
            {
                let mail = createMailObject(email.trimmed)
                self.emailList.insert(mail, at: 0)
            }
        }
    }
    private func setData()
    {
        guard  let emailList = entityDetail?.emailDetail  else {
            return
        }
        self.emailList = emailList
    }
    private func updateUI() {
        if emailList.isEmpty  {
            lblSendMailTo.isHidden = true
        } else {
            lblSendMailTo.isHidden = false
        }
    }
    private func apiCallForReSendMail() {
        var parameter = SendAgreement()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId") ?? "")"
        if let entityDetail = entityDetail {
            
            parameter.leadId = entityDetail.leadDetail?.leadID
            parameter.leadNumber = entityDetail.leadDetail?.leadNumber
            parameter.isProposalFromMobile = entityDetail.leadDetail?.isProposalFromMobile
            parameter.documentsDetail = entityDetail.documentDetail
            parameter.emailDetail = emailList
            parameter.companyId = entityDetail.leadDetail?.companyID?.description ?? companyId
        } 
        presenter.apiCallForReSendMail(parameter)
    }
    private func apiCallToNotifyCustomer()
    {
        if fromReschedule
        {
            guard let scheduleResponse = rescheduleResponse else { return }
            
            print(emailList)
            
            if emailList.count > 0
            {
                var parameter = NotifyCustomerRequest()
                parameter.companyKey = scheduleResponse.companyKey
                parameter.workOrderNo = scheduleResponse.workOrderNo?.description
                //let emails = emailList.trimmed.components(separatedBy: ",")
                let emails = emailList.compactMap({$0.emailID}).joined(separator: ",").trimmed
                let emailArr = emails.components(separatedBy: ",")
                parameter.toEmails = emailArr
                mailPresenter.apiCallForNotify(parameter)
            }
        }
        else
        {
            guard let scheduleResponse = scheduleResponse else { return }
            
            print(emailList)
            
            if emailList.count > 0
            {
                var parameter = NotifyCustomerRequest()
                parameter.companyKey = scheduleResponse.companyKey
                parameter.workOrderNo = scheduleResponse.workOrderNo?.description
                //let emails = emailList.trimmed.components(separatedBy: ",")
                let emails = emailList.compactMap({$0.emailID}).joined(separator: ",").trimmed
                let emailArr = emails.components(separatedBy: ",")
                parameter.toEmails = emailArr
                mailPresenter.apiCallForNotify(parameter)
            }
        }
       
    }
    
    private func setIsAgreementGenerated() {
        
        let statusL = "\(entityDetail?.leadDetail?.statusSysName ?? "")"
        let stageL = "\(entityDetail?.leadDetail?.stageSysName ?? "")"

        if statusL.caseInsensitiveCompare("complete") == .orderedSame  &&
            stageL.caseInsensitiveCompare("Won") == .orderedSame{
            
            sendAgreement.leadDetail?.isAgreementGenerated = true
            
        }

    }
    // MARK: - Actions
    @IBAction func actionBack(_ sender: Any)
    {
        
        self.setIsAgreementGenerated()
        sendAgreementNew.emailDetail = emailList
        self.navigationController?.popViewController(animated: false)

    }
    @objc private func actionSelectMail(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func actionCancel(_ sender: Any) {
        if isFromWorkOrder {
            self.navigationController?.popViewController(animated: false)
        } else {
        if isResend {
            self.navigationController?.popViewController(animated: true)
            
        } else {
            for (index, email) in emailList.enumerated() {
                var emailT = email
                emailT.isMailSent = false
                emailList[index] = emailT
            }
            sendAgreement.emailDetail = emailList
            sendAgreement.leadDetail?.isMailSend =  false
            showLoader()
            if imagesToUpload.isEmpty {
                presenter.apiCallForSendMail(sendAgreement)
            } else {
                presenter.apiCallForUploadSignature(type: imagesToUpload.first!.type, image: imagesToUpload.first!.image, fileName: imagesToUpload.first!.fileName)
            }
        }
        }
    }
    @IBAction func actionSendMail(_ sender: Any) {
        
        self.setIsAgreementGenerated()

        guard emailList.count > 0 else {
            showAlert(message: "Please add email to send email")
            return
        }
        if isFromWorkOrder
        {
            apiCallToNotifyCustomer()
        }
        else
        {
        sendAgreement.emailDetail = emailList
        let emails = emailList.filter({ (email) -> Bool in
            email.isAddToProfile == true && email.isMailSent == true
        })
        let primaryMail =  emails.compactMap({$0.emailID}).joined(separator: ",").trimmed
        sendAgreement.primaryEmail = primaryMail
        sendAgreement.leadDetail?.isMailSend =  true
        if isResend {
            apiCallForReSendMail()
        } else {
            showLoader()
            if imagesToUpload.isEmpty {
                if sendAgreement.leadDetail?.isCustomerNotPresent == true {
                    sendAgreement.paymentInfo?.customerSignature = ""
                }
                presenter.apiCallForSendMail(sendAgreement)
            } else {
                presenter.apiCallForUploadSignature(type: imagesToUpload.first!.type, image: imagesToUpload.first!.image, fileName: imagesToUpload.first!.fileName)
            }
        }
        }
    }
    
    @IBAction func actionCreateFollowup(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        if let destination = storyboard.instantiateViewController(withIdentifier: "CreatTaskViewController") as? CreatTaskViewController {
            destination.propertyDetails = propertyDetails
            self.present(destination, animated: true, completion: nil)
        }
    }
    @IBAction func actionAdd(_ sender: Any) {
        guard !typedMail.isEmpty  else {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter email", viewcontrol: self)
            return
        }
        guard typedMail.isValidEmail()  else {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid email", viewcontrol: self)
            return
        }
        let mail = createMailObject()
        guard !emailList.contains(mail) else {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Email already exist", viewcontrol: self)
            return
        }
        
        emailList.insert(mail, at: 0)

        txtEmail.text = ""
        typedMail = ""
        tableView.reloadData()
        
    }
    private func createMailObject(_ emailUser: String? = nil) -> EmailDetail {
        var email = EmailDetail()
        if let mail = emailUser , !mail.trimmed.isEmpty {
            email.emailID = mail
        } else {
            email.emailID = typedMail
        }
        email.isMailSent = true
        email.createdDate = AppUtility.convertDateToString(Date(), toFormat: ConstantV.modifiedDateFormat)
        email.isAddToProfile = switchPrimaryMail.isOn
        return email
    }
    // MARK: - Action
    @objc func actionEmailSelected(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        emailList[sender.tag].isMailSent = sender.isSelected
    }
}

// MARK: - UITableViewDataSource
extension ServiceReportViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emailList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ServiceScheduleTableViewCell.self), for: indexPath) as! ServiceScheduleTableViewCell
        
        cell.btnSelectMail.tag = indexPath.row
        cell.btnSelectMail.addTarget(self, action: #selector(actionEmailSelected(_:)), for: .touchUpInside)
        let email = emailList[indexPath.row]
        cell.lblEmail.text = email.emailID
        cell.btnSelectMail.isSelected = email.isMailSent ?? false
        cell.lblEmail.textColor = email.isAddToProfile ?? false ? .appThemeColor : .gray
        // cell.btnSelectMail.addTarget(self, action: #selector(actionSelectMail), for: .touchUpInside)
        return cell
    }
}
// MARK: - UITableViewDelegate
extension ServiceReportViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension ServiceReportViewController: ServiceReportView {
    func uploadImage(type: UploadType, uploadResponse: UploadResponse) {
        if type == .address {
            uploadResponseAddress = uploadResponse
        } else if type == .customer {
            uploadResponseCustomer = uploadResponse
        } else if type == .technician {
            uploadResponseTechician = uploadResponse
        }
        if let index = imagesToUpload.firstIndex(where: { (image) -> Bool in
            return image.type == type
        }) {
            imagesToUpload.remove(at: index)
            if !imagesToUpload.isEmpty {
                presenter.apiCallForUploadSignature(type: imagesToUpload.first!.type, image: imagesToUpload.first!.image, fileName: imagesToUpload.first!.fileName)
            } else {
                if let uploadResponse = uploadResponseAddress {
                    sendAgreement.addressImagePath = uploadResponse.name
                }
                if let uploadResponse = uploadResponseCustomer {
                    sendAgreement.paymentInfo?.customerSignature = uploadResponse.name
                }
                if let uploadResponse = uploadResponseTechician {
                    sendAgreement.paymentInfo?.salesSignature = uploadResponse.name
                }
                if sendAgreement.leadDetail?.isCustomerNotPresent == true {
                    sendAgreement.paymentInfo?.customerSignature = ""
                } else if sendAgreement.leadDetail?.isCustomerNotPresent == false,  sendAgreement.paymentInfo?.customerSignature?.trimmed == "" {
                    hideLoader()
                    showAlert(message: "Customer signature is not been uploaded, please try again later")
                    return
                }
                if let galleryImages = propertyDetails?.galleryImages, galleryImages.count > 0  {
                    
                    let images =  galleryImages.filter({$0.tempImage != nil })
                    if !images.isEmpty {
                        galleryPresenter.apiCallForUploadingImages(parameters: galleryImages)
                    } else {
                        sendAgreement.imagesDetail = galleryImages
                        presenter.apiCallForSendMail(sendAgreement)
                    }
                } else {
                    presenter.apiCallForSendMail(sendAgreement)
                }
            }
        } else {
            hideLoader()
        }
    }
    func mailSendSuccessfully(_ output: EntityDetail) {
        if output.leadDetail?.leadNumber != nil {
            self.entityDetail = output
            
            if fromReschedule
            {
                /*for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: ScheduleViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }*/
                
                
                var isVCFound = false
                var controllerVC = UIViewController()
                for controller in self.navigationController!.viewControllers as Array { if controller.isKind(of: ScheduleViewController.self) {
                    
                    isVCFound = true
                    controllerVC = controller
               
                }
                }
                if isVCFound == true {
                    self.navigationController?.popToViewController(controllerVC, animated: false)
                }
                
                
            }
            else
            {
                //Nilind
    //            self.entityDetail?.webLeadId = output.webLeadId
    //            self.entityDetail?.leadNumber = output.leadNumber
    //            self.entityDetail?.thirdPartyAccountId = output.thirdPartyAccountId
    //            self.entityDetail?.billToLocationId = output.billToLocationId
                
                NotificationCenter.default.post(name: .didUCreatedLead, object: propertyDetails)
                self.performSegue(withIdentifier: "UnwindToAgreement", sender: self)
            }

        }
        else
        {
            self.hideLoader()
            self.showAlert(message: "Unable to send the Agreement, please try again later")
        }
    }
    func resendAgreementSuccessfully(_ output: Bool) {
        if output == true {
            NotificationCenter.default.post(name: .didUCreatedLead, object: propertyDetails)
            self.performSegue(withIdentifier: "UnwindToAgreement", sender: self)
        } else {
            self.hideLoader()
            showAlert(message: "Unable to resend the Agreement, please try again later")
        }
    }
}
extension ServiceReportViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            typedMail = text.replacingCharacters(in: textRange,
                                                 with: string)
        }
        return true
    }
}
//MARK:- GalleryView
extension ServiceReportViewController: GalleryView {
    func imageUploadedSuccessfully(uploadResponse: [UploadResponse], indexes: [Int]) {
        var galleryImages = propertyDetails!.galleryImages
        for (index, response) in uploadResponse.enumerated() {
            if  indexes.count > index {
                let uploadIndex = indexes[index]
                if galleryImages.count > uploadIndex {
                    galleryImages[uploadIndex].leadImagePath = (response.relativePath ?? "") + (response.name ?? "")
                }
            }
        }
        sendAgreement.imagesDetail = galleryImages
        presenter.apiCallForSendMail(sendAgreement)
    }
}
extension ServiceReportViewController: WorkOrderMailView {
    func notifiedSuccessfully(_ object: Bool) {
        if object
        {
            if fromReschedule
            {
                /*for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: ScheduleViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: false)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadViewSetup_Notification"), object: nil, userInfo: nil)
                        break
                    }
                }*/
                
                var isVCFound = false
                var controllerVC = UIViewController()
                for controller in self.navigationController!.viewControllers as Array { if controller.isKind(of: ScheduleViewController.self) {
                    
                    isVCFound = true
                    controllerVC = controller
               
                }
                }
                if isVCFound == true {
                    self.navigationController?.popToViewController(controllerVC, animated: false)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadViewSetup_Notification"), object: nil, userInfo: nil)
                }
                
            }
            else
            {
                if nsud.bool(forKey: "IsScheduleFromViewSchedule") == true
                {
                    /*for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: ScheduleViewController.self) {
                            self.navigationController!.popToViewController(controller, animated: false)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadViewSetup_Notification"), object: nil, userInfo: nil)
                            break
                        }
                    }*/
                    
                    var isVCFound = false
                    var controllerVC = UIViewController()
                    for controller in self.navigationController!.viewControllers as Array { if controller.isKind(of: ScheduleViewController.self) {
                        
                        isVCFound = true
                        controllerVC = controller
                   
                    }
                    }
                    if isVCFound == true {
                        self.navigationController?.popToViewController(controllerVC, animated: false)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadViewSetup_Notification"), object: nil, userInfo: nil)
                    }
                    
                    nsud.setValue(false, forKey: "IsScheduleFromViewSchedule")
                    nsud.synchronize()
                    
                }
                else
                {
                    self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)

                }

            }
        }
        else
        {
            showAlertWithCompletion(message: "Unable to send Email") {
           
            }
        }
    }
}
