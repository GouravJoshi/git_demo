//
//  ServiceGalleryService.swift
//  DPS
//
//  Created by Vivek Patel on 20/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class ServiceGalleryService {
    
    func uploadImage(_ images: [UIImage], _ callBack:@escaping (_ object: [UploadResponse]?,_ error:String?) -> Void) {
        
        let url = URL.Gallery.uploadImages
       
        AF.upload(multipartFormData: { multiPart in
            for image in images {
                if let imageData = image.jpegData(compressionQuality: 1.0) {
                    let timeStamp  = Date().currentTimeMillis()
                    multiPart.append(imageData, withName: "filename", fileName: "\(String.random())-\(timeStamp).jpeg", mimeType: "image/jpeg")
                }
            }
            
        }, to: url)
        .responseDecodable(of: [UploadResponse].self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
        .responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print(value)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func uploadImageForGeneralInfo(_strServiceUrlMainServiceAutomation: String, _ images: [UIImage], _ imagesNames: [String], _ callBack:@escaping (_ object: [UploadResponse]?,_ error:String?) -> Void) {
        
        let url =  _strServiceUrlMainServiceAutomation + UrlServiceGeneralInfoImageUploadAsync
       
        AF.upload(multipartFormData: { multiPart in
            
            for k1 in 0 ..< images.count {
                
                let strImgName = Global().splitString("\(imagesNames[k1])", "\\")
                
                if let imageData = images[k1].jpegData(compressionQuality: 1.0) {
                    multiPart.append(imageData, withName: "filename", fileName: "\(strImgName ?? "")", mimeType: "image/jpeg")
                }
            }
            /*for image in images {
                if let imageData = image.jpegData(compressionQuality: 1.0) {
                    let timeStamp  = Date().currentTimeMillis()
                    multiPart.append(imageData, withName: "filename", fileName: "\(String.random())-\(timeStamp).jpeg", mimeType: "image/jpeg")
                }
            }*/
            
        }, to: url)
        .responseDecodable(of: [UploadResponse].self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
        .responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print(value)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func uploadImageSales(strUrl: String, _ images: [UIImage], _ imagesNames: [String], _ callBack:@escaping (_ object: [UploadResponse]?,_ error:String?) -> Void) {
        
        let url =  strUrl
       
        AF.upload(multipartFormData: { multiPart in
            
            for k1 in 0 ..< images.count {
                
                let strImgName = Global().splitString("\(imagesNames[k1])", "\\")
                
                if let imageData = images[k1].jpegData(compressionQuality: 1.0) {
                    multiPart.append(imageData, withName: "filename", fileName: "\(strImgName ?? "")", mimeType: "image/jpeg")
                }
            }
            /*for image in images {
                if let imageData = image.jpegData(compressionQuality: 1.0) {
                    let timeStamp  = Date().currentTimeMillis()
                    multiPart.append(imageData, withName: "filename", fileName: "\(String.random())-\(timeStamp).jpeg", mimeType: "image/jpeg")
                }
            }*/
            
        }, to: url)
        .responseDecodable(of: [UploadResponse].self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
        .responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print(value)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    func uploadImageSalesOld(strImageName : String , strUrll : String , _ callBack:@escaping (_ object: Any?,_ error: Any?) -> Void) {
        
        if fileAvailableAtPath(strname: strImageName) {
            
            
            let imagee = getImagefromDirectory(strname: strImageName)
            
            let dataToSend = imagee.jpegData(compressionQuality: 1.0)!
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strImageName, url: strUrll) { (responce, status) in
                
                if(status)
                {
                    debugPrint(responce)
                    
                    if((responce.value(forKey: "data")is NSDictionary) || (responce.value(forKey: "data")is NSArray))
                    {
                        callBack(responce, status)
                    }
                }
            }
        }
    }
}
