//
//  ServiceReportModel.swift
//  DPS
//
//  Created by Vivek Patel on 10/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

// MARK: - SendAgreement
struct SendAgreement: Codable {
    
    static let shared =  SendAgreement()
    
    var companyKey, title: String?
    var webLeadID, accountID: Int?
    var thirdPartyAccountNumber: String?
    var crmContactID, crmCompanyID: Int?
    var createNewAccount: Bool?
    var flowType, branchSysName, departmentSysName, firstName: String?
    var lastName, primaryEmail, cellPhone1: String?
    var customerAddressID: Int?
    var address1, address2, cityName, zipcode: String?
    var stateID, countryID: Int?
    var county, schoolDistrict, addressSubType, taxSysName: String?
    var sourceIDS: [Int]?
    var visitStatusID: Int?
    var visitDescription: String?
    var addressImagePath, area, noofBedroom, noofBathroom: String?
    var noofStory, assignedToType, comment: String?
    var assignedToID: String?
    var webLeadServices: [WebLeadService]?
    var primaryServiceID: Int?
    var additionalServiceIDS: [Int]?
    var industryID: Int?
    var fieldSalesPerson: String?
    var scheduleDate, scheduleTime: String?
    var paymentInfo: PaymentInfo?
    var documentsDetail: [DocumentDetail]?
    var leadDetail: LeadDetail?
    var emailDetail: [EmailDetail]?
    var soldServiceStandardDetail: [SoldServiceStandardDetail]?
    var leadNumber: String?
    var isProposalFromMobile: Bool?
    var leadId: Int?
    var companyId: String?
    var leadContactDetailEXTSerDc: [LeadContactDetailEXTSerDc]?
    var currentServicesProvider: String?
    var visitCount: Int?
    var objectionIds: [Int]?
    var billingAddressSameAsService: Bool?
    var billingPOCId: Int?
    var billingFirstName: String?
    var billingLastName: String?
    var billingPrimaryEmail: String?
    var billingCellPhone1: String?
    var billingAddress1: String?
    var billingAddress2: String?
    var billingCityName: String?
    var billingStateId: Int?
    var billingCountryId: String?
    var billingZipcode: String?
    var billingState: String?
    var billingCountry: String?
    var tempFullBillingAddress: String?
    var state: String?
    var country: String?
    var electronicAuthorizationForm: ElectronicAuthorizationForm?
    var imagesDetail: [DTDLeadImagesDetailExtSerDc]?
    var cancelReasonMasterId: Int?
    var cancelReasonDescription: String?
    var leadAppliedDiscounts: [LeadAppliedDiscount]?
    var latitude: Double?
    var longitude: Double?
    
    //Nilind
    var secondCall: Bool?
    
    enum CodingKeys: String, CodingKey {
        case companyKey = "CompanyKey"
        case title = "Title"
        case webLeadID = "WebLeadId"
        case accountID = "AccountId"
        case thirdPartyAccountNumber = "ThirdPartyAccountNumber"
        case leadNumber = "LeadNumber"
        case crmContactID = "CrmContactId"
        case crmCompanyID = "CrmCompanyId"
        case createNewAccount = "CreateNewAccount"
        case flowType = "FlowType"
        case branchSysName = "BranchSysName"
        case departmentSysName = "DepartmentSysName"
        case firstName = "FirstName"
        case lastName = "LastName"
        case primaryEmail = "PrimaryEmail"
        case cellPhone1 = "CellPhone1"
        case customerAddressID = "CustomerAddressId"
        case address1 = "Address1"
        case address2 = "Address2"
        case cityName = "CityName"
        case zipcode = "Zipcode"
        case stateID = "StateId"
        case countryID = "CountryId"
        case county = "County"
        case schoolDistrict = "SchoolDistrict"
        case addressSubType = "AddressSubType"
        case taxSysName = "TaxSysName"
        case sourceIDS = "SourceIds"
        case visitStatusID = "VisitStatusId"
        case visitDescription = "VisitDescription"
        case visitCount = "VisitCount"
        case addressImagePath = "AddressImagePath"
        case area = "Area"
        case noofBedroom = "NoofBedroom"
        case noofBathroom = "NoofBathroom"
        case noofStory = "NoofStory"
        case assignedToID = "AssignedToId"
        case assignedToType = "AssignedToType"
        case comment = "Comment"
        case webLeadServices = "WebLeadServices"
        case primaryServiceID = "PrimaryServiceId"
        case additionalServiceIDS = "AdditionalServiceIds"
        case industryID = "IndustryId"
        case fieldSalesPerson = "FieldSalesPerson"
        case scheduleDate = "ScheduleDate"
        case scheduleTime = "ScheduleTime"
        case paymentInfo = "PaymentInfo"
        case documentsDetail = "DocumentsDetail"
        case leadDetail = "LeadDetail"
        case emailDetail = "EmailDetail"
        case soldServiceStandardDetail = "SoldServiceStandardDetail"
        case isProposalFromMobile = "IsProposalFromMobile"
        case leadId = "LeadId"
        case companyId = "CompanyId"
        case leadContactDetailEXTSerDc = "LeadContactDetailExtSerDc"
        case currentServicesProvider = "CurrentServicesProvider"
        case objectionIds = "ObjectionIds"
        case billingAddressSameAsService = "BillingAddressSameAsService"
        case billingPOCId = "BillingPOCId"
        case billingFirstName = "BillingFirstName"
        case billingLastName = "BillingLastName"
        case billingPrimaryEmail = "BillingPrimaryEmail"
        case billingCellPhone1 = "BillingCellPhone1"
        case billingAddress1 = "BillingAddress1"
        case billingAddress2 = "BillingAddress2"
        case billingCityName = "BillingCityName"
        case billingStateId = "BillingStateId"
        case billingCountryId = "BillingCountryId"
        case billingZipcode = "BillingZipcode"
        case billingState = "BillingState"
        case billingCountry = "BillingCountry"
        case state = "State"
        case country = "Country"
        case electronicAuthorizationForm = "ElectronicAuthorizationForm"
        case imagesDetail = "ImagesDetail"
        case cancelReasonMasterId = "CancelReasonMasterId"
        case cancelReasonDescription = "CancelReasonDescription"
        case leadAppliedDiscounts = "LeadAppliedDiscounts"
        //Nilind
        case secondCall = "SecondCall"
        case latitude = "Latitude"
        case longitude = "Longitude"
        
    }
    init() {}
}

// MARK: - WebLeadService
struct WebLeadService: Codable {
    var serviceID: Int?
    var isPrimary: Bool?
    
    enum CodingKeys: String, CodingKey {
        case serviceID = "ServiceId"
        case isPrimary = "IsPrimary"
    }
}

// MARK: - PaymentInfoEXTSerDc
struct PaymentInfoEXTSerDc: Codable {
    var leadPaymentDetailID: Int?
    var paymentMode: String?
    var amount: Double?
    var checkNo, licenseNo, expirationDate, specialInstructions: String?
    var agreement, proposal: String?
    var customerSignature: String?
    var salesSignature, inspection: String?
    var checkFrontImagePath, checkBackImagePath, proposalAdditionalNotes: String?
    var createdDate: String?
    var createdBy: Int?
    //  var modifiedDate: String?
    //  var modifiedBy: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case leadPaymentDetailID = "LeadPaymentDetailId"
        case paymentMode = "PaymentMode"
        case amount = "Amount"
        case checkNo = "CheckNo"
        case licenseNo = "LicenseNo"
        case expirationDate = "ExpirationDate"
        case specialInstructions = "SpecialInstructions"
        case agreement = "Agreement"
        case proposal = "Proposal"
        case customerSignature = "CustomerSignature"
        case salesSignature = "SalesSignature"
        case inspection = "Inspection"
        case checkFrontImagePath = "CheckFrontImagePath"
        case checkBackImagePath = "CheckBackImagePath"
        case proposalAdditionalNotes = "ProposalAdditionalNotes"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        // case modifiedDate = "ModifiedDate"
        //case modifiedBy = "ModifiedBy"
    }
}
