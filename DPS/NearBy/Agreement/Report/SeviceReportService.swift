//
//  SeviceReportService.swift
//  DPS
//
//  Created by Vivek Patel on 10/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire
class  SeviceReportService {
    func sendMail(parameters: SendAgreement, _ callBack:@escaping (_ object: EntityDetail?,_ error:String?) -> Void) {
        let url = URL.ServiceReport.sendMail
        print(url)
        //AF.sessionConfiguration.timeoutIntervalForRequest = 600
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: EntityDetail.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
            
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
            do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode(EntityDetail.self, from: data)
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
    func resendMail(parameters: SendAgreement, _ callBack:@escaping (_ object: Bool?,_ error:String?) -> Void) {
        let url = URL.ServiceReport.resendMail
        
        if #available(iOS 13.0, *) {

            AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: Bool.self) { response in
                switch response.result{
                case let .success(result):
                    callBack(result,nil)
                    break
                case let .failure(error):
                    callBack(nil, error.localizedDescription)
                    break
                }
            }.responseJSON { (dataResponse) in
                switch dataResponse.result{
                case .success(let value):
                    print(value)
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
            
        } else {

            //  For iOS 12 and below devices
            AF.request(url, method: .post, parameters: parameters.dictionary, encoding: JSONEncoding.default, headers: URL.headers).responseJSON { (response:AFDataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    callBack(true,nil)
                    break
                case .failure(_):
                    print(response.error ?? 0)
                    callBack(false, alertSomeError)
                    break
                }
            }
            
        }
  
    }
    func uploadImage( image: UIImage?, fileName: String?, type: UploadType, callBack:@escaping (_ object: [UploadResponse]?,_ error:String?) -> Void) {
        
        var url = URL.Signature.upload
        
        if type == .address {
            url = URL.LeadD2D.uploadImage
        }
        if type == .cheque {
            url = URL.Signature.uploadCheque
        }
        if type == .eafSign {
            url = URL.Signature.uploadEAF
        }
        
        let compressedImage = image?.compressTo(expectedSizeInKb: 100)
        AF.upload(multipartFormData: { multiPart in
            if let imageData = compressedImage?.jpegData(compressionQuality: 0.6) {
                multiPart.append(imageData, withName: "filename", fileName: fileName, mimeType: "image/jpeg")
            }
        }, to: url){ (urlRequest: inout URLRequest) in
            urlRequest.timeoutInterval = 180
        }
        .responseDecodable(of: [UploadResponse].self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
        .responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
}
