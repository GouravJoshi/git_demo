//
//  ServiceReportPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 10/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

protocol ServiceReportView: CommonView {
    func mailSendSuccessfully(_ output: EntityDetail)
    func resendAgreementSuccessfully(_ output: Bool)
    func uploadImage(type: UploadType, uploadResponse: UploadResponse)
}

class ServiceReportPresenter: NSObject {
    weak private var delegate: ServiceReportView?
    private lazy var service = {
        return SeviceReportService()
    }()
    
    init(_ delegate: ServiceReportView) {
        self.delegate = delegate
    }
    
    func apiCallForSendMail( _ parameters: SendAgreement, isDecline:Bool? = false) {
        if isDecline ?? false {
            self.delegate?.showLoader()
        }
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        service.sendMail(parameters: parameters) { (object, error) in
            if isDecline ?? false {
                self.delegate?.hideLoader()
            }
            if let object = object{
                self.delegate?.mailSendSuccessfully(object)
            } else {
                self.delegate?.hideLoader()
                self.delegate?.showAlert(message: error)
            }
        }
    }
    
    func apiCallForUploadSignature(type: UploadType, image: UIImage, fileName: String) {
        
        service.uploadImage(image: image, fileName: fileName, type: type) { (object, error) in
            
            if let object = object?.first{
                self.delegate?.uploadImage(type: type, uploadResponse: object)
            } else {
                self.delegate?.hideLoader()
                self.delegate?.showAlert(message: error)
            }
        }
    }
    func apiCallForReSendMail( _ parameters: SendAgreement) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        self.delegate?.showLoader()
        service.resendMail(parameters: parameters) { (object, error) in
            if let object = object {
                self.delegate?.resendAgreementSuccessfully(object)
            } else {
                self.delegate?.hideLoader()
                self.delegate?.showAlert(message: error)
            }
        }
    }
}

