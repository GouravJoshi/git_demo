//
//  WorkOrderMailPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 18/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
import Foundation
protocol WorkOrderMailView: CommonView {
    func notifiedSuccessfully(_ object: Bool)
}

class WorkOrderMailPresenter: NSObject {
    weak private var delegate: WorkOrderMailView?
    private lazy var service = {
        
        return AppoinmentService()
    }()
    
    init(_ delegate: WorkOrderMailView) {
        self.delegate = delegate
    }
    
    func apiCallForNotify(_ parameters: NotifyCustomerRequest) {
        //delegate?.showLoader()
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        service.notifyCustomer(parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.notifiedSuccessfully(object)
            }
        }
    }
}
