//
//  ServiceScheduleTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ServiceScheduleTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnSelectMail: UIButton!
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
