//
//  ServiceAgreementPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 12/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
protocol ServiceAgreementView: CommonView {
    func uploadImage(type: UploadType, uploadResponse: UploadResponse)
    func gotSigntureStatus(response : RefreshCustomerSignatureModel)
    //func gotSigntureStatus()

}

class ServiceAgreementPresenter: NSObject {
    weak private var delegate: ServiceAgreementView?
    private lazy var service = {
        return SendAgreementService()
    }()
    
    init(_ delegate: ServiceAgreementView) {
        self.delegate = delegate
    }
    
    func apiCallForRefreshSignatureStatus(_ leadId: String, companyKey: String)
    {
        delegate?.showLoader()
        service.getRefreshSignatureStatus(leadId: leadId , companyKey:companyKey) { (object, error) in
           
            self.delegate?.hideLoader()
            if let object = object
            {
                self.delegate?.gotSigntureStatus(response: object)
                //self.delegate?.gotSigntureStatus(response: object)
                //self.delegate?.callBackOnSignature()
                //self.delegate?.gotSigntureStatus(response: object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    
    func apiCallForUploadSignature(type: UploadType, image: UIImage, fileName: String) {
        
        service.uploadImage(image: image, fileName: fileName, type: type) { (object, error) in
            
            if let object = object?.first{
                self.delegate?.uploadImage(type: type, uploadResponse: object)
            } else {
                self.delegate?.hideLoader()
                self.delegate?.showAlert(message: error)
            }
        }
    }
}
