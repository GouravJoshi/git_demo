//
//  SendAgreementService.swift
//  DPS
//
//  Created by Vivek Patel on 12/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class SendAgreementService {
    
    func uploadImage( image: UIImage?, fileName: String?, type: UploadType, callBack:@escaping (_ object: [UploadResponse]?,_ error:String?) -> Void) {
        let url = URL.Signature.upload
        let compressedImage = image?.compressTo(expectedSizeInKb: 100)
        AF.upload(multipartFormData: { multiPart in
            if let imageData = compressedImage?.jpegData(compressionQuality: 0.6) {
                multiPart.append(imageData, withName: "filename", fileName: fileName, mimeType: "image/jpeg")
            }
        }, to: url){ (urlRequest: inout URLRequest) in
            urlRequest.timeoutInterval = 180
        }
        .responseDecodable(of: [UploadResponse].self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
        .responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    func uploadImageEAF( image: UIImage?, fileName: String?, type: UploadType, callBack:@escaping (_ object: [UploadResponse]?,_ error:String?) -> Void) {
        let url = URL.Signature.uploadEAF
        let compressedImage = image?.compressTo(expectedSizeInKb: 100)
        AF.upload(multipartFormData: { multiPart in
            if let imageData = compressedImage?.jpegData(compressionQuality: 0.6) {
                multiPart.append(imageData, withName: "filename", fileName: fileName, mimeType: "image/jpeg")
            }
        }, to: url){ (urlRequest: inout URLRequest) in
            urlRequest.timeoutInterval = 180
        }
        .responseDecodable(of: [UploadResponse].self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
        .responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    func getRefreshSignatureStatus(leadId: String, companyKey: String, _ callBack:@escaping (_ object: RefreshCustomerSignatureModel?,_ error:String?) -> Void)
    {
        let url = URL.RefreshSignature.refreshSignatureUrl.replacingOccurrences(of: "(LeadId)", with: leadId).replacingOccurrences(of: "(CompanyKey)", with: companyKey)

        AF.request(url, method: .get) .responseDecodable(of: RefreshCustomerSignatureModel.self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode(RefreshCustomerSignatureModel.self, from: data)
                    print(object)
                    
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
   
}
