//
//  ServiceSetupViewController.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan.All rights reserved.
////  Copyright © 2020 Saavan. All rights reserved.


import UIKit

enum ListOptions {
    case more
    case less
}
final class ServiceSetupViewController: BaseViewController {
    
    // MARK: - Variable
    var services: [SoldServiceStandardDetail] = []
    var propertyDetails: PropertyDetails?
    var entityDetail: EntityDetail?
    private var iSelectedIndex = -1
    private var selectedRoute: Route?
    private var employees: [Employee] = [] {
        didSet{
            availabilities = employees.flatMap { ($0.availability ?? [])}
        }
    }
    
    private var availabilities: [Availability] = [] {
        didSet{
            updateUI()
        }
    }
    private lazy var presenter = {
        return ServiceSetupPresenter(self)
    }()
    
    lazy var states: [State] = []
    private var routeResponse: RouteResponse?
    private var rowsCount = 0
    private var listOptions: ListOptions = .more {
        didSet {
            switch listOptions {
            case .more:
                lblOptionns.text = "More Options"
            default:
                lblOptionns.text = "Less Options"
            }
        }
    }
    private var selectedDate: Date?
    var fromReschedule = Bool()
    var strTimeRangeId = String()
    var strTimeRangeValue = String()
    var strScheduleDateTime = String()
    var strReScheduleDate = String()
    var strReScheduleTime = String()
    var strWorkOrderId = Int()
    var timeRangeIdL = Int()
    
 
    // MARK: - Outltes
    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCustomOption: UIButton!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var viewAddress: UIStackView!
    @IBOutlet var images: [UIImageView]!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewEmail: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var viewMoreOptions: UIView!
    @IBOutlet weak var lblOptionns: UILabel!
    @IBOutlet weak var viewPhone: UIStackView!
    @IBOutlet weak var viewName: UIStackView!
    @IBOutlet weak var imgProperty: UIImageView!
    @IBOutlet weak var imgEmail: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imgEmail, color: .appDarkGreen)
        }
    }
    @IBOutlet weak var imgAddress: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imgAddress, color: .appDarkGreen)
        }
    }
    @IBOutlet weak var imgPhone: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imgPhone, color: .appDarkGreen)
            
        }
    }
    @IBOutlet weak var imgName: UIImageView!{
        didSet {
            AppUtility.imageColor(image: imgName, color: .appDarkGreen)
            
        }
    }
    
    @IBOutlet weak var imgArrow: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imgArrow, color: .appThemeColor)
        }
    }
    
    @IBOutlet weak var imgViewDrop: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imgViewDrop, color:UIColor.black)
        }
    }
    @IBOutlet weak var imgCalender: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imgCalender, color: .appThemeColor)
            
        }
    }
    
    @IBOutlet var viewScheduleDate: UIView!
    @IBOutlet var lblScheduleDateTime: UILabel!
    
    @IBOutlet var viewTimeRange: UIView!
    
    @IBOutlet var btnTimeRange: UIButton!
    
    @IBOutlet var btnHome: UIBarButtonItem!
    
    var aryTimeRange = NSArray()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Service Setup"
        
        if fromReschedule
        {
            viewScheduleDate.isHidden = false
            viewTimeRange.isHidden = false
            btnHome.isEnabled = false
            // change for showing Timme Range Id
            
            if (timeRangeIdL != 0)  {
                let aryTemp = aryTimeRange.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "RangeofTimeId")!)".lowercased().contains("\(timeRangeIdL)")
                }
                
                if(aryTemp.count != 0){
                    var dict = NSDictionary()
                    dict = aryTemp[0] as! NSDictionary
                    let StartInterval = "\(dict.value(forKey: "StartInterval")!)"
                    let EndInterval = "\(dict.value(forKey: "EndInterval")!)"
                    let strrenge = StartInterval + " - " + EndInterval
                    //cell.lblTimeRange.text = "Time Range " + strrenge
                    self.btnTimeRange.setTitle(strrenge, for: .normal)
                    self.strTimeRangeId = "\(timeRangeIdL)"
                    self.strTimeRangeValue = strrenge
                }else{
                    self.btnTimeRange.setTitle("", for: .normal)
                }
                
            }
            
        }
        else
        {
            viewScheduleDate.isHidden = true
            btnHome.isEnabled = true
        }
        lblScheduleDateTime.text = strScheduleDateTime
        
        setData()
        apiCallForGetEmployees()
        viewMoreOptions.isHidden = true
        setFooterView()
                
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
        self.navigationController?.isNavigationBarHidden = false
                
    }
    private func updateUI() {
        
        if availabilities.count <= 5 {
            if availabilities.count == 0 {
                constraintTableViewHeight.constant = 0
            } else {
                constraintTableViewHeight.constant = CGFloat(availabilities.count * 80) + 120
            }
        } else {
            constraintTableViewHeight.constant = 520
        }
        if availabilities.count > 5 {
            rowsCount = 5
            viewMoreOptions.isHidden = false
        } else {
            viewMoreOptions.isHidden = true
            rowsCount = availabilities.count
        }
        tableView.reloadData()
    }
    
    private func apiCallForGetEmployees() {
        
        var params = EmployeeRequest()
        
        if let entityDetail = entityDetail {
            params.serviceAddress = entityDetail.leadDetail?.servicesAddress1
            params.departmentSysName = entityDetail.leadDetail?.departmentSysName
            params.lat = entityDetail.leadDetail?.latitude
            params.lng = entityDetail.leadDetail?.longitude
        } else if let lead = propertyDetails?.opportunity {
            params.departmentSysName = lead.departmentSysName
            params.serviceAddress = lead.serviceAddress?.address1
            params.lat = lead.latitude
            params.lng = lead.longitude
        } else {
            if let lead = propertyDetails?.lead {
                params.serviceAddress = lead.address1
                params.lat = lead.latitude
                params.lng = lead.longitude
            } else if let lead = propertyDetails?.account {
                params.serviceAddress = lead.serviceAddress?.address1
                params.lat = lead.latitude
                params.lng = lead.longitude
            } else {
                params.serviceAddress = propertyDetails?.propertyInfo?.address
                params.lat = propertyDetails?.propertyInfo?.latitude ?? 0.0
                params.lng = propertyDetails?.propertyInfo?.longitude ?? 0.0
            }
            if let categories = AppUserDefaults.shared.masterDetails?.categories {
                for category in categories {
                    if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
                        params.departmentSysName = category.departmentSysName
                        break
                    }
                }
            }
        }
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        params.companyKey = companyKey
        params.radius = 50.0
        params.startDate = AppUtility.convertDateToString(Date(), toFormat: ConstantV.modifiedDateFormat)
        if let endDate
            = Calendar.current.date(byAdding: .day, value: 3, to: Date()) {
            params.endDate = AppUtility.convertDateToString(endDate, toFormat: ConstantV.modifiedDateFormat)
        } else {
            params.endDate = AppUtility.convertDateToString(Date(), toFormat: ConstantV.modifiedDateFormat)
        }
        params.isDoorToDoor = true
        presenter.apiCallForGetEmployees(params)
    }
    private func apiCallForGetRoute() {
        
        var params = RouteRequest()
        params.countrySysName = "UnitedStates"
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        
        
        params.cityName = propertyDetails?.propertyInfo?.placeMark?.subLocality ?? ""
        params.companyKey = companyKey
        params.countryID = "1"
        params.citySysName = ""
        params.stateSysName = ""
        params.customerAddressID = ""
        params.accountNumber = ""
        params.address2 = ""
        params.stateID = ""
        params.address1 = ""
        params.zipcode = ""
        
        params.stateID = propertyDetails?.propertyInfo?.state?.stateID.description
        params.stateSysName = propertyDetails?.propertyInfo?.state?.stateSysName ?? ""
        params.zipcode = propertyDetails?.propertyInfo?.placeMark?.postalCode ??  propertyDetails?.propertyInfo?.zipCode
        
        if let lead = propertyDetails?.opportunity {
            params.address1 = lead.serviceAddress?.address1 ?? ""
            params.address2 = lead.serviceAddress?.address2 ?? ""
            params.cityName = lead.serviceAddress?.cityName ?? ""
            params.accountNumber = lead.accountNo ?? ""
            params.zipcode = lead.serviceAddress?.zipcode ?? ""
            params.stateID = lead.serviceAddress?.stateID?.description ?? ""
            params.customerAddressID = lead.serviceAddress?.customerAddressID?.description ?? ""
        } else  if let lead = propertyDetails?.lead {
            params.address1 = lead.address1 ?? ""
            params.address2 = lead.address2 ?? ""
            if let state = states.filter({$0.stateID == lead.stateID}).first {
                params.stateSysName = state.stateSysName ?? ""
            }
            params.stateID = lead.stateID?.description ?? ""
            params.accountNumber = lead.accountNo ?? ""
            params.cityName = lead.cityName ?? ""
            params.zipcode = lead.zipcode ?? ""
        } else if let lead = propertyDetails?.account {
            params.address1 = lead.serviceAddress?.address1 ?? ""
            params.address2 = lead.serviceAddress?.address2 ?? ""
            params.accountNumber = lead.accountNumber ?? ""
            params.cityName = lead.serviceAddress?.cityName ??  ""
            params.zipcode = lead.serviceAddress?.zipcode ?? ""
            params.stateID = lead.serviceAddress?.stateID?.description ?? ""
            //   params.stateSysName = lead.serviceAddress?.s
            if let state = states.filter({$0.stateID == lead.serviceAddress?.stateID}).first {
                params.stateSysName = state.stateSysName ?? ""
            }
            params.customerAddressID = lead.serviceAddress?.customerAddressID?.description ?? ""
        }
        
        presenter.apiCallForGetRoute(params)
    }
    private func doScheduleService() -> ScheduleService? {
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        var scheudle = ScheduleService()
        scheudle.companykey = companyKey
        
        scheudle.primaryServiceSysName = services.first?.serviceSysName
        
        if let accountNo = entityDetail?.leadDetail?.accountNo {
            scheudle.accountNumber = accountNo
        } else if let lead = propertyDetails?.lead {
            scheudle.accountNumber = lead.accountNo
            scheudle.leadNo = lead.leadNumber
        } else if let lead = propertyDetails?.opportunity {
            scheudle.accountNumber = lead.accountNo
            //scheudle.leadNo = lead.num
        }
        if let leadNumber = entityDetail?.leadDetail?.leadNumber {
            scheudle.leadNo = leadNumber
        }
        scheudle.typeoftime = "1"
        scheudle.initialServicePrice = services.first?.totalInitialPrice
        if iSelectedIndex != -1 {
            if let route = employees.filter({$0.employeeNo == availabilities[iSelectedIndex].employeeNo}).first {
                scheudle.routeSysName = route.routeNo
            }
            if let date = availabilities[iSelectedIndex].availableDate {
                scheudle.setupStartDate = AppUtility.convertStringToFormat(fromString: date, toFormat: ConstantV.modifiedDateFormat)
                scheudle.initialServiceDate = AppUtility.convertStringToFormat(fromString: date, toFormat: ConstantV.modifiedDateFormat)
            }
            if let startTime = availabilities[iSelectedIndex].startTime {
                scheudle.initialServiceTime = startTime
            }
        } else if let date = selectedDate {
            scheudle.setupStartDate = AppUtility.convertDateToString(date, toFormat: ConstantV.modifiedDateFormat)
            scheudle.initialServiceDate = AppUtility.convertDateToString(date, toFormat: ConstantV.modifiedDateFormat)
            scheudle.initialServiceTime = AppUtility.convertDateToString(date, toFormat: "HH:mm:ss")
            scheudle.routeSysName = selectedRoute?.routeNo
        }
        scheudle.initialServiceDuration = AppUserDefaults.shared.masterDetails?.categories.flatMap { $0.services}.filter{$0.sysName == services.first?.serviceSysName }.first?.initialEstimatedDuration
        scheudle.initialServiceSysNames =  services.map({($0.serviceSysName ?? "")}).joined(separator: ",")
        scheudle.nonPreferablerangeSysName = ""
        scheudle.preferablerangeSysName = ""
        scheudle.initialServiceInstruction = ""
        scheudle.setupInstruction = ""
        scheudle.setupInstruction = ""
        scheudle.rangeofTimeId = strTimeRangeId
        return scheudle
        
    }
    private func doReScheduleService() -> ScheduleService? {
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"

        var scheudle = ScheduleService()
        scheudle.companykey = companyKey
        if iSelectedIndex != -1
        {
            if let route = employees.filter({$0.employeeNo == availabilities[iSelectedIndex].employeeNo}).first
            {
                scheudle.routeSysName = route.routeNo
            }
            if let date = availabilities[iSelectedIndex].availableDate {
                //scheudle.setupStartDate = AppUtility.convertStringToFormat(fromString: date, toFormat: ConstantV.modifiedDateFormat)
                scheudle.rescheduleDate = AppUtility.convertStringToFormat(fromString: date, toFormat: ConstantV.modifiedDateFormat)
            }
            if let startTime = availabilities[iSelectedIndex].startTime {
                scheudle.rescheduleTime = startTime
            }
        } else if let date = selectedDate {
            //scheudle.setupStartDate = AppUtility.convertDateToString(date, toFormat: ConstantV.modifiedDateFormat)
            scheudle.rescheduleDate = AppUtility.convertDateToString(date, toFormat: ConstantV.modifiedDateFormat)
            scheudle.rescheduleTime = AppUtility.convertDateToString(date, toFormat: "HH:mm:ss")
            scheudle.routeSysName = selectedRoute?.routeNo
        }
        
        scheudle.rangeofTimeId = strTimeRangeId
        scheudle.workOrderId = strWorkOrderId
        scheudle.createdBy = Int(employeeID)
        scheudle.result = ""
        scheudle.errors = ""
        return scheudle
        
    }
    
    private func fetchStates() {
        guard let path = Bundle.main.path(forResource: "StateListNew_Production", ofType: "json") else {
            return
        }
        do {
            let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            states = try JSONDecoder().decode([State].self, from: jsonData)
        }
        catch {
            print("States", error.localizedDescription)
        }
    }
    @IBAction func actionHome(_ sender: Any) {
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
    }
    private func setData()
    {

        guard let propertyInfo = propertyDetails?.propertyInfo else { return }
        lblName.text = propertyInfo.name
        lblPhone.text = propertyInfo.phone
        lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
        lblEmail.text = propertyInfo.email
        lblAddress.text = propertyInfo.address
        viewName.isHidden = lblName.text == "" || lblName.text == nil ? true : false
        viewEmail.isHidden = lblEmail.text == "" || lblEmail.text == nil ? true : false
        viewPhone.isHidden = lblPhone.text == "" || lblPhone.text == nil ?  true : false
        viewAddress.isHidden = lblAddress.text == "" || lblAddress.text == nil ? true : false
        if let image = propertyInfo.imageManual {
            imgProperty.image = image
        } else {
            guard let imgUrl = propertyInfo.imageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),  let url = URL(string: imgUrl) else {
                return
            }
            imgProperty.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details"))
        }
        
    }
    private func showPicker() {
        var rows:[String] = []
        var selectedRow =  0
        if let routeResponse = routeResponse {
            let otherRoutes = routeResponse.otherRoute
            let suggestedRoutes =  routeResponse.suggestedRoute
            var routes: [Route] = []
            routes.append(contentsOf: otherRoutes ?? [])
            routes.append(contentsOf: suggestedRoutes ?? [])
            guard !routes.isEmpty else {
                showAlert(message: "No route available")
                return
            }
            for (index,row) in routes.enumerated() {
                if txtName.text == row.leadEmployeeName {
                    selectedRow = index
                }
                rows.append(row.leadEmployeeName ?? "")
            }
            showStringPicker(sender: txtName, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                self.txtName.text = value
                self.selectedRoute = routes[index]
                self.btnCustomOption.isSelected = true
            }
        }
    }
    
    // MARK: - Outltes
    @IBAction func actionMoreOptions(_ sender: Any) {
        
        if listOptions == .more, availabilities.count > (rowsCount + 3) {
            rowsCount += 3
        } else if listOptions == .less, rowsCount - 3 > 5{
            rowsCount -= 3
        } else if listOptions == .more {
            rowsCount = availabilities.count
            listOptions = .less
        } else {
            listOptions = .more
            rowsCount = 5
        }
        tableView.reloadData()
    }
    @IBAction func actionCustomOption(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        iSelectedIndex = -1
        tableView.reloadData()
    }
    @IBAction func actionProceed(_ sender: Any) {
        guard iSelectedIndex != -1 || btnCustomOption.isSelected else {
            showAlert(message: "Please select atleast one technician to proceed")
            return
        }
        var customTechnician = CustomTechnician()
        if btnCustomOption.isSelected, iSelectedIndex == -1 {
            guard !txtDate.text!.isEmpty else {
                showAlert(message: "Please select date")
                return
            }
            guard !txtName.text!.isEmpty else {
                showAlert(message: "Please select route")
                return
            }
            customTechnician.name = selectedRoute?.leadEmployeeName
            customTechnician.date = txtDate.text
        }
        guard strTimeRangeId.count != 0 else {
            showAlert(message: "Please select time range")
            return
        }
        if fromReschedule
        {
            let scheduleDetail = doReScheduleService()
            guard let schedule = scheduleDetail else { return }
            
            let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
            if let destination = storyboard.instantiateViewController(withIdentifier: "ConfirmAppointmentViewController") as? ConfirmAppointmentViewController
            {
                destination.services = services
                destination.schedule = schedule
                destination.fromReschedule = fromReschedule

                destination.entityDetail = entityDetail
                if iSelectedIndex != -1 {
                    destination.technician = availabilities[iSelectedIndex]
                    destination.isCustomTechnician = false
                } else {
                    destination.isCustomTechnician = true
                    destination.customTecnician = customTechnician
                }
                destination.propertyDetails = propertyDetails
                destination.strTimeRangeValue = strTimeRangeValue
                self.navigationController?.pushViewController(destination, animated: true)
            }
        }
        else
        {
            let scheduleDetail = doScheduleService()
            guard let schedule = scheduleDetail else { return }
            
            let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
            if let destination = storyboard.instantiateViewController(withIdentifier: "ConfirmAppointmentViewController") as? ConfirmAppointmentViewController
            {
                destination.services = services
                destination.schedule = schedule
                destination.fromReschedule = fromReschedule

                destination.entityDetail = entityDetail
                if iSelectedIndex != -1 {
                    destination.technician = availabilities[iSelectedIndex]
                    destination.isCustomTechnician = false
                } else {
                    destination.isCustomTechnician = true
                    destination.customTecnician = customTechnician
                }
                destination.propertyDetails = propertyDetails
                destination.strTimeRangeValue = strTimeRangeValue

                self.navigationController?.pushViewController(destination, animated: true)
            }
        }

    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnTimeRange(_ sender: Any) {
        
        if(aryTimeRange.count != 0){
            OpenTimeRengeView(aryTime: aryTimeRange)

        }else{
            if(isInternetAvailable()){
                CallTimeRangeAPI()
            }else{
               showAlert(message: alertInternet)
            }
        }
    }
    
    
    func CallTimeRangeAPI() {
        
        let strURL = MainUrl + UrlSalesRangeofTime + Global().getCompanyKey()
        WebService.callAPIBYGETWithoutKey(parameter: NSDictionary(), url: strURL) { [self] (responce, status) in
            print(responce)
            if(responce.value(forKey: "data") is NSArray){
               aryTimeRange = NSArray()
                let  aryTemp = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                aryTimeRange = aryTemp.filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("1") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("true"))
                } as NSArray
                OpenTimeRengeView(aryTime: aryTimeRange)
            }else{
                showAlert(message: alertSomeError)
            }
        }
        
    }
    func OpenTimeRengeView(aryTime : NSArray)  {
        var rows:[String] = []
        for (_,row) in aryTime.enumerated() {
            let StartInterval = "\((row as AnyObject).value(forKey: "StartInterval")!)"
            let EndInterval = "\((row as AnyObject).value(forKey: "EndInterval")!)"

            let strrenge = StartInterval + " - " + EndInterval
            rows.append(strrenge)
        }
        if(rows.count != 0){
            showStringPicker(sender: txtName, selectedRow: 0, rows: rows)
            { [self] (index, value, view) in
                self.btnTimeRange.setTitle(value, for: .normal)
                let dict = self.aryTimeRange.object(at: index) as! NSDictionary
                self.strTimeRangeValue = value
                self.strTimeRangeId = "\(dict.value(forKey: "RangeofTimeId") ?? "")"
            }
        }else{
            showAlert(message: "Time Range not found ")

        }
       
    }
    
}
// MARK: - UITableViewDataSource
extension ServiceSetupViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if availabilities.isEmpty {
                return 0
            }
            return 1
        } else {
            return rowsCount
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TechnicianHeaderTableViewCell.self), for: indexPath) as! TechnicianHeaderTableViewCell
            return cell
        } else  {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TechnicianScheduleTableViewCell.self), for: indexPath) as! TechnicianScheduleTableViewCell
            
            if iSelectedIndex == indexPath.row {
                cell.isSelected = true
            } else {
                cell.isSelected = false
            }
            let availability = availabilities[indexPath.row]
            if let employee  = employees.first(where: { (employee) -> Bool in
                return employee.employeeNo == availability.employeeNo
            }) {
                cell.imagePath = employee.employeePhoto
            } else {
                cell.imagePath = nil
            }
            cell.availability =  availability
            return cell
        }
    }
}
// MARK: - UITableViewDelegate
extension ServiceSetupViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            iSelectedIndex = indexPath.row
            btnCustomOption.isSelected = false
            //tableView.reloadData()
        }
    }
}
// MARK: - UITextFieldDelegate
extension ServiceSetupViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate {
            showDatePicker(sender: textField, selectedDate: Date(), minimumDate: Date(),  pickerType: .dateTime) { (selectedDate, _) in
                let format = DateFormatter()
                format.dateFormat = "MM/d/yyyy-hh:mm a"
                let formattedDate = format.string(from: selectedDate)
                textField.text = formattedDate
                self.selectedDate = selectedDate
                self.iSelectedIndex = -1
                self.btnCustomOption.isSelected = true
            }
            return false
        } else if textField == txtName {
            if  let _ = routeResponse {
                showPicker()
            } else {
                apiCallForGetRoute()
            }
            return false
        }
        return true
    }
}
// MARK: - ServiceSetupView
extension ServiceSetupViewController: ServiceSetupView {
    func gotEmployees(_ output: [Employee]) {
        self.employees = output
    }
    func gotRoutes(_ output: RouteResponse?) {
        self.routeResponse = output
        showPicker()
    }
}
