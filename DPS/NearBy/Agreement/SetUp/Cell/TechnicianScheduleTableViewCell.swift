//
//  TechnicianScheduleTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 02/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import Kingfisher

class TechnicianScheduleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgCalender: UIImageView! 
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewBackround: UIView!
    
    var imagePath: String? {
        didSet {
            guard let imagePath = imagePath, let url = URL(string: imagePath) else { imageView?.image = nil; return }
            imgView.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details"))
        }
    }
    
    var availability: Availability? {
        didSet {
            lblName.text = availability?.employeeName
            lblDesignation.text = nil
            if  let availableDate = availability?.availableDate, let date =  AppUtility.convertStringToFormat(fromString: availableDate, toFormat: "MM/dd/yyyy") {
                if  let startTime = availability?.startTime, let time = AppUtility.convertStringToFormat(fromString: startTime, toFormat: "hh:mm a") {
                    lblDate.text = time + " on " + date
                } else {
                    lblDate.text = date
                }
            } else {
                lblDate.text = nil
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        guard let _ = availability else {
            AppUtility.imageColor(image: imgCalender, color: .appThemeColor)
            return
        }
        if selected {
            viewBackround.backgroundColor = .appThemeColor
            lblDate.textColor = .white
            lblName.textColor = .white
            lblDesignation.textColor = .white
            AppUtility.imageColor(image: imgCalender, color: .white)
        } else {
            lblDate.textColor = .appDarkGreen
            lblName.textColor = .appDarkGreen
            lblDesignation.textColor = .appDarkGreen
            viewBackround.backgroundColor = .appLightGray
            AppUtility.imageColor(image: imgCalender, color: .appThemeColor)

        }
    }
    
}
