//
//  AgreementServiceMonthTableViewCell.swift
//  DPS
//
//  Created by Saavan Patidar on 01/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class AgreementServiceMonthTableViewCell: UITableViewCell {

    @IBOutlet var btnJanuary: UIButton!
    @IBOutlet var btnFebruary: UIButton!
    @IBOutlet var btnMarch: UIButton!
    @IBOutlet var btnApril: UIButton!
    @IBOutlet var btnMay: UIButton!
    @IBOutlet var btnJune: UIButton!
    @IBOutlet var btnJuly: UIButton!
    @IBOutlet var btnAugust: UIButton!
    @IBOutlet var btnSeptember: UIButton!
    @IBOutlet var btnOctober: UIButton!
    @IBOutlet var btnNovember: UIButton!
    @IBOutlet var btnDecember: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
