//
//  ServiceSetupService.swift
//  DPS
//
//  Created by Vivek Patel on 10/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class ServiceSetupService {
    func getEmployees(parameters:EmployeeRequest, _ callBack:@escaping (_ object: [Employee]?,_ error:String?) -> Void) {
        let url = URL.ServiceReport.getBestFitEmployees
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers).responseDecodable(of: [Employee].self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
            
        }
    }
    func getRoutes(parameters:RouteRequest, _ callBack:@escaping (_ object: RouteResponse?,_ error:String?) -> Void) {
        let url = URL.ServiceReport.getRoute
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers).responseDecodable(of: RouteResponse.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                
                break
            }
        }
    }
}
