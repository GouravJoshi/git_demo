//
//  ServiceSetupModel.swift
//  DPS
//
//  Created by Vivek Patel on 10/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

// MARK: - Employee
struct EmployeeRequest: Codable {
    var companyKey, serviceAddress, startDate, endDate: String?
    var lat, lng: Double?
    var radius: Double?
    var departmentSysName: String?
    var isDoorToDoor: Bool?

    
    enum CodingKeys: String, CodingKey {
        case companyKey = "CompanyKey"
        case serviceAddress = "ServiceAddress"
        case startDate = "StartDate"
        case endDate = "EndDate"
        case lat = "Lat"
        case lng = "Lng"
        case radius = "Radius"
        case departmentSysName = "DepartmentSysName"
        case isDoorToDoor = "IsDoorToDoor"

    }
    init() {}
    
}

// MARK: - Employee
struct Employee: Codable {
    var routeNo, routeName, employeeNo, employeeName: String?
    var employeePhoto: String?
    var minDistance: Double?
    var minDistanceAvailability: String?
    var availability: [Availability]?
    
    enum CodingKeys: String, CodingKey {
        case routeNo = "RouteNo"
        case routeName = "RouteName"
        case employeeNo = "EmployeeNo"
        case employeeName = "EmployeeName"
        case employeePhoto = "EmployeePhoto"
        case minDistance = "MinDistance"
        case minDistanceAvailability = "MinDistanceAvailability"
        case availability = "Availability"
    }
}

// MARK: - Availability
struct Availability: Codable {
    var availableDate, startTime, endTime: String?
    var distance: Double?
    var isEmployeeOnly: Bool?
    var employeeNo, employeeName, color: String?
    enum CodingKeys: String, CodingKey {
        case availableDate = "AvailableDate"
        case startTime = "StartTime"
        case endTime = "EndTime"
        case distance = "Distance"
        case isEmployeeOnly = "IsEmployeeOnly"
        case employeeNo = "EmployeeNo"
        case employeeName = "EmployeeName"
        case color = "Color"
        
    }
}
struct CustomTechnician: Codable {
    var name: String?
    var date: String?
    
}
struct ScheduleService: Codable {
    
    var primaryServiceSysName: String?
    var accountNumber: String?
    var companykey: String?
    var setupStartDate: String?
    var leadNo: String?
    var typeoftime: String?
    var nonPreferablerangeSysName: String?
    var preferablerangeSysName: String?
    var routeSysName: String?
    var initialServicePrice: Double?
    var initialServiceDuration: String?
    var initialServiceDate: String?
    var initialServiceSysNames: String?
    var initialServiceTime: String?
    var lockInitialRoute: Bool?
    var lockInitialtime: Bool?
    var initialServiceInstruction: String?
    var setupInstruction: String?
    var rangeofTimeId: String?
    //Nilind
    var workOrderId: Int?
    var rescheduleDate: String?
    var rescheduleTime: String?
    var createdBy: Int?
    var result: String?
    var errors: String?


    

    enum CodingKeys: String, CodingKey {
        case primaryServiceSysName = "PrimaryServiceSysName"
        case accountNumber = "AccountNumber"
        case companykey = "Companykey"
        case setupStartDate = "SetupStartDate"
        case leadNo = "LeadNo"
        case typeoftime = "Typeoftime"
        case nonPreferablerangeSysName = "NonPreferablerangeSysName"
        case preferablerangeSysName = "PreferablerangeSysName"
        case routeSysName = "RouteSysName"
        case initialServicePrice = "InitialServicePrice"
        case initialServiceDuration = "InitialServiceDuration"
        case initialServiceDate = "InitialServiceDate"
        case initialServiceSysNames = "InitialServiceSysNames"
        case initialServiceTime = "InitialServiceTime"
        case lockInitialRoute = "LockInitialRoute"
        case lockInitialtime = "LockInitialtime"
        case initialServiceInstruction = "InitialServiceInstruction"
        case setupInstruction = "SetupInstruction"
        case rangeofTimeId = "RangeofTimeId"
        case workOrderId = "WorkOrderId"
        case rescheduleDate = "RescheduleDate"
        case rescheduleTime = "RescheduleTime"
        case createdBy = "CreatedBy"
        case result = "Result"
        case errors = "Errors"



    }
}

// MARK: - RouteRequest
struct RouteRequest: Codable {
    var countrySysName, cityName, address2, citySysName: String?
    var accountNumber, companyKey, customerAddressID, stateID: String?
    var zipcode, stateSysName, countryID, address1: String?
    
    enum CodingKeys: String, CodingKey {
        case countrySysName = "CountrySysName"
        case cityName = "CityName"
        case address2 = "Address2"
        case citySysName = "CitySysName"
        case accountNumber = "AccountNumber"
        case companyKey = "CompanyKey"
        case customerAddressID = "CustomerAddressId"
        case stateID = "StateId"
        case zipcode = "Zipcode"
        case stateSysName = "StateSysName"
        case countryID = "CountryId"
        case address1 = "Address1"
    }
}

struct SyncRequest: Codable {
    var initialServiceTime, preferablerangeSysName, setupInstruction, primaryServiceSysName: String?
    var typeoftime, accountNumber, setupStartDate, leadNo: String?
    var nonPreferablerangeSysName, companykey, routeSysName, initialServiceSysNames: String?
    var initialServiceDuration, lockInitialRoute, initialServicePrice, initialServiceDate: String?
    var lockInitialtime, initialServiceInstruction: String?
    
    enum CodingKeys: String, CodingKey {
        case initialServiceTime = "InitialServiceTime"
        case preferablerangeSysName = "PreferablerangeSysName"
        case setupInstruction = "SetupInstruction"
        case primaryServiceSysName = "PrimaryServiceSysName"
        case typeoftime = "Typeoftime"
        case accountNumber = "AccountNumber"
        case setupStartDate = "SetupStartDate"
        case leadNo = "LeadNo"
        case nonPreferablerangeSysName = "NonPreferablerangeSysName"
        case companykey = "Companykey"
        case routeSysName = "RouteSysName"
        case initialServiceSysNames = "InitialServiceSysNames"
        case initialServiceDuration = "InitialServiceDuration"
        case lockInitialRoute = "LockInitialRoute"
        case initialServicePrice = "InitialServicePrice"
        case initialServiceDate = "InitialServiceDate"
        case lockInitialtime = "LockInitialtime"
        case initialServiceInstruction = "InitialServiceInstruction"
    }
}

// MARK: - RouteResponse
struct RouteResponse: Codable {
    var suggestedRoute, otherRoute: [Route]?
    
    enum CodingKeys: String, CodingKey {
        case suggestedRoute = "SuggestedRoute"
        case otherRoute = "OtherRoute"
    }
}

// Route.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let route = try? newJSONDecoder().decode(Route.self, from: jsonData)

import Foundation

// MARK: - Route
struct Route: Codable {
    var routeID: Int?
    var routeName, routeNo, leadEmployeeNo: String?
    var leadEmployeeName: String?
    var boundStatus, flag: JSONNull?
    var isActive: Bool?
    
    enum CodingKeys: String, CodingKey {
        case routeID = "RouteId"
        case routeName = "RouteName"
        case routeNo = "RouteNo"
        case leadEmployeeNo = "LeadEmployeeNo"
        case leadEmployeeName = "LeadEmployeeName"
        case boundStatus = "BoundStatus"
        case flag = "Flag"
        case isActive = "IsActive"
    }
}
