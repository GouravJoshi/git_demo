//
//  ServiceSetupPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 10/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
// Navin PAtidar hxgchghuj

import Foundation

protocol ServiceSetupView: CommonView {
    func gotEmployees(_ output: [Employee])
    func gotRoutes(_ output: RouteResponse?)
}

class ServiceSetupPresenter: NSObject {
    weak private var delegate: ServiceSetupView?
    private lazy var service = {
        return ServiceSetupService()
    }()
    
    init(_ delegate: ServiceSetupView) {
        self.delegate = delegate
    }
    
    func apiCallForGetEmployees( _ parameters: EmployeeRequest) {
       
        delegate?.showLoader()
        service.getEmployees(parameters: parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object{
                self.delegate?.gotEmployees(object)
                
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    func apiCallForGetRoute( _ parameters: RouteRequest) {
        delegate?.showLoader()
        service.getRoutes(parameters: parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object{
                self.delegate?.gotRoutes(object)
                
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    
}
