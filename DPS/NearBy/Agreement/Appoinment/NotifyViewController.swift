//
//  NotifyViewController.swift
//  DPS
//
//  Created by Vivek Patel on 17/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
protocol NotifyCustomerDelegate {
    func notifyCustomer(text: Bool , email: Bool)
}
final class NotifyViewController: BaseViewController {
    var delegate: NotifyCustomerDelegate?
    var entityDetail: EntityDetail?
    var propertyDetails: PropertyDetails?
    var isEmail = false
    var isText = false
    // MARK: - Outlets
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var switchMail: UISwitch!
    @IBOutlet weak var switchText: UISwitch!
    @IBOutlet weak var viewText: UIStackView!
    @IBOutlet weak var viewEmail: UIStackView!
    // MARK: -  View Life Cycle
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    private func setUI() {
        if isText {
            viewText.isHidden = false
        } else {
            viewText.isHidden = true
        }
        if isEmail {
            viewEmail.isHidden = false
        } else {
            viewEmail.isHidden = true
        }
    }
    // MARK: - Actions
    @IBAction func actionSave(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.notifyCustomer(text: self.switchText.isOn, email: self.switchMail.isOn)
        }
    }
}
