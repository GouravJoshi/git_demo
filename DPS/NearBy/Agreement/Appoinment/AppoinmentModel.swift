//
//  AppoinmentModel.swift
//  DPS
//
//  Created by Vivek Patel on 03/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

// MARK: - Employee
struct AppoinmentResponse: Codable {
    var companyKey: String?
    var accountNumber: String?
    var createdBy: Int?
    var createdDate: String?
    var employeeNo: String?
    var initialServiceDate: String?
    var initialServiceDuration: String?
    var initialServiceSysNames: String?
    var initialServiceTime: String?
    var leadNo: String?
    var primaryServiceSysName: String?
    var routeSysName: String?
    var serviceSetupId: Int?
    var setupStartDate: String?
    // var typeoftime: String?
    var workOrderNo: String?
    
    enum CodingKeys: String, CodingKey {
        case companyKey = "companykey"
        case accountNumber = "AccountNumber"
        case createdBy = "CreatedBy"
        case createdDate = "CreatedDate"
        case employeeNo = "EmployeeNo"
        case initialServiceDate = "InitialServiceDate"
        case initialServiceDuration = "InitialServiceDuration"
        case initialServiceSysNames = "InitialServiceSysNames"
        case initialServiceTime = "InitialServiceTime"
        case leadNo = "LeadNo"
        case primaryServiceSysName = "PrimaryServiceSysName"
        case routeSysName = "RouteSysName"
        case serviceSetupId = "ServiceSetupId"
        case setupStartDate = "SetupStartDate"
        // case typeoftime = "Typeoftime"
        case workOrderNo = "WorkOrderNo"
    }
}
struct RescheduleResponse: Codable {
    var companyKey: String?
    var rangeofTimeId: Int?
    var workOrderId: Int?
    var workOrderNo: String?
    var employeeNo: String?
    var rescheduleDate: String?
    var rescheduleTime: String?
    var routeSysName: String?
    var createdBy: Int?
    var result: String?
    var Errors: [String]?
    
    enum CodingKeys: String, CodingKey {
        case companyKey = "Companykey"
        case rangeofTimeId = "RangeofTimeId"
        case workOrderId = "WorkOrderId"
        case workOrderNo = "WorkOrderNo"
        case employeeNo = "EmployeeNo"
        case rescheduleDate = "RescheduleDate"
        case rescheduleTime = "RescheduleTime"
        case routeSysName = "RouteSysName"
        case createdBy = "CreatedBy"
        case result = "Result"
        case Errors = "Errors"

    }
}
// MARK: - RouteRequest
struct UpdateSetupRequest: Codable {
    var countrySysName, cityName, address2, citySysName: String?
    var accountNumber, companyKey, customerAddressID, stateID: String?
    var zipcode, stateSysName, countryID, address1: String?
    
    enum CodingKeys: String, CodingKey {
        case countrySysName = "CountrySysName"
        case cityName = "CityName"
        case address2 = "Address2"
        case citySysName = "CitySysName"
        case accountNumber = "AccountNumber"
        case companyKey = "CompanyKey"
        case customerAddressID = "CustomerAddressId"
        case stateID = "StateId"
        case zipcode = "Zipcode"
        case stateSysName = "StateSysName"
        case countryID = "CountryId"
        case address1 = "Address1"
    }
}
struct NotifyCustomerRequest: Codable {
    var companyKey: String?
    var workOrderNo: String?
    var toEmails: [String] = []
    enum CodingKeys: String, CodingKey {
        case companyKey = "CompanyKey"
        case workOrderNo = "WorkOrderNo"
        case toEmails = "ToEmails"
    }
    init() {}
}

struct EmployeeListD2D : Codable {
    var addressLine2: String?
    var employeeId: Int?
    var fullName: String?
    var lastName: String?
    var employeeNo: String?
    var createdBy: Int?
    var primaryEmail: String?
    var firstName: String?
    //  var stateId: Int?
    // var countryId: Int?
    var primaryPhone: String?
    var middleName: String?
    var addressLine1: String?
    var isActive: Bool?
    // var cityId: String?
    enum CodingKeys: String, CodingKey {
        case addressLine2 = "AddressLine2"
        case employeeId = "EmployeeId"
        case fullName = "FullName"
        case lastName = "LastName"
        case employeeNo = "EmployeeNo"
        case createdBy = "CreatedBy"
        case primaryEmail = "PrimaryEmail"
        case firstName = "FirstName"
        //  case stateId = "StateId"
        // case countryId = "CountryId"
        case primaryPhone = "PrimaryPhone"
        case middleName = "MiddleName"
        case addressLine1 = "AddressLine1"
        //  case cityId = "CityId"
        case isActive = "IsActive"
    }
}
