//
//  AppoinmentService.swift
//  DPS
//
//  Created by Vivek Patel on 14/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class AppoinmentService {
    func setupSchedule(parameters: ScheduleService, _ callBack:@escaping (_ object: AppoinmentResponse?,_ error:String?) -> Void) {
        let url = URL.ServiceReport.saveSetup
        print(url)
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: AppoinmentResponse.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode(AppoinmentResponse.self, from: data)
                    print(object)
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
    func setupReSchedule(parameters: ScheduleService, _ callBack:@escaping (_ object: RescheduleResponse?,_ error:String?) -> Void) {
        let url = URL.ServiceReport.rescheduleSetup
        print(url)
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: RescheduleResponse.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode(RescheduleResponse.self, from: data)
                    print(object)
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
    func updateStatus(_ leadNumber: String, isInitialSetup: String, _ callBack:@escaping (_ object: String?,_ error:String?) -> Void) {
        let url = URL.ServiceReport.updateSetup.replacingOccurrences(of: "(LeadId)", with: leadNumber).replacingOccurrences(of: "(IsInitialSetup)", with: isInitialSetup)
        print(url)
        AF.request(url, method: .post, headers: URL.headers) .responseDecodable(of: String.self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                if #available(iOS 13.0, *) {
                    callBack(nil, error.localizedDescription)
                } else {
                    callBack("Success", nil)
                }
                break
            }
        }
    }
    func notifyCustomer(_ parameters: NotifyCustomerRequest, _ callBack:@escaping (_ object: Bool?,_ error:String?) -> Void) {
        let url = URL.Schedule.notifyViaMail
        print(url)
        
        if #available(iOS 13.0, *) {

            AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: Bool.self) { response in
                switch response.result{
                case let .success(result):
                    callBack(result,nil)
                    break
                case let .failure(error):
                    callBack(nil, error.localizedDescription)
                    break
                }
            }.responseJSON { (dataResponse) in
                switch dataResponse.result{
                case .success(let value):
                    print(value)
                    
                    break
                case .failure(let error):
                    print(error)
                    break
                    
                }
            }
            
        } else {
            
            //  For iOS 12 and below devices
            AF.request(url, method: .post, parameters: parameters.dictionary, encoding: JSONEncoding.default, headers: URL.headers).responseJSON { (response:AFDataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    callBack(true,nil)
                    break
                case .failure(_):
                    print(response.error ?? 0)
                    callBack(false, alertSomeError)
                    break
                }
            }
            
        }

    }
}
