//
//  AppoinmentSuccessViewController.swift
//  DPS
//
//  Created by Vivek Patel on 05/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol MapViewNavigationDelegate {
    func actionBack()
}
class AppoinmentSuccessViewController: UIViewController {
    var delegate:MapViewNavigationDelegate?
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: Actions
    @IBAction func actionDone(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.actionBack()
            
        }
    }
}
