//
//  ServiceListTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 05/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class ServiceListTableViewCell: UITableViewCell {
    // MARK: - Variables
    var service : SoldServiceStandardDetail? {
        didSet {
            guard let service = service else { return }
            if let serviceDetails = AppUserDefaults.shared.masterDetails  {
                let name = serviceDetails.categories.flatMap { $0.services}.filter{$0.sysName == service.serviceSysName}.first?.name
                lblName.text = name
            } else {
                lblName.text = service.serviceSysName
            }
            lblFrequency.text = service.serviceFrequency
            lblInitialPrice.text = service.totalInitialPrice?.currency
            lblFinalPrice.text = service.totalMaintPrice?.currency
            lblDescription.attributedText = service.serviceDescription?.trimmed.html2AttributedString
            lblSeprator.isHidden  =  lblDescription.attributedText?.string == "" || lblDescription.attributedText?.string == nil  ? true : false
        }
    }
    //MARK: - Outltes
    @IBOutlet weak var imgView: UIImageView! {
        didSet {
        AppUtility.imageColor(image: imgView, color: .black)
    }
    }
  
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var lblSeprator: UILabel!
    @IBOutlet weak var lblInitialPrice: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFrequency: UILabel!
    @IBOutlet weak var lblFinalPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
