//
//  PaymentWorkOrderD2DTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 18/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class PaymentWorkOrderD2DTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCardDescription: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    // MARK: - Variables
   
    var paymentInfo: PaymentInfo? {
        didSet {
            guard  let paymentInfo = paymentInfo else { return }
            lblPaymentMode.text = paymentInfo.paymentMode
            lblAmount.text = paymentInfo.amount?.currency
            if let dateString = paymentInfo.createdDate, let paidDate = AppUtility.convertStringToFormat(fromString: dateString, toFormat: "MM/dd/yyyy") {
                lblDate.text = "Total amount paid on \(paidDate)"
            }
        }
    }
    @IBOutlet weak var lblAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
