//
//  AppoinnmentPresentert.swift
//  DPS
//
//  Created by Vivek Patel on 14/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
protocol ScheduleView: CommonView {
    func sucessfullyScheduled(_ output: AppoinmentResponse)
    func sucessfullyReScheduled(_ output: RescheduleResponse)
    func successfullyUpdated(_ output: String)
    func notifiedSuccessfully(_ object: Bool)
}

class AppoinmentPresenter: NSObject {
    weak private var delegate: ScheduleView?
    private lazy var service = {
        
        return AppoinmentService()
    }()
    
    init(_ delegate: ScheduleView) {
        self.delegate = delegate
    }
    
    func apiCallForSchedule( _ parameters: ScheduleService) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        delegate?.showLoader()
        service.setupSchedule(parameters: parameters) { (object, error) in
            if let object = object {
                self.delegate?.sucessfullyScheduled(object)
            } else {
                self.delegate?.hideLoader()
                self.delegate?.showAlert(message: error)
            }
        }
    }
    func apiCallForReSchedule( _ parameters: ScheduleService) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        delegate?.showLoader()
        service.setupReSchedule(parameters: parameters) { (object, error) in
            if let object = object {
                self.delegate?.sucessfullyReScheduled(object)
            } else {
                self.delegate?.hideLoader()
                self.delegate?.showAlert(message: error)
            }
        }
    }
    func apiCallToUpdateSetup(_ leadNumber: String) {
        //  let encoder = JSONEncoder()
        /*encoder.outputFormatting = .prettyPrinted
         let orderJsonData = try! encoder.encode(parameters)
         print(String(data: orderJsonData, encoding: .utf8)!)*/
        let isInitialSetup = "true"
        //delegate?.showLoader()
        service.updateStatus(leadNumber, isInitialSetup:isInitialSetup) { (object, error) in
            if let object = object{
                if #available(iOS 13.0, *) {
                    self.delegate?.successfullyUpdated(object)
                } else {
                    self.delegate?.successfullyUpdated("Success")
                }
            } else {
                self.delegate?.hideLoader()
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    func apiCallForNotify(_ parameters: NotifyCustomerRequest) {
        //delegate?.showLoader()
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        service.notifyCustomer(parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.notifiedSuccessfully(object)
            }
        }
    }
}
