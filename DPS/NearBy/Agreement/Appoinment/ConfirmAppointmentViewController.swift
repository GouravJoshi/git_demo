//
//  CofirmAppointmentViewController.swift
//  DPS
//
//  Created by Vivek Patel on 05/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ConfirmAppointmentViewController: BaseViewController {
    private enum Sections: Int, CaseIterable {
        case property
        case headerTech
        case technician
        case service
        case pricing
        case payment
    }
    // MARK: - Outlets
    var services: [SoldServiceStandardDetail] = []
    var propertyDetails: PropertyDetails?
    var schedule: ScheduleService?
    var technician: Availability?
    var entityDetail: EntityDetail?
    var customTecnician: CustomTechnician?
    var isCustomTechnician = false
    private var isSendText = false
    private var isSendEmail = false
    private var scheduleResponse: AppoinmentResponse?
    private var rescheduleResponse: RescheduleResponse?

    private lazy var presenter = {
        return AppoinmentPresenter(self)
    }()
    var isUpdatedSuccessfully = false
    var fromReschedule = Bool()
    var strTimeRangeId = String()
    var strTimeRangeValue = String()

    @IBOutlet var btnHome: UIBarButtonItem!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Confirm Appointment"
        setFooterView()
        setEmployeeListToUserDefault()
        // Do any additional setup after loading the view.
        if fromReschedule
        {
            btnHome.isEnabled = false
            
        }
        else
        {
            btnHome.isEnabled = true
        }
        
    }
    private func getSendTextMeassage() -> String {
        guard let scheduleResponse = scheduleResponse else { return "" }
        var employeeList: [EmployeeListD2D] = []
        var leadTecnician: EmployeeListD2D?
        var instpector: EmployeeListD2D?
        var technicianName = ""
        var technicianEmail = ""
        var technicianPhone = ""
        var inspectorName = ""
        var inspectorEmail = ""
        var inspectorPhone = ""
        if let list = AppUserDefaults.shared.employeeList {
            employeeList = list
        }
        
        //Nilind
//        var finalStirng = AppUserDefaults.shared.leadDetailMaster?.textTemplateMasters.filter({$0.templateName == enumSendTextTemplateWorkOrder}).first(where: { (text) -> Bool in
//            return text.templateName == enumSendTextTemplateWorkOrder
//        })?.templateContent ?? ""
//        print(finalStirng)
        
        var finalStirng = AppUserDefaults.shared.masterDetails?.textTemplatesSelectListDcs.filter({$0.templateName == enumSendTextTemplateWorkOrder}).first(where: { (text) -> Bool in
            return text.templateName == enumSendTextTemplateWorkOrder
        })?.templateContent ?? ""
        print(finalStirng)
        
        //end
        
        if  !employeeList.isEmpty {
            leadTecnician = employeeList.filter({$0.employeeNo == scheduleResponse.employeeNo}).first
            instpector = employeeList.filter({$0.employeeId  == scheduleResponse.createdBy}).first
            
        }
        if let leadTecnician = leadTecnician {
            technicianName = leadTecnician.fullName ?? ""
            technicianEmail = leadTecnician.primaryEmail ?? ""
            technicianPhone = leadTecnician.primaryPhone ?? ""
        }
        if let instpector = instpector {
            inspectorName = instpector.fullName ?? ""
            inspectorEmail = instpector.primaryEmail ?? ""
            inspectorPhone = instpector.primaryPhone ?? ""
        }
        finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianName##", with: technicianName)
        finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianEmail##", with: technicianEmail)
        finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianPhone##", with: technicianPhone)
        finalStirng = finalStirng.replacingOccurrences(of: "##InspectorName##", with: inspectorName)
        finalStirng = finalStirng.replacingOccurrences(of: "##InspectorEmail##", with: inspectorEmail)
        finalStirng = finalStirng.replacingOccurrences(of: "##InspectorPhone##", with: inspectorPhone)
        let date = scheduleResponse.initialServiceDate ?? ""
        let scheduleDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "dd/MM/yyyy") ?? ""
        let time = scheduleResponse.initialServiceTime ?? ""
        let scheduleTime = AppUtility.convertStringToFormat(fromString: time, toFormat: "HH:mm:ss a") ?? ""
        let customerName = entityDetail?.leadDetail?.firstName ?? ""
        finalStirng = finalStirng.replacingOccurrences(of: "##ScheduleDate##", with: scheduleDate)
        finalStirng = finalStirng.replacingOccurrences(of: "##ScheduleTime##", with: scheduleTime)
        finalStirng = finalStirng.replacingOccurrences(of: "##CustomerName##", with: customerName)
        return finalStirng
    }
    
    @objc func actionOpenEmail() {
        if let email = propertyDetails?.propertyInfo?.email?.trimmed {
            actionEmail(email: email)
        }
    }
    @objc func actionOpenCall() {
        if let number = propertyDetails?.propertyInfo?.phone?.trimmed {
            actionCall(number: number)
        }
    }
    private func openMessageComposer() {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = getSendTextMeassage()
            var numberList = ""
            if let phoneDetails = entityDetail?.leadContactDetailEXTSerDc {
                numberList =  phoneDetails.compactMap({$0.contactNumber}).joined(separator: ",").trimmed
            } else if let phone = propertyDetails?.propertyInfo?.phone, !phone.trimmed.isEmpty {
                numberList = phone
            }
            let numbers = numberList.components(separatedBy: ",")
            controller.recipients = numbers
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    private func apiCallToNotifyCustomer() {
        guard let scheduleResponse = scheduleResponse else { return }
        if let emailList = entityDetail?.leadDetail?.primaryEmail,  !emailList.trimmed.isEmpty  {
            var parameter = NotifyCustomerRequest()
            parameter.companyKey = scheduleResponse.companyKey
            parameter.workOrderNo = scheduleResponse.workOrderNo?.description
            let emails = emailList.trimmed.components(separatedBy: ",")
            parameter.toEmails = emails
            presenter.apiCallForNotify(parameter)
        }
    }
    private func apiCallToGenerateWorkOrder()
    {
        guard let schedule = schedule else { return }
        if let scheduleResponse = self.scheduleResponse, let _ = scheduleResponse.accountNumber , let leadId = entityDetail?.leadDetail?.leadID?.description {
            presenter.apiCallToUpdateSetup(leadId)
        }
        else
        {
            if fromReschedule
            {
                presenter.apiCallForReSchedule(schedule)
            }
            else
            {
                presenter.apiCallForSchedule(schedule)
            }
        }
    }
    private func pushToReportView()
    {
        if isUpdatedSuccessfully
        {
            if self.isSendEmail
            {
                let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
                guard let destination = storyboard.instantiateViewController(withIdentifier: "ServiceReportViewController") as? ServiceReportViewController else { return }
                destination.isFromWorkOrder = true
                destination.scheduleResponse = scheduleResponse
                destination.entityDetail = entityDetail
                destination.propertyDetails = propertyDetails
                self.navigationController?.pushViewController(destination, animated: true)
                
            }
            else if self.isSendText
            {
                let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
                guard let destination = storyboard.instantiateViewController(withIdentifier: "SendTextViewController") as? SendTextViewController else {
                    return
                }
                destination.isFromWorkOrder = true
                destination.scheduleResponse = scheduleResponse
                destination.entityDetail = entityDetail
                destination.propertyDetails = propertyDetails
                self.navigationController?.pushViewController(destination, animated: true)
            }
        }
        else
        {
           
            apiCallToGenerateWorkOrder()

        }
    }
    // MARK: - Actions
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSendByText(_ sender: Any) {
        isSendText = true
        isSendEmail = false
        pushToReportView()
    }
    @IBAction func actionSendByMail(_ sender: Any) {
        isSendText = false
        isSendEmail = true
        pushToReportView()
    }
    @IBAction func actionProceed(_ sender: Any) {
        var numbers = ""
        var emails = ""
        if let phoneDetails = entityDetail?.leadContactDetailEXTSerDc {
            numbers =  phoneDetails.compactMap({$0.contactNumber}).joined(separator: ",").trimmed
        } else if let phone = propertyDetails?.propertyInfo?.phone, !phone.trimmed.isEmpty {
            numbers = phone
        }
        if let email = entityDetail?.leadDetail?.primaryEmail, !email.trimmed.isEmpty {
            emails = email
        }
        if !emails.isEmpty || !numbers.isEmpty {
            let storyboard =  UIStoryboard.init(name: "ScheduleD2D", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "NotifyViewController") as! NotifyViewController
            controller.isEmail = !emails.isEmpty
            controller.isText = !numbers.isEmpty
            controller.delegate = self
            controller.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(controller, animated: true, completion: nil)
        } else {
            guard let schedule = schedule else { return }
            if let scheduleResponse = self.scheduleResponse, let _ = scheduleResponse.accountNumber , let leadId = entityDetail?.leadDetail?.leadID?.description {
                presenter.apiCallToUpdateSetup(leadId)
            } else {
                presenter.apiCallForSchedule(schedule)
            }
        }
    }
    @IBAction func actionHome(_ sender: Any) {
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
    }
}
// MARK: - UITableViewDataSource
extension ConfirmAppointmentViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == Sections.service.rawValue {
            return services.count
        } else if section == Sections.pricing.rawValue {
            return  0 // 2
        }
        else if section == Sections.payment.rawValue {
            return  0
        }
        else {
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections(rawValue: indexPath.section) {
        
        case .property:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PropertyDetailHeaderTableViewCell.self), for: indexPath) as! PropertyDetailHeaderTableViewCell
            cell.propertyInfo = propertyDetails?.propertyInfo
            let tap = UITapGestureRecognizer(target: self, action: #selector(actionOpenCall))
            cell.stackPhone.addGestureRecognizer(tap)
            let emailTap = UITapGestureRecognizer(target: self, action: #selector(actionOpenEmail))
            cell.stackEmail.addGestureRecognizer(emailTap)
            return cell
        case .service:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ServiceListTableViewCell.self), for: indexPath) as! ServiceListTableViewCell
            let service = services[indexPath.row]
            cell.service = service
            
            cell.titleView.isHidden = indexPath.row == 0 ? false : true
            /* if let serviceDetails = AppUserDefaults.shared.masterDetails  {
             let name = serviceDetails.categories.flatMap { $0.services}.filter{$0.sysName == service.serviceSysName}.first?.name
             cell.lblName.text = name
             } else {
             cell.lblName.text = service.serviceSysName
             }
             if let serviceDescription = service.serviceDescription, !serviceDescription.trimmed.isEmpty {
             cell.lblDescription.isHidden = false
             } else {
             cell.lblDescription.isHidden = true
             }
             cell.lblDescription.attributedText = service.serviceDescription?.trimmed.html2AttributedString
             
             cell.lblFrequency.text = service.frequencySysName*/
            
            
            return cell
            
        case .pricing:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementPricingTableViewCell.self), for: indexPath) as! AgreementPricingTableViewCell
            if indexPath.row == 0 {
                cell.lblTitle.text  = "Initial Amount"
                let initialAmount = services.compactMap({$0.totalInitialPrice}).sum()
                let taxableServicesAmount = services.filter({($0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential") || ($0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential")}).compactMap({$0.totalInitialPrice}).sum()
                let discount = services.compactMap({$0.discount}).sum()
                let totalAmount = initialAmount - discount
                let totalTaxableAmount = taxableServicesAmount - discount
                var taxAmount = 0.0
                if let taxPercentage = Double(entityDetail?.leadDetail?.tax ?? "") {
                    taxAmount = (totalTaxableAmount/100)*(taxPercentage)
                }
                let billingAmount = totalAmount + taxAmount
                cell.lblSubTotal.text = initialAmount.currency
                cell.lblTax.text = taxAmount.currency
                cell.lblDiscount.text = discount.currency
                cell.lblBillingAmount.text = billingAmount.currency
                cell.viewTax.isHidden = true
                
            } else {
                cell.lblTitle.text  = "Maintenance Amount"
                let initialAmount = services.compactMap({$0.totalMaintPrice}).sum()
                let taxableServicesAmount = services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).compactMap({$0.totalMaintPrice}).sum()
                let discount = services.compactMap({$0.serviceTempMaitenance}).sum()
                let totalAmount = initialAmount - discount
                let totalTaxableAmount = taxableServicesAmount - discount
                var taxAmount = 0.0
                if let taxPercentage =  Double(entityDetail?.leadDetail?.tax ?? "") {
                    taxAmount = (totalTaxableAmount/100)*(taxPercentage)
                }
                let billingAmount = totalAmount + taxAmount
                cell.lblSubTotal.text = initialAmount.currency
                cell.lblTax.text = taxAmount.currency
                cell.lblDiscount.text = discount.currency
                cell.lblBillingAmount.text = billingAmount.currency
                cell.viewTax.isHidden = true
            }
            
            return cell
        case .headerTech:
            let cell = tableView.dequeueReusableCell(withIdentifier:  "TechnicianHeaderTableViewCellSecond") as! TechnicianHeaderTableViewCell
            //cell.lblTimeRange.text = "\(strTimeRangeValue)"
            return cell
        case .technician:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TechnicianScheduleTableViewCell.self), for: indexPath) as! TechnicianScheduleTableViewCell
            cell.lblDesignation.isHidden = true
            if isCustomTechnician {
                cell.lblName.text = customTecnician?.name
                cell.lblDate.text = customTecnician?.date
            } else {
                cell.lblName.text = technician?.employeeName
                if let dateString = technician?.availableDate, let timeString = technician?.startTime,  let date = AppUtility.convertStringToFormat(fromString: dateString, toFormat: "MM/dd/yyyy"),
                   let time = AppUtility.convertStringToFormat(fromString: timeString, toFormat: "hh:mm a")  {
                    cell.lblDate.text = date + "-" + time
                } else {
                    cell.lblDate.text = nil
                }
            }
            return cell
        case .payment:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentWorkOrderD2DTableViewCell.self), for: indexPath) as!  PaymentWorkOrderD2DTableViewCell
            cell.paymentInfo = entityDetail?.paymentInfo
            return cell
        case .none:
            return UITableViewCell()
        }
        
    }
}
// MARK: - UITableViewDelegate
extension ConfirmAppointmentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderTitleTableViewCell.self)) as! HeaderTitleTableViewCell
        switch Sections(rawValue: section) {
        case .pricing:
            cell.lblTitle.text = "Final Price"
            return cell.contentView
        case .headerTech:
            cell.lblTitle.text = "Agreement Details"
            return cell.contentView
//        case .payment:
//            cell.lblTitle.text = "Payment"
//            return cell.contentView
        default:
            return UIView()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch Sections(rawValue: section) {
        case .headerTech: //.payment .pricing,
            return 1//44
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        switch Sections(rawValue: section) {
        case .technician: //.payment .pricing,
            return 44
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderTitleTableViewCell.self)) as! HeaderTitleTableViewCell
        switch Sections(rawValue: section) {
        case .technician:
            //cell.lblTitle.text = "Time Range \(strTimeRangeValue)"
            //cell.lblTitle.textColor = UIColor.darkGray
            //cell.lblTitle.font = UIFont.systemFont(ofSize: 14)
            let attrs = [
                NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16),
                NSAttributedString.Key.foregroundColor : UIColor.theme()
                ] as [NSAttributedString.Key : Any]
            let buttonTitleStr = NSMutableAttributedString(string:"Time Range  ", attributes:attrs)
            
            let attrs2 = [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
                NSAttributedString.Key.foregroundColor : UIColor.black] as [NSAttributedString.Key : Any]
            let buttonTitleStr2 = NSMutableAttributedString(string:strTimeRangeValue, attributes:attrs2)
            
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            attributedString.append(buttonTitleStr2)

            cell.lblTitle.attributedText = attributedString
            
            
            return cell.contentView
        default:
            return UIView()
        }
    }
}

// MARK: - MapViewNavigationDelegate
extension ConfirmAppointmentViewController: MapViewNavigationDelegate {
    func actionBack() {
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
    }
}
extension ConfirmAppointmentViewController: ScheduleView {
    func proceedAfterEmail() {
        if isSendText {
            #if TARGET_OS_SIMULATOR
            let storyboard =  UIStoryboard.init(name: "Agreement", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AppoinmentSuccessViewController") as! AppoinmentSuccessViewController
            controller.delegate = self
            controller.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(controller, animated: true, completion: nil)
            #else
            openMessageComposer()
            #endif
        }  else {
            let storyboard =  UIStoryboard.init(name: "Agreement", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AppoinmentSuccessViewController") as! AppoinmentSuccessViewController
            controller.delegate = self
            controller.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(controller, animated: true, completion: nil)
        }
    }
    func notifiedSuccessfully(_ object: Bool) {
        if object {
            proceedAfterEmail()
        } else {
            showAlertWithCompletion(message: "Unable to send Email") {
                self.proceedAfterEmail()
            }
        }
    }
    func sucessfullyScheduled(_ output: AppoinmentResponse)
    {
        if let _ = output.accountNumber , let leadId = entityDetail?.leadDetail?.leadID?.description {
            self.scheduleResponse = output
            presenter.apiCallToUpdateSetup(leadId)
        }
        else
        {
            self.hideLoader()
            showAlert(message: "Unable to create work order now, please try again later")
        }
    }
    func sucessfullyReScheduled(_ output: RescheduleResponse)
    {
        if let _ = output.result , let leadId = entityDetail?.leadDetail?.leadID?.description
        {
            if output.result == "Success"
            {
                self.rescheduleResponse = output
                presenter.apiCallToUpdateSetup(leadId)
            }
            else
            {
                self.hideLoader()
                let error = output.Errors![0]
                showAlert(message: "Unable to reschedule work order due to error: \(error)")
                //showAlert(message: "\(output.Errors?.description ?? "Unable to reschedule work order now, please try again later")")

                //showAlert(message: "\(output.Errors?.description ?? "Unable to reschedule work order now, please try again later")")
                //showAlert(message: "Unable to reschedule work order now, please try again later")
            }
           
        }
        else
        {
            self.hideLoader()
            showAlert(message: "Unable to reschedule work order now, please try again later")
        }
    }

    func successfullyUpdated(_ output: String) {
        isUpdatedSuccessfully = true
        hideLoader()
        if output == "Success" {
            if isSendEmail {
                let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
                guard let destination = storyboard.instantiateViewController(withIdentifier: "ServiceReportViewController") as? ServiceReportViewController else { return }
                destination.isFromWorkOrder = true
                destination.scheduleResponse = scheduleResponse
                destination.entityDetail = entityDetail
                destination.propertyDetails = propertyDetails
                destination.fromReschedule = fromReschedule
                destination.rescheduleResponse = rescheduleResponse

                self.navigationController?.pushViewController(destination, animated: true)
                
                // apiCallToNotifyCustomer()
            } else if isSendText {
                let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
                guard let destination = storyboard.instantiateViewController(withIdentifier: "SendTextViewController") as? SendTextViewController else {
                    return
                }
                destination.isFromWorkOrder = true
                destination.scheduleResponse = scheduleResponse
                destination.entityDetail = entityDetail
                destination.propertyDetails = propertyDetails
                destination.fromReschedule = fromReschedule
                destination.rescheduleResponse = rescheduleResponse

                self.navigationController?.pushViewController(destination, animated: true)
                /* hideLoader()
                 #if TARGET_OS_SIMULATOR
                 #else
                 openMessageComposer()
                 #endif*/
            } /* else {
             hideLoader()
             let storyboard =  UIStoryboard.init(name: "Agreement", bundle: nil)
             let controller = storyboard.instantiateViewController(withIdentifier: "AppoinmentSuccessViewController") as! AppoinmentSuccessViewController
             controller.delegate = self
             controller.modalPresentationStyle = .overFullScreen
             self.navigationController?.present(controller, animated: true, completion: nil)
             }*/
        } else {
            hideLoader()
            //need to confirm  the flow
            showAlert(message: "Unable to update setup now, please try again later")
        }
    }
}
extension ConfirmAppointmentViewController: NotifyCustomerDelegate {
    func notifyCustomer(text: Bool , email: Bool) {
        isSendText = text
        isSendEmail = email
        guard let schedule = schedule else { return }
        presenter.apiCallForSchedule(schedule)
    }
}
extension ConfirmAppointmentViewController: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true) {
            let storyboard =  UIStoryboard.init(name: "Agreement", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AppoinmentSuccessViewController") as! AppoinmentSuccessViewController
            controller.delegate = self
            controller.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(controller, animated: true, completion: nil)
        }
    }
}

