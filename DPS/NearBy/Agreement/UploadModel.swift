//
//  UploadModel.swift
//  DPS
//
//  Created by Vivek Patel on 12/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

struct UploadResponse: Codable {
    
    var extensionText: String?
    //var length: Int?
    var name: String?
    var relativePath: String?
    
    enum CodingKeys: String, CodingKey {
        case extensionText = "Extension"
        //case length = "Length"
        case name = "Name"
        case relativePath = "RelativePath"
    }
}

struct ImageToUpload: Comparable {
    static func < (lhs: ImageToUpload, rhs: ImageToUpload) -> Bool {
        return lhs.type == rhs.type
    }
    
    var image: UIImage
    var type: UploadType
    var fileName: String
}
