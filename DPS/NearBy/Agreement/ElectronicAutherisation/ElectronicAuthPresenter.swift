//
//  ElectronicAuthPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 09/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

import Foundation
protocol ElectronicAuthView: CommonView {
    func uploadImage(type: UploadType, uploadResponse: UploadResponse)
}

class ElectronicAuthPresenter: NSObject {
    weak private var delegate: ElectronicAuthView?
    private lazy var service = {
        return SendAgreementService()
    }()
    
    init(_ delegate: ElectronicAuthView) {
        self.delegate = delegate
    }
    
    func apiCallForUploadSignature(type: UploadType, image: UIImage, fileName: String) {
        self.delegate?.showLoader()
        service.uploadImageEAF(image: image, fileName: fileName, type: type) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object?.first{
                self.delegate?.uploadImage(type: type, uploadResponse: object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    
}
