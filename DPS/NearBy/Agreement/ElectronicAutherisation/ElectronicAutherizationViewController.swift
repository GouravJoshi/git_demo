//
//  Electronic AuthorizationViewController.swift
//  Electronic Authorization i Phone
//
//  Created by New User on 02/12/20.
//

import UIKit
protocol ElectronicFormDelegate {
    func gotFormDetails(_ electronicFromDetails: ElectronicAuthorizationForm)
}
final class ElectronicAuthorizationViewController: BaseViewController {
    //MARK:- Variable
    var selectedState: State?
    var propertyDetails:PropertyDetails?
    var entityDetail: EntityDetail?
    private lazy var presenter = {
        return ElectronicAuthPresenter(self)
    }()
    private var customerSignaturePath: String?
    var formDetail: ElectronicAuthorizationForm?
    private var electronicAuthorizationForm = ElectronicAuthorizationForm()
    var delegate: ElectronicFormDelegate?
    var isEditable = true
    //MARK: - Outlets
    @IBOutlet weak var btnEftChecking: UIButton!
    @IBOutlet weak var btnMonthly: UIButton!
    @IBOutlet weak var btnFrequency: UIButton!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var imgViewCompany: UIImageView!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtViewTitle: UITextView!
    @IBOutlet weak var txtViewPreNotes: UITextView!
    @IBOutlet weak var txtviewTerms: UITextView!
    @IBOutlet weak var btnCredit: UIButton!
    @IBOutlet weak var txtViewPostNotes: UITextView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtMiddleName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtAddressLine1: UITextField!
    @IBOutlet weak var txtAddressLine2: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtDayofmonth: UITextField!
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtAccountName: UITextField!
    @IBOutlet weak var txtRountingName: UITextField!
    @IBOutlet weak var imgViewCustomerSignature: UIImageView!
    @IBOutlet weak var viewMain: UIStackView!
    
    @IBOutlet var lblBankName: UILabel!
    @IBOutlet var lblAccountNo: UILabel!
    @IBOutlet var lblRoutingNo: UILabel!
    
    @IBOutlet var btnMobileNoForCreditCard: UIButton!
    
    @IBOutlet var const_TxtViewTitle_H: NSLayoutConstraint!
    @IBOutlet var const_TxtViewPreNotes_H: NSLayoutConstraint!
    @IBOutlet var const_TxtViewTerms_H: NSLayoutConstraint!
    @IBOutlet var const_TxtViewPostNotes_H: NSLayoutConstraint!

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Payment Authorization"
        txtViewTitle.layer.borderWidth = 0.0
        setData()
    }
    private func setData() {
        
        txtViewTitle.isEditable = false
        txtViewPreNotes.isEditable = false
        txtViewPostNotes.isEditable = false
        txtviewTerms.isEditable = false
        txtViewTitle.delegate = self
        txtViewPreNotes.delegate = self
        txtViewPostNotes.delegate = self
        txtviewTerms.delegate = self

        
        if  isEditable {
            viewMain.isUserInteractionEnabled = true
            btnTerms.isUserInteractionEnabled = true
            btnSubmit.isUserInteractionEnabled = true
        } else {
            viewMain.isUserInteractionEnabled = false
            btnTerms.isUserInteractionEnabled = false
            btnSubmit.isUserInteractionEnabled = false
        }
        if let formDetail = formDetail {
            self.electronicAuthorizationForm = formDetail
            setCoomonData(electronicAuthorizationForm)
        } else if let entityDetail = entityDetail, let formDetails = entityDetail.electronicAuthorizationForm  {
            self.electronicAuthorizationForm = formDetails
            setCoomonData(electronicAuthorizationForm)
        } else  {
            if let masterDetails = AppUserDefaults.shared.masterDetails, let electronicAuthorizationFormMaster = masterDetails.electronicAuthorizationFormMaster {
                txtviewTerms.attributedText = electronicAuthorizationFormMaster.terms?.trimmed.html2AttributedString
                txtViewPreNotes.attributedText = electronicAuthorizationFormMaster.preNotes?.trimmed.html2AttributedString
                txtViewPostNotes.attributedText = electronicAuthorizationFormMaster.postNotes?.trimmed.html2AttributedString
                txtViewTitle.attributedText = electronicAuthorizationFormMaster.title?.trimmed.html2AttributedString
                btnFrequency.isSelected =  true
                btnEftChecking.isSelected = true
            }
            if let propertyDetails = propertyDetails {
                switch propertyDetails.propertyType {
                case .empty:
                    txtFirstName.text = propertyDetails.propertyInfo?.firstName
                    txtLastName.text = propertyDetails.propertyInfo?.lastName
                    txtMiddleName.text = propertyDetails.propertyInfo?.middleName
                    txtEmail.text = propertyDetails.propertyInfo?.email
                    txtContact.text = propertyDetails.propertyInfo?.phone
                    txtCity.text = propertyDetails.propertyInfo?.city
                    txtZipcode.text = propertyDetails.propertyInfo?.zipCode
                    txtState.text = propertyDetails.propertyInfo?.state?.stateSysName
                    txtAddressLine1.text = propertyDetails.propertyInfo?.address?.components(separatedBy: ",").first
                    selectedState = propertyDetails.propertyInfo?.state
                    electronicAuthorizationForm.leadNo = entityDetail?.leadDetail?.leadNumber
                    electronicAuthorizationForm.accountNo = entityDetail?.leadDetail?.accountNo
                    
                case .lead:
                    txtFirstName.text = propertyDetails.lead?.firstName
                    txtLastName.text = propertyDetails.lead?.lastName
                    txtMiddleName.text = propertyDetails.lead?.middleName
                    txtEmail.text = propertyDetails.lead?.primaryEmail
                    txtContact.text = propertyDetails.lead?.cellPhone1
                    txtCity.text = propertyDetails.lead?.cityName
                    txtZipcode.text = propertyDetails.lead?.zipcode
                    if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                        return state.stateID == propertyDetails.lead?.stateID
                    }) {
                        txtState.text = state.name
                        selectedState = state
                    }
                    txtAddressLine1.text =  propertyDetails.lead?.address1
                    txtAddressLine2.text =  propertyDetails.lead?.address2
                    electronicAuthorizationForm.accountNo = propertyDetails.lead?.accountNo
                    electronicAuthorizationForm.leadNo = propertyDetails.lead?.leadNumber
                case .account:
                    txtFirstName.text = propertyDetails.account?.firstName
                    txtLastName.text = propertyDetails.account?.lastName
                    txtMiddleName.text = propertyDetails.account?.middleName
                    txtEmail.text = propertyDetails.account?.primaryEmail
                    txtContact.text = propertyDetails.account?.primaryPhone
                    txtCity.text = propertyDetails.account?.serviceAddress?.cityName
                    txtZipcode.text = propertyDetails.account?.serviceAddress?.zipcode
                    if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                        return state.stateID == propertyDetails.account?.serviceAddress?.stateID
                    }) {
                        txtState.text = state.name
                        selectedState = state
                    }
                    txtAddressLine1.text = propertyDetails.account?.serviceAddress?.address1
                    txtAddressLine2.text = propertyDetails.account?.serviceAddress?.address2
                    electronicAuthorizationForm.accountNo = propertyDetails.account?.accountNumber
                //electronicAuthorizationForm.leadNo = propertyDetails.account?.
                case .opportunity:
                    txtFirstName.text = propertyDetails.opportunity?.firstName
                    txtLastName.text = propertyDetails.opportunity?.lastName
                    txtMiddleName.text = propertyDetails.opportunity?.middleName
                    txtEmail.text = propertyDetails.opportunity?.primaryEmail
                    txtContact.text = propertyDetails.opportunity?.primaryPhone
                    txtCity.text = propertyDetails.opportunity?.serviceAddress?.cityName
                    txtZipcode.text = propertyDetails.opportunity?.serviceAddress?.zipcode
                    if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                        return state.stateID == propertyDetails.opportunity?.serviceAddress?.stateID
                    }) {
                        txtState.text = state.name
                        selectedState = state
                    }
                    txtAddressLine1.text = propertyDetails.opportunity?.serviceAddress?.address1
                    txtAddressLine2.text = propertyDetails.opportunity?.serviceAddress?.address2
                    electronicAuthorizationForm.accountNo = propertyDetails.opportunity?.accountNo
                    electronicAuthorizationForm.leadNo = propertyDetails.opportunity?.opportunityNumber
                }
            }
            //electronicAuthorizationForm.accountNo = propertyDetails?.opportunity?.accountNo
            //electronicAuthorizationForm.leadNo = propertyDetails?.opportunity?.opportunityNumber
        }
        if btnMonthly.isSelected == true
        {
            txtDayofmonth.isHidden = false
        }
        else
        {
            txtDayofmonth.isHidden = true
        }
        hideBankDetail()
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        var strPhoneNo = "\(dictLoginData.value(forKeyPath: "Company.PrimaryPhone") ?? "")"
        if strPhoneNo.count == 0 || strPhoneNo == ""
        {
            strPhoneNo = "Us at office";
        }
        btnMobileNoForCreditCard.setTitle(strPhoneNo, for: .normal)

        let strCompanyImgUrl = "\(dictLoginData.value(forKeyPath: "Company.LogoImagePath") ?? "")"
        
        if  let urlString = strCompanyImgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)  {
            imgViewCompany.kf.setImage(with: url)
        }
        
        textViewDidChange(txtViewPreNotes)
        textViewDidChange(txtViewPostNotes)
        textViewDidChange(txtviewTerms)



    }
    private func setCoomonData(_ formDetails:ElectronicAuthorizationForm) {
        txtFirstName.text = formDetails.firstName
        txtLastName.text = formDetails.lastName
        txtMiddleName.text = formDetails.middleName
        txtEmail.text = formDetails.email
        txtContact.text = formDetails.phone
        txtCity.text = formDetails.cityName
        txtZipcode.text = formDetails.zipcode
        txtAddressLine1.text = formDetails.address1
        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
            return state.stateID == formDetails.stateID
        }) {
            txtState.text = state.name
            selectedState = state
        }
        txtviewTerms.text = formDetails.terms
        txtViewPreNotes.text = formDetails.preNotes
        txtViewPostNotes.text = formDetails.postNotes
        txtViewTitle.text = formDetails.title
        btnTerms.isSelected =  formDetails.termsSignUp ?? false
        txtDayofmonth.text = formDetails.monthlyDate?.description
        txtBankName.text = formDetails.bankName
        self.customerSignaturePath = formDetails.signaturePath
        if  formDetails.paymentMethod == "EFT" {
            btnEftChecking.isSelected = true
            btnCredit.isSelected = false
        } else {
            btnCredit.isSelected = true
            btnEftChecking.isSelected = false
        }
        if formDetails.frequency == "Monthly" {
            btnMonthly.isSelected =  true
            btnFrequency.isSelected =  false
        } else {
            btnFrequency.isSelected =  true
            btnMonthly.isSelected =  false
        }
        hideBankDetail()
        if let date = formDetails.date {
        let dateString = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy") // dd/MM/yyyy
            txtDate.text = dateString
        }
        if let customerSignature = formDetails.signaturePath {
            if  let urlString = (URL.salesAutoBaseUrl  + "/Documents/Signature/" + customerSignature).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)  {
                imgViewCustomerSignature.kf.setImage(with: url)
            }
            
        }
        
    }
    func proceedValidation() {
        
        if let email = txtEmail.text?.trimmed, !email.isEmpty {
            guard email.isValidEmail() else {
                showAlert(message: "Please Enter Valid Email")
                return
            }
        }
        guard  let customerSignaturePath = customerSignaturePath, !customerSignaturePath.isEmpty else {
            showAlert(message: "Please add customer signature")
            return
        }
        if btnFrequency.isSelected {
            electronicAuthorizationForm.frequency = "Service"
        } else {
            guard !txtDayofmonth.text!.isEmpty else {
                showAlert(message: "Please enter day of month.")
                return
            }
            electronicAuthorizationForm.frequency = "Monthly"
            electronicAuthorizationForm.monthlyDate = Int(txtDayofmonth.text ?? "")
        }
        if btnEftChecking.isSelected {
            electronicAuthorizationForm.paymentMethod = "EFT"
            guard !txtBankName.text!.isEmpty else {
                showAlert(message: "Please enter bank name")
                return
            }
            guard !txtAccountName.text!.isEmpty else {
                showAlert(message: "Please enter account number.")
                return
            }
            guard !txtRountingName.text!.isEmpty else {
                showAlert(message: "Please enter routing number.")
                return
            }
            electronicAuthorizationForm.bankAccountNo = txtAccountName.text
            electronicAuthorizationForm.routingNo = txtRountingName.text
            electronicAuthorizationForm.bankName = txtBankName.text
            
        } else {
            electronicAuthorizationForm.paymentMethod = "CreditCard"
        }
        guard !txtDate.text!.isEmpty else {
            showAlert(message: "Please select date.")
            return
        }
        electronicAuthorizationForm.terms = txtviewTerms.text
        electronicAuthorizationForm.termsSignUp = btnTerms.isSelected
        electronicAuthorizationForm.title = txtViewTitle.text
        electronicAuthorizationForm.zipcode = txtZipcode.text
        electronicAuthorizationForm.address1 = txtAddressLine1.text
        electronicAuthorizationForm.address2 = txtAddressLine2.text
        electronicAuthorizationForm.email = txtEmail.text
        electronicAuthorizationForm.middleName = txtMiddleName.text
        electronicAuthorizationForm.cityName = txtCity.text
        electronicAuthorizationForm.date = txtDate.text
        electronicAuthorizationForm.postNotes = txtViewPostNotes.text
        electronicAuthorizationForm.preNotes = txtViewPreNotes.text
        electronicAuthorizationForm.stateID = selectedState?.stateID
        electronicAuthorizationForm.firstName =  txtFirstName.text
        electronicAuthorizationForm.phone = txtContact.text
        electronicAuthorizationForm.lastName = txtLastName.text
        electronicAuthorizationForm.signaturePath = "\("\\Signatures\\")" + "\(customerSignaturePath)"
        var createdBy: Int?
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        createdBy = dictLoginData.value(forKey: "EmployeeId")! as? Int
        electronicAuthorizationForm.createdBy = createdBy
        self.navigationController?.popViewController(animated: true)
        delegate?.gotFormDetails(electronicAuthorizationForm)
    }
    func showDayofmonth()
    {
        if btnMonthly.currentImage == UIImage(named: "RadioButton-Unselected.png")
        {
            txtDayofmonth.isHidden = true
        }
        else if btnMonthly.currentImage == UIImage(named: "RadioButton-Selected.png")
        {
            txtDayofmonth.isHidden = false
        }
        else
        {
            txtDayofmonth.isHidden = true
        }
    }
    // MARK: - Actions
    
    @IBAction func actionOpenSignatureView(_ sender: UITapGestureRecognizer) {
        openSignatureView()
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionTerms(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        proceedValidation()
    }
    @IBAction func actionAtfreqService(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            btnMonthly.isSelected = false

        } else {
            btnMonthly.isSelected = true
        }
        
        if btnMonthly.isSelected == true
        {
            txtDayofmonth.isHidden = false
        }
        else
        {
            txtDayofmonth.isHidden = true
        }
        
    }
    @IBAction func actionEasymthlyPayment(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            btnFrequency.isSelected = false
        } else {
            btnFrequency.isSelected = true
        }
        
        if btnMonthly.isSelected == true
        {
            txtDayofmonth.isHidden = false
        }
        else
        {
            txtDayofmonth.isHidden = true
        }
    }
    
    @IBAction func actionCheckingDraft(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            btnCredit.isSelected = false
        } else {
            btnCredit.isSelected = true
        }
        hideBankDetail()
    }
    @IBAction func actionCreditCard(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            btnEftChecking.isSelected = false
        } else {
            btnEftChecking.isSelected = true
        }
        hideBankDetail()
        
    }
    func hideBankDetail()
    {
        if btnCredit.isSelected == true
        {
            lblBankName.isHidden = true
            txtBankName.isHidden = true
            lblAccountNo.isHidden = true
            txtAccountName.isHidden = true
            lblRoutingNo.isHidden = true
            txtRountingName.isHidden = true
        }
        else
        {
            lblBankName.isHidden = false
            txtBankName.isHidden = false
            lblAccountNo.isHidden = false
            txtAccountName.isHidden = false
            lblRoutingNo.isHidden = false
            txtRountingName.isHidden = false
        }
    }
    @IBAction func actionOnPhoneNo(_ sender: Any)
    {
        if btnMobileNoForCreditCard.currentTitle == "Us at office"
        {
            
        }
        else
        {
            Global().calling(btnMobileNoForCreditCard.titleLabel?.text)
        }

    }
    
}
// MARK: - UITextFieldDelegate
extension ElectronicAuthorizationViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate {
            showDatePicker(sender: txtDate, selectedDate: Date(), minimumDate: Date(), pickerType: .date) { (selectedDate, _) in
                let format = DateFormatter()
                format.dateFormat = "MM/dd/yyyy"//"dd/MM/yyyy" 
                let formattedDate = format.string(from: selectedDate)
                self.txtDate.text = formattedDate
            }
            return false
        } else  if textField == txtState {
            let states = AppUtility.fetchStates() ?? []
            var rows:[String] = []
            var selectedRow = 0
            guard !states.isEmpty else {
                showAlert(message: "No record found !!")
                return false }
            let types = states
            for (index,row) in types.enumerated() {
                if textField.text == row.name{
                    selectedRow = index
                }
                rows.append(row.name ?? "")
            }
            showStringPicker(sender: txtState, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                self.txtState.text = value
            }
            return false
        }
        return true
        
    }
}
extension ElectronicAuthorizationViewController : UITextViewDelegate
{
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == txtViewPreNotes
        {
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            const_TxtViewPreNotes_H.constant = newFrame.size.height + 50
            //textView.frame = newFrame;
        }
        else if textView == txtviewTerms
        {
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            const_TxtViewTerms_H.constant = newFrame.size.height + 50

            //textView.frame = newFrame;
        }
        else if textView == txtViewPostNotes
        {
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            const_TxtViewPostNotes_H.constant = newFrame.size.height + 50

            //textView.frame = newFrame;
        }
      
        
        
    }
}
extension ElectronicAuthorizationViewController {
    @objc private func openSignatureView() {
        
        guard let vc = UIStoryboard.init(name: "Agreement", bundle: nil).instantiateViewController(withIdentifier: "SignatureViewController") as? SignatureViewController else { return }
        vc.title = "Customer's Signature"
        vc.signatureActionHandler  =  { [weak self](image) -> () in
            self?.imgViewCustomerSignature.image = image
            let timeStamp  = Date().currentTimeMillis()
            if let image = image {
                self?.presenter.apiCallForUploadSignature(type: .customer, image: image, fileName: "\("\\Signatures\\")\(String.random())-\(timeStamp).jpeg")
            }
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}

extension ElectronicAuthorizationViewController: ElectronicAuthView {
    func uploadImage(type: UploadType, uploadResponse: UploadResponse) {
        self.customerSignaturePath = uploadResponse.name
    }
}
