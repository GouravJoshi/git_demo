//
//  HelpTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 02/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class HelpTableViewCell: UITableViewCell {
// MARK: - Outlets
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
