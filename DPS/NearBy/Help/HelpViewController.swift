//
//  HelpViewController.swift
//  DPS
//
//  Created by Vivek Patel on 02/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import Foundation

final class HelpViewController: BaseViewController {
    // MARK: - Variables
    var employeeHelpDocuments: [EmployeeHelpDocument] = [] {
        didSet {
            lblNorecordFound.isHidden = !employeeHelpDocuments.isEmpty
            tableView.reloadData()
        }
    }
    // MARK: - Outlets
    @IBOutlet weak var lblNorecordFound: UILabel!
    @IBOutlet weak var tableView: UITableView!
    // MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Help Center"
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        if let employeeHelpDocuments = AppUserDefaults.shared.leadDetailMaster?.employeeHelpDocuments {
            self.employeeHelpDocuments = employeeHelpDocuments
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func shareDocuments(path: String) {
        let fileURL = URL(fileURLWithPath: path)
        // try jsonData.write(to: fileURL, options: .atomic)
        let vc = UIActivityViewController(activityItems: [fileURL], applicationActivities: [])
        self.dismiss(animated: true) {
            self.present(vc, animated: false)
        }
    }
}

//MARK: -UITableViewDataSource
extension HelpViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employeeHelpDocuments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HelpTableViewCell.self), for: indexPath) as! HelpTableViewCell
        let help = employeeHelpDocuments[indexPath.row]
        if help.type == "Link" {
            cell.imgView.image = UIImage(named: "Link")
        } else {
            cell.imgView.image = UIImage(named: "downloadD2D")
            print("Document")
        }
        //cell.lblTitle.text = "Demo Title"
        cell.lblTitle.text = help.title
        cell.lblDescription.text = help.welcomeDescription
        return cell
    }
}
extension HelpViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let help = employeeHelpDocuments[indexPath.row]
        if help.type == "Link" {
            guard let urlString = help.path, let tempstring = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: tempstring) else { return }
            UIApplication.shared.open(url)
        } else {
            
            guard let urlString = help.path, let tempstring = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: tempstring) else { return }
            showLoader()
            FileDownloader.loadFileAsync(url: url) { (path, error) in
                print("PDF File downloaded to : \(path!)")
                self.hideLoader()
                DispatchQueue.main.async {
                    self.shareDocuments(path: path ?? "")
                }
            }
        }
    }
}
class FileDownloader {
    
    static func loadFileSync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)
        
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else if let dataFromURL = NSData(contentsOf: url)
        {
            if dataFromURL.write(to: destinationUrl, atomically: true)
            {
                print("file saved [\(destinationUrl.path)]")
                completion(destinationUrl.path, nil)
            }
            else
            {
                print("error saving file")
                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                completion(destinationUrl.path, error)
            }
        }
        else
        {
            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
            completion(destinationUrl.path, error)
        }
    }
    
    static func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)
        
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
                                            {
                                                data, response, error in
                                                if error == nil
                                                {
                                                    if let response = response as? HTTPURLResponse
                                                    {
                                                        if response.statusCode == 200
                                                        {
                                                            if let data = data
                                                            {
                                                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                                                {
                                                                    completion(destinationUrl.path, error)
                                                                }
                                                                else
                                                                {
                                                                    completion(destinationUrl.path, error)
                                                                }
                                                            }
                                                            else
                                                            {
                                                                completion(destinationUrl.path, error)
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    completion(destinationUrl.path, error)
                                                }
                                            })
            task.resume()
        }
    }
}
