//
//  MapService.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class MapService{
    
    func geocodeLocation(_ location: CLLocation, callBack: @escaping (_ placemark: CLPlacemark?, _ error:String?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: {
            placemarks, error in
            if error == nil && placemarks?.count ?? 0 > 0, let placemark = placemarks?.last {
                callBack(placemark, nil)
            } else {
                callBack(nil, error?.localizedDescription ?? "Unknown Error")
                
            }
        })
        
    }
    func getPlaceId(_ url: URL, callBack: @escaping (_ placemark: LocationResult?, _ error:String?) -> Void) {
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                do {
                    //let response = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    let object = try JSONDecoder().decode(SearchLocation.self, from: data)
                    guard let loactionDetails = object.results.first else { return }
                    DispatchQueue.main.async {
                        callBack(loactionDetails, nil)
                    }
                } catch let error {
                    callBack(nil, error.localizedDescription)
                    
                }
            } else {
                callBack(nil, error?.localizedDescription ?? "Unknown Error")
            }
        }.resume()
    }
    
    func getPlaceDetail(_ url: URL, callBack: @escaping (_ placeDetail: PlaceComponets?, _ error:String?) -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                do {
                    //                    let response = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    //                    print(response)
                    let object = try JSONDecoder().decode(PlaceDetail.self, from: data)
                    guard let loactionDetails = object.result else { return }
                    DispatchQueue.main.async {
                        callBack(loactionDetails, nil)
                    }
                    
                } catch let error {
                    callBack(nil, error.localizedDescription)
                }
            } else {
                callBack(nil, error?.localizedDescription ?? "Unknown Error")
            }
        }.resume()
        
    }
    func getPlaceDetailUsingZilliow(_ url: URL, callBack: @escaping (_ mapDetail: MapResult?, _ error:String?) -> Void) {
        
        XMLConverter.convertXMLURL(url) { (success, value, error) in
            if let value = value {
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode(MapResult.self, from: data)
                    if (object.searchResult.response?.results?.result) != nil {
                        callBack(object, nil)
                    }
                    else{
                        callBack(nil, error?.localizedDescription ?? "Unknown Error")
                    }
                } catch let error {
                    callBack(nil, error.localizedDescription )
                }
            } else {
                callBack(nil, error?.localizedDescription ?? "Unknown Error")
            }
        }
    }
    
    func getAssingAreaForUser(empId:String, companyKey: String, _ callBack:@escaping (_ object: MapGeoFencingD2D?,_ error:String?) -> Void) {
        let url = URL.GeoMapping.getAssignArea.replacingOccurrences(of: "(employeeId)", with: empId).replacingOccurrences(of: "(companykey)", with: companyKey)
        print(url)
        AF.request(url, method: .get, headers: URL.headers) .responseDecodable(of: MapGeoFencingD2D?.self) { response in
            
            switch response.result
            {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
            
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode(MapGeoFencingD2D?.self, from: data)
                    print(object)
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
    
}
