//
//  PlaceAnnotation.swift
//  DPS
//
//  Created by Vivek Patel on 16/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import MapKit

class PlaceAnnotation: NSObject, MKAnnotation {
    
    /*
     This property is declared with `@objc dynamic` to meet the API requirement that the coordinate property on all MKAnnotations
     must be KVO compliant.
     */
    @objc dynamic var coordinate: CLLocationCoordinate2D
    
    @objc dynamic var title: String?
    var lead: Lead?
    var opportunity: Opportunity?
    var account: Account?
    var searched = false
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        super.init()
    }
}
