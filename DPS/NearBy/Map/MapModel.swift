//
//  MapModel.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import UIKit

struct MapResult: Codable {
    var searchResult: MapResults
    enum CodingKeys: String, CodingKey {
        case searchResult = "SearchResults:searchresults"
    }
}
struct MapResults: Codable {
    var response: MapResponse?
    var message:ErrorMapMessage?
}
struct MapResponse: Codable {
    var results: MapLocationResults?
}
struct MapLocationResults:Codable {
    var result: MapLocationResultType
}
struct MapLocationResult: Codable {
    var FIPScounty: String?
    var bathrooms: String?
    var bedrooms: String?
    var finishedSqFt: String?
    var lotSizeSqFt: String?
    var totalRooms: String?
    var lastUpdated: String?
    var zpid: String?
    var address: MapLocationAddress
    
    enum CodingKeys: String, CodingKey {
        case FIPScounty
        case bathrooms
        case bedrooms
        case finishedSqFt
        case lotSizeSqFt
        case totalRooms
        case zpid
        case address
        case lastUpdated = "last-updated"
    }
}
struct MapLocationAddress: Codable {
    var city: String?
    var latitude: String?
    var longitude: String?
    var state: String?
    var street: String?
    var zipcode: String?
}
struct ErrorMapMessage: Codable {
    var code:String?
    var text:String?
    
}

enum MapLocationResultType: Codable {
    case array([MapLocationResult])
    case dictAny(MapLocationResult)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([MapLocationResult].self) {
            self = .array(x)
            return
        }
        if let x = try? container.decode(MapLocationResult.self) {
            self = .dictAny(x)
            return
        }
        throw DecodingError.typeMismatch(MapLocationResultType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MapLocationResult"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .array(let x):
            try container.encode(x)
        case .dictAny(let x):
            try container.encode(x)
        }
    }
}
struct PropertyInfo {
    var placeMark: CLPlacemark?
    var name: String?
    var email: String?
    var phone: String?
    var address: String?
    var noOfFloor: String?
    var bedRoom: String?
    var bathRoom: String?
    var area: String?
    var type: String?
    var imageUrl:String?
    var city:String?
    var country:String?
    var zipCode:String?
    var firstName:String?
    var lastName:String?
    var middleName:String?
    var flowType:String?
    var state: State?
    var latitude: Double?
    var longitude: Double?
    var imageManual: UIImage?
   // var isGoogleImage: Bool?
    var lotSizeSqFt: String?
    var visitStaus: VisitStatus?
}
struct PropertyDetails {
    //var newPropertyInfo: PropertyInfo?
    //var mapDetails: MapLocationResult?
    //var locationDetails: PlaceComponets?
    var propertyInfo: PropertyInfo?
    var lead: Lead?
    var opportunity: Opportunity?
    var account: Account?
    var propertyType: PropertyType = .empty
    var galleryImages: [DTDLeadImagesDetailExtSerDc] = []

}


enum PropertyType: String, Encodable {
    case lead
    case opportunity
    case account
    case empty
}
