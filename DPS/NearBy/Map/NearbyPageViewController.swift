//
//  NearbyPageViewController.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol NearByTabsDelegate {
    func tabChanged(_ index:Int)
}

final class NearbyPageViewController: UIPageViewController {
    // MARK:- Variable
    private var pageViewControllers:[UIViewController] = []
    var delegatePage:NearByTabsDelegate?
    var index = 0
    weak var delegateLocation: NearbyMapDelegate?
    var nearbyFilterData = NearbyFilterData()
    
    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        self.view.subviews.compactMap({ $0 as? UIScrollView }).first?.delaysContentTouches = false
        self.view.subviews.compactMap({ $0 as? UIScrollView }).first?.canCancelContentTouches = false
        
        let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
        let page = storyboard.instantiateViewController(withIdentifier: "NearbyMapViewController") as! NearbyMapViewController
        page.delegate = self
        page.nearbyFilterData = nearbyFilterData
        pageViewControllers.append(page)
        
        let page1 =  UIStoryboard.init(name: "MapD2D", bundle: nil).instantiateViewController(withIdentifier: "NearByAppleMapViewController") as! NearByAppleMapViewController
        page1.delegate = self
        page1.nearbyFilterData = nearbyFilterData
        pageViewControllers.append(page1)
        
        let page2 = storyboard.instantiateViewController(withIdentifier: "NearByListViewController") as! NearByListViewController
        // page2.delegate = self
        pageViewControllers.append(page2)
        
        guard let first  = pageViewControllers.first else{
            return
        }
        setViewControllers([first], direction: .forward, animated: false, completion: nil)
    }
}


// MARK: - UIPageViewControllerDataSource
extension NearbyPageViewController:UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pageViewControllers.firstIndex(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0  else { return nil }
        
        guard pageViewControllers.count > previousIndex else { return nil }
        
        return pageViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pageViewControllers.firstIndex(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pageViewControllers.count else { return nil }
        
        guard pageViewControllers.count > nextIndex else { return nil }
        
        return pageViewControllers[nextIndex]
    }
}
// MARK: - UIPageViewControllerDelegate
extension NearbyPageViewController:UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        if let _ = pendingViewControllers.first as? NearbyMapViewController {
            //tab = source.tab
            index = 0
        } else if  let _ = pendingViewControllers.first as? NearByAppleMapViewController {
            //tab = source.tab
            index = 1
        } else if  let _ = pendingViewControllers.first as? NearByListViewController {
            index = 2
        }
        
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else{
            return
        }
        delegatePage?.tabChanged(index)
    }
}

// MARK:- Setting Protocol
extension NearbyPageViewController: NearbyDelegate {
    
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        if let source = pageViewControllers[0] as? NearbyMapViewController{
            source.gotSelectedLocation(locationDetails)
        }
        if let source = pageViewControllers[1] as? NearByAppleMapViewController {
            source.gotSelectedLocation(locationDetails)
        }
    }
    func changeMapLoction(location: CLLocation, address:String) {
        if let source = pageViewControllers[0] as? NearbyMapViewController{
            source.changeMapLocation(location, address: address)
        }
        if let source = pageViewControllers[1] as? NearByAppleMapViewController {
            source.changeMapLocation(location, address: address)
        }
    }
    func gotEntitiesResponse(leads: [Lead], opportunities: [Opportunity], accounts: [Account], isLocationChange: Bool) {
        if let source = pageViewControllers[0] as? NearbyMapViewController{
            source.isLocationChange = isLocationChange
            source.accounts = accounts
            source.opportunities = opportunities
            source.leads = leads
        }
        if let source = pageViewControllers[1] as? NearByAppleMapViewController{
            source.accounts = accounts
            source.opportunities = opportunities
            source.leads = leads
        }
        if let source = pageViewControllers[2] as? NearByListViewController{
            source.accounts = accounts
            source.opportunities = opportunities
            source.leads = leads
            source.setData()
        }
    }
    func gotShapesData(_ output: [GeoFencingD2D]?) {
        if let source = pageViewControllers[0] as? NearbyMapViewController{
            source.shapesData = output
        }
        if let source = pageViewControllers[1] as? NearByAppleMapViewController{
            source.shapesData = output
        }
    }
    func tabChanged(_ selectedTab: Int) {
        if index < selectedTab {
            setViewControllers([pageViewControllers[selectedTab]], direction: .forward, animated: true, completion: nil)
        } else{
            setViewControllers([pageViewControllers[selectedTab]], direction: .reverse, animated: true, completion: nil)
        }
        index = selectedTab
    }
    func updateNearByFilter(_ nearbyFilterData: NearbyFilterData) {
        self.nearbyFilterData = nearbyFilterData
        if let source = pageViewControllers[0] as? NearbyMapViewController {
            source.nearbyFilterData = nearbyFilterData
        }
        if let source = pageViewControllers[1] as? NearByAppleMapViewController {
            source.nearbyFilterData = nearbyFilterData
        }
    }
    func searchLocation(_ selectedTab: Int, searchText: String?, searchedArea searchedArae: GeoFencingD2D?) {
        if let source = pageViewControllers[selectedTab] as? NearbyMapViewController {
            source.actionSearch()
        } else  if let source = pageViewControllers[selectedTab] as? NearByAppleMapViewController {
            source.actionSearch()
        } else  if let source = pageViewControllers[selectedTab] as? NearByListViewController {
            source.searchInList(searchText ?? "")
        }
    }
}
// MARK: - NearbyMapDelegate
extension NearbyPageViewController: NearbyMapDelegate {
    func gotCurrentLocation(latitude: Double, longitude: Double) {
        delegateLocation?.gotCurrentLocation(latitude: latitude, longitude: longitude)
    }
}
