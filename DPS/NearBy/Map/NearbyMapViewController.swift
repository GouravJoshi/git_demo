//
//  NearbyMapViewController.swift
//  DPS
//
//  Created by Vivek Patel on 15/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire


class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var lead: Lead?
    var opportunity: Opportunity?
    var account: Account?
    var liveLocation: LiveLocationD2D?
    
    init(position: CLLocationCoordinate2D, lead: Lead? = nil, opportunity: Opportunity? = nil, account: Account? = nil, liveLocation: LiveLocationD2D? = nil) {
        self.position = position
        self.lead = lead
        self.opportunity = opportunity
        self.account = account
        self.liveLocation = liveLocation
    }
}

protocol NearbyMapDelegate: NSObjectProtocol {
    func gotCurrentLocation(latitude: Double, longitude: Double)
}
final class NearbyMapViewController: BaseViewController {
    
    // MARK:- Variables
    @IBOutlet weak var btnLayer: UIButton!
    @IBOutlet weak var btnNavigator: UIButton!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var lblAccoutsCount: UILabel!
    @IBOutlet weak var lblOpportunityCount: UILabel!
    @IBOutlet weak var lblLeadsCount: UILabel!
    // MARK: - Variable
    private var mapView: GMSMapView!
    private let locationService = LocationService.instance
    private var kCameraLatitude = 40.692167
    private var kCameraLongitude = -102.664972
    private var placesClient: GMSPlacesClient!
    private var clusterManager: GMUClusterManager?
    private var placeDetail: PlaceComponets?
    private var mapDetail: MapResult?
    private let dispatchGroup = DispatchGroup()
    private lazy var states: [State] = []
    var nearbyFilterData = NearbyFilterData()
    private lazy var presenter = {
        return MapPresenter(self)
    }()
    var shapesData: [GeoFencingD2D]?
    var leads: [Lead] = [] {
        didSet{
            generateClusterFromLeads(leads)
        }
    }
    var opportunities: [Opportunity] = [] {
        didSet{
            generateClusterFromOpportunities(opportunities)
        }
    }
    var accounts: [Account] = [] {
        didSet{
            generateClusterFromAccounts(accounts)
        }
    }
    weak var delegate: NearbyMapDelegate?
    var bounds: GMSCoordinateBounds?
    lazy var items: [POIItem] = []
    var isLocationChange = false
    var isFromFilter = false
    
    // MARK: - Outlets
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var viewMap: UIView!
    weak var nearbyDelegate: NearbyDelegate?

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        getSavedLatitudeLongitude()
        setUI()
        states = AppUtility.fetchStates() ?? []
        setGoogleMap()
        apiCallForAssingedAreaInitial()
        
        NotificationCenter.default.addObserver(
                    self,
                    selector: #selector(self.refreshFromFilter),
                    name: NSNotification.Name(rawValue: "FilerNotify"),
                    object: nil)
        
        nearbyFilterData.isType = "Current"

    }

    
    @objc private func refreshFromFilter(notification: NSNotification){
            print("YYYYYYeeeeeeSS")
            self.isFromFilter = true
            apiCallForAssingedAreaInitial()
        }
    
    
    // MARK: - Covinience
    private func apiCallForAssingedAreaInitial() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        presenter.apiCallToGetAssignedAreaForUser(employeeID,companyKey: companyKey, isLoaderShow: true)
    }
    
    private func drawShapeForSingleUser(_ shape: MapGeoFencingD2D) {
        
        let dictAssigned = NSMutableDictionary()
        dictAssigned.setValue(false, forKey: "Assigned")
        
        if shape.modeType == "polygon" {
            let path = GMSMutablePath()
            var polygonCoordinates: [CLLocationCoordinate2D] = []
            if let polygonLatLngArray = shape.polygonLatLngArray {
                for cordiantes in polygonLatLngArray {
                    if let lat =  cordiantes.lat ,  let lng = cordiantes.lng {
                        path.add(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                        polygonCoordinates.append(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                    }
                }
                for cordiantes in polygonLatLngArray {
                    if let lat =  cordiantes.lat ,  let lng = cordiantes.lng {
                        path.add(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                        polygonCoordinates.append(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                        break
                    }
                }
                
                let polygon = GMSPolyline(path: path)
                //polygon.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
                polygon.strokeColor = .white
                polygon.strokeWidth = 3
                polygon.map = mapView
                polygon.isTappable = true
                polygon.userData = shape
                
                if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                    polygon.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
                }
//                if Int(self.mapView.camera.zoom) > (shape.mapZoom ?? 18 - 2) {
                    let polygonCenter = getCenterCoordOfPolygon(polygonCoordinates)
                dictAssigned.setValue("\(polygonCenter.latitude)", forKey: "latitude")
                dictAssigned.setValue("\(polygonCenter.longitude)", forKey: "longitude")
                dictAssigned.setValue(true, forKey: "Assigned")

                let annotation = GMSMarker()
                    let position = CLLocationCoordinate2D(latitude: (polygonCenter.latitude), longitude: polygonCenter.longitude)
                    annotation.position = position
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                    // label.center = CGPoint(x: 160, y: 285)
                    label.textAlignment = .center
                    label.textColor = .white
                    label.text = shape.area
                    //annotation.iconView = label
                    annotation.map = mapView
//                }
            }
        } else if  shape.modeType == "circle" {
            if let lat =  shape.circleCenter?.lat, let lng = shape.circleCenter?.lat, let radiusString = shape.circleRadius, let radius = Double(radiusString)  {
                let circleCenter = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                
                let circle = GMSCircle(position: circleCenter, radius: radius)
                circle.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
                circle.strokeColor = .white
                circle.strokeWidth = 3
                circle.map = mapView
                circle.userData = shape
                circle.isTappable = true
                if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                    circle.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
                }
               
                dictAssigned.setValue("\(lat)", forKey: "latitude")
                dictAssigned.setValue("\(lng)", forKey: "longitude")
                dictAssigned.setValue(true, forKey: "Assigned")

                let annotation = GMSMarker()
                let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                annotation.position = position
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                label.textAlignment = .center
                label.textColor = .white
                label.text = shape.area
                //annotation.iconView = label
                annotation.map = mapView
                
            }
        }
        else if  shape.modeType == "rectangle" {
            let path = GMSMutablePath()
            
            guard let rectangleSouthWest = shape.rectangleSouthWest, let rectangleNorthEast = shape.rectangleNorthEast, let latSouth =  rectangleSouthWest.lat, let lngSouth = rectangleSouthWest.lng, let latNorth =  rectangleNorthEast.lat, let lngNorth = rectangleNorthEast.lng  else { return }

            var polygonCoordinates: [CLLocationCoordinate2D] = []
            
            path.add(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
            polygonCoordinates.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
            
            path.add(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
            polygonCoordinates.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
            
            path.add(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
            polygonCoordinates.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
            
            path.add(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
            polygonCoordinates.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
            
            path.add(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
            polygonCoordinates.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
            
//            path.add(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
//            polygonCoordinates.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
            
            let polygon = GMSPolyline(path: path)
            //polygon.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
            polygon.strokeColor = .white
            polygon.strokeWidth = 3
            polygon.map = mapView
            polygon.isTappable = true
            polygon.userData = shape
            
            if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                polygon.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
            }
            let polygonCenter = getCenterCoordOfPolygon(polygonCoordinates)
           
            dictAssigned.setValue("\(polygonCenter.latitude)", forKey: "latitude")
            dictAssigned.setValue("\(polygonCenter.longitude)", forKey: "longitude")
            dictAssigned.setValue(true, forKey: "Assigned")

            let annotation = GMSMarker()
            let position = CLLocationCoordinate2D(latitude: (polygonCenter.latitude), longitude: polygonCenter.longitude)
            annotation.position = position
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            // label.center = CGPoint(x: 160, y: 285)
            label.textAlignment = .center
            label.textColor = .white
            label.text = shape.area
            //annotation.iconView = label
            annotation.map = mapView
            
        }
        nsud.setValue(dictAssigned, forKey: "FilterLocationAssigned")
        
        if self.isFromFilter != true {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) { [self] in
                
                if shape.mapCenterLatLng?.lat != nil {
                    
                    if nearbyFilterData.isType == "Assigned" {
                        
                        self.delegate?.gotCurrentLocation(latitude: Double(shape.mapCenterLatLng?.lat ?? self.kCameraLatitude), longitude: Double(shape.mapCenterLatLng?.lng ?? self.kCameraLongitude))

                    }else if nearbyFilterData.isType == "Current" {
                        
                        getCurrentLocation()
                        
                    }
                    
                }else {
                    
                    getCurrentLocation()
                    
                }
                
            }
            
        }else {
            
            self.isFromFilter = false
            
        }
        
    }
    
    private func drawShape(_ shapes: [GeoFencingD2D]?) {
        guard let shapes = shapes else {
            return
        }
        for shape in shapes {
            if shape.modeType == "polygon" {
                let path = GMSMutablePath()
                var polygonCoordinates: [CLLocationCoordinate2D] = []
                if let polygonLatLngArray = shape.polygonLatLngArray {
                    for cordiantes in polygonLatLngArray {
                        if let lat =  cordiantes.lat ,  let lng = cordiantes.lng {
                            path.add(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                            polygonCoordinates.append(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                        }
                    }
                    let polygon = GMSPolygon(path: path)
                    polygon.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
                    polygon.strokeColor = .white
                    polygon.strokeWidth = 3
                    polygon.map = mapView
                    polygon.isTappable = true
                    polygon.userData = shape
                    
                    if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                        polygon.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
                    }
                    if Int(self.mapView.camera.zoom) > (shape.mapZoom ?? 18 - 2) {
                        let polygonCenter = getCenterCoordOfPolygon(polygonCoordinates)
                        
                        let annotation = GMSMarker()
                        let position = CLLocationCoordinate2D(latitude: (polygonCenter.latitude), longitude: polygonCenter.longitude)
                        annotation.position = position
                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                        // label.center = CGPoint(x: 160, y: 285)
                        label.textAlignment = .center
                        label.textColor = .white
                        label.text = shape.area
                        annotation.iconView = label
                        annotation.map = mapView
                    }
                }
            } else if  shape.modeType == "circle" {
                if let lat =  shape.circleCenter?.lat, let lng = shape.circleCenter?.lat, let radiusString = shape.circleRadius, let radius = Double(radiusString)  {
                    let circleCenter = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    
                    let circle = GMSCircle(position: circleCenter, radius: radius)
                    circle.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
                    circle.strokeColor = .white
                    circle.strokeWidth = 3
                    circle.map = mapView
                    circle.userData = shape
                    circle.isTappable = true
                    if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                        circle.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
                    }
                    let annotation = GMSMarker()
                    let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    annotation.position = position
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                    label.textAlignment = .center
                    label.textColor = .white
                    label.text = shape.area
                    annotation.iconView = label
                    annotation.map = mapView
                    
                }
            } else if  shape.modeType == "rectangle" {
                let rectanglePath = GMSMutablePath()
                guard let rectangleSouthWest = shape.rectangleSouthWest, let rectangleNorthEast = shape.rectangleNorthEast, let latSouth =  rectangleSouthWest.lat, let lngSouth = rectangleSouthWest.lng, let latNorth =  rectangleNorthEast.lat, let lngNorth = rectangleNorthEast.lng  else { return }
                rectanglePath.add(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
                rectanglePath.add(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
                rectanglePath.add(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
                rectanglePath.add(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
                var rectangleCoordinates: [CLLocationCoordinate2D] = []
                rectangleCoordinates.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
                rectangleCoordinates.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
                rectangleCoordinates.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
                rectangleCoordinates.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
                let rectangle = GMSPolygon(path: rectanglePath)
                if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                    rectangle.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
                }
                rectangle.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
                rectangle.strokeColor = .white
                rectangle.strokeWidth = 3
                rectangle.map = mapView
                rectangle.userData = shape
                rectangle.isTappable = true
                let polygonCenter = getCenterCoordOfPolygon(rectangleCoordinates)
                let annotation = GMSMarker()
                let position = CLLocationCoordinate2D(latitude: (polygonCenter.latitude), longitude: polygonCenter.longitude)
                annotation.position = position
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                label.textAlignment = .center
                label.textColor = .white
                label.text = shape.area
                annotation.iconView = label
                annotation.map = mapView
            }
        }
    }
    // MARK: - Action
    @IBAction func actionSetting(_ sender: Any) {
        mapView.mapType = .satellite
    }
    
    @IBAction func actionLayer(_ sender: Any) {
        mapView.mapType = .hybrid
    }
    
    @IBAction func actioNavigation(_ sender: Any) {
        mapView.mapType = .terrain
    }
    @IBAction func actionSearch(_ sender: Any) {
        
        let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
        guard  let controller = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as? SearchPlaceViewController else { return }
        //controller.modalPresentationStyle = .
        controller.delegate = self
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    func  actionSearch() {
        let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as? SearchPlaceViewController  else { return }
        //controller.modalPresentationStyle = .
        controller.delegate = self
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
}
// MARK: - Convenience
extension NearbyMapViewController {
    private func setUI() {
        btnSearch.layer.cornerRadius = 0
        AppUtility.buttonImageColor(btn: btnSetting, image: "Group 772", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnNavigator, image: "Group 765", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnLayer, image: "Group 768", color: .appDarkGreen)
    }
    private func getSavedLatitudeLongitude() {
        kCameraLatitude = nearbyFilterData.userLocation.latitude ?? ConstantV.latitude
        kCameraLongitude = nearbyFilterData.userLocation.longitude ?? ConstantV.longitude
    }
    
    //MARK: Melissadata API Calling
     func apiCallMelissadataForPlaceDetail(_ propertyType: PropertyType, lead: Lead? = nil, opportunity: Opportunity? = nil, account: Account? = nil, location: CLLocation? = nil, state: State? = nil, placeMark: CLPlacemark? = nil, propertyInfo: PropertyInfo? = nil)  {
        
        var  propertyInfoNew = PropertyInfo()
        propertyInfoNew = propertyInfo!
       let dictRecord = NSMutableDictionary()
        dictRecord.setValue(propertyInfo?.address, forKey: "AddressLine1")
        dictRecord.setValue(propertyInfo?.city, forKey: "City")
        dictRecord.setValue(propertyInfo?.state!.stateShortName, forKey: "State")
        dictRecord.setValue(propertyInfo?.zipCode, forKey: "PostalCode")

//        dictRecord.setValue("18715 Rogers Lk", forKey: "AddressLine1")
//            dictRecord.setValue("San Antonio", forKey: "City")
//            dictRecord.setValue("TX", forKey: "State")
//            dictRecord.setValue("70258", forKey: "PostalCode")
        
        let aryRecord = NSMutableArray()
        aryRecord.add(dictRecord)
        let dictData = NSMutableDictionary()

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let apiKeyMellissa = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.MelissaKey")!)"

        dictData.setValue(apiKeyMellissa.count == 0 ? MelissaKey : apiKeyMellissa, forKey: "CustomerId")
        dictData.setValue("Property V4 - LookupProperty", forKey: "TransmissionReference")
        dictData.setValue("1", forKey: "TotalRecords")
        dictData.setValue("GrpAll", forKey: "Columns")
        dictData.setValue(aryRecord, forKey: "Records")
        var jsonString = String()

        if(JSONSerialization.isValidJSONObject(dictData) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictData, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            print(dictData)
        Global().getServerResponseForSendLead(toServer: UrlMelissa, requestData, (NSDictionary() as! [AnyHashable : Any]), "", "") { (success, response, error) in
            self.hideLoader()

                if(success)
                {
                    if((response as NSDictionary?)?.value(forKey: "Records") is NSArray){
                        
                        var aryRecord = NSMutableArray()
                        
                        aryRecord = ((response as NSDictionary?)?.value(forKey: "Records") as! NSArray).mutableCopy()as! NSMutableArray
                        
                        if(aryRecord.count != 0){
                            
                            if((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") is NSDictionary){
                                
                                var dictRecord = NSMutableDictionary()
                                
                                dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                let dictIntRoomInfo =  removeNullFromDict(dict: dictRecord)
                                
                                propertyInfoNew.bathRoom = "\(dictIntRoomInfo.value(forKey: "BathCount")!)"
                                
                                propertyInfoNew.bedRoom = "\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)"
                                
                                propertyInfoNew.noOfFloor = "\(dictIntRoomInfo.value(forKey: "StoriesCount")!)"
                                
                                propertyInfoNew.flowType = "Residential"
                                
                                var typeInfo = "Residential"
                                
                                if let noofBedroom = Int("\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)")?.description , !"\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)".isEmpty , let noofBathroom =   Int("\(dictIntRoomInfo.value(forKey: "BathCount")!)")?.description  , !"\(dictIntRoomInfo.value(forKey: "BathCount")!)".isEmpty {
                                    
                                    typeInfo.append(" \(noofBedroom) Bed /" )
                                    
                                    typeInfo.append(" \(noofBathroom) Bath." )
                                    
                                } else {
                                    
                                    if let noofBedroom = Int("\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)")?.description , !noofBedroom.isEmpty {
                                        
                                        typeInfo.append(" \(noofBedroom) Bed")
                                        
                                    }
                                    
                                    if let noofBathroom =  Int("\(dictIntRoomInfo.value(forKey: "BathCount")!)")?.description  , !noofBathroom.isEmpty {
                                        
                                        typeInfo.append(" \(noofBathroom) Bath.")
                                        
                                    }
                                    
                                }
                                
                                propertyInfoNew.type = typeInfo
                                
                            }else{
                                
                                propertyInfoNew.bathRoom = ""
                                
                                propertyInfoNew.bedRoom = ""
                                
                                propertyInfoNew.noOfFloor = ""
                                
                                propertyInfoNew.type = ""
                                
                                propertyInfoNew.flowType = ""
                                
                            }
                            
                            if((aryRecord[0] as AnyObject).value(forKey: "PropertySize") is NSDictionary){
                                
                                var dictRecord = NSMutableDictionary()
                                
                                dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PropertySize") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                
                                let dictPropertySize =  removeNullFromDict(dict: dictRecord)
                                
                                propertyInfoNew.lotSizeSqFt = "\(dictPropertySize.value(forKey: "AreaBuilding")!)"
                                
                                propertyInfoNew.area = "\(dictPropertySize.value(forKey: "AreaBuilding")!)"
                                
                            }else{
                                
                                propertyInfoNew.lotSizeSqFt = ""
                                
                                propertyInfoNew.area = ""
                                
                            }
                            
                            if((aryRecord[0] as AnyObject).value(forKey: "PrimaryOwner") is NSDictionary){
                                
                                var dictRecord = NSMutableDictionary()
                                
                                dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PrimaryOwner") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                
                                
                                let dictPropertySize =  removeNullFromDict(dict: dictRecord)
                                
                                propertyInfoNew.firstName = "\(dictPropertySize.value(forKey: "Name1First")!)"
                                
                                propertyInfoNew.middleName = "\(dictPropertySize.value(forKey: "Name1Middle")!)"
                                
                                propertyInfoNew.lastName = "\(dictPropertySize.value(forKey: "Name1Last")!)"
                                
                                propertyInfoNew.name = "\(dictPropertySize.value(forKey: "Name1Full")!)"
                                
                                
                                
                            }else{
                                
                                propertyInfoNew.firstName = ""
                                
                                propertyInfoNew.middleName = ""
                                
                                propertyInfoNew.lastName = ""
                                
                                propertyInfoNew.name = ""
                                
                            }
                            
                        }
                        
                    }
                    
                }
                else
                {
                    
                }
            self.openVisitDeatils(.empty, location: location, state: state, placeMark: placeMark, propertyInfo: propertyInfoNew)

        }

        /*guard let urlString =  "http://www.zillow.com/webservice/getUpdatedPropertyDetails?zws-id=X1-ZWz19zkb2lkumj_7zykg&address=\(address)&citystatezip=\(cityStateZip)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
         self.delegateMap?.alertInZilliow("Unknown Error")
         return
         }*/
//        guard let urlString =  "http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=X1-ZWz19zkb2lkumj_7zykg&address=\(address)&citystatezip=\(cityStateZip)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else { return }
//
//        service.getPlaceDetailUsingZilliow(url) { (mapResult, error) in
//            if let mapResult = mapResult {
//                self.delegateMap?.hideLoader()
//
    //            self.delegateMap?.gotPlaceDetailByZilliow(output: mapResult)
//            } else {
//                self.delegateMap?.hideLoader()
//                self.delegateMap?.alertInZilliow(error ?? "Unknown Error")
//            }
//        }
    }
}
// MARK: - Map Convenience
extension NearbyMapViewController {
    private func setGoogleMap() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
        let apiKey = "\(key)"
        GMSServices.provideAPIKey(apiKey)
        let camera = GMSCameraPosition.camera(withLatitude: kCameraLatitude,
                                              longitude: kCameraLongitude, zoom: 18)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView?.delegate = self
        self.mapView?.mapType = .hybrid
        
        mapView?.isMyLocationEnabled = true
        self.viewMap.addSubview(mapView ?? UIView())
        mapView?.translatesAutoresizingMaskIntoConstraints = false
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .right, .left]
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: mapView as Any, attribute: $0, relatedBy: .equal, toItem: mapView?.superview, attribute: $0, multiplier: 1, constant: 0)
        })
        setUpCluster()
        //getCurrentLocation()
        
    }
    private func getCurrentLocation() {
        locationService.getLocation { location in
            guard let location = location else {
                self.delegate?.gotCurrentLocation(latitude: self.kCameraLatitude, longitude: self.kCameraLongitude)
                return
            }
            self.kCameraLatitude = location.coordinate.latitude
            self.kCameraLongitude = location.coordinate.longitude
            self.nearbyFilterData.userLocation.latitude = location.coordinate.latitude
            self.nearbyFilterData.userLocation.longitude = location.coordinate.longitude
            self.delegate?.gotCurrentLocation(latitude: self.kCameraLatitude, longitude: self.kCameraLongitude)
            self.mapView?.camera = GMSCameraPosition(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18)
        }
    }
    private func setMapMarker(_ locationDetails: LocationResult) {
        let marker = GMSMarker()
        self.mapView?.camera = GMSCameraPosition(latitude: locationDetails.geometry.location.latitude, longitude: locationDetails.geometry.location.longitude, zoom: 18)
        marker.position = CLLocationCoordinate2D(latitude: locationDetails.geometry.location.latitude, longitude:  locationDetails.geometry.location.longitude)
        // mapView?.mapType = .terrain
        marker.title = locationDetails.formattedAddress
        marker.snippet = locationDetails.formattedAddress
        let image =  UIImage(named: "Group 758")
        marker.icon = image?.maskWithColor(color: .systemRed)
        marker.map = self.mapView
    }
  
    private func showAlertForBranchChange(strAppoinntmentBranch : String, strEmpLoginBranchSysName : String) {
        
        var loggedBranchNameLocal = ""
        var appointmentBranchNameLocal = ""

        var arrBranch = NSMutableArray()
        arrBranch = getBranches()
        
        for item in arrBranch
        {
            
            let dict = item as! NSDictionary
            
            if strEmpLoginBranchSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                loggedBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
            if strAppoinntmentBranch == "\(dict.value(forKey: "SysName") ?? "")"
            {
                appointmentBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
        }
        
        let strMsssggg = "You are currently in " + "\(loggedBranchNameLocal)" + " branch. Please switch branch to work on the " + "\(appointmentBranchNameLocal)" + " branch."
        
        //You are currently in Lodi branch. Please switch branch to work on the Fresno branch.
        
        let alert = UIAlertController(title: alertMessage, message: strMsssggg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Switch Branch", style: .destructive, handler: { (nil) in
            
            self.goToSwitchBranch()
            
        }))
        alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
            
            // Do Nothing On This.
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getBranches() -> NSMutableArray {
        let temparyBranch = NSMutableArray()

        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                temparyBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
        return temparyBranch
    }
    
    func goToSwitchBranch() {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "DashBoard" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SwitchBranchVC") as! SwitchBranchVC
         
        vc.reDirectWhere = "NearByMap"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    private func openVisitDeatils(_ propertyType: PropertyType, lead: Lead? = nil, opportunity: Opportunity? = nil, account: Account? = nil, location: CLLocation? = nil, state: State? = nil, placeMark: CLPlacemark? = nil, propertyInfo: PropertyInfo? = nil)  {
        
        // Check if Logged In Branch is different
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpLoginBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"

        var proceedFurther = true
        
        if lead != nil {
            
            let arrOfBranchesSysName = lead?.branchesSysNames
            
            proceedFurther = false
            
            if arrOfBranchesSysName?.count ?? 0 > 0 {
                
                for k in 0 ..< arrOfBranchesSysName!.count {

                    let strBrancchL = "\(arrOfBranchesSysName![k])"
                    
                    if strBrancchL == strEmpLoginBranchSysName {
                        
                        proceedFurther = true
                        break
                        
                    }
                    
                }
                
            }
            

            if !proceedFurther {
                
                self.showAlertForBranchChange(strAppoinntmentBranch: "\(opportunity?.branchSysName ?? "")", strEmpLoginBranchSysName: strEmpLoginBranchSysName)

            }
            
        }else if opportunity != nil {
            
            if opportunity?.branchSysName != strEmpLoginBranchSysName {
                
                proceedFurther = false
                // Show Alert of Branch
                                
                self.showAlertForBranchChange(strAppoinntmentBranch: "\(opportunity?.branchSysName ?? "")", strEmpLoginBranchSysName: strEmpLoginBranchSysName)
                
            }
            
        }
        
        if proceedFurther {
            
            let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
            guard let controller = storyboard.instantiateViewController(withIdentifier: "EntityDetailViewController") as? EntityDetailViewController else { return }
            controller.modalPresentationStyle = .overFullScreen
          
            var propertyDetails = PropertyDetails()
            //  propertyDetails.state = state
            propertyDetails.propertyType = propertyType
            propertyDetails.lead = lead
            propertyDetails.opportunity = opportunity
            propertyDetails.account = account
            propertyDetails.propertyInfo = propertyInfo

            // change in Logic according to Settings config. for showing Property Info.

            if(nsud.value(forKey: "isEnableAutoPropertyInformation") != nil){
                
                if(nsud.value(forKey: "isEnableAutoPropertyInformation") as! Bool){

                    
                }else{
                    
                    //
                    if propertyDetails.propertyInfo == nil {
                        var  propertyInfoNew = PropertyInfo()
                        propertyInfoNew.name = "Unknown"
                        propertyInfoNew.firstName = "Unknown"
                        propertyDetails.propertyInfo = propertyInfoNew

                    }
                    
                }
                
            }else{
                
                //
                if propertyDetails.propertyInfo == nil {
                    var  propertyInfoNew = PropertyInfo()
                    propertyInfoNew.name = "Unknown"
                    propertyInfoNew.firstName = "Unknown"
                    propertyDetails.propertyInfo = propertyInfoNew

                }
                
            }
            

            
            // propertyDetails.latitude = location?.coordinate.latitude
            //  propertyDetails.longitude = location?.coordinate.longitude
            // propertyDetails.locationDetails = placeDetail
            // propertyDetails.placeMark = placeMark
            controller.propertyDetails = propertyDetails
            placeDetail = nil
            mapDetail = nil
            controller.delegate = self
            controller.selectedLocation = location
            let navController = UINavigationController.init(rootViewController: controller)
            navController.isNavigationBarHidden = true
            navController.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(navController, animated: true, completion: nil)
            
            
        }
        
    }
    func changeMapLocation(_ location: CLLocation, address:String) {
        
        self.mapView.clear()
        
        let marker = GMSMarker()
        self.mapView?.camera = GMSCameraPosition(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18)
        marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        marker.title = address
        marker.snippet = address
        let image =  UIImage(named: "Group 758")
        marker.icon = image?.maskWithColor(color: .systemRed)
        marker.map = self.mapView
        mapView.animate(to: GMSCameraPosition(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18))
    }
}
// MARK: - Convenience Cluster
extension NearbyMapViewController {
    
    private func setUpCluster() {
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        clusterManager?.setDelegate(self, mapDelegate: self)
        clusterManager?.cluster()
    }
    private func generateClusterFromLeads(_ leads: [Lead]) {
        
        lblLeadsCount.text = leads.count.description
        //   let extent = 0.2
        if bounds == nil {
            bounds = GMSCoordinateBounds()
        }
        var leadCount = 0
        for lead in leads {
            let lat =  lead.latitude ?? 0.0
            let lng = lead.longitude ?? 0.0
            if opportunities.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil &&   accounts.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil {
                if lat > -90.0 && lat < 90.0 , lng > -180.0 && lng < 180.0 {
                    // lat =  lat +  extent * randomScale()
                    // lng = lng + extent * randomScale()
                    let item =  POIItem(position: CLLocationCoordinate2DMake(lat, lng), lead: lead)
                    bounds = bounds?.includingCoordinate(item.position)
                    //clusterManager?.add(item)
                    leadCount += 1
                    items.append(item)
                }
            }
        }
        lblLeadsCount.text = leadCount.description
        self.clusterManager?.clearItems()
        if !isLocationChange {
//            self.mapView.clear()
//            drawShape(shapesData)
        }
        self.clusterManager?.add(self.items)
    }
    private func generateClusterFromOpportunities(_ opportunities: [Opportunity]) {
        
        lblOpportunityCount.text = opportunities.count.description
        //let extent = 0.2
        if bounds == nil {
            bounds = GMSCoordinateBounds()
        }
        var oppCount = 0
        for object in opportunities {
            let lat =  object.latitude ?? 0.0
            let lng = object.longitude ?? 0.0
            if accounts.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil {
                // lat =  lat + extent * randomScale()
                // lng = lng + extent * randomScale()
                if lat > -90.0 && lat < 90.0 , lng > -180.0 && lng < 180.0{
                    let item =  POIItem(position: CLLocationCoordinate2DMake(lat, lng), opportunity: object)
                    bounds = bounds?.includingCoordinate(item.position)
                    //clusterManager?.add(item)
                    oppCount += 1
                    items.append(item)
                }
            }
        }
        lblOpportunityCount.text = oppCount.description
    }
    private func generateClusterFromAccounts(_ accounts: [Account]) {
        self.items.removeAll()
        lblAccoutsCount.text = accounts.count.description
        //  let extent = 0.2
        if bounds == nil {
            bounds = GMSCoordinateBounds()
        }
        var accCount = 0
        for object in accounts {
            let lat =  object.latitude ?? 0.0
            let lng = object.longitude ?? 0.0
            if lat > -90.0 && lat < 90.0 , lng > -180.0 && lng < 180.0{
                let item =  POIItem(position: CLLocationCoordinate2DMake(lat, lng), account: object)
                bounds = bounds?.includingCoordinate(item.position)
                //clusterManager?.add(item)
                accCount += 1
                items.append(item)
            }
        }
        lblAccoutsCount.text = accCount.description
        // guard let bounds = self.bounds else { return }
        // self.mapView.animate(with: .fit(bounds, withPadding: 100.0))
    }
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
}
// MARK: - GMUClusterManagerDelegate & GMSMapViewDelegate
extension NearbyMapViewController: GMSMapViewDelegate, GMUClusterManagerDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            
            if let lead = poiItem.lead {
                
                let locationL = CLLocation(latitude: lead.latitude!, longitude: lead.longitude!)

                openVisitDeatils(.lead, lead: lead, location: locationL)
                
            } else if let opportunity = poiItem.opportunity {
                
                let locationL = CLLocation(latitude: opportunity.latitude!, longitude: opportunity.longitude!)
                
                openVisitDeatils(.opportunity, opportunity: opportunity, location: locationL)
                
            } else if let account = poiItem.account {
                
                let locationL = CLLocation(latitude: account.latitude!, longitude: account.longitude!)

                openVisitDeatils(.account, account: account, location: locationL)
            }
        } else if let cluster = marker.userData as? GMUStaticCluster {
            let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                     zoom: mapView.camera.zoom + 1)
            let update = GMSCameraUpdate.setCamera(newCamera)
            mapView.moveCamera(update)
        } else {
            let location = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
            //presenter.apiCallForAddress(location)
            
            // change in Logic according to Settings config. for showing Property Info.
            
            if(nsud.value(forKey: "isEnableAutoPropertyInformation") != nil){
                if(nsud.value(forKey: "isEnableAutoPropertyInformation") as! Bool){
                    
                    //
                    presenter.apiCallForAddress(location)

                }else{
                    
                    self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)
                    
                }
            }else{
                
                self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)

            }
            
        }
        return false
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        //presenter.apiCallForAddress(location)
        
        // change in Logic according to Settings config. for showing Property Info.

        if(nsud.value(forKey: "isEnableAutoPropertyInformation") != nil){
            if(nsud.value(forKey: "isEnableAutoPropertyInformation") as! Bool){
                
                //
                presenter.apiCallForAddress(location)

            }else{
                
                self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)
                
            }
        }else{
            
            self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)

        }
        
    }
}
// MARK: - UpdateMapDelegate
extension NearbyMapViewController: UpdateMapDelegate {
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        self.nearbyFilterData.userLocation.latitude = locationDetails.geometry.location.latitude
        self.nearbyFilterData.userLocation.longitude = locationDetails.geometry.location.longitude
        self.nearbyFilterData.userLocation.address = locationDetails.formattedAddress
        setMapMarker(locationDetails)
    }
}
// MARK: - Visit action delegate
extension NearbyMapViewController: VisitActionDelegate {
    
    func actionGetQuote(_ propertyDetails: PropertyDetails) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "VisitServiceDetailViewController") as? VisitServiceDetailViewController else {
            return
        }
        destination.propertyDetails = propertyDetails
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
// MARK: - Map view
extension NearbyMapViewController: MapView {
    
    func gotAddress(_ fullAddress: String, address: String, cityStateZip: String, location: CLLocation, placeMark: CLPlacemark) {
        
        guard let administrativeArea = placeMark.administrativeArea?.lowercased() else {
            self.hideLoader()
            showAlert(message: "Unable to find your state")
            return
        }
        guard let state = states.filter({ (stateT) -> Bool in
            return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
        }).first else {
            self.hideLoader()
            showAlert(message: "We are not serving in this state")
            return
        }
        
        dispatchGroup.enter()
        presenter.apiCallForPlaceIdFromAddress(fullAddress, address: address, cityStateZip: cityStateZip)
     //   dispatchGroup.enter()
        self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
            var mapDetail: MapLocationResult?
            if let mapDetails =  self.mapDetail?.searchResult.response?.results?.result {
                switch mapDetails {
                case  .dictAny(let mapDetails):
                    mapDetail = mapDetails
                case.array(let mapDetails):
                    mapDetail = mapDetails.first
                }
            }
            var  propertyInfo = PropertyInfo()
            propertyInfo.firstName = self.placeDetail?.name
            propertyInfo.name = self.placeDetail?.name
            propertyInfo.phone = self.placeDetail?.formattedPhoneNumber?.trimmed
            propertyInfo.firstName = self.placeDetail?.name
            propertyInfo.flowType = "Residential"
            propertyInfo.latitude = location.coordinate.latitude
            propertyInfo.longitude = location.coordinate.longitude
            propertyInfo.state = state
            propertyInfo.placeMark = placeMark
            if let mapDetail = mapDetail {
                let address = "\(mapDetail.address.street ?? ""), \(mapDetail.address.city ?? ""), \(mapDetail.address.state ?? ""), \(mapDetail.address.zipcode ?? "")"
                propertyInfo.address = address
                propertyInfo.area = mapDetail.finishedSqFt
                propertyInfo.bathRoom =  mapDetail.bathrooms
                propertyInfo.bedRoom = mapDetail.bedrooms
                propertyInfo.city = mapDetail.address.city
                propertyInfo.lotSizeSqFt = mapDetail.lotSizeSqFt
                // propertyInfo.country =
                propertyInfo.state?.name = mapDetail.address.state
                propertyInfo.zipCode = mapDetail.address.zipcode
            } else {
                let address = "\(placeMark.name ?? ""), \( placeMark.locality ?? ""), \(placeMark.administrativeArea ?? ""), \(placeMark.postalCode ?? "")"
                propertyInfo.address = address
                propertyInfo.city = placeMark.locality
                propertyInfo.country = placeMark.country
                propertyInfo.state?.name = placeMark.administrativeArea
                propertyInfo.zipCode = placeMark.postalCode
                propertyInfo.name = placeMark.name
                
            }
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
            let apiKey = "\(key)"
            if  let photos = self.placeDetail?.photos?.first, let photoRefrence = photos.photoRefrence {
                if let urlString =  "https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=\(photoRefrence)&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)
                {
                    AF.request(url).responseData { (response) in
                        if let data = response.data {
                            propertyInfo.imageManual = UIImage(data: data, scale:1)
                            let timeStamp  = Date().currentTimeMillis()
                            let fileName =  "\(String.random())-\(timeStamp).jpeg"
                            propertyInfo.imageUrl = fileName
                        }
                       // self.hideLoader()
                        self.apiCallMelissadataForPlaceDetail(.empty, location: location, state: state, placeMark: placeMark, propertyInfo: propertyInfo)
                    }
                } else {
                  //  self.hideLoader()
                    self.apiCallMelissadataForPlaceDetail(.empty, location: location, state: state, placeMark: placeMark, propertyInfo: propertyInfo)
                }
            } else {
              //  self.hideLoader()
                self.apiCallMelissadataForPlaceDetail(.empty, location: location, state: state, placeMark: placeMark, propertyInfo: propertyInfo)
            }

        })
     //   presenter.apiCallZilliowForPlaceDetail(address: address, cityStateZip: cityStateZip)
    }
    func gotPlaceDetail(output: PlaceComponets) {
        self.placeDetail = output
        dispatchGroup.leave()
    }
    
    func gotPlaceDetailByZilliow(output: MapResult) {
        self.mapDetail = output
        dispatchGroup.leave()
    }
    func alertInZilliow(_ message:String) {
        dispatchGroup.leave()
    }
    func noGoogleLocation() {
        dispatchGroup.leave()
    }
    
    func gotAssignedAreaForUser(_ output: MapGeoFencingD2D?) {
        //print(output)
        self.mapView.clear()
        self.drawShapeForSingleUser(output!)
     //   self.shapesData = output
     //   nearbyDelegate?.gotShapesDataSingleUser(output)
    }
    func gotNoAssignedArea(_ message: String) {
            getCurrentLocation()
        }
    
}
// MARK: - GMUClusterRendererDelegate
extension NearbyMapViewController: GMUClusterRendererDelegate {
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        
        if (marker.userData! is POIItem) {
            guard let customClusterItem = (marker.userData as? POIItem) else {
                return
            }
            var image =  UIImage(named: "Group 758")
            var color = UIColor.blue
            if let statusName = customClusterItem.lead?.visitStatusSysName, let colorCode = AppUserDefaults.shared.leadDetailMaster?.visitsStatus.first(where: { (status) -> Bool in
                return status.sysName == statusName
            })?.colorCode {
                // marker.title = item.address1
                if let tempColor = UIColor(hex:colorCode) {
                    color = tempColor
                }
            } else  if let sysName = customClusterItem.opportunity?.opportunityStage, let colorCode = AppUserDefaults.shared.leadDetailMaster?.leadStageMasters.first(where: { (status) -> Bool in
                return status.sysName?.lowercased() == sysName.lowercased()
            })?.colorCode {
                
                if let tempColor = UIColor(hex:colorCode) {
                    color = tempColor
                }
                // marker.title = item.address1
            } else  if let sysName = customClusterItem.opportunity?.opportunityStatus, let colorCode = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus.first(where: { (status) -> Bool in
                return status.statusName?.lowercased() == sysName.lowercased()
            })?.colorCode {
                
                if let tempColor = UIColor(hex:colorCode) {
                    color = tempColor
                }
                // marker.title = item.address1
            } else if let _ = customClusterItem.account {
                image =  UIImage(named: "Group 915")
            }
            marker.icon = image?.maskWithColor(color: color)
        }
    }
    
}
