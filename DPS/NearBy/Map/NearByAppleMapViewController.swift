//
//  NearByAppleMapViewController.swift
//  DPS
//
//  Created by Vivek Patel on 16/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire

class NearByAppleMapViewController: BaseViewController {
    
    
    private enum AnnotationReuseID: String {
        case pin
    }
    
    // MARK: - Outlet
    @IBOutlet weak var btnLayer: UIButton!
    @IBOutlet weak var btnNavigator: UIButton!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var lblAccoutsCount: UILabel!
    @IBOutlet weak var lblOpportunityCount: UILabel!
    @IBOutlet weak var lblLeadsCount: UILabel!
    @IBOutlet private var mapView: MKMapView!
    @IBOutlet weak var btnSearch: UIButton!
    
    // MARK: - Variables
    private let locationService = LocationService.instance
    private var kCameraLatitude = 40.692167
    private var kCameraLongitude = -102.664972
    private var placeDetail: PlaceComponets?
    private var mapDetail: MapResult?
    private let dispatchGroup = DispatchGroup()
    private lazy var states: [State] = []
    private lazy var leadAnnotations: [MKAnnotation] = []
    var nearbyFilterData = NearbyFilterData()
    private lazy var presenter = {
        return MapPresenter(self)
    }()
    
    private var needToSetCurrentLocation = false
    private var currentPlacemark: CLPlacemark?
    private var boundingRegion: MKCoordinateRegion = MKCoordinateRegion(MKMapRect.world) {
        didSet {
            self.mapView?.region = boundingRegion
        }
    }
    var isLocationChange = false
    var leads: [Lead] = [] {
        didSet{
            generateClusterFromLeads(leads)
        }
    }
    var opportunities: [Opportunity] = [] {
        didSet{
            generateClusterFromOpportunities(opportunities)
        }
    }
    var accounts: [Account] = [] {
        didSet{
            generateClusterFromAccounts(accounts)
        }
    }
    private var localSearch: MKLocalSearch? {
        willSet {
            localSearch?.cancel()
        }
    }
    weak var delegate: NearbyMapDelegate?
    private var locationManager:CLLocationManager!
    var shapesData: [GeoFencingD2D]?
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setGestureOnMap()
        
        mapView.showsUserLocation = true
        mapView.delegate = self
        mapView.region = boundingRegion
        // Make sure `MKPinAnnotationView` and the reuse identifier is recognized in this map view.
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: AnnotationReuseID.pin.rawValue)
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        mapView.mapType = .hybrid
        getSavedLatitudeLongitude()
        setUI()
        //drawShapes()
        states = AppUtility.fetchStates() ?? []
        if !nearbyFilterData.isFilter  {
            //getCurrentLocation()
            
        } else {
            let address = nearbyFilterData.userLocation.address ?? ""
            if let latitude =  nearbyFilterData.userLocation.latitude , let longitude = nearbyFilterData.userLocation.longitude {
                let location = CLLocation(latitude: latitude, longitude: longitude)
                changeMapLocation(location, address: address)
                generateClusterFromAccounts(accounts)
                generateClusterFromOpportunities(opportunities)
                generateClusterFromLeads(leads)
            }
        }
        
        apiCallForAssingedAreaInitial()

    }
    override func viewDidAppear(_ animated: Bool) {
        if !needToSetCurrentLocation  {
            determineCurrentLocation()
        }
    }
    
    // MARK: - Covinience
    private func apiCallForAssingedAreaInitial() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        presenter.apiCallToGetAssignedAreaForUser(employeeID,companyKey: companyKey, isLoaderShow: true)
    }
    
    // MARK: - Action
    @IBAction func actionSetting(_ sender: Any) {
        mapView.mapType = .satellite
    }
    
    @IBAction func actionLayer(_ sender: Any) {
        mapView.mapType = .hybrid
    }
    
    @IBAction func actioNavigation(_ sender: Any) {
        mapView.mapType = .standard
    }
    @IBAction func actionSearch(_ sender: Any) {
        
        guard let suggestionController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: SuggestionsViewController.self)) as? SuggestionsViewController else { return }
        suggestionController.delegate = self
        suggestionController.currentPlacemark = currentPlacemark
        suggestionController.searchRegion = boundingRegion
        present(suggestionController, animated: true, completion: nil)
    }
    @IBAction func revealRegionDetailsWithLongPressOnMap(sender: UILongPressGestureRecognizer) {
        if sender.state != UIGestureRecognizer.State.began { return }
        let touchLocation = sender.location(in: mapView)
        let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
        print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
    }
    
    private func drawShapeForSingleUser(_ shape: MapGeoFencingD2D) {
        self.mapView.removeOverlays(self.mapView.overlays)
        let dictAssigned = NSMutableDictionary()
        dictAssigned.setValue(false, forKey: "Assigned")
        if shape.modeType == "circle" {
            if let loc = shape.circleCenter, let lat = loc.lat, let lng = loc.lng, let radiusString = shape.circleRadius, let radius = Double(radiusString) {
                let center = CLLocationCoordinate2D(latitude: lat , longitude: lng)
                let circle = MKCircle(center: center, radius: radius)
                self.mapView?.addOverlay(circle)

                if loc == nil || loc == nil {
                    
                    if !nearbyFilterData.isFilter  {
                        getCurrentLocation()
                        generateClusterFromAccounts(accounts)
                        generateClusterFromOpportunities(opportunities)
                        generateClusterFromLeads(leads)
                    } else {
                        
                        if nearbyFilterData.isType == "Assigned" {

                            let address = nearbyFilterData.userLocation.address ?? ""
                            if let latitude =  nearbyFilterData.userLocation.latitude , let longitude = nearbyFilterData.userLocation.longitude {
                                let location = CLLocation(latitude: latitude, longitude: longitude)
                                changeMapLocation(location, address: address)
                                generateClusterFromAccounts(accounts)
                                generateClusterFromOpportunities(opportunities)
                                generateClusterFromLeads(leads)
                            }

                        }else if nearbyFilterData.isType == "Current" {
                            
                            getCurrentLocation()
                            
                        }
                        
                    }
                    
                } else {
                    
                    
                    if nearbyFilterData.isType == "Assigned" {
                        
                        let locationLocal = CLLocation(latitude: lat, longitude: lng)
                        changeMapLocation(locationLocal, address: "")
                      
                        generateClusterFromAccounts(accounts)
                        generateClusterFromOpportunities(opportunities)
                        generateClusterFromLeads(leads)
                        dictAssigned.setValue("\(lat)", forKey: "latitude")
                        dictAssigned.setValue("\(lng)", forKey: "longitude")
                        dictAssigned.setValue(true, forKey: "Assigned")
                        
                    }else if nearbyFilterData.isType == "Current" {
                        
                        getCurrentLocation()
                        
                    }
                    
                }


            }
        } else if shape.modeType == "polygon" {
            
            let latlocal = shape.mapCenterLatLng?.lat
            let longLocal = shape.mapCenterLatLng?.lng
            
            if latlocal == nil || longLocal == nil {
               
                if !nearbyFilterData.isFilter  {
                    getCurrentLocation()
                    generateClusterFromAccounts(accounts)
                    generateClusterFromOpportunities(opportunities)
                    generateClusterFromLeads(leads)
                } else {
                    
                    if nearbyFilterData.isType == "Assigned" {
                        
                        let address = nearbyFilterData.userLocation.address ?? ""
                        if let latitude =  nearbyFilterData.userLocation.latitude , let longitude = nearbyFilterData.userLocation.longitude {
                            let location = CLLocation(latitude: latitude, longitude: longitude)
                            changeMapLocation(location, address: address)
                            generateClusterFromAccounts(accounts)
                            generateClusterFromOpportunities(opportunities)
                            generateClusterFromLeads(leads)
                            dictAssigned.setValue("\(latitude)", forKey: "latitude")
                            dictAssigned.setValue("\(longitude)", forKey: "longitude")
                            dictAssigned.setValue(true, forKey: "Assigned")
                        }

                    }else if nearbyFilterData.isType == "Current" {
                        getCurrentLocation()
                        generateClusterFromAccounts(accounts)
                        generateClusterFromOpportunities(opportunities)
                        generateClusterFromLeads(leads)
                    }
                    
                }
                
            } else {
                
                if nearbyFilterData.isType == "Assigned" {
                    
                    let locationLocal = CLLocation(latitude: latlocal!, longitude: longLocal!)
                    changeMapLocation(locationLocal, address: "")
                    
                    generateClusterFromAccounts(accounts)
                    generateClusterFromOpportunities(opportunities)
                    generateClusterFromLeads(leads)
                    dictAssigned.setValue("\(latlocal!)", forKey: "latitude")
                    dictAssigned.setValue("\(longLocal!)", forKey: "longitude")
                    dictAssigned.setValue(true, forKey: "Assigned")

                }else if nearbyFilterData.isType == "Current" {
                    getCurrentLocation()
                    generateClusterFromAccounts(accounts)
                    generateClusterFromOpportunities(opportunities)
                    generateClusterFromLeads(leads)
                }
                
            }
            
            var points: [CLLocationCoordinate2D] = []
            if let polygonLatLngArray = shape.polygonLatLngArray {
                for cordiantes in polygonLatLngArray {
                    if let lat =  cordiantes.lat ,  let lng = cordiantes.lng {
                        points.append(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                    }
                }
                let polygon = MKPolygon(coordinates: points, count: points.count)
                self.mapView?.addOverlay(polygon)
                
            }
        }
        else
        {
            
            let latlocal = shape.mapCenterLatLng?.lat
            let longLocal = shape.mapCenterLatLng?.lng
            
            if latlocal == nil || longLocal == nil {
                
                if !nearbyFilterData.isFilter  {
                    getCurrentLocation()
                    generateClusterFromAccounts(accounts)
                    generateClusterFromOpportunities(opportunities)
                    generateClusterFromLeads(leads)
                } else {
                    
                    if nearbyFilterData.isType == "Assigned" {
                        
                        let address = nearbyFilterData.userLocation.address ?? ""
                        if let latitude =  nearbyFilterData.userLocation.latitude , let longitude = nearbyFilterData.userLocation.longitude {
                            let location = CLLocation(latitude: latitude, longitude: longitude)
                            changeMapLocation(location, address: address)
                            generateClusterFromAccounts(accounts)
                            generateClusterFromOpportunities(opportunities)
                            generateClusterFromLeads(leads)
                            dictAssigned.setValue("\(latitude)", forKey: "latitude")
                            dictAssigned.setValue("\(longitude)", forKey: "longitude")
                            dictAssigned.setValue(true, forKey: "Assigned")
                            
                        }

                    }else if nearbyFilterData.isType == "Current" {
                        getCurrentLocation()
                        generateClusterFromAccounts(accounts)
                        generateClusterFromOpportunities(opportunities)
                        generateClusterFromLeads(leads)
                    }
                    
                }
                
            } else {
                
                
                if nearbyFilterData.isType == "Assigned" {
                    
                    let locationLocal = CLLocation(latitude: latlocal!, longitude: longLocal!)
                    changeMapLocation(locationLocal, address: "")
                    
                    generateClusterFromAccounts(accounts)
                    generateClusterFromOpportunities(opportunities)
                    generateClusterFromLeads(leads)
                    dictAssigned.setValue("\(latlocal!)", forKey: "latitude")
                    dictAssigned.setValue("\(longLocal!)", forKey: "longitude")
                    dictAssigned.setValue(true, forKey: "Assigned")
                    
                }else if nearbyFilterData.isType == "Current" {
                    getCurrentLocation()
                    generateClusterFromAccounts(accounts)
                    generateClusterFromOpportunities(opportunities)
                    generateClusterFromLeads(leads)
                }
                
            }
            
            
            var rectanglePath: [CLLocationCoordinate2D] = []
            guard let rectangleSouthWest = shape.rectangleSouthWest, let rectangleNorthEast = shape.rectangleNorthEast, let latSouth =  rectangleSouthWest.lat, let lngSouth = rectangleSouthWest.lng, let latNorth =  rectangleNorthEast.lat, let lngNorth = rectangleNorthEast.lng  else { return }
            rectanglePath.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
            rectanglePath.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
            rectanglePath.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
            rectanglePath.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
//            rectanglePath.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
//            rectanglePath.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
            let rectangle = MKPolygon(coordinates: rectanglePath, count: rectanglePath.count)
            self.mapView?.addOverlay(rectangle)
            
        }
        nsud.setValue(dictAssigned, forKey: "FilterLocationAssigned")

    }
    func drawShapes() {
        guard let shapesData = shapesData else { return }
        for shape in shapesData {
            if shape.modeType == "circle" {
                if let loc = shape.circleCenter, let lat = loc.lat, let lng = loc.lng, let radiusString = shape.circleRadius, let radius = Double(radiusString) {
                    let center = CLLocationCoordinate2D(latitude: lat , longitude: lng)
                    let circle = MKCircle(center: center, radius: radius)
                    self.mapView?.addOverlay(circle)
                }
            } else if shape.modeType == "polygon" {
                var points: [CLLocationCoordinate2D] = []
                if let polygonLatLngArray = shape.polygonLatLngArray {
                    for cordiantes in polygonLatLngArray {
                        if let lat =  cordiantes.lat ,  let lng = cordiantes.lng {
                            points.append(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                        }
                    }
                    let polygon = MKPolygon(coordinates: points, count: points.count)
                   
                    self.mapView?.addOverlay(polygon)
                    
                   /* let renderer = MKPolygonRenderer(polygon: polygon )
                    renderer.fillColor=UIColor.red.withAlphaComponent(0.5)
                    renderer.strokeColor=UIColor.orange
                    renderer.lineWidth=4
                    self.mapView.addOverlay(renderer)
                    
                    let myPolyline = MKPolyline(coordinates: points, count: points.count)
                    
                    mapView.addOverlay(myPolyline)*/

                }
                /* if let lat =  shapesData.mapCenterLatLng?.lat , let lng = shapesData.mapCenterLatLng?.lng {
                 let mapCenter = CLLocationCoordinate2D(latitude:lat, longitude: lng)
                 let region = MKCoordinateRegion(center: mapCenter , latitudinalMeters: CLLocationDistance(exactly: 5000)!, longitudinalMeters: CLLocationDistance(exactly: 5000)!)
                 mapView?.setRegion((mapView?.regionThatFits(region))!, animated: true)*/
            } else {
                var rectanglePath: [CLLocationCoordinate2D] = []
                guard let rectangleSouthWest = shape.rectangleSouthWest, let rectangleNorthEast = shape.rectangleNorthEast, let latSouth =  rectangleSouthWest.lat, let lngSouth = rectangleSouthWest.lng, let latNorth =  rectangleNorthEast.lat, let lngNorth = rectangleNorthEast.lng  else { return }
                rectanglePath.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
                rectanglePath.append(CLLocationCoordinate2D(latitude: latNorth, longitude: latNorth))
                rectanglePath.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
                rectanglePath.append(CLLocationCoordinate2D(latitude: latNorth, longitude: latNorth))
                rectanglePath.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
                rectanglePath.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
                let rectangle = MKPolygon(coordinates: rectanglePath, count: rectanglePath.count)
                self.mapView?.addOverlay(rectangle)
            }
        }
    }
    
    //MARK: Melissadata API Calling
     func apiCallMelissadataForPlaceDetail(_ propertyType: PropertyType, lead: Lead? = nil, opportunity: Opportunity? = nil, account: Account? = nil, location: CLLocation? = nil, state: State? = nil, placeMark: CLPlacemark? = nil, propertyInfo: PropertyInfo? = nil)  {
        
        var  propertyInfoNew = PropertyInfo()
        propertyInfoNew = propertyInfo!
       let dictRecord = NSMutableDictionary()
        dictRecord.setValue(propertyInfo?.address, forKey: "AddressLine1")
        dictRecord.setValue(propertyInfo?.city, forKey: "City")
        dictRecord.setValue(propertyInfo?.state!.stateShortName, forKey: "State")
        dictRecord.setValue(propertyInfo?.zipCode, forKey: "PostalCode")

//        dictRecord.setValue("18715 Rogers Lk", forKey: "AddressLine1")
//            dictRecord.setValue("San Antonio", forKey: "City")
//            dictRecord.setValue("TX", forKey: "State")
//            dictRecord.setValue("70258", forKey: "PostalCode")
        
        let aryRecord = NSMutableArray()
        aryRecord.add(dictRecord)
        let dictData = NSMutableDictionary()

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let apiKeyMellissa = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.MelissaKey")!)"

        dictData.setValue(apiKeyMellissa.count == 0 ? MelissaKey : apiKeyMellissa, forKey: "CustomerId")
        dictData.setValue("Property V4 - LookupProperty", forKey: "TransmissionReference")
        dictData.setValue("1", forKey: "TotalRecords")
        dictData.setValue("GrpAll", forKey: "Columns")
        dictData.setValue(aryRecord, forKey: "Records")
        var jsonString = String()

        if(JSONSerialization.isValidJSONObject(dictData) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictData, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            print(dictData)
        Global().getServerResponseForSendLead(toServer: UrlMelissa, requestData, (NSDictionary() as! [AnyHashable : Any]), "", "") { (success, response, error) in
            self.hideLoader()

                if(success)
                {
                if((response as NSDictionary?)?.value(forKey: "Records") is NSArray){
                    
                var aryRecord = NSMutableArray()
                    aryRecord = ((response as NSDictionary?)?.value(forKey: "Records") as! NSArray).mutableCopy()as! NSMutableArray
                    
                    if(aryRecord.count != 0)
                    {
                        
                        if((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") is NSDictionary){
                            
                            var dictRecord = NSMutableDictionary()
                            
                            dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            let dictIntRoomInfo =  removeNullFromDict(dict: dictRecord)
                            
                            propertyInfoNew.bathRoom = "\(dictIntRoomInfo.value(forKey: "BathCount")!)"
                            
                            propertyInfoNew.bedRoom = "\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)"
                            
                            propertyInfoNew.noOfFloor = "\(dictIntRoomInfo.value(forKey: "StoriesCount")!)"
                            
                            propertyInfoNew.flowType = "Residential"
                            
                            
                            var typeInfo = "Residential"
                            
                            if let noofBedroom = Int("\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)")?.description , !"\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)".isEmpty , let noofBathroom =   Int("\(dictIntRoomInfo.value(forKey: "BathCount")!)")?.description  , !"\(dictIntRoomInfo.value(forKey: "BathCount")!)".isEmpty {
                                
                                typeInfo.append(" \(noofBedroom) Bed /" )
                                
                                typeInfo.append(" \(noofBathroom) Bath." )
                                
                            } else {
                                
                                if let noofBedroom = Int("\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)")?.description , !noofBedroom.isEmpty {
                                    
                                    typeInfo.append(" \(noofBedroom) Bed")
                                    
                                }
                                
                                if let noofBathroom =  Int("\(dictIntRoomInfo.value(forKey: "BathCount")!)")?.description  , !noofBathroom.isEmpty {
                                  
                                    typeInfo.append(" \(noofBathroom) Bath.")
                                    
                                }
                                
                            }
                            
                            propertyInfoNew.type = typeInfo
                           
                        }else{
                            
                            propertyInfoNew.bathRoom = ""
                            
                            propertyInfoNew.bedRoom = ""
                            
                            propertyInfoNew.noOfFloor = ""
                            
                            propertyInfoNew.type = ""
                            
                            propertyInfoNew.flowType = ""
                              
                        }
                        
                        if((aryRecord[0] as AnyObject).value(forKey: "PropertySize") is NSDictionary){
                            
                            var dictRecord = NSMutableDictionary()
                            
                            dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PropertySize") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                         
                            let dictPropertySize =  removeNullFromDict(dict: dictRecord)
                            
                            propertyInfoNew.lotSizeSqFt = "\(dictPropertySize.value(forKey: "AreaBuilding")!)"
                            
                            propertyInfoNew.area = "\(dictPropertySize.value(forKey: "AreaBuilding")!)"
                           
                            
                        }else{
                            
                            propertyInfoNew.lotSizeSqFt = ""
                            
                            propertyInfoNew.area = ""
                           
                        }
                       
                        if((aryRecord[0] as AnyObject).value(forKey: "PrimaryOwner") is NSDictionary){
                            
                            var dictRecord = NSMutableDictionary()
                            
                            dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PrimaryOwner") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            let dictPropertySize =  removeNullFromDict(dict: dictRecord)
                          
                            propertyInfoNew.firstName = "\(dictPropertySize.value(forKey: "Name1First")!)"
                            
                            propertyInfoNew.middleName = "\(dictPropertySize.value(forKey: "Name1Middle")!)"
                            
                            propertyInfoNew.lastName = "\(dictPropertySize.value(forKey: "Name1Last")!)"
                            
                            propertyInfoNew.name = "\(dictPropertySize.value(forKey: "Name1Full")!)"
                          
                        }else{
                            
                            propertyInfoNew.firstName = ""
                            
                            propertyInfoNew.middleName = ""
                            
                            propertyInfoNew.lastName = ""
                            
                            propertyInfoNew.name = ""
                            
                        }
                        
                    }

                }
            }
                else
                {
                    
                }
            self.openVisitDeatils(.empty, location: location, state: state, placeMark: placeMark, propertyInfo: propertyInfoNew)

        }

        /*guard let urlString =  "http://www.zillow.com/webservice/getUpdatedPropertyDetails?zws-id=X1-ZWz19zkb2lkumj_7zykg&address=\(address)&citystatezip=\(cityStateZip)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
         self.delegateMap?.alertInZilliow("Unknown Error")
         return
         }*/
//        guard let urlString =  "http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=X1-ZWz19zkb2lkumj_7zykg&address=\(address)&citystatezip=\(cityStateZip)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else { return }
//
//        service.getPlaceDetailUsingZilliow(url) { (mapResult, error) in
//            if let mapResult = mapResult {
//                self.delegateMap?.hideLoader()
//
    //            self.delegateMap?.gotPlaceDetailByZilliow(output: mapResult)
//            } else {
//                self.delegateMap?.hideLoader()
//                self.delegateMap?.alertInZilliow(error ?? "Unknown Error")
//            }
//        }
    }
}

// MARK: - UpdateMapDelegate
extension NearByAppleMapViewController: UpdateMapDelegate {
    func actionSearch() {
        guard let suggestionController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: SuggestionsViewController.self)) as? SuggestionsViewController else { return }
        suggestionController.delegate = self
        suggestionController.currentPlacemark = currentPlacemark
        suggestionController.searchRegion = boundingRegion
        present(suggestionController, animated: true, completion: nil)
    }
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        self.nearbyFilterData.userLocation.latitude = locationDetails.geometry.location.latitude
        self.nearbyFilterData.userLocation.longitude = locationDetails.geometry.location.longitude
        self.nearbyFilterData.userLocation.address = locationDetails.formattedAddress
        setMapMarker(locationDetails)
    }
    private func setMapMarker(_ locationDetails: LocationResult) {
        
        let location = CLLocation(latitude: locationDetails.geometry.location.latitude, longitude: locationDetails.geometry.location.longitude)
        self.changeLocation(location, address: "")
    }
}
// MARK: - Convenience
extension NearByAppleMapViewController {
    private func setUI() {
        btnSearch.layer.cornerRadius = 0
        AppUtility.buttonImageColor(btn: btnSetting, image: "Group 772", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnNavigator, image: "Group 765", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnLayer, image: "Group 768", color: .appDarkGreen)
    }
    private func getSavedLatitudeLongitude() {
        kCameraLatitude = nearbyFilterData.userLocation.latitude ?? ConstantV.latitude
        kCameraLongitude = nearbyFilterData.userLocation.longitude ?? ConstantV.longitude
    }
    
    func changeMapLocation(_ location: CLLocation, address:String) {
        
        let pin = MKPointAnnotation()
        pin.title = address
        pin.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        mapView?.addAnnotation(pin)
        changeLocation(location, address: address)
    }
    private func changeLocation(_ location: CLLocation, address:String) {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let mRegion = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView?.setRegion(mRegion, animated: true)
        needToSetCurrentLocation = true
        //  self.boundingRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 12_000, longitudinalMeters: 12_000)
        mapView?.setCamera(MKMapCamera(lookingAtCenter: location.coordinate, fromDistance: 1000, pitch: 0, heading: .greatestFiniteMagnitude), animated: true)
    }
    private func setupMapItem(_ mapItems: [MKMapItem]) {
        
        clearSearchedAnnotations()
        let annotations = mapItems.compactMap { (mapItem) -> PlaceAnnotation? in
            guard let coordinate = mapItem.placemark.location?.coordinate else { return nil }
            
            let annotation = PlaceAnnotation(coordinate: coordinate)
            annotation.title = mapItem.name
            annotation.searched = true
            return annotation
        }
        mapView.addAnnotations(annotations)
    }
    private func clearSearchedAnnotations() {
        
        var annotations: [MKAnnotation] = []
        for annotation in mapView.annotations {
            if let placeAnnotation = annotation as? PlaceAnnotation, placeAnnotation.lead == nil, placeAnnotation.opportunity == nil , placeAnnotation.account == nil  {
                annotations.append(placeAnnotation)
            }
        }
        mapView.removeAnnotations(annotations)
        drawShapes()
    }
}
// MARK: - Convenience Cluster
extension NearByAppleMapViewController {
    
    
    private func generateClusterFromAccounts(_ accounts: [Account]) {
        
        mapView?.removeAnnotations(leadAnnotations)
        leadAnnotations.removeAll()
        drawShapes()
        var accCount = 0
        for object in accounts {
            let lat =  object.latitude ?? 0.0
            let lng = object.longitude ?? 0.0
            if lat > -90.0 && lat < 90.0 , lng > -180.0 && lng < 180.0{
                let coordinate = CLLocationCoordinate2DMake(lat, lng)
                let annotation = PlaceAnnotation(coordinate: coordinate)
                annotation.account = object
                annotation.searched = true
                self.leadAnnotations.append(annotation)
                accCount += 1
            }
        }
        lblAccoutsCount?.text = accCount.description
        // guard let bounds = self.bounds else { return }
        // self.mapView.animate(with: .fit(bounds, withPadding: 100.0))
    }
    private func generateClusterFromOpportunities(_ opportunities: [Opportunity]) {
        
        lblOpportunityCount?.text = opportunities.count.description
        
        var oppCount = 0
        for object in opportunities {
            let lat =  object.latitude ?? 0.0
            let lng = object.longitude ?? 0.0
            if accounts.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil {
                // lat =  lat + extent * randomScale()
                // lng = lng + extent * randomScale()
                if lat > -90.0 && lat < 90.0 , lng > -180.0 && lng < 180.0{
                    let coordinate = CLLocationCoordinate2DMake(lat, lng)
                    let annotation = PlaceAnnotation(coordinate: coordinate)
                    annotation.opportunity = object
                    annotation.searched = true
                    self.leadAnnotations.append(annotation)
                    oppCount += 1
                }
            }
        }
        lblOpportunityCount?.text = oppCount.description
    }
    
    private func generateClusterFromLeads(_ leads: [Lead]) {
        
        var leadCount = 0
        for object in leads {
            let lat =  object.latitude ?? 0.0
            let lng = object.longitude ?? 0.0
            if opportunities.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil &&   accounts.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil {
                if lat > -90.0 && lat < 90.0 , lng > -180.0 && lng < 180.0 {
                    let coordinate = CLLocationCoordinate2DMake(lat, lng)
                    let annotation = PlaceAnnotation(coordinate: coordinate)
                    annotation.lead = object
                    annotation.searched = true
                    self.leadAnnotations.append(annotation)
                    leadCount += 1
                }
            }
        }
        lblLeadsCount?.text = leadCount.description
        self.mapView?.addAnnotations(leadAnnotations)
        /*
         self.clusterManager?.clearItems()
         if !isLocationChange {
         self.mapView.clear()
         }
         self.clusterManager?.add(self.items)*/
    }
    
}
// MARK: - Location Manager
extension NearByAppleMapViewController: CLLocationManagerDelegate {
    
    //MARK:- CLLocationManagerDelegate Methods
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let mUserLocation:CLLocation = locations[0] as CLLocation
        let center = CLLocationCoordinate2D(latitude: mUserLocation.coordinate.latitude, longitude: mUserLocation.coordinate.longitude)
        let mRegion = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(mRegion, animated: true)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error - locationManager: \(error.localizedDescription)")
    }
    //MARK:- Intance Methods
    
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startMonitoringSignificantLocationChanges()
        }
    }
}
// MARK: - Convenience
extension NearByAppleMapViewController {
    private func getCurrentLocation() {
        locationService.getLocation { location in
            
            guard let location = location else {
                self.delegate?.gotCurrentLocation(latitude: self.kCameraLatitude, longitude: self.kCameraLongitude)
                return
            }
            self.kCameraLatitude = location.coordinate.latitude
            self.kCameraLongitude = location.coordinate.longitude
            self.nearbyFilterData.userLocation.latitude = location.coordinate.latitude
            self.nearbyFilterData.userLocation.longitude = location.coordinate.longitude
            self.delegate?.gotCurrentLocation(latitude: self.kCameraLatitude, longitude: self.kCameraLongitude)
            
            self.changeLocation(location, address: "")
            self.getPlaceMark(location)
        }
    }
    private func getPlaceMark(_ location: CLLocation) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemark, error) in
            guard error == nil else { return }
            
            self.currentPlacemark = placemark?.first
//            self.boundingRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 12_000, longitudinalMeters: 12_000)
            self.boundingRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 200, longitudinalMeters: 200)

        }
    }
    
    private func showAlertForBranchChange(strAppoinntmentBranch : String, strEmpLoginBranchSysName : String) {
        
        var loggedBranchNameLocal = ""
        var appointmentBranchNameLocal = ""

        var arrBranch = NSMutableArray()
        arrBranch = getBranches()
        
        for item in arrBranch
        {
            
            let dict = item as! NSDictionary
            
            if strEmpLoginBranchSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                loggedBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
            if strAppoinntmentBranch == "\(dict.value(forKey: "SysName") ?? "")"
            {
                appointmentBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
        }
        
        let strMsssggg = "You are currently in " + "\(loggedBranchNameLocal)" + " branch. Please switch branch to work on the " + "\(appointmentBranchNameLocal)" + " branch."
        
        //You are currently in Lodi branch. Please switch branch to work on the Fresno branch.
        
        let alert = UIAlertController(title: alertMessage, message: strMsssggg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Switch Branch", style: .destructive, handler: { (nil) in
            
            self.goToSwitchBranch()
            
        }))
        alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
            
            // Do Nothing On This.
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getBranches() -> NSMutableArray {
        let temparyBranch = NSMutableArray()

        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                temparyBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
        return temparyBranch
    }
    
    func goToSwitchBranch() {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "DashBoard" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SwitchBranchVC") as! SwitchBranchVC
         
        vc.reDirectWhere = "NearByMap"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    private func openVisitDeatils(_ propertyType: PropertyType, lead: Lead? = nil, opportunity: Opportunity? = nil, account: Account? = nil, location: CLLocation? = nil, state: State? = nil, placeMark: CLPlacemark? = nil, propertyInfo: PropertyInfo? = nil)  {
        
        
        
        // Check if Logged In Branch is different
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpLoginBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"

        var proceedFurther = true
        
        if lead != nil {
            
            let arrOfBranchesSysName = lead?.branchesSysNames
            
            proceedFurther = false
            
            if arrOfBranchesSysName?.count ?? 0 > 0 {
                
                for k in 0 ..< arrOfBranchesSysName!.count {

                    let strBrancchL = "\(arrOfBranchesSysName![k])"
                    
                    if strBrancchL == strEmpLoginBranchSysName {
                        
                        proceedFurther = true
                        break
                        
                    }
                    
                }
                
            }
            

            if !proceedFurther {
                
                self.showAlertForBranchChange(strAppoinntmentBranch: "\(opportunity?.branchSysName ?? "")", strEmpLoginBranchSysName: strEmpLoginBranchSysName)

            }
            
        }else if opportunity != nil {
            
            if opportunity?.branchSysName != strEmpLoginBranchSysName {
                
                proceedFurther = false
                // Show Alert of Branch
                                
                self.showAlertForBranchChange(strAppoinntmentBranch: "\(opportunity?.branchSysName ?? "")", strEmpLoginBranchSysName: strEmpLoginBranchSysName)
                
            }
            
        }
        
        if proceedFurther {
         
            let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
            guard let controller = storyboard.instantiateViewController(withIdentifier: "EntityDetailViewController") as? EntityDetailViewController else { return }
            controller.modalPresentationStyle = .overFullScreen
            var propertyDetails = PropertyDetails()
            //
            if let placeMark = placeMark {
                self.nearbyFilterData.userLocation.latitude = location?.coordinate.latitude
                self.nearbyFilterData.userLocation.longitude = location?.coordinate.longitude
                let address = "\(placeMark.name ?? ""), \( placeMark.locality ?? ""), \(placeMark.administrativeArea ?? ""), \(placeMark.postalCode ?? "")"
                self.nearbyFilterData.userLocation.address = address
            }
            //  propertyDetails.state = state
            propertyDetails.propertyType = propertyType
            propertyDetails.lead = lead
            propertyDetails.opportunity = opportunity
            propertyDetails.account = account
            propertyDetails.propertyInfo = propertyInfo
            
            // change in Logic according to Settings config. for showing Property Info.

            if(nsud.value(forKey: "isEnableAutoPropertyInformation") != nil){
                
                if(nsud.value(forKey: "isEnableAutoPropertyInformation") as! Bool){

                    
                }else{
                    
                    //
                    if propertyDetails.propertyInfo == nil {
                        var  propertyInfoNew = PropertyInfo()
                        propertyInfoNew.name = "Unknown"
                        propertyInfoNew.firstName = "Unknown"
                        propertyDetails.propertyInfo = propertyInfoNew

                    }
                    
                }
                
            }else{
                
                //
                if propertyDetails.propertyInfo == nil {
                    var  propertyInfoNew = PropertyInfo()
                    propertyInfoNew.name = "Unknown"
                    propertyInfoNew.firstName = "Unknown"
                    propertyDetails.propertyInfo = propertyInfoNew

                }
                
            }
            
            
            // propertyDetails.latitude = location?.coordinate.latitude
            // propertyDetails.longitude = location?.coordinate.longitude
            // propertyDetails.locationDetails = placeDetail
            // propertyDetails.placeMark = placeMark
            controller.propertyDetails = propertyDetails
            placeDetail = nil
            mapDetail = nil
            controller.delegate = self
            controller.selectedLocation = location
            let navController = UINavigationController.init(rootViewController: controller)
            navController.isNavigationBarHidden = true
            navController.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(navController, animated: true, completion: nil)
            
        }
        
    }
}
// MARK: - Map view delegate
extension NearByAppleMapViewController: MKMapViewDelegate {
    func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
        print("Failed to load the map: \(error)")
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? PlaceAnnotation else { return nil }
        
        // Annotation views should be dequeued from a reuse queue to be efficent.
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: AnnotationReuseID.pin.rawValue, for: annotation) as? MKMarkerAnnotationView
        view?.canShowCallout = true
        view?.clusteringIdentifier = "searchResult"
        
        // This is for the searched only
        if annotation.searched {
            let infoButton = UIButton(type: .detailDisclosure)
            view?.rightCalloutAccessoryView = infoButton
        }
        if let statusName = annotation.lead?.visitStatusSysName, let colorCode = AppUserDefaults.shared.leadDetailMaster?.visitsStatus.first(where: { (status) -> Bool in
            return status.sysName == statusName
        })?.colorCode {
            if let tempColor = UIColor(hex:colorCode) {
                view?.markerTintColor  = tempColor
            }
        } else  if let sysName = annotation.opportunity?.opportunityStage, let colorCode = AppUserDefaults.shared.leadDetailMaster?.leadStageMasters.first(where: { (status) -> Bool in
            return status.sysName?.lowercased() == sysName.lowercased()
        })?.colorCode {
            
            if let tempColor = UIColor(hex:colorCode) {
                view?.markerTintColor  = tempColor
            }
        } else  if let sysName = annotation.opportunity?.opportunityStatus, let colorCode = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus.first(where: { (status) -> Bool in
            return status.statusName?.lowercased() == sysName.lowercased()
        })?.colorCode {
            
            if let tempColor = UIColor(hex:colorCode) {
                view?.markerTintColor  = tempColor
            }
        } else if annotation.account != nil {
            view?.markerTintColor = .clear
            view?.glyphTintColor = .clear
            view?.tintColor = .clear
            view?.image = UIImage(named: "Group 915")
        } else {
            let infoButton = UIButton(type: .detailDisclosure)
            view?.rightCalloutAccessoryView = infoButton
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleAnnotationPress(_:)))
        tap.cancelsTouchesInView = true
        view?.addGestureRecognizer(tap)
        return view
    }
    @objc func handleAnnotationPress(_ gestureReconizer: UIGestureRecognizer) { }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = view.annotation as? PlaceAnnotation else { return }
        if annotation.searched, let latitude = view.annotation?.coordinate.latitude, let longitude = view.annotation?.coordinate.longitude  {
            let location = CLLocation(latitude: latitude, longitude: longitude)
            //presenter.apiCallForAddress(location)
            // change in Logic according to Settings config. for showing Property Info.
            
            if(nsud.value(forKey: "isEnableAutoPropertyInformation") != nil){
                if(nsud.value(forKey: "isEnableAutoPropertyInformation") as! Bool){
                    
                    //
                    presenter.apiCallForAddress(location)

                }else{
                    
                    self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)
                    
                }
            }else{
                
                self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)

            }
        }
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        guard let annotation = view.annotation as? PlaceAnnotation else {
            if let clustered = view.annotation as? MKClusterAnnotation {
                var minLat = CLLocationDegrees(exactly: 90)!
                var maxLat = CLLocationDegrees(exactly: -90)!
                var minLong = CLLocationDegrees(exactly: 180)!
                var maxLong = CLLocationDegrees(exactly: -180)!
                clustered.memberAnnotations.forEach { (annotation) in
                    let coordinate = annotation.coordinate
                    minLat = min(minLat, coordinate.latitude)
                    maxLat = max(maxLat, coordinate.latitude)
                    minLong = min(minLong, coordinate.longitude)
                    maxLong = max(maxLong, coordinate.longitude)
                }
                
                let centerLat = (minLat + maxLat) / 2
                let centerLong = (minLong + maxLong) / 2
                let center = CLLocationCoordinate2D(latitude: centerLat, longitude: centerLong)
                let span = MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 1.5, longitudeDelta: (maxLong - minLong) * 1.5) // with some padding
                let region = MKCoordinateRegion(center: center, span: span)
                
                mapView.setRegion(region, animated: true)
            } else {
                if let latitude = view.annotation?.coordinate.latitude, let longitude = view.annotation?.coordinate.longitude  {
                    let location = CLLocation(latitude: latitude, longitude: longitude)
                    //presenter.apiCallForAddress(location)
                    // change in Logic according to Settings config. for showing Property Info.
                    
                    if(nsud.value(forKey: "isEnableAutoPropertyInformation") != nil){
                        if(nsud.value(forKey: "isEnableAutoPropertyInformation") as! Bool){
                            
                            //
                            presenter.apiCallForAddress(location)

                        }else{
                            
                            self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)
                            
                        }
                    }else{
                        
                        self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)

                    }
                }
            }
            return
        }
        if let lead = annotation.lead {
            let locationL = CLLocation(latitude: lead.latitude!, longitude: lead.longitude!)

            openVisitDeatils(.lead, lead: lead, location: locationL)
        } else if let opportunity = annotation.opportunity {
            let locationL = CLLocation(latitude: opportunity.latitude!, longitude: opportunity.longitude!)

            openVisitDeatils(.opportunity, opportunity: opportunity, location: locationL)
        } else if let account = annotation.account {
            let locationL = CLLocation(latitude: account.latitude!, longitude: account.longitude!)

            openVisitDeatils(.account, account: account, location: locationL)
        } else {
            if let latitude = view.annotation?.coordinate.latitude, let longitude = view.annotation?.coordinate.longitude  {
                let location = CLLocation(latitude: latitude, longitude: longitude)
                //presenter.apiCallForAddress(location)
                
                // change in Logic according to Settings config. for showing Property Info.
                
                if(nsud.value(forKey: "isEnableAutoPropertyInformation") != nil){
                    if(nsud.value(forKey: "isEnableAutoPropertyInformation") as! Bool){
                        
                        //
                        presenter.apiCallForAddress(location)

                    }else{
                        
                        self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)
                        
                    }
                }else{
                    
                    self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)

                }
                
            }
            
        }
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if let clustered = view.annotation as? MKClusterAnnotation {
            var minLat = CLLocationDegrees(exactly: 90)!
            var maxLat = CLLocationDegrees(exactly: -90)!
            var minLong = CLLocationDegrees(exactly: 180)!
            var maxLong = CLLocationDegrees(exactly: -180)!
            clustered.memberAnnotations.forEach { (annotation) in
                let coordinate = annotation.coordinate
                minLat = min(minLat, coordinate.latitude)
                maxLat = max(maxLat, coordinate.latitude)
                minLong = min(minLong, coordinate.longitude)
                maxLong = max(maxLong, coordinate.longitude)
            }
            
            let centerLat = (minLat + maxLat) / 2
            let centerLong = (minLong + maxLong) / 2
            let center = CLLocationCoordinate2D(latitude: centerLat, longitude: centerLong)
            let span = MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 1.5, longitudeDelta: (maxLong - minLong) * 1.5) // with some padding
            let region = MKCoordinateRegion(center: center, span: span)
            mapView.setRegion(region, animated: true)
        }
        return
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let shape = overlay as? MKCircle {
            let circleView = MKCircleRenderer(circle: shape)
            circleView.lineWidth = 4
            circleView.fillColor = UIColor.clear//UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            circleView.strokeColor = .white
            return circleView
        } else {
            let shapePolygone = (overlay as? MKPolygon)!
            let polygonView = MKPolygonRenderer(polygon: shapePolygone)
            polygonView.lineWidth = 4
            polygonView.fillColor = UIColor.clear//UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            polygonView.strokeColor = .white
            return polygonView
        }
        
    }
    
}
// MARK: - Map view
extension NearByAppleMapViewController: MapView {
    
    func gotAddress(_ fullAddress: String, address: String, cityStateZip: String, location: CLLocation, placeMark: CLPlacemark) {
        
        guard let administrativeArea = placeMark.administrativeArea?.lowercased() else {
            self.hideLoader()
            showAlert(message: "Unable to find your state")
            
            return
        }
        guard let state = states.filter({ (stateT) -> Bool in
            return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
        }).first else {
            self.hideLoader()
            showAlert(message: "We are not serving in this state")
            return
        }
        
        dispatchGroup.enter()
        presenter.apiCallForPlaceIdFromAddress(fullAddress, address: address, cityStateZip: cityStateZip)
        self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
            var mapDetail: MapLocationResult?
            if let mapDetails =  self.mapDetail?.searchResult.response?.results?.result {
                switch mapDetails {
                case  .dictAny(let mapDetails):
                    mapDetail = mapDetails
                case.array(let mapDetails):
                    mapDetail = mapDetails.first
                }
            }
            var  propertyInfo = PropertyInfo()
            propertyInfo.firstName = self.placeDetail?.name
            propertyInfo.name = self.placeDetail?.name
            propertyInfo.phone = self.placeDetail?.formattedPhoneNumber?.trimmed
            propertyInfo.firstName = self.placeDetail?.name
            propertyInfo.flowType = "Residential"
            propertyInfo.latitude = location.coordinate.latitude
            propertyInfo.longitude = location.coordinate.longitude
            propertyInfo.state = state
            propertyInfo.placeMark = placeMark
            if let mapDetail = mapDetail {
                let address = "\(mapDetail.address.street ?? ""), \(mapDetail.address.city ?? ""), \(mapDetail.address.state ?? ""), \(mapDetail.address.zipcode ?? "")"
                propertyInfo.address = address
                propertyInfo.area = mapDetail.finishedSqFt
                propertyInfo.bathRoom =  mapDetail.bathrooms
                propertyInfo.bedRoom = mapDetail.bedrooms
                propertyInfo.city = mapDetail.address.city
                propertyInfo.lotSizeSqFt = mapDetail.lotSizeSqFt
                // propertyInfo.country =
                propertyInfo.state?.name = mapDetail.address.state
                propertyInfo.zipCode = mapDetail.address.zipcode
            } else {
                let address = "\(placeMark.name ?? ""), \( placeMark.locality ?? ""), \(placeMark.administrativeArea ?? ""), \(placeMark.postalCode ?? "")"
                propertyInfo.address = address
                propertyInfo.city = placeMark.locality
                propertyInfo.country = placeMark.country
                propertyInfo.state?.name = placeMark.administrativeArea
                propertyInfo.zipCode = placeMark.postalCode
                propertyInfo.name = placeMark.name
                
            }
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
            let apiKey = "\(key)"
            if  let photos = self.placeDetail?.photos?.first, let photoRefrence = photos.photoRefrence {
                if let urlString =  "https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=\(photoRefrence)&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)
                {
                    AF.request(url).responseData { (response) in
                        if let data = response.data {
                            propertyInfo.imageManual = UIImage(data: data, scale:1)
                            let timeStamp  = Date().currentTimeMillis()
                            let fileName =  "\(String.random())-\(timeStamp).jpeg"
                            propertyInfo.imageUrl = fileName
                        }
                       // self.hideLoader()
                        self.apiCallMelissadataForPlaceDetail(.empty, location: location, state: state, placeMark: placeMark, propertyInfo: propertyInfo)
                    }
                } else {
                  //  self.hideLoader()
                    self.apiCallMelissadataForPlaceDetail(.empty, location: location, state: state, placeMark: placeMark, propertyInfo: propertyInfo)
                }
            } else {
              //  self.hideLoader()
                self.apiCallMelissadataForPlaceDetail(.empty, location: location, state: state, placeMark: placeMark, propertyInfo: propertyInfo)
            }

        })
    }
    func gotPlaceDetail(output: PlaceComponets) {
        self.placeDetail = output
        dispatchGroup.leave()
    }
    
    func gotPlaceDetailByZilliow(output: MapResult) {
        self.mapDetail = output
        dispatchGroup.leave()
    }
    func alertInZilliow(_ message:String) {
        dispatchGroup.leave()
    }
    func noGoogleLocation() {
        dispatchGroup.leave()
    }
    
    func gotAssignedAreaForUser(_ output: MapGeoFencingD2D?)
    {
        print(output)
        //self.mapView.cle
        self.drawShapeForSingleUser(output!)
    }
}
// MARK: - Visit action delegate
extension NearByAppleMapViewController: VisitActionDelegate {
    
    func actionGetQuote(_ propertyDetails: PropertyDetails) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "VisitServiceDetailViewController") as? VisitServiceDetailViewController else {
            return
        }
        destination.propertyDetails = propertyDetails
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
}

extension NearByAppleMapViewController: SuggestionsDelegate {
    /// - Parameter suggestedCompletion: A search completion provided by `MKLocalSearchCompleter` when tapping on a search completion table row
    func search(for suggestedCompletion: MKLocalSearchCompletion) {
        showLoader()
        let searchRequest = MKLocalSearch.Request(completion: suggestedCompletion)
        search(using: searchRequest)
    }
    
    /// - Parameter queryString: A search string from the text the user entered into `UISearchBar`
    private func search(for queryString: String?) {
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = queryString
        search(using: searchRequest)
    }
    
    /// - Tag: SearchRequest
    private func search(using searchRequest: MKLocalSearch.Request) {
        // Confine the map search area to an area around the user's current location.
        searchRequest.region = boundingRegion
        
        // Include only point of interest results. This excludes results based on address matches.
        if #available(iOS 13.0, *) {
            searchRequest.resultTypes = .pointOfInterest
        } else {
            // Fallback on earlier versions
        }
        
        localSearch = MKLocalSearch(request: searchRequest)
        localSearch?.start { [unowned self] (response, error) in
            self.hideLoader()
            guard error == nil else {
                self.displaySearchError(error)
                return
            }
            // Used when setting the map's region in `prepareForSegue`.
            if let updatedRegion = response?.boundingRegion {
                self.boundingRegion = updatedRegion
            }
            if let items  = response?.mapItems {
                self.setupMapItem(items)
            }
        }
    }
    private func displaySearchError(_ error: Error?) {
        if let error = error as NSError?, let errorString = error.userInfo[NSLocalizedDescriptionKey] as? String {
            let alertController = UIAlertController(title: "Could not find any places.", message: errorString, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
}
extension NearByAppleMapViewController {
    
    private func setGestureOnMap() {
        
        let tap = UILongPressGestureRecognizer(target: self, action: #selector(handleMapPress(_:)))
        tap.delaysTouchesBegan = true
        tap.cancelsTouchesInView = false
        tap.minimumPressDuration = 0.2
        self.mapView.addGestureRecognizer(tap)
    }
    
    @objc func handleMapPress(_ gestureReconizer: UIGestureRecognizer) {
        guard gestureReconizer.state == UIGestureRecognizer.State.ended else {
            return
        }
        let tapLocation = gestureReconizer.location(in: self.mapView)
        if let subview = self.mapView.hitTest(tapLocation, with: nil) {
            
            if !subview.isKind(of: MKPinAnnotationView.self) && !subview.isKind(of: MKAnnotationView.self) && !subview.isKind(of: MKClusterAnnotation.self)  {
                let touchLocation = gestureReconizer.location(in: mapView)
                let locationCoordinate = mapView.convert(touchLocation,toCoordinateFrom: mapView)
                print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
                let location = CLLocation(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
                //self.presenter.apiCallForAddress(location)
                
                // change in Logic according to Settings config. for showing Property Info.
                
                if(nsud.value(forKey: "isEnableAutoPropertyInformation") != nil){
                    if(nsud.value(forKey: "isEnableAutoPropertyInformation") as! Bool){
                        
                        //
                        presenter.apiCallForAddress(location)

                    }else{
                        
                        self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)
                        
                    }
                }else{
                    
                    self.openVisitDeatils(.empty, location: location, state: nil, placeMark: nil, propertyInfo: nil)

                }
                return
            }
        }
    }
}
