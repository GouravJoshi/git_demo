//
//  MapPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import UIKit

protocol CommonView: NSObjectProtocol {
    func showLoader()
    func hideLoader()
    func showAlert(message: String?)
}
protocol MapView: CommonView {
    func gotPlaceDetail(output: PlaceComponets)
    func gotPlaceDetailByZilliow(output: MapResult)
    func gotAddress(_ fullAddress: String, address: String, cityStateZip: String, location: CLLocation, placeMark: CLPlacemark)
    func alertInZilliow(_ message: String)
    func noGoogleLocation()
    func gotAssignedAreaForUser(_ output: MapGeoFencingD2D?)
    func gotNoAssignedArea(_ message: String)
}
extension MapView {
    func gotAssignedAreaForUser(_ output: MapGeoFencingD2D?) {}
    func gotNoAssignedArea(_ message: String) {}
}

class MapPresenter: NSObject {
    weak private var delegateMap: MapView?
    private lazy var service = {
        return MapService()
    }()
    
    init(_ delegateMap:MapView) {
        self.delegateMap = delegateMap
    }
    func apiCallForAddress(_ location: CLLocation) {
        delegateMap?.showLoader()
        service.geocodeLocation(location) { (placemark, error) in
            if let placemark = placemark {
                let postalAddress = placemark.postalAddress
                let address = placemark.name ?? ""
                let cityStateZip = "\(placemark.locality ?? ""),\(placemark.administrativeArea ?? ""),\(placemark.postalCode ?? "")"
                self.delegateMap?.gotAddress(postalAddress?.description ?? "", address: address, cityStateZip: cityStateZip, location: location, placeMark: placemark)
            } else {
                self.delegateMap?.hideLoader()
                self.delegateMap?.showAlert(message: error)
            }
        }
        
    }
    func apiCallForPlaceIdFromAddress(_ fullAddress: String, address: String, cityStateZip: String) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
        let apiKey = "\(key)"
        guard let urlString = "https://maps.googleapis.com/maps/api/geocode/json?address=\(fullAddress)&sensor=false&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
            self.delegateMap?.noGoogleLocation()
            return
        }
        service.getPlaceId(url) { (loaction, error) in
            if let loaction = loaction {
                self.getPlaceDetailByPlaceId(loaction.placeId ?? "", address: address, cityStateZip: cityStateZip)
            } else {
                self.delegateMap?.noGoogleLocation()
            }
        }
        
    }
    func getPlaceDetailByPlaceId(_ placeId: String, address: String, cityStateZip: String) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
        let apiKey = "\(key)"
        guard let urlString =  "https://maps.googleapis.com/maps/api/place/details/json?place_id=\(placeId)&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else { return }
        service.getPlaceDetail(url) { (placeDetail, error) in
            if let placeDetail = placeDetail {
                self.delegateMap?.gotPlaceDetail(output: placeDetail)
            } else {
                self.delegateMap?.noGoogleLocation()
            }
        }
    }
    func apiCallZilliowForPlaceDetail(address: String, cityStateZip: String) {
        /*guard let urlString =  "http://www.zillow.com/webservice/getUpdatedPropertyDetails?zws-id=X1-ZWz19zkb2lkumj_7zykg&address=\(address)&citystatezip=\(cityStateZip)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
         self.delegateMap?.alertInZilliow("Unknown Error")
         return
         }*/
        guard let urlString =  "http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=X1-ZWz19zkb2lkumj_7zykg&address=\(address)&citystatezip=\(cityStateZip)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else { return }
        
        service.getPlaceDetailUsingZilliow(url) { (mapResult, error) in
            if let mapResult = mapResult {
                self.delegateMap?.gotPlaceDetailByZilliow(output: mapResult)
            } else {
                self.delegateMap?.hideLoader()
                self.delegateMap?.alertInZilliow(error ?? "Unknown Error")
            }
        }
    }
    
    func apiCallToGetAssignedAreaForUser( _ empId: String, companyKey: String, isLoaderShow: Bool? = false) {
        if let isLoaderShow = isLoaderShow, isLoaderShow {
            self.delegateMap?.showLoader()
        }
        service.getAssingAreaForUser(empId: empId, companyKey: companyKey) { (object, error) in
            self.delegateMap?.hideLoader()
            if let object = object {
                self.delegateMap?.gotAssignedAreaForUser(object)
            } else {
                self.delegateMap?.gotNoAssignedArea(error ?? "Unknown Error")
            }
        }
    }
}

