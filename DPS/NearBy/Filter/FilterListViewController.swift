//
//  FilterListViewController.swift
//  DPS
//
//  Created by Vivek Patel on 06/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

struct MultiSelectRow {
    var title:String
    var isSelected = false
}
enum PickerListType {
    case visit
    case leads
    case oppotunity
}
final class FilterListViewController: UIViewController {
    // MARK: - Variables
    var viewClicked:UIView?
    var selectedRow = -1
    var pickerType:PickerListType?
    var doneAction:MultiSelectActionHandler?
    lazy var rows:[MultiSelectRow] = []
    private var allRows:[MultiSelectRow] = []
    
    // MARK: - Outlets
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var txtSearchBar: UISearchBar!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var tableView: UITableView!
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Filter"
        allRows = rows
        setUI()
        txtSearchBar.delegate = self
        setNorecord()
        
    }
    private func setUI() {
        switch pickerType {
        case .visit:
            lblTitle.text = "Select Visit Result"
        case .leads:
            lblTitle.text = "Select Leads"
        case .oppotunity:
            lblTitle.text = "Select Oppotunity"
        case .none:
            break
        }
    }
    private func setNorecord() {
        if rows.isEmpty {
            selectView.isHidden = true
            lblNoRecordFound.isHidden = false
        } else {
            lblNoRecordFound.isHidden = true
            selectView.isHidden = false
        }
    }
    // MARK: - Actions
    @IBAction func actionDone(_ sender: Any) {
        var selectedRows:[Int] = []
        
        for (index,row) in rows.enumerated() {
            if row.isSelected{
                selectedRows.append(index)
            }
        }
        
        if let doneAction = doneAction{
            doneAction(selectedRows, viewClicked)
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - UITableViewDataSource
extension FilterListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FilterListTableViewCell.self), for: indexPath) as! FilterListTableViewCell
        let row =  rows[indexPath.row]
        cell.lblTitle.text = row.title
        cell.imgSeletion.isHidden = row.isSelected ? false : true
        switch pickerType {
        case .visit:
            cell.imgView.image = #imageLiteral(resourceName: "redio_2")
        case .leads:
            cell.imgView.image = #imageLiteral(resourceName: "comment_CRM")
        case .oppotunity:
            cell.imgView.image = #imageLiteral(resourceName: "comment_CRM")
        case .none:
            break
        }
        return cell
    }
}
// MARK: - UITableViewDelegate
extension FilterListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var row =  rows[indexPath.row]
        row.isSelected = !row.isSelected
        rows[indexPath.row] = row
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

// MARK:- SearchBarDelegate
extension FilterListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            rows = allRows
        } else {
            rows = allRows.filter({ (row) -> Bool in
                return row.title.localizedCaseInsensitiveContains(searchText)
            })
        }
        setNorecord()
        tableView.reloadData()
    }
}


