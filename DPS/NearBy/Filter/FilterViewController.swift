//
//  FilterViewController.swift
//  DPS
//
//  Created by Vivek Patel on 06/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  2021

import UIKit
import TagListView

protocol MapFilterDelegate: NSObjectProtocol {
    func refreshData(_ isLocationChange: Bool, nearbyFilterData: NearbyFilterData)
}

final class FilterViewController: BaseViewController {
    // MARK: - Variables
    private lazy var presenter = {
        return FilterPresenter(self)
    }()
    
    lazy var selectedVisits: [VisitStatus] = [] {
        didSet {
            self.lblSelectVisit.isHidden = !self.selectedVisits.isEmpty
        }
    }
    lazy var selectedLeads: [WebLeadStatus] = []{
        didSet {
            self.lblLeads.isHidden = !self.selectedLeads.isEmpty
        }
    }
    lazy var selectedOpportunities: [OpportunityStatus] = []{
        didSet {
            self.lblOpportunity.isHidden = !self.selectedOpportunities.isEmpty
        }
    }
    private var pickerListType: PickerListType?
    lazy private var locationService = LocationService.instance
    weak var delegate: MapFilterDelegate?
    var changes = false
    var isLocationChange = false
    var nearbyFilterData = NearbyFilterData()
    // MARK: - Outlets
    @IBOutlet var tagViews: [TagListView]!
    @IBOutlet weak var lblSelectVisit: UILabel!
    @IBOutlet weak var lblLeads: UILabel!
    @IBOutlet weak var switchAccount: UISwitch!
    @IBOutlet weak var switchOpportunity: UISwitch!
    @IBOutlet var images: [UIImageView]!
    @IBOutlet weak var txtCurrentLocation: UITextField!
    @IBOutlet weak var viewLeads: UIView!
    @IBOutlet weak var switchLeads: UISwitch!
    @IBOutlet weak var viewOpportunity: UIView!
    @IBOutlet weak var lblOpportunity: UILabel!
    @IBOutlet weak var txtRadius: UITextField!
    @IBOutlet weak var tagViewVisitStatus: TagListView!
    @IBOutlet weak var tagViewOpportunity: TagListView!
    @IBOutlet weak var tagViewLead: TagListView!
    
    
    @IBOutlet weak var view_Current: UIView!
    @IBOutlet weak var view_Custome: UIView!
    @IBOutlet weak var view_Assigned: UIView!

    @IBOutlet weak var img_Current: UIImageView!
    @IBOutlet weak var img_Custome: UIImageView!
    @IBOutlet weak var img_Assigned: UIImageView!
    
    @IBOutlet weak var heightTxtAddress: NSLayoutConstraint!

    // MARK: - View Lfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Filter"
        setupUIFromDefaultFilter()
        setTagUI()
        setFooterView()
        setUI()
        
        
        
    }
 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
        self.navigationController?.isNavigationBarHidden = false
       // self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            // self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    // MARK: - Actions
    

    
    
    @IBAction func actionOpportunity(_ sender: UISwitch) {
        viewOpportunity.isHidden = !sender.isOn
    }
    @IBAction func actionLeads(_ sender: UISwitch) {
        viewLeads.isHidden = !sender.isOn
    }
    @IBAction func actionCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionDone(_ sender: Any) {
        if txtCurrentLocation.text?.trimmed.count == 0{
            showAlert(message: "Address required!")
          } else{
            nearbyFilterData.isFilter = true

            if nearbyFilterData.leads != selectedLeads {
                changes = true
            }
            nearbyFilterData.leads = selectedLeads
            
            if nearbyFilterData.opportunities != selectedOpportunities {
                changes = true
            }
            nearbyFilterData.opportunities = selectedOpportunities
            
            if nearbyFilterData.visits != selectedVisits {
                changes = true
            }
            nearbyFilterData.visits = selectedVisits
            
            if nearbyFilterData.isLead != switchLeads.isOn{
                changes = true
            }
            nearbyFilterData.isLead = switchLeads.isOn ? true : false
            if nearbyFilterData.isOpportunity != switchOpportunity.isOn{
                changes = true
            }
            nearbyFilterData.isOpportunity = switchOpportunity.isOn
            if nearbyFilterData.isAccount != switchAccount.isOn {
                changes = true
            }
            nearbyFilterData.isAccount = switchAccount.isOn
            if nearbyFilterData.radius != Double(txtRadius.text ?? "0") ?? 0{
                changes = true
            }
            nearbyFilterData.radius = Double(txtRadius.text ?? "0") ?? 0
            
            self.navigationController?.popViewController(animated: true)
            
            changes = true
            
            if changes {
                if nearbyFilterData.isType == "Assigned" {
                    NotificationCenter.default.post(name: Notification.Name("FilerNotify"), object: nil, userInfo: nil)
                }
                self.delegate?.refreshData( isLocationChange, nearbyFilterData: nearbyFilterData)
            }
        }
      
    }
    
    @IBAction func actionselectVisit(_ sender: Any) {
        
        guard let status = AppUserDefaults.shared.leadDetailMaster?.visitsStatus else {
            showAlert(message: "No record found !!")
            return }
        var rows: [MultiSelectRow] = []
        for row in status {
            if let visitStatusName = row.visitStatusName {
                var isSelected = false
                if selectedVisits.contains(row) {
                    isSelected = true
                }
                let row = MultiSelectRow(title: visitStatusName, isSelected: isSelected)
                rows.append(row)
            }
        }
        showMultiSelectionPicker(sender: tagViewVisitStatus , rows: rows, pickerListType:.visit) { [self] (indexs, view) in
            self.tagViewVisitStatus.removeAllTags()
            self.selectedVisits.removeAll()
            for index in indexs{
                let value = status[index]
                self.selectedVisits.append(value)
            }
            self.tagViewVisitStatus.addTags(self.selectedVisits.compactMap({$0.visitStatusName}))
        }
    }
    @IBAction func actionShowLeads(_ sender: Any) {
        
        guard let leads = AppUserDefaults.shared.leadDetailMaster?.leadsStatus else {
            showAlert(message: "No record found !!")
            return }
        var rows: [MultiSelectRow] = []
        for row in leads {
            if let name = row.name {
                var isSelected = false
                if self.selectedLeads.contains(row) {
                    isSelected = true
                }
                let row = MultiSelectRow(title: name, isSelected: isSelected)
                rows.append(row)
            }
        }
        showMultiSelectionPicker(sender: tagViewLead , rows: rows, pickerListType:.leads) { (indexs, view) in
            self.tagViewLead.removeAllTags()
            self.selectedLeads.removeAll()
            for index in indexs{
                let value = leads[index]
                self.selectedLeads.append(value)
            }
            self.tagViewLead.addTags(self.selectedLeads.compactMap({$0.name}))
            
        }
    }
    @IBAction func actionShowOpportunity(_ sender: Any) {
        
        guard let opportunities = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus else {
            showAlert(message: "No record found !!")
            return
            
        }
        var rows: [MultiSelectRow] = []
        for row in opportunities {
            if let statusName = row.statusName {
                var isSelected = false
                if selectedOpportunities.contains(row) {
                    isSelected = true
                }
                let row = MultiSelectRow(title: statusName, isSelected: isSelected)
                rows.append(row)
            }
        }
        showMultiSelectionPicker(sender: tagViewOpportunity , rows: rows,  pickerListType:.oppotunity) { (indexs, view) in
            self.tagViewOpportunity.removeAllTags()
            self.selectedOpportunities.removeAll()
            for index in indexs {
                let value = opportunities[index]
                self.selectedOpportunities.append(value)
            }
            self.tagViewOpportunity.addTags(self.selectedOpportunities.compactMap({$0.statusName}))
        }
    }
    @IBAction func actionGetCurrentLocation(_ sender: Any) {
        txtCurrentLocation.text = ""
        setForCurrent()
    }
    @IBAction func actionGetCustomeLocation(_ sender: Any) {
        setForCustome()
        txtCurrentLocation.text = ""

    }
    @IBAction func actionAssignedLocation(_ sender: Any) {
        setForAssigned()
        txtCurrentLocation.text = ""
        var lat = 0.0 , long = 0.0
        if(nsud.value(forKey: "FilterLocationAssigned") != nil){
            if((nsud.value(forKey: "FilterLocationAssigned") as! NSDictionary).value(forKey: "Assigned") as! Bool){
                view_Assigned.isHidden = false
                let dict = nsud.value(forKey: "FilterLocationAssigned") as! NSDictionary
                lat = Double("\(dict.value(forKey: "latitude")!)")!
                long = Double("\(dict.value(forKey: "longitude")!)")!
                self.nearbyFilterData.userLocation.latitude = lat
                self.nearbyFilterData.userLocation.longitude = long
                self.presenter.apiCallForAddress(CLLocation(latitude: lat, longitude: long))

            }
        }

    }
    func setForCurrent() {
        getCurrentLocation()
        txtCurrentLocation.isUserInteractionEnabled = false
        heightTxtAddress.constant = 0.0
        img_Current.image = UIImage(named: "redio_button_2.png")
        img_Custome.image = UIImage(named: "redio_button_1.png")
        img_Assigned.image = UIImage(named: "redio_button_1.png")
        nearbyFilterData.isType = "Current"
    }
    func setForCustome() {
        heightTxtAddress.constant = 35.0
        txtCurrentLocation.isUserInteractionEnabled = true
        img_Current.image = UIImage(named: "redio_button_1.png")
        img_Custome.image = UIImage(named: "redio_button_2.png")
        img_Assigned.image = UIImage(named: "redio_button_1.png")
        nearbyFilterData.isType = "Custome"

    }
    func setForAssigned() {
        txtCurrentLocation.isUserInteractionEnabled = false
        heightTxtAddress.constant = 35.0
    

        img_Current.image = UIImage(named: "redio_button_1.png")
        img_Custome.image = UIImage(named: "redio_button_1.png")
        img_Assigned.image = UIImage(named: "redio_button_2.png")
        nearbyFilterData.isType = "Assigned"

    }
    
}
// MARK: - Conveinece
extension FilterViewController {
    
    private func setupUIFromDefaultFilter() {
        
        let filterData = self.nearbyFilterData
        switchAccount.isOn = filterData.isAccount
        switchLeads.isOn = filterData.isLead
        switchOpportunity.isOn = filterData.isOpportunity
        viewOpportunity.isHidden = !switchOpportunity.isOn
        viewLeads.isHidden = !switchLeads.isOn
        selectedVisits = filterData.visits
        selectedOpportunities  = filterData.opportunities
        selectedLeads = filterData.leads
        self.tagViewOpportunity.addTags(filterData.opportunities.compactMap({$0.statusName}))
        self.tagViewLead.addTags(filterData.leads.compactMap({$0.name}))
        self.tagViewVisitStatus.addTags(filterData.visits.compactMap({$0.visitStatusName}))
        self.txtRadius.text = filterData.radius.description
        
      
       
       //Check For Radio Buttion
        view_Assigned.isHidden = true
        var lat = 0.0 , long = 0.0
        if(nsud.value(forKey: "FilterLocationAssigned") != nil){
            if((nsud.value(forKey: "FilterLocationAssigned") as! NSDictionary).value(forKey: "Assigned") as! Bool){
                view_Assigned.isHidden = false
//                if(nearbyFilterData.isType == ""){
//                    nearbyFilterData.isType = "Assigned"
//                }
                let dict = nsud.value(forKey: "FilterLocationAssigned") as! NSDictionary
                lat = Double("\(dict.value(forKey: "latitude")!)")!
                long = Double("\(dict.value(forKey: "longitude")!)")!
              
            }
        }
        if(nearbyFilterData.isType == "Custome"){
            self.setForCustome()
            if let address = self.nearbyFilterData.userLocation.address {
                txtCurrentLocation.text = address
            } else {
                getCurrentLocation()
            }
            
        }
        else if(nearbyFilterData.isType == "Assigned"){
            
            self.setForAssigned()
            self.presenter.apiCallForAddress(CLLocation(latitude: lat, longitude: long))

        }else{
            if let address = self.nearbyFilterData.userLocation.address {
                txtCurrentLocation.text = address
            } else {
                getCurrentLocation()
            }
            self.setForCurrent()
        }
    }
    
    private func getCurrentLocation() {
        locationService.getLocation { location in
            guard let location = location else { return }
            self.nearbyFilterData.userLocation.latitude = location.coordinate.latitude
            self.nearbyFilterData.userLocation.longitude = location.coordinate.longitude
            self.changes = true
            self.presenter.apiCallForAddress(location)
            
        }
    }
    private func setUI() {
        for image in images {
            AppUtility.imageColor(image: image, color: .appDarkGreen)
        }
    }
    private func setTagUI() {
        for view in tagViews {
            view.delegate = self
            view.textFont = .systemFont(ofSize: 13)
            view.shadowRadius = 2
            view.shadowOpacity = 0.4
            view.tagBackgroundColor = .appThemeColor
            view.shadowColor = UIColor.black
            view.shadowOffset = CGSize(width: 1, height: 1)
            view.paddingX = 10
            view.paddingY = 10
            view.marginY = 15
            view.cornerRadius = 16
            view.alignment = .left
            view.removeIconLineColor = .white
            view.enableRemoveButton = true
            view.removeButtonIconSize = 10
            view.removeIconLineWidth = 2
        }
    }
    
}
// MARK: - UITextFieldDelegate
extension FilterViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCurrentLocation {
            let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
            
            let controller = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as! SearchPlaceViewController
           // controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            self.navigationController?.present(controller, animated: true, completion: nil)
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            let characterSet = CharacterSet(charactersIn: string)
            if textField == txtRadius {
                let repeatingDecimalCount = updatedText.filter{$0 == "."}.count
                let allowedCharacters = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "."))
                
                guard allowedCharacters.isSuperset(of: characterSet) && repeatingDecimalCount <= 1 else{
                    return false
                }
            }
        }
        return true
    }
}
// MARK: - TagListViewDelegate
extension FilterViewController: TagListViewDelegate {
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        
        guard let index = sender.tagViews.firstIndex(of: tagView) else {
            return
        }
        sender.removeTagView(tagView)
        if sender == tagViewVisitStatus {
            selectedVisits.remove(at: index)
        } else if sender == tagViewOpportunity {
            selectedOpportunities.remove(at: index)
        } else if sender == tagViewLead {
            selectedLeads.remove(at: index)
        }
    }
}


// MARK: - UpdateMapDelegate
extension FilterViewController: UpdateMapDelegate {
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        self.txtCurrentLocation.text = locationDetails.formattedAddress
        nearbyFilterData.userLocation.latitude = locationDetails.geometry.location.latitude
        nearbyFilterData.userLocation.longitude = locationDetails.geometry.location.longitude
        nearbyFilterData.userLocation.address = locationDetails.formattedAddress
        self.changes = true
        isLocationChange = true
    }
}
// MARK: - FilterView
extension FilterViewController: FilterView {
    func gotAddress(_ output: String) {
        txtCurrentLocation.text = output
        heightTxtAddress.constant = 35.0

        nearbyFilterData.userLocation.address = output
        self.changes = true
        //let location = Location(latitude: nearbyFilterData.userLocation.latitude ?? 0, longitude: nearbyFilterData.userLocation.longitude ?? 0)
        //let locationDetails = LocationResult(formattedAddress: output, geometry: Geometry(location: location), placeId: nil)
        isLocationChange = true
    }
}
