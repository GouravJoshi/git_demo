//
//  FilterListTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 06/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class FilterListTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgSeletion: UIImageView!{
        didSet {
            AppUtility.imageColor(image: imgSeletion, color: .black)
            
        }
    }
        @IBOutlet weak var imgView: UIImageView!
        override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        }
        
    }
