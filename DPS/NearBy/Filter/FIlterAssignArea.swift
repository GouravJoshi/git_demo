//
//  FIlterAssignArea.swift
//  DPS
//
//  Created by NavinPatidar on 3/30/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit


protocol AssignAreaFilter : class
{
    
    func refreshData(dictData : NSDictionary ,tag : Int)
    
}

class FIlterAssignArea: UIViewController {

    
    @IBOutlet weak var txtCurrentLocation: UITextField!
    lazy private var locationService = LocationService.instance
    weak var delegate: AssignAreaFilter?
    @IBOutlet weak var image: UIImageView!

    var latitude: Double?
    var longitude: Double?
    var address: String?
    private lazy var presenter = {
        return FilterPresenter(self)
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Filter"
        txtCurrentLocation.delegate = self
        AppUtility.imageColor(image: image, color: .appDarkGreen)
        if(latitude == nil || latitude == 0.0){
            getCurrentLocation()

        }
        txtCurrentLocation.text = self.address
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
        self.navigationController?.isNavigationBarHidden = false
       // self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            // self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    @IBAction func actionGetCurrentLocation(_ sender: Any) {
        getCurrentLocation()
    }
    @IBAction func actionCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionDone(_ sender: Any) {
     
        address = txtCurrentLocation.text!
        let dict = NSMutableDictionary()
        dict.setValue("\(self.latitude!)", forKey: "latitude")
        
        dict.setValue("\(self.longitude!)", forKey: "longitude")
        dict.setValue("\(self.address!)", forKey: "address")
        self.navigationController?.popViewController(animated: true)
        delegate?.refreshData(dictData: dict, tag: 0)
    }
    private func getCurrentLocation() {
        locationService.getLocation { [self] location in
            guard let location = location else { return }
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            self.presenter.apiCallForAddress(location)

        }
    }
}
// MARK: - UITextFieldDelegate
extension FIlterAssignArea: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCurrentLocation {
            let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
            
            let controller = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as! SearchPlaceViewController
           // controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            self.navigationController?.present(controller, animated: true, completion: nil)
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            let characterSet = CharacterSet(charactersIn: string)
//            if textField == txtRadius {
//                let repeatingDecimalCount = updatedText.filter{$0 == "."}.count
//                let allowedCharacters = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "."))
//                
//                guard allowedCharacters.isSuperset(of: characterSet) && repeatingDecimalCount <= 1 else{
//                    return false
//                }
//            }
        }
        return true
    }
}
// MARK: - UpdateMapDelegate
extension FIlterAssignArea: UpdateMapDelegate {
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        self.txtCurrentLocation.text = locationDetails.formattedAddress
        self.latitude = locationDetails.geometry.location.latitude
        self.longitude = locationDetails.geometry.location.longitude
        self.address = locationDetails.formattedAddress

    }
}
// MARK: - FilterView
extension FIlterAssignArea: FilterView {
    func showLoader() {
        
    }
    
    func hideLoader() {
        
    }
    
    func showAlert(message: String?) {
        
    }
    
    func gotAddress(_ output: String) {
        txtCurrentLocation.text = output
        self.address = output
      
    }
}
