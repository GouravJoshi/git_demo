//
//  ClearFilterViewController.swift
//  DPS
//
//  Created by Vivek Patel on 02/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
//MARK:- Variables
enum FilterDays: Int, CaseIterable {
    case today = 0
    case yesterday
    case lastWeek
    case lastMonth
    case thisMonth
    var title: String {
        switch self {
        case .today:
            return  "Today"
        case .yesterday:
            return  "Yesterday"
        case .lastWeek:
            return "Last Week"
        case .lastMonth:
            return "Last Month"
        case .thisMonth:
            return "This Month"
        }
    }
}
struct FilterSelection {
    var filterDays: FilterDays
    var isSelected = false
}

final class ClearFilterViewController: BaseViewController {
    //MARK: - View Life Cycle
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtfromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!
    //MARK: - Outlets
    var rows: [FilterSelection] = []
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        for day in FilterDays.allCases {
            rows.append(FilterSelection(filterDays: day))
        }
        self.title = "Filter"
        setFooterView()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func actionCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionClearData(_ sender: Any) {
        if !txtfromDate.text!.isEmpty || !txtToDate.text!.isEmpty {
            guard !txtfromDate.text!.isEmpty else {
                showAlert(message: "Please select from date")
                return
            }
            guard !txtToDate.text!.isEmpty else {
                showAlert(message: "Please select to date")
                return
            }
            let startDate = AppUtility.convertStringToDate(txtfromDate.text!)
            let endDate =  AppUtility.convertStringToDate(txtToDate.text!)
            guard startDate! <= endDate! else {
                showAlert(message: "From date should be less than or equal to To date")
                return
            }
        }
        
    }
    @IBAction func actionDone(_ sender: Any) {
    }
}
//MARK: - UITableViewDataSource
extension ClearFilterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FilterDays.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ClearFilterTableViewCell.self), for: indexPath) as! ClearFilterTableViewCell
        let row = rows[indexPath.row]
        cell.lblTitle.text = FilterDays(rawValue: indexPath.row)?.title
        if row.isSelected {
            cell.imgCheck.isHidden = false
        } else {
            cell.imgCheck.isHidden = true
        }
        // cell.imgCheck.isHidden = rows[indexPath.row].isSelected ? false : true
        return cell
    }
    
}
extension ClearFilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var row =  rows[indexPath.row]
        row.isSelected = !row.isSelected
        rows[indexPath.row] = row
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}
//MARK: - UITextFieldDelegate
extension ClearFilterViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtfromDate {
            showDatePicker(sender: txtfromDate, selectedDate: Date(), maximumDate: Date(), pickerType: .date) { (selectedDate, _) in
                let format = DateFormatter()
                format.dateFormat = "dd-MM-yyyy"
                let formattedDate = format.string(from: selectedDate)
                self.txtfromDate.text = formattedDate
            }
            return false
        } else if textField == txtToDate {
            
            showDatePicker(sender: txtToDate, selectedDate: Date(), maximumDate: Date(),  pickerType: .date) { (selectedDate, _) in
                let format = DateFormatter()
                format.dateFormat = "dd-MM-yyyy"
                let formattedDate = format.string(from: selectedDate)
                self.txtToDate.text = formattedDate
            }
            return false
        }
        return true
        
    }
}
