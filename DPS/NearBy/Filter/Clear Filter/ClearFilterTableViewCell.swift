//
//  ClearFilterTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 02/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ClearFilterTableViewCell: UITableViewCell {
//MARK: - Outlets
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
