//
//  FilterModel.swift
//  DPS
//
//  Created by Vivek Patel on 06/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct NearbyFilterData: Codable {
    var leads: [WebLeadStatus] = []
    var visits: [VisitStatus] = []
    var opportunities: [OpportunityStatus] = []
    var radius = 4.0
    var isAccount = false // before it was true changes as requested by udita madam
    var isLead = true
    var isOpportunity = true
    var userLocation = UserLocation()
    var isFilter = false
    var isType = ""
    init() {}
}

struct UserLocation: Codable {
    var latitude: Double?
    var longitude: Double?
    var address: String?
}
