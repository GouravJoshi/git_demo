//
//  FilterPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 07/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import UIKit

protocol FilterView: CommonView {
    func gotAddress(_ output: String)
}

class FilterPresenter: NSObject {
    weak private var delegate: FilterView?
    private lazy var service = {
        return MapService()
    }()
    
    init(_ delegate: FilterView) {
        self.delegate = delegate
    }
    
    
    func apiCallForAddress(_ location: CLLocation) {
        FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
        service.geocodeLocation(location) { (placemark, error) in
            FTIndicator.dismissProgress()
            if let placemark = placemark {
                
                let address = "\(placemark.subThoroughfare ?? "") \(placemark.thoroughfare ?? ""), \(placemark.locality ?? ""), \(placemark.administrativeArea ?? ""), \(placemark.postalCode ?? "")"
                self.delegate?.gotAddress(address)
                
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    
}

