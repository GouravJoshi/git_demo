//
//  SalesPersonTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 02/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class SalesPersonTableViewCell: UITableViewCell {
    //MARK: - Variables
    var person: EmployeeListD2D? {
        didSet {
            guard let person = person else { return }
            lblName.text = person.fullName
            lblPhone.text = person.primaryPhone
            lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
            lblEmail.text = person.primaryEmail
            viewEmail.isHidden = person.primaryEmail?.isEmpty ?? true
            viewPhone.isHidden = person.primaryPhone?.isEmpty ?? true
        }
    }
    //MARK: - Outlets
    @IBOutlet weak var viewEmail: UIStackView!
    @IBOutlet weak var viewAvailability: UIStackView!
    @IBOutlet weak var viewPhone: UIStackView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAvailability: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet var images: [UIImageView]! {
        didSet{
            for image in images {
                AppUtility.imageColor(image: image, color: .appThemeColor)
            }
        }
    }
    @IBOutlet weak var btnAssign: UIButton!
    @IBOutlet weak var lblName: UILabel!
    //MARK: - View Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
