//
//  AssignSalesPersonViewController.swift
//  DPS
//
//  Created by Vivek Patel on 02/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class AssignSalesPersonViewController: BaseViewController {
    //MARK:- Variables
    var rowsCount = 0
    var selectedEmployee: EmployeeListD2D?
    //  var employees: [Employee] = []
    var employeeList: [EmployeeListD2D] = [] {
        didSet {
            setUI()
            tableView?.reloadData()
        }
    }
    private var listOptions: ListOptions = .more {
        didSet {
            switch listOptions {
            case .more:
                lblMoreOptions.text = "More Options"
            default:
                lblMoreOptions.text = "Less Options"
            }
        }
    }
    private lazy var presenterGeo = {
        return GeoMappingPresenter(self)
    }()
    private lazy var presenter = {
        return ServiceSetupPresenter(self)
    }()
    var propertyDetails: PropertyDetails?
    var geofencingDetails: GeoFencingD2D?
    var delegate: MapViewNavigationDelegate?
    //MARK: - Outlets
    @IBOutlet weak var lblMoreOptions: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewMoreOption: UIView!
    @IBOutlet weak var arrowViewMore: UIImageView!
    //MARK: - View life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "GeoMapping"
        AppUtility.imageColor(image: arrowViewMore, color: .appDarkGreen)
        setEmployeeListToUserDefault()
        if let list = AppUserDefaults.shared.employeeList {
            employeeList = list
            setUI()
            apiCallForGetEmployees()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
    }
    private func apiCallForGetEmployees() {
        
        var params = EmployeeRequest()
        /*params.serviceAddress = entityDetail.leadDetail?.servicesAddress1
         params.departmentSysName = entityDetail.leadDetail?.departmentSysName
         params.lat = entityDetail.leadDetail?.latitude
         params.lng = entityDetail.leadDetail?.longitude*/
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        params.companyKey = companyKey
        params.radius = 50.0
        params.startDate = AppUtility.convertDateToString(Date(), toFormat: ConstantV.modifiedDateFormat)
        if let endDate
            = Calendar.current.date(byAdding: .day, value: 3, to: Date()) {
            params.endDate = AppUtility.convertDateToString(endDate, toFormat: ConstantV.modifiedDateFormat)
        } else {
            params.endDate = AppUtility.convertDateToString(Date(), toFormat: ConstantV.modifiedDateFormat)
        }
        params.isDoorToDoor = true

        presenter.apiCallForGetEmployees(params)
    }
    //MARK: - Actions
    @objc private func actionAssign(_ sender: UIButton) {
        guard var geofencingDetails = geofencingDetails  else { return }
        geofencingDetails.employeeId = employeeList[sender.tag].employeeId
        selectedEmployee = employeeList[sender.tag]
        presenterGeo.apiCallToAssignArea(geofencingDetails)
    }
    @objc private func actionOnCall(_ sender: UITapGestureRecognizer) {
        guard let row = sender.view?.tag else { return }
        if let number = employeeList[row].primaryPhone {
            actionCall(number: number)
        }
    }
    @objc private func actionOnEmail(_ sender: UITapGestureRecognizer) {
        guard let row = sender.view?.tag else { return }
        if let email = employeeList[row].primaryEmail {
            actionEmail(email: email)
        }
    }
    // MARK: - Action
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Outltes
    @IBAction func actionMoreOptions(_ sender: Any) {
        
        if listOptions == .more, employeeList.count > (rowsCount + 3) {
            rowsCount += 3
        } else if listOptions == .less, rowsCount - 3 > 5{
            rowsCount -= 3
        } else if listOptions == .more {
            rowsCount = employeeList.count
            listOptions = .less
        } else {
            listOptions = .more
            rowsCount = 5
        }
        tableView.reloadData()
    }
    private func setUI() {
        if employeeList.count <= 6 {
            rowsCount = employeeList.count
            viewMoreOption.isHidden = true
        } else {
            rowsCount = 6
            viewMoreOption.isHidden = false
        }
        tableView?.reloadData()
    }
    
}

//MARK: -UITableViewDataSource
extension AssignSalesPersonViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SalesPersonTableViewCell.self), for: indexPath) as! SalesPersonTableViewCell
        let person = employeeList[indexPath.row]
        cell.person = person
        cell.btnAssign.addTarget(self, action: #selector(actionAssign), for: .touchUpInside)
        let tap = UITapGestureRecognizer(target: self, action: #selector(actionOnCall))
        cell.viewPhone.tag = indexPath.row
        cell.viewPhone.addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(actionOnEmail))
        cell.viewEmail.tag = indexPath.row
        cell.viewEmail.addGestureRecognizer(tap2)
        cell.btnAssign.tag = indexPath.row
        return cell
    }
}
// MARK: - ServiceSetupView
extension AssignSalesPersonViewController: ServiceSetupView {
    func gotEmployees(_ output: [Employee]) {
        // self.employees = output
    }
    func gotRoutes(_ output: RouteResponse?) { }
}
// MARK: - GeoMappingView
extension AssignSalesPersonViewController: GeoMappingView {
    func gotAssignedArea(_ output: [GeoFencingD2D]?) {
        
    }
    func gotEmployeesList(_ output: [GeoFencingD2D]?) {
        
    }
    func areaAssigned(_ output: GeoFencingD2D) {
        /*let storyboard =  UIStoryboard.init(name: "Sales", bundle: nil)
         let controller = storyboard.instantiateViewController(withIdentifier: "AssignPopupViewController") as! AssignPopupViewController
         controller.modalPresentationStyle = .overFullScreen
         if let selectedEmployee = selectedEmployee {
         controller.employeeName = selectedEmployee.fullName
         }
         controller.delegate = self
         self.navigationController?.present(controller, animated: true, completion: nil)*/
        showAlertWithCompletion(message: "Assinged successfully") {
            self.delegate?.actionBack()
            self.navigationController?.popViewController(animated: true)
        }
    }
}
