//
//  AssignPopupViewController.swift
//  DPS
//
//  Created by Vivek Patel on 03/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class AssignPopupViewController: UIViewController {
    // MARK:- Variables
    var employeeName: String?
    var delegate: MapViewNavigationDelegate?
    // MARK:- Outlets
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var txtView: UITextView!
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblAssignTo.text = "Assign to"
       // guard let employeeName = employeeName else { return }
        //txtView.text = "\(employeeName)"
    }
    //MARK:- Actions
    @IBAction func actionSeeAll(_ sender: Any) {
    }
    @IBAction func actionSend(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.actionBack()
        }
    }
}
