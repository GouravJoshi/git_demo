//
//  DeclinePopupViewController.swift
//  DPS
//
//  Created by Vivek Patel on 17/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class DeclinePopupViewController: BaseViewController {
    //MARK:- Variables
    var selectedReason: CancelReasonDc?
    var sendAgreement = SendAgreement()
    var delegate:MapViewNavigationDelegate?
    private lazy var presenter = {
        return ServiceReportPresenter(self)
    }()
    
    //MARK:- Outlets
    @IBOutlet weak var txtReason: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnDissmiss: UIButton! {
        didSet {
            AppUtility.buttonImageColor(btn: btnDissmiss, image: "Group 771", color: .lightGray)
        }
    }
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    //MARK:- Actions
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        guard  !txtReason.text!.isEmpty else {
            showAlert(message: "Please select Reason")
            return }
        sendAgreement.cancelReasonMasterId = selectedReason?.cancelReasonMasterID
        sendAgreement.cancelReasonDescription = txtDescription.text ?? ""
        presenter.apiCallForSendMail(sendAgreement, isDecline:true)
    }
}
// MARK: - UITextFieldDelegate
extension DeclinePopupViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtReason {
            var rows:[String] = []
            var selectedRow = 0
            
            guard  let objects = AppUserDefaults.shared.masterDetails?.cancelReasonDc, !objects.isEmpty else {
                showAlert(message: "No record found !!")
                return false
            }
            let reasons = objects.filter({$0.type == "Opportunity"})
            guard !reasons.isEmpty else {
                showAlert(message: "No record found !!")
                return false
            }
            for (index,row) in reasons.enumerated() {
                if selectedReason?.cancelReasonMasterID == row.cancelReasonMasterID {
                    selectedRow = index
                }
                rows.append(row.cancelReason ?? "")
            }
            showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                textField.text = value
                self.selectedReason = reasons[index]
            }
            return false
        }
        return true
    }
}
extension DeclinePopupViewController: ServiceReportView {
    func uploadImage(type: UploadType, uploadResponse: UploadResponse) {
        
    }
    func mailSendSuccessfully(_ output: EntityDetail) {
        self.dismiss(animated: true) {
            self.delegate?.actionBack()
        }
    }
    func resendAgreementSuccessfully(_ output: Bool) {
        
    }
}
