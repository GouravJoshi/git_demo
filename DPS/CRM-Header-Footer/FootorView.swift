/**
 Copyright (c) 2020, OptiRisk India Pvt. Ltd. All rights reserved.
 This code is developed for TPMS mobile application.
*/
//        Saavan Patidar 2021    485938045908349

import UIKit

class FootorView: UIView {
    
    @IBOutlet var headerContentView: UIView!
  
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var onClickHomeButtonAction: (() -> Void)? = nil
    var onClickAccountButtonAction: (() -> Void)? = nil
    var onClickScheduleButtonAction: (() -> Void)? = nil
    var onClickSalesButtonAction: (() -> Void)? = nil

    
    @IBOutlet var btnDash: UIButton!
    @IBOutlet var btnSchedule: UIButton!
    @IBOutlet var btnSales: UIButton!
    @IBOutlet var btnAccount: UIButton!
    
    @IBOutlet var lblDash: UILabel!
    @IBOutlet var lblSchedule: UILabel!
    @IBOutlet var lblSales: UILabel!
    @IBOutlet var lblAccount: UILabel!
    
    @IBOutlet var lblIndicatorDash: UILabel!
    @IBOutlet var lblIndicatorSchedule: UILabel!
    @IBOutlet var lblIndicatorSales: UILabel!
    @IBOutlet var lblIndicatorAccount: UILabel!

    
    
    @IBOutlet weak var lblScheduleCount: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
   
    private func commonInit(){
        
        Bundle.main.loadNibNamed(DeviceType.IS_IPAD ? "FootorView_iPad" : "FootorView", owner: self, options: nil)
        addSubview(headerContentView)
        headerContentView.frame = self.bounds
        headerContentView.autoresizingMask = .flexibleHeight
        setBottomMenuOption()
    }
    
    func setBottomMenuOption() {
        createBadgeView()
        var strTitle = ""
        if(nsud.value(forKey: "DPS_BottomMenuOptionTitle") != nil){
            strTitle = "\(nsud.value(forKey: "DPS_BottomMenuOptionTitle")!)"
        }
        self.btnDash.tintColor = UIColor.lightGray
        self.lblDash.textColor = UIColor.lightGray
        self.btnSchedule.tintColor = UIColor.lightGray
        self.lblSchedule.textColor = UIColor.lightGray
        self.btnSales.tintColor = UIColor.lightGray
        self.lblSales.textColor = UIColor.lightGray
        self.btnAccount.tintColor = UIColor.lightGray
        self.lblAccount.textColor = UIColor.lightGray
        
 
        self.lblIndicatorDash.backgroundColor = UIColor.clear
        self.lblIndicatorSchedule.backgroundColor = UIColor.clear
        self.lblIndicatorSales.backgroundColor = UIColor.clear
        self.lblIndicatorAccount.backgroundColor = UIColor.clear
        
        switch strTitle {
     
        case "Home":
            self.btnDash.tintColor = hexStringToUIColor(hex: appThemeColor)
            self.lblDash.textColor = hexStringToUIColor(hex: appThemeColor)
            self.lblIndicatorDash.backgroundColor = hexStringToUIColor(hex: appThemeColor)

            break;
        case "Schedule":
            self.btnSchedule.tintColor = hexStringToUIColor(hex: appThemeColor)
            self.lblSchedule.textColor = hexStringToUIColor(hex: appThemeColor)
            self.lblIndicatorSchedule.backgroundColor = hexStringToUIColor(hex: appThemeColor)

            break;
        case "Sales":
            self.btnSales.tintColor = hexStringToUIColor(hex: appThemeColor)
            self.lblSales.textColor = hexStringToUIColor(hex: appThemeColor)
            self.lblIndicatorSales.backgroundColor = hexStringToUIColor(hex: appThemeColor)

            break;
        case "Account":
            self.btnAccount.tintColor = hexStringToUIColor(hex: appThemeColor)
            self.lblAccount.textColor = hexStringToUIColor(hex: appThemeColor)
            self.lblIndicatorAccount.backgroundColor = hexStringToUIColor(hex: appThemeColor)

            break;
        default:
            break;
        }
        
        
        
    }
    @IBAction func clickHome(_ sender: UIButton) {
        if self.onClickHomeButtonAction != nil{
            self.onClickHomeButtonAction!()
        }
    
    }
    @IBAction func clickSchedule(_ sender: UIButton) {
        print("Schedule")
        if self.onClickScheduleButtonAction != nil{
            self.onClickScheduleButtonAction!()
        }
    }
    @IBAction func clickSales(_ sender: UIButton) {
        print("Account")
        if self.onClickSalesButtonAction != nil{
            self.onClickSalesButtonAction!()
        }
    }
    @IBAction func clickAccount(_ sender: UIButton) {
        print("Account")
        if self.onClickAccountButtonAction != nil{
            self.onClickAccountButtonAction!()
        }
    }
    @IBAction func goBack(_ sender: Any) {
        self.appDelegate.navigationController?.popViewController(animated: true)
    }
    func createBadgeView() {
        lblScheduleCount.text = ""
        lblScheduleCount.layer.cornerRadius = DeviceType.IS_IPAD ? 18.0 : 12.0
        lblScheduleCount.layer.masksToBounds = true
        lblScheduleCount.backgroundColor = UIColor.clear
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                if(dictData.count != 0){
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount") ?? "")"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }
                    if strScheduleCount.count > 0 {
                        lblScheduleCount.text = strScheduleCount
                        lblScheduleCount.backgroundColor = UIColor.red

                    }
                    if strTaskCount.count > 0 {
                   
              
                    }
                    
                }
                
            }
            
        }
        
    }
  
    
}

