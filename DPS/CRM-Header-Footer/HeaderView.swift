/**
 Copyright (c) 2020, OptiRisk India Pvt. Ltd. All rights reserved.
 This code is developed for TPMS mobile application.
*///gjhfdhsafahfd

// Saavan Patidar Paidar ji ne bola ki code commit kro

import UIKit

class HeaderView: UIView ,UISearchBarDelegate{
    @IBOutlet var headerContentView: UIView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblCenter: UILabel!

    @IBOutlet weak var scrollTopMenu: UIScrollView!
    @IBOutlet weak var searchBarTask: UISearchBar!
    @IBOutlet weak var widthBackButton: NSLayoutConstraint!
    @IBOutlet weak var leadingSearchBar: NSLayoutConstraint!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var onClickScrollButton: (() -> Void)? = nil
    var onClickAddButton: (() -> Void)? = nil
    var onClickFilterButton: (() -> Void)? = nil
    var onClickTopMenuButton : (((String))  -> Void)? = nil
    var onClickBackButton: (() -> Void)? = nil
    var onClickAddButtonWithSender: ((_ sender: UIButton) -> Void)? = nil
    var onClickVoiceRecognizationButton: (() -> Void)? = nil

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    override func layoutSublayers(of layer: CALayer) {
       
    }
    @IBAction func clickAdd(_ sender: UIButton) {
        
        var strTitle = ""
        if(nsud.value(forKey: "DPS_TopMenuOptionTitle") != nil){
            strTitle = "\(nsud.value(forKey: "DPS_TopMenuOptionTitle")!)"
        }
        
        if(strTitle == "List"){
            
            self.onClickAddButtonWithSender!(sender)

        }else{
            
            self.onClickAddButton!()
            
        }

    }
    @IBAction func clickFilter(_ sender: UIButton) {
        if self.onClickFilterButton != nil{
            self.onClickFilterButton!()
        }
    
    }
    @IBAction func goVoiceRecognization(_ sender: Any) {
        if self.onClickVoiceRecognizationButton != nil{
            self.onClickVoiceRecognizationButton!()
        }
    }
    @IBAction func goBack(_ sender: Any) {
        if self.onClickBackButton != nil{
            self.onClickBackButton!()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
 
    private func commonInit(){
        Bundle.main.loadNibNamed(DeviceType.IS_IPAD ? "HeaderView_iPad" : "HeaderView", owner: self, options: nil)

        addSubview(headerContentView)
        headerContentView.frame = self.bounds
        //headerContentView.autoresizingMask = .flexibleHeight
        //headerContentView.autoresizingMask = .flexibleWidth
        headerContentView.autoresizingMask = [.flexibleHeight , .flexibleWidth]
        self.widthBackButton.constant = 0.0
        self.leadingSearchBar.constant = DeviceType.IS_IPAD ?  12.0 : 08.0
        setTopMenuOption()
    }
    
    func setTopMenuOption() {
        
        var strTitle = ""
        if(nsud.value(forKey: "DPS_TopMenuOptionTitle") != nil){
            strTitle = "\(nsud.value(forKey: "DPS_TopMenuOptionTitle")!)"
        }
   
        searchBarTask.placeholder = " Search \(strTitle.replacingOccurrences(of: "Assosi", with: ""))"
        if #available(iOS 13.0, *) {
            searchBarTask.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 14)
        } else {
            // Fallback on earlier versions
        }
        if let txfSearchField = searchBarTask.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
        }
        searchBarTask.layer.borderWidth = 0
        searchBarTask.layer.opacity = 1.0
        
        
        for item in self.scrollTopMenu.subviews {
            item.removeFromSuperview()
        }
        var xPoint = 20 ,xOffset = 10
        
        var arrayTopMenuOption = ["Lead", "Opportunity", "Map", "Tasks", "Activity", "View Schedule" , "Signed Agreements"]

      
        if(strTitle.contains("Assosi")){
            self.widthBackButton.constant = DeviceType.IS_IPAD ?  64.0 : 44.0
            self.leadingSearchBar.constant = DeviceType.IS_IPAD ?  74.0 : 50.0
            
            arrayTopMenuOption = ["\(strTitle.replacingOccurrences(of: "Assosi", with: ""))"]
        }else{
            if(strTitle == "Contacts" || strTitle == "Company"){
                arrayTopMenuOption = ["Contacts", "Company"]
            }
            
            if(strTitle == "List" || strTitle == "Calendar"){
                arrayTopMenuOption = ["List", "Calendar"]
            }
            self.widthBackButton.constant = 0.0
            self.leadingSearchBar.constant = DeviceType.IS_IPAD ?  12.0 : 08.0
        }
        
        /*if(arrayTopMenuOption.count == 2){
            let size = "\(strTitle)".size(withAttributes:[.font: UIFont.systemFont(ofSize:DeviceType.IS_IPAD ?  24.0 : 18.0)])

            xPoint = Int(self.lblCenter.frame.origin.x - (size.width + 10))
        }*/
        if(arrayTopMenuOption.count == 2){
                 let size = "\(strTitle)".size(withAttributes:[.font: UIFont.systemFont(ofSize:DeviceType.IS_IPAD ?  24.0 : 18.0)])
                 xPoint = Int(self.frame.width/2 - ((size.width) + 20))
        }
        if(arrayTopMenuOption.count == 1){
            let size = "\(strTitle)".size(withAttributes:[.font: UIFont.systemFont(ofSize:DeviceType.IS_IPAD ?  24.0 : 18.0)])
            xPoint = Int(self.frame.width/2 - ((size.width/2)-20))
            
        }
        
        for (index, element) in arrayTopMenuOption.enumerated() {
            print("\(index): \(element)")
            let button = UIButton()
            let lbl = UILabel()
            let size = "\(element)".size(withAttributes:[.font: UIFont.systemFont(ofSize:DeviceType.IS_IPAD ?  24.0 : 18.0)])
//            if(strTitle == "List" || strTitle == "Calendar" || strTitle == "Contacts" || strTitle == "Company"){
//
//                button.frame = CGRect(x: xPoint, y: 0, width: Int(size.width), height: Int(self.scrollTopMenu.frame.height))
//
//                lbl.frame = CGRect(x: xPoint-8, y: Int(button.frame.maxY - 5.0), width: Int(size.width) + 16, height: 5)
//            }else{
//
//
//            }
            button.frame = CGRect(x: xPoint, y: 0, width: Int(size.width + 10) , height: Int(self.scrollTopMenu.frame.height))
            lbl.frame = CGRect(x: xPoint-8, y: Int(button.frame.maxY - 5.0), width: Int(button.frame.width) + 15 , height: 5)
            
            button.setTitle("\(element)", for: .normal)
           // button.backgroundColor = UIColor.black
            button.titleLabel?.textColor = UIColor.white
            self.scrollTopMenu.addSubview(button)
        
            print(strTitle)
            print("-------\(element)")
            switch strTitle.replacingOccurrences(of: "Assosi", with: "") {
            case "\(element)":
                lbl.backgroundColor =  UIColor.white
                button.titleLabel?.font = UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 22 : 16)
                xOffset = xPoint

            default:
                lbl.backgroundColor = UIColor.clear
                button.titleLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 22 : 16)
            }
            lbl.layer.cornerRadius = 5.0
            xPoint = xPoint + Int(button.frame.width) + Int(DeviceType.IS_IPAD ? 60 : 40)

            self.scrollTopMenu.addSubview(lbl)
            button.addTarget(self, action: #selector(goToTopMenu(sender:)), for: .touchUpInside)
            self.scrollTopMenu.contentSize = CGSize(width: xPoint, height: Int(scrollTopMenu.frame.height))
            
        }
        if (arrayTopMenuOption.count > 2){
            self.scrollTopMenu.contentOffset.x = CGFloat(xOffset)
        }

        self.setNeedsDisplay()
      
    }
   
    @objc func goToTopMenu(sender : UIButton) {
        self.onClickTopMenuButton!((sender.titleLabel?.text!)!)
    }


}

extension String {
    func size(OfFont font: UIFont) -> CGSize {
        return (self as NSString).size(withAttributes: [NSAttributedString.Key.font: font])
    }
}
