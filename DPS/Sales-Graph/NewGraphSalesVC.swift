//
//  NewGraphSalesVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 21/08/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Changes.

import UIKit
import SwiftyXMLParser

enum UserAgent
{
    static let userAgentUrl = "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36"
}

class NewGraphSalesVC: UIViewController {

    // MARK: - ------------- Outlet -----------
    
    @IBOutlet weak var graphDrawingWebView: WKWebView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    var dispatchGroup = DispatchGroup()
    
    //Graph Legend

        @IBOutlet weak var viewGraphLegend: UIView!
        @IBOutlet weak var heightGraphLegend: NSLayoutConstraint!
        @IBOutlet weak var heightScrollGraphLegend: NSLayoutConstraint!


    
    // MARK: - ------------- Global Variable -----------

    @objc  var strLeadId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var isLoadedViewWillAppear = false
    var goBack = false
    var loader = UIAlertController()
     
         // MARK: - ------------- Variable -----------

    override func viewDidLoad() {
        
        self.view.isUserInteractionEnabled = false

        super.viewDidLoad()
        loadWebGraphView()
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if isLoadedViewWillAppear == false
        {
            isLoadedViewWillAppear = true
            loadWebGraphView()
            
        }
        
        showGraphLegend()

        self.view.isUserInteractionEnabled = true

    }
    
    override func viewDidDisappear(_ animated: Bool) {
    
        //isLoadedViewWillAppear = false
    }
    // MARK: ---------------------------------------showGraphLegend------------------------------------------

    func showGraphLegend() {
        //For graph Legend
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: { [self] in
            
            
            LegendGraphView().CreatFormLegend(viewLegend: viewGraphLegend, controllor: self) { viewContain  in
                
                let heightScroll = Int(viewContain.tag) + 50
              
                if getGraphLegendCategory().count != 0 {
                    self.heightGraphLegend.constant = 150.0
                  
                }else{
                    self.heightGraphLegend.constant = 0.0
                }
                self.heightScrollGraphLegend.constant = CGFloat(heightScroll)
                
               
            }

        })
        
    
    }
    // MARK: - ------------- Action -----------

    @IBAction func actionOnBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
        //loadWebGraphView()
    }
    
    @IBAction func actionOnSave(_ sender: Any)
    {
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)

        saveGraphAsXML()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) //0.3
        {
            self.goBack = true
            
            self.saveGraphAsXML()
            
            //print("----------------------- go back to global image view  called----------------------- ")
        }
        
//        self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
//            // All data downloaded
//
//            print("Notify main thread called")
//            DispatchQueue.main.async {
//                // Reload Table
//
//                Global().updateSalesModifydate(self.strLeadId as String)
//                self.dismiss(animated: false, completion: nil)
//                print("----------------------- go back to global image view  called----------------------- ")
//            }
//        })
        

//        Global().updateSalesModifydate(strLeadId as String)
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) //0.3
//        {
//            self.dismiss(animated: false, completion: nil)
//
//            print("----------------------- go back to global image view  called----------------------- ")
//        }

        
    }
    
    
    
    // MARK: - ------------- Other Function -----------

    // MARK: - ------------- Graph Function -----------
    func loadWebGraphView()
    {
        
        print("----------------------- loadWebGraphView called----------------------- ")
        
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"
        //self.graphDrawingWebView.navigationDelegate = self
        self.graphDrawingWebView.scrollView.bounces = false
        self.graphDrawingWebView.allowsBackForwardNavigationGestures = false
        
        //let xmlFileName = (self.strWoId as String) + ".xml"
        var isXmlFileExists = false
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "true"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlFileName = "\(dictData.value(forKey: "leadXmlPath") ?? "")"
                
            isXmlFileExists = checkIfImageExistAtPath(strFileName: xmlFileName)
                

        }
                
        let bundle = Bundle.main
        
        let filename = "index"
        let fileExtension = "html"
        let directory = "graphing"
        
        let isGraphDebugEnabled = nsud.bool(forKey: "graphDebug")
        if isGraphDebugEnabled {
            
            // general: For Sales Flow
            //catermite: For Termite Flow
            
            let url = URL(string: "https://graphing.z30.web.core.windows.net/index.html?selectedCategory=general")!
            
            //https://graphing.z30.web.core.windows.net/index.html
            //
            
            self.graphDrawingWebView.load(URLRequest(url: url))
            
        } else {
            
            let indexUrl = bundle.url(forResource: filename, withExtension: fileExtension, subdirectory: directory)
            
            let fullUrl = URL(string: "?selectedCategory=catermite", relativeTo: indexUrl)
            let request = URLRequest(url: fullUrl!)
            self.graphDrawingWebView.load(request)

        }
        
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"

        //self.graphDrawingWebView.scrollView.isScrollEnabled = false
        
        self.view.layoutIfNeeded()
        
        if isXmlFileExists {
            
            RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)
            
            self.loadSavedXMLGraph()
            
        }
        else
        {
            
            //RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)

            //loadGraphCategory()
            
        }
        
    }
    func loadSavedXMLGraph() {
        
        
        print("--------------- loadSavedXMLGraph called --------------------")
        
        do {
            
            var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "true"))
            
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "True"))
                
            }
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "1"))
                
            }

            //loadGraphCategory() // test commit
            
            if arrayOfImagesXML.count > 0 {
                
                let dictData = arrayOfImagesXML[0] as! NSManagedObject
                
                let xmlFileName = "\(dictData.value(forKey: "leadXmlPath") ?? "")"
                
                let path = (getDirectoryPath() as NSString).appendingPathComponent(xmlFileName)
                
                let xmlContents = try String(contentsOfFile: path)
                
                print(xmlContents)
                
                let xmlContentEdited = "'\(xmlContents.trimmingCharacters(in: .whitespacesAndNewlines))'"

                print(xmlContentEdited)
                
                let fileName = "'\(xmlFileName)'"
                
                //print("window.graphEditor.openGraphXML(\(xmlContentEdited), \(fileName))")
                
                graphDrawingWebView.evaluateJavaScript("window.graphEditor.openGraphXML(\(xmlContentEdited), \(fileName))")  { (result, error) in
                    guard error == nil else {
                        print("there was an error")
                        print(error!.localizedDescription)
                        print(error!)
                        return
                    }
                    
                }
                
            }
            
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func saveGraphAsXML() {
                
        print("--------------- saveGraphAsXML called---------------------- ")
        
        
        graphDrawingWebView.evaluateJavaScript("window.graphEditor.saveGraphXML()", completionHandler: { (results, error) in
            
            
            if results != nil {
                
                self.saveGraphToDocumentDirectoryAsXML(stringXML: results as! String)
                
                /*
                 let xml = try? XML.parse(results as! String)
                 let strcount =  xml?.mxGraphModel.root.mxCell.all?.count
                 let strBackGroundImage = xml?.mxGraphModel.attributes["backgroundImage"] ?? ""
                 
                 if(strcount ??  0 > 3 || strBackGroundImage.count > 3){
                     
                     self.saveGraphToDocumentDirectoryAsXML(stringXML: results as! String)
                     
                   
                 }else{
                     if(strcount == nil){
                         showAlertWithoutAnyAction(strtitle: Alert, strMessage: graphXmlParseMsg, viewcontrol: self)
                     }
             
                 }
                 */
                
            }
            
        })
        
    }
    
    func getDocumentsDirectory() -> URL {
           
           let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
           return paths[0]
           
       }
    
    func saveGraphToDocumentDirectoryAsXML(stringXML : String) {
        
        let str = stringXML
        
        //let xmlFileName = "\\Documents\\GraphXMLFiles\\XML"  + "Graph" + "\(getUniqueValueForId())" + ".xml"
        let xmlFileName = "\\Documents\\GraphXML\\XML"  + "Graph" + "\(strLeadId)\(getUniqueValueForId())" + ".xml"
        
        saveGraphXmlToImageDetail(graphXmlPath: xmlFileName)

        let filename = getDocumentsDirectory().appendingPathComponent(xmlFileName)
        
        do {
            try str.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
            print("failed to write.8 ")
        }
        
    }
    
    func saveGraphXmlToImageDetail(graphXmlPath : String)
    {
        
        deleteMainGraphImageBeforeSaving()

        let strImageName = "\\Documents\\UploadImages\\Img"  + "Graph" + "\(strLeadId)\(getUniqueValueForId())" + ".jpeg"
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadImagePath")
        arrOfKeys.add("leadImageType")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("leadImageId")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("leadId")
        arrOfKeys.add("leadImageCaption")
        arrOfKeys.add("descriptionImageDetail")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        
        arrOfKeys.add("isAddendum")
        arrOfKeys.add("isNewGraph")
        arrOfKeys.add("leadXmlPath")
        arrOfKeys.add("isImageSyncforMobile")

        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String() // EmployeeId
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            
            strUserName = "\(value)"
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId") {
            
            strEmployeeid = "\(value)"
            
        }
        
        let coordinate = Global().getLocation()
        let latitude = "\(coordinate.latitude)"
        let longitude = "\(coordinate.longitude)"
        
        let leadImageId = "Mobile" + "\(strLeadId)" + getUniqueValueForId()
        let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
        
        
        arrOfValues.add("") //createdBy
        arrOfValues.add("") //createdDate
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strImageName)
        arrOfValues.add("Graph")
        arrOfValues.add(strEmployeeid)
        arrOfValues.add(leadImageId) // woImageId
        arrOfValues.add(modifiedDate ?? "")
        arrOfValues.add(strLeadId)
        arrOfValues.add("") // imageCaption
        arrOfValues.add("") // imageDescription
        arrOfValues.add(latitude) // latitude
        arrOfValues.add(longitude) //  longitude
        arrOfValues.add("false") //  isAddendum
        arrOfValues.add("true") //  isNewGraph
        arrOfValues.add(graphXmlPath) //  leadXMLPath
        arrOfValues.add("false")

        // Fetch Image From WebView Graph
        
      /*  graphDrawingWebView.evaluateJavaScript("window.graphEditor.exportImage()", completionHandler: { (results, error) in
            
             //print(results ?? "results")
             //print("Results")
            
            self.getImageFromXMl(strImageName: strImageName)

            if results != nil {
                
                
            }
            
        })*/

        
//        saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        // Save Main Graph to service auto Also
        
        
        // Save Main Graph to sales auto Also
        let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
        
        let arrOfKeysSales = NSMutableArray()
        let arrOfValuesSales = NSMutableArray()
        
        arrOfKeysSales.add("createdBy")
        arrOfKeysSales.add("createdDate")
        arrOfKeysSales.add("companyKey")
        arrOfKeysSales.add("userName")
        arrOfKeysSales.add("leadImagePath")
        arrOfKeysSales.add("leadImageType")
        arrOfKeysSales.add("modifiedBy")
        arrOfKeysSales.add("leadImageId")
        arrOfKeysSales.add("modifiedDate")
        arrOfKeysSales.add("leadId")
        arrOfKeysSales.add("leadImageCaption")
        arrOfKeysSales.add("descriptionImageDetail")
        arrOfKeysSales.add("latitude")
        arrOfKeysSales.add("longitude")
        arrOfKeysSales.add("isAddendum")
        arrOfKeysSales.add("isNewGraph")
        arrOfKeysSales.add("leadXmlPath")
        arrOfKeysSales.add("isImageSyncforMobile")
        arrOfKeysSales.add("isInternalUsage")

        
        
        arrOfValuesSales.add("") //createdBy
        arrOfValuesSales.add("") //createdDate
        arrOfValuesSales.add(strCompanyKey)
        arrOfValuesSales.add(strUserName)
        arrOfValuesSales.add(strImageNameSales)
        arrOfValuesSales.add("Graph")
        arrOfValuesSales.add(strEmployeeid)
        arrOfValuesSales.add(leadImageId) // leadImageId
        arrOfValuesSales.add(modifiedDate ?? "")
        arrOfValuesSales.add(strLeadId)
        arrOfValuesSales.add("") // imageCaption
        arrOfValuesSales.add("") // imageDescription
        arrOfValuesSales.add(latitude) // latitude
        arrOfValuesSales.add(longitude) //  longitude
        arrOfValuesSales.add("false") //  isAddendum
        arrOfValuesSales.add("true") //  isNewGraph
        arrOfValuesSales.add(graphXmlPath) //  leadXMLPath
        arrOfValuesSales.add("false")
        arrOfValuesSales.add("false")
        
        
        
        // Fetch Image From WebView Graph
        
        /*graphDrawingWebView.evaluateJavaScript("window.graphEditor.exportImage()", completionHandler: { (results, error) in
            
             //print(results ?? "results")
             //print("Results")
            
            self.getImageFromXMl(strImageName: strImageNameSales)

            if results != nil {
                
                
            }
            
        })*/
        
        self.getImageFromXMl(strImageName: strImageNameSales)
        
        saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeysSales, arrayOfValue: arrOfValuesSales)
        
    }
    
    func getImageFromXMl(strImageName : String) {
        
        print("---------------- getImageFromXMl called-------------------")
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"
        
        // self.graphDrawingWebView.evaluateJavaScript("window.graphEditor.convertToImage(\(false))", completionHandler: { (results, error) in //ear;ier

        self.graphDrawingWebView.evaluateJavaScript("window.graphEditor.convertToImage(\(true))", completionHandler: { (results, error) in
            
            
            
            //print(results ?? "results")
            //print("Results")
            
            if results != nil {
                
                var strBase64Img = String()
                
                strBase64Img = results as! String
                
                if strBase64Img == "Not able to save image." {
                    
                    //
                    
                }else{
                    
                    strBase64Img = strBase64Img.replacingOccurrences(of: "data:image/jpeg;base64,", with: "")
                    
                    if strBase64Img == "/9j/4AAQSkZJRgABAQAASABIAAD/4QBMRXhpZgAATU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAAaADAAQAAAABAAAAAQAAAAD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUHYzZjwCyBOmACZjs+EJ+/8AAEQgAAQABAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/bAEMAAgICAgICAwICAwUDAwMFBgUFBQUGCAYGBgYGCAoICAgICAgKCgoKCgoKCgwMDAwMDA4ODg4ODw8PDw8PDw8PD//bAEMBAgICBAQEBwQEBxALCQsQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEP/dAAQAAf/aAAwDAQACEQMRAD8A+wKKKK/rQ/mM/9k=" {
                        
                        // No Image Added for Graph so delete Image Detail Obj from DB
                        
                        self.deleteMainGraphImageBeforeSaving()
                        
                    }else{
                        
                        let dataDecoded:NSData = NSData(base64Encoded: strBase64Img, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
                        
                        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
                        
                        //let imageResized = Global().resizeImageGloballl(decodedimage)
                        
                     //Temp Comment 25 Aug   //saveImageDocumentDirectory(strFileName: strImageName, image: decodedimage)
                        
                      
                        
                        // Save Image To Sales Auto Also
                        
                        let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
                        
                        saveImageDocumentDirectory(strFileName: strImageNameSales, image: decodedimage)
                        
                        print("----------------------- Save image to document directory called----------------------------")
                        
                        //self.dispatchGroup.leave()
                        
//                        DispatchQueue.main.async {
//                            // Reload Table
//
//                            self.dispatchGroup.leave()
//                        }
                        
                        
                        Global().updateSalesModifydate(self.strLeadId as String)
                        
                        DispatchQueue.main.async {
                            // Reload Table

                            //self.dismiss(animated: false, completion: nil)

                            //print("----------------------- go back to global image view  called----------------------- ")
                            
                            if self.goBack
                            {
                                self.loader.dismiss(animated: true)
                                {
                                    self.dismiss(animated: false, completion: nil)
                                    self.goBack = false
                                }
                            }
                        }

//                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) //0.3
//                        {
//                            self.dismiss(animated: false, completion: nil)
//
//                            print("----------------------- go back to global image view  called----------------------- ")
//                        }

                        
                    }
                    
                }
                
            }
            
        })
        
    }
    
    func deleteMainGraphImageBeforeSaving() {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0
        {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlImageName = "\(dictData.value(forKey: "leadImagePath") ?? "")"

            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && isNewGraph == %@", self.strLeadId,"true"))
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && isNewGraph == %@", self.strLeadId,"True"))
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && isNewGraph == %@", self.strLeadId,"1"))
            
            Global().removeImage(fromDocumentDirectory: xmlImageName)
            
            let strImageNameSales = xmlImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
            
            Global().removeImage(fromDocumentDirectory: strImageNameSales)
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strLeadId,strImageNameSales))

            // jugad for deleting sales image if synced from web
            
            let strNameTemp = "UploadImages\\" + strImageNameSales
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strLeadId,strNameTemp))
            
            
            // one more jugad for deleting sales image if synced from web Nilind
                       
            let strNameTempNew = xmlImageName.replacingOccurrences(of: "UploadImages\\", with: "")
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strLeadId,strNameTempNew))
            


        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewGraphSalesVC: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished navigating to url \(String(describing: webView.url))" )

        webView.evaluateJavaScript("myFunction()") { (any, error) in
            dump(error)
            print(any)
        }

    }
    
    
}

