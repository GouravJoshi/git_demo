


//
//  BeginAuditView.m
//  AuditApp
//
//  Created by Rakesh Jain on 06/02/14.
//  Copyright (c) 2014 Rakesh Jain. All rights reserved.
//

#import "BeginAuditView.h"
#import "ArticleCell.h"
#import "DropDownCellDps.h"
//#import "JSONKit.h"
#import "AppDelegate.h"
#import "TSActionSheet.h"
#import "TSPopoverController.h"
#import "zoomPopup.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import <SafariServices/SafariServices.h>

#import "Header.h"
#import "Global.h"
#import "SendMailViewController.h"
#import "ServiceSendMailViewController.h"
#import "SalesAutomationAgreementProposal.h"
#import "AppointmentView.h"
#import "AllImportsViewController.h"
#import "DPS-Swift.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 173;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 0;

@interface BeginAuditView ()
{
    NSString *strGGQCompanyKey,*strSalesUrlMain,*strServiceUrlMain;
    Global *global;
}

@end

@implementation BeginAuditView
@synthesize activeTitle,activeTag,popoverController,arrayTblDis,arrayTblPrio,arayData,btnActiveFindingImage,tblView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView){
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:15.0];
        titleView.shadowColor = [UIColor clearColor];
        titleView.textColor = [UIColor clearColor]; // Change to desired color
        self.navigationItem.titleView = titleView;
    }
    titleView.text = title;
    [titleView sizeToFit];
}
-(void)viewDidLoad{
    
    [btnPrev setHidden:YES];
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strGGQCompanyKey=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.GGQCompanyKey"]];
    
    strSalesUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    //  [self submittingSuveyStatus];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection and relaunch the application" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertt show];
        alertt.tag=5;
        [activity stopAnimating];
    }
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [imgAds setUserInteractionEnabled:YES];
    [imgAds addGestureRecognizer:singleTap];
    
    
    // AppDelegate  *appD =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
    [self setTitle:@"Begin Audit"];
    isMarkAsDonePresed=NO;
    isRadioBtn=NO;
    RbiIndex=-1;
    CbIIndex=-1;
    TfiIndex=-1;
    TviIndex=-1;
    DdiIndex=-1;
    noneIindex = -1;
    QCount=0;
    checkBoxCount=0;
    isCompleted=NO;
    isSendPressed=NO;
    isCheckBox=NO;
    isRadioBtn=NO;
    radioNum=0;
    indexradio=0;
    sIndex=-1;
    CheckComplition=0;
    isChanged=NO;
    isNextQ=NO;
    isSkipQ=NO;
    arrayDataStore = [[NSMutableArray alloc] init];
    StringDescription=[[NSString alloc]init];
    arr=[[NSMutableArray alloc]init];
    arrayHeight = [[NSMutableArray alloc] init];
    [super viewDidLoad];
    scrollView.delegate=self;
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithTitle:@"Setting" style:UIBarButtonItemStylePlain target:self action:@selector(gotoSetting)];
    [rightBtn setImage:[UIImage imageNamed:@"services.png"]];
    rightBtn.tintColor = [UIColor clearColor];
    self.navigationItem.rightBarButtonItem = rightBtn;
    arrayQid = [[NSMutableArray alloc] init];
    arrayDataTemp=[[NSMutableArray alloc] init];
    arrayArticles = [[NSMutableArray alloc] init];
    arraySliderValue = [[NSMutableArray alloc] init];
    arrayFindings = [[NSMutableArray alloc] init];
    arrayTblPrio= [[NSMutableArray alloc] init];
    arrayTblDis = [[NSMutableArray alloc] init];
    arrayPriority = [[NSMutableArray alloc] initWithObjects:@"High",@"Medium",@"Low" ,nil];
    arrayCBAns=[[NSMutableArray alloc]init];
    arayData = [[NSMutableArray alloc] init];
    dicQuestion=[[NSMutableDictionary alloc]init];
    radioDictObjectKey=[[NSMutableDictionary alloc]init];
    arrayTitles = [[NSMutableArray alloc] init];
    arrayFindId =[[NSMutableArray alloc] init];
    arrayGroupName = [[NSMutableArray alloc] init];
    dic=[[NSMutableDictionary alloc]init];
    arrayNoneBool = [[NSMutableArray alloc] init];
    [activity setHidden:NO];
    [activity startAnimating];
    [DejalBezelActivityView activityViewForView:self.view];
    arrayViews = [[NSMutableArray alloc] init];
    arrayFindImages = [[NSMutableArray alloc] init];
    ratingindex=-1;
    tesint =0;
    isRemovedArticle=NO;
    swipCount=0;
    
    tblView.frame = CGRectMake(tblView.frame.origin.x, tblView.frame.origin.y, tblView.frame.size.width, tblView.frame.size.height+82);
    tblView.dataSource=self;
    tblView.delegate=self;
    
    tblView.scrollEnabled=NO;
    buttonsY=btnNext.frame.origin.y;
    responseData = [[NSMutableData alloc] init];
    boolCount=0;
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    aId=[def valueForKey:@"SurveyID"];
    // aId=@"80519";
    NSString *urlStr=[NSString stringWithFormat:@"%@key=questionList&aid=%@",UrlMainGGQ,aId];
    
    NSLog(@"url on load %@",urlStr);
    NSString *properlyEscapedURL = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:properlyEscapedURL]];
    //[NSURLConnection connectionWithRequest:request delegate:self]  ;
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    [theConnection start];
    
    [scrollView setFrame:CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height)];
    self.navigationController.navigationBarHidden=YES;
    
    UISwipeGestureRecognizer  *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(Previous:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    // [self.view addGestureRecognizer:swipeRight];
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(next:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [btnMarkAsdone setHidden:NO];
    UITapGestureRecognizer *messagesTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messagesBucketTap)];
    [messagesTap setDelegate:self];
    [messagesTap setNumberOfTapsRequired:1];
    //[messagesTap setNumberOfTouchesRequired:2];
    [scrollView addGestureRecognizer:messagesTap];
    
    isFromImage=NO;
}
-(void)tapDetected{
    
    
    
}
- (void)openLinkNew:(NSString *)url {
    NSURL *URL = [NSURL URLWithString:url];
    
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = (id)self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:URL]) {
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}

-(void)messagesBucketTap
{
    [self.view endEditing:NO];
}
-(void)viewDidDisappear:(BOOL)animated
{
    isNextQ=NO;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    mandCount=0;
    mantCountForCheck=0;
    if ((isFromImage=YES)) {
        swipCount=countSwipe;
        isFromImage=NO;
    } else {
        swipCount=0;
    }
    
    // [self performSelector:@selector(stopActivity) withObject:nil afterDelay:2.0];
}
-(void)stopActivity
{
    [activity stopAnimating];
    [activity setHidesWhenStopped:YES];
    [DejalBezelActivityView removeView];
}
-(void)afterCompletingSurvey{
    
    NSUserDefaults *defsIsService=[NSUserDefaults standardUserDefaults];
    
    BOOL isServiceSurvey=[defsIsService boolForKey:@"isServiceSurvey"];
    
    if (isServiceSurvey)
    {
        
        NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
        [defsss setBool:YES forKey:@"isFromSurveyToSendEmail"];
        [defsss synchronize];
        
        BOOL isMechanicalSurvey=[defsIsService boolForKey:@"isMechanicalSurvey"];
        
        if (isMechanicalSurvey) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            SendMailMechanicaliPadViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SendMailMechanicaliPadViewController"];
            objByProductVC.strFromWhere=@"S";
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        } else {
            
            
            if ([_isFromSendEmail  isEqual: @"true"])
            {
                [self.navigationController popViewControllerAnimated:false];
            }
            else
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                                         bundle: nil];
                ServiceSendMailViewController
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewController"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
            
        }
        
    }
    else
    {
        
        NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
        [defsss setBool:YES forKey:@"isFromSurveyToSendEmail"];
        [defsss synchronize];
    
        BOOL isNewSalesSurvey=[defsIsService boolForKey:@"isNewSalesSurvey"];
        
        if (isNewSalesSurvey)
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesNew_SendEmailVC"
                                                                     bundle: nil];
            SalesNew_SendEmailVC
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SalesNew_SendEmailVC"];
            objByProductVC.strForSendProposal = _strStageType;
            [self.navigationController pushViewController:objByProductVC animated:NO];
        }
        else  //Old Sales Survey
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMainSelectService"
                                                                     bundle: nil];
            SendMailViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SendMailViewController"];
            objByProductVC.strForSendProposal = _strStageType;
            [self.navigationController pushViewController:objByProductVC animated:NO];
        }
        
       
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1) {
        if (buttonIndex==0) {
            
            [self afterCompletingSurvey];
            
            //            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
            //                                                                     bundle: nil];
            //            DashBoardView
            //            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardView"];
            //            [self.navigationController pushViewController:objByProductVC animated:NO];
        }
        else if (buttonIndex==1) {
            
            [self afterCompletingSurvey];
            
            //            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
            //                                                                     bundle: nil];
            //            DashBoardView
            //            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardView"];
            //            [self.navigationController pushViewController:objByProductVC animated:NO];
        }
        
    }
    
    else if (alertView.tag==2)
    {
        if (buttonIndex==0) {
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
        else{
            [self showImage:nil];
        }
    }
    else if (alertView.tag==5)
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"DashBoard"
                                                                 bundle: nil];
        DashBoardNew_iPhoneVC
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    }
}
#pragma mark Connection Methohs
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [responseData setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [responseData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    UIAlertView *alertt = [[UIAlertView alloc]
                           initWithTitle:@"Error"
                           message:@"Please check your network connection and relaunch the application"
                           delegate:self
                           cancelButtonTitle:@"Dismiss"
                           otherButtonTitles:nil, nil];
    [alertt show];
}
-(void)submittingSuveyStatus{//SendSurveyStatus
    //Yaha Par Survey Status Bhejna hai
    // NSDictionary *ResponseDict;
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSString *strLeadId=[defsLead valueForKey:@"LeadId"];
    // aId
    
    
    
    NSUserDefaults *defsIsService=[NSUserDefaults standardUserDefaults];
    
    BOOL isServiceSurvey=[defsIsService boolForKey:@"isServiceSurvey"];
    
    NSString *strUrl;
    
    if (isServiceSurvey)
    {
        
        [global fetchWorkOrderFromDbToUpdateSurveyStatus:strLeadId :@"Complete"];
        
        BOOL isMechanicalSurvey=[defsIsService boolForKey:@"isMechanicalSurvey"];
        
        if (isMechanicalSurvey) {
            
            strUrl=[NSString stringWithFormat:@"%@%@WorkorderId=%@&SurveyId=%@&SurveyStatus=%@",strServiceUrlMain,UrlUpdateServiceSurveyStatus,strLeadId,aId,@"Complete"];
            
        } else {
            
            strUrl=[NSString stringWithFormat:@"%@%@WorkorderId=%@&SurveyId=%@&SurveyStatus=%@",strServiceUrlMain,UrlUpdateServiceSurveyStatus,strLeadId,aId,@"Complete"];
            
        }
        
    }
    else
    {
        
        [global fetchLeadFromDbToUpdateSurveyStatus :strLeadId :@"Complete" :_strStageType];
        
        strUrl=[NSString stringWithFormat:@"%@%@leadId=%@&SurveyId=%@&SurveyStatus=%@",strSalesUrlMain,UrlUpdateSalesSurveyStatus,strLeadId,aId,@"Complete"];
        
    }
    
    NSLog(@"Survey Status Submitting URL===%@",strUrl);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"SendSurveyStatus" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     //                     NSString *returnString =[response valueForKey:@"ReturnMsg"];
                     //                     if ([returnString isEqualToString:@"Success"]) {
                     //                         [global AlertMethod:Info :SuccessMailSend];
                     //                     } else {
                     //                         [global AlertMethod:Info :SuccessMailSend];
                     //                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(isMarkAsDonePresed){
        NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
        [boolUserDefaults setBool:NO forKey:@"isSurvey"];
        // [boolUserDefaults setValue:nil forKey:@"AuditID"];
        [boolUserDefaults synchronize];
        NSError  *error;
        NSMutableDictionary *dictt = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                                     options:kNilOptions
                                                                       error:&error];
        
        NSString *strTitle =[NSString stringWithFormat:@"Audit Score # %d",[[[dictt valueForKey:@"result"] objectAtIndex:0] intValue]]; //[[dic valueForKey:@"result"] objectAtIndex:0];
        UIAlertView *alertt = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Survey submitted successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alertt.tag=1;
        [alertt show];
        [self afterCompletingSurvey];
        [self submittingSuveyStatus];
        
        if (![[dict valueForKey:@"type"] isEqualToString:@"Customer Survey"]){
            // [self uploadImage];
        }
    }
    else
    {
        if (isSendPressed) {
            NSArray *array = [self.navigationController viewControllers];
            [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
            if (![[dict valueForKey:@"type"] isEqualToString:@"Customer Survey"]){
                //   [self uploadImage];
            }
        }
        else
        {
            NSError  *error;
            dict = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                   options:kNilOptions
                                                     error:&error];
            if ([[dict valueForKey:@"type"] isEqualToString:@"Customer Survey"]) {
                [btnSaveProgress setTitle:@"Cancel" forState:UIControlStateNormal];
                [btnMarkAsdone setTitle:@"Submit" forState:UIControlStateNormal];
            }
            else{
                isCompleted=YES;
            }
            if ([dict count]>0) {
                @try {
                    if ([[dict valueForKey:@"finds"] valueForKey:@"desc"] ==[NSNull class]) {
                    }
                    else
                    {
                        @try {
                            for (int j=0; j<[[[dict valueForKey:@"finds"] valueForKey:@"desc"] count]; j++) {
                                
                                tblView.frame = CGRectMake(tblView.frame.origin.x, tblView.frame.origin.y, tblView.frame.size.width, tblView.frame.size.height+82);
                                NSString *strdisc=   [[[dict valueForKey:@"finds"] valueForKey:@"desc"] objectAtIndex:j];
                                NSString *strfindId=[NSString stringWithFormat:@"%d",[[[[dict valueForKey:@"finds"] valueForKey:@"id"] objectAtIndex:j]intValue]];//[[[[dict valueForKey:@"finds"] valueForKey:@"id"] objectAtIndex:j]intValue];
                                // [arrayFindId addObject:[[[dict valueForKey:@"finds"] valueForKey:@"id"] objectAtIndex:j]];
                                NSString *strPriority=[[[dict valueForKey:@"finds"] valueForKey:@"Pty"] objectAtIndex:j];
                                
                                NSString *strWebImageName=[[[dict valueForKey:@"finds"] valueForKey:@"fimg"] objectAtIndex:j];
                                NSString *strImageName = [NSString stringWithFormat:@"img%@%d.jpg",aId,j+600];
                                
                                NSMutableDictionary *dictFind = [NSMutableDictionary dictionaryWithObjectsAndKeys:strdisc,@"description",strPriority,@"Priority",strfindId,@"fID",strWebImageName,@"FImg",@"0",@"fIndex",nil];
                                //update finddict from server remaining......
                                
                                [arrayTblDis addObject:dictFind];
                                NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                
                                // If you go to the folder below, you will find those pictures
                                
                                
                                // NSLog(@"saving png");
                                NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@",docDir,strImageName];
                                BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pngFilePath];
                                if (!fileExists) {
                                    [self saveData:strWebImageName :strImageName];
                                    
                                }
                                
                            }
                        }
                        @catch (NSException *exception) {
                            
                        }
                        @finally {
                            
                        }
                    }
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                }
            }
            if ([[dict valueForKey:@"status"] isKindOfClass:[NSArray class]]) {
                
                UIAlertView *alerts = [[UIAlertView alloc] initWithTitle:@"No Questions found!" message:@"No questions availibe for you currently" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alerts show];
            }
            else
            {
                if ([[dict valueForKey:@"status"] isEqualToString:@"InProgress"]) {
                    
                    isCompleted=YES;
                    
                    @try {
                        
                        
                        for(int i =0 ;i<[[dict valueForKey:@"qList"] count];i++)
                        {
                            @try {
                                
                                [arrayGroupName addObject:[[[dict objectForKey:@"qList"] valueForKey:@"GroupName"] objectAtIndex:i]];
                                lblQCategory.text=[arrayGroupName objectAtIndex:swipCount];
                                NSString *strTypee =[[[dict objectForKey:@"qList"] valueForKey:@"type"] objectAtIndex:i];
                                NSString *strTitle =[[[dict objectForKey:@"qList"] valueForKey:@"title"] objectAtIndex:i];
                                NSString *strId =[[[dict objectForKey:@"qList"] valueForKey:@"id"] objectAtIndex:i];
                                NSString *strValue=[[[dict objectForKey:@"qList"] valueForKey:@"value"] objectAtIndex:i];
                                NSString *strLabels=[[[dict objectForKey:@"qList"] valueForKey:@"lables"] objectAtIndex:i];
                                NSString *strRating = [[[dict objectForKey:@"qList"] valueForKey:@"rating"] objectAtIndex:i];
                                BOOL strYesNo=[[[[dict objectForKey:@"qList"] valueForKey:@"IsYesNo"] objectAtIndex:i] boolValue];
                                NSString *strWatage =[[[dict objectForKey:@"qList"] valueForKey:@"wg"] objectAtIndex:i];
                                NSString *stringBool;
                                [arrayTitles addObject:strTitle];
                                lblQno.text = [NSString stringWithFormat:@"Question: %d/%lu",swipCount+1,(unsigned long)[arrayTitles count]];
                                if (strYesNo) {
                                    stringBool=@"Yes";
                                }
                                else
                                {
                                    stringBool=@"No";
                                    
                                }
                                
                                // IsYesNo
                                NSDictionary *dictTempt = [NSDictionary dictionaryWithObjectsAndKeys:strTypee,@"type",strTitle,@"title",strId,@"id",strValue,@"value",strLabels,@"labels",stringBool,@"isyesNo",strRating,@"rating",strWatage,@"w", nil];
                                
                                
                                [arrayDataStore addObject:dictTempt];
                                
                            }
                            @catch (NSException *exception) {
                                
                            }
                            @finally {
                                
                            }
                        }
                    }
                    @catch (NSException *exception) {
                        
                    }
                    @finally {
                    }
                }
                else
                {
                    isCompleted=NO;
                    for(int i =0 ;i<[[dict valueForKey:@"qList"] count];i++)
                    {
                        [arrayGroupName addObject:[[[dict objectForKey:@"qList"] valueForKey:@"GroupName"] objectAtIndex:i]];
                        lblQCategory.text=[arrayGroupName objectAtIndex:swipCount];
                        NSString *strTypee =[[[dict objectForKey:@"qList"] valueForKey:@"type"] objectAtIndex:i];
                        NSString *strTitle =[[[dict objectForKey:@"qList"] valueForKey:@"title"] objectAtIndex:i];
                        NSString *strId =[[[[dict objectForKey:@"qList"] valueForKey:@"id"] objectAtIndex:i] stringValue];
                        NSString *strValue=[[[dict objectForKey:@"qList"] valueForKey:@"value"] objectAtIndex:i];
                        NSString *strLabels=[[[dict objectForKey:@"qList"] valueForKey:@"lables"] objectAtIndex:i];
                        NSString *strRating = [[[dict objectForKey:@"qList"] valueForKey:@"rating"] objectAtIndex:i];
                        NSString *strWatage =[[[dict objectForKey:@"qList"] valueForKey:@"wg"] objectAtIndex:i];
                        BOOL strYesNo=[[[[dict objectForKey:@"qList"] valueForKey:@"IsYesNo"] objectAtIndex:i] boolValue];
                        if (strValue.length<1) {
                            strValue=@"";
                        }
                        NSString *strYesNo1;
                        if (strYesNo) {
                            strYesNo1=@"Yes";
                        }
                        else
                        {
                            strYesNo1=@"No";
                        }
                        // IsYesNo
                        
                        [arrayTitles addObject:strTitle];
                        NSDictionary *dictTempt = [NSDictionary dictionaryWithObjectsAndKeys:strTypee,@"type",strTitle,@"title",strId,@"id",strValue,@"value",strLabels,@"labels",strYesNo1,@"isyesNo",strRating,@"rating",strWatage,@"w", nil];
                        [arrayDataStore addObject:dictTempt];
                        lblQno.text = [NSString stringWithFormat:@"Question: %d/%lu",swipCount+1,(unsigned long)[arrayTitles count]];
                    }
                }
                [activity setHidden:YES];
                [activity stopAnimating];
                [DejalBezelActivityView removeView];
                if (isRemovedArticle==NO) {
                    //  [self addButtonsToScrollView];
                }
            }
        }
    }
    
    if (!isRemovedArticle) {
        if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            
            [self addViewsiniPhone];
            NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
            BOOL isFromSign = [boolUserDefaults boolForKey:@"isFromSignFind"];
            if (isFromSign) {
                NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
                [boolUserDefaults setBool:NO forKey:@"isFromSignFind"];
                [boolUserDefaults synchronize];
            }
            else
            {
                //    [self showFindingButtons];
            }
        }
        else{
            
            [self addViewsiniPhone];
            NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
            BOOL isFromSign = [boolUserDefaults boolForKey:@"isFromSignFind"];
            if (isFromSign) {
                NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
                [boolUserDefaults setBool:NO forKey:@"isFromSignFind"];
                [boolUserDefaults synchronize];
            }
            else
            {
                //    [self showFindingButtons];
            }
        }
        
    }
    [self addViewsiniPhone];
}

-(void)addViewsiniPhone
{
    // NSLog(@"Array temp data %@",arrayDataStore);
    for (int i=0; i<[arrayDataStore count]; i++) {
        // NSLog(@"Array temp data %@",arrayDataStore);
        NSDictionary *dictMain = [arrayDataStore objectAtIndex:i];
        if ([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"radio"]==NSOrderedSame)
        {
            //**********Add View At which radio buttons and Tiltle Label will genarate **********
            
            CGRect buttonFrame = CGRectMake(0, 0,320, 200);
            UIView *view = [[UIView alloc] init];
            [view setFrame:buttonFrame];
            CALayer *bottomBorder = [CALayer layer];
            [view.layer addSublayer:bottomBorder];
            view.backgroundColor=[UIColor clearColor];
            CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(308, MAXFLOAT)];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [UIScreen mainScreen].bounds.size.width-30, s.height)];
            lbl.text = [dictMain valueForKey:@"title"];
            [lbl setAdjustsFontSizeToFitWidth:YES];
            [lbl setFont:[UIFont systemFontOfSize:15]];
            [lbl setNumberOfLines:9];
            [view addSubview:lbl];
            
            CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,30,30);
            NSString *str2=[dictMain valueForKey:@"labels"];
            NSArray *arrayStr=[str2 componentsSeparatedByString:@","];
            int btnCount=[arrayStr count];
            float viewHeight = btnCount*42+lbl.frame.size.height+20;
            for (int i=0; i<btnCount; i++)
            {
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame = btnFrame;
                NSString *title = [NSString stringWithFormat:@"%@",[dictMain valueForKey:@"id"]];
                btn.tag=[title intValue]+200+i+QCount;
                [btn setTitle:title forState:UIControlStateNormal];
                btn.titleLabel.textColor = [UIColor clearColor];
                [btn addTarget:nil action:@selector(checkRadioButton:) forControlEvents:UIControlEventTouchUpInside];
                [view addSubview:btn];
                UILabel *lblYes = [[UILabel alloc] init];
                lblYes.frame = CGRectMake(btn.frame.origin.x+50, btn.frame.origin.y+5, 200, 100);
                lblYes.text = [arrayStr objectAtIndex:i];
                [lblYes setNumberOfLines:2];
                lblYes.center=CGPointMake(lblYes.center.x, btn.center.y);
                if ([arrayStr count]>0)
                {
                    for (int j=0; j<[arrayStr count]; j++)
                    {
                        if ([lblYes.text isEqualToString:[dictMain valueForKey:@"value"]])
                        {
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                }
                [lblYes setAdjustsFontSizeToFitWidth:YES];
                [lblYes setTag:btn.tag+200];
                [view addSubview:lblYes];
                [lblYes setFont:[UIFont systemFontOfSize:15]];
                btnFrame.origin.y+=btnFrame.size.height+12; //it changes Y exix of radio button
            }
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight)];
            [arrayViews addObject:view];
        }
        if ([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"checkbox"]==NSOrderedSame)
        {
            UIView *view = [[UIView alloc] init];
            CGRect buttonFrame = CGRectMake(0, 0,320, 200);
            [view setFrame:buttonFrame];
            view.backgroundColor=[UIColor clearColor];
            
            CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [UIScreen mainScreen].bounds.size.width-30, s.height)];
            [lbl setAdjustsFontSizeToFitWidth:YES];
            [lbl setNumberOfLines:8];
            lbl.text =[dictMain valueForKey:@"title"];
            [view addSubview:lbl];
            
            CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,25,25);
            NSArray *arrayValuesToCreateButtons = [[dictMain valueForKey:@"labels"] componentsSeparatedByString:@","];
            int btnCount=[arrayValuesToCreateButtons count];
            int viewHeight = btnCount*30+lbl.frame.size.height+12;
            for (int i=0; i<btnCount; i++){
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame = btnFrame;
                NSString *title = [arrayValuesToCreateButtons objectAtIndex:i];
                btn.tag=[[dictMain valueForKey:@"id"] intValue];
                btn.titleLabel.textColor = [UIColor clearColor];
                [btn setTitle:title forState:UIControlStateNormal];
                
                [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                
                NSArray *arrayResult = [[dictMain valueForKey:@"value"] componentsSeparatedByString:@","];
                for (NSString *strCheck in arrayResult) {
                    // NSLog(@"str to check -- %@",strCheck);
                    if ([btn.titleLabel.text isEqualToString:strCheck]) {
                        [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        //[btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                    }
                }
                [btn addTarget:nil action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchUpInside];
                btnFrame.origin.y+=btnFrame.size.height+15;
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+50)];
                [view addSubview:btn];
                
                UILabel *lblItem = [[UILabel alloc] init];
                lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, 250, 100);
                [lblItem setFont:[UIFont systemFontOfSize:12]];
                lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                [lblItem setNumberOfLines:2];
                lblItem.text =title;
                [view addSubview:lblItem];
            }
            [arrayViews addObject:view];
        }
        /*
         if (([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"TextBox"]==NSOrderedSame ))
         {
         
         UIView *view = [[UIView alloc] init];
         [view setFrame:CGRectMake(0, 0, 308, 90)];
         view.backgroundColor=[UIColor clearColor];
         
         CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(308, MAXFLOAT)];
         UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 308, s.height)];
         lbl.text = [dictMain valueForKey:@"title"];
         [lbl setNumberOfLines:5];
         [view addSubview:lbl];
         
         CGRect txtfldFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+10,280,30);
         float viewHeight = lbl.frame.size.height+txtfldFrame.size.height+30;
         UITextField *txtfld = [[UITextField alloc] init];
         txtfld.frame =CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,280,30);
         txtfld.backgroundColor=[UIColor clearColor];
         txtfld.borderStyle = UITextBorderStyleRoundedRect;
         // txtfld.layer.borderWidth = 0.5f;
         txtfld.layer.borderColor = [[UIColor grayColor] CGColor];
         txtfld.tag = [[dictMain valueForKey:@"id"] intValue];
         txtfld.delegate=self;
         [view addSubview:txtfld];
         
         [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight)];
         [arrayViews addObject:view];
         }
         */
        if (([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"textarea"]==NSOrderedSame )||([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"TextBox"]==NSOrderedSame ))
        {
            UIView *view = [[UIView alloc] init];
            [view setFrame:CGRectMake(0, 0, 308, 150)];
            view.backgroundColor=[UIColor clearColor];
            
            
            
            CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(255, MAXFLOAT)];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [UIScreen mainScreen].bounds.size.width-30, s.height)];
            lbl.text = [dictMain valueForKey:@"title"];
            [lbl setFont:[UIFont systemFontOfSize:15]];
            [lbl setNumberOfLines:6];
            [view addSubview:lbl];
            
            CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-30,100);
            float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
            UITextView *txtView= [[UITextView alloc] init];
            txtView.text = [dictMain valueForKey:@"value"];
            txtView.frame =txtViewFrame;
            txtView.tag = [[dictMain valueForKey:@"id"] intValue];
            // txtView.layer.borderWidth = 0.5f;
            txtView.layer.borderWidth = 1.5f;
            txtView.layer.cornerRadius = 4.0f;
            txtView.layer.borderColor = [[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
            [txtView setBackgroundColor:[UIColor clearColor]];
            txtView.delegate=self;
            [view addSubview:txtView];
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight)];
            [arrayViews addObject:view];
        }
        if (([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"none"]==NSOrderedSame ))
        {
            if ([[dictMain valueForKey:@"isyesNo"] isEqualToString:@"No"]) //add slider if
            {
                
                //account_rating.png
                
                UIView *view = [[UIView alloc] init];
                [view setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 150)];
                view.backgroundColor=[UIColor clearColor];
                CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(280, MAXFLOAT)];
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [UIScreen mainScreen].bounds.size.width-30, s.height)];
                lbl.numberOfLines=8;
                [lbl setFont:[UIFont systemFontOfSize:15]];
                lbl.text = [dictMain valueForKey:@"title"];
                [view addSubview:lbl];
                int sliderTag = [[dictMain valueForKey:@"id"] intValue];
                UISlider *slider = [[UISlider alloc] init];
                UILabel *lblrat = [[UILabel alloc] init];
                slider.frame = CGRectMake(lbl.frame.origin.x+22, lbl.frame.size.height+20, [UIScreen mainScreen].bounds.size.width-30-22-22-10-20, 100);
                slider.tag=sliderTag;
                lblrat.tag = slider.tag+1;
                slider.minimumValue=0.0;
                slider.maximumValue=10.0;
                lblrat.frame = CGRectMake(slider.frame.origin.x+[UIScreen mainScreen].bounds.size.width-30-22-22-20, slider.frame.origin.y-20, 60, 50);
                // float viewHeight = lbl.frame.size.height+slider.frame.size.height+30;
                if ([[dictMain valueForKey:@"rating"] length]<1)
                {
                    lblrat.text=@"0/10";
                }
                else
                {
                    lblrat.text=[NSString stringWithFormat:@"%d/10",[[dictMain valueForKey:@"rating"] intValue]];
                    slider.value=[[dictMain valueForKey:@"rating"] floatValue];
                }
                NSString *strHeight = [NSString stringWithFormat:@"%f",view.frame.size.height];
                [arrayHeight addObject:strHeight];
                [slider addTarget:self action:@selector(changeSliderNone:) forControlEvents:UIControlEventValueChanged];
                UILabel *lblRatDis = [[UILabel alloc] initWithFrame:CGRectMake(slider.frame.origin.x+50, slider.frame.origin.y+slider.frame.size.height+12, 100, 20)];
                [lblRatDis setFont:[UIFont systemFontOfSize:12]];
                [lblRatDis setTextAlignment:NSTextAlignmentCenter];
                //lblRatDis.center.y=scrollView.center;
                lblRatDis.text = @"Rating: 0 to 10";
                [view addSubview:slider];
                [view addSubview:lblrat];
                [view addSubview:lblRatDis];
                
                float viewHeight = lbl.frame.size.height+slider.frame.size.height+lblRatDis.frame.size.height+50;
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+10)];
                
                [arrayViews addObject:view];
            }
            else
            {
                UIView *view = [[UIView alloc] init];
                [view setFrame:CGRectMake(0, 0, 320, 150)];
                CALayer *bottomBorder = [CALayer layer];
                [view.layer addSublayer:bottomBorder];
                view.backgroundColor=[UIColor clearColor];
                CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(255, MAXFLOAT)];
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [UIScreen mainScreen].bounds.size.width-30, s.height)];
                lbl.text = [dictMain valueForKey:@"title"];
                [lbl setAdjustsFontSizeToFitWidth:YES];
                [lbl setFont:[UIFont systemFontOfSize:15]];
                [lbl setNumberOfLines:8];
                [view addSubview:lbl];
                CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,30,30);
                
                int btnCount=3;
                float viewHeight = btnCount*30+lbl.frame.size.height+30;
                NSArray *arrayTitle = [[NSArray alloc] initWithObjects:@"Yes",@"No",@"N/A", nil];
                for (int i = 0 ; i<[arrayTitle count]; i++) {
                    UIButton *btn;
                    btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    btn.frame = btnFrame;
                    NSString *title = [NSString stringWithFormat:@"%@",[arrayTitle objectAtIndex:i]];
                    btn.tag=[[dictMain valueForKey:@"id"] intValue];
                    [btn setTitle:title forState:UIControlStateNormal];
                    btn.titleLabel.textColor = [UIColor clearColor];
                    [btn addTarget:nil action:@selector(checkNoneRadioButton:) forControlEvents:UIControlEventTouchUpInside];
                    [view addSubview:btn];
                    UILabel *lblYes = [[UILabel alloc] init];
                    lblYes.frame = CGRectMake(btn.frame.origin.x+50, btn.frame.origin.y+5, 200, 20);
                    lblYes.text = title;//[arrayTitle objectAtIndex:lblCount];
                    [view addSubview:lblYes];
                    
                    if ([title isEqualToString:[dictMain valueForKey:@"value"]]) {//if value found
                        [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                    }
                    btnFrame.origin.y+=btnFrame.size.height+8;
                }
                [view setFrame:CGRectMake(0, 0, view.frame.size.width, viewHeight)];
                
                [arrayViews addObject:view];
            }
            
        }
        
        NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
        BOOL isFromSign = [boolUserDefaults boolForKey:@"isFromSign"];
        if (isFromSign) {
        }
        else
        {
            [self addView];
        }
        
    }
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults setBool:NO forKey:@"isFromSign"];
    [boolUserDefaults synchronize];
}




-(void)addViewsiniPad
{
    for (int i=0; i<[arrayDataStore count]; i++) {
        
        NSDictionary *dictMain = [arrayDataStore objectAtIndex:i];
        
        if ([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"radio"]==NSOrderedSame ) {
            
            CGRect buttonFrame = CGRectMake(0, 0,735,450);
            UIView *view = [[UIView alloc] init];
            [view setFrame:buttonFrame];
            CALayer *bottomBorder = [CALayer layer];
            [view.layer addSublayer:bottomBorder];
            view.backgroundColor=[UIColor clearColor];
            
            
            CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:30] constrainedToSize:CGSizeMake(702, MAXFLOAT)];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(24, 0, 702, s.height)];
            [lbl setFont:[UIFont systemFontOfSize:30]];
            
            lbl.text = [dictMain valueForKey:@"title"];
            [lbl setAdjustsFontSizeToFitWidth:YES];
            
            [lbl setNumberOfLines:8];
            [view addSubview:lbl];
            CGRect btnFrame = CGRectMake(lbl.frame.origin.x-9, lbl.frame.origin.y+lbl.frame.size.height+15,60,60);
            
            NSString *str2=[dictMain valueForKey:@"labels"];
            NSArray *arrayStr=[str2 componentsSeparatedByString:@","];
            
            int btnCount=[arrayStr count];
            int viewHeight = btnCount*60+8+lbl.frame.size.height+30;
            for (int i=0; i<btnCount; i++){
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame = btnFrame;
                NSString *title = [NSString stringWithFormat:@"%@",[dictMain valueForKey:@"id"]];
                btn.tag=[title intValue]+200+i+QCount;
                [btn setTitle:title forState:UIControlStateNormal];
                btn.titleLabel.textColor = [UIColor clearColor];
                [btn addTarget:nil action:@selector(checkRadioButton:) forControlEvents:UIControlEventTouchUpInside];
                
                [view addSubview:btn];
                UILabel *lblYes = [[UILabel alloc] init];
                lblYes.frame = CGRectMake(btn.frame.origin.x+btn.frame.size.width+12, btn.frame.origin.y+10, 632, 100);
                lblYes.center=CGPointMake(lblYes.center.x, btn.center.y);
                lblYes.text = [arrayStr objectAtIndex:i];
                [lblYes setNumberOfLines:2];
                if ([arrayStr count]>0) {
                    for (int j=0; j<[arrayStr count]; j++) {
                        if ([lblYes.text isEqualToString:[dictMain valueForKey:@"value"]]) {
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                        }
                    }
                }
                else{
                    [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                }
                [lbl setAdjustsFontSizeToFitWidth:YES];
                [lblYes setFont:[UIFont systemFontOfSize:26]];
                [lblYes setTag:btn.tag+200];
                [view addSubview:lblYes];
                btnFrame.origin.y+=btnFrame.size.height+8; //it changes Y exix of radio button
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+35)];// it will manage view(superView of radio buttons and labels) hieght finaly.
                bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height, view.frame.size.width, 2.0f);
                
                bottomBorder.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"line-for-goget.png"]].CGColor;
            }
            //[scrollView addSubview:view];
            [arrayViews addObject:view];
        }
        if ([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"checkbox"]==NSOrderedSame ) {
            
            UIView *view = [[UIView alloc] init];
            CGRect buttonFrame =  CGRectMake(0, 0,735,350);
            
            [view setFrame:buttonFrame];
            view.backgroundColor=[UIColor clearColor];
            
            CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:30] constrainedToSize:CGSizeMake(702, MAXFLOAT)];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 702, s.height)];
            //[lbl setAdjustsFontSizeToFitWidth:YES];
            [lbl setNumberOfLines:8];
            lbl.text = [dictMain valueForKey:@"title"];
            [lbl setFont:[UIFont systemFontOfSize:30]];
            
            [view addSubview:lbl];
            CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+12,60,60);
            NSArray *arrayValuesToCreateButtons = [[dictMain valueForKey:@"labels"] componentsSeparatedByString:@","];
            int btnCount=[arrayValuesToCreateButtons count];
            int viewHeight = btnCount*60+lbl.frame.size.height+8;
            for (int i=0; i<btnCount; i++){
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame = btnFrame;
                NSString *title = [arrayValuesToCreateButtons objectAtIndex:i];
                btn.tag=[[dictMain valueForKey:@"id"] intValue];
                btn.titleLabel.textColor = [UIColor clearColor];
                [btn setTitle:title forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                
                NSArray *arrayResult = [[dictMain valueForKey:@"value"] componentsSeparatedByString:@","];
                for (NSString *strCheck in arrayResult) {
                    if ([btn.titleLabel.text isEqualToString:strCheck]) {
                        [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        //[btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                    }
                }
                
                [btn addTarget:nil action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchUpInside];
                btnFrame.origin.y+=btnFrame.size.height+15;
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+50)];
                // CALayer *bottomBorder = [CALayer layer];
                // bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height, view.frame.size.width, 2.0f);
                // bottomBorder.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"black_button.png"]].CGColor;
                // [view.layer addSublayer:bottomBorder];
                [view addSubview:btn];
                UILabel *lblItem = [[UILabel alloc] init];
                lblItem.frame = CGRectMake(btn.frame.origin.x+btn.frame.size.width+10, btn.frame.origin.y+5, 632, 100);
                lblItem.center= CGPointMake(lblItem.center.x, btn.center.y);
                lblItem.text =title;
                [lblItem setNumberOfLines:2];
                [lblItem setFont:[UIFont systemFontOfSize:25]];
                //[arrayCheckboxItems objectAtIndex:i];
                [view addSubview:lblItem];
            }
            
            [arrayViews addObject:view];
        }
        /*
         if (([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"TextBox"]==NSOrderedSame )) {
         UIView *view = [[UIView alloc] init];
         [view setFrame:CGRectMake(0, 0,735,300)];
         
         
         CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:30] constrainedToSize:CGSizeMake(702, MAXFLOAT)];
         UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 702, s.height)];
         
         
         view.backgroundColor=[UIColor clearColor];
         
         [lbl setAdjustsFontSizeToFitWidth:YES];
         lbl.text = [dictMain valueForKey:@"title"];
         [lbl setFont:[UIFont systemFontOfSize:30]];
         
         [view addSubview:lbl];
         CGRect txtfldFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,702,75);
         UITextField *txtfld = [[UITextField alloc] init];
         txtfld.backgroundColor=[UIColor clearColor];
         //txtfld.layer.borderWidth = 0.5f;
         txtfld.borderStyle = UITextBorderStyleRoundedRect;
         // txtfld.layer.borderColor = [UIColor grayColor].CGColor;
         // txtfld.layer.borderWidth = 2.0f;
         
         txtfld.text= [dictMain valueForKey:@"value"];
         [txtfld setFont:[UIFont systemFontOfSize:30]];
         txtfld.layer.borderColor = [[UIColor grayColor] CGColor];
         txtfld.tag = [[dictMain valueForKey:@"id"] intValue];
         txtfld.frame =txtfldFrame;
         txtfld.delegate=self;
         
         [view addSubview:txtfld];
         float viewHeight = lbl.frame.size.height+txtfld.frame.size.height+40;
         [view setFrame:CGRectMake(0, 0, view.frame.size.width, viewHeight)];
         [arrayViews addObject:view];
         }*/
        if (([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"textarea"]==NSOrderedSame )||([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"TextBox"]==NSOrderedSame )){
            UIView *view = [[UIView alloc] init];
            [view setFrame:CGRectMake(0, 0,735, 250)];
            
            CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:30] constrainedToSize:CGSizeMake(702, MAXFLOAT)];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 702, s.height)];
            view.backgroundColor=[UIColor clearColor];
            
            lbl.text = [dictMain valueForKey:@"title"];
            [lbl setFont:[UIFont systemFontOfSize:30]];
            //[lbl setAdjustsFontSizeToFitWidth:YES];
            [lbl setNumberOfLines:8];
            [view addSubview:lbl];
            CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,656,150);
            UITextView *txtView= [[UITextView alloc] init];
            txtView.text = [dictMain valueForKey:@"value"];
            [txtView setFont:[UIFont systemFontOfSize:25]];
            txtView.frame =txtViewFrame;
            
            txtView.tag = [[dictMain valueForKey:@"id"] intValue];
            txtView.layer.borderWidth = 0.5f;
            txtView.layer.borderColor = [UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1].CGColor;
            txtView.layer.borderWidth = 2.0f;
            txtView.layer.cornerRadius = 5.0f;
            [txtView setBackgroundColor:[UIColor clearColor]];
            txtView.delegate=self;
            float viewHeight = lbl.frame.size.height+txtView.frame.size.height+30;
            [view setFrame:CGRectMake(0, 0, scrollView.frame.size.width, viewHeight)];
            [view addSubview:txtView];
            [arrayViews addObject:view];
        }
        if (([[dictMain valueForKey:@"type"]caseInsensitiveCompare:@"none"]==NSOrderedSame )){
            if ([[dictMain valueForKey:@"isyesNo"] isEqualToString:@"No"])
            {
                UIView *view = [[UIView alloc] init];
                [view setFrame:CGRectMake(0, 0, 735, 230)];
                view.backgroundColor=[UIColor clearColor];
                
                
                
                CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:30] constrainedToSize:CGSizeMake(702, MAXFLOAT)];
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 702, s.height)];
                
                lbl.numberOfLines=8;
                [lbl setFont:[UIFont systemFontOfSize:30]];
                lbl.text = [dictMain valueForKey:@"title"];
                [view addSubview:lbl];
                int sliderTag = [[dictMain valueForKey:@"id"] intValue];
                UISlider *slider = [[UISlider alloc] init];
                UILabel *lblrat = [[UILabel alloc] init];
                slider.frame = CGRectMake(lbl.frame.origin.x+32, lbl.frame.origin.y+lbl.frame.size.height+15, 532, 100);
                slider.tag=sliderTag;
                lblrat.tag = slider.tag+1;
                slider.minimumValue=0.0;
                slider.maximumValue=10.0;
                lblrat.frame = CGRectMake(slider.frame.origin.x+slider.frame.size.width+15, slider.frame.origin.y+5,92, 30);
                [lblrat setFont:[UIFont systemFontOfSize:20]];
                
                if ([[dictMain valueForKey:@"rating"] length]<1) {
                    lblrat.text=@"0/10";
                }
                else
                {
                    lblrat.text=[NSString stringWithFormat:@"%d/10",[[dictMain valueForKey:@"rating"] intValue]];
                    [lblrat setFont:[UIFont systemFontOfSize:25]];
                    slider.value=[[dictMain valueForKey:@"rating"] floatValue];
                }
                // lblrat.tag =2;
                //  CALayer *bottomBorder = [CALayer layer];
                //  bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height, view.frame.size.width+10, 2.0f);
                // bottomBorder.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"black_button.png"]].CGColor;
                //  [view.layer addSublayer:bottomBorder];
                NSString *strHeight = [NSString stringWithFormat:@"%f",view.frame.size.height];
                [arrayHeight addObject:strHeight];
                [slider addTarget:self action:@selector(changeSliderNone:) forControlEvents:UIControlEventValueChanged];
                UILabel *lblRatDis = [[UILabel alloc] initWithFrame:CGRectMake(slider.frame.origin.x+50, slider.frame.origin.y+slider.frame.size.height+17, 286, 30)];
                [lblRatDis setFont:[UIFont systemFontOfSize:20]];
                [lblRatDis setTextAlignment:NSTextAlignmentCenter];
                //lblRatDis.center.y=scrollView.center;
                lblRatDis.text = @"Rating: 0 to 10";
                [lblRatDis setFont:[UIFont systemFontOfSize:20]];
                lblRatDis.center=CGPointMake(slider.center.x, lblRatDis.frame.origin.y);
                [view addSubview:slider];
                [view addSubview:lblrat];
                [view addSubview:lblRatDis];
                
                float viewHeight = lbl.frame.size.height+slider.frame.size.height+lblRatDis.frame.size.height+50;
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+10)];
                
                [arrayViews addObject:view];
            }
            else
            {
                
                UIView *view = [[UIView alloc] init];
                [view setFrame:CGRectMake(0, 0,735,300)];
                CALayer *bottomBorder = [CALayer layer];
                [view.layer addSublayer:bottomBorder];
                view.backgroundColor=[UIColor clearColor];
                
                CGSize s = [[dictMain valueForKey:@"title"] sizeWithFont:[UIFont systemFontOfSize:30] constrainedToSize:CGSizeMake(702, MAXFLOAT)];
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 702, s.height)];
                
                lbl.text = [dictMain valueForKey:@"title"];
                //[lbl setAdjustsFontSizeToFitWidth:YES];
                [lbl setNumberOfLines:8];
                [lbl setFont:[UIFont systemFontOfSize:30]];
                
                [view addSubview:lbl];
                CGRect btnFrame = CGRectMake(lbl.frame.origin.x-9, lbl.frame.origin.y+lbl.frame.size.height+12,60,60);
                
                
                int btnCount=3;
                NSArray *arrayTitle = [[NSArray alloc] initWithObjects:@"Yes",@"No",@"N/A", nil];
                for (int i = 0 ; i<[arrayTitle count]; i++) {
                    
                    UIButton *btn;
                    btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    btn.frame = btnFrame;
                    NSString *title = [NSString stringWithFormat:@"%@",[arrayTitle objectAtIndex:i]];
                    btn.tag=[[dictMain valueForKey:@"id"] intValue];
                    [btn setTitle:title forState:UIControlStateNormal];
                    btn.titleLabel.textColor = [UIColor clearColor];
                    [btn addTarget:nil action:@selector(checkNoneRadioButton:) forControlEvents:UIControlEventTouchUpInside];
                    [view addSubview:btn];
                    UILabel *lblYes = [[UILabel alloc] init];
                    lblYes.frame = CGRectMake(btn.frame.origin.x+btn.frame.size.width+12, btn.frame.origin.y+5, 632, 46);
                    [lblYes setFont:[UIFont systemFontOfSize:30]];
                    lblYes.text = title;//[arrayTitle objectAtIndex:lblCount];
                    [view addSubview:lblYes];
                    
                    
                    if ([title isEqualToString:[dictMain valueForKey:@"value"]]) {//if value found
                        [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                        
                    }
                    else
                    {
                        
                        [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                        
                    }
                    btnFrame.origin.y+=btnFrame.size.height+8;
                }
                
                float viewHeight = btnCount*60+lbl.frame.size.height+40;
                [view setFrame:CGRectMake(0, 0, view.frame.size.width, viewHeight)];
                
                [arrayViews addObject:view];
            }
            
        }
        NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
        BOOL isFromSign = [boolUserDefaults boolForKey:@"isFromSign"];
        if (isFromSign) {
        }
        else
        {
            [self addView];
        }
    }
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults setBool:NO forKey:@"isFromSign"];
    [boolUserDefaults synchronize];
}





//-----------------------------button ations------------------------------------------
-(void)checkRadioButton:(id)sender{
    
    if (CheckComplition==0 && [arraySliderValue count]>0 ) {
        //[arayData removeAllObjects];
        
        CheckComplition++;
        
    }
    
    isChanged=YES;
    isRadioBtn=YES;
    isCheckBox=NO;
    //iIndex++;
    UIButton *btn = (UIButton *)sender;
    UILabel *lbl = (UILabel *)[self.view viewWithTag:btn.tag+200];
    
    
    UIView *view = [arrayViews objectAtIndex:swipCount];
    
    for (UIButton *Btns in [view subviews]) {
        
        if ([Btns isKindOfClass:[UIButton class]]) {
            [Btns setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        }
        
    }
    [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    //int activeTagg=btn.tag;
    
    // int iTag = btn.tag-200-[btn.titleLabel.text intValue];
    
    qid =[btn.titleLabel.text intValue];
    if (RbiIndex>=0) {
        if (CurrentTitle==[btn.titleLabel.text intValue]) {
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict = (NSDictionary *)[arayData objectAtIndex:RbiIndex];
            [newDict addEntriesFromDictionary:oldDict];
            [newDict setObject:[NSString stringWithFormat:@"%d",qNo] forKey:@"id"];
            [newDict setObject:lbl.text forKey:@"value"];
            [arayData replaceObjectAtIndex:RbiIndex withObject:newDict];
            RbiIndex = [arayData indexOfObject:newDict];
            RCorunt=-5;
        }
        else
        {
            NSMutableDictionary *tempData = [[NSMutableDictionary alloc]
                                             initWithObjectsAndKeys:btn.titleLabel.text ,
                                             @"id",lbl.text, @"value",@"0", @"w",@"0", @"r",nil];
            [arayData addObject:tempData];
            RbiIndex =[arayData indexOfObject:tempData];
            CurrentTitle =[btn.titleLabel.text intValue];
            RCorunt=+5;
            RbiIndex=-1;
        }
    }
    else
    {
        
        NSString *strWtg;
        
        for (int i=0; i<[arrayDataStore count]; i++) {
            
            NSDictionary *dictTemp = [arrayDataStore objectAtIndex:i];
            
            if ([[dictTemp valueForKey:@"id"] isEqualToString:btn.titleLabel.text]) {
                strWtg=[dictTemp valueForKey:@"w"];
            }
        }
        
        NSMutableDictionary *tempData = [[NSMutableDictionary alloc]
                                         initWithObjectsAndKeys:btn.titleLabel.text ,
                                         @"id",lbl.text, @"value",strWtg, @"w",@"0", @"r",nil];
        [arayData addObject:tempData];
        RbiIndex =[arayData indexOfObject:tempData];
        CurrentTitle =[btn.titleLabel.text intValue];
        RCorunt=0;
    }
    // NSLog(@"array watage %@",arayData);
}


-(void)checkCheckBoxes:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    NSString *strWtg;
    int dictinDex=-1;
    for (int i=0; i<[arrayDataStore count]; i++) {
        
        NSDictionary *dictTemp = [arrayDataStore objectAtIndex:i];
        
        if ([[dictTemp valueForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%ld",(long)btn.tag]]) {
            strWtg=[dictTemp valueForKey:@"w"];
            
        }
    }
    if ([arrayCBAns count]>1) {
        [arrayCBAns removeAllObjects];
    }
    
    isRadioBtn=NO;
    isCheckBox=YES;
    checkBoxCount++;
    
    for (NSDictionary *dictnary in arayData) {
        
        if ([[NSString stringWithFormat:@"%ld",(long)btn.tag] isEqualToString:[dictnary valueForKey:@"id"]]) {
            
            dictinDex=[arayData indexOfObject:dictnary]; //dictinDex is index of dictnary of current Question in Array
        }
    }
    
    
    // NSLog(@"selected %@",btn.titleLabel.text);
    if ([[UIImage imageNamed:@"uncheck.png"] isEqual:btn.currentImage]){
        
        [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        //[arrayCBAns addObject:btn.titleLabel.text];
    }
    else
    {
        [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        [arrayCBAns removeObject:btn.titleLabel.text];
        
    }
    
    UIView *CurrentViwview = [arrayViews objectAtIndex:swipCount];
    
    for (UIButton *btn in [CurrentViwview subviews]) { //it will return checked button from view
        
        if ([btn isKindOfClass:[UIButton class]]) {
            if ([[UIImage imageNamed:@"checked.png"] isEqual:btn.currentImage]){
                
                [arrayCBAns addObject:btn.titleLabel.text];
            }
            
        }
        
    }
    
    
    if (dictinDex<0) {
        
        myStringComasap = [arrayCBAns componentsJoinedByString:@","];
        NSMutableDictionary *CbData = [[NSMutableDictionary alloc]
                                       initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)btn.tag],
                                       @"id",myStringComasap, @"value",@"0", @"r",strWtg, @"w",nil];
        [arayData addObject:CbData];
    }
    else
    {
        myStringComasap = [arrayCBAns componentsJoinedByString:@","];
        NSMutableDictionary *CbData = [[NSMutableDictionary alloc]
                                       initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)btn.tag],
                                       @"id",myStringComasap, @"value",@"0", @"r",strWtg, @"w",nil];
        //[newDictDD setObject:@"" forKey:@"value"];
        [arayData replaceObjectAtIndex:dictinDex withObject:CbData];
        
    }
    // NSLog(@"selected in loop %@",arayData);
}
-(void)changeSliderNone:(id)sender{
    UISlider *slider=(UISlider *)sender;
    isNextQ=NO;
    if (CheckComplition==0) {
        // [arayData removeAllObjects];
        CheckComplition++;
        
    }
    int sliderValue=(int)(slider.value);
    NSNumber *number = [NSNumber numberWithFloat: sliderValue];
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setMaximumFractionDigits:1];
    // UISlider *sliderold = (UISlider *)[self.view viewWithTag:slider.tag];
    if (slider.value<1) {
        isNextQ=YES;
    }
    
    sliderValuestr=[NSString stringWithFormat:@"%d",sliderValue];
    NSString *strQid = [NSString stringWithFormat:@"%ld",(long)slider.tag];
    UILabel *lbl = (UILabel *)[self.view viewWithTag:slider.tag+1];
    lbl.text = [NSString stringWithFormat:@"%@/10",number];
    NSString *strWatage;
    for (NSDictionary *dictTemp in arrayDataStore) {
        if ([strQid isEqualToString:[dictTemp valueForKey:@"id"]] ) {
            strWatage=[dictTemp valueForKey:@"w"];
        }
    }
    if(noneIindex>=0)
    {
        NSString *QID =[NSString stringWithFormat:@"%ld",(long)slider.tag];
        if([arayData count] > 0 )
        {
            for(int qi=0;qi< [arayData count];qi++)
            {
                NSDictionary *objQindex = [arayData objectAtIndex:qi];
                if([[objQindex valueForKey:@"id"]isEqual:QID])
                {
                    noneIindex = [arayData indexOfObject:objQindex];
                }
            }
        }
        NSDictionary *dictTemp = [arayData objectAtIndex:noneIindex];
        if ([[dictTemp valueForKey:@"id"]isEqualToString:strQid])
        {
            //          int inexx = [arrayItemsId indexOfObject:strQid];
            //          NSString *wtg = [NSString stringWithFormat:@"%@",[arrayWatage objectAtIndex:inexx]];
            NSMutableDictionary *newDictDD = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict = (NSDictionary *)[arayData objectAtIndex:noneIindex];
            [newDictDD addEntriesFromDictionary:oldDict];
            [newDictDD setObject:[NSString stringWithFormat:@"%ld",(long)slider.tag] forKey:@"id"],
            [newDictDD setObject:[number stringValue] forKey:@"r"];
            [newDictDD setObject:strWatage forKey:@"w"];
            //[newDictDD setObject:@"" forKey:@"value"];
            [arayData replaceObjectAtIndex:noneIindex withObject:newDictDD];
            noneIindex = [arayData indexOfObject:newDictDD];
        }
        else{
            noneIindex=-1;
        }
    }
    else{
        NSMutableDictionary *dictt = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)slider.tag],@"id",[number stringValue] ,@"r" ,@"0",@"value",nil];
        [arayData addObject:dictt];
        mantCountForCheck++;
        noneIindex = [arayData indexOfObject:dictt];
        sIndex++;
    }
    // NSLog(@"array data %@",arayData);
}

#pragma mark TableView Delegate methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //[tblView setHidden:NO];
    
    NSLog(@"Tableview-----%lu",(unsigned long)[self.arrayTblDis count]);
    return [self.arrayTblDis count];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        return 45;
    }
    else
    {
        return 69;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"ArticleCell";
    
    ArticleCell *cell = (ArticleCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArticleCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    else{
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArticleCell_iPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    cell.txtInCell.tag=501+indexPath.row;
    cell.btnRemove.tag=indexPath.row+1;
    cell.txtPrio.tag=indexPath.row+200;
    cell.btnImgeFindings.tag=indexPath.row+600;
    
    if ([arrayTblDis count]>0) {
        
        NSMutableDictionary *dictTemp = [arrayTblDis objectAtIndex:indexPath.row];
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSString *strIndex = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
        
        [newDict addEntriesFromDictionary:dictTemp];
        [newDict setObject:strIndex forKey:@"fIndex"];
        [arrayTblDis replaceObjectAtIndex:indexPath.row withObject:newDict];
        
        // NSLog(@"tbl priority -----%@",dictTemp);
        cell.Lblnumber.text= [NSString stringWithFormat:@"%ld.",indexPath.row+1];
        cell.txtInCell.text =[dictTemp objectForKey:@"description"];
        cell.txtPrio.text= [dictTemp objectForKey:@"Priority"];
        if (cell.txtInCell.text.length<1) {
            [cell.txtInCell becomeFirstResponder];
        }
        
        
        cell.backgroundColor = [UIColor clearColor];
        
        
        NSString *strImageName = [dictTemp objectForKey:@"FImg"];
        
        // NSLog(@"image name in cll will %@",strImageName);
        UIImage *imageTemp = [self getImageFromDirectory:strImageName];
        [cell.btnImgeFindings setImage:imageTemp forState:UIControlStateNormal];
        // cell.btnImgeFindings.imageView.image=imageTemp;
        
        if (!cell.btnImgeFindings.imageView.image) {
            [cell.btnImgeFindings setImage:[UIImage imageNamed:@"noimage.png"] forState:UIControlStateNormal];
        }
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        
        
    }
    return cell;
}

-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark TextField Methods

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    if ([textField.text isEqualToString:@"priority"]) {
    //        [textField resignFirstResponder];
    //        [self.view endEditing:YES];
    //    }
    if ([textField.text isEqualToString:@"Priority"]||[textField.placeholder isEqualToString:@"priority"]||[textField.text isEqualToString:@"High"]||[textField.text isEqualToString:@"Medium"]||[textField.text isEqualToString:@"Low"]) {
        [textField resignFirstResponder];
        [self.view endEditing:YES];
    }
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)myTextField{
    isRadioBtn=NO;
    isCheckBox=NO;
    if ([myTextField tag]>=501){
        txtTempDiscription=myTextField;
        CGRect textFieldRect =[self.view.window convertRect:myTextField.bounds fromView:myTextField];
        CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
        CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
        CGFloat numerator =midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
        CGFloat denominator =(MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
        CGFloat heightFraction = numerator / denominator;
        if (heightFraction < 0.0){
            heightFraction = 0.0;
        }
        else if (heightFraction > 1.0){
            heightFraction = 1.0;
        }
        UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
        if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown){
            animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
        }
        else{
            animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
        }
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y -= animatedDistance;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
        [self.view setFrame:viewFrame];
        [UIView commitAnimations];
        return YES;
    }
    
    else if ([myTextField.placeholder isEqualToString:@"priority"]){
        CheckComplition=1;
        if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            
            [txtTempDiscription resignFirstResponder];
            ActivePriority=@"High";
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                // code here
                
                [myTextField resignFirstResponder];
                [self.view endEditing:YES]; // added this in for case when keyboard was already on screen
                //[self editStartDate:myTextField];
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
                self.view.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
                [UIView commitAnimations];
                isInTableView =YES;
                AppDelegate  *appD =(AppDelegate *)[[UIApplication sharedApplication] delegate];
                float hght =myTextField.tag-200;
                float chngdht = hght+420;
                appD.point=CGPointMake(100, chngdht);
                actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
                [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
                CGRect pickerFrame = CGRectMake(0, 0, 0, 0);
                UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
                pickerView.showsSelectionIndicator = YES;
                pickerView.dataSource = self;
                pickerView.delegate = self;
                txtPriorityTemp=myTextField;
                [actionSheet addSubview:pickerView];
                alertController = [UIAlertController alertControllerWithTitle:nil
                                                                      message:@"\n\n\n\n\n\n\n\n\n\n"
                                                               preferredStyle:UIAlertControllerStyleActionSheet];
                //pickerView.frame=CGRectMake(0, 0, 120, 30);
                UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 305.0f, 44.f)];
                toolView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blank-headerNew.png"]];
                CGRect buttonFrame = CGRectMake(220, 5, 70, 30);
                UIButton *selButton = [[UIButton alloc] initWithFrame:buttonFrame];
                //[selButton setBackgroundImage:[UIImage imageNamed:@"red_bgnew.png"] forState:UIControlStateNormal];
                [selButton setTitle:@"Done" forState:UIControlStateNormal];
                [selButton addTarget: self
                              action: @selector(dismissActionSheet:)
                    forControlEvents: UIControlEventTouchDown];
                selButton.tag= myTextField.tag+10;
                [toolView addSubview:selButton];
                [alertController.view addSubview:pickerView];
                [alertController.view addSubview:toolView];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            
            else
            {
                isInTableView =YES;
                [myTextField resignFirstResponder];
                if ([self isKeyboardOnScreen]) {
                    [self.view endEditing:YES];
                }
                
                actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
                [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
                CGRect pickerFrame = CGRectMake(0, 0, 0, 0);
                UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
                pickerView.showsSelectionIndicator = YES;
                pickerView.dataSource = self;
                pickerView.delegate = self;
                txtPriorityTemp=myTextField;
                [actionSheet addSubview:pickerView];
                UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:NSLocalizedString(@"Done", @"")]];
                closeButton.momentary = YES;
                closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
                //  closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
                closeButton.tintColor = [UIColor blackColor];
                [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
                closeButton.tag=myTextField.tag+10;
                [actionSheet addSubview:closeButton];
                [actionSheet showInView:self.view];
                [actionSheet setBounds:CGRectMake(0, 15, 320, 450)];
                CGRect textFieldRect =[self.view.window convertRect:myTextField.bounds fromView:myTextField];
                CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
                CGFloat midline = textFieldRect.origin.y + 0.5 * 300;
                CGFloat numerator =midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
                CGFloat denominator =(MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
                CGFloat heightFraction = numerator / denominator;
                
                if (heightFraction < 0.0){
                    heightFraction = 0.0;
                }
                else if (heightFraction > 1.0){
                    heightFraction = 1.0;
                }
                UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
                if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
                {
                    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
                }
                else
                {
                    animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
                }
                CGRect viewFrame = self.view.frame;
                viewFrame.origin.y -= animatedDistance;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
                // [self.view setFrame:viewFrame];
                [UIView commitAnimations];
            }
        }
        else{
            //if device is iPad
            
            
            // NSLog(@"active index on textfield %d",myTextField.tag);
            activeFindIndex=myTextField.tag-200;
            [self performSelector:@selector(resignKeyBoard) withObject:nil afterDelay:0.001];
            UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
            UIView *popoverView = [[UIView alloc] init];   //view
            popoverView.backgroundColor = [UIColor clearColor];
            CGRect pickerFrame = CGRectMake(0, 0, 0, 0);
            UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            
            txtPriorityTemp=myTextField;
            // NSLog(@"textfield tag is %d",myTextField.tag);
            [popoverView addSubview:pickerView];
            popoverContent.view = popoverView;
            popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            popoverController.delegate=self;
            [popoverController setPopoverContentSize:CGSizeMake(320, 200) animated:NO];
            [popoverController presentPopoverFromRect:txtPriorityTemp.frame inView:tblView permittedArrowDirections:NO animated:YES];
            [myTextField resignFirstResponder];
            
            // [self.view endEditing:YES];
        }
    }
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (CheckComplition==0 ) {
        //[arayData removeAllObjects];
        for (NSDictionary *dictTemp in arayData) {
            if ([[dictTemp valueForKey:@"id"]  isEqual:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
                // TviIndex=[arayData indexOfObject:dictTemp];
                // NSLog(@"text to remove %@",dictTemp);
                [arayData removeObject:dictTemp];
            }
        }
    }
    
    
    
    if(textField.tag>=501){
        NSInteger indexNum=textField.tag-501;
        
        //  NSLog(@"active index will----%ld",(long)indexNum);
        NSMutableDictionary *dictTemp = [arrayTblDis objectAtIndex:indexNum];
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        
        
        [newDict addEntriesFromDictionary:dictTemp];
        [newDict setObject:textField.text forKey:@"description"];
        [arrayTblDis replaceObjectAtIndex:indexNum withObject:newDict];
        [tblFindings reloadData];
    }
    else{
        if (![textField.text isEqualToString:@"High"]||![textField.text isEqualToString:@"Medium"]||![textField.text isEqualToString:@"Low"]) {
            NSMutableDictionary *tempDataText = [[NSMutableDictionary alloc]
                                                 initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)textField.tag],
                                                 @"id",textField.text, @"value",@"0", @"r",@"0", @"w",nil];
            [arayData addObject:tempDataText];
            
            TfiIndex=[arayData indexOfObject:tempDataText];
            
            //  NSLog(@"arraydata %@",arayData);
        }
        
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [txtfldCome resignFirstResponder];
    [txtfldFeedback resignFirstResponder];
    [txtfldComment resignFirstResponder];
    [txtfldCell resignFirstResponder];
    [textField resignFirstResponder];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
    
    return YES;
}

#pragma mark TextView Delegate Methods
-(void)textViewDidBeginEditing:(UITextView *)textField{
    
    isRadioBtn=NO;
    isCheckBox=NO;
    //  CGPoint point = CGPointMake(0,100);
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(0, -60, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}
-(void)textViewDidEndEditing:(UITextView *)textField
{
    // NSLog(@"array data %@",arayData);
    
    NSString *strWatage;
    
    for (NSDictionary *dictTemp in arrayDataStore) {
        
        if ([[NSString stringWithFormat:@"%ld",(long)textField.tag] isEqualToString:[dictTemp valueForKey:@"id"]] ) {
            strWatage=[dictTemp valueForKey:@"w"];
        }
    }
    
    if (CheckComplition==0 ) {
        //[arayData removeAllObjects];
        
        NSMutableArray *toDelete = [NSMutableArray array];
        
        for (NSDictionary *dictTemp in arayData) {
            
            if ([[dictTemp valueForKey:@"id"]  isEqual:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
                
                // TviIndex=[arayData indexOfObject:dictTemp];
                [toDelete removeObject:dictTemp];
            }
        }
        [arayData removeObject:toDelete];
        //CheckComplition++;
        
    }
    
    typeStr = @"TextView";
    StringDescription=textField.text;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
    
    int TextViewindex=-1;
    
    for (NSDictionary *dictTemp in arayData) {
        
        if ([[dictTemp valueForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
            
            TextViewindex=[arayData indexOfObject:dictTemp];
            
        }
        
    }
    if (TextViewindex<0) {
        NSMutableDictionary *tempDataText = [[NSMutableDictionary alloc]
                                             initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)textField.tag],
                                             @"id",textField.text, @"value",@"0", @"r",strWatage, @"w",nil];
        [arayData addObject:tempDataText];
    }
    else
    {
        
        NSMutableDictionary *newDictTV = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[arayData objectAtIndex:TextViewindex];
        [newDictTV addEntriesFromDictionary:oldDict];
        [newDictTV setObject:[NSString stringWithFormat:@"%ld",(long)textField.tag] forKey:@"id"],
        [newDictTV setObject:textField.text forKey:@"value"],
        [arayData replaceObjectAtIndex:TextViewindex withObject:newDictTV];
        
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

-(IBAction)addArticle:(id)sender
{
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        NSMutableDictionary *mdict = [arrayTblDis lastObject];
        
        if (![[mdict valueForKey:@"description"] isEqualToString:@""]&&![self issPace:[mdict valueForKey:@"description"]]&&![[mdict valueForKey:@"Priority"] isEqualToString:@"Priority"]) {
            
            //[self showArticleHeader];
            
            
            NSMutableDictionary *dictFind = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"description",@"Priority",@"Priority",@"0",@"fID",@"",@"FImg",@"0",@"fIndex",nil];
            
            [arrayTblDis addObject:dictFind];
            
            [tblView setHidden:NO];
            [tblView reloadData];
            // [self showArticleHeader];
            CGFloat scrollViewHeight = 0.0f;
            tblView.frame = CGRectMake(0, tblView.frame.origin.y, tblView.frame.size.width, tblView.frame.size.height+45);
            for (UIView* view in scrollView.subviews)
            {
                scrollViewHeight += view.frame.size.height;
            }
            [scrollView setContentSize:(CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+45))];
            
            // scrollHeight =scrollView.contentSize.height;
            
            CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
            if (bottomOffset.y>0) {
                [scrollView setContentOffset:bottomOffset animated:YES];
            }
        }
        
        else{
            
            UIAlertView *alertt = [[UIAlertView alloc] initWithTitle:@"" message:@"Complete Previous findings properly" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            
            [alertt show];
        }
    }
    else
    {
        NSMutableDictionary *mdict = [arrayTblDis lastObject];
        
        if (![[mdict valueForKey:@"description"] isEqualToString:@""]&&![self issPace:[mdict valueForKey:@"description"]]&&![[mdict valueForKey:@"Priority"] isEqualToString:@"Priority"]) {
            
            //[self showArticleHeader];
            
            
            NSMutableDictionary *dictFind = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"description",@"Priority",@"Priority",@"0",@"fID",@"",@"FImg",@"0",@"fIndex",nil];
            
            [arrayTblDis addObject:dictFind];
            
            [tblView setHidden:NO];
            [tblView reloadData];
            // [self showArticleHeader];
            CGFloat scrollViewHeight = 0.0f;
            tblView.frame = CGRectMake(0, tblView.frame.origin.y, tblView.frame.size.width, tblView.frame.size.height+74);
            for (UIView* view in scrollView.subviews)
            {
                scrollViewHeight += view.frame.size.height;
            }
            [scrollView setContentSize:(CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+74))];
            
            // scrollHeight =scrollView.contentSize.height;
            
            CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
            if (bottomOffset.y>0) {
                [scrollView setContentOffset:bottomOffset animated:YES];
            }
        }
        
        else{
            
            UIAlertView *alertt = [[UIAlertView alloc] initWithTitle:@"" message:@"Complete Previous findings properly" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            
            [alertt show];
        }
        
    }
    
    
}
-(IBAction)removeArticle:(id)sender{
    UIButton *btn = (UIButton *)sender;
    [self.view endEditing:YES];
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        if ([arrayTblDis count]>0) {
            
            
            tblView.frame = CGRectMake(0, tblView.frame.origin.y, scrollView.frame.size.width, tblView.frame.size.height-74);
            isRemovedArticle=YES;
            
            // NSLog(@"button tag will---%d",btn.tag);
            // NSLog(@"array tbl Dict will---%@",arrayTblDis);
            NSMutableDictionary *mDict = [arrayTblDis objectAtIndex:btn.tag-1];
            
            if ([[mDict valueForKey:@"fID"] intValue]>0) {
                NSString *strImageName = [mDict valueForKey:@"FImg"];
                NSString *strUrl=[NSString stringWithFormat:@"%@key=deletefinds&findid=%@",UrlMainGGQ,[mDict valueForKey:@"fID"]];
                NSString *jk=[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:jk]];
                connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                [self removeImage:strImageName];
            }
            if ([arrayTblDis count]>0) {
                [arrayTblDis removeObjectAtIndex:btn.tag-1];
            }
            
            
            [tblView reloadData];
            [scrollView setContentSize:(CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height-74))];
        }
        
    }
    else{
        
        if ([arrayTblDis count]>0) {
            
            
            tblView.frame = CGRectMake(0, tblView.frame.origin.y, scrollView.frame.size.width, tblView.frame.size.height-45);
            isRemovedArticle=YES;
            
            NSLog(@"button tag will---%ld",btn.tag-1);
            
            NSMutableDictionary *mDict = [arrayTblDis objectAtIndex:btn.tag-1];
            int intRemoveIndex= [[mDict valueForKey:@"fIndex"] intValue];
            if ([[mDict valueForKey:@"fID"] intValue]>0) {
                NSString *strImageName = [mDict valueForKey:@"FImg"];
                NSString *strUrl=[NSString stringWithFormat:@"%@key=deletefinds&findid=%@",UrlMainGGQ,[mDict valueForKey:@"fID"]];
                NSString *jk=[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:jk]];
                connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                [self removeImage:strImageName];
            }
            if ([arrayTblDis count]>0) {
                
                [arrayTblDis removeObjectAtIndex:intRemoveIndex];
                
                // [self next:nil];
                //[self Previous:nil];
                [tblFindings reloadData];
                [tblView reloadData];
                
                //[self rel]
            }
            else
            {
                [tblView setHidden:YES];
            }
            
            
            [scrollView setContentSize:(CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height-45))];
        }
    }
    NSLog(@"array tbl Dict will---%@",arrayTblDis);
    
}
-(void)relodeTbl
{
    [tblView reloadData];
    [tblFindings reloadData];
}
-(void)hideArticleHeader{
    [imgHdr setHidden:YES];
    [lblDisHdr setHidden:YES];
    [lblHdrAction setHidden:YES];
    [lblHdrPrio setHidden:YES];
    [lblSn setHidden:YES];
}
-(void)showArticleHeader{
    [imgHdr setHidden:NO];
    [lblDisHdr setHidden:NO];
    [lblHdrAction setHidden:NO];
    [lblHdrPrio setHidden:NO];
    [lblSn setHidden:NO];
}
- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    [txtPriorityTemp endEditing:NO];
    
    
    return YES;
}
-(void)resignKeyBoard
{
    
    if ([self isKeyboardOnScreen]) {
        [self.view endEditing:YES];
    }
    
}
- (BOOL)isKeyboardOnScreen
{
    BOOL isKeyboardShown = NO;
    NSArray *windows = [UIApplication sharedApplication].windows;
    if (windows.count > 1) {
        NSArray *wSubviews =  [windows[1]  subviews];
        if (wSubviews.count) {
            CGRect keyboardFrame = [wSubviews[0] frame];
            CGRect screenFrame = [windows[1] frame];
            if (keyboardFrame.origin.y+keyboardFrame.size.height == screenFrame.size.height) {
                isKeyboardShown = YES;
            }
        }
    }
    
    return isKeyboardShown;
}
-(void)showPickerForDropDown{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, -50, 300, 200)];
    
    pickerView.showsSelectionIndicator = YES;
    
    pickerView.tag=201;
    pickerView.dataSource = self;
    
    pickerView.delegate = self;
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:NSLocalizedString(@"Done", @"")]];
    
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    //closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    // closeButton.tag=myTextField.tag+10;
    CGRect textFieldRect =[self.view.window convertRect:combo1.view.bounds fromView:combo1.view];
    CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * 300;
    CGFloat numerator =midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =(MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0){
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else{
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)dismissActionSheet:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    UITextField *txtFld = (UITextField *)[self.view viewWithTag:btn.tag-10];
    // UITextField *txtField = (UITextField *)sender;
    // ActivePriority = txtField.text;
    CGSize contentSize = scrollView.frame.size;
    contentSize.width = 308;
    if(btn.tag>=210)
    {
        if (ActivePriority.length==0) {
            ActivePriority=@"High";
        }
        NSInteger indexNum=txtFld.tag-200;
        
        NSMutableDictionary *dictTemp = [arrayTblDis objectAtIndex:indexNum];
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        
        
        [newDict addEntriesFromDictionary:dictTemp];
        [newDict setObject:ActivePriority forKey:@"Priority"];
        [arrayTblDis replaceObjectAtIndex:indexNum withObject:newDict];
        txtFld.text=ActivePriority;
        [tblFindings reloadData];
        
        
    }
    
    //[actionSheet resignFirstResponder];
    // [actionSheet dismissWithClickedButtonIndex:1 animated:YES];
    
    CGRect viewFrame = self.view.frame;
    if (viewFrame.origin.y<0){
        viewFrame.origin.y = 0;
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
    // CGFloat scrollViewHeight = 0.0f;
    //    for (UIView* view in scrollView.subviews){
    //        scrollViewHeight += view.frame.size.height;
    //    }
    //
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
        
    }
}
#pragma mark PickerView delegate Mathods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag==201) {
        // return [arrayDropDownItems count];
        return 1;
    }
    else{
        return 3;
    }
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag==201){
        // return [arrayDropDownItems objectAtIndex:row];
        return @"";
    }
    else{
        if (row==0) {
            return @"High";
        }
        else if (row==1){
            return @"Medium";
        }
        else{
            return @"Low";
        }
    }
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag==201) {
        //        activeTitle= [arrayDropDownItems objectAtIndex:row];
        //        [btnCombo setTitle:[arrayDropDownItems objectAtIndex:row] forState:UIControlStateNormal];
        //        NSMutableDictionary *tempDataText = [[NSMutableDictionary alloc]
        //                                             initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)qNo],
        //                                             @"id",[arrayDropDownItems objectAtIndex:row], @"value",@"0", @"r",@"", @"w",nil];
        // [arayData addObject:tempDataText];
        // DdiIndex=[arayData indexOfObject:tempDataText];
        
        
    }
    else{
        // ActivePriority = [arrayPriority objectAtIndex:row];
        
        //NSMutableDictionary *dictTemp = [arrayTblDis objectAtIndex:0];
        
        
        if (row==1){
            ActivePriority= @"Medium";
        }
        else if (row==2){
            ActivePriority= @"Low";
        }
        else {
            ActivePriority= @"High";
        }
        
        txtPriorityTemp.text=ActivePriority;
        
    }
    
    //NSLog(@"active index is %d",activeFindIndex);
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        NSMutableDictionary *dictTemp = [arrayTblDis objectAtIndex:activeFindIndex];
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        
        
        [newDict addEntriesFromDictionary:dictTemp];
        [newDict setObject:ActivePriority forKey:@"Priority"];
        [arrayTblDis replaceObjectAtIndex:activeFindIndex withObject:newDict];
        
        [tblFindings reloadData];
        
        
        [popoverController dismissPopoverAnimated:YES];
        [self.view endEditing:YES];
    }
}

-(IBAction)sendData:(id)sender
{
    AppDelegate  *appD =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [activity setHidden:NO];
    [activity startAnimating];
    [DejalBezelActivityView removeView];
    //  NSString *companyKey=appD.companyKey;
    //   NSString *aid=appD.AutoId;
    UIButton *btn = (UIButton *)sender;
    
    if ([btn.titleLabel.text isEqualToString:@"Cancel"]) {
        NSArray *array = [self.navigationController viewControllers];
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
    }
    else
    {
        NSMutableDictionary *dictFind = [arrayTblDis lastObject];
        // NSLog(@"findings on save %@",dictFind);
        if ([[dictFind valueForKey:@"Priority"] isEqualToString:@"Priority"]||[[dictFind valueForKey:@"description"] isEqualToString:@""]||[self issPace:[dictFind valueForKey:@"description"]]){
            
            UIAlertView *alertt = [[ UIAlertView alloc] initWithTitle:@"" message:@"Please enter findings properly" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [activity stopAnimating];
            [activity setHidden:YES];
            [DejalBezelActivityView removeView];
            [alertt show];
        }
        else{
            // NSMutableDictionary *dictFind = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"description",@"Priority",@"Priority",@"0",@"fID",@"",@"FImg",@"0",@"fIndex",nil];
            
            isSendPressed=YES;
            // NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:@"ad",@"and", nil];
            NSError *error;
            for (int i=0; i<[self->arrayTblDis count]; i++){
                NSMutableDictionary *dictFind = [arrayTblDis objectAtIndex:i];
                
                NSIndexPath *myIP = [NSIndexPath indexPathForRow:i inSection:0];
                UITableViewCell *cell = [tblView cellForRowAtIndexPath:myIP];
                
                UITextField *tfDis = (UITextField *)[cell.contentView viewWithTag:i+501];
                UITextField *tfPrio = (UITextField *)[cell.contentView viewWithTag:i+200];
                
                // NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
                // [dateformate setDateFormat:@"mmss"]; // Date formater
                //  NSString *date = [dateformate stringFromDate:[NSDate date]]; // Convert date to string
                NSString *strImageName;
                if ([[UIImage imageNamed:[NSString stringWithFormat:@"%@",[dictFind valueForKey:@"FImg"]]] isEqual:[UIImage imageNamed:@"noimage.png"]]) {
                    
                    strImageName=@"";
                }
                else{
                    strImageName = [dictFind valueForKey:@"FImg"];
                    //strImageName  = [NSString stringWithFormat:@"img%d%@.jpg",i,date];
                    
                }
                NSMutableDictionary *tempDataTextFld = [[NSMutableDictionary alloc]
                                                        initWithObjectsAndKeys:
                                                        tfDis.text, @"desc",tfPrio.text, @"Pty",[dictFind valueForKey:@"fID"],@"id" ,strImageName,@"fimg" ,nil];
                [arrayFindings addObject:tempDataTextFld];
            }
            if ([arrayTblDis count]>0) {
                NSMutableDictionary *tempDataTextFld = [[NSMutableDictionary alloc]init];
                [arrayFindings addObject:tempDataTextFld];
            }
            [arrayFindings removeLastObject];
            // [arayData addObject:strdata];
            if ([arayData count]==0) {
                for (int i=0; i<[arrayDataStore count]; i++) {
                    NSDictionary *dictTemp = [arrayDataStore objectAtIndex:i];
                    
                    //  NSLog(@"dict to send %@",dictTemp);
                    NSDictionary *dictToSend = [NSDictionary dictionaryWithObjectsAndKeys:[dictTemp valueForKey:@"id"],@"id",[dictTemp valueForKey:@"rating"],@"r",[dictTemp valueForKey:@"w"],@"w",[dictTemp valueForKey:@"value"],@"value", nil];
                    [arayData addObject:dictToSend];
                }
                
            }
            
            // NSLog(@"arraydata on send %@",arayData);
            
            NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:arrayFindings options:NSJSONWritingPrettyPrinted error:&error];
            
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arayData options:NSJSONWritingPrettyPrinted error:&error];
            // NSData *dt = [NSData data];
            
            NSString *jsonString = [[NSString alloc] initWithData:jsonData1 encoding:NSUTF8StringEncoding];
            
            NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
            
            NSString *strdata = [NSString stringWithFormat:@"%@",jsonString];
            
            NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
            
            NSString * newReplacedStringfirst = [strdatafirst stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            NSString * firstStr = [newReplacedStringfirst stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString * newReplacedString = [strdata stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            NSString * lastStr = [newReplacedString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            //convert object to data
            
            [responseData setLength:0];
            // NSUserDefaults *df=[NSUserDefaults standardUserDefaults];
            // NSInteger  uID=[[df stringForKey:@"surveyId"] integerValue];
            //NSString *k=[NSString stringWithFormat:@"http://www.gogetquality.com/application/audit!saveMSurveyProgress.cc?survey=%@&Sid=%d&status=save&findings=%@",firstStr,uID,lastStr];
            NSString *k1=[NSString stringWithFormat:@"%@Key=insertAuditNew&audit=%@&Aid=%@&status=Save&findings=%@&CompanyKey=%@",UrlMainGGQ,firstStr,aId,@"",strGGQCompanyKey];
            NSString *jk=[k1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            //  NSLog(@"url to send data %@",k);
            
            //"r" : "0",    "id" : "160",    "value" : "N\/A",    "w" : "0"
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:jk]];
            
            // Create url connection and fire request
            connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        }
    }
    
}
-(IBAction)markAsDone:(id)sender
{
    NSMutableDictionary *dictFind = [arrayTblDis lastObject];
    if (isNextQ==YES) {
        UIAlertView *alerttt = [[UIAlertView alloc] initWithTitle:@"Rating ?" message:@"Please rate question" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alerttt show];
    }
    
    else if ([[dictFind valueForKey:@"Priority"] isEqualToString:@"Priority"]||[[dictFind valueForKey:@"description"] isEqualToString:@""]){
        
        UIAlertView *alertt = [[ UIAlertView alloc] initWithTitle:@"" message:@"Please enter findings properly" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [activity stopAnimating];
        [activity setHidden:YES];
        [DejalBezelActivityView removeView];
        [alertt show];
    }
    
    else{
        isSendPressed=NO;
        AppDelegate  *appD =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        //  NSString *companyKey=appD.companyKey;
        //  NSString *aid=appD.AutoId;
        NSError *error;
        isMarkAsDonePresed=YES;
        
        [activity setHidden:NO];
        [activity startAnimating];
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Submitting Feedback..."];
        for (int i=0; i<[self->arrayTblDis count]; i++){
            NSIndexPath *myIP = [NSIndexPath indexPathForRow:i inSection:0];
            UITableViewCell *cell = [tblView cellForRowAtIndexPath:myIP];
            UITextField *tfDis = (UITextField *)[cell.contentView viewWithTag:i+501];
            UITextField *tfPrio = (UITextField *)[cell.contentView viewWithTag:i+200];
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"mmss"]; // Date formater
            NSString *date = [dateformate stringFromDate:[NSDate date]]; // Convert date to string
            NSString *strImageName;
            if ([[UIImage imageNamed:[NSString stringWithFormat:@"%@",[dictFind valueForKey:@"FImg"]]] isEqual:[UIImage imageNamed:@"noimage.png"]]) {
                
                strImageName=@"";
            }
            else{
                strImageName  = [dictFind valueForKey:@"FImg"];
                
            }
            
            NSMutableDictionary *tempDataTextFld = [[NSMutableDictionary alloc]
                                                    initWithObjectsAndKeys:
                                                    tfDis.text, @"desc",tfPrio.text, @"Pty",strImageName,@"fimg",@"Open",@"status",[dictFind valueForKey:@"fID"],@"id",nil];
            [arrayFindings addObject:tempDataTextFld];
        }
        if ([arrayTblDis count]==0){
            NSMutableDictionary *tempDataTextFld = [[NSMutableDictionary alloc]
                                                    init];
            [arrayFindings addObject:tempDataTextFld];
        }
        
        if ([arayData count]==0) {
            // arayData=[arrayDataTemp mutableCopy];
            for (int i=0; i<[arrayDataStore count]; i++) {
                NSDictionary *dictTemp = [arrayDataStore objectAtIndex:i];
                //  NSLog(@"dict to send %@",dictTemp);
                NSDictionary *dictToSend = [NSDictionary dictionaryWithObjectsAndKeys:[dictTemp valueForKey:@"id"],@"id",[dictTemp valueForKey:@"rating"],@"r",[dictTemp valueForKey:@"w"],@"w",[dictTemp valueForKey:@"value"],@"value", nil];
                [arayData addObject:dictToSend];
            }
        }
        NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:arrayFindings options:NSJSONWritingPrettyPrinted error:&error];
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arayData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData1 encoding:NSUTF8StringEncoding];
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSString *strdata = [NSString stringWithFormat:@"%@",jsonString];
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        NSString * newReplacedStringfirst = [strdatafirst stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString * firstStr =newReplacedStringfirst;// [newReplacedStringfirst stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString * newReplacedString = [strdata stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString * lastStr =newReplacedString; //[newReplacedString stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([arrayTblDis count]==0) {
            lastStr=@"0";
        }
        [responseData setLength:0];
        if (firstStr.length>0) {
            NSString *k1=[NSString stringWithFormat:@"%@Key=insertAuditNew&audit=%@&Aid=%@&status=Done&findings=%@&CompanyKey=%@",UrlMainGGQ,firstStr,aId,@"",strGGQCompanyKey];
            NSString *jk=[k1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:jk]];
            //NSLog(@"url on mark as done--%@",k);
            
            connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        }
        
        else
        {
            UIAlertView *alerts = [[UIAlertView alloc] initWithTitle:@"Audit ?" message:@"Please Add Answers" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            
            [alerts show];
        }
        // NSArray *array = [self.navigationController viewControllers];
        // [self.navigationController popToViewController:[array objectAtIndex:2] animated:YES];
        // SignatureViewController *sigh = [[SignatureViewController alloc] initWithNibName:@"SignatureView" bundle:nil];
        //
        //[self presentViewController:sigh animated:YES completion:nil];
    }
}
-(IBAction)back:(id)sender
{
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
    //                                                             bundle: nil];
    //    DashBoardView
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardView"];
    //    [self.navigationController pushViewController:objByProductVC animated:NO];
    
//    if ([_isFromSendEmail  isEqual: @"true"]){
//        [self.navigationController popViewControllerAnimated:false];
//    }
    NSUserDefaults *defsIsService=[NSUserDefaults standardUserDefaults];
    
    BOOL isServiceSurvey=[defsIsService boolForKey:@"isServiceSurvey"];
    
    if (isServiceSurvey) {
        
        BOOL isMechanicalSurvey=[defsIsService boolForKey:@"isMechanicalSurvey"];
        
        if (isMechanicalSurvey) {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[SendMailMechanicaliPadViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            SendMailMechanicaliPadViewController *myController = (SendMailMechanicaliPadViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
        } else {
            
            if ([_isFromSendEmail  isEqual: @"true"])
            {
                    [self.navigationController popViewControllerAnimated:false];
            }
            else
            {
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[ServiceSendMailViewController class]]) {
                        index=k1;
                        //break;
                    }
                }
                ServiceSendMailViewController *myController = (ServiceSendMailViewController *)[self.navigationController.viewControllers objectAtIndex:index];
                // myController.typeFromBack=_lbl_LeadInfo_Status.text;
                [self.navigationController popToViewController:myController animated:NO];
            }
           
            
        }
        
        
    }
    else
    {
        BOOL isNewSalesSurvey=[defsIsService boolForKey:@"isNewSalesSurvey"];
        
        if (isNewSalesSurvey)
        {
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[SalesNew_SendEmailVC class]]) {
                    index=k1;
                    //break;
                }
            }
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesNew_SendEmailVC"
                                                                     bundle: nil];
            SalesNew_SendEmailVC
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SalesNew_SendEmailVC"];
            objByProductVC.strForSendProposal = _strStageType;
            [self.navigationController popToViewController:objByProductVC animated:NO];
        }
        else  //Old Sales Survey
        {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[SendMailViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            SendMailViewController *myController = (SendMailViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            myController.strForSendProposal = _strStageType;
            [self.navigationController popToViewController:myController animated:NO];
        }
    }
}
//-(IBAction)gotoSetting{
//    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
//        if([UIScreen mainScreen].bounds.size.height == 480.0)
//        {
//            //move to your iphone5 xib
//            //SettingViewController *svc = [[SettingViewController alloc] initWithNibName:@"SettingViewController_iPhone5" bundle:nil];
//            Setting *svc = [[ Setting alloc] initWithNibName:@"Setting3.5" bundle:nil];
//            [self.navigationController pushViewController:svc animated:YES];
//        }
//        else{
//            //move to your iphone4s xib
//            //            SettingViewController *svc = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
//            Setting *svc = [[ Setting alloc] initWithNibName:@"Setting" bundle:nil];
//            [self.navigationController pushViewController:svc animated:YES];
//        }
//    }
//}

-(void)showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    UITextField *txtfld = (UITextField *)sender;
    TSActionSheet *actionSheett = [[TSActionSheet alloc] initWithTitle:@"Select Priority"];
    
    // [actionSheett getPOint:txtfld.frame.origin.x :txtfld.frame.origin.y];
    // [actionSheett destructiveButtonWithTitle:@"" block:nil];
    [actionSheett addButtonWithTitle:@"High" block:^{
        txtfld.text=@"High";
    }];
    [actionSheett addButtonWithTitle:@"Medium" block:^{
        txtfld.text=@"Medium";
    }];
    [actionSheett addButtonWithTitle:@"Low" block:^{
        txtfld.text=@"Low";
    }];
    [actionSheett cancelButtonWithTitle:@"Cancel" block:nil];
    // actionSheett.cornerRadius = 5;
    
    [actionSheett showWithTouch:event];
}
-(void)showPopover:(id)sender forEvent:(UIEvent*)event
{
    UITableViewController *tableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    tableViewController.view.frame = CGRectMake(0,0, 320, 400);
    TSPopoverController *popoverControllerr = [[TSPopoverController alloc] initWithContentViewController:tableViewController];
    
    // popoverControllerr.cornerRadius = 5;
    popoverControllerr.titleText = @"change order";
    popoverControllerr.popoverBaseColor = [UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    popoverControllerr.popoverGradient= NO;
    [popoverControllerr showPopoverWithTouch:event];
    
}
-(void)addView
{
    UIView *vieww = [arrayViews objectAtIndex:swipCount];
    [vieww setFrame:CGRectMake(0, 0, vieww.frame.size.width, vieww.frame.size.height)];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, vieww.frame.size.height, scrollView.frame.size.width, 2.0f);
    bottomBorder.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"black_button.png"]].CGColor;
    [vieww.layer addSublayer:bottomBorder];
    [scrollView addSubview:vieww];
    for (UISlider *slider in [vieww subviews]) {
        if ([slider isKindOfClass:[UISlider class]]) {
            
            if (slider.value<1.0) {
                isNextQ=YES;
            }
            else
            {
                isNextQ=NO;
            }
        }
    }
    if (swipCount==0) {
        //[self showFindingButtons];
        
        if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            float scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height+tblView.frame.size.height+60;
            [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
        }
        else
        {
            float tblheight = [arrayTblDis count]*45;
            tblView = [[UITableView alloc]initWithFrame:CGRectMake(0, btnMore.frame.origin.y+50, scrollView.frame.size.width, tblheight) style:UITableViewStylePlain ];
            int scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height+tblView.frame.size.height+75;
            [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
        }
        
    }
    if ([arrayTitles count]==1) {
        [btnNext setEnabled:NO];
        [btnPrev setEnabled:NO];
        [btnSkip setHidden:YES];
        [btnMarkAsdone setHidden:NO];
    }
}

-(IBAction)next:(id)sender
{
    // NSLog(@"view on change %@",arrayDataTemp);
    
    if (isNextQ==NO) {
        swipCount++;
        CheckComplition=0;
        if (swipCount<[arrayTitles count]) {
            lblQno.text = [NSString stringWithFormat:@"Question: %d/%lu",swipCount+1,(unsigned long)[arrayTitles count]];
            lblQCategory.text=[arrayGroupName objectAtIndex:swipCount];
            UIView *vieww = [arrayViews objectAtIndex:swipCount];
            [arrayViews replaceObjectAtIndex:swipCount withObject:vieww];
            for (UIView *view in [scrollView subviews]){
                [view removeFromSuperview];
            }
            if (swipCount>=0)
            {
                if (swipCount+1==[arrayTitles count]) {
                    [btnNext setEnabled:NO];
                    [btnMarkAsdone setHidden:NO];
                    [btnSkip setHidden:YES];
                    
                }
                //[self buttonAnimationForAddFindings];.
                //[btnSkip setHidden:YES];
                [btnMarkAsdone setHidden:NO];
                //   [self showFindingButtons];
                //scrollView.scrollEnabled=YES;
                if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
                    float scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height+tblView.frame.size.height+60;
                    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
                }
                else
                {
                    int scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height+tblView.frame.size.height+75;
                    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
                }
                
            }
            else
            {
                
                if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
                    float scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height;
                    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
                    
                }
                else
                {
                    float scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height+50;
                    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
                }
                
            }
            [self addView];
        }
        btnPrev.enabled=YES;
        [btnPrev setHidden:NO];
        
        if (swipCount==arrayTitles.count-1) {
            
            [btnNext setHidden:YES];
            
        }
    }
    else
    {
        UIAlertView *alertt = [[UIAlertView alloc] initWithTitle:@"Rating ?" message:@"Please rate current question" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertt show];
        
    }
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height)];
    
}

//-(void)showFindingButtons
//{
//    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
//        if (![[dict valueForKey:@"type"] isEqualToString:@"Customer Survey"]) {
//            btnMore = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//            UIView *view ;
//            if (swipCount>=0) {
//                view = [arrayViews objectAtIndex:swipCount];
//            }
//            btnMore.frame = CGRectMake(160,view.frame.size.height+30 , 120, 30);
//           // NSLog(@"btnMore Y exis = %f",view.frame.origin.y+view.frame.size.height+10);
//            [btnMore setBackgroundImage:[UIImage imageNamed:@"black_button.png"] forState:UIControlStateNormal];
//            [btnMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [btnMore setTitle:@"Add Findings" forState:UIControlStateNormal];
//            [btnMore addTarget:nil action:@selector(addArticle:) forControlEvents:UIControlEventTouchUpInside];
//            [scrollView addSubview:btnMore];
//            // buttonFrame.origin.y+=btnMore.frame.size.height+20;
//            float tblheight = [arrayTblDis count]*45;
//            tblView = [[UITableView alloc]initWithFrame:CGRectMake(0, btnMore.frame.origin.y+50, scrollView.frame.size.width, tblheight) style:UITableViewStylePlain ];
//            tblView.delegate=self;
//            tblView.dataSource=self;
//            tblView.scrollEnabled=NO;
//            [tblView setBackgroundColor:[UIColor clearColor]];
//            [tblView setSeparatorColor:[UIColor clearColor]];
//            [tblView setHidden:NO];
//            [btnMore setHidden:NO];
//            [scrollView addSubview:imgHdr];
//            [scrollView addSubview:tblView];
//        }
//    }
//    else{
//
//        if (![[dict valueForKey:@"type"] isEqualToString:@"Customer Survey"]) {
//            btnMore = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//            UIView *view ;
//            if (swipCount>=0) {
//                view = [arrayViews objectAtIndex:swipCount];
//            }
//            btnMore.frame = CGRectMake(512,view.frame.size.height+30 ,177,51);
//           // NSLog(@"btnMore Y exis = %f",view.frame.size.height+10);
//            [btnMore setBackgroundImage:[UIImage imageNamed:@"black_button.png"] forState:UIControlStateNormal];
//            [btnMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [btnMore setTitle:@"Add Findings" forState:UIControlStateNormal];
//            btnMore.titleLabel.font = [UIFont boldSystemFontOfSize:20];
//            [btnMore addTarget:nil action:@selector(addArticle:) forControlEvents:UIControlEventTouchUpInside];
//            [scrollView addSubview:btnMore];
//            // buttonFrame.origin.y+=btnMore.frame.size.height+20;
//            float tblheight = [arrayTblDis count]*74;
//            tblView = [[UITableView alloc]initWithFrame:CGRectMake(0, btnMore.frame.origin.y+63, scrollView.frame.size.width, tblheight) style:UITableViewStylePlain ];
//            tblView.delegate=self;
//            tblView.dataSource=self;
//            tblView.scrollEnabled=NO;
//            [tblView setBackgroundColor:[UIColor clearColor]];
//            [tblView setSeparatorColor:[UIColor clearColor]];
//            [tblView setHidden:NO];
//            [btnMore setHidden:NO];
//            [scrollView addSubview:imgHdr];
//            [scrollView addSubview:tblView];
//            CGFloat scrollViewHeight = 0.0f;
//            for (UIView* view in scrollView.subviews){
//                scrollViewHeight += view.frame.size.height;
//            }
//          //[scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollViewHeight/2)];
//            //NSLog(@"scroll content size on add srticle %f",scrollViewHeight);
//        }
//    }
//}

-(IBAction)Previous:(id)sender
{
    //scrollView.scrollEnabled=NO;
    [scrollView setContentOffset:CGPointMake(0,0) animated:NO];
    
    btnNext.enabled=YES;
    
    [btnNext setHidden:NO];
    
    //tblView.hidden=YES;
    [btnMarkAsdone setHidden:NO];
    [btnSkip setHidden:NO];
    //swipCount--;
    if (swipCount>=0)
    {
        if (swipCount>0)
        {
            swipCount--;
        }
        
        if (arrayViews.count == 0) //Ruchika Code Adding one more if for arrayViewCount == 0 20 Oct 2021
        {
            if ([_isFromSendEmail  isEqual: @"true"]){
                [self.navigationController popViewControllerAnimated:false];
                
            }
        }
        else  //Previous code was without else
        {
            UIView *vieww = [arrayViews objectAtIndex:swipCount];
            [arrayViews replaceObjectAtIndex:swipCount withObject:vieww];
            lblQno.text = [NSString stringWithFormat:@"Question: %d/%lu",swipCount+1,(unsigned long)[arrayTitles count]];
            lblQCategory.text=[arrayGroupName objectAtIndex:swipCount];
            for (UIView *view in [scrollView subviews]) {
                [view removeFromSuperview];
            }
            [self addView];
            if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
                //float scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height;
                //[scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
                //  [self showFindingButtons];
                float scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height+tblView.frame.size.height+60;
                [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
            }
            else
            {
                //  [self showFindingButtons];
                float scrollContentHeight = vieww.frame.size.height+btnMore.frame.size.height+tblView.frame.size.height+60;
                [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollContentHeight)];
            }
        }
        
    }
    
    
    if (swipCount==0){
        btnPrev.enabled=NO;
        [btnPrev setHidden:YES];
        
    }
    isNextQ=NO;
    
}
-(void)checkNoneRadioButton:(id)sender
{
    isNextQ=NO;
    UIButton *btn = (UIButton *)sender;
    
    NSString *strWatage;
    NSInteger dictIndex=-1;
    for (NSDictionary *dictTemp in arrayDataStore) {
        if ([[NSString stringWithFormat:@"%ld",(long)btn.tag] isEqualToString:[dictTemp valueForKey:@"id"]] ) {
            strWatage=[dictTemp valueForKey:@"w"];
        }
    }
    UIView *view = [arrayViews objectAtIndex:swipCount];
    for (UIButton *Btns in [view subviews]) {
        if ([Btns isKindOfClass:[UIButton class]]) {
            [Btns setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        }
    }
    [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    NSString *strRating;
    
    if ([btn.titleLabel.text caseInsensitiveCompare:@"Yes"]== NSOrderedSame ) {
        strRating = [NSString stringWithFormat:@"10"];
    }
    if ([btn.titleLabel.text caseInsensitiveCompare:@"No"]== NSOrderedSame ) {
        strRating = [NSString stringWithFormat:@"1"];
    }
    if ([btn.titleLabel.text caseInsensitiveCompare:@"N/A"]== NSOrderedSame ) {
        strRating = [NSString stringWithFormat:@"10"];
    }
    // add data in array to submitt
    
    for (int i =0; i<[arayData count]; i++) {
        NSMutableDictionary *dicttemp = [arayData objectAtIndex:i];
        if ([[dicttemp valueForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%ld",(long)btn.tag]]) {
            dictIndex= [arayData indexOfObject:dicttemp];
        }
    }
    
    if (dictIndex<0) {
        NSMutableDictionary *dictt = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)btn.tag],@"id",strRating ,@"r" ,btn.titleLabel.text,@"value",strWatage,@"w",nil];
        [arayData addObject:dictt];
    }
    else
    {
        NSMutableDictionary *dictt = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)btn.tag],@"id",strRating ,@"r" ,btn.titleLabel.text,@"value",strWatage,@"w",nil];
        //[arayData addObject:dictt];
        [arayData replaceObjectAtIndex:dictIndex withObject:dictt];
    }
    // NSLog(@"watage is %@",arayData);
    
}
-(void)buttonAnimationForAddFindings
{
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         btnSaveProgress.hidden=NO;
                         btnMarkAsdone.hidden=NO;
                         btnNext.frame= CGRectMake(btnNext.frame.origin.x, buttonsY, btnNext.frame.size.width, btnNext.frame.size.height);
                         btnPrev.frame= CGRectMake(btnPrev.frame.origin.x, buttonsY, btnPrev.frame.size.width, btnPrev.frame.size.height);
                     }
                     completion:nil];
}
-(void)onPeviousAnimation
{
    [UIView animateWithDuration:0.0 delay:0.0 options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         btnSaveProgress.hidden=YES;
                         btnMarkAsdone.hidden=NO;
                         btnNext.frame= CGRectMake(btnNext.frame.origin.x, btnSaveProgress.frame.origin.y, btnNext.frame.size.width, btnNext.frame.size.height);
                         btnPrev.frame= CGRectMake(btnPrev.frame.origin.x, btnMarkAsdone.frame.origin.y, btnPrev.frame.size.width, btnPrev.frame.size.height);
                     }
                     completion:nil];
}
-(void)resetAll{
    RbiIndex=-1;
    CbIIndex=-1;
    TfiIndex=-1;
    TviIndex=-1;
    DdiIndex=-1;
    noneIindex=-1;
    QCount=0;
    checkBoxCount=0;
    isCompleted=NO;
    isSendPressed=NO;
    isCheckBox=NO;
    isRadioBtn=NO;
    radioNum=0;
    indexradio=0;
    sIndex=-1;
    CheckComplition=0;
    ratingindex=-1;
    tesint =0;
    mandCount=0;
}
-(IBAction)skipQuestion:(id)sender
{
    //NSArray *array = [self.navigationController viewControllers];
    // [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
    isNextQ=NO;
    [self next:nil];
}
-(BOOL)disablesAutomaticKeyboardDismissal
{
    return YES;
}
#pragma mark camera mathod

-(IBAction)CaptureImage:(id)sender
{
    isFromImage=YES;
    UIButton *btn = (UIButton *)sender;
    btnActiveFindingImage = btn;
    
    activeFindIndex=btnActiveFindingImage.tag-600;
    
    
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Update or Preview ?" message:@"" delegate:self cancelButtonTitle:@"Update Image" otherButtonTitles:@"Preview", nil];
    
    alertview.tag=2;
    [alertview show];
    
    countSwipe=swipCount;
    // UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    // picker.delegate = self;
    // picker.allowsEditing = YES;
    // picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    //[self presentViewController:picker animated:YES completion:NULL];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    isFromImage=YES;
    swipCount=countSwipe;
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //[arrayFindImages replaceObjectAtIndex:btnActiveFindingImage.tag-600 withObject:chosenImage];
    //[btnActiveFindingImage setImage:chosenImage forState:UIControlStateNormal];
    // btnActiveFindingImage.imageView.image=chosenImage;
    //[self uploadImage];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"mmss"]; // Date formater
    NSString *date = [dateformate stringFromDate:[NSDate date]];
    
    NSString *strImageName = [NSString stringWithFormat:@"img%@%@.jpg",aId,date];
    
    NSMutableDictionary *dictTemp = [arrayTblDis objectAtIndex:activeFindIndex];
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    
    
    [newDict addEntriesFromDictionary:dictTemp];
    [newDict setObject:strImageName forKey:@"FImg"];
    [arrayTblDis replaceObjectAtIndex:activeFindIndex withObject:newDict];
    
    
    
    // NSLog(@"image name picker indexxxxx --- %d",activeFindIndex);
    
    [self saveDataFromPicker:strImageName :chosenImage];
    [tblFindings reloadData];
    [tblView reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    swipCount=countSwipe;
    isFromImage=YES;
}
#pragma mark upload Image
-(void)uploadImage
{
    for (int i =0; i<[arrayTblDis count]; i++) {
        
        UIButton *btn = (UIButton *)[self.view viewWithTag:600+i];
        
        NSMutableDictionary *dictMutable = [arrayTblDis objectAtIndex:i];
        
        // UIImage *currentImg = [self scaleDown:btn.imageView.image withSize:CGSizeMake(200, 200)];
        NSString *strImageName = [dictMutable valueForKey:@"FImg"];
        NSData *imageData = UIImageJPEGRepresentation(btn.imageView.image, 0.001f);
        //if (![[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrayFindImages objectAtIndex:i]]] isEqual:[UIImage imageNamed:@"noimage.png"]])
        // {
        // setting up the URL to post to
        NSString *urlString = @"http://www.gogetquality.com/survey/FImageUploader.ashx";
        // setting up the request object now
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"-------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%ld.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        //    [body appendData:[NSData utf8DataWithFormat:
        //                      @"Content-Disposition: form-data; name=\"%@\"; filename=\"%ld.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n",
        //                      @"image_file",lgTime]];
        NSDictionary *dicttempImg = [arrayFindings objectAtIndex:i];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        //NSLog(@"uploaded images---> %@",[dicttempImg valueForKey:@"fimg"]);
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        // NSLog(@"%@",request);
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        //image.image=nil;
        if ([returnString isEqualToString:@"OK"]) {
            //  NSLog(@"image send succesfully");
        }
        // }
    }
    
}
-(IBAction)showImage:(id)sender {
    
    // UIButton *btn = (UIButton *)sender;
    CGSize destinationSize;
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        destinationSize = CGSizeMake(600, 600);
    }
    else{
        destinationSize = CGSizeMake(300, 300);
    }
    // NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.gogetquality.com/survey/C_findings/%@",[arrayFindImages objectAtIndex:indexPath.row]]]];
    // UIImage* imagecur = [[UIImage alloc] initWithData:imageData];
    
    UIGraphicsBeginImageContext(destinationSize);
    [btnActiveFindingImage.imageView.image drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    
    
    UIImageView *image = [[UIImageView alloc] initWithImage:newImage];
    
    
    
    zoomPopup *popup = [[zoomPopup alloc] initWithMainview:self.view andStartRect:btnActiveFindingImage.frame];
    [popup setBlurRadius:10];
    [popup showPopup:image];
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)saveData:(NSString *)imagName :(NSString *)StrImageType
{
    
    UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.gogetquality.com/survey/C_findings/%@",imagName]]]];
    
    // NSLog(@"%f,%f",ima,ge.size.width,image.size.height);
    
    // Let's save the file into Document folder.
    // You can also change this to your desktop for testing. (e.g. /Users/kiichi/Desktop/)
    // NSString *deskTopDir = @"/Users/kiichi/Desktop";
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    // If you go to the folder below, you will find those pictures
    
    
    // NSLog(@"saving png");
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@",docDir,StrImageType];
    // NSLog(@"image from web %@",pngFilePath);
    NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
    [data1 writeToFile:pngFilePath atomically:YES];
    
}
-(void)saveDataFromPicker:(NSString *)imagName :(UIImage *)image
{
    
    // UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://citizencop.in/manage/PledgeLogo/%@",imagName]]]];
    
    // NSLog(@"%f,%f",ima,ge.size.width,image.size.height);
    
    // Let's save the file into Document folder.
    // You can also change this to your desktop for testing. (e.g. /Users/kiichi/Desktop/)
    // NSString *deskTopDir = @"/Users/kiichi/Desktop";
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    // If you go to the folder below, you will find those pictures
    
    
    
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@",docDir,imagName];
    // NSLog(@"saving png %@",pngFilePath);
    NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
    [data1 writeToFile:pngFilePath atomically:YES];
    
}

-(UIImage *)getImageFromDirectory:(NSString *)imageName{
    NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *imagePath =  [directoryPath objectAtIndex:0];
    imagePath= [imagePath stringByAppendingPathComponent:imageName];
    
    // NSLog(@"image path will --%@",imagePath);
    NSData *dataa = [NSData dataWithContentsOfFile:imagePath];
    UIImage *img =  [UIImage imageWithData:dataa];
    return img;
}
- (void)removeImage:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *imagePath =  [directoryPath objectAtIndex:0];
    imagePath= [imagePath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:imagePath error:&error];
    if (success) {
        UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        //  [removeSuccessFulAlert show];
    }
    else
    {
        //  NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}
-(BOOL)issPace : (NSString *)strForcheck
{
    BOOL isSpace=NO;
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [strForcheck stringByTrimmingCharactersInSet:charSet];
    if ([trimmedString isEqualToString:@""]) {
        // it's empty or contains only white spaces
        isSpace=YES;
    }
    return isSpace;
}
@end
