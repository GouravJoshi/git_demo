//
//  EmailListDetail_iPhoneVC.swift
//  DPS
//
//  Created by NavinPatidar on 9/22/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import WebKit

class EmailListDetail_iPhoneVC: UIViewController, SFSafariViewControllerDelegate {
    //
    //MARK:
    //MARK: -------------IBOutlet--------------
    
    @IBOutlet weak var tv_EmailListDetail: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewContain: UIView!
    
    var webView = WKWebView()
    
    var aryEmailListDetail = NSMutableArray() , aryAttechment = NSMutableArray()
    var loader = UIAlertController()
    var EmailHistoryId = "" , strComeFrom = ""
    
    //MARK:
    //MARK: ---------life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if(strComeFrom == "HistoryDetail"){
            lblTitle.text = "Details"
            if(EmailHistoryId != ""){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.getEmailListHistoryAPI()
                }
            }
        }else if(strComeFrom == "HistoryMessageAgreement"){
            lblTitle.text = "Agreement"
            self.aryEmailListDetail = NSMutableArray()
            self.aryEmailListDetail = self.aryAttechment
            self.tv_EmailListDetail.reloadData()
            
        }else{
            lblTitle.text = "Message"
            if(EmailHistoryId != ""){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.getEmailListMessageAPI()
                }
            }
        }
        self.tv_EmailListDetail.tableFooterView = UIView()
        btnClose.layer.cornerRadius = DeviceType.IS_IPAD ? 20.0 : 15.0
    }
    //MARK:
    //MARK: ----------IBAction
    
    @IBAction func action_Back(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: false)
    }
    //MARK:
    //MARK: ----------API Calling
    
    func getEmailListHistoryAPI() {
        
        loader = loader_Show(controller: self, strMessage: "Fetching Detail...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        //let strURL = "https://pcrmsstaging.pestream.com//api/LeadNowAppToSalesProcess/GetEmailHistoryDetailsById?EmailHistoryId=\(EmailHistoryId)"
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "//api/LeadNowAppToSalesProcess/GetEmailHistoryDetailsById?EmailHistoryId=" + "\(EmailHistoryId)"

        
        print("getEmailListAPI API called -- %@",strURL)
        
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                {
                    if let dictData = response.value(forKey: "data"){
                        print(dictData)
                        if((dictData as! NSArray).count > 0){
                            self.aryEmailListDetail = (dictData as! NSArray).mutableCopy()as! NSMutableArray
                            self.tv_EmailListDetail.reloadData()
                        }
                        else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                        }
                    }
                    else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func getEmailListMessageAPI() {
        
        loader = loader_Show(controller: self, strMessage: "Fetching Detail...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        //let strURL = "https://pcrmsstaging.pestream.com//api/LeadNowAppToSalesProcess/GetEmailHistoryMessageById?EmailHistoryId=\(EmailHistoryId)"
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "//api/LeadNowAppToSalesProcess/GetEmailHistoryMessageById?EmailHistoryId=" + "\(EmailHistoryId)"
        //209946  "\(EmailHistoryId)"
        
        print("getEmailListAPI API called -- %@",strURL)
        
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL, responseStringComing: "EmailHistoryMessage") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                {
                  
                    if let dictData = response.value(forKey: "data")
                    {
                        print(dictData)
                        
                        var  str = String()
                        str = "\(response.value(forKey: "data") ?? "")"
                        if(str.count > 0){
                            var strEmailData = String()

                            strEmailData = str.replacingOccurrences(of: "Documents", with: "Documents//")
                            strEmailData = strEmailData.replacingOccurrences(of: "\\", with: "")
                            let resultPrefix = strEmailData.prefix(1)
                            let resultSuffix = strEmailData.suffix(1)
                            if resultPrefix == "\"" {
                                strEmailData = String(strEmailData.dropFirst())
                            }
                            if resultSuffix == "\"" {
                                strEmailData = String(strEmailData.dropLast())
                            }
                            if((dictData as! String).count > 0)
                            {
                                self.webView.frame = self.tv_EmailListDetail.frame
                                
                                
                                //var imgUrlNew = dictData as! String
                                
                                //self.webView.loadHTMLString(imgUrlNew , baseURL: nil)
                                self.webView.loadHTMLString(strEmailData as String, baseURL: nil)
                                
                                self.viewContain.addSubview(self.webView)
                                
                            }
                            else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                            }

                        }
                    }
                    else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    //MARK:- WKNavigationDelegate
    
    func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        
    }
    
    
    func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        
    }
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension EmailListDetail_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  aryEmailListDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(strComeFrom == "HistoryMessageAgreement"){
            let cell = tv_EmailListDetail.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "EmailListDetailHistoryAgreement1" : "EmailListDetailHistoryAgreement", for: indexPath as IndexPath) as! EmailListDetailHistory
            cell.imgView.layer.cornerRadius = 5.0
            cell.imgView.backgroundColor = UIColor.lightGray
            let dict = aryEmailListDetail.object(at: indexPath.row)as! NSDictionary
            let str = "\(dict.value(forKey: "MediaTypeSysName")!)"
            cell.lblTitle.text = "\(dict.value(forKey: "Title")!)"
            
            if(str == "Image"){
                if let imgURL = dict.value(forKey: "Path")
                {
                    if("\(imgURL)" == "<null>" || "\(imgURL)".count == 0 || "\(imgURL)" == " ")
                    {
                        cell.imgView.image = UIImage(named: "no_image.jpg")
                        cell.imgView.isHidden = true
                    }
                    else
                    {
                        
                        cell.imgView.isHidden = false
                        var imgUrlNew = imgURL as! String
                        imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\", with: "/")
                        imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\", with: "/")
                        imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\", with: "/")
                        imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\", with: "/")
                        imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\", with: "/")
                        imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                        imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                        
                        imgUrlNew = imgUrlNew.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                        
                        cell.imgView.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                    }
                }else{
                    cell.imgView.isHidden = true
                }
            }else if str.caseInsensitiveCompare("Document") == .orderedSame
            {
                let image: UIImage = UIImage(named: "exam")!
                cell.imgView.image = image
            }
            else if str.caseInsensitiveCompare("Audio") == .orderedSame
            {
                let image: UIImage = UIImage(named: "audio")!
                cell.imgView.image = image
            }
            else if str.caseInsensitiveCompare("Video") == .orderedSame
            {
                let image: UIImage = UIImage(named: "video")!
                cell.imgView.image = image
            }
           
            return cell
        }else{
            let cell = tv_EmailListDetail.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "EmailListDetailHistory1" : "EmailListDetailHistory", for: indexPath as IndexPath) as! EmailListDetailHistory
                   
            
            let dict = removeNullFromDict(dict: (aryEmailListDetail.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)

            
            
                   cell.lblDate.text = " "
                   if let str_strDueDate = dict.value(forKey: "EventDateClient"){
                       if("\(str_strDueDate)".count > 0 && "\(str_strDueDate)" != "<null>" && "\(str_strDueDate)" != " "){
                           var date = ""
                           var time = ""
                           date = Global().convertDate("\(str_strDueDate)")
                           time = Global().convertTime("\(str_strDueDate)")
                           cell.lblDate.text = "\(date) \(time)"
                       }
                   }
                   
                   cell.lblAction.text = "\(dict.value(forKey: "Event")!)" == "" ? " " : "\(dict.value(forKey: "Event")!)"
                   cell.lblReason.text = "\(dict.value(forKey: "Reason")!)" == "" ? " " : "\(dict.value(forKey: "Reason")!)"
                   cell.lblIPaddress.text = "\(dict.value(forKey: "IPAddress")!)" == "" ? " " : "\(dict.value(forKey: "IPAddress")!)"
                   cell.lblRecipient.text = "\(dict.value(forKey: "RecipientEmailAddress")!)" == "" ? " " : "\(dict.value(forKey: "RecipientEmailAddress")!)"
                   return cell
        }
       
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(strComeFrom == "HistoryMessageAgreement"){
            let dict = removeNullFromDict(dict: (aryEmailListDetail.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)

            
            let cell = tv_EmailListDetail.cellForRow(at: indexPath) as! EmailListDetailHistory
            
            if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Image") == .orderedSame
            {
                let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
                let vc = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as! PreviewImageVC
                vc.img = cell.imgView.image!
                self.present(vc, animated: false, completion: nil)
                
            }
            else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Document") == .orderedSame
            {
              
                let  strPdfNameNew = "\(dict.value(forKey: "Path") ?? "")".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                      guard let url = URL(string: "\(strPdfNameNew)") else { return }
                      UIApplication.shared.open(url)
//                let safariVC = SFSafariViewController(url: NSURL(string: encodedURL!)! as URL)
//                self.present(safariVC, animated: true, completion: nil)
//                  safariVC.delegate = self
                
            }
            else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Audio") == .orderedSame
            {
                
                let audioURL = "\("\( dict.value(forKey: "Path") ?? "")")"
                let player = AVPlayer(url: URL(string: audioURL)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
            else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Video") == .orderedSame
            {
                let audioURL = "\("\( dict.value(forKey: "Path") ?? "")")"
                let player = AVPlayer(url: URL(string: audioURL)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
    }
    }
// MARK: -
// MARK: ----------EmailList---------
class EmailListDetailHistory: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var lblRecipient: UILabel!
    
    @IBOutlet weak var lblAction: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblIPaddress: UILabel!
    
    @IBOutlet weak var lblReason: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

