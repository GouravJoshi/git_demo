//
//  EmailListNew_iPhoneVC.swift
//  DPS
//
//  Created by NavinPatidar on 9/21/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class EmailListNew_iPhoneVC: UIViewController, SFSafariViewControllerDelegate {
    
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_EmailList: UITableView!
    
    
    var aryEmailList = NSMutableArray()
    var loader = UIAlertController()
    var refType = "Lead"
    @objc var refNo = ""

    //MARK:
    //MARK: ---------life Cycle----------
    override func viewDidLoad() {
        super.viewDidLoad()
        getEmailListAPI()
    }
    
    //MARK:
    //MARK: ----------IBAction
    
    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    //MARK:
    //MARK: ----------API Calling
    
    func getEmailListAPI() {
        
        loader = loader_Show(controller: self, strMessage: "Fetching List...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        //let strURL = "https://pcrmsstaging.pestream.com//api/LeadNowAppToSalesProcess/GetEmailHistoryForMobile?CompanyKey=Production&Reftype=Lead&RefNo=17911"
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "//api/LeadNowAppToSalesProcess/GetEmailHistoryForMobile?CompanyKey=" + "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)" + "&Reftype=" + refType + "&RefNo=" + refNo
        
        print("getEmailListAPI API called -- %@",strURL)
         
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                {
                    if let dictData = response.value(forKey: "data"){
                        print(dictData)
                        if((dictData as! NSArray).count > 0){
                            self.aryEmailList = (dictData as! NSArray).mutableCopy()as! NSMutableArray
                            self.tv_EmailList.reloadData()
                        }
                        else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                        }
                    }
                    else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension EmailListNew_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  aryEmailList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_EmailList.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "EmailList1" : "EmailList", for: indexPath as IndexPath) as! EmailList
        
        cell.btnViewDetail.layer.cornerRadius = 8.0
        cell.btnViewMessage.layer.cornerRadius = 8.0
        cell.btnViewAgreeMent.layer.cornerRadius = 8.0
        
        cell.btnViewDetail.layer.borderWidth = 1.0
        cell.btnViewMessage.layer.borderWidth = 1.0
        cell.btnViewAgreeMent.layer.borderWidth = 1.0
        
        cell.btnViewDetail.layer.borderColor = hexStringToUIColor(hex: appThemeColor).cgColor
        cell.btnViewMessage.layer.borderColor = hexStringToUIColor(hex: appThemeColor).cgColor
        cell.btnViewAgreeMent.layer.borderColor = hexStringToUIColor(hex: appThemeColor).cgColor
        
        cell.btnViewDetail.addTarget(self, action: #selector(ViewDetail), for: .touchUpInside)
        cell.btnViewMessage.addTarget(self, action: #selector(ViewMessage), for: .touchUpInside)
        cell.btnViewAgreeMent.addTarget(self, action: #selector(ViewAgreeMent), for: .touchUpInside)
        
        cell.btnViewDetail.tag = indexPath.row
        cell.btnViewMessage.tag = indexPath.row
        cell.btnViewAgreeMent.tag = indexPath.row
        
        cell.imgView.layer.cornerRadius = DeviceType.IS_IPAD ? 32.0 : 25.0
        cell.imgView.layer.borderWidth = 1.0
        cell.imgView.layer.borderColor = hexStringToUIColor(hex: appThemeColor).cgColor
        
        let dict = removeNullFromDict(dict: (aryEmailList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        
        cell.lblSubject.text = "\(dict.value(forKey: "EmailSubject")!)"
        
        cell.lblDate.text = " "
        if let str_strDueDate = dict.value(forKey: "CreatedDate"){
            if("\(str_strDueDate)".count > 0 && "\(str_strDueDate)" != "<null>" && "\(str_strDueDate)" != " "){
                var date = ""
                var time = ""
                date = Global().convertDate("\(str_strDueDate)")
                time = Global().convertTime("\(str_strDueDate)")
                cell.lblDate.text = "\(date) \(time)"
            }
        }
        
        cell.lblTo.text = "\(dict.value(forKey: "EmailTo")!)"
        cell.lblFrom.text = "\(dict.value(forKey: "EmailFrom")!)"
        cell.lblName.text = "\(dict.value(forKey: "CreatedByName")!)"
        let imgUrlNew = replaceBackSlasheFromUrl(strUrl: "\(dict.value(forKey: "CreatedByUserImage")!)")
        cell.imgView.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    @objc func ViewDetail(sender : UIButton) {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailListDetail_iPhoneVC") as? EmailListDetail_iPhoneVC
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc!.modalTransitionStyle = .coverVertical
        let dict = aryEmailList.object(at: sender.tag)as! NSDictionary
        vc!.strComeFrom = "HistoryDetail"
        vc!.EmailHistoryId = "\(dict.value(forKey: "EmailHistoryId")!)"
        self.present(vc!, animated: false, completion: nil)
    }
    @objc func ViewMessage(sender : UIButton) {
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailListDetail_iPhoneVC") as? EmailListDetail_iPhoneVC
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc!.modalTransitionStyle = .coverVertical
        let dict = aryEmailList.object(at: sender.tag)as! NSDictionary
        vc!.strComeFrom = "HistoryMessage"
        vc!.EmailHistoryId = "\(dict.value(forKey: "EmailHistoryId")!)"
        self.present(vc!, animated: false, completion: nil)
    }
    
    @objc func ViewAgreeMent(sender : UIButton) {
        self.view.endEditing(true)
              let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailListDetail_iPhoneVC") as? EmailListDetail_iPhoneVC
              vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
              vc!.modalTransitionStyle = .coverVertical
              let dict = aryEmailList.object(at: sender.tag)as! NSDictionary
             vc!.aryAttechment = (dict.value(forKey: "LeadMedias") as! NSArray).mutableCopy()as! NSMutableArray
              vc!.strComeFrom = "HistoryMessageAgreement"
              self.present(vc!, animated: false, completion: nil)
    }
}

// MARK: -
// MARK: ----------EmailList---------
class EmailList: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblSubject: UILabel!
    
    @IBOutlet weak var lblFrom: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnViewMessage: UIButton!
    @IBOutlet weak var btnViewDetail: UIButton!
    @IBOutlet weak var btnViewAgreeMent: UIButton!
    @IBOutlet weak var lblTo: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

