//
//  SwitchBranchVC.swift
//  DPS
//
//  Created by Saavan Patidar on 18/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SwitchBranchVC: UIViewController {

    // MARK: ---------------- Outlets ----------------
    @IBOutlet var btnSelectBranch: UIButton!
    @IBOutlet var btnRestBranch: UIButton!

    @IBOutlet var txtFldSelectBranch: ACFloatingTextField!
    
    // MARK: ---------------- Variable ----------------
    
    var dictLoginData = NSDictionary()
    var dictResponseBranch = NSDictionary()

    var dictLoginDataOrignal = NSDictionary()

    var dictBranch = NSDictionary()
    var loader = UIAlertController()
    var strCompanyKey = String()
    var strBranchId = String()
    var strDefaultBranchSysName = String()
    var arrBranch = NSMutableArray()
    var reDirectWhere = ""


    
    override func viewDidLoad() {
        super.viewDidLoad()

       setInitialValue()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: ---------------- Actions ----------------

    @IBAction func actionOnSelectBranch(_ sender: UIButton) {
        
        
        sender.tag = 1000
        gotoPopViewWithArray(sender: sender, aryData: arrBranch, strTitle: "Select Branch")

    }
    
    @IBAction func actionOnResetBranch(_ sender: Any) {
        
        if btnRestBranch.currentImage == UIImage(named: "NPMA_UnCheck")
        {
            btnRestBranch.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            

            for item in arrBranch
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SysName") ?? "")" == "\(dictLoginDataOrignal.value(forKey: "EmployeeBranchSysName") ?? "")" {
                    
                    txtFldSelectBranch.text = "\(dict.value(forKey: "Name") ?? "")"
                    break
                }
                
            }
            
        }
        else
        {
            btnRestBranch.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            

            
        }
        
    }
    
    
    @IBAction func actionOnCancel(_ sender: Any) {
        
        self.goBacckk(strTypeee: "Cancel")

    }
    
    
    @IBAction func actionOnSave(_ sender: Any) {
        
        if btnRestBranch.currentImage == UIImage(named: "NPMA_UnCheck")
        {
            self.getBranchDetail()

        }
        else // For Reset
        {
            nsud.setValue(dictLoginDataOrignal, forKey: "LoginDetails")
            nsud.synchronize()
            self.goBacckk(strTypeee: "Save")

        }
        
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
                
        self.goBacckk(strTypeee: "Back")

    }
    
    // MARK: - ------------------ Other Function -------------------
    
    fileprivate func goBacckk(strTypeee : String)  {

        if reDirectWhere == "Dashboard" {
            
            goToDashBoardVC()
            
        }else{
            
            if strTypeee == "Save" {
                
                goToDashBoardVC()

            } else {
                
                self.navigationController?.popViewController(animated: false)

            }
            
        }
        
    }
    
    fileprivate func setInitialValue()  {
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        dictLoginDataOrignal = nsud.value(forKey: "LoginDetailsOrignal") as! NSDictionary

        btnRestBranch.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        strBranchId = "\(dictLoginData.value(forKey: "EmployeeBranchId") ?? "")"
        strDefaultBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName") ?? "")"
        
        arrBranch = getBranches()
        
        for item in arrBranch
        {
            let dict = item as! NSDictionary
            
            if strDefaultBranchSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                txtFldSelectBranch.text = "\(dict.value(forKey: "Name") ?? "")"
                strBranchId =  "\(dict.value(forKey: "BranchMasterId") ?? "")"
                break
            }
        }
    }
    
    
    func getBranches() -> NSMutableArray {
        let temparyBranch = NSMutableArray()

        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                temparyBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
        return temparyBranch
    }
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func manageEmployeeBranch()
    {
        print(dictResponseBranch)
        
        let dictBranchData:[String : String] = ["BranchAddressLine1" : "\(dictResponseBranch.value(forKey: "BranchAddressLine1") ?? "")" ,
                                                "BranchAddressLine2" : "\(dictResponseBranch.value(forKey: "BranchAddressLine2") ?? "")",
                                                "BranchCity" : "\(dictResponseBranch.value(forKey: "BranchCity") ?? "")",
                                                "BranchCountry" : "\(dictResponseBranch.value(forKey: "BranchCountry") ?? "")",
                                                "BranchState" : "\(dictResponseBranch.value(forKey: "BranchState") ?? "")",
                                                "BranchZipCode" : "\(dictResponseBranch.value(forKey: "BranchZipCode") ?? "")"]
        
        
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        var dict = NSMutableDictionary()
        dict = NSMutableDictionary(dictionary: dictLoginData)
        
        dict.removeObject(forKey: "Branch")
        dict.setObject(dictBranchData, forKey: "Branch" as NSCopying)
        
        dict.removeObject(forKey: "EmployeeBranchSysName")
        dict.setValue("\(dictResponseBranch.value(forKey: "SysName") ?? "")", forKey: "EmployeeBranchSysName")
        
        
        dict.removeObject(forKey: "EmployeeBranchId")
        dict.setValue("\(dictResponseBranch.value(forKey: "BranchMasterId") ?? "")", forKey: "EmployeeBranchId")
        
        
        var dictCompany = NSMutableDictionary()
        dictCompany = NSMutableDictionary(dictionary: dictLoginData.value(forKey: "Company") as! NSDictionary)
        dictCompany.removeObject(forKey: "BusinessLicenceNo")
        dictCompany.setValue("\(dictResponseBranch.value(forKey: "BusinessLicenceNo") ?? "")", forKey: "BusinessLicenceNo")

        dict.removeObject(forKey: "Company")
        dict.setObject(dictCompany, forKey: "Company" as NSCopying)



        dictLoginData = dict as NSDictionary
        
        nsud.setValue(dictLoginData, forKey: "LoginDetails")
        nsud.synchronize()
        print(dict)
    }
    func goToDashBoardVC() {
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    // MARK: -  ------------------------------ API Calling------------------------------
    
    func getBranchDetail()
    {
        if isInternetAvailable()
        {
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            callApiToGetBranch()
        }
        else
        {
             Global().displayAlertController(alertMessage, "\(ErrorInternetMsg)", self)
                    
        }
        
    }
    func callApiToGetBranch()
    {
        var strUrl = URL.SwitchBranch.switchBranchUrl + "\(strCompanyKey)&branchId=\(strBranchId)"
        
        strUrl = strUrl.trimmingCharacters(in: .whitespaces)
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
            
            self.loader.dismiss(animated: false)
            { [self] in
                if(Status)
                {
                    
                    let dictResponse = removeNullFromDict(dict: (Response["data"] as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                    
                    if dictResponse.isKind(of: NSDictionary.self) && dictResponse.count > 0
                    {
                        
                        self.dictResponseBranch = dictResponse
                        manageEmployeeBranch()
                        
                        self.goBacckk(strTypeee: "Save")

                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    }

                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
                
            }
            
            
        }
        
    }

}

extension SwitchBranchVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if tag == 1000
        {
            dictBranch = dictData
            txtFldSelectBranch.text = "\(dictBranch.value(forKey: "Name") ?? "")"
            btnRestBranch.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            
            strBranchId = "\(dictBranch.value(forKey: "BranchMasterId") ?? "")"
            
        }
    }
}
