//
//  Leader_iPhoneVC.swift
//  DPS
//
//  Created by NavinPatidar on 1/7/21.
//  Copyright © 2021 Saavan. All rights reserved.
//navin patidar ios developer


import UIKit

class Leader_iPhoneVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------

    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var lblSalesTotal: UILabel!
    @IBOutlet weak var lblProposed: UILabel!
    @IBOutlet weak var lbl_Proposal: UILabel!
    @IBOutlet weak var lbl_Closing: UILabel!

    @IBOutlet weak var btn_SalesTotal: UIButton!
    @IBOutlet weak var btn_Proposed: UIButton!
    @IBOutlet weak var btn_Proposal: UIButton!
    @IBOutlet weak var btn_Closing: UIButton!
    var dictLoginData = NSDictionary()

    var loader = UIAlertController()

    var arrOfFilteredDataTemp = [NSDictionary]()

    var arryEmployee = NSMutableArray() ,arySelectedBranch = NSMutableArray() , aryTVList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var tagForSorting = 1
    
    
    var dictFilterData = NSMutableDictionary()
    var strFromDate = "" , strToDate = "" , strBranchName = "" , strBranchSysName = ""

    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        if(nsud.value(forKey: "LoginDetails") != nil){
            if(nsud.value(forKey: "LoginDetails") is NSDictionary){
                dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            }
          
        }
        if(dictLoginData.count != 0){
            tv_List.tableFooterView = UIView()
            refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
             refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
            // tv_List.addSubview(refreshControl)
            //tv_List.dataSource = self
            //tv_List.delegate = self
            self.setCornerOnTopView()
            if #available(iOS 13.0, *) {
                searchBar.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
            }
            if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
                txfSearchField.borderStyle = .none
                txfSearchField.backgroundColor = .white
                txfSearchField.layer.cornerRadius = 15
                txfSearchField.layer.masksToBounds = true
                txfSearchField.layer.borderWidth = 1
                txfSearchField.layer.borderColor = UIColor.white.cgColor
            }
            searchBar.layer.borderWidth = 0
            searchBar.layer.opacity = 1.0
           
            if(nsud.value(forKey: "DashBoardEmployeeLeaderFilter") != nil){
                if(nsud.value(forKey: "DashBoardEmployeeLeaderFilter") is NSDictionary){
                    ApplyFilter(dict: (nsud.value(forKey: "DashBoardEmployeeLeaderFilter") as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                }else{
                    self.ApplyFilter(dict: NSMutableDictionary())
                }
            }else{
                self.ApplyFilter(dict: NSMutableDictionary())
            }
        }
     

      
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            //self.setTopMenuOption()
            self.setFooterMenuOption()
        })
        
        
      
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            //self.setTopMenuOption()
            self.setFooterMenuOption()
        })
      }
    
    
    @objc func refresh(_ sender: AnyObject) {
        self.Get_LeadetData_API()
    }
    
    
    // MARK:
    // MARK: ----------Filter Function
    func ApplyFilter(dict : NSMutableDictionary)  {
        print(dict)
        strBranchSysName = ""
        strBranchName = ""
        dictFilterData = NSMutableDictionary()
     
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
      
      
        if(dict.count == 0){
            strBranchSysName = "\(Global().strEmpBranchSysName() ?? "")"
            strBranchName = Global().getBranchName(fromSysNam: strBranchName)
            
            let objDelegate = StasticsClass()
            objDelegate.delegate = self as StasticsClassDelegate
            objDelegate.getStart_EndDate("This Month")
            dictFilterData.setValue("\(self.strFromDate)", forKey: "FromDate")
            dictFilterData.setValue("\(self.strToDate)", forKey: "ToDate")
        }else{
            arySelectedBranch = NSMutableArray()
            arySelectedBranch = (dict.value(forKey: "SelectedBranch")as! NSArray).mutableCopy()as! NSMutableArray
            if(self.arySelectedBranch.count != 0){
                let dict = NSMutableDictionary()
                dict.setValue("All Branches", forKey: "Name")
                dict.setValue("0", forKey: "BranchMasterId")
                if(arySelectedBranch.contains(dict)){
                  
                }else{
                    for item in arySelectedBranch {
                        strBranchName =  strBranchName + "\((item as AnyObject).value(forKey: "Name") ?? ""),"
                    }
                    if(strBranchName.count != 0){
                        strBranchName = String(strBranchName.dropLast())
                    }
                    SetBranchNameBYBranchSysName(strBranch: strBranchName)
                }
            
            }
            dictFilterData.setValue("\(dict.value(forKey: "fromDate") ?? "")", forKey: "FromDate")
            dictFilterData.setValue("\(dict.value(forKey: "toDate") ?? "")", forKey: "ToDate")
        }
        if(strBranchSysName == ""){
            strBranchSysName = "\(Global().strEmpBranchSysName() ?? "")"
            strBranchName = Global().getBranchName(fromSysNam: strBranchName)
        }
        
        dictFilterData.setValue(companyKey, forKey: "CompanyKey")
        dictFilterData.setValue(strBranchSysName, forKey: "BranchSysNames")
            self.Get_LeadetData_API()
        
    }
    
    func SetBranchNameBYBranchSysName(strBranch : String) {
        
        let aryBranch =  NSMutableArray()
        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                aryBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
     
        arySelectedBranch = NSMutableArray()
        let resultBranch = strBranch.split(separator: ",")
        for item in resultBranch {
            for item1 in aryBranch {
                if(((item1 as! NSDictionary).value(forKey: "Name")) as! String  == "\(item)"){
                    arySelectedBranch.add((item1))
                }
            }
        }
        
        for item in arySelectedBranch {
            strBranchSysName =  strBranchSysName + "\((item as AnyObject).value(forKey: "SysName") ?? ""),"
        }
        if(strBranchSysName.count != 0){
            strBranchSysName = String(strBranchSysName.dropLast())
        }
    }
    // MARK:
    // MARK: ----------UiInitialization
    func setCornerOnTopView()  {
        self.btn_SalesTotal.layer.borderWidth = 1.0
        self.btn_Proposed.layer.borderWidth = 1.0
        self.btn_Proposal.layer.borderWidth = 1.0
        self.btn_Closing.layer.borderWidth = 1.0
     
        self.btn_SalesTotal.layer.borderColor = UIColor.white.cgColor
        self.btn_Proposed.layer.borderColor = UIColor.white.cgColor
        self.btn_Proposal.layer.borderColor = UIColor.white.cgColor
        self.btn_Closing.layer.borderColor = UIColor.white.cgColor
        
        switch self.tagForSorting {
        case 1:
            self.btn_SalesTotal.layer.borderColor = UIColor.black.cgColor
        case 2:
            self.btn_Proposed.layer.borderColor = UIColor.black.cgColor
        case 3:
            self.btn_Proposal.layer.borderColor = UIColor.black.cgColor
        case 4:
            self.btn_Closing.layer.borderColor = UIColor.black.cgColor
        default:
            break
        }
    }
    
    // MARK:
    // MARK: ----------IBAction
    @IBAction func actionOnPerformance(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionOnSorting(_ sender: UIButton) {
        self.view.endEditing(true)
        self.tagForSorting = sender.tag
       
        dataSortByCondition()
        self.setCornerOnTopView()
    }
    @IBAction func actionOnFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterDashboardNew_iPhoneVC") as? FilterDashboardNew_iPhoneVC
            vc?.strViewComeFrom = "Leader"
         vc!.delegate = self
         self.present(vc!, animated: false, completion: nil)
    }
    
    @IBAction func actionOnAdd(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let global = Global()
        let isnet = global.isNetReachable()
        if isnet {
            
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as UIView
                popoverController.sourceRect = sender.bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
            }
            
            let AddTask = (UIAlertAction(title: "Add Task", style: .default , handler:{ (UIAlertAction)in
                
                print("User click Add Task button")
                let defs = UserDefaults.standard
                defs.setValue(nil, forKey: "TaskFilterSort")
                defs.synchronize()
                
                let objWebService = WebService()
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
                
                objByProductVC.dictTaskDetailsData = objWebService.setDefaultTaskDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
            }))
            
            AddTask.setValue(#imageLiteral(resourceName: "Task"), forKey: "image")
            AddTask.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddTask)
            
            
            
            let AddActivity = (UIAlertAction(title: "Add Activity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Activity button")
                
                let objWebService = WebService()
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
                
                
                
                objByProductVC.dictActivityDetailsData = objWebService.setDefaultActivityDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddActivity.setValue(#imageLiteral(resourceName: "Add Activity"), forKey: "image")
            AddActivity.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddActivity)
            
            let AddLead = (UIAlertAction(title: "Add Lead/Opportunity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Add Lead button")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
                self.navigationController?.pushViewController(vc, animated: false)
            }))
            AddLead.setValue(#imageLiteral(resourceName: "Lead"), forKey: "image")
            AddLead.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddLead)
            
            
            let AddContact = (UIAlertAction(title: "Add Contact", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Contact  button")
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
                
                
                
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddContact.setValue(#imageLiteral(resourceName: "Icon ionic-md-person"), forKey: "image")
            AddContact.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddContact)
            
            
            let AddCompany = (UIAlertAction(title: "Add Company", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Company  button")
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
                
                
                self.navigationController?.pushViewController(controller, animated: false)
            }))
            AddCompany.setValue(#imageLiteral(resourceName: "CompanyAdd"), forKey: "image")
            AddCompany.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddCompany)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Add Cancel  button")
            }))
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        } else {
            global.alertMethod(Alert, ErrorInternetMsg)
        }
        
    }
    
    
@IBAction func actionOnClickVoiceRecognizationButton(_ sender: UIButton) {
    self.view.endEditing(true)
    if self.aryTVList.count != 0 {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
         vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.delegate = self
        vc.modalTransitionStyle = .coverVertical
        self.present(vc, animated: true, completion: {})
    }
}
    // MARK: --DataSortByCondition

    
    func dataSortByCondition() {
        let green = "B8E0D6"
        let Yello = "F3EDB8"
        let orange = "FCDCBE"
        let blue = "B5DDEF"

        let aryTemp = NSMutableArray()
   
        
        for item in self.aryTVList {
            let dict = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
            var strSalesTotal = "0" , strProposed = "0", strProposals = "0", strClosing = "0"
         
            strSalesTotal = "\(dict.value(forKey: "SalesValue")!)" == "" ? "0" : "\(dict.value(forKey: "SalesValue")!)"
            strProposed = "\(dict.value(forKey: "ProposalsValue")!)" == "" ? "0" : "\(dict.value(forKey: "ProposalsValue")!)"
            strProposals = "\(dict.value(forKey: "ProposalsCount")!)" == "" ? "0" : "\(dict.value(forKey: "ProposalsCount")!)"
            strClosing = "\(dict.value(forKey: "ClosingPercentage")!)" == "" ? "0" : "\(dict.value(forKey: "ClosingPercentage")!)"

            let v1 = CGFloat(Double(strSalesTotal)!)
            let v2 = CGFloat(Double(strProposed)!)
            let v3 = CGFloat(Double(strProposals)!)
            let v4 = CGFloat(Double(strClosing)!)
           
            let numbers = [v1, v2, v3, v4]
            let numMax = numbers.max()
            
            //let tempArr = "\(numMax ?? 0.0)".components(separatedBy: ".")
            //let valueTemp = tempArr[0]
            
            let v11 = (v1/numMax!)
            let v22 = (v2/numMax!)
            let v33 = (v3/numMax!)
            let v44 = (v4/numMax!)
            dict.setValue(index, forKey: "index")
            
            if(self.tagForSorting == 1){
                dict.setValue(v1, forKey: "V")

                
                dict.setValue(convertDoubleToCurrency(amount: (strSalesTotal as NSString).doubleValue), forKey: "V1")
                dict.setValue(convertDoubleToCurrency(amount: (strProposed as NSString).doubleValue), forKey: "V2")
                dict.setValue(strProposals, forKey: "V3")
                dict.setValue("\(strClosing)%", forKey: "V4")


                dict.setValue("\(v11)", forKey: "V11")
                dict.setValue("\(v22)", forKey: "V22")
                dict.setValue("\(v33)", forKey: "V33")
                dict.setValue("\(v44)", forKey: "V44")
                
                
                dict.setValue(green, forKey: "C1")
                dict.setValue(Yello, forKey: "C2")
                dict.setValue(orange, forKey: "C3")
                dict.setValue(blue, forKey: "C4")
                

                
                
            }
            else if(self.tagForSorting == 2){
                
                dict.setValue(v2, forKey: "V")

                
                dict.setValue(convertDoubleToCurrency(amount: (strProposed as NSString).doubleValue), forKey: "V1")
                dict.setValue(convertDoubleToCurrency(amount: (strSalesTotal as NSString).doubleValue), forKey: "V2")
                dict.setValue(strProposals, forKey: "V3")
                dict.setValue("\(strClosing)%", forKey: "V4")
                

                
                dict.setValue("\(v22)", forKey: "V11")
                dict.setValue("\(v11)", forKey: "V22")
                dict.setValue("\(v33)", forKey: "V33")
                dict.setValue("\(v44)", forKey: "V44")
                
                dict.setValue(Yello, forKey: "C1")
                dict.setValue(green, forKey: "C2")
                dict.setValue(orange, forKey: "C3")
                dict.setValue(blue, forKey: "C4")
                

            }
            else if(self.tagForSorting == 3){
                

                dict.setValue(v3, forKey: "V")

                dict.setValue(strProposals, forKey: "V1")
                dict.setValue(convertDoubleToCurrency(amount: (strSalesTotal as NSString).doubleValue), forKey: "V2")
                dict.setValue(convertDoubleToCurrency(amount: (strProposed as NSString).doubleValue), forKey: "V3")
                dict.setValue("\(strClosing)%", forKey: "V4")
                
   
                dict.setValue("\(v33)", forKey: "V11")
                dict.setValue("\(v11)", forKey: "V22")
                dict.setValue("\(v22)", forKey: "V33")
                dict.setValue("\(v44)", forKey: "V44")
                
                dict.setValue(orange, forKey: "C1")
                dict.setValue(green, forKey: "C2")
                dict.setValue(Yello, forKey: "C3")
                dict.setValue(blue, forKey: "C4")
                

            }
            else if(self.tagForSorting == 4){
                
                dict.setValue(v4, forKey: "V")

                dict.setValue("\(strClosing)%", forKey: "V1")
                dict.setValue(convertDoubleToCurrency(amount: (strSalesTotal as NSString).doubleValue), forKey: "V2")
                dict.setValue(convertDoubleToCurrency(amount: (strProposed as NSString).doubleValue), forKey: "V3")
                dict.setValue(strProposals, forKey: "V4")
                

                dict.setValue("\(v44)", forKey: "V11")
                dict.setValue("\(v11)", forKey: "V22")
                dict.setValue("\(v22)", forKey: "V33")
                dict.setValue("\(v33)", forKey: "V44")
                
                dict.setValue(blue, forKey: "C1")
                dict.setValue(green, forKey: "C2")
                dict.setValue(Yello, forKey: "C3")
                dict.setValue(orange, forKey: "C4")
                

            }
            aryTemp.add(dict)
        }
        
        print(aryTemp.count)
        
        var arrOfFilteredDataTemp = [NSDictionary] ()
        arrOfFilteredDataTemp = aryTemp as! [NSDictionary]
        let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["V"] as? CGFloat)! > (($1 )["V"] as? CGFloat)!
        })
        
     
        self.aryTVList = NSMutableArray()
        self.aryTVList = (sortedArray as NSArray).mutableCopy()as! NSMutableArray
        self.arryEmployee = NSMutableArray()
        self.arryEmployee = (sortedArray as NSArray).mutableCopy()as! NSMutableArray
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.tv_List.dataSource = self
            self.tv_List.delegate = self
            self.tv_List.reloadData()
        }

    }
  
    // MARK: ---------------------------Footer Functions ---------------------------
    func setFooterMenuOption() {
       
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        
        nsud.setValue("Home", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        footorView.removeFromSuperview()
        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            //self.GotoDashboardViewController()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            self.GotoScheduleViewController()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.GotoAccountViewController()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
            print("call home")
            self.GotoSalesViewController()
        }
       
    }

    func GotoAccountViewController() {
        self.view.endEditing(true)
        let objWebService = WebService()
        objWebService.setDefaultCrmValue()
        
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    func GotoSalesViewController() {
        self.view.endEditing(true)
        let objWebService = WebService()
        objWebService.setDefaultCrmValue()
        let mainStoryboard = UIStoryboard(
            name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
            bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "WebLeadVC") as? WebLeadVC
        self.navigationController?.pushViewController(objByProductVC!, animated: false)
    }
    
    func GotoDashboardViewController() {
        
        self.view.endEditing(true)
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func GotoScheduleViewController() {
        self.view.endEditing(true)
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            if strAppointmentFlow == "New" {
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "Main",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension Leader_iPhoneVC  {
    func Get_LeadetData_API() {
        print(dictFilterData)

        if (isInternetAvailable()){
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
                self.present(self.loader, animated: false, completion: nil)
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetLeaderboardDetails
            
            WebService.callAPIBYPOST(parameter: dictFilterData, url: strURL) { (response, status) in
                self.refreshControl.endRefreshing()
                self.loader.dismiss(animated: false) { [self] in
                    print(response)
                    if(status){
                        if(response.value(forKey: "data") is NSDictionary){
                            let data = response.value(forKey: "data")as! NSDictionary
                            if(data.value(forKey: "Employees") is NSArray){
                                self.arryEmployee = NSMutableArray()
                                self.arryEmployee = (data.value(forKey: "Employees") as! NSArray).mutableCopy()as! NSMutableArray
                                
                                self.aryTVList = NSMutableArray()
                                self.aryTVList = (data.value(forKey: "Employees") as! NSArray).mutableCopy()as! NSMutableArray
                                
                                let lblSalesTotalValue = ("\(data.value(forKey: "SalesValueTotal") ?? "0")" as NSString).doubleValue
                                self.lblSalesTotal.text = "Sales Total\n" + convertDoubleToCurrency(amount: lblSalesTotalValue)
                                
                                let lblProposedValue = ("\(data.value(forKey: "ProposalsValueTotal") ?? "0")" as NSString).doubleValue
                                self.lblProposed.text = "Total Proposed\n" + convertDoubleToCurrency(amount: lblProposedValue)
            
                                self.lbl_Proposal.text = "Proposals Number\n\(data.value(forKey: "ProposalsCountTotal") ?? "0")"
                                self.lbl_Closing.text = "Closing \n\(data.value(forKey: "ClosingPercentageTotal") ?? "0")%"
                                
                                self.SaveDataInlocalDataBase(dict: data)
                                
                                self.dataSortByCondition()
                            }
                        }
                       
                    }else{
                        showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                  
              
            }
        }
        else{
            
            let arrayLeadDetail = getDataFromLocal(strEntity: "LeaderBoard", predicate: NSPredicate(format: "companyKey == %@ && userName == %@", Global().getCompanyKey(),Global().getUserName()))
            var obj = NSManagedObject()
            if(arrayLeadDetail.count != 0){
                obj = arrayLeadDetail[0] as! NSManagedObject
                let data = obj.value(forKey: "list") as! NSDictionary
                if(data.count != 0){
                    if(data.value(forKey: "Employees") is NSArray){
                        self.arryEmployee = NSMutableArray()
                        self.arryEmployee = (data.value(forKey: "Employees") as! NSArray).mutableCopy()as! NSMutableArray
                        
                        self.aryTVList = NSMutableArray()
                        self.aryTVList = (data.value(forKey: "Employees") as! NSArray).mutableCopy()as! NSMutableArray
                        
                        let lblSalesTotalValue = ("\(data.value(forKey: "SalesValueTotal") ?? "0")" as NSString).doubleValue
                        self.lblSalesTotal.text = "Sales Total\n" + convertDoubleToCurrency(amount: lblSalesTotalValue)
                        
                        let lblProposedValue = ("\(data.value(forKey: "ProposalsValueTotal") ?? "0")" as NSString).doubleValue
                        self.lblProposed.text = "Total Proposed\n" + convertDoubleToCurrency(amount: lblProposedValue)
                        
                        
                        self.lbl_Proposal.text = "Proposals Number\n\(data.value(forKey: "ProposalsCountTotal") ?? "0")"
                        self.lbl_Closing.text = "Closing \n\(data.value(forKey: "ClosingPercentageTotal") ?? "0")%"
                        
                        self.dataSortByCondition()
                    }
                }
               
            }
            else{
                showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
            }
        }
    }
    func SaveDataInlocalDataBase(dict : NSDictionary)  {
        
        
        deleteAllRecordsFromDB(strEntity: "LeaderBoard", predicate: NSPredicate(format: "companyKey == %@ && userName == %@", Global().getCompanyKey(),Global().getUserName()))

        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("list")
        arrOfValues.add(dict)
        
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
       
        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
       
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
        
        saveDataInDB(strEntity: "LeaderBoard", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension Leader_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTVList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "LeaderListCelliPad1" : "LeaderListCell1", for: indexPath as IndexPath) as! LeaderListCell
        
        if self.tagForSorting == 2{
            
            cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "LeaderListCelliPad2" : "LeaderListCell2", for: indexPath as IndexPath) as! LeaderListCell
            
        }else if self.tagForSorting == 3{
            
            cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "LeaderListCelliPad3" : "LeaderListCell3", for: indexPath as IndexPath) as! LeaderListCell
            
        }else if self.tagForSorting == 4{
            
            cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "LeaderListCelliPad4" : "LeaderListCell4", for: indexPath as IndexPath) as! LeaderListCell
            
        }
        
        cell.imgView.layer.cornerRadius = 8.0
        cell.imgView.layer.borderWidth = 1.0
        cell.imgView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
       
        let dict = removeNullFromDict(dict: (aryTVList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        cell.lblTitleName.text = "\(dict.value(forKey: "FullName") ?? "")"
        let imgName = "\(dict.value(forKey: "EmployeePhoto") ?? "")"
      
        cell.imgView.setImageWith(URL(string: imgName), placeholderImage: UIImage(named: "about-us.png"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
        
        cell.lblAmount1.text = "\(dict.value(forKey: "V1") ?? "")"
        cell.lblAmount2.text = "\(dict.value(forKey: "V2") ?? "")"
        cell.lblAmount3.text = "\(dict.value(forKey: "V3") ?? "")"
        cell.lblAmount4.text = "\(dict.value(forKey: "V4") ?? "")"
        
        let numberFormatter = NumberFormatter()
        let V11 = numberFormatter.number(from: "\(dict.value(forKey: "V11") ?? "0")")
        let V22 = numberFormatter.number(from: "\(dict.value(forKey: "V22") ?? "0")")
        let V33 = numberFormatter.number(from: "\(dict.value(forKey: "V33") ?? "0")")
        let V44 = numberFormatter.number(from: "\(dict.value(forKey: "V44") ?? "0")")
        
        //DispatchQueue.main.async { () -> Void in

            cell.pro_1.progress = V11!.floatValue
            cell.pro_2.progress = V22!.floatValue
            cell.pro_3.progress = V33!.floatValue
            cell.pro_4.progress = V44!.floatValue
                
            cell.pro_1.tintColor = hexStringToUIColor(hex: "\(dict.value(forKey: "C1")!)")
            cell.pro_2.tintColor = hexStringToUIColor(hex: "\(dict.value(forKey: "C2")!)")
            cell.pro_3.tintColor = hexStringToUIColor(hex: "\(dict.value(forKey: "C3")!)")
            cell.pro_4.tintColor = hexStringToUIColor(hex: "\(dict.value(forKey: "C4")!)")
            
        //}
          

      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryTVList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryTVList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
}

// MARK: -
// MARK: ----------OfferCell---------
class LeaderListCell: UITableViewCell {
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet weak var pro_1: UIProgressView!
    @IBOutlet weak var pro_2: UIProgressView!
    @IBOutlet weak var pro_3: UIProgressView!
    @IBOutlet weak var pro_4: UIProgressView!
    
    @IBOutlet weak var lblAmount1: UILabel!
    @IBOutlet weak var lblAmount2: UILabel!
    @IBOutlet weak var lblAmount3: UILabel!
    @IBOutlet weak var lblAmount4: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
//        pro_1 = UIProgressView()
//        pro_2 = UIProgressView()
//        pro_3 = UIProgressView()
//        pro_4 = UIProgressView()
        
        pro_1.progress = 0
        pro_2.progress = 0
        pro_3.progress = 0
        pro_4.progress = 0
        
        // reset view for cell before it gets reused
      }
    
}

// MARK: -
// MARK: ----------StasticsClassDelegate-------
extension Leader_iPhoneVC : StasticsClassDelegate
{
    func getData(_ dict: [AnyHashable : Any]!)
    {
        let dictNew = dict! as NSDictionary
        strFromDate = "\(dictNew.value(forKey: "StartDate") ?? "")"
        strToDate = "\(dictNew.value(forKey: "EndDate") ?? "")"
    }
}


// MARK: -
// MARK: ----------Filter Delegate---------
extension Leader_iPhoneVC : FilterDashboardNewProtocol{
    func getDataFromFilterDashboardNew(dict: NSDictionary, tag: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "Leader_iPhoneVC") as! Leader_iPhoneVC
                    
            self.navigationController?.pushViewController(vc, animated: false)
           // self.ApplyFilter(dict: dict.mutableCopy()as! NSMutableDictionary)
        }
    }
}

extension Leader_iPhoneVC : UISearchBarDelegate{

// MARK: UISearchBar delegate

func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    searchAutocomplete(searchText: searchText)
}

func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
    self.view.endEditing(true)
    
    if(searchBar.text?.count == 0)
    {
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter text to be searched.", viewcontrol: self)
    }
}

func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.text = ""
    self.view.endEditing(true)
    self.aryTVList = NSMutableArray()
    self.aryTVList = arryEmployee
    self.tv_List.dataSource = self
    self.tv_List.delegate = self
    self.tv_List.reloadData()
}
    func searchAutocomplete(searchText: String) -> Void{
        if searchText.isEmpty {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.resignFirstResponder()
            }
            self.aryTVList = NSMutableArray()
            self.aryTVList = arryEmployee
            self.tv_List.dataSource = self
            self.tv_List.delegate = self
            self.tv_List.reloadData()
        }
        
        else{
            
            let aryTemp = arryEmployee.filter { (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "FullName") ??  "")".lowercased().contains(searchText.lowercased()) }
            
            self.aryTVList = NSMutableArray()
            self.aryTVList = (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            self.tv_List.dataSource = self
            self.tv_List.delegate = self
            self.tv_List.reloadData()
        }
        
    }
}

// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension Leader_iPhoneVC : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if #available(iOS 13.0, *) {
            self.searchBar.searchTextField.text = str
        } else {
            self.searchBar.text = str
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.searchAutocomplete(searchText: str)
        }
    }
}
