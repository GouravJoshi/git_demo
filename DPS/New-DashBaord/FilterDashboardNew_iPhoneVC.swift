//
//  FilterDashboardNew_iPhoneVC.swift
//  DPS
//
//  Created by NavinPatidar on 7/9/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Test 2020
//  Test 202020



import UIKit

protocol FilterDashboardNewProtocol : class
{
    func getDataFromFilterDashboardNew(dict : NSDictionary ,tag : Int)
}
class FilterDashboardNew_iPhoneVC: UIViewController {
    // MARK:
    // MARK: ---------IBOutlet
    @IBOutlet weak var tv_Filter: UITableView!
    @IBOutlet weak var txtfld_FromDate: ACFloatingTextField!
    @IBOutlet weak var txtfld_Todate: ACFloatingTextField!
    @IBOutlet weak var txtfld_BranchName: ACFloatingTextField!

    @IBOutlet weak var view_Branch: UIView!

    // MARK:
      // MARK: ---------VAriable
      var aryFilterData = ["Yesterday", "Today" , "Tomorrow" , "This week" , "This Month"]
     var delegate: FilterDashboardNewProtocol?
    var strtag = Int()
    
    var selectiontag = "" ,  strFromDate = "" , strToDate = "" , strViewComeFrom = ""
    
    
    var arySelectedBranch = NSMutableArray()  , aryBranch = NSMutableArray()
    // MARK:
         // MARK: ---------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tv_Filter.estimatedRowHeight = 70.0
        // Default hai hatana hai wapis date button ka bhi user interaction disabled hai dekh lena
        // Clear button ki action bhi hata rakhi hai aur did select pr bhi hata rakha hai
        selectiontag = ""
        txtfld_FromDate.text = ""
        txtfld_Todate.text = ""
        
        //For DashBOard
        if(strViewComeFrom == "Dashboard"){
            self.view_Branch.isHidden = true
            if nsud.value(forKey: "DashBoardEmployeePerformanceFilter") is NSDictionary {
                let dictData = nsud.value(forKey: "DashBoardEmployeePerformanceFilter") as! NSDictionary

                if dictData.count > 0  {
                    
                    selectiontag = "\(dictData.value(forKey: "days") ?? "")"
                    
                    if selectiontag == "Custom Date" {
                        
                        txtfld_FromDate.text = "\(dictData.value(forKey: "fromDate") ?? "")"
                        txtfld_Todate.text = "\(dictData.value(forKey: "toDate") ?? "")"
                        
                    }else{
                        
                        txtfld_FromDate.text = ""
                        txtfld_Todate.text = ""
                        
                    }
               
                }
            }
        }
        // For Leader
        else{
            self.view_Branch.isHidden = false

            if nsud.value(forKey: "DashBoardEmployeeLeaderFilter") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardEmployeeLeaderFilter") as! NSDictionary

                if dictData.count > 0  {
                    
                    selectiontag = "\(dictData.value(forKey: "days") ?? "")"
                    
                    if selectiontag == "Custom Date" {
                        
                        txtfld_FromDate.text = "\(dictData.value(forKey: "fromDate") ?? "")"
                        txtfld_Todate.text = "\(dictData.value(forKey: "toDate") ?? "")"
                        
                    }else{
                        
                        txtfld_FromDate.text = ""
                        txtfld_Todate.text = ""
                        
                    }
                  
                        arySelectedBranch = NSMutableArray()
                        arySelectedBranch = (dictData.value(forKey: "SelectedBranch")as! NSArray).mutableCopy()as! NSMutableArray
                        var strBranchName = ""
                        for item in arySelectedBranch {
                            if((item as AnyObject).value(forKey: "Name") is String){
                                strBranchName =  strBranchName + "\((item as AnyObject).value(forKey: "Name")!),"
                            }
                           
                        }
                    let dict = NSMutableDictionary()
                    dict.setValue("All Branches", forKey: "Name")
                    dict.setValue("0", forKey: "BranchMasterId")
                    if(arySelectedBranch.contains(dict)){
                        strBranchName = "All Branches,"
                    }
                        if(strBranchName.count != 0){
                            strBranchName = String(strBranchName.dropLast())
                        }
                        txtfld_BranchName.text = strBranchName
                        
                    
                }
            }
                if(self.arySelectedBranch.count == 0){
                    setDefaultBranch()
                }
        }
   
     
    }
    override func viewWillAppear(_ animated: Bool) {
     //   DeviceType.IS_IPAD ? AppUtility.lockOrientation(.all) : AppUtility.lockOrientation(.portrait)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     //   AppUtility.lockOrientation(.portrait)
    }
    
    // MARK:
       // MARK: ----------IBAction
    
    @IBAction func action_Back(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func action_Clear(_ sender: Any) {
        
       
        
       
        
        if(strViewComeFrom == "Dashboard"){
            selectiontag = ""
            txtfld_FromDate.text = ""
            txtfld_Todate.text = ""
            selectiontag = ""
            self.tv_Filter.reloadData()

        }else{
            selectiontag = ""
            txtfld_FromDate.text = ""
            txtfld_Todate.text = ""
            txtfld_BranchName.text = ""
            self.arySelectedBranch = NSMutableArray()
            nsud.setValue(nil, forKey: "DashBoardEmployeeLeaderFilter")
            nsud.synchronize()
            self.tv_Filter.reloadData()

        }

    }
    @IBAction func action_Save(_ sender: Any) {
        
        if(isInternetAvailable()){
            let dictFilter = NSMutableDictionary()
            if(selectiontag == "Yesterday"){
                
                let objDelegate = StasticsClass()
                objDelegate.delegate = self as StasticsClassDelegate
                objDelegate.getStart_EndDate("Yesterday")
                
                dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                dictFilter.setValue("\(strToDate)", forKey: "toDate")
                dictFilter.setValue("Yesterday", forKey: "days")
                
            }
            else if(selectiontag == "Today"){
                
                let objDelegate = StasticsClass()
                objDelegate.delegate = self as StasticsClassDelegate
                objDelegate.getStart_EndDate("Today")
                
                dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                dictFilter.setValue("\(strToDate)", forKey: "toDate")
                dictFilter.setValue("Today", forKey: "days")
            }
            else if(selectiontag == "Tomorrow"){
                
                let objDelegate = StasticsClass()
                objDelegate.delegate = self as StasticsClassDelegate
                objDelegate.getStart_EndDate("Tomorrow")
                
                dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                dictFilter.setValue("\(strToDate)", forKey: "toDate")
                dictFilter.setValue("Tomorrow", forKey: "days")
            }
            else if(selectiontag == "This week"){
                
                let objDelegate = StasticsClass()
                objDelegate.delegate = self as StasticsClassDelegate
                objDelegate.getStart_EndDate("This Week")
                
                dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                dictFilter.setValue("\(strToDate)", forKey: "toDate")
                
                dictFilter.setValue("This week", forKey: "days")
            }
            else if(selectiontag == "This Month"){
                
                let objDelegate = StasticsClass()
                objDelegate.delegate = self as StasticsClassDelegate
                objDelegate.getStart_EndDate("This Month")
                
                dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                dictFilter.setValue("\(strToDate)", forKey: "toDate")
                dictFilter.setValue("This Month", forKey: "days")
                
            }else if (txtfld_FromDate.text?.count != 0 && txtfld_Todate.text?.count != 0){
                
                
                if(txtfld_FromDate.text!.count > 0 && txtfld_Todate.text!.count > 0){
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                    let fromDate = dateFormatter.date(from: self.txtfld_FromDate.text!)
                    let toDate = dateFormatter.date(from: self.txtfld_Todate.text!)
                
                if(fromDate! > toDate!){
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
                    
                }else{
                    dictFilter.setValue("\(txtfld_FromDate.text!)", forKey: "fromDate")
                    dictFilter.setValue("\(txtfld_Todate.text!)", forKey: "toDate")
                    dictFilter.setValue("Custom Date", forKey: "days")
                }
                }

            }else if(selectiontag == ""){
                let objDelegate = StasticsClass()
                objDelegate.delegate = self as StasticsClassDelegate
                objDelegate.getStart_EndDate("Today")
                if(strViewComeFrom == "Dashboard"){
                    dictFilter.setValue("", forKey: "fromDate")
                    dictFilter.setValue("", forKey: "toDate")
                    dictFilter.setValue("", forKey: "days")
                }else{
                    dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                    dictFilter.setValue("\(strToDate)", forKey: "toDate")
                    dictFilter.setValue("Today", forKey: "days")
                }
               
            }
            
            //Value set for leader filter
            if(strViewComeFrom == "Dashboard"){
                nsud.setValue(dictFilter, forKey: "DashBoardEmployeePerformanceFilter")
                nsud.synchronize()
            }else{
               
                dictFilter.setValue(self.arySelectedBranch, forKey: "SelectedBranch")
                nsud.setValue(dictFilter, forKey: "DashBoardEmployeeLeaderFilter")
                nsud.synchronize()
            }

            if(dictFilter.count != 0){

                self.dismiss(animated: false, completion: nil)

                //sleep(1)

                delegate?.getDataFromFilterDashboardNew(dict:dictFilter, tag: strtag)
                
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }
        
       
      
    }
    @IBAction func action_FromDate(_ sender: UIButton) {
        sender.tag = 1
         gotoDatePickerView(sender: sender, strType: "Date", minTime: false , maxTime: true)
     }
    @IBAction func action_Todate(_ sender: UIButton) {
        sender.tag = 2

        gotoDatePickerView(sender: sender, strType: "Date", minTime: false , maxTime: true)
     }
    
 
    
    @IBAction func action_Branch(_ sender: UIButton) {
        aryBranch = NSMutableArray()
        aryBranch = getBranches()
            if(aryBranch.count > 0)
            {
                let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
                let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
                vc.strTitle = "Select"
                vc.strTag = 310
                let dict = NSMutableDictionary()
                dict.setValue("All Branches", forKey: "Name")
                dict.setValue("0", forKey: "BranchMasterId")
                aryBranch.insert(dict, at: 0)
                

                vc.arySelectedItems = self.arySelectedBranch
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .coverVertical
                vc.handelDataSelectionTable = self
                vc.aryTBL = addSelectInArray(strName: "", array: aryBranch)
                self.present(vc, animated: false, completion: {})
            }
            else
            {
                Global().displayAlertController(Alert, NoDataAvailableee, self)
            }
     }
    
    
    func getBranches() -> NSMutableArray {
        let temparyBranch = NSMutableArray()

        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                temparyBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
        return temparyBranch
    }
    func setDefaultBranch() {
        let strBranchSysName = "\(Global().strEmpBranchSysName() ?? "")"
        print(strBranchSysName)
        let strBranchName = Global().getBranchName(fromSysNam: strBranchSysName)
        txtfld_BranchName.text = strBranchName
        print(strBranchSysName)
        aryBranch = NSMutableArray()
        aryBranch = getBranches()
       
        let resultBranch = strBranchSysName.split(separator: ",")
        for item in resultBranch {
            for item1 in aryBranch {
                if((item1 as! NSDictionary).value(forKey: "SysName") as! String == "\(item)"){
                    arySelectedBranch.add((item1))
                }
            }
        }
    }
    
    // MARK:
         // MARK: ---------Open Picker For Date Select
      
    func gotoDatePickerView(sender: UIButton, strType:String , minTime : Bool , maxTime : Bool )  {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = minTime
        vc.chkForMaxDate = maxTime
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
       // vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension FilterDashboardNew_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  aryFilterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tv_Filter.dequeueReusableCell(withIdentifier: "FilterDashboardCell", for: indexPath as IndexPath) as! FilterDashboardCell
        cell.lblTitle.text =  "\(aryFilterData[indexPath.row])"
        if(selectiontag == "\(aryFilterData[indexPath.row])"){
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        }else{
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
            return cell
      
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return DeviceType.IS_IPAD ? 70.0 : 50.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectiontag = "\(aryFilterData[indexPath.row])"
        txtfld_FromDate.text = ""
        txtfld_Todate.text = ""
        self.tv_Filter.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: DeviceType.IS_IPAD ? 70 : 50))
        customView.backgroundColor = UIColor.groupTableViewBackground
        let lbl = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 8, height: DeviceType.IS_IPAD ? 70 : 50))
        lbl.text = "Custom Date"
        lbl.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 21 : 16)
        customView.addSubview(lbl)
        return customView
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: DeviceType.IS_IPAD ? 70 : 50))
     customView.backgroundColor = UIColor.groupTableViewBackground
     let lbl = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 8, height: DeviceType.IS_IPAD ? 70 : 50))
     lbl.text = "Days"
     lbl.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 21 : 16)
     customView.addSubview(lbl)
     return customView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 70.0 : 50.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  DeviceType.IS_IPAD ? 70.0 : 50.0
    }
}


// MARK: -
// MARK: ----------FilterDashboardCell---------
class FilterDashboardCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: ----------DatePickerProtocol-------
extension FilterDashboardNew_iPhoneVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        selectiontag = ""
        self.tv_Filter.reloadData()
        if(tag == 1)
        {
            txtfld_FromDate.text = strDate
        }
        if(tag == 2)
        {
            txtfld_Todate.text = strDate
        }
       
    }
}
// MARK: -
// MARK: ----------StasticsClassDelegate-------
extension FilterDashboardNew_iPhoneVC : StasticsClassDelegate
{
    func getData(_ dict: [AnyHashable : Any]!)
    {
        let dictNew = dict! as NSDictionary
        strFromDate = "\(dictNew.value(forKey: "StartDate") ?? "")"
        strToDate = "\(dictNew.value(forKey: "EndDate") ?? "")"
    }
    
}



extension FilterDashboardNew_iPhoneVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 310){
            if(dictData.count != 0){
                var strBranchStatus = ""
                arySelectedBranch = NSMutableArray()
                arySelectedBranch = (dictData.value(forKey: "multi")as! NSArray).mutableCopy()as! NSMutableArray
                
                let dict = NSMutableDictionary()
                dict.setValue("All Branches", forKey: "Name")
                dict.setValue("0", forKey: "BranchMasterId")
                if(arySelectedBranch.contains(dict)){
                    strBranchStatus = "All Branches,"
                }else{
                for item in arySelectedBranch {
                    strBranchStatus =  strBranchStatus + "\((item as AnyObject).value(forKey: "Name")!),"
                }
                }
                if(strBranchStatus.count != 0){
                    strBranchStatus = String(strBranchStatus.dropLast())
                }
                txtfld_BranchName.text = strBranchStatus
            }else{
                txtfld_BranchName.text = ""
                arySelectedBranch = NSMutableArray()
            }
        }
    }
}
