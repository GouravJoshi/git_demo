//
//  SpeechRecognitionVC.swift
//  DPS
//
//  Created by NavinPatidar on 2/24/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import Speech

protocol SpeechRecognitionDelegate : class
{
    
    func getDataFromSpeechRecognition(str : String)
    
}

class SpeechRecognitionVC: UIViewController {

    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- IBOutlet
    //------------------------------------------------------------------------------
    @IBOutlet weak var lbl_Title: UILabel!

    @IBOutlet weak var lbl_speechText: UILabel!
    @IBOutlet weak var btn_effect: UIButton!
    @IBOutlet weak var btn_Close: UIButton!
    @IBOutlet weak var btn_Done: UIButton!
    @IBOutlet weak var btn_Retry: UIButton!

    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //------------------------------------------------------------------------------
    weak var delegate: SpeechRecognitionDelegate?

    let speechRecognizer        = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    var recognitionRequest      : SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask         : SFSpeechRecognitionTask?
    let audioEngine             = AVAudioEngine()
  
    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- Life Cycle
    //------------------------------------------------------------------------------
  
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btn_Retry.isHidden = true
        self.CheckMicroPhonePermission()
        if audioEngine.isRunning {
            self.audioEngine.stop()
            self.recognitionRequest?.endAudio()
           
        } else {
            self.startRecording()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.addRippleEffect(to: self.btn_effect)
        }
    }
    
    override func viewDidLayoutSubviews() {
       
       super.viewDidLayoutSubviews()
        btn_Close.layer.cornerRadius = 8.0
        btn_Close.clipsToBounds = true
        btn_Close.backgroundColor = UIColor.white
        btn_Close.setTitle("Close", for: .normal)
        btn_Close.setTitleColor(UIColor.black, for: .normal)
        
        
        btn_Done.layer.cornerRadius = 8.0
        btn_Done.clipsToBounds = true
        btn_Done.backgroundColor = UIColor.white
        btn_Done.setTitle("Done", for: .normal)
        btn_Done.setTitleColor(UIColor.black, for: .normal)
    }
    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- IBAction
    //------------------------------------------------------------------------------
  
    @IBAction func actionOnDone(_ sender: UIButton) {
        self.delegate?.getDataFromSpeechRecognition(str: lbl_speechText.tag == 99 ? lbl_speechText.text! : "")
        self.dismiss(animated: true) {}
    }
    @IBAction func actionOnClose(_ sender: UIButton) {
        self.delegate?.getDataFromSpeechRecognition(str: "")
        self.dismiss(animated: true) {}
    }
    @IBAction func actionOnRetry(_ sender: UIButton) {
        self.viewWillAppear(true)
    }
    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- Extra Function
    //------------------------------------------------------------------------------
  
    func addRippleEffect(to referenceView: UIView) {
      
          /*! Creates a circular path around the view*/
          let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height))
          /*! Position where the shape layer should be */
          let shapePosition = CGPoint(x: referenceView.bounds.size.width / 2.0, y: referenceView.bounds.size.height / 2.0)
          let rippleShape = CAShapeLayer()
          rippleShape.bounds = CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height)
          rippleShape.path = path.cgPath
          rippleShape.fillColor = UIColor.clear.cgColor
        rippleShape.strokeColor = hexStringToUIColor(hex: appThemeColor).cgColor
          rippleShape.lineWidth = 3
          rippleShape.position = shapePosition
          rippleShape.opacity = 0

          /*! Add the ripple layer as the sublayer of the reference view */
          referenceView.layer.addSublayer(rippleShape)
          /*! Create scale animation of the ripples */
          let scaleAnim = CABasicAnimation(keyPath: "transform.scale")
          scaleAnim.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
          scaleAnim.toValue = NSValue(caTransform3D: CATransform3DMakeScale(2, 2, 1))
          /*! Create animation for opacity of the ripples */
          let opacityAnim = CABasicAnimation(keyPath: "opacity")
          opacityAnim.fromValue = 1
          opacityAnim.toValue = 0
          /*! Group the opacity and scale animations */
          let animation = CAAnimationGroup()
          animation.animations = [scaleAnim, opacityAnim]
          animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
          animation.duration = CFTimeInterval(1.0)
          animation.repeatCount = .infinity
          animation.isRemovedOnCompletion = true
          rippleShape.add(animation, forKey: "rippleEffect")
      }
    
    func startRecording() {
        self.btn_Retry.isHidden = true
        // Clear all previous session data and cancel task
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }

        // Create instance of audio session to record voice
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: AVAudioSession.Mode.measurement, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }

        self.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()

        let inputNode = audioEngine.inputNode

        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }

        recognitionRequest.shouldReportPartialResults = true

        self.recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            var isFinal = false
            if result != nil {
                self.lbl_speechText.tag = 99
                self.lbl_speechText.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
                print("--------_Final", isFinal)
                if isFinal {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                        self.delegate?.getDataFromSpeechRecognition(str: self.lbl_speechText.text!)
//                        self.dismiss(animated: false) {}
//                    }
                }
            }
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
                //self.btnStart.isEnabled = true
                self.lbl_Title.text = "Sorry,We didn't quite get that"
                self.lbl_speechText.text = "Try repeating your request."
                self.lbl_speechText.tag = 0
                self.btn_Retry.isHidden = false
                
                for layer in self.btn_effect.layer.sublayers! {
                   if layer.isKind(of: CAShapeLayer.self) {
                      layer.removeFromSuperlayer()
                   }
                }

            }
        })

        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }

        self.audioEngine.prepare()

        do {
            try self.audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        lbl_Title.text = "Listening..."
        lbl_speechText.text = "Say something,like:\n\n \u{2022}   iPhone\n\n \u{2022}   12345"
        self.lbl_speechText.tag = 0

    }

    func CheckSpeechRecognizationPermission()  {
        SFSpeechRecognizer.requestAuthorization { [self] (authStatus) in  //4
                switch authStatus {  //5
                case .authorized:
                    print("User  access to speech recognition")
                    
                case .denied:
                    ErrorAlertView(message: "User denied access to speech recognition.\nGo to iPhone/iPad Settings  > Apps  > Speech Recognition.")

                    print("User denied access to speech recognition")
                case .restricted:
                    ErrorAlertView(message: "Speech recognition restricted on this device.\nGo to iPhone/iPad Settings  > Apps  > Speech Recognition.")

                    print("Speech recognition restricted on this device")
                case .notDetermined:
                    ErrorAlertView(message: "Speech recognition not yet authorized")

                    print("Speech recognition not yet authorized")
                }
                
        }
        
    }
    
    func CheckMicroPhonePermission()  {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            print("Permission granted")
            CheckSpeechRecognizationPermission()
        case AVAudioSession.RecordPermission.denied:
            print("Pemission denied")
            ErrorAlertView(message: "User denied access to Microphone.\nGo to iPhone/iPad Settings  > Apps  > Microphone.")
        case AVAudioSession.RecordPermission.undetermined:
            print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                self.CheckSpeechRecognizationPermission()
            })
        }
    }
    
    func ErrorAlertView(message : String)  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
               //call any function
            
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in

                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                           return
                       }

                       if UIApplication.shared.canOpenURL(settingsUrl) {
                           UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                               print("Settings opened: \(success)") // Prints true
                           })
                       }
            }))
            
            self.present(alert, animated: true, completion: nil)
            
           }
        

    }
}
//------------------------------------------------------------------------------
// MARK:-
// MARK:- SFSpeechRecognizerDelegate Methods
//------------------------------------------------------------------------------

extension SpeechRecognitionVC: SFSpeechRecognizerDelegate {

    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            print(available , "availabilityDidChangeavailabilityDidChange")
            //self.btnStart.isEnabled = true
        } else {
            print(available , "availabilityDidChangeavailabilityDidChange")

            //self.btnStart.isEnabled = false
        }
    }
}
