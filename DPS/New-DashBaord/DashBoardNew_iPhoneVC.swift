//
//  DashBoardNew_iPhoneVC.swift
//  DPS
//  peSTream 2020
//  Created by NavinPatidar on 7/7/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar Test 2020
//  Saavan Test 2020
//  Saavan

import UIKit
import Charts
import FirebaseAnalytics

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}

class DashBoardNew_iPhoneVC: UIViewController,ChartViewDelegate  {
    
    // MARK:
    // MARK: ---------IBOutlet
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collection_DashBoard: UICollectionView!
    @IBOutlet var barChartView: BarChartView!
    @IBOutlet weak var tv_NavigationDrawer: UITableView!
    @IBOutlet weak var view_Menu: UIView!
    
    @IBOutlet weak var viewTopButton: UIView!
    
    @IBOutlet weak var btnChartAmount: UIButton!
    @IBOutlet weak var btnChartCount: UIButton!
    @IBOutlet weak var viewChartButton: UIView!
    
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    @IBOutlet weak var widthMenuView: NSLayoutConstraint!
    @IBOutlet weak var lbltaskCount: UILabel!
    @IBOutlet weak var lblScheduleCount: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var btnPerformance: UIButton!
    @IBOutlet weak var btnLeaders: UIButton!
    
    @IBOutlet weak var lblPerformance: UILabel!
    @IBOutlet weak var lblLeaders: UILabel!
    
    @IBOutlet weak var const_BarChartView_H: NSLayoutConstraint!
    
    @IBOutlet weak var viewChart: UIView!
    var leaderViewController :Leader_iPhoneVC?
    
    // MARK:
    // MARK: ---------VAriable
    var aryColltionData = NSMutableArray() , arytv_NavigationDrawer = NSMutableArray()
    var dictDashBoardData = NSMutableDictionary()
    
    var xaxisValue: [String] = []
    var unitsProposed: [Double] = []
    var unitsSold: [Double] = []
    var loader = UIAlertController()
    var loaderPerformance = UIAlertController()
    
    var strEmpNo = "" , strEmpName = "" , strSalesProcessCompanyId = "" , strCompanyKey = "" , strCompanyId = "" , strServiceUrlMain = "" , strCompanyIdServiceAuto = "" , strServiceUrlMainServiceAutomation = "" , strServiceUrlMainSalesAutomation = "" , strCustomerTabType = "" , strEmployeeId = "" , strChartType = "Amount" , strTaskHeading = "Today's Task" , strServiceUrlMainReceivePO = ""
    
    var dictLoginData = NSDictionary()
    
    let dispatchGroup = DispatchGroup()
    let dispatchGroupPerformance = DispatchGroup()
    
    var isAnimateView = true
    
    var badgeSchedule = SPBadge()
    var badgeTasks = SPBadge()
    
    var secondViewController :Leader_iPhoneVC?
    var arrDashboardWidget = NSArray()
    
    var showChart = true
    
    var isOpenMenu = false
    
    // MARK:
    // MARK: ----------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      //  nsud.set(nil, forKey: "LoginDetails")
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            nsud.set("New", forKey: "AppointmentFlow")
            //nsud.set("Old", forKey: "AppointmentFlow")
            nsud.synchronize()
            if(DeviceType.IS_IPAD){
                
    //            lbltaskCount.text = "0"
    //            lblScheduleCount.text = "0"
    //            lbltaskCount.layer.cornerRadius = 18.0
    //            lbltaskCount.backgroundColor = UIColor.red
    //            lblScheduleCount.layer.cornerRadius = 18.0
    //            lblScheduleCount.backgroundColor = UIColor.red
    //            lbltaskCount.layer.masksToBounds = true
    //            lblScheduleCount.layer.masksToBounds = true
                
                nsud.set("New", forKey: "AppointmentFlow")
                //nsud.set("Old", forKey: "AppointmentFlow")
                nsud.synchronize()
                
            }
            
            // self.navigationController?.viewControllers = [self]
            
            barChartView.isHidden = true
            isAnimateView = true
            
            // Call Initial Loading Functions
            
            getLoginData()
            self.setUI()
            setAllDefaultsNull()
            setBoolInUserDefaults()
            
            // Check If For Signed Agreements From Notification Click
            if nsud.bool(forKey: "SignedAgreementNotification") == true {
                UserDefaults.standard.set(false, forKey: "SignedAgreementNotification")
                goToSignedAgreements()
            }
            
            // Check if Disk Space is Full
            
            Global().diskSpaceCheck(self)
            
            if(isInternetAvailable()){
                
                // Logic to check if User's Branch is changed then sync Masters
                
                var isBranchChanged = false
                
                let empBranchL = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
                
                if(nsud.value(forKey: "SavedEmployeeBranchSysName") != nil){

                    let savedEmpBranchL = "\(nsud.value(forKey: "SavedEmployeeBranchSysName")!)"
                    
                    if empBranchL != savedEmpBranchL {
                        
                        // Sync Masters
                        isBranchChanged = true
                        nsud.setValue(empBranchL, forKey: "SavedEmployeeBranchSysName")

                    }
                    
                }else{
                    
                    isBranchChanged = true
                    nsud.setValue(empBranchL, forKey: "SavedEmployeeBranchSysName")

                }
                
                
                // Check If to Download
                
                if Global().logicForDownloadingMasters() == true {
                    
                    nsud.setValue(empBranchL, forKey: "SavedEmployeeBranchSysName")
                    
                    self.downloadAllMaster(yesDwonload: true)
                    
                }else if isBranchChanged{
                    
                    nsud.setValue(empBranchL, forKey: "SavedEmployeeBranchSysName")

                    self.downloadAllMaster(yesDwonload: true)
                    
                } else{
                    
                    setDataOnView()
                    
                    //objcGetEmployeePerformanceDetailsInBackGround(FromDate: "", ToDate: "")
                    DispatchQueue.global(qos: .background).async {
                        self.getEmpPerformanceAsync(FromDate: "", ToDate: "")
                    }

                    //objcGetEmployeeChartDetailsBackGround()
                    DispatchQueue.global(qos: .background).async {
                        self.getEmpChartAsync()
                    }
                    
                    self.checkIfFromShortCut()
                    
                }
                
            }else{
                
                setDataOnView()
                
            }
            
            
            
            
            // Status Bar color to theme color
            
            if #available(iOS 13.0, *) {
                let statusBar1 =  UIView()
                statusBar1.frame = (UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame)!
                statusBar1.backgroundColor = UIColor.theme()
                UIApplication.shared.keyWindow?.addSubview(statusBar1)
                
            } else {
                let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
                statusBar1.backgroundColor = UIColor.theme()
            }
            
            nsud.setValue(NSDictionary(), forKey: "DashBoardEmployeePerformanceFilter")
            nsud.synchronize()
            
        }
        
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view_Menu.removeFromSuperview()

        appUpdateAlert()
        
        if !Check_Login_Session_expired() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                //self.setTopMenuOption()
                self.setFooterMenuOption()
            })
            
            // DeviceType.IS_IPAD ? AppUtility.lockOrientation(.all) : AppUtility.lockOrientation(.portrait)
            alertToForceFullyUpdateApp()
        }
       
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //de   AppUtility.lockOrientation(.portrait)
        
        // Don't forget to reset when view is being removed
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            //self.setTopMenuOption()
            self.setFooterMenuOption()
        })
    }
    // MARK:
    // MARK: ---------UI Initialize
    
    func appUpdateAlert()  {
        
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        //var strAlertMsg = "Your app is outdated, please update the app, Latest App Version #: \(appVersion ?? "")"
        // check if ForcefullyUpdate App
        
        if nsud.bool(forKey: "AppUpdateAvailable") == true {
            
            // Alert To ForceFullyUpdateApp
            
            //var strAlertMsg = "Your app is outdated, please update the app, Latest App Version #: \(appVersion ?? "")"
            
            var strAlertMsg = "Your app is out dated. Please update the app from app store. Latest App Version #: \(appVersion ?? "")"
            
            if "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.AppUpdateMessage") ?? "")".count != 0
            {
                strAlertMsg = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.AppUpdateMessage") ?? "")"
            }
            let alert = UIAlertController(title: Alert, message: strAlertMsg, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in

            }))

            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: {
            })
            
        }
        
    }
    
    func setUI() {
        
//        DispatchQueue.global().async {
//            do {
//
//                let update = try self.isUpdateAvailable()
//
//                print("update",update)
//                DispatchQueue.main.async {
//                    if update{
//
//                    }
//                }
//            } catch {
//                print(error)
//            }
//        }
        // check if New Version is Available
        
        let arrOfImplemetedMenusOnApp = ["Profile","Dashboard","Appointment","Manage Opportunity/Lead","Signed Agreement","Task","Activity","Contacts List","Settings","Opportunity/Lead History","Sync Masters","Day Clock In/Out","Statistics","Nearby","Change Password","View schedule","Logout","Weekly Time Sheet","Generate WorkOrder","Purchase Order History","Quote History","Receive Purchase Order","View Schedule","Assign an Area","Live Tracking","Help","Switch Branch","Pending Approval","Support"]
        
        let aryMenuMaster = dictLoginData.value(forKey: "MenuLinkMasters") as! NSArray
        if aryMenuMaster.count > 0 {
            let dict = NSMutableDictionary()
            dict.setValue("Profile", forKey: "title")
            dict.setValue("edit_profile_nav_icon2.png", forKey: "image")
            self.arytv_NavigationDrawer.add(dict)
            for item in aryMenuMaster {
                if((item as AnyObject).value(forKey: "Title")as! String != "Profile" && (item as AnyObject).value(forKey: "Title")as! String != "Logout" && arrOfImplemetedMenusOnApp.contains((item as AnyObject).value(forKey: "Title")as! String)){
                    let dict = NSMutableDictionary()
                    dict.setValue((item as AnyObject).value(forKey: "Title")as! String, forKey: "title")
                    dict.setValue((item as AnyObject).value(forKey: "Icon")as! String, forKey: "image")
                    self.arytv_NavigationDrawer.add(dict)
                }
            }
            let dict1 = NSMutableDictionary()
            
//            let dictSwitchBranch = NSMutableDictionary()
//            dictSwitchBranch.setValue("Switch Branch", forKey: "title")
//            dictSwitchBranch.setValue("services.png", forKey: "image")
            
            dict1.setValue("Logout", forKey: "title")
            dict1.setValue("logout.png", forKey: "image")
            
//            self.arytv_NavigationDrawer.add(dictSwitchBranch)
            
            self.arytv_NavigationDrawer.add(dict1)
            self.tv_NavigationDrawer.reloadData()
            
        }else{
            
            // get MenuOptions
            
            let dictDataMenu = Global().dictMenuOptions()
            
            let slideMenuArray = dictDataMenu?.value(forKey: "slideMenuArray") as! NSArray
            let slideMenuIconsArray = dictDataMenu?.value(forKey: "slideMenuIconsArray") as! NSArray
            
            for k in 0 ..< slideMenuIconsArray.count {
                
                let dict = NSMutableDictionary()
                dict.setValue(slideMenuArray[k], forKey: "title")
                dict.setValue(slideMenuIconsArray[k], forKey: "image")
                self.arytv_NavigationDrawer.add(dict)
                
            }
            
            self.tv_NavigationDrawer.reloadData()
            
        }
        
        //---------
        
        /*if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
         txfSearchField.borderStyle = .none
         txfSearchField.backgroundColor = .white
         txfSearchField.layer.cornerRadius = 15
         txfSearchField.layer.masksToBounds = true
         txfSearchField.layer.borderWidth = 1
         txfSearchField.layer.borderColor = UIColor.white.cgColor
         }
         searchBar.layer.borderColor = hexStringToUIColor(hex: appThemeColor).cgColor
         searchBar.layer.borderWidth = 1
         searchBar.layer.opacity = 1.0*/
        
        btnLeaders.layer.cornerRadius = btnLeaders.frame.size.height/2
        btnLeaders.layer.borderWidth = 0
        
        viewTopButton.layer.cornerRadius = viewTopButton.frame.size.height/2
        viewTopButton.layer.borderWidth = 0
        
        btnPerformance.layer.cornerRadius = btnPerformance.frame.size.height/2
        btnPerformance.layer.borderWidth = 0
        
        //
        btnChartAmount.layer.cornerRadius = btnChartAmount.frame.size.height/2
        btnChartAmount.layer.borderWidth = 0
        
        btnChartCount.layer.cornerRadius = btnChartCount.frame.size.height/2
        btnChartCount.layer.borderWidth = 0
        
        viewChartButton.layer.cornerRadius = viewChartButton.frame.size.height/2
        viewChartButton.layer.borderWidth = 0
        
        
        setupView()
        
    }
    // MARK:- General Methods -
    
    func setupView() {
        
        //legend
        let legend = barChartView.legend
        legend.enabled = true
        legend.horizontalAlignment = .left
        legend.verticalAlignment = .top
        legend.orientation = .horizontal
        legend.drawInside = false
        legend.yOffset = 10.0;
        legend.xOffset = 10.0;
        legend.yEntrySpace = 0.0;
        legend.textColor = UIColor.black
        legend.font =   DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 15) : UIFont.systemFont(ofSize: 10)
        // Y - Axis Setup
        let yaxis = barChartView.leftAxis
        yaxis.spaceTop = 0.35
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = true
        yaxis.labelTextColor = UIColor.black
        yaxis.axisLineColor = UIColor.black
        yaxis.labelFont = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 15) : UIFont.systemFont(ofSize: 10)
        barChartView.rightAxis.enabled = false
        /*barChartView.scaleYEnabled = false
         barChartView.scaleXEnabled = false
         barChartView.pinchZoomEnabled = false
         barChartView.doubleTapToZoomEnabled = false*/
        
        // X - Axis Setup
        let xaxis = barChartView.xAxis
        let formatter = CustomLabelsXAxisValueFormatter()//custom value formatter
        formatter.labels = xaxisValue
        xaxis.valueFormatter = formatter
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bottom
        xaxis.labelTextColor = UIColor.black
        xaxis.centerAxisLabelsEnabled = true
        xaxis.axisLineColor = UIColor.black
        xaxis.granularityEnabled = true
        xaxis.enabled = true
        xaxis.labelFont = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 15) : UIFont.systemFont(ofSize: 10)
        barChartView.delegate = self
        barChartView.noDataText = "No Data Available"
        barChartView.noDataTextColor = hexStringToUIColor(hex: appThemeColor)
        barChartView.chartDescription?.textColor = UIColor.clear
        setChart()
    }
    func setChart() {
        barChartView.noDataText = "Loading...!!"
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []
        for i in 0..<xaxisValue.count {
            
            let dataEntry = BarChartDataEntry(x: Double(i) , y: Double(unitsProposed[i]))
            dataEntries.append(dataEntry)
            let dataEntry1 = BarChartDataEntry(x: Double(i) , y: Double(unitsSold[i]))
            dataEntries1.append(dataEntry1)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: " Proposed ")
        chartDataSet.valueFont = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 12) : UIFont.systemFont(ofSize: 7)
        let chartDataSet1 = BarChartDataSet(entries: dataEntries1, label: " Sold ")
        chartDataSet1.valueFont = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 12) : UIFont.systemFont(ofSize: 7)
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1]
        chartDataSet.colors = [UIColor(red: 252/255, green: 213/255, blue: 97/255, alpha: 1)]
        chartDataSet1.colors = [UIColor(red: 134/255, green: 197/255, blue: 120/255, alpha: 1)]
        let chartData = BarChartData(dataSets: dataSets)
        let groupSpace = 0.4
        let barSpace = 0.03
        let barWidth = 0.2
        chartData.barWidth = barWidth
        
        barChartView.xAxis.axisMinimum = 0.0
        barChartView.xAxis.axisMaximum = 0.0 + chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace) * Double(xaxisValue.count)
        
        chartData.groupBars(fromX: 0.0, groupSpace: groupSpace, barSpace: barSpace)
        
        barChartView.xAxis.granularity = barChartView.xAxis.axisMaximum / Double(xaxisValue.count)
        barChartView.data = chartData
        barChartView.notifyDataSetChanged()
        barChartView.setVisibleXRangeMaximum(4)
        
        if isAnimateView {
            barChartView.animate(yAxisDuration: 1.0, easingOption: .easeInOutBounce)
        }
        
        //barChartView.animate(yAxisDuration: 1.0, easingOption: .linear)
        
        // chartData.setValueTextColor(UIColor.white)
    }
    
    // MARK: ---------------------------Footer Functions ---------------------------
    func setFooterMenuOption() {
        
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        
        nsud.setValue("Home", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))
        
        footorView.removeFromSuperview()
        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            
            Analytics.setDefaultEventParameters([
              "Action_Name": "Schedule - DashBoard Footer",
              "EmployeeId": Global().getEmployeeId(),
              "Username": Global().getEmployeeName(viaId: Global().getEmployeeId()),
              "Action_FromView": "DashBoard View"
            ])
            
            self.GotoScheduleViewController()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            
            Analytics.setDefaultEventParameters([
              "Action_Name": "Account - DashBoard Footer",
              "EmployeeId": Global().getEmployeeId(),
              "Username": Global().getEmployeeName(viaId: Global().getEmployeeId()),
              "Action_FromView": "DashBoard View"
            ])
            
            self.GotoAccountViewController()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
            print("call home")
            
            Analytics.setDefaultEventParameters([
              "Action_Name": "Sales - DashBoard Footer",
              "EmployeeId": Global().getEmployeeId(),
              "Username": Global().getEmployeeName(viaId: Global().getEmployeeId()),
              "Action_FromView": "DashBoard View"
            ])
            
            self.GotoSalesViewController()
        }
        
    }
    
    func GotoAccountViewController() {
        self.view.endEditing(true)
        let objWebService = WebService()
        objWebService.setDefaultCrmValue()
        
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func GotoSalesViewController() {
        self.view.endEditing(true)
        let objWebService = WebService()
        objWebService.setDefaultCrmValue()
        let mainStoryboard = UIStoryboard(
            name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
            bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "WebLeadVC") as? WebLeadVC
        self.navigationController?.pushViewController(objByProductVC!, animated: false)
    }
    func GotoScheduleViewController() {
        self.view.endEditing(true)
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "Main",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }
    }
    // MARK:- ChartView Delegate -
    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: Highlight) {
        //        print("\(entry.value) in \(xaxisValue[entry.x])")
    }
    
    
    
    func setDataOnView(dictResponce : NSMutableDictionary)  {
        
        if(dictResponce.count != 0){
            self.dictDashBoardData = NSMutableDictionary()
            self.dictDashBoardData = dictResponce.mutableCopy()as! NSMutableDictionary
            manageDashboardWidget()
        }
        self.collection_DashBoard.reloadData()
        
        //self.createBadgeView()
        
    }
    
    func setDataOnChartView(dictResponce : NSMutableDictionary)  {
        
        self.xaxisValue = []
        self.unitsSold = []
        self.unitsProposed = []
        
        if(dictResponce.count != 0){
            
            if strChartType == "Amount" {
                
                if(dictResponce.value(forKey: "AmountChartValues") is NSArray){
                    self.xaxisValue = []
                    self.unitsSold = []
                    self.unitsProposed = []
                    self.barChartView.isHidden = false
                    
                    for item in dictResponce.value(forKey: "AmountChartValues")as! NSArray {
                        let x = (item as AnyObject).value(forKey: "MonthName")
                        let Proposed = (item as AnyObject).value(forKey: "Proposal")
                        let Sold = (item as AnyObject).value(forKey: "Sold")
                        self.xaxisValue.append("\(x!)")
                        self.unitsProposed.append(Double("\(Proposed!)")!)
                        self.unitsSold.append(Double("\(Sold!)")!)
                    }
                }
                
            } else {
                
                if(dictResponce.value(forKey: "CountChartValues") is NSArray){
                    self.xaxisValue = []
                    self.unitsSold = []
                    self.unitsProposed = []
                    self.barChartView.isHidden = false
                    
                    for item in dictResponce.value(forKey: "CountChartValues")as! NSArray {
                        let x = (item as AnyObject).value(forKey: "MonthName")
                        let Proposed = (item as AnyObject).value(forKey: "Proposal")
                        let Sold = (item as AnyObject).value(forKey: "Sold")
                        self.xaxisValue.append("\(x!)")
                        self.unitsProposed.append(Double("\(Proposed!)")!)
                        self.unitsSold.append(Double("\(Sold!)")!)
                    }
                }
                
            }
            
        }
        setupView()
    }
    
    
    // MARK:
    // MARK: ----------IBAction
    
    @IBAction func actionOnChartAmount(_ sender: UIButton) {
        self.view.endEditing(true)
        btnChartAmount.backgroundColor = hexStringToUIColor(hex: "D0B50E")
        btnChartAmount.setTitleColor(.white, for: .normal)
        btnChartCount.backgroundColor = UIColor.clear
        btnChartCount.setTitleColor(.black, for: .normal)
        strChartType = "Amount"
        isAnimateView = true
        
        if(nsud.value(forKey: "DashBoardPerformanceChart") != nil){
            
            let dictDataChart = nsud.value(forKey: "DashBoardPerformanceChart")
            
            self.setDataOnChartView(dictResponce: (dictDataChart as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            
        }
        
    }
    
    @IBAction func actionOnChartCount(_ sender: UIButton) {
        self.view.endEditing(true)
        btnChartCount.backgroundColor = hexStringToUIColor(hex: "D0B50E")
        btnChartCount.setTitleColor(.white, for: .normal)
        btnChartAmount.backgroundColor = UIColor.clear
        btnChartAmount.setTitleColor(.black, for: .normal)
        strChartType = "Count"
        isAnimateView = true
        
        if(nsud.value(forKey: "DashBoardPerformanceChart") != nil){
            
            let dictDataChart = nsud.value(forKey: "DashBoardPerformanceChart")
            self.setDataOnChartView(dictResponce: (dictDataChart as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            
        }
       
    }
    @IBAction func actionOnPerformance(_ sender: UIButton) {
        self.view.endEditing(true)
        self.lblLeaders.backgroundColor = UIColor.clear
        self.lblPerformance.backgroundColor = UIColor.white

        //self.removeChild()
       
    
    }
    @IBAction func actionOnLeaderss(_ sender: UIButton) {
        self.view.endEditing(true)
        
        nsud.setValue(NSDictionary(), forKey: "DashBoardEmployeeLeaderFilter")
        nsud.synchronize()
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "Leader_iPhoneVC") as! Leader_iPhoneVC
                
        self.navigationController?.pushViewController(vc, animated: false)
        
    
    }
    func goToSwitchBranch() {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "DashBoard" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SwitchBranchVC") as! SwitchBranchVC
                
        vc.reDirectWhere = "Dashboard"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func action_Back(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        /*if isInternetAvailable() {
            
            let dictPaymentDetails = NSMutableDictionary()

            dictPaymentDetails.setValue("441667", forKey: "leadId")
            dictPaymentDetails.setValue("1364380", forKey: "billToId")
            dictPaymentDetails.setValue("1.00", forKey: "amount")
            dictPaymentDetails.setValue("Ankit Kasliwal", forKey: "nameOnCard")
            dictPaymentDetails.setValue("11802 Warfield st11802 Warfield st", forKey: "billingAddress")
            dictPaymentDetails.setValue("78216", forKey: "billingPostalCode")
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestPacPayment" : "PestPacPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "PestPacPaymentVC") as? PestPacPaymentVC
            vc?.dictPaymentDetails = dictPaymentDetails
            self.navigationController?.pushViewController(vc!, animated: false)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }*/
        
        if isInternetAvailable() {
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterDashboardNew_iPhoneVC") as? FilterDashboardNew_iPhoneVC
            vc!.delegate = self
            vc!.strViewComeFrom = "Dashboard"
            self.navigationController?.present(vc!, animated: true)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }
        
    }
    
    @IBAction func actionOnMenu(_ sender: UIButton) {
        
        isOpenMenu = true
        
        self.view.endEditing(true)
        self.view.addSubview(self.view_Menu!)
        self.view_Menu.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.view_Menu.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }) { (completed) in
        }
    }
    
    @IBAction func actionOnMenuClose(_ sender: UIButton) {
        self.view.endEditing(true)

        isOpenMenu = false
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.view_Menu.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }) { (completed) in
            self.view_Menu.removeFromSuperview()
        }
        
    }
    
    @IBAction func actionOnLead(_ sender: UIButton) {
        self.view.endEditing(true)
        GotoSalesViewController()
        
    }
    @IBAction func actionOnTask(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let objWebService = WebService()
        objWebService.setDefaultCrmValue()
        
        let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
        objByProductVC.strFromVC = "DashBoardView"
        let userDef = UserDefaults.standard
        userDef.setValue(nil, forKey: "dictFilterTask")
        userDef.synchronize()
        self.navigationController?.pushViewController(objByProductVC, animated: false)
        
    }
    
    @IBAction func actionOnContact(_ sender: UIButton) {
        self.view.endEditing(true)
        GotoAccountViewController()
    }
    @IBAction func actionOnAppointment(_ sender: UIButton) {
        self.view.endEditing(true)
        GotoScheduleViewController()
    }
    @IBAction func actionOnMore(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let global = Global()
        let isnet = global.isNetReachable()
        if isnet {
            
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            
            
            
            let Near = (UIAlertAction(title: "Near By", style: .default , handler:{ (UIAlertAction)in
                print("User click Near By button")
                let global = Global()
                let isnet = global.isNetReachable()
                if isnet {
                    /*
                     if(DeviceType.IS_IPAD){
                     let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                     let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
                     self.navigationController?.pushViewController(vc!, animated: false)
                     }else{
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone*/
                    //self.navigationController?.pushViewController(vc!, animated: false)
                    
                    let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
                
                else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
            }))
            Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
            Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(Near)
            
            let Signed = (UIAlertAction(title: "Signed View Agreements", style: .default , handler:{ (UIAlertAction)in
                print("User click Signed View Agreements")
                if(DeviceType.IS_IPAD){
                    let mainStoryboard = UIStoryboard(
                        name: "CRMiPad",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
                    self.navigationController?.present(objByProductVC!, animated: true)
                }else{
                    let mainStoryboard = UIStoryboard(
                        name: "CRM",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
                    self.navigationController?.present(objByProductVC!, animated: true)
                }
                
            }))
            
            Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
            Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(Signed)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Add Cancel  button")
            }))
            
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as UIView
                popoverController.sourceRect = sender.bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
            }
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        } else {
            global.alertMethod(Alert, ErrorInternetMsg)
        }
        
    }
    
    @IBAction func actionOnAdd(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let global = Global()
        let isnet = global.isNetReachable()
        if isnet {
            
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as UIView
                popoverController.sourceRect = sender.bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
            }
            
            let AddTask = (UIAlertAction(title: "Add Task", style: .default , handler:{ (UIAlertAction)in
                
                print("User click Add Task button")
                let defs = UserDefaults.standard
                defs.setValue(nil, forKey: "TaskFilterSort")
                defs.synchronize()
                
                let objWebService = WebService()
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
                
                objByProductVC.dictTaskDetailsData = objWebService.setDefaultTaskDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
            }))
            
            AddTask.setValue(#imageLiteral(resourceName: "Task"), forKey: "image")
            AddTask.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddTask)
            
            
            
            let AddActivity = (UIAlertAction(title: "Add Activity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Activity button")
                
                let objWebService = WebService()
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
                
                
                
                objByProductVC.dictActivityDetailsData = objWebService.setDefaultActivityDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddActivity.setValue(#imageLiteral(resourceName: "Add Activity"), forKey: "image")
            AddActivity.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddActivity)
            
            let AddLead = (UIAlertAction(title: "Add Lead/Opportunity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Add Lead button")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
                self.navigationController?.pushViewController(vc, animated: false)
            }))
            AddLead.setValue(#imageLiteral(resourceName: "Lead"), forKey: "image")
            AddLead.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddLead)
            
            
            let AddContact = (UIAlertAction(title: "Add Contact", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Contact  button")
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
                
                
                
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddContact.setValue(#imageLiteral(resourceName: "Icon ionic-md-person"), forKey: "image")
            AddContact.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddContact)
            
            
            let AddCompany = (UIAlertAction(title: "Add Company", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Company  button")
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
                
                
                self.navigationController?.pushViewController(controller, animated: false)
            }))
            AddCompany.setValue(#imageLiteral(resourceName: "CompanyAdd"), forKey: "image")
            AddCompany.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddCompany)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Add Cancel  button")
            }))
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        } else {
            global.alertMethod(Alert, ErrorInternetMsg)
        }
        
    }
    
    // MARK: -
    // MARK: --------API Calling From Objective C to Swift----------------Start All Functions
    
    func objcGetEmployeeListAPI() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMain + UrlGetEmployeeList + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "Employee", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetCrmMasterAPI() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMain + UrlGetLeadDetailMaster + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "CRM", withCallback: { success, response, error in
                //print(response)
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetSalesMasterAPIInBackground() {
                
        DispatchQueue.global(qos: .background).async(execute: {
        
        //DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainSalesAutomation + UrlGetMasterSalesAutomation + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "Sales", withCallback: { success, response, error in
                
                DispatchQueue.main.async {

                    //
                    print("Sales Master Background Synced")
                    
                }
                
            })
            
        //})
        
        })
        
    }
    
    func objcGetSalesMasterAPI() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainSalesAutomation + UrlGetMasterSalesAutomation + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "Sales", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetSalesDynamicMasterAPI() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainSalesAutomation + UrlSalesDynamicFormMasters + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "SalesDynamic", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetServiceMasterAPI() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainServiceAutomation + UrlGetMasterServiceAutomation + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "Service", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetServiceDynamicMasterAPI() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainServiceAutomation + UrlServiceDynamicFormMasters + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "ServiceDynamic", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetServiceProductMasterAPI() {
                
        DispatchQueue.global(qos: .background).async(execute: {
        
        //DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainServiceAutomation + UrlGetMasterProductChemicalsServiceAutomation + self.strCompanyIdServiceAuto
            
            Global().getDataFromSwift(strURL, "ServiceProduct", withCallback: { success, response, error in
                
                DispatchQueue.main.async {
                
                    // Nothing to do already saved In NsUserDefaults
                    
                }
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        //})
        
        })
        
    }
    
    func objcGetServiceProductMasterAPIInMainQueue() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainServiceAutomation + UrlGetMasterProductChemicalsServiceAutomation + self.strCompanyIdServiceAuto
            
            Global().getDataFromSwift(strURL, "ServiceProduct", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    func objcGetServiceEquipmentMasterAPI() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainServiceAutomation + UrlGetMasterEquipmentDynamicForm + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "ServiceEquipment", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                print(self.dispatchGroup.debugDescription)
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetEmployeePerformanceDetailsInBackGround(FromDate : String ,ToDate : String )
    {
        
        isAnimateView = false
                
        DispatchQueue.global(qos: .background).async(execute: {
        
        //DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMain + UrlGetDashBoardDetailsNew + self.strEmployeeId + "&FromDate=\(FromDate)&ToDate=\(ToDate)"
            
            Global().getDataFromSwift(strURL, "EmployeePerformanceDetails", withCallback: { success, response, error in
                
                DispatchQueue.main.async {
                
                    self.setDataOnView()
                
                }

                
            })
            
        //})
        
        })
        
    }
    func objcGetEmployeePerformanceDetailsInMainQueue(FromDate : String ,ToDate : String )
    {
        
        isAnimateView = false
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMain + UrlGetDashBoardDetailsNew + self.strEmployeeId + "&FromDate=\(FromDate)&ToDate=\(ToDate)"
            
            Global().getDataFromSwift(strURL, "EmployeePerformanceDetails", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }

                self.setDataOnView()
                
                //}
                
                if success {
                    
                    
                    
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetEmployeeChartDetailsBackGround()
    {
        
        isAnimateView = false
        
        DispatchQueue.global(qos: .background).async(execute: {
        
        //DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMain + UrlGetDashBoardDetailsChartData + self.strEmployeeId
            
            Global().getDataFromSwift(strURL, "EmployeeChartDetails", withCallback: { success, response, error in
                
                DispatchQueue.main.async {
                
                    self.setDataOnView()
                
                }
                
            })
            
        //})
        
        })
        
    }
    
    func objcGetEmployeeChartDetailsInMainQueue()
    {
        
        isAnimateView = false
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMain + UrlGetDashBoardDetailsChartData + self.strEmployeeId
            
            Global().getDataFromSwift(strURL, "EmployeeChartDetails", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                self.setDataOnView()
                
                //}
                
                if success {
                    
                    
                    
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func deleteMechanicalWOs() {
        
        Global().deleteFromCoreDataMechancialWOs()
        
    }
    
    func objcGetPlumbingMasterAPI() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainServiceAutomation + UrlMechanicalGetAllMastersNew + self.strCompanyKey + UrlGetTotalWorkOrdersServiceAutomationEmployeeNo + self.strEmpNo + "&branchSysName=" + "\(Global().strEmpBranchSysName() ?? "")"
            
            Global().getDataFromSwift(strURL, "PlumbingMaster", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    
    func objcGetWdoMasterAPI() {
                
        DispatchQueue.global(qos: .background).async(execute: {
        
        //DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainServiceAutomation + UrlWdoMaster + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "WDOMaster", withCallback: { success, response, error in
                
                DispatchQueue.main.async {
                
                    // Nothing to do already saved In NsUserDefaults
                    
                }
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        //})
        
        })
        
    }
    
    func objcGetWdoMasterAPIInMainQueue() {
        
        self.dispatchGroup.enter()
        
        //DispatchQueue.global(qos: .default).async(execute: {
        
        DispatchQueue.main.async(execute: {
            
            let strURL = self.strServiceUrlMainServiceAutomation + UrlWdoMaster + self.strCompanyKey
            
            Global().getDataFromSwift(strURL, "WDOMaster", withCallback: { success, response, error in
                
                //DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
                //}
                
                if success {
                } else {
                    
                    // Error Do Nothing
                    
                }
                
            })
            
        })
        
        //})
        
    }
    func manageDashboardWidget()
    {
        //return  self.dictDashBoardData.count != 0 ? 6 : 0
        
        if nsud.value(forKey: "DashboardWidgets") is NSArray
        {
            arrDashboardWidget = nsud.value(forKey: "DashboardWidgets") as! NSArray
            
            let arrData = dictLoginData.value(forKey: "DashboardWidgetMasters") as! NSArray
            
            let arrNew = NSMutableArray()

            for item in arrDashboardWidget
            {
                
                let str = item as! String
                
                for itemSecond in arrData
                {
                    let dictSecond = itemSecond as! NSDictionary
                    
                    if str == "\(dictSecond.value(forKey: "Title") ?? "")"
                    {
                        arrNew.add(dictSecond)
                    }
                }
            }
              
            arrDashboardWidget = arrNew
            
        }
        
        let arrTemp = NSMutableArray()
        
        showChart = false
        
        for item in arrDashboardWidget
        {
            let dict  = item as! NSDictionary
            
            if "\(dict.value(forKey: "Title") ?? "")" == "Chart"
            {
                showChart = true
            }
            else
            {
                arrTemp.add(dict)
            }
        }
        arrDashboardWidget = arrTemp

        if showChart
        {
            barChartView.isHidden = false
            
            
            viewChartButton.isHidden = false
            btnChartAmount.isHidden = false
            btnChartCount.isHidden = false

        }
        else
        {
            barChartView.isHidden = true
            viewChartButton.isHidden = true
            btnChartAmount.isHidden = true
            btnChartCount.isHidden = true

        }
        
        if DeviceType.IS_IPAD
        {
            if showChart
            {
                const_BarChartView_H.constant = 500
            }
            else
            {
                const_BarChartView_H.constant = 0
            }
        }
        else
        {
            if showChart
            {
                viewChart.isHidden = false
                
            }
            else
            {
                viewChart.isHidden = true
            }
        }
    

    }
    
    // MARK: -
    // MARK: --------API Calling From Objective C to Swift----------------End All Functions
    
    
    // MARK: --------------------------- DashBaord Performance Function After Filter ---------------------------
    
    func getEmployeePerformanceOnFilter(FromDate : String ,ToDate : String)  {
        
        loaderPerformance = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        
        self.present(loaderPerformance, animated: false, completion: nil)
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroupPerformance.enter(); print("dispatchGroupPerformanceEntered")
        
        let strURL = self.strServiceUrlMain + UrlGetDashBoardDetailsNew + self.strEmployeeId + "&FromDate=\(FromDate)&ToDate=\(ToDate)"
        
        print("Employee Performance API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroupPerformance.leave(); print("dispatchGroupPerformance Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Employee Performance API Data From SQL Server.")
                
                print("Employee Performance API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSDictionary {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        print("Employee Performance Data on Applying Filter ----- %@",dictData)
                        
                        let dictTemp = dictData.value(forKey: "data") as! NSDictionary
                        
                        nsud.setValue(dictTemp, forKey: "DashBoardPerformance")
                        
                        nsud.synchronize()
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    
    
    // MARK: --------------------------- DashBaord Performance and Chart In Background ---------------------------
    
    func getEmpPerformanceAsync(FromDate : String ,ToDate : String)  {
        
        isAnimateView = false

        let start1 = CFAbsoluteTimeGetCurrent()
                
        let strURL = self.strServiceUrlMain + UrlGetDashBoardDetailsNew + self.strEmployeeId + "&FromDate=\(FromDate)&ToDate=\(ToDate)"
        
        print("Employee Performance API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                if(status)
                {
                    
                    let diff1 = CFAbsoluteTimeGetCurrent() - start1
                    print("Took \(diff1) seconds to get Employee Performance API Data From SQL Server.")
                    
                    print("Employee Performance API Response")
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        if response.value(forKey: "data") is NSDictionary {
                            
                            let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                            
                            print("Employee Performance Data on Applying Filter ----- %@",dictData)
                            
                            let dictTemp = dictData.value(forKey: "data") as! NSDictionary
                            
                            nsud.setValue(dictTemp, forKey: "DashBoardPerformance")
                            
                            nsud.synchronize()
                            
                            self.setDataOnView()

                        }
                    }
                }
            }
        }
    }
    
    
    func getEmpChartAsync()  {
        
        isAnimateView = false
        
        let start1 = CFAbsoluteTimeGetCurrent()
                
        let strURL = self.strServiceUrlMain + UrlGetDashBoardDetailsChartData + self.strEmployeeId

        print("Employee Chart API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                if(status)
                {
                    
                    let diff1 = CFAbsoluteTimeGetCurrent() - start1
                    print("Took \(diff1) seconds to get Employee Chart API Data From SQL Server.")
                    
                    print("Employee Chart API Response")
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        if response.value(forKey: "data") is NSDictionary {
                            
                            let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                            
                            print("Employee Chart Data In BackGround ----- %@",dictData)
                            
                            let dictTemp = dictData.value(forKey: "data") as! NSDictionary
                            
                            nsud.setValue(dictTemp, forKey: "DashBoardPerformanceChart")
                            
                            nsud.synchronize()
                            
                            self.setDataOnView()

                        }
                    }
                }
            }
        }
    }
    
    //SalesInspectionFormBuilder - Saavan April 2022
    
    func getSalesInspectionFormBuilderMaster() {
        
        let start1 = CFAbsoluteTimeGetCurrent()

        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strBranchSysNameL = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        
        let strCompanyIdL = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")!)" + "/api/MobileToSaleAuto/GetInspectionFormBuilderMappingMasterByCompanyKeyandBranch?companyKey=\(strCompanyKey)&BranchSysName=\(strBranchSysNameL )&companyid=\(strCompanyIdL)&Type=Sales"
        
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "InspectionFormBuilder") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(Status){
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to GetInspectionFormBuilderMappingMasterByCompanyKeyandBranch API Data From SQL Server.")
                
                let arrData = Response["data"] as! NSArray
                
                if arrData.isKind(of: NSArray.self)
                {
                    if arrData.count > 0
                    {
                        // Before Save Delete All Data from DB
                        
                        deleteAllRecordsFromDB(strEntity: "SalesInspectionFormBuilder", predicate: NSPredicate(format: "companyKey == %@ && userName == %@", Global().getCompanyKey(),Global().getUserName()))
                        
                        // Save Into DB.
                        
                        for index in 0..<arrData.count {
                            
                            let dictDataTemp = arrData[index] as! NSDictionary
                            
                            // Saving Data in DB
                            
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("companyKey")
                            arrOfKeys.add("userName")

                            arrOfKeys.add("branchSysName")
                            arrOfKeys.add("companyId")
                            arrOfKeys.add("createdBy")
                            arrOfKeys.add("createdDate")
                            arrOfKeys.add("flowType")
                            arrOfKeys.add("inspectionFormBuilderBranchMappingId")
                            arrOfKeys.add("inspectionFormBuilderMappingMasterId")
                            arrOfKeys.add("isActive")
                            arrOfKeys.add("modifiedDate")
                            arrOfKeys.add("modifiedBy")
                            arrOfKeys.add("templateKey")
                            arrOfKeys.add("templateName")
                            arrOfKeys.add("type")

                            arrOfValues.add(Global().getCompanyKey())
                            arrOfValues.add(Global().getUserName())
                            
                            arrOfValues.add("\(dictDataTemp.value(forKey: "BranchSysName") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "CompanyId") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "CreatedBy") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "CreatedDate") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "FlowType") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "InspectionFormBuilderBranchMappingId") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "InspectionFormBuilderMappingMasterId") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "IsActive") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "ModifiedDate") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "ModifiedBy") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "TemplateKey") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "TemplateName") ?? "")")
                            arrOfValues.add("\(dictDataTemp.value(forKey: "Type") ?? "")")
                            
                            saveDataInDB(strEntity: "SalesInspectionFormBuilder", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                            
                        }
                    }
                }
            }
        }
    }
    
    //MARK: ----------UserDefineFiled---------
    
    func callGetUserDefinedFieldsJsonByEntityList_API(){

        if (isInternetAvailable()){
            
            let dict = NSMutableDictionary()
            dict.setValue("\(Global().getCompanyKey() ?? "")", forKey: "CompanyKey")
            dict.setValue([Userdefinefiled_DynemicForm_Type.ServiceAddress, Userdefinefiled_DynemicForm_Type.BillingAddress , Userdefinefiled_DynemicForm_Type.Lead , Userdefinefiled_DynemicForm_Type.Opportunity], forKey: "EntityList")
            let strURL = MainUrl + UrlGetMasterGetUserDefinedFieldsJsonByEntityList
            WebService.postDataWithHeadersToServer(dictJson: dict as NSDictionary, url: strURL, strType: "dict"){ (response, status) in
                if(status){
                    deleteAllRecordsFromDB(strEntity: Userdefinefiled_Entity.Entity_UDFMasterData, predicate: NSPredicate(format: "companyKey == %@",Global().getCompanyKey()))

                    let dictdata = response.value(forKey: "data") as! NSDictionary
                  
                    if(dictdata.value(forKey: "UdfMasters") is NSArray){
                        let udfmaster = dictdata.value(forKey: "UdfMasters") as! NSArray
                        let arrOfValues = NSMutableArray()
                        let arrOfKeys = NSMutableArray()

                        arrOfKeys.add("companyKey")
                        arrOfKeys.add("userName")
                        arrOfKeys.add("udfmasterData")

                        arrOfValues.add(Global().getCompanyKey())
                        arrOfValues.add(Global().getUserName())
                        arrOfValues.add(udfmaster)
                        saveDataInDB(strEntity: Userdefinefiled_Entity.Entity_UDFMasterData, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    }
              
                }
                
   
                
            }
        }
    }
}


// MARK: -
// MARK: ----------UICollectionViewDelegate

extension DashBoardNew_iPhoneVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //return  self.dictDashBoardData.count != 0 ? 6 : 0
        return arrDashboardWidget.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(indexPath.section)
        print(indexPath.row)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath as IndexPath) as! DashboardCell
        
        let dict = arrDashboardWidget.object(at: indexPath.row) as! NSDictionary
        
        
        if "\(dict.value(forKey: "Title") ?? "")" == "Today's Task"  //Task
        {
            cell.dashBoard_title_first.text = "\(dict.value(forKey: "Title") ?? "")"
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "Task_Today") ?? "")"
            cell.dashBoard_title_third.text = "Pending \(self.dictDashBoardData.value(forKey: "Task_Pending") ?? "")"
        }
        else if "\(dict.value(forKey: "Title") ?? "")" == "Activities Completed" //Activities
        {
            cell.dashBoard_title_first.text = "Activities Completed"
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "ActivityCount")!)"
            
            if "\(self.dictDashBoardData.value(forKey: "ActivityCountLastMonth")!)".count > 0 {
                
                let ratioPercent = ("\(self.dictDashBoardData.value(forKey: "ActivityCountRatio")!)" as NSString).doubleValue
                let ratioPercentToShow =  String(format: "%.02f", Double(ratioPercent))
                
                if "\(self.dictDashBoardData.value(forKey: "ActivityCountRatio")!)".contains("-") {
                    
                    cell.dashBoard_title_third.text = "\(self.dictDashBoardData.value(forKey: "ActivityCountLastMonth")!) / \(ratioPercentToShow)%"
                    
                } else {
                    
                    cell.dashBoard_title_third.text = "\(self.dictDashBoardData.value(forKey: "ActivityCountLastMonth")!) / +\(ratioPercentToShow)%"
                    
                }
                
            } else {
                
                cell.dashBoard_title_third.text = ""
                
            }
        }
        else if "\(dict.value(forKey: "Title") ?? "")" == "Lead Created" //Lead Created
        {
            cell.dashBoard_title_first.text = "Lead Created"
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "WebLeadCount")!)"
            
            if "\(self.dictDashBoardData.value(forKey: "WebLeadCountLastMonth")!)".count > 0 {
                
                let ratioPercent = ("\(self.dictDashBoardData.value(forKey: "WebLeadCountRatio")!)" as NSString).doubleValue
                let ratioPercentToShow =  String(format: "%.02f", Double(ratioPercent))
                
                if "\(self.dictDashBoardData.value(forKey: "WebLeadCountRatio")!)".contains("-") {
                    
                    cell.dashBoard_title_third.text = "\(self.dictDashBoardData.value(forKey: "WebLeadCountLastMonth")!) / \(ratioPercentToShow)%"
                    
                } else {
                    
                    cell.dashBoard_title_third.text = "\(self.dictDashBoardData.value(forKey: "WebLeadCountLastMonth")!) / +\(ratioPercentToShow)%"
                    
                }
                
            } else {
                
                cell.dashBoard_title_third.text = ""
                
            }
        }
        else if "\(dict.value(forKey: "Title") ?? "")" == "Opportunities" //Opportunities
        {
            cell.dashBoard_title_first.text = "Opportunities"
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "LeadCount")!)"
            
            let leadsValue = ("\(self.dictDashBoardData.value(forKey: "LeadsValue")!)" as NSString).doubleValue
            cell.dashBoard_title_third.text = convertDoubleToCurrency(amount: leadsValue)
            
        }
        else if "\(dict.value(forKey: "Title") ?? "")" == "Proposals" //Proposals
        {
            cell.dashBoard_title_first.text = "Proposals"
            
            let ProposalsValue = ("\(self.dictDashBoardData.value(forKey: "ProposalsValue")!)" as NSString).doubleValue
            
            cell.dashBoard_title_Second.text = convertDoubleToCurrency(amount: ProposalsValue)
            
            if "\(self.dictDashBoardData.value(forKey: "ProposalsValueLastMonth")!)".count > 0 {
                
                let ratioPercent = ("\(self.dictDashBoardData.value(forKey: "ProposalsValueRatio")!)" as NSString).doubleValue
                let ratioPercentToShow =  String(format: "%.02f", Double(ratioPercent))
                
                if "\(self.dictDashBoardData.value(forKey: "ProposalsValueRatio")!)".contains("-") {
                    
                    let ProposalsValueLastMonth = ("\(self.dictDashBoardData.value(forKey: "ProposalsValueLastMonth")!)" as NSString).doubleValue
                    cell.dashBoard_title_third.text = convertDoubleToCurrency(amount: ProposalsValueLastMonth) + "/ \(ratioPercentToShow)%"
                } else {
                    
                    let ProposalsValueLastMonth = ("\(self.dictDashBoardData.value(forKey: "ProposalsValueLastMonth")!)" as NSString).doubleValue
                    cell.dashBoard_title_third.text = convertDoubleToCurrency(amount: ProposalsValueLastMonth) + "/ +\(ratioPercentToShow)%"
                }
                
            } else {
                
                cell.dashBoard_title_third.text = ""
                
            }
        }
        else if "\(dict.value(forKey: "Title") ?? "")" == "Sales" //Sales
        {
            cell.dashBoard_title_first.text = "Sales"
            
            let SalesValue = ("\(self.dictDashBoardData.value(forKey: "SalesValue")!)" as NSString).doubleValue
            
            cell.dashBoard_title_Second.text = convertDoubleToCurrency(amount: SalesValue)
            
            if "\(self.dictDashBoardData.value(forKey: "SalesValueLastMonth")!)".count > 0 {
                
                let ratioPercent = ("\(self.dictDashBoardData.value(forKey: "SalesValueRatio")!)" as NSString).doubleValue
                let ratioPercentToShow =  String(format: "%.02f", Double(ratioPercent))
                
                let SalesValueLastMonth = ("\(self.dictDashBoardData.value(forKey: "SalesValueLastMonth")!)" as NSString).doubleValue
                
                if "\(self.dictDashBoardData.value(forKey: "SalesValueRatio")!)".contains("-") {
                    
                    cell.dashBoard_title_third.text = "\(convertDoubleToCurrency(amount: SalesValueLastMonth)) / \(ratioPercentToShow)%"
                    
                } else {
                    
                    cell.dashBoard_title_third.text = "\(convertDoubleToCurrency(amount: SalesValueLastMonth)) / +\(ratioPercentToShow)%"
                    
                }
                
            } else {
                
                cell.dashBoard_title_third.text = ""
                
            }
        }
        else
        {
            cell.dashBoard_title_first.text = "\(dict.value(forKey: "Title") ?? "")"
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "Task_Today") ?? "")"
            cell.dashBoard_title_third.text = "Pending \(self.dictDashBoardData.value(forKey: "Task_Pending") ?? "")"
        }
        
        /*switch indexPath.row {
        case 0:
            
            cell.dashBoard_title_first.text = strTaskHeading
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "Task_Today")!)"
            cell.dashBoard_title_third.text = "Pending \(self.dictDashBoardData.value(forKey: "Task_Pending")!)"
            
        case 1:
            cell.dashBoard_title_first.text = "Activities Completed"
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "ActivityCount")!)"
            
            if "\(self.dictDashBoardData.value(forKey: "ActivityCountLastMonth")!)".count > 0 {
                
                let ratioPercent = ("\(self.dictDashBoardData.value(forKey: "ActivityCountRatio")!)" as NSString).doubleValue
                let ratioPercentToShow =  String(format: "%.02f", Double(ratioPercent))
                
                if "\(self.dictDashBoardData.value(forKey: "ActivityCountRatio")!)".contains("-") {
                    
                    cell.dashBoard_title_third.text = "\(self.dictDashBoardData.value(forKey: "ActivityCountLastMonth")!) / \(ratioPercentToShow)%"
                    
                } else {
                    
                    cell.dashBoard_title_third.text = "\(self.dictDashBoardData.value(forKey: "ActivityCountLastMonth")!) / +\(ratioPercentToShow)%"
                    
                }
                
            } else {
                
                cell.dashBoard_title_third.text = ""
                
            }
            
        case 2:
            cell.dashBoard_title_first.text = "Lead Created"
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "WebLeadCount")!)"
            
            if "\(self.dictDashBoardData.value(forKey: "WebLeadCountLastMonth")!)".count > 0 {
                
                let ratioPercent = ("\(self.dictDashBoardData.value(forKey: "WebLeadCountRatio")!)" as NSString).doubleValue
                let ratioPercentToShow =  String(format: "%.02f", Double(ratioPercent))
                
                if "\(self.dictDashBoardData.value(forKey: "WebLeadCountRatio")!)".contains("-") {
                    
                    cell.dashBoard_title_third.text = "\(self.dictDashBoardData.value(forKey: "WebLeadCountLastMonth")!) / \(ratioPercentToShow)%"
                    
                } else {
                    
                    cell.dashBoard_title_third.text = "\(self.dictDashBoardData.value(forKey: "WebLeadCountLastMonth")!) / +\(ratioPercentToShow)%"
                    
                }
                
            } else {
                
                cell.dashBoard_title_third.text = ""
                
            }
            
        case 3:
            cell.dashBoard_title_first.text = "Opportunities"
            cell.dashBoard_title_Second.text = "\(self.dictDashBoardData.value(forKey: "LeadCount")!)"
            
            let leadsValue = ("\(self.dictDashBoardData.value(forKey: "LeadsValue")!)" as NSString).doubleValue
            cell.dashBoard_title_third.text = convertDoubleToCurrency(amount: leadsValue)
            
        //cell.dashBoard_title_third.text = "$\(self.dictDashBoardData.value(forKey: "LeadsValue")!)"
        case 4:
            cell.dashBoard_title_first.text = "Proposals"
            
            let ProposalsValue = ("\(self.dictDashBoardData.value(forKey: "ProposalsValue")!)" as NSString).doubleValue
            
            cell.dashBoard_title_Second.text = convertDoubleToCurrency(amount: ProposalsValue)
            
            if "\(self.dictDashBoardData.value(forKey: "ProposalsValueLastMonth")!)".count > 0 {
                
                let ratioPercent = ("\(self.dictDashBoardData.value(forKey: "ProposalsValueRatio")!)" as NSString).doubleValue
                let ratioPercentToShow =  String(format: "%.02f", Double(ratioPercent))
                
                if "\(self.dictDashBoardData.value(forKey: "ProposalsValueRatio")!)".contains("-") {
                    
                    let ProposalsValueLastMonth = ("\(self.dictDashBoardData.value(forKey: "ProposalsValueLastMonth")!)" as NSString).doubleValue
                    cell.dashBoard_title_third.text = convertDoubleToCurrency(amount: ProposalsValueLastMonth) + "/ \(ratioPercentToShow)%"
                } else {
                    
                    let ProposalsValueLastMonth = ("\(self.dictDashBoardData.value(forKey: "ProposalsValueLastMonth")!)" as NSString).doubleValue
                    cell.dashBoard_title_third.text = convertDoubleToCurrency(amount: ProposalsValueLastMonth) + "/ +\(ratioPercentToShow)%"
                }
                
            } else {
                
                cell.dashBoard_title_third.text = ""
                
            }
            
        case 5:
            cell.dashBoard_title_first.text = "Sales"
            
            let SalesValue = ("\(self.dictDashBoardData.value(forKey: "SalesValue")!)" as NSString).doubleValue
            
            cell.dashBoard_title_Second.text = convertDoubleToCurrency(amount: SalesValue)
            
            if "\(self.dictDashBoardData.value(forKey: "SalesValueLastMonth")!)".count > 0 {
                
                let ratioPercent = ("\(self.dictDashBoardData.value(forKey: "SalesValueRatio")!)" as NSString).doubleValue
                let ratioPercentToShow =  String(format: "%.02f", Double(ratioPercent))
                
                let SalesValueLastMonth = ("\(self.dictDashBoardData.value(forKey: "SalesValueLastMonth")!)" as NSString).doubleValue
                
                if "\(self.dictDashBoardData.value(forKey: "SalesValueRatio")!)".contains("-") {
                    
                    cell.dashBoard_title_third.text = "\(convertDoubleToCurrency(amount: SalesValueLastMonth)) / \(ratioPercentToShow)%"
                    
                } else {
                    
                    cell.dashBoard_title_third.text = "\(convertDoubleToCurrency(amount: SalesValueLastMonth)) / +\(ratioPercentToShow)%"
                    
                }
                
            } else {
                
                cell.dashBoard_title_third.text = ""
                
            }
            
        default:
            break
        }
        */
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collection_DashBoard.frame.size.width)  / 2 - 1 , height:(self.collection_DashBoard.frame.size.height)  / 3)
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if isAnimateView {
            
            cell.alpha = 0.4
            cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration: 1.0) {
                cell.alpha = 1
                cell.transform = .identity
            }
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dict = arrDashboardWidget.object(at: indexPath.row) as! NSDictionary

        if "\(dict.value(forKey: "Title") ?? "")" == "Today's Task"  //Task 0
        {
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let objWebService = WebService()
                objWebService.setDefaultCrmValue()
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
                
                objByProductVC.strFromVC = "DashBoardView"
                let userDef = UserDefaults.standard
                userDef.setValue(nil, forKey: "dictFilterTask")
                userDef.synchronize()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
        }

        else if "\(dict.value(forKey: "Title") ?? "")" == "Activities Completed"  //Task 1
        {
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let objWebService = WebService()
                objWebService.setDefaultCrmValue()
                
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
                
                
                objByProductVC.strFromVC = "DashBoardView"
                let userDef = UserDefaults.standard
                userDef.setValue(nil, forKey: "dictFilterActivity")
                userDef.synchronize()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
        }
        
        else if "\(dict.value(forKey: "Title") ?? "")" ==  "Lead Created" //Task 2
        {
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let mainStoryboard = UIStoryboard(
                    name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "WebLeadVC") as? WebLeadVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
        }
        else if "\(dict.value(forKey: "Title") ?? "")" == "Opportunities" //Task 3
        {
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
                self.navigationController?.pushViewController(controller, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
        }
        else
        {
            
        }
        
        /*switch indexPath.row {
        case 0:
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let objWebService = WebService()
                objWebService.setDefaultCrmValue()
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
                
                objByProductVC.strFromVC = "DashBoardView"
                let userDef = UserDefaults.standard
                userDef.setValue(nil, forKey: "dictFilterTask")
                userDef.synchronize()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        case 1:
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let objWebService = WebService()
                objWebService.setDefaultCrmValue()
                
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
                
                
                objByProductVC.strFromVC = "DashBoardView"
                let userDef = UserDefaults.standard
                userDef.setValue(nil, forKey: "dictFilterActivity")
                userDef.synchronize()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        case 2:
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let mainStoryboard = UIStoryboard(
                    name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "WebLeadVC") as? WebLeadVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        case 3:
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
                self.navigationController?.pushViewController(controller, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        default:
            break
        }
        */
        
    }
}

class DashboardCell: UICollectionViewCell {
    //DashBoard
    @IBOutlet weak var dashBoard_title_first: UILabel!
    @IBOutlet weak var dashBoard_title_third: UILabel!
    @IBOutlet weak var dashBoard_title_Second: UILabel!
    
}
// MARK: -
// MARK: --------API Calling
extension DashBoardNew_iPhoneVC {
    
    func recievePurchaseOrder() {
        
        
        let ac = UIAlertController(title: "Enter purchase order#", message: nil, preferredStyle: .alert)
        
        
        ac.addTextField { (textField) in
            
            textField.placeholder = "Enter purchase order#"
            textField.text = ""
            textField.delegate = self
            textField.keyboardType = .decimalPad
            textField.tag = 77
            
        }
        
        //        let ok = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
        //
        //        }
        
        let submitAction = UIAlertAction(title: "Search", style: .default)
        { [unowned ac] _ in
            
            
            let answer = ac.textFields![0]
            
            if answer.text!.count > 0 {
                
                //Call API For Search PO NO
                
                self.callAPIToSearchPO(strPoNo: "\(answer.text ?? "")")
                
            }
            self.view.endEditing(true)
            print("\(answer)")
            // do something interesting with "answer" here
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        {  _ in
            
            self.view.endEditing(true)
            // do something interesting with "answer" here
        }
        
        ac.addAction(submitAction)
        ac.addAction(cancelAction)
        //ac.addAction(ok)
        present(ac, animated: true)
        
    }
    
    func downloadAllMaster(yesDwonload : Bool) {
        
        // Check If to Download
        
        if yesDwonload == true {
            
            // Download All Masters
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            // Call Employee List Master
            
            self.present(loader, animated: false, completion: nil)
            
            //objcGetEmployeePerformanceDetailsInBackGround(FromDate: "", ToDate: "")
            DispatchQueue.global(qos: .background).async {
                self.getEmpPerformanceAsync(FromDate: "", ToDate: "")
            }
            
            //objcGetEmployeeChartDetailsBackGround()
            DispatchQueue.global(qos: .background).async {
                self.getEmpChartAsync()
            }
            DispatchQueue.global(qos: .background).async {
                self.callGetUserDefinedFieldsJsonByEntityList_API()
            }
            
            objcGetEmployeeListAPI()
            
            // Check if CRM enabled get CRM master
            if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.IsActive") as! Bool) == true {
                
                //All CRM Masters
                
                objcGetCrmMasterAPI()
                
            }
            
            // Check if Sales Auto enabled get Sales Auto master
            if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.IsActive") as! Bool) == true {
                
                // All Sales Auto Masters
                
                objcGetSalesMasterAPI()
                
                objcGetSalesDynamicMasterAPI()
                
                //SalesInspectionFormBuilder - Saavan April 2022
                
                getSalesInspectionFormBuilderMaster()
                
            }
            
            // Check if Service Auto enabled get Service Auto master
            if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.IsActive") as! Bool) == true {
                
                // All Service Auto Masters
                
                objcGetServiceMasterAPI()
                
                objcGetServiceDynamicMasterAPI()
                
                //objcGetServiceProductMasterAPI()  //Removed this as it's data is coming in other API
                
                objcGetServiceEquipmentMasterAPI()
                
                if DeviceType.IS_IPAD {
                    
                    // commenting for patch to not call mechanical services
                    //deleteMechanicalWOs()
                    
                    objcGetPlumbingMasterAPI()
                                        
                }
                
            }
            self.callTimeRangeAPI()
            
            
            self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                
                // All data downloaded
                
                self.loader.dismiss(animated: false) {
                    
                    self.checkIfFromShortCut()
                    
                    DispatchQueue.main.async {
                        
                        self.setDataOnView()
                        
                        //self.createBadgeView()
                        
                    }
                    
                }
                
            })
            
        }else{
            
            setDataOnView()
            
            //objcGetEmployeePerformanceDetailsInBackGround(FromDate: "", ToDate: "")
            DispatchQueue.global(qos: .background).async {
                self.getEmpPerformanceAsync(FromDate: "", ToDate: "")
            }
            
            //objcGetEmployeeChartDetailsBackGround()
            DispatchQueue.global(qos: .background).async {
                self.getEmpChartAsync()
            }
        }
        
    }
    
    func downloadAllMasterOld(yesDwonload : Bool) {
        
        // Check If to Download
        
        if yesDwonload == true {
            
            // Download All Masters
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            // Call Employee List Master
            
            callAPIToGetEmployeePerformanceDetails(FromDate: "", ToDate: "")
            
            callAPIToGetEmployeePerformanceDetailsChartData()
            
            /*let strURL = strServiceUrlMain + UrlGetEmployeeList + strCompanyKey
             
             Global().getEmployeeListNew(strURL)*/
            
            callApiToGetEmployeeList()
            
            // Check if CRM enabled get CRM master
            if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.IsActive") as! Bool) == true {
                
                //All CRM Masters
                
                callApiToGetCrmMaster()
                
                /*let strURL = strServiceUrlMain + UrlGetLeadDetailMaster + strCompanyKey
                 
                 Global().getLeadCountNew(strURL)*/
                
                
            }
            
            // Check if Sales Auto enabled get Sales Auto master
            if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.IsActive") as! Bool) == true {
                
                // All Sales Auto Masters
                
                self.callApiToGetSalesMaster()
                
                self.callApiToGetSalesDynamicFormMaster()
                
            }
            
            // Check if Service Auto enabled get Service Auto master
            if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.IsActive") as! Bool) == true {
                
                // All Service Auto Masters
                
                self.callApiToGetServiceMaster()
                
                self.callApiToGetServiceDynamicFormMaster()
                
                self.callApiToGetServiceProductChemicalMaster()
                
                self.callApiToGetServiceEquipmentFormMaster()
                
                /*var strURL = strServiceUrlMainServiceAutomation + UrlGetMasterServiceAutomation + strCompanyKey
                 
                 Global().getMasterServiceAutoNew(strURL)
                 
                 strURL = strServiceUrlMainServiceAutomation + UrlServiceDynamicFormMasters + strCompanyKey
                 
                 Global().getServiceDynamicFormMasterNew(strURL)
                 
                 strURL = strServiceUrlMainServiceAutomation + UrlGetMasterProductChemicalsServiceAutomation + strCompanyIdServiceAuto
                 
                 Global().getProductChemicalNew(strURL)
                 
                 strURL = strServiceUrlMainServiceAutomation + UrlGetMasterEquipmentDynamicForm + strCompanyKey
                 
                 Global().getMasterEquipmentsNew(strURL)*/
                
            }
            
            self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                
                // All data downloaded
                
                self.loader.dismiss(animated: false) {
                    
                    self.checkIfFromShortCut()
                    
                }
                
            })
            
        }else{
            
            setDataOnView()
            
            callAPIToGetEmployeePerformanceDetailsInBackGround(FromDate: "", ToDate: "")
            
            callAPIToGetEmployeePerformanceDetailsChartDataBackGround()
            
        }
        
    }
    // EquipmentServiceType ,EquipmentServiceTypeMasterDcs ,EquipmentTypeMasterDcs AreaMasterDcs ,HeatingSourceMasterDcs ,SizeMasterDcs ,InstallationCategoryDcs ,EquipmentSetupMinDcs
    
    func setDataOnView() {
        
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            let dictData = nsud.value(forKey: "DashBoardPerformance")
            self.setDataOnView(dictResponce: (dictData as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            //manageDashboardWidget()
            //createBadgeView()
            
        }
        
        if(nsud.value(forKey: "DashBoardPerformanceChart") != nil){
            
            let dictDataChart = nsud.value(forKey: "DashBoardPerformanceChart")
            self.setDataOnChartView(dictResponce: (dictDataChart as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            
        }
        
        if !isOpenMenu {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                //self.setTopMenuOption()
                self.setFooterMenuOption()
            })
            
        }
        
    }
    // MARK:- Function to dissmiss side menu
    func dissmissMenu() {
        self.view.endEditing(true)
        UIView.animate(
            withDuration: 0.2,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.view_Menu.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }) { (completed) in
            self.view_Menu.removeFromSuperview()
        }
    }
    func isUpdateAvailable() throws -> Bool {
        guard let info = Bundle.main.infoDictionary,
              let currentVersion = info["CFBundleShortVersionString"] as? String,
              let identifier = info["CFBundleIdentifier"] as? String,
              let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
            throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            print("version in app store", version,currentVersion);
            
            return version != currentVersion
        }
        throw VersionError.invalidResponse
    }
    
    
    func callTimeRangeAPI() {
        self.dispatchGroup.enter()

        let strURL = MainUrl + UrlSalesRangeofTime + Global().getCompanyKey()
        WebService.callAPIBYGETWithoutKey(parameter: NSDictionary(), url: strURL) { [self] (Response, status) in
            print(Response)
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }

            if(status)
            {
                
                
                print("Time Range API Response")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSArray {
                        
                        let dictData = Global().convertNullArray(Response as? [AnyHashable : Any])! as NSDictionary
                        
                        let arrTimeRange = dictData.value(forKey: "data") as! NSArray
                        
                        nsud.setValue(arrTimeRange, forKey: "MasterSalesTimeRange")
                        
                    }
                    
                }
                
            }
            
        
        }
        
    }
    
    
    func callAPIToSearchPO(strPoNo : String )
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        let strURL = strServiceUrlMainReceivePO + UrlGetPO + strCompanyKey + UrlGetPONo + strPoNo
        
        print("strPoNo API called --- %@",strURL)
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        
        self.present(loader, animated: false, completion: nil)
        
        WebService.GetNewDashBoardArray(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            self.loader.dismiss(animated: false) {
                
                if(status == true)
                {
                    
                    let diff1 = CFAbsoluteTimeGetCurrent() - start1
                    print("Took \(diff1) seconds to get strPoNo API Data From SQL Server.")
                    
                    let arrTemp = response.value(forKey: "data") as! NSArray
                    
                    if arrTemp .isKind(of: NSArray.self) {
                        
                        if arrTemp.count > 0 {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "MainiPad",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ReceivedPOViewController") as? ReceivedPOViewController
                            objByProductVC?.dictReceivedPO = arrTemp[0] as! [AnyHashable : Any]
                            //self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            self.navigationController?.present(objByProductVC!, animated: false, completion: nil)
                            
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                            
                        }
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
                
            }
            
        }
    }
    func callAPIToGetEmployeePerformanceDetails(FromDate : String ,ToDate : String )
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMain + UrlGetDashBoardDetailsNew + strEmployeeId + "&FromDate=\(FromDate)&ToDate=\(ToDate)"
        
        print("Performance API called --- %@",strURL)
        
        self.present(loader, animated: false, completion: nil)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
            if let first = count.first, let int = Int(first), int > 0  {
                self.dispatchGroup.leave()
            }
            if(status == true)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Performance API Data From SQL Server.")
                
                print("Performance API Response")
                
                if let dictData = response.value(forKey: "data"){
                    if((dictData as! NSDictionary).count > 0){
                        
                        var dictDataWithoutNull = (response.value(forKey: "data") as! NSDictionary)
                        
                        dictDataWithoutNull = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictDataWithoutNull as! [AnyHashable : Any]))! as NSDictionary
                        
                        nsud.setValue(dictDataWithoutNull, forKey: "DashBoardPerformance")
                        
                        self.setDataOnView(dictResponce: (dictDataWithoutNull ).mutableCopy()as! NSMutableDictionary)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                            //self.setTopMenuOption()
                            self.setFooterMenuOption()
                        })
                        
                    }
                    else{
                        
                    }
                }
                else{
                    
                }
            }
            else
            {
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
            //  }
            
        }
    }
    
    func callAPIToGetEmployeePerformanceDetailsInBackGround(FromDate : String ,ToDate : String )
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        isAnimateView = false
        
        let strURL = strServiceUrlMain + UrlGetDashBoardDetailsNew + strEmployeeId + "&FromDate=\(FromDate)&ToDate=\(ToDate)"
        
        print("Performance API Background called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            if(status == true)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Performance API in Background Data From SQL Server.")
                
                print("Performance API Background Response")
                
                if let dictData = response.value(forKey: "data"){
                    if((dictData as! NSDictionary).count > 0){
                        
                        var dictDataWithoutNull = (response.value(forKey: "data") as! NSDictionary)
                        
                        dictDataWithoutNull = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictDataWithoutNull as! [AnyHashable : Any]))! as NSDictionary
                        
                        nsud.setValue(dictDataWithoutNull, forKey: "DashBoardPerformance")
                        
                        self.setDataOnView(dictResponce: (dictDataWithoutNull ).mutableCopy()as! NSMutableDictionary)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                            //self.setTopMenuOption()
                            self.setFooterMenuOption()
                        })
                        
                    }
                }
            }
        }
    }
    
    func callAPIToGetEmployeePerformanceDetailsChartData()
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMain + UrlGetDashBoardDetailsChartData + strEmployeeId
        
        print("Performance Chart API called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
            if let first = count.first, let int = Int(first), int > 0  {
                self.dispatchGroup.leave()
            }
            if(status == true)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get perfromance chart APi Data From SQL Server.")
                
                print("Performance Chart API Response")
                
                if let dictData = response.value(forKey: "data"){
                    if((dictData as! NSDictionary).count > 0){
                        
                        var dictDataWithoutNull = (response.value(forKey: "data") as! NSDictionary)
                        
                        dictDataWithoutNull = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictDataWithoutNull as! [AnyHashable : Any]))! as NSDictionary
                        
                        nsud.setValue(dictDataWithoutNull, forKey: "DashBoardPerformanceChart")
                        
                        self.setDataOnChartView(dictResponce: (dictData as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                        
                    }
                    else{
                        
                    }
                }
                else{
                    
                }
            }
            else
            {
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
            //  }
            
        }
    }
    
    func callAPIToGetEmployeePerformanceDetailsChartDataBackGround()
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        let strURL = strServiceUrlMain + UrlGetDashBoardDetailsChartData + strEmployeeId
        
        print("Performance Chart API background called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            if(status == true)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get perfromance chart API in Background Data From SQL Server.")
                
                print("Performance Chart API background Response")
                
                if let dictData = response.value(forKey: "data"){
                    if((dictData as! NSDictionary).count > 0){
                        
                        var dictDataWithoutNull = (response.value(forKey: "data") as! NSDictionary)
                        
                        dictDataWithoutNull = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictDataWithoutNull as! [AnyHashable : Any]))! as NSDictionary
                        
                        nsud.setValue(dictDataWithoutNull, forKey: "DashBoardPerformanceChart")
                        
                        self.setDataOnChartView(dictResponce: (dictData as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                        
                    }
                    else{
                        
                    }
                }
                else{
                    
                }
            }
            else
            {
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
            //  }
            
        }
    }
    
    func getLoginData() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
        strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName")!)"
        strSalesProcessCompanyId = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.CompanyId")!)"
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
        strCompanyIdServiceAuto = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId")!)"
        strServiceUrlMainServiceAutomation = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)"
        strServiceUrlMainSalesAutomation = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")!)"
        strCustomerTabType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.CustomerTabType")!)"
        strServiceUrlMainReceivePO = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.CoreServiceModule.ServiceUrl")!)"
        
    }
    
    func alertToForceFullyUpdateApp() {
        
        // check if ForcefullyUpdate App
        
        if nsud.bool(forKey: "AppUpdateAvailable") == true &&  (dictLoginData.value(forKey: "DeviceVersionForceUpdate") as! Bool) == true {
            
            // Alert To ForceFullyUpdateApp
            
            let alert = UIAlertController(title: alertMessage, message: "A new version of peSTream is available and it's mandatory to update.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Update", style: .cancel, handler: { (nil) in
                
                if let url = URL(string: "itms-apps://itunes.apple.com/in/app/id1174328672?mt=8"),
                   UIApplication.shared.canOpenURL(url){
                    
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                    
                }
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func checkIfFromShortCut() {
        
        let shortCutType = "\(nsud.value(forKey: "YesFromShortcut")!)"
        
        if shortCutType == "AddTask" {
            
            UserDefaults.standard.set("", forKey: "YesFromShortcut")
            
            print("User click Add Task button")
            let defs = UserDefaults.standard
            defs.setValue(nil, forKey: "TaskFilterSort")
            defs.synchronize()
            
            let objWebService = WebService()
            
            let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
            objByProductVC.dictTaskDetailsData = objWebService.setDefaultTaskDataFromDashBoard()
            self.navigationController?.pushViewController(objByProductVC, animated: false)
            
        }
        if shortCutType == "AddActivity" {
            
            UserDefaults.standard.set("", forKey: "YesFromShortcut")
            
            print("User click Add Activity button")
            
            let objWebService = WebService()
            let mainStoryboard = UIStoryboard(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as? AddActivity_iPhoneVC
            objByProductVC?.dictActivityDetailsData = objWebService.setDefaultActivityDataFromDashBoard()
            self.navigationController?.pushViewController(objByProductVC!, animated: false)
            
        }
        if shortCutType == "AddLead" {
            
            UserDefaults.standard.set("", forKey: "YesFromShortcut")
            
            print("User click Add Add Lead button")
            
            let mainStoryboard = UIStoryboard(
                name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect",
                bundle: nil)
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
            self.navigationController?.pushViewController(vc, animated: false)
            
            
        }
        if shortCutType == "AddContact" {
            
            UserDefaults.standard.set("", forKey: "YesFromShortcut")
            
            print("User click Add Contact  button")
            
            let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
            self.navigationController?.pushViewController(objByProductVC, animated: false)
            
        }
        
    }
    
    func setBoolInUserDefaults() {
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsCommercialService") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isEquipmentEnabled")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isEquipmentEnabled")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsAfterImage_ServiceAuto") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isCompulsoryAfterImageService")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isCompulsoryAfterImageService")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsBeforeImage_ServiceAuto") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isCompulsoryBeforeImageService")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isCompulsoryBeforeImageService")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.NoChemical_ServiceAuto") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isNoChemical")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isNoChemical")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceJobDescription_ServiceAuto") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isNoServiceJobDescription")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isNoServiceJobDescription")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsEmployeePresetSignature_ServiceAuto") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isPreSetSignService")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isPreSetSignService")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsEmployeePresetSignature_SalesAuto") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isPreSetSignSales")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isPreSetSignSales")
            
        }
        
        UserDefaults.standard.set("\(dictLoginData.value(forKey: "EmployeeSignature")!)", forKey: "ServiceTechSignPath")
        
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.AllowChangeLeadAssignee") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isFieldPersonAssign")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isFieldPersonAssign")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.AllowDirectLeadSchedule") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "isScheduleNow")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isScheduleNow")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.TaxcodeRequired") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "TaxCodeReq")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "TaxCodeReq")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsWebLeadSourceRequired") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "IsSourceMandatory")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "IsSourceMandatory")
            
        }
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.CityStateLookupEnabled") as! Bool) == true {
            
            UserDefaults.standard.set(true, forKey: "IsCityStateLookupEnabled")
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "IsCityStateLookupEnabled")
            
        }
        
    }
    
    func setAllDefaultsNull() {
        
        nsud.setValue(strCustomerTabType, forKey: "CustomerTabType")
        nsud.setValue(false, forKey: "forUpdate")
        nsud.setValue(false, forKey: "fromAddAccountExisting")
        nsud.setValue(false, forKey: "fromAddAccount")
        nsud.setValue(false, forKey: "fromHistory")
        nsud.setValue(false, forKey: "isRecorder")
        nsud.setValue(false, forKey: "fromSelectService")
        
        nsud.setValue(false, forKey: "yesAudio")
        nsud.setValue(false, forKey: "backFromUpdateback")
        nsud.setValue(false, forKey: "fromHistoryTOutBox")
        nsud.setValue(false, forKey: "isFromSurveyToSendEmail")
        nsud.setValue(false, forKey: "yesDeletedImageInPreview")
        nsud.setValue(nil, forKey: "DeletedImages")
        
        nsud.setValue(false, forKey: "forCheckFrontImageDelete")
        nsud.setValue(false, forKey: "forCheckBackImageDelete")
        nsud.setValue(false, forKey: "isFromBackServiceDynamci")
        nsud.setValue(false, forKey: "YesSurveyService")
        nsud.setValue(nil, forKey: "DeleteExtraWorkOrderFromDBSales")
        nsud.setValue(nil, forKey: "DeleteExtraWorkOrderFromDBService")
        
        nsud.setValue(false, forKey: "yesEditImage")
        nsud.setValue(false, forKey: "yesEditedImageDescription")
        nsud.setValue(false, forKey: "yesEditedImageCaption")
        nsud.setValue(false, forKey: "firstGraphImage")
        nsud.setValue(false, forKey: "FromDashBoardToTaskUpdate")
        nsud.setValue(false, forKey: "FromDashBoardToAddActivty")
        
        nsud.setValue(false, forKey: "FromContactListViewActivity")
        nsud.setValue(false, forKey: "FromContactListView")
        nsud.setValue(false, forKey: "servicegraph")
        nsud.setValue(false, forKey: "termitegraph")
        nsud.setValue(false, forKey: "graphImagePathTermite")
        nsud.setValue(false, forKey: "yesDeleteGraphTermite")
        
        nsud.setValue(false, forKey: "isEditedCompanyReloadData")
        nsud.setValue(false, forKey: "isEditedCompany")
        nsud.setValue(false, forKey: "dismissView")
        nsud.setValue(false, forKey: "CRMNewFlow")
        nsud.setValue(false, forKey: "fromCRMNewFlow")
        nsud.setValue(false, forKey: "fromWebLeadAssociation")
        
        nsud.setValue(false, forKey: "fromOpportunityAssociation")
        nsud.setValue(nil, forKey: "opportunityFilterData")
        nsud.setValue(nil, forKey: "webLeadFilterData")
        nsud.setValue("", forKey: "cureentTab")
        nsud.setValue("", forKey: "cureentSwipeCount")
        nsud.setValue("", forKey: "cureentTabOpportunity")
        
        nsud.setValue("", forKey: "cureentSwipeCountOpportunity")
        nsud.setValue(false, forKey: "fromCompanyVC")
        nsud.setValue(false, forKey: "ChangeInParameter")
        nsud.synchronize()
        
    }
    
    func callApiToGetEmployeeList(){
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMain + UrlGetEmployeeList + strCompanyKey
        
        print("Employee API called -- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            
            if(Status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Employee Data From SQL Server.")
                
                print("Employee API Response")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSArray {
                        
                        let dictData = Global().convertNullArray(Response as? [AnyHashable : Any])! as NSDictionary
                        
                        let arrEmplistLocal = dictData.value(forKey: "data") as! NSArray
                        
                        nsud.setValue(arrEmplistLocal, forKey: "EmployeeList")
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetCrmMaster(){
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMain + UrlGetLeadDetailMaster + strCompanyKey
        
        print("CRM Master API called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            if(Status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get CRM Master Data From SQL Server.")
                
                print("CRM Master API Response")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSDictionary {
                        
                        var dictData = (Response.value(forKey: "data") as! NSDictionary)
                        
                        dictData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictData as! [AnyHashable : Any]))! as NSDictionary
                        
                        if dictData.value(forKey: "LeadStatusMasters") is NSArray {
                            
                            let arrOfStatusMaster = dictData.value(forKey: "LeadStatusMasters") as! NSArray
                            
                            Global().setCrmMastersInUserDefaults(arrOfStatusMaster as? [Any], dictData as? [AnyHashable : Any])
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetSalesMaster(){
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMainSalesAutomation + UrlGetMasterSalesAutomation + strCompanyKey
        
        print("Sales Master 1 API called --- %@",strURL)
        
        WebService.getRequestUsingNsUrlSession(dictJson: NSDictionary(), url: strURL) { (Response, Status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            if(Status)
            {
                
                print("Sales Master 1 API Response")
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Sales Master Data From SQL Server.")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSDictionary {
                        
                        var dictData = (Response.value(forKey: "data") as! NSDictionary)
                        
                        dictData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictData as! [AnyHashable : Any]))! as NSDictionary
                        
                        nsud.setValue(dictData, forKey: "MasterSalesAutomation")
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    func callApiToGetSalesDynamicFormMaster(){
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMainSalesAutomation + UrlSalesDynamicFormMasters + strCompanyKey
        
        print("Sales Master 2 Dynamic form API called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            if(Status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Sales Dynamic Master Data From SQL Server.")
                
                print("Sales Master 2 Dynamic form API Response")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSArray {
                        
                        let ResponseDict = Global().convertNullArray(Response as? [AnyHashable : Any])! as NSDictionary
                        
                        nsud.setValue(ResponseDict.value(forKey: "data"), forKey: "MasterSalesAutomationDynamicForm")
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetServiceMaster(){
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMainServiceAutomation + UrlGetMasterServiceAutomation + strCompanyKey
        
        print("Service Master 1 form API called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            
            if(Status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Master 1 Data From SQL Server.")
                
                print("Service Master 1 form API Response")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSDictionary {
                        
                        var dictData = (Response.value(forKey: "data") as! NSDictionary)
                        
                        dictData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictData as! [AnyHashable : Any]))! as NSDictionary
                        
                        nsud.setValue(dictData, forKey: "MasterServiceAutomation")
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetServiceDynamicFormMaster(){
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMainServiceAutomation + UrlServiceDynamicFormMasters + strCompanyKey
        
        print("Service Master 2 Dynamic form API called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            
            if(Status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service dynamci Master 2 Data From SQL Server.")
                
                print("Service Master 2 Dynamic form API Response")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSArray {
                        
                        let ResponseDict = Global().convertNullArray(Response as? [AnyHashable : Any])! as NSDictionary
                        
                        nsud.setValue(ResponseDict.value(forKey: "data"), forKey: "MasterServiceAutomationDynamicForm")
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    func callApiToGetServiceProductChemicalMaster(){
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMainServiceAutomation + UrlGetMasterProductChemicalsServiceAutomation + strCompanyIdServiceAuto
        
        print("Service Master 3 Product Chemical form API called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            
            if(Status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service product Master 3 Data From SQL Server.")
                
                print("Service Master 3 Product Chemical form API Response")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSDictionary {
                        
                        var dictData = (Response.value(forKey: "data") as! NSDictionary)
                        
                        dictData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictData as! [AnyHashable : Any]))! as NSDictionary
                        
                        nsud.setValue(dictData, forKey: "ProductChemicalsServiceAutomation")
                        nsud.setValue(dictData, forKey: "getMasterProductChemicalsServiceAutomation")
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetServiceEquipmentFormMaster(){
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        dispatchGroup.enter()
        
        let strURL = strServiceUrlMainServiceAutomation + UrlGetMasterEquipmentDynamicForm + strCompanyKey
        
        print("Service Master 4 Equipment form API called --- %@",strURL)
        
        WebService.GetNewDashBoard(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            
            if(Status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Equipment Master 4 Data From SQL Server.")
                
                print("Service Master 4 Equipment form API Response")
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if Response.value(forKey: "data") is NSArray {
                        
                        let ResponseDict = Global().convertNullArray(Response as? [AnyHashable : Any])! as NSDictionary
                        
                        nsud.setValue(ResponseDict.value(forKey: "data"), forKey: "MasterEquipmentsDynamicForm")
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    @objc func actionOnButtonUpdate(sender : UIButton){
        
        // Goto App Store To Update App
        
        if let url = URL(string: "itms-apps://itunes.apple.com/in/app/id1174328672?mt=8"),
           UIApplication.shared.canOpenURL(url){
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        }
        
    }
    
    // MARK: - ----------------Redirection Funtions to other views
    // MARK: -
    
    func goToSignedAgreements() {
        if(DeviceType.IS_IPAD){
            
            let mainStoryboard =  UIStoryboard.init(name: "CRMiPad", bundle: nil)
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as! SignedAgreementiPadVC
            self.navigationController?.present(controller, animated: false, completion: nil)
            
        }else{
            let controller = storyboardCRMiPhone.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as! SignedAgreementiPhoneVC
            self.navigationController?.present(controller, animated: false, completion: nil)
        }
        
        
    }
    
    func goToLoginVC() {
        
        if(DeviceType.IS_IPAD){
            
            let mainStoryboard =  UIStoryboard.init(name: "MainiPad", bundle: nil)
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewControlleriPad") as! LoginViewControlleriPad
            self.navigationController?.pushViewController(controller, animated: false)
        }else{
            let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(controller, animated: false)
        }

    }
    
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
            lbltaskCount.isHidden = true
            lblScheduleCount.isHidden = true
        }
        
        badgeSchedule.removeFromSuperview()
        badgeTasks.removeFromSuperview()
        
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }
                    
                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lblScheduleCount.text = strScheduleCount
                            lblScheduleCount.isHidden = false
                        }else{
                            badgeSchedule = SPBadge()
                            badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                            badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeSchedule.badge = strScheduleCount
                            btnScheduleFooter.addSubview(badgeSchedule)
                        }
                        
                        
                    }
                    
                    
                    if strTaskCount.count > 0 {
                        
                        if(DeviceType.IS_IPAD){
                            lbltaskCount.text = strTaskCount
                            lbltaskCount.isHidden = false
                            
                        }else{
                            badgeTasks = SPBadge()
                            badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                            badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeTasks.badge = strTaskCount
                            btnTasksFooter.addSubview(badgeTasks)
                        }
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    // MARK: --------------------------- Call Master's Functions in Swift ---------------------------
    
    func getSalesAutoMasterSwift()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = self.strServiceUrlMainSalesAutomation + UrlGetMasterSalesAutomation + self.strCompanyKey

        print("Sales Auto Master Swift API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "SalesAutoMaster_DashBoardVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroup.leave()
                }
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Sales Auto Master Swift API API Data From SQL Server.")
                
                print("Sales Auto Master Swift API Response")
                
                self.objcGetSalesMasterAPI()
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSDictionary {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let dictMasterLocal = dictData.value(forKey: "data") as! NSDictionary
                        
                        // MasterSalesAutomation
                        
                    }
                    
                }
            }
            
        }
        
    }
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DashBoardNew_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arytv_NavigationDrawer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let cell = tv_NavigationDrawer.dequeueReusableCell(withIdentifier: "NavigationDrawerCell1", for: indexPath as IndexPath) as! NavigationDrawerCell
            
            //  let dict = arytv_NavigationDrawer.object(at: indexPath.row)as! NSDictionary
            cell.lblTitleName.text =  "\(strEmpName)\nEmp #: \(strEmpNo)"
            cell.lblTitleSubName.text =  "Version Date: \(VersionDate)\nVersion: \(VersionNumber)"
            
            //Navin
            
            if((nsud.value(forKey: "EmployeePhoto") != nil))
            {
                let strUrl = "\(nsud.value(forKey: "EmployeePhoto") ?? "")"
                let strNewString = strUrl.replacingOccurrences(of: "\\", with: "/")
                
                cell.imgProfile.setImageWith(URL(string: strNewString), placeholderImage: UIImage(named: "profile.png"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
            }
            else
            {
                cell.imgProfile.image = UIImage(named: "profile.png")
            }
            
            //End
            
            if nsud.bool(forKey: "AppUpdateAvailable") {
                
                // Update Available show button
                
                cell.btnUpdateApp.isHidden = false
                cell.btnUpdateApp.addTarget(self, action: #selector(self.actionOnButtonUpdate(sender:)), for: .touchUpInside)
                
            }else{
                
                cell.btnUpdateApp.isHidden = true
                
            }
            
            cell.imgProfile.layer.borderWidth = 1.0
            cell.imgProfile.layer.masksToBounds = false
            cell.imgProfile.layer.borderColor = UIColor.white.cgColor
            cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.size.width / 2
            cell.imgProfile.clipsToBounds = true
            
            return cell
        }else{
            
            let cell = tv_NavigationDrawer.dequeueReusableCell(withIdentifier: "NavigationDrawerCell2", for: indexPath as IndexPath) as! NavigationDrawerCell
            let dict = arytv_NavigationDrawer.object(at: indexPath.row )as! NSDictionary
            
            cell.lblTitle.text = dict["title"]as? String
            cell.img.image = UIImage(named: "\(dict["image"]!)")
            
            if (dict["title"]as? String == "Appointment"){
                cell.lblTitle.text = "Schedule"; // Appointment
            }
            if (dict["title"]as? String == "Manage Opportunity/Lead"){
                cell.lblTitle.text = "Leads"; // Leads
            }
            if (dict["title"]as? String == "Contacts List"){
                cell.lblTitle.text = "Contacts";
            }
            if (dict["title"]as? String == "Opportunity/Lead History"){
                cell.lblTitle.text = "Leads History"; //Leads History
            }
            if (dict["title"]as? String == "Nearby"){
                cell.lblTitle.text = "Map"; //Leads History
            }
            return cell
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //
        let dict = arytv_NavigationDrawer.object(at: indexPath.row )as! NSDictionary
        let titleLabel = dict["title"]as? String
        
        if titleLabel == "Dashboard" {
            
            self.view_Menu.removeFromSuperview()
            
        }else if titleLabel == "Appointment" {
            if(DeviceType.IS_IPAD){
                
                let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                
                if strAppointmentFlow == "New" {
                    
                    let mainStoryboard = UIStoryboard(
                        name: "Appointment_iPAD",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                } else {
                    
                    let mainStoryboard = UIStoryboard(
                        name: "MainiPad",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                }
            }else{
                
                let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                
                if strAppointmentFlow == "New" {
                    
                    let mainStoryboard = UIStoryboard(
                        name: "Appointment",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                } else {
                    
                    let mainStoryboard = UIStoryboard(
                        name: "Main",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                }
            }
            
            
        }else if titleLabel == "Manage Opportunity/Lead" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let mainStoryboard = UIStoryboard(
                    name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "WebLeadVC") as? WebLeadVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        }else if titleLabel == "Signed Agreement" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                self.goToSignedAgreements()
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        }else if titleLabel == "Task" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let objWebService = WebService()
                objWebService.setDefaultCrmValue()
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
                
                objByProductVC.strFromVC = "DashBoardView"
                let userDef = UserDefaults.standard
                userDef.setValue(nil, forKey: "dictFilterTask")
                userDef.synchronize()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        }else if titleLabel == "Activity" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let objWebService = WebService()
                objWebService.setDefaultCrmValue()
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
                
                objByProductVC.strFromVC = "DashBoardView"
                let userDef = UserDefaults.standard
                userDef.setValue(nil, forKey: "dictFilterActivity")
                userDef.synchronize()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        }else if titleLabel == "Contacts List" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                self.view.endEditing(true)
                let objWebService = WebService()
                objWebService.setDefaultCrmValue()
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
                
                self.navigationController?.pushViewController(vc, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        }else if titleLabel == "Settings" {
            
            if(DeviceType.IS_IPAD){
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SettingsViewiPad") as? SettingsViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
            }else{
                let mainStoryboard = UIStoryboard(
                    name: "Main",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SettingsView") as? SettingsView
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
            }
            
            
            
            
        }else if titleLabel == "Opportunity/Lead History" {
            
            if(DeviceType.IS_IPAD){
                let global = Global()
                let isnet = global.isNetReachable()
                if isnet {
                    let mainStoryboard = UIStoryboard(
                        name: "MainiPad",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HistoryViewiPad") as? HistoryViewiPad
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                } else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
                
            }else{
                let global = Global()
                let isnet = global.isNetReachable()
                if isnet {
                    
                    let mainStoryboard = UIStoryboard(
                        name: "Main",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HistoryView") as? HistoryView
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                } else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
            }
            
            
        }else if titleLabel == "Sync Masters" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                
                let alert = UIAlertController(title: alertMessage, message: "Are you sure you want to sync masters ?", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction (title: "Sync Masters", style: .default, handler: { (nil) in
                    
                    self.view_Menu.removeFromSuperview()
                    
                    self.downloadAllMaster(yesDwonload: true)
                    
                    
                }))
                alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
                    
                    
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
            }else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        }else if titleLabel == "Day Clock In/Out" {
            let global = Global()
            let isnet = global.isNetReachable()
            
            if(DeviceType.IS_IPAD){
                if isnet {
                    let mainStoryboard = UIStoryboard(
                        name: "MainiPad",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ClockInOutViewControlleriPad") as? ClockInOutViewControlleriPad
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                }else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
            }else{
                
                if isnet {
                    let mainStoryboard = UIStoryboard(
                        name: "Main",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ClockInOutViewController") as? ClockInOutViewController
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                }else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
            }
            
            
        }else if titleLabel == "Statistics" {
            let global = Global()
            let isnet = global.isNetReachable()
            if(DeviceType.IS_IPAD){
                if isnet {
                    let mainStoryboard = UIStoryboard(
                        name: "MainiPad",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "StatisticsViewController") as? StatisticsViewController
                    self.navigationController?.present(objByProductVC!, animated: true)
                } else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
            }else{
                if isnet {
                    let mainStoryboard = UIStoryboard(
                        name: "Main",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "StatisticsViewControlleriPhone") as? StatisticsViewController
                    self.navigationController?.present(objByProductVC!, animated: true)
                    
                } else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
            }
            
            
        }
        else if titleLabel == "Weekly Time Sheet" {
            let global = Global()
            let isnet = global.isNetReachable()
            if(DeviceType.IS_IPAD){
                if isnet {
                    let mainStoryboard = UIStoryboard(
                        name: "MainiPad",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "WeeklyTimeSheetViewControlleriPad") as? WeeklyTimeSheetViewControlleriPad
                    self.navigationController?.present(objByProductVC!, animated: true)
                } else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
            }else{
                
                global.alertMethod(Alert, InfoComingSoon)
                
            }
        }else if titleLabel == "Pending Approval" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            
            if(DeviceType.IS_IPAD){
                
                if isnet {
                    
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_PendingApprovalListVC") as? WDO_PendingApprovalListVC
                          
                    self.navigationController?.pushViewController(vc!, animated: false)
                    
                } else {
                    
                    global.alertMethod(Alert, ErrorInternetMsg)
                    
                }
                
            }else{
                
                global.alertMethod(Alert, InfoComingSoon)
                
            }
        }else if titleLabel == "Nearby" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            /*
             if(DeviceType.IS_IPAD){
             if isnet {
             let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
             self.navigationController?.pushViewController(vc!, animated: false)
             } else {
             global.alertMethod(Alert, ErrorInternetMsg)
             }
             
             }else{
             if isnet {
             let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
             self.navigationController?.pushViewController(vc!, animated: false)
             } else {
             global.alertMethod(Alert, ErrorInternetMsg)
             }
             }*/
            if isnet {
                let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
                self.navigationController?.pushViewController(vc!, animated: false)
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
            
        }else if titleLabel == "View Schedule" {
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                dissmissMenu()
                let storyboard = UIStoryboard(name: "ScheduleD2D", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ScheduleViewController") as? ScheduleViewController
                self.navigationController?.pushViewController(vc!, animated: false)
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
            
        } else if titleLabel == "Assign an Area" {
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                dissmissMenu()
                let storyboard =  UIStoryboard.init(name: "GeoFencingD2D", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "GeoFencingViewController") as? GeoFencingViewController else { return }
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
            
        } else if titleLabel == "Live Tracking" {
            let global = Global()
            let isnet = global.isNetReachable()
            if isnet {
                dissmissMenu()
                let storyboard =  UIStoryboard.init(name: "LiveTracking", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "LiveTrackingViewController") as? LiveTrackingViewController else { return }
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
            
        }
        else if titleLabel == "Help" {
            
            let global = Global()
                      
            let isnet = global.isNetReachable()
                      
            if isnet {
                          
                dissmissMenu()
                          
                let storyboard =  UIStoryboard.init(name: "Menu", bundle: nil)
                          
                guard let controller = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController else { return }
                          
                self.navigationController?.pushViewController(controller, animated: true)
                      
            } else {
                          
                global.alertMethod(Alert, ErrorInternetMsg)
                      
            }
            
        }
        else if titleLabel == "Support" {
            
            let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_DashboardVC") as? HelpTicket_DashboardVC
            self.navigationController?.pushViewController(objByProductVC!, animated: true)
            
        }
        else if titleLabel == "Change Password" {
            
            let global = Global()
            let isnet = global.isNetReachable()
            
            if isnet {
                
                let mainStoryboard = UIStoryboard(
                    name: "CRM",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordiPadNew") as? ChangePasswordiPadNew
                //self.navigationController?.present(objByProductVC!, animated: true)
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                global.alertMethod(Alert, ErrorInternetMsg)
            }
            
        }
        else if titleLabel == "Generate WorkOrder"{
            
            if Global().isGenerateWoMechanical() {
                
                if isInternetAvailable() {
                    let storyboard = UIStoryboard(name: "CreateWorkOrder_iPad", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "CreateWorkOrder_iPadViewController") as? CreateWorkOrder_iPadViewController
                    self.navigationController?.pushViewController(vc!, animated: false)
                } else {
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
                }
                
            } else {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "You can not create work order", viewcontrol: self)
            }
            
        }
        else if titleLabel == "Purchase Order History"{
            
            if isInternetAvailable() {
                let mainStoryboard = UIStoryboard(
                    name: "MechanicaliPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "PoHistoryViewController") as? PoHistoryViewController
                objByProductVC?.strWorkOrderNo = ""
                self.navigationController?.present(objByProductVC!, animated: true)
            } else {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            }
            
        } else if titleLabel == "Quote History"{
            
            if isInternetAvailable() {
                let mainStoryboard = UIStoryboard(
                    name: "MechanicaliPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "QuoteHistoryViewController") as? QuoteHistoryViewController
                objByProductVC?.strWorkOrderID = ""
                self.navigationController?.present(objByProductVC!, animated: true)
            } else {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            }
            
        }
        else if titleLabel == "Receive Purchase Order"{
            
            if isInternetAvailable() {
                
                recievePurchaseOrder()
                
            } else {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            }
            
        }
        else if titleLabel == "Switch Branch"{
            
            if isInternetAvailable() {
                
                    goToSwitchBranch()
                
            } else {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            }
            
        }
        else if titleLabel == "Logout" {
            
            let alert = UIAlertController(title: alertMessage, message: "Are you sure you want to Logout ?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Logout", style: .destructive, handler: { (nil) in
                
                // Clear all userdeafults
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                
                UserDefaults.standard.set(true, forKey: "AllAppointments")
                UserDefaults.standard.set(true, forKey: "SortByScheduleDate")
                UserDefaults.standard.set(true, forKey: "SortByScheduleDateAscending")
                UserDefaults.standard.set(true, forKey: "SortByModifiedDateAscending")
                UserDefaults.standard.set(false, forKey: "graphDebug")
                
                UserDefaults.standard.set(NSArray(), forKey: "AppointmentStatus")
                UserDefaults.standard.set(true, forKey: "fromAppDelegate")
                
                UserDefaults.standard.set(false, forKey: "firstInstall_4Dec2018")
                UserDefaults.standard.set(false, forKey: "firstInstall_13June2019")
                UserDefaults.standard.set("No", forKey: "YesFromShortcut")
                UserDefaults.standard.set(true, forKey: "isEnableAutoPropertyInformation")

                // go to login view again
                
                self.goToLoginVC()
                
                
            }))
            alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
                
                
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    
}


// MARK: -
// MARK: ----------NavigationDrawerCell---------
class NavigationDrawerCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblTitleSubName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnUpdateApp: UIButton!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: ----------Filter Delegate---------
extension DashBoardNew_iPhoneVC : FilterDashboardNewProtocol{
    
    func getDataFromFilterDashboardNew(dict: NSDictionary, tag: Int) {
        
        strTaskHeading = "Today's Task"
        
        if "\(dict.value(forKey: "fromDate")!)".count > 0 && "\(dict.value(forKey: "toDate")!)".count > 0{
            
            let fromDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dict.value(forKey: "fromDate")!)", "MM/dd/yyyy", "")! as NSDate//dateFormatter.date(from: "\(dict.value(forKey: "fromDate")!)")
            let toDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dict.value(forKey: "toDate")!)", "MM/dd/yyyy", "")! as NSDate //dateFormatter.date(from: "\(dict.value(forKey: "toDate")!)")
            let currentDate = Global().convertStringDateToDate(forCrmNewFlow: "\(Global().strCurrentDateFormatted("MM/dd/yyyy", "") ?? "")", "MM/dd/yyyy", "")! as NSDate //dateFormatter.date(from: "\(Global().strCurrentDateFormatted("MM/dd/yyyy", "UTC") ?? "")")
            
            if( (fromDate == currentDate) && (currentDate == toDate) ){
                
                strTaskHeading = "Today's Task"
                
            }else{
                
                strTaskHeading = "Task"
                
            }
            
        }
        
        if isInternetAvailable() {
            
            self.getEmployeePerformanceOnFilter(FromDate: "\(dict.value(forKey: "fromDate")!)", ToDate: "\(dict.value(forKey: "toDate")!)")
            
            self.dispatchGroupPerformance.notify(queue: DispatchQueue.main, execute: {
                
                self.loaderPerformance.dismiss(animated: false) {
                    
                    self.setDataOnView()
                    
                }
                
            })
            
        }
        
        
    }
    
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
extension DashBoardNew_iPhoneVC:UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        /*let count = (textField.text!.count) as Int
         
         var intMaxLenght = 0
         
         if string.contains(".") {
         
         intMaxLenght = 50
         
         }else if textField.text!.contains(".") {
         
         intMaxLenght = 50
         
         } else {
         
         intMaxLenght = 50
         
         }
         
         return Global().isNumberOnly(withDecimal: string, range, Int32(intMaxLenght), Int32(count),".0123456789", textField.text!)*/
        
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
    }
}

