//
//  SalesFormBuilderPresenter.swift
//  DPS
//
//  Created by Saavan Patidar on 26/04/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import Foundation
protocol SalesFormBuilderDelegate: CommonView {
    func sucessfullyGetHTML(_ output: String)
    func sucessfullyGetPDF(_ output: String)
    func sucessfullyGetReloadResponse(_ output: [String: Any])
    func failResponse(_ output: String)
}
extension SalesFormBuilderDelegate {
    func sucessfullyGetReloadResponse(_ output: [String: Any]){}
    func failResponse(_ output: String){}
}

class SalesFormBuilderPresenter: NSObject {
    weak private var delegate: SalesFormBuilderDelegate?
    private lazy var service = {
        
        return SalesFormBuilderService()
    }()
    
    init(_ delegate: SalesFormBuilderDelegate) {
        self.delegate = delegate
    }
    
    
    /**
    * Method name: apiCallForFormBuilder
    * Description:This method is for FormBuilder View controller and it calls the private method to call a FormBuilder API.
    * Parameters:
    */
    public func apiCallForFormBuilder(parameters: [String: Any]) {
        apiCallForFormBuilderHttp(parameters: parameters);
    }
    
    //MARK: Private Functions
    private func apiCallForFormBuilderHttp(parameters: [String: Any]) {
        delegate?.showLoader()
        SalesFormBuilderService.sharedInstance.requestSetupFormBuilder(parameters: parameters) { (result, error) in
            if(error != nil){

            }else if (result != nil){
                print(result as Any)
                self.delegate?.sucessfullyGetHTML(result ?? "")
            }
        }
    }
    
    
    /**
    * Method name: requestToGetPdf
    * Description:
    * Parameters:
    */
    public func requestToGetPdf(strbase64: String, contractorId: String, accountNo: String, isRelativePath: Bool) {
        requestToGetPdfHttp(strbase64: strbase64, contractorId: contractorId, accountNo: accountNo, isRelativePath: isRelativePath);
    }
    
    //MARK: Private Functions
    private func requestToGetPdfHttp(strbase64: String, contractorId: String, accountNo: String, isRelativePath: Bool) {
        
        SalesFormBuilderService.sharedInstance.requestToGetPdf(strbase64: strbase64, contractorId: contractorId, accountNo: accountNo, isRelativePath: isRelativePath) { (result, error) in
            
            if(error != nil){
            }else if (result != nil){

                print(result as Any)
                self.delegate?.sucessfullyGetPDF(result ?? "")
            }
        }
    }
    
    /**
    * Method name: requestToGetPdf
    * Description:
    * Parameters:
    */
    public func requestToReloadForm(strUrlToReload: String) {
        requestToReloadFormHttp(strUrlToReload: strUrlToReload);
    }
    
    //MARK: Private Functions
    private func requestToReloadFormHttp(strUrlToReload: String) {
        
        SalesFormBuilderService.sharedInstance.requestToGetResponse(strUrlToReload: strUrlToReload) { (result, error) in
            
            if(error != nil){
                print("Error")
                self.delegate?.failResponse("fail")
            }else if (result != nil){
                self.delegate?.sucessfullyGetReloadResponse(result ?? [:])
            }
        }
    }
}
