//
//  SalesFormBuilderService.swift
//  DPS
//
//  Created by Saavan Patidar on 26/04/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import Foundation
import UIKit

class SalesFormBuilderService {
    
    //Shared Instance
    static let sharedInstance = SalesFormBuilderService()
    /**
    * Method name: requestSetupFormBuilder
    * Description:This methods communicates to the AWS Mobile Client User pool to Register the User.
    * Parameters: User
    * Return: SignIUpResult, Error
    */
    func requestSetupFormBuilder(parameters: [String: Any], andCompletion completionBlock: @escaping (_ result: String?, Error?) -> Void){
        let endPoint = ""
        let method = ""

        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: parameters,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                       encoding: .ascii)
            let data = ["templateData"    : theJSONText!]
            print(data)
            
            
            ServiceManager.sharedInstance.postRequestWithUrlSessionForHtmlString(endPoint, withParams: data, withMethod: method) { (response) in
                
                switch(response){
                case .failure(let error):
                    completionBlock(nil, error)
                    break
                case .success(let response):
                    completionBlock(response, nil)
                    print(response)
                    break;
                }
            }
        }
    }
    
    
    /**
    * Method name: requestToGetPdf
    * Description:.
    * Parameters: User
    * Return: SignIUpResult, Error
    */
    func requestToGetPdf(strbase64: String, contractorId: String, accountNo: String, isRelativePath: Bool, andCompletion completionBlock: @escaping (_ result: String?, Error?) -> Void){
        let endPoint = "MobileToSaleAuto"
        let method = "getFileFromBase64_mobile"
        //MobileToSaleAuto/getFileFromBase64_mobile
        
        let data = ["strbase64"    : strbase64,
                    "contractorId"    : contractorId,
                    "accountNo"    : accountNo,
                    "IsRelativePath"    : isRelativePath] as [String : Any]
        print(data)
        
        
        ServiceManager.sharedInstance.postRequestWithUrlSessionForHtmlString(endPoint, withParams: data, withMethod: method) { (response) in
            
            switch(response){
            case .failure(let error):
                completionBlock(nil, error)
                break
            case .success(let response):
                completionBlock(response, nil)
                print(response)
                break;
            }
        }
        
    }
    
    /**
    * Method name: requestToGetResponse
    * Description:.
    * Parameters: User
    * Return: SignIUpResult, Error
    */
    func requestToGetResponse(strUrlToReload: String, andCompletion completionBlock: @escaping (_ result: [String: Any]?, Error?) -> Void){
        print(strUrlToReload)
        
        ServiceManager.sharedInstance.getRequestWithDirectUrlSessionReloadForm(strUrlToReload) { (response) in
            
            switch(response){
            case .failure(let error):
                completionBlock(nil, error)
                break
            case .success(let response):
                completionBlock(response, nil)
                print(response)
                break;
            }
        }
    }
}

