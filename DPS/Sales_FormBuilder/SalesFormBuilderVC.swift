//
//  SalesFormBuilderVC.swift
//  DPS
//
//  Created by Saavan Patidar on 26/04/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import UIKit
import WebKit

protocol SaveFormBuilderProtocol : class {
    func getDataOnSaveFormBuilder(data : Any)
}

class SalesFormBuilderVC: UIViewController, WKNavigationDelegate {
    // MARK:-----IBOutlet
    
    @IBOutlet weak var view_Header: UIView!
    @IBOutlet weak var loadSpinner: UIActivityIndicatorView!

    // MARK:
    // MARK:-----Global Variable
    
    weak var handelDataOnSavingFormBuilder: SaveFormBuilderProtocol?
    var loader = UIAlertController()
    
    var webView: WKWebView!

    private lazy var presenter = {
        return SalesFormBuilderPresenter(self)
    }()
    
    var matchesGeneralInfo : NSManagedObject!
    var dictTemplateObj: [String: Any]?
    //var objPaymentInfo : NSManagedObject!
    //var dictPricingInfo: [String: Any] = [:]
    //var arrSoldService: [NSManagedObject] = []
    var arrImageDetails: [[String: Any]] = []
    var arrGraphDetails: [[String: Any]] = []
    
    let inset: CGFloat                          = 5
    let minimumLineSpacing: CGFloat             = 0
    let minimumInteritemSpacing: CGFloat        = 0
    //var loadCount: Int                          = 0
    var role                                    = 1

    var strOverViewPDF                          = ""
    var strLeadStatusGlobal                     = ""
    var strStageSysName                         = ""
    var strTemplateKey                          = ""
    let global                                  = Global()
    var strCompanyKey                           = ""
    var strUserName                             = ""
    var strLeadId                               = ""
    var strBaseUrl                              = ""
    var strAccountNo                            = ""
    var strSavedPdfUrl                          = ""

    // MARK:
    // MARK:-----View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        self.loadSpinner.isHidden = true

        self.strLeadId = "\(nsud.value(forKey: "LeadId") ?? "")"
        self.setLeadDetails()
        self.loginDetails()
        
        if getOpportunityStatus() == false {
            
            //self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            //self.present(self.loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.localStorageWebView()//4.
            }
        }else{
            //show only pdf
            self.startLoading()
            //self.loadPdfOverView(strPDF: strSavedPdfUrl)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loginDetails()
        self.setLeadDetails()
    }
    
    // MARK: -----IBAction
    @IBAction func action_Back(_ sender: Any) {
        self.view.endEditing(true)
        self.back()
    }

    @IBAction func action_Reload(_ sender: Any) {
        let alert = UIAlertController(title: alertMessage, message: "Reload will remove saved data. Are you sure you want to remove data?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "YES", style: .default, handler: { (nil) in
            //Call api to close ticket
            self.apiToRefreshForm()
        }))
        alert.addAction(UIAlertAction (title: "NO", style: .cancel, handler: { (nil) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func apiToRefreshForm() {
        //Call Api To Reload form
        var strFormbuilderUrl = ""
        if MainUrl == "https://pcrs.pestream.com/" {
            strFormbuilderUrl = "https://formbuilder.pestream.com:8443/document-builder/api/v1/template/"
        }else {
            strFormbuilderUrl = "https://formbuilderstaging.pestream.com:8443/document-builder/api/v1/template/"
        }
        let pram = "reload_template/\(self.strTemplateKey)/contractorId/\(self.strLeadId)/companyId/\(self.strCompanyKey)"
        
        let apiUrl = strFormbuilderUrl + pram
        print(apiUrl)
        
        //Call API
        showLoader()
        self.presenter.requestToReloadForm(strUrlToReload: apiUrl)
    }
    // MARK:
    // MARK: -----Functions
    func back() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func getOpportunityStatus() -> Bool {
        if strLeadStatusGlobal.caseInsensitiveCompare("Complete") == .orderedSame && strStageSysName.caseInsensitiveCompare("Won") == .orderedSame {
            return true
        } else {
            return false
        }
    }
    
    func loginDetails() {
        
        let dictLoginData: [String : Any] = nsud.value(forKey: "LoginDetails") as! [String : Any]
        print(dictLoginData)
        
        self.strCompanyKey = (dictLoginData["Company"]
                                as? [String: Any])?["CompanyKey"]
            as? String ?? ""
        self.strUserName = (dictLoginData["Company"]
                                as? [String: Any])?["Username"]
            as? String ?? ""
        
        self.strBaseUrl = (((dictLoginData["Company"]
            as? [String: Any])?["CompanyConfig"] as? [String: Any])?["SalesAutoModule"] as? [String: Any])?["ServiceUrl"] as? String ?? ""//"\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        print(self.strBaseUrl)
        
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func setLeadDetails() {
        
        print(self.matchesGeneralInfo)

        strLeadStatusGlobal = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        self.strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        
    }
    
    func startLoading() {
        self.loadSpinner.isHidden = false
        self.loadSpinner.startAnimating()
        self.loadPdfOverView(strPDF: self.strSavedPdfUrl)
        self.webView.navigationDelegate = self
    }
    
    func stopLoading() {
        self.loadSpinner.stopAnimating()
        self.loadSpinner.isHidden = true
    }
    
    // MARK:
    // MARK:----- WebView Delegate Functions
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

        //loadCount -= 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self!.stopLoading()
        }
    }
    
    func localStorageWebView() {
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strFirstName = "\(matchesGeneralInfo.value(forKey: "firstName") ?? "")"
        let strLastName = "\(matchesGeneralInfo.value(forKey: "lastName") ?? "")"
        let strMiddleName = "\(matchesGeneralInfo.value(forKey: "middleName") ?? "")"
        let strSAddress1 = "\(matchesGeneralInfo.value(forKey: "servicesAddress1") ?? "")"
        let strSAddress2 = "\(matchesGeneralInfo.value(forKey: "serviceAddress2") ?? "")"
        let strSCity = "\(matchesGeneralInfo.value(forKey: "serviceCity") ?? "")"
        let strSState = "\(matchesGeneralInfo.value(forKey: "serviceState") ?? "")"
        let strSZip = "\(matchesGeneralInfo.value(forKey: "serviceZipcode") ?? "")"
        let strPhone = formattedNumber(number: ReplaceBlankToSpace(str: "\(matchesGeneralInfo.value(forKey: "primaryPhone") ?? "")"))
        let strMobile = formattedNumber(number: ReplaceBlankToSpace(str: "\(matchesGeneralInfo.value(forKey: "cellNo") ?? "")"))
        let strEmail = "\(matchesGeneralInfo.value(forKey: "primaryEmail") ?? "")"
        let strSecEmail = "\(matchesGeneralInfo.value(forKey: "secondaryEmail") ?? "")"
        self.strTemplateKey = "\(self.dictTemplateObj?["templateKey"] ?? "")"
        
        
        /**************Basic****************/
        let strCustomerCompanyName = "\(matchesGeneralInfo.value(forKey: "companyName") ?? "")"
        let strAccountName = Global().strFullName(fromCoreDB: matchesGeneralInfo) ?? ""
        let strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        //let strEmpFullName = "\(matchesGeneralInfo.value(forKey: "firstName") ?? "") \(matchesGeneralInfo.value(forKey: "lastName") ?? "")"
//        let strEmpLicenseNo = "\(objPaymentInfo.value(forKey: "licenseNo") ?? "")"
        let strEmpLicenseNo = "\(dictLoginData.value(forKey: "LicenceNo") ?? "")"
        let strEmpFullName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        let strDateTime = global.strCurrentDate()
        let arrTemp = strDateTime?.split(separator: " ")
        let time = (arrTemp?[1] ?? "") + " " + (arrTemp?[2] ?? "")
        /******************************/
        
        let strBillingAddressLine1  = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? "")"
        let strBillingAddressLine2  = "\(matchesGeneralInfo.value(forKey: "billingAddress2") ?? "")"
        let strBillingCity          = "\(matchesGeneralInfo.value(forKey: "billingCity") ?? "")"
        let strBillingState         = "\(matchesGeneralInfo.value(forKey: "billingState") ?? "")"
        let strBillingZip           = "\(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
        let strBillingAddressLine1Line2 = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? "")" + " " + "\(matchesGeneralInfo.value(forKey: "billingAddress2") ?? "")"
        let strBillingFullAddress   = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? ""), \(matchesGeneralInfo.value(forKey: "billingAddress2") ?? ""), \(matchesGeneralInfo.value(forKey: "billingCity") ?? ""), \(matchesGeneralInfo.value(forKey: "billingState") ?? ""), \(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
        let strBillingCityStateZip  = "\(matchesGeneralInfo.value(forKey: "billingCity") ?? ""), \(matchesGeneralInfo.value(forKey: "billingState") ?? ""), \(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
        
        
        let paramObject: [String: Any] = ["templateKey": self.strTemplateKey,
                                          "date": "",
                                          "role": "\(role)",
                                          "type": "IOS",
                                          "contractorId": self.strLeadId,
                                          "companyKey": self.strCompanyKey,
                                          "showBtnReview": false,
                                          "isCustomerLink": false,
                                          "json": ["[LastFirstName]": "\(strLastName) \(strFirstName)",
                                                   "[FullName]": "\(strFirstName) \(strLastName)",
                                                   "[FirstName]": strFirstName,
                                                   "[LastName]": strLastName,
                                                   "[MiddleName]": strMiddleName,
                                                   "[ServiceAddressLine1]": strSAddress1,
                                                   "[ServiceAddressLine2]": strSAddress2,
                                                   "[ServiceCity]": strSCity,
                                                   "[City]": strSCity,
                                                   "[Block]": "",
                                                   "[District]": "",
                                                   "[ServiceState]": strSState,
                                                   "[ServiceZip]": strSZip,
                                                   "[ServiceAddressLine1Line2]": "\(strSAddress1) \(strSAddress2)",
                                                   "[ServiceFullAddress]": "\(strSAddress1) \(strSAddress2), \(strSCity), \(strSState), \(strSZip)",
                                                   "[ServiceCityStateZip]": "\(strSCity), \(strSState), \(strSZip)",
                                                   "[PhoneNumber]": strPhone,
                                                   "[MobileNumber]": strMobile,
                                                   "[Email]": strEmail,
                                                   "[AlternateEmail]": strSecEmail,
                                                   "[customImage]": self.arrImageDetails,
                                                   "[customGraph]": self.arrGraphDetails,
                                                   
                                                   "[CustomerCompanyName]": strCustomerCompanyName,
                                                   "[AccountName]": strAccountName,
                                                   "[Account#]": strAccountNo,
                                                   "[EmpFullName]": strEmpFullName,
                                                   "[EmpLicenseNo]": strEmpLicenseNo,
                                              
                                                   "[BillingAddressLine1]": strBillingAddressLine1,
                                                   "[BillingAddressLine2]": strBillingAddressLine2,
                                                   "[BillingCity]": strBillingCity,
                                                   "[BillingState]": strBillingState,
                                                   "[BillingZip]": strBillingZip,
                                                   "[BillingAddressLine1Line2]": strBillingAddressLine1Line2,
                                                   "[BillingFullAddress]": strBillingFullAddress,
                                                   "[BillingCityStateZip]": strBillingCityStateZip,
                                                  
                                                   "[CurrentDate]": global.getCurrentDate(),
                                                   "[CurrentDateTime]": global.strCurrentDate(),
                                                   "[CurrentTime]": time]]
        let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
        if let jsonString = String(data: jsonData!, encoding: .utf8) {
            // print(jsonString)
            //messageHandlers
            let messageHandlers = "buttonClicked"
            let configuration = WKWebViewConfiguration()
            let contentController = WKUserContentController()
            let js = "localStorage.setItem('templateData', JSON.stringify(\(jsonString)));"
            let userScript = WKUserScript(source: js, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: false)
            contentController.addUserScript(userScript)
            configuration.userContentController = contentController
            configuration.userContentController.add(self, name: messageHandlers)
            
            webView = WKWebView(frame: CGRect.init(x: 0, y: self.view_Header.frame.height + (hasNotch ? notch_TopSafeArea : WithoutNotch_TopSafeArea), width: self.view.bounds.width, height: self.view.bounds.height - self.view_Header.frame.height - (hasNotch ? notch_BottomSafeArea : WithoutNotch_BottomSafeArea)), configuration: configuration)
            webView.scrollView.maximumZoomScale = 10
            webView.scrollView.minimumZoomScale = 1
            var strFormbuilderUrl = ""
            if MainUrl == "https://pcrs.pestream.com/" {
                strFormbuilderUrl = "https://formbuilder.pestream.com:8443/#/template-preview"
            }else {
                strFormbuilderUrl = "https://formbuilderstaging.pestream.com:8443/#/template-preview"
            }
            webView.load(URLRequest(url: URL(string: strFormbuilderUrl)!))
            self.view.addSubview(webView)
            
//            webView.topAnchor.constraint(equalTo: self.viewForWeb.topAnchor).isActive = true
//            webView.leftAnchor.constraint(equalTo: self.viewForWeb.leftAnchor).isActive = true
//            webView.rightAnchor.constraint(equalTo: self.viewForWeb.rightAnchor).isActive = true
//            webView.bottomAnchor.constraint(equalTo: self.viewForWeb.bottomAnchor).isActive = true
            webView.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    func ReplaceBlankToSpace(str: String) -> String {
        return str == "" ? " " : str
    }
    
    func loadPdfOverView(strPDF: String) {
        let urlStr = strPDF
        
        let replaced = urlStr.replacingOccurrences(of: "\\", with: "")
        print(replaced)
        let strPdfRelative = replaced.replacingOccurrences(of: "\"", with: "")
        
        let pdfLink = self.strBaseUrl + "/Documents" + strPdfRelative
        print(pdfLink)

        let url = URL(string: pdfLink)
        
        if self.webView != nil {
            if url != nil {
                self.webView.load(URLRequest(url: url!))
            }
        }else {
            webView = WKWebView(frame: CGRect.init(x: 0, y: self.view_Header.frame.height + (hasNotch ? notch_TopSafeArea : WithoutNotch_TopSafeArea), width: self.view.bounds.width, height: self.view.bounds.height - self.view_Header.frame.height - (hasNotch ? notch_BottomSafeArea : WithoutNotch_BottomSafeArea)))
            
            webView.scrollView.maximumZoomScale = 10
            webView.scrollView.minimumZoomScale = 1
            
            if url != nil {
                self.webView.load(URLRequest(url: url!))
                self.view.addSubview(webView)
                
//                webView.topAnchor.constraint(equalTo: self.viewForWeb.topAnchor).isActive = true
//                webView.leftAnchor.constraint(equalTo: self.viewForWeb.leftAnchor).isActive = true
//                webView.rightAnchor.constraint(equalTo: self.viewForWeb.rightAnchor).isActive = true
//                webView.bottomAnchor.constraint(equalTo: self.viewForWeb.bottomAnchor).isActive = true
                webView.translatesAutoresizingMaskIntoConstraints = false
            }
        }
    }
    
    func saveInspectionFormBuilderDocumentsInDB(strPdfPath:String) {
        
        // Saving Data in DB
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
   
        arrOfKeys.add("leadId")
        arrOfKeys.add("accountNo")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("descriptionDocument")
        arrOfKeys.add("docType")
        arrOfKeys.add("fileName")
        arrOfKeys.add("leadDocumentId")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("scheduleStartDate")
        arrOfKeys.add("title")
        arrOfKeys.add("userName")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("otherDocSysName")
        arrOfKeys.add("docFormatType")
        arrOfKeys.add("isAddendum")
        arrOfKeys.add("isSync")
        arrOfKeys.add("isToUpload")
        arrOfKeys.add("inspectionFormBuilderMappingMasterId")
        arrOfKeys.add("templateKey")
        
        arrOfValues.add(strLeadId)
        arrOfValues.add("\(self.strAccountNo)")
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS", ""))
        arrOfValues.add("")
        arrOfValues.add("Inspection")
        arrOfValues.add(strPdfPath)
        arrOfValues.add(Global().getReferenceNumber())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS", "")) //  "yyyy-MM-dd'T'HH:mm:ss.SSS" "MM/dd/yyyy"
        arrOfValues.add("\(matchesGeneralInfo.value(forKey: "scheduleStartDate") ?? "")")
        arrOfValues.add("\(self.dictTemplateObj?["templateName"] ?? "")")
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add("")
        arrOfValues.add("pdf")
        arrOfValues.add("false")
        arrOfValues.add("false")
        arrOfValues.add("true")
        arrOfValues.add("\(self.dictTemplateObj?["InspectionFormBuilderMappingMasterId"] ?? "")")
        arrOfValues.add("\(self.dictTemplateObj?["templateKey"] ?? "")")

        saveDataInDB(strEntity: "DocumentsDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    //Deepak.s
    func deleteInspectionFormIf_AlreadyExist() {
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && templateKey == %@", self.strLeadId, self.strTemplateKey))
        if arrayData.count > 0 {
            for item in arrayData
            {
                let matches = item as! NSManagedObject
                print(matches.value(forKey: "templateKey"))
                deleteDataFromDB(obj: matches)
            }
        }
    }
}

//MARK: - API Response
//MARK: -
extension SalesFormBuilderVC: SalesFormBuilderDelegate {
    func showLoader() {
        
        self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
    }
    
    func hideLoader() {
        
        DispatchQueue.main.async {
            self.loader.dismiss(animated: false) {}
        }
    }
    
    func showAlert(message: String?) {
        
    }
    
    func sucessfullyGetHTML(_ output: String) {
        print(output)
        
//        if output != "" {
//            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: { [self] in
//                //webView.loadHTMLString(output, baseURL: nil)
//            })
//        }
    }
    
    func sucessfullyGetPDF(_ output: String) {
        
        hideLoader()
        
        print(output)
     //   let strUrl = output.replacingOccurrences(of: "\\", with: "")

        let replaced = output.replacingOccurrences(of: "\\", with: "")
        print(replaced)
        let strPdfRelative = replaced.replacingOccurrences(of: "\"", with: "")
        print(strPdfRelative)
        
        // Save in Lead Document Table.
        self.deleteInspectionFormIf_AlreadyExist()
        self.saveInspectionFormBuilderDocumentsInDB(strPdfPath: strPdfRelative)
        
        self.strOverViewPDF = strPdfRelative//strUrl
               
        
        
        if output != "" {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: { [self] in
                let alert = UIAlertController(title: alertMessage, message: "Inspection form Saved successfully.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                    
                        self.handelDataOnSavingFormBuilder?.getDataOnSaveFormBuilder(data: "")
                        self.back()
                }))
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    func sucessfullyGetReloadResponse(_ output: [String: Any]) {
        print(output)
        self.hideLoader()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.localStorageWebView()
            /* let strStatus = "\(output["status"] ?? "")"
            if strStatus == "200" {
                self.localStorageWebView()
            }else {
                //Message
                //Inspection template not exists,Please save template first
//                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Inspection form is not exist, please save first.", viewcontrol: self)
            }*/
        }
    }
    
    func failResponse(_ output: String)
    {
        print("FFFFFFFFFF: ", output)
        self.hideLoader()
    }
}

//MARK:- WK WebView Delegate
//MARK:-
extension SalesFormBuilderVC:WKScriptMessageHandler, WKUIDelegate {
            
    func userContentController(
        _ userContentController:
            WKUserContentController,
        didReceive message: WKScriptMessage) {
        
        if message.name == "buttonClicked",
           let dict = message.body as? NSDictionary {
            showLoader()
            //Get PDF from Api Calling
            self.presenter.requestToGetPdf(strbase64: dict["resPdfBase64"] as! String, contractorId: "\(self.role)", accountNo: self.strAccountNo, isRelativePath: true)
            self.role = 2
        }
    }
}
