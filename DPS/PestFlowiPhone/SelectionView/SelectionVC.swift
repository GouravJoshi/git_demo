//
//  SelctionVC.swift
//  DPS
//
//  Created by Saavan Patidar on 09/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//


// 101 Condicive condition Group master for conditions
// 102 Material Group master for Add  Materials
// 103 Target Group master for Add  Materials
// 104 Target Group master for Add  Pests

import UIKit

class SelectionVC: UIViewController {
    
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    open weak var delegate:SelectionVCDelegate?
    open weak var delegateDictData:SelectionVcDictDataDelegate?

    // MARK: - ----Variable
    
    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
    var strTitle = String()
    var strTypeOfSelection = String()
    var strTag = Int()
    var aryForSelectionView = NSMutableArray()
    var strSelectedTitle = String()
    var strBranchId = String()
    var dictDataSelected = NSDictionary()
    @objc var arySelectedTargets = NSMutableArray()
    @objc var aryTargetsToBeShownOnSelection = NSMutableArray()

    // MARK: - ----Life Cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        lblTitle.text = strTitle
        aryForListData = aryList
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        viewHeader.backgroundColor = UIColor.theme()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - ----IBAction
    
    @IBAction func actionOnBack(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if(self.strTag == 103){
            
            
        }else{
            
            self.delegate?.dimissViewww(tag: 123)

        }
        self.dismiss(animated: false) {}
        
    }
    
    // MARK: - -----------------------------------Functions-----------------------------------
    
    func goToSelectionClass(strTagg : Int)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRM", bundle: nil)
        let vc: SelectionClass = storyboardIpad.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strSelectedTitle
        vc.strTag = strTagg
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = aryForSelectionView
        vc.arySelectedTargets = arySelectedTargets
        //self.present(vc, animated: true, completion: {})
        present(vc, animated: false, completion: {})
        
    }
    
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SelectionVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "SelectionVCCell", for: indexPath as IndexPath) as! SelectionVCCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        if(self.strTag == 101){
            
            cell.lblTitle.text = "\(dict.value(forKey: "ConditionGroupName")!)"
            
        }else if(self.strTag == 102){
            
            cell.lblTitle.text = "\(dict.value(forKey: "ProductCategoryName")!)"
            
        } else if(self.strTag == 103){
            
            cell.lblTitle.text = "\(dict.value(forKey: "TargetGroupName")!)"
            
        }else if(self.strTag == 104){
            
            cell.lblTitle.text = "\(dict.value(forKey: "TargetGroupName")!)"
            
        }else if(self.strTag == 1){
            
            cell.lblTitle.text = "\(dict.value(forKey: "Name")!)"
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        
        if strTag == 101 {
            
            let dictData = aryList.object(at: indexPath.row)as? NSDictionary
            
            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionMasters" , strBranchId: strBranchId)
            
            if array.count > 0 {
                
                let nsGroupid = dictData!.value(forKey: "ConditionGroupId") as! NSNumber
                let strGroupid =  String("\(nsGroupid)")
                
                aryForSelectionView = getGroupDataOnly(arrOfData: array, strGroupId: strGroupid , strKeyName: "ConditionGroupId")
                
                //aryForSelectionView = array

                if aryForSelectionView.count > 0 {
                    
                    dictDataSelected = dictData!
                    
                    strSelectedTitle = "\(dictDataSelected.value(forKey: "ConditionGroupName") ?? "")"
                    
                    self.goToSelectionClass(strTagg: 102)

                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                    
                }

            }else{
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                
            }
            
        } else if strTag == 102 {
            
            // Material Groups
            
            let dictData = aryList.object(at: indexPath.row)as? NSDictionary
            
            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductMasters" , strBranchId: strBranchId)
            
            if array.count > 0 {
                
                let nsGroupid = dictData!.value(forKey: "ProductCategoryId") as! NSNumber
                let strGroupid =  String("\(nsGroupid)")
                
                aryForSelectionView = getGroupDataOnly(arrOfData: array, strGroupId: strGroupid , strKeyName: "ProductCategoryId")
                
                //aryForSelectionView = array

                if aryForSelectionView.count > 0 {
                    
                    dictDataSelected = dictData!
                    
                    strSelectedTitle = "\(dictDataSelected.value(forKey: "ProductCategoryName") ?? "")"

                    self.goToSelectionClass(strTagg: 103)
                    
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                    
                }
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                
            }
            
        } else if strTag == 103 {
            
            //  Material Groups
            
            let dictData = aryList.object(at: indexPath.row)as? NSDictionary
            
            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "TargetMasters" , strBranchId: strBranchId)
            
            let aryTemp = array
            
            // To Show Only Targets which are in Product masters
            
            if aryTargetsToBeShownOnSelection.contains("ShowAllTarget") {
                
            }else{
                
                array = NSMutableArray()

                for item in aryTemp {
                    
                    let dictData = (item as AnyObject) as! NSDictionary
                    
                    let strTargetIdLocal = "\(dictData.value(forKey: "TargetId") ?? "")"
                    
                    if aryTargetsToBeShownOnSelection.contains(strTargetIdLocal) {
                        
                        array.add(dictData)
                        
                    }
                    
                }
                
            }
            
            if array.count > 0 {
                
                let nsGroupid = dictData!.value(forKey: "TargetGroupId") as! NSNumber
                let strGroupid =  String("\(nsGroupid)")
                
                aryForSelectionView = getGroupDataOnly(arrOfData: array, strGroupId: strGroupid ,strKeyName: "TargetGroupId")
                
                //aryForSelectionView = array
                
                if aryForSelectionView.count > 0 {
                    
                    dictDataSelected = dictData!
                    
                    strSelectedTitle = "\(dictDataSelected.value(forKey: "TargetGroupName") ?? "")"

                    self.goToSelectionClass(strTagg: 104)
                    
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                    
                }
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                
            }
            
        }else if strTag == 104 {
            
            // Pest Groups
            
            let dictData = aryList.object(at: indexPath.row)as? NSDictionary
            
            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "TargetMasters" , strBranchId: strBranchId)
            
            if array.count > 0 {
                
                let nsGroupid = dictData!.value(forKey: "TargetGroupId") as! NSNumber
                let strGroupid =  String("\(nsGroupid)")
                
                aryForSelectionView = getGroupDataOnly(arrOfData: array, strGroupId: strGroupid ,strKeyName: "TargetGroupId")
                
                //aryForSelectionView = array
                
                if aryForSelectionView.count > 0 {
                    
                    dictDataSelected = dictData!
                    
                    strSelectedTitle = "\(dictDataSelected.value(forKey: "TargetGroupName") ?? "")"

                    self.goToSelectionClass(strTagg: 105)
                    
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                    
                }
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                
            }
            
        }else {
            
            self.goToSelectionClass(strTagg: 1)
            
        }
        
    }
    
    
}
// MARK: - ----------------SlectionCell
// MARK: -
class SelectionVCCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
}

// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  SelectionVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "Name contains[c] %@ OR ConditionGroupName contains[c] %@ OR TargetGroupName contains[c] %@ OR ProductCategoryName  contains[c] %@ OR SubCategoryName contains[c] %@ OR CategoryName contains[c] %@", argumentArray: [Searching, Searching , Searching , Searching, Searching , Searching])
        if !(Searching.length == 0) {
            
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(aryList.count == 0){
        }
    }
}

// MARK:-
// MARK:- ---------PopUpDelegate

extension SelectionVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        
        if(tag == 102){
            
            let mergedDict = dictDataSelected + dictData
            
            self.delegateDictData?.dimissViewwwOnSelection(tag: 123, dictData: mergedDict)

            print(mergedDict)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }else if(tag == 103){
            
            let mergedDict = dictDataSelected + dictData
            
            self.delegateDictData?.dimissViewwwOnSelection(tag: 123, dictData: mergedDict)
            
            print(mergedDict)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }else if(tag == 104){
            
            let mergedDict = dictDataSelected + dictData
            
            self.delegateDictData?.dimissViewwwOnSelection(tag: 124, dictData: mergedDict)
            
            print(mergedDict)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }else if(tag == 105){
            
            let mergedDict = dictDataSelected + dictData
            
            self.delegateDictData?.dimissViewwwOnSelection(tag: 123, dictData: mergedDict)
            
            print(mergedDict)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }else if(tag == 1){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }
        
    }
}


// MARK: -------Protocol

protocol SelectionVCDelegate : class{
    
    func dimissViewww(tag : Int)
    
}

protocol SelectionVcDictDataDelegate : class{
    
    func dimissViewwwOnSelection(tag : Int ,  dictData : NSDictionary)
    
}
