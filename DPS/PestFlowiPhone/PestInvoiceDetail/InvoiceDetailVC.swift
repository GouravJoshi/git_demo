//
//  InvoiceDetailVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 22/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
// ServiceJobDescriptionId    ServiceJobDescription

import UIKit

class InvoiceDetailVC: UIViewController
{
    
    // MARK: -  ----- Outlet  -----
    
    @IBOutlet weak var scroolVieww: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var MainVieww: UIView!
    
    @IBOutlet weak var const_ServiceReportType_H: NSLayoutConstraint!
    
    @IBOutlet weak var viewServiceReportType: UIView!
    
    @IBOutlet weak var viewServiceDetail: UIView!
    
    @IBOutlet weak var tblServiceDetails: UITableView!
    
    @IBOutlet weak var const_TblViewServiceDetail_H: NSLayoutConstraint!
    
    @IBOutlet weak var const_ViewServiceDetail_H: NSLayoutConstraint!
    
    @IBOutlet weak var btnServiceJobTitle: UIButton!

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnGeneralInfo: UIButton!
    
    @IBOutlet weak var btnInspection: UIButton!
    
    @IBOutlet weak var btnServiceDetail: UIButton!
    
    @IBOutlet weak var btnInvoice: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnRecordAudio: UIButton!
    
    // MARK: -  ----- Outlet View Customer Info -----
    
    @IBOutlet weak var viewCustomerInfo: UIView!
    @IBOutlet weak var lblWorkorderId: UILabel!
    @IBOutlet weak var lblAccountNo: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var btnPrimaryEmail: UIButton!
    @IBOutlet weak var btnSecondaryEmail: UIButton!
    @IBOutlet weak var btnPrimaryPhone: UIButton!
    @IBOutlet weak var btnSecondaryPhone: UIButton!
    @IBOutlet weak var btnAddressCustomerInfo: UIButton!
    
    // MARK: -  ----- Outlet View Billing Address -----
    
    @IBOutlet weak var viewBillingAddress: UIView!
    @IBOutlet weak var btnBillingAddress: UIButton!
    
    
    
    // MARK: -  ----- Outlet View Service Address -----
    
    @IBOutlet weak var viewServiceAddress: UIView!
    @IBOutlet weak var btnServiceAddress: UIButton!
    
    
    // MARK: -  ----- Outlet View Service Job Title -----
    
    @IBOutlet weak var viewServiceJobTitle: UIView!
    @IBOutlet weak var lblServiceJobTitle: UILabel!
    @IBOutlet weak var txtViewServiceJobDescription: UITextView!
    
    @IBOutlet weak var const_ServiceJobTitle_H: NSLayoutConstraint!
    
    // MARK: -  ----- Outlet View Inspection Form -----
    
    @IBOutlet weak var viewInspectionForm: UIView!
    
    @IBOutlet weak var const_ViewInspection_H: NSLayoutConstraint!
    // MARK: -  ----- Outlet View Payment Mode -----
    
    @IBOutlet weak var viewPaymentMode: UIView!
    
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnCreditCard: UIButton!
    @IBOutlet weak var btnBill: UIButton!
    @IBOutlet weak var btnAutochargeCustomer: UIButton!
    @IBOutlet weak var btnPaymentPending: UIButton!
    @IBOutlet weak var btnNoCharge: UIButton!
    @IBOutlet weak var txtFieldAmount: UITextField!
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var viewForCheckDetail: UIView!
    @IBOutlet weak var txtFieldCheckNo: UITextField!
    @IBOutlet weak var txtFieldDrivingLicenseNo: UITextField!
    @IBOutlet weak var btnExpirationDate: UIButton!
    @IBOutlet weak var btnCheckFrontImage: UIButton!
    @IBOutlet weak var btnCheckBackImage: UIButton!
    
    
    @IBOutlet weak var const_ViewPaymentMode_H: NSLayoutConstraint!
    @IBOutlet weak var const_ViewCheck_H: NSLayoutConstraint!
    
    // MARK: -  ----- Outlet View Payment Detail -----
    
    @IBOutlet weak var viewPaymentDetail: UIView!
    @IBOutlet weak var lblChargesForCurrentServices: UILabel!
    @IBOutlet weak var lblTaxValue: UILabel!
    @IBOutlet weak var lblTotalDueValue: UILabel!
    
    // MARK: -  ----- Outlet View Other Detail -----
    
    @IBOutlet weak var viewOtherDetail: UIView!
    
    @IBOutlet weak var lblTimeIn: UILabel!
    @IBOutlet weak var lblTimeOut: UILabel!
    @IBOutlet weak var txtViewTechnicianComment: UITextView!
    @IBOutlet weak var txtViewOfficeNotes: UITextView!
    @IBOutlet weak var imgViewTechnicianSignature: UIImageView!
    @IBOutlet weak var imgViewCustinerSignature: UIImageView!
    
    @IBOutlet weak var btnTechnicianSignature: UIButton!
    @IBOutlet weak var btnCustomerSignature: UIButton!
    @IBOutlet weak var btnCustomerNotPresent: UIButton!
    
    
    
    // MARK: -  ----- Outlet View Service Terms -----
    
    @IBOutlet weak var viewServiceTerms: UIView!
    
    @IBOutlet weak var txtViewServiceTerms: UITextView!
    
    @IBOutlet weak var const_viewServiceTerms_H: NSLayoutConstraint!
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    
    @objc var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var dictOfServiceJobDescriptionSelected = NSDictionary()
    var strNoOfArea = String()
    var strNoOfAreaInspected = String()
    var arrOfDeviceType = NSArray()
    var arrOfPests = NSArray()
    var arrOfArea = NSArray()
    var arrOfServiceReportType = NSArray()
    var strServiceReportType = String()

    // MARK: -  ----- Variable Declaration -----
    
    // MARK: - --- String ----
    
    var strEmpID = String()
    var strEmpName = String()
    var strFromEmail = String()
    var strGlobalPaymentMode = String()
    
    var strCustomerSign = String()
    var strTechnicianSign = String()
    
    var strCheckFrontImage = String()
    var strCheckBackImage = String()
    
    var strAudioName = String()
    
    
    // MARK: - --- Array ----
    
    var arrData = NSMutableArray()
    
    // MARK: - --- Dictionary ----
    
    var dictLoginData = NSDictionary()
    var dictDepartmentNameFromSysName = NSDictionary()
    
    // MARK: - --- Bool ----
    
    var chkFrontImage = Bool()
    var chkCustomerNotPresent = Bool()
    var isPreSetSignGlobal = Bool()
    var yesEditedSomething = Bool()
    
    // MARK: -  ----- Variable Declaration -----
    
    
    @objc   var dictJsonDynamicForm = NSDictionary()
    @objc   var workOrderDetail = NSManagedObject()
    @objc   var paymentInfoDetail = NSManagedObject()
    @objc   var strPaymentModes = String()
    @objc   var strDepartmentIdd = String()
    @objc   var strPaidAmount = String()
    
    
    
    // MARK: -  --------------------------- ViewDidLoad ---------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        strGlobalPaymentMode = ""
        strCheckFrontImage = ""
        strCheckBackImage = ""
        strCustomerSign = ""
        strTechnicianSign = ""
        strAudioName = ""
        chkFrontImage = false
        chkCustomerNotPresent = false
        isPreSetSignGlobal = false
        yesEditedSomething = false
        
        lblTitle.text = "\(nsud.value(forKey: "lblName") ?? "")"
        
        btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
        
        //temp
        /* var strUrl = "https://tcrs.Pestream.com//Documents/\\BranchLogo/AutomationIndiaBranch_20180710103008072_logo.jpg" as String
         
         strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
         
         let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: "custImg.jpg")
         
         let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: "custImg.jpg")
         
         if isImageExists!
         {
         imgViewCustinerSignature.image = image
         }
         else
         {
         //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
         let nsUrl = URL(string: strUrl)
         imgViewCustinerSignature.load(url: nsUrl! as URL , strImageName: strUrl)
         }*/
        //end
        
        
        
        
        
        
        
        
        
        
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strServiceReportTypeLocal = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
        
        if strServiceReportTypeLocal == "CompanyEmail"
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
        }
        else
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        }
        
        //strWoId = "353189"
        
        
        fetchWorkOrderDetailFromCoreData()
        fetchPaymentInfoDetailFromCoreData()
        
        
        setBorderColor(item: txtFieldAmount)
        setBorderColor(item: txtViewOfficeNotes)
        setBorderColor(item: txtViewTechnicianComment)
        setBorderColor(item: txtViewServiceJobDescription)
        setBorderColor(item: txtFieldCheckNo)
        setBorderColor(item: txtViewServiceTerms)
        setBorderColor(item: txtFieldDrivingLicenseNo)
        //setBorderColor(item: btnSave)
        buttonRound(sender: btnSave)
        buttonRound(sender: btnCheckBackImage)
        buttonRound(sender: btnCheckFrontImage)
        buttonRound(sender: btnExpirationDate)
        setBorderColor(item: btnServiceJobTitle)

        
        
        setColorBorderForView(item: viewCustomerInfo)
        setColorBorderForView(item: viewBillingAddress)
        setColorBorderForView(item: viewServiceAddress)
        setColorBorderForView(item: viewServiceJobTitle)
        setColorBorderForView(item: viewInspectionForm)
        setColorBorderForView(item: viewPaymentMode)
        setColorBorderForView(item: viewPaymentDetail)
        setColorBorderForView(item: viewOtherDetail)
        setColorBorderForView(item: viewServiceTerms)
        setColorBorderForView(item: viewServiceDetail)

        assignValues()
        getTermsCondition()
        
        fetchDepartmentNameBySysName()
        setDynamicData()
        
        // Do any additional setup after loading the view.
        
        fetchServiceDetailsData()

        createDynamicServiceReportView()
        
        // Check to hide service job description isNoServiceJobDescription
        
        let isNoServiceJobDescription = nsud.bool(forKey: "isNoServiceJobDescription")
        
        if isNoServiceJobDescription {
            const_ServiceJobTitle_H.constant = 0
        }
        //const_ServiceJobTitle_H.constant = 0

    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        var strIns = String()
        var strCust = String()
        var strImageName = String()
        //strImageName = nsud.value(forKey: "imagePath") as! String
        strIns = nsud.value(forKey: "fromInspectorSignService") as? String ?? ""
        strCust = nsud.value(forKey: "fromCustomerSignService") as? String ?? ""
        
        if (strIns == "fromInspectorSignService")
        {
            strImageName = nsud.value(forKey: "imagePath") as! String
            strTechnicianSign = strImageName
            yesEditedSomething = true
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            
            
            if let dirPath = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strImageName)
                let image    = UIImage(contentsOfFile: imageURL.path)
                imgViewTechnicianSignature.image = image
                
            }
            saveImageDocumentDirectory(strFileName: strImageName, image: imgViewTechnicianSignature.image!)
            nsud.set("abc", forKey: "fromInspectorSignService")
            nsud.synchronize()
            
        }
        if (strCust == "fromCustomerSignService")
        {
            strImageName = nsud.value(forKey: "imagePath") as! String
            strCustomerSign = strImageName
            yesEditedSomething = true
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            if let dirPath = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strImageName)
                let image    = UIImage(contentsOfFile: imageURL.path)
                imgViewCustinerSignature.image = image
            }
            
            saveImageDocumentDirectory(strFileName: strImageName, image: imgViewCustinerSignature.image!)
            nsud.set("abc", forKey: "fromCustomerSignService")
            nsud.synchronize()
        }
        
        if strTechnicianSign.count>0
        {
            downloadTechnicianSign(strImageName: strTechnicianSign)
        }
        
        if strCustomerSign.count>0
        {
            downloadCustomerSign(strImageName: strCustomerSign)
        }
        
        
        //For Preset
        
        let isPreSetSign = nsud.bool(forKey: "isPreSetSignService")
        
        var strSignUrl = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
        strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
        if ((isPreSetSign) && (strSignUrl.count>0) && !(strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed"))
        {
            /* if strImageName.count == 0
             {
             strImageName = "imgTechnician" + getUniqueString()
             strTechnicianSign = strImageName
             }*/
            strImageName = Global().strTechSign(strSignUrl)
            strTechnicianSign = Global().strTechSign(strSignUrl)
            
            let nsUrl = URL(string: strSignUrl)
            
            imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
            
            saveImageDocumentDirectory(strFileName: strTechnicianSign, image: imgViewTechnicianSignature.image!)
            
            btnTechnicianSignature.isEnabled = false
            isPreSetSignGlobal = true
            
        }
        else
        {
            isPreSetSignGlobal = false
            btnTechnicianSignature.isEnabled = true
            
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed")
            {
                btnTechnicianSignature.isEnabled = false
            }
            if strTechnicianSign.count>0
            {
                let strIsPresetWO = objWorkorderDetail.value(forKey: "isEmployeePresetSignature") as! String
                
                if strIsPresetWO == "true" || strIsPresetWO == "1"
                {
                    downloadTechnicianPreset(strImageName: strTechnicianSign, stringUrl: strSignUrl)
                }
                else
                {
                    downloadTechnicianSign(strImageName: strTechnicianSign)
                }
                
                
            }
        }
        
        //For Audio
        if (nsud.value(forKey: "yesAudio") != nil)
        {
            yesEditedSomething = true
            let isAudio = nsud.bool(forKey: "yesAudio")
            
            if isAudio
            {
                
                strAudioName = nsud.value(forKey: "AudioNameService") as! String
                
            }
            
        }
        
        methodForDisable()
        
    }
    
    // MARK: -  ----- Action  -----
    
    @IBAction func action_ServiceJobTitle(_ sender: UIButton) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "Title", array: array)
            
            gotoPopViewWithArray(sender: sender, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        
        self.view.endEditing(true)
        
        // Save Office Notes and Comments On Back in DB
        
        if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed")
        {
            
        }else{
            
            saveDataOnBack()
            
        }
        
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnRecordAudio(_ sender: Any)
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        
        if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed")
        {
            
        }else{
            
            alert.addAction(UIAlertAction(title: "Record", style: .default , handler:{ (UIAlertAction)in
                
                let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
                testController?.strFromWhere = "ServiceInvoice"
                        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
                
                
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
            
            let audioName = nsud.value(forKey: "AudioNameService")
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
            testController!.strAudioName = audioName as! String
                    testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    @IBAction func action_Image(_ sender: Any) {
        
        self.goToGlobalmage(strType: "After")
        
    }
    
    @IBAction func action_ServiceHistory(_ sender: Any) {
        
        self.goToServiceHistory()
        
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: Any) {
        
        self.goToCustomerSalesDocuments()
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")
        
        
    }
    
    @IBAction func action_NotesHistory(_ sender: Any) {
        
        self.goToNotesHistory()
        
    }
    
    
    @IBAction func actionOnSave(_ sender: Any)
    {
        if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed")
        {
            let mainStoryboard = UIStoryboard(name: "Service_iPhone", bundle: nil)
            let objVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSendMailViewController") as? ServiceSendMailViewController
            
            var strFromWheree = NSString()
            strFromWheree = "PestInvoice"
            
            objVC?.fromWhere = strFromWheree as String
            self.navigationController?.pushViewController(objVC!, animated: false)
        }
        else
        {
            
            finalSave(strPaymentMode: strGlobalPaymentMode)
            
            /* if strGlobalPaymentMode.count == 0 && chkCustomerNotPresent == false
             {
             showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select Payment Mode", viewcontrol: self)
             }
             else if strCustomerSign.count == 0
             {
             showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
             }
             else
             {
             finalSave(strPaymentMode: strGlobalPaymentMode)
             }*/
        }
    }
    func finalSave(strPaymentMode: String)
    {
        if strGlobalPaymentMode == "Cash" || strGlobalPaymentMode == "Check" || strGlobalPaymentMode == "CreditCard"
        {
            
            let valTextAmount = Float(txtFieldAmount.text!)
            
            if txtFieldAmount.text == "" || txtFieldAmount.text == "0" || txtFieldAmount.text == "00.00" ||  (Double(txtFieldAmount.text!) == nil || valTextAmount == 0  )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter amount", viewcontrol: self)
            }
                
            else
            {
                if chkCustomerNotPresent == true
                {
                    if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                        
                    }
                        
                    else if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    }
                    else if ((strServiceReportType.count == 0) && (arrOfServiceReportType.count > 0))
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: Alert_SelectServiceReportType, viewcontrol: self)
                    }
                    else
                    {
                        if strGlobalPaymentMode == "Check"
                        {
                            if (txtFieldCheckNo.text == "" )
                            {
                                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check no", viewcontrol: self)
                            }
                            else
                            {
                                updatePaymentInfo()
                                updateWorkOrderDetail()
                            }
                        }else{
                            
                            updatePaymentInfo()
                            updateWorkOrderDetail()
                            
                        }
                    }
                    
                }
                else
                {
                    if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                        
                    }
                    else if strCustomerSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                        
                    }
                    else if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    }
                    else if ((strServiceReportType.count == 0) && (arrOfServiceReportType.count > 0))
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: Alert_SelectServiceReportType, viewcontrol: self)
                    }
                    else
                    {
                        
                        if strGlobalPaymentMode == "Check"
                        {
                            if (txtFieldCheckNo.text == "" )
                            {
                                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check no", viewcontrol: self)
                            }
                            else
                            {
                                updatePaymentInfo()
                                updateWorkOrderDetail()
                            }
                        }else{
                            
                            updatePaymentInfo()
                            updateWorkOrderDetail()
                            
                        }
                        
                    }
                }
            }
            
        }
        else if strGlobalPaymentMode == "Bill" || strGlobalPaymentMode == "NoCharge" || strGlobalPaymentMode == "AutoChargeCustomer" || strGlobalPaymentMode == "PaymentPending"
        {
            if chkCustomerNotPresent == true
            {
                if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                else if checkImage() == true
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                }
                else if ((strServiceReportType.count == 0) && (arrOfServiceReportType.count > 0))
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: Alert_SelectServiceReportType, viewcontrol: self)
                }
                else
                {
                    updatePaymentInfo()
                    updateWorkOrderDetail()
                }
            }
            else
            {
                if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                else if strCustomerSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                    
                }
                else if checkImage() == true
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                }
                else if ((strServiceReportType.count == 0) && (arrOfServiceReportType.count > 0))
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: Alert_SelectServiceReportType, viewcontrol: self)
                }
                else
                {
                    updatePaymentInfo()
                    updateWorkOrderDetail()
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select Payment Mode", viewcontrol: self)
        }
        
        
    }
    
    
    
    // MARK: -  ----- Action View Customer Info -----
    
    @IBAction func actionOnPrimaryEmail(_ sender: Any)
    {
        global.emailComposer(btnPrimaryEmail.titleLabel?.text, "", "", self)
        
    }
    
    @IBAction func actionOnSecondaryEmail(_ sender: Any)
    {
        global.emailComposer(btnSecondaryEmail.titleLabel?.text, "", "", self)
        
    }
    
    @IBAction func actionOnPrimaryPhone(_ sender: Any)
    {
        if ((btnPrimaryPhone.titleLabel?.text?.count)!>0)
        {
            global.calling(btnPrimaryPhone.titleLabel?.text)
            
        }
    }
    
    @IBAction func actionOnSecondaryPhone(_ sender: Any)
    {
        if ((btnSecondaryPhone.titleLabel?.text?.count)!>0)
        {
            global.calling(btnSecondaryPhone.titleLabel?.text)
            
        }
    }
    
    @IBAction func actionOnAddressCustomerInfo(_ sender: Any)
    {
        global.redirect(onAppleMap: self, btnAddressCustomerInfo.titleLabel?.text)
    }
    
    
    // MARK: -  ----- Action View Billing Address -----
    
    @IBAction func actionOnBillingAddress(_ sender: Any)
    {
        global.redirect(onAppleMap: self, btnBillingAddress.titleLabel?.text)
        
    }
    
    // MARK: -  ----- Action View Service Address -----
    
    @IBAction func actionOnServiceAddress(_ sender: Any)
    {
        global.redirect(onAppleMap: self, btnServiceAddress.titleLabel?.text)
        
    }
    
    
    // MARK: -  ----- Action View Payment Mode -----
    
    @IBAction func actionOnCash(_ sender: Any)
    {
        yesEditedSomething = true
        cashPaymentMode()
        
        
    }
    
    @IBAction func actionOnCheck(_ sender: Any)
    {
        yesEditedSomething = true
        checkPaymentMode()
        
    }
    
    @IBAction func actionOnCreditCard(_ sender: Any)
    {
        yesEditedSomething = true
        creditCardPaymentMode()
        
    }
    
    @IBAction func actionOnBill(_ sender: Any)
    {
        yesEditedSomething = true
        noBillPaymentMode()
    }
    
    @IBAction func actionOnAutoChargeCustomer(_ sender: Any)
    {
        yesEditedSomething = true
        autoChargeCustomerPaymentMode()
        
    }
    
    @IBAction func actionOnPaymentPending(_ sender: Any)
    {
        yesEditedSomething = true
        paymentPendingPaymentMode()
        
    }
    
    @IBAction func actionOnNoCharge(_ sender: Any)
    {
        yesEditedSomething = true
        noChargePaymentMode()
        
    }
    
    @IBAction func actionOnExpirationDate(_ sender: Any)
    {
        //btnStartTime.tag = 10
        
        self.gotoDatePickerView(sender: sender as! UIButton, strType: "Date")
    }
    
    @IBAction func actionOnCheckFrontImage(_ sender: Any)
    {
        
        chkFrontImage = true
        yesEditedSomething = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    @IBAction func actionOnCheckBackImage(_ sender: Any)
    {
        
        chkFrontImage = false
        yesEditedSomething = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    // MARK: -  ----- Action View Other Detail -----
    
    
    @IBAction func actionOnCustomerNotPresent(_ sender: Any)
    {
        if (btnCustomerNotPresent.backgroundImage(for: .normal) == UIImage(named: "check_box_1.png"))
        {
            btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            imgViewCustinerSignature.isHidden = true
            btnCustomerSignature.isHidden = true
            chkCustomerNotPresent = true
        }
        else
        {
            btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            
            imgViewCustinerSignature.isHidden = false
            btnCustomerSignature.isHidden = false
            
            chkCustomerNotPresent = false
            
        }
    }
    
    @IBAction func actionOnTechnicianSignature(_ sender: Any)
    {
        nsud.set("fromTechService", forKey: "signatureType")
        nsud.synchronize()
        
        let mainStoryboard = UIStoryboard(name: "Service_iPhone", bundle: nil)
        let objVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSignViewController") as? ServiceSignViewController
        self.present(objVC!, animated: false, completion: nil)
    }
    
    @IBAction func actionOnCustomerSignature(_ sender: Any)
    {
        
        nsud.set("fromCustService", forKey: "signatureType")
        nsud.synchronize()
        
        let mainStoryboard = UIStoryboard(name: "Service_iPhone", bundle: nil)
        let objVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSignViewController") as? ServiceSignViewController
        //self.navigationController?.pushViewController(objVC!, animated: true)
        self.present(objVC!, animated: false, completion: nil)
        
    }
    
    // MARK: - --------- Function -------------
    
    
    
    // MARK: - --------------------------Core Data Function --------------------------
    
    func fetchWorkOrderDetailFromCoreData()
    {
        let arryOfWorkOrderData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfWorkOrderData.count > 0
        {
            
            objWorkorderDetail = arryOfWorkOrderData[0] as! NSManagedObject
            
            assignValues()
            
        }
        else
        {
            
            // showAlertWithoutAnyAction(strtitle: alertMessage, assignstrMessage: alertNoWo, viewcontrol: self)
            
            //self.back()
            
        }
    }
    
    
    func savePaymentInfoInCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = ["CheckBackImagePath",
                     "CheckFrontImagePath",
                     "CheckNo",
                     "CreatedBy",
                     "CreatedDate",
                     "DrivingLicenseNo",
                     "ExpirationDate",
                     "ModifiedBy",
                     "ModifiedDate",
                     "PaidAmount",
                     "PaymentMode",
                     "WoPaymentId"]
        
        arrOfValues = ["",
                       "",
                       "",
                       strUserName,
                       global.getCurrentDate(),
                       "",
                       "" ,
                       strUserName,
                       global.getCurrentDate(),
                       "",
                       "Cash",
                       ""]
        
        saveDataInDB(strEntity: "PaymentInfoServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    func fetchPaymentInfoDetailFromCoreData()
    {
        
        let arryOfWorkOrderPaymentData = getDataFromCoreDataBaseArray(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfWorkOrderPaymentData.count>0
        {
            let objPaymentInfoData = arryOfWorkOrderPaymentData[0] as! NSManagedObject
            paymentInfoDetail = objPaymentInfoData
            strPaymentModes = paymentInfoDetail.value(forKey: "paymentMode")  as! String
            strPaidAmount = paymentInfoDetail.value(forKey: "paidAmount") as! String
            
            txtFieldAmount.text = strPaidAmount
            
        }
        else
        {
            savePaymentInfoInCoreData()
            fetchPaymentInfoDetailFromCoreData()
        }
        
        
        if strPaymentModes == "Cash"
        {
            cashPaymentMode()
        }
        else if strPaymentModes == "Check"
        {
            checkPaymentMode()
        }
        else if strPaymentModes == "CreditCard"
        {
            creditCardPaymentMode()
        }
        else if strPaymentModes == "AutoChargeCustomer"
        {
            autoChargeCustomerPaymentMode()
        }
        else if strPaymentModes == "NoCharge"
        {
            noChargePaymentMode()
        }
        else if strPaymentModes == "NoBill"
        {
            noBillPaymentMode()
        }
        else if strPaymentModes == "PaymentPending"
        {
            paymentPendingPaymentMode()
        }
        else
        {
            cashPaymentMode()
        }
        
        txtFieldCheckNo.text =  "\(paymentInfoDetail.value(forKey: "checkNo") ?? "")"
        txtFieldDrivingLicenseNo.text = "\(paymentInfoDetail.value(forKey: "drivingLicenseNo") ?? "")"
        
        btnExpirationDate.setTitle("\(paymentInfoDetail.value(forKey: "expirationDate") ?? "")", for: .normal)
    }
    func fetchDynamicFormData()
    {
        let arryOfDynamicFormDataData = getDataFromCoreDataBaseArray(strEntity: "ServiceDynamicForm", predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arryOfDynamicFormDataData.count>0
        {
            var dictFinalInspection = NSDictionary()
            
            let objDynamicFormDataData = arryOfDynamicFormDataData[0] as! NSManagedObject
            let arr = objDynamicFormDataData.value(forKey: "arrFinalInspection") as? NSArray
            
            if arr != nil && (arr?.count)! > 0
            {
                
                dictFinalInspection = (arr![0] as? NSDictionary)!
                
            }
            else if (objDynamicFormDataData.value(forKey: "arrFinalInspection") is NSDictionary)
            {
                
                dictFinalInspection = (objDynamicFormDataData.value(forKey: "arrFinalInspection") as? NSDictionary)!
                
            }else {
                
                dictFinalInspection = NSDictionary()
                
            }
            
            dictJsonDynamicForm = dictFinalInspection
            
        }
        
    }
    
    func updatePaymentInfo()
    {
        // Update Payment Info
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("paymentMode")
        arrOfKeys.add("paidAmount")
        arrOfKeys.add("checkNo")
        arrOfKeys.add("drivingLicenseNo")
        arrOfKeys.add("expirationDate")
        arrOfKeys.add("checkFrontImagePath")
        arrOfKeys.add("checkBackImagePath")
        
        
        arrOfValues.add(strGlobalPaymentMode)
        arrOfValues.add(txtFieldAmount.text ?? "")
        arrOfValues.add(txtFieldCheckNo.text ?? "")
        arrOfValues.add(txtFieldDrivingLicenseNo.text ?? "")
        arrOfValues.add(btnExpirationDate.titleLabel?.text ?? "")
        arrOfValues.add(strCheckFrontImage)
        arrOfValues.add(strCheckBackImage)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    func updateWorkOrderDetail()
    {
        // Update Payment Info
        
        if chkCustomerNotPresent == true
        {
            strCustomerSign = ""
        }
        
        //let isCumpulsory = nsud.bool(forKey: "isCompulsoryAfterImageService") as! Bool
        
        var strWorkOrderStatusFinal = String()
        strWorkOrderStatusFinal = "Completed"
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let date = Date()
        var strTimeOut = String()
        strTimeOut = dateFormatter.string(from: date)
        
        var coordinate = CLLocationCoordinate2D()
        coordinate = Global().getLocation()
        let strTimeOutLat = "\(coordinate.latitude)"
        let strTimeOutLong = "\(coordinate.longitude)"
        
        
        var strResendStatus = String()
        strResendStatus = "0"
        if strWorkOrderStatus == "Incomplete"
        {
            strResendStatus = "0"
        }
        
        
        var isElementIntegraiton = Bool()
        isElementIntegraiton = Bool("\((dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsElementIntegration")) ?? "false")") ?? false
        if isElementIntegraiton == true
        {
            if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
            {
                strWorkOrderStatusFinal = "InComplete"
            }
        }
        
        var strCustomerNotPresent = String()
        
        if chkCustomerNotPresent == false
        {
            strCustomerNotPresent = "false"
        }
        else
        {
            strCustomerNotPresent = "true"
        }
        
        var strPresetStatus = String()
        
        if isPreSetSignGlobal == true
        {
            strPresetStatus = "true"
        }
        else
        {
            strPresetStatus = "false"
        }
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strServiceJobTitle = ""
        var strServiceJobTitleId = ""
        
        if btnServiceJobTitle.titleLabel?.text == strSelectString {
            
            strServiceJobTitle = ""
            strServiceJobTitleId = ""
            
        }else{
            
            strServiceJobTitle = (btnServiceJobTitle.titleLabel?.text)!
            strServiceJobTitleId = "\(dictOfServiceJobDescriptionSelected.value(forKey: "ServiceJobDescriptionId") ?? "")"
            
        }
        
        
        arrOfKeys = ["technicianSignaturePath",
                     "customerSignaturePath",
                     "audioFilePath",
                     "technicianComment",
                     "officeNotes",
                     "timeOut",
                     "timeOutLatitude",
                     "timeOutLongitude",
                     "isResendInvoiceMail",
                     "workorderStatus",
                     "isCustomerNotPresent",
                     "IsRegularPestFlow",
                     "isEmployeePresetSignature",
                     "serviceJobDescription",
                     "serviceReportFormat",
                     "serviceJobDescriptionId"
        ]
        //strWorkOrderStatusFinal = "InCompleted"
        arrOfValues = [strTechnicianSign,
                       strCustomerSign,
                       strAudioName,
                       txtViewTechnicianComment.text,
                       txtViewOfficeNotes.text,
                       strTimeOut,
                       strTimeOutLat,
                       strTimeOutLong,
                       strResendStatus,
                       strWorkOrderStatusFinal,
                       strCustomerNotPresent,
                       "true",
                       strPresetStatus,
                       strServiceJobTitle,
                       strServiceReportType,
                       strServiceJobTitleId
            
            
        ]
        
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            if yesEditedSomething
            {
                updateModifyDate()
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            if isElementIntegraiton == true
            {
                if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
                {
                    print("Go to credit card ciew")
                    goToCreditCardScreen()
                }
                else
                {
                    print("Go to send mail")
                    goToSendEmailScreen()
                }
            }
            else
            {
                
                print("Go to send mail")
                goToSendEmailScreen()
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func checkImage() -> Bool
    {
        
        let chkImage = nsud.bool(forKey: "isCompulsoryAfterImageService")
        
        if chkImage == true
        {
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, "After"))
            
            if arryOfData.count > 0
            {
                return false
            }
            else
            {
                return true
            }
            
        }
        else
        {
            
            return false
            
        }

    }
    
    
    // MARK: - --------- Other Local Function -------------
    
    func fetchServiceDetailsData() {
        
        const_ViewServiceDetail_H.constant = 0
        const_TblViewServiceDetail_H.constant = 0
        
        arrOfPests = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))

        var arrOfNoDevices = NSArray()
        var arrOfNoArea = NSArray()
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "WOServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
            
            arrOfNoDevices = getDataFromCoreDataBaseArray(strEntity: "WOServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
            
        }else{
            
            arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0"))
            
            arrOfNoArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String , strAddressAreaId: "" , strMobileAddressAreaId: "" , strWoStatus: "InComplete")
            
            arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: "" , strWoStatus: "InComplete" , strToCheckBlankAddressId: "No")
            
        }
        
        arrOfArea = arrOfNoArea
        
        let tempCount = NSMutableArray()
        
        if arrOfArea.count > 0 {
            
            for k in 0 ..< arrOfArea.count {
                
                let obj = arrOfNoArea[k] as! NSManagedObject

                // serviceAreaId
                
                if isWoCompleted(objWo: objWorkorderDetail) {
                    
                    let areaTemp = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@ && addressAreaId == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0" , "\(obj.value(forKey: "serviceAreaId") ?? "")"))
                    
                    if areaTemp.count > 0 {
                        
                        let objTemp = areaTemp[0] as! NSManagedObject
                        
                        let isInspected = objTemp.value(forKey: "isInspected") as! Bool
                        
                        if isInspected {
                            
                            tempCount.add(obj)
                            
                        }
                        
                    }
                    
                }else{
                    
                    let isInspected = obj.value(forKey: "isInspected") as! Bool
                    
                    if isInspected {
                        
                        tempCount.add(obj)
                        
                    }
                    
                }
                
            }
            
        }

        strNoOfArea = "\(arrOfNoArea.count)"
        strNoOfAreaInspected = "\(tempCount.count)"
        
        let tempDeviceTypeId = NSMutableArray()

        if arrOfNoDevices.count > 0 {
            
            for k in 0 ..< arrOfNoDevices.count {
                
                let obj = arrOfNoDevices[k] as! NSManagedObject
                
                if isWoCompleted(objWo: objWorkorderDetail) {
                    
                    let deviceTemp = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedDevice == %@ && deviceId == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0" , "\(obj.value(forKey: "serviceDeviceId") ?? "")"))
                    
                    if deviceTemp.count > 0 {
                        
                        let objTemp = deviceTemp[0] as! NSManagedObject
                        
                        let deviceTypeId = "\(objTemp.value(forKey: "devieTypeId") ?? "")"
                        
                        if tempDeviceTypeId.contains(deviceTypeId) {
                            
                            
                        }else{
                            
                            tempDeviceTypeId.add(deviceTypeId)
                            
                        }
                        
                    }
                    
                }else{
                    
                    let deviceTypeId = "\(obj.value(forKey: "devieTypeId") ?? "")"
                    
                    if tempDeviceTypeId.contains(deviceTypeId) {
                        
                        
                    }else{
                        
                        tempDeviceTypeId.add(deviceTypeId)
                        
                    }
                    
                }
                
            }
            
        }
        
        arrOfDeviceType = tempDeviceTypeId
        
        var hieght = (arrOfPests.count*112) + (arrOfDeviceType.count*112)
        
        if arrOfNoArea.count > 0 {
            
            hieght = hieght + 82
            
        }

        const_TblViewServiceDetail_H.constant = CGFloat(hieght + 150)
        
        const_ViewServiceDetail_H.constant = const_TblViewServiceDetail_H.constant + 50
        
        tblServiceDetails.reloadData()
        
    }
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSelectionOnEditOpportunity = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    func saveDataOnBack()  {
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = ["technicianComment",
                     "officeNotes"
        ]
        //strWorkOrderStatusFinal = "InCompleted"
        arrOfValues = [txtViewTechnicianComment.text,
                       txtViewOfficeNotes.text
        ]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            updateModifyDate()
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        }
        
    }
    
    func goToSendEmailScreen()
    {
        
        //let strTimeInFromDb = "\(objWorkorderDetail.value(forKey: "timeIn"))"
        
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let objSendMail = storyboardIpad.instantiateViewController(withIdentifier: "ServiceSendMailViewController") as? ServiceSendMailViewController
        
        var strValue = String()
        var strFromWheree = NSString()
        strFromWheree = "PestInvoice"
        
        if chkCustomerNotPresent == true
        {
            strValue = "no"
        }
        else
        {
            strValue = "yes"
        }
        objSendMail?.isCustomerPresent = strValue as NSString
        objSendMail?.fromWhere = strFromWheree as String
        //self.navigationController?.pushViewController(objSendMail!, animated: false)
        self.navigationController?.pushViewController(objSendMail!, animated: false)

        /*
         
         NSString *strTimeInFromDb = [NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"timeIn"]];
         if ([strTimeInFromDb isEqualToString:@""] || (strTimeInFromDb.length==0) || [strTimeInFromDb isEqualToString:@"(null)"]) {
         
         [self addPickerViewForTimeIn:@"sendmail"];
         
         } else {
         
         strGlobalWorkOrderStatus=@"InComplete";
         
         if (yesEditedSomething) {
         NSLog(@"Yes Edited Something In Db");
         
         [self updateModifyDate];
         
         }
         UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         ServiceSendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewController"];
         
         NSString *strValue;
         
         if (isCustomerNotPrsent) {
         strValue=@"no";
         } else {
         strValue=@"yes";
         }
         
         objSendMail.isCustomerPresent=strValue;
         [self.navigationController pushViewController:objSendMail animated:NO];
         */
    }
    func goToCreditCardScreen()
    {
        
        if isInternetAvailable() == true
        {
            let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
            let objCreditCardView = storyboardIpad.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
            
            var strValue = String()
            if chkCustomerNotPresent == true
            {
                strValue = "no"
            }
            else
            {
                strValue = "yes"
            }
            objCreditCardView?.isCustomerPresent = strValue as NSString
            objCreditCardView?.strAmount = txtFieldAmount.text
            objCreditCardView?.strGlobalWorkOrderId = strWoId as String
            objCreditCardView?.strTypeOfService = "service"
            objCreditCardView?.workOrderDetailNew = objWorkorderDetail
            objCreditCardView?.strFromWhichView = "PestFlow"
            objCreditCardView?.fromWhere = "PestInvoice"
            self.navigationController?.pushViewController(objCreditCardView!, animated: false)
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        /*
         NSString *strTimeInFromDb = [NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"timeIn"]];
         
         if ([strTimeInFromDb isEqualToString:@""] || (strTimeInFromDb.length==0) || [strTimeInFromDb isEqualToString:@"(null)"]) {
         
         [self addPickerViewForTimeIn:@"sendcreditcardview"];
         
         } else {
         
         if (yesEditedSomething) {
         NSLog(@"Yes Edited Something In Db");
         
         [self updateModifyDate];
         
         }
         
         Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
         NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
         if (netStatusWify1== NotReachable)
         {
         [global AlertMethod:@"ALert" :ErrorInternetMsgPayment];
         [DejalActivityView removeView];
         }
         else
         {
         UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
         CreditCardIntegration *objCreditCard=[storyBoard instantiateViewControllerWithIdentifier:@"CreditCardIntegration"];
         objCreditCard.strAmount=_txtAmountSingle.text;
         objCreditCard.strGlobalWorkOrderId=strWorkOrderId;
         objCreditCard.strTypeOfService=@"service";
         objCreditCard.workOrderDetailNew=_workOrderDetail;
         NSString *strValue;
         
         if (isCustomerNotPrsent) {
         strValue=@"no";
         } else {
         strValue=@"yes";
         }
         
         objCreditCard.isCustomerPresent=strValue;
         
         
         [self.navigationController pushViewController:objCreditCard animated:NO];
         }
         
         }
         */
    }
    
    func heightForLabel(text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    func getTermsCondition()
    {
        var strTremsnConditions = String()
        strTremsnConditions = ""
        let dictData = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        
        if dictData.value(forKey: "ServiceReportTermsAndConditions") is NSArray
        {
            let arrOfServiceJobDescriptionTemplate = dictData.value(forKey: "ServiceReportTermsAndConditions") as! NSArray
            
            for item in arrOfServiceJobDescriptionTemplate
            {
                let dict = item as! NSDictionary
                
                if ( "\((dict.value(forKey: "DepartmentId"))!)" == "\((objWorkorderDetail.value(forKey: "departmentId"))!)" )
                {
                    strTremsnConditions = "\(dict.value(forKey: "SR_TermsAndConditions") ?? "")"
                    break
                }
                
            }
        }

        txtViewServiceTerms.attributedText = htmlAttributedString(strHtmlString: strTremsnConditions)
        
        textViewDidChange(textView: txtViewServiceTerms)
    }
    
    
    func downloadCustomerSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgViewCustinerSignature.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            strUrl = strUrl + "//Documents/" + strImageName
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            
            let nsUrl = URL(string: strUrl)
            imgViewCustinerSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    func downloadTechnicianSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgViewTechnicianSignature.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            strUrl = strUrl + "//Documents/" + strImageName
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            let nsUrl = URL(string: strUrl)
            imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    func downloadTechnicianPreset(strImageName: String, stringUrl: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgViewTechnicianSignature.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            //strUrl = strUrl + "//Documents/" + strImageName
            strUrl = stringUrl
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            let nsUrl = URL(string: strUrl)
            
            imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    
    func methodForDisable()
    {
        if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed")
        {
            txtViewServiceJobDescription.isEditable = false
            txtViewOfficeNotes.isEditable = false
            txtViewServiceTerms.isEditable = false
            txtViewTechnicianComment.isEditable = false
            txtFieldAmount.isEnabled = false
            
            btnCash.isEnabled = false
            btnCheck.isEnabled = false
            btnCreditCard.isEnabled = false
            btnBill.isEnabled = false
            btnInvoice.isEnabled = false
            btnNoCharge.isEnabled = false
            btnAutochargeCustomer.isEnabled = false
            btnPaymentPending.isEnabled = false
            btnTechnicianSignature.isEnabled = false
            btnCustomerSignature.isEnabled = false
            
            txtFieldCheckNo.isEnabled = false
            txtFieldDrivingLicenseNo.isEnabled = false
            btnExpirationDate.isEnabled = false
            btnServiceJobTitle.isEnabled = false
            btnCustomerNotPresent.isEnabled = false
        }
        else
        {
            
            btnServiceJobTitle.isEnabled = true
            txtViewServiceTerms.isEditable = false
            btnCustomerNotPresent.isEnabled = true
            txtViewServiceJobDescription.isEditable = true
            txtViewOfficeNotes.isEditable = true
            txtViewTechnicianComment.isEditable = true
            txtFieldAmount.isEnabled = true
            
            btnCash.isEnabled = true
            btnCheck.isEnabled = true
            btnCreditCard.isEnabled = true
            btnBill.isEnabled = true
            btnInvoice.isEnabled = true
            btnNoCharge.isEnabled = true
            btnAutochargeCustomer.isEnabled = true
            btnPaymentPending.isEnabled = true
            btnTechnicianSignature.isEnabled = true
            btnCustomerSignature.isEnabled = true
            
            txtFieldCheckNo.isEnabled = true
            txtFieldDrivingLicenseNo.isEnabled = true
            btnExpirationDate.isEnabled = true
        }
        
        //For Preset
        
        let isPreSetSign = nsud.bool(forKey: "isPreSetSignService")

        var strSignUrl = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
        strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
        if ((isPreSetSign) && (strSignUrl.count>0) && !(strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed"))
        {
            btnTechnicianSignature.isEnabled = false
            isPreSetSignGlobal = true
        }
        else
        {
            isPreSetSignGlobal = false
            btnTechnicianSignature.isEnabled = true
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed")
            {
                btnTechnicianSignature.isEnabled = false
            }
        }
        
    }
    func cashPaymentMode()
    {
        btnCash.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        const_ViewPaymentMode_H.constant = 530 - 160
        
        lblAmount.isHidden = false
        txtFieldAmount.isHidden = false
        
        strGlobalPaymentMode = "Cash"
    }
    func checkPaymentMode()
    {
        btnCheck.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 160
        const_ViewPaymentMode_H.constant = 530
        
        lblAmount.isHidden = false
        txtFieldAmount.isHidden = false
        
        strGlobalPaymentMode = "Check"
        
        createDynamicServiceReportView()
        
    }
    func creditCardPaymentMode()
    {
        btnCreditCard.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        const_ViewPaymentMode_H.constant = 530 - 160
        
        lblAmount.isHidden = false
        txtFieldAmount.isHidden = false
        strGlobalPaymentMode = "CreditCard"
    }
    func noBillPaymentMode()
    {
        btnBill.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        const_ViewPaymentMode_H.constant = 530 - 160
        
        lblAmount.isHidden = true
        txtFieldAmount.isHidden = true
        strGlobalPaymentMode = "Bill"
    }
    func autoChargeCustomerPaymentMode()
    {
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        const_ViewPaymentMode_H.constant = 530 - 160
        
        lblAmount.isHidden = true
        txtFieldAmount.isHidden = true
        strGlobalPaymentMode = "AutoChargeCustomer"
    }
    func paymentPendingPaymentMode()
    {
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        const_ViewPaymentMode_H.constant = 530 - 160
        
        lblAmount.isHidden = true
        txtFieldAmount.isHidden = true
        strGlobalPaymentMode = "PaymentPending"
    }
    func noChargePaymentMode()
    {
        btnNoCharge.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        const_ViewPaymentMode_H.constant = 530 - 160
        
        lblAmount.isHidden = true
        txtFieldAmount.isHidden = true
        strGlobalPaymentMode = "NoCharge"
    }
    
    
    func assignValues() -> Void
    {
        
        strWorkOrderStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
        
        txtViewOfficeNotes.text = "\(objWorkorderDetail.value(forKey: "officeNotes") ?? "")"
        txtViewTechnicianComment.text = "\(objWorkorderDetail.value(forKey: "technicianComment") ?? "")"
        lblCustomerName.text = global.strFullName(for: objWorkorderDetail)

        let strCustomerNotPresent = "\(objWorkorderDetail.value(forKey: "isCustomerNotPresent") ?? "" )"
        if strCustomerNotPresent == "true" || strCustomerNotPresent == "1"
        {
            chkCustomerNotPresent = true
            
            btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            imgViewCustinerSignature.isHidden = true
            btnCustomerSignature.isHidden = true
            
            txtViewServiceJobDescription.isEditable = false
        }
        else
        {
            chkCustomerNotPresent = false
            btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            imgViewCustinerSignature.isHidden = false
            btnCustomerSignature.isHidden = false
            txtViewServiceJobDescription.isEditable = true
            
        }
        
        
        strCustomerSign = "\(objWorkorderDetail.value(forKey: "customerSignaturePath") ?? "")"
        strTechnicianSign = "\(objWorkorderDetail.value(forKey: "technicianSignaturePath") ?? "")"
        
        strAudioName = "\(objWorkorderDetail.value(forKey: "audioFilePath") ?? "")"
        
        
        lblAccountNo.text = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        lblWorkorderId.text = "\(objWorkorderDetail.value(forKey: "workorderId") ?? "")"
        
        btnPrimaryEmail.setTitle("\(objWorkorderDetail.value(forKey: "primaryEmail") ?? "")", for: .normal)
        
        btnSecondaryEmail.setTitle("\(objWorkorderDetail.value(forKey: "secondaryEmail") ?? "")", for: .normal)
        
        btnPrimaryPhone.setTitle("\(objWorkorderDetail.value(forKey: "primaryPhone") ?? "")", for: .normal)
        
        btnSecondaryPhone.setTitle("\(objWorkorderDetail.value(forKey: "secondaryPhone") ?? "")", for: .normal)
        
        var strServiceAddress = String()
        
        strServiceAddress = global.strCombinedAddressService(for: objWorkorderDetail)
        
        
        btnAddressCustomerInfo.setTitle(strServiceAddress, for: .normal)
        btnServiceAddress.setTitle(strServiceAddress, for: .normal)
        
        
        var strBillingAddress = String()
        
        strBillingAddress = global.strCombinedAddressBilling(for: objWorkorderDetail)
        
        btnBillingAddress.setTitle(strBillingAddress, for: .normal)
        
        
        lblTimeIn.text = "Time In: " + "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
        lblTimeOut.text = "Time Out: " + "\(objWorkorderDetail.value(forKey: "timeOut") ?? "")"
        
        
        
        
        //lblServiceJobTitle.text = ""  //serviceJobDescription //serviceJobDescriptionId
        
        let dictServiceJobDesc = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        if dictServiceJobDesc.count > 0
        {
            let arrOfServiceJobDescriptionTemplate = dictServiceJobDesc.value(forKey: "ServiceJobDescriptionTemplate") as! NSArray
            
            if arrOfServiceJobDescriptionTemplate.count > 0
            {
                for index in 0 ..< arrOfServiceJobDescriptionTemplate.count
                {
                    let dict = arrOfServiceJobDescriptionTemplate[index] as! NSDictionary
                    
                    if ("\(dict.value(forKey: "ServiceJobDescriptionId") ?? "")" == "\(objWorkorderDetail.value(forKey: "serviceJobDescriptionId") ?? "")")
                    {
                        
                        //lblServiceJobTitle.text = "\(dict.value(forKey: "Title") ?? "")"
                        
                        btnServiceJobTitle.setTitle("\(dict.value(forKey: "Title") ?? strSelectString)", for: .normal)
                        
                        dictOfServiceJobDescriptionSelected = dict
                        
                    }
                    
                }
            }
            
            
        }
        
        
        //txtViewServiceJobDescription.text = "\(objWorkorderDetail.value(forKey: "serviceJobDescription") ?? "")"
        txtViewServiceJobDescription.attributedText = htmlAttributedString(strHtmlString: "\(objWorkorderDetail.value(forKey: "serviceJobDescription") ?? "")")

        var total = Double()
        total = Double("\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))")! + Double("\((objWorkorderDetail.value(forKey: "previousBalance") ?? ""))")! + Double("\((objWorkorderDetail.value(forKey: "tax") ?? ""))")!
        
        lblChargesForCurrentServices.text = "\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))"
        
        lblTaxValue.text = "\((objWorkorderDetail.value(forKey: "tax") ?? ""))"
        
        lblTotalDueValue.text = "\(total)"
        
        strServiceReportType = "\((objWorkorderDetail.value(forKey: "serviceReportFormat") ?? ""))"
        
    }
    
    
    func setColorBorder(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    func setColorBorderForView(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        //testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanicaliPhone") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
                testController?.modalPresentationStyle = .fullScreen
        testController?.strWoId = "\(strWoId)"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
                testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strModuleType = "ServicePestFlow"
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func fetchDepartmentNameBySysName()
    {
        
        var dictSalesLeadMaster = NSDictionary()
        dictSalesLeadMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        
        var arrBranchMaster = NSArray()
        arrBranchMaster = dictSalesLeadMaster.value(forKey: "BranchMasters") as! NSArray
        
        let arrDeptVal = NSMutableArray()
        let arrDeptKey = NSMutableArray()
        
        var dictMasterKeyValue = NSDictionary()
        
        for item in arrBranchMaster
        {
            let dict = (item as AnyObject)as! NSDictionary
            
            var arrDepartments = NSArray()
            arrDepartments = dict.value(forKey: "Departments") as! NSArray
            
            for itemNew in arrDepartments
            {
                let dictNew = (itemNew as AnyObject)as! NSDictionary
                
                var strName = String()
                strName = "\(dictNew.value(forKey: "Name") ?? "")"
                
                var strSysName = String()
                strSysName = "\(dictNew.value(forKey: "SysName") ?? "")"
                
                arrDeptVal.add(strName)
                arrDeptKey.add(strSysName)
                
                
                
            }
            
            
            
        }
        
        dictMasterKeyValue = NSDictionary(objects: arrDeptVal as! [Any], forKeys: arrDeptKey as! [NSCopying])
        dictDepartmentNameFromSysName = dictMasterKeyValue
        print(dictMasterKeyValue)
        
    }
    func setDynamicData()
    {
        
        fetchDynamicFormData()
        
        var chkValuePresent = Bool()
        var count = Int()
        count = 0
        
        if dictJsonDynamicForm.count > 0 {
            
            
            chkValuePresent = false
            
            print(dictJsonDynamicForm)
            var dictMain = NSDictionary()
            dictMain = dictJsonDynamicForm
            
            let BtnMainView = UIButton()
            BtnMainView.frame = CGRect (x: 0, y: 50, width: UIScreen.main.bounds.width-10, height: 42)
            let strDeptName = "\(dictMain.value(forKey: "DepartmentSysName") ?? "")"
            if strDeptName.count > 0 {
                
                BtnMainView.setTitle("\(dictDepartmentNameFromSysName.value(forKey: strDeptName)!)", for: .normal)
                
            } else {
                
                BtnMainView.setTitle("N/A", for: .normal)
                BtnMainView.frame = CGRect (x: 0, y: 50, width: UIScreen.main.bounds.width-10, height: 0)
                
            }
            
            //BtnMainView.setTitle("\(dictDepartmentNameFromSysName.value(forKey: strDeptName)!)", for: .normal)
            
            BtnMainView.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
            BtnMainView.titleLabel?.textColor = UIColor.white
            BtnMainView.backgroundColor = UIColor.darkGray
            
            viewInspectionForm.addSubview(BtnMainView)
            
            
            var finalHeight = 0
            
            finalHeight = Int(BtnMainView.frame.maxY)
            
            //For Section
            var arrSections = NSArray()
            
            arrSections = dictMain.value(forKey: "Sections") as! NSArray
            
            var uiViewSection = UIView()
            
            var maxYViewSection = 0
            
            for index in 0 ..< arrSections.count
            {
                print("Item \(index)")
                
                chkValuePresent = false
                
                var heightSection = 0
                
                var heightControl = 0
                
                
                let dictSection = arrSections[index] as! NSDictionary
                
                uiViewSection = UIView()
                
                if index == 0
                {
                    uiViewSection.frame = CGRect(x: 0, y: Int(BtnMainView.frame.maxY) + 10, width: Int(UIScreen.main.bounds.width - 10) , height: 50)
                    
                }
                else
                {
                    uiViewSection.frame = CGRect(x: 0, y: CGFloat(maxYViewSection) + 10, width: UIScreen.main.bounds.width - 10 , height: 50)
                    
                }
                
                let labelTitleSection = UILabel()
                
                labelTitleSection.backgroundColor = UIColor.lightGray
                labelTitleSection.font = UIFont.boldSystemFont(ofSize: 16)
                
                labelTitleSection.frame = CGRect(x: 0, y: heightSection + 5, width: Int(UIScreen.main.bounds.width-10), height: 30)
                
                labelTitleSection.text = "\(dictSection.value(forKey: "Name") ?? "")"
                labelTitleSection.textAlignment = NSTextAlignment.center
                
                //uiViewSection.backgroundColor = UIColor.purple
                
                heightSection = Int(labelTitleSection.frame.maxY)
                
                uiViewSection.addSubview(labelTitleSection)
                
                
                let arrControls = dictSection.value(forKey: "Controls") as! NSArray
                
                
                for indexControls in 0 ..< arrControls.count
                {
                    let dictControl = arrControls[indexControls] as! NSDictionary
                    
                    if "\(dictControl.value(forKey: "Value") ?? "")".count>0
                    {
                        chkValuePresent = true
                        
                        let labelTitleControl = UILabel()
                        let labelTitleControlValue = UILabel()
                        
                        labelTitleControl.font = UIFont.boldSystemFont(ofSize: 14)
                        labelTitleControlValue.font = UIFont.systemFont(ofSize: 12)
                        
                        //labelTitleControl.backgroundColor = UIColor.lightGray
                        // labelTitleControlValue.backgroundColor = UIColor.darkGray
                        
                        if indexControls == 0
                        {
                            labelTitleControl.frame = CGRect(x: 5, y: heightSection + 5, width: Int(UIScreen.main.bounds.width-10-10), height: 30)
                            
                        }
                        else
                        {
                            if heightControl == 0
                            {
                                labelTitleControl.frame = CGRect(x: 5, y: 30 + 5, width: Int(UIScreen.main.bounds.width-10-10), height: 30)
                            }
                            else
                            {
                                labelTitleControl.frame = CGRect(x: 5, y: heightControl + 5, width: Int(UIScreen.main.bounds.width-10-10), height: 30)
                            }
                            
                        }
                        
                        
                        labelTitleControlValue.frame = CGRect(x: 10, y: Int(labelTitleControl.frame.maxY + 5), width: Int(UIScreen.main.bounds.width-10-10), height: 30)
                        
                        
                        
                        labelTitleControl.text = "\(dictControl.value(forKey: "Label") ?? "")" + ":"
                        
                        labelTitleControlValue.text = "\(dictControl.value(forKey: "Value") ?? "")"
                        
                        //Height Calculation Title
                        
                        let heightForLabelControlTitle = self.heightForLabel(text: labelTitleControl.text ?? "", font: labelTitleControl.font, width: labelTitleControl.frame.size.width)
                        
                        labelTitleControl.frame = CGRect(x: labelTitleControl.frame.origin.x, y: labelTitleControl.frame.origin.y, width: UIScreen.main.bounds.width-10, height: heightForLabelControlTitle)
                        
                        labelTitleControl.numberOfLines = 500
                        
                        
                        //Height Calculation Value
                        
                        let heightForLabelControlValue = self.heightForLabel(text: labelTitleControlValue.text ?? "", font: labelTitleControlValue.font, width: labelTitleControlValue.frame.size.width)
                        
                        labelTitleControlValue.frame = CGRect(x: labelTitleControlValue.frame.origin.x, y: labelTitleControl.frame.maxY + 10, width: UIScreen.main.bounds.width-10, height: heightForLabelControlValue)
                        
                        labelTitleControlValue.numberOfLines = 500
                        
                        
                        //labelTitleControl.textAlignment = NSTextAlignment.center
                        
                        heightControl = Int(labelTitleControlValue.frame.maxY)//Int(labelTitleControl.frame.maxY) + Int(labelTitleControlValue.frame.maxY)
                        
                        uiViewSection.addSubview(labelTitleControl)
                        uiViewSection.addSubview(labelTitleControlValue)
                        
                        
                    }
                    else
                    {
                        continue
                        
                    }
                    
                }
                heightSection = heightSection + heightControl
                
                uiViewSection.frame = CGRect(x: 0, y: uiViewSection.frame.origin.y, width: UIScreen.main.bounds.width - 10 , height: CGFloat(heightSection + 5))
                
                maxYViewSection =  Int(uiViewSection.frame.maxY)
                
                finalHeight = finalHeight + heightSection
                
                viewInspectionForm.addSubview(uiViewSection)
                
                if chkValuePresent == false
                {
                    labelTitleSection.isHidden = true
                    count = count + 1
                }
                
            }
            
            if count == arrSections.count
            {
                const_ViewInspection_H.constant = 0
                viewInspectionForm.isHidden = true
            }
            else
            {
                const_ViewInspection_H.constant = CGFloat(finalHeight+Int(BtnMainView.frame.maxY) - count * 55)
                viewInspectionForm.isHidden = false
            }
            
        }else{
            
            const_ViewInspection_H.constant = 0
            viewInspectionForm.isHidden = true
            
        }
    
        
    }
    
    
    @objc func actionOnServiceReportType(sender: UIButton) {
        
        let dictData = arrOfServiceReportType[sender.tag] as! NSDictionary

        //sender.tag
        if(sender.currentImage == UIImage(named: "RadioButton-Unselected.png"))
        {
            
            sender.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            
            strServiceReportType = "\(dictData.value(forKey: "Value") ?? "")"
            
        }
        else
        {
            
            sender.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            
            if "\(dictData.value(forKey: "Value") ?? "")" == strServiceReportType {
                
                strServiceReportType = ""
                
            }
            
        }
        
        createDynamicServiceReportView()
        
    }
    
    func createDynamicServiceReportView() {
        
        viewServiceReportType.backgroundColor = UIColor.white
        
        for view in self.viewServiceReportType.subviews {
            
            view.removeFromSuperview()
            
        }
        
        var yAxix = 0.0

        arrOfServiceReportType = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "ServiceReportFormats", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "BranchId")

        for (index, item) in arrOfServiceReportType.enumerated() {
            
            if index == 0 {
                
                // To Put Heading
                
                let lblHeading = UILabel()
                lblHeading.text = "Service Report Type"
                lblHeading.font = UIFont.boldSystemFont(ofSize: 17)
                lblHeading.numberOfLines = 0
                lblHeading.frame = CGRect(x: 0, y: Int(yAxix), width: Int(UIScreen.main.bounds.width) - 40, height: 40)
                    
                self.viewServiceReportType.addSubview(lblHeading)

                yAxix = Double(CGFloat(yAxix) + 50)
                
            }

            
            
            let btnCheckBox = UIButton()
            btnCheckBox.setTitleColor(UIColor.clear, for: .normal)
            btnCheckBox.frame = CGRect(x: 0, y: Int(yAxix), width: 30, height: 30)
            btnCheckBox.setTitle("", for: .normal)
            
            if("\((item as! NSDictionary).value(forKey: "Value") ?? "")" == strServiceReportType){
                
                btnCheckBox.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                
            }else{
                
                btnCheckBox.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                
            }
            
            btnCheckBox.tag = index
            btnCheckBox.addTarget(self, action: #selector(actionOnServiceReportType), for: .touchUpInside)
            
            if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Reset" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "reset" {
                btnCheckBox.isUserInteractionEnabled = false
            }else{
                btnCheckBox.isUserInteractionEnabled = true
            }
            
            let lblOption = UILabel()
            lblOption.text = "\((item as! NSDictionary).value(forKey: "Text") ?? "")"
            lblOption.font = UIFont.systemFont(ofSize: 14)
            lblOption.numberOfLines = 0
            lblOption.frame = CGRect(x: 38, y: Int(yAxix), width: Int(UIScreen.main.bounds.width) - 40, height:  Int(WebService().estimatedHeightOfLabelNew(text: lblOption.text!, view: self.viewServiceReportType,fontSize: 14)))
            
            self.viewServiceReportType.addSubview(btnCheckBox)
            self.viewServiceReportType.addSubview(lblOption)
            
            let hghtTemp = lblOption.frame.height < 30 ? 30 : lblOption.frame.height
            
            yAxix = Double(CGFloat(yAxix) +  hghtTemp  + 10)
            
        }
        
        self.const_ServiceReportType_H.constant = CGFloat(yAxix)
        
    }
    
}
// MARK: --------------------- DatePicker  Delegate --------------

extension InvoiceDetailVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        yesEditedSomething = true
        btnExpirationDate.setTitle(strDate, for: .normal)
        //strFromTimeNonBillable = strDate
        
        
        
    }
    
    
}

// MARK:- -----------------------------------Selction From Table view On Pop Up -----------------------------------

extension InvoiceDetailVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(dictData: NSDictionary, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 48 {
            
            if dictData["Title"] as? String  == strSelectString {
                
                dictOfServiceJobDescriptionSelected = NSDictionary ()
                
                //txtViewServiceJobDescription.text = ""
                txtViewServiceJobDescription.attributedText = htmlAttributedString(strHtmlString: "")

                btnServiceJobTitle.setTitle(strSelectString, for: .normal)
                
            }else{
                
                dictOfServiceJobDescriptionSelected = dictData
                
                //txtViewServiceJobDescription.text = dictData["ServiceJobDescription"] as? String
                txtViewServiceJobDescription.attributedText = htmlAttributedString(strHtmlString: (dictData["ServiceJobDescription"] as? String)!)

                btnServiceJobTitle.setTitle(dictData["Title"] as? String, for: .normal)

            }
            
        }
        
    }
    
}
// MARK: --------------------- ImageView Delegate --------------


extension InvoiceDetailVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            if chkFrontImage
            {
                strCheckFrontImage = "\\Documents\\UploadImages\\Img"  + "CheckFrontImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckFrontImage, image: pickedImage)
            }
            else
            {
                strCheckBackImage = "\\Documents\\UploadImages\\Img"  + "CheckBackImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckBackImage, image: pickedImage)
            }
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
}
// MARK: --------------------- Text Field Delegate --------------

extension InvoiceDetailVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        yesEditedSomething = true
        
        if ( textField == txtFieldDrivingLicenseNo || textField == txtFieldCheckNo  )
        {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
            
        }
            
        else if ( textField == txtFieldAmount  )
        {
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            
        }
        else
        {
            
            return true
            
        }
    }
    
}
// MARK: --------------------- Text View Delegate --------------

extension InvoiceDetailVC : UITextViewDelegate
{
    
    private func textViewDidChange(textView: UITextView)
    {
        const_viewServiceTerms_H.constant = 50
        
        if textView == txtViewServiceTerms
        {
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            //textView.frame = newFrame;
            const_viewServiceTerms_H.constant = newFrame.size.height
            
            /* if  const_viewServiceTerms_H.constant>400
             {
             const_viewServiceTerms_H.constant = 400
             }*/
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        yesEditedSomething = true
        
        return true
    }
    
}

// MARK: - ----------------Cell
// MARK: -

class ServiceAreaDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblNoOfArea: UILabel!
    @IBOutlet weak var lblNoOfInspectedArea: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}

class ServiceDeviceDetailCell: UITableViewCell {
    
    
    @IBOutlet weak var lblDeviceTypeName: UILabel!
    @IBOutlet weak var lblDeviceNoOfInspected: UILabel!
    @IBOutlet weak var lblActivityInspected: UILabel!
    @IBOutlet weak var lblSkipped: UILabel!
    @IBOutlet weak var lblReplaced: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}

class ServicePestDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblPestName: UILabel!
    @IBOutlet weak var lblAreaName: UILabel!
    @IBOutlet weak var lblFinding: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension InvoiceDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
      return 50
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
            let strHeader = "Area Summary"
            
            let vw = UIView(frame: CGRect(x: 8, y: 0, width: tableView.frame.width-16, height: 50))
            
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let lblHeader = UILabel()
            lblHeader.frame = vw.frame
            lblHeader.text = strHeader
            lblHeader.textColor = UIColor.darkGray
            lblHeader.font = UIFont.systemFont(ofSize: 15)
            vw.addSubview(lblHeader)
            
            return vw
            
        } else if section == 1  {
            
            let strHeader = "Device Inspection Summary"
            
            let vw = UIView(frame: CGRect(x: 8, y: 0, width: tableView.frame.width-16, height: 50))
            
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let lblHeader = UILabel()
            lblHeader.frame = vw.frame
            lblHeader.text = strHeader
            lblHeader.textColor = UIColor.darkGray
            lblHeader.font = UIFont.systemFont(ofSize: 15)
            vw.addSubview(lblHeader)
            
            return vw
            
        }else {
            
            let strHeader = "Pest Summary"
            
            let vw = UIView(frame: CGRect(x: 8, y: 0, width: tableView.frame.width-16, height: 50))
            
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let lblHeader = UILabel()
            lblHeader.frame = vw.frame
            lblHeader.text = strHeader
            lblHeader.textColor = UIColor.darkGray
            lblHeader.font = UIFont.systemFont(ofSize: 15)
            vw.addSubview(lblHeader)
            
            return vw
            
        }
        
    }
    
    // set view for footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            if arrOfArea.count > 0 {
                
                return 1

            }else{
                
                return 0

            }
            
        } else if section == 1 {
            
            return arrOfDeviceType.count
            
        } else {
            
            return arrOfPests.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tblServiceDetails.dequeueReusableCell(withIdentifier: "ServiceAreaDetailCell", for: indexPath as IndexPath) as! ServiceAreaDetailCell
            
            cell.lblNoOfArea.text = "# Of Area: \(strNoOfArea)"
            cell.lblNoOfInspectedArea.text = "# Of Area Inspected: \(strNoOfAreaInspected)"
            
            return cell
            
        } else if indexPath.section == 1 {
            
            let cell = tblServiceDetails.dequeueReusableCell(withIdentifier: "ServiceDeviceDetailCell", for: indexPath as IndexPath) as! ServiceDeviceDetailCell
            
            let strDeviceTypeId = "\(arrOfDeviceType[indexPath.row])"
            
            var deviceSysNameLocal = String()

            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "deviceTypeMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                if array.count > 0 {
                    
                    for k1 in 0 ..< array.count {
                        
                        let dictData = array[k1] as! NSDictionary
                        
                        // to add which target groups to be shown for product
                        
                        if strDeviceTypeId == "\(dictData.value(forKey: "DeviceTypeId") ?? "")"  {
                            
                            deviceSysNameLocal = "\(dictData.value(forKey: "DeviceTypeName") ?? "")"
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
            }
            
            
            let arrOfDevices =  getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "devieTypeId == %@ && companyKey == %@ && isDeletedDevice == %@ && serviceAddressId == %@",strDeviceTypeId,Global().getCompanyKey(),"0","\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")"))

            //let arrOfDevicesOnWo =  getDataFromCoreDataBaseArray(strEntity: "WOServiceDevices", predicate: NSPredicate(format: "workOrderId == %@",strWoId))

            let tempCountDeviceInspection = NSMutableArray()
            let tempCountDeviceReplaced = NSMutableArray()
            let tempCountDeviceActivity = NSMutableArray()

            if arrOfDevices.count > 0 {
                
                for k in 0 ..< arrOfDevices.count {
                    
                    let obj = arrOfDevices[k] as! NSManagedObject
                    
                    var strDeviceIdLocal = "\(obj.value(forKey: "deviceId") ?? "")"
                    
                    if strDeviceIdLocal.count == 0 || strDeviceIdLocal == "0" {
                        
                        strDeviceIdLocal = "\(obj.value(forKey: "mobileDeviceId") ?? "")"
                        
                    }
                    
                    let arrOfDevicesInspection =  getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "mobileServiceDeviceId == %@ && companyKey == %@ && workOrderId == %@",strDeviceIdLocal,Global().getCompanyKey(),strWoId))

                    if arrOfDevicesInspection.count > 0 {
                        
                        tempCountDeviceInspection.add(strDeviceIdLocal)
                        
                        // deviceReplacementReasonId
                        
                        let obj = arrOfDevicesInspection[0] as! NSManagedObject
                        
                        let strDeviceReplacementReasonId = "\(obj.value(forKey: "deviceReplacementReasonId") ?? "")"
                        
                        if strDeviceReplacementReasonId.count > 0 {
                            
                            tempCountDeviceReplaced.add(strDeviceIdLocal)
                            
                        }
                        
                        let isActivity = obj.value(forKey: "activity") as! Bool
                        
                        if isActivity {
                            
                            tempCountDeviceActivity.add(strDeviceIdLocal)
                            
                        }
                        
                    }
                    
                    
                }
                
            }
            
            cell.lblDeviceTypeName.text = deviceSysNameLocal
            
            cell.lblDeviceNoOfInspected.text = "# Inspected: \(tempCountDeviceInspection.count) of " + "\(arrOfDevices.count)"
            
            let skipped = arrOfDevices.count - tempCountDeviceInspection.count
            
            cell.lblSkipped.text = "# Skipped: \(skipped) of " + "\(arrOfDevices.count)"
            
            cell.lblReplaced.text = "# Replaced: \(tempCountDeviceReplaced.count) of " + "\(arrOfDevices.count)"

            cell.lblActivityInspected.text = "Inspected/Activity: \(tempCountDeviceActivity.count) of " + "\(arrOfDevices.count)"

            return cell
            
        } else {
            
            let cell = tblServiceDetails.dequeueReusableCell(withIdentifier: "ServicePestDetailCell", for: indexPath as IndexPath) as! ServicePestDetailCell
            
            let objArea = arrOfPests[indexPath.row] as! NSManagedObject
            
            cell.lblPestName.text = "\(objArea.value(forKey: "targetName") ?? "N/A")"
            cell.lblAreaName.text = "\(objArea.value(forKey: "parentAreaName") ?? "N/A")"

            if (cell.lblAreaName.text?.count)! < 1 {
                
                cell.lblAreaName.text = "Area"
                
            }
            
            let pestStatus = "\(objArea.value(forKey: "pestStatus") ?? "")"
            var strSubTitle = String()
            
            if pestStatus == "1" {
                
                strSubTitle = "\(objArea.value(forKey: "quantity") ?? "")  Captured"
                
            } else if pestStatus == "2" {
                
                strSubTitle = "\(objArea.value(forKey: "quantity") ?? "")  Sighted"
                
            } else{
                
                strSubTitle = "Evidence: \(objArea.value(forKey: "evidence") ?? "")"
                
            }
            
            cell.lblFinding.text = strSubTitle
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            return 82
            
        } else if indexPath.section == 1 {
            
            return 112
            
        } else {
            
            return 112
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    
    
}
