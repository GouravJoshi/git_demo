//
//  PreviewImageVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/14/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class PreviewImageVC: UIViewController,UIScrollViewDelegate {

    @objc var img = UIImage()
    @objc var strimgUrl = String()

    @IBOutlet weak var imageViewLarge: UIImageView!

    @IBOutlet weak var scrollViewImage: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     

        imageViewLarge.image = img
        imageViewLarge.contentMode = .scaleAspectFit
        scrollViewImage.contentSize = imageViewLarge.bounds.size
        scrollViewImage.minimumZoomScale = 1.0
        scrollViewImage.maximumZoomScale = 5.0
        scrollViewImage.zoomScale = 0.0
        
        if strimgUrl != "" {
            imageViewLarge.setImageWith(URL(string: strimgUrl), placeholderImage: UIImage(named: "no_image.jpg"), options: SDWebImageOptions(rawValue: 1), completed: { image, error, type, url in
                if(image != nil){
                  
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Image not found.", viewcontrol: self)

                }
            }, usingActivityIndicatorStyle: .gray)
        }
        
    
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageViewLarge
    }
    
    
    @IBAction func actionOnBack(_ sender: Any) {
        
        dismiss(animated: false)

    }
}
