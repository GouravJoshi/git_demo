//
//  AddServiceListVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class AddServiceListVC: UIViewController, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnCancelSearch: UIButton!

    //MARK: -Varialbes
    var arrOfCategories = [[String: Any]]()
    var strWoId = String()
    var strCompanyKey = ""
    var strUserName = ""
    
    var arrOfSearchList = [[String: Any]]()
    var isSeaching = Bool()
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblView.delegate = self
        self.tblView.dataSource = self
        txtSearch.delegate = self
        fetchServiceDetail()
        tblView.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions
    @IBAction func btnCancelSearchAction(_ sender: Any) {
        isSeaching = false
        arrOfSearchList.removeAll()
        txtSearch.text = ""
        btnCancelSearch.isHidden = true
        tblView.reloadData()
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        
    }
    
    //MARK: - Custom Functions
    func fetchServiceDetail()
    {
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        if  dictMaster.value(forKey: "Categories") is NSArray
        {
            let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
            for itemCategory in arrCategory
            {
                let dictCategory = itemCategory as! NSDictionary
                self.arrOfCategories.append(dictCategory as! [String : Any])
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if range.location == 0  && (string == " ") {
            btnCancelSearch.isHidden = true
            return false
        }
        else{
            if searchedText == ""{
                self.arrOfSearchList.removeAll()
                self.isSeaching = false
                self.tblView.reloadData()
                btnCancelSearch.isHidden = true
                
            }
            else  if searchedText.count > 0 {
                self.arrOfSearchList.removeAll()
                btnCancelSearch.isHidden = false
                arrOfSearchList = arrOfCategories.filter({
                    // this is where you determine whether to include the specific element, $0
                    return ($0["Name"]! as! String).lowercased().contains(searchedText.lowercased()) //&& ($0["compType"]! as! String).contains(txtSearch.text!)
                    // or whatever search method you're using instead
                })
                self.isSeaching = true
                tblView.reloadData()
                print(arrOfSearchList)
                
            } else {
                self.arrOfSearchList.removeAll()
                self.isSeaching = false
                btnCancelSearch.isHidden = true
                self.tblView.reloadData()
                
            }
            return true
        }
    }
    
}

extension AddServiceListVC : ServiceCategoryAddedDismissTrue{
    func dismissView(isTrue: Bool) {
        print("called")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            self.navigationController?.popViewController(animated: false)
        }
    }
}
extension AddServiceListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSeaching == true {
            return arrOfSearchList.count
        }
        else{
            return arrOfCategories.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddServiceListCell") as! AddServiceListCell
        
        cell.selectionStyle = .none
        if self.isSeaching == true {
            
            cell.lblServiceName.text = arrOfSearchList[indexPath.row]["Name"] as? String ?? ""//"1A Pest"
            
        }
        else{
            
            cell.lblServiceName.text = arrOfCategories[indexPath.row]["Name"] as? String ?? ""//"1A Pest"
        }
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        
        let vc = UIStoryboard.init(name: "ServicePestNewFlow" , bundle: Bundle.main).instantiateViewController(withIdentifier: "ServiceCategoryVC") as? ServiceCategoryVC
        
        if self.isSeaching == true {
            let arrOfServices = arrOfSearchList[indexPath.row]["Services"] as? [[String: Any]]
            if arrOfServices!.count > 0
            {
                vc!.strServiceCategoryName = arrOfSearchList[indexPath.row]["Name"] as? String ?? ""
                vc!.strServiceCategoryId = String(describing: arrOfSearchList[indexPath.row]["CategoryId"] as? NSNumber)
                vc!.strServiceCategorySysName = arrOfSearchList[indexPath.row]["SysName"] as? String ?? ""
                vc!.strWoId = strWoId
                vc!.strUserName = strUserName
                vc!.strCompanyKey = strCompanyKey
                vc!.delegate = self
                vc!.arrOfServices = arrOfServices!
                self.navigationController?.pushViewController(vc!, animated: false)
            }
            else{
                self.view.makeToast("No services Available.")
            }
        }
        else{
            let arrOfServices = arrOfCategories[indexPath.row]["Services"] as? [[String: Any]]
            if arrOfServices!.count > 0
            {
                vc!.strServiceCategoryName = arrOfCategories[indexPath.row]["Name"] as? String ?? ""
                vc!.strServiceCategoryId = String(describing: arrOfCategories[indexPath.row]["CategoryId"] as? NSNumber)
                vc!.strServiceCategorySysName = arrOfCategories[indexPath.row]["SysName"] as? String ?? ""
                vc!.strWoId = strWoId
                vc!.strUserName = strUserName
                vc!.strCompanyKey = strCompanyKey
                vc!.delegate = self
                vc!.arrOfServices = arrOfServices!
                self.navigationController?.pushViewController(vc!, animated: false)
            }
            else{
                self.view.makeToast("No services Available.")
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
