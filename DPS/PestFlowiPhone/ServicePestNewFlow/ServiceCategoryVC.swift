//
//  ServiceCategoryVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 24/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//


import UIKit

protocol ServiceCategoryAddedDismissTrue: class {
    
    func dismissView(isTrue: Bool)
}

class ServiceCategoryVC: UIViewController {
    
    
    //MARK: -IBOutlets
    @IBOutlet weak var tblView: UITableView!
    
    
    //MARK: -Variables
    var strWoId = String()
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceCategoryId = String()
    var strServiceCategoryName = String()
    var strServiceCategorySysName = String()
    
    //For normal
    var arrOfServices = [[String: Any]]()
    var arrOfCheckedServices = [Int]()
    var arrOfEditedPrice = [String]()
    var arrOfEditedQuantity = [String]()
    var arrOfTaxable = [Int]()
    var arrOfSelectedServices = [[String: Any]]()
    let global = Global()

    
    @objc var objServiceCategoryDetail = NSMutableArray()//[NSManagedObject()]

    weak var delegate: ServiceCategoryAddedDismissTrue?
    
    //MARK: -View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.delegate = self
        tblView.dataSource = self
        
        self.tblView.reloadData()
        self.createIndexFilled()
        getDataOfServices()
    }

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK: - Custom functions
    
    func getDataOfServices(){
        objServiceCategoryDetail.removeAllObjects()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@", strWoId))
    
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objServiceCategoryDetail.add(dict)
            }
        }
        setSelectedIfServiceAlreadyThere()
    }
    
    func setSelectedIfServiceAlreadyThere(){
        for i in 0..<objServiceCategoryDetail.count{
            let dict = objServiceCategoryDetail[i] as! NSManagedObject
            for p in 0..<arrOfServices.count{
                print("service id \(dict.value(forKey: "serviceId") as! String)")
                print("CoreServiceMasterId id \(String(describing: arrOfServices[p]["CoreServiceMasterId"] as! NSNumber))")
                
                if dict.value(forKey: "serviceId") as! String == String(describing: arrOfServices[p]["CoreServiceMasterId"] as! NSNumber)
                {
                    
                    let strServiceId = String(describing: arrOfServices[p]["CoreServiceMasterId"] as! NSNumber)
                    let strServiceName = arrOfServices[p]["Name"] as? String
                    let strServiceSysName = arrOfServices[p]["SysName"] as? String
                    let strServiceQuantity = dict.value(forKey: "serviceQuantity") as! String
                    let strServicePrice = dict.value(forKey: "servicePrice") as! String
                    let strTaxable = dict.value(forKey: "taxable") as! String

                    arrOfCheckedServices.remove(at: p)
                    arrOfCheckedServices.insert(1, at: p)
                    
                    arrOfTaxable.remove(at: p)
                    arrOfTaxable.insert(Int(strTaxable) ?? 0, at: p)

                    arrOfEditedPrice.remove(at: p)
                    arrOfEditedPrice.insert(strServicePrice, at: p)
                    
                    arrOfEditedQuantity.remove(at: p)
                    arrOfEditedQuantity.insert(strServiceQuantity, at: p)
                    
                    let dict = ["serviceId": strServiceId,
                                "serviceName": strServiceName!,
                                "serviceSysName": strServiceSysName!,
                                "serviceQuantity" : strServiceQuantity,
                                "servicePrice": strServicePrice,
                                "serviceTaxable": strTaxable] as [String: Any]
                    
                    arrOfSelectedServices.append(dict)
                    
                    tblView.reloadData()

                    
                }

            }
        }
    }
    
    func createIndexFilled(){
        for i in 0..<arrOfServices.count{
            arrOfCheckedServices.insert(0, at: i)
            arrOfTaxable.insert(0, at: i)
            arrOfEditedPrice.insert(String(describing: arrOfServices[i]["Price"] as? NSNumber ?? 0.00), at: i)
            arrOfEditedQuantity.insert("1", at: i)
        }
    }
    
}


extension ServiceCategoryVC : UITextFieldDelegate{
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.tag != -1 {
            
            let index = IndexPath(item: textField.tag, section: 0)
            let cell = self.tblView.cellForRow(at: index) as! ServiceCategoryCell
            
            if ( textField == cell.txtCategoryPrice)
            {
                let chk = decimalValidation(textField: textField, range: range, string: string)
                if chk == false{
                    self.view.makeToast("Please enter valid entity")
                }
                return chk
            }
            if textField == cell.txtCategoryQuantity  {
                
                let isValidd = txtFieldValidation(textField: cell.txtCategoryQuantity, string: string, returnOnly: "NUMBER", limitValue: 2)
                
                return isValidd
            }
            return true
        }
        
        else
        {
            return true
        }
    }
    
    //MARK: - UITextField Delegate
    func textFieldDidEndEditing(_ textField: UITextField) { //delegate method
        print("txt")
        
        let index = IndexPath(item: textField.tag, section: 0)
        let cell = self.tblView.cellForRow(at: index) as! ServiceCategoryCell
        
        if textField == cell.txtCategoryQuantity{
            
            let strServiceQauntity = cell.txtCategoryQuantity.text
            
            arrOfEditedQuantity.remove(at: textField.tag)
            arrOfEditedQuantity.insert(strServiceQauntity!, at: textField.tag)
            
            if arrOfCheckedServices[textField.tag] == 1{
                
                let strServiceId = String(describing: arrOfServices[textField.tag]["CoreServiceMasterId"] as! NSNumber)
                let strServiceName = arrOfServices[textField.tag]["Name"] as? String
                let strServiceSysName = arrOfServices[textField.tag]["SysName"] as? String
                let strServiceQuantity = arrOfEditedQuantity[textField.tag]
                let strServicePrice = arrOfEditedPrice[textField.tag]
                let strTaxable = String(arrOfTaxable[textField.tag])

                for i in 0..<arrOfSelectedServices.count{
                    if strServiceId == arrOfSelectedServices[i]["serviceId"] as! String{
                        
                        arrOfSelectedServices.remove(at: i)
                        
                        let dict = ["serviceId": strServiceId,
                                    "serviceName": strServiceName!,
                                    "serviceSysName": strServiceSysName!,
                                    "serviceQuantity" : strServiceQuantity,
                                    "servicePrice": strServicePrice,
                                    "serviceTaxable": strTaxable]as [String: Any]
                        
                        arrOfSelectedServices.insert(dict, at: i)
                        break
                    }
                    
                }
                tblView.reloadData()
            }
        }
        else if textField == cell.txtCategoryPrice{
            
            let strServicePrice = cell.txtCategoryPrice.text
            arrOfEditedPrice.remove(at: textField.tag)
            arrOfEditedPrice.insert(strServicePrice!, at: textField.tag)
            
            if arrOfCheckedServices[textField.tag] == 1{
                
                let strServiceId = String(describing: arrOfServices[textField.tag]["CoreServiceMasterId"] as! NSNumber)
                let strServiceName = arrOfServices[textField.tag]["Name"] as? String
                let strServiceSysName = arrOfServices[textField.tag]["SysName"] as? String
                let strServiceQuantity = arrOfEditedQuantity[textField.tag]
                let strServicePrice = arrOfEditedPrice[textField.tag]
                let strTaxable = String(arrOfTaxable[textField.tag])
                
                for i in 0..<arrOfSelectedServices.count{
                    if strServiceId == arrOfSelectedServices[i]["serviceId"] as! String{
                        
                        arrOfSelectedServices.remove(at: i)
                        
                        let dict = ["serviceId": strServiceId,
                                    "serviceName": strServiceName!,
                                    "serviceSysName": strServiceSysName!,
                                    "serviceQuantity" : strServiceQuantity,
                                    "servicePrice": strServicePrice,
                                    "serviceTaxable": strTaxable] as [String: Any]
                        
                        arrOfSelectedServices.insert(dict, at: i)
                        break
                    }
                    
                }
                tblView.reloadData()
            }
        }
        
    }
}


extension ServiceCategoryVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCategoryCell") as! ServiceCategoryCell
        
        cell.selectionStyle = .none
        
        cell.lblCategoryName.text = arrOfServices[indexPath.row]["Name"] as? String ?? ""//"1A Pest"
        
//        cell.callbackForEditedPrice = { (tempArr) in
//
//            self.arrOfEditedPrice.removeAll()
//            self.arrOfEditedPrice = tempArr
//            self.tblView.reloadData()
//        }
//
//        cell.callbackForEditedQuantity = { (temArr) in
//
//            self.arrOfEditedQuantity.removeAll()
//            self.arrOfEditedQuantity = temArr
//            self.tblView.reloadData()
//        }
        
        cell.txtCategoryPrice.delegate = self
        cell.txtCategoryQuantity.delegate = self
        
        cell.txtCategoryPrice.tag = indexPath.row
        cell.txtCategoryQuantity.tag = indexPath.row
                
        cell.txtCategoryQuantity.text = arrOfEditedQuantity[indexPath.row]
        
        let strPrice = arrOfEditedPrice[indexPath.row]
        
        if strPrice != ""{
            cell.txtCategoryPrice.text = strPrice
        }
        else{
            cell.txtCategoryPrice.text = "0.00"
        }
        
        if arrOfCheckedServices[indexPath.row] == 0{
            cell.btnCheckbox.setImage(UIImage.init(named: "check_box_1.png"), for: .normal)
        }
        else if arrOfCheckedServices[indexPath.row] == 1{
            cell.btnCheckbox.setImage(UIImage.init(named: "check_box_2.png"), for: .normal)
            
        }
        
        if arrOfTaxable[indexPath.row] == 0{
            cell.btnTaxable.setImage(UIImage.init(named: "check_box_1.png"), for: .normal)
        }
        else if arrOfTaxable[indexPath.row] == 1{
            cell.btnTaxable.setImage(UIImage.init(named: "check_box_2.png"), for: .normal)
            
        }
        
        cell.btnCheckbox.tag = indexPath.row
        cell.btnCheckbox.addTarget(self, action: #selector(btnCheckboxAction), for: .touchUpInside)
        
        cell.btnTaxable.tag = indexPath.row
        cell.btnTaxable.addTarget(self, action: #selector(btnTaxableAction), for: .touchUpInside)
        return  cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    @objc func btnTaxableAction(sender: UIButton!){
        
        if arrOfTaxable[sender.tag] == 0{
            
            arrOfTaxable.remove(at: sender.tag)
            arrOfTaxable.insert(1, at: sender.tag)
            
            if arrOfCheckedServices[sender.tag] == 0{
                
                arrOfCheckedServices.remove(at: sender.tag)
                arrOfCheckedServices.insert(1, at: sender.tag)
                
                let strServiceId = String(describing: arrOfServices[sender.tag]["CoreServiceMasterId"] as! NSNumber)
                let strServiceName = arrOfServices[sender.tag]["Name"] as? String
                let strServiceSysName = arrOfServices[sender.tag]["SysName"] as? String
                let strServiceQuantity = arrOfEditedQuantity[sender.tag]
                let strServicePrice = arrOfEditedPrice[sender.tag]
                let strTaxable = String(arrOfTaxable[sender.tag])
                
                
                let dict = ["serviceId": strServiceId,
                            "serviceName": strServiceName!,
                            "serviceSysName": strServiceSysName!,
                            "serviceQuantity" : strServiceQuantity,
                            "servicePrice": strServicePrice,
                            "serviceTaxable": strTaxable] as [String: Any]
                
                arrOfSelectedServices.append(dict)
                tblView.reloadData()
            }
            else if arrOfCheckedServices[sender.tag] == 1{
                
                let strServiceId = String(describing: arrOfServices[sender.tag]["CoreServiceMasterId"] as! NSNumber)
                let strServiceName = arrOfServices[sender.tag]["Name"] as? String
                let strServiceSysName = arrOfServices[sender.tag]["SysName"] as? String
                let strServiceQuantity = arrOfEditedQuantity[sender.tag]
                let strServicePrice = arrOfEditedPrice[sender.tag]
                let strTaxable = String(arrOfTaxable[sender.tag])
                
                for i in 0..<arrOfSelectedServices.count{
                    if strServiceId == arrOfSelectedServices[i]["serviceId"] as! String{
                        
                        arrOfSelectedServices.remove(at: i)
                        let dict = ["serviceId": strServiceId,
                                    "serviceName": strServiceName!,
                                    "serviceSysName": strServiceSysName!,
                                    "serviceQuantity" : strServiceQuantity,
                                    "servicePrice": strServicePrice,
                                    "serviceTaxable": strTaxable] as [String: Any]
                        
                        arrOfSelectedServices.insert(dict, at: i)
                        break
                    }
                }
                tblView.reloadData()
            }
        }
        
        else if arrOfTaxable[sender.tag] == 1{
            
            arrOfTaxable.remove(at: sender.tag)
            arrOfTaxable.insert(0, at: sender.tag)
            
            if arrOfCheckedServices[sender.tag] == 1{
                
                let strServiceId = String(describing: arrOfServices[sender.tag]["CoreServiceMasterId"] as! NSNumber)
                let strServiceName = arrOfServices[sender.tag]["Name"] as? String
                let strServiceSysName = arrOfServices[sender.tag]["SysName"] as? String
                let strServiceQuantity = arrOfEditedQuantity[sender.tag]
                let strServicePrice = arrOfEditedPrice[sender.tag]
                let strTaxable = String(arrOfTaxable[sender.tag])
                
                for i in 0..<arrOfSelectedServices.count{
                    if strServiceId == arrOfSelectedServices[i]["serviceId"] as! String{
                        
                        arrOfSelectedServices.remove(at: i)
                        let dict = ["serviceId": strServiceId,
                                    "serviceName": strServiceName!,
                                    "serviceSysName": strServiceSysName!,
                                    "serviceQuantity" : strServiceQuantity,
                                    "servicePrice": strServicePrice,
                                    "serviceTaxable": strTaxable] as [String: Any]
                        
                        arrOfSelectedServices.insert(dict, at: i)
                    }
                }
                tblView.reloadData()
            }
        }
    }
    
    @objc func btnCheckboxAction(sender: UIButton!){
        
//        let index = IndexPath(item: sender.tag, section: 0)
//        let cell = self.tblView.cellForRow(at: index) as! ServiceCategoryCell
        
        let strServiceId = String(describing: arrOfServices[sender.tag]["CoreServiceMasterId"] as! NSNumber) //ServiceMasterId
        let strServiceName = arrOfServices[sender.tag]["Name"] as? String
        let strServiceSysName = arrOfServices[sender.tag]["SysName"] as? String
        let strServiceQuantity = arrOfEditedQuantity[sender.tag]
        let strServicePrice = arrOfEditedPrice[sender.tag]
        let strTaxable = String(arrOfTaxable[sender.tag]) //arrOfServices[i]["Price"] as? String
        
        if arrOfCheckedServices[sender.tag] == 0{
            
            arrOfCheckedServices.remove(at: sender.tag)
            arrOfCheckedServices.insert(1, at: sender.tag)
            
            let dict = ["serviceId": strServiceId,
                        "serviceName": strServiceName!,
                        "serviceSysName": strServiceSysName!,
                        "serviceQuantity" : strServiceQuantity,
                        "servicePrice": strServicePrice,
                        "serviceTaxable": strTaxable] as [String: Any]
            
            arrOfSelectedServices.append(dict)
            
            tblView.reloadData()
        }
        else if arrOfCheckedServices[sender.tag] == 1{
            arrOfCheckedServices.remove(at: sender.tag)
            arrOfCheckedServices.insert(0, at: sender.tag)
            
           
            if arrOfTaxable[sender.tag] == 1{
                arrOfTaxable.remove(at: sender.tag)
                arrOfTaxable.insert(0, at: sender.tag)

            }
            for i in 0..<arrOfSelectedServices.count{
                if strServiceId == arrOfSelectedServices[i]["serviceId"] as! String{
                    arrOfSelectedServices.remove(at: i)
                    break
                }
            }
            
            tblView.reloadData()
        }
    }
    
    @IBAction func btnAddServicesAction(sender: UIButton!){
        
        if arrOfSelectedServices.count == 0 {
            self.view.makeToast("Please select at least one service")
        }
        else{
            
            
            for index in 0..<arrOfSelectedServices.count{
                
                let strServiceId = arrOfSelectedServices[index]["serviceId"] as! String
                let strServiceName = arrOfSelectedServices[index]["serviceName"] as? String
                let strServiceSysName = arrOfSelectedServices[index]["serviceSysName"] as? String
                let strServiceQuantity = arrOfSelectedServices[index]["serviceQuantity"] as? String
                let strServicePrice = arrOfSelectedServices[index]["servicePrice"] as? String
                let strTaxable = arrOfSelectedServices[index]["serviceTaxable"] as? String
                
                let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@ && serviceId == %@", strWoId, strServiceId ))
            
                if arryOfData.count > 0 {
                    
                    print("Duplicate Values")
                    // Delete existing Service and Read this as a new service
                    
                    deleteAllRecordsFromDB(strEntity: "ServiceCategoryGeneralInfo", predicate:NSPredicate(format: "workorderId == %@ && serviceId == %@", strWoId, strServiceId))
                    
                    SaveServiceDetialsInTheCoreData(strServiceId: strServiceId, strServiceName: strServiceName!, strServicePrice: strServicePrice!, strServiceQuantity: strServiceQuantity!, strServiceSysName: strServiceSysName!, strTaxable: strTaxable!)
                    
                }
                else{
                    
                    SaveServiceDetialsInTheCoreData(strServiceId: strServiceId, strServiceName: strServiceName!, strServicePrice: strServicePrice!, strServiceQuantity: strServiceQuantity!, strServiceSysName: strServiceSysName!, strTaxable: strTaxable!)

                }
            }
        }
        self.delegate?.dismissView(isTrue: true)
        self.navigationController?.popViewController(animated: false)
    }
        
    
    
    func SaveServiceDetialsInTheCoreData(strServiceId: String, strServiceName: String,strServicePrice: String,  strServiceQuantity: String,strServiceSysName: String, strTaxable : String){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("serviceCategoryId")
        arrOfKeys.add("serviceCategoryName")
        arrOfKeys.add("serviceCategorySysName")
        arrOfKeys.add("serviceId")
        arrOfKeys.add("serviceName")
        arrOfKeys.add("servicePrice")
        arrOfKeys.add("serviceQuantity")
        arrOfKeys.add("serviceSysName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("taxable")
        arrOfKeys.add("userName")
        arrOfKeys.add("companyKey")
        
        
        arrOfValues.add(strServiceCategoryId)
        arrOfValues.add(strServiceCategoryName)
        arrOfValues.add(strServiceCategorySysName)
        arrOfValues.add(strServiceId)
        arrOfValues.add(strServiceName)
        arrOfValues.add(strServicePrice)
        arrOfValues.add(strServiceQuantity)
        arrOfValues.add(strServiceSysName)
        arrOfValues.add(strWoId)
        arrOfValues.add(strTaxable)
        arrOfValues.add(strUserName)
        arrOfValues.add(strCompanyKey)
        
        saveDataInDB(strEntity: "ServiceCategoryGeneralInfo", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        updateModifyDate()
        //Update Modify Date In Work Order DB
        updateServicePestModifyDate(strWoId: self.strWoId as String)

        
    }
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
}




//        @objc func btnAddServicesAction(sender: UIButton!){
//
//            for i in 0..<arrOfCheckedServices.count{
//
//                if arrOfCheckedServices[i] == 1{
//
//                    let index = IndexPath(item: i, section: 0)
//                    let cell = self.tblView.cellForRow(at: index) as! ServiceCategoryCell
//
//                    let strServiceId = String(describing: arrOfServices[i]["CoreServiceMasterId"] as! NSNumber) //ServiceMasterId
//                    let strServiceName = arrOfServices[i]["Name"] as? String
//                    let strServiceSysName = arrOfServices[i]["SysName"] as? String
//                    let strServiceQuantity = cell.txtCategoryQuantity.text
//                    let strServicePrice = cell.txtCategoryPrice.text! //arrOfServices[i]["Price"] as? String
//
//                    SaveServiceDetialsInTheCoreData(strServiceId: strServiceId, strServiceName: strServiceName!, strServicePrice: strServicePrice, strServiceQuantity: strServiceQuantity!, strServiceSysName: strServiceSysName!)
//                }
//            }
//
//
//            self.navigationController?.popViewController(animated: false)
//        }



