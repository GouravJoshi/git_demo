//
//  AddAreaDocumentVC.swift
//  DPS
//
//  Created by APPLE on 22/04/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class AddAreaDocumentVC: UIViewController, UIDocumentPickerDelegate {
    
    // MARK: - ----IB-Outlets
    
    @IBOutlet weak var txtTitle: ACFloatingTextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnActions: UIButton!
    
    // MARK: - ----Variable
    
    var strWoId = String()
    var strWoNo = String()
    var strFrom = String()
    var imagePicker = UIImagePickerController()
    var strMobileServiceDocumentId = String ()
    var strServiceDocumentId = String ()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var strAssociationType = String ()
    var objWorkorderDetail = NSManagedObject()
    var strMobileConditionDocumentId = String ()
    var strConditionDocumentId = String ()
    var strConditionId = String ()
    var strGlobalName = String ()
    var strGlobalDocumentType = String ()
    var globalData  = Data ()
    var timer = Timer()
    var videoURL = NSURL()
    // MARK: - ----Views Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblName.text = ""
        btnActions.isHidden = true
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //For Audio
        if (nsud.value(forKey: "yesAudioDocument") != nil)
        {
            
            let isAudio = nsud.bool(forKey: "yesAudioDocument")
            
            if isAudio
            {
                
                deleteData()
                
                nsud.set(false, forKey: "yesAudioDocument")
                nsud.synchronize()
                
                self.strGlobalDocumentType = "audio"
                strGlobalName = nsud.value(forKey: "AudioNameDocument") as! String
                globalData = GetDataFromDocumentDirectory(strFileName: strGlobalName)
                
                lblName.text = strGlobalName
                
                btnActions.isHidden = false
                
                btnActions.setImage(UIImage(named: "audio"), for: .normal)
                
            }
            
        }
        
    }
    
    // MARK: - ----Button Action
    
    @IBAction func action_Back(_ sender: Any) {
        
        deleteData()
        
        self.back()
        
    }
    
    @IBAction func action_Browse(_ sender: ButtonWithShadow) {
        
        self.selectDocument(sender: sender)
        
    }
    
    
    @IBAction func action_Save(_ sender: UIButton) {
        
        self.saveDocuments()
        
    }
    @IBAction func keyDown(_ sender: Any) {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func action_Play(_ sender: Any) {
        
        if strGlobalDocumentType == "image" {
            
            let uiImage: UIImage = UIImage(data: globalData)!
            
            let  imageView = UIImageView(image: uiImage)
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
                    testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
            
        }else if strGlobalDocumentType == "pdf" {
            
            savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
            
            goToPdfView(strPdfName: strGlobalName)
            
        }else if strGlobalDocumentType == "video" {
            
            
            let player = AVPlayer(url: videoURL as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
            
        }
            
            
            
        else{
            
            playAudioo(strAudioName: strGlobalName)
            
        }
        
    }
    
    
    // MARK: - ----Functionssssss
    
    func deleteData() {
        
        if strGlobalDocumentType == "audio"{
            
            if strGlobalName.count > 0 {
                
                removeImageFromDirectory(itemName: strGlobalName)
                
            }
            
        }
        
        if strGlobalDocumentType == "pdf"{
            
            if strGlobalName.count > 0 {
                
                removeImageFromDirectory(itemName: strGlobalName)
                
            }
            
        }
        
        if strGlobalDocumentType == "image"{
            
            if strGlobalName.count > 0 {
                
                removeImageFromDirectory(itemName: strGlobalName)
                
            }
            
        }
        
    }
    
    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func playAudioo(strAudioName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToPdfView(strPdfName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func saveDocuments() {
        
        if (txtTitle.text?.count)! < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_EnterTitle, viewcontrol: self)
            
        } else if (strGlobalName.count) < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectDoc, viewcontrol: self)
            
        } else {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("WorkOrderNo")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("documentType")
            //arrOfKeys.add("isToUpload")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(strWoId)
            arrOfValues.add(strWoNo)
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(strGlobalDocumentType)
            //arrOfValues.add("Yes")
            
            
            // Saving Data in Document Directory
            
            if strGlobalDocumentType == "image" {
                
                let uiImage: UIImage = UIImage(data: globalData)!
                
                let imageResized = Global().resizeImageGloballl(uiImage)
                
                saveImageDocumentDirectory(strFileName: strGlobalName, image: imageResized!)
                
            }else if strGlobalDocumentType == "pdf" {
                
                savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
                
            }
            else if strGlobalDocumentType == "video" {
                
                savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
                
            }
            else{
                
                // audio already saved in DB
                
            }
            
            if strFrom == "Area" {
                var docPath = ""
                if strGlobalDocumentType == "image"{
                     docPath = "\\Documents\\UploadImages\\" + strGlobalName

                }
                else if strGlobalDocumentType == "audio"{
                    docPath = "\\Documents\\UploadAudio\\" + strGlobalName
                }
                else if strGlobalDocumentType == "pdf" {
                    docPath =  "\\Documents\\ConditionImage\\" + strGlobalName
                }
                else{
                    docPath =  "\\Documents\\ConditionImage\\" + strGlobalName
                }
                arrOfKeys.add("serviceAreaDocumentId")
                arrOfKeys.add("addressAreaId")
                arrOfKeys.add("documnetTitle")
                arrOfKeys.add("documentPath")
                
                arrOfValues.add(Global().getReferenceNumber())
                arrOfValues.add(strConditionId)
                arrOfValues.add(txtTitle.text!)
                arrOfValues.add(docPath)
                
                saveDataInDB(strEntity: "ServiceAreaDocuments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
                
            }else {
                //Device
                var docPath = ""
                if strGlobalDocumentType == "image"{
                     docPath = "\\Documents\\UploadImages\\" + strGlobalName

                }
                else if strGlobalDocumentType == "audio"{
                    docPath = "\\Documents\\UploadAudio\\" + strGlobalName
                }
                else if strGlobalDocumentType == "pdf" {
                    docPath =  "\\Documents\\ConditionImage\\" + strGlobalName
                }
                else{
                    docPath =  "\\Documents\\ConditionImage\\" + strGlobalName
                }
                
                arrOfKeys.add("serviceDeviceDocumentId")
                arrOfKeys.add("deviceId")
                arrOfKeys.add("documnetTitle")
                arrOfKeys.add("documentPath")
                
                arrOfValues.add(Global().getReferenceNumber())
                arrOfValues.add(strConditionId)
                arrOfValues.add(txtTitle.text!)
                arrOfValues.add(docPath)
                
                saveDataInDB(strEntity: "ServiceDeviceDocuments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
            }
            
            
        }
        
    }
    
    func selectDocument(sender: ButtonWithShadow)  {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            self.strGlobalDocumentType = "image"
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            self.strGlobalDocumentType = "image"
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: "Voice Note", style: .default , handler:{ (UIAlertAction)in
            
            self.strGlobalDocumentType = "audio"
            
            self.goToRecordView()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Video", style: .default , handler:{ (UIAlertAction)in
            
            self.strGlobalDocumentType = "video"
            self.imagePicker = UIImagePickerController()
            self.imagePicker.sourceType = .camera
            self.imagePicker.cameraDevice = .rear
            self.imagePicker.videoMaximumDuration = 30
            self.imagePicker.delegate = self
            //self.imagePicker.videoQuality = .typeLow
            let mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)
            let videoMediaTypesOnly = (mediaTypes as NSArray?)?.filtered(using: NSPredicate(format: "(SELF contains %@)", "movie"))
            let movieOutputPossible: Bool = videoMediaTypesOnly != nil
            if movieOutputPossible {
                if let videoMediaTypesOnly = videoMediaTypesOnly as? [String] {
                    self.imagePicker.mediaTypes = videoMediaTypesOnly
                }
                self.timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.pickerDismiss), userInfo: nil, repeats: false)
                self.present(self.imagePicker, animated: true)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "More...", style: .default , handler:{ (UIAlertAction)in
         let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypeImage),String(kUTTypePDF)], in: UIDocumentPickerMode.import)
                                    documentPicker.delegate = self
                                  documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                                  documentPicker.popoverPresentationController?.sourceView = self.view

                                  self.present(documentPicker, animated: true, completion: nil)

            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        if let popoverController = alert.popoverPresentationController {
                              popoverController.sourceView = sender as UIView
                              popoverController.sourceRect = sender.bounds
                            //  popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                          }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    @objc func pickerDismiss()
    {
        showAlertWithoutAnyAction(strtitle: "Max Limit Exceeded", strMessage: "Your video recorded succesfully!", viewcontrol: self)
        imagePicker.stopVideoCapture()
    }
    func goToRecordView()  {
        
        let isAudioPermission = Global().isAudioPermissionAvailable()
        if isAudioPermission {
            let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
            
            if strFrom == "Condition" {
                
                testController?.strFromWhere = "PestConditionDocument"
                
            } else {
                
                testController?.strFromWhere = "PestDocument"
                
            }
            //testController?.strFromWhere = "PestDocument"
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
        }
        else{
            let alertController = UIAlertController (title: Alert, message: alertAudioVideoPermission, preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Go-To Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    // MARK: - -----------------------------------UIDocumentPickerViewController Delegate Method-----------------------------------
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        
        print("import result : \(myURL)")
        
        do {
            
            let isMoreThenTwoMB = Global().isMoreThenTwoMB(myURL, 2)
            
            if isMoreThenTwoMB {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_DataLimit, viewcontrol: self)
                
            } else {
                
                deleteData()
                
                let myUrlString = "\(myURL)"
                
                lblName.text = Global().strDocName(fromPath: myUrlString)
                
                if myUrlString.contains(".pdf") || myUrlString.contains(".PDF") {
                    
                    let data = try Data(contentsOf: myURL)
                    
                    globalData = data
                    self.strGlobalDocumentType = "pdf"
                    
                    if strFrom == "Condition" {
                        
                        strGlobalName = "pdfC" + "\(getUniqueValueForId())" + ".pdf"
                        
                    }else{
                        
                        strGlobalName = "pdfW" + "\(getUniqueValueForId())" + ".pdf"
                        
                    }
                    
                    btnActions.isHidden = false
                    
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                    
                }else{
                    
                    // image
                    let data = try Data(contentsOf: myURL)
                    let uiImage: UIImage = UIImage(data: data)!
                    
                    globalData = data
                    self.strGlobalDocumentType = "image"
                    
                    if strFrom == "Condition" {
                        
                        strGlobalName = "imgC" + "\(getUniqueValueForId())" + ".png"
                        
                    }else{
                        
                        strGlobalName = "imgW" + "\(getUniqueValueForId())" + ".png"
                        
                    }
                    
                    btnActions.isHidden = false
                    
                    btnActions.setImage(uiImage, for: .normal)
                    
                }
                
            }
            
            
        } catch _ as NSError  {
            
        }catch {
            
        }
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
         print("view was cancelled")
         //dismiss(animated: true, completion: nil)
     }
    
    func documentMenu(_ documentMenu: UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
    }
}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension AddAreaDocumentVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        deleteData()
        if(self.strGlobalDocumentType == "video"){ // For Video
            // video
            btnActions.isHidden = false
            
            btnActions.setImage(UIImage(named: "video"), for: .normal)
            
            self.timer.invalidate()
            let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String
            if (mediaType == "public.movie") {
                videoURL = (info[UIImagePickerController.InfoKey.mediaURL] as? NSURL)!
                print("videoURL---------\(videoURL)")
                self.strGlobalDocumentType = "video"
                if let data = try? Data(contentsOf: videoURL as URL)
                {
                    self.globalData = data
                    btnActions.isHidden = false
                    if strFrom == "Condition" {
                        strGlobalName = "videoC" + "\(getUniqueValueForId())" + ".mp4"
                    }else{
                        strGlobalName = "videoW" + "\(getUniqueValueForId())" + ".mp4"
                    }
                    lblName.text = strGlobalName
                }else{
                    
                }
                
            }
        }else{
            let imageData = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.jpegData(compressionQuality:1.0)
            
            //let data = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.pngData() as NSData?
            
            btnActions.isHidden = false
            
            btnActions.setImage(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, for: .normal)
            
            globalData = imageData!
            
            self.strGlobalDocumentType = "image"
            
            if strFrom == "Condition" {
                
                strGlobalName = "imgC" + "\(getUniqueValueForId())" + ".png"
                
            }else{
                
                strGlobalName = "imgW" + "\(getUniqueValueForId())" + ".png"
                
            }
            
            lblName.text = strGlobalName
            
        }
    }
}
