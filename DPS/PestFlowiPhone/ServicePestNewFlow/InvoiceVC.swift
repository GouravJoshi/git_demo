//
//  InvoiceVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class InvoiceVC: UIViewController, AddCommentProtocol, DatePickerProtocol {
    
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnChecklist: UIButton!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var btnClock: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var btnRecordAudio: UIButton!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewForPicker: UIView!
    @IBOutlet weak var btnPickerDone: UIButton!

    //MARK: -Varialbe
    var arrOfTblViewCells = ["General Info Header", "Area Summary", "Device Title", "Pest Title", "Service Title", "Tax", "Take Payment", "Tech Comment", "Office Note", "Customer Signature", "Tehnician's Signature", "Service Terms", "Service Report Type"]
    
    var arrOfCreditCardOption =  ["Cash", "Check", "Credit Card", "Bill", "Auto Charge Customer", "Payment Pending", "No Charge"]

    var strWoId = ""
    var deviceGlobalIndex = 0
    var pestSummaryGlobalIndex = 0
    var isBtnOpenPOCBillingClciked = Bool()
    
    var strWorkOrderStatus = ""
    var strServicePocId = String()
    var strBillingPocId = String()
    
    var strButtonTitleOfStartDate = ""
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var isBtnOpenAdditionalInoClciked = Bool()
    var strServicesStatus = String()
    var strDepartmentType = String()
    var strServiceAddImageName = String()
    var dictLoginData = NSDictionary()
    var strCompanyKey = String()
    var strUserName = String()
    var chkCustomerNotPresent = Bool()
    var strEmpID = String()
    var strEmpName = String()
    var strFromEmail = String()
    
    var arrOfDeviceType = NSArray()
    var arrOfPests = NSArray()
    var arrOfArea = NSArray()
    var arrOfServiceReportType = NSArray()
    var strServiceReportType = String()
    var strNoOfArea = String()
    var strNoOfAreaInspected = String()
    
    var strCustomerSign = String()
    var strTechnicianSign = String()
    var yesEditedSomething = Bool()
    
    var arrOfCollectionOfDevices = [[String]]()
    var arrOfCollectionOfDevicesTitle = [String]()
    
    var arrOfTblPestSummary = [[String]]()
    @objc var objServiceCategoryDetail = NSMutableArray()
    var arrOfServices = [[String]]()
    
    var indexForTakePayment = Int()
    var selectedPaymentMode = 0
    
    var indexForServiceReportType = Int()
    var selectedDefaultReportType = 0
    
    
    //Payment
    var strAmount = ""
    var strCheckNo = ""
    var strDrivingLicences = ""
    var strCheckbackImage = ""
    var strCheckFrontImage = ""
    var strCheckBackImage = ""
    var strExpiryDate = ""
    
    var chkFrontImage = Bool()
    var isCollectPayment = false

    var strGlobalPaymentMode = ""
    @objc var paymentInfoDetail = NSManagedObject()
    @objc var strPaymentModes = String()
    @objc var strDepartmentIdd = String()
    @objc var strPaidAmount = String()
    
    
    var isPreSetSignGlobal = Bool()
    
    
    //Audio
    var strAudioName = String()
    
    
    var imgTechSign = UIImage()
    var imgCustomerSign = UIImage()
    
    var customerSignUrl : URL? = nil
    var strCustomersignImageName = ""
    
    var techSignUrl : URL? = nil
    var strTechsignImageName = ""
    
    //Timesheet
    var arrOfTimeSheetData = NSMutableArray()
    var arrOfStartSubject = NSMutableArray()
    var finalDurationHHMM = String()
    var strGlobalEndTime = String()

    var taxableTotal = Float()
    var nonTaxableTotal = Float()
    var isPreview = false
    var strTotalDue = ""
    var strTax      = ""

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.dataSource = self
        pickerView.delegate = self
        viewForPicker.isHidden = true
        viewForPicker.alpha  = 0
        strGlobalPaymentMode = "Cash"
        isBtnOpenPOCBillingClciked = false
        isBtnOpenAdditionalInoClciked = true

        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        
        let arryOfData1 = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData1.count > 0 {
            objWorkorderDetail = arryOfData1[0] as! NSManagedObject
        }
        let strCustomerNotPresent = "\(objWorkorderDetail.value(forKey: "isCustomerNotPresent") ?? "" )"
        
        if strCustomerNotPresent == "true" || strCustomerNotPresent == "1"
        {
            chkCustomerNotPresent = true
            arrOfTblViewCells = ["General Info Header", "Area Summary", "Device Title", "Pest Title", "Service Title", "Tax", "Take Payment", "Tech Comment", "Office Note", "Tehnician's Signature", "Service Terms", "Service Report Type"]
        }
        else
        {
            
            chkCustomerNotPresent = false
            arrOfTblViewCells = ["General Info Header", "Area Summary", "Device Title", "Pest Title", "Service Title", "Tax", "Take Payment", "Tech Comment", "Office Note", "Customer Signature", "Tehnician's Signature", "Service Terms", "Service Report Type"]
        }
        
        strAudioName = ""
        isPreSetSignGlobal = false
        
        lblTitle.text = "\(nsud.value(forKey: "lblName") ?? "")"
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        if "\(objWorkorderDetail.value(forKey: "serviceReportFormat") ?? "")" != ""{
            
            strServiceReportType = "\(objWorkorderDetail.value(forKey: "serviceReportFormat") ?? "")"
        }
        else{
            strServiceReportType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportFormat") ?? "")"
        }
        
        let strServiceReportTypeLocal = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
        
        if strServiceReportTypeLocal == "CompanyEmail"
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
        }
        else
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        }
        
        tblView.delegate = self
        tblView.dataSource = self
        
        fetchServiceDetailsData()
       
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        fetchTimeSheetData()
        btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
        btnChecklist.layer.borderWidth = 1.0
        btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)
        strButtonTitleOfStartDate = UserDefaults.standard.value(forKey: "StartStopButtonTitle") as! String
        if !isPreview {
            if strButtonTitleOfStartDate == "Start"{
                setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Pause")
            }
            else if strButtonTitleOfStartDate == "Pause"{
                setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Start")
            }
        }
        
        var strIns = String()
        var strCust = String()
        var strImageName = String()
        strCustomerSign = "\(objWorkorderDetail.value(forKey: "customerSignaturePath") ?? "")"
        strTechnicianSign = "\(objWorkorderDetail.value(forKey: "technicianSignaturePath") ?? "")"
        
        //strImageName = nsud.value(forKey: "imagePath") as! String
        strIns = nsud.value(forKey: "fromInspectorSignService") as? String ?? ""
        strCust = nsud.value(forKey: "fromCustomerSignService") as? String ?? ""
        
        if (strIns == "fromInspectorSignService")
        {
            strImageName = nsud.value(forKey: "imagePath") as! String
            strTechnicianSign = strImageName
            yesEditedSomething = true
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            
            
            if let dirPath = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strImageName)
                let image    = UIImage(contentsOfFile: imageURL.path)
                imgTechSign = image!
                
            }
            saveImageDocumentDirectory(strFileName: strImageName, image: imgTechSign)
            nsud.set("abc", forKey: "fromInspectorSignService")
            nsud.synchronize()
            
        }
        if (strCust == "fromCustomerSignService")
        {
            strImageName = nsud.value(forKey: "imagePath") as! String
            strCustomerSign = strImageName
            yesEditedSomething = true
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            if let dirPath = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strImageName)
                let image    = UIImage(contentsOfFile: imageURL.path)
                imgCustomerSign = image!
            }
            
            saveImageDocumentDirectory(strFileName: strImageName, image: imgCustomerSign)
            nsud.set("abc", forKey: "fromCustomerSignService")
            nsud.synchronize()
        }
        
        if strTechnicianSign.count>0
        {
            downloadTechnicianSign(strImageName: strTechnicianSign)
        }
        
        if strCustomerSign.count>0
        {
            downloadCustomerSign(strImageName: strCustomerSign)
        }
        
        
        //For Preset
        
        let isPreSetSign = nsud.bool(forKey: "isPreSetSignService")
        
        var strSignUrl = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
        strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
        if ((isPreSetSign) && (strSignUrl.count>0) && !(strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed"))
        {
            /* if strImageName.count == 0
             {
             strImageName = "imgTechnician" + getUniqueString()
             strTechnicianSign = strImageName
             }*/
            strImageName = Global().strTechSign(strSignUrl)
            strTechnicianSign = Global().strTechSign(strSignUrl)
            
            let nsUrl = URL(string: strSignUrl)
            
            techSignUrl = nsUrl! as URL
            strTechsignImageName = strImageName
            //imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
            
            saveImageDocumentDirectory(strFileName: strTechnicianSign, image: imgTechSign)
            
            // abc123 btnTechnicianSignature.isEnabled = false
            isPreSetSignGlobal = true
            
        }
        else
        {
            isPreSetSignGlobal = false
            // abc123 btnTechnicianSignature.isEnabled = true
            
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed")
            {
                //abc123  btnTechnicianSignature.isEnabled = false
            }
            if strTechnicianSign.count>0
            {
                let strIsPresetWO = objWorkorderDetail.value(forKey: "isEmployeePresetSignature") as! String
                
                if strIsPresetWO == "true" || strIsPresetWO == "1"
                {
                    downloadTechnicianPreset(strImageName: strTechnicianSign, stringUrl: strSignUrl)
                }
                else
                {
                    downloadTechnicianSign(strImageName: strTechnicianSign)
                }
                
                
            }
        }
        
        
        //For Audio
        if (nsud.value(forKey: "yesAudio") != nil)
        {
            yesEditedSomething = true
            let isAudio = nsud.bool(forKey: "yesAudio")
            
            if isAudio
            {
                
                strAudioName = nsud.value(forKey: "AudioNameService") as! String
                
            }
            
        }
        
        getTheTax()
    }
    //MARK: - IBActions
    
    @objc func btnCollectPaymentAction(sender: UIButton!){
        let index = IndexPath(item: 0, section: 0)
        let cell = self.tblView.cellForRow(at: index) as! GeneralHeaderCell

        if cell.btnCollectPayment.tag == 0{
            cell.btnCollectPayment.tag = 1
            isCollectPayment = true
            self.tblView.reloadData()
            updateCollectPayment()
        }
        else if cell.btnCollectPayment.tag == 1{
            cell.btnCollectPayment.tag = 0
            isCollectPayment = false
            self.tblView.reloadData()
            updateCollectPayment()

        }
        
    }
    
    func updateCollectPayment(){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("isCollectPayment")
        arrOfValues.add(String(isCollectPayment))

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    @objc func actionOnSignature(_ sender: UIButton)
    {
        if sender.tag == 1{
            nsud.set("fromCustService", forKey: "signatureType")
            nsud.synchronize()
            
            let mainStoryboard = UIStoryboard(name: "Service_iPhone", bundle: nil)
            let objVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSignViewController") as? ServiceSignViewController
            //self.navigationController?.pushViewController(objVC!, animated: true)
            self.present(objVC!, animated: false, completion: nil)
        }
        else if sender.tag == 2 {
            nsud.set("fromTechService", forKey: "signatureType")
            nsud.synchronize()
            
            let mainStoryboard = UIStoryboard(name: "Service_iPhone", bundle: nil)
            let objVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSignViewController") as? ServiceSignViewController
            self.present(objVC!, animated: false, completion: nil)
        }
    }
    
    
    @objc func btnCustomerNotPresent(_ sender: UIButton)
    {
        if chkCustomerNotPresent == false
        {
            chkCustomerNotPresent = true
            arrOfTblViewCells.remove(at: sender.tag + 1)
            tblView.reloadData()
        }
        else if chkCustomerNotPresent == true
        {
            chkCustomerNotPresent = false
            arrOfTblViewCells.insert("Customer Signature", at: sender.tag + 1)
            tblView.reloadData()
        }
    }
    
    @IBAction func actionOnRecordAudio(_ sender: Any)
    {
        let isAudioPermission = Global().isAudioPermissionAvailable()
        if isAudioPermission {
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed")
            {
                
                let syncedAudioFile = "\(objWorkorderDetail.value(forKey: "audioFilePath") ?? "")"
                if  syncedAudioFile != ""{
                    
                    let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
                    
                    alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
                        
                        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
                        
                        testController!.strAudioName = syncedAudioFile
                        testController?.modalPresentationStyle = .fullScreen
                        self.present(testController!, animated: false, completion: nil)
                        
                    }))
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                    }))
                    alert.popoverPresentationController?.sourceView = self.view
                    self.present(alert, animated: true, completion: {
                    })
                }
                else{
                    showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "No recording available", viewcontrol: self)
                }
            }
            else{
                
                let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
                
                alert.addAction(UIAlertAction(title: "Record", style: .default , handler:{ (UIAlertAction)in
                    
                    let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
                    testController?.strFromWhere = "ServiceInvoice"
                    testController?.modalPresentationStyle = .fullScreen
                    self.present(testController!, animated: false, completion: nil)
                }))
                
                let audioName = nsud.value(forKey: "AudioNameService") as? String ?? ""
                let syncedAudioFile = "\(objWorkorderDetail.value(forKey: "audioFilePath") ?? "")"
                if audioName != "" || syncedAudioFile != ""{
                    alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
                        
                        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
                        if syncedAudioFile != ""{
                            testController!.strAudioName = syncedAudioFile
                        }
                        else{
                            testController!.strAudioName = audioName
                        }
                        testController?.modalPresentationStyle = .fullScreen
                        self.present(testController!, animated: false, completion: nil)
                        
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                }))
                alert.popoverPresentationController?.sourceView = self.view
                self.present(alert, animated: true, completion: {
                })
            }
        }
        else{
            let alertController = UIAlertController (title: Alert, message: alertAudioVideoPermission, preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Go-To Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func actionOnCheckFrontImage(_ sender: Any)
    {
        
        chkFrontImage = true
        yesEditedSomething = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    @objc func actionOnCheckBackImage(_ sender: Any)
    {
        
        chkFrontImage = false
        yesEditedSomething = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        viewForPicker.isHidden = true
        viewForPicker.alpha  = 0
        self.tblView.reloadData()
        
    }
    
    @objc func actionOnTakePayment(_ sender: UIButton){
//        pickerView.reloadAllComponents()
//        viewForPicker.isHidden = false
//        viewForPicker.alpha  = 1
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "-----Select-----"
        vc.strTag = 6001
        if arrOfCreditCardOption.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelPaymentTable = self
            vc.aryPayment = arrOfCreditCardOption
            self.present(vc, animated: false, completion: {})
        }
        else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    @objc func actionOnExpirationDate(_ sender: UIButton){
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = "Date"
        self.present(vc, animated: true, completion: {})
        
        // self.gotoDatePickerView(sender: textField as! UITextField, strType: "Date")
    }
    
    @objc func btnOpenTimeSheetAction(_ sender: Any){
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "TimesheetInvoiceVC") as? TimesheetInvoiceVC
        testController?.strWoId = strWoId
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
    
    @objc func btnTechCommnetAction(sender: UIButton!){
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTechCommentsVC") as? AddTechCommentsVC
        
        vc!.strWorkorderId = (objWorkorderDetail.value(forKey: "workorderId") as? String)!
        vc!.strTechComment = (objWorkorderDetail.value(forKey: "technicianComment") as? String ?? "")
        vc?.delegateToAddComment = self
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    @objc func btnEditHeaderInfoAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditGenralInfoVC") as? EditGenralInfoVC
        
        let strTempAcountNumber = objWorkorderDetail.value(forKey: "accountNo") as? String
        if strTempAcountNumber != ""{
            vc!.strAccountNumber = strTempAcountNumber ?? ""
        }
        vc!.strWorkOrderId = strWoId as String
        vc!.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    
    @objc func btnRadioSelectServiceReportType(_ sender: UIButton){
        
        let index = IndexPath(item: indexForServiceReportType, section: 0)
        let SRTCell = self.tblView.cellForRow(at: index) as! ServiceReportTypeInvoiceCell
        
        if sender.tag == 1{
            
            selectedDefaultReportType = SRTCell.btnRadioDefaultWithPricing.tag
            strServiceReportType = "DefaultWithPricing"
            
        }
        else  if sender.tag == 2{
            
            selectedDefaultReportType = SRTCell.btnRadioDefaultWithoutPricing.tag
            strServiceReportType = "DefaultWithoutPricing"
            
        }
        else  if sender.tag == 3{
            
            selectedDefaultReportType = SRTCell.btnRadioSummaryWithPricing.tag
            strServiceReportType = "SummaryWithPricing"
            
        }
        else  if sender.tag == 4{
            
            selectedDefaultReportType = SRTCell.btnRadioSummaryWithoutPricing.tag
            strServiceReportType = "SummaryWithoutPricing"
        }
        
        tblView.reloadData()
    }
  
    @objc func btnOpenInfoViewAction(sender: UIButton!){
        if sender.tag == 0 {
            
            isBtnOpenPOCBillingClciked = false
            isBtnOpenAdditionalInoClciked = true
        }
        else {
            
            isBtnOpenPOCBillingClciked = true
            isBtnOpenAdditionalInoClciked = false
        }
        
        
        tblView.reloadData()
    }
    
    @IBAction func btnGalleryAction(_ sender: Any) {
        self.goToGlobalmage(strType: "After")
        
    }
    
    @IBAction func btnStaticsAction(_ sender: Any) {
        self.goToGlobalmage(strType: "Graph")
        
    }
    
    @IBAction func btnCheklistAction(_ sender: Any) {
        goToInspectionView()
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed")
        {
            let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
            let objSendMail = storyboardIpad.instantiateViewController(withIdentifier: "SendEmailVC") as? SendEmailVC
            var strFromWheree = NSString()
            strFromWheree = "PestInvoice"

            objSendMail?.fromWhere = strFromWheree as String
            objSendMail?.strWoId = strWoId
            self.navigationController?.pushViewController(objSendMail!, animated: false)
            
//
//            let mainStoryboard = UIStoryboard(name: "Service_iPhone", bundle: nil)
//            let objVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSendMailViewController") as? ServiceSendMailViewController
//
//            var strFromWheree = NSString()
//            strFromWheree = "PestInvoice"
//
//            objVC?.fromWhere = strFromWheree as String
//            self.navigationController?.pushViewController(objVC!, animated: false)
        }
        else
        {
            isPreview = false
            finalSave(strPaymentMode: strGlobalPaymentMode)
        }
    }
    
    
    @IBAction func btnStartTimeAction(_ sender: Any){
        /* btnStartTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime")*/
        
        let str = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")" //"\(lblStartTimeValue.titleLabel?.text ?? "")"
        
        if str == ""
        {
            self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: false)
            strButtonTitleOfStartDate = "Pause"
        }
        else
        {
            var alertTitle = ""
            if strButtonTitleOfStartDate == "Start"{
                alertTitle = "Start"
            }
            else if strButtonTitleOfStartDate == "Pause"{
                alertTitle = "Pause"
            }
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want \(alertTitle) time", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: alertTitle, style: .default , handler:{ (nil)in
                
                if self.strButtonTitleOfStartDate == "Start"{
                    self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: true)
                }
                else if self.strButtonTitleOfStartDate == "Pause"{
                    self.getStartTimeIn(isStartTime: false, isStrTimeInAvial: true)
                }
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (nil)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    @IBAction func btnOpenActionSheetAction(_ sender: Any) {
        self.showOtherOptionsActionSheet()
    }
    
    
    //MARK: - Duration calculations
    func getDuration(strStartDate: String, strPauseDate: String) -> String{
        
        if strStartDate.count == 0 {
            
            return "\(0)"

        }else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            
           
            var startDate = dateFormatter.date(from: strStartDate)
            if startDate == nil{
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.local
                dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm:ss"
                startDate = dateFormatter2.date(from: strStartDate)
            }
            let pauseDate = dateFormatter.date(from: strPauseDate)!
            let resultedOffSet = pauseDate.offsetFrom(date: startDate!)
            
            let result = getDurationInterval(strOffSet: resultedOffSet)
            print(result)
            return result
            
        }
        
    }
    
    func getDurationInterval(strOffSet : String) -> String{
        let resultantOffSet = strOffSet
        
        let arr = resultantOffSet.components(separatedBy: " ")
        var strDay = "0"
        var strHr = "0"
        var strMin = "0"
        var strSec = "0"
        
        if resultantOffSet.contains("d"){
            strDay = arr[0].components(separatedBy: "d")[0]
            strHr = arr[1].components(separatedBy: "h")[0]
            strMin = arr[2].components(separatedBy: "m")[0]
            strSec = arr[3].components(separatedBy: "s")[0]
            
        }
        else if resultantOffSet.contains("h"){
            strHr = arr[0].components(separatedBy: "h")[0]
            strMin = arr[1].components(separatedBy: "m")[0]
            strSec = arr[2].components(separatedBy: "s")[0]
            
        }
        else if arr.count == 1{
            strSec = arr[0].components(separatedBy: "s")[0]
        }
        else{
            strMin = arr[0].components(separatedBy: "m")[0]
            strSec = arr[1].components(separatedBy: "s")[0]
        }
        
        strDay = strDay.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strDay = strDay.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strHr = strHr.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strHr = strHr.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strMin = strMin.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strMin = strMin.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strSec = strSec.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strSec = strSec.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        let dayInt = Int(strDay)!
        let hrInt = Int(strHr)!
        let minInt = Int(strMin)!
        let secInt = Int(strSec)!
        
        print(dayInt)
        print(hrInt)
        print(minInt)
        print(secInt)
        
        var dayConvertsInHours = Int()
        if dayInt != 0{
            dayConvertsInHours = dayInt * 24
        }
        let result = (dayConvertsInHours * 60 * 60) + (hrInt * 60 * 60) + (minInt * 60) + secInt  //INSECONDS
        //String(minInt) + " Min " + String(secInt) + " secs" // 2 Min 12 secs
        //
        return String(result)
    }
    
    
    //MARK: - Custom Funtions
    //Timesheet DB calling
    func fetchTimeSheetData(){
        arrOfTimeSheetData.removeAllObjects()
        //Featching all the data from DB for start stop
        
        let tempArr = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        for index in 0..<tempArr.count{
            let dict = tempArr[index] as! NSManagedObject
            arrOfTimeSheetData.add(dict)
        }
        
        getTheDataOfStartsubjectOnly()
    }
    
    func getTheDataOfStartsubjectOnly(){
        var FinalDuration = 0
        finalDurationHHMM = ""
        strGlobalEndTime = ""
        
        arrOfStartSubject.removeAllObjects()
        for i in 0..<arrOfTimeSheetData.count{
            let obj = arrOfTimeSheetData[i] as! NSManagedObject
            let strSubject = "\(obj.value(forKey: "subject") as! String)"
            if strSubject == "START"{
                arrOfStartSubject.add(arrOfTimeSheetData[i])
            }
        }
        
        for p in 0..<arrOfStartSubject.count{
            let obj = arrOfStartSubject[p] as! NSManagedObject
            let strDuration = "\(obj.value(forKey: "duration") as! String)"
            if strDuration == ""{
                if "\(obj.value(forKey: "startTime") as! String)" != "" && "\(obj.value(forKey: "stopTime") as! String)" != ""{
                    
                    var tempDuration = "0"
                    tempDuration = getDuration(strStartDate:  "\(obj.value(forKey: "startTime") as! String)", strPauseDate:  "\(obj.value(forKey: "stopTime") as! String)")
                    
                    FinalDuration = FinalDuration + Int(tempDuration)!
                    
                    strGlobalEndTime = "\(obj.value(forKey: "stopTime") as! String)"
                    strGlobalEndTime = changeStringDateToGivenFormat(strDate: strGlobalEndTime, strRequiredFormat: "MM/dd/yyyy hh:mm a")
                    
                }
                else{
                    
                    let tempStartTime = "\(obj.value(forKey: "startTime") as! String)"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = NSTimeZone.local
                    dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
                    
                    let date = Date()
                    var str = String()
                    str = dateFormatter.string(from: date)
                    
                    let strDuration  = getDuration(strStartDate: tempStartTime, strPauseDate: str)
                    FinalDuration = FinalDuration + Int(strDuration)!
                    
                    //currentdateFormatter
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.timeZone = NSTimeZone.local
                    dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm a"
                    let strWithoutSeconds = dateFormatter2.string(from: date)
                    strGlobalEndTime = strWithoutSeconds
                }
                
            }
            else{
                if strDuration != ""{
                    FinalDuration = FinalDuration + Int(strDuration)!
                    strGlobalEndTime = "\(obj.value(forKey: "stopTime") as! String)"
                    strGlobalEndTime = changeStringDateToGivenFormat(strDate: strGlobalEndTime, strRequiredFormat: "MM/dd/yyyy hh:mm a")
                }
            }
        }
        finalDurationHHMM = secondsToHoursMinutesSeconds(seconds: FinalDuration)
        print(finalDurationHHMM)
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        let hours = String(describing:seconds / 3600)
        let minutes = String(describing:seconds / 60 % 60)
        let seconds = String(describing:seconds % 60)
        var finalDuration = String()
        if hours != "" || hours != "0"{
            finalDuration = hours + "h" + minutes + "m"
        }
        else if minutes != "" || minutes != "0"{
            finalDuration = minutes + "m"
        }
        else{
            finalDuration = seconds
        }
        
        return finalDuration
    }

    
   
    
    func downloadCustomerSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgCustomerSign = image!
            strCustomersignImageName = strImageName
            
            tblView.reloadData()
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            strUrl = strUrl + "//Documents/" + strImageName
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            
            let nsUrl = URL(string: strUrl)
            customerSignUrl = nsUrl! as URL
            strCustomersignImageName = strImageName
            tblView.reloadData()
            //imgViewCustinerSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    func downloadTechnicianSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgTechSign = image!
            strTechsignImageName = strImageName
            
            tblView.reloadData()
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            strUrl = strUrl + "//Documents/" + strImageName
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            let nsUrl = URL(string: strUrl)
            techSignUrl = nsUrl! as URL
            strTechsignImageName = strImageName
            tblView.reloadData()
            
            //imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    func downloadTechnicianPreset(strImageName: String, stringUrl: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgTechSign = image!
            tblView.reloadData()
            
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            //strUrl = strUrl + "//Documents/" + strImageName
            strUrl = stringUrl
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            let nsUrl = URL(string: strUrl)
            
            techSignUrl = nsUrl! as URL
            strTechsignImageName = strImageName
            tblView.reloadData()
            
            //imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        yesEditedSomething = true
        strExpiryDate = strDate
        tblView.reloadData()
        //  btnExpirationDate.setTitle(strDate, for: .normal)
    }
    
    func addTechComment(strTitle: String, strId: String, strDesription: String) {
        //TechnicianComment
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("TechnicianComment")
        arrOfValues.add(strDesription)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            updateModifyDate()
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
        self.tblView.reloadData()
    }
    
    func getStartTimeIn(isStartTime: Bool,  isStrTimeInAvial: Bool)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        
        let strtemp = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
        var strDate = ""
        if strtemp != ""{
            
            strDate  = changeStringDateToGivenFormat(strDate: strtemp, strRequiredFormat: "MM/dd/yyyy hh:mm:ss a")
        }
        if isStrTimeInAvial == false{
            self.saveTimeInDB(startTime: str)
            setStartButtonTextWithDateTime(strTempDateAndTime: str, strTimeType: "Start")
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
        }
        if strButtonTitleOfStartDate == "Start"{
            
            
            let obj =  fetchStartTimeWithStartStatus()
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
            
            strButtonTitleOfStartDate = "Pause"
            
            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
            
            
            
        }
        else if strButtonTitleOfStartDate == "Pause"{
            
            let obj =  fetchStartTimeWithStartStatus()
            
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
            
            strButtonTitleOfStartDate = "Start"
            
            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "PAUSE", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Pause")
            
            
        }
        
    }
    
    func saveTimeInDB(startTime : String){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("TimeIn")
        arrOfValues.add(startTime)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            updateModifyDate()
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    func fetchStartTimeWithStartStatus() -> NSMutableArray{
        
        let objTimeSheetLastIndex = NSMutableArray()
        var queryParam  = ""
        if strButtonTitleOfStartDate == "Start"{
            queryParam = "PAUSE"
        }
        if strButtonTitleOfStartDate == "Pause"{
            queryParam = "START"
        }
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@  && status == %@", strWoId, queryParam))
        
        //Featching all the data from DB for start stop
        let tempArr = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        let tempDict = NSMutableArray()
        for index in 0..<tempArr.count{
            let dict = tempArr[index] as! NSManagedObject
            tempDict.add(dict)
        }
        
        for j in 0..<tempDict.count{
            let tempIndex = tempDict[j] as! NSManagedObject
            print("Start Time: \(tempIndex.value(forKey: "startTime") as! String))")
            print("Stop Time: \(tempIndex.value(forKey: "stopTime") as! String))")
            print("Duration: \(tempIndex.value(forKey: "duration") as! String))")
            print("status: \(tempIndex.value(forKey: "status") as! String))")
            print("status: \(tempIndex.value(forKey: "subject") as! String))")
            
        }
        
        if arryOfData.count > 0 {
            let dict = arryOfData.lastObject as! NSManagedObject
            objTimeSheetLastIndex.add(dict)
        }
        return objTimeSheetLastIndex
    }
    
    func saveStartStopTimeInTimeSheet(strStartTime: String, strStopTime: String, strStatus: String, strDuration: String, isStart: Bool){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        if isStart == true{
            arrOfKeys.add("companyKey")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("userName")
            arrOfKeys.add("startTime")
            arrOfKeys.add("stopTime")
            arrOfKeys.add("duration")
            arrOfKeys.add("status")
            arrOfKeys.add("subject")
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strWoId)
            arrOfValues.add(strUserName)
            arrOfValues.add(strStartTime)
            arrOfValues.add(strStopTime)
            arrOfValues.add(strDuration)
            arrOfValues.add(strStatus)
            arrOfValues.add(strStatus)
            
            saveDataInDB(strEntity: "GeneralInfoWorkorderTimeSheet", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        else if isStart == false{
            
            arrOfKeys.add("stopTime")
            arrOfKeys.add("duration")
            arrOfKeys.add("status")
            
            arrOfValues.add(strStopTime)
            arrOfValues.add(strDuration)
            arrOfValues.add(strStatus)
            
            let isSuccess = getDataFromDbToUpdate(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && startTime == %@", strWoId, strStartTime), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                UserDefaults.standard.setValue(strButtonTitleOfStartDate, forKey: "StartStopButtonTitle")
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                fetchTimeSheetData()
                tblView.reloadData()
                
                if strButtonTitleOfStartDate == "Start"{
                    switch UIDevice.current.userInterfaceIdiom {
                    case .phone:
                        navigationController?.popToViewController(ofClass: GenralInfoVC.self)
                    case .pad:
                        navigationController?.popToViewController(ofClass: GeneralInfoNewPestiPad.self)
                    default:
                        break
                    }
                }
                
            }
            else {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        
    }
    
    func setStartButtonTextWithDateTime(strTempDateAndTime: String, strTimeType: String){
        
        //        btnStartTime?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        //        var buttonText = NSString()
        //        if strTimeType == "Start" {
        //            buttonText = "Pause"//\nST:" + strTempDateAndTime as NSString
        //        }
        //        else{
        //            buttonText = "Start"//\nST:" + strTempDateAndTime as NSString
        //        }
        //        //"hello\nthere"
        //
        //           //getting the range to separate the button title strings
        //           let newlineRange: NSRange = buttonText.range(of: "\n")
        //
        //           //getting both substrings
        //           var substring1 = ""
        //           var substring2 = ""
        //
        //           if(newlineRange.location != NSNotFound) {
        //               substring1 = buttonText.substring(to: newlineRange.location)
        //               substring2 = buttonText.substring(from: newlineRange.location)
        //           }
        //
        //           //assigning diffrent fonts to both substrings
        //           let font1: UIFont = UIFont(name: "Arial", size: 14.0)!
        //           let attributes1 = [NSMutableAttributedString.Key.font: font1]
        //           let attrString1 = NSMutableAttributedString(string: substring1, attributes: attributes1)
        //
        //           let font2: UIFont = UIFont(name: "Arial", size: 9.0)!
        //           let attributes2 = [NSMutableAttributedString.Key.font: font2]
        //           let attrString2 = NSMutableAttributedString(string: substring2, attributes: attributes2)
        //
        //           //appending both attributed strings
        //           attrString1.append(attrString2)
        //
        //           //assigning the resultant attributed strings to the button
        //           btnStartTime.titleLabel?.textAlignment = NSTextAlignment.center
        //
        if !isPreview {
            if strTimeType == "Start" {
                btnStartTime.setTitle("Pause", for: .normal)
                btnStartTime.backgroundColor = hexStringToUIColor(hex: "9A9A9A")
            }
            else{
                btnStartTime.setTitle("Start", for: .normal)
                btnStartTime.backgroundColor = hexStringToUIColor(hex: "36A085")
            }
        }
        
        //  btnStartTime.setAttributedTitle(attrString1, for: [])
    }
    
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId as NSString
        testController?.strModuleType = "ServicePestFlow"
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Reset" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "reset"{
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func showOtherOptionsActionSheet()  {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let currentSetupAction = UIAlertAction(title: "Current Setup", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCurrentSetupAction()
        })
        let customerSalesDocumentsAction = UIAlertAction(title: "Customer Sales Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCustomerSalesDocument()
        })
        
        let notesHistoryAction = UIAlertAction(title: "Notes History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openNotesHistory()
        })
        let serviceHistoryAction = UIAlertAction(title: "Service History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openServiceHistory()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(currentSetupAction)
        optionMenu.addAction(customerSalesDocumentsAction)
        optionMenu.addAction(notesHistoryAction)
        optionMenu.addAction(serviceHistoryAction)
        optionMenu.addAction(cancelAction)
        
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func openCurrentSetupAction(){
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController?.strCompanyKey = strCompanyKey
        testController?.strWOId = strWoId as String
        testController?.strUserName = strUserName
        
        self.present(testController!, animated: false, completion: nil)
    }
    
    func openCustomerSalesDocument(){
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func openNotesHistory(){
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController!.strWoId = strWoId as String
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = false;
        testController!.strAccccountId = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        
    }
    
    func openServiceHistory(){
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
    
    func goToInspectionView()
    {
        
        let testController = UIStoryboard.init(name: "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "InspectionChecklistCopyVC") as! InspectionChecklistCopyVC
        
        testController.isFromNewPestFlow = "YES"
        let strTempAcountNumber = objWorkorderDetail.value(forKey: "accountNo") as? String
        if strTempAcountNumber != ""{
            testController.strAccountNumber  = strTempAcountNumber ?? ""
        }
        testController.matchesWorkOrderZSync = objWorkorderDetail
        self.navigationController?.pushViewController(testController, animated: false)
    }
    
    func createArrOfTblView(){
        var arrOfdeviceInspectionSummary = [String]()
        var arrOfPestSummary = [String]()
        var arrOfServicesCount = [String]()
        
        if arrOfDeviceType.count > 0 {
            for i in 0..<arrOfDeviceType.count{
                arrOfdeviceInspectionSummary.append("Device Inspection Summary")
                let strDeviceTypeId = "\(arrOfDeviceType[i])"
                
                var deviceSysNameLocal = String()
                
                var array = NSMutableArray()
                
                array = getDataFromServiceAutoMasters(strTypeOfMaster: "deviceTypeMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
                
                if array.count > 0 {
                    
                    if array.count > 0 {
                        
                        for k1 in 0 ..< array.count {
                            
                            let dictData = array[k1] as! NSDictionary
                            
                            // to add which target groups to be shown for product
                            
                            if strDeviceTypeId == "\(dictData.value(forKey: "DeviceTypeId") ?? "")"  {
                                
                                deviceSysNameLocal = "\(dictData.value(forKey: "DeviceTypeName") ?? "")"
                                
                                break
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                
                let arrOfDevices =  getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "devieTypeId == %@ && companyKey == %@ && isDeletedDevice == %@ && serviceAddressId == %@",strDeviceTypeId,Global().getCompanyKey(),"0","\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")"))
                
                //let arrOfDevicesOnWo =  getDataFromCoreDataBaseArray(strEntity: "WOServiceDevices", predicate: NSPredicate(format: "workOrderId == %@",strWoId))
                
                let tempCountDeviceInspection = NSMutableArray()
                let tempCountDeviceReplaced = NSMutableArray()
                let tempCountDeviceActivity = NSMutableArray()
                
                if arrOfDevices.count > 0 {
                    
                    for k in 0 ..< arrOfDevices.count {
                        
                        let obj = arrOfDevices[k] as! NSManagedObject
                        
                        var strDeviceIdLocal = "\(obj.value(forKey: "deviceId") ?? "")"
                        
                        if strDeviceIdLocal.count == 0 || strDeviceIdLocal == "0" {
                            
                            strDeviceIdLocal = "\(obj.value(forKey: "mobileDeviceId") ?? "")"
                            
                        }
                        
                        let arrOfDevicesInspection =  getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "mobileServiceDeviceId == %@ && companyKey == %@ && workOrderId == %@",strDeviceIdLocal,Global().getCompanyKey(),strWoId))
                        
                        if arrOfDevicesInspection.count > 0 {
                            
                            tempCountDeviceInspection.add(strDeviceIdLocal)
                            
                            // deviceReplacementReasonId
                            
                            let obj = arrOfDevicesInspection[0] as! NSManagedObject
                            
                            let strDeviceReplacementReasonId = "\(obj.value(forKey: "deviceReplacementReasonId") ?? "")"
                            
                            if strDeviceReplacementReasonId.count > 0 {
                                
                                tempCountDeviceReplaced.add(strDeviceIdLocal)
                                
                            }
                            
                            let isActivity = obj.value(forKey: "activity") as! Bool
                            
                            if isActivity {
                                
                                tempCountDeviceActivity.add(strDeviceIdLocal)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
                arrOfCollectionOfDevicesTitle.append(deviceSysNameLocal)
                
                let skipped = arrOfDevices.count - tempCountDeviceInspection.count
                
                let arrtemp = ["# Inspected: \(tempCountDeviceInspection.count) of " + "\(arrOfDevices.count)", "# Skipped: \(skipped) of " + "\(arrOfDevices.count)",  "# Replaced: \(tempCountDeviceReplaced.count) of " + "\(arrOfDevices.count)", "Inspected/Activity: \(tempCountDeviceActivity.count) of " + "\(arrOfDevices.count)"]
                
                
                arrOfCollectionOfDevices.append(arrtemp)
            }
        }
        
        if arrOfDeviceType.count > 0 {
            arrOfTblViewCells.insert(contentsOf: arrOfdeviceInspectionSummary, at: 3)
        }
        
        
        if arrOfPests.count > 0 {
            for j in 0..<arrOfPests.count{
                
                arrOfPestSummary.append("Pest Summary")
                
                let objArea = arrOfPests[j] as! NSManagedObject
                
                let strTargetName = objArea.value(forKey: "targetName") as? String ?? ""
                let strParenAreaname = objArea.value(forKey: "parentAreaName") as? String ?? ""
                
                let pestStatus = "\(objArea.value(forKey: "pestStatus") ?? "")"
                var strSubTitle = String()
                
                if pestStatus == "1" {
                    
                    strSubTitle = "\(objArea.value(forKey: "quantity") ?? "")  Captured"
                    
                } else if pestStatus == "2" {
                    
                    strSubTitle = "\(objArea.value(forKey: "quantity") ?? "")  Sighted"
                    
                } else{
                    
                    strSubTitle = "Evidence: \(objArea.value(forKey: "evidence") ?? "")"
                    
                }
                print(strSubTitle)
                var arrOfNewPest = [String]()
                arrOfNewPest = [strTargetName, strParenAreaname, strSubTitle]
                arrOfTblPestSummary.append(arrOfNewPest)
                
            }
        }
        
        if arrOfPestSummary.count > 0 {
            arrOfTblViewCells.insert(contentsOf: arrOfPestSummary, at: 3 + arrOfdeviceInspectionSummary.count + 1)
        }
        
        arrOfServices.removeAll()
        if objServiceCategoryDetail.count > 0{
            for i in 0..<objServiceCategoryDetail.count{
                arrOfServicesCount.append("Services")
                
                let obj = objServiceCategoryDetail[i] as! NSManagedObject
                
                let strServiceName = obj.value(forKey: "serviceName") as? String ?? ""
                let strServicePrice = obj.value(forKey: "servicePrice") as? String ?? ""
                let strServiceQuantity = obj.value(forKey: "serviceQuantity") as? String ?? ""
                let strServiceId =  obj.value(forKey: "serviceId") as? String ?? ""
                arrOfServices.append([strServiceName, strServicePrice, strServiceQuantity, strServiceId])
                
            }
        }
        print(arrOfServices)
        if arrOfServicesCount.count > 0 {
            arrOfTblViewCells.insert(contentsOf: arrOfServicesCount, at: arrOfPestSummary.count + arrOfdeviceInspectionSummary.count + 5)
        }
        
        tblView.reloadData()
        fetchPaymentInfoDetailFromCoreData()
    }
    
    func getTheTax(){
        
        nonTaxableTotal = Float()
        taxableTotal = Float()

        let isTaxExampt = "\(objWorkorderDetail.value(forKey: "isTaxExampt") ?? "")"
        
        let strTaxPercentage = "\(objWorkorderDetail.value(forKey: "taxPercent") ?? "0.00")"
        
        let floatTaxPercentage : Float = NSString(string: strTaxPercentage).floatValue
        
        if isTaxExampt == "false" || isTaxExampt == "0" {
            if objServiceCategoryDetail.count > 0{
                for i in 0..<objServiceCategoryDetail.count{
                    
                    let obj = objServiceCategoryDetail[i] as! NSManagedObject
                    
                    let strServicePrice = obj.value(forKey: "servicePrice") as? String ?? ""
                    let strServiceQuantity = obj.value(forKey: "serviceQuantity") as? String ?? ""
                    let strServiceTaxable =  obj.value(forKey: "taxable") as? String ?? ""
                    
                    if strServiceTaxable == "1" || strServiceTaxable == "true" {
                        let priceCrossQuantiy = Float(strServicePrice)! * Float(strServiceQuantity)!
                        
                        var taxAppliedPrice = priceCrossQuantiy * floatTaxPercentage
                        taxAppliedPrice = taxAppliedPrice / 100
                        
                        self.taxableTotal = taxableTotal + Float(taxAppliedPrice)
                    }
                    let priceCrossQuantiy = Float(strServicePrice)! * Float(strServiceQuantity)!
                    self.nonTaxableTotal = nonTaxableTotal + priceCrossQuantiy
                }
                
                print("Tax Amount: \(taxableTotal)")
                print("Total Amount: \(nonTaxableTotal)")
            }
        }
        tblView.reloadData()
    }
    
    func fetchServiceDetailsData() {
        
        arrOfPests = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        var arrOfNoDevices = NSArray()
        var arrOfNoArea = NSArray()
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "WOServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
            
            arrOfNoDevices = getDataFromCoreDataBaseArray(strEntity: "WOServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
            
        }else{
            
            arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0"))
            
            arrOfNoArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String , strAddressAreaId: "" , strMobileAddressAreaId: "" , strWoStatus: "InComplete")
            
            arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: "" , strWoStatus: "InComplete" , strToCheckBlankAddressId: "No")
            
        }
        
        arrOfArea = arrOfNoArea
        
        let tempCount = NSMutableArray()
        
        if arrOfArea.count > 0 {
            
            for k in 0 ..< arrOfArea.count {
                
                let obj = arrOfNoArea[k] as! NSManagedObject
                
                // serviceAreaId
                
                if isWoCompleted(objWo: objWorkorderDetail) {
                    
                    let areaTemp = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@ && addressAreaId == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0" , "\(obj.value(forKey: "serviceAreaId") ?? "")"))
                    
                    if areaTemp.count > 0 {
                        
                        let objTemp = areaTemp[0] as! NSManagedObject
                        
                        let isInspected = objTemp.value(forKey: "isInspected") as! Bool
                        
                        if isInspected {
                            
                            tempCount.add(obj)
                            
                        }
                        
                    }
                    
                }else{
                    
                    let isInspected = obj.value(forKey: "isInspected") as! Bool
                    
                    if isInspected {
                        
                        tempCount.add(obj)
                        
                    }
                    
                }
                
            }
            
        }
        
        strNoOfArea = "\(arrOfNoArea.count)"
        strNoOfAreaInspected = "\(tempCount.count)"
        
        let tempDeviceTypeId = NSMutableArray()
        
        if arrOfNoDevices.count > 0 {
            
            for k in 0 ..< arrOfNoDevices.count {
                
                let obj = arrOfNoDevices[k] as! NSManagedObject
                
                if isWoCompleted(objWo: objWorkorderDetail) {
                    
                    let deviceTemp = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedDevice == %@ && deviceId == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0" , "\(obj.value(forKey: "serviceDeviceId") ?? "")"))
                    
                    if deviceTemp.count > 0 {
                        
                        let objTemp = deviceTemp[0] as! NSManagedObject
                        
                        let deviceTypeId = "\(objTemp.value(forKey: "devieTypeId") ?? "")"
                        
                        if tempDeviceTypeId.contains(deviceTypeId) {
                            
                            
                        }else{
                            
                            tempDeviceTypeId.add(deviceTypeId)
                            
                        }
                        
                    }
                    
                }else{
                    
                    let deviceTypeId = "\(obj.value(forKey: "devieTypeId") ?? "")"
                    
                    if tempDeviceTypeId.contains(deviceTypeId) {
                        
                        
                    }else{
                        
                        tempDeviceTypeId.add(deviceTypeId)
                        
                    }
                    
                }
                
            }
            
        }
        
        arrOfDeviceType = tempDeviceTypeId
        
        var hieght = (arrOfPests.count*112) + (arrOfDeviceType.count*112)
        
        if arrOfNoArea.count > 0 {
            
            hieght = hieght + 82
            
        }
        
        //Services
        objServiceCategoryDetail.removeAllObjects()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objServiceCategoryDetail.add(dict)
            }
        }
        
        self.createArrOfTblView()
        
    }
    
    
    func downloadServiceAddImage()
    {
        
        let index = IndexPath(item: 0, section: 0)
        let cell = self.tblView.cellForRow(at: index) as! GeneralHeaderCell
        
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strServiceAddImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strServiceAddImageName)
        
        if isImageExists!
        {
            cell.imgView.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strServiceAddImageUrl = String()
            
            strServiceAddImageUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            //let strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            
            var strUrl = String()//= strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            
            if strServiceAddImageName.contains("Documents")
            {
                strUrl = strServiceAddImageUrl  + strServiceAddImageName
            }
            else
            {
                strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            }
            
            strUrl  = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            let nsUrl = URL(string: strUrl)
            
            cell.imgView.load(url: nsUrl! as URL , strImageName: strServiceAddImageName)
        }
    }
    
}

extension InvoiceVC: UITextFieldDelegate, UITextViewDelegate{
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.tag != 0{
            
            let index = IndexPath(item: textView.tag, section: 0)
            let cell = self.tblView.cellForRow(at: index) as! OfficeNotesInvoiceCell
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("officeNotes")
            arrOfValues.add(cell.lblOfficeDescription.text ?? "")
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
            self.tblView.reloadData()
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let index = IndexPath(item: indexForTakePayment, section: 0)
        let cell = self.tblView.cellForRow(at: index) as! TakePaymentInvoiceCell
        
        strAmount = cell.txtAmount.text!
        strCheckNo = cell.txtCheck.text!
        strDrivingLicences = cell.txtDrivingLicense.text!
        //  strExpiryDate = cell.txtExpirationDate.text!
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        yesEditedSomething = true
        let index = IndexPath(item: indexForTakePayment, section: 0)
        let cell = self.tblView.cellForRow(at: index) as! TakePaymentInvoiceCell
        
        if ( textField == cell.txtDrivingLicense || textField == cell.txtCheck  )
        {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
            
        }
        
        else if ( textField == cell.txtAmount  )
        {
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            
        }
        else
        {
            
            return true
            
        }
    }
}

extension InvoiceVC{
    //Payment Stuff
    func fetchPaymentInfoDetailFromCoreData()
    {
        
        let arryOfWorkOrderPaymentData = getDataFromCoreDataBaseArray(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfWorkOrderPaymentData.count>0
        {
            let objPaymentInfoData = arryOfWorkOrderPaymentData[0] as! NSManagedObject
            paymentInfoDetail = objPaymentInfoData
            strPaymentModes = paymentInfoDetail.value(forKey: "paymentMode")  as! String
            strPaidAmount = paymentInfoDetail.value(forKey: "paidAmount") as! String
            
            strAmount = strPaidAmount
            
        }
        else
        {
            savePaymentInfoInCoreData()
            fetchPaymentInfoDetailFromCoreData()
        }
        
        
        if strPaymentModes == "Cash"
        {
            cashPaymentMode()
        }
        else if strPaymentModes == "Check"
        {
            checkPaymentMode()
        }
        else if strPaymentModes == "CreditCard" ||   strPaymentModes == "Credit Card"
        {
            creditCardPaymentMode()
        }
        else if strPaymentModes == "AutoChargeCustomer" ||  strPaymentModes == "Auto Charge Customer"
        {
            autoChargeCustomerPaymentMode()
        }
        else if strPaymentModes == "NoCharge" ||  strPaymentModes == "No Charge"
        {
            noChargePaymentMode()
        }
        else if strPaymentModes == "NoBill" ||  strPaymentModes == "No Bill"
        {
            noBillPaymentMode()
        }
        else if strPaymentModes == "PaymentPending" ||  strPaymentModes == "Payment Pending"
        {
            paymentPendingPaymentMode()
        }
        else
        {
            cashPaymentMode()
        }
        
        strCheckNo =  "\(paymentInfoDetail.value(forKey: "checkNo") ?? "")"
        strDrivingLicences = "\(paymentInfoDetail.value(forKey: "drivingLicenseNo") ?? "")"
        strExpiryDate = "\(paymentInfoDetail.value(forKey: "expirationDate") ?? "")"
        
        tblView.reloadData()
    }
    
    func savePaymentInfoInCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = ["CheckBackImagePath",
                     "CheckFrontImagePath",
                     "CheckNo",
                     "CreatedBy",
                     "CreatedDate",
                     "DrivingLicenseNo",
                     "ExpirationDate",
                     "ModifiedBy",
                     "ModifiedDate",
                     "PaidAmount",
                     "PaymentMode",
                     "WoPaymentId"]
        
        arrOfValues = ["",
                       "",
                       "",
                       strUserName,
                       global.getCurrentDate(),
                       "",
                       "" ,
                       strUserName,
                       global.getCurrentDate(),
                       "",
                       "Cash",
                       ""]
        
        saveDataInDB(strEntity: "PaymentInfoServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    func cashPaymentMode()
    {
        selectedPaymentMode = 0
        strGlobalPaymentMode = "Cash"
    }
    
    func checkPaymentMode()
    {
       
        strGlobalPaymentMode = "Check"
        selectedPaymentMode = 1
        
    }
    func creditCardPaymentMode()
    {
        strGlobalPaymentMode = "CreditCard"
        selectedPaymentMode = 2
        
    }
    func noBillPaymentMode()
    {
        strGlobalPaymentMode = "Bill"
        selectedPaymentMode = 3
        
    }
    func autoChargeCustomerPaymentMode()
    {
        strGlobalPaymentMode = "AutoChargeCustomer"
        selectedPaymentMode = 4
        
    }
    func paymentPendingPaymentMode()
    {
        strGlobalPaymentMode = "PaymentPending"
        selectedPaymentMode = 5
    }
    func noChargePaymentMode()
    {
        strGlobalPaymentMode = "NoCharge"
        selectedPaymentMode = 6
    }
    
    func updatePaymentInfo()
    {
        // Update Payment Info
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("paymentMode")
        arrOfKeys.add("paidAmount")
        arrOfKeys.add("checkNo")
        arrOfKeys.add("drivingLicenseNo")
        arrOfKeys.add("expirationDate")
        arrOfKeys.add("checkFrontImagePath")
        arrOfKeys.add("checkBackImagePath")
        
        
        if strGlobalPaymentMode == "NoCharge" ||  strGlobalPaymentMode == "No Charge"
        {
            arrOfValues.add("NoCharge")

        }
        else if strGlobalPaymentMode == "AutoChargeCustomer" || strGlobalPaymentMode == "Auto Charge Customer"{
            arrOfValues.add("AutoChargeCustomer")

        }
        else if strGlobalPaymentMode == "PaymentPending" || strGlobalPaymentMode == "Payment Pending" {
            arrOfValues.add("PaymentPending")

        }
        else if strGlobalPaymentMode == "Credit Card" || strGlobalPaymentMode == "Credit Card"{
            arrOfValues.add("CreditCard")
        }
        else
        {
            arrOfValues.add(strGlobalPaymentMode)
        }

        arrOfValues.add(strAmount)
        arrOfValues.add(strCheckNo)
        arrOfValues.add(strDrivingLicences)
        arrOfValues.add(strExpiryDate)
        arrOfValues.add(strCheckFrontImage)
        arrOfValues.add(strCheckBackImage)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    func checkImage() -> Bool
    {
        
        let chkImage = nsud.bool(forKey: "isCompulsoryAfterImageService")
        
        if chkImage == true
        {
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, "After"))
            
            if arryOfData.count > 0
            {
                return false
            }
            else
            {
                return true
            }
            
        }
        else
        {
            
            return false
            
        }
        
    }
    
    func finalSave(strPaymentMode: String)
    {
        if strGlobalPaymentMode == "Cash" || strGlobalPaymentMode == "Check" || strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
        {
            
            let valTextAmount = Float(strAmount)
            
            if strAmount == "" || strAmount == "0" || strAmount == "00.00" ||  (Double(strAmount) == nil || valTextAmount == 0  )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter amount", viewcontrol: self)
            }
            
            else
            {
                if chkCustomerNotPresent == true
                {
                    if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                        
                    }
                    
                    else if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    }
                    else if ((strServiceReportType.count == 0) && (arrOfServiceReportType.count > 0))
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: Alert_SelectServiceReportType, viewcontrol: self)
                    }
                    else
                    {
                        if strGlobalPaymentMode == "Check"
                        {
                            if (strCheckNo == "" )
                            {
                                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check no", viewcontrol: self)
                            }
                            else
                            {
                                updatePaymentInfo()
                                updateWorkOrderDetail()
                            }
                        }else{
                            
                            updatePaymentInfo()
                            updateWorkOrderDetail()
                            
                        }
                    }
                    
                }
                else
                {
                    if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                        
                    }
                    //                    else if strCustomerSign.count == 0
                    //                    {
                    //                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                    //
                    //                    }
                    else if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    }
                    else if ((strServiceReportType.count == 0) && (arrOfServiceReportType.count > 0))
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: Alert_SelectServiceReportType, viewcontrol: self)
                    }
                    else
                    {
                        
                        if strGlobalPaymentMode == "Check"
                        {
                            if (strCheckNo == "" )
                            {
                                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check no", viewcontrol: self)
                            }
                            else
                            {
                                updatePaymentInfo()
                                updateWorkOrderDetail()
                            }
                        }else{
                            
                            updatePaymentInfo()
                            updateWorkOrderDetail()
                            
                        }
                        
                    }
                }
            }
            
        }
        else if strGlobalPaymentMode == "Bill" || strGlobalPaymentMode == "NoCharge" || strGlobalPaymentMode == "AutoChargeCustomer" || strGlobalPaymentMode == "PaymentPending" || strGlobalPaymentMode == "No Charge" || strGlobalPaymentMode == "Auto Charge Customer" || strGlobalPaymentMode == "Payment Pending"
        {
            if chkCustomerNotPresent == true
            {
                if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                else if checkImage() == true
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                }
                else if ((strServiceReportType.count == 0) && (arrOfServiceReportType.count > 0))
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: Alert_SelectServiceReportType, viewcontrol: self)
                }
                else
                {
                    updatePaymentInfo()
                    updateWorkOrderDetail()
                }
            }
            else
            {
                if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                //                else if strCustomerSign.count == 0
                //                {
                //                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                //
                //                }
                else if checkImage() == true
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                }
                else if ((strServiceReportType.count == 0) && (arrOfServiceReportType.count > 0))
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: Alert_SelectServiceReportType, viewcontrol: self)
                }
                else
                {
                    updatePaymentInfo()
                    updateWorkOrderDetail()
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select Payment Mode", viewcontrol: self)
        }
        
        
    }
    
    func updateWorkOrderDetail()
    {
        // Update Payment Info
        
        if chkCustomerNotPresent == true
        {
            strCustomerSign = ""
        }
        
        //let isCumpulsory = nsud.bool(forKey: "isCompulsoryAfterImageService") as! Bool
        
        var strWorkOrderStatusFinal = String()
        strWorkOrderStatusFinal = "InComplete"
        //strWorkOrderStatusFinal = "Completed"

        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let date = Date()
        var strTimeOut = String()
        strTimeOut = dateFormatter.string(from: date)
        
        var coordinate = CLLocationCoordinate2D()
        coordinate = Global().getLocation()
        let strTimeOutLat = "\(coordinate.latitude)"
        let strTimeOutLong = "\(coordinate.longitude)"
        
        
        var strResendStatus = String()
        strResendStatus = "0"
        if strWorkOrderStatus == "Incomplete"
        {
            strResendStatus = "0"
        }
        
        
        var isElementIntegraiton = Bool()
        isElementIntegraiton = Bool("\((dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsElementIntegration")) ?? "false")") ?? false
        if isElementIntegraiton == true
        {
            if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
            {
                strWorkOrderStatusFinal = "InComplete"
            }
        }
        
        var strCustomerNotPresent = String()
        
        if chkCustomerNotPresent == false
        {
            strCustomerNotPresent = "false"
        }
        else
        {
            strCustomerNotPresent = "true"
        }
        
        var strPresetStatus = String()
        
        if isPreSetSignGlobal == true
        {
            strPresetStatus = "true"
        }
        else
        {
            strPresetStatus = "false"
        }
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strServiceJobTitle = ""
        var strServiceJobTitleId = ""
        
        //Commenting job title
        //        if btnServiceJobTitle.titleLabel?.text == strSelectString {
        //
        //            strServiceJobTitle = ""
        //            strServiceJobTitleId = ""
        //
        //        }else{
        //
        //            strServiceJobTitle = (btnServiceJobTitle.titleLabel?.text)!
        //            strServiceJobTitleId = "\(dictOfServiceJobDescriptionSelected.value(forKey: "ServiceJobDescriptionId") ?? "")"
        //
        //        }
        strTotalDue = strTotalDue.replacingOccurrences(of: "$", with: "")
        strTax = strTax.replacingOccurrences(of: "$", with: "")

        arrOfKeys = ["technicianSignaturePath",
                     "customerSignaturePath",
                     "audioFilePath",
                     "timeOut",
                     "timeOutLatitude",
                     "timeOutLongitude",
                     "isResendInvoiceMail",
                     "workorderStatus",
                     "isCustomerNotPresent",
                     "IsRegularPestFlow",
                     "isEmployeePresetSignature",
                     "serviceJobDescription",
                     "serviceReportFormat",
                     "serviceJobDescriptionId",
                     "invoiceAmount",
                     "productionAmount",
                     "tax"
        ]
        //strWorkOrderStatusFinal = "InCompleted"
        arrOfValues = [strTechnicianSign,
                       strCustomerSign,
                       strAudioName,
                       strTimeOut,
                       strTimeOutLat,
                       strTimeOutLong,
                       strResendStatus,
                       strWorkOrderStatusFinal,
                       strCustomerNotPresent,
                       "true",
                       strPresetStatus,
                       strServiceJobTitle,
                       strServiceReportType,
                       strServiceJobTitleId,
                       strTotalDue,
                       strTotalDue,
                       strTax
                       
        ]
        
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            if yesEditedSomething
            {
                updateModifyDate()
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            if isElementIntegraiton == true
            {
                if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
                {
                    print("Go to credit card ciew")
                    goToCreditCardScreen()
                }
                else
                {
                    print("Go to send mail")
                    goToSendEmailScreen()
                }
            }
            else
            {
                
                print("Go to send mail")
                goToSendEmailScreen()
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    func goToSendEmailScreen()
    {
        
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let objSendMail = storyboardIpad.instantiateViewController(withIdentifier: "SendEmailVC") as? SendEmailVC
        var strValue = String()
        var strFromWheree = NSString()
        strFromWheree = "PestInvoice"
        
        if chkCustomerNotPresent == true
        {
            strValue = "no"
        }
        else
        {
            strValue = "yes"
        }
        objSendMail?.isCustomerPresent = strValue
        objSendMail?.strIncompleteSyncForPDF = isPreview
        objSendMail?.fromWhere = strFromWheree as String
        objSendMail?.strWoId = strWoId
        objSendMail?.isPreview = isPreview
        self.navigationController?.pushViewController(objSendMail!, animated: false)
        
        
        //        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        //        let objSendMail = storyboardIpad.instantiateViewController(withIdentifier: "ServiceSendMailViewController") as? ServiceSendMailViewController
        //
        //        var strValue = String()
        //        var strFromWheree = NSString()
        //        strFromWheree = "PestInvoice"
        //
        //        if chkCustomerNotPresent == true
        //        {
        //            strValue = "no"
        //        }
        //        else
        //        {
        //            strValue = "yes"
        //        }
        //        objSendMail?.isCustomerPresent = strValue as NSString
        //        objSendMail?.fromWhere = strFromWheree as String
        //        //self.navigationController?.pushViewController(objSendMail!, animated: false)
        //        self.navigationController?.pushViewController(objSendMail!, animated: false)
        
    }
    
    func goToCreditCardScreen()
    {
        
        if isInternetAvailable() == true
        {
            let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
            let objCreditCardView = storyboardIpad.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
            
            var strValue = String()
            if chkCustomerNotPresent == true
            {
                strValue = "no"
            }
            else
            {
                strValue = "yes"
            }
            objCreditCardView?.isCustomerPresent = strValue as NSString
            objCreditCardView?.strAmount = strAmount
            objCreditCardView?.strGlobalWorkOrderId = strWoId as String
            objCreditCardView?.strTypeOfService = "service"
            objCreditCardView?.workOrderDetailNew = objWorkorderDetail
            objCreditCardView?.strFromWhichView = "PestFlow"
            objCreditCardView?.fromWhere = "PestInvoice"
            self.navigationController?.pushViewController(objCreditCardView!, animated: false)
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    @objc func actionOnServiceAutoAddressClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let strAddress = global.strCombinedAddressBilling(for: objWorkorderDetail)
            
            if strAddress!.count > 0
            {
                
                let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
                // Create the actions
                
                let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    Global().redirect(onAppleMap: self, strAddress)
                    
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    //Deepak.s
    @IBAction func action_on_printPreview(_ sender: Any) {
        isPreview = true
        updatePaymentInfo()
        updateWorkOrderDetail()
    }
}

extension InvoiceVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            if chkFrontImage
            {
                strCheckFrontImage = "\\Documents\\UploadImages\\Img"  + "CheckFrontImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckFrontImage, image: pickedImage)
            }
            else
            {
                strCheckBackImage = "\\Documents\\UploadImages\\Img"  + "CheckBackImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckBackImage, image: pickedImage)
            }
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
}
extension InvoiceVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfTblViewCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrOfTblViewCells[indexPath.row] == "General Info Header"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralHeaderCell") as! GeneralHeaderCell
            
            strWorkOrderStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
            
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                cell.btnEditHeaderInfo.isHidden = true
                self.btnGallery.isEnabled  = false
                self.btnGraph.isEnabled = false
                self.btnStartTime.isEnabled = false
                
                self.btnGallery.setBackgroundImage(UIImage(named: "galleryGray"), for: .normal)
                self.btnGraph.setBackgroundImage(UIImage(named: "staticsGray"), for: .normal)
                
                btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
                btnChecklist.layer.borderWidth = 1.0
                btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)
                
            }
            else{
                cell.btnEditHeaderInfo.isHidden = false
                self.btnGallery.isEnabled  = true
                self.btnGraph.isEnabled = true
                self.btnStartTime.isEnabled = true
                
                
                btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
                btnChecklist.layer.borderWidth = 1.0
                btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)
                
                self.btnGallery.setBackgroundImage(UIImage(named: "gallery"), for: .normal)
                self.btnGraph.setBackgroundImage(UIImage(named: "statics"), for: .normal)
                
            }
            
            if (strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset"){
                
                btnChecklist.layer.borderColor = UIColor(hex: "9A9A9A")?.cgColor
                btnChecklist.layer.borderWidth = 1.0
                btnChecklist.setTitleColor(hexStringToUIColor(hex: "9A9A9A"), for: .normal)
                
                btnChecklist.isEnabled = false
            }
            
            let isBillingAddressSameAsServiceAddress = "\(objWorkorderDetail.value(forKey: "isBillingAddressAsSameServiceAddress") ?? "")"
            
            if isBillingAddressSameAsServiceAddress == "1" || isBillingAddressSameAsServiceAddress == "true" || isBillingAddressSameAsServiceAddress == "True" {
                
                cell.btnBillingAddressSameAsService.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
                
            }else{
                
                cell.btnBillingAddressSameAsService.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
                
            }
            
            let strIsCollectPayment = "\(objWorkorderDetail.value(forKey: "isCollectPayment") ?? "")"
            
            if strIsCollectPayment == "1" || strIsCollectPayment == "true" || strIsCollectPayment == "True" {
                
                isCollectPayment = true
                cell.btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
                
            }else{
                
               isCollectPayment = false
                cell.btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
                
            }
            cell.btnCollectPayment.isUserInteractionEnabled = false
       //     cell.btnCollectPayment.addTarget(self, action: #selector(btnCollectPaymentAction), for: .touchUpInside)
//            cell.lblServiceSubTitle.text = "\(objWorkorderDetail.value(forKey: "Services") ?? "")"

            cell.btnOpenAdditonalInfo.addTarget(self, action: #selector(btnOpenInfoViewAction), for: .touchUpInside)
            cell.btnOpenBillingPOCInfo.addTarget(self, action: #selector(btnOpenInfoViewAction), for: .touchUpInside)

            cell.btnEditHeaderInfo.addTarget(self, action: #selector(btnEditHeaderInfoAction), for: .touchUpInside)
             
            cell.btnOpenTimeSheet.addTarget(self, action:  #selector(btnOpenTimeSheetAction), for: .touchUpInside)
            
            if isBtnOpenAdditionalInoClciked == true{
                cell.btnOpenAdditonalInfo.backgroundColor = hexStringToUIColor(hex: "FFFFFF")
                cell.btnOpenBillingPOCInfo.backgroundColor = UIColor.hexStringToColor(hex: "E9E9E9")
                cell.btnOpenAdditonalInfo.tintColor = hexStringToUIColor(hex: "000000")
                cell.btnOpenBillingPOCInfo.tintColor = hexStringToUIColor(hex: "000000")
                
                cell.viewOfAdditionalInfo.isHidden = false
                cell.viewOfPOCAdditionalInfo.isHidden = true
            }
            else if isBtnOpenAdditionalInoClciked == false{
                
                cell.btnOpenAdditonalInfo.backgroundColor = hexStringToUIColor(hex: "E9E9E9")
                cell.btnOpenBillingPOCInfo.backgroundColor = UIColor.hexStringToColor(hex: "FFFFFF")
                cell.btnOpenAdditonalInfo.tintColor = hexStringToUIColor(hex: "000000")
                cell.btnOpenBillingPOCInfo.tintColor = hexStringToUIColor(hex: "000000")
                
                cell.viewOfAdditionalInfo.isHidden = true
                cell.viewOfPOCAdditionalInfo.isHidden = false
            }
            
        
            strDepartmentType = "\(objWorkorderDetail.value(forKey: "departmentType") ?? "")"
            
            strWorkOrderStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
            
            nsud.set(strWorkOrderStatus, forKey: "workOrderStatus")
            nsud.synchronize()
            
            strServicePocId = "\(objWorkorderDetail.value(forKey: "servicePOCId") ?? "")"
            strBillingPocId = "\(objWorkorderDetail.value(forKey: "billingPOCId") ?? "")"
            
            
            //Service Address Image Download
            strServiceAddImageName = "\(objWorkorderDetail.value(forKey: "serviceAddressImagePath") ?? "")"
            
            
            if strServiceAddImageName.count > 0
            {
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strServiceAddImageName)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strServiceAddImageName)
                
                if isImageExists!
                {
                    cell.imgView.image = image
                }
                else
                {
                    //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
                    
                    var strServiceAddImageUrl = String()
                    
                    strServiceAddImageUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
                    
                    //let strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    
                    var strUrl = String()//= strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    
                    if strServiceAddImageName.contains("Documents")
                    {
                        strUrl = strServiceAddImageUrl  + strServiceAddImageName
                    }
                    else
                    {
                        strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    }
                    
                    strUrl  = strUrl.replacingOccurrences(of: "\\", with: "/")
                    
                    //let nsUrl = URL(fileURLWithPath: strUrl)
                    
                    cell.imgView.imageFromServerURL(urlString: strUrl)
                    // cell.imgView.load(url: nsUrl, strImageName: strServiceAddImageName)
                    
                }
            }
            
            let str = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
            let strDate  = changeStringDateToGivenFormatWithoutT(strDate: str, strRequiredFormat: "MM/dd/yyyy hh:mm a")
            cell.lblStartTime.text = strDate
            cell.lblEndTime.text = strGlobalEndTime
            cell.lblDuration.text = finalDurationHHMM
            
            let strName = global.strFullName(for: objWorkorderDetail)
            if strName != ""{
                cell.btnUserName.setTitle(strName, for: .normal)
                cell.heightConstraintForUsername.constant = 17
                cell.viewUsername.alpha = 1
                
            }
            else{
                cell.heightConstraintForUsername.constant = 0
                cell.viewUsername.alpha = 0
                
            }
            
            let strOfficeName = (objWorkorderDetail.value(forKey: "companyName")) as! String
            if strOfficeName != ""{
                cell.btnOfficeName.setTitle((strOfficeName), for: .normal)
                cell.heightConstraintForOffice.constant = 17
                cell.viewOffice.alpha = 1
            }
            else{
                cell.heightConstraintForOffice.constant = 0
                cell.viewOffice.alpha = 0
            }
            
            if "\(objWorkorderDetail.value(forKey: "primaryEmail") ?? "")" == "" &&  "\(objWorkorderDetail.value(forKey: "secondaryEmail") ?? "")" == ""{
                cell.lblAdditionalInfoEmailIcon.text = ""
            }
            else{
                cell.lblAdditionalInfoEmailIcon.text = "@"
            }
            
            
            if "\(objWorkorderDetail.value(forKey: "primaryPhone") ?? "")" == "" &&  "\(objWorkorderDetail.value(forKey: "secondaryPhone") ?? "")" == ""{
                cell.imgAdditionalInfoPhoneIcon.image = nil
            }
            else{
                cell.imgAdditionalInfoPhoneIcon.image = UIImage(named: "phone")
            }
            
            
            let strEmailName = (objWorkorderDetail.value(forKey: "primaryEmail")) as! String
            if strEmailName != ""{
                cell.btnEmail.setTitle((strEmailName), for: .normal)
                cell.heightConstraintForEmail.constant = 17
                cell.viewEmail.alpha = 1
                
            }
            else{
                cell.heightConstraintForEmail.constant = 0
                cell.viewEmail.alpha = 0
                
            }
            
            
            let strPrimaryPhoneNumber = (objWorkorderDetail.value(forKey: "primaryPhone"))  as! String
            let strCellNumber = (objWorkorderDetail.value(forKey: "cellNo"))  as! String
            
            if strCellNumber != ""
            {
                cell.btnPhone.setTitle(formattedNumber(number: strCellNumber), for: .normal)
                cell.heightConstraintForPhone.constant = 17
                cell.viewPhone.alpha = 1
                
            }
            else if strPrimaryPhoneNumber != ""
            {
                cell.btnPhone.setTitle((formattedNumber(number: strPrimaryPhoneNumber)), for: .normal)
                cell.heightConstraintForPhone.constant = 17
                cell.viewPhone.alpha = 1
                
            }
            else{
                cell.heightConstraintForPhone.constant = 0
                cell.viewPhone.alpha = 0
                
            }
            
            let strMarker = (objWorkorderDetail.value(forKey: "keyMap")) as! String
            if strMarker != ""{
                cell.btnMarker.setTitle((strMarker), for: .normal)
                cell.heightConstraintForMarker.constant = 17
                cell.viewMarker.alpha = 1
                
            }
            else{
                cell.heightConstraintForMarker.constant = 0
                cell.viewMarker.alpha = 0
                
            }
            
            let strRoute = (objWorkorderDetail.value(forKey: "routeNo")) as! String
            if strRoute != ""{
                cell.btnRoute.setTitle(("Route: " + strRoute), for: .normal)
                cell.heightConstraintForRoute.constant = 17
                cell.viewRoute.alpha = 1
                
            }
            else{
                cell.heightConstraintForRoute.constant = 0
                cell.viewRoute.alpha = 0
                
            }
            
            let strTruck = (objWorkorderDetail.value(forKey: "driveTimeStr")) as! String
            if strTruck != ""{
                
                cell.btnTruck.setTitle("Drive: " + strTruck, for: .normal)
                cell.heightConstraintForTruck.constant = 17
                cell.viewTruck.alpha = 1
                
            }
            else{
                cell.heightConstraintForTruck.constant = 0
                cell.viewTruck.alpha = 0
                
            }
            
            
            let strPEmail = (objWorkorderDetail.value(forKey: "primaryEmail"))  as! String
            if strPEmail != ""
            {
                cell.lblPrimaryEmail.text = strPEmail
                
            }
            else{
                cell.lblPrimaryEmail.text = ""
                
            }
            
            let strSEmail = (objWorkorderDetail.value(forKey: "secondaryEmail"))  as! String
            if strSEmail != ""
            {
                cell.lblSecondaryEmail.text = strSEmail
                
            }
            else{
                cell.lblSecondaryEmail.text = ""
                
            }
            
            let strPPhoneName = (objWorkorderDetail.value(forKey: "primaryPhone"))  as! String
            if strPPhoneName != ""
            {
                cell.lblPrimaryContactNumber.text  = formattedNumber(number: strPPhoneName)
                
            }
            else{
                cell.lblPrimaryContactNumber.text = ""
                
            }
            
            let strSPhoneName = (objWorkorderDetail.value(forKey: "secondaryPhone"))  as! String
            if strSPhoneName != ""
            {
                cell.lblSecondaryContactNumber.text  = formattedNumber(number: strSPhoneName)
                
            }
            else{
                cell.lblSecondaryContactNumber.text = ""
                
            }
            let strSchoolDistrict = (objWorkorderDetail.value(forKey: "billingSchoolDistrict"))  as! String
            if strSchoolDistrict != ""
            {
                cell.lblSchoolDisctrict.text  = strSchoolDistrict
                
            }
            else{
                cell.lblSchoolDisctrict.text = ""
                
            }
            
            let strCountry = (objWorkorderDetail.value(forKey: "serviceCounty"))  as! String
            let strState = (objWorkorderDetail.value(forKey: "serviceState"))  as! String
            if strCountry != ""
            {
                cell.lblCountry.text  = strCountry + ", " + strState
            }
            else{
                cell.lblCountry.text = ""
                
            }
            
            let strGateCode = (objWorkorderDetail.value(forKey: "serviceGateCode"))  as! String
            if strGateCode != ""
            {
                cell.lblGateCode.text  = strGateCode
                
            }
            else{
                cell.lblGateCode.text = ""
                
            }
            
            var strStartTime = "\(objWorkorderDetail.value(forKey: "scheduleStartDateTime") ?? "")"
            
            if (strStartTime.count) > 0
            {
                strStartTime =  changeStringDateToGivenFormat(strDate:  strStartTime, strRequiredFormat: "MM/dd/yyyy hh:mm a")
            }
            else
            {
                strStartTime =  ""
                
            }
            
            cell.btnAddress.setTitle( global.strCombinedAddressBilling(for: objWorkorderDetail), for: .normal)
            
            cell.btnAddress.addTarget(self, action: #selector(actionOnServiceAutoAddressClick), for: .touchUpInside)
            
            let tapGR = UITapGestureRecognizer(target: self, action: #selector(actionOnServiceAutoAddressClick))
            cell.btnDirection.addGestureRecognizer(tapGR)
            cell.btnDirection.isUserInteractionEnabled = true

            var strThirdPartyAccountNo = String()
            strThirdPartyAccountNo = "\(objWorkorderDetail.value(forKey: "thirdPartyAccountNo") ?? "")"
            if strThirdPartyAccountNo.count > 0
            {
                lblTitle.text = "A/C #: " + strThirdPartyAccountNo + " Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
                
                nsud.set(lblTitle.text, forKey: "lblThirdPartyAccountNo")
                nsud.synchronize()
            }
            else
            {
                lblTitle.text = "A/C #: " + "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")" + " Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
            }
            
            nsud.set(lblTitle.text, forKey: "lblName")
            nsud.set("\(objWorkorderDetail.value(forKey: "accountNo")!)", forKey: "workOrderAccountNo")
            nsud.set("\(objWorkorderDetail.value(forKey: "accountNo")!)", forKey: "AccNoService")
            nsud.set("\(objWorkorderDetail.value(forKey: "departmentId")!)", forKey: "DepartmentIdService")
            nsud.synchronize()
            
            if "\(objWorkorderDetail.value(forKey: "billingPrimaryEmail") ?? "")" == "" &&  "\(objWorkorderDetail.value(forKey: "billingSecondaryEmail") ?? "")" == ""{
                cell.lblEmailIcon.text = ""
            }
            else{
                cell.lblEmailIcon.text = "@"
            }
            
            
            if "\(objWorkorderDetail.value(forKey: "billingPrimaryPhone") ?? "")" == "" &&  "\(objWorkorderDetail.value(forKey: "billingSecondaryPhone") ?? "")" == ""{
                cell.imgPhoneIcon.image = nil
            }
            else{
                cell.imgPhoneIcon.image = UIImage(named: "phone")
            }
            
            cell.lblPOCPrimaryEmail.text  = "\(objWorkorderDetail.value(forKey: "billingPrimaryEmail") ?? "")"
            
            cell.lblPOCSecondaryEmail.text  = "\(objWorkorderDetail.value(forKey: "billingSecondaryEmail") ?? "")"
            
            cell.lblPOCPrimaryContactNumber.text  = formattedNumber(number: "\(objWorkorderDetail.value(forKey: "billingPrimaryPhone") ?? "")")
            cell.lblPOCSecondaryContactNumber.text  = formattedNumber(number:"\(objWorkorderDetail.value(forKey: "billingSecondaryPhone") ?? "")")
            
            var strBillingContactName = String()
            strBillingContactName = "\(objWorkorderDetail.value(forKey: "billingFirstName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "billingMiddleName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "billingLastName") ?? "")"
            
            cell.lblNameBillingPOC.text  = strBillingContactName
            cell.lblDesignationPOCBilling.text  = ""
            
            return cell
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Area Summary"{
            
            let areaCell = tableView.dequeueReusableCell(withIdentifier: "AreaSummaryInvoiceCell") as! AreaSummaryInvoiceCell
            
            areaCell.lblTitle.text  = "Area Summary"
            areaCell.lblArea1.text = strNoOfArea + " Areas"
            areaCell.lblArea2.text = strNoOfAreaInspected + " Areas Inspected"
            
            return areaCell
        }
        
        else if arrOfTblViewCells[indexPath.row] == "Device Title"{
            let titleCell = tableView.dequeueReusableCell(withIdentifier: "TitleInvoiceCell") as! TitleInvoiceCell
            
            titleCell.lblTitle.text  = "Device Inspection Summary"
            return titleCell
        }
        else if arrOfTblViewCells[indexPath.row] == "Device Inspection Summary"{
            
            let deviceInspectionCell = tableView.dequeueReusableCell(withIdentifier: "CollectionInvoiceCell") as! CollectionInvoiceCell
            
            
            deviceInspectionCell.lblTitle.text = arrOfCollectionOfDevicesTitle[indexPath.row - 3]//deviceSysNameLocal
            
            deviceInspectionCell.arrOfCollection = arrOfCollectionOfDevices[indexPath.row - 3]
            deviceInspectionCell.collectionView.reloadData()
            
            return deviceInspectionCell
        }
        else if arrOfTblViewCells[indexPath.row] == "Pest Title"{
            let titleCell = tableView.dequeueReusableCell(withIdentifier: "TitleInvoiceCell") as! TitleInvoiceCell
            
            titleCell.lblTitle.text  = "Pest Summary"
            
            return titleCell
        }
        else if arrOfTblViewCells[indexPath.row] == "Pest Summary"{
            //for pest
            let pestCell = tableView.dequeueReusableCell(withIdentifier: "AreaSummaryInvoiceCell") as! AreaSummaryInvoiceCell
            
            let actualIndex = arrOfCollectionOfDevices.count + 4
            pestCell.lblTitle.text  = arrOfTblPestSummary[indexPath.row - actualIndex][0]
            
            if arrOfTblPestSummary[indexPath.row - actualIndex][1] == ""{
                pestCell.lblArea1.text  = "Area"
            }
            else{
                pestCell.lblArea1.text  = arrOfTblPestSummary[indexPath.row - actualIndex][1]
            }
            
            pestCell.lblArea2.text  = arrOfTblPestSummary[indexPath.row - actualIndex][2]
            
            
            return pestCell
        }
        else if arrOfTblViewCells[indexPath.row] == "Service Title"{
            let titleCell = tableView.dequeueReusableCell(withIdentifier: "TitleInvoiceCell") as! TitleInvoiceCell
            
            titleCell.lblTitle.text  = "Services"
            
            return titleCell
        }
        
        else if arrOfTblViewCells[indexPath.row] == "Services" {
            
            let serviceCell = tableView.dequeueReusableCell(withIdentifier: "ServiceAddedInvoiceCell") as! ServiceAddedInvoiceCell
            
            let actualIndex = arrOfCollectionOfDevices.count + arrOfTblPestSummary.count +  5
            
            serviceCell.lblServieName.text = arrOfServices[indexPath.row - actualIndex][0]
            
            let servicePrice = Float(arrOfServices[indexPath.row - actualIndex][1])
            
            if String(describing: servicePrice) != "0.0" ||  String(describing: servicePrice) != ""{
               
                serviceCell.lblServieQantityAndAmount.text = arrOfServices[indexPath.row - actualIndex][2] + "   *   " +  String(describing: servicePrice!)
            }
            else{
                
                serviceCell.lblServieQantityAndAmount.text =  arrOfServices[indexPath.row - actualIndex][2]  + "   *   " +  "0.0"
                
            }
            
           // serviceCell.lblServieQantityAndAmount.text = arrOfServices[indexPath.row - actualIndex][2]  + "   *   " +  arrOfServices[indexPath.row - actualIndex][1]
            
            return serviceCell
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Tax"{
            let billingAndTaxCell = tableView.dequeueReusableCell(withIdentifier: "BillingAndTaxInvoiceCell") as! BillingAndTaxInvoiceCell
            
            
            var total = Double()
            
            total =  Double(nonTaxableTotal + taxableTotal)
                    
            let subtotal = nonTaxableTotal
            
            billingAndTaxCell.lblSubtotalAmount.text = String(format: "%.2f", subtotal)
            
            let amountTax = taxableTotal
            
            billingAndTaxCell.lblTaxAmount.text = String(format: "%.2f", amountTax)
            
            billingAndTaxCell.lblTotalDueAmount.text = String(format: "%.2f", total)
            strTotalDue = billingAndTaxCell.lblSubtotalAmount.text ?? ""
            strTax      = billingAndTaxCell.lblTaxAmount.text ?? ""
            
            return billingAndTaxCell
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Take Payment"{
            let takePaymentCell = tableView.dequeueReusableCell(withIdentifier: "TakePaymentInvoiceCell") as! TakePaymentInvoiceCell
            
            indexForTakePayment = indexPath.row
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                takePaymentCell.btnTakePayment.isEnabled = false
           
                takePaymentCell.txtAmount.isEnabled = false
                takePaymentCell.txtCheck.isEnabled = false
                takePaymentCell.txtDrivingLicense.isEnabled = false
                takePaymentCell.btnExpirationDate.isEnabled = false
                takePaymentCell.btnCheckBackImage.isEnabled = false
                takePaymentCell.btnCheckFrontImage.isEnabled = false
                takePaymentCell.btnTakePayment.setTitle(strGlobalPaymentMode, for: .normal)
                takePaymentCell.btnTakePayment.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
            }
            else{
                
                takePaymentCell.btnTakePayment.isEnabled = true
                takePaymentCell.btnTakePayment.setTitle(strGlobalPaymentMode, for: .normal)
                takePaymentCell.btnTakePayment.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                takePaymentCell.btnTakePayment.addTarget(self, action: #selector(actionOnTakePayment), for: .touchUpInside)

                takePaymentCell.txtAmount.delegate = self
                takePaymentCell.txtCheck.delegate = self
                takePaymentCell.txtDrivingLicense.delegate = self
                takePaymentCell.btnExpirationDate.addTarget(self, action: #selector(actionOnExpirationDate), for: .touchUpInside)
                
                
            
                if selectedPaymentMode == 0 || selectedPaymentMode == 2{
                    takePaymentCell.heightConstaintFortxtAmount.constant = 50
                    takePaymentCell.txtAmount.alpha = 1
                    takePaymentCell.txtAmount.text = strAmount
                }
                
                else if selectedPaymentMode == 1 {
                    takePaymentCell.heightConstaintFortxtAmount.constant = 50
                    takePaymentCell.txtAmount.alpha = 1
                    takePaymentCell.heightConstaintForViewOfAdditionalPaymentInfo.constant = 220
                    takePaymentCell.viewForAdditionalPaymentInfo.alpha = 1
                    
                    takePaymentCell.btnCheckBackImage.addTarget(self, action: #selector(actionOnCheckBackImage), for: .touchUpInside)
                    
                    takePaymentCell.btnCheckFrontImage.addTarget(self, action: #selector(actionOnCheckFrontImage), for: .touchUpInside)
                    
                    takePaymentCell.txtAmount.text = strAmount
                    takePaymentCell.txtCheck.text = strCheckNo
                    takePaymentCell.txtDrivingLicense.text = strDrivingLicences
                    takePaymentCell.txtExpirationDate.text = strExpiryDate
                }
                else{
                    takePaymentCell.heightConstaintFortxtAmount.constant = 0
                    takePaymentCell.txtAmount.alpha = 0
                    takePaymentCell.heightConstaintForViewOfAdditionalPaymentInfo.constant = 0//220
                    takePaymentCell.viewForAdditionalPaymentInfo.alpha = 0
                }
            }
            return takePaymentCell
            
        }
        
        else if arrOfTblViewCells[indexPath.row] == "Tech Comment"{
            let techCommentInvoiceCell = tableView.dequeueReusableCell(withIdentifier: "TechCommentInvoiceCell") as! TechCommentInvoiceCell
            
            techCommentInvoiceCell.lblTitle.text = "Tech Comment"
            
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                techCommentInvoiceCell.btnEditTechComment.isEnabled = false
            }
            else{
                techCommentInvoiceCell.btnEditTechComment.underline()
                techCommentInvoiceCell.btnEditTechComment.addTarget(self, action: #selector(btnTechCommnetAction), for: .touchUpInside)
            }
            
            let strTechCmment = objWorkorderDetail.value(forKey: "technicianComment") as? String
            if strTechCmment != ""{
                techCommentInvoiceCell.lblDescription.text = strTechCmment
            }
            else{
                techCommentInvoiceCell.lblDescription.text =  ""
            }
            return techCommentInvoiceCell
            
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Office Note"{
            let officeNotesInvoiceCell = tableView.dequeueReusableCell(withIdentifier: "OfficeNotesInvoiceCell") as! OfficeNotesInvoiceCell
            
            officeNotesInvoiceCell.lblTitle.text = "Office Note"
            officeNotesInvoiceCell.lblOfficeDescription.delegate = self
            officeNotesInvoiceCell.lblOfficeDescription.tag = indexPath.row
            
            let strOfficeNoteDescription = objWorkorderDetail.value(forKey: "officeNotes") as? String
            if strOfficeNoteDescription != ""{
                officeNotesInvoiceCell.lblOfficeDescription.text = strOfficeNoteDescription
            }
            else{
                officeNotesInvoiceCell.lblOfficeDescription.text = ""
            }
            
            
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                officeNotesInvoiceCell.lblOfficeDescription.isEditable = false
                officeNotesInvoiceCell.btnCustomerNotPresent.isEnabled = false
            }
            else{
                officeNotesInvoiceCell.lblOfficeDescription.isEditable = true
                officeNotesInvoiceCell.btnCustomerNotPresent.isEnabled = true
                officeNotesInvoiceCell.btnCustomerNotPresent.tag = indexPath.row
                officeNotesInvoiceCell.btnCustomerNotPresent.addTarget(self, action: #selector(btnCustomerNotPresent), for: .touchUpInside)
            }
            
            if chkCustomerNotPresent == true
            {
                officeNotesInvoiceCell.btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            }
            else
            {
                officeNotesInvoiceCell.btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
                
            }
            
            return officeNotesInvoiceCell
        }
        else if arrOfTblViewCells[indexPath.row] == "Customer Signature"{
            
            let customerSignatureInvoiceCell = tableView.dequeueReusableCell(withIdentifier: "CustomerSignatureInvoiceCell") as! CustomerSignatureInvoiceCell
            
            customerSignatureInvoiceCell.lblTitle.text = "Customer Signature"
            if imgCustomerSign != nil{
                strCustomerSign = strCustomersignImageName
                customerSignatureInvoiceCell.imgSignature.image = imgCustomerSign
            }
            else if customerSignUrl != nil || strCustomersignImageName != ""{
                
                strCustomerSign = strCustomersignImageName
                
                customerSignatureInvoiceCell.imgSignature.load(url: customerSignUrl! , strImageName: strCustomersignImageName)
                
            }
            else{
                strCustomerSign = ""
                customerSignatureInvoiceCell.imgSignature.image = strDataNotFoundImage
            }
            
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                customerSignatureInvoiceCell.btnAddSignature.isEnabled = false
            }
            else{
                customerSignatureInvoiceCell.btnAddSignature.isEnabled = true
                customerSignatureInvoiceCell.btnAddSignature.tag = 1
                customerSignatureInvoiceCell.btnAddSignature.addTarget(self, action: #selector(actionOnSignature), for: .touchUpInside)
            }
            return customerSignatureInvoiceCell
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Tehnician's Signature"{
            
            let customerSignatureInvoiceCell = tableView.dequeueReusableCell(withIdentifier: "CustomerSignatureInvoiceCell") as! CustomerSignatureInvoiceCell
            
            customerSignatureInvoiceCell.lblTitle.text = "Tehnician's Signature"
            let isPreSetSign = nsud.bool(forKey: "isPreSetSignService")
            if isPreSetSign == true{
                if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
                {
                    customerSignatureInvoiceCell.btnAddSignature.isEnabled = false
                }
                if techSignUrl != nil || strTechsignImageName != ""{
                    strTechnicianSign = strTechsignImageName
                    customerSignatureInvoiceCell.btnAddSignature.isEnabled = false
                    customerSignatureInvoiceCell.imgSignature.load(url: techSignUrl! , strImageName: strTechsignImageName)
                }
            }
            else{
                if imgTechSign != nil{
                    strTechnicianSign = strTechsignImageName
                    customerSignatureInvoiceCell.imgSignature.image = imgTechSign
                }
                else if techSignUrl != nil || strTechsignImageName != ""{
                    strTechnicianSign = strTechsignImageName
                    customerSignatureInvoiceCell.imgSignature.load(url: techSignUrl! , strImageName: strTechsignImageName)
                    
                }
                else{
                    strTechnicianSign = ""
                    customerSignatureInvoiceCell.imgSignature.image = strDataNotFoundImage
                }
                
                if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
                {
                    customerSignatureInvoiceCell.btnAddSignature.isEnabled = false
                }
                else{
                    customerSignatureInvoiceCell.btnAddSignature.tag = 2
                    customerSignatureInvoiceCell.btnAddSignature.addTarget(self, action: #selector(actionOnSignature), for: .touchUpInside)
                }
            }
            return customerSignatureInvoiceCell
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Service Terms"{
            let serviceTermsInvoiceCell = tableView.dequeueReusableCell(withIdentifier: "ServiceTermsInvoiceCell") as! ServiceTermsInvoiceCell
            
            var strTremsnConditions = String()
            strTremsnConditions = ""
            let dictData = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            
            if dictData.value(forKey: "ServiceReportTermsAndConditions") is NSArray
            {
                let arrOfServiceJobDescriptionTemplate = dictData.value(forKey: "ServiceReportTermsAndConditions") as! NSArray
                
                for item in arrOfServiceJobDescriptionTemplate
                {
                    let dict = item as! NSDictionary
                    
                    if ( "\((dict.value(forKey: "DepartmentId"))!)" == "\((objWorkorderDetail.value(forKey: "departmentId"))!)" )
                    {
                        strTremsnConditions = "\(dict.value(forKey: "SR_TermsAndConditions") ?? "")"
                        break
                    }
                    
                }
            }
            
            serviceTermsInvoiceCell.txtDescription.isUserInteractionEnabled = true
            serviceTermsInvoiceCell.txtDescription.attributedText = htmlAttributedString(strHtmlString: strTremsnConditions)
            
            // textViewDidChange(textView: serviceTermsInvoiceCell.txtDescription)
            return serviceTermsInvoiceCell
        }
        
        else if arrOfTblViewCells[indexPath.row] == "Service Report Type"{
            let serviceReportTypeInvoiceCell = tableView.dequeueReusableCell(withIdentifier: "ServiceReportTypeInvoiceCell") as! ServiceReportTypeInvoiceCell
            
            indexForServiceReportType = indexPath.row
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                serviceReportTypeInvoiceCell.btnRadioDefaultWithPricing.isEnabled = false
                serviceReportTypeInvoiceCell.btnRadioDefaultWithoutPricing.isEnabled = false
                serviceReportTypeInvoiceCell.btnRadioSummaryWithPricing.isEnabled = false
                serviceReportTypeInvoiceCell.btnRadioSummaryWithoutPricing.isEnabled = false
            }
            else{
                
                if strServiceReportType == "DefaultWithPricing"{
                    
                    serviceReportTypeInvoiceCell.btnRadioDefaultWithPricing.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioDefaultWithoutPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioSummaryWithPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioSummaryWithoutPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                }
                else if strServiceReportType == "DefaultWithoutPricing"{
                    serviceReportTypeInvoiceCell.btnRadioDefaultWithPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioDefaultWithoutPricing.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioSummaryWithPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioSummaryWithoutPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                }
                else if strServiceReportType == "SummaryWithPricing"{
                    
                    serviceReportTypeInvoiceCell.btnRadioDefaultWithPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioDefaultWithoutPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioSummaryWithPricing.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioSummaryWithoutPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    
                }
                else if strServiceReportType == "SummaryWithoutPricing"{
                    serviceReportTypeInvoiceCell.btnRadioDefaultWithPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioDefaultWithoutPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioSummaryWithPricing.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                    serviceReportTypeInvoiceCell.btnRadioSummaryWithoutPricing.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                }
                
                
                serviceReportTypeInvoiceCell.btnRadioDefaultWithPricing.tag = 1
                serviceReportTypeInvoiceCell.btnRadioDefaultWithPricing.addTarget(self, action: #selector(btnRadioSelectServiceReportType), for: .touchUpInside)
                
                serviceReportTypeInvoiceCell.btnRadioDefaultWithoutPricing.tag = 2
                serviceReportTypeInvoiceCell.btnRadioDefaultWithoutPricing.addTarget(self, action: #selector(btnRadioSelectServiceReportType), for: .touchUpInside)
                
                serviceReportTypeInvoiceCell.btnRadioSummaryWithPricing.tag = 3
                serviceReportTypeInvoiceCell.btnRadioSummaryWithPricing.addTarget(self, action: #selector(btnRadioSelectServiceReportType), for: .touchUpInside)
                
                serviceReportTypeInvoiceCell.btnRadioSummaryWithoutPricing.tag = 4
                serviceReportTypeInvoiceCell.btnRadioSummaryWithoutPricing.addTarget(self, action: #selector(btnRadioSelectServiceReportType), for: .touchUpInside)
            }
            return serviceReportTypeInvoiceCell
        }
        else{
            let titleCell = tableView.dequeueReusableCell(withIdentifier: "TitleInvoiceCell") as! TitleInvoiceCell
            
            return titleCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var heightOfTheCell = CGFloat()
        
        if arrOfTblViewCells[indexPath.row] == "General Info Header"{
           
            return 380
        }
        else if arrOfTblViewCells[indexPath.row] == "Area Summary"{
            heightOfTheCell = 60
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Device Inspection Summary"{
            heightOfTheCell = 60
        }
        else if arrOfTblViewCells[indexPath.row] == "Pest Summary"{
            heightOfTheCell = 60
        }
        else if arrOfTblViewCells[indexPath.row] == "Tax"{
            heightOfTheCell = 90
        }
        else if arrOfTblViewCells[indexPath.row] == "Services" {
            return 30
        }
        else if arrOfTblViewCells[indexPath.row] == "Take Payment"{
            if selectedPaymentMode == 0 || selectedPaymentMode == 2{
                heightOfTheCell = 160
            }
            else if selectedPaymentMode == 1{
                heightOfTheCell = 340
            }
            else{
                heightOfTheCell = 100
            }
        }
        else if arrOfTblViewCells[indexPath.row] == "Tech Comment"{
            heightOfTheCell = UITableView.automaticDimension
        }
        else if arrOfTblViewCells[indexPath.row] == "Office Note"{
            heightOfTheCell = 125
        }
        else if arrOfTblViewCells[indexPath.row] == "Customer Signature"{
            heightOfTheCell = 150
        }
        else if arrOfTblViewCells[indexPath.row] == "Tehnician's Signature"{
            heightOfTheCell = 150
        }
        else if arrOfTblViewCells[indexPath.row] == "Service Terms"{
            heightOfTheCell = 150
        }
        else if arrOfTblViewCells[indexPath.row] == "Service Report Type"{
            heightOfTheCell = 190
        }
        else{
            heightOfTheCell = 35
        }
        return heightOfTheCell
    }
}

extension InvoiceVC: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrOfCreditCardOption.count
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrOfCreditCardOption[row]

    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        selectedPaymentMode = row
        strGlobalPaymentMode = arrOfCreditCardOption[row]
    }
}

class AreaSummaryInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblArea1: UILabel!
    @IBOutlet weak var lblArea2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class CollectionInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrOfCollection = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class TitleInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class ServiceAddedInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblServieName: UILabel!
    @IBOutlet weak var lblServieQantityAndAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class BillingAndTaxInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTaxTitle: UILabel!
    @IBOutlet weak var lblTaxAmount: UILabel!
    
    @IBOutlet weak var lblSubtotalTitle: UILabel!
    @IBOutlet weak var lblSubtotalAmount: UILabel!
    
    @IBOutlet weak var lblTotalDueTitle: UILabel!
    @IBOutlet weak var lblTotalDueAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class TakePaymentInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTakePayment: UILabel!
    @IBOutlet weak var btnTakePayment: UIButton!
    
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtCheck: UITextField!
    @IBOutlet weak var txtDrivingLicense: UITextField!
    @IBOutlet weak var txtExpirationDate: UITextField!
    @IBOutlet weak var btnExpirationDate: UIButton!
    
    @IBOutlet weak var btnCheckFrontImage: UIButton!
    @IBOutlet weak var btnCheckBackImage: UIButton!
    
    @IBOutlet weak var viewForAdditionalPaymentInfo: UIView!
    
    @IBOutlet weak var heightConstaintForViewOfAdditionalPaymentInfo: NSLayoutConstraint!
    @IBOutlet weak var heightConstaintFortxtAmount: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class TechCommentInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnEditTechComment: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class OfficeNotesInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOfficeDescription: UITextView!
    @IBOutlet weak var lblCustomerNotPresent: UILabel!
    @IBOutlet weak var btnCustomerNotPresent: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class CustomerSignatureInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewSignature: UIView!
    @IBOutlet weak var imgSignature: UIImageView!
    @IBOutlet weak var btnAddSignature: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class ServiceTermsInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class ServiceReportTypeInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var lblDefaultWithPricing: UILabel!
    @IBOutlet weak var btnRadioDefaultWithPricing: UIButton!
    
    @IBOutlet weak var lblDefaultWithoutPricing: UILabel!
    @IBOutlet weak var btnRadioDefaultWithoutPricing: UIButton!
    
    @IBOutlet weak var lblSummaryWithPricing: UILabel!
    @IBOutlet weak var btnRadioSummaryWithPricing: UIButton!
    
    @IBOutlet weak var lblSummaryWithoutPricing: UILabel!
    @IBOutlet weak var btnRadioSummaryWithoutPricing: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension InvoiceVC: PaymentInfoDetails
{
    func getPaymentInfo(dictData: String, index: Int) {
       
           if(dictData.count == 0){
           }else{
            print(dictData)
            selectedPaymentMode = index
            strGlobalPaymentMode = dictData
            tblView.reloadData()
           }
       
    }
}

