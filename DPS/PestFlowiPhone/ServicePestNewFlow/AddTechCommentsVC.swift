//
//  AddTechCommentsVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol AddCommentProtocol: class {
    
    func addTechComment(strTitle: String, strId: String, strDesription: String)
}
class AddTechCommentsVC: UIViewController, UITextViewDelegate, DidSelectTheFavouriteCell {
   
    // MARK: -IBOutlets
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var btnCheckbox: UIButton!
    @IBOutlet weak var lblSaveThisNote: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var heightConstraintForNoteTitleView: NSLayoutConstraint!
    @IBOutlet weak var btnMarkAsFav: UIButton!
    @IBOutlet weak var placeholderLabel :UILabel!
    @IBOutlet weak var viewForNote :UIView!


    //MARK: - Variable
    var strWorkorderId = ""
    var charLenth = 0
    var selectedFavCommentId = ""
    var strTechComment = ""
    var strFavTechCommet = "false"
    weak var delegateToAddComment: AddCommentProtocol?

    // MARK: -View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(strWorkorderId)
        txtComment.delegate = self
        heightConstraintForNoteTitleView.constant = 0
        viewForNote.isHidden = true
        
       
        
        if strTechComment != ""{
            let numberOfChars = strTechComment.count
            if numberOfChars > 0 {
                self.placeholderLabel.isHidden = true
                txtComment.text = strTechComment
            }

            else if numberOfChars == 0{
                self.placeholderLabel.isHidden = false
                self.placeholderLabel.text = "Add Comment..."
            }
        }
        // Do any additional setup after loading the view.
    }

    //MARK: -IBAction
    @IBAction func btnAddAction(_ sender: Any) {
        
        if txtComment.text == ""{
            self.view.makeToast("Description Should not be blank")
        }
        else if btnCheckbox.tag == 1{
            
            let titleTrimmedString = txtTitle.text?.trimmingCharacters(in: .whitespaces)
            
            if titleTrimmedString == ""{
              self.view.makeToast("Title should not be blank")
            }
            else {
                if btnMarkAsFav.tag == 1{
                    callWebServiceToMarkCommentFavourite()
                }
                callWebServiceToPostComment()
            }
        }
        else {
//            if btnMarkAsFav.tag == 1{
//                callWebServiceToMarkCommentFavourite()
//            }
//            if selectedFavCommentId == ""{
//                callWebServiceToPostComment()
//            }
//            else{
                self.view.makeToast("Comment Added Successfully")
                self.delegateToAddComment?.addTechComment(strTitle: self.txtTitle.text!, strId: self.selectedFavCommentId, strDesription: self.txtComment.text!)
                self.navigationController?.popViewController(animated: false)
//            }
        }
    }
    
    @IBAction func btnCheckboxAction(_ sender: Any) {
        if btnCheckbox.tag == 0{
            btnCheckbox.tag = 1
            btnCheckbox.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            heightConstraintForNoteTitleView.constant = 60
            viewForNote.isHidden = false
        }
        else if btnCheckbox.tag == 1{
            btnCheckbox.tag = 0
            btnCheckbox.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            heightConstraintForNoteTitleView.constant = 0
            viewForNote.isHidden = true
        }
    }
    
    @IBAction func btnMoveToFavouriteAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "FavouriteListVC") as? FavouriteListVC
        vc?.delegate = self
        vc?.strWorkOrderId  = strWorkorderId
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        vc?.strEmployeeId  = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnFavAction(_ sender: Any) {
        if btnMarkAsFav.tag == 0{
            btnMarkAsFav.tag = 1
            strFavTechCommet = "true"
            btnMarkAsFav.setImage(UIImage(named: "fav"), for: .normal)
        }
        else if btnMarkAsFav.tag == 1{
            btnMarkAsFav.tag = 0
            strFavTechCommet = "false"
            btnMarkAsFav.setImage(UIImage(named: "unfav"), for: .normal)
            
        }
    }
    //MARK: - Custom Functions
    func didSelectTheTechComment(strTitle: String, strId: String, strDesription: String) {
//        if title != ""{
//            btnCheckbox.tag = 1
//            btnCheckbox.setImage(UIImage(named: "NPMA_Check"), for: .normal)
//            heightConstraintForNoteTitleView.constant = 60
//            viewForNote.isHidden = false
//            txtTitle.text = strTitle
//        }
//        else{
//            btnCheckbox.tag = 0
//            btnCheckbox.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
//            heightConstraintForNoteTitleView.constant = 0
//            viewForNote.isHidden = true
//
//        }
        let numberOfChars = strDesription.count
        if numberOfChars > 0 {
            self.placeholderLabel.isHidden = true
            txtComment.text = strDesription
        }
            
        else if numberOfChars == 0{
            self.placeholderLabel.isHidden = false
            self.placeholderLabel.text = "Add Comment..."
        }
        
        selectedFavCommentId = strId
    }
    
    func getPostData() -> [String: Any]{
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"

        let innerParam = [
                     "WorkOrderId":strWorkorderId,
                     "CompanyId": strCompanyId,
                     "EmployeeId": strEmpID,
                     "NoteId": "",
                     "Description": self.txtComment.text!,
                     "Title": self.txtTitle.text ?? "",
                     "IsActive": "true",
                     "IsFavorite" : strFavTechCommet,
                     "CreatedBy" :  strEmpID] as [String: Any]
        
        let param = ["ServiceNoteFavoriteDetailExtDcs" : [innerParam]] as [String: Any]
        
        return param
    }

    //MARK: - Call WebServices
    func callWebServiceToPostComment()
    {
        
        let loaderLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loaderLocal, animated: true, completion: nil)
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.addTechComment
        
        WebService.postDataWithHeadersToServer(dictJson: getPostData() as NSDictionary, url: strURL, strType: "dict"){ (response, status) in
            
            loaderLocal.dismiss(animated: false) {
                if(status == true)
                {
                    self.view.makeToast("Comment Added Successfully")
                    self.delegateToAddComment?.addTechComment(strTitle: self.txtTitle.text!, strId: self.selectedFavCommentId, strDesription: self.txtComment.text!)
                    self.navigationController?.popViewController(animated: false)
                }
            }
        }
        
    }
    
    func callWebServiceToMarkCommentFavourite()
    {
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.markFavouiteComment
        
        WebService.postRequestWithHeaders(dictJson: getPostData() as NSDictionary , url: strURL, responseStringComing: "") { (response, status) in
            
            if(status == true)
            {
                self.view.makeToast("Comment Added Successfully")
                self.navigationController?.popViewController(animated: false)
                
            }
            
        }
        
    }
    
    
    //MARK: - UItextView Delelgate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        else{
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            charLenth = numberOfChars
            if newText.count > 0 {
                self.placeholderLabel.isHidden = true
            }
                
            else if newText.count == 0{
                self.placeholderLabel.isHidden = false
                self.placeholderLabel.text = "Add Comment..."
                
            }
            return numberOfChars < 1000
        }
    }
}
