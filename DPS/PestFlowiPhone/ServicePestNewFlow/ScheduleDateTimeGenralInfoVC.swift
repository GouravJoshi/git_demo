//
//  ScheduleDateTimeGenralInfoVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 29/04/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class ScheduleDateTimeGenralInfoVC: UIViewController, UITextViewDelegate, DatePickerProtocol  {

    //MARK: - IBOutlets
    @IBOutlet weak var txtSeviceDate: UITextField!
    @IBOutlet weak var txtOtherTime1: UITextField!
    @IBOutlet weak var txtOtherTime2: UITextField!

    @IBOutlet weak var btnSpecificTime: UIButton!
    @IBOutlet weak var btnTimeRange: UIButton!
    
    @IBOutlet weak var imgSpecificTime: UIImageView!
    @IBOutlet weak var imgTimeRange: UIImageView!
    
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var btnOpenTimeRangePicker: UIButton!

    @IBOutlet weak var viewLastTimeSection: UIView!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewForPicker: UIView!
    @IBOutlet weak var btnPickerDone: UIButton!

    @IBOutlet weak var btnOptionMenu: UIButton!
    
    //MARK: -Varialbe
    var strWorkOrder = ""
    let global = Global()
    var arrOfTimeRange = [[String: Any]]()
    var pickerSelectedIndex = Int()
    
    var strServiceDate = ""
    var strServiceTime = ""

    var strStartInterval = ""
    var strEndInterval = ""

    var strEmpID = String()
    var strEmpName = String()
    var dictLoginData = NSDictionary()
    var strCompanyKey = String()
    var strUserName = String()

    @objc var objWorkorderDetail = NSManagedObject()
    @objc var objNonPreferedDetail = NSMutableArray()
    var strmon = ""
    var strTue =  ""
    var strWed =  ""
    var strThur = ""
    var strFri =  ""
    var strSat  = ""
    var strSun =  ""
    var strNonPreferDateandTime = ""
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWorkOrder))
        
        if arryOfData.count > 0 {
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
        }
        
        
        txtSeviceDate.addTarget(self, action: #selector(txtServiceDateFunction), for: .touchDown)
        txtOtherTime1.addTarget(self, action: #selector(txtOtherTime1Function), for: .touchDown)
        txtOtherTime2.addTarget(self, action: #selector(txtOtherTime2Function), for: .touchDown)

        pickerView.dataSource = self
        pickerView.delegate = self
        txtOtherTime1.inputView = pickerView

        viewForPicker.isHidden = true
        viewForPicker.alpha  = 0

        btnOpenTimeRangePicker.isHidden = true

        btnTimeRange.tag = 0
        btnSpecificTime.tag = 1
        lblFirst.text = "Service Time"
        imgSpecificTime.image = UIImage(named: "RadioButton-Selected.png")
        imgTimeRange.image = UIImage(named: "RadioButton-Unselected.png")
        viewLastTimeSection.isHidden = true
        viewLastTimeSection.alpha = 0
        
        let strEarliestStartTime = "\(objWorkorderDetail.value(forKey: "earliestStartTimeStr") ?? "")"
        let strLatestStartTime = "\(objWorkorderDetail.value(forKey: "latestStartTimeStr") ?? "")"

        if strEarliestStartTime.count == 0 && strLatestStartTime.count == 0{
            
            btnTimeRange.tag = 0
            btnSpecificTime.tag = 1
            
            btnOpenTimeRangePicker.isHidden = true
            
            imgTimeRange.image = UIImage(named: "RadioButton-Unselected.png")
            imgSpecificTime.image = UIImage(named: "RadioButton-Selected.png")
            
            lblFirst.text = "Service Time"
            
            viewLastTimeSection.isHidden = true
            viewLastTimeSection.alpha = 0
            
            assignValue(strWhichRadioBUttonClicked: "Specific Time")
            
        }
        if strEarliestStartTime.count > 0 && strLatestStartTime.count > 0 {
            btnTimeRange.tag = 1
            btnSpecificTime.tag = 0
            
            imgTimeRange.image = UIImage(named: "RadioButton-Selected.png")
            imgSpecificTime.image = UIImage(named: "RadioButton-Unselected.png")
            
            btnOpenTimeRangePicker.isHidden = false
            lblFirst.text = "Time Range"
            lblSecond.text = "Service Time"

            viewLastTimeSection.isHidden = false
            viewLastTimeSection.alpha = 1
            
            assignValue(strWhichRadioBUttonClicked: "Time Range")
        }
        
        getNonPrefferedDateAndTime()
    }
    
    
    //MARK: - IBAction
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func btnRadioButtonAction(_ sender: Any) {
        
        if btnSpecificTime.tag == 1{
            btnTimeRange.tag = 1
            btnSpecificTime.tag = 0
            
            imgTimeRange.image = UIImage(named: "RadioButton-Selected.png")
            imgSpecificTime.image = UIImage(named: "RadioButton-Unselected.png")
            
            btnOpenTimeRangePicker.isHidden = false
            lblFirst.text = "Time Range"
            lblSecond.text = "Service Time"

            viewLastTimeSection.isHidden = false
            viewLastTimeSection.alpha = 1
            
            assignValue(strWhichRadioBUttonClicked: "Time Range")
        }
        else if btnTimeRange.tag == 1{
            btnTimeRange.tag = 0
            btnSpecificTime.tag = 1
            
            btnOpenTimeRangePicker.isHidden = true
            
            imgTimeRange.image = UIImage(named: "RadioButton-Unselected.png")
            imgSpecificTime.image = UIImage(named: "RadioButton-Selected.png")
            
            lblFirst.text = "Service Time"
            
            viewLastTimeSection.isHidden = true
            viewLastTimeSection.alpha = 0
            
            assignValue(strWhichRadioBUttonClicked: "Specific Time")
        }
        
        
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        viewForPicker.isHidden = true
        viewForPicker.alpha  = 0
        
//        if strWhichRadioBUttonClicked == "Time Range"{
//            txtOtherTime1.text =  strStartInterval + "  -  " + strEndInterval
//        }
        
        saveData()
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        viewForPicker.isHidden = true
        viewForPicker.alpha  = 0
        
        strStartInterval = arrOfTimeRange[pickerSelectedIndex]["StartInterval"] as? String ?? ""
        txtOtherTime2.text = arrOfTimeRange[pickerSelectedIndex]["StartInterval"] as? String ?? ""
        strEndInterval =  arrOfTimeRange[pickerSelectedIndex]["EndInterval"] as? String ?? ""

    }

    @IBAction func btnOpenPickerAction(_ sender: UIButton) {
        getTimeRangeList()
    }
    

    @IBAction func btnOpenActionSheetAction(_ sender: Any) {
        self.showOtherOptionsActionSheet()
    }
    
    
    //MARK: - API Calling
    func getNonPrefferedDateAndTime(){
        
        objNonPreferedDetail.removeAllObjects()
        
        let strworkorderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkorderNonPrefrableDateAndTime", predicate: NSPredicate(format: "workOrderId == %@", strworkorderNo))
    
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objNonPreferedDetail.add(dict)
            }
        }
        
        if objNonPreferedDetail.count > 0 {
            let obj = objNonPreferedDetail[0] as! NSManagedObject
            
            strmon = obj.value(forKey:"isMondayPreferable") as? String ?? ""
            strTue = obj.value(forKey:"isTuesdayPreferable") as? String ?? ""
            strWed = obj.value(forKey:"isWednesdayPreferable") as? String ?? ""
            strThur = obj.value(forKey:"isThurdayPreferable") as? String ?? ""
            strFri = obj.value(forKey:"isFridayPreferable") as? String ?? ""
            strSat  = obj.value(forKey:"isSaturdayPreferable") as? String ?? ""
            strSun = obj.value(forKey: "isSundayPreferable") as? String ?? ""
            strNonPreferDateandTime = obj.value(forKey:"nonPreferredTime") as? String ?? ""
                
        }
        
    }
    func getTimeRangeList(){
                
        let strURL =  URL.ServiceNewPestFlow.getTimeRangeList
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
            if(status)
            {
                self.arrOfTimeRange = response.value(forKey: "data") as! [[String:Any]]
                openTimeRange()
//                print(self.arrOfTimeRange)
//                pickerView.reloadAllComponents()
//                viewForPicker.isHidden = false
//                viewForPicker.alpha  = 1
                
            }
            
        }
    }
    
    
    //MARK: - Custom  Functions
    func openTimeRange(){
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "-----Select-----"
        vc.strTag = 5001
        if arrOfTimeRange.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBLTimeRange = arrOfTimeRange
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
            
        }
    }
    
    
    func showOtherOptionsActionSheet()  {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let currentSetupAction = UIAlertAction(title: "Current Setup", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCurrentSetupAction()
        })
        let customerSalesDocumentsAction = UIAlertAction(title: "Customer Sales Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCustomerSalesDocument()
        })
        
        let notesHistoryAction = UIAlertAction(title: "Notes History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openNotesHistory()
        })
        let serviceHistoryAction = UIAlertAction(title: "Service History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openServiceHistory()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(currentSetupAction)
        optionMenu.addAction(customerSalesDocumentsAction)
        optionMenu.addAction(notesHistoryAction)
        optionMenu.addAction(serviceHistoryAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.btnOptionMenu
        self.present(optionMenu, animated: true, completion: {
        })
            }
    
    func openCurrentSetupAction(){
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController?.strCompanyKey = strCompanyKey
        testController?.strWOId = strWorkOrder
        testController?.strUserName = strUserName

        self.present(testController!, animated: false, completion: nil)
    }
    
    func openCustomerSalesDocument(){
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func openNotesHistory(){
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController!.strWoId = strWorkOrder as String
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = false;
        testController!.strAccccountId = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        
    }
    
    func openServiceHistory(){
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
    
    
    func assignValue(strWhichRadioBUttonClicked: String){
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWorkOrder))
        
        if arryOfData.count > 0 {
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
        }
        
        var strScheduleStartDate = ""
        var strScheduleStartTime = ""

        let strStartDateTime = "\(objWorkorderDetail.value(forKey: "scheduleStartDateTime") ?? "")"
        if (strStartDateTime.count) > 0
        {
            strScheduleStartDate =  changeStringDateToGivenFormat(strDate:  strStartDateTime, strRequiredFormat: "MM/dd/yyyy")
            strScheduleStartTime =  changeStringDateToGivenFormat(strDate:  strStartDateTime, strRequiredFormat: "hh:mm a")

        }
        else
        {
            strScheduleStartDate = ""
            strScheduleStartTime = ""
        }
       
        var strEarliestStartTime = "\(objWorkorderDetail.value(forKey: "earliestStartTime") ?? "")"
        var strLatestStartTime = "\(objWorkorderDetail.value(forKey: "latestStartTime") ?? "")"

        strEarliestStartTime = changeStringDateToGivenFormat(strDate:  strEarliestStartTime.components(separatedBy: " ")[0], strRequiredFormat: "hh:mm a")
        
        strLatestStartTime = changeStringDateToGivenFormat(strDate:  strLatestStartTime.components(separatedBy: " ")[0] , strRequiredFormat: "hh:mm a")
        
        if strEarliestStartTime.count < 1 {

            strEarliestStartTime = ""

        }
        if strLatestStartTime.count < 1 {

            strLatestStartTime = ""

        }
        
        if strWhichRadioBUttonClicked == "Specific Time"{
            txtSeviceDate.text = strScheduleStartDate
            txtOtherTime1.text = strScheduleStartTime
            
        }
        
        else if strWhichRadioBUttonClicked == "Time Range" {
            txtSeviceDate.text = strScheduleStartDate
            txtOtherTime2.text = strScheduleStartTime
            if strLatestStartTime != "" || strEarliestStartTime != ""{
                txtOtherTime1.text =  strEarliestStartTime +  " - " + strLatestStartTime
                strStartInterval = strLatestStartTime
                strEndInterval = strEarliestStartTime
            }
            else{
                txtOtherTime1.text = ""
            }
        }
    }
    
    @objc func txtServiceDateFunction(textField: UITextField) {
        print("1")
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.strType = "Date"
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strTag = 1
        vc.chkForMinDate = true
        
        self.present(vc, animated: true, completion: {})
    }

    @objc func txtOtherTime1Function(textField: UITextField) {
        self.view.endEditing(true)
        print("2")
            let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
            
            let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.strTag = 2
            //vc.chkForMinDate = true
            vc.handleDateSelectionForDatePickerProtocol = self
            self.present(vc, animated: true, completion: {})
        
    }
    
    @objc func txtOtherTime2Function(textField: UITextField) {
        self.view.endEditing(true)
        print("3")
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.strTag = 3
        //vc.chkForMinDate = true
        vc.handleDateSelectionForDatePickerProtocol = self
        self.present(vc, animated: true, completion: {})
    }
    
    func CheckForDays() -> Bool {
        let strTempDate = txtSeviceDate.text!.toDate(withFormat: "MM/dd/yyyy")
        let day = strTempDate!.dayOfWeek()
        
        if strmon == "1"{
            if day?.lowercased() == "Monday".lowercased(){
                return true
            }
        }
        else if strTue == "1"{
            if day?.lowercased() == "Tuesday".lowercased(){
                return true

            }
        }
        else if strWed == "1"{
            if day?.lowercased() == "Wednesday".lowercased(){
                return true

            }
        }
        else if strThur == "1"{
            if day?.lowercased() == "Thursday".lowercased(){
                return true

            }
        }
        else if strFri == "1"{
            if day?.lowercased() == "Friday".lowercased(){
                return true

            }
        }
        else if strSat == "1"{
            if day?.lowercased() == "Saturday".lowercased(){
                return true
            }
        }
        else if strSun == "1"{
            if day?.lowercased() == "Sunday".lowercased(){
                return true
            }
        }
        
        return false
    }
    func saveData(){
        if btnSpecificTime.tag == 1{
            if txtSeviceDate.text == ""{
                self.view.makeToast("Service date should not be blank")
            }
            else if txtOtherTime1.text == ""{
                self.view.makeToast("Service time should not be blank")
            }
            else if CheckForDays() == true{
                
                alert(message: "You are attempting to schedule an appoinmtment for the customer's non-preferred day", title: "Warning:")
            }
            else{
                 
                let strScheduleDateAndTime = txtSeviceDate.text! + " " + txtOtherTime1.text!
               
                let arrOfValues = NSMutableArray()
                let arrOfKeys = NSMutableArray()
                
                arrOfKeys.add("ScheduleStartDateTime")
                arrOfKeys.add("LatestStartTimeStr")
                arrOfKeys.add("EarliestStartTimeStr")
                arrOfKeys.add("LatestStartTime")
                arrOfKeys.add("EarliestStartTime")
                
                arrOfValues.add(strScheduleDateAndTime)
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWorkOrder), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess
                {
                    updateModifyDate()
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWorkOrder)
                    self.navigationController?.popViewController(animated: false)
                    
                } else {
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
        else{
            if txtSeviceDate.text == ""{
                self.view.makeToast("Service date should not be blank")
            }
            else if txtOtherTime1.text == ""{
                self.view.makeToast("Time Range should not be blank")
            }
            else if txtOtherTime2.text == ""{
                self.view.makeToast("Service time should not be blank")
            }
            else if CheckForDays() == true{
                
                alert(message: "You are attempting to schedule an appoinmtment for the customer's non-preferred day", title: "Warning:")
            }
            else if strNonPreferDateandTime == strStartInterval + " - " + strEndInterval{
                self.view.makeToast("You have selected non prefferable time")
            }
            else{
                 
                
                let strScheduleDateAndTime = txtSeviceDate.text! + " " + txtOtherTime2.text!
                
                let arrOfValues = NSMutableArray()
                let arrOfKeys = NSMutableArray()
                
                arrOfKeys.add("ScheduleStartDateTime")
                arrOfKeys.add("LatestStartTimeStr")
                arrOfKeys.add("EarliestStartTimeStr")
                arrOfKeys.add("LatestStartTime")
                arrOfKeys.add("EarliestStartTime")
                
                arrOfValues.add(strScheduleDateAndTime)
                arrOfValues.add(strEndInterval)
                arrOfValues.add(strStartInterval)
                arrOfValues.add(changeStringDateToGivenFormat(strDate: strEndInterval, strRequiredFormat: "HH:mm"))
                arrOfValues.add(changeStringDateToGivenFormat(strDate: strStartInterval, strRequiredFormat: "HH:mm"))
                
                print("==============")
                print(changeStringDateToGivenFormat(strDate: strEndInterval, strRequiredFormat: "HH:mm"))
                print(changeStringDateToGivenFormat(strDate: strStartInterval, strRequiredFormat: "HH:mm"))

                let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWorkOrder), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess
                {
                    updateModifyDate()
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWorkOrder)
                    self.navigationController?.popViewController(animated: false)
                } else {
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int) {
        //ScheduleOnStartDateTime
        if tag == 1{
            
            txtSeviceDate.text = strDate
            
        }
        
        else if tag == 2{
            print("2")
            if btnSpecificTime.tag == 1{
                txtOtherTime1.text = strDate
            }
            else {
                txtOtherTime2.text = strDate
            }

        }
        else if tag == 3{
            print("3")
            if btnSpecificTime.tag == 1{
                txtOtherTime1.text = strDate
            }
            else {
                txtOtherTime2.text = strDate
            }
        }
    }
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWorkOrder), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWorkOrder as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
}


// MARK: - UIPickerViewDelegate

extension ScheduleDateTimeGenralInfoVC: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrOfTimeRange.count
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let strStartInterval = arrOfTimeRange[row]["StartInterval"] as? String ?? ""
        let strEndInterval =  arrOfTimeRange[row]["EndInterval"] as? String ?? ""
        return strStartInterval + "  -  " + strEndInterval
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let strStartInterval = arrOfTimeRange[row]["StartInterval"] as? String ?? ""
        let strEndInterval =  arrOfTimeRange[row]["EndInterval"] as? String ?? ""
        pickerSelectedIndex = row
        txtOtherTime1.text = strStartInterval + "  -  " + strEndInterval
    }
}

extension ScheduleDateTimeGenralInfoVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 5001)
       {
           if(dictData.count == 0){
               txtOtherTime1.text = ""
               txtOtherTime2.text = ""
               strStartInterval =  ""
               strEndInterval =  ""
           }else{
            print(dictData)
            strStartInterval = dictData.value(forKey: "StartInterval") as! String
            strEndInterval = dictData.value(forKey: "EndInterval") as! String
            
            txtOtherTime1.text = strStartInterval + "  -  " + strEndInterval
            txtOtherTime2.text = strStartInterval
           }
       }
    }
}

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
}


extension String {

    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)

        return date

    }
}

extension UIViewController {
  func alert(message: String, title: String = "") {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "Continue", style: .default, handler: nil)
    alertController.addAction(OKAction)
    self.present(alertController, animated: true, completion: nil)
  }
}
