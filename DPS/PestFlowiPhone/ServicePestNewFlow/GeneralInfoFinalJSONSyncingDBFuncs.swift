//
//  GeneralInfoFinalJSONSyncingDBFuncs.swift
//  DPS
//
//  Created by Akshay Hastekar on 15/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

@objc class GeneralInfoFinalJSONSyncingDBFuncs: NSObject {
    
    @objc func fetchWorkOrderDetailServiceAuto(strWoId: String, IsFromPDF: Bool, IsReset: Bool)-> [String: Any]{
        var arrOfReturn = [String: Any]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arrOfData.count == 0{
            
            arrOfReturn = [ "WorkorderDetail" : ""]
        }
        else{
            
            for i in 0..<arrOfData.count{
                
                // let matches = arrOfData[i] as! [String: Any]
                var tempDict = [String: Any]()
                let matches = arrOfData[i] as! NSManagedObject
                var arrWoDetailKey = NSArray()
                arrWoDetailKey = Array(matches.entity.attributesByName.keys) as NSArray
                
                let strCustomerSign = matches.value(forKey: "customerSignaturePath") as? String ?? ""
                let strTechSign = matches.value(forKey: "technicianSignaturePath") as? String ?? ""
                let strAudioNameGlobal =  matches.value(forKey: "audioFilePath") as? String ?? ""
                if strCustomerSign.count == 0 {
                    //[arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
                }
                
                if strTechSign.count == 0 {
                    //[arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
                }
                
                for p in 0..<arrWoDetailKey.count{
                    var strvalue = matches.value(forKey: arrWoDetailKey[p] as? String ?? "")
                    
                    if strvalue is Date{
                        //strvalue = strvalue as! NSDate
                        strvalue = String(changeDateToStringTemp(date: (strvalue as! NSDate) as Date))
                    }
                    else if strvalue is NSArray{
                        strvalue = Array(matches.value(forKey: arrWoDetailKey[p] as! String) as! NSArray)
                    }
                    else if strvalue == nil {
                        strvalue = ""
                        
                    }
                    else if strvalue as? Any.Type == NSNull.self || strvalue as! String == "" {
                        strvalue = ""
                    }
                    else {
                        strvalue = strvalue as! String
                    }
                    let dictObj = [ arrWoDetailKey[p] as! String: strvalue!]
                    tempDict.mergeDict(dict: dictObj as [String : Any])
                }
                
                let strISNPMATermite = tempDict["isNPMATermite"] as? String ?? ""
                
                if strISNPMATermite.lowercased() == "true" || strISNPMATermite == "1" {
                    
                    tempDict["isNPMATermite"] = "true"
                } else if strISNPMATermite.lowercased() == "false" || strISNPMATermite == "0" || strISNPMATermite == "" {
                    
                    tempDict["isNPMATermite"] = "false"
                }
                
                let strIsPDFGen = tempDict["isPDFGenerated"] as? String ?? ""
                
                if strIsPDFGen.lowercased() == "true" || strIsPDFGen == "1" {
                    
                    tempDict["isPDFGenerated"] = "true"
                } else if strIsPDFGen.lowercased() == "false" || strIsPDFGen == "0" || strIsPDFGen == "" {
                    
                    tempDict["isPDFGenerated"] = "false"
                }
                
                let strIsTaxExampt = tempDict["isTaxExampt"] as? String ?? ""
                if strIsTaxExampt.lowercased() == "true" || strIsTaxExampt == "1" {
                    
                    tempDict["isTaxExampt"] = "true"
                } else if strIsTaxExampt.lowercased() == "false" || strIsTaxExampt == "0" || strIsTaxExampt == "" {
                    
                    tempDict["isTaxExampt"] = "false"
                }
                
                let strIsRegularPestFlowOnBranch = tempDict["isRegularPestFlowOnBranch"] as? String ?? ""
                
                if strIsRegularPestFlowOnBranch.lowercased() == "true" || strIsRegularPestFlowOnBranch == "1" {
                    
                    tempDict["isRegularPestFlowOnBranch"] = "true"
                    
                } else if strIsRegularPestFlowOnBranch.lowercased() == "false" || strIsRegularPestFlowOnBranch == "0" || strIsRegularPestFlowOnBranch == "" {
                    
                    tempDict["isRegularPestFlowOnBranch"] = "false"
                }
                
                let strIsResendInvoiceMail = tempDict["isResendInvoiceMail"] as? String ?? ""
                
                if strIsResendInvoiceMail.lowercased() == "true" || strIsResendInvoiceMail == "1" {
                    
                    tempDict["isResendInvoiceMail"] = "true"
                    
                } else if strIsResendInvoiceMail.lowercased() == "false" || strIsResendInvoiceMail == "0" || strIsResendInvoiceMail == "" {
                    
                    tempDict["isResendInvoiceMail"] = "false"
                }
                
                let strIsCollectPayment = tempDict["isCollectPayment"] as? String ?? ""
                
                if strIsCollectPayment.lowercased() == "true" || strIsCollectPayment == "1" {
                    
                    tempDict["isCollectPayment"] = "true"
                    
                } else if strIsCollectPayment.lowercased() == "false" || strIsCollectPayment == "0" || strIsCollectPayment == "" {
                    
                    tempDict["isCollectPayment"] = "false"
                }
                
                
                if IsFromPDF == true{
                    tempDict["workorderStatus"] = "InComplete"
                }
                else{
                    tempDict["workorderStatus"] = "Completed"
                }
                if IsReset == true{
                    tempDict["workorderStatus"] = "Reset"
                }
                arrOfReturn = [ "WorkorderDetail" : tempDict]
                
            }
        }
        return arrOfReturn
    }
    
    @objc func fetchServiceAreaDocumentsDc(strWoId: String)-> [String: Any]{
        
        var arrOfReturn = [String: Any]()
        var tempDict = [[String: Any]]()
        
        let arrOfData  = getDataFromCoreDataBaseArray(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfData.count == 0 {
            arrOfReturn = ["ServiceAreaDocuments" : []]
        }
        else{
            
            for i in 0..<arrOfData.count{
                let matches = arrOfData[i] as! NSManagedObject
                let strAddressAreaId = "\(matches.value(forKey: "addressAreaId") ?? "")"
                let strDocumnetTitle = "\(matches.value(forKey: "documnetTitle") ?? "")"
                let strServiceAreaDocumentId =  "\(matches.value(forKey: "serviceAreaDocumentId") ?? "N/A")"
                let strDocumentPath = "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType = "\(matches.value(forKey: "documentType") ?? "")"
                
                let  obj = [
                    "ServiceAreaDocumentId" : strServiceAreaDocumentId,
                    "MobileServiceAreaId" : strAddressAreaId,
                    "DocumnetTitle" : strDocumnetTitle,
                    "DocumentPath" : strDocumentPath,
                    "DocumentType" : strDocumentType
                ] as [String: Any]
                
                tempDict.append(obj)
            }
            
            arrOfReturn = ["ServiceAreaDocuments"  : tempDict]
            
        }
        return arrOfReturn
    }
    
    @objc func fetchServiceDeviceDocumentsDc(strWoId: String)-> [String: Any]{
        
        var arrOfReturn = [String: Any]()
        var tempDict = [[String: Any]]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfData.count == 0 {
            arrOfReturn = ["ServiceDeviceDocuments" : []]
        }
        else{
            
            for i in 0..<arrOfData.count{
                let matches = arrOfData[i] as! NSManagedObject
                
                let strDeviceId = "\(matches.value(forKey: "deviceId") ?? "N/A")"
                let strDocumnetTitle = "\(matches.value(forKey: "documnetTitle") ?? "N/A")"
                let strServiceDeviceDocumentId =  "\(matches.value(forKey: "serviceDeviceDocumentId") ?? "N/A")"
                let strDocumentPath = "\(matches.value(forKey: "documentPath") ?? "N/A")"
                let strDocumentType = "\(matches.value(forKey: "documentType") ?? "N/A")"
                
                let dict = [
                    "ServiceDeviceDocumentId" : strServiceDeviceDocumentId,
                    "MobileServiceDeviceId" : strDeviceId,
                    "DocumnetTitle" : strDocumnetTitle,
                    "DocumentPath" : strDocumentPath,
                    "DocumentType" : strDocumentType ] as [String: Any]
                
                tempDict.append(dict)
            }
            
            arrOfReturn = ["ServiceDeviceDocuments"  : tempDict]
        }
        return arrOfReturn
    }
    
    @objc func fetchWOProductDetail(strWoId: String)-> [String: Any]{
        var arrOfReturn = [String: Any]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "WorkorderDetailChemicalListService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if (arrOfData.count==0)
        {
            let arrPaymentInfoValue = NSMutableArray()
            arrOfReturn = [ "WoProductDetail" : arrPaymentInfoValue]
            
        }else{
            let matches = arrOfData[0] as! NSManagedObject
            let arrOfWoProductDetail = matches.value(forKey: "chemicalList") as! [[String: Any]]
            let arrOfWoOtherProductDetail = matches.value(forKey: "otherChemicalList")  as! [[String: Any]]
            
            arrOfReturn = [ "WoProductDetail" : arrOfWoProductDetail,
                            "WoOtherProductDetail" : arrOfWoOtherProductDetail
            ]
            
        }
        
        return arrOfReturn
    }
    
    @objc func fetchWOServiceStartStopDict(strWoId: String)-> [String: Any]{
        
        var arrOfReturn = [String: Any]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if (arrOfData.count==0)
        {
            let arrOfService = NSMutableArray()
            arrOfReturn = [ "WorkOrderBreakTimeExtDcs" : arrOfService]
            
        }else{
            var tempDict = [[String: Any]]()
            
            for i in 0..<arrOfData.count{
                
                let matches = arrOfData[i] as! NSManagedObject
                
                let strStartTime = matches.value(forKey: "startTime") as! String
                let strStopTime = matches.value(forKey: "stopTime")  as! String
                let strSubject =  matches.value(forKey: "subject")  as! String
                var strWorkOrderBreakTimeId =  matches.value(forKey: "workOrderBreakTimeId")  as? String ?? ""

                if strWorkOrderBreakTimeId == ""{
                    strWorkOrderBreakTimeId = "0"
                }
                
                let Obj =  [
                    "WorkOrderBreakTimeId" : strWorkOrderBreakTimeId ,
                    "WorkOrderId" : strWoId,
                    "BreakPauseStatus" : strSubject.lowercased(),
                    "StartTime" :strStartTime ,
                    "EndTime" : strStopTime,
                    "EmployeeId" : Global().getEmployeeId(),
                ] as [String: Any]
                
                tempDict.append(Obj)
            }
            
            arrOfReturn = [ "WorkOrderBreakTimeExtDcs" : tempDict]
            
        }
        
        return arrOfReturn
    }
    
    @objc func fetchWOServiceCategoryGeneralInfo(strWoId: String, strWoNo: String)-> [String: Any]{
        
        var arrOfReturn = [String: Any]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if (arrOfData.count==0)
        {
            let arrOfService = NSMutableArray()
            arrOfReturn = [ "WorkOrderServicesExtDc" : arrOfService]
            
        }else{
            
            var tempDict = [[String: Any]]()
            
            for i in 0..<arrOfData.count{
                
                let matches = arrOfData[i] as! NSManagedObject
                
                let strQty = matches.value(forKey: "serviceQuantity") as! String
                let strPrice = matches.value(forKey: "servicePrice")  as! String
                let strServiceId =  matches.value(forKey: "serviceId")  as! String
                let strServiceName = matches.value(forKey: "serviceName") as! String
                var strServicePrimarykey =  matches.value(forKey: "workorderServiceId")  as? String ?? ""
                var strIsTaxable = matches.value(forKey: "taxable") as! String
                
                
                if strIsTaxable.lowercased() == "true" || strIsTaxable == "1" {
                    
                    strIsTaxable = "true"
                    
                } else if strIsTaxable.lowercased() == "false" || strIsTaxable == "0" || strIsTaxable == "" {
                    
                    strIsTaxable = "false"
                }
                
                if strServicePrimarykey == ""{
                    strServicePrimarykey = "0"
                }
                
                let Obj =  [
                    "WorkorderServiceId" : strServicePrimarykey,
                    "IsPrimary" : "",
                    "WorkOrderId" : strWoId,
                    "ServiceId" : strServiceId,
                    "SoldServiceNonStandardId" : "",
                    "SoldServiceStandard" : "",
                    "Price" : strPrice,
                    "Quantity" : strQty,
                    "ProductionValue" : "",
                    "EstimatedTimeReq" : "",
                    "ServiceInstruction" : "",
                    "IsTaxable" : strIsTaxable,
                    "WorkOrderNo" : strWoNo,
                    "ServiceName" : strServiceName ] as [String: Any]
                
                tempDict.append(Obj)
            }
            
            arrOfReturn = [ "WorkOrderServicesExtDc" : tempDict]
            
        }
        return arrOfReturn
    }
    
    
    @objc func fetchPaymentInfoServiceAuto(strWoId: String)-> [String: Any]{
        var arrOfReturn = [String: Any]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if (arrOfData.count==0)
        {
            let arrPaymentInfoValue = NSMutableArray()
            arrOfReturn = [ "PaymentInfo" : arrPaymentInfoValue]
            
        }else
        {
            
            for i in 0..<arrOfData.count{
                
                // let matches = arrOfData[i] as! [String: Any]
                var tempDict = [String: Any]()
                let matches = arrOfData[i] as! NSManagedObject
                var arrWoPaymentKey = NSArray()
                arrWoPaymentKey = Array(matches.entity.attributesByName.keys) as NSArray
                
                let strCheckFrontImage = matches.value(forKey: "checkFrontImagePath") as? String ?? ""
                let strCheckBackImage = matches.value(forKey: "checkBackImagePath") as? String ?? ""
                if strCheckFrontImage.count == 0 {
                    //[arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
                }
                
                if strCheckBackImage.count == 0 {
                    //[arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
                }
                
                for p in 0..<arrWoPaymentKey.count{
                    var strvalue = matches.value(forKey: arrWoPaymentKey[p] as? String ?? "")
                    
                    if strvalue is Date{
                        //strvalue = strvalue as! NSDate
                        strvalue = String(changeDateToStringTemp(date: (strvalue as! NSDate) as Date))
                    }
                    else if strvalue is NSArray{
                        strvalue = Array(matches.value(forKey: arrWoPaymentKey[p] as! String) as! NSArray)
                    }
                    else if strvalue == nil {
                        strvalue = ""
                        
                    }
                    else if strvalue as? Any.Type == NSNull.self || strvalue as! String == "" {
                        strvalue = ""
                    }
                    else {
                        strvalue = strvalue as! String
                    }
                    let dictObj = [ arrWoPaymentKey[p] as! String: strvalue!]
                    tempDict.mergeDict(dict: dictObj as [String : Any])
                }
                
                arrOfReturn = [ "PaymentInfo" : tempDict]
                
            }
        }
        
        return arrOfReturn
    }
    @objc func fetchImageDetailServiceAuto(strWoId: String)-> [String: Any]{
        
        var arrOfReturn = [String: Any]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arrOfData.count == 0
        {
            arrOfReturn = ["ImagesDetail":""]
        }
        else{
            var arrOfDict = [[String: Any]]()
            var tempDict = [String: Any]()
            
            for i in 0..<arrOfData.count{
                
                let matches = arrOfData[i] as! NSManagedObject
                var arrWoImageKey = NSArray()
                arrWoImageKey = Array(matches.entity.attributesByName.keys) as NSArray
                
                let strWoImagePath = matches.value(forKey: "woImagePath") as? String ?? ""
                if strWoImagePath.count == 0 {
                    //[arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
                }
                
                for p in 0..<arrWoImageKey.count{
                    var strvalue = matches.value(forKey: arrWoImageKey[p] as? String ?? "")
                    
                    if strvalue is Date{
                        //strvalue = strvalue as! NSDate
                        strvalue = String(changeDateToStringTemp(date: (strvalue as! NSDate) as Date))
                    }
                    else if strvalue is NSArray{
                        strvalue = Array(matches.value(forKey: arrWoImageKey[p] as! String) as! NSArray)
                    }
                    else if strvalue == nil {
                        strvalue = ""
                        
                    }
                    else if strvalue as? Any.Type == NSNull.self || strvalue as! String == "" {
                        strvalue = ""
                    }
                    else {
                        strvalue = strvalue as! String
                    }
                    let dictObj = [ arrWoImageKey[p] as! String: strvalue!]
                    tempDict.mergeDict(dict: dictObj as [String : Any])
                    
                }
                arrOfDict.append(tempDict)
                
            }
            arrOfReturn = [ "ImagesDetail" : arrOfDict]
            
        }
        return arrOfReturn
    }
    
    @objc func fetchEmailDetailServiceAuto(strWoId: String)-> [String: Any]{
        var arrOfReturn = [String: Any]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arrOfData.count == 0
        {
            arrOfReturn = ["EmailDetail":""]
        }
        else{
            var arrOfDict = [[String: Any]]()
            var tempDict = [String: Any]()
            
            for i in 0..<arrOfData.count{
                
                let matches = arrOfData[i] as! NSManagedObject
                var arrWoImageKey = NSArray()
                arrWoImageKey = Array(matches.entity.attributesByName.keys) as NSArray
                
                for p in 0..<arrWoImageKey.count{
                    var strvalue = matches.value(forKey: arrWoImageKey[p] as? String ?? "")
                    
                    if strvalue is Date{
                        //strvalue = strvalue as! NSDate
                        strvalue = String(changeDateToStringTemp(date: (strvalue as! NSDate) as Date))
                    }
                    else if strvalue is NSArray{
                        strvalue = Array(matches.value(forKey: arrWoImageKey[p] as! String) as! NSArray)
                    }
                    else if strvalue == nil {
                        strvalue = ""
                        
                    }
                    else if strvalue as? Any.Type == NSNull.self || strvalue as! String == "" {
                        strvalue = ""
                    }
                    else {
                        strvalue = strvalue as! String
                    }
                    let dictObj = [ arrWoImageKey[p] as! String: strvalue!]
                    tempDict.mergeDict(dict: dictObj as [String : Any])
                    
                }
                arrOfDict.append(tempDict)
                
            }
            arrOfReturn = [ "EmailDetail" : arrOfDict]
            
        }
        
        return arrOfReturn
    }
    
    @objc func fetchWorkOrderDocuments(strWoId: String)-> [String: Any]{
        var arrOfReturn = [String: Any]()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arrOfData.count == 0
        {
            arrOfReturn = ["WoOtherDocuments":""]
        }
        else{
            var arrOfDict = [[String: Any]]()
            var tempDict = [String: Any]()
            
            for i in 0..<arrOfData.count{
                
                let matches = arrOfData[i] as! NSManagedObject
                var arrWoImageKey = NSArray()
                arrWoImageKey = Array(matches.entity.attributesByName.keys) as NSArray
                
                for p in 0..<arrWoImageKey.count{
                    var strvalue = matches.value(forKey: arrWoImageKey[p] as? String ?? "")
                    
                    if strvalue is Date{
                        //strvalue = strvalue as! NSDate
                        strvalue = String(changeDateToStringTemp(date: (strvalue as! NSDate) as Date))
                    }
                    else if strvalue is NSArray{
                        strvalue = Array(matches.value(forKey: arrWoImageKey[p] as! String) as! NSArray)
                    }
                    else if strvalue == nil {
                        strvalue = ""
                        
                    }
                    else if strvalue as? Any.Type == NSNull.self || strvalue as! String == "" {
                        strvalue = ""
                    }
                    else {
                        strvalue = strvalue as! String
                    }
                    let dictObj = [ arrWoImageKey[p] as! String: strvalue!]
                    tempDict.mergeDict(dict: dictObj as [String : Any])
                    
                }
                arrOfDict.append(tempDict)
                
            }
            arrOfReturn = [ "WoOtherDocuments" : arrOfDict]
            
        }
        
        return arrOfReturn
    }
    @objc func fetchServicePestDataAllFromDBObjC(strWoId: String)-> [String: Any]{
        
        var arrOfReturn = [String: Any]()
        var strServiceAddressIdTemp = String()
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arrOfData.count > 0
        {
            let objTemp = arrOfData[0] as! NSManagedObject
            
            strServiceAddressIdTemp = objTemp.value(forKey: "serviceAddressId") as! String
        }
        
        let arrOfServiceArea = fetchServicePestNewFlowData(strEntityName: "ServiceAreas" , strIsDocument: "No", strServiceAddressIdForAreaDevice: strServiceAddressIdTemp, strWoId: strWoId)
        
        // ServiceDevices
        let arrOfServiceDevices = fetchServicePestNewFlowData(strEntityName: "ServiceDevices" , strIsDocument: "No", strServiceAddressIdForAreaDevice: strServiceAddressIdTemp, strWoId: strWoId)
        
        // ServiceProducts
        let arrOfServiceProducts = fetchServicePestNewFlowData(strEntityName: "ServiceProducts" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // ServicePests
        let arrOfServicePests = fetchServicePestNewFlowData(strEntityName: "ServicePests" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // ServiceConditions
        let arrOfServiceConditions = fetchServicePestNewFlowData(strEntityName: "ServiceConditions" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // ServiceComments
        let arrOfServiceComments = fetchServicePestNewFlowData(strEntityName: "ServiceComments" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // ServiceConditionComments
        let arrOfServiceConditionComments = fetchServicePestNewFlowData(strEntityName: "ServiceConditionComments" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // ServicePestDocuments
        let arrOfSAPestDocuments = fetchServicePestNewFlowData(strEntityName: "SAPestDocuments" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // ServiceConditionDocuments
        let arrOfServiceConditionDocuments = fetchServicePestNewFlowData(strEntityName: "ServiceConditionDocuments" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // ServiceDeviceInspection
        let arrOfServiceDeviceInspections = fetchServicePestNewFlowData(strEntityName: "ServiceDeviceInspections" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // WOServiceDevices
        let arrOfWOServiceDevices = fetchServicePestNewFlowData(strEntityName: "WOServiceDevices" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        // WOServiceAreas
        let arrOfWOServiceAreas = fetchServicePestNewFlowData(strEntityName: "WOServiceAreas" , strIsDocument: "No", strServiceAddressIdForAreaDevice: "", strWoId: strWoId)
        
        arrOfReturn = [
            "ServiceAreas" :  arrOfServiceArea,
            "ServiceDevices" : arrOfServiceDevices,
            "ServiceProducts" : arrOfServiceProducts,
            "ServicePests" : arrOfServicePests,
            "ServiceConditions" : arrOfServiceConditions,
            "ServiceComments" : arrOfServiceComments,
            "ServiceConditionComments" :arrOfServiceConditionComments,
            "SAPestDocuments" : arrOfSAPestDocuments,
            "ServiceConditionDocuments" : arrOfServiceConditionDocuments,
            "ServiceDeviceInspections" : arrOfServiceDeviceInspections,
            "WOServiceDevices" : arrOfWOServiceDevices,
            "WOServiceAreas" : arrOfWOServiceAreas ]
        
        return arrOfReturn
    }
    
    @objc func fetchServicePestNewFlowData(strEntityName: String, strIsDocument: String, strServiceAddressIdForAreaDevice: String, strWoId: String) -> [[String: Any]]{
        
        var arrOfAllDocumentsToSend = [[String: Any]]()
        var tempDict = [String: Any]()
        
        if strServiceAddressIdForAreaDevice.count > 0{
            let arrofData = getDataFromCoreDataBaseArray(strEntity: strEntityName, predicate: NSPredicate(format: "serviceAddressId == %@", strServiceAddressIdForAreaDevice))
            if arrofData.count == 0{
            }
            else{
                for i in 0..<arrofData.count{
                    let matches = arrofData[i] as! NSManagedObject
                    var arrOfKey = NSArray()
                    arrOfKey = Array(matches.entity.attributesByName.keys) as NSArray
                    let dictOfData = matches.dictionaryWithValues(forKeys: arrOfKey as! [String])
                    
                    if strIsDocument.lowercased() == "Yes".lowercased(){
                        let strIsToUpload = dictOfData["isToUpload"] as! String
                        if strIsToUpload.lowercased() == "Yes".lowercased(){
                            arrOfAllDocumentsToSend.append(dictOfData)
                        }
                    }
                    for p in 0..<arrOfKey.count{
                        var strvalue = dictOfData["\(arrOfKey[p])"]
                        
                        
                        if strvalue is Date{
                            strvalue = String(changeDateToStringTemp(date: (strvalue as! NSDate) as Date))
                        }
                        else if strvalue is NSArray{
                            strvalue = Array(matches.value(forKey: arrOfKey[p] as! String) as! NSArray)
                        }
                        else if strvalue == nil {
                            strvalue = ""
                        }
                        else if strvalue is NSNumber {
                            
                            let strBoolKey = "\(arrOfKey[p])"
                            if strBoolKey == "isInspectingFirstTime" || strBoolKey == "IsActive" || strBoolKey == "isActive" || strBoolKey == "IsDeletedArea" || strBoolKey == "isDeletedArea" || strBoolKey == "IsInspected" || strBoolKey == "isInspected" || strBoolKey == "IsDeletedDevice" || strBoolKey == "isDeletedDevice" {
                                
                                strvalue = String(describing: strvalue as? NSNumber ?? 0)
                                
                                if strvalue as! String == "1" || strvalue as! String == "true"{
                                    strvalue = "true"
                                }
                                else if strvalue as! String == "0" || strvalue as! String == "false"{
                                    strvalue = "false"
                                }
                                
                            }
                            
                            else{
                                strvalue = String(describing: strvalue as? NSNumber ?? 0)
                                
                            }
                            
                        }
                        else if strvalue is NSNull{
                            strvalue = ""
                        }
                        else if strvalue as? Any.Type == NSNull.self || strvalue as! String == "" {
                            strvalue = ""
                        }
                        else {
                            
                            let strBoolKey = "\(arrOfKey[p])"
                            
                            if strBoolKey == "isInspectingFirstTime" || strBoolKey == "IsActive" || strBoolKey == "IsDeletedArea" || strBoolKey == "IsInspected" || strBoolKey == "IsDeletedDevice" || strBoolKey == "isDeletedDevice" {
                                
                                strvalue =  strvalue as? String ?? ""
                                
                                if strvalue as! String == "1" || strvalue as! String == "true"{
                                    strvalue = "true"
                                }
                                else if strvalue as! String == "0" || strvalue as! String == "false"{
                                    strvalue = "false"
                                }
                            }
                            else{
                                strvalue =  strvalue as? String ?? ""
                            }
                        }
                        
                        let  dictObj = [ arrOfKey[p] as! String: strvalue!]
                        tempDict.mergeDict(dict: dictObj as [String : Any])
                    }
                    arrOfAllDocumentsToSend.append(tempDict)
                }
            }
        }
        else{
            let arrofData = getDataFromCoreDataBaseArray(strEntity: strEntityName, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
            if arrofData.count == 0{
            }
            else{
                for i in 0..<arrofData.count{
                    let matches = arrofData[i] as! NSManagedObject
                    var arrOfKey = NSArray()
                    arrOfKey = Array(matches.entity.attributesByName.keys) as NSArray
                    let dictOfData = matches.dictionaryWithValues(forKeys: arrOfKey as! [String])
                    
                    if strIsDocument.lowercased() == "Yes".lowercased(){
                        let strIsToUpload = dictOfData["isToUpload"] as! String
                        if strIsToUpload.lowercased() == "Yes".lowercased(){
                            arrOfAllDocumentsToSend.append(dictOfData)
                        }
                    }
                    for p in 0..<arrOfKey.count{
                        var strvalue = dictOfData["\(arrOfKey[p])"]
                        
                        if strvalue is Date{
                            strvalue = String(changeDateToStringTemp(date: (strvalue as! NSDate) as Date))
                        }
                        else if strvalue is NSArray{
                            strvalue = Array(matches.value(forKey: arrOfKey[p] as! String) as! NSArray)
                        }
                        else if strvalue == nil {
                            strvalue = ""
                        }
                        else if strvalue is NSNumber {
                            
                            let strBoolKey = "\(arrOfKey[p])"
                            if strBoolKey == "isInspectingFirstTime" || strBoolKey == "IsActive" || strBoolKey == "isActive" || strBoolKey == "IsDeletedArea" || strBoolKey == "isDeletedArea" || strBoolKey == "IsInspected" || strBoolKey == "isInspected" || strBoolKey == "IsDeletedDevice" || strBoolKey == "isDeletedDevice" {
                                
                                strvalue = String(describing: strvalue as? NSNumber ?? 0)
                                
                                if strvalue as! String == "1" || strvalue as! String == "true"{
                                    strvalue = "true"
                                }
                                else if strvalue as! String == "0" || strvalue as! String == "false"{
                                    strvalue = "false"
                                }
                                
                            }
                            
                            else{
                                strvalue = String(describing: strvalue as? NSNumber ?? 0)
                                
                            }
                            
                        }
                        else if strvalue is NSNull{
                            strvalue = ""
                        }
                        else if strvalue as? Any.Type == NSNull.self || strvalue as! String == "" {
                            strvalue = ""
                        }
                        else {
                            
                            let strBoolKey = "\(arrOfKey[p])"
                            
                            if strBoolKey == "isInspectingFirstTime" || strBoolKey == "IsActive" || strBoolKey == "IsDeletedArea" || strBoolKey == "IsInspected" || strBoolKey == "IsDeletedDevice" || strBoolKey == "isDeletedDevice" {
                                
                                strvalue =  strvalue as? String ?? ""
                                
                                if strvalue as! String == "1" || strvalue as! String == "true"{
                                    strvalue = "true"
                                }
                                else if strvalue as! String == "0" || strvalue as! String == "false"{
                                    strvalue = "false"
                                }
                            }
                            else{
                                strvalue =  strvalue as? String ?? ""
                            }
                        }
                        let dictObj = [ arrOfKey[p] as! String: strvalue!]
                        tempDict.mergeDict(dict: dictObj as [String : Any])
                    }
                    arrOfAllDocumentsToSend.append(tempDict)
                }
            }
        }
        
        return arrOfAllDocumentsToSend
    }
    
    @objc func fetchAudioToBeSync(strWoId: String)-> [String]{
        
        var arrOfReturn = [String]()
        let arrOfWorkOrder = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arrOfWorkOrder.count == 0{
            print("no images")
        }
        else{
            
            for i in 0..<arrOfWorkOrder.count{
                
                let matches = arrOfWorkOrder[i] as! NSManagedObject
                
                let strAudioNameGlobal =  matches.value(forKey: "audioFilePath") as? String ?? ""
                
                if strAudioNameGlobal.count != 0 {
                    arrOfReturn.append(strAudioNameGlobal)
                }
            }
        }

        
        
        
        // Service Device Images
        let arrOfDeviceDocImages  = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfDeviceDocImages.count == 0 {
            print("no data in Device doc")
        }
        else{
            
            for i in 0..<arrOfDeviceDocImages.count{
                let matches = arrOfDeviceDocImages[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "audio".lowercased(){
                    if strImagePath?.count != 0 {
                        arrOfReturn.append(strImagePath!)
                    }
                }
            }
        }
    
        // Service Area Images
        
        let arrOfData2  = getDataFromCoreDataBaseArray(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfData2.count == 0 {
            print("no data in Area doc")
        }
        else{
            
            for i in 0..<arrOfData2.count{
                let matches = arrOfData2[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "audio".lowercased(){
                    if strImagePath?.count != 0 {
                        arrOfReturn.append(strImagePath!)
                    }
                }
            }
        }
        
        // CC Images workOrderId = %@ && sAConditionDocumentId = %@ && companyKey
        
        let arrOfDataCC  = getDataFromCoreDataBaseArray(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfDataCC.count == 0 {
            print("no data in  CC doc")
        }
        else{
            
            for i in 0..<arrOfDataCC.count{
                let matches = arrOfDataCC[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "audio".lowercased(){
                    if strImagePath?.count != 0 {
                        arrOfReturn.append(strImagePath!)
                    }
                }
            }
        }

        return arrOfReturn
    }
    
    @objc func fetchImagesToBeSync(strWoId: String)-> [String]{
        
        var arrOfReturn = [String]()
        
        //getting workorder signature images and one Audio file
        let arrOfWorkOrder = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arrOfWorkOrder.count == 0{
            print("no images")
        }
        else{
            
            for i in 0..<arrOfWorkOrder.count{
                
                let matches = arrOfWorkOrder[i] as! NSManagedObject
                
                let strCustomerSign = matches.value(forKey: "customerSignaturePath") as? String ?? ""
                let strTechSign = matches.value(forKey: "technicianSignaturePath") as? String ?? ""
                
                if strCustomerSign.count != 0 {
                    arrOfReturn.append(strCustomerSign)
                }
                
                if strTechSign.count != 0 {
                    arrOfReturn.append(strTechSign)
                }
                
            }
        }
        
        
        let arrOfPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if (arrOfPaymentInfo.count==0)
        {
            print("no images")
        }else
        {
            
            for i in 0..<arrOfPaymentInfo.count{
                
                let matches = arrOfPaymentInfo[i] as! NSManagedObject
                let strCheckFrontImage = matches.value(forKey: "checkFrontImagePath") as? String ?? ""
                let strCheckBackImage = matches.value(forKey: "checkBackImagePath") as? String ?? ""
                
                if strCheckFrontImage.count != 0 {
                    arrOfReturn.append(strCheckFrontImage)
                }
                
                if strCheckBackImage.count != 0 {
                    arrOfReturn.append(strCheckBackImage)
                }
                
            }
        }
        
        //Images for before Gallary
        
        let arrOfImagedetailServiceAuto = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arrOfImagedetailServiceAuto.count == 0
        {
            print("no data")
        }
        else{
            for i in 0..<arrOfImagedetailServiceAuto.count{
                let matches = arrOfImagedetailServiceAuto[i] as! NSManagedObject
                let strWoImagePath = matches.value(forKey: "woImagePath") as? String ?? ""
                if strWoImagePath.count != 0 {
                    arrOfReturn.append(strWoImagePath)
                }
            }
        }
        
        
        
        // Service Device Images
        let arrOfDeviceDocImages  = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfDeviceDocImages.count == 0 {
            print("no data in Device doc")
        }
        else{
            
            for i in 0..<arrOfDeviceDocImages.count{
                let matches = arrOfDeviceDocImages[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "image".lowercased(){
                    if strImagePath?.count != 0 {
                        arrOfReturn.append(strImagePath!)
                    }
                }
            }
        }
    
        // Service Area Images
        
        let arrOfData2  = getDataFromCoreDataBaseArray(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfData2.count == 0 {
            print("no data in Area doc")
        }
        else{
            
            for i in 0..<arrOfData2.count{
                let matches = arrOfData2[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "image".lowercased(){
                    if strImagePath?.count != 0 {
                        arrOfReturn.append(strImagePath!)
                    }
                }
            }
        }
        
        // CC Images workOrderId = %@ && sAConditionDocumentId = %@ && companyKey
        
        let arrOfDataCC  = getDataFromCoreDataBaseArray(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfDataCC.count == 0 {
            print("no data in  CC doc")
        }
        else{
            
            for i in 0..<arrOfDataCC.count{
                let matches = arrOfDataCC[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "image".lowercased(){
                    if strImagePath?.count != 0 {
                        arrOfReturn.append(strImagePath!)
                    }
                }
            }
        }
        return arrOfReturn

    }
    
    @objc func fetchDocumentToBeSync(strWoId: String, strServiceAutoUrlMain: String, strWoNo: String){
        
        var strModifyDateToSendToServerAll = ""
        
        let arrayOfWoLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceAutoModifyDate, predicate: NSPredicate(format: "userName == %@", Global().getUserName()))
        
        if arrayOfWoLocal.count > 0 {
            
            let objTemp = arrayOfWoLocal[0] as! NSManagedObject
            strModifyDateToSendToServerAll = "\(objTemp.value(forKey: "modifyDate") ?? "")"
            
        }
        
        let dictDataTemp = SalesCoreDataVC().fetchServiceData(fromDB: strModifyDateToSendToServerAll, strWoId, strWoNo)
        
        // Uploading Documnets To Server
        
        let arrOfAllDocumentsToSend = dictDataTemp.value(forKey: "arrOfAllDocumentsToSend") as! NSMutableArray
        
        for k1 in 0 ..< arrOfAllDocumentsToSend.count {
            
            let objTemp = arrOfAllDocumentsToSend[k1] as! NSDictionary
            
            let arrTemp = objTemp.allKeys as NSArray
            
            if arrTemp.contains("serviceAddressDocName") {
                
                var strServiceAddressDocPath = "\(objTemp.value(forKey: "serviceAddressDocPath") ?? "")"
                
                strServiceAddressDocPath = Global().strDocName(fromPath: strServiceAddressDocPath)
                
                let strURL = strServiceAutoUrlMain + UrlServicePestDocUploadAsync
                
                self.uploadImageServicePestNewFlow(strImageName: strServiceAddressDocPath, strUrll: strURL, strType: "serviceAddressDocNameSAPestDocuments" , dictDataTemp: objTemp, strIddd: strWoId)
                
            }else{
                
                var strServiceAddressDocPath = "\(objTemp.value(forKey: "documentPath") ?? "")"
                
                strServiceAddressDocPath = Global().strDocName(fromPath: strServiceAddressDocPath)
                
                let strURL = strServiceAutoUrlMain + UrlServicePestConditionDocUploadAsync
                
                self.uploadImageServicePestNewFlow(strImageName: strServiceAddressDocPath, strUrll: strURL, strType: "documentPathSAPestDocuments" , dictDataTemp: objTemp , strIddd: strWoId)
                
            }
            
        }
        
        // Service Device Doc
        let arrOfDeviceDocImages  = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfDeviceDocImages.count == 0 {
            print("no data in Device doc")
        }
        else{
            
            for i in 0..<arrOfDeviceDocImages.count{
                let matches = arrOfDeviceDocImages[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "pdf".lowercased(){
                    if strImagePath?.count != 0 {
                        let strURL = strServiceAutoUrlMain +  UrlServicePestConditionDocUploadAsync
                        self.uploadDocumentsServicePestNewFlow(strImageName: strImagePath!, strUrll: strURL)
                    }
                }
                if strDocumentType.lowercased() != "pdf".lowercased() && strDocumentType.lowercased() != "image".lowercased() && strDocumentType.lowercased() != "audio".lowercased(){
                    if strImagePath?.count != 0 {
                        let strURL = strServiceAutoUrlMain +  UrlServicePestConditionDocUploadAsync
                        self.uploadDocumentsServicePestNewFlow(strImageName: strImagePath!, strUrll: strURL)
                    }
                }
            }
        }
        
        // Service Area Images
        
        let arrOfData2  = getDataFromCoreDataBaseArray(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfData2.count == 0 {
            print("no data in Area doc")
        }
        else{
            
            for i in 0..<arrOfData2.count{
                let matches = arrOfData2[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "pdf".lowercased(){
                    if strImagePath?.count != 0 {
                        let strURL = strServiceAutoUrlMain +  UrlServicePestConditionDocUploadAsync
                        self.uploadDocumentsServicePestNewFlow(strImageName: strImagePath!, strUrll: strURL)
                    }
                }
                if strDocumentType.lowercased() != "pdf".lowercased() && strDocumentType.lowercased() != "image".lowercased() && strDocumentType.lowercased() != "audio".lowercased(){
                    if strImagePath?.count != 0 {
                        let strURL = strServiceAutoUrlMain +  UrlServicePestConditionDocUploadAsync
                        self.uploadDocumentsServicePestNewFlow(strImageName: strImagePath!, strUrll: strURL)
                    }
                }
            }
        }
        
        // CC Images workOrderId = %@ && sAConditionDocumentId = %@ && companyKey
        
        let arrOfDataCC  = getDataFromCoreDataBaseArray(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
        
        if arrOfDataCC.count == 0 {
            print("no data in  CC doc")
        }
        else{
            
            for i in 0..<arrOfDataCC.count{
                let matches = arrOfDataCC[i] as! NSManagedObject
                let strDocumentPath =  "\(matches.value(forKey: "documentPath") ?? "")"
                let strDocumentType =  "\(matches.value(forKey: "documentType") ?? "")"
                let strImagePath = Global().strDocName(fromPath: strDocumentPath)
                if strDocumentType.lowercased() == "pdf".lowercased(){
                    if strImagePath?.count != 0 {
                        let strURL = strServiceAutoUrlMain +  UrlServicePestConditionDocUploadAsync
                        self.uploadDocumentsServicePestNewFlow(strImageName: strImagePath!, strUrll: strURL)
                    }
                }
                if strDocumentType.lowercased() != "pdf".lowercased() && strDocumentType.lowercased() != "image".lowercased() && strDocumentType.lowercased() != "audio".lowercased(){
                    if strImagePath?.count != 0 {
                        let strURL = strServiceAutoUrlMain +  UrlServicePestConditionDocUploadAsync
                        self.uploadDocumentsServicePestNewFlow(strImageName: strImagePath!, strUrll: strURL)
                    }
                }
            }
        }
    }
    
    func uploadDocumentsServicePestNewFlow(strImageName : String , strUrll : String) {
        
        if fileAvailableAtPath(strname: strImageName) {
            
            //let imagee = getImagefromDirectory(strname: strImageName)
            
            //let dataToSend = imagee.jpegData(compressionQuality: 1.0)!
            
            let dataToSend = GetDataFromDocumentDirectory(strFileName: strImageName)
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strImageName, url: strUrll) { (responce, status) in
                
                if(status)
                {
                    debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        print(responce.value(forKey: "data")is NSDictionary)
                        
                    }
                    else{
                        print("failed to upload document")
                    }
                }
            }
        }
    }
    
    func uploadImageServicePestNewFlow(strImageName : String , strUrll : String , strType : String , dictDataTemp : NSDictionary , strIddd : String) {
        
        if fileAvailableAtPath(strname: strImageName) {
            
            //let imagee = getImagefromDirectory(strname: strImageName)
            
            //let dataToSend = imagee.jpegData(compressionQuality: 1.0)!
            
            let dataToSend = GetDataFromDocumentDirectory(strFileName: strImageName)

            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strImageName, url: strUrll) { (responce, status) in
                                
                if(status)
                {
                    debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        
                        if strType == "serviceAddressDocNameSAPestDocuments" {
                            
                            // Updating in DB that Doc is synced
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("isToUpload")
                            arrOfValues.add("no")
                            
                            let mobileServiceAddressDocId = "\(dictDataTemp.value(forKey: "mobileServiceAddressDocId") ?? "")"
                            
                            if mobileServiceAddressDocId.count > 0 {
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId = %@ && mobileServiceAddressDocId = %@ && companyKey = %@",strIddd,mobileServiceAddressDocId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            } else {
                                
                                let serviceAddressDocId = "\(dictDataTemp.value(forKey: "serviceAddressDocId") ?? "")"
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId = %@ && serviceAddressDocId = %@ && companyKey = %@",strIddd,serviceAddressDocId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            }
                            
                        } else if strType == "documentPathSAPestDocuments" {
                            
                            // Updating in DB that Doc is synced
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("isToUpload")
                            arrOfValues.add("no")
                            
                            let sAConditionDocumentId = "\(dictDataTemp.value(forKey: "sAConditionDocumentId") ?? "")"
                            
                            if sAConditionDocumentId.count > 0 {
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId = %@ && sAConditionDocumentId = %@ && companyKey = %@",strIddd,sAConditionDocumentId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            }
                            
                        } else {
                            
                        }
                        
                    }
                }
            }
        }
    }
}

     
                
            
