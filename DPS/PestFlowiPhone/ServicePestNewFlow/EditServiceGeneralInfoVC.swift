//
//  EditServiceGeneralInfoVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 11/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class EditServiceGeneralInfoVC: UIViewController, UITextFieldDelegate {
    
    //MARK: -IBOutlets
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var txtCategoryQuantity: UITextField!
    @IBOutlet weak var txtCategoryPrice: UITextField!
    @IBOutlet weak var btnIsTaxble: UIButton!

    //MARK: -Variables
    var strEditServiceName = ""
    var strEditServiceQuantity = ""
    var strEditServicePrice = ""
    var strEditServiceId = ""
    var strEditIsTaxble = ""
    var strWoId = ""
    let global = Global()

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblCategoryName.text = strEditServiceName
        self.txtCategoryQuantity.text = strEditServiceQuantity
        self.txtCategoryPrice.text = strEditServicePrice
        
        txtCategoryPrice.delegate = self
        txtCategoryQuantity.delegate = self
        if strEditIsTaxble == "true" || strEditIsTaxble == "1"{
            
             btnIsTaxble.setImage(UIImage (named: "check_box_2.png"), for: .normal)
            
        }else{
            
            btnIsTaxble.setImage(UIImage (named: "check_box_1.png"), for: .normal)
            
        }
    }
    
    //MARK: - UItetfield Delegate
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
            
            if ( textField == txtCategoryPrice)
            {
                let chk = decimalValidation(textField: textField, range: range, string: string)
                if chk == false{
                    self.view.makeToast("Please enter valid entity")
                }
                return chk
            }
            if textField == txtCategoryQuantity  {
                
                let isValidd = txtFieldValidation(textField: txtCategoryQuantity, string: string, returnOnly: "NUMBER", limitValue: 2)
                
                return isValidd
            }
            return true
        
    }
    
    //MARK: -IBAction
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnUpdateAction(_ sender: Any) {
        if txtCategoryQuantity.text == "0"  || txtCategoryQuantity.text == "" {
            self.view.makeToast("Quantity should not be 0")
        }
        else{
            if txtCategoryPrice.text == ""{
                strEditServicePrice =  "0.00"
            }
            else{
                strEditServicePrice =  txtCategoryPrice.text!
            }
            strEditServiceQuantity =  txtCategoryQuantity.text!
            EditService()
            
        }
    }
    
    @IBAction func btnTaxableAction(_ sender: Any) {
         
        if strEditIsTaxble == "true" || strEditIsTaxble == "1"{
            strEditIsTaxble = "false"
             btnIsTaxble.setImage(UIImage (named: "check_box_1.png"), for: .normal)
            
        }
        else if strEditIsTaxble == "false" || strEditIsTaxble == "0"{
            strEditIsTaxble = "true"
            btnIsTaxble.setImage(UIImage (named: "check_box_2.png"), for: .normal)
        }
    }
    
    func EditService(){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("servicePrice")
        arrOfKeys.add("serviceQuantity")
        arrOfKeys.add("taxable")

        arrOfValues.add(strEditServicePrice)
        arrOfValues.add(strEditServiceQuantity)
        arrOfValues.add(strEditIsTaxble)
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@ && serviceId == %@", strWoId, strEditServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            self.updateModifyDate()
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            self.navigationController?.popViewController(animated: false)
            print("Success")
        }
        else {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
    }
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
}
