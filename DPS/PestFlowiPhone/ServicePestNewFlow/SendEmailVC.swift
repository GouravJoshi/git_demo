//
//  SendEmailVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 09/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SendEmailVC: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewOfDocuments: UITableView!
    
    @IBOutlet weak var lblFromEmailId: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSendEmail: UIButton!
    
    @IBOutlet weak var btnTakeServey: UIButton!
    
    @IBOutlet weak var widthConstranitOfTakeSurvey: NSLayoutConstraint!
    
    
    @IBOutlet weak var btnPDFContinue: UIButton!
    @IBOutlet weak var heightConstraintForEmailTbl: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForDocTbl: NSLayoutConstraint!
    
    @IBOutlet weak var srlView: UIScrollView!
    @IBOutlet weak var webViewForPDF: WKWebView!
    @IBOutlet weak var lblHeaderTitle: UILabel!

    //MARK: - Variable
    var activityIndicator: UIActivityIndicatorView!
    let dispatch_group_FinalSync = DispatchGroup()
    var loader = UIAlertController()

    var isCustomerPresent = ""
    var fromWhere = ""
    
 
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWoId = ""
    var dictLoginData = NSDictionary()
    
    var strCompanyKey = String()
    var strUserName = String()
    var strEmpID = String()
    var strEmpName = String()
    var strFromEmail = String()
    var strServiceReportType = String()
    
    @objc var objEmailSent = NSMutableArray()
    @objc var objDocumentEmails = NSMutableArray()
    
    var strServiceUrlMainServiceAutomation = ""
    var strDefaultEmployeeEmail = ""
    var strCompanyIdResendMail = ""
    var dictDetailsFortblView = NSDictionary()
    var matchesServiceDynamic = NSManagedObject()
    var strIncompleteSyncForPDF = Bool()
    var strBackRoundImage = UIImage()
    var IsForReset = Bool()
    var isPreview = false

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getTheDataFromLoginNSUD()
        getWorkorderDetaildata()
        tableViewDelegateDatasourcesAndSettings()
        DeleteAndSaveDefaultEmails()
        loadDBDatas()
        getSendMailTitle()
        getTakeServeyIsHiddenOrNot()
        showPreviewData()
        
        //Reset
        let isFromSurvey = nsud.bool(forKey: "isFromSurveyToSendEmail")
        if isFromSurvey {
            if strBackRoundImage == nil{
                let viewTemp = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
                viewTemp.backgroundColor = UIColor.white
                view.addSubview(viewTemp)
            }
            else{
                let imgViewBackround = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
                imgViewBackround.image = strBackRoundImage
                view.addSubview(imgViewBackround)
            }
            
            nsud.setValue(false, forKey: "isFromSurveyToSendEmail")
            nsud.synchronize()
            
            if !isInternetAvailable(){
                
                goToAppointmentView()
                showAlertWithoutAnyAction(strtitle: "Info", strMessage: "Data saved offline", viewcontrol: self)
                
            }
            else
            {
                IsForReset = true
                self.perform(#selector(callMethodAfterDelay), with: nil, afterDelay: 0.2)
            }
        }
        
        nsud.setValue(true, forKey: "isServiceSurvey")
        nsud.setValue(false, forKey: "isMechanicalSurvey")
        nsud.setValue(false, forKey: "isNewSalesSurvey")

        //Temp Disable Nilind 03 Sept 2021
        
        //btnTakeServey.isHidden = false
    }
    
    func showPreviewData()  {
        if isPreview {
            lblHeaderTitle.text = "Agreement Preview"

            webViewForPDF.isHidden = false
            webViewForPDF.navigationDelegate = self
            webViewForPDF.uiDelegate = self
            
            activityIndicator = UIActivityIndicatorView()
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = UIActivityIndicatorView.Style.gray

            view.addSubview(activityIndicator)

            if strIncompleteSyncForPDF == true{
                
                srlView.isHidden = true
                webViewForPDF.isHidden = false
//                btnPDFContinue.isHidden = false
                
                if !isInternetAvailable(){
                    
                    webViewForPDF.isHidden = true
//                    btnPDFContinue.isHidden = true
                    srlView.isHidden = false
                    strIncompleteSyncForPDF = false
                }else{
                    self.perform(#selector(callMethodAfterDelay), with: nil, afterDelay: 0.2)
                }
                
            }
            else{
                webViewForPDF.isHidden = true
//                btnPDFContinue.isHidden = true
                srlView.isHidden = false
            }
        }else {
            lblHeaderTitle.text = "Send Email"

//            btnPDFContinue.isHidden = true
            webViewForPDF.isHidden = true
            srlView.isHidden = false
        }
        
    }
    
    //MARK: - Custom Function
    func goToAppointmentViewNew() {
        
        let defsAppointemnts = UserDefaults.standard
        
        let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as? String
        
        if strAppointmentFlow == "New" {
            
            var index = 0
            let arrstack = navigationController?.viewControllers
            for k1 in 0..<arrstack!.count {
                if arrstack![k1] is AppointmentVC {
                    index = k1
                }
            }
            let myController = navigationController?.viewControllers[index] as? AppointmentVC
            if let myController = myController {
                navigationController?.popToViewController(myController, animated: false)
            }

        }
        
        else{
            var index = 0
            let arrstack = navigationController?.viewControllers
            for k1 in 0..<(arrstack?.count ?? 0) {
                if arrstack?[k1] is AppointmentView {
                    index = k1
                }
            }
            let myController = navigationController?.viewControllers[index] as? AppointmentView
            if let myController = myController {
                navigationController?.popToViewController(myController, animated: false)
            }
        }
    }
    
    func getTakeServeyIsHiddenOrNot(){
        
        //UD key YesSurveyService is true then hidden
        
        var isTakeServeyTrue = Bool()
        isTakeServeyTrue = nsud.bool(forKey: "YesSurveyService")
        var isTakeServeyAleadyCompleted = Bool()
        isTakeServeyAleadyCompleted = global.isSurveyCompletedService(strWoId)
        let strSurveyId = nsud.value(forKey:"SurveyID") as! String

        if isTakeServeyTrue && isCustomerPresent.lowercased() != "no".lowercased() && isTakeServeyAleadyCompleted != true && strSurveyId != "0" {
            btnTakeServey.isHidden = false
            switch UIDevice.current.userInterfaceIdiom {
            
            case .phone:
                widthConstranitOfTakeSurvey.constant = 100

            case .pad:
                widthConstranitOfTakeSurvey.constant = 200

            default:
                break
            }
        }
        else{
            btnTakeServey.isHidden = true
            widthConstranitOfTakeSurvey.constant = 0
        }
        
//        //customer not present then hide it
//        if isCustomerPresent == "no"{
//            btnTakeServey.isHidden = true
//            widthConstranitOfTakeSurvey.constant = 0
//        }
//        else{
//            btnTakeServey.isHidden = false
//            switch UIDevice.current.userInterfaceIdiom {
//
//            case .phone:
//                widthConstranitOfTakeSurvey.constant = 100
//
//            case .pad:
//                widthConstranitOfTakeSurvey.constant = 200
//
//            default:
//                break
//            }
//
//        }
//
//        // isServeryCompleted then hide
//        var isTakeServeyAleadyCompleted = Bool()
//
//        isTakeServeyAleadyCompleted = global.isSurveyCompletedService(strWoId)
//
//        if isTakeServeyAleadyCompleted == true{
//            btnTakeServey.isHidden = true
//            widthConstranitOfTakeSurvey.constant = 0
//        }
//        else{
//            btnTakeServey.isHidden = false
//            switch UIDevice.current.userInterfaceIdiom {
//
//            case .phone:
//                widthConstranitOfTakeSurvey.constant = 100
//
//            case .pad:
//                widthConstranitOfTakeSurvey.constant = 200
//
//            default:
//                break
//            }
//
//        }
//
//        let strSurveyId = nsud.value(forKey:"SurveyID") as! String
//
//        if strSurveyId == "0"{
//            btnTakeServey.isHidden = true
//            widthConstranitOfTakeSurvey.constant = 0
//        }
//        else{
//            btnTakeServey.isHidden = false
//            switch UIDevice.current.userInterfaceIdiom {
//
//            case .phone:
//                widthConstranitOfTakeSurvey.constant = 100
//
//            case .pad:
//                widthConstranitOfTakeSurvey.constant = 200
//
//            default:
//                break
//            }
//
//        }
//
        
    }
    func getSendMailTitle(){
        
        let isResendInvoiceMailtrue = "\(objWorkorderDetail.value(forKey: "isResendInvoiceMail") ?? "")"
        
        if isResendInvoiceMailtrue.lowercased() == "true".lowercased() ||  isResendInvoiceMailtrue == "1"{
            
            btnSendEmail.setTitle("Re-Send Mail", for: .normal)
        }
        else{
            btnSendEmail.setTitle("Send Mail", for: .normal)
            
            let strSendServiceReport = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SendServiceReport") ?? "")"
            
            if  strSendServiceReport.lowercased() == "AfterWorkorderCompletion".lowercased(){
                btnSendEmail.setTitle("Send Mail", for: .normal)
                
            }
            else{
                btnSendEmail.setTitle("Save Preference", for: .normal)
                
            }
        }
    }
    
    func getWorkorderDetaildata(){
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
        }
        
    }
    
    func getTheDataFromLoginNSUD(){
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        strServiceReportType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportFormat") ?? "")"
        
        let strServiceReportTypeLocal = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
        
        if strServiceReportTypeLocal == "CompanyEmail"
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
        }
        else
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        }
        
        strServiceUrlMainServiceAutomation = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") ?? "")"
        
        strDefaultEmployeeEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        
        strCompanyIdResendMail =  "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId")!)"
        
        dictDetailsFortblView =  nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        
    }
    
    func tableViewDelegateDatasourcesAndSettings(){
        
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.reloadData()
        calculateHeightForTabls()
        
        tblViewOfDocuments.delegate = self
        tblViewOfDocuments.dataSource = self
        
        lblFromEmailId.text = strFromEmail
        self.tblView.tableFooterView = UIView()
        self.tblViewOfDocuments.tableFooterView = UIView()
    }
    
    
    func loadDBDatas(){
        fetchEmailDetailServiceAuto()
        fetchSendDocumentList()
    }
    
    func DeleteAndSaveDefaultEmails(){
        //Delete and featch for defaut emails
        DeleteEmailIdFromCoreData()
        fetchDefaultEmailFromDb()
    }
    
    func DeleteEmailIdFromCoreData(){
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isDefaultEmail == %@", strWoId, "true"))
        
        for i in 0..<arrOfData.count{
            let objTemp = arrOfData[i] as! NSManagedObject
            deleteDataFromDB(obj: objTemp)
        }
    }
    
    func fetchDefaultEmailFromDb(){
        
        let arrOfDefaultEmailDB = dictDetailsFortblView.value(forKey: "DefaultEmails") as! NSArray
        
        let strBranchIdFromWorkorderDetail = "\(objWorkorderDetail.value(forKey: "branchId") ?? "")"
        
        if arrOfDefaultEmailDB.count > 0 {
            let arrOfDefaultsEmails = NSMutableArray()
            let arrOfDefaultsEmailsInvoiceId = NSMutableArray()
            for i in 0..<arrOfDefaultEmailDB.count{
                
                let dict = arrOfDefaultEmailDB[i] as! NSDictionary
                let tempBranchId = String(describing: dict.value(forKey: "BranchId") as! NSNumber)
                
                if tempBranchId == strBranchIdFromWorkorderDetail{
                    print("mached")
                    let tempEmailId = dict.value(forKey: "EmailId") as! String
                    if tempEmailId == "##EmployeeEmail##" {
                        if objDocumentEmails.contains(strDefaultEmployeeEmail){
                            print("Email exist")
                        }
                        else{
                            print("save to DB")
                            saveDataToDB(strEmailId: strDefaultEmployeeEmail, strIsDefaultEmail: "true")
                        }
                    }
                    else{
                        arrOfDefaultsEmails.add(tempEmailId)
                        arrOfDefaultsEmailsInvoiceId.add(dict.value(forKey: "WoInvoiceMailId") as? String ?? "" )
                    }
                }
                
            }
            saveEmailToCoreDataDefaults(arrEmailDetail: arrOfDefaultsEmails, arrOfDefaultsEmailsInvoiceId: arrOfDefaultsEmailsInvoiceId)
            
        }
        else{
            
        }
        
        loadDBDatas()
    }
    
    
    
    func methodOnResponse(){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("zSync")
        arrOfKeys.add("isResendInvoiceMail")

        arrOfValues.add("no")
        arrOfValues.add("True")

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: "Success", strMessage: "Data Synced Successfully", viewcontrol: self)
            
        }
        
        self.goToAppointmentView()
        
    }
    
    func saveEmailToCoreDataDefaults(arrEmailDetail: NSMutableArray, arrOfDefaultsEmailsInvoiceId: NSMutableArray){
        
        for i in 0..<arrEmailDetail.count {
            if objDocumentEmails.contains(arrEmailDetail.object(at: i)){
                
            }
            else{
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("emailId")
                arrOfKeys.add("isInvoiceMailSent")
                arrOfKeys.add("isMailSent")
                arrOfKeys.add("workorderId")
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("isCustomerEmail")
                arrOfKeys.add("woInvoiceMailId")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                arrOfKeys.add("subject")
                arrOfKeys.add("isDefaultEmail")
                
                
                arrOfValues.add(arrEmailDetail.object(at: i))
                arrOfValues.add("false")
                arrOfValues.add("true")
                arrOfValues.add(strWoId)
                arrOfValues.add("0")
                arrOfValues.add("0")
                arrOfValues.add("true")
                arrOfValues.add(arrOfDefaultsEmailsInvoiceId.object(at: i))
                arrOfValues.add("")
                arrOfValues.add(global.modifyDateService())
                arrOfValues.add("")
                arrOfValues.add("true")
                
                
                saveDataInDB(strEntity: "EmailDetailServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
        }
        
        loadDBDatas()
    }
    
    func gotoBeginAuditView(){
        
        switch UIDevice.current.userInterfaceIdiom {
        
        case .phone:
            if UIScreen.main.bounds.size.height == 480.0 {
                //move to your iphone5 xib
                let bav = BeginAuditView(nibName: "BeginAuditView", bundle: nil)
                bav.isFromSendEmail = "true"
                navigationController?.pushViewController(bav, animated: false)
                
            } else {
                //move to your iphone4s xib
                let bav = BeginAuditView(nibName: "BeginAuditView_iphone5", bundle: nil)
                bav.isFromSendEmail = "true"
                navigationController?.pushViewController(bav, animated: false)
            }

        case .pad:
            
            let bav = BeginAuditViewiPad(nibName: "BeginAuditView_iPad", bundle: nil)
            bav.isFromSendEmail = "true"
            navigationController?.pushViewController(bav, animated: false)
            
        default:
            break
            
        }
        
    }
    
    func goToAppointmentView(){
        let strAppointmentFlow = UserDefaults.standard.value(forKey: "AppointmentFlow")
        
        if strAppointmentFlow as! String == "New"{
            
            if DeviceType.IS_IPAD
            {
                let mainStoryboard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                navigationController?.pushViewController(objByProductVC!, animated: false)
            }
            else
            {
                let mainStoryboard = UIStoryboard(name: "Appointment", bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                navigationController?.pushViewController(objByProductVC!, animated: false)
            }
            
        }
        else{
            
            if DeviceType.IS_IPAD
            {
                let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                navigationController?.pushViewController(objByProductVC!, animated: false)
            }
            else
            {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                navigationController?.pushViewController(objByProductVC!, animated: false)
            }
           
            
        }
    }
    
    @objc func callMethodAfterDelay(){
        
        if !isInternetAvailable()
        {
            
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "No internet connection", viewcontrol: self)
            
        }else{
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)

            // Syncing Images All
            
            dispatch_group_FinalSync.enter()
            let arrOfStringImages = GeneralInfoFinalJSONSyncingDBFuncs().fetchImagesToBeSync(strWoId: self.strWoId)
            let arrOfImages = self.getUIImageFromString(arrOfImagesString : arrOfStringImages)
            ServiceGalleryService().uploadImageForGeneralInfo(_strServiceUrlMainServiceAutomation: self.strServiceUrlMainServiceAutomation, arrOfImages, arrOfStringImages) { (object, error) in
                
                self.dispatch_group_FinalSync.leave()

                if let object = object {
                    print(object)
                } else {
                    print(error ?? "")
                }
                
            }

            // Syncing Audio
            let arrofAudios = GeneralInfoFinalJSONSyncingDBFuncs().fetchAudioToBeSync(strWoId: self.strWoId)
            
            for i in 0..<arrofAudios.count{
                let strAudio = arrofAudios[i]
                if strAudio != ""{
                    self.uploadAudioService(strAudioName: strAudio, strUrll: self.strServiceUrlMainServiceAutomation + UrlAudioUploadAsync)
                }
            }
            
            // Syncing Other Documents
            dispatch_group_FinalSync.enter()
            GeneralInfoFinalJSONSyncingDBFuncs().fetchDocumentToBeSync(strWoId: self.strWoId, strServiceAutoUrlMain:  "\(self.dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") ?? "")" , strWoNo:  "\(self.objWorkorderDetail.value(forKey: "workOrderNo") ?? "")")
            self.dispatch_group_FinalSync.leave()
            
            // Syncing Dynamic Form
            self.fetchFromCoreDataToSendServiceDynamic()
            
            // Syncing Device Dynamic Form
            self.fetchFromCoreDataToSendDeviceDynamicForms()
            
            // Changed By Saavan All Docs Images Audio etc etc Synced
            self.dispatch_group_FinalSync.notify(queue: DispatchQueue.main, execute: {
                
                let dataToBeSync = self.createFinalJson()
                
                if dataToBeSync.count != 0{
                    
                    self.callWebServiceToUploadFinalJsonForSyncing()
                    
                }
                else{
                    
                    self.loader.dismiss(animated: false) {
                        
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                    }
                    
                }
                
            })
            
        }
        
    }
    
    func uploadAudioService(strAudioName : String , strUrll : String) {
        
        if fileAvailableAtPath(strname: strAudioName) {
            
            self.dispatch_group_FinalSync.enter()
            
            let dataToSend = GetDataFromDocumentDirectory(strFileName: strAudioName)
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strAudioName, url: strUrll) { (responce, status) in
                
                self.dispatch_group_FinalSync.leave()
                
                if(status)
                {
                    debugPrint(responce)

                }
            }
        }
    }
    
    func getUIImageFromString(arrOfImagesString: [String]) -> [UIImage]{
        var arrOfUIImages = [UIImage]()
        
        for i in 0..<arrOfImagesString.count{
            print(arrOfImagesString[i])
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: arrOfImagesString[i])
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: arrOfImagesString[i])
            print("\(arrOfImagesString[i]) image exist \(isImageExists)")

            if isImageExists == true{
                arrOfUIImages.append(image!)
            }
            
        }
        
        return arrOfUIImages
    }
    
    
    //MARK: -WEbview delelates
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        showActivityIndicator(show: false)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showActivityIndicator(show: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showActivityIndicator(show: false)
    }
    
    func showActivityIndicator(show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    //MARK: -IBActions
    
    @IBAction func btnPDFContinueAction(_ sender: Any) {
        btnPDFContinue.isHidden = true
        webViewForPDF.isHidden = true
        srlView.isHidden = false
    }
    @IBAction func btnTakeServeyAction(_ sender: Any) {
        
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "No internet connection", viewcontrol: self)
        }
        else{
            gotoBeginAuditView()
        }
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnAddEmailAction(_ sender: Any) {
        addNewEmailId()
        self.txtEmail.text = ""
        fetchEmailDetailServiceAuto()
        
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnSendEmailAction(_ sender: Any) {
        
        let isResendInvoiceMailtrue = "\(objWorkorderDetail.value(forKey: "isResendInvoiceMail") ?? "")"
        
        if isResendInvoiceMailtrue.lowercased() == "true".lowercased() ||  isResendInvoiceMailtrue == "1"{
            
            let emailCount = emailCountFetch()
            if emailCount == 0
            {
                global.alertMethod("Alert!", "No email to send")
            }
            else
            {
                callWebServicePostResendEmail()
                
            }
        }
        else{
            
            let emailCount = emailCountFetch()
            if emailCount == 0
            {
                global.alertMethod("Alert!", "No email to send")
            }
            else
            {
                if !isInternetAvailable(){
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("workorderStatus")
                    arrOfValues.add("Completed")
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId = %@",strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //goToAppointmentView()
                        //global.updateWoIsMailSent(strWoId, "true")
                        //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Data saved offline", viewcontrol: self)
                        
                        let alert = UIAlertController(title: alertInfo, message: "Data saved offline", preferredStyle: .alert)

                        alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in

                            self.global.updateWoIsMailSent(self.strWoId, "true")
                            self.goToAppointmentView()
                        }))

                        alert.popoverPresentationController?.sourceView = self.view
                        self.present(alert, animated: true, completion: {
                        })
                        
                        
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }
                    
                }
                else{
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("workorderStatus")
                    arrOfValues.add("Completed")
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId = %@",strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        self.perform(#selector(callMethodAfterDelay), with: nil, afterDelay: 0.2)
                        
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }
                    
                }
            }
        }
    }
    
    
    //MARK: - DB Func
    func fetchFromCoreDataToSendServiceDynamic(){
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceDynamicForm", predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfData.count == 0 {
            
            
        }
        else{
            
            matchesServiceDynamic = arrOfData[0] as! NSManagedObject;
            
            if matchesServiceDynamic.value(forKey: "arrFinalInspection") is NSDictionary {
                
                let arrTemp = matchesServiceDynamic.value(forKey: "arrFinalInspection") as! NSDictionary
                
                if arrTemp.count > 0 {
                    sendingFinalDynamicJsonToServerService(dictToBeSend: arrTemp)
                }
                else{
                    print(Sorry)
                }
                
            }else if matchesServiceDynamic.value(forKey: "arrFinalInspection") is NSArray {
                
                let arrTemp = matchesServiceDynamic.value(forKey: "arrFinalInspection") as! NSArray
                
                if arrTemp.count > 0 {
                    
                    if arrTemp[0] is NSDictionary {
                        
                        sendingFinalDynamicJsonToServerService(dictToBeSend: arrTemp[0] as! NSDictionary)
                        
                    }else{
                        print(Sorry)
                    }
                    
                }
                else{
                    print(Sorry)
                }
                
            }
                        
        }
        
    }
    
    func createFinalJson() -> [String: Any]{

        

        var dictOfDeailSerViewOrder = [String: Any]()

         

        if IsForReset == true{

            dictOfDeailSerViewOrder = GeneralInfoFinalJSONSyncingDBFuncs().fetchWorkOrderDetailServiceAuto(strWoId: strWoId, IsFromPDF: strIncompleteSyncForPDF, IsReset: true)

        }

        else{

            dictOfDeailSerViewOrder = GeneralInfoFinalJSONSyncingDBFuncs().fetchWorkOrderDetailServiceAuto(strWoId: strWoId, IsFromPDF: strIncompleteSyncForPDF, IsReset: false)

        }

        

        let dictOfWOProductDetail = GeneralInfoFinalJSONSyncingDBFuncs().fetchWOProductDetail(strWoId: strWoId)

        

        let dictOfPaymentInfoServiceAuto = GeneralInfoFinalJSONSyncingDBFuncs().fetchPaymentInfoServiceAuto(strWoId: strWoId)

        

        let dictOfImageDetailserviceAuto = GeneralInfoFinalJSONSyncingDBFuncs().fetchImageDetailServiceAuto(strWoId: strWoId)

        

        let dictOfEmailDetailServiceAuto = GeneralInfoFinalJSONSyncingDBFuncs().fetchEmailDetailServiceAuto(strWoId: strWoId)

        

        let dictOfWorkOrderDocuments = GeneralInfoFinalJSONSyncingDBFuncs().fetchWorkOrderDocuments(strWoId: strWoId)

        

        let dictOfServicePestDataAllFromDBObjC = GeneralInfoFinalJSONSyncingDBFuncs().fetchServicePestDataAllFromDBObjC(strWoId: strWoId)

        

        let dictOfServiceCategory = GeneralInfoFinalJSONSyncingDBFuncs().fetchWOServiceCategoryGeneralInfo(strWoId: strWoId, strWoNo: "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")")

        

        let dictOfServiceAreaDocumentsDc = GeneralInfoFinalJSONSyncingDBFuncs().fetchServiceAreaDocumentsDc(strWoId: strWoId)

         

        let dictOfServiceDeviceDocumentsDc = GeneralInfoFinalJSONSyncingDBFuncs().fetchServiceDeviceDocumentsDc(strWoId: strWoId)

        

        let dictOfTimeSheet = GeneralInfoFinalJSONSyncingDBFuncs().fetchWOServiceStartStopDict(strWoId: strWoId)

        

        let dictOfFloridaTermiteServiceDetail = ["FloridaTermiteServiceDetail": ""] as [String:Any]

        let dictOfImageDetailsTermite = ["ImageDetailsTermite" : []] as [String:Any]

        let dictOfTexasTermiteServiceDetail = ["TexasTermiteServiceDetail" : ""] as [String: Any]

        let dictOfWoEquipmentDetail = ["WoEquipmentDetail" : []] as [String: Any]

        

        

        var dictOfIsPreview = [String: Any]()

        

        if strIncompleteSyncForPDF == true{

            dictOfIsPreview = ["IsPreview" : true]


        }

        else{

            dictOfIsPreview = ["IsPreview" : ""]

        }

        

        var finalDict = [String: Any]()

        

        finalDict.mergeDict(dict: dictOfDeailSerViewOrder)

        finalDict.mergeDict(dict: dictOfWOProductDetail)

        finalDict.mergeDict(dict: dictOfPaymentInfoServiceAuto)

        finalDict.mergeDict(dict: dictOfImageDetailserviceAuto)

        finalDict.mergeDict(dict: dictOfEmailDetailServiceAuto)

        finalDict.mergeDict(dict: dictOfWorkOrderDocuments)

        finalDict.mergeDict(dict: dictOfServicePestDataAllFromDBObjC)

        finalDict.mergeDict(dict: dictOfFloridaTermiteServiceDetail)

        finalDict.mergeDict(dict: dictOfImageDetailsTermite)

        finalDict.mergeDict(dict: dictOfTexasTermiteServiceDetail)

        finalDict.mergeDict(dict: dictOfWoEquipmentDetail)

        finalDict.mergeDict(dict: dictOfIsPreview)

        finalDict.mergeDict(dict: dictOfServiceCategory)

        finalDict.mergeDict(dict: dictOfServiceAreaDocumentsDc)

        finalDict.mergeDict(dict: dictOfServiceDeviceDocumentsDc)

        finalDict.mergeDict(dict: dictOfTimeSheet)

        print("Final JSON: \(finalDict)")

        

        return finalDict

    }

    
    func emailCountFetch() -> Int{
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        var intCountOfEmails = 0
        for i in 0..<arrOfData.count{
            
            let objTemp = arrOfData[i] as! NSManagedObject
            let strIsMailSent = "\(objTemp.value(forKey: "isMailSent") ?? "")"
            let strIsInvoiceMailSent = "\(objTemp.value(forKey: "isInvoiceMailSent") ?? "")"

            if strIsMailSent.lowercased() == "true".lowercased() || strIsInvoiceMailSent.lowercased() ==  "true".lowercased(){
                intCountOfEmails = 1 + intCountOfEmails
            }
        }
        return intCountOfEmails
    }
    
    func fetchEmailDetailServiceAuto(){
        
        objEmailSent.removeAllObjects()
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        for i in 0..<arrOfData.count{
            
            let objTemp = arrOfData[i] as! NSManagedObject
            objEmailSent.add(objTemp)
        }
        
        
        for p in 0..<objEmailSent.count{
            
            let objTemp = objEmailSent[p] as! NSManagedObject
            let strEmailId = "\(objTemp.value(forKey: "emailId") ?? "")"

            let arrOfFalseObj = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && emailId == %@", strWoId, strEmailId))
            
            if arrOfFalseObj.count > 1{
                for q in 0..<arrOfFalseObj.count{
                    if arrOfFalseObj.count == q{
                        break
                    }
                    let objTemp = arrOfFalseObj[q] as! NSManagedObject
                    deleteDataFromDB(obj: objTemp)
                }
            }
        }
        
        
        objEmailSent.removeAllObjects()
        let arrOfData1 = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        for i in 0..<arrOfData1.count{
            
            let objTemp = arrOfData1[i] as! NSManagedObject
            objEmailSent.add(objTemp)
        }
        
        self.tblView.reloadData()
        calculateHeightForTabls()
    }
    
    func fetchSendDocumentList(){
        
        objDocumentEmails.removeAllObjects()
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        for i in 0..<arrOfData.count{
            
            let objTemp = arrOfData[i] as! NSManagedObject
            objDocumentEmails.add(objTemp)
        }
        
        tblViewOfDocuments.reloadData()
    }
    
    func addNewEmailId(){
        
        if txtEmail.text == ""{
            self.view.makeToast("Please add Email id")
        }
        else{
            var check = Bool()
            for i in 0..<objEmailSent.count{
                let objTemp = objEmailSent[i] as! NSManagedObject
                let strEmailId = "\(objTemp.value(forKey: "emailId") ?? "")"
                
                if strEmailId == txtEmail.text{
                    check = true
                }
            }
            
            if check == true{
                self.view.makeToast("Email Id already exist")
            }
            else{
                if txtEmail.text?.isValidEmail() == false {
                    self.view.makeToast("Please enter valid Email Id")
                }
                else{
                    saveDataToDB(strEmailId: "", strIsDefaultEmail: "false")
                }
            }
        }
    }
    
    
    
    func saveDataToDB(strEmailId:String, strIsDefaultEmail: String){
        var saveMailID = ""
        if txtEmail.text == ""{
            saveMailID = strEmailId
        }
        else{
            saveMailID = txtEmail.text!
        }
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("emailId")
        arrOfKeys.add("isInvoiceMailSent")
        arrOfKeys.add("isMailSent")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("isCustomerEmail")
        arrOfKeys.add("woInvoiceMailId")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("subject")
        arrOfKeys.add("isDefaultEmail")
        
        arrOfValues.add(saveMailID)
        arrOfValues.add("false")
        arrOfValues.add("true")
        arrOfValues.add(strWoId)
        arrOfValues.add("0")
        arrOfValues.add("0")
        arrOfValues.add("true")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add(global.modifyDateService())
        arrOfValues.add("")
        arrOfValues.add(strIsDefaultEmail)
        
        
        saveDataInDB(strEntity: "EmailDetailServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        updateModifyDate()
        updateServicePestModifyDate(strWoId: self.strWoId as String)
        
        
    }
    
    func updateDocumentTitleChecked(strIsChecked: String, strEmailId: String){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("IsChecked")
        
        arrOfValues.add(strIsChecked)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId == %@ && title == %@", strWoId, strEmailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            print("Success")
            updateModifyDate()
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
        tblViewOfDocuments.reloadData()
    }
    
    func updateIsInvoiceMailSentInTOEmailServiceAuto(strIsInvoiceMailSent: String, strEmailId: String){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("isInvoiceMailSent")
        
        arrOfValues.add(strIsInvoiceMailSent)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && emailId == %@", strWoId, strEmailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            print("Success")
            updateModifyDate()
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
        tblView.reloadData()
        calculateHeightForTabls()

    }
    
    func updateIsServiceDetailMailSentInTOEmailServiceAuto(strIsServiceDetailMailSent: String, strEmailId: String){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("isMailSent")
        
        arrOfValues.add(strIsServiceDetailMailSent)
        
        let isSuccess =   getDataFromDbToUpdate(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && emailId == %@", strWoId, strEmailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            print("Success")
            updateModifyDate()
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
            
        } else {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
        
        tblView.reloadData()
        calculateHeightForTabls()

    }
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    //MARK: - Call Web service
    func callWebServiceToUploadFinalJsonForSyncing(){
        
        let errorNew: Error? = nil
        var json: Data? = nil
        var jsonString = String()

       /* if JSONSerialization.isValidJSONObject(createFinalJson()) {
            // Serialize the dictionary
            do {
                json = try JSONSerialization.data(withJSONObject: createFinalJson(), options: .prettyPrinted)
            } catch let errorNew {
                print(errorNew)
            }
            
            
            // If no errors, let's view the JSON
            if json != nil && errorNew == nil {
            
                jsonString = String(data: json!, encoding: .utf8)
                print("Service Automation JSON Send Mail: \(String(describing: jsonString))")
            }
        }*/
                
        if JSONSerialization.isValidJSONObject(createFinalJson()) {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: createFinalJson(), options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print("Service Automation JSON Send Mail: \(jsonString)")
            
        }
        
        let requestData = jsonString.data(using: .utf8)
        let strUrl = String(format: "%@%@", strServiceUrlMainServiceAutomation,UrlUpdateWorkorderDetail)
        let dictTemp = [String: Any]()
        
        DispatchQueue.global(qos: .default).async(execute: { [self] in
            
            global.getServerResponseForSendLead(toServer: strUrl, requestData, dictTemp, "", "serviceOrder", withCallback: { [self] success, response, error in
                DispatchQueue.main.async(execute: { [self] in
                    
                    self.loader.dismiss(animated: false) {

                        if success {
                            
                            global.updateWoIsMailSent(strWoId, "false")

                            if strIncompleteSyncForPDF == true{
                                
                                print("is preview get link with incomplete status")
                                
                                let strIsPreviewPDFUrl = response!["PdfReportUrl"] as! String
                                var strServicePDFBaseUrl = String()
                                
                                strServicePDFBaseUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") ?? "")"
                                
                                var strUrl = strServicePDFBaseUrl + strIsPreviewPDFUrl
                                strUrl  = strUrl.replacingOccurrences(of: "\\", with: "/")
                                
                                if strUrl != ""{
                                    let url = URL(string: strUrl)
                                    self.webViewForPDF.load(URLRequest(url: url!))
                                }
                                else{
                                    self.view.makeToast("Url not found")
                                }
                                
                                if response!["RegularflowResults"] is [String: Any]{
                                    
                                    let dictRegularPestFlow = response!["RegularflowResults"] as! NSDictionary
                                    
                                    let objWebService = WebService()
                                    
                                    objWebService.savingServicePestAllDataDB(dictWorkOrderDetail: dictRegularPestFlow, strWoId: String(describing: response!["WorkOrderId"] as! NSNumber), strType: "WithoutWoDetail", strToDeleteAreasDevices: "Yes")
                                                                        
                                }
                                
                                strIncompleteSyncForPDF = false

                            }
                            else if response!["RegularflowResults"] is [String: Any]{
                                
                                let dictRegularPestFlow = response!["RegularflowResults"] as! NSDictionary
                                
                                let objWebService = WebService()
                                
                                objWebService.savingServicePestAllDataDB(dictWorkOrderDetail: dictRegularPestFlow, strWoId: String(describing: response!["WorkOrderId"] as! NSNumber), strType: "WithoutWoDetail", strToDeleteAreasDevices: "Yes")
                                
                                methodOnResponse()
                                
                            }
                        }else{
                            
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                        }
                    }
                })
            })
        })
    }
    
    func fetchFromCoreDataToSendDeviceDynamicForms(){
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "DeviceInspectionDynamicForm", predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfData.count == 0 {
           
            
            
        }
        else
            {
                
                let arrOfEquipToSend = NSMutableArray()

                for k in 0..<arrOfData.count {
                    
                    var dictDataService = NSDictionary()
                    matchesServiceDynamic = arrOfData[k] as! NSManagedObject
                    
                    if matchesServiceDynamic.value(forKey: "arrFinalInspection") is NSDictionary {
                        
                        let arrTemp = matchesServiceDynamic.value(forKey: "arrFinalInspection") as! NSDictionary
                        
                        if arrTemp.count > 0 {
                            
                            dictDataService = arrTemp
                            
                        }
                        
                    }else if matchesServiceDynamic.value(forKey: "arrFinalInspection") is NSArray {
                        
                        let arrTemp = matchesServiceDynamic.value(forKey: "arrFinalInspection") as! NSArray
                        
                        if arrTemp.count > 0 {
                            
                            dictDataService = arrTemp[0] as! NSDictionary
                            
                        }
                        
                    }
                    
                    arrOfEquipToSend.add(dictDataService)
                
                }

                if arrOfEquipToSend.count == 0 {
                    
                    
                } else {
                    
                    sendingDeviceDynamicFormsToServer(arrFinalDynamicJson: arrOfEquipToSend)
                    
                }
            }
            
        }
    
    func sendingDeviceDynamicFormsToServer(arrFinalDynamicJson: NSArray){
        
        if arrFinalDynamicJson.count == 0{
            //methodOnResponse()
        }
        else{
            
            self.dispatch_group_FinalSync.enter()

            let errorNew: Error? = nil
            var json: Data? = nil
            var jsonString: String? = nil
            
            if JSONSerialization.isValidJSONObject(arrFinalDynamicJson) {
                // Serialize the dictionary
                do {
                    json = try JSONSerialization.data(withJSONObject: arrFinalDynamicJson, options: .prettyPrinted)
                } catch let errorNew {
                    print(errorNew)
                }
                
                // If no errors, let's view the JSON
                if json != nil && errorNew == nil {
                    jsonString = String(data: json!, encoding: .utf8)
                    print("Device Dynamic Form Json: \(String(describing: jsonString))")
                }
            }
            
            let requestData = jsonString!.data(using: .utf8)
            let strUrl = String(format: "%@%@", strServiceUrlMainServiceAutomation,UrlDeviceDynamicFormSubmission)
            
            DispatchQueue.global(qos: .default).async(execute: { [self] in
                
                global.getServerResponse(forSalesDynamicJson: strUrl, requestData, withCallback: {success, response, error in

                    self.dispatch_group_FinalSync.leave()
                    
                    if success {
                        
                        
                    }else{
                        
                        
                    }
                    
                })
            })
            
        }
    }
    
    func sendingFinalDynamicJsonToServerService(dictToBeSend: NSDictionary){
        
        self.dispatch_group_FinalSync.enter()
        
        let errorNew: Error? = nil
        var json: Data? = nil
        var jsonString: String? = nil
        
        if JSONSerialization.isValidJSONObject(dictToBeSend) {
            // Serialize the dictionary
            do {
                json = try JSONSerialization.data(withJSONObject: dictToBeSend, options: .prettyPrinted)
            } catch let errorNew {
                print(errorNew)
            }
            
            // If no errors, let's view the JSON
            if json != nil && errorNew == nil {
                jsonString = String(data: json!, encoding: .utf8)
                print("Service Automation Dynamic Form JSON: \(String(describing: jsonString))")
            }
        }
        
        let requestData = jsonString!.data(using: .utf8)
        let strUrl = String(format: "%@%@", strServiceUrlMainServiceAutomation,UrlServiceDynamicFormSubmission)
        
        DispatchQueue.global(qos: .default).async(execute: { [self] in
            
            global.getServerResponse(forSalesDynamicJson: strUrl, requestData, withCallback: {success, response, error in
                
                self.dispatch_group_FinalSync.leave()
                
                if success {
                    let res = response as! [String:Any]
                    
                    if (res["ReturnMsg"] as! String).contains("Success"){
                        
                        
                    } else {
                        
                        
                    }
                    
                }
                else{
                    //let strTitle = Alert
                    //let strMsg = Sorry
                    //global.alertMethod(strTitle, strMsg)
                }
                
            })
        })
        
    }
    
    func callWebServicePostResendEmail(){
        
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "No internet connection", viewcontrol: self)
        }
        else{
            
            let errorNew: Error? = nil
            var json: Data? = nil
            var jsonString: String? = nil
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)

            if JSONSerialization.isValidJSONObject(resendMailService()) {
                // Serialize the dictionary
                do {
                    json = try JSONSerialization.data(withJSONObject: resendMailService(), options: .prettyPrinted)
                } catch let errorNew {
                    print(errorNew)
                }
                
                // If no errors, let's view the JSON
                if json != nil && errorNew == nil {
                    jsonString = String(data: json!, encoding: .utf8)
                    print("Final Service Automation JSON Resend Mail: \(String(describing: jsonString))")
                }
            }
            
            let requestData = jsonString!.data(using: .utf8)
            let strUrl = String(format: "%@%@", strServiceUrlMainServiceAutomation, UrlResendMailService)
            let dictTemp = [String: Any]()
            
            DispatchQueue.global(qos: .default).async(execute: { [self] in
                
                global.getServerResponseForSendLead(toServer: strUrl, requestData, dictTemp, "", "resendMailService", withCallback: { [self] success, response, error in
                    DispatchQueue.main.async(execute: { [self] in
                        
                        //Sending Dynamic Json
                        self.loader.dismiss(animated: false) {
                        if success {
                            let res = response as! [String:Any]
                            if res["ReturnMsg"] as! String == "true"{
                                //if response.value(forKey: "ReturnMsg") == "true" {
                                
                                global.alertMethod(Info, "Mail Sent Successfully")
                                goToAppointmentViewNew()
                            } else {
                                
                                global.alertMethod(Info, SorryEmail)
                            }
                        }
                        else{
                            let strTitle = Alert
                            let strMsg = Sorry
                            global.alertMethod(strTitle, strMsg)
                        }
                        }
                    })
                })
            })
        }
    }
    
    
}

extension SendEmailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView{
            return objEmailSent.count
        }
        else{
            return objDocumentEmails.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendEmailCell") as! SendEmailCell
            
            let objTemp = objEmailSent[indexPath.row] as! NSManagedObject
            
            let strEmailId = "\(objTemp.value(forKey: "emailId") ?? "")"
            let strIsInvoiceMailSent = "\(objTemp.value(forKey: "isInvoiceMailSent") ?? "")"
            let strIsServiceDetailMailSent = "\(objTemp.value(forKey: "isMailSent") ?? "")"
            
            
            cell.btncheckInvoiceDetail.tag = indexPath.row
            cell.btncheckInvoiceDetail.addTarget(self, action: #selector(btnCheckedInvoiceAction), for: .touchUpInside)
            
            cell.btnCheckServiceDetail.tag = indexPath.row
            cell.btnCheckServiceDetail.addTarget(self, action: #selector(btnCheckedServiceDetailAction), for: .touchUpInside)
            
            if strIsInvoiceMailSent == "false"{
                cell.btncheckInvoiceDetail.setImage(UIImage.init(named: "check_box_1.png"), for: .normal)
            }
            else if strIsInvoiceMailSent == "true"{
                cell.btncheckInvoiceDetail.setImage(UIImage.init(named: "check_box_2.png"), for: .normal)
                
            }
            else{
                cell.btncheckInvoiceDetail.setImage(UIImage.init(named: "check_box_1.png"), for: .normal)
                
            }
            
            if strIsServiceDetailMailSent == "false"{
                cell.btnCheckServiceDetail.setImage(UIImage.init(named: "check_box_1.png"), for: .normal)
            }
            else if strIsServiceDetailMailSent == "true"{
                cell.btnCheckServiceDetail.setImage(UIImage.init(named: "check_box_2.png"), for: .normal)
                
            }
            else{
                cell.btnCheckServiceDetail.setImage(UIImage.init(named: "check_box_1.png"), for: .normal)
                
            }
            cell.lblEmailId.text = strEmailId
            
            return cell
        }
        
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendDocumentCell") as! SendDocumentCell
            
            let objTemp = objDocumentEmails[indexPath.row] as! NSManagedObject
            
            let strEmailId = "\(objTemp.value(forKey: "title") ?? "")"
            let strIsChecked = "\(objTemp.value(forKey: "isChecked") ?? "")"
            
            
            cell.btnCheckDocumentEmail.tag = indexPath.row
            cell.btnCheckDocumentEmail.addTarget(self, action: #selector(btnCheckedDcoumentAction), for: .touchUpInside)
            
            
            if strIsChecked == "false"{
                cell.btnCheckDocumentEmail.setImage(UIImage.init(named: "check_box_1.png"), for: .normal)
            }
            else if strIsChecked == "true"{
                cell.btnCheckDocumentEmail.setImage(UIImage.init(named: "check_box_2.png"), for: .normal)
                
            }
            else{
                cell.btnCheckDocumentEmail.setImage(UIImage.init(named: "check_box_2.png"), for: .normal)
                
            }
            
            cell.lblEmailId.text = strEmailId
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        calculateHeightForTabls()
        if tableView == tblView{
            return 85
        }
        else{
            return 50
        }
    }
    
    func calculateHeightForTabls(){
        if objEmailSent.count > 0 {
            let singleCellHeight = 85
            let calcuatedheightForEmailTbl = singleCellHeight * objEmailSent.count
            heightConstraintForEmailTbl.constant = CGFloat(calcuatedheightForEmailTbl)
        }
        else{
            heightConstraintForEmailTbl.constant = 0

        }
        if objDocumentEmails.count > 0 {
            let singleCellHeight = 50
            let calcuatedheightForDocTbl = singleCellHeight * objDocumentEmails.count
            heightConstraintForDocTbl.constant = CGFloat(calcuatedheightForDocTbl)
        }
        else{
            heightConstraintForDocTbl.constant = 0
        }
    }
    
    @objc func btnCheckedDcoumentAction(sender: UIButton!){
        
        let objTemp = objDocumentEmails[sender.tag] as! NSManagedObject
        
        let strIsCheckedDocument = "\(objTemp.value(forKey: "isChecked") ?? "")"
        let strEmailId = "\(objTemp.value(forKey: "title") ?? "")"
        
        if strIsCheckedDocument == "true"{
            updateDocumentTitleChecked(strIsChecked: "false", strEmailId: strEmailId)
        }
        else if strIsCheckedDocument == "false"{
            updateDocumentTitleChecked(strIsChecked: "true" , strEmailId: strEmailId)
            
        }
        else if strIsCheckedDocument == ""{
            updateDocumentTitleChecked(strIsChecked: "true" , strEmailId: strEmailId)
            
        }
    }
    
    @objc func btnCheckedInvoiceAction(sender: UIButton!){
        
        let objTemp = objEmailSent[sender.tag] as! NSManagedObject
        
        let strIsInvoiceMailSent = "\(objTemp.value(forKey: "isInvoiceMailSent") ?? "")"
        let strEmailId = "\(objTemp.value(forKey: "emailId") ?? "")"
        
        if strIsInvoiceMailSent == "true"{
            updateIsInvoiceMailSentInTOEmailServiceAuto(strIsInvoiceMailSent: "false", strEmailId: strEmailId)
        }
        else if strIsInvoiceMailSent == "false"{
            updateIsInvoiceMailSentInTOEmailServiceAuto(strIsInvoiceMailSent: "true" , strEmailId: strEmailId)
            
        }
        else if strIsInvoiceMailSent == ""{
            updateIsInvoiceMailSentInTOEmailServiceAuto(strIsInvoiceMailSent: "true" , strEmailId: strEmailId)
            
        }
    }
    @objc func btnCheckedServiceDetailAction(sender: UIButton!){
        let objTemp = objEmailSent[sender.tag] as! NSManagedObject
        
        let strIsServiceDetailMailSent = "\(objTemp.value(forKey: "isMailSent") ?? "")"
        let strEmailId = "\(objTemp.value(forKey: "emailId") ?? "")"
        
        if strIsServiceDetailMailSent == "true"{
            updateIsServiceDetailMailSentInTOEmailServiceAuto(strIsServiceDetailMailSent: "false", strEmailId: strEmailId)
        }
        else if strIsServiceDetailMailSent == "false"{
            updateIsServiceDetailMailSentInTOEmailServiceAuto(strIsServiceDetailMailSent: "true", strEmailId: strEmailId)
            
        }
        
        else if strIsServiceDetailMailSent == ""{
            updateIsServiceDetailMailSentInTOEmailServiceAuto(strIsServiceDetailMailSent: "true", strEmailId: strEmailId)
            
        }
    }
    
    
    //MARK: - FinalDict For Resend Mail Service
    func resendMailService() -> [String: Any]{
        
        let dictFinalResendMail = [
            
            "CompanyId": strCompanyIdResendMail,
            "WorkorderId": strWoId,
            "WorkOrderNo" : strWoId,
            "WoOtherDocuments": getWorkorderOtherDocumentsDictOFArr(),
            "EmailDetail" : getWorkOrderEmailDetail() ] as [String: Any]
        
        print(dictFinalResendMail)
        
        return dictFinalResendMail
    }
    
    func getWorkorderOtherDocumentsDictOFArr() -> [[String: Any]]{
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        var arrOfDocumentEmails = [[String: Any]]()
        
        for i in 0..<arrOfData.count{
            
            let objTemp = arrOfData[i] as! NSManagedObject
            
            let strIsActive = "\(objTemp.value(forKey: "isActive") ?? "")"
            let strIsChecked = "\(objTemp.value(forKey: "isChecked") ?? "")"
            let strIsDefault = "\(objTemp.value(forKey: "isDefault") ?? "")"
            let strOtherDocSysName = "\(objTemp.value(forKey: "otherDocSysName")  ?? "")"
            let strOtherDocumentPath = "\(objTemp.value(forKey: "otherDocumentPath") ?? "")"
            let strTitle = "\(objTemp.value(forKey: "title") ?? "")"
            let strWoOtherDocumentId = "\(objTemp.value(forKey: "woOtherDocumentId") ?? "")"
            let strWorkorderId = "\(objTemp.value(forKey: "workorderId")  ?? "")"
            
            let tempdict = ["IsActive" : strIsActive,
                            "IsChecked" : strIsChecked,
                            "IsDefault" : strIsDefault,
                            "OtherDocSysName" : strOtherDocSysName,
                            "OtherDocumentPath" : strOtherDocumentPath,
                            "Title" : strTitle,
                            "WoOtherDocumentId" : strWoOtherDocumentId,
                            "WorkorderId" : strWorkorderId ] as [String:Any]
            
            arrOfDocumentEmails.append(tempdict)
        }
        
        return arrOfDocumentEmails
    }
    
    func getWorkOrderEmailDetail()-> [[String: Any]]{
        
        let arrOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        var arrOfEmails = [[String: Any]]()
        
        for i in 0..<arrOfData.count{
            
            let objTemp = arrOfData[i] as! NSManagedObject
            
            let strCreatedBy = "\(objTemp.value(forKey: "createdBy") ?? "")"
            let strCreatedDate = "\(objTemp.value(forKey: "createdDate") ?? "")"
            let strEmailId = "\(objTemp.value(forKey: "emailId") ?? "")"
            let strIsCustomerEmail = "\(objTemp.value(forKey: "isCustomerEmail") ?? "")"
            let strIsDefaultEmail = "\(objTemp.value(forKey: "isDefaultEmail") ?? "")"
            let strIsInvoiceMailSent = "\(objTemp.value(forKey: "isInvoiceMailSent") ?? "")"
            let strIsMailSent = "\(objTemp.value(forKey: "isMailSent") ?? "")"
            let strModifiedBy = "\(objTemp.value(forKey: "modifiedBy") ?? "")"
            let strModifiedDate = "\(objTemp.value(forKey: "modifiedDate") ?? "")"
            let strSubject = "\(objTemp.value(forKey: "subject") ?? "")"
            let strWoInvoiceMailId = "\(objTemp.value(forKey: "woInvoiceMailId") ?? "")"
            
            let tempdict = [
                "CreatedBy" : strCreatedBy,
                "CreatedDate" : strCreatedDate,
                "EmailId" : strEmailId,
                "IsCustomerEmail" : strIsCustomerEmail,
                "IsDefaultEmail" : strIsDefaultEmail,
                "IsInvoiceMailSent" : strIsInvoiceMailSent,
                "IsMailSent" : strIsMailSent,
                "ModifiedBy" : strModifiedBy,
                "ModifiedDate" : strModifiedDate,
                "Subject" : strSubject,
                "WoInvoiceMailId" : strWoInvoiceMailId] as [String:Any]
            
            arrOfEmails.append(tempdict)
        }
        
        
        return arrOfEmails
    }
}


class SendEmailCell: UITableViewCell {
    
    @IBOutlet weak var lblEmailId: UILabel!
    @IBOutlet weak var btnCheckServiceDetail: UIButton!
    @IBOutlet weak var btncheckInvoiceDetail: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class SendDocumentCell: UITableViewCell {
    
    @IBOutlet weak var lblEmailId: UILabel!
    @IBOutlet weak var btnCheckDocumentEmail: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
