//
//  BilllingAddressVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class BilllingAddressVC: UIViewController {

    // MARK: -IBOutlets
    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var btnCheckAction: UIButton!
    @IBOutlet weak var lblSelectedBIllingAddress: UILabel!
    @IBOutlet weak var btnBillingAddressDropDown: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtContactNumber: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    //MARK: -View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: -UIButton Action
    @IBAction func btnSaveAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)

    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)

    }
    
}
