//
//  GeneralHeaderCell.swift
//  DPS
//
//  Created by Akshay Hastekar on 08/04/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class GeneralHeaderCell: UITableViewCell {

    @IBOutlet weak var heightConstraintForAddress: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForPhone: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForEmail: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForUsername: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForOffice: NSLayoutConstraint!
    
    @IBOutlet weak var heightConstraintForTruck: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForMarker: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForRoute: NSLayoutConstraint!

    @IBOutlet weak var heightConstraintForViewAdditionlService: NSLayoutConstraint!
    
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewUsername: UIView!
    @IBOutlet weak var viewOffice: UIView!
    
    @IBOutlet weak var viewTruck: UIView!
    @IBOutlet weak var viewMarker: UIView!
    @IBOutlet weak var viewRoute: UIView!

    @IBOutlet weak var btnUserName: UIButton!
    @IBOutlet weak var btnOfficeName: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnBillingAddressSameAsService: UIButton!
    @IBOutlet weak var btnBillingAddressSameAsServiceText: UIButton!
    @IBOutlet weak var btnOpenTimeSheet: UIButton!

    @IBOutlet weak var btnDirection: UIImageView!

    @IBOutlet weak var btnOpenAdditonalInfo: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnMarker: UIButton!
    @IBOutlet weak var lblTags: UILabel!
    @IBOutlet weak var btnTruck: UIButton!
    @IBOutlet weak var btnRoute: UIButton!

    @IBOutlet weak var lblAccountBalance: UILabel!
    @IBOutlet weak var lblAccountAlert: UILabel!
    @IBOutlet weak var lblRangeOfDateAndTime: UILabel!
    @IBOutlet weak var lblDateAndTime: UILabel!
    @IBOutlet weak var btnEditHeaderInfo: UIButton!

    
    @IBOutlet weak var viewOfAdditionalInfo: UIView!
    @IBOutlet weak var heightConstraintForViewAdditionlInfo: NSLayoutConstraint!
    @IBOutlet weak var genralViewHeader: UIView!

    @IBOutlet weak var lblEmailIcon: UILabel!
    @IBOutlet weak var imgPhoneIcon: UIImageView!

    @IBOutlet weak var lblAdditionalInfoEmailIcon: UILabel!
    @IBOutlet weak var imgAdditionalInfoPhoneIcon: UIImageView!

    @IBOutlet weak var lblPrimaryEmail: UILabel!
    @IBOutlet weak var lblSecondaryEmail: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblSchoolDisctrict: UILabel!
    @IBOutlet weak var lblGateCode: UILabel!
    @IBOutlet weak var lblPrimaryContactNumber: UILabel!
    @IBOutlet weak var lblSecondaryContactNumber: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblDuration: UILabel!

    @IBOutlet weak var lblServiceTitle: UILabel!
    @IBOutlet weak var lblServiceSubTitle: UILabel!

    @IBOutlet weak var btnCopy: UIButton!
    @IBOutlet weak var btnAddServices: UIButton!

    @IBOutlet weak var lblCollectPayment: UILabel!
    @IBOutlet weak var btnCollectPayment: UIButton!

    @IBOutlet weak var viewOfPOCAdditionalInfo: UIView!
    @IBOutlet weak var heightConstraintForViewPOCAdditionlInfo: NSLayoutConstraint!
    
    @IBOutlet weak var btnOpenBillingPOCInfo: UIButton!

    @IBOutlet weak var lblPOCPrimaryEmail: UILabel!
    @IBOutlet weak var lblPOCSecondaryEmail: UILabel!
    @IBOutlet weak var lblNameBillingPOC: UILabel!
    @IBOutlet weak var lblDesignationPOCBilling: UILabel!
    @IBOutlet weak var lblPOCPrimaryContactNumber: UILabel!
    @IBOutlet weak var lblPOCSecondaryContactNumber: UILabel!

    @IBOutlet weak var btnEditDateAndTimeAtion: UIButton!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        btnUserName.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnOfficeName.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnPhone.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnEmail.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnTruck.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnRoute.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left

        btnMarker.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnAddress.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnBillingAddressSameAsService.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnBillingAddressSameAsServiceText.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
     //   btnOpenAdditonalInfo.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
     //   self.btnOpenAdditonalInfo.underline()
        self.btnEditHeaderInfo.underline()
     //   self.btnOpenBillingPOCInfo.underline()

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
