//
//  TimesheetInvoiceVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 24/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class TimesheetInvoiceVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblDateAndDay: UILabel!

    //MARK: - Variable
    var strWoId = String()
    var arrOfTimeSheetData = NSMutableArray()
    
    //MARK: - View life cylcle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "EEEE, MMM dd, yyyy"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        lblDateAndDay.text = str
      

        tblView.delegate = self
        tblView.dataSource = self
        fetchStartTimeWithStartStatus()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom Functions
   
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    func fetchStartTimeWithStartStatus(){
        
        //Featching all the data from DB for start stop
        let tempArr = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        for index in 0..<tempArr.count{
            let dict = tempArr[index] as! NSManagedObject
            arrOfTimeSheetData.add(dict)
        }
        tblView.reloadData()
    }
    
    //MARK: - IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
extension TimesheetInvoiceVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfTimeSheetData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let timesheetCell = tableView.dequeueReusableCell(withIdentifier: "TimesheetCell") as! TimesheetCell
        
        let obj = arrOfTimeSheetData[indexPath.row] as! NSManagedObject
        
        let strStartTime = "\(obj.value(forKey: "startTime") as! String)"
        let strStopTime = "\(obj.value(forKey: "stopTime") as! String)"
        
        if strStartTime != "" && strStopTime != ""{
                timesheetCell.lblDateToFrom.text = strStartTime +  " - " + strStopTime
        }
        else if strStartTime != "" && strStopTime == ""{
                timesheetCell.lblDateToFrom.text = strStartTime
        }
        else{
            timesheetCell.lblDateToFrom.text = ""
        }
        
        let strSubject = "\(obj.value(forKey: "subject") as! String)"
        
        if strSubject.lowercased() == "START".lowercased() {
            let image = UIImage(named: "play")
            timesheetCell.btnPlayPauseImage.setImage(image, for: .normal)
            
            timesheetCell.lblDateToFrom.textColor = UIColor(hex: "36A085")
        }
        else if strSubject.lowercased()  == "PAUSE".lowercased() {
            let image = UIImage(named: "pause")?.withRenderingMode(.alwaysTemplate)
            timesheetCell.btnPlayPauseImage.setImage(image, for: .normal)
            
            timesheetCell.lblDateToFrom.textColor = UIColor(hex: "9A9A9A")
            
        }
        else{
            timesheetCell.btnPlayPauseImage.setImage(UIImage(named: ""), for: .normal)
        }
        return timesheetCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}

class TimesheetCell: UITableViewCell {
    
    @IBOutlet weak var lblDateToFrom: UILabel!
    @IBOutlet weak var btnPlayPauseImage: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
