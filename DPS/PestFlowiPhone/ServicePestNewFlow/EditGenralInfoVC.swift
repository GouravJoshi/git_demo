//
//  EditGenralInfoVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class EditGenralInfoVC: UIViewController, UITextFieldDelegate {

    //MARK: -IBOutlets
    
    @IBOutlet weak var lblWoNumber: UILabel!
    @IBOutlet weak var txtUserName: UITextField!
    
    @IBOutlet weak var txtPrimaryPhoneNumber: UITextField!
    @IBOutlet weak var txtSecondayPhoneNumber: UITextField!
    
    @IBOutlet weak var txtCellPhone: UITextField!

    @IBOutlet weak var txtPrimaryEmail: UITextField!
    @IBOutlet weak var txtSecondaryEmail: UITextField!

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnUploadEditImageServiceContact: UIButton!
    
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblBillingAddressSameAsServiceAddress: UILabel!
    @IBOutlet weak var btnOptionMenu: UIButton!

    
    //MARK: -Variable
    var strAccountNumber = ""
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderId = ""
    var strServiceAddImageName = String()
    var dictLoginData = NSDictionary()

    var strCompanyKey = ""
    var strWOId = String()
    var strUserName = ""

    //MARK: -View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWOId))
        
        if arryOfData.count > 0 {
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
        }
        
        
        txtUserName.isEnabled = false
        txtPrimaryEmail.delegate = self
        txtSecondaryEmail.delegate = self
        txtPrimaryPhoneNumber.delegate = self
        txtSecondayPhoneNumber.delegate = self
        txtCellPhone.delegate = self
        lblWoNumber.text = " Work Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"

        assignValues()
        // Do any additional setup after loading the view.
    }
    
    func Validation() -> String {
        
        if txtPrimaryEmail.text?.count != 0{
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtPrimaryEmail.text!)

            if !(isValid) {
                return "Please enter valid primary email!"
            }
        }
        if txtSecondaryEmail.text?.count != 0{
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtSecondaryEmail.text!)

            if !(isValid) {
                return "Please enter valid secondary email!"
            }
        }
        if txtPrimaryPhoneNumber.text?.count != 0{
            if txtPrimaryPhoneNumber.text!.count < 12 {
                return "Please enter valid primary phone!"
            }
        }
        if txtSecondayPhoneNumber.text?.count != 0{
            if txtSecondayPhoneNumber.text!.count < 12 {
                return "Please enter valid secondary phone!"
            }
        }
        if txtCellPhone.text?.count != 0{
            if txtCellPhone.text!.count < 12 {
                return "Please enter valid cell phone!"
            }
        }
       
        return ""
    }
    
    func assignValues()
    {
        print("WorkOrderDetails \(objWorkorderDetail)")
        
        txtUserName.text = global.strFullName(for: objWorkorderDetail)
        
        let strEmailName = (objWorkorderDetail.value(forKey: "primaryEmail")) as! String
        if strEmailName != ""{
            txtPrimaryEmail.text = strEmailName
        }
        else{
            txtPrimaryEmail.text = ""
        }
        
        let strSEmail = (objWorkorderDetail.value(forKey: "secondaryEmail"))  as! String
        if strSEmail != ""
        {
            txtSecondaryEmail.text = strSEmail
            
        }
        else{
            txtSecondaryEmail.text = ""
        }
        
        let strPhoneName = (objWorkorderDetail.value(forKey: "primaryPhone"))  as! String
        if strPhoneName != ""
        {
            txtPrimaryPhoneNumber.text = formattedNumber(number: strPhoneName)
            
        }
        else{
            txtPrimaryPhoneNumber.text = ""
            
        }
        
        let strSPhoneName = (objWorkorderDetail.value(forKey: "secondaryPhone"))  as! String
        if strSPhoneName != ""
        {
            txtSecondayPhoneNumber.text  = formattedNumber(number: strSPhoneName)
            
        }
        else{
            txtSecondayPhoneNumber.text = ""
            
        }
        
        let strCellNo = (objWorkorderDetail.value(forKey: "cellNo"))  as! String
        if strCellNo != ""
        {
            txtCellPhone.text  = formattedNumber(number: strCellNo)
            
        }
        else{
            txtCellPhone.text = ""
            
        }
        
        strServiceAddImageName = "\(objWorkorderDetail.value(forKey: "serviceAddressImagePath") ?? "")"
        
        if strServiceAddImageName.count > 0
        {
            
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strServiceAddImageName)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strServiceAddImageName)
                
                if isImageExists!
                {
                    self.imgView.image = image
                }
                else
                {
                    //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
                    
                    var strServiceAddImageUrl = String()
                    
                    dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                    strServiceAddImageUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
                    
                    //let strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    
                    var strUrl = String()//= strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    
                    if strServiceAddImageName.contains("Documents")
                    {
                        strUrl = strServiceAddImageUrl  + strServiceAddImageName
                    }
                    else
                    {
                        strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    }
                    
                    strUrl  = strUrl.replacingOccurrences(of: "\\", with: "/")
                    
                    //let nsUrl = URL(fileURLWithPath: strUrl)
                    
                    self.imgView.imageFromServerURL(urlString: strUrl)
                   // cell.imgView.load(url: nsUrl, strImageName: strServiceAddImageName)
                    
                }
        }
    }
    //MARK: -IBAction
    
    @IBAction func btnBackAtion(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func btnOpenActionSheetAction(_ sender: Any) {
        self.showOtherOptionsActionSheet()
    }
    
    @IBAction func btnCheckBoxAtion(_ sender: Any) {
    }

    @IBAction func btnEditImageAction(_ sender: Any) {
        
        self.view.endEditing(true)

        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            //  BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        
        /* alert.popoverPresentationController?.sourceView = self.view
         
         self.present(alert, animated: true, completion: {
         
         
         })*/
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad )
        {
            if let currentPopoverpresentioncontroller = alert.popoverPresentationController
            {
                currentPopoverpresentioncontroller.sourceView = btnUploadEditImageServiceContact
                currentPopoverpresentioncontroller.sourceRect = btnUploadEditImageServiceContact.bounds
                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection.up
                self.present(alert, animated: true, completion: nil)
            }
                
            else{
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
        
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if(Validation() != ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Validation(), viewcontrol: self)
        }else{
            updateWorkOrderDetail()
            saveEmailToCoreData()
            self.navigationController?.popViewController(animated: false)
        }
       // saveData()
    }
    
    //MARK: -Functions
    
    func trimTextField()
    {
        txtPrimaryEmail.text = txtPrimaryEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        txtSecondaryEmail.text = txtSecondaryEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
    
    func saveData()
    {
        trimTextField()
        if txtPrimaryPhoneNumber.text != ""{
            if validate(value: txtPrimaryPhoneNumber.text!) == false{
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid primary phone number", viewcontrol: self)
            }
            else if txtSecondayPhoneNumber.text != ""{
                if validate(value: txtSecondayPhoneNumber.text!) == false{
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid secondary phone number", viewcontrol: self)
                }
                else if txtCellPhone.text != ""{
                    if validate(value: txtCellPhone.text!) == false{
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid cell number", viewcontrol: self)
                    }
                    else if ((global.checkIfCommaSepartedEmailsAreValid(txtPrimaryEmail.text!) == false) && ((txtPrimaryEmail.text?.count)!>0 ) )
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid primary email", viewcontrol: self)
                    }
                    else if ((global.checkIfCommaSepartedEmailsAreValid(txtSecondaryEmail.text!) == false) && ((txtSecondaryEmail.text?.count)!>0 ) )
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid secondary email", viewcontrol: self)
                    }
                    else
                    {
                        updateWorkOrderDetail()
                        saveEmailToCoreData()
                        self.navigationController?.popViewController(animated: false)
                    }
                }
                else if ((global.checkIfCommaSepartedEmailsAreValid(txtPrimaryEmail.text!) == false) && ((txtPrimaryEmail.text?.count)!>0 ) )
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid primary email", viewcontrol: self)
                }
                else if ((global.checkIfCommaSepartedEmailsAreValid(txtSecondaryEmail.text!) == false) && ((txtSecondaryEmail.text?.count)!>0 ) )
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid secondary Email", viewcontrol: self)
                }
                else
                {
                    updateWorkOrderDetail()
                    saveEmailToCoreData()
                    self.navigationController?.popViewController(animated: false)
                }
            }
            else if txtCellPhone.text != ""{
                if validate(value: txtCellPhone.text!) == false{
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid cell number", viewcontrol: self)
                }
                else if ((global.checkIfCommaSepartedEmailsAreValid(txtPrimaryEmail.text!) == false) && ((txtPrimaryEmail.text?.count)!>0 ) )
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid primary email", viewcontrol: self)
                }
                else if ((global.checkIfCommaSepartedEmailsAreValid(txtSecondaryEmail.text!) == false) && ((txtSecondaryEmail.text?.count)!>0 ) )
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid secondary Email", viewcontrol: self)
                }
                else
                {
                    updateWorkOrderDetail()
                    saveEmailToCoreData()
                    self.navigationController?.popViewController(animated: false)
                }
            }
            else if ((global.checkIfCommaSepartedEmailsAreValid(txtPrimaryEmail.text!) == false) && ((txtPrimaryEmail.text?.count)!>0 ) )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid primary Email", viewcontrol: self)
            }
            else if ((global.checkIfCommaSepartedEmailsAreValid(txtSecondaryEmail.text!) == false) && ((txtSecondaryEmail.text?.count)!>0 ) )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid secondary Email", viewcontrol: self)
            }
            
            else
            {
                updateWorkOrderDetail()
                saveEmailToCoreData()
                self.navigationController?.popViewController(animated: false)
            }
        }
        else if strServiceAddImageName != ""{
            updateWorkOrderDetail()
            self.navigationController?.popViewController(animated: false)
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid Entry", viewcontrol: self)
        }
    }
        
    func updateWorkOrderDetail()
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        //Key Array
        arrOfKeys.add("primaryEmail")
        arrOfKeys.add("secondaryEmail")
        arrOfKeys.add("primaryPhone")
        arrOfKeys.add("secondaryPhone")
        arrOfKeys.add("cellNo")
        arrOfKeys.add("serviceAddressImagePath")

        //Value Array
        arrOfValues.add(txtPrimaryEmail.text ?? "")
        arrOfValues.add(txtSecondaryEmail.text ?? "")
        arrOfValues.add(txtPrimaryPhoneNumber.text ?? "")
        arrOfValues.add(txtSecondayPhoneNumber.text ?? "")
        arrOfValues.add(txtCellPhone.text ?? "")
        arrOfValues.add(strServiceAddImageName)

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWorkOrderId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
            updateModifyDate()
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWorkOrderId as String)
            self.navigationController?.popViewController(animated: false)
        }
        else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
    }
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWorkOrderId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWorkOrderId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func saveEmailToCoreData()
    {
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strEmailPrimary = txtPrimaryEmail.text
        
        let strEmailScondary = txtSecondaryEmail.text
        
        var arrEmailPrimary = strEmailPrimary!.components(separatedBy: ",") as NSArray
        
        //let conciseUniqueValues = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
        arrEmailPrimary = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrEmailPrimary)")
        
        
        var arrEmailSecondary = strEmailScondary!.components(separatedBy: ",") as NSArray
        
        arrEmailSecondary = arrEmailSecondary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWorkOrderId))
        
        let arrEmailToSave = NSMutableArray()
        
        
        for i in 0 ..< arrEmailPrimary.count
        {
            let strPrimaryEmail = "\(arrEmailPrimary[i])"
            var chkEmailExist = Bool()
            chkEmailExist = false
            
            for j in 0 ..< arryOfData.count
            {
                
                let  objEmailDetail = arryOfData[j] as! NSManagedObject
                let strEmailId = "\(objEmailDetail.value(forKey: "emailId")!)"
                if strEmailId == strPrimaryEmail
                {
                    chkEmailExist = true
                    break
                }
                else
                {
                    chkEmailExist = false
                }
            }
            
            if chkEmailExist == false
            {
                arrEmailToSave.add(strPrimaryEmail)
            }
            
        }
        for i in 0 ..< arrEmailSecondary.count
        {
            let strSecondaryEmail = "\(arrEmailSecondary[i])"
            
            if strSecondaryEmail.count > 0
            {
                var chkEmailExist = Bool()
                chkEmailExist = false
                
                for j in 0 ..< arryOfData.count
                {
                    
                    let  objEmailDetail = arryOfData[j] as! NSManagedObject
                    let strEmailId = "\(objEmailDetail.value(forKey: "emailId")!)"
                    if strEmailId == strSecondaryEmail
                    {
                        chkEmailExist = true
                        break
                    }
                    else
                    {
                        chkEmailExist = false
                    }
                }
                
                if chkEmailExist == false
                {
                    arrEmailToSave.add(strSecondaryEmail)
                }
            }
        }
        
        //NSArray *uniqueArray = [[NSSet setWithArray:duplicateArray] allObjects];
        
        /*   let conciseUniqueValues = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
         print("Unique array \(conciseUniqueValues)")*/
        
        //conciseUniqueValues = [5, 6, 9, 7, 4]
        
        for i in 0 ..< arrEmailToSave.count
        {
            let strEmailToSave = "\(arrEmailToSave[i])"
            
            if strEmailToSave.count > 0
            {
                
                var arrOfKeys = NSMutableArray()
                var arrOfValues = NSMutableArray()
                
                arrOfKeys = ["createdBy",
                             "createdDate",
                             "emailId",
                             "isCustomerEmail",
                             "isMailSent",
                             "companyKey",
                             "modifiedBy",
                             "modifiedDate",
                             "subject",
                             "userName",
                             "workorderId",
                             "woInvoiceMailId"]
                
                arrOfValues = ["0",
                               "0",
                               strEmailToSave,
                               "true",
                               "true",
                               strCompanyKey,
                               "",
                               global.getCurrentDate(),
                               "",
                               strUserName,
                               strWorkOrderId,
                               ""]
                
                saveDataInDB(strEntity: "EmailDetailServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            
        }
        
        
    }
    
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: value)
        return result
    }
    
//    func isValidPhone(phone: String) -> Bool {
//            let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
//            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
//            return phoneTest.evaluate(with: phone)
//        }
    
    func showOtherOptionsActionSheet()  {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let currentSetupAction = UIAlertAction(title: "Current Setup", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCurrentSetupAction()
        })
        let customerSalesDocumentsAction = UIAlertAction(title: "Customer Sales Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCustomerSalesDocument()
        })
        
        let notesHistoryAction = UIAlertAction(title: "Notes History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openNotesHistory()
        })
        let serviceHistoryAction = UIAlertAction(title: "Service History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openServiceHistory()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(currentSetupAction)
        optionMenu.addAction(customerSalesDocumentsAction)
        optionMenu.addAction(notesHistoryAction)
        optionMenu.addAction(serviceHistoryAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.btnOptionMenu
        self.present(optionMenu, animated: true, completion: {
        })
        
    }
    
    func openCurrentSetupAction(){
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController?.strCompanyKey = strCompanyKey
        testController?.strWOId = strWOId   as String
        testController?.strUserName = strUserName

        self.present(testController!, animated: false, completion: nil)
    }
    
    func openCustomerSalesDocument(){
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = strAccountNumber
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func openNotesHistory(){
        
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = false;
        testController!.strAccccountId = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func openServiceHistory(){
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
}

extension EditGenralInfoVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            imgView.contentMode = .scaleAspectFit
            imgView.image = pickedImage
            //strServiceAddImageName = "\\Documents\\UploadImages\\Img"  + "ServiceAddImg" + "\(getUniqueValueForId())" + ".jpg"
            
            strServiceAddImageName = "\\Documents\\CustomerAddressImages\\Img"  + "ServiceAddImg" + "\(getUniqueValueForId())" + ".jpg" // UploadImages
            
            saveImageDocumentDirectory(strFileName: strServiceAddImageName, image: pickedImage)
            //yesEditedSomething = true
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension EditGenralInfoVC {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtPrimaryPhoneNumber {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = textField.text!
                strTextt = String(strTextt.dropLast())
                textField.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        
        if textField == txtSecondayPhoneNumber  {
            
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = textField.text!
                strTextt = String(strTextt.dropLast())
                textField.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
            
        }
        
        if textField == txtCellPhone  {
            
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = textField.text!
                strTextt = String(strTextt.dropLast())
                textField.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
            
        }
        return true
    }
    
}
