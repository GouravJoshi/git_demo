//
//  SelectDeviceVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 20/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SelectDeviceVC: UIViewController {

    //MARK: - IBOUtlets
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var tvlist: UITableView!

    //MARK: - Variable
    var arrOfDevice = NSArray()
    @objc var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var index = -1

    var strDeviceIdIdLocal = String()
    var strMobileDeviceIdIdLocal = String()
    var  strDeviceName = String()
    
    weak var delegateToAddDeivceTargetInPest: AddDeivceTargetInPest?

    override func viewDidLoad() {
        super.viewDidLoad()

        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0

        tvlist.delegate = self
        tvlist.dataSource = self
        
        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
       
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            strCompanyKey = "\(value)"
        }
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            strUserName = "\(value)"
        }
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.dismiss(animated: false, completion: nil)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchDeviceList()
    }
    
    //MARK: - IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        dismiss(animated: false)

    }

    @IBAction func btnSaveAction(_ sender: Any) {
        
        let objArea = arrOfDevice[index] as! NSManagedObject
        
        strDeviceIdIdLocal = objArea.value(forKey: "deviceId") as! String
        strMobileDeviceIdIdLocal = objArea.value(forKey: "mobileDeviceId") as! String
        strDeviceName = "\(objArea.value(forKey: "deviceName") ?? "N/A")"
        
        self.delegateToAddDeivceTargetInPest?.addDevice(strDeviceIdIdLocal: strDeviceIdIdLocal, strMobileDeviceIdIdLocal: strMobileDeviceIdIdLocal, strDeviceName: strDeviceName)
        
        dismiss(animated: false)
        
    }
    
    //MARK: - DB
    
    func fetchDeviceList() {
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                
                arrOfDevice = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressExist")

            }else{

                arrOfDevice = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressExist")

            }
            
        }else{
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{

                arrOfDevice = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "Yes")

            }else{

                arrOfDevice = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressblank")
                
            }

        }

        tvlist.reloadData()
        
    }
    
    
    @objc func pressRadioButton(_ button: UIButton) {
        
        print("RAdio Button with tag: \(button.tag) clicked!")
        
        index = button.tag
        
        tvlist.reloadData()
                
    }
}

extension SelectDeviceVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfDevice.count
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "SelectDeviceCell", for: indexPath as IndexPath) as! SelectDeviceCell
        
        let objArea = arrOfDevice[indexPath.row] as! NSManagedObject
        
        cell.lblTitle.text = "\(objArea.value(forKey: "deviceName") ?? "N/A")"

        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(pressRadioButton(_:)), for: .touchUpInside)
        
        if index == indexPath.row {
            
            cell.btnRadio.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            
        }else{
            
            cell.btnRadio.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            
        }
        
        tvlist.separatorColor = UIColor.clear
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
}

protocol AddDeivceTargetInPest: class {
    
    func addDevice(strDeviceIdIdLocal: String, strMobileDeviceIdIdLocal: String, strDeviceName: String)
}

class SelectDeviceCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRadio: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}
