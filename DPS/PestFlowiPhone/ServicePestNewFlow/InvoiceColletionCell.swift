//
//  InvoiceColletionCell.swift
//  DPS
//
//  Created by Akshay Hastekar on 22/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class InvoiceColletionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
}

extension CollectionInvoiceCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InvoiceColletionCell", for: indexPath) as! InvoiceColletionCell
        
        cell.lblTitle.text = arrOfCollection[indexPath.row]
        
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        var sectionInsets = UIEdgeInsets()
//        sectionInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
//        return sectionInsets.left
//    }
    
    //       public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //
    //           var width = CGFloat()
    //           if isforiPad == true {
    //               width = collectionView.frame.size.width/CGFloat(1)
    //           }
    //           else{
    //               width = collectionView.frame.size.width/CGFloat(2)
    //           }
    //           return CGSize(width: width, height: hr)
    //       }
}
