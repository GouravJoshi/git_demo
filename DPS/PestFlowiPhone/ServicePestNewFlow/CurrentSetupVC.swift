//
//  CurrentSetupVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 13/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class CurrentSetupVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!

    //MARK: -Variable
    var arrOfCurrentSetup = [[String: Any]]()
    @objc var objCurrentSetup = NSMutableArray()//[NSManagedObject()]

    @objc var strAccountNo = String()
    var strCompanyKey = ""
    var strWOId = String()
    var strUserName = ""
    
    @objc var tempstrAccNo = NSString ()
    @objc var tempstrWOID = NSString ()
    
    var strEmpID = String()
    var strEmpName = String()
    var dictLoginData = NSDictionary()
    @objc var objWorkorderDetail = NSManagedObject()

    
    //MARK: -View Life Cycle
    override func viewDidLoad() {
        
        if tempstrWOID != ""{
            self.strWOId = self.tempstrWOID as String
        }
        if tempstrAccNo != ""{
            self.strAccountNo = self.tempstrAccNo as String

        }
        tblView.delegate = self
        tblView.dataSource = self

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWOId))
        
        if arryOfData.count > 0 {
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
        }
        
        
        if !isInternetAvailable()
        {           // Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
            getDataFromDB()
        }
        else
        {
            getCurrentSetUpList()
        }
        super.viewDidLoad()

    }
    
    func getDataFromDB(){
        
        objCurrentSetup.removeAllObjects()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoCurrentSetup", predicate: NSPredicate(format: "workorderId == %@", strWOId))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objCurrentSetup.add(dict)
            }
            tblView.isHidden = false
            tblView.reloadData()

        }
        else{
            tblView.isHidden = true
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
        }
        
    }
    
    func clearDBBeforeAddingAPIData(){
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoCurrentSetup", predicate: NSPredicate(format: "workorderId == %@", self.strWOId))
        
        if arryOfData.count > 0
        {
            for i in 0..<arryOfData.count{
                let objData = arryOfData[i] as! NSManagedObject
                deleteDataFromDB(obj: objData)
            }
        }
    }
        
        
    func getCurrentSetUpList(){
            
        let strURL = String(format: URL.ServiceNewPestFlow.getCurrentSetupList , strCompanyKey, strAccountNo) //"P7878800004")
            
            
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
                if(status)
                {
                    self.arrOfCurrentSetup.removeAll()
                    self.clearDBBeforeAddingAPIData()
                    self.arrOfCurrentSetup = response.value(forKey: "data") as! [[String:Any]]
                    for i in 0..<self.arrOfCurrentSetup.count{
                        SaveCurrentSetupCoreData(arrOfCurrentSetupIndex: arrOfCurrentSetup[i])
                    }
                    getDataFromDB()
                    tblView.reloadData()
                }
            }
    }
    
    //MARK: -IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
   
    //MARK: -DB
    func SaveCurrentSetupCoreData(arrOfCurrentSetupIndex: [String: Any]){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("amount")
        arrOfKeys.add("currentSetupId")
        arrOfKeys.add("freqeuncy")
        arrOfKeys.add("lastGeneration")
        arrOfKeys.add("nextService")
        arrOfKeys.add("route")
        arrOfKeys.add("service")
        arrOfKeys.add("status")
        arrOfKeys.add("technician")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("userName")
        arrOfKeys.add("companyKey")

        
        /*  {
         "ContactName": "Rajkumar  Lodhi/infocratsweb solution",
         "Services": "test",
         "Frequency": "Monthly",
         "LastGenerationDate": null,
         "NextGenerationDate": "2021-04-02T00:00:00",
         "WorkOrderGenerated": null,
         "SetupStartDate": "2021-03-09T00:00:00",
         "SetupEndDate": null,
         "PrimaryRoute": "Route _plum",
         "Address": "102 Doyle St. Mumbai , Portland, California, 78374, United States",
         "PrimaryTechnicain": null,
         "Status": "New",
         "NextServiceDate": null,
         "Amount": 90.00
     }*/
        arrOfValues.add( "$" + String(describing : arrOfCurrentSetupIndex["Amount"] as? NSNumber ?? 0))
        arrOfValues.add(arrOfCurrentSetupIndex["ContactName"] as? String ?? "")
        arrOfValues.add(arrOfCurrentSetupIndex["Frequency"] as? String ?? "")
        arrOfValues.add(arrOfCurrentSetupIndex["LastGenerationDate"] as? String ?? "")
        arrOfValues.add(arrOfCurrentSetupIndex["NextGenerationDate"] as? String ?? "")
        arrOfValues.add(arrOfCurrentSetupIndex["PrimaryRoute"] as? String ?? "")
        arrOfValues.add(arrOfCurrentSetupIndex["Services"] as? String ?? "")
        arrOfValues.add(arrOfCurrentSetupIndex["Status"] as? String ?? "")
        arrOfValues.add(arrOfCurrentSetupIndex["PrimaryTechnicain"] as? String ?? "")
        arrOfValues.add(strWOId)
        arrOfValues.add(strUserName)
        arrOfValues.add(strCompanyKey)
        
        saveDataInDB(strEntity: "GeneralInfoCurrentSetup", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }

    
}

extension CurrentSetupVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return objCurrentSetup.count
        
        
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentSetupCell") as! CurrentSetupCell
        
        cell.selectionStyle = .none
        let obj = objCurrentSetup[indexPath.row] as! NSManagedObject

        cell.lblService.text = "\(obj.value(forKey: "service")!)" == "" ? "-" : "\(obj.value(forKey: "service")!)"
        
        cell.lblRoute.text = "\(obj.value(forKey: "route")!)" == "" ? "-" : "\(obj.value(forKey: "route")!)"
        
        cell.lblTech.text = "\(obj.value(forKey: "technician")!)" == "" ? "-" : "\(obj.value(forKey: "technician")!)"
        
        cell.lblFrequency.text = "\(obj.value(forKey: "freqeuncy")!)" == "" ? "-" : "\(obj.value(forKey: "freqeuncy")!)"
        
        
        if "\(obj.value(forKey: "nextService")!)" != ""{
            
            cell.lblNextService.text = changeStringDateToGivenFormat(strDate: "\(obj.value(forKey: "nextService")!)", strRequiredFormat: "MM/dd/yyyy")
        }
        else{
            cell.lblNextService.text = "-"
        }
        if "\(obj.value(forKey: "lastGeneration")!)" != ""{
            
            cell.lblLastGeneration.text = changeStringDateToGivenFormat(strDate: "\(obj.value(forKey: "lastGeneration")!)", strRequiredFormat: "MM/dd/yyyy")
        }
        else{
            cell.lblLastGeneration.text = "-"
        }
        
        cell.lblStatus.text = "\(obj.value(forKey: "status")!)" == "" ? "-" : "\(obj.value(forKey: "status")!)"
        
        cell.lblAmount.text = "\(obj.value(forKey: "amount")!)" == "" ? "-" : "\(obj.value(forKey: "amount")!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 245
        }
        else{
            return 220
        }
    }
    
}


class CurrentSetupCell: UITableViewCell {
    
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblRoute: UILabel!
    @IBOutlet weak var lblTech: UILabel!
    @IBOutlet weak var lblFrequency: UILabel!

    @IBOutlet weak var lblNextService: UILabel!
    @IBOutlet weak var lblLastGeneration: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
   
}
