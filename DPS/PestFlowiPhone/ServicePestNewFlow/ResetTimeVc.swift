//
//  ResetTimeVc.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
protocol onRemoveMemberDelegate: class {
    
    func onRemoveMember()
    
}
class ResetTimeVc: UIViewController {


    //MARK: -IBOutlets
    @IBOutlet weak var lblResetTimeReason: UILabel!
    @IBOutlet weak var btnResetTimeReasonDropDown: UIButton!
    @IBOutlet weak var txtViewNotes: UITextView!
    @IBOutlet weak var btnResetReason: UIButton!
    
    
    //MARK: - Variable
    var strWoId = ""
    var dictBillingContactData = NSDictionary()
    var strResetId = String()
    var strBillingPocId = String()
    var dictLoginData = NSDictionary()
    let global = Global()
    weak var delegate: onRemoveMemberDelegate?


    //MARK: -View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        txtViewNotes.text = ""
        // Do any additional setup after loading the view.
    }
    

    //MARK: -Custom functions
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
        }
        else
        {
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
        
    }
    
    
    func saveResetDataIntoDB(){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("resetId")
        arrOfKeys.add("resetDescription")
        arrOfKeys.add("workorderStatus")
        
        arrOfValues.add(strResetId)
        arrOfValues.add(txtViewNotes.text ?? "")
        arrOfValues.add("Reset")
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            updateModifyDate()
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        } else {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
        
    }
    //MARK: -IBActions
    @IBAction func btnCrossAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        if (strResetId == "")
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select reset reason", viewcontrol: self)
        }
        else
        {
            self.saveResetDataIntoDB()
            nsud.set(true, forKey: "isFromSurveyToSendEmail")
            nsud.set(true, forKey: "resetSuccess")
            self.delegate?.onRemoveMember()
            self.dismiss(animated: false, completion: nil)

        }
    }
    
    @IBAction func btnResetTimeReasonDropDownAction(_ sender: Any) {
        
        self.view.endEditing(true)

        if (nsud.value(forKey: "MasterServiceAutomation") != nil) {
            
            let dictDetailServiceMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            
            if nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
                
                if dictDetailServiceMaster.value(forKey: "ResetReasons") is NSArray {
                    
                    let arrOfLogType = (dictDetailServiceMaster.value(forKey: "ResetReasons") as! NSArray).mutableCopy()as! NSMutableArray
                    
                    if arrOfLogType.count > 0
                    {
                        let arrOfData = arrOfLogType
                        if arrOfData.count > 0
                        {
                            btnResetReason.tag = 41
                            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)
                    }
                    
                }
                
            }
            
        }else{

            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)

        }
        
    }
    
    //MARK: - Reset Flow
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            //vc.handleSelection = self
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
  
    //MARK: -WebService Handler
    
}

extension ResetTimeVc : CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int)
    {
         if tag == 41
        {
            lblResetTimeReason.text = dictData["ResetReason"] as? String
            strResetId = "\(dictData.value(forKey: "ResetId") ?? "")"
        }
       
    }
    
}
