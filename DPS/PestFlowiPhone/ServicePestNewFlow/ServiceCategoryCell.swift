//
//  ServiceCategoryCell.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class ServiceCategoryCell: UITableViewCell {

    @IBOutlet weak var btnCheckbox: UIButton!
    @IBOutlet weak var btnTaxable: UIButton!

    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var txtCategoryQuantity: UITextField!
    @IBOutlet weak var txtCategoryPrice: UITextField!

    var arrOfEditedQuantity = [String]()
    var arrOfEditedPrice = [String]()
    
    var callbackForEditedQuantity: (([String]) -> ())?
    var callbackForEditedPrice: (([String]) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
//
//    @objc func txtCategoryQuantityAction(textField: UITextField) {
//
////        let index = IndexPath(item: textField.tag, section: 0)
////        let cell = self.tblView.cellForRow(at: index) as! ServiceCategoryCell
//
//        let strServiceQauntity = txtCategoryQuantity.text
//        arrOfEditedQuantity.remove(at: textField.tag)
//        arrOfEditedQuantity.insert(strServiceQauntity!, at: textField.tag)
//
//        if callbackForEditedQuantity != nil {
//            callbackForEditedQuantity!(arrOfEditedQuantity)
//        }
//
//    }
//
//    @objc func txtCategoryPriceAction(textField: UITextField) {
////        let index = IndexPath(item: textFi eld.tag, section: 0)
////        let cell = self.tblView.cellForRow(at: index) as! ServiceCategoryCell
//
//        let strServicePrice = txtCategoryPrice.text
//        arrOfEditedPrice.remove(at: textField.tag)
//        arrOfEditedPrice.insert(strServicePrice!, at: textField.tag)
//
//        if callbackForEditedPrice != nil {
//            callbackForEditedPrice!(arrOfEditedPrice)
//        }
//    }
}


class ServiceCategoryFooterCell: UITableViewCell {

    @IBOutlet weak var btnAddServices: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


class AddServiceListCell: UITableViewCell {

    @IBOutlet weak var lblServiceName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
