//
//  GenralInfoVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class GenralInfoVC: UIViewController, AddCommentProtocol, onRemoveMemberDelegate, MFMailComposeViewControllerDelegate  {
    
  
    //MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnChecklist: UIButton!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var btnClock: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var widthConstaintOfContnueButton: NSLayoutConstraint!

    //MARK: -Varialbe
    var arrOfTblViewCells = [String]()
    
    var serviceGlobalIndex = 0
    @objc var strWoId = NSString ()
    var strEmpID = String()
    var strEmpName = String()
    var strFromEmail = String()
    var strResetId = String()
    var strServicePocId = String()
    var strBillingPocId = String()
    var strServicesStatus = String()
    var strDepartmentType = String()
    var isBtnOpenAdditionalInoClciked = Bool()
    var isBtnOpenPOCBillingClciked = Bool()

    var dictLoginData = NSDictionary()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var isCollectPayment = false
    @objc var objServiceCategoryDetail = NSMutableArray()//[NSManagedObject()]
    var arrOfServices = [[String]]()
    var yesEditedSomething = Bool()
    var strButtonTitleOfStartDate = ""

    var heightAttributesAccordingText = CGFloat()
    var heightTargetsAccordingText = CGFloat()

    var loader = UIAlertController()

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
        }
        
        nsud.setValue(false, forKey: "resetSuccess")
        nsud.synchronize()
        
        if !isInternetAvailable()
        {
            showAccountAndNotesAlert(isOffline: true)
        }
        else
        {
            getNotesAlert()
        }
        
        tblView.delegate = self
        tblView.dataSource = self
        
        isBtnOpenPOCBillingClciked = false
        isBtnOpenAdditionalInoClciked = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblView.reloadData()
        getClockStatus()
        
        
        print("will app called")
        let tempReset = nsud.bool(forKey: "resetSuccess")
        if tempReset == true{
            resetSuccess(strReset: "true")
        }
        
        if (strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
        {
            btnStartTime.isEnabled = false
        }
        print("get service data called")
        getDataOfServices()
        if strButtonTitleOfStartDate.lowercased() == "Start".lowercased(){
            btnGraph.isUserInteractionEnabled = false
            btnGallery.isUserInteractionEnabled = false
            btnReset.isUserInteractionEnabled = false
            btnContinue.isUserInteractionEnabled = false
            btnChecklist.isUserInteractionEnabled = false
        }
        else{
            btnGraph.isUserInteractionEnabled = true
            btnGallery.isUserInteractionEnabled = true
            btnReset.isUserInteractionEnabled = true
            btnContinue.isUserInteractionEnabled = true
            btnChecklist.isUserInteractionEnabled = true
        }
        

    }
    
    //MARK: - Alert notes API work
    
    func clearDBBeforeAddingAPIData(){
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "AlertNotes", predicate: NSPredicate(format: "workorderId == %@", "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"))
        
        if arryOfData.count > 0
        {
            for i in 0..<arryOfData.count{
                let objData = arryOfData[i] as! NSManagedObject
                deleteDataFromDB(obj: objData)
            }
        }
    }
    
    func getDataFromDB() -> NSMutableArray{
        let objOfNotes = NSMutableArray()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "AlertNotes", predicate: NSPredicate(format: "workorderId == %@", "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objOfNotes.add(dict)
            }
        }
        return objOfNotes
    }
    
    func getNotesAlert(){
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        
        self.present(loader, animated: false, completion: nil)
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let refType = "workorderno"
        
        let strURL = String(format:  "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")" +  URL.ServiceNewPestFlow.getNotesAlert, refType, "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")" , global.getCompanyKey())
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
            
            loader.dismiss(animated: true)
            if(status)
            {
                self.clearDBBeforeAddingAPIData()
                let arrOfNotesTemp = response.value(forKey: "data") as! [[String:Any]]
                for i in 0..<arrOfNotesTemp.count{
                    setDataIntoCoreDate(arrOfCurrentIndex: arrOfNotesTemp[i])
                }
                    showAccountAndNotesAlert(isOffline: false)
            }
        }
    }
    
    func showAccountAndNotesAlert(isOffline: Bool){
        if isOffline == true{
            let strAlertMessage = objWorkorderDetail.value(forKey: "accountDescription") as! String
            
            if strAlertMessage.count > 0 {
                
                let alert = UIAlertController(title: "Alert", message: strAlertMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        else{
            let arrOfNotes = getDataFromDB()
            let strAlertMessage = objWorkorderDetail.value(forKey: "accountDescription") as! String
            var arrOfListOfMessages = [String]()
            if strAlertMessage != ""{
                arrOfListOfMessages.append(strAlertMessage)
            }
            for i in 0..<arrOfNotes.count{
                
                let obj = arrOfNotes[i] as! NSManagedObject
                arrOfListOfMessages.append("\(obj.value(forKey: "noteText")!)")
            }
            
            if arrOfNotes.count > 0 || strAlertMessage != ""{
                let alert = UIAlertController(title: "Alert", message: arrOfListOfMessages.toBulletList(), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func setDataIntoCoreDate(arrOfCurrentIndex: [String: Any]){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("noteText")
        arrOfKeys.add("noteId")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("username")
        
        
        arrOfValues.add(arrOfCurrentIndex["Note"] as? String ?? "")
        arrOfValues.add(arrOfCurrentIndex["LeadNoteId"] as? String ?? "")
        arrOfValues.add("\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")")
        arrOfValues.add(global.getCompanyKey())
        arrOfValues.add(global.getUserName())
        
        saveDataInDB(strEntity: "AlertNotes", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
   
    
    //MARK: - IBActions
    func onRemoveMember() {
        self.dismiss(animated: false, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            print("Delay")
            let tempReset = nsud.bool(forKey: "resetSuccess")
            if tempReset == true{
                resetSuccess(strReset: "true")
            }
        
        }
       
    }

    @objc func btnOpenInfoViewAction(sender: UIButton!){
        //Creating cell with the help of btn sender.tag
       
        if sender.tag == 0 {
            
            isBtnOpenPOCBillingClciked = false
            isBtnOpenAdditionalInoClciked = true
        }
        else {
            
            isBtnOpenPOCBillingClciked = true
            isBtnOpenAdditionalInoClciked = false
        }
        
        tblView.reloadData()

    }
    
    @objc func btnAddServiceAction(sender: UIButton!) {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddServiceListVC") as? AddServiceListVC
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    @objc func btnOpenPOCBillingInfoAction(sender: UIButton!){
        //Creating cell with the help of btn sender.tag
        let index = IndexPath(item: 0, section: 0)
        let cell = self.tblView.cellForRow(at: index) as! GeneralHeaderCell
        
        if cell.btnOpenBillingPOCInfo.tag == 0 {
            cell.btnOpenBillingPOCInfo.tag = 1
            tblView.reloadData()
        }
        else if cell.btnOpenBillingPOCInfo.tag == 1 {
            cell.btnOpenBillingPOCInfo.tag = 0
            tblView.reloadData()
        }
        
    }
    
    @IBAction func btnlockInOutAction(_ sender: Any) {
        if !isInternetAvailable()
        {
            Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
        }
        else
        {
            let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
            let clockInOutVC = storyboardIpad.instantiateViewController(withIdentifier: "ClockInOutViewController") as? ClockInOutViewController
            self.navigationController?.pushViewController(clockInOutVC!, animated: false)
        }
        

    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        
    }
    @objc func btnEditDateAndTimeAtion(_ sender: Any){

        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleDateTimeGenralInfoVC") as? ScheduleDateTimeGenralInfoVC
        
        vc?.strWorkOrder = strWoId as String
        self.navigationController?.pushViewController(vc!, animated: false)
    
    }
    
    
    @objc func btnEditHeaderInfoAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditGenralInfoVC") as? EditGenralInfoVC
        
        let strTempAcountNumber = objWorkorderDetail.value(forKey: "accountNo") as? String
        if strTempAcountNumber != ""{
            vc!.strAccountNumber = strTempAcountNumber ?? ""
        }
        vc!.strWorkOrderId = strWoId as String
        vc!.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    @IBAction func btnGalleryAction(_ sender: Any) {
        self.goToGlobalmage(strType: "Before")
        
    }
    
    @IBAction func btnStaticsAction(_ sender: Any) {
        self.goToGlobalmage(strType: "Graph")
        
    }
    
    @IBAction func btnCheklistAction(_ sender: Any) {
        UserDefaults.standard.setValue(strButtonTitleOfStartDate, forKey: "StartStopButtonTitle")
        goToInspectionView()
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Reset" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "reset"{
            UserDefaults.standard.setValue(strButtonTitleOfStartDate, forKey: "StartStopButtonTitle")
            
            let vc = UIStoryboard.init(name: "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ServiceDetailVC") as? ServiceDetailVC
            
            vc!.isFromServicePestNewFlow = true;
            vc!.strWoId = strWoId;
            
            self.navigationController?.pushViewController(vc!, animated: false)
        }
        else{
            
            if checkImage() == true
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one before image", viewcontrol: self)
            }
            else
            {
                UserDefaults.standard.setValue(strButtonTitleOfStartDate, forKey: "StartStopButtonTitle")
                
                let vc = UIStoryboard.init(name: "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ServiceDetailVC") as? ServiceDetailVC
                
                vc!.isFromServicePestNewFlow = true;
                vc!.strWoId = strWoId;
                
                self.navigationController?.pushViewController(vc!, animated: false)
            }
            
        }
        
        
    }
    
    
    @IBAction func btnResetAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResetTimeVc") as? ResetTimeVc
        
        vc?.modalPresentationStyle = .overCurrentContext
        vc?.delegate = self
        vc?.strWoId = strWoId as String
        self.present(vc!, animated: false, completion: nil)
        
    }
    
    @IBAction func btnStartTimeAction(_ sender: Any){
        /* btnStartTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime")*/
        
        let str = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")" //"\(lblStartTimeValue.titleLabel?.text ?? "")"
        
        if str == ""
        {
            self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: false)
            strButtonTitleOfStartDate = "Pause"
        }
        else
        {
            var alertTitle = ""
            if strButtonTitleOfStartDate == "Start"{
                alertTitle = "Start"
            }
            else if strButtonTitleOfStartDate == "Pause"{
                alertTitle = "Pause"
            }
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want \(alertTitle) time", preferredStyle: UIAlertController.Style.alert)
            
            
            alert.addAction(UIAlertAction(title: alertTitle, style: .default , handler:{ (nil)in
                
                if self.strButtonTitleOfStartDate == "Start"{
                    self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: true)
                }
                else if self.strButtonTitleOfStartDate == "Pause"{
                    self.getStartTimeIn(isStartTime: false, isStrTimeInAvial: true)
                }
                
                if self.strButtonTitleOfStartDate.lowercased() == "Start".lowercased(){
                    self.btnGraph.isUserInteractionEnabled = false
                    self.btnGallery.isUserInteractionEnabled = false
                    self.btnReset.isUserInteractionEnabled = false
                    self.btnContinue.isUserInteractionEnabled = false
                    self.btnChecklist.isUserInteractionEnabled = false
                    self.tblView.reloadData()
                }
                else{
                    self.btnGraph.isUserInteractionEnabled = true
                    self.btnGallery.isUserInteractionEnabled = true
                    self.btnReset.isUserInteractionEnabled = true
                    self.btnContinue.isUserInteractionEnabled = true
                    self.btnChecklist.isUserInteractionEnabled = true
                    self.tblView.reloadData()
                }
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (nil)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnAddCopyAction(_ sender: Any) {
    }
    
    @IBAction func btnOpenActionSheetAction(_ sender: Any) {
        self.showOtherOptionsActionSheet()
    }
    
    //MARK: - DB Functions
    func deleteServiceFromDatbase(strServiceId: String){
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in
                        
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@  && serviceId == %@", self.strWoId, strServiceId))
            
            if arryOfData.count > 0
            {
                let objData = arryOfData[0] as! NSManagedObject
                deleteDataFromDB(obj: objData)
                self.arrOfTblViewCells.removeAll()
                self.getDataOfServices()
                self.updateModifyDate()
                updateServicePestModifyDate(strWoId: self.strWoId as String)

            }
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
       
    
    }
    func getDataOfServices(){
        
        objServiceCategoryDetail.removeAllObjects()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@", strWoId))
    
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objServiceCategoryDetail.add(dict)
            }
        }
        createTableViewArray()
    }
    
    func getDataFromDbToUpdate(strEntity: String , predicate : NSPredicate , arrayOfKey: NSMutableArray , arrayOfValue: NSMutableArray)-> Bool {
        
        let context = getContext()

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
        fetchRequest.predicate = predicate
        do {
            
            let dataTemp = (try getContext().fetch(fetchRequest) as NSArray)
            
            if dataTemp.count > 0 {
                
                let objToUpdate = dataTemp[0] as! NSManagedObject
                
                for index in 0..<arrayOfKey.count {
                    
                    objToUpdate.setValue(arrayOfValue[index], forKey: "\(arrayOfKey[index])")
                    
                }
                
            }else{
                
                return false
            }

            
            do {
                try context.save()
            } catch _ as NSError  {
                
            } catch {
                
            }
            
            return true

        } catch
        {
            let fetchError = error as NSError
            return false

        }
        
    }
    
    //MARK: - Duration calculations
    func getDuration(strStartDate: String, strPauseDate: String) -> String{
        
        if strStartDate.count == 0 {
            
            return "\(0)"

        }else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            
           
            var startDate = dateFormatter.date(from: strStartDate)
            if startDate == nil{
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.local
                dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm:ss"
                startDate = dateFormatter2.date(from: strStartDate)
            }
            let pauseDate = dateFormatter.date(from: strPauseDate)!
            let resultedOffSet = pauseDate.offsetFrom(date: startDate!)
            
            let result = getDurationInterval(strOffSet: resultedOffSet)
            print(result)
            return result
            
        }
        
    }
    
    func getDurationInterval(strOffSet : String) -> String{
        let resultantOffSet = strOffSet
        
        let arr = resultantOffSet.components(separatedBy: " ")
        var strDay = "0"
        var strHr = "0"
        var strMin = "0"
        var strSec = "0"
        
        if resultantOffSet.contains("d"){
            strDay = arr[0].components(separatedBy: "d")[0]
            strHr = arr[1].components(separatedBy: "h")[0]
            strMin = arr[2].components(separatedBy: "m")[0]
            strSec = arr[3].components(separatedBy: "s")[0]

        }
        else if resultantOffSet.contains("h"){
            strHr = arr[0].components(separatedBy: "h")[0]
            strMin = arr[1].components(separatedBy: "m")[0]
            strSec = arr[2].components(separatedBy: "s")[0]

        }
        else if arr.count == 1{
            strSec = arr[0].components(separatedBy: "s")[0]
        }
        else{
           strMin = arr[0].components(separatedBy: "m")[0]
           strSec = arr[1].components(separatedBy: "s")[0]
        }
        
        strDay = strDay.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strDay = strDay.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strHr = strHr.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strHr = strHr.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strMin = strMin.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strMin = strMin.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strSec = strSec.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strSec = strSec.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        let dayInt = Int(strDay)!
        let hrInt = Int(strHr)!
        let minInt = Int(strMin)!
        let secInt = Int(strSec)!

        print(dayInt)
        print(hrInt)
        print(minInt)
        print(secInt)

        var dayConvertsInHours = Int()
        if dayInt != 0{
          dayConvertsInHours = dayInt * 24
        }
        let result = (dayConvertsInHours * 60 * 60) + (hrInt * 60 * 60) + (minInt * 60) + secInt  //INSECONDS
         //String(minInt) + " Min " + String(secInt) + " secs" // 2 Min 12 secs
        //
        return String(result)
    }
    
    
    //MARK: - Custom functions
    func checkImage() -> Bool
    {
        let chkImage = nsud.bool(forKey: "isCompulsoryBeforeImageService")
        
        if chkImage == true
        {
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, "Before"))
            
            if arryOfData.count > 0
            {
                
                return false
                
            }
            else
            {
                return true
            }
            
        }
        else
        {
            
            return false
            
        }
        
    }
    
    func getClockStatus()
    {
        var strClockStatus = String()
        
        if !isInternetAvailable()
        {
            
            strClockStatus = global.fetchClockInOutDetailCoreData()
            
            if strClockStatus == "" || strClockStatus.count == 0 || strClockStatus == "<null>"
            {
                btnClock.setBackgroundImage(UIImage(named: "Time"), for: .normal)
            }
            else if strClockStatus == "WorkingTime"
            {
                btnClock.setBackgroundImage(UIImage(named: "Time"), for: .normal)
                
            }
            else if strClockStatus == "BreakTime"
            {
                btnClock.setBackgroundImage(UIImage(named: "Time"), for: .normal)
            }
            else
            {
                btnClock.setBackgroundImage(UIImage(named: "Time"), for: .normal)
                
            }
        }
        else
        {
            DispatchQueue.global(qos: .background).async
            {
                strClockStatus = self.global.getCurrentTimerOfClock()
                
                DispatchQueue.main.async
                {
                    if strClockStatus == "" || strClockStatus.count == 0 || strClockStatus == "<null>"
                    {
                        self.btnClock.setBackgroundImage(UIImage(named: "Time"), for: .normal)
                    }
                    else if strClockStatus == "WorkingTime"
                    {
                        self.btnClock.setBackgroundImage(UIImage(named: "Time"), for: .normal)
                        
                    }
                    else if strClockStatus == "BreakTime"
                    {
                        self.btnClock.setBackgroundImage(UIImage(named: "Time"), for: .normal)
                    }
                    else
                    {
                        self.btnClock.setBackgroundImage(UIImage(named: "Time"), for: .normal)
                        
                    }
                    
                }
            }
        }
        
    }
    
    func getStartTimeIn(isStartTime: Bool,  isStrTimeInAvial: Bool)
    {
        yesEditedSomething = true
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        
        let strtemp = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
        var strDate = ""
        if strtemp != ""{
                        
            strDate  = changeStringDateToGivenFormat(strDate: strtemp, strRequiredFormat: "MM/dd/yyyy hh:mm:ss a")
        }
        if isStrTimeInAvial == false{
            self.saveTimeInDB(startTime: str)
            setStartButtonTextWithDateTime(strTempDateAndTime: str, strTimeType: "Start")
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
        }
        if strButtonTitleOfStartDate == "Start"{
            

            let obj =  fetchStartTimeWithStartStatus()
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
            
            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
            
            strButtonTitleOfStartDate = "Pause"
            

        }
        else if strButtonTitleOfStartDate == "Pause"{
            
            let obj =  fetchStartTimeWithStartStatus()
            
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
                        
            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "PAUSE", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Pause")

            strButtonTitleOfStartDate = "Start"
            
        }
        
    }
    
   
    func fetchStartTimeWithStartStatus() -> NSMutableArray{
        
        let objTimeSheetLastIndex = NSMutableArray()
        var queryParam  = ""
        if strButtonTitleOfStartDate == "Start"{
            queryParam = "PAUSE"
        }
        if strButtonTitleOfStartDate == "Pause"{
            queryParam = "START"
        }
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@  && status == %@", strWoId, queryParam))
    
        //Featching all the data from DB for start stop
        let tempArr = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))

        let tempDict = NSMutableArray()
        for index in 0..<tempArr.count{
            let dict = tempArr[index] as! NSManagedObject
            tempDict.add(dict)
        }

        for j in 0..<tempDict.count{
            let tempIndex = tempDict[j] as! NSManagedObject
            print("Start Time: \(tempIndex.value(forKey: "startTime") as! String))")
            print("Stop Time: \(tempIndex.value(forKey: "stopTime") as! String))")
            print("Duration: \(tempIndex.value(forKey: "duration") as! String))")
            print("status: \(tempIndex.value(forKey: "status") as! String))")
            print("subject: \(tempIndex.value(forKey: "subject") as! String))")

        }
        
        if arryOfData.count > 0 {
                let dict = arryOfData.lastObject as! NSManagedObject
                objTimeSheetLastIndex.add(dict)
        }
        return objTimeSheetLastIndex
    }
    
    func saveStartStopTimeInTimeSheet(strStartTime: String, strStopTime: String, strStatus: String, strDuration: String, isStart: Bool){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        if isStart == true{
            arrOfKeys.add("companyKey")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("userName")
            arrOfKeys.add("startTime")
            arrOfKeys.add("stopTime")
            arrOfKeys.add("duration")
            arrOfKeys.add("status")
            arrOfKeys.add("subject")
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strWoId)
            arrOfValues.add(strUserName)
            arrOfValues.add(strStartTime)
            arrOfValues.add(strStopTime)
            arrOfValues.add(strDuration)
            arrOfValues.add(strStatus)
            arrOfValues.add(strStatus)

            saveDataInDB(strEntity: "GeneralInfoWorkorderTimeSheet", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        else if isStart == false{
            
            arrOfKeys.add("stopTime")
            arrOfKeys.add("duration")
            arrOfKeys.add("status")
            
            arrOfValues.add(strStopTime)
            arrOfValues.add(strDuration)
            arrOfValues.add(strStatus)
            
            let isSuccess = getDataFromDbToUpdate(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && startTime == %@", strWoId, strStartTime), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                btnReset.isEnabled = true
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                print("Success")
            }
            else {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }
     
    }
    
    func setStartButtonTextWithDateTime(strTempDateAndTime: String, strTimeType: String){
        
        if strTimeType == "Start" {
            btnStartTime.setTitle("Pause", for: .normal)
            btnStartTime.backgroundColor = hexStringToUIColor(hex: "9A9A9A")
        }
        else{
            btnStartTime.setTitle("Start", for: .normal)
            btnStartTime.backgroundColor = hexStringToUIColor(hex: "36A085")
        }
    }
    
    func createTableViewArray(){
        
        arrOfTblViewCells = ["General Info Header", "Targets", "Attributes", "Setup Instructions", "Work Order Instruction", "Tech Comment"]
        arrOfServices.removeAll()
        var arrOfServicesCount = [String]()
        if objServiceCategoryDetail.count > 0{
            for i in 0..<objServiceCategoryDetail.count{
                arrOfServicesCount.append("Services")
                
                let obj = objServiceCategoryDetail[i] as! NSManagedObject
                
                let strServiceName = obj.value(forKey: "serviceName") as? String ?? ""
                let strServicePrice = obj.value(forKey: "servicePrice") as? String ?? ""
                let strServiceQuantity = obj.value(forKey: "serviceQuantity") as? String ?? ""
                let strServiceId =  obj.value(forKey: "serviceId") as? String ?? ""
                let strIsTaxable = obj.value(forKey: "taxable") as? String ?? ""
                
                arrOfServices.append([strServiceName, strServicePrice, strServiceQuantity, strServiceId, strIsTaxable])

            }
        }
        print(arrOfServices)
        arrOfTblViewCells.insert(contentsOf: arrOfServicesCount, at: 1)
        tblView.reloadData()
    }
    
    func resetSuccess(strReset: String) {
        if strReset == "true"{
            
            let vc = UIStoryboard.init(name: "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "SendEmailVC") as! SendEmailVC
            vc.fromWhere = "WDO"
            vc.strWoId = strWoId as String
            //vc.strBackRoundImage = image!
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func addTechComment(strTitle: String, strId: String, strDesription: String) {
        //TechnicianComment
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("TechnicianComment")
        arrOfValues.add(strDesription)

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
        self.tblView.reloadData()
    }
    
    func saveTimeInDB(startTime : String){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("TimeIn")
        arrOfValues.add(startTime)

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
        self.tblView.reloadData()
    }
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strModuleType = "ServicePestFlow"
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Reset" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "reset"{
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func showOtherOptionsActionSheet()  {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let currentSetupAction = UIAlertAction(title: "Current Setup", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCurrentSetupAction()
        })
        let customerSalesDocumentsAction = UIAlertAction(title: "Customer Sales Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCustomerSalesDocument()
        })
        
        let notesHistoryAction = UIAlertAction(title: "Notes History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openNotesHistory()
        })
        let serviceHistoryAction = UIAlertAction(title: "Service History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openServiceHistory()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(currentSetupAction)
        optionMenu.addAction(customerSalesDocumentsAction)
        optionMenu.addAction(notesHistoryAction)
        optionMenu.addAction(serviceHistoryAction)
        optionMenu.addAction(cancelAction)
        
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func openCurrentSetupAction(){
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController?.strCompanyKey = strCompanyKey
        testController?.strWOId = strWoId as String
        testController?.strUserName = strUserName

        self.present(testController!, animated: false, completion: nil)
    }
    
    func openCustomerSalesDocument(){
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func openNotesHistory(){
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController!.strWoId = strWoId as String
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = false;
        testController!.strAccccountId = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func openServiceHistory(){
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
    

    
    func downloadServiceAddImage()
    {
        
        let index = IndexPath(item: 0, section: 0)
        let cell = self.tblView.cellForRow(at: index) as! GeneralHeaderCell
        
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strServiceAddImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strServiceAddImageName)
        
        if isImageExists!
        {
            cell.imgView.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strServiceAddImageUrl = String()
            
            strServiceAddImageUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            //let strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            
            var strUrl = String()//= strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            
            if strServiceAddImageName.contains("Documents")
            {
                strUrl = strServiceAddImageUrl  + strServiceAddImageName
            }
            else
            {
                strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            }
            
            strUrl  = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            let nsUrl = URL(string: strUrl)
            
            cell.imgView.load(url: nsUrl! as URL , strImageName: strServiceAddImageName)
        }
        
    }
    
    func goToInspectionView()
    {
        
        let testController = UIStoryboard.init(name: "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "InspectionChecklistCopyVC") as! InspectionChecklistCopyVC
        
        testController.isFromNewPestFlow = "YES"
        let strTempAcountNumber = objWorkorderDetail.value(forKey: "accountNo") as? String
        if strTempAcountNumber != ""{
            testController.strAccountNumber  = strTempAcountNumber ?? ""
        }
        testController.matchesWorkOrderZSync = objWorkorderDetail
        self.navigationController?.pushViewController(testController, animated: false)
    }

}

extension GenralInfoVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfTblViewCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrOfTblViewCells[indexPath.row] == "General Info Header"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralHeaderCell") as! GeneralHeaderCell
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
            
            if arryOfData.count > 0 {
                objWorkorderDetail = arryOfData[0] as! NSManagedObject
            }
            
            cell.btnOpenAdditonalInfo.addTarget(self, action: #selector(btnOpenInfoViewAction), for: .touchUpInside)

            cell.btnOpenBillingPOCInfo.addTarget(self, action: #selector(btnOpenInfoViewAction), for: .touchUpInside)

            cell.btnEditHeaderInfo.addTarget(self, action: #selector(btnEditHeaderInfoAction), for: .touchUpInside)
            
            cell.btnEditDateAndTimeAtion.addTarget(self, action: #selector(btnEditDateAndTimeAtion), for: .touchUpInside)
            
            if isBtnOpenAdditionalInoClciked == true{
                cell.btnOpenAdditonalInfo.backgroundColor = hexStringToUIColor(hex: "FFFFFF")
                cell.btnOpenBillingPOCInfo.backgroundColor = UIColor.hexStringToColor(hex: "E9E9E9")
                cell.btnOpenAdditonalInfo.tintColor = hexStringToUIColor(hex: "000000")
                cell.btnOpenBillingPOCInfo.tintColor = hexStringToUIColor(hex: "000000")
                
                cell.viewOfAdditionalInfo.isHidden = false
                cell.viewOfPOCAdditionalInfo.isHidden = true
            }
            else if isBtnOpenAdditionalInoClciked == false{
                
                cell.btnOpenAdditonalInfo.backgroundColor = hexStringToUIColor(hex: "E9E9E9")
                cell.btnOpenBillingPOCInfo.backgroundColor = UIColor.hexStringToColor(hex: "FFFFFF")
                cell.btnOpenAdditonalInfo.tintColor = hexStringToUIColor(hex: "000000")
                cell.btnOpenBillingPOCInfo.tintColor = hexStringToUIColor(hex: "000000")
                
                cell.viewOfAdditionalInfo.isHidden = true
                cell.viewOfPOCAdditionalInfo.isHidden = false
            }
            let strAlertMessage = objWorkorderDetail.value(forKey: "accountDescription") as! String

            cell.lblAccountAlert.text = " Account Alert:  " + strAlertMessage
            
            let strAccountBalance = objWorkorderDetail.value(forKey: "previousBalance") as! String
            if strAccountBalance != ""{
                cell.lblAccountBalance.text = " Account Balance:  " + strAccountBalance
            }
            else{
                cell.lblAccountBalance.text = " Account Balance:  " + "0.00"
            }
            
            strDepartmentType = "\(objWorkorderDetail.value(forKey: "departmentType") ?? "")"//[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"departmentType"]];
            
            
            // IsCollectPayment  isCollectPayment
            
            let strIsCollectPayment = "\(objWorkorderDetail.value(forKey: "isCollectPayment") ?? "")"
            
            if strIsCollectPayment == "1" || strIsCollectPayment == "true" || strIsCollectPayment == "True" {
                
                isCollectPayment = true
                cell.btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
                
            }else{
                
               isCollectPayment = false
                cell.btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
                
            }
            
            //cell.btnCollectPayment.addTarget(self, action: #selector(btnCollectPaymentAction), for: .touchUpInside)
            
            strWorkOrderStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
            
            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                cell.btnEditHeaderInfo.isHidden = true
                cell.btnEditDateAndTimeAtion.isHidden = true
                self.btnReset.isEnabled = false
                self.btnReset.alpha = 0.6
                self.btnGallery.isEnabled  = false
                self.btnGraph.isEnabled = false
                self.btnStartTime.isEnabled = false
                
                self.btnReset.setBackgroundImage(UIImage(named: "reset"), for: .normal)
                self.btnGallery.setBackgroundImage(UIImage(named: "galleryGray"), for: .normal)
                self.btnGraph.setBackgroundImage(UIImage(named: "staticsGray"), for: .normal)
                
                btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
                btnChecklist.layer.borderWidth = 1.0
                btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)

            }
            else{
                
                cell.btnEditHeaderInfo.isHidden = false
                cell.btnEditDateAndTimeAtion.isHidden = false
                cell.btnEditDateAndTimeAtion.underline()
                self.btnReset.isEnabled = true
                self.btnReset.alpha = 1
                self.btnGallery.isEnabled  = true
                self.btnGraph.isEnabled = true
                self.btnStartTime.isEnabled = true


                btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
                btnChecklist.layer.borderWidth = 1.0
                btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)

                self.btnReset.setBackgroundImage(UIImage(named: "reset"), for: .normal)
                self.btnGallery.setBackgroundImage(UIImage(named: "gallery"), for: .normal)
                self.btnGraph.setBackgroundImage(UIImage(named: "statics"), for: .normal)
                
                if dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsRescheduleServiceAppointment") is Bool {
                    let isEditButtonEnable = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsRescheduleServiceAppointment") as! Bool
                    if isEditButtonEnable {
                        cell.btnEditDateAndTimeAtion.isHidden = false
                    } else {
                        cell.btnEditDateAndTimeAtion.isHidden = true
                    }
                }else{
                    cell.btnEditDateAndTimeAtion.isHidden = true
                }
                
            }
            
            if (strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset"){
            
                btnChecklist.layer.borderColor = UIColor(hex: "9A9A9A")?.cgColor
                btnChecklist.layer.borderWidth = 1.0
                btnChecklist.setTitleColor(hexStringToUIColor(hex: "9A9A9A"), for: .normal)

                btnContinue.isEnabled = false
                btnContinue.backgroundColor = UIColor(hex: "9A9A9A")
                //btnContinue.layer.borderWidth = 1.0

                btnChecklist.isEnabled = false
            }
            
            let str = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
            if str != ""{
                
                if strWorkOrderStatus.lowercased() != "Reset".lowercased(){
                    btnChecklist.isEnabled = true
                }
                let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
                
                let strDate = changeStringDateToGivenFormat(strDate: str, strRequiredFormat: "MM/dd/yyyy hh:mm:ss a")
                
                let objTimeSheetLastIndex = NSMutableArray()
                if arryOfData.count > 0 {
                    let dict = arryOfData[arryOfData.count - 1] as! NSManagedObject
                    
                    objTimeSheetLastIndex.add(dict)
                    
                    let managedObject = objTimeSheetLastIndex[0] as! NSManagedObject
                    let strStatus = managedObject.value(forKey: "status") as? String ?? ""
                    
                    if strStatus == ""{
                        
                        let strSubject = managedObject.value(forKey: "subject") as? String ?? ""
                        if strSubject.lowercased() == "START".lowercased(){
                            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
                            strButtonTitleOfStartDate = "Pause"
                            //saveStartStopTimeInTimeSheet(strStartTime: strDate, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
                        }
                        else  if strSubject.lowercased() == "PAUSE".lowercased(){
                            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Pause")
                            strButtonTitleOfStartDate = "Start"                           // saveStartStopTimeInTimeSheet(strStartTime: strDate, strStopTime: "", strStatus: "PAUSE", strDuration: "", isStart: true)
                        }
                        else{
                            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
                            saveStartStopTimeInTimeSheet(strStartTime: strDate, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
                        }
                       
                        btnContinue.isHidden = false
                        widthConstaintOfContnueButton.constant = 80
                    }
                    
                    else if strStatus == "START"{
                        strButtonTitleOfStartDate = "Pause"
                        setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
                        btnContinue.isHidden = false
                        widthConstaintOfContnueButton.constant = 80
                    }
                    
                    else if strStatus == "PAUSE"{
                        strButtonTitleOfStartDate = "Start"
                        setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Pause")
                        btnContinue.isHidden = false
                        widthConstaintOfContnueButton.constant = 80
                    }
                    
                }
                else{
                    strButtonTitleOfStartDate = "Pause"
                    setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
                    saveStartStopTimeInTimeSheet(strStartTime: strDate, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
                    btnContinue.isHidden = false
                    widthConstaintOfContnueButton.constant = 80
                }
                
           
            }
           
            else{
                btnContinue.isHidden = true
                btnReset.isEnabled = false
                btnChecklist.layer.borderColor = UIColor(hex: "9A9A9A")?.cgColor
                btnChecklist.setTitleColor(hexStringToUIColor(hex: "9A9A9A"), for: .normal)

                btnChecklist.layer.borderWidth = 1.0
                btnChecklist.isEnabled = false
                widthConstaintOfContnueButton.constant = 0
            }
            
            if strButtonTitleOfStartDate.lowercased() == "Start".lowercased(){
                btnGraph.isUserInteractionEnabled = false
                btnGallery.isUserInteractionEnabled = false
                btnReset.isUserInteractionEnabled = false
                btnContinue.isUserInteractionEnabled = false
                btnChecklist.isUserInteractionEnabled = false
                cell.btnEditHeaderInfo.isUserInteractionEnabled = false
                cell.btnEditDateAndTimeAtion.isUserInteractionEnabled = false
                cell.btnAddress.isUserInteractionEnabled = false
                cell.btnAddServices.isUserInteractionEnabled = false
            }
            else{
                btnGraph.isUserInteractionEnabled = true
                btnGallery.isUserInteractionEnabled = true
                btnReset.isUserInteractionEnabled = true
                btnContinue.isUserInteractionEnabled = true
                btnChecklist.isUserInteractionEnabled = true
                cell.btnEditHeaderInfo.isUserInteractionEnabled = true
                cell.btnEditDateAndTimeAtion.isUserInteractionEnabled = true
                cell.btnAddress.isUserInteractionEnabled = true
                cell.btnAddServices.isUserInteractionEnabled = true
                
            }
            
            
            nsud.set(strWorkOrderStatus, forKey: "workOrderStatus")
            nsud.synchronize()
            
            strServicePocId = "\(objWorkorderDetail.value(forKey: "servicePOCId") ?? "")"
            strBillingPocId = "\(objWorkorderDetail.value(forKey: "billingPOCId") ?? "")"
            
            
            //Service Address Image Download
            strServiceAddImageName = "\(objWorkorderDetail.value(forKey: "serviceAddressImagePath") ?? "")"
            
            
            if strServiceAddImageName.count > 0
            {
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strServiceAddImageName)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strServiceAddImageName)
                
                if isImageExists!
                {
                    cell.imgView.image = image
                }
                else
                {
                    //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
                    
                    var strServiceAddImageUrl = String()
                    
                    strServiceAddImageUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
                    
                    //let strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    
                    var strUrl = String()//= strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    
                    if strServiceAddImageName.contains("Documents")
                    {
                        strUrl = strServiceAddImageUrl  + strServiceAddImageName
                    }
                    else
                    {
                        strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
                    }
                    
                    strUrl  = strUrl.replacingOccurrences(of: "\\", with: "/")
                    
                    let nsUrl = URL(fileURLWithPath: strUrl)
                    
                    cell.imgView.imageFromServerURL(urlString: strUrl)
                    // cell.imgView.load(url: nsUrl, strImageName: strServiceAddImageName)
                    
                }
            }
            
            let strName = global.strFullName(for: objWorkorderDetail)
            if strName != ""{
                cell.btnUserName.setTitle(strName, for: .normal)
                cell.heightConstraintForUsername.constant = 17
                cell.viewUsername.alpha = 1
                
            }
            else{
                cell.heightConstraintForUsername.constant = 0
                cell.viewUsername.alpha = 0
                
            }
            
            let strOfficeName = (objWorkorderDetail.value(forKey: "companyName")) as! String
            if strOfficeName != ""{
                cell.btnOfficeName.setTitle((strOfficeName), for: .normal)
                cell.heightConstraintForOffice.constant = 17
                cell.viewOffice.alpha = 1
            }
            else{
                cell.heightConstraintForOffice.constant = 0
                cell.viewOffice.alpha = 0
            }
            
            let strEmailName = (objWorkorderDetail.value(forKey: "primaryEmail")) as! String
            if strEmailName != ""{
                cell.btnEmail.setTitle((strEmailName), for: .normal)
                cell.heightConstraintForEmail.constant = 17
                cell.viewEmail.alpha = 1
                cell.btnEmail.addTarget(self, action: #selector(actionOnServiceAutoEmailClick), for: .touchUpInside)

            }
            else{
                cell.heightConstraintForEmail.constant = 0
                cell.viewEmail.alpha = 0
                
            }
            
            
            let strPrimaryPhoneNumber = (objWorkorderDetail.value(forKey: "primaryPhone"))  as! String
            let strCellNumber = (objWorkorderDetail.value(forKey: "cellNo"))  as! String
            
            if strCellNumber != ""
            {
                cell.btnPhone.setTitle(formattedNumber(number: strCellNumber), for: .normal)
                cell.heightConstraintForPhone.constant = 17
                cell.viewPhone.alpha = 1
                
            }
            else if strPrimaryPhoneNumber != ""
            {
                cell.btnPhone.setTitle((formattedNumber(number: strPrimaryPhoneNumber)), for: .normal)
                cell.heightConstraintForPhone.constant = 17
                cell.viewPhone.alpha = 1
                
            }
            else{
                cell.heightConstraintForPhone.constant = 0
                cell.viewPhone.alpha = 0
                
            }
            
            let strMarker = (objWorkorderDetail.value(forKey: "keyMap")) as! String
            if strMarker != ""{
                cell.btnMarker.setTitle((strMarker), for: .normal)
                cell.heightConstraintForMarker.constant = 17
                cell.viewMarker.alpha = 1
                
            }
            else{
                cell.heightConstraintForMarker.constant = 0
                cell.viewMarker.alpha = 0
                
            }
            
            let isBillingAddressSameAsServiceAddress = "\(objWorkorderDetail.value(forKey: "isBillingAddressAsSameServiceAddress") ?? "")"
            
            if isBillingAddressSameAsServiceAddress == "1" || isBillingAddressSameAsServiceAddress == "true" || isBillingAddressSameAsServiceAddress == "True" {
                
                cell.btnBillingAddressSameAsService.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
                
            }else{
                
                cell.btnBillingAddressSameAsService.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
                
            }
            
            let strTempTags = objWorkorderDetail.value(forKey: "tags")
            let strTags = strTempTags as? String ?? ""
            if strTags == nil {
                cell.lblTags.text = ""
            }
            else if strTags != ""{
                cell.lblTags.text =  "Tags: " + strTags
            }
            else{
                cell.lblTags.text = ""
            }
            
            let strRoute = (objWorkorderDetail.value(forKey: "routeNo")) as! String
            if strRoute != ""{
                cell.btnRoute.setTitle(("Route: " + strRoute), for: .normal)
                cell.heightConstraintForRoute.constant = 17
                cell.viewRoute.alpha = 1
                
            }
            else{
                cell.heightConstraintForRoute.constant = 0
                cell.viewRoute.alpha = 0
                
            }
            
            let strTruck = (objWorkorderDetail.value(forKey: "driveTimeStr")) as! String
            if strTruck != ""{
                
                cell.btnTruck.setTitle("Drive: " + strTruck, for: .normal)
                cell.heightConstraintForTruck.constant = 17
                cell.viewTruck.alpha = 1
                
            }
            else{
                cell.heightConstraintForTruck.constant = 0
                cell.viewTruck.alpha = 0
                
            }
            
            if "\(objWorkorderDetail.value(forKey: "primaryEmail") ?? "")" == "" &&  "\(objWorkorderDetail.value(forKey: "secondaryEmail") ?? "")" == ""{
                cell.lblAdditionalInfoEmailIcon.text = ""
            }
            else{
                cell.lblAdditionalInfoEmailIcon.text = "@"
            }
            
            
            if "\(objWorkorderDetail.value(forKey: "primaryPhone") ?? "")" == "" &&  "\(objWorkorderDetail.value(forKey: "secondaryPhone") ?? "")" == ""{
                cell.imgAdditionalInfoPhoneIcon.image = nil
            }
            else{
                cell.imgAdditionalInfoPhoneIcon.image = UIImage(named: "phone")
            }
            
            let strPEmail = (objWorkorderDetail.value(forKey: "primaryEmail"))  as! String
            if strPEmail != ""
            {
                cell.lblPrimaryEmail.text = strPEmail
                
            }
            else{
                cell.lblPrimaryEmail.text = ""
                
            }
            
            let strSEmail = (objWorkorderDetail.value(forKey: "secondaryEmail"))  as! String
            if strSEmail != ""
            {
                cell.lblSecondaryEmail.text = strSEmail
                
            }
            else{
                cell.lblSecondaryEmail.text = ""
                
            }
            
            let strPPhoneName = (objWorkorderDetail.value(forKey: "primaryPhone"))  as! String
            if strPPhoneName != ""
            {
                cell.lblPrimaryContactNumber.text  = formattedNumber(number: strPPhoneName)
                
            }
            else{
                cell.lblPrimaryContactNumber.text = ""
                
            }
            
            let strSPhoneName = (objWorkorderDetail.value(forKey: "secondaryPhone"))  as! String
            if strSPhoneName != ""
            {
                cell.lblSecondaryContactNumber.text  = formattedNumber(number:  strSPhoneName)
                
            }
            else{
                cell.lblSecondaryContactNumber.text = ""
                
            }
            let strSchoolDistrict = (objWorkorderDetail.value(forKey: "billingSchoolDistrict"))  as! String
            if strSchoolDistrict != ""
            {
                cell.lblSchoolDisctrict.text  = strSchoolDistrict
                
            }
            else{
                cell.lblSchoolDisctrict.text = ""
                
            }
            
            let strCountry = (objWorkorderDetail.value(forKey: "serviceCounty"))  as! String
            let strState = (objWorkorderDetail.value(forKey: "serviceState"))  as! String
            if strCountry != ""
            {
                cell.lblCountry.text  = strCountry + ", " + strState

                
            }
            else{
                cell.lblCountry.text = ""
                
            }
            
            let strGateCode = (objWorkorderDetail.value(forKey: "serviceGateCode"))  as! String
            if strGateCode != ""
            {
                cell.lblGateCode.text  = strGateCode
                
            }
            else{
                cell.lblGateCode.text = ""
                
            }
           
            var strStartTime = "\(objWorkorderDetail.value(forKey: "scheduleStartDateTime") ?? "")"
            
            if (strStartTime.count) > 0
            {
                strStartTime =  changeStringDateToGivenFormat(strDate:  strStartTime, strRequiredFormat: "MM/dd/yyyy hh:mm a")
            }
            else
            {
                strStartTime =  ""
                
            }
            
            
            var strEndTime = "\(objWorkorderDetail.value(forKey: "scheduleStartDateTime") ?? "")"
            
            if (strEndTime.count) > 0
            {
                strEndTime =  changeStringDateToGivenFormat(strDate:  strEndTime, strRequiredFormat: "MM/dd/yyyy hh:mm a")
            }
            else
            {
                strEndTime =  " "
                
            }
            
            cell.lblDateAndTime.text = strStartTime //+ " - " +  strEndTime
            
            
            var strEarliestStartTime = "\(objWorkorderDetail.value(forKey: "earliestStartTime") ?? "")"
            
            var strLatestStartTime = "\(objWorkorderDetail.value(forKey: "latestStartTime") ?? "")"
              
            strEarliestStartTime = changeStringDateToGivenFormat(strDate:  strEarliestStartTime.components(separatedBy: " ")[0], strRequiredFormat: "hh:mm a")
            
            strLatestStartTime = changeStringDateToGivenFormat(strDate:  strLatestStartTime.components(separatedBy: " ")[0] , strRequiredFormat: "hh:mm a")
            
            if strEarliestStartTime.count < 1 {
                
                strEarliestStartTime = ""
                
            }
            if strLatestStartTime.count < 1 {
                
                strLatestStartTime = ""
                
            }
          
            let strRangeTime = changeStringDateToGivenFormat(strDate:  strStartTime, strRequiredFormat: "MM/dd/yyyy")
            if strLatestStartTime != "" || strEarliestStartTime != ""{
                cell.lblRangeOfDateAndTime.text = "Range:  " + strRangeTime + " " + strEarliestStartTime +  " - " +  strLatestStartTime
            }
            else{
                cell.lblRangeOfDateAndTime.text = ""
            }
            cell.btnAddress.setTitle( global.strCombinedAddressBilling(for: objWorkorderDetail), for: .normal)
           
            cell.btnAddress.addTarget(self, action: #selector(actionOnServiceAutoAddressClick), for: .touchUpInside)
            
            let tapGR = UITapGestureRecognizer(target: self, action: #selector(actionOnServiceAutoAddressClick))
            cell.btnDirection.addGestureRecognizer(tapGR)
            cell.btnDirection.isUserInteractionEnabled = true
            //cell.btnDirection.addTarget(self, action: #selector(actionOnServiceAutoAddressClick), for: .touchUpInside)

            var strThirdPartyAccountNo = String()
            strThirdPartyAccountNo = "\(objWorkorderDetail.value(forKey: "thirdPartyAccountNo") ?? "")"
            if strThirdPartyAccountNo.count > 0
            {
                lblTitle.text = "A/C #: " + strThirdPartyAccountNo + " Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
                
                nsud.set(lblTitle.text, forKey: "lblThirdPartyAccountNo")
                nsud.synchronize()
            }
            else
            {
                lblTitle.text = "A/C #: " + "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")" + " Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
            }
            
            if "\(objWorkorderDetail.value(forKey: "billingPrimaryEmail") ?? "")" == "" &&  "\(objWorkorderDetail.value(forKey: "billingSecondaryEmail") ?? "")" == ""{
                cell.lblEmailIcon.text = ""
            }
            else{
                cell.lblEmailIcon.text = "@"
            }
            
            
            if "\(objWorkorderDetail.value(forKey: "billingPrimaryPhone") ?? "")" == "" &&  "\(objWorkorderDetail.value(forKey: "billingSecondaryPhone") ?? "")" == ""{
                cell.imgPhoneIcon.image = nil
            }
            else{
                cell.imgPhoneIcon.image = UIImage(named: "phone")
            }
            
            nsud.set(lblTitle.text, forKey: "lblName")
            nsud.set("\(objWorkorderDetail.value(forKey: "accountNo")!)", forKey: "workOrderAccountNo")
            nsud.set("\(objWorkorderDetail.value(forKey: "accountNo")!)", forKey: "AccNoService")
            nsud.set("\(objWorkorderDetail.value(forKey: "departmentId")!)", forKey: "DepartmentIdService")
            nsud.synchronize()
            
            cell.lblServiceSubTitle.text = "\(objWorkorderDetail.value(forKey: "services") ?? "")"
            
            cell.lblPOCPrimaryEmail.text  = "\(objWorkorderDetail.value(forKey: "billingPrimaryEmail") ?? "")"
            
            cell.lblPOCSecondaryEmail.text  = "\(objWorkorderDetail.value(forKey: "billingSecondaryEmail") ?? "")"
            
            cell.lblPOCPrimaryContactNumber.text  = formattedNumber(number:  "\(objWorkorderDetail.value(forKey: "billingPrimaryPhone") ?? "")")
            cell.lblPOCSecondaryContactNumber.text  = formattedNumber(number:  "\(objWorkorderDetail.value(forKey: "billingSecondaryPhone") ?? "")")
            
            var strBillingContactName = String()
            strBillingContactName = "\(objWorkorderDetail.value(forKey: "billingFirstName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "billingMiddleName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "billingLastName") ?? "")"
            
            cell.lblNameBillingPOC.text  = strBillingContactName
            cell.lblDesignationPOCBilling.text  = ""

            if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                cell.btnAddServices.isHidden = true
                cell.btnCollectPayment.isEnabled = false
            }
            else{
                cell.btnAddServices.isHidden = false
                cell.btnAddServices.addTarget(self, action: #selector(btnAddServicesAction), for: .touchUpInside)
                cell.btnCollectPayment.isEnabled = true
            }
            return cell
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Services" {
            
            let serviceCell = tableView.dequeueReusableCell(withIdentifier: "GenralInfoAddServiceCell") as! GenralInfoAddServiceCell
        
            let servicePrice = Float(arrOfServices[indexPath.row - 1][1])
            serviceCell.lblServiceName.text = arrOfServices[indexPath.row - 1][0]
            if String(describing: servicePrice) != "0.0" ||  String(describing: servicePrice) != ""{
               
                serviceCell.lblServiceQantityAndPrice.text = arrOfServices[indexPath.row - 1][2]  + "   *   " +  String(describing: servicePrice!)
            }
            else{
                
                serviceCell.lblServiceQantityAndPrice.text = arrOfServices[indexPath.row - 1][2]  + "   *   " +  "0.0"
                
            }
        
            return serviceCell
            
        }
        else if arrOfTblViewCells[indexPath.row] == "Targets" || arrOfTblViewCells[indexPath.row] == "Attributes"{
            
            let targetcell = tableView.dequeueReusableCell(withIdentifier: "GenralInfoTargetsAndAttributes") as! GenralInfoTargetsAndAttributes
            
            if arrOfTblViewCells[indexPath.row]  == "Targets"{
                targetcell.lblTitle.text = "Targets: "
                let strTarget = objWorkorderDetail.value(forKey: "targets") as! String
                if strTarget != ""{

                    targetcell.lblDescription.text = strTarget
                    
                    heightTargetsAccordingText = CGFloat()
                    
                    let font = UIFont.systemFont(ofSize: 14.0)
                    self.heightTargetsAccordingText = heightForView(text: targetcell.lblDescription.text!, font: font, width: targetcell.lblDescription.frame.size.width)
                    
                    targetcell.heightConstraintForLblValue.constant = self.heightTargetsAccordingText
                    targetcell.lblDescription.contentMode = .top

                }
                else{
                    targetcell.lblDescription.text = ""
                }
            }
            else if arrOfTblViewCells[indexPath.row]  == "Attributes"{
                targetcell.lblTitle.text = "Attributes: "
                let strArr = objWorkorderDetail.value(forKey: "atributes") as! String
                if strArr != ""{

                    targetcell.lblDescription.text = strArr
                    
                    heightAttributesAccordingText = CGFloat()

                    let font = UIFont.systemFont(ofSize: 14.0)
                    self.heightAttributesAccordingText = heightForView(text: targetcell.lblDescription.text!, font: font, width: targetcell.lblDescription.frame.size.width)
                    

                    targetcell.heightConstraintForLblValue.constant = self.heightAttributesAccordingText
                    targetcell.lblDescription.contentMode = .top

                }
                else{
                    targetcell.lblDescription.text = ""
                }
            }
            
            return  targetcell
        }
        
        else {
            
            let SetupInstructionAndTechCommnetCell = tableView.dequeueReusableCell(withIdentifier: "GenralInfoSetupInstructionAndTechCommnetsCell") as! GenralInfoSetupInstructionAndTechCommnetsCell
            
            SetupInstructionAndTechCommnetCell.lblTitleName.text = arrOfTblViewCells[indexPath.row]
            
            if arrOfTblViewCells[indexPath.row]  == "Setup Instructions"{
                SetupInstructionAndTechCommnetCell.btnAddCommnent.isHidden = true
                let strSD = objWorkorderDetail.value(forKey: "otherInstruction") as! String
                if strSD != ""{
                    SetupInstructionAndTechCommnetCell.lblDescription.text = strSD
                }
                else{
                    SetupInstructionAndTechCommnetCell.lblDescription.text = ""
                }
            }
            else if arrOfTblViewCells[indexPath.row]  == "Work Order Instruction"{
                SetupInstructionAndTechCommnetCell.btnAddCommnent.isHidden = true
                
                let strSI = objWorkorderDetail.value(forKey: "serviceInstruction") as! String
                if strSI != ""{
                    SetupInstructionAndTechCommnetCell.lblDescription.text = strSI
                }
                else{
                    SetupInstructionAndTechCommnetCell.lblDescription.text = ""
                }
            }
            else if arrOfTblViewCells[indexPath.row]  == "Tech Comment"{
                if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
                {
                    SetupInstructionAndTechCommnetCell.btnAddCommnent.isHidden = true
                    
                }
                else{
                    SetupInstructionAndTechCommnetCell.btnAddCommnent.isHidden = false
                    SetupInstructionAndTechCommnetCell.btnAddCommnent.tag = indexPath.row
                    SetupInstructionAndTechCommnetCell.btnAddCommnent.addTarget(self, action: #selector(btnTechCommnetAction), for: .touchUpInside)
                }
                
                let strTechComm = objWorkorderDetail.value(forKey: "technicianComment") as! String
                if strTechComm != ""{
                    SetupInstructionAndTechCommnetCell.lblDescription.text = strTechComm
                }
                else{
                    SetupInstructionAndTechCommnetCell.lblDescription.text = ""
                }
                
                if strButtonTitleOfStartDate.lowercased() == "Start".lowercased(){
                    SetupInstructionAndTechCommnetCell.btnAddCommnent.isUserInteractionEnabled = false
                }
                else{
                    SetupInstructionAndTechCommnetCell.btnAddCommnent.isUserInteractionEnabled = true
                }
            }
            return  SetupInstructionAndTechCommnetCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if arrOfTblViewCells[indexPath.row] == "General Info Header"{
            
            return 450
        }
        
        else if arrOfTblViewCells[indexPath.row] == "Services" {
            return 30
        }
        else if arrOfTblViewCells[indexPath.row] == "Targets"{
            if heightTargetsAccordingText == 0{
                return 30
            }
            else{
                return heightTargetsAccordingText + 25
                
            }
        }
        else if arrOfTblViewCells[indexPath.row] == "Attributes"{
            if heightAttributesAccordingText == 0{
                return 30
            }
            else{
                return heightAttributesAccordingText + 25
                
            }
        }
        else{
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
          return true
      }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
    -> UISwipeActionsConfiguration? {
        
        if arrOfTblViewCells[indexPath.row] == "Services" && strWorkOrderStatus.lowercased() == "Incomplete".lowercased(){
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                // delete the item here
                print(self.arrOfServices[indexPath.row - 1][0])
                let strServiceId = self.arrOfServices[indexPath.row - 1][3]
                self.deleteServiceFromDatbase(strServiceId : strServiceId)
                if #available(iOS 13.0, *)
                {
                    completionHandler(true)
                }
                
            }
            deleteAction.image = UIImage(named: "trashServiceWhite.png")
            deleteAction.backgroundColor = UIColor(hex: "b51e32")
            
            let editAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in

                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditServiceGeneralInfoVC") as? EditServiceGeneralInfoVC
                
            
                vc?.strEditServiceId = self.arrOfServices[indexPath.row - 1][3]
                vc?.strEditServiceName = self.arrOfServices[indexPath.row - 1][0]
                vc?.strEditServicePrice = self.arrOfServices[indexPath.row - 1][1]
                vc?.strEditServiceQuantity = self.arrOfServices[indexPath.row - 1][2]
                vc?.strWoId = self.strWoId as String
                vc?.strEditIsTaxble = self.arrOfServices[indexPath.row - 1][4]

                self.navigationController?.pushViewController(vc!, animated: false)

                if #available(iOS 13.0, *)
                {
                    completionHandler(true)
                }
                
            }
            editAction.image = UIImage(named: "editServiceWhite.png")
            editAction.backgroundColor = UIColor(hex: "d1982e")
            
            let configuration = UISwipeActionsConfiguration(actions: [editAction, deleteAction])
            return configuration
        }
        else{
            let configuration = UISwipeActionsConfiguration(actions: [])
            return configuration
        }
    }
    
    @objc func actionOnServiceAutoPhoneClick(sender: UIButton!)
    {
        let phone = (objWorkorderDetail.value(forKey: "primaryPhone"))  as! String

        if phone != ""{
            global.calling(phone)
        }
        
    }
    
    @objc func actionOnServiceAutoEmailClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let email = (objWorkorderDetail.value(forKey: "primaryEmail"))  as! String

            if email != ""{
                self.sendEmail(strEmail: "\(objWorkorderDetail.value(forKey: "primaryEmail")!)")
            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    // MARK: -  ------------------------------ Send Mail Delegate ------------------------------
    
    func sendEmail(strEmail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(strEmail)"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        //controller.dismiss(animated: true)
        dismiss(animated: false) {
            
            if result == .sent || result == .failed{
                print("scussess")
                
            }
            
        }
    }
    
    @objc func actionOnServiceAutoAddressClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let strAddress = global.strCombinedAddressBilling(for: objWorkorderDetail)
            
            if strAddress!.count > 0
            {
                
                let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
                // Create the actions
                
                let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    Global().redirect(onAppleMap: self, strAddress)
                    
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func btnTechCommnetAction(sender: UIButton!){
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTechCommentsVC") as? AddTechCommentsVC
        
        vc!.strWorkorderId = (objWorkorderDetail.value(forKey: "workorderId") as? String)!
        vc!.strTechComment = (objWorkorderDetail.value(forKey: "technicianComment") as? String ?? "")
        vc?.delegateToAddComment = self
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @objc func btnAddServicesAction(sender: UIButton!){
        
        let vc = UIStoryboard.init(name: "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddServiceListVC") as? AddServiceListVC
        
        vc!.strUserName = strUserName
        vc!.strCompanyKey = strCompanyKey
        vc!.strWoId = strWoId as String
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    @objc func btnCollectPaymentAction(sender: UIButton!){
        let index = IndexPath(item: 0, section: 0)
        let cell = self.tblView.cellForRow(at: index) as! GeneralHeaderCell

        if cell.btnCollectPayment.tag == 0{
            cell.btnCollectPayment.tag = 1
            isCollectPayment = true
            self.tblView.reloadData()
            updateCollectPayment()
        }
        else if cell.btnCollectPayment.tag == 1{
            cell.btnCollectPayment.tag = 0
            isCollectPayment = false
            self.tblView.reloadData()
            updateCollectPayment()

        }
        
    }
    
    func updateCollectPayment(){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("isCollectPayment")
        arrOfValues.add(String(isCollectPayment))

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
}


class GenralInfoAddServiceCell: UITableViewCell {
    
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblServiceQantityAndPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class GenralInfoTargetsAndAttributes: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var heightConstraintForLblValue: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class GenralInfoSetupInstructionAndTechCommnetsCell: UITableViewCell {
    
    @IBOutlet weak var btnAddCommnent: UIButton!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}


extension UIImageView {
public func imageFromServerURL(urlString: String) {
    self.image = nil
    let urlStringNew = urlString.replacingOccurrences(of: " ", with: "%20")
    URLSession.shared.dataTask(with: NSURL(string: urlStringNew)! as URL, completionHandler: { (data, response, error) -> Void in

        if error != nil {
            return
        }
        DispatchQueue.main.async(execute: { () -> Void in
            let image = UIImage(data: data!)
            self.image = image
        })

    }).resume()
}}


extension Date {

func offsetFrom(date: Date) -> String {

    let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
    let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self)

    let seconds = "\(difference.second ?? 0)s"
    let minutes = "\(difference.minute ?? 0)m" + " " + seconds
    let hours = "\(difference.hour ?? 0)h" + " " + minutes
    let days = "\(difference.day ?? 0)d" + " " + hours

    if let day = difference.day, day          > 0 { return days }
    if let hour = difference.hour, hour       > 0 { return hours }
    if let minute = difference.minute, minute > 0 { return minutes }
    if let second = difference.second, second > 0 { return seconds }
    return ""
}
}

@IBDesignable class PaddingLabel: UILabel {

    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }

    override var bounds: CGRect {
        didSet {
            // ensures this works within stack views if multi-line
            preferredMaxLayoutWidth = bounds.width - (leftInset + rightInset)
        }
    }
}

extension Sequence where Self.Element == String {

  func toBulletList(_ bulletIndicator: String = "•",
                    itemSeparator: String = "\n",
                    spaceCount: Int = 2) -> String {
    let bullet = bulletIndicator + String(repeating: " ", count: spaceCount)
    let list = self
      .map { bullet + $0 }
      .reduce("", { $0 + ($0.isEmpty ? $0 : itemSeparator) + $1 })
    return list
  }
}
