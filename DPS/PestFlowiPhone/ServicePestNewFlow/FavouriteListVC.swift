//
//  FavouriteListVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol DidSelectTheFavouriteCell: class {
    
    func didSelectTheTechComment(strTitle: String, strId: String, strDesription: String)
}


class FavouriteListVC: UIViewController, UITextFieldDelegate {

    //MARK: -IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnCancelSearch: UIButton!

    //MARK: -Variables
    var arrOfFavUnFavList = [[String: Any]]()
    var arrOfGeneralNotesMaster = [[String: Any]]()
    
    var objGeneralInfoList = NSMutableArray()
    var strEmployeeId = ""
    var strWorkOrderId = ""
    var heightForDescription = CGFloat()
    weak var delegate: DidSelectTheFavouriteCell?

    var arrOfSearchList = NSMutableArray()
    var isSeaching = Bool()
    var loader = UIAlertController()

    //MARK: -View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.delegate = self
        tblView.dataSource = self
        txtSearch.delegate = self
        
        if !isInternetAvailable(){
            fetchTechCommentListFromDB()
        }
        else{
            getGeneralNotesByMaster()
            getTechCommentList()
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnCancelSearchAction(_ sender: Any) {
        isSeaching = false
        arrOfSearchList.removeAllObjects()
        txtSearch.text = ""
        btnCancelSearch.isHidden = true
        tblView.reloadData()
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK: - UITextfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if range.location == 0  && (string == " ") {
            btnCancelSearch.isHidden = true
            return false
        }
        else{
            if searchedText == ""{
                self.arrOfSearchList.removeAllObjects()
                self.isSeaching = false
                self.tblView.reloadData()
                btnCancelSearch.isHidden = true
                
            }
            else  if searchedText.count > 0 {
                self.arrOfSearchList.removeAllObjects()
                btnCancelSearch.isHidden = false
               
                arrOfSearchList.removeAllObjects()
                
                let arrOfData = getDataFromCoreDataBaseArraySorted(strEntity: "GeneralInfoTechComment", predicate: NSPredicate(format: "workOrderId == %@ && isFavorite == %@ && title contains[c] %@", self.strWorkOrderId, "true", searchedText), sort: NSSortDescriptor(key: "title", ascending: true))
                
                for i in 0..<arrOfData.count{
                    let obj = arrOfData[i] as! NSManagedObject
                    arrOfSearchList.add(obj)
                }

                let arrOfData2 = getDataFromCoreDataBaseArraySorted(strEntity: "GeneralInfoTechComment", predicate: NSPredicate(format: "workOrderId == %@ && isFavorite == %@ && title contains[c] %@", self.strWorkOrderId, "false", searchedText), sort: NSSortDescriptor(key: "title", ascending: true))
                
                for i in 0..<arrOfData2.count{
                    let obj = arrOfData2[i] as! NSManagedObject
                    arrOfSearchList.add(obj)
                }
                
                
//
//                arrOfSearchList = arrOfGeneralNotesMaster.filter({
//                    // this is where you determine whether to include the specific element, $0
//                    return ($0["Title"]! as! String).lowercased().contains(searchedText.lowercased()) //&& ($0["compType"]! as! String).contains(txtSearch.text!)
//                    // or whatever search method you're using instead
//                })
                self.isSeaching = true
                tblView.reloadData()
                
            } else {
                self.arrOfSearchList.removeAllObjects()
                self.isSeaching = false
                btnCancelSearch.isHidden = true
                self.tblView.reloadData()
                
            }
            return true
        }
    }
    //MARK: - Custom Functions
   
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    
    //MARK: - DB funcs
    
    func setDataIntoCoreDate(arrOfCurrentIndex: [String: Any]){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyId")
        arrOfKeys.add("techDescription")
        arrOfKeys.add("isActive")
        arrOfKeys.add("isFavorite")
        arrOfKeys.add("noteId")
        arrOfKeys.add("title")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("UserName")
        
        
        arrOfValues.add(String(describing : arrOfCurrentIndex["CompanyId"] as? NSNumber ?? 0))
        arrOfValues.add(arrOfCurrentIndex["GeneralNoteDescription"] as? String ?? "")
        
        let tempIsActive = String(describing : arrOfCurrentIndex["IsActive"] as? NSNumber ?? 0)
        
        if tempIsActive == "0" || tempIsActive == "false"{
            arrOfValues.add("false")
        }
        else if tempIsActive == "1" || tempIsActive == "true"{
            arrOfValues.add("true")
        }
        
        arrOfValues.add("false")
        arrOfValues.add(String(describing : arrOfCurrentIndex["GeneralNoteId"] as? NSNumber ?? 0))
        arrOfValues.add(arrOfCurrentIndex["Title"] as? String ?? "")
        arrOfValues.add(strWorkOrderId)
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        
        saveDataInDB(strEntity: "GeneralInfoTechComment", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    
    func setFavTechCommnetInDB(arrOfCurrentIndex: [String: Any]){
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoTechComment", predicate: NSPredicate(format: "workOrderId == %@", self.strWorkOrderId))

        for i in 0..<arryOfData.count{
            let tempDict = arryOfData[i] as! NSManagedObject
            
            let strGeneralMasterNoteId = tempDict.value(forKey: "noteId") as! String
            let strNoteId = String(describing: arrOfCurrentIndex["NoteId"] as? NSNumber ?? 0)
            print("masterNoteId : \(strGeneralMasterNoteId)")
            print("FavNoteId : \(strNoteId)")

            if strGeneralMasterNoteId == strNoteId{
                updateIsFavTrue(strNoteId: strNoteId, strIsFav: "true")
                break
            }
        }
        
    }
    
    func clearDBBeforeAddingAPIData(){
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoTechComment", predicate: NSPredicate(format: "workOrderId == %@", self.strWorkOrderId))
        
        if arryOfData.count > 0
        {
            for i in 0..<arryOfData.count{
                let objData = arryOfData[i] as! NSManagedObject
                deleteDataFromDB(obj: objData)
            }
        }
    }
    func updateIsFavTrue(strNoteId: String, strIsFav: String){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("isFavorite")
        arrOfValues.add(strIsFav)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "GeneralInfoTechComment", predicate: NSPredicate(format: "noteId == %@ && workOrderId == %@", strNoteId, self.strWorkOrderId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            let arrOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoTechComment", predicate: NSPredicate(format: "workOrderId == %@", self.strWorkOrderId))
            
            for i in 0..<arrOfData.count {
                let temp = arrOfData[i] as! NSManagedObject
                print(temp.value(forKey: "isFavorite") as? String ?? "")

            }
            
            fetchTechCommentListFromDB()
           // print(isSuccess)
        } else {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    func fetchTechCommentListFromDB(){
        
        objGeneralInfoList.removeAllObjects()
        
        let arrOfData = getDataFromCoreDataBaseArraySorted(strEntity: "GeneralInfoTechComment", predicate: NSPredicate(format: "workOrderId == %@ && isFavorite == %@", self.strWorkOrderId, "true"), sort: NSSortDescriptor(key: "title", ascending: true))
        
        for i in 0..<arrOfData.count{
            let obj = arrOfData[i] as! NSManagedObject
            objGeneralInfoList.add(obj)
        }

        let arrOfData2 = getDataFromCoreDataBaseArraySorted(strEntity: "GeneralInfoTechComment", predicate: NSPredicate(format: "workOrderId == %@ && isFavorite == %@", self.strWorkOrderId, "false"), sort: NSSortDescriptor(key: "title", ascending: true))
        
        for i in 0..<arrOfData2.count{
            let obj = arrOfData2[i] as! NSManagedObject
            objGeneralInfoList.add(obj)
        }
        
        
        tblView.reloadData()
    }
    
    
    //MARK: - Call WebService
    func getTechCommentList(){
            
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
                
        let strURL = String(format:  "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.getListOfComment , strCompanyId, strEmpID)
            
            
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
                if(status)
                {
                    self.loader.dismiss(animated: false) {

                    self.arrOfFavUnFavList.removeAll()
                    self.arrOfFavUnFavList = response.value(forKey: "data") as! [[String:Any]]
                    for i in 0..<arrOfFavUnFavList.count{
                        setFavTechCommnetInDB(arrOfCurrentIndex: arrOfFavUnFavList[i])
                    }
                    fetchTechCommentListFromDB()
                    tblView.reloadData()
                    }
                }
            }
    }
    
    
    func getGeneralNotesByMaster(){
            
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
                
        let strURL = String(format:  "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.getGenreralNotesByMaster , strCompanyId)
            
            
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
                if(status)
                {
                    self.arrOfGeneralNotesMaster.removeAll()
                    self.clearDBBeforeAddingAPIData()
                    self.arrOfGeneralNotesMaster = response.value(forKey: "data") as! [[String:Any]]
                    for i in 0..<self.arrOfGeneralNotesMaster.count{
                        setDataIntoCoreDate(arrOfCurrentIndex: arrOfGeneralNotesMaster[i])
                    }
                    tblView.reloadData()
                }
            }
    }
    
    func callWebServiceToMarkCommentFavourite(strCompanyId: String, strNoteId: String, strDescription: String, strTitle: String, strIsActive: String, strIsFav: String)
    {
        let loaderLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loaderLocal, animated: true, completion: nil)
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.markFavouiteComment
        
        let innerParam = ["WorkOrderId":strWorkOrderId,
                     "CompanyId": strCompanyId,
                     "EmployeeId": strEmployeeId,
                     "NoteId": strNoteId,
                     "Description": strDescription,
                     "Title": strTitle,
                     "IsFavorite" : strIsFav,
                     "IsActive": strIsActive,
                     "CreatedBy" :  strEmployeeId] as [String: Any]
    
        let param = ["ServiceNoteFavoriteDetailExtDcs" : [innerParam]] as [String: Any]
            
        WebService.postRequestWithHeaders(dictJson: param as NSDictionary , url: strURL, responseStringComing: "") { (response, status) in
            
            loaderLocal.dismiss(animated: false) {

                
                self.getGeneralNotesByMaster()
                self.getTechCommentList()
                self.tblView.reloadData()
                self.tblView.reloadData()
                if strIsFav == "true"{
                    self.view.makeToast("Comment marked favourite successfully")
                }
                else{
                    self.view.makeToast("Comment marked unfavourite successfully")
                }
            }
        }
        
    }
    
    func callWebServiceToDeleteComment(strNoteId: String)
    {
        let loaderLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loaderLocal, animated: true, completion: nil)
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.deleteTechComment
    
        let param = [strNoteId] as [String]
            
        WebService.postArrayRequestWithHeadersNewRuchika(dictJson: param, url: strURL, responseStringComing: "Success") { (response, status) in
            
            loaderLocal.dismiss(animated: false) {
                if response.value(forKey: "status") as! String == "success"
                {
                    self.view.makeToast("Comment deleted successfully")
                    self.getGeneralNotesByMaster()
                    self.getTechCommentList()
                    self.isSeaching = false
                    self.arrOfSearchList.removeAllObjects()
                    self.txtSearch.text = ""
                    self.btnCancelSearch.isHidden = true
                    self.tblView.reloadData()
                }
            }
        }
        
    }
    
}
extension FavouriteListVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSeaching == true {
            return arrOfSearchList.count
        }
        else{
        return objGeneralInfoList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.isSeaching == true {
            
            let obj = arrOfSearchList[indexPath.row] as! NSManagedObject
            
            if obj.value(forKey: "isActive") as? String ?? "" == "true"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "FavTblCell") as! FavTblCell
                cell.selectionStyle = .none
                cell.lblTitle.text = obj.value(forKey:"title") as? String ?? ""
                cell.lblTitle.underLine()
                cell.lblDescription.text = obj.value(forKey:"techDescription") as? String ?? ""
                
                let font = UIFont(name: "Helvetica", size: 11.0)
                heightForDescription = heightForView(text: cell.lblDescription.text!, font: font!, width: cell.lblDescription.frame.size.width)
                
                if obj.value(forKey: "isFavorite") as? String ?? "" == "true"
                {
                    cell.btnFav.setImage(UIImage(named: "fav"), for: .normal)

                }
                else{
                    cell.btnFav.setImage(UIImage(named: "unfav"), for: .normal)

                }

                cell.btnFav.tag = indexPath.row
                cell.btnFav.addTarget(self, action: #selector(btnFavAction), for: .touchUpInside)
                
                return  cell
            }
        }
        else{
            
            let obj = objGeneralInfoList[indexPath.row] as! NSManagedObject

            if obj.value(forKey: "isActive") as? String ?? "" == "true"{
                
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "FavTblCell") as! FavTblCell
                cell.selectionStyle = .none
                
                cell.lblTitle.text = obj.value(forKey:"title") as? String ?? ""
                cell.lblTitle.underLine()
                cell.lblDescription.text = obj.value(forKey:"techDescription") as? String ?? ""
                
                let font = UIFont(name: "Helvetica", size: 11.0)
                heightForDescription = heightForView(text: cell.lblDescription.text!, font: font!, width: cell.lblDescription.frame.size.width)
                
                if obj.value(forKey: "isFavorite") as? String ?? "" == "true"
                {
                    cell.btnFav.setImage(UIImage(named: "fav"), for: .normal)

                }
                else{
                    cell.btnFav.setImage(UIImage(named: "unfav"), for: .normal)

                }
                
                cell.btnFav.tag = indexPath.row
                cell.btnFav.addTarget(self, action: #selector(btnFavAction), for: .touchUpInside)
                return  cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if self.isSeaching == true {
            
            let obj = arrOfSearchList[indexPath.row] as! NSManagedObject

            delegate?.didSelectTheTechComment(strTitle: obj.value(forKey: "title") as? String ?? "", strId: obj.value(forKey: "noteId") as? String ?? "", strDesription: obj.value(forKey: "techDescription") as? String ?? "")

            self.navigationController?.popViewController(animated: false)
        }
        else{
            let obj = objGeneralInfoList[indexPath.row] as! NSManagedObject

            delegate?.didSelectTheTechComment(strTitle: obj.value(forKey: "title") as? String ?? "", strId: obj.value(forKey: "noteId") as? String ?? "", strDesription: obj.value(forKey: "techDescription") as? String ?? "")
        
        self.navigationController?.popViewController(animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
    -> UISwipeActionsConfiguration? {
        
        if self.isSeaching == true {
            
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                // delete the item here
                let obj = self.objGeneralInfoList[indexPath.row] as! NSManagedObject

                let strNoteId = obj.value(forKey: "noteId") as? String ?? ""
                self.callWebServiceToDeleteComment(strNoteId: strNoteId)
                completionHandler(true)
            }
            
            deleteAction.image = UIImage(named: "trashServiceWhite.png")
            deleteAction.backgroundColor = UIColor(hex: "b51e32")
            
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
            
        }
        else{
            
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                // delete the item here
                let obj = self.objGeneralInfoList[indexPath.row] as! NSManagedObject

                let strNoteId = obj.value(forKey: "noteId") as? String ?? ""
                self.callWebServiceToDeleteComment(strNoteId: strNoteId)
                completionHandler(true)
            }
            deleteAction.image = UIImage(named: "trashServiceWhite.png")
            deleteAction.backgroundColor = UIColor(hex: "b51e32")
            
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
        }
        
    }
    
    
    @objc func btnFavAction(sender: UIButton!){
        if self.isSeaching == true {
            
            let obj = self.objGeneralInfoList[sender.tag] as! NSManagedObject
            
            let strCompanyId = obj.value(forKey: "companyId") as? String ?? ""
            let strNoteId =  obj.value(forKey: "noteId") as? String ?? ""
            let strDescription = obj.value(forKey: "techDescription") as? String ?? ""
            let strTitle = obj.value(forKey: "title") as? String ?? ""
            let strIsActive = obj.value(forKey: "isActive") as? String ?? ""
            
            var strFinalIsActive = ""
            var strIsFav = ""
            
            let index = IndexPath(item: sender.tag, section: 0)
            let cell = self.tblView.cellForRow(at: index) as! FavTblCell
            
            if cell.btnFav.currentImage == UIImage(named: "fav"){
                cell.btnFav.setImage(UIImage(named: "unfav"), for: .normal)
                strIsFav = "false"
                
            }
            else if cell.btnFav.currentImage == UIImage(named: "unfav"){
                cell.btnFav.setImage(UIImage(named: "fav"), for: .normal)
                strIsFav = "true"
            }
            
            if strIsActive == "1" || strIsActive == "true"{
                strFinalIsActive = "true"
            }
            else{
                strFinalIsActive = "false"
            }
            
            callWebServiceToMarkCommentFavourite(strCompanyId: strCompanyId, strNoteId: strNoteId, strDescription: strDescription, strTitle: strTitle, strIsActive:  strFinalIsActive, strIsFav: strIsFav)
            
        }
        else{
            
            let obj = self.objGeneralInfoList[sender.tag] as! NSManagedObject
            
            let strCompanyId = obj.value(forKey: "companyId") as? String ?? ""
            let strNoteId =  obj.value(forKey: "noteId") as? String ?? ""
            let strDescription = obj.value(forKey: "techDescription") as? String ?? ""
            let strTitle = obj.value(forKey: "title") as? String ?? ""
            let strIsActive = obj.value(forKey: "isActive") as? String ?? ""
            
            var strFinalIsActive = ""
            var strIsFav = ""
            
            let index = IndexPath(item: sender.tag, section: 0)
            let cell = self.tblView.cellForRow(at: index) as! FavTblCell
            
            if cell.btnFav.currentImage == UIImage(named: "fav"){
                cell.btnFav.setImage(UIImage(named: "unfav"), for: .normal)
                strIsFav = "false"
                
            }
            else if cell.btnFav.currentImage == UIImage(named: "unfav"){
                cell.btnFav.setImage(UIImage(named: "fav"), for: .normal)
                strIsFav = "true"
            }
            
            if strIsActive == "1" || strIsActive == "true"{
                strFinalIsActive = "true"
            }
            else{
                strFinalIsActive = "false"
            }
            callWebServiceToMarkCommentFavourite(strCompanyId: strCompanyId, strNoteId: strNoteId, strDescription: strDescription, strTitle: strTitle, strIsActive:  strFinalIsActive, strIsFav: strIsFav)
        }
    }
    
    
    @objc func btnReadMoreAction(sender: UIButton!){
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 + heightForDescription
    }
    
    
}

extension UILabel{

    func underLine(){
        if let textUnwrapped = self.text{
            let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
            let underlineAttributedString = NSAttributedString(string: textUnwrapped, attributes: underlineAttribute)
            self.attributedText = underlineAttributedString
        }
    }
}



