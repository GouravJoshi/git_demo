//
//  InspectionVC.h
//  DPS
//  peSTream
//  Created by Saavan Patidar on 21/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//
//  Saavan Patidar


#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface InspectionVC : UIViewController<UIScrollViewDelegate,NSFetchedResultsControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entitySalesDynamic,*entityModifyDateServiceAuto;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    NSEntityDescription *entityPaymentInfo;
    NSFetchRequest *requestPaymentInfo;
    NSSortDescriptor *sortDescriptorPaymentInfo;
    NSArray *sortDescriptorsPaymentInfo;
    NSManagedObject *matchesPaymentInfo;
    NSArray *arrAllObjPaymentInfo;
    
    
    NSEntityDescription *entityProductDetail;
    NSFetchRequest *requestProductDetail;
    NSSortDescriptor *sortDescriptorProductDetail;
    NSArray *sortDescriptorsProductDetail;
    NSManagedObject *matchesProductDetail;
    NSArray *arrAllObjProductDetail;
    
    NSEntityDescription *entityImageDetail,*entityPaymentInfoServiceAuto;
    NSFetchRequest *requestImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSManagedObject *matchesImageDetail;
    NSArray *arrAllObjImageDetail;
    
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
}

@property (strong, nonatomic) IBOutlet UITextField *txt_DrivingLicenseNo;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_BillPaymentView_Top;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_BillPaymentView_H;
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollForDynamicView;
- (IBAction)action_Save:(id)sender;
- (IBAction)action_Cancel:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;
@property (strong, nonatomic) IBOutlet UIView *paymentInfoView;
@property (strong, nonatomic) IBOutlet UITextField *txtInvoicePayment;
@property (strong, nonatomic) IBOutlet UITextField *txtProductionPayment;
@property (strong, nonatomic) IBOutlet UITextField *txtPreviousBalance;
@property (strong, nonatomic) IBOutlet UITextField *txtTax;
@property (strong, nonatomic) IBOutlet UITextField *txtTotalDue;
@property (strong, nonatomic) IBOutlet UIView *paymentTypeView;
- (IBAction)action_Cash:(id)sender;
- (IBAction)action_Check:(id)sender;
- (IBAction)action_CreditCard:(id)sender;
- (IBAction)action_Bill:(id)sender;
- (IBAction)action_AutoChargerCust:(id)sender;
- (IBAction)action_PaymentPending:(id)sender;
- (IBAction)action_NoCharge:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCash;
@property (strong, nonatomic) IBOutlet UIButton *btnCheck;
@property (strong, nonatomic) IBOutlet UIButton *btnCreditCard;
@property (strong, nonatomic) IBOutlet UIButton *btnBill;
@property (strong, nonatomic) IBOutlet UIButton *btnAutoCharger;
@property (strong, nonatomic) IBOutlet UIButton *btnPaymentPending;
@property (strong, nonatomic) IBOutlet UIButton *btnNoCharge;
@property (strong, nonatomic) IBOutlet UILabel *lblLinePaymentInfo;
@property (strong, nonatomic) IBOutlet UIView *lastViewAfterImage;
- (IBAction)action_AfterImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *checkVieww;
@property (strong, nonatomic) IBOutlet UITextField *txtAmount;
@property (strong, nonatomic) IBOutlet UITextField *txtCheckNo;
- (IBAction)action_CheckExpDate:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckExpDate;
- (IBAction)action_CheckFrontImage:(id)sender;
- (IBAction)action_CheckBackImage:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtAmountSingle;
@property (strong, nonatomic) IBOutlet UIView *amountViewSingle;
- (IBAction)action_Refersh:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerPaymentInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerProductDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;
- (IBAction)action_ServiceDocuments:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AccountNos;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) IBOutlet UIView *beforeImageView;
@property (strong, nonatomic) IBOutlet UIView *afterImageView;
@property (strong, nonatomic) IBOutlet UIView *beforeImageThumbNailView;
@property (strong, nonatomic) IBOutlet UIView *afterImageThumbNailView;
@property (strong, nonatomic)  NSManagedObject *matchesWorkOrderZSync;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg1;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg2;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg3;
- (IBAction)action_ButtonImg1:(id)sender;
- (IBAction)action_ButtonImg2:(id)sender;
- (IBAction)action_ButtonImg3:(id)sender;
- (IBAction)actionOnBeforeImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewAddOtherProductList;
@property (strong, nonatomic) IBOutlet UITextField *txtAddOtherProductName;
@property (strong, nonatomic) IBOutlet UITextField *txtAddOtherProductEPARegNo;
@property (strong, nonatomic) IBOutlet UITextField *txtAddOtherProductPercentage;
@property (strong, nonatomic) IBOutlet UITextField *txtAddOtherProductAmount;
- (IBAction)action_AddOtherProductUnit:(id)sender;
- (IBAction)action_AddOtherProductMethod:(id)sender;
- (IBAction)action_AddOtherProductTarget:(id)sender;
- (IBAction)action_AddOtherProductFinally:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewOtherChemicalList;
@property (strong, nonatomic) IBOutlet UIButton *btnUnitOtherMain;
@property (strong, nonatomic) IBOutlet UIButton *btnAppMethodOtherMain;
@property (strong, nonatomic) IBOutlet UIButton *btnTargetOtherMain;
- (IBAction)actionOnTempChemicalList:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddOtherProductMain;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenCash;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenCheck;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenCreditCard;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenBill;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenAutoChargeCustomer;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenPaymentPending;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenNoCharge;
@property (strong, nonatomic) IBOutlet UIButton *btnServiceJobDescriptionTitle;
- (IBAction)action_ServiceJobDescriptionTitle:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textView_ServiceJobDescriptions;
@property (strong, nonatomic) IBOutlet UIView *viewServiceJobDescriptions;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionView;

//Graph
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewGraph;
@property (strong, nonatomic) IBOutlet UIView *viewGraphImage;
@property (strong, nonatomic) IBOutlet UIView *viewFooter;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraphhh;

- (IBAction)action_GraphImage:(id)sender;
- (IBAction)action_AddGraph:(id)sender;
- (IBAction)action_CloseGraphView:(id)sender;

- (IBAction)action_AddEquipMentView:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddEquipMent;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;
@property (strong, nonatomic) IBOutlet UIButton *btnSavenContinue;

- (IBAction)action_BeforeImageNew:(id)sender;
- (IBAction)action_GraphImageNew:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveNew;
@property (strong, nonatomic) IBOutlet UIView *viewAddEquipment;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleHeader;


@end


NS_ASSUME_NONNULL_END
