//
//  InspectionVC.m
//  DPS
//
//  Created by Saavan Patidar on 21/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//
//  Saavan Patidar


#import "InspectionVC.h"
#import "AllImportsViewController.h"
#import "GeneralInfoAppointmentView.h"
#import "InvoiceAppointmentView.h"
#import "DejalActivityView.h"

#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "SalesAutomationInspection.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "AppDelegate.h"
#import "SalesAutomationSelectService.h"
#import "ServiceDynamicForm+CoreDataProperties.h"
//#import "ServiceDynamicForm.h"
#import <SafariServices/SafariServices.h>
#import "ImagePreviewGeneralInfoAppointmentView.h"
#import "PaymentInfoServiceAuto.h"
#import "PaymentInfoServiceAuto+CoreDataProperties.h"
#import "ServiceAutoModifyDate.h"
#import "ServiceAutoModifyDate+CoreDataProperties.h"
#import "ServiceDocumentsViewController.h"
//#import "ImageDetailsServiceAuto.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "ChemicalListPreview.h"
#import "EquipmentsViewController.h"
#import "EditImageViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "DPS-Swift.h"
#import "DPS-Swift.h"

@interface InspectionVC ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate>
{
    Global *global;
    NSDictionary *ResponseDict,*dictForAppoint,*dictChemicalList;
    UIButton *btnTemp;
    UILabel *lblTemp,*lblBorder,*lblBorder2;
    NSArray *arrOfLeads;
    NSMutableArray *arrResponseInspection;
    int heightViewForm,yViewForm;
    int heightSection,ySection;
    UIView *MainViewForm,*ViewFormSections,*viewForm,*viewSection,*MainViewFormOther;
    UIButton *BtnMainView;
    UILabel *labelChemicalHeadGlobal;
    //New change on 24 august 2016 for dynamic form
    NSMutableArray *arrayViews;
    NSMutableArray *arrayOfButtonsMain;
    NSMutableArray *arrOfHeightsViews;
    NSMutableArray *arrOfYAxisViews;
    NSMutableArray *arrOfButtonImages,*arrOfBeforeImageAll,*arrOfImagenameCollewctionView;
    
    //For DropDown i.e TAble View
    UITableView *tblData;
    NSMutableArray *arrDataTblView;
    UIView *viewBackGround;
    NSMutableArray *arrOfIndexes;
    NSMutableArray *arrOfControlssMain;
    BOOL isMultipleSelect,isUnit,isTarget,isAppMethodInfo,isUnitOther,isTargetOther,isAppMethodInfoOther,isUnitOtherMain,isTargetOtherMain,isAppMethodInfoOtherMain,isNoServiceJobDescriptions;
    UIView *viewForDate;
    UIDatePicker *pickerDate;
    NSMutableArray *arraySelecteValues,*arraySelecteValuesToShow;
    int indexMultipleSelectTosetValue;
    NSString *strWorkOrderId,*strDepartmentId;
    NSString *strEmpID,*strCompanyKey,*strCompanyId,*strServiceUrlMain,*strUserName;
    NSDictionary *finalJsonDict;
    NSString *strGlobalDateToShow,*strGlobalDatenTime;
    NSMutableArray *arrOFImagesName;
    NSMutableArray *arrOfCheckBackImage;
    NSString *strTypeOfImageSelected,*strGlobalDropDownSelectedValue;
    
    //Change for Chemical List
    
    UIView *MainViewFormChemical,*ViewFormSectionsChemical,*viewFormChemical,*viewSectionChemical,*MainViewFormChemicalOther;
    UIButton *BtnMainViewChemical,*BtnMainViewChemicalOther;
    
    //New change on 24 august 2016 for dynamic form
    NSMutableArray *arrayViewsChemical,*arrayProductName,*arrayOfUnit,*arrayOfTarget,*arrayOfAppMethodInfo,*arrayProductNameOther,*arrayOfUnitOther,*arrayOfAppMethodInfoOther,*arrayOfTargetOther,*arrayViewsChemicalOther;
    NSMutableArray *arrayOfButtonsMainChemical,*arrayOfButtonsMainChemicalOther,*arrayOfButtonsMainChemicalHidden,*arrayOfButtonsMainChemicalOtherHidden;
    NSMutableArray *arrOfHeightsViewsChemical,*arrOfHeightsViewsChemicalOther;
    NSMutableArray *arrOfYAxisViewsChemical,*arrOfYAxisViewsChemicalOther;
    NSMutableArray *arrOfButtonImagesChemical;
    NSMutableArray *arrOfIndexesChemical,*arrOfIndexesChemicalOther;
    NSMutableArray *arrOfControlssMainChemical,*arrOfControlssMainChemicalOther;
    NSMutableArray *arrOfWorkOrderProductDetails,*arrOfWorkOrderOtherChemicalList;
    NSMutableArray *arrOfTempWOPD,*arrOfSavedChemicalListOtherAllValues;
    NSMutableArray *arrTargetId,*arrAppMethodInfoId,*arrUnitId,*arrSelectedChemicals,*arrTxtAmount,*arrTxtPercent,*arrSetHeightZero,*arrEPARegNumber,*arrOfSelectedChemicalToSendToNextView,*arrOfTxtFieldsChemicalList,*arrEPARegNumberOther,*arrTxtPercentOther,*arrTxtAmountOther,*arrSelectedChemicalsOther,*arrOfSelectedChemicalToSendToNextViewOther,*arrUnitIdOther,*arrAppMethodInfoIdOther,*arrTargetIdOther,*arrSetHeightZeroOther,*arrOfImageCaption,*arrOfImageDescription;
    
    NSString *strGlobalPaymentMode,*strGlobalWorkOrderId,*strWorkOrderStatuss;
    BOOL isPaymentInfo,isCheckFrontImage,yesEditedSomething,isFromBeforeImage;
    BOOL noAmount,forFirstTimeToOpen;
    NSString *strGlobalAppMethodIdOther,*strGlobalUnitIdOther,*strGlobalTargetIdOther,*strPrimaryServiceSysName,*strCategorySysName,*strGlobalServiceJobDescriptionsText,*strGlobalServiceJobDescriptionId;
    NSDictionary *dictDeptNameByKey;
    UIPickerView *myPickerView;
    NSMutableArray *arrPickerServiceJobDescriptions;
    UIToolbar *toolBar;
    float heightForServiceJobDescriptions;
    UITextField *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    
    
    //Graph Image
    NSMutableArray *arrOfImagenameCollewctionViewGraph,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph,*arrOfBeforeImageAllGraph;
    
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    
}
@end

@implementation InspectionVC

- (void)viewDidLoad
{
    
    _btnSave.layer.cornerRadius = 10;
    _btnSave.layer.borderWidth = 1;
    _btnSave.layer.borderColor = UIColor.clearColor.CGColor;
    _btnSave.layer.masksToBounds = false;
    _btnSave.layer.shadowColor = UIColor.darkGrayColor.CGColor;
    _btnSave.layer.shadowOffset = CGSizeMake(1, 1);
    _btnSave.layer.shadowOpacity = 0.5;
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
    
    if (isNoServiceJobDescriptions) {
        
        heightForServiceJobDescriptions=-50;
        [_viewServiceJobDescriptions setHidden:YES];
        
    } else {
        
        [_viewServiceJobDescriptions setHidden:NO];
        heightForServiceJobDescriptions=_viewServiceJobDescriptions.frame.size.height;
        
    }
    
    [self fetchDepartmentNameBySysName];
    forFirstTimeToOpen=NO;
    yesEditedSomething=NO;
    isFromBeforeImage=NO;
    isNoServiceJobDescriptions=NO;
    
    [super viewDidLoad];
    isUnit=NO;
    isTarget=NO;
    isPaymentInfo=NO;
    isAppMethodInfo=NO;
    isCheckFrontImage=YES;
    noAmount=NO;
    [_txtInvoicePayment setEnabled:NO];
    [_txtProductionPayment setEnabled:NO];
    [_txtPreviousBalance setEnabled:NO];
    [_txtTax setEnabled:NO];
    [_txtTotalDue setEnabled:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionOnChemicalListDownload) name:@"ChemicalList" object:nil];
    
    strTypeOfImageSelected=@"No";
    arrayViewsChemical = [[NSMutableArray alloc] init];
    arrayViewsChemicalOther=[[NSMutableArray alloc]init];
    arrayOfButtonsMainChemical = [[NSMutableArray alloc] init];
    arrayOfButtonsMainChemicalHidden=[[NSMutableArray alloc]init];
    arrayOfButtonsMainChemicalOther =[[NSMutableArray alloc]init];
    arrayOfButtonsMainChemicalOtherHidden =[[NSMutableArray alloc]init];
    arrOfHeightsViewsChemical = [[NSMutableArray alloc] init];
    arrOfHeightsViewsChemicalOther = [[NSMutableArray alloc]init];
    arrOfYAxisViewsChemical = [[NSMutableArray alloc] init];
    arrOfYAxisViewsChemicalOther=[[NSMutableArray alloc]init];
    arrOfButtonImagesChemical = [[NSMutableArray alloc] init];
    arrayProductName=[[NSMutableArray alloc] init];
    arrayProductNameOther=[[NSMutableArray alloc]init];
    arrayOfUnit=[[NSMutableArray alloc] init];
    arrayOfUnitOther=[[NSMutableArray alloc]init];
    arrayOfTarget=[[NSMutableArray alloc] init];
    arrayOfTargetOther=[[NSMutableArray alloc]init];
    arrayOfAppMethodInfo=[[NSMutableArray alloc]init];
    arrayOfAppMethodInfoOther=[[NSMutableArray alloc]init];
    arrOfWorkOrderProductDetails=[[NSMutableArray alloc] init];
    arrOfWorkOrderOtherChemicalList=[[NSMutableArray alloc]init];
    arrOfTempWOPD=[[NSMutableArray alloc] init];
    arrSelectedChemicals=[[NSMutableArray alloc] init];
    arrSelectedChemicalsOther=[[NSMutableArray alloc]init];
    arrTargetId=[[NSMutableArray alloc] init];
    arrTargetIdOther=[[NSMutableArray alloc]init];
    arrAppMethodInfoId=[[NSMutableArray alloc]init];
    arrAppMethodInfoIdOther=[[NSMutableArray alloc]init];
    arrUnitId=[[NSMutableArray alloc] init];
    arrUnitIdOther=[[NSMutableArray alloc]init];
    arrTxtAmount=[[NSMutableArray alloc] init];
    arrTxtAmountOther=[[NSMutableArray alloc]init];
    arrTxtPercent=[[NSMutableArray alloc] init];
    arrTxtPercentOther=[[NSMutableArray alloc]init];
    arrEPARegNumber=[[NSMutableArray alloc]init];
    arrEPARegNumberOther=[[NSMutableArray alloc]init];
    arrSetHeightZero=[[NSMutableArray alloc] init];
    arrSetHeightZeroOther=[[NSMutableArray alloc]init];
    arrOfCheckBackImage=[[NSMutableArray alloc] init];
    arrOfSelectedChemicalToSendToNextView=[[NSMutableArray alloc]init];
    arrOfSelectedChemicalToSendToNextViewOther=[[NSMutableArray alloc]init];
    arrOfTxtFieldsChemicalList=[[NSMutableArray alloc]init];
    arrOfBeforeImageAll=[[NSMutableArray alloc]init];
    arrOfSavedChemicalListOtherAllValues=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    
    
    //Graph
    arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    
    //Graph
    arrOfBeforeImageAllGraph=[[NSMutableArray alloc]init];
    
    
    
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    //Graph
    _viewGraphImage.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _viewGraphImage.layer.borderWidth=1.0;
    _viewGraphImage.layer.cornerRadius=5.0;
    
    //============================================================================
    //============================================================================
#pragma mark- ----------------Temporary Change-------------------
    //============================================================================
    //============================================================================
    
    //Temporary HAi Comment KArna Yad SE
    //    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    //    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"ServiceWorkOrder.json"];
    //    NSError * error;
    //    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    //    NSDictionary *dictOfWorkkkkorders = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    //    NSArray *arrrr=[dictOfWorkkkkorders valueForKey:@"WorkOrderExtSerDcs"];
    //
    //    NSDictionary *WOPD=arrrr[15];
    //
    //    NSArray *arrWOPD=[WOPD valueForKey:@"WoProductDetail"];
    //
    //    [arrOfWorkOrderProductDetails addObjectsFromArray:arrWOPD];
    
    
    
    
    //end of temp change
    
    viewFormChemical=[[UIView alloc]init];
    viewSectionChemical=[[UIView alloc]init];
    
    //============================================================================
    //============================================================================
    
    arrOFImagesName=[[NSMutableArray alloc] init];
    
    global = [[Global alloc] init];
    [self getClockStatus];
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    //ServiceAutoCompanyId
    strCompanyId      =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ServiceAutoCompanyId"]];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    strDepartmentId=[defsLead valueForKey:@"strDepartmentId"];
    if (strWorkOrderId.length==0) {
        strWorkOrderId=@"52";
    }
    if (strDepartmentId.length==0) {
        strDepartmentId=@"10012";
    }
    
    _lbl_AccountNos.text=[defsLead valueForKey:@"lblName"];
    
    NSString *strThirdPartyAccountNo=[defsLead valueForKey:@"lblThirdPartyAccountNo"];
    if (strThirdPartyAccountNo.length>0) {
        _lbl_AccountNos.text=strThirdPartyAccountNo;
    }
    
    isMultipleSelect=NO;
    indexMultipleSelectTosetValue=-100000;
    //============================================================================
    //============================================================================
    
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
    arrayViews = [[NSMutableArray alloc] init];
    arrayOfButtonsMain = [[NSMutableArray alloc] init];
    arrOfHeightsViews = [[NSMutableArray alloc] init];
    arrOfYAxisViews = [[NSMutableArray alloc] init];
    arrOfButtonImages = [[NSMutableArray alloc] init];
    
    heightViewForm=0;yViewForm=0;
    heightSection=0;ySection=0;
    viewForm=[[UIView alloc]init];
    viewSection=[[UIView alloc]init];
    [super viewDidLoad];
    global = [[Global alloc] init];
    
    //Comment hata na hai yad se
    [self fetchWorkOrderFromDataBase];
    
    [self serviceAutomationInspection];
    
    //  [self dynamicFormCreation];
    
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 100, 40);
    lblTemp.frame=CGRectMake(0, 0, 100, 40);
    arrOfLeads=[[NSMutableArray alloc]init];
    arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"Genral Info",@"Inspection",@"Select Serivce",@"Service Summary",@"Agreement", nil];
    CGFloat scrollWidth=arrOfLeads.count*110;
    
    [self addButtons];
    //_cnstrnScrollDynamic_H.constant=1500;
    
    // Do any additional setup after loading the view.
    
    [self fetchImageDetailFromDataBaseNewForBeforeImage];
    
    _viewAddOtherProductList.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _viewAddOtherProductList.layer.borderWidth=1.0;
    _viewAddOtherProductList.layer.cornerRadius=5.0;
    
    _textView_ServiceJobDescriptions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _textView_ServiceJobDescriptions.layer.borderWidth=1.0;
    _textView_ServiceJobDescriptions.layer.cornerRadius=5.0;
    
    
    // self.imageCollectionView.collectionViewLayout = [[CustomImageFlowLayout alloc] init];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    NSUserDefaults *defLbl = [NSUserDefaults standardUserDefaults];
    _lblTitleHeader.text = [defLbl valueForKey:@"lblName"];
    
}

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    [_btnSavenContinue setEnabled:NO];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    [_btnSavenContinue setEnabled:YES];
}


-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
    
    BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
    
    if (isEquipEnabled) {
        
        [_btnAddEquipMent setHidden:NO];
        
    }else{
        
        [_btnAddEquipMent setHidden:YES];
        
    }
    
    //Graph
    [self fetchGraphImageDetailFromDataBase];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
    
    if (isNoServiceJobDescriptions) {
        
        heightForServiceJobDescriptions=-50;
        [_viewServiceJobDescriptions setHidden:YES];
        
    } else {
        [_viewServiceJobDescriptions setHidden:NO];
        heightForServiceJobDescriptions=_viewServiceJobDescriptions.frame.size.height;
        
    }
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        
        
        ////Akshay 22 may 2019
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        ////
        
    } else {
        
        [self setEditableNoForCompleteWO];
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        NSString *strFromWhere=[defs valueForKey:@"forWhichCheckImage"];
        if ([strFromWhere isEqualToString:@"Front"])
        {
            yesEditedSomething=YES;
            NSLog(@"Yes Something Edited");
            arrOFImagesName=nil;
            arrOFImagesName=[[NSMutableArray alloc]init];
            [defs setValue:@"KuchNhi" forKey:@"forWhichCheckImage"];
            [defs synchronize];
            
        } else if ([strFromWhere isEqualToString:@"Back"]){
            yesEditedSomething=YES;
            NSLog(@"Yes Something Edited");
            arrOfCheckBackImage=nil;
            arrOfCheckBackImage=[[NSMutableArray alloc]init];
            [defs setValue:@"KuchNhi" forKey:@"forWhichCheckImage"];
            [defs synchronize];
            
        }else{
            [defs setValue:@"KuchNhi" forKey:@"forWhichCheckImage"];
            [defs synchronize];
        }
        
        [self methodForImageOnViewWillAppear];
        
    }
    
    
    float heightCollectionView = (float)(arrOfImagenameCollewctionView.count)/3;
    
    int roundedUp = ceil(heightCollectionView); NSLog(@"%d",roundedUp);
    
    [_beforeImageThumbNailView setFrame:CGRectMake(_beforeImageThumbNailView.frame.origin.x, _beforeImageThumbNailView.frame.origin.y, _beforeImageThumbNailView.frame.size.width, _beforeImageThumbNailView.frame.size.height)];
    _imageCollectionView.frame=CGRectMake(_imageCollectionView.frame.origin.x, _imageCollectionView.frame.origin.y, _imageCollectionView.frame.size.width, _imageCollectionView.frame.size.height);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [txtFieldCaption resignFirstResponder];
    [txtViewImageDescription resignFirstResponder];
    
}
//============================================================================
//============================================================================
#pragma mark- ----------------Actions-------------------
//============================================================================
//============================================================================


- (IBAction)action_BeforeImageNew:(id)sender{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                             bundle: nil];
    GlobalImageVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objByProductVC.strHeaderTitle = @"Before";
    objByProductVC.strWoId = strGlobalWorkOrderId;
    objByProductVC.strModuleType = @"ServicePestFlow";
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"])
    {
        objByProductVC.strWoStatus = @"Completed";
    }else{
        objByProductVC.strWoStatus = @"InComplete";
    }
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}
- (IBAction)action_GraphImageNew:(id)sender{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                             bundle: nil];
    GlobalImageVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objByProductVC.strHeaderTitle = @"Graph";
    objByProductVC.strWoId = strGlobalWorkOrderId;
    objByProductVC.strModuleType = @"ServicePestFlow";
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"])
    {
        objByProductVC.strWoStatus = @"Completed";
    }else{
        objByProductVC.strWoStatus = @"InComplete";
    }
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}

- (IBAction)action_Back:(id)sender {
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    [defsBack setBool:YES forKey:@"isFromBackServiceDynamci"];
    [defsBack synchronize];
    
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
    //                                                             bundle: nil];
    //    DashBoardView
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardView"];
    //    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[GeneralInfoVC class]]) {
            index=k1;
            //break;
        }
    }
    GeneralInfoVC *myController = (GeneralInfoVC *)[self.navigationController.viewControllers objectAtIndex:index];
    // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    [self.navigationController popToViewController:myController animated:NO];
    
}

- (IBAction)action_Save:(id)sender {
    
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"])
    {
        [self finalSaveChemicalList];
        [self finalSaveChemicalListOther];
        //[self goToNextViewww];
        [self goToServiceDetailView];

    }
    else
        
    {
        
        [self.view endEditing:YES];
        
        //START new change for email valid overall
        BOOL isEmailValid;
        isEmailValid=YES;
        NSString *errorMessageTemp;
        for (int k=0; k<arrResponseInspection.count; k++) {
            
            NSDictionary *dictData=arrResponseInspection[k];
            NSArray *arrSectionsTemp=[dictData valueForKey:@"Sections"];
            
            for (int j=0; j<arrSectionsTemp.count; j++) {
                
                NSDictionary *dictDataControls=arrSectionsTemp[j];
                NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
                
                for (int l=0; l<arrOfControls.count; l++) {
                    
                    NSDictionary *dictOfOptions=arrOfControls[l];
                    if ([[dictOfOptions valueForKey:@"Element"] isEqualToString:@"el-email"]) {
                        
                        NSString *strValueee=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
                        if (strValueee.length==0) {
                            
                            isEmailValid=YES;
                            
                        }else{
                            
                            strValueee =  [strValueee stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            errorMessageTemp=[self validationCheck :strValueee :@"el-email"];
                            
                            if (errorMessageTemp) {
                                
                                isEmailValid=NO;
                                
                            }else{
                                
                                isEmailValid=YES;
                                
                            }
                        }
                    }
                }
            }
        }
        
        //END new change for email valid overall
        
        if (isEmailValid) {
            
            [self saveToCoreDataServiceDynamicFinal:arrResponseInspection];
            
            if (yesEditedSomething) {
                
                NSLog(@"Yes Something Edited and Finaly Saved");
                [self updateModifyDate];
                [self updateZSyncWorkOrder];
                
            }
            
            [self goToServiceDetailView];
            
        } else {
            
            NSString *strTitle = Alert;
            [global AlertMethod:strTitle :@"Please enter valid email id only"];
            
        }
    }
    
}

-(void)goToServiceDetailView{
    // IsRegularPestFlow  IsRegularPestFlowOnBranch
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"])
    {

        NSString *strIsRegularPestFlow = [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"isRegularPestFlow"]];
        
        if ([strIsRegularPestFlow isEqualToString: @"1"] || [strIsRegularPestFlow isEqualToString: @"true"] || [strIsRegularPestFlow isEqualToString: @"True"]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                                     bundle: nil];
            ServiceDetailVC
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDetailVC"];
            objByProductVC.strWoId = strGlobalWorkOrderId;
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }else{
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                                     bundle: nil];
            ServiceDetailAppointmentView
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDetailAppointmentView"];
            objByProductVC.matchesWorkOrderZSync=matchesWorkOrder;
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }
        
    }else{
        
        NSString *strIsRegularPestFlow = [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"isRegularPestFlowOnBranch"]];
        
        if ([strIsRegularPestFlow isEqualToString: @"1"] || [strIsRegularPestFlow isEqualToString: @"true"] || [strIsRegularPestFlow isEqualToString: @"True"]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                                     bundle: nil];
            ServiceDetailVC
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDetailVC"];
            objByProductVC.strWoId = strGlobalWorkOrderId;
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }else{
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                                     bundle: nil];
            ServiceDetailAppointmentView
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDetailAppointmentView"];
            objByProductVC.matchesWorkOrderZSync=matchesWorkOrder;
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }
        
    }
    
}

- (IBAction)action_Cancel:(id)sender {
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    [defsBack setBool:YES forKey:@"isFromBackServiceDynamci"];
    [defsBack synchronize];
    
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
    //                                                             bundle: nil];
    //    InvoiceAppointmentView
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"InvoiceAppointmentView"];
    //    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[GeneralInfoAppointmentView class]]) {
            index=k1;
            //break;
        }
    }
    GeneralInfoAppointmentView *myController = (GeneralInfoAppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
    // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    [self.navigationController popToViewController:myController animated:NO];
    
}

//============================================================================
#pragma mark- DYNAMIC LABEL AND BUTTON METHOD
//============================================================================

-(void)addButtons
{
    for(int i =0;i<arrOfLeads.count;i++)
    {
        
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+40, 0, 30, 30)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 15;
        lbl.layer.borderWidth=1.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 100, 20);
            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 30, 30);
            lbl.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor =  [UIColor lightGrayColor];//[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 100, 20);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 30, 30);
            lbl.textColor = [UIColor lightGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            
        }
        for(int j=0;j<=1;j++)
        {
            if (i==j)
            {
                
                
                lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
                lbl.textColor = [UIColor whiteColor];
                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];
        btn.titleLabel.numberOfLines=20000;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
}


//============================================================================
//============================================================================
#pragma mark- SalesAutomation API
//============================================================================
//============================================================================
-(void)FetchFromCoreDataToShowServiceDynamicInCaseOnline{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strWorkOrderId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            
            [self dynamicFormCreation];
            
        }else{
            
            [self fetchServiceAutomationService];
            
            
        }
        //[self dynamicFormCreation];
        
        
    }
    else
    {
        matches=arrAllObj[0];
        
        NSString *strIsSentToServer=[matches valueForKey:@"isSentToServer"];
        strIsSentToServer=@"No";
        if ([strIsSentToServer isEqualToString:@"Yes"]) {
            
            [self fetchServiceAutomationService];
            
        } else {
            
            NSArray *arrTemp = [matches valueForKey:@"arrFinalInspection"];
            
            if ([arrTemp isKindOfClass:[NSDictionary class]]) {
                
                arrResponseInspection =[[NSMutableArray alloc]init];
                NSDictionary *dictdata=(NSDictionary*)arrTemp;
                [arrResponseInspection addObject:dictdata];
                
            } else {
                
                arrResponseInspection =[[NSMutableArray alloc]init];
                [arrResponseInspection addObjectsFromArray:arrTemp];
                
            }
            
            [self dynamicFormCreation];
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)fetchServiceAutomationService{
    
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSArray *arrOfMAsterData=[defsNew objectForKey:@"MasterServiceAutomationDynamicForm"];
    
    NSMutableArray *arrOfDynamicFormData=[[NSMutableArray alloc]init];
    
    if (arrOfMAsterData.count==0) {
        
        [self addFrameIfNothingIsPresent];
        
        [self defaultViewSettingForAmount];
        
    } else {
        
        BOOL yesGotDynamicForm;
        yesGotDynamicForm=NO;
        
        //ServiceSysName Se Check Karna hai
        
        if (strPrimaryServiceSysName.length==0) {
            
            yesGotDynamicForm=NO;
            
        } else {
            
            for (int k=0; k<arrOfMAsterData.count; k++) {
                
                NSDictionary *dictData=arrOfMAsterData[k];
                
                NSArray *arrServiceSysNameMapping=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceSysNameMapping"]] componentsSeparatedByString:@","];
                
                if ([arrServiceSysNameMapping containsObject:strPrimaryServiceSysName]) {
                    
                    [arrOfDynamicFormData addObject:dictData];
                    yesGotDynamicForm=YES;
                    break;
                    
                }
            }
            
        }
        
        ////CatyegorySysName Se Check Karna hai
        
        if (!yesGotDynamicForm) {
            
            if (strCategorySysName.length==0) {
                
                yesGotDynamicForm=NO;
                
            } else {
                for (int k=0; k<arrOfMAsterData.count; k++) {
                    
                    NSDictionary *dictData=arrOfMAsterData[k];
                    
                    NSArray *arrCategorySysNameMapping=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysNameMapping"]] componentsSeparatedByString:@","];
                    
                    if ([arrCategorySysNameMapping containsObject:strCategorySysName]) {
                        
                        [arrOfDynamicFormData addObject:dictData];
                        yesGotDynamicForm=YES;
                        break;
                        
                    }
                }
            }
        }
        ////Department Id Se Check Karna hai  DepartmentIdsMapping
        
        if (!yesGotDynamicForm) {
            
            for (int k=0; k<arrOfMAsterData.count; k++) {
                
                NSDictionary *dictData=arrOfMAsterData[k];
                
                NSArray *arrDepartmentIdsMapping=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentIdsMapping"]] componentsSeparatedByString:@","];
                
                if ([arrDepartmentIdsMapping containsObject:strDepartmentId]) {
                    
                    [arrOfDynamicFormData addObject:dictData];
                    yesGotDynamicForm=YES;
                    break;
                    
                }
            }
        }
        
        
        ////Finally jo phle chal raha tha single department wala
        
        if (!yesGotDynamicForm) {
            
            for (int k=0; k<arrOfMAsterData.count; k++) {
                
                NSDictionary *dictData=arrOfMAsterData[k];
                
                NSString *strDepartmentIdIds=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
                
                if ([strDepartmentIdIds isEqualToString:strDepartmentId]) {
                    
                    [arrOfDynamicFormData addObject:dictData];
                    
                    break;
                }
            }
        }
    }
    
    if (arrOfDynamicFormData.count>0) {
        
        arrResponseInspection=[[NSMutableArray alloc]init];
        arrResponseInspection=arrOfDynamicFormData;
        [self saveWorkOrderIdDynamicForm];
        
        [self dynamicFormCreation];
        
        // [self saveToCoreDataServiceDynamic:arrResponseInspection];
        
    }else{
        
        [self dynamicFormCreation];
        
    }
    
    // [self getChemicalsListServiceAutomation];
    
    //if  [self defaultViewSettingForAmount];
    
}

-(void)serviceAutomationInspection
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [self FetchFromCoreDataToShowServiceDynamic];
        
    }
    else
    {
        
        [self FetchFromCoreDataToShowServiceDynamicInCaseOnline];
        
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)actionOnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionOnSave:(id)sender
{
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"])
    {
        
        [self goToServiceDetailView];
        
    }
    else
        
    {
        [self.view endEditing:YES];
        //START new change for email valid overall
        BOOL isEmailValid;
        isEmailValid=YES;
        NSString *errorMessageTemp;
        for (int k=0; k<arrResponseInspection.count; k++) {
            
            NSDictionary *dictData=arrResponseInspection[k];
            NSArray *arrSectionsTemp=[dictData valueForKey:@"Sections"];
            
            for (int j=0; j<arrSectionsTemp.count; j++) {
                
                NSDictionary *dictDataControls=arrSectionsTemp[j];
                NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
                
                for (int l=0; l<arrOfControls.count; l++) {
                    
                    NSDictionary *dictOfOptions=arrOfControls[l];
                    if ([[dictOfOptions valueForKey:@"Element"] isEqualToString:@"el-email"]) {
                        
                        NSString *strValueee=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
                        if (strValueee.length==0) {
                            
                            isEmailValid=YES;
                            
                        }else{
                            
                            strValueee =  [strValueee stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            errorMessageTemp=[self validationCheck :strValueee :@"el-email"];
                            
                            if (errorMessageTemp) {
                                
                                isEmailValid=NO;
                                
                            }else{
                                
                                isEmailValid=YES;
                                
                            }
                        }
                    }
                }
            }
        }
        
        //END new change for email valid overall
        
        if (isEmailValid) {
            
            [self saveToCoreDataServiceDynamicFinal:arrResponseInspection];
            if (yesEditedSomething) {
                
                NSLog(@"Yes Something Edited and Finaly Saved");
                
                [self updateModifyDate];
                [self updateZSyncWorkOrder];
                
            }
            
            [self goToServiceDetailView];
        } else {
            
            NSString *strTitle = Alert;
            [global AlertMethod:strTitle :@"Please enter valid email id only"];
            
        }
    }
}

-(void)dynamicFormCreation
{
    
    //        //Temporary HAi Comment KArna Yad SE
    //        arrResponseInspection=[[NSMutableArray alloc]init];
    //        NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    //        NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"DynamicFormJson.json"];
    //        NSError * error;
    //        NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    //        NSArray *arrOfCountry = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    //        [arrResponseInspection addObjectsFromArray:arrOfCountry];
    
    CGFloat scrollViewHeight=0.0;
    
    arrOfIndexes =[[NSMutableArray alloc]init];
    arrOfControlssMain =[[NSMutableArray alloc]init];
    int tagToSetForIndexes=0;
    
    for (int i=0; i<arrResponseInspection.count; i++)
    {
        NSDictionary *dictMain=[arrResponseInspection objectAtIndex:i];
        if (!MainViewForm.frame.size.height) {
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, 42)];
        }
        else{
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 42)];
        }
        BtnMainView.tag=i;
        // [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        //Nilind 23 Feb
        // [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentSysName"] forState:UIControlStateNormal];//dictDeptNameByKey
        NSString *strDept=[dictDeptNameByKey valueForKey:[dictMain valueForKey:@"DepartmentSysName"]];
        if ([strDept isEqual:nil]||[strDept isEqualToString:@""])
        {
            strDept=@"No Department Available";
        }
        [BtnMainView setTitle:strDept forState:UIControlStateNormal];
        //End
        
        BtnMainView.titleLabel.numberOfLines=20000;
        
        //  UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 40, 40)];
        //    imgView.image=[UIImage imageNamed:@"minus_icon.png"];
        
        //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
        //   [BtnMainView addSubview:imgView];
        
        [_scrollForDynamicView addSubview:BtnMainView];
        
        [arrayOfButtonsMain addObject:BtnMainView];
        //   [arrOfButtonImages addObject:imgView];
        
        // Form Creation
        MainViewForm=[[UIView alloc]init];
        
        MainViewForm.backgroundColor=[UIColor clearColor];
        
        MainViewForm.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 1000);
        
        MainViewForm.tag=i+3000;
        
        [_scrollForDynamicView addSubview:MainViewForm];
        
        NSArray *arrSections=[dictMain valueForKey:@"Sections"];
        
        int yAxisViewFormSections=0.0;
        int heightVIewFormSections=0.0;
        int heightMainViewFormSections=0.0;
        
        
        for (int j=0; j<arrSections.count; j++)
        {
            NSDictionary *dictSection=[arrSections objectAtIndex:j];
            NSArray *arrControls=[dictSection valueForKey:@"Controls"];
            
            // Section Creation Start
            heightVIewFormSections=0.0;
            ViewFormSections=[[UIView alloc]init];
            ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width, 400);
            
            ViewFormSections.backgroundColor=[UIColor whiteColor];
            UILabel *lblTitleSection=[[UILabel alloc]init];
            lblTitleSection.backgroundColor=[UIColor lightGrayColor];
            lblTitleSection.layer.borderWidth = 1.5f;
            lblTitleSection.layer.cornerRadius = 4.0f;
            lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
            
            lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30);
            lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]];
            //lblTitleSection.text=@"asdfsfsadfsadfsadfsadjfhsadjfhsadjhfjksdhfjkhsadjkfhsakjdfhasjkdhfsajdfhjsdkhfsdjfhjsdfhjksdfhkjdsafhjkasdhfkjsadhfjksahfkjsadhfkjsdahfkjsadhfjkasdhfuk";
            lblTitleSection.textAlignment=NSTextAlignmentCenter;
            lblTitleSection.numberOfLines=20000;
            [lblTitleSection setAdjustsFontSizeToFitWidth:YES];
            [ViewFormSections addSubview:lblTitleSection];
            
            ViewFormSections.layer.borderWidth = 1.5f;
            ViewFormSections.layer.cornerRadius = 4.0f;
            ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
            
            [MainViewForm addSubview:ViewFormSections];
            
            CGFloat xPositionOfControls= 0.0;
            CGFloat yPositionOfControls = 0.0;
            CGFloat heightOfControls= 0.0;
            CGFloat widthOfControls= 0.0;
            
            for (int k=0; k<arrControls.count; k++)
            {
                NSDictionary *dictControls=[arrControls objectAtIndex:k];
                UILabel *lblTitleControl=[[UILabel alloc]init];
                lblTitleControl.backgroundColor=[UIColor lightGrayColor];
                lblTitleControl.frame=CGRectMake(0, 30*k+35, [UIScreen mainScreen].bounds.size.width, 30);
                lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
                
                lblTitleControl.textAlignment=NSTextAlignmentCenter;
                // [ViewFormSections addSubview:lblTitleControl];
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - CheckBox-Combo
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"]) {
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSArray *arrOfIndexChecked;
                    if (!(strIndexOfValue.length==0)) {
                        arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    }
                    
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    
                    if (s.height<30) {
                        s.height=30;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+25,25,25);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    NSMutableArray *arrOfValuesToCheck=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        [arrOfValuesToCheck addObject:[dictData valueForKey:@"Value"]];
                        
                    }
                    int btnCount=[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        btn.titleLabel.textColor = [UIColor clearColor];
                        [btn setTitle:title forState:UIControlStateNormal];
                        
                        [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                        
                        for (int s=0; s<arrOfIndexChecked.count; s++) {
                            
                            NSString *strValueTocompare=[NSString stringWithFormat:@"%@",arrOfIndexChecked[s]];
                            if ([[arrOfValuesToCheck objectAtIndex:l] isEqualToString:strValueTocompare]) {
                                [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                            }
                            
                        }
                        
                        [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 50)];
                        [view addSubview:btn];
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 45);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                            [btnHidden setEnabled:NO];
                        } else {
                            [btnHidden setEnabled:YES];
                        }
                        
                        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                            [btn setEnabled:NO];
                        } else {
                            [btn setEnabled:YES];
                        }
                        
                        [arrOfControlssMain addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice]-50, MAXFLOAT)];
                        
                        if (lblItemHeight.height<40) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y-8, [global globalWidthDevice]-50, 40);
                            
                        } else {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y-8, [global globalWidthDevice]-50, lblItemHeight.height);
                            
                        }
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:200000];
                        lblItem.text =title;
                        [view addSubview:lblItem];
                        
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                    }
                    
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - CheckBox
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"]) {
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSArray *arrOfIndexChecked;
                    if (!(strIndexOfValue.length==0)) {
                        arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    }
                    
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    
                    if (s.height<30) {
                        s.height=30;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+25,25,25);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    NSMutableArray *arrOfValuesToCheck=[[NSMutableArray alloc]init];
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        [arrOfValuesToCheck addObject:[dictData valueForKey:@"Value"]];
                        
                    }
                    int btnCount=[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        // NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        btn.titleLabel.textColor = [UIColor clearColor];
                        [btn setTitle:title forState:UIControlStateNormal];
                        
                        [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                        
                        for (int s=0; s<arrOfIndexChecked.count; s++) {
                            
                            NSString *strValueTocompare=[NSString stringWithFormat:@"%@",arrOfIndexChecked[s]];
                            if ([[arrOfValuesToCheck objectAtIndex:l] isEqualToString:strValueTocompare]) {
                                [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                            }
                        }
                        
                        [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 50)];
                        [view addSubview:btn];
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 45);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                            [btnHidden setEnabled:NO];
                        } else {
                            [btnHidden setEnabled:YES];
                        }
                        
                        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                            [btn setEnabled:NO];
                        } else {
                            [btn setEnabled:YES];
                        }
                        
                        [arrOfControlssMain addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice]-50, MAXFLOAT)];
                        
                        if (lblItemHeight.height<40) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y-8, [global globalWidthDevice]-50, 40);
                            
                        } else {
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y-8, [global globalWidthDevice]-50, lblItemHeight.height);
                            
                        }
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:200000];
                        lblItem.text =title;
                        [view addSubview:lblItem];
                        
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                    }
                    //Nilind 28 Feb
                    /*
                     if(btnCount==0)
                     {
                     UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                     btn.frame = btnFrame;
                     NSString *title = lbl.text;//[arrOfValues objectAtIndex:0];
                     
                     // NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,0];
                     NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,0];
                     
                     [arrOfIndexes addObject:strTaggg];
                     
                     
                     
                     btn.tag=tagToSetForIndexes;
                     
                     tagToSetForIndexes++;
                     
                     btn.titleLabel.textColor = [UIColor clearColor];
                     [btn setTitle:title forState:UIControlStateNormal];
                     
                     [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                     
                     for (int s=0; s<arrOfIndexChecked.count; s++) {
                     
                     NSString *strValueTocompare=[NSString stringWithFormat:@"%@",arrOfIndexChecked[s]];
                     if ([title isEqualToString:strValueTocompare]) {
                     [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                     }
                     }
                     
                     [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                     
                     // btnFrame.origin.y+=btnFrame.size.height+15;
                     [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 50)];
                     [view addSubview:btn];
                     
                     if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                     [btn setEnabled:NO];
                     } else {
                     [btn setEnabled:YES];
                     }
                     
                     [arrOfControlssMain addObject:btn];
                     
                     UILabel *lblItem = [[UILabel alloc] init];
                     
                     CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                     
                     if (lblItemHeight.height<40) {
                     
                     lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, 250, 40);
                     
                     } else {
                     lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, 250, lblItemHeight.height);
                     
                     }
                     [lblItem setFont:[UIFont systemFontOfSize:15]];
                     lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                     [lblItem setNumberOfLines:200000];
                     lblItem.text =title;
                     [view addSubview:lblItem];
                     
                     btnFrame.origin.y+=lblItem.frame.size.height+5;
                     
                     int tempheight=lblItem.frame.size.height;
                     
                     viewHeight =viewHeight+tempheight;
                     }
                     */
                    //End
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Radio---Combo
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                //el-radio-combo
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"]){
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    if (s.height<30) {
                        s.height=30;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+25,25,25);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    NSMutableArray *arrOfValuesToCheck=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        [arrOfValuesToCheck addObject:[dictData valueForKey:@"Value"]];
                        
                    }
                    int btnCount=(int)[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    NSMutableArray *arrOfTempRadioBtns=[[NSMutableArray alloc]init];
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        [btn setTitle:@"RadioBtnnComboooo" forState:UIControlStateNormal];
                        btn.titleLabel.textColor = [UIColor clearColor];
                        
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        /*
                         int indexToCheck=-1;
                         
                         if(!(strIndexOfValue.length==0)){
                         
                         indexToCheck =[strIndexOfValue intValue];
                         
                         }
                         
                         if (indexToCheck==l) {
                         
                         [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                         
                         }else{
                         
                         [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                         
                         }
                         */
                        
                        
                        if(!(strIndexOfValue.length==0)){
                            
                            if ([strIndexOfValue isEqualToString:[NSString stringWithFormat:@"%@",arrOfValuesToCheck[l]]]) {
                                
                                [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                                
                            }else{
                                
                                [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                                
                            }
                        }else{
                            
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                            
                        }
                        
                        
                        
                        [btn addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                        
                        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                            [btn setEnabled:NO];
                        } else {
                            [btn setEnabled:YES];
                        }
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 50)];
                        [view addSubview:btn];
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 45);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                            [btnHidden setEnabled:NO];
                        } else {
                            [btnHidden setEnabled:YES];
                        }
                        
                        [arrOfTempRadioBtns addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice]-50, MAXFLOAT)];
                        
                        if (lblItemHeight.height<40) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y-8, [global globalWidthDevice]-50, 40);
                            
                        } else {
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y-8, [global globalWidthDevice]-50, lblItemHeight.height);
                            
                        }
                        
                        //lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, 250, 100);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:200000];
                        lblItem.text =title;
                        [view addSubview:lblItem];
                        
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                    }
                    
                    for (int l=0; l<btnCount; l++){
                        
                        [arrOfControlssMain addObject:arrOfTempRadioBtns];
                        
                    }
                    
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                /*
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 #pragma mark - Radio Button
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 
                 
                 
                 else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"]){
                 
                 //**********Add View At which radio buttons and Tiltle Label will genarate **********
                 if (!yPositionOfControls) {
                 yPositionOfControls=40;
                 }
                 
                 NSString *strValueOfRadio=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                 
                 CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);;
                 UIView *view = [[UIView alloc] init];
                 [view setFrame:buttonFrame];
                 CALayer *bottomBorder = [CALayer layer];
                 [view.layer addSublayer:bottomBorder];
                 view.backgroundColor=[UIColor whiteColor];
                 CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(308, MAXFLOAT)];
                 UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 255, s.height)];
                 lbl.text = [dictControls valueForKey:@"Label"];
                 [lbl setAdjustsFontSizeToFitWidth:YES];
                 [lbl setFont:[UIFont systemFontOfSize:15]];
                 [lbl setNumberOfLines:9];
                 [view addSubview:lbl];
                 
                 CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,30,30);
                 
                 NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                 int indexToSetValue=-1;
                 NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                 for (int k=0; k<arrOptions.count; k++) {
                 
                 NSDictionary *dictData=arrOptions[k];
                 
                 [arrOfValues addObject:[dictData valueForKey:@"Value"]];
                 if ([strValueOfRadio isEqualToString:[dictData valueForKey:@"Value"]]) {
                 indexToSetValue=k;
                 }
                 
                 }
                 
                 int btnCount=[arrOfValues count];
                 
                 float viewHeight = btnCount*42+lbl.frame.size.height+20;
                 for (int l=0; l<btnCount; l++)
                 {
                 UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(15, 10, 255, 40)];
                 btn.frame = btnFrame;
                 NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                 [arrOfIndexes addObject:strTaggg];
                 btn.tag=tagToSetForIndexes;
                 tagToSetForIndexes++;
                 
                 if (l==0) {
                 
                 [btn setTitle:@"firstRadioBtnn" forState:UIControlStateNormal];
                 btn.titleLabel.textColor = [UIColor clearColor];
                 
                 }
                 
                 [btn addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                 [view addSubview:btn];
                 
                 if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                 [btn setEnabled:NO];
                 } else {
                 [btn setEnabled:YES];
                 }
                 
                 UILabel *lblYes = [[UILabel alloc] init];
                 lblYes.frame = CGRectMake(btn.frame.origin.x+50, btn.frame.origin.y+5, 200, 100);
                 lblYes.text = [arrOfValues objectAtIndex:l];
                 NSString *str=lblYes.text;
                 [lblYes setNumberOfLines:2];
                 lblYes.center=CGPointMake(lblYes.center.x, btn.center.y);
                 
                 [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                 if (indexToSetValue==-1) {
                 
                 }else if (indexToSetValue==k){
                 
                 [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                 }
                 
                 [arrOfControlssMain addObject:btn];
                 
                 [lblYes setAdjustsFontSizeToFitWidth:YES];
                 [lblYes setTag:btn.tag+200];
                 [view addSubview:lblYes];
                 [lblYes setFont:[UIFont systemFontOfSize:15]];
                 btnFrame.origin.y+=btnFrame.size.height+12; //it changes Y exix of radio button
                 }
                 
                 [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight+20)];
                 
                 heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                 yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                 
                 [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                 
                 [ViewFormSections addSubview:view];
                 
                 }
                 */
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - TextArea i.e TextViewww
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],80);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextView *txtView= [[UITextView alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    [view addSubview:txtView];
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEditable:NO];
                    } else {
                        [txtView setEditable:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Paragraph i.e paragraph
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-paragraph"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],80);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextView *txtView= [[UITextView alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.editable=NO;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    [view addSubview:txtView];
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEditable:NO];
                    } else {
                        [txtView setEditable:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-textbox
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - TextBox i.e TextField
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textbox"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - EmailId Field
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-email"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeEmailAddress;
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Phone Number Field
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-phone"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-currency
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Currency
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-currency"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Decimal
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-decimal"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeDecimalPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
#pragma mark - Number
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-number"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-auto-num  el-precent el-url el-upload
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-auto-num
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-auto-num"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-precent
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                //   else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-percent"]){
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-percent"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-url
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-url"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    //                        CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-30,35);
                    //                        float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    //                        UITextField *txtView= [[UITextField alloc] init];
                    //                        txtView.text = [dictControls valueForKey:@"Value"];
                    //                        txtView.frame =txtViewFrame;
                    //                        txtView.keyboardType=UIKeyboardTypeWebSearch;
                    //
                    //                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    //
                    //                        [arrOfIndexes addObject:strTaggg];
                    //
                    //                        txtView.tag=tagToSetForIndexes;
                    //                        tagToSetForIndexes++;
                    //
                    //                        //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    //                        // txtView.layer.borderWidth = 0.5f;
                    //                        txtView.layer.borderWidth = 1.5f;
                    //                        txtView.layer.cornerRadius = 4.0f;
                    //                        txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    //                        [txtView setBackgroundColor:[UIColor clearColor]];
                    //                        txtView.delegate=self;
                    //
                    //                        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    //                        leftView.backgroundColor = [UIColor clearColor];
                    //
                    //
                    //                        txtView.leftView = leftView;
                    //
                    //                        txtView.leftViewMode = UITextFieldViewModeAlways;
                    //                        txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    //
                    //                        [view addSubview:txtView];
                    //
                    //                        [arrOfControlssMain addObject:txtView];
                    
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-30, 40)];
                    
                    float viewHeight = lbl.frame.size.height+btnDropDown.frame.size.height+30;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(oPenURLonWeb:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                    
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    //[dictControls valueForKey:@"Value"]
                    
                    [view addSubview:btnDropDown];
                    
                    UILabel *lblUnderLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 33, btnDropDown.frame.size.width, 1)];
                    
                    lblUnderLine.backgroundColor=[UIColor blueColor];
                    
                    //  [btnDropDown addSubview:lblUnderLine];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-upload
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-upload"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],35);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [txtView setEnabled:NO];
                    } else {
                        [txtView setEnabled:YES];
                    }
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Date Picker
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-date"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 40)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(oPenDatePicker:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [btnDropDown setEnabled:NO];
                    } else {
                        [btnDropDown setEnabled:YES];
                    }
                    
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Date Time Picker
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-date-time"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 40)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(oPenDateTimePicker:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [btnDropDown setEnabled:NO];
                    } else {
                        [btnDropDown setEnabled:YES];
                    }
                    
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - DropDown
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]){
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 40)];
                    
                    UIButton *ImgDropDown = [[UIButton alloc] initWithFrame:CGRectMake(btnDropDown.frame.size.width-40,0, 40, 40)];
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    ImgDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(oPenDropDown:) forControlEvents:UIControlEventTouchDown];
                    [ImgDropDown addTarget:self action:@selector(oPenDropDown:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor clearColor] CGColor];
                    
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [btnDropDown setEnabled:NO];
                        [ImgDropDown setEnabled:NO];
                    } else {
                        [btnDropDown setEnabled:YES];
                        [ImgDropDown setEnabled:YES];
                    }
                    
                    btnDropDown.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
                    //drop_down1.png
                    [btnDropDown setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    //15 March
                    
                    NSArray *arrTemp=[dictControls valueForKey:@"Options"];
                    NSString *strTempValueToCheck=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSString *strNameToSet;
                    
                    for (int q=0; q<arrTemp.count; q++) {
                        
                        NSDictionary *dictDataTemp=arrTemp[q];
                        
                        NSString *strTemp=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"Value"]];
                        
                        if ([strTemp isEqualToString:strTempValueToCheck]) {
                            
                            strNameToSet=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"Name"]];;
                            
                        }
                        
                    }
                    [btnDropDown setTitle:strNameToSet forState:UIControlStateNormal];
                    
                    //End 15 March
                    
                    //  [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [ImgDropDown setImage:[UIImage imageNamed:@"drop_down1.png"] forState:UIControlStateNormal];
                    [btnDropDown addSubview:ImgDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //el-multi-select
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Multiple Select
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    //Nilind
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    //lbl.backgroundColor=[UIColor redColor];
                    [lbl setFont:[UIFont systemFontOfSize:15]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    //End
                    
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 40)];
                    
                    UIButton *ImgDropDown = [[UIButton alloc] initWithFrame:CGRectMake(btnDropDown.frame.size.width-40,0, 40, 40)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    ImgDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(multipleSelect:) forControlEvents:UIControlEventTouchDown];
                    [ImgDropDown addTarget:self action:@selector(multipleSelect:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.backgroundColor=[UIColor lightGrayColor];
                    btnDropDown.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
                    //drop_down1.png
                    [btnDropDown setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                    NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                    NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOfValues.count; k++) {
                        
                        for (int l=0; l<arrOfOptions.count; l++) {
                            
                            NSDictionary *dictData=arrOfOptions[l];
                            NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                            
                            if ([strValue isEqualToString:arrOfValues[k]]) {
                                
                                [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                
                            }
                            
                        }
                        
                    }
                    NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@","];
                    
                    [btnDropDown setTitle:strTextView forState:UIControlStateNormal];
                    
                    
                    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
                        [btnDropDown setEnabled:NO];
                        [ImgDropDown setEnabled:NO];
                    } else {
                        [btnDropDown setEnabled:YES];
                        [ImgDropDown setEnabled:YES];
                    }
                    
                    [view addSubview:btnDropDown];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    [ImgDropDown setImage:[UIImage imageNamed:@"drop_down1.png"] forState:UIControlStateNormal];
                    [btnDropDown addSubview:ImgDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
            }
            
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
#pragma mark - Final Size scroll and view and all
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            
            [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrControls.count*10+50)];
            
            heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrControls.count*10+50;
            
            MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width, heightMainViewFormSections+60);
            
            yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+10;
            
        }
        
        scrollViewHeight=scrollViewHeight+MainViewForm.frame.size.height;
        [arrayViews addObject:MainViewForm];
        
        CGFloat heightTemp=MainViewForm.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewForm.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        
        
        [arrOfHeightsViews addObject:strTempHeight];
        [arrOfYAxisViews addObject:strTempYAxis];
        
    }
    
    //MainViewForm.backgroundColor = UIColor.greenColor;
    
    _scrollForDynamicView.backgroundColor=[UIColor clearColor];
    
    _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+100);
    
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,MainViewForm.frame.origin.y+MainViewForm.frame.size.height+100)];
    
    //[self addEquipmentView];
    
    /*
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        BOOL isNoChemicalList=[defs boolForKey:@"isNoChemical"];
        
        if (isNoChemicalList) {
            
            [self noChemicalListMethod];
            [self defaultViewSettingForAmount];
            
        } else {
            
            [self getChemicalsListServiceAutomation];
            
        }
        
    }
    else
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        BOOL isNoChemicalList=[defs boolForKey:@"isNoChemical"];
        
        if (isNoChemicalList) {
            
            [self noChemicalListMethod];
            [self defaultViewSettingForAmount];
            
        } else {
            
            [self getChemicalsListServiceAutomation];
            
        }
    }
     
     */
}

//============================================================================
//============================================================================
#pragma mark- Single Tap Methods
//============================================================================
//============================================================================
-(void)tapDetectedOnBackGroundView{
    [viewBackGround removeFromSuperview];
}

//============================================================================
//============================================================================
#pragma mark- ------------Radio Button Methods--------------
//============================================================================
//============================================================================
-(void)checkRadioButton:(id)sender{
    
    [self HideALlTextFieldsOpened];
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=btn.tag;
    
    
    UIButton *btnRadiButton=arrOfControlssMain[taggg];
    
    
    [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    
    if ([arrOfControlssMain[taggg-1] isKindOfClass:[UIButton class]]) {
        
        UIButton *btnlast=arrOfControlssMain[taggg-1];
        
        if ([btnlast.currentTitle isEqualToString:@"firstRadioBtnn"]) {
            
            [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            
        }else{
            
            UIButton *btnlast=arrOfControlssMain[taggg+1];
            
            [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            
        }
        
    }else{
        
        UIButton *btnlast=arrOfControlssMain[taggg+1];
        
        [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
    //firstRadioBtnn
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    [self commonMethodToSaveValue:strValue :taggg];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Radio Button Combooo Methods--------------
//============================================================================
//============================================================================

-(void)checkRadioButtonCombo:(id)sender{
    
    BOOL isValueToSet;
    isValueToSet=NO;
    
    [self HideALlTextFieldsOpened];
    
    //RadioBtnnComboooo
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    
    int taggToCheckTemp=0;
    
    NSArray *arrOfRadioBtnsTemp = arrOfControlssMain[taggg];
    
    for (int k=0; k<arrOfRadioBtnsTemp.count; k++) {
        
        UIButton *btnRadiButton=arrOfRadioBtnsTemp[k];
        if (btnRadiButton.tag==taggg) {
            taggToCheckTemp=k;
            
            
            if ([btnRadiButton.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
                
                [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                isValueToSet=NO;
            } else {
                
                [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                isValueToSet=YES;
            }
            
            
            //  [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            
        }
    }
    
    int taggToCheck=taggToCheckTemp-1;
    
    for (int s=taggToCheck;s>=0; s--) {
        
        if ([arrOfRadioBtnsTemp[s] isKindOfClass:[UIButton class]]) {
            
            UIButton *btnlast=arrOfRadioBtnsTemp[s];
            
            if ([btnlast.currentTitle isEqualToString:@"RadioBtnnComboooo"]) {
                
                [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
            }
        }
        else
            break;
    }
    
    taggToCheck=taggToCheckTemp+1;
    
    for (int s=taggToCheck;s<arrOfRadioBtnsTemp.count; s++) {
        
        if ([arrOfRadioBtnsTemp[s] isKindOfClass:[UIButton class]]) {
            
            UIButton *btnlast=arrOfRadioBtnsTemp[s];
            
            if ([btnlast.currentTitle isEqualToString:@"RadioBtnnComboooo"]) {
                
                [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
            }
        }
        else
            break;
    }
    
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    if (!isValueToSet) {
        strValue=@"";
    }
    
    [self commonMethodToSaveValue:strValue :taggg];
    
    
    //firstRadioBtnn
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Check Box Button Methods--------------
//============================================================================
//============================================================================
-(void)checkCheckBoxes:(id)sender{
    
    [self HideALlTextFieldsOpened];
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=btn.tag;
    
    UIButton *btnCheckBox=arrOfControlssMain[taggg];
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"uncheck.png"];
    
    UIImage *img = [btnCheckBox imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        [btnCheckBox setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        
        [btnCheckBox setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    //Nilind 28 Feb
    /*  NSString *strAlreadySavedValue,*strValue;
     if(arrDataOptionss.count==0)
     {
     strAlreadySavedValue=[dictOfOptions valueForKey:@"Value"];
     strValue=strAlreadySavedValue;
     }
     else
     {
     strAlreadySavedValue=[dictOfOptions valueForKey:@"Value"];
     
     NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
     
     strValue=[dictOfValues valueForKey:@"Value"];
     }*/
    //End
    NSString *strAlreadySavedValue=[dictOfOptions valueForKey:@"Value"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    NSString *strFinalValue;
    
    if (strAlreadySavedValue.length==0) {
        
        strFinalValue=strValue;
        
    }
    else
    {
        
        NSArray *arrOfValue=[strAlreadySavedValue componentsSeparatedByString:@","];
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        [arrTemp addObjectsFromArray:arrOfValue];
        
        
        if ([arrTemp containsObject:strValue]) {
            
            [arrTemp removeObject:strValue];
            
        }
        else{
            
            [arrTemp addObject:strValue];
            
        }
        
        arrOfValue = arrTemp;
        
        strFinalValue=[arrOfValue componentsJoinedByString:@","];
        
    }
    
    [self commonMethodToSaveValue:strFinalValue :taggg];
    
}


//============================================================================
//============================================================================
#pragma mark- ------------URL OPEN ON WEB Methods--------------
//============================================================================
//============================================================================
-(void)oPenURLonWeb:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSString *strURL=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (strURL.length>0) {
        
        [self openLinkNew:strURL];
        
    }else{
        [global AlertMethod:@"Info" :@"No Link Available"];
        // [self openLinkNew:@"http://www.citizencop.org/"];
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Open In Safari Methods--------------
//============================================================================
//============================================================================

- (void)openLinkNew:(NSString *)url {
    NSURL *URL = [NSURL URLWithString:url];
    
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = (id)self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:URL]) {
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Date Picker Methods--------------
//============================================================================
//============================================================================

-(void)oPenDatePicker:(id)sender{
    
    [self HideALlTextFieldsOpened];
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    strGlobalDateToShow=[dictOfOptions valueForKey:@"Value"];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self addPickerViewDateTo : taggggs];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateTo:(int)intTagg
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //  [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strGlobalDateToShow.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDateToShow];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    btnDone.tag=intTagg;
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    UIButton *btn=(UIButton*)sender;
    int taggggs=btn.tag;
    
    if (taggggs==-7) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
        
        BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
        if (yesSameDay) {
            [_btnCheckExpDate setTitle:strDate forState:UIControlStateNormal];
            strGlobalDateToShow=strDate;
            [viewForDate removeFromSuperview];
            [viewBackGround removeFromSuperview];
        }
        else
        {
            
            NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
            
            if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending)
            {
                
                [global AlertMethod:@"Alert" :@"Back dates can't be selected"];
                
            }
            else
            {
                [_btnCheckExpDate setTitle:strDate forState:UIControlStateNormal];
                strGlobalDateToShow=strDate;
                [viewForDate removeFromSuperview];
                [viewBackGround removeFromSuperview];
            }
        }
        
    } else
    {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
        
        BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
        if (yesSameDay) {
            
            UIButton *btnDropdown=arrOfControlssMain[taggggs];
            
            [btnDropdown setTitle:strDate forState:UIControlStateNormal];
            
            [self commonMethodToSaveValue:strDate :taggggs];
            strGlobalDateToShow=strDate;
            [viewForDate removeFromSuperview];
            [viewBackGround removeFromSuperview];
            
        }
        else{
            
            NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
            
            if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
                
                [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
                
            }else{
                
                UIButton *btnDropdown=arrOfControlssMain[taggggs];
                
                [btnDropdown setTitle:strDate forState:UIControlStateNormal];
                strGlobalDateToShow=strDate;
                [self commonMethodToSaveValue:strDate :taggggs];
                
                [viewForDate removeFromSuperview];
                [viewBackGround removeFromSuperview];
                
            }
        }
    }
}
//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

-(void)HideALlTextFieldsOpened{
    
    for (int k=0; k<arrOfControlssMain.count; k++) {
        if ([[arrOfControlssMain objectAtIndex:k] isKindOfClass:[UITextField class]]) {
            
            UITextField *textFeild=arrOfControlssMain[k];
            [textFeild resignFirstResponder];
            
        }
        if ([[arrOfControlssMain objectAtIndex:k] isKindOfClass:[UITextView class]]) {
            
            UITextView *textVieww=arrOfControlssMain[k];
            [textVieww resignFirstResponder];
            
        }
    }
    
}
//============================================================================
//============================================================================
#pragma mark- ------------Time Picker Methods--------------
//============================================================================
//============================================================================

-(void)oPenDateTimePicker:(id)sender{
    
    [self HideALlTextFieldsOpened];
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    strGlobalDatenTime=[dictOfOptions valueForKey:@"Value"];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self addDAteTimePickerView : taggggs];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Time PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addDAteTimePickerView:(int)intTagg
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //  [pickerDate setMinimumDate:[NSDate date]];
    
    
    if (!(strGlobalDatenTime.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:MM:SS"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDatenTime];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDateAndTime;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    btnDone.tag=intTagg;
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateTimeOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)setDateTimeOnDone:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    int taggggs=btn.tag;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:MM:SS"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        
        UIButton *btnDropdown=arrOfControlssMain[taggggs];
        
        [btnDropdown setTitle:strDate forState:UIControlStateNormal];
        strGlobalDatenTime=strDate;
        [self commonMethodToSaveValue:strDate :taggggs];
        
        [viewForDate removeFromSuperview];
        [viewBackGround removeFromSuperview];
        
    }
    else{
        
        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
        
        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
            
            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
            
        }else{
            
            UIButton *btnDropdown=arrOfControlssMain[taggggs];
            
            [btnDropdown setTitle:strDate forState:UIControlStateNormal];
            strGlobalDatenTime=strDate;
            [self commonMethodToSaveValue:strDate :taggggs];
            
            [viewForDate removeFromSuperview];
            [viewBackGround removeFromSuperview];
            
        }
        
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Drop Down Methods--------------
//============================================================================
//============================================================================

-(void)oPenDropDown:(id)sender{
    
    [self HideALlTextFieldsOpened];
    
    isMultipleSelect=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSArray *arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    [arrDataTblView addObjectsFromArray:arrDataOptionss];
    
    strGlobalDropDownSelectedValue=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=taggg;
        [self tableLoad:taggg];
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Drop Down Methods--------------
//============================================================================
//============================================================================

-(void)multipleSelect:(id)sender{
    
    [self HideALlTextFieldsOpened];
    
    isUnit=NO;
    isTarget=NO;
    isAppMethodInfo=NO;
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSArray *arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    [arrDataTblView addObjectsFromArray:arrDataOptionss];
    
    
    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (!(strIndexOfValue.length==0)) {
        
        NSArray *arrayvalus=[strIndexOfValue componentsSeparatedByString:@","];
        
        for (int i=0; i<[arrayvalus count]; i++)
        {
            [arraySelecteValues addObject:arrayvalus[i]];
            // [arraySelecteValuesToShow addObject:arrayvalus[i]];
        }
        
    }
    
    NSArray *arrOfOptions=[dictOfOptions valueForKey:@"Options"];
    NSArray *arrOfValues=[[dictOfOptions valueForKey:@"Value"] componentsSeparatedByString:@","];
    NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfValues.count; k++) {
        
        for (int l=0; l<arrOfOptions.count; l++) {
            
            NSDictionary *dictData=arrOfOptions[l];
            NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
            
            if ([strValue isEqualToString:arrOfValues[k]]) {
                
                [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                
            }
            
        }
        
    }
    // NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@","];
    
    [arraySelecteValuesToShow addObjectsFromArray:arrOfValuesToSet];
    
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=taggg;
        isMultipleSelect=YES;
        [self tableLoad:taggg];
    }
    
    
}

-(void)tableLoad:(NSInteger)btntag
{
    
    //    NSInteger i;
    //    i=btntag;
    //    switch (i)
    //    {
    //        case 101:
    //        {
    //            [self setTableFrame];
    //            break;
    //        }
    //        case 102:
    //        {
    //            [self setTableFrame];
    //            break;
    //        }
    //
    //        default:
    //            break;
    //    }
    
    NSInteger i;
    i=btntag;
    
    if (i==1874919421) {
        
        [self setTableFrameForServiceJobDescription];
        
    } else {
        
        [self setTableFrame];
        
    }
    
    
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}
-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    //    singleTap1.numberOfTapsRequired = 1;
    //    [viewBackGround setUserInteractionEnabled:YES];
    //    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-10, 42)];
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(btnDoneAction:) forControlEvents:UIControlEventTouchDown];
    
    
    UIButton *btnCancel=[[UIButton alloc]initWithFrame:CGRectMake(btnDone.frame.origin.x+btnDone.frame.size.width+15, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-10, 42)];
    
    [btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCancel.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnCancel addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchDown];
    
    
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    [viewBackGround addSubview:btnDone];
    [viewBackGround addSubview:btnCancel];
    
    
}


-(void)setTableFrameForServiceJobDescription
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    //    singleTap1.numberOfTapsRequired = 1;
    //    [viewBackGround setUserInteractionEnabled:YES];
    //    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-10, 42)];
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(btnDoneActionServiceJobDescription:) forControlEvents:UIControlEventTouchDown];
    
    
    UIButton *btnCancel=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width, 42)];
    
    [btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCancel.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnCancel addTarget:self action:@selector(btnCancelActionServiceJobDescription:) forControlEvents:UIControlEventTouchDown];
    
    
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    // [viewBackGround addSubview:btnDone];
    [viewBackGround addSubview:btnCancel];
    tblData.tag=1874919421;
    [tblData reloadData];
    tblData.tag=1874919421;
    NSLog(@"Data====%@",arrPickerServiceJobDescriptions);
    
}

-(void)btnDoneActionServiceJobDescription :(id)sender{
    
    
    
}

-(void)btnCancelActionServiceJobDescription :(id)sender{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
}
//============================================================================
//============================================================================
#pragma mark- ------------------------------------------------------------------------------------------------------------Button Done Action Methods--------------
//============================================================================
//============================================================================

-(void)btnDoneAction:(id)sender{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    if (isMultipleSelect) {
        
        if (!(indexMultipleSelectTosetValue==-100000)) {
            
            UIButton *btnDropdown=arrOfControlssMain[indexMultipleSelectTosetValue];
            
            NSString *joinedComponents = [arraySelecteValues componentsJoinedByString:@","];
            NSString *joinedComponentsToShow = [arraySelecteValuesToShow componentsJoinedByString:@","];
            if (!(joinedComponents.length==0)) {
                
                [btnDropdown setTitle:joinedComponentsToShow forState:UIControlStateNormal];
                [self commonMethodToSaveValue:joinedComponents :indexMultipleSelectTosetValue];
                
            } else {
                [btnDropdown setTitle:@"" forState:UIControlStateNormal];
                [self commonMethodToSaveValue:joinedComponents :indexMultipleSelectTosetValue];
                
            }
        }
    }
    if (isAppMethodInfo) {
        
        if (!(indexMultipleSelectTosetValue==-100000)) {
            UIButton *btnDropdown=arrayOfAppMethodInfo[indexMultipleSelectTosetValue];
            
            NSString *strTempp=[arraySelecteValues componentsJoinedByString:@","];
            NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)indexMultipleSelectTosetValue+40000,strTempp];
            NSMutableArray *strMethodNameToShowFinally=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrDataTblView.count; k++) {
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:k];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
                if ([arraySelecteValues containsObject:str]) {
                    [strMethodNameToShowFinally addObject:[dictData valueForKey:@"ApplicationMethodName"]];
                    //ApplicationMethodName
                }
            }
            
            NSString *strFinalName=[strMethodNameToShowFinally componentsJoinedByString:@","];
            if (strFinalName.length==0) {
                strFinalName=@"Select Method";
            }
            if (arrAppMethodInfoId.count>indexMultipleSelectTosetValue) {
                
                //  NSString *strNewJugad=[NSString stringWithFormat:@"%@,%@",arrAppMethodInfoId[btnIndex],[dicData valueForKey:@"ApplicationMethodId"]];
                [arrAppMethodInfoId replaceObjectAtIndex:indexMultipleSelectTosetValue withObject:strJugad];
                
                [btnDropdown setTitle:strFinalName forState:UIControlStateNormal];
                
            }else{
                [arrAppMethodInfoId addObject:strJugad];
                
                [btnDropdown setTitle:strFinalName forState:UIControlStateNormal];
                
                
            }
        }
        
    }
    if (isAppMethodInfoOther) {
        
        if (!(indexMultipleSelectTosetValue==-100000)) {
            UIButton *btnDropdown=arrayOfAppMethodInfoOther[indexMultipleSelectTosetValue];
            
            NSString *strTempp=[arraySelecteValues componentsJoinedByString:@","];
            NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)indexMultipleSelectTosetValue+400000,strTempp];
            NSMutableArray *strMethodNameToShowFinally=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrDataTblView.count; k++) {
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:k];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
                if ([arraySelecteValues containsObject:str]) {
                    [strMethodNameToShowFinally addObject:[dictData valueForKey:@"ApplicationMethodName"]];
                    //ApplicationMethodName
                }
            }
            
            NSString *strFinalName=[strMethodNameToShowFinally componentsJoinedByString:@","];
            if (strFinalName.length==0) {
                strFinalName=@"Select Method";
            }
            if (arrAppMethodInfoIdOther.count>indexMultipleSelectTosetValue) {
                
                //  NSString *strNewJugad=[NSString stringWithFormat:@"%@,%@",arrAppMethodInfoId[btnIndex],[dicData valueForKey:@"ApplicationMethodId"]];
                [arrAppMethodInfoIdOther replaceObjectAtIndex:indexMultipleSelectTosetValue withObject:strJugad];
                
                [btnDropdown setTitle:strFinalName forState:UIControlStateNormal];
                
            }else{
                [arrAppMethodInfoIdOther addObject:strJugad];
                
                [btnDropdown setTitle:strFinalName forState:UIControlStateNormal];
                
                
            }
        }
    }
    
    
    if (isAppMethodInfoOtherMain) {
        
        NSMutableArray *strMethodNameToShowFinally=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrDataTblView.count; k++) {
            
            NSDictionary *dictData=[arrDataTblView objectAtIndex:k];
            NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
            if ([arraySelecteValues containsObject:str]) {
                [strMethodNameToShowFinally addObject:[dictData valueForKey:@"ApplicationMethodName"]];
                //ApplicationMethodName
            }
        }
        
        NSString *strFinalName=[strMethodNameToShowFinally componentsJoinedByString:@","];
        if (strFinalName.length==0) {
            strFinalName=@"Select";
        }
        strGlobalAppMethodIdOther=[arraySelecteValues componentsJoinedByString:@","];
        [_btnAppMethodOtherMain setTitle:strFinalName forState:UIControlStateNormal];
        
    }
    
    if (isTarget) {
        
        if (!(indexMultipleSelectTosetValue==-100000)) {
            UIButton *btnDropdown=arrayOfTarget[indexMultipleSelectTosetValue];
            
            NSString *strTempp=[arraySelecteValues componentsJoinedByString:@","];
            NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)indexMultipleSelectTosetValue+20000,strTempp];
            NSMutableArray *strMethodNameToShowFinally=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrDataTblView.count; k++) {
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:k];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
                if ([arraySelecteValues containsObject:str]) {
                    [strMethodNameToShowFinally addObject:[dictData valueForKey:@"TargetName"]];
                    //ApplicationMethodName
                }
            }
            
            NSString *strFinalName=[strMethodNameToShowFinally componentsJoinedByString:@","];
            if (strFinalName.length==0) {
                strFinalName=@"Select Target";
            }
            if (arrTargetId.count>indexMultipleSelectTosetValue) {
                
                //  NSString *strNewJugad=[NSString stringWithFormat:@"%@,%@",arrAppMethodInfoId[btnIndex],[dicData valueForKey:@"ApplicationMethodId"]];
                [arrTargetId replaceObjectAtIndex:indexMultipleSelectTosetValue withObject:strJugad];
                
                [btnDropdown setTitle:strFinalName forState:UIControlStateNormal];
                
            }else{
                
                [arrTargetId addObject:strJugad];
                
                [btnDropdown setTitle:strFinalName forState:UIControlStateNormal];
                
            }
        }
    }
    
    //
    if (isTargetOther) {
        
        if (!(indexMultipleSelectTosetValue==-100000)) {
            UIButton *btnDropdown=arrayOfTargetOther[indexMultipleSelectTosetValue];
            
            NSString *strTempp=[arraySelecteValues componentsJoinedByString:@","];
            NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)indexMultipleSelectTosetValue+200000,strTempp];
            NSMutableArray *strMethodNameToShowFinally=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrDataTblView.count; k++) {
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:k];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
                if ([arraySelecteValues containsObject:str]) {
                    [strMethodNameToShowFinally addObject:[dictData valueForKey:@"TargetName"]];
                    //ApplicationMethodName
                }
            }
            
            NSString *strFinalName=[strMethodNameToShowFinally componentsJoinedByString:@","];
            if (strFinalName.length==0) {
                strFinalName=@"Select Target";
            }
            if (arrTargetIdOther.count>indexMultipleSelectTosetValue) {
                
                //  NSString *strNewJugad=[NSString stringWithFormat:@"%@,%@",arrAppMethodInfoId[btnIndex],[dicData valueForKey:@"ApplicationMethodId"]];
                [arrTargetIdOther replaceObjectAtIndex:indexMultipleSelectTosetValue withObject:strJugad];
                
                [btnDropdown setTitle:strFinalName forState:UIControlStateNormal];
                
            }else{
                [arrTargetIdOther addObject:strJugad];
                
                [btnDropdown setTitle:strFinalName forState:UIControlStateNormal];
                
            }
        }
    }
    
    
    //
    
    if (isTargetOtherMain) {
        
        NSMutableArray *strMethodNameToShowFinally=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrDataTblView.count; k++) {
            
            NSDictionary *dictData=[arrDataTblView objectAtIndex:k];
            NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
            if ([arraySelecteValues containsObject:str]) {
                [strMethodNameToShowFinally addObject:[dictData valueForKey:@"TargetName"]];
                //ApplicationMethodName
            }
        }
        
        NSString *strFinalName=[strMethodNameToShowFinally componentsJoinedByString:@","];
        if (strFinalName.length==0) {
            strFinalName=@"Select";
        }
        strGlobalTargetIdOther=[arraySelecteValues componentsJoinedByString:@","];
        [_btnTargetOtherMain setTitle:strFinalName forState:UIControlStateNormal];
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Button Cancel Action Methods--------------
//============================================================================
//============================================================================

-(void)btnCancelAction:(id)sender{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------------------------------------------------------------------------------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1874919421) {
        return [arrPickerServiceJobDescriptions count];
    }else
        return [arrDataTblView count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if (tableView.tag==1874919421) {
        
        
        NSDictionary *dictData=[arrPickerServiceJobDescriptions objectAtIndex:indexPath.row];
        
        NSString *title = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Title"]];
        
        cell.textLabel.text=title;
        
        if ([strGlobalServiceJobDescriptionId isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]]]) {
            
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
        }else{
            
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
        }
        
        cell.textLabel.numberOfLines=20000;
        
        if ([UIScreen mainScreen].bounds.size.height==667) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }
        else
            cell.textLabel.font=[UIFont systemFontOfSize:15];
        return cell;
        
    }else{
        if (!(arrDataTblView.count==0)) {
            
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            if (isMultipleSelect) {
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                
                NSString *str=[dictData valueForKey:@"Value"];
                
                cell.textLabel.text=[dictData valueForKey:@"Name"];
                
                long index=100000;
                
                for (int k=0; k<arraySelecteValues.count; k++) {
                    if ([arraySelecteValues containsObject:str]) {
                        index=indexPath.row;
                        break;
                    }
                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
            }else if (isTarget){
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
                long index=100000;
                for (int k=0; k<arraySelecteValues.count; k++) {
                    if ([arraySelecteValues containsObject:str]) {
                        index=indexPath.row;
                        // break;
                    }
                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                cell.textLabel.text=[dictData valueForKey:@"TargetName"];
                
            }else if (isTargetOther){
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
                long index=100000;
                for (int k=0; k<arraySelecteValues.count; k++) {
                    if ([arraySelecteValues containsObject:str]) {
                        index=indexPath.row;
                        // break;
                    }
                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                cell.textLabel.text=[dictData valueForKey:@"TargetName"];
                
            }else if (isTargetOtherMain){
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
                long index=100000;
                for (int k=0; k<arraySelecteValues.count; k++) {
                    if ([arraySelecteValues containsObject:str]) {
                        index=indexPath.row;
                        // break;
                    }
                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                cell.textLabel.text=[dictData valueForKey:@"TargetName"];
                
            }else if (isUnit){
                
                NSDictionary *dicData=arrDataTblView[indexPath.row];
                cell.textLabel.text=[dicData valueForKey:@"UnitName"];
                
            }else if (isUnitOther){
                
                NSDictionary *dicData=arrDataTblView[indexPath.row];
                cell.textLabel.text=[dicData valueForKey:@"UnitName"];
                
            }else if (isUnitOtherMain){
                
                NSDictionary *dicData=arrDataTblView[indexPath.row];
                cell.textLabel.text=[dicData valueForKey:@"UnitName"];
                
            }else if (isAppMethodInfo){
                //ApplicationMethodId
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
                long index=100000;
                for (int k=0; k<arraySelecteValues.count; k++) {
                    if ([arraySelecteValues containsObject:str]) {
                        index=indexPath.row;
                        // break;
                    }
                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                cell.textLabel.text=[dictData valueForKey:@"ApplicationMethodName"];
                
            }else if (isAppMethodInfoOther){
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
                long index=100000;
                for (int k=0; k<arraySelecteValues.count; k++) {
                    if ([arraySelecteValues containsObject:str]) {
                        index=indexPath.row;
                        // break;
                    }
                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                cell.textLabel.text=[dictData valueForKey:@"ApplicationMethodName"];
                
            }else if (isAppMethodInfoOtherMain){
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                NSString *str=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
                long index=100000;
                for (int k=0; k<arraySelecteValues.count; k++) {
                    if ([arraySelecteValues containsObject:str]) {
                        index=indexPath.row;
                        // break;
                    }
                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                cell.textLabel.text=[dictData valueForKey:@"ApplicationMethodName"];
                
            }
            else{
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                cell.textLabel.text=[dictData valueForKey:@"Name"];
                NSString *str=[dictData valueForKey:@"Value"];
                long index=100000;
                if ([strGlobalDropDownSelectedValue isEqualToString:str]) {
                    index=indexPath.row;
                }
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
            }
        }
        cell.textLabel.numberOfLines=20000;
        
        if ([UIScreen mainScreen].bounds.size.height==667) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }
        else
            cell.textLabel.font=[UIFont systemFontOfSize:15];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    yesEditedSomething=YES;
    
    if (tableView.tag==1874919421) {
        
        NSDictionary *dictData=[arrPickerServiceJobDescriptions objectAtIndex:indexPath.row];
        NSString *strServiceJobDescription = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [strServiceJobDescription dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
         _textView_ServiceJobDescriptions.attributedText = attributedString;
        
        //_textView_ServiceJobDescriptions.text=strServiceJobDescription;
        
        strGlobalServiceJobDescriptionsText = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
        strGlobalServiceJobDescriptionId = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]];
        [_btnServiceJobDescriptionTitle setTitle:[dictData valueForKey:@"Title"] forState:UIControlStateNormal];
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    } else {
        
        if (!(arrDataTblView.count==0)) {
            
            if (isMultipleSelect) {
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues addObject:[dictData valueForKey:@"Value"]];
                    [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues removeObject:[dictData valueForKey:@"Value"]];
                    [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
                }
                
                NSLog(@"tableview selected for multile select===%@",arraySelecteValues);
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                
                NSInteger btnIndex;
                btnIndex=tblData.tag;
                indexMultipleSelectTosetValue=btnIndex;
                
            }else if (isTarget)
            {
                
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]]];
                    // [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues removeObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]]];
                    // [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
                }
                
            }else if (isTargetOther)
            {
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]]];
                    // [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues removeObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]]];
                    // [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
                }
                
            }else if (isTargetOtherMain)
            {
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]]];
                    // [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues removeObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]]];
                    // [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
                }
                
            }else if (isAppMethodInfo)
            {
                
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]]];
                    // [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues removeObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]]];
                    // [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
                }
                
            }else if (isAppMethodInfoOther)
            {
                
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]]];
                    // [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues removeObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]]];
                    // [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
                }
                
            }else if (isAppMethodInfoOtherMain)
            {
                
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]]];
                    // [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [arraySelecteValues removeObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]]];
                    // [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
                }
                
            }else if (isUnit){
                NSDictionary *dicData=arrDataTblView[indexPath.row];
                //[dicData valueForKey:@"UnitName"];
                NSInteger btnIndex;
                btnIndex=tblData.tag-15000;
                
                UIButton *btnDropdown=arrayOfUnit[btnIndex];
                
                [btnDropdown setTitle:[dicData valueForKey:@"UnitName"] forState:UIControlStateNormal];
                
                NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)tblData.tag,[dicData valueForKey:@"UnitId"]];
                
                if (arrUnitId.count>btnIndex) {
                    [arrUnitId replaceObjectAtIndex:btnIndex withObject:strJugad];
                }else{
                    [arrUnitId addObject:strJugad];
                    
                }
                
                //  [arrUnitId addObject:strJugad];
                
                //  [self commonMethodToSaveValueChemicalList:[dicData valueForKey:@"UnitName"] :(int)tblData.tag :@"unit"];
                
            }else if (isUnitOther){
                NSDictionary *dicData=arrDataTblView[indexPath.row];
                //[dicData valueForKey:@"UnitName"];
                NSInteger btnIndex;
                btnIndex=tblData.tag-150000;
                
                UIButton *btnDropdown=arrayOfUnitOther[btnIndex];
                
                [btnDropdown setTitle:[dicData valueForKey:@"UnitName"] forState:UIControlStateNormal];
                
                NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)tblData.tag,[dicData valueForKey:@"UnitId"]];
                
                if (arrUnitIdOther.count>btnIndex) {
                    [arrUnitIdOther replaceObjectAtIndex:btnIndex withObject:strJugad];
                }else{
                    [arrUnitIdOther addObject:strJugad];
                    
                }
                
                // [arrUnitIdOther addObject:strJugad];
                
                //  [self commonMethodToSaveValueChemicalList:[dicData valueForKey:@"UnitName"] :(int)tblData.tag :@"unit"];
                
            }else if (isUnitOtherMain){
                NSDictionary *dicData=arrDataTblView[indexPath.row];
                strGlobalUnitIdOther=[NSString stringWithFormat:@"%@",[dicData valueForKey:@"UnitId"]];
                [_btnUnitOtherMain setTitle:[dicData valueForKey:@"UnitName"] forState:UIControlStateNormal];
            }
            else{
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                
                NSInteger btnIndex;
                btnIndex=tblData.tag;
                
                UIButton *btnDropdown=arrOfControlssMain[btnIndex];
                
                [btnDropdown setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                
                //[self commonMethodToSaveValue:[dictData valueForKey:@"Name"] :btnIndex];
                [self commonMethodToSaveValue:[dictData valueForKey:@"Value"] :btnIndex];
            }
            
        }
        
        if ((isMultipleSelect) || (isAppMethodInfo) || (isAppMethodInfoOther) || (isAppMethodInfoOtherMain) || (isTarget) || (isTargetOther) || (isTargetOtherMain)) {
            
            
        } else {
            
            [viewBackGround removeFromSuperview];
            [tblData removeFromSuperview];
            
        }
        
    }
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Open Close MainView METHODS-----------------
//============================================================================
//============================================================================
-(void)actionOpenCloseMAinView:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    int tagggg=btn.tag;
    
    UIButton *btnTempp=[arrayOfButtonsMain objectAtIndex:tagggg];
    
    [arrayOfButtonsMain replaceObjectAtIndex:tagggg withObject:btnTempp];
    
    UIView *viewTempp=[arrayViews objectAtIndex:tagggg];
    
    if (viewTempp.frame.size.height==0) {
        
        UIImageView *imgView=arrOfButtonImages[tagggg];
        //  [imgView setImage:[UIImage imageNamed:@"minus_icon.png"]];
        
        NSString *strTempHeightLast=arrOfHeightsViews[tagggg];
        CGFloat hiTempLast=[strTempHeightLast floatValue];
        
        [viewTempp setFrame:CGRectMake(btnTempp.frame.origin.x, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, btnTempp.frame.size.width, hiTempLast)];
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTempp.frame.size.height)];
        
        [viewTempp setHidden:NO];
    } else {
        
        UIImageView *imgView=arrOfButtonImages[tagggg];
        [imgView setImage:[UIImage imageNamed:@"add_white.png"]];
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTempp.frame.size.height)];
        
        [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
        [viewTempp setHidden:YES];
    }
    [arrayViews replaceObjectAtIndex:tagggg withObject:viewTempp];
    
    for (int k=tagggg+1; k<arrayViews.count; k++) {
        
        
        UIButton *btnTemppNew=[arrayOfButtonsMain objectAtIndex:k];
        UIView *viewTemppNew=[arrayViews objectAtIndex:k];
        UIView *viewTemppNewLasttt;
        CGFloat hiTempLast;
        if (k==0) {
            viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
        } else {
            viewTemppNewLasttt=[arrayViews objectAtIndex:k-1];
            NSString *strTempHeightLast=arrOfHeightsViews[k-1];
            hiTempLast =[strTempHeightLast floatValue];
            
        }
        
        [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
        [btnTemppNew addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        [arrayOfButtonsMain replaceObjectAtIndex:k withObject:btnTemppNew];
        
        [viewTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
        if (viewTemppNew.frame.size.height==0) {
            
            UIImageView *imgView=arrOfButtonImages[k];
            [imgView setImage:[UIImage imageNamed:@"add_white.png"]];
            
            [viewTemppNew setHidden:YES];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTemppNew.frame.size.height)];
            
        }else{
            UIImageView *imgView=arrOfButtonImages[k];//checked.png
            //   [imgView setImage:[UIImage imageNamed:@"minus_icon.png"]];
            
            [viewTemppNew setHidden:NO];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTemppNew.frame.size.height)];
            
        }
        [arrayViews replaceObjectAtIndex:k withObject:viewTemppNew];
        
    }
    
    [self setScrollHeightonClick];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
}
-(void)textViewDidEndEditing:(UITextView *)textField
{
    int taggg=textField.tag;
    if (taggg==1874919419) {
        
    }else if (taggg==1874919420) {
        
    } else {
        
        [self commonMethodToSaveValue:textField.text :taggg];
        
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT FIELD DELEGATE METHODS-----------------
//============================================================================
//============================================================================
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    if (textField.tag==10001) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] >= 2)
        {
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if (!([sepStr length]>2)) {
                if ([sepStr length]==2 && [string isEqualToString:@"."]) {
                    return NO;
                }
                return YES;
            }
            else{
                return NO;
            }
        }
    }
    if (textField.tag==10000) {
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] >= 2)
        {
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if (!([sepStr length]>2)) {
                if ([sepStr length]==2 && [string isEqualToString:@"."]) {
                    return NO;
                }
                return YES;
            }
            else{
                return NO;
            }
        }
        
    }
    if (textField.tag==10001) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 15) ? NO : YES;
    }else if (textField.tag==10000) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 15) ? NO : YES;
    }
    else  if (textField.tag==1874919418) {
        //Image Caption
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 250) ? NO : YES;
    }else  if (textField.tag==1874919419) {
        //Image Description
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 250) ? NO : YES;
    }
    else
        return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    
    
    textField.leftView = leftView;
    
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField.tag==10000) {
        
    }else if (textField.tag==10001){
        
    }
    else if (textField.tag==10002){
        
    }else if (textField.tag==10001){
        
    }
    else {
        
        int taggg=textField.tag;
        
        //Phle 35 phir 30 phir 25 bade se chota
        if (taggg>=350000){
            
            
            NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)textField.tag,textField.text];
            
            for (int k=0; k<arrEPARegNumberOther.count; k++) {
                
                NSString *strTags=arrEPARegNumberOther[k];
                
                NSArray *arrValue=[strTags componentsSeparatedByString:@","];
                
                NSString *strTagToCheck=arrValue[0];
                
                if ([strTagToCheck isEqualToString:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
                    
                    [arrEPARegNumberOther replaceObjectAtIndex:k withObject:strJugad];
                    
                }
            }
            // [arrEPARegNumber addObject:strJugad];
            
            //   [self commonMethodToSaveValueChemicalList:textField.text :taggg :@"amount"];
            
        }else if (taggg>=300000) {
            
            int tempText=[textField.text intValue];
            
            if (tempText>100) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter percentage in between 100" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
                textField.text=@"";
                
            } else {
                
                NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)textField.tag,textField.text];
                
                for (int k=0; k<arrTxtPercentOther.count; k++) {
                    
                    NSString *strTags=arrTxtPercentOther[k];
                    
                    NSArray *arrValue=[strTags componentsSeparatedByString:@","];
                    
                    NSString *strTagToCheck=arrValue[0];
                    
                    if ([strTagToCheck isEqualToString:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
                        
                        [arrTxtPercentOther replaceObjectAtIndex:k withObject:strJugad];
                        
                    }
                }
                
                // [arrTxtPercent addObject:strJugad];
                
            }
            
            //  [self commonMethodToSaveValueChemicalList:textField.text :taggg :@"percent"];
            
        }else if (taggg>=250000){
            
            NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)textField.tag,textField.text];
            
            for (int k=0; k<arrTxtAmountOther.count; k++) {
                
                NSString *strTags=arrTxtAmountOther[k];
                
                NSArray *arrValue=[strTags componentsSeparatedByString:@","];
                
                NSString *strTagToCheck=arrValue[0];
                
                if ([strTagToCheck isEqualToString:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
                    
                    [arrTxtAmountOther replaceObjectAtIndex:k withObject:strJugad];
                    
                }
            }
            
            // [arrTxtAmount addObject:strJugad];
            
            //   [self commonMethodToSaveValueChemicalList:textField.text :taggg :@"amount"];
            
        }else if (taggg>=35000){
            
            
            NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)textField.tag,textField.text];
            
            for (int k=0; k<arrEPARegNumber.count; k++) {
                
                NSString *strTags=arrEPARegNumber[k];
                
                NSArray *arrValue=[strTags componentsSeparatedByString:@","];
                
                NSString *strTagToCheck=arrValue[0];
                
                if ([strTagToCheck isEqualToString:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
                    
                    [arrEPARegNumber replaceObjectAtIndex:k withObject:strJugad];
                    
                }
            }
            // [arrEPARegNumber addObject:strJugad];
            
            //   [self commonMethodToSaveValueChemicalList:textField.text :taggg :@"amount"];
            
        }else if (taggg>=30000) {
            
            int tempText=[textField.text intValue];
            
            if (tempText>100) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter percentage in between 100" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
                textField.text=@"";
                
            } else {
                
                NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)textField.tag,textField.text];
                
                for (int k=0; k<arrTxtPercent.count; k++) {
                    
                    NSString *strTags=arrTxtPercent[k];
                    
                    NSArray *arrValue=[strTags componentsSeparatedByString:@","];
                    
                    NSString *strTagToCheck=arrValue[0];
                    
                    if ([strTagToCheck isEqualToString:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
                        
                        [arrTxtPercent replaceObjectAtIndex:k withObject:strJugad];
                        
                    }
                }
                
                // [arrTxtPercent addObject:strJugad];
                
            }
            
            //  [self commonMethodToSaveValueChemicalList:textField.text :taggg :@"percent"];
            
        }else if (taggg>=25000){
            
            NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)textField.tag,textField.text];
            
            for (int k=0; k<arrTxtAmount.count; k++) {
                
                NSString *strTags=arrTxtAmount[k];
                
                NSArray *arrValue=[strTags componentsSeparatedByString:@","];
                
                NSString *strTagToCheck=arrValue[0];
                
                if ([strTagToCheck isEqualToString:[NSString stringWithFormat:@"%ld",(long)textField.tag]]) {
                    
                    [arrTxtAmount replaceObjectAtIndex:k withObject:strJugad];
                    
                }
            }
            
            // [arrTxtAmount addObject:strJugad];
            
            //   [self commonMethodToSaveValueChemicalList:textField.text :taggg :@"amount"];
            
        }
        else {
            
            [self commonMethodToSaveValue:textField.text :taggg];
            
        }
    }
}

//============================================================================
//============================================================================
#pragma mark ---------------- Validation Check Method--------------------------
//============================================================================
//============================================================================

-(NSString *)validationCheck :(NSString*)stringTovalidate :(NSString*)typeOfelement
{
    NSString *errorMessage;
    if (stringTovalidate.length>0) {
        
        if ([typeOfelement isEqualToString:@"el-email"]) {
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            if (![emailPredicate evaluateWithObject:stringTovalidate])
            {
                errorMessage = @"Please enter valid email id only";
            }
            if ([stringTovalidate containsString:@" "])
            {
                errorMessage = @"Please enter valid email id only";
            }
        }
        else if ([typeOfelement isEqualToString:@""]){
            
        }
        
    }
    return errorMessage;
}
//============================================================================
//============================================================================
#pragma mark ---------------- Common Method To Save Chemical Value---------------------
//============================================================================
//============================================================================

-(void)commonMethodToSaveValueChemicalList :(NSString*)strValueToSet :(int)tagggg :(NSString*)strType{
    
    if (arrOfWorkOrderProductDetails.count==0) {
        
        yesEditedSomething=YES;
        NSLog(@"Yes Something Edited");
        NSString *strWOPId,*strPId,*strPName,*strUId,*strTId,*strDPercent,*strPAmount;
        
        if ([strType isEqualToString:@"amount"]) {
            
            strPAmount=strValueToSet;
            strWOPId=0;
            strPId=@"";
            strPId=@"";
            strPId=@"";
            strPId=@"";
            strPId=@"";
            strPId=@"";
            
        }else if ([strType isEqualToString:@"percent"]){
            
        }else if ([strType isEqualToString:@"unit"]){
            
        }else if ([strType isEqualToString:@"target"]){
            
        }
        
        
        NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                   strWOPId,
                                   @"true",
                                   strPId,
                                   strPName,
                                   strUId,
                                   strTId,
                                   strDPercent,
                                   strPAmount,nil];
        NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                 @"WoProductId",
                                 @"Checked",
                                 @"ProductId",
                                 @"ProductName",
                                 @"UnitId",
                                 @"TargetId",
                                 @"DefaultPercentage",
                                 @"ProductAmount",nil];
        
        NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
        
        [arrOfTempWOPD addObject:dict_ToSendLeadInfo];
        
        
    }else{
        
    }
    
}
//============================================================================
//============================================================================
#pragma mark ---------------- Common Method To Save value---------------------
//============================================================================
//============================================================================
-(void)saveWorkOrderIdDynamicForm{
    
    NSDictionary *dictData=arrResponseInspection[0];
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                            [dictData valueForKey:@"InspectionId"],
                                            strWorkOrderId,
                                            [dictData valueForKey:@"FormId"],
                                            [dictData valueForKey:@"FormName"],
                                            [dictData valueForKey:@"CshtmlFileName"],
                                            [dictData valueForKey:@"CshtmlFilePath"],
                                            [dictData valueForKey:@"HtmlConvertedFileName"],
                                            [dictData valueForKey:@"HtmlConvertedFilePath"],
                                            [dictData valueForKey:@"HtmlFileName"],
                                            [dictData valueForKey:@"HtmlFilePath"],
                                            [dictData valueForKey:@"CompanyId"],
                                            [dictData valueForKey:@"DepartmentSysName"],
                                            arrSections,nil];
    
    NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                          @"InspectionId",
                                          @"WorkorderId",
                                          @"FormId",
                                          @"FormName",
                                          @"CshtmlFileName",
                                          @"CshtmlFilePath",
                                          @"HtmlConvertedFileName",
                                          @"HtmlConvertedFilePath",
                                          @"HtmlFileName",
                                          @"HtmlFilePath",
                                          @"CompanyId",
                                          @"DepartmentSysName",
                                          @"Sections",nil];
    
    NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
    
    [arrResponseInspection replaceObjectAtIndex:0 withObject:dictOfarrResponseInspection];
    
}
-(void)commonMethodToSaveValue :(NSString*)strValueToSet :(int)tagggg{
    
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    int taggg=tagggg;
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    //arrSections=[dictData valueForKey:@"Sections"];
    [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    // arrOfControls=[dictDataControls valueForKey:@"Controls"];
    [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    
    
    //For Validations new On 18 sep sunday at home
    
    NSString *errorMessage;
    
    if ([[dictOfOptions valueForKey:@"Element"] isEqualToString:@"el-email"]) {
        
        strValueToSet =  [strValueToSet stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        errorMessage=[self validationCheck :strValueToSet :@"el-email"];
        
        
    }
    
    //    if (errorMessage) {
    //        NSString *strTitle = Alert;
    //        [global AlertMethod:strTitle :errorMessage];
    //        [DejalBezelActivityView removeView];
    //    } else {
    
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    }
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    
#pragma mark- ---------------------Dictionary of Controls To Be Updated-----------------
    
    //Dictionary of Controls To Be Updated
    
    NSArray *arrOfOptionsToSend=arrDataOptionss;
    
    NSArray *objValueControls=[NSArray arrayWithObjects:
                               [dictOfOptions valueForKey:@"Position"],
                               [dictOfOptions valueForKey:@"Element"],
                               [dictOfOptions valueForKey:@"Label"],
                               strValueToSet,
                               [dictOfOptions valueForKey:@"Id"],
                               [dictOfOptions valueForKey:@"Name"],
                               [dictOfOptions valueForKey:@"PlaceHolder"],
                               [dictOfOptions valueForKey:@"HelpText"],
                               [dictOfOptions valueForKey:@"Type"],
                               [dictOfOptions valueForKey:@"MaxLength"],
                               [dictOfOptions valueForKey:@"MinLength"],
                               [dictOfOptions valueForKey:@"Required"],
                               [dictOfOptions valueForKey:@"ReadOnly"],
                               [dictOfOptions valueForKey:@"Default"],
                               [dictOfOptions valueForKey:@"Format"],
                               [dictOfOptions valueForKey:@"Rows"],
                               [dictOfOptions valueForKey:@"ClassName"],
                               [dictOfOptions valueForKey:@"IsPrimaryControl"],
                               arrOfOptionsToSend,nil];
    
    NSArray *objKeyControls=[NSArray arrayWithObjects:
                             @"Position",
                             @"Element",
                             @"Label",
                             @"Value",
                             @"Id",
                             @"Name",
                             @"PlaceHolder",
                             @"HelpText",
                             @"Type",
                             @"MaxLength",
                             @"MinLength",
                             @"Required",
                             @"ReadOnly",
                             @"Default",
                             @"Format",
                             @"Rows",
                             @"ClassName",
                             @"IsPrimaryControl",
                             @"Options",nil];
    
    NSDictionary *dictOfControlsToUpdate=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
    
    [arrOfControls replaceObjectAtIndex:indexKKK withObject:dictOfControlsToUpdate];
    
    
#pragma mark- ---------------------Dictinary Of Sections To Be Updated-----------------
    
    //Dictinary Of Sections To Be Updated
    
    NSArray *objValueSections=[NSArray arrayWithObjects:
                               [dictDataControls valueForKey:@"Name"],
                               [dictDataControls valueForKey:@"ColumnType"],
                               [dictDataControls valueForKey:@"IsPrimaryControl"],
                               arrOfControls,nil];
    NSArray *objKeySections=[NSArray arrayWithObjects:
                             @"Name",
                             @"ColumnType",
                             @"IsPrimaryControl",
                             @"Controls",nil];
    
    NSDictionary *dictOfSectionToUpdate=[[NSDictionary alloc] initWithObjects:objValueSections forKeys:objKeySections];
    
    [arrSections replaceObjectAtIndex:indexJJJ withObject:dictOfSectionToUpdate];
    
    
#pragma mark- ---------------------Dictionary of arrResponseInspection-----------------
    
    //Dictionary of arrResponseInspection
    
    
    NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                            [dictData valueForKey:@"InspectionId"],
                                            [dictData valueForKey:@"WorkorderId"],
                                            [dictData valueForKey:@"FormId"],
                                            [dictData valueForKey:@"FormName"],
                                            [dictData valueForKey:@"CshtmlFileName"],
                                            [dictData valueForKey:@"CshtmlFilePath"],
                                            [dictData valueForKey:@"HtmlConvertedFileName"],
                                            [dictData valueForKey:@"HtmlConvertedFilePath"],
                                            [dictData valueForKey:@"HtmlFileName"],
                                            [dictData valueForKey:@"HtmlFilePath"],
                                            [dictData valueForKey:@"CompanyId"],
                                            [dictData valueForKey:@"DepartmentSysName"],
                                            arrSections,nil];
    
    NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                          @"InspectionId",
                                          @"WorkorderId",
                                          @"FormId",
                                          @"FormName",
                                          @"CshtmlFileName",
                                          @"CshtmlFilePath",
                                          @"HtmlConvertedFileName",
                                          @"HtmlConvertedFilePath",
                                          @"HtmlFileName",
                                          @"HtmlFilePath",
                                          @"CompanyId",
                                          @"DepartmentSysName",
                                          @"Sections",nil];
    
    NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
    
    //Saavan Doubt
    [arrResponseInspection replaceObjectAtIndex:indexIII withObject:dictOfarrResponseInspection];
    
    
#pragma mark- ---------------------Converting Finally To Json-----------------
    
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictOfarrResponseInspection])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictOfarrResponseInspection options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL FORM JSON: %@", jsonString);
        }
    }
    
    finalJsonDict=dictOfarrResponseInspection;
    // }
}

-(void)sendingFinalDynamicJsonToServer{
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrResponseInspection,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"InspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:finalJsonDict])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:finalJsonDict options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL Service FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlServiceDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     if ([str containsString:@"Success"]) {
                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                     }else{
                         
                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Core Data Methods-----------------
//============================================================================
//============================================================================


-(void)saveToCoreDataServiceDynamic :(NSArray*)arrOfSalesDynamicToSave{
    
    //==================================================================================
    //==================================================================================
    [self deleteFromCoreDataServiceDynamic];
    //==================================================================================
    //==================================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    
    ServiceDynamicForm *objSalesDynamic = [[ServiceDynamicForm alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
    objSalesDynamic.arrFinalInspection=arrOfSalesDynamicToSave;
    objSalesDynamic.workOrderId=strWorkOrderId;
    objSalesDynamic.companyKey=strCompanyKey;
    objSalesDynamic.userName=strUserName;
    objSalesDynamic.empId=strEmpID;
    objSalesDynamic.departmentId=strDepartmentId;
    objSalesDynamic.isSentToServer=@"Yes";
    NSError *error1;
    [context save:&error1];
    
    //==================================================================================
    //==================================================================================
    [self FetchFromCoreDataToShowServiceDynamic];
    //==================================================================================
    //==================================================================================
    
}
-(void)saveToCoreDataServiceDynamicFinal :(NSArray*)arrOfSalesDynamicToSave{
    
    //==================================================================================
    //==================================================================================
    [self deleteFromCoreDataServiceDynamic];
    //==================================================================================
    //==================================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    
    ServiceDynamicForm *objSalesDynamic = [[ServiceDynamicForm alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
    objSalesDynamic.arrFinalInspection=arrOfSalesDynamicToSave;
    objSalesDynamic.workOrderId=strWorkOrderId;
    objSalesDynamic.companyKey=strCompanyKey;
    objSalesDynamic.userName=strUserName;
    objSalesDynamic.empId=strEmpID;
    objSalesDynamic.departmentId=strDepartmentId;
    objSalesDynamic.isSentToServer=@"No";
    
    NSError *error1;
    [context save:&error1];
    
}
-(void)FetchFromCoreDataToShowServiceDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strWorkOrderId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            
            [self dynamicFormCreation];
            
        }else{
            
            [self fetchServiceAutomationService];
            
            
        }
        
    }
    else
    {
        matches=arrAllObj[0];
        NSDictionary *arrTemp = [matches valueForKey:@"arrFinalInspection"];
        
        if ([arrTemp isKindOfClass:[NSArray class]]) {
            
            arrResponseInspection =[[NSMutableArray alloc]init];
            [arrResponseInspection addObjectsFromArray:(NSArray*)arrTemp];
            
        } else {
            
            arrResponseInspection =[[NSMutableArray alloc]init];
            if (arrTemp.count==0) {
                
            } else {
                
                [arrResponseInspection addObject:arrTemp];
                
            }
            
        }
        
        //   arrResponseInspection =[[NSMutableArray alloc]init];
        //   [arrResponseInspection addObject:arrTemp];
        [self dynamicFormCreation];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)deleteFromCoreDataServiceDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strWorkOrderId];
    
    [allData setPredicate:predicate];
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)updateServiceDynamicJsonInDB
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strWorkOrderId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        [global AlertMethod:@"Error" :@"Unable to save.Please try again later."];
    }
    else
    {
        matches=arrAllObj[0];
        
        [matches setValue:arrResponseInspection forKey:@"arrFinalInspection"];
        
        NSError *error1;
        [context save:&error1];
        
        // [self goToNextScreen];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)goToNextScreen{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    SalesAutomationSelectService *objSalesAutoService=[storyboard instantiateViewControllerWithIdentifier:@"SalesAutomationSelectService"];
    [self.navigationController pushViewController:objSalesAutoService animated:YES];
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Payment Type Radio Button Methods-----------------
//============================================================================
//============================================================================

- (IBAction)action_Cash:(id)sender {
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnCashSetting];
    
}
-(void)btnCashSetting{
    
    strGlobalPaymentMode=@"Cash";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self amountSingleViewSettings];
    
}
- (IBAction)action_Check:(id)sender {
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnCheckSetting];
    
}
-(void)btnCheckSetting{
    
    strGlobalPaymentMode=@"Check";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self CheckViewSettings];
    
}
- (IBAction)action_CreditCard:(id)sender {
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnCreditCArdSetting];
    
}

-(void)btnCreditCArdSetting{
    
    strGlobalPaymentMode=@"CreditCard";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self amountSingleViewSettings];
    
}
- (IBAction)action_Bill:(id)sender {
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnBillSetting];
    
}

-(void)btnBillSetting{
    
    strGlobalPaymentMode=@"Bill";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self noAmountViewSettings];
    
}
- (IBAction)action_AutoChargerCust:(id)sender {
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnAutoChargeSetting];
    
}

-(void)btnAutoChargeSetting{
    
    strGlobalPaymentMode=@"AutoChargeCustomer";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self noAmountViewSettings];
    
}
- (IBAction)action_PaymentPending:(id)sender {
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnPaymentPendingSetting];
    
}

-(void)btnPaymentPendingSetting{
    
    strGlobalPaymentMode=@"PaymentPending";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self noAmountViewSettings];
    
    
}
- (IBAction)action_NoCharge:(id)sender {
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnNoChargeSetting];
    
}

-(void)btnNoChargeSetting{
    
    strGlobalPaymentMode=@"NoCharge";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    [self noAmountViewSettings];
    
}

//**************************************
#pragma mark- Check Exp Date...
//**************************************

- (IBAction)action_CheckExpDate:(id)sender {
    
    [self HideALlTextFieldsOpened];
    [_txtAmount resignFirstResponder];
    [_txtAmountSingle resignFirstResponder];
    [_txt_DrivingLicenseNo resignFirstResponder];
    [_txtCheckNo resignFirstResponder];
    
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    int taggggs=-7;
    strGlobalDateToShow=_btnCheckExpDate.titleLabel.text;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self addPickerViewDateTo : taggggs];
    
}

//**************************************
#pragma mark- Image Selecting
//**************************************

- (IBAction)action_CheckFrontImage:(id)sender {
    
    isFromBeforeImage=NO;
    isCheckFrontImage=YES;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
    
}
- (IBAction)action_CheckBackImage:(id)sender {
    
    isFromBeforeImage=NO;
    isCheckFrontImage=NO;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
    
}
- (IBAction)action_AfterImage:(id)sender {
    
    isFromBeforeImage=NO;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"PreviewImage", @"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
    
}
-(void)openGalleryy{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==0)
    {
        if (isCheckFrontImage) {
            
            if (arrOFImagesName.count==0) {
                
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }else{
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setBool:YES forKey:@"forCheckFrontImageDelete"];
                [defs setBool:NO forKey:@"forCheckBackImageDelete"];
                [defs synchronize];
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                ImagePreviewGeneralInfoAppointmentView
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
                objByProductVC.arrOfImages=arrOFImagesName;
                objByProductVC.indexOfImage=@"0";
                objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
                //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
                [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
            }
            
        } else {
            
            if (arrOfCheckBackImage.count==0) {
                
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }else{
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setBool:YES forKey:@"forCheckBackImageDelete"];
                [defs setBool:NO forKey:@"forCheckFrontImageDelete"];
                [defs synchronize];
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                ImagePreviewGeneralInfoAppointmentView
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
                objByProductVC.arrOfImages=arrOfCheckBackImage;
                objByProductVC.indexOfImage=@"0";
                objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
                //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
                [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
            }
        }
    }
    else if (buttonIndex == 1)
    {
        NSArray *arrTemp;
        if (isCheckFrontImage) {
            
            arrTemp=arrOFImagesName;
            
        }else{
            
            arrTemp=arrOfCheckBackImage;
            
        }
        
        if (arrTemp.count>=1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check front image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        } else {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
                
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                             //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                             //                                     [alert show];
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
    }
    else if (buttonIndex == 2)
    {
        NSArray *arrTemp;
        if (isCheckFrontImage) {
            
            arrTemp=arrOFImagesName;
            
        }else{
            
            arrTemp=arrOfCheckBackImage;
            
        }
        
        if (arrTemp.count>=1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check back image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        } else {
            
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
                
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                             //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                             //                                     [alert show];
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
    }
}

//**************************************
#pragma mark- Amount And Check View Settings according To Radio Selected
//**************************************

-(void)CheckViewSettings{
    
    noAmount=NO;
    
    [_lastViewAfterImage removeFromSuperview];
    [_amountViewSingle removeFromSuperview];
    
    CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _checkVieww.frame.size.height);
    frameForCheckView.origin.x=10;
    frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    //    CGFloat widthLblLine=0;
    //    widthLblLine=_lblLinePaymentInfo.frame.origin.x;
    //    _lblLinePaymentInfo.frame=CGRectMake(_lblLinePaymentInfo.frame.origin.x, _lblLinePaymentInfo.frame.origin.y, self.view.frame.size.width-widthLblLine-widthLblLine, _lblLinePaymentInfo.frame.size.height);
    
    [_checkVieww setFrame:frameForCheckView];
    
    [_scrollForDynamicView addSubview:_checkVieww];
    
    CGFloat scrollHeightTemp=_scrollForDynamicView.contentSize.height;
    
    //  _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, scrollHeightTemp+_checkVieww.frame.size.height);
    
    //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,scrollHeightTemp+_checkVieww.frame.size.height)];
    
    
    //**************************************
    //**************************************
    
    
    //Yaha Change Kiya
    CGRect frameForbeforeImageView=CGRectMake(10, _checkVieww.frame.origin.y+_checkVieww.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
    frameForbeforeImageView.origin.x=10;
    frameForbeforeImageView.origin.y=_checkVieww.frame.origin.y+_checkVieww.frame.size.height+10;
    
    [_beforeImageView setFrame:frameForbeforeImageView];
    
    [_scrollForDynamicView addSubview:_beforeImageView];
    
    if (!(arrOfBeforeImageAll.count==0)) {
        
        CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
        frameForbeforeImageViewThumnNail.origin.x=10;
        frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
        
        [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
        
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
        
    }else{
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
    }
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y)];
    
}

-(void)amountSingleViewSettings{
    
    noAmount=NO;
    
    [_checkVieww removeFromSuperview];
    
    [_lastViewAfterImage removeFromSuperview];
    
    CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
    frameForCheckView.origin.x=10;
    frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    //    CGFloat widthLblLine=0;
    //    widthLblLine=_lblLinePaymentInfo.frame.origin.x;
    //    _lblLinePaymentInfo.frame=CGRectMake(_lblLinePaymentInfo.frame.origin.x, _lblLinePaymentInfo.frame.origin.y, self.view.frame.size.width-widthLblLine-widthLblLine, _lblLinePaymentInfo.frame.size.height);
    
    [_amountViewSingle setFrame:frameForCheckView];
    
    [_scrollForDynamicView addSubview:_amountViewSingle];
    
    CGFloat scrollHeightTemp=_scrollForDynamicView.contentSize.height;
    
    // _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, scrollHeightTemp+_amountViewSingle.frame.size.height);
    
    // [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,scrollHeightTemp+_amountViewSingle.frame.size.height)];
    
    
    //**************************************
    //**************************************
    
    
    //Yaha Change Kiya
    CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
    frameForbeforeImageView.origin.x=10;
    frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
    
    [_beforeImageView setFrame:frameForbeforeImageView];
    
    [_scrollForDynamicView addSubview:_beforeImageView];
    
    if (!(arrOfBeforeImageAll.count==0)) {
        
        CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
        frameForbeforeImageViewThumnNail.origin.x=10;
        frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
        
        [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
        
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
        
    }else{
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
    }
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y)];
    
}

-(void)noAmountViewSettings{
    
    noAmount=YES;
    
    [_checkVieww removeFromSuperview];
    
    [_lastViewAfterImage removeFromSuperview];
    
    CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
    frameForCheckView.origin.x=10;
    frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    //    CGFloat widthLblLine=0;
    //    widthLblLine=_lblLinePaymentInfo.frame.origin.x;
    //    _lblLinePaymentInfo.frame=CGRectMake(_lblLinePaymentInfo.frame.origin.x, _lblLinePaymentInfo.frame.origin.y, self.view.frame.size.width-widthLblLine-widthLblLine, _lblLinePaymentInfo.frame.size.height);
    
    [_amountViewSingle setFrame:frameForCheckView];
    
    // [_scrollForDynamicView addSubview:_amountViewSingle];
    
    CGFloat scrollHeightTemp=_scrollForDynamicView.contentSize.height;
    
    // _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, scrollHeightTemp+_amountViewSingle.frame.size.height);
    
    // [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,scrollHeightTemp+_amountViewSingle.frame.size.height)];
    
    
    //**************************************
    //**************************************
    
    
    //Yaha Change Kiya
    CGRect frameForbeforeImageView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
    frameForbeforeImageView.origin.x=10;
    frameForbeforeImageView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    [_beforeImageView setFrame:frameForbeforeImageView];
    
    [_scrollForDynamicView addSubview:_beforeImageView];
    
    if (!(arrOfBeforeImageAll.count==0)) {
        
        CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
        frameForbeforeImageViewThumnNail.origin.x=10;
        frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
        
        [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
        
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
        
    }else{
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
    }
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y)];
    
}

//============================================================================
//============================================================================
#pragma mark- ImagePicker Finished  Methods
//============================================================================
//============================================================================

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if (isFromBeforeImage)
    {
        
        isFromBeforeImage=NO;
        
        yesEditedSomething=YES;
        NSLog(@"Yes Edited Something In Db");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
        
        [arrOfBeforeImageAll addObject:strImageNamess];
        
        [arrOfImagenameCollewctionView addObject:strImageNamess];
        [_imageCollectionView reloadData];
        
        
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        [self resizeImage:chosenImage :strImageNamess];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        //Lat long code
               
               CLLocationCoordinate2D coordinate = [global getLocation] ;
               NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
               NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
               [arrImageLattitude addObject:latitude];
               [arrImageLongitude addObject:longitude];
        
        //imageCaption
        
        NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
        
        BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
        
        if (yesImageCaption) {
            
            [self alertViewCustom];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
            //[self saveImageToCoreData];;
            
        }
       
        
        [self defaultViewSettingForAmount];
        
        
    }
    else{
        
        // isFromImagePicker=YES;
        yesEditedSomething=YES;
        NSLog(@"Yes Something Edited");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\CheckImg%@%@.jpg",strDate,strTime];
        
        UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
        
        [self resizeImage:image :strImageNamess];
        
        //[self saveImage:image :strImageNamess];
        
        if (isCheckFrontImage)
        {
            
            [arrOFImagesName addObject:strImageNamess];
            
        }
        else
        {
            
            [arrOfCheckBackImage addObject:strImageNamess];
            
        }
        
        [self.navigationController dismissViewControllerAnimated: YES completion: nil];
        
    }
}

//Change for Image Caption

-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=1874919419;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=1874919420;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollForDynamicView.contentSize.height - _scrollForDynamicView.bounds.size.height);
        [_scrollForDynamicView setContentOffset:bottomOffset animated:YES];
        
        //[self saveImageToCoreData];;
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollForDynamicView.contentSize.height - _scrollForDynamicView.bounds.size.height);
    [_scrollForDynamicView setContentOffset:bottomOffset animated:YES];
    
    //[self saveImageToCoreData];;
    
}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=1874919418;
        textField.delegate=(id)self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=1874919419;
        textField.delegate=(id)self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollForDynamicView.contentSize.height - _scrollForDynamicView.bounds.size.height);
            [_scrollForDynamicView setContentOffset:bottomOffset animated:YES];
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollForDynamicView.contentSize.height - _scrollForDynamicView.bounds.size.height);
        [_scrollForDynamicView setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}
-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              //  [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    //    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //    CGFloat screenWidth = screenRect.size.width;
    //    if (screenWidth == 320)
    //    {
    //        actualWidth =1000;
    //        actualHeight =1000;
    //    }
    //    else if (screenWidth == 375)
    //    {
    //        actualWidth =365;
    //        actualHeight =365;
    //    }
    //    else if (screenWidth == 414)
    //    {
    //        actualWidth =404;
    //        actualHeight =404;
    //    }
    //    else
    //    {
    //        actualWidth =404;
    //        actualHeight =404;
    //    }
    //
    //    actualWidth =1000;
    //    actualHeight =1000;
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    // NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

//============================================================================
//============================================================================
#pragma mark- Save & Remove Image  Methods
//============================================================================
//============================================================================

- (void)saveImage: (UIImage*)image :(NSString*)imageName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}



- (IBAction)action_Refersh:(id)sender {
    
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
    //                                                             bundle: nil];
    //    InvoiceAppointmentView
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"InvoiceAppointmentView"];
    //    objByProductVC.dictJsonDynamicForm=finalJsonDict;
    //    objByProductVC.arrChemicalList=[dictChemicalList valueForKey:@""];
    //    objByProductVC.workOrderDetail=matchesWorkOrder;
    //    objByProductVC.paymentInfoDetail=matchesPaymentInfo;
    //    objByProductVC.arrAllObjImageDetail=arrAllObjImageDetail;
    //    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceDetailAppointmentView
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDetailAppointmentView"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

//============================================================================
//============================================================================
#pragma mark Get Master Service Automation Methods
//============================================================================
//============================================================================
-(void)getMasterServiceAutomationInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getMasterServiceAutomation];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getMasterServiceAutomation{
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMain,UrlGetMasterServiceAutomation,strCompanyKey];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"getMasterServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",response];
                     UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     // [alert show];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

//============================================================================
//============================================================================
#pragma mark Get Master Product/Chemicals Service Automation Methods
//============================================================================
//============================================================================
-(void)getChemicalsListServiceAutomationInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getChemicalsListServiceAutomation];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getChemicalsListServiceAutomation{
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsMaster=[NSUserDefaults standardUserDefaults];
    NSDictionary *dicDataChmical=[defsMaster objectForKey:@"MasterServiceAutomation"];
    if (dicDataChmical.count==0 || dicDataChmical==nil) {
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to fetch data from server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    } else {
        dictChemicalList=dicDataChmical;
        [self chemicalListDynamicViewCreations:dictChemicalList];
        
    }
    [self defaultViewSettingForAmount];
}


-(void)getChemicalsListServiceAutomationOld{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Chemicals List..."];
    //  strCompanyId=@"1";
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMain,UrlGetMasterProductChemicalsServiceAutomation,strCompanyId];
    
    // strUrl=@"http://tsrs.stagingsoftware.com//api/MobileToServiceAuto/GetAllProductByCompany?CompanyId=113";
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrl:strUrl :@"getMasterProductChemicalsServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",response];
                     UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     //  [alert show];
                     
                     dictChemicalList=response;
                     
                     NSString *strException;
                     @try {
                         if([dictChemicalList objectForKey:@"ExceptionMessage"] != nil) {
                             // The key existed...
                             strException=[dictChemicalList valueForKey:@"ExceptionMessage"];
                             
                         }else{
                             
                         }
                     } @catch (NSException *exception) {
                         
                     } @finally {
                         
                     }
                     if (strException.length==0) {
                         
                         //  [self saveToCoreDataServiceDynamic:(NSArray*)response];
                         [self chemicalListDynamicViewCreations:dictChemicalList];
                         
                     } else {
                         
                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to fetch data from server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                         
                     }
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
                 
                 [self defaultViewSettingForAmount];
                 
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

-(void)actionOnChemicalListDownload{
    
    
    
}
-(void)defaultViewSettingForAmount{
    
    if ([strGlobalPaymentMode caseInsensitiveCompare:@"Cash"] == NSOrderedSame || strGlobalPaymentMode.length==0) {
        
        [self performSelector:@selector(btnCashSetting) withObject:nil afterDelay:1.0];
        // [self btnCashSetting];
        
    } else if ([strGlobalPaymentMode caseInsensitiveCompare:@"CreditCard"] == NSOrderedSame){
        
        [self performSelector:@selector(btnCreditCArdSetting) withObject:nil afterDelay:1.0];
        //   [self btnCreditCArdSetting];
        
    }else if ([strGlobalPaymentMode caseInsensitiveCompare:@"NoCharge"] == NSOrderedSame){
        
        [self performSelector:@selector(btnNoChargeSetting) withObject:nil afterDelay:1.0];
        // [self btnNoChargeSetting];
        
    }
    else if ([strGlobalPaymentMode caseInsensitiveCompare:@"Check"] == NSOrderedSame){
        
        [self performSelector:@selector(btnCheckSetting) withObject:nil afterDelay:1.0];
        //[self btnCheckSetting];
        
    }
    else if ([strGlobalPaymentMode caseInsensitiveCompare:@"PaymentPending"] == NSOrderedSame){
        
        [self performSelector:@selector(btnPaymentPendingSetting) withObject:nil afterDelay:1.0];
        //  [self btnPaymentPending];
        
    }
    else if ([strGlobalPaymentMode caseInsensitiveCompare:@"AutoChargeCustomer"] == NSOrderedSame){
        
        [self performSelector:@selector(btnAutoChargeSetting) withObject:nil afterDelay:1.0];
        // [self btnAutoChargeSetting];
        
        
    }
    else {
        
        [self performSelector:@selector(btnBillSetting) withObject:nil afterDelay:1.0];
        // [self btnBillSetting];
        
    }
    
}
//============================================================================
//============================================================================
#pragma mark Chemical List Dynamic Form Methods
//============================================================================
//============================================================================

-(void)chemicalListDynamicViewCreations :(NSDictionary*)dictOfChemicalList{
    
    NSArray *arrOfProductMaster=[dictOfChemicalList valueForKey:@"ProductMasters"];
    NSArray *arrOfUnitMaster=[dictOfChemicalList valueForKey:@"UnitMasters"];
    NSArray *arrOfTargetMaster=[dictOfChemicalList valueForKey:@"TargetMasters"];
    NSArray *arrOfAppMethodInfoMaster=[dictOfChemicalList valueForKey:@"ApplicationMethodMasters"];
    NSMutableArray *arrTempValues=[[NSMutableArray alloc]init];
    
    //Arr of Product Ids Already Present
    NSMutableArray *arrTempProductIds=[[NSMutableArray alloc]init];
    for (int k=0; k<arrOfWorkOrderProductDetails.count; k++) {
        
        NSDictionary *dictData=arrOfWorkOrderProductDetails[k];
        NSString *strId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
        [arrTempProductIds addObject:strId];
        
    }
    
    //arrOfProductMaster
    for (int k=0; k<arrOfProductMaster.count; k++) {
        
        NSDictionary *dictData=arrOfProductMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        NSString *strIsActiveProduct=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsActive"]];
        NSString *strProductId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
        
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            if ([strIsActiveProduct isEqualToString:@"true"] || [strIsActiveProduct isEqualToString:@"1"] || [arrTempProductIds containsObject:strProductId]) {
                
                [arrTempValues addObject:dictData];
                
            }
            
        }
    }
    arrOfProductMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfUnitMaster
    for (int k=0; k<arrOfUnitMaster.count; k++) {
        
        NSDictionary *dictData=arrOfUnitMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrTempValues addObject:dictData];
            
        }
    }
    arrOfUnitMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfTargetMaster
    for (int k=0; k<arrOfTargetMaster.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrTempValues addObject:dictData];
            
        }
    }
    arrOfTargetMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfAppMethodInfoMaster
    for (int k=0; k<arrOfAppMethodInfoMaster.count; k++) {
        
        NSDictionary *dictData=arrOfAppMethodInfoMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrTempValues addObject:dictData];
            
        }
    }
    arrOfAppMethodInfoMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    NSMutableArray *arrTempValueAppMethod=[[NSMutableArray alloc]init];
    
    NSArray *objValueControls=[NSArray arrayWithObjects:
                               @"0",
                               @"Select Method",
                               @"00001",nil];
    
    NSArray *objKeyControls=[NSArray arrayWithObjects:
                             @"ApplicationMethodId",
                             @"ApplicationMethodName",
                             @"DepartmentId",nil];
    
    NSDictionary *dictAddExtra=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
    
    // [arrTempValueAppMethod addObject:dictAddExtra];
    
    [arrTempValueAppMethod addObjectsFromArray:arrOfAppMethodInfoMaster];
    
    arrOfAppMethodInfoMaster=arrTempValueAppMethod;
    
    //            //Temporary HAi Comment KArna Yad SE
    //            arrResponseInspection=[[NSMutableArray alloc]init];
    //            NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    //            NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"ChemicalList.json"];
    //            NSError * error;
    //            NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    //            NSDictionary *arrOfCountry = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    //            dictOfChemicalList=arrOfCountry;
    //            dictChemicalList=dictOfChemicalList;
    //            arrOfProductMaster=[dictOfChemicalList valueForKey:@""];
    
    CGFloat scrollViewHeight=0.0;
    
    arrOfIndexesChemical =[[NSMutableArray alloc]init];
    arrOfControlssMainChemical =[[NSMutableArray alloc]init];
    
    CGFloat yXisForBtnCheckUncheck=0.0;
    yXisForBtnCheckUncheck=MainViewForm.frame.size.height+MainViewForm.frame.origin.y+50;
    
    UILabel *labelChemicalHead=[[UILabel alloc]initWithFrame:CGRectMake(10, yXisForBtnCheckUncheck, [UIScreen mainScreen].bounds.size.width-20, 40)];
    
    labelChemicalHead.text=@"Chemical List";
    labelChemicalHead.textColor=[UIColor whiteColor];
    labelChemicalHead.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    labelChemicalHead.textAlignment=UITextAlignmentCenter;
    labelChemicalHead.font=[UIFont boldSystemFontOfSize:20];
    [_scrollForDynamicView addSubview:labelChemicalHead];
    
    if (arrOfProductMaster.count==0) {
        
        [labelChemicalHead setHidden:YES];
        
    }
    
    for (int i=0; i<arrOfProductMaster.count; i++)
    {
        NSDictionary *dictProductMasterDetail=[arrOfProductMaster objectAtIndex:i];
        
        if (!MainViewFormChemical.frame.size.height) {
            BtnMainViewChemical=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck+50, 30, 30)];
        }
        else{
            BtnMainViewChemical=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck+45, 30, 30)];
        }
        NSLog(@"BtnMainViewChemical====%@",BtnMainViewChemical);
        BtnMainViewChemical.tag=i+5555;
        BtnMainViewChemical.titleLabel.numberOfLines=20000;
        [BtnMainViewChemical addTarget:self action:@selector(actionOpenCloseChemicalList:) forControlEvents:UIControlEventTouchDown];
        // BtnMainViewChemical.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        
        NSString *strProductId=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductId"]];
        BOOL isMatchedProductId;
        isMatchedProductId=NO;
        NSString *strProductAmountt,*strProductPercentt,*strUnitt,*strTargett,*strUnittNameShow,*strTargetNameShow,*strEPAReg,*strMethodInfo,*strMethodInfoToShow;
        
        for (int k=0; k<arrOfWorkOrderProductDetails.count; k++) {
            
            NSDictionary *dictData=arrOfWorkOrderProductDetails[k];
            NSString *strId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
            
            if ([strId isEqualToString:strProductId]) {
                
                strProductAmountt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductAmount"]];
                strProductPercentt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DefaultPercentage"]];
                strUnitt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"UnitId"]];
                strTargett=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Targets"]];
                strEPAReg=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EPARegNumber"]];
                strMethodInfo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethods"]];
                
                for (int j=0; j<arrOfUnitMaster.count; j++) {
                    
                    NSDictionary *dict=arrOfUnitMaster[j];
                    NSString *strUId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitId"]];
                    NSString *strUnitNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitName"]];
                    if ([strUId isEqualToString:strUnitt]) {
                        
                        strUnittNameShow=strUnitNamee;
                        
                    }
                    
                }
                
                NSMutableArray *arrTempAppNAme=[[NSMutableArray alloc]init];
                
                for (int j=0; j<arrOfTargetMaster.count; j++) {
                    
                    NSDictionary *dict=arrOfTargetMaster[j];
                    NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetId"]];
                    NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetName"]];
                    NSArray *arrTempAppMethodInfoId=[strTargett componentsSeparatedByString:@","];
                    if ([arrTempAppMethodInfoId containsObject:strApplicationMethodId]) {
                        
                        [arrTempAppNAme addObject:strApplicationMethodIdNamee];
                        
                        strTargetNameShow=[arrTempAppNAme componentsJoinedByString:@","];
                        
                    }
                    
                }
                
                
                arrTempAppNAme=[[NSMutableArray alloc]init];
                
                for (int j=0; j<arrOfAppMethodInfoMaster.count; j++) {
                    
                    NSDictionary *dict=arrOfAppMethodInfoMaster[j];
                    NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodId"]];
                    NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodName"]];
                    NSArray *arrTempAppMethodInfoId=[strMethodInfo componentsSeparatedByString:@","];
                    if ([arrTempAppMethodInfoId containsObject:strApplicationMethodId]) {
                        
                        [arrTempAppNAme addObject:strApplicationMethodIdNamee];
                        
                        strMethodInfoToShow=[arrTempAppNAme componentsJoinedByString:@","];
                        
                    }
                    
                }
                
                isMatchedProductId=YES;
                break;
                
            }
        }
        
        
        if (isMatchedProductId) {
            
            [BtnMainViewChemical setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            NSString *strTag=[NSString stringWithFormat:@"%ld",(long)BtnMainViewChemical.tag];
            
            [arrSelectedChemicals addObject:strTag];
            
        } else {
            
            for (int k=0; k<arrOfProductMaster.count; k++) {
                
                NSDictionary *dictData=arrOfProductMaster[k];
                NSString *strId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
                
                if ([strId isEqualToString:strProductId]) {
                    
                    strProductAmountt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductAmount"]];
                    strProductPercentt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DefaultPercentage"]];
                    strUnitt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"UnitId"]];
                    strTargett=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
                    strEPAReg=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EPARegNumber"]];
                    strMethodInfo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
                    
                    for (int j=0; j<arrOfUnitMaster.count; j++) {
                        
                        NSDictionary *dict=arrOfUnitMaster[j];
                        NSString *strUId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitId"]];
                        NSString *strUnitNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitName"]];
                        if ([strUId isEqualToString:strUnitt]) {
                            
                            strUnittNameShow=strUnitNamee;
                            
                        }
                        
                    }
                    
                    for (int j=0; j<arrOfTargetMaster.count; j++) {
                        
                        NSDictionary *dict=arrOfTargetMaster[j];
                        NSString *strTId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetId"]];
                        NSString *strTargetNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetName"]];
                        if ([strTId isEqualToString:strTargett]) {
                            
                            strTargetNameShow=strTargetNamee;
                            
                        }
                        
                    }
                    
                    
                    for (int j=0; j<arrOfAppMethodInfoMaster.count; j++) {
                        
                        NSDictionary *dict=arrOfAppMethodInfoMaster[j];
                        NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodId"]];
                        NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodName"]];
                        if ([strApplicationMethodId isEqualToString:strMethodInfo]) {
                            
                            strMethodInfoToShow=strApplicationMethodIdNamee;
                            
                        }
                        
                    }
                    
                    // isMatchedProductId=YES;
                    break;
                    
                }
            }
            /*
             if (isMatchedProductId) {
             
             [BtnMainViewChemical setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
             
             NSString *strTag=[NSString stringWithFormat:@"%ld",(long)BtnMainViewChemical.tag];
             
             [arrSelectedChemicals addObject:strTag];
             
             }else
             {
             */
            [arrSetHeightZero addObject:@"1"];
            
            [BtnMainViewChemical setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            //  }
            
        }
        
        
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [BtnMainViewChemical setEnabled:NO];
        } else {
            [BtnMainViewChemical setEnabled:YES];
        }
        
        [_scrollForDynamicView addSubview:BtnMainViewChemical];
        
        [arrayOfButtonsMainChemical addObject:BtnMainViewChemical];
        
        
        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
        btnHidden.frame =CGRectMake(0, BtnMainViewChemical.frame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 45);
        btnHidden.tag=i+5555;
        [btnHidden setBackgroundColor:[UIColor clearColor]];
        [btnHidden addTarget:self action:@selector(actionOpenCloseChemicalList:) forControlEvents:UIControlEventTouchDown];
        [_scrollForDynamicView addSubview:btnHidden];
        [arrayOfButtonsMainChemicalHidden addObject:btnHidden];
        
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnHidden setEnabled:NO];
        } else {
            [btnHidden setEnabled:YES];
        }
        
        // Form Creation
        MainViewFormChemical=[[UIView alloc]init];
        
        MainViewFormChemical.frame=CGRectMake(0, yXisForBtnCheckUncheck+BtnMainViewChemical.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 140);
        
        MainViewFormChemical.tag=i+9999;
        MainViewFormChemical.backgroundColor=[UIColor whiteColor];
        
        [_scrollForDynamicView addSubview:MainViewFormChemical];
        
        UILabel *lblTitleSection=[[UILabel alloc]init];
        lblTitleSection.backgroundColor=[UIColor lightGrayColor];
        lblTitleSection.layer.borderWidth = 1.5f;
        lblTitleSection.layer.cornerRadius = 4.0f;
        lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
        
        lblTitleSection.frame=CGRectMake(BtnMainViewChemical.frame.origin.x+BtnMainViewChemical.frame.size.width+5, BtnMainViewChemical.frame.origin.y, [UIScreen mainScreen].bounds.size.width-BtnMainViewChemical.frame.origin.x-BtnMainViewChemical.frame.size.width-15, 30);
        
        lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductName"]];
        
        NSLog(@"lblTitleSection.text %@",lblTitleSection.text);
        
        lblTitleSection.textAlignment=NSTextAlignmentLeft;
        
        [arrayProductName addObject:lblTitleSection];
        
        [_scrollForDynamicView addSubview:lblTitleSection];
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt EPAReg-----------------
        //============================================================================
        //============================================================================
        //    UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        
        UITextField *txtFieldEPAReg=[[UITextField alloc]init];
        txtFieldEPAReg.placeholder=@"EPAReg #";
        txtFieldEPAReg.text=strEPAReg;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        txtFieldEPAReg.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldEPAReg.layer.borderWidth = 1.5f;
        txtFieldEPAReg.layer.cornerRadius = 4.0f;
        txtFieldEPAReg.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldEPAReg.tag=i+35000;
        
        NSString *strJugad2=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldEPAReg.tag,txtFieldEPAReg.text];
        
        [arrEPARegNumber addObject:strJugad2];
        
        [arrOfTxtFieldsChemicalList addObject:txtFieldEPAReg];
        
        txtFieldEPAReg.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldEPAReg setEnabled:NO];
        } else {
            [txtFieldEPAReg setEnabled:YES];
        }
        //   txtFieldAmount.inputView=vieww;
        txtFieldEPAReg.frame=CGRectMake(BtnMainViewChemical.frame.origin.x-10, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldEPAReg];
        
        
        
        //============================================================================
        //============================================================================
#pragma mark----------------------Txt Percentage-----------------
        //============================================================================
        //============================================================================
        
        UITextField *txtFieldPercentage=[[UITextField alloc]init];
        txtFieldPercentage.placeholder=@"Percentage";
        txtFieldPercentage.text=strProductPercentt;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"DefaultPercentage"]];
        txtFieldPercentage.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldPercentage.layer.borderWidth = 1.5f;
        txtFieldPercentage.layer.cornerRadius = 4.0f;
        txtFieldPercentage.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldPercentage.tag=i+30000;
        
        NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldPercentage.tag,txtFieldPercentage.text];
        
        [arrTxtPercent addObject:strJugad];
        [arrOfTxtFieldsChemicalList addObject:txtFieldPercentage];
        
        //    txtFieldPercentage.inputView=vieww;
        txtFieldPercentage.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldPercentage setEnabled:NO];
        } else {
            [txtFieldPercentage setEnabled:YES];
        }
        txtFieldPercentage.frame=CGRectMake(txtFieldEPAReg.frame.origin.x+txtFieldEPAReg.frame.size.width+5, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldPercentage];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt Amount-----------------
        //============================================================================
        //============================================================================
        //    UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        
        UITextField *txtFieldAmount=[[UITextField alloc]init];
        txtFieldAmount.placeholder=@"Amount";
        txtFieldAmount.text=strProductAmountt;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        txtFieldAmount.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldAmount.layer.borderWidth = 1.5f;
        txtFieldAmount.layer.cornerRadius = 4.0f;
        txtFieldAmount.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldAmount.tag=i+25000;
        
        NSString *strJugad1=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldAmount.tag,txtFieldAmount.text];
        [arrTxtAmount addObject:strJugad1];
        [arrOfTxtFieldsChemicalList addObject:txtFieldAmount];
        
        txtFieldAmount.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldAmount setEnabled:NO];
        } else {
            [txtFieldAmount setEnabled:YES];
        }
        
        //   txtFieldAmount.inputView=vieww;
        txtFieldAmount.frame=CGRectMake(txtFieldEPAReg.frame.origin.x, txtFieldEPAReg.frame.origin.y+txtFieldEPAReg.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldAmount];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Unit-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnUnit=[[UIButton alloc]init];
        
        btnUnit.frame=CGRectMake(txtFieldPercentage.frame.origin.x, txtFieldEPAReg.frame.origin.y+txtFieldEPAReg.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strUnittNameShow.length==0) {
            [btnUnit setTitle:@"Select Unit" forState:UIControlStateNormal];
            
        } else {
            [btnUnit setTitle:strUnittNameShow forState:UIControlStateNormal];
            
        }
        [btnUnit setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnUnit.tag=i+15000;
        [btnUnit addTarget:self action:@selector(actionUnitList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfUnit addObject:btnUnit];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnUnit setEnabled:NO];
        } else {
            [btnUnit setEnabled:YES];
        }
        [MainViewFormChemical addSubview:btnUnit];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn AppMethodInfo-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnAppMethodInfo=[[UIButton alloc]init];
        
        btnAppMethodInfo.frame=CGRectMake(txtFieldAmount.frame.origin.x, txtFieldAmount.frame.origin.y+txtFieldAmount.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strMethodInfoToShow.length==0) {
            [btnAppMethodInfo setTitle:@"Select Method" forState:UIControlStateNormal];
            
        } else {
            [btnAppMethodInfo setTitle:strMethodInfoToShow forState:UIControlStateNormal];
            
        }
        [btnAppMethodInfo setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnAppMethodInfo.tag=i+40000;
        
        NSString *strCombinedAppMethodId=[NSString stringWithFormat:@"%ld,%@",(long)btnAppMethodInfo.tag,strMethodInfo];
        [arrAppMethodInfoId addObject:strCombinedAppMethodId];
        
        [btnAppMethodInfo addTarget:self action:@selector(actionAppMethodInfoList:) forControlEvents:UIControlEventTouchDown];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnAppMethodInfo setEnabled:NO];
        } else {
            [btnAppMethodInfo setEnabled:YES];
        }
        [arrayOfAppMethodInfo addObject:btnAppMethodInfo];
        [MainViewFormChemical addSubview:btnAppMethodInfo];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Target-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnTarget=[[UIButton alloc]init];
        
        btnTarget.frame=CGRectMake(btnUnit.frame.origin.x, btnUnit.frame.origin.y+btnUnit.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strTargetNameShow.length==0) {
            [btnTarget setTitle:@"Select Target" forState:UIControlStateNormal];
            
        } else {
            [btnTarget setTitle:strTargetNameShow forState:UIControlStateNormal];
            
        }
        [btnTarget setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnTarget.tag=i+20000;
        
        NSString *strCombinedTargetId=[NSString stringWithFormat:@"%ld,%@",(long)btnTarget.tag,strTargett];
        [arrTargetId addObject:strCombinedTargetId];
        
        [btnTarget addTarget:self action:@selector(actionTargetList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfTarget addObject:btnTarget];
        [MainViewFormChemical addSubview:btnTarget];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnTarget setEnabled:NO];
        } else {
            [btnTarget setEnabled:YES];
        }
        
        
        scrollViewHeight=scrollViewHeight+MainViewFormChemical.frame.size.height;
        
        [arrayViewsChemical addObject:MainViewFormChemical];
        
        CGFloat heightTemp=MainViewFormChemical.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewFormChemical.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        
        [arrOfHeightsViewsChemical addObject:strTempHeight];
        
        [arrOfYAxisViewsChemical addObject:strTempYAxis];
        
        if (!isMatchedProductId) {
            
            UIView *viewTempp=[arrayViewsChemical objectAtIndex:arrayOfButtonsMainChemical.count-1];
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[arrayOfButtonsMainChemical.count-1];
            [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
            [viewTempp setHidden:YES];
            
            yXisForBtnCheckUncheck=yXisForBtnCheckUncheck+62;
            
            
        }else{
            yXisForBtnCheckUncheck=yXisForBtnCheckUncheck+150;
            
        }
        
        
        //            NSArray *arrTempp=[dictChemicalList valueForKey:@""];
        //            NSDictionary *dictOfData=arrTempp[i];
        //            [arrOfSelectedChemicalToSendToNextView addObject:dictOfData];
        
    }
    
    
    _scrollForDynamicView.backgroundColor=[UIColor clearColor];
    
    //        _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, _scrollForDynamicView.frame.size.height+arrOfProductMaster.count*150+200);
    //
    //
    //        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+arrOfProductMaster.count*150+200)];
    
    
    CGRect frameForPaymentInfo;
    if (MainViewFormChemical.frame.size.height==0) {
        
        if (!(arrayOfButtonsMainChemical.count==0)) {
            UIView *viewTempp=[arrayViewsChemical objectAtIndex:arrayOfButtonsMainChemical.count-1];
            
            frameForPaymentInfo=CGRectMake(10, viewTempp.frame.origin.y+viewTempp.frame.size.height, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForPaymentInfo.origin.x=10;
            frameForPaymentInfo.origin.y=viewTempp.frame.origin.y+viewTempp.frame.size.height+20;
        }else{
            
            frameForPaymentInfo=CGRectMake(10, MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForPaymentInfo.origin.x=10;
            frameForPaymentInfo.origin.y=MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10;
            
        }
    }else{
        if (MainViewFormChemical==nil) {
            //MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10
            
            frameForPaymentInfo=CGRectMake(10, MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForPaymentInfo.origin.x=10;
            frameForPaymentInfo.origin.y=MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10;
            
            
        }else{
            //MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
            frameForPaymentInfo=CGRectMake(10, MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForPaymentInfo.origin.x=10;
            frameForPaymentInfo.origin.y=MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80;
        }
    }
    
    [_viewOtherChemicalList setFrame:frameForPaymentInfo];
    
    [_scrollForDynamicView addSubview:_viewOtherChemicalList];
    
    [self performSelector:@selector(actionOpenCloseChemicalListAtStarting:) withObject:nil afterDelay:0.0];
    
}


-(void)otherchemicalListDynamicViewCreations :(NSDictionary*)dictOfChemicalList{
    
    NSArray *arrOfProductMaster=[dictOfChemicalList valueForKey:@"ProductMasters"];
    NSArray *arrOfUnitMaster=[dictOfChemicalList valueForKey:@"UnitMasters"];
    NSArray *arrOfTargetMaster=[dictOfChemicalList valueForKey:@"TargetMasters"];
    NSArray *arrOfAppMethodInfoMaster=[dictOfChemicalList valueForKey:@"ApplicationMethodMasters"];
    NSMutableArray *arrTempValues=[[NSMutableArray alloc]init];
    
    //Arr of Product Ids Already Present
    NSMutableArray *arrTempProductIds=[[NSMutableArray alloc]init];
    for (int k=0; k<arrOfWorkOrderProductDetails.count; k++) {
        
        NSDictionary *dictData=arrOfWorkOrderProductDetails[k];
        NSString *strId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
        [arrTempProductIds addObject:strId];
        
    }
    
    //arrOfProductMaster
    for (int k=0; k<arrOfProductMaster.count; k++) {
        
        NSDictionary *dictData=arrOfProductMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        NSString *strIsActiveProduct=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsActive"]];
        NSString *strProductId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
        
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            if ([strIsActiveProduct isEqualToString:@"true"] || [strIsActiveProduct isEqualToString:@"1"] || [arrTempProductIds containsObject:strProductId]) {
                
                [arrTempValues addObject:dictData];
                
            }
            
        }
    }
    arrOfProductMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfUnitMaster
    for (int k=0; k<arrOfUnitMaster.count; k++) {
        
        NSDictionary *dictData=arrOfUnitMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        NSString *strIsActiveProduct=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsActive"]];
        NSString *strProductId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            if ([strIsActiveProduct isEqualToString:@"true"] || [strIsActiveProduct isEqualToString:@"1"] || [arrTempProductIds containsObject:strProductId]) {
                
                [arrTempValues addObject:dictData];
                
            }
            
        }
    }
    arrOfUnitMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfTargetMaster
    for (int k=0; k<arrOfTargetMaster.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrTempValues addObject:dictData];
            
        }
    }
    arrOfTargetMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfAppMethodInfoMaster
    for (int k=0; k<arrOfAppMethodInfoMaster.count; k++) {
        
        NSDictionary *dictData=arrOfAppMethodInfoMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrTempValues addObject:dictData];
            
        }
    }
    arrOfAppMethodInfoMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    NSMutableArray *arrTempValueAppMethod=[[NSMutableArray alloc]init];
    
    NSArray *objValueControls=[NSArray arrayWithObjects:
                               @"0",
                               @"Select Method",
                               @"00001",nil];
    
    NSArray *objKeyControls=[NSArray arrayWithObjects:
                             @"ApplicationMethodId",
                             @"ApplicationMethodName",
                             @"DepartmentId",nil];
    
    NSDictionary *dictAddExtra=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
    
    //  [arrTempValueAppMethod addObject:dictAddExtra];
    
    [arrTempValueAppMethod addObjectsFromArray:arrOfAppMethodInfoMaster];
    
    arrOfAppMethodInfoMaster=arrTempValueAppMethod;
    
    CGFloat scrollViewHeight=0.0;
    
    arrOfIndexesChemicalOther =[[NSMutableArray alloc]init];
    arrOfControlssMainChemicalOther =[[NSMutableArray alloc]init];
    
    CGFloat yXisForBtnCheckUncheck=0.0;
    yXisForBtnCheckUncheck=MainViewFormChemical.frame.size.height+MainViewFormChemical.frame.origin.y+10;
    
    labelChemicalHeadGlobal=[[UILabel alloc]initWithFrame:CGRectMake(10, yXisForBtnCheckUncheck, [UIScreen mainScreen].bounds.size.width-20, 40)];
    
    labelChemicalHeadGlobal.text=@"Other Chemical List";
    labelChemicalHeadGlobal.textColor=[UIColor whiteColor];
    labelChemicalHeadGlobal.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    labelChemicalHeadGlobal.textAlignment=UITextAlignmentCenter;
    labelChemicalHeadGlobal.font=[UIFont boldSystemFontOfSize:20];
    if (arrOfWorkOrderOtherChemicalList.count==0) {
        
    }else{
        [_scrollForDynamicView addSubview:labelChemicalHeadGlobal];
    }
    
    [arrOfSavedChemicalListOtherAllValues addObjectsFromArray:arrOfWorkOrderOtherChemicalList];
    
    for (int i=0; i<arrOfWorkOrderOtherChemicalList.count; i++)
    {
        BOOL isMatchedProductId;
        isMatchedProductId=NO;
        
        NSDictionary *dictOtherProductName=[arrOfWorkOrderOtherChemicalList objectAtIndex:i];
        
        if (!MainViewFormChemicalOther.frame.size.height) {
            BtnMainViewChemicalOther=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck+50, 30, 30)];
        }
        else{
            BtnMainViewChemicalOther=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck+45, 30, 30)];
        }
        BtnMainViewChemicalOther.tag=i+55555;
        BtnMainViewChemicalOther.titleLabel.numberOfLines=20000;
        [BtnMainViewChemicalOther addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
        // BtnMainViewChemical.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        
        NSString *strProductAmountt,*strProductPercentt,*strUnitt,*strTargett,*strUnittNameShow,*strTargetNameShow,*strEPAReg,*strMethodInfo,*strMethodInfoToShow;
        
        strProductAmountt=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"ProductAmount"]];
        strProductPercentt=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"DefaultPercentage"]];
        strUnitt=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"UnitId"]];
        strTargett=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"Targets"]];
        strEPAReg=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"EPARegNumber"]];
        strMethodInfo=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"ApplicationMethods"]];
        
        for (int j=0; j<arrOfUnitMaster.count; j++) {
            
            NSDictionary *dict=arrOfUnitMaster[j];
            NSString *strUId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitId"]];
            NSString *strUnitNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitName"]];
            if ([strUId isEqualToString:strUnitt]) {
                
                strUnittNameShow=strUnitNamee;
                
            }
            
        }
        
        NSMutableArray *arrTempAppNAme =[[NSMutableArray alloc]init];
        
        
        for (int j=0; j<arrOfTargetMaster.count; j++) {
            
            NSDictionary *dict=arrOfTargetMaster[j];
            NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetId"]];
            NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetName"]];
            NSArray *arrTempAppMethodInfoId=[strTargett componentsSeparatedByString:@","];
            if ([arrTempAppMethodInfoId containsObject:strApplicationMethodId]) {
                
                [arrTempAppNAme addObject:strApplicationMethodIdNamee];
                
                strTargetNameShow=[arrTempAppNAme componentsJoinedByString:@","];
                
            }
            
        }
        
        arrTempAppNAme =[[NSMutableArray alloc]init];
        
        for (int j=0; j<arrOfAppMethodInfoMaster.count; j++) {
            
            NSDictionary *dict=arrOfAppMethodInfoMaster[j];
            NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodId"]];
            NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodName"]];
            NSArray *arrTempAppMethodInfoId=[strMethodInfo componentsSeparatedByString:@","];
            if ([arrTempAppMethodInfoId containsObject:strApplicationMethodId]) {
                
                [arrTempAppNAme addObject:strApplicationMethodIdNamee];
                
                strMethodInfoToShow=[arrTempAppNAme componentsJoinedByString:@","];
                
            }
            
        }
        
        
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [BtnMainViewChemicalOther setEnabled:NO];
        } else {
            [BtnMainViewChemicalOther setEnabled:YES];
        }
        
        [_scrollForDynamicView addSubview:BtnMainViewChemicalOther];
        
        [arrayOfButtonsMainChemicalOther addObject:BtnMainViewChemicalOther];
        
        
        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
        btnHidden.frame =CGRectMake(0, BtnMainViewChemicalOther.frame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 45);
        btnHidden.tag=i+55555;
        [btnHidden setBackgroundColor:[UIColor clearColor]];
        [btnHidden addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
        [_scrollForDynamicView addSubview:btnHidden];
        [arrayOfButtonsMainChemicalOtherHidden addObject:btnHidden];
        
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnHidden setEnabled:NO];
        } else {
            [btnHidden setEnabled:YES];
        }
        
        // Form Creation
        MainViewFormChemicalOther=[[UIView alloc]init];
        
        MainViewFormChemicalOther.frame=CGRectMake(0, yXisForBtnCheckUncheck+BtnMainViewChemicalOther.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 140);
        
        MainViewFormChemicalOther.tag=i+99999;
        MainViewFormChemicalOther.backgroundColor=[UIColor whiteColor];
        
        [_scrollForDynamicView addSubview:MainViewFormChemicalOther];
        
        UILabel *lblTitleSection=[[UILabel alloc]init];
        lblTitleSection.backgroundColor=[UIColor lightGrayColor];
        lblTitleSection.layer.borderWidth = 1.5f;
        lblTitleSection.layer.cornerRadius = 4.0f;
        lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
        
        lblTitleSection.frame=CGRectMake(BtnMainViewChemicalOther.frame.origin.x+BtnMainViewChemicalOther.frame.size.width+5, BtnMainViewChemicalOther.frame.origin.y, [UIScreen mainScreen].bounds.size.width-BtnMainViewChemicalOther.frame.origin.x-BtnMainViewChemicalOther.frame.size.width-15, 30);
        
        lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"ProductName"]];
        
        lblTitleSection.textAlignment=NSTextAlignmentLeft;
        
        [arrayProductNameOther addObject:lblTitleSection];
        
        [_scrollForDynamicView addSubview:lblTitleSection];
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt EPAReg-----------------
        //============================================================================
        //============================================================================
        //    UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        
        UITextField *txtFieldEPAReg=[[UITextField alloc]init];
        txtFieldEPAReg.placeholder=@"EPAReg #";
        txtFieldEPAReg.text=strEPAReg;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        txtFieldEPAReg.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldEPAReg.layer.borderWidth = 1.5f;
        txtFieldEPAReg.layer.cornerRadius = 4.0f;
        txtFieldEPAReg.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldEPAReg.tag=i+350000;
        
        NSString *strJugad2=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldEPAReg.tag,txtFieldEPAReg.text];
        
        [arrEPARegNumberOther addObject:strJugad2];
        
        [arrOfTxtFieldsChemicalList addObject:txtFieldEPAReg];
        
        txtFieldEPAReg.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldEPAReg setEnabled:NO];
        } else {
            [txtFieldEPAReg setEnabled:YES];
        }
        //   txtFieldAmount.inputView=vieww;
        txtFieldEPAReg.frame=CGRectMake(BtnMainViewChemicalOther.frame.origin.x-10, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemicalOther addSubview:txtFieldEPAReg];
        
        
        
        //============================================================================
        //============================================================================
#pragma mark----------------------Txt Percentage-----------------
        //============================================================================
        //============================================================================
        
        UITextField *txtFieldPercentage=[[UITextField alloc]init];
        txtFieldPercentage.placeholder=@"Percentage";
        txtFieldPercentage.text=strProductPercentt;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"DefaultPercentage"]];
        txtFieldPercentage.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldPercentage.layer.borderWidth = 1.5f;
        txtFieldPercentage.layer.cornerRadius = 4.0f;
        txtFieldPercentage.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldPercentage.tag=i+300000;
        
        NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldPercentage.tag,txtFieldPercentage.text];
        
        [arrTxtPercentOther addObject:strJugad];
        [arrOfTxtFieldsChemicalList addObject:txtFieldPercentage];
        
        //    txtFieldPercentage.inputView=vieww;
        txtFieldPercentage.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldPercentage setEnabled:NO];
        } else {
            [txtFieldPercentage setEnabled:YES];
        }
        txtFieldPercentage.frame=CGRectMake(txtFieldEPAReg.frame.origin.x+txtFieldEPAReg.frame.size.width+5, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemicalOther addSubview:txtFieldPercentage];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt Amount-----------------
        //============================================================================
        //============================================================================
        //    UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        
        UITextField *txtFieldAmount=[[UITextField alloc]init];
        txtFieldAmount.placeholder=@"Amount";
        txtFieldAmount.text=strProductAmountt;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        txtFieldAmount.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldAmount.layer.borderWidth = 1.5f;
        txtFieldAmount.layer.cornerRadius = 4.0f;
        txtFieldAmount.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldAmount.tag=i+250000;
        
        NSString *strJugad1=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldAmount.tag,txtFieldAmount.text];
        [arrTxtAmountOther addObject:strJugad1];
        [arrOfTxtFieldsChemicalList addObject:txtFieldAmount];
        
        txtFieldAmount.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldAmount setEnabled:NO];
        } else {
            [txtFieldAmount setEnabled:YES];
        }
        
        //   txtFieldAmount.inputView=vieww;
        txtFieldAmount.frame=CGRectMake(txtFieldEPAReg.frame.origin.x, txtFieldEPAReg.frame.origin.y+txtFieldEPAReg.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemicalOther addSubview:txtFieldAmount];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Unit-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnUnit=[[UIButton alloc]init];
        
        btnUnit.frame=CGRectMake(txtFieldPercentage.frame.origin.x, txtFieldEPAReg.frame.origin.y+txtFieldEPAReg.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strUnittNameShow.length==0) {
            [btnUnit setTitle:@"Select Unit" forState:UIControlStateNormal];
            
        } else {
            [btnUnit setTitle:strUnittNameShow forState:UIControlStateNormal];
            
        }
        [btnUnit setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnUnit.tag=i+150000;
        [btnUnit addTarget:self action:@selector(actionUnitListOther:) forControlEvents:UIControlEventTouchDown];
        [arrayOfUnitOther addObject:btnUnit];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnUnit setEnabled:NO];
        } else {
            [btnUnit setEnabled:YES];
        }
        [MainViewFormChemicalOther addSubview:btnUnit];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn AppMethodInfo-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnAppMethodInfo=[[UIButton alloc]init];
        
        btnAppMethodInfo.frame=CGRectMake(txtFieldAmount.frame.origin.x, txtFieldAmount.frame.origin.y+txtFieldAmount.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strMethodInfoToShow.length==0) {
            [btnAppMethodInfo setTitle:@"Select Method" forState:UIControlStateNormal];
            
        } else {
            [btnAppMethodInfo setTitle:strMethodInfoToShow forState:UIControlStateNormal];
            
        }
        [btnAppMethodInfo setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnAppMethodInfo.tag=i+400000;
        
        NSString *strCombinedAppMethodId=[NSString stringWithFormat:@"%ld,%@",(long)btnAppMethodInfo.tag,strMethodInfo];
        [arrAppMethodInfoIdOther addObject:strCombinedAppMethodId];
        
        [btnAppMethodInfo addTarget:self action:@selector(actionAppMethodInfoListOther:) forControlEvents:UIControlEventTouchDown];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnAppMethodInfo setEnabled:NO];
        } else {
            [btnAppMethodInfo setEnabled:YES];
        }
        [arrayOfAppMethodInfoOther addObject:btnAppMethodInfo];
        [MainViewFormChemicalOther addSubview:btnAppMethodInfo];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Target-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnTarget=[[UIButton alloc]init];
        
        btnTarget.frame=CGRectMake(btnUnit.frame.origin.x, btnUnit.frame.origin.y+btnUnit.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strTargetNameShow.length==0) {
            [btnTarget setTitle:@"Select Target" forState:UIControlStateNormal];
            
        } else {
            [btnTarget setTitle:strTargetNameShow forState:UIControlStateNormal];
            
        }
        [btnTarget setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnTarget.tag=i+200000;
        
        NSString *strCombinedTargetId=[NSString stringWithFormat:@"%ld,%@",(long)btnTarget.tag,strTargett];
        [arrTargetIdOther addObject:strCombinedTargetId];
        
        [btnTarget addTarget:self action:@selector(actionTargetListOther:) forControlEvents:UIControlEventTouchDown];
        [arrayOfTargetOther addObject:btnTarget];
        [MainViewFormChemicalOther addSubview:btnTarget];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnTarget setEnabled:NO];
        } else {
            [btnTarget setEnabled:YES];
        }
        
        
        scrollViewHeight=scrollViewHeight+MainViewFormChemicalOther.frame.size.height;
        
        [arrayViewsChemicalOther addObject:MainViewFormChemicalOther];
        
        CGFloat heightTemp=MainViewFormChemicalOther.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewFormChemicalOther.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        
        [arrOfHeightsViewsChemicalOther addObject:strTempHeight];
        
        [arrOfYAxisViewsChemicalOther addObject:strTempYAxis];
        
        NSString *strChecked =[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"Checked"]];
        
        
        
        if ([strChecked isEqualToString:@"true"] || [strChecked isEqualToString:@"TRUE"] || [strChecked caseInsensitiveCompare:@"true"] == NSOrderedSame) {
            
            isMatchedProductId=YES;
            
            [BtnMainViewChemicalOther setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            NSString *strTag=[NSString stringWithFormat:@"%ld",(long)BtnMainViewChemicalOther.tag];
            
            [arrSelectedChemicalsOther addObject:strTag];
            
        } else {
            
            [arrSetHeightZeroOther addObject:@"1"];
            
            [BtnMainViewChemicalOther setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
        }
        
        if (!isMatchedProductId) {
            
            UIView *viewTempp=[arrayViewsChemicalOther objectAtIndex:arrayOfButtonsMainChemicalOther.count-1];
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[arrayOfButtonsMainChemicalOther.count-1];
            [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
            [viewTempp setHidden:YES];
            
            yXisForBtnCheckUncheck=yXisForBtnCheckUncheck+62;
            
            
        }else{
            yXisForBtnCheckUncheck=yXisForBtnCheckUncheck+150;
            
        }
    }
    
    
    _scrollForDynamicView.backgroundColor=[UIColor clearColor];
    
    /*
     CGRect frameForViewOtherChemicalList;
     
     frameForViewOtherChemicalList=CGRectMake(10, _viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
     frameForViewOtherChemicalList.origin.x=10;
     frameForViewOtherChemicalList.origin.y=_viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+20;
     
     [_viewOtherChemicalList setFrame:frameForViewOtherChemicalList];
     */
    
    _viewOtherChemicalList.frame=CGRectMake(0, _viewOtherChemicalList.frame.origin.y, _viewOtherChemicalList.frame.size.width, arrOfWorkOrderOtherChemicalList.count*150+62);
    
    CGRect frameForViewOtherChemicalList;
    if (MainViewFormChemical.frame.size.height==0) {
        
        if (!(arrayOfButtonsMainChemical.count==0)) {
            UIView *viewTempp=[arrayViewsChemical objectAtIndex:arrayOfButtonsMainChemical.count-1];
            
            frameForViewOtherChemicalList=CGRectMake(10, viewTempp.frame.origin.y+viewTempp.frame.size.height, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForViewOtherChemicalList.origin.x=10;
            frameForViewOtherChemicalList.origin.y=viewTempp.frame.origin.y+viewTempp.frame.size.height+20;
        }else{
            
            frameForViewOtherChemicalList=CGRectMake(10, MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForViewOtherChemicalList.origin.x=10;
            frameForViewOtherChemicalList.origin.y=MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10;
            
        }
    }else{
        if (MainViewFormChemical==nil) {
            //MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10
            
            frameForViewOtherChemicalList=CGRectMake(10, MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForViewOtherChemicalList.origin.x=10;
            frameForViewOtherChemicalList.origin.y=MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10;
            
            
        }else{
            //MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
            frameForViewOtherChemicalList=CGRectMake(10, MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForViewOtherChemicalList.origin.x=10;
            frameForViewOtherChemicalList.origin.y=MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80;
        }
    }
    
    //OtherChemical Nhi Aa Rahe the to lagaya
    //[_viewOtherChemicalList setFrame:MainViewFormChemical.frame];
    
    [_viewOtherChemicalList setFrame:frameForViewOtherChemicalList];
    
    
    [_scrollForDynamicView addSubview:_viewOtherChemicalList];
    
    //**************************************
#pragma mark- Add Other Chemical List View Settings
    //**************************************
    
    
    CGRect frameForAddOtherChemical=CGRectMake(10, _viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+20, self.view.frame.size.width-20, _viewAddOtherProductList.frame.size.height);
    frameForAddOtherChemical.origin.x=10;
    if (_viewOtherChemicalList.frame.size.height==0) {
        frameForAddOtherChemical.origin.y=_viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+10;
        
    }else{//MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
        if (MainViewFormChemicalOther.frame.size.height==0) {
            
            frameForAddOtherChemical.origin.y=_viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+20;
            
        }else{
            frameForAddOtherChemical.origin.y=_viewOtherChemicalList.frame.origin.y;//+_viewOtherChemicalList.frame.size.height+20;
        }
    }
    
    
    [_viewAddOtherProductList setFrame:frameForAddOtherChemical];
    
    [_scrollForDynamicView addSubview:_viewAddOtherProductList];
    
    // Service job descriptions Changes
    
    CGRect frameForServiceJobDescriptions;
    frameForServiceJobDescriptions=CGRectMake(0, _viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+10, self.view.frame.size.width, heightForServiceJobDescriptions);
    frameForServiceJobDescriptions.origin.x=0;
    frameForServiceJobDescriptions.origin.y=_viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+80;
    [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
    
    [_scrollForDynamicView addSubview:_viewServiceJobDescriptions];
    
    
    CGRect frameForPaymentInfo;
    frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
    frameForPaymentInfo.origin.x=10;
    frameForPaymentInfo.origin.y=_viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+80;
    [_paymentInfoView setFrame:frameForPaymentInfo];
    
    [_scrollForDynamicView addSubview:_paymentInfoView];
    
    //**************************************
#pragma mark- Payment Type View Settings
    //**************************************
    
    
    CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
    frameForPaymentType.origin.x=10;
    if (_paymentInfoView.frame.size.height==0) {
        frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
        
    }else{//MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
        if (MainViewFormChemicalOther.frame.size.height==0) {
            
            frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20;
            
        }else{
            frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20;
        }
    }
    
    
    [_paymentTypeView setFrame:frameForPaymentType];
    
    [_scrollForDynamicView addSubview:_paymentTypeView];
    
    
    
    //**************************************
#pragma mark- Amount And Check View Settings
    //**************************************
    
    
    CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
    frameForCheckView.origin.x=10;
    frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    
    [_amountViewSingle setFrame:frameForCheckView];
    
    [_scrollForDynamicView addSubview:_amountViewSingle];
    
    
    CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
    frameForbeforeImageView.origin.x=10;
    frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
    
    [_beforeImageView setFrame:frameForbeforeImageView];
    
    [_scrollForDynamicView addSubview:_beforeImageView];
    
    if (!(arrOfBeforeImageAll.count==0)) {
        
        CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
        frameForbeforeImageViewThumnNail.origin.x=10;
        frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
        
        [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
        
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
        
    }else{
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
    }
    
    
    
    _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, _lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y+600);
    
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y+600)];
    
    
    if (arrResponseInspection.count==0) {
        
        _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, _scrollForDynamicView.frame.size.height+arrOfProductMaster.count*150+200+_amountViewSingle.frame.size.height+_lastViewAfterImage.frame.size.height+_paymentTypeView.frame.size.height+_paymentInfoView.frame.size.height+heightForServiceJobDescriptions-arrSetHeightZeroOther.count*100+250);
        
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+arrOfProductMaster.count*150+200+_amountViewSingle.frame.size.height+_lastViewAfterImage.frame.size.height+_paymentTypeView.frame.size.height+_paymentInfoView.frame.size.height+heightForServiceJobDescriptions-arrSetHeightZeroOther.count*100+250)];
        
    }
    
    [self performSelector:@selector(actionOpenCloseChemicalListAtStartingOther:) withObject:nil afterDelay:0.0];
    
}

-(void)otherchemicalListDynamicViewCreationsOnlyOnce :(NSDictionary*)dictOfChemicalList :(NSArray*)arrOfChemicalListOnce{
    
    BOOL oneChemicalisAlreadyThere;
    oneChemicalisAlreadyThere=NO;
    
    if (arrOfWorkOrderOtherChemicalList.count>1) {
        oneChemicalisAlreadyThere=YES;
    } else {
        oneChemicalisAlreadyThere=NO;
    }
    
    NSArray *arrOfProductMaster=[dictOfChemicalList valueForKey:@"ProductMasters"];
    NSArray *arrOfUnitMaster=[dictOfChemicalList valueForKey:@"UnitMasters"];
    NSArray *arrOfTargetMaster=[dictOfChemicalList valueForKey:@"TargetMasters"];
    NSArray *arrOfAppMethodInfoMaster=[dictOfChemicalList valueForKey:@"ApplicationMethodMasters"];
    NSMutableArray *arrTempValues=[[NSMutableArray alloc]init];
    
    //Arr of Product Ids Already Present
    NSMutableArray *arrTempProductIds=[[NSMutableArray alloc]init];
    for (int k=0; k<arrOfWorkOrderProductDetails.count; k++) {
        
        NSDictionary *dictData=arrOfWorkOrderProductDetails[k];
        NSString *strId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
        [arrTempProductIds addObject:strId];
        
    }
    
    //arrOfProductMaster
    for (int k=0; k<arrOfProductMaster.count; k++) {
        
        NSDictionary *dictData=arrOfProductMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        NSString *strIsActiveProduct=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsActive"]];
        NSString *strProductId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            if ([strIsActiveProduct isEqualToString:@"true"] || [strIsActiveProduct isEqualToString:@"1"] || [arrTempProductIds containsObject:strProductId]) {
                
                [arrTempValues addObject:dictData];
                
            }
            
        }
    }
    arrOfProductMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfUnitMaster
    for (int k=0; k<arrOfUnitMaster.count; k++) {
        
        NSDictionary *dictData=arrOfUnitMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrTempValues addObject:dictData];
            
        }
    }
    arrOfUnitMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfTargetMaster
    for (int k=0; k<arrOfTargetMaster.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrTempValues addObject:dictData];
            
        }
    }
    arrOfTargetMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    //arrOfAppMethodInfoMaster
    for (int k=0; k<arrOfAppMethodInfoMaster.count; k++) {
        
        NSDictionary *dictData=arrOfAppMethodInfoMaster[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrTempValues addObject:dictData];
            
        }
    }
    arrOfAppMethodInfoMaster=arrTempValues;
    arrTempValues=nil;
    arrTempValues=[[NSMutableArray alloc]init];
    
    
    NSMutableArray *arrTempValueAppMethod=[[NSMutableArray alloc]init];
    
    NSArray *objValueControls=[NSArray arrayWithObjects:
                               @"0",
                               @"Select Method",
                               @"00001",nil];
    
    NSArray *objKeyControls=[NSArray arrayWithObjects:
                             @"ApplicationMethodId",
                             @"ApplicationMethodName",
                             @"DepartmentId",nil];
    
    NSDictionary *dictAddExtra=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
    
    //  [arrTempValueAppMethod addObject:dictAddExtra];
    
    [arrTempValueAppMethod addObjectsFromArray:arrOfAppMethodInfoMaster];
    
    arrOfAppMethodInfoMaster=arrTempValueAppMethod;
    
    CGFloat scrollViewHeight=0.0;
    
    arrOfIndexesChemicalOther =[[NSMutableArray alloc]init];
    arrOfControlssMainChemicalOther =[[NSMutableArray alloc]init];
    
    CGFloat yXisForBtnCheckUncheck=0.0;
    
    if (oneChemicalisAlreadyThere) {
        
        yXisForBtnCheckUncheck=MainViewFormChemical.frame.size.height+MainViewFormChemical.frame.origin.y+10;
        
    } else {
        
        yXisForBtnCheckUncheck=MainViewFormChemical.frame.size.height+MainViewFormChemical.frame.origin.y+10;
        
        //  labelChemicalHeadGlobal=[[UILabel alloc]initWithFrame:CGRectMake(10, yXisForBtnCheckUncheck, [UIScreen mainScreen].bounds.size.width-20, 40)];
        
        [labelChemicalHeadGlobal setHidden:NO];
        
        [labelChemicalHeadGlobal setFrame:CGRectMake(10, yXisForBtnCheckUncheck, [UIScreen mainScreen].bounds.size.width-20, 40)];
        
        labelChemicalHeadGlobal.text=@"Other Chemical List";
        labelChemicalHeadGlobal.textColor=[UIColor whiteColor];
        labelChemicalHeadGlobal.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
        labelChemicalHeadGlobal.textAlignment=UITextAlignmentCenter;
        labelChemicalHeadGlobal.font=[UIFont boldSystemFontOfSize:20];
        // [_scrollForDynamicView addSubview:labelChemicalHeadGlobal];
        
    }
    
    
    for (int i=0; i<arrOfChemicalListOnce.count; i++)
    {
        BOOL isMatchedProductId;
        isMatchedProductId=NO;
        
        NSDictionary *dictOtherProductName=[arrOfChemicalListOnce objectAtIndex:i];
        
        if (!MainViewFormChemicalOther.frame.size.height) {
            BtnMainViewChemicalOther=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck+50, 30, 30)];
        }
        else{
            BtnMainViewChemicalOther=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck+45, 30, 30)];
        }
        BtnMainViewChemicalOther.tag=arrOfWorkOrderOtherChemicalList.count-1+55555;
        BtnMainViewChemicalOther.titleLabel.numberOfLines=20000;
        [BtnMainViewChemicalOther addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
        // BtnMainViewChemical.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        
        NSString *strProductAmountt,*strProductPercentt,*strUnitt,*strTargett,*strUnittNameShow,*strTargetNameShow,*strEPAReg,*strMethodInfo,*strMethodInfoToShow;
        
        strProductAmountt=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"ProductAmount"]];
        strProductPercentt=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"DefaultPercentage"]];
        strUnitt=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"UnitId"]];
        strTargett=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"Targets"]];
        strEPAReg=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"EPARegNumber"]];
        strMethodInfo=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"ApplicationMethods"]];
        
        for (int j=0; j<arrOfUnitMaster.count; j++) {
            
            NSDictionary *dict=arrOfUnitMaster[j];
            NSString *strUId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitId"]];
            NSString *strUnitNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitName"]];
            if ([strUId isEqualToString:strUnitt]) {
                
                strUnittNameShow=strUnitNamee;
                
            }
            
        }
        
        NSMutableArray *arrTempAppNAme=[[NSMutableArray alloc]init];
        
        for (int j=0; j<arrOfTargetMaster.count; j++) {
            
            NSDictionary *dict=arrOfTargetMaster[j];
            NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetId"]];
            NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetName"]];
            NSArray *arrTempAppMethodInfoId=[strTargett componentsSeparatedByString:@","];
            if ([arrTempAppMethodInfoId containsObject:strApplicationMethodId]) {
                
                [arrTempAppNAme addObject:strApplicationMethodIdNamee];
                
                strTargetNameShow=[arrTempAppNAme componentsJoinedByString:@","];
                
            }
            
        }
        
        arrTempAppNAme=[[NSMutableArray alloc]init];
        
        for (int j=0; j<arrOfAppMethodInfoMaster.count; j++) {
            
            NSDictionary *dict=arrOfAppMethodInfoMaster[j];
            NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodId"]];
            NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodName"]];
            NSArray *arrTempAppMethodInfoId=[strMethodInfo componentsSeparatedByString:@","];
            if ([arrTempAppMethodInfoId containsObject:strApplicationMethodId]) {
                
                [arrTempAppNAme addObject:strApplicationMethodIdNamee];
                
                strMethodInfoToShow=[arrTempAppNAme componentsJoinedByString:@","];
                
            }
            
        }
        
        
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [BtnMainViewChemicalOther setEnabled:NO];
        } else {
            [BtnMainViewChemicalOther setEnabled:YES];
        }
        
        [_scrollForDynamicView addSubview:BtnMainViewChemicalOther];
        
        [arrayOfButtonsMainChemicalOther addObject:BtnMainViewChemicalOther];
        
        
        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
        btnHidden.frame =CGRectMake(0, BtnMainViewChemicalOther.frame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 45);
        btnHidden.tag=arrOfWorkOrderOtherChemicalList.count-1+55555;
        [btnHidden setBackgroundColor:[UIColor clearColor]];
        [btnHidden addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
        [_scrollForDynamicView addSubview:btnHidden];
        [arrayOfButtonsMainChemicalOtherHidden addObject:btnHidden];
        
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnHidden setEnabled:NO];
        } else {
            [btnHidden setEnabled:YES];
        }
        
        // Form Creation
        MainViewFormChemicalOther=[[UIView alloc]init];
        
        MainViewFormChemicalOther.frame=CGRectMake(0, yXisForBtnCheckUncheck+BtnMainViewChemicalOther.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 140);
        
        MainViewFormChemicalOther.tag=arrOfWorkOrderOtherChemicalList.count-1+99999;
        MainViewFormChemicalOther.backgroundColor=[UIColor whiteColor];
        
        [_scrollForDynamicView addSubview:MainViewFormChemicalOther];
        
        UILabel *lblTitleSection=[[UILabel alloc]init];
        lblTitleSection.backgroundColor=[UIColor lightGrayColor];
        lblTitleSection.layer.borderWidth = 1.5f;
        lblTitleSection.layer.cornerRadius = 4.0f;
        lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
        
        lblTitleSection.frame=CGRectMake(BtnMainViewChemicalOther.frame.origin.x+BtnMainViewChemicalOther.frame.size.width+5, BtnMainViewChemicalOther.frame.origin.y, [UIScreen mainScreen].bounds.size.width-BtnMainViewChemicalOther.frame.origin.x-BtnMainViewChemicalOther.frame.size.width-15, 30);
        
        lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"ProductName"]];
        
        lblTitleSection.textAlignment=NSTextAlignmentLeft;
        
        [arrayProductNameOther addObject:lblTitleSection];
        
        [_scrollForDynamicView addSubview:lblTitleSection];
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt EPAReg-----------------
        //============================================================================
        //============================================================================
        //    UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        
        UITextField *txtFieldEPAReg=[[UITextField alloc]init];
        txtFieldEPAReg.placeholder=@"EPAReg #";
        txtFieldEPAReg.text=strEPAReg;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        txtFieldEPAReg.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldEPAReg.layer.borderWidth = 1.5f;
        txtFieldEPAReg.layer.cornerRadius = 4.0f;
        txtFieldEPAReg.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldEPAReg.tag=arrOfWorkOrderOtherChemicalList.count-1+350000;
        
        NSString *strJugad2=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldEPAReg.tag,txtFieldEPAReg.text];
        
        [arrEPARegNumberOther addObject:strJugad2];
        
        [arrOfTxtFieldsChemicalList addObject:txtFieldEPAReg];
        
        txtFieldEPAReg.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldEPAReg setEnabled:NO];
        } else {
            [txtFieldEPAReg setEnabled:YES];
        }
        //   txtFieldAmount.inputView=vieww;
        txtFieldEPAReg.frame=CGRectMake(BtnMainViewChemicalOther.frame.origin.x-10, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemicalOther addSubview:txtFieldEPAReg];
        
        
        
        //============================================================================
        //============================================================================
#pragma mark----------------------Txt Percentage-----------------
        //============================================================================
        //============================================================================
        
        UITextField *txtFieldPercentage=[[UITextField alloc]init];
        txtFieldPercentage.placeholder=@"Percentage";
        txtFieldPercentage.text=strProductPercentt;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"DefaultPercentage"]];
        txtFieldPercentage.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldPercentage.layer.borderWidth = 1.5f;
        txtFieldPercentage.layer.cornerRadius = 4.0f;
        txtFieldPercentage.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldPercentage.tag=arrOfWorkOrderOtherChemicalList.count-1+300000;
        
        NSString *strJugad=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldPercentage.tag,txtFieldPercentage.text];
        
        [arrTxtPercentOther addObject:strJugad];
        [arrOfTxtFieldsChemicalList addObject:txtFieldPercentage];
        
        //    txtFieldPercentage.inputView=vieww;
        txtFieldPercentage.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldPercentage setEnabled:NO];
        } else {
            [txtFieldPercentage setEnabled:YES];
        }
        txtFieldPercentage.frame=CGRectMake(txtFieldEPAReg.frame.origin.x+txtFieldEPAReg.frame.size.width+5, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemicalOther addSubview:txtFieldPercentage];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt Amount-----------------
        //============================================================================
        //============================================================================
        //    UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        
        UITextField *txtFieldAmount=[[UITextField alloc]init];
        txtFieldAmount.placeholder=@"Amount";
        txtFieldAmount.text=strProductAmountt;//[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        txtFieldAmount.keyboardType=UIKeyboardTypeDecimalPad;
        txtFieldAmount.layer.borderWidth = 1.5f;
        txtFieldAmount.layer.cornerRadius = 4.0f;
        txtFieldAmount.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldAmount.tag=arrOfWorkOrderOtherChemicalList.count-1+250000;
        
        NSString *strJugad1=[NSString stringWithFormat:@"%ld,%@",(long)txtFieldAmount.tag,txtFieldAmount.text];
        [arrTxtAmountOther addObject:strJugad1];
        [arrOfTxtFieldsChemicalList addObject:txtFieldAmount];
        
        txtFieldAmount.delegate=self;
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [txtFieldAmount setEnabled:NO];
        } else {
            [txtFieldAmount setEnabled:YES];
        }
        
        //   txtFieldAmount.inputView=vieww;
        txtFieldAmount.frame=CGRectMake(txtFieldEPAReg.frame.origin.x, txtFieldEPAReg.frame.origin.y+txtFieldEPAReg.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemicalOther addSubview:txtFieldAmount];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Unit-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnUnit=[[UIButton alloc]init];
        
        btnUnit.frame=CGRectMake(txtFieldPercentage.frame.origin.x, txtFieldEPAReg.frame.origin.y+txtFieldEPAReg.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strUnittNameShow.length==0) {
            [btnUnit setTitle:@"Select Unit" forState:UIControlStateNormal];
            
        } else {
            [btnUnit setTitle:strUnittNameShow forState:UIControlStateNormal];
            
        }
        [btnUnit setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnUnit.tag=arrOfWorkOrderOtherChemicalList.count-1+150000;
        
        [btnUnit addTarget:self action:@selector(actionUnitListOther:) forControlEvents:UIControlEventTouchDown];
        [arrayOfUnitOther addObject:btnUnit];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnUnit setEnabled:NO];
        } else {
            [btnUnit setEnabled:YES];
        }
        [MainViewFormChemicalOther addSubview:btnUnit];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn AppMethodInfo-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnAppMethodInfo=[[UIButton alloc]init];
        
        btnAppMethodInfo.frame=CGRectMake(txtFieldAmount.frame.origin.x, txtFieldAmount.frame.origin.y+txtFieldAmount.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strMethodInfoToShow.length==0) {
            [btnAppMethodInfo setTitle:@"Select Method" forState:UIControlStateNormal];
            
        } else {
            [btnAppMethodInfo setTitle:strMethodInfoToShow forState:UIControlStateNormal];
            
        }
        [btnAppMethodInfo setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnAppMethodInfo.tag=arrOfWorkOrderOtherChemicalList.count-1+400000;
        
        NSString *strCombinedAppMethodId=[NSString stringWithFormat:@"%ld,%@",(long)btnAppMethodInfo.tag,strMethodInfo];
        [arrAppMethodInfoIdOther addObject:strCombinedAppMethodId];
        
        [btnAppMethodInfo addTarget:self action:@selector(actionAppMethodInfoListOther:) forControlEvents:UIControlEventTouchDown];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnAppMethodInfo setEnabled:NO];
        } else {
            [btnAppMethodInfo setEnabled:YES];
        }
        [arrayOfAppMethodInfoOther addObject:btnAppMethodInfo];
        [MainViewFormChemicalOther addSubview:btnAppMethodInfo];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Target-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnTarget=[[UIButton alloc]init];
        
        btnTarget.frame=CGRectMake(btnUnit.frame.origin.x, btnUnit.frame.origin.y+btnUnit.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        if (strTargetNameShow.length==0) {
            [btnTarget setTitle:@"Select Target" forState:UIControlStateNormal];
            
        } else {
            [btnTarget setTitle:strTargetNameShow forState:UIControlStateNormal];
            
        }
        [btnTarget setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnTarget.tag=arrOfWorkOrderOtherChemicalList.count-1+200000;
        
        NSString *strCombinedTargetId=[NSString stringWithFormat:@"%ld,%@",(long)btnTarget.tag,strTargett];
        [arrTargetIdOther addObject:strCombinedTargetId];
        
        [btnTarget addTarget:self action:@selector(actionTargetListOther:) forControlEvents:UIControlEventTouchDown];
        [arrayOfTargetOther addObject:btnTarget];
        [MainViewFormChemicalOther addSubview:btnTarget];
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            [btnTarget setEnabled:NO];
        } else {
            [btnTarget setEnabled:YES];
        }
        
        
        scrollViewHeight=scrollViewHeight+MainViewFormChemicalOther.frame.size.height;
        
        [arrayViewsChemicalOther addObject:MainViewFormChemicalOther];
        
        CGFloat heightTemp=MainViewFormChemicalOther.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewFormChemicalOther.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        
        [arrOfHeightsViewsChemicalOther addObject:strTempHeight];
        
        [arrOfYAxisViewsChemicalOther addObject:strTempYAxis];
        
        NSString *strChecked =[NSString stringWithFormat:@"%@",[dictOtherProductName valueForKey:@"Checked"]];
        
        
        if ([strChecked isEqualToString:@"true"] || [strChecked isEqualToString:@"TRUE"]) {
            isMatchedProductId=YES;
            
            [BtnMainViewChemicalOther setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            NSString *strTag=[NSString stringWithFormat:@"%ld",(long)BtnMainViewChemicalOther.tag];
            
            [arrSelectedChemicalsOther addObject:strTag];
            
        } else {
            
            [arrSetHeightZeroOther addObject:@"1"];
            
            [BtnMainViewChemicalOther setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
        }
        
        if (!isMatchedProductId) {
            
            UIView *viewTempp=[arrayViewsChemicalOther objectAtIndex:arrayOfButtonsMainChemicalOther.count-1];
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[arrayOfButtonsMainChemicalOther.count-1];
            [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
            [viewTempp setHidden:YES];
            
            yXisForBtnCheckUncheck=yXisForBtnCheckUncheck+62;
            
            
        }else{
            yXisForBtnCheckUncheck=yXisForBtnCheckUncheck+150;
            
        }
    }
    
    
    _scrollForDynamicView.backgroundColor=[UIColor clearColor];
    
    _viewOtherChemicalList.frame=CGRectMake(0, _viewOtherChemicalList.frame.origin.y, _viewOtherChemicalList.frame.size.width, arrOfWorkOrderOtherChemicalList.count*150+62);
    
    CGRect frameForViewOtherChemicalList;
    if (MainViewFormChemicalOther.frame.size.height==0) {
        
        if (!(arrayOfButtonsMainChemicalOther.count==0)) {
            UIView *viewTempp=[arrayViewsChemicalOther objectAtIndex:arrayOfButtonsMainChemicalOther.count-1];
            
            frameForViewOtherChemicalList=CGRectMake(10, viewTempp.frame.origin.y+viewTempp.frame.size.height, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForViewOtherChemicalList.origin.x=10;
            frameForViewOtherChemicalList.origin.y=viewTempp.frame.origin.y+viewTempp.frame.size.height+20;
        }else{
            
            frameForViewOtherChemicalList=CGRectMake(10, MainViewFormChemical.frame.size.height+MainViewFormChemical.frame.origin.y+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForViewOtherChemicalList.origin.x=10;
            frameForViewOtherChemicalList.origin.y=MainViewFormChemical.frame.size.height+MainViewFormChemical.frame.origin.y+10;
            
        }
    }else{
        if (MainViewFormChemicalOther==nil) {
            //MainViewForm.frame.size.height+MainViewForm.frame.origin.y+10
            
            frameForViewOtherChemicalList=CGRectMake(10, MainViewFormChemical.frame.size.height+MainViewFormChemical.frame.origin.y+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForViewOtherChemicalList.origin.x=10;
            frameForViewOtherChemicalList.origin.y=MainViewFormChemical.frame.size.height+MainViewFormChemical.frame.origin.y+10;
            
            
        }else{
            //MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
            frameForViewOtherChemicalList=CGRectMake(10, MainViewFormChemicalOther.frame.origin.y+MainViewFormChemicalOther.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
            frameForViewOtherChemicalList.origin.x=10;
            frameForViewOtherChemicalList.origin.y=MainViewFormChemicalOther.frame.origin.y+MainViewFormChemicalOther.frame.size.height+80;
        }
    }
    
    [_viewOtherChemicalList setFrame:MainViewFormChemical.frame];
    
    //  [_scrollForDynamicView addSubview:_viewOtherChemicalList];
    
    //**************************************
#pragma mark- Add Other Chemical List View Settings
    //**************************************
    
    
    CGRect frameForAddOtherChemical=CGRectMake(10, MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+20, self.view.frame.size.width-20, _viewAddOtherProductList.frame.size.height);
    frameForAddOtherChemical.origin.x=10;
    if (_viewOtherChemicalList.frame.size.height==0) {
        frameForAddOtherChemical.origin.y=_viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+10;
        
    }else{//MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
        if (MainViewFormChemicalOther.frame.size.height==0) {
            
            frameForAddOtherChemical.origin.y=_viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+20;
            
        }else{
            frameForAddOtherChemical.origin.y=_viewOtherChemicalList.frame.origin.y;//+_viewOtherChemicalList.frame.size.height+20;
        }
    }
    
    // frameForAddOtherChemical.origin.y=frameForAddOtherChemical.origin.y-500;
    
    [_viewAddOtherProductList setFrame:frameForAddOtherChemical];
    
    //  [_scrollForDynamicView addSubview:_viewAddOtherProductList];
    
    
    // Service job descriptions Changes
    
    CGRect frameForServiceJobDescriptions;
    frameForServiceJobDescriptions=CGRectMake(0, _viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+10, self.view.frame.size.width, heightForServiceJobDescriptions);
    frameForServiceJobDescriptions.origin.x=0;
    frameForServiceJobDescriptions.origin.y=_viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+80;
    [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
    
    
    CGRect frameForPaymentInfo;
    frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
    frameForPaymentInfo.origin.x=10;
    frameForPaymentInfo.origin.y=_viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+80;
    [_paymentInfoView setFrame:frameForPaymentInfo];
    
    //  [_scrollForDynamicView addSubview:_paymentInfoView];
    
    //**************************************
#pragma mark- Payment Type View Settings
    //**************************************
    
    
    CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
    frameForPaymentType.origin.x=10;
    if (_paymentInfoView.frame.size.height==0) {
        frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
        
    }else{//MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
        if (MainViewFormChemicalOther.frame.size.height==0) {
            
            frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20;
            
        }else{
            frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20;
        }
    }
    
    
    [_paymentTypeView setFrame:frameForPaymentType];
    
    // [_scrollForDynamicView addSubview:_paymentTypeView];
    
    
    
    //**************************************
#pragma mark- Amount And Check View Settings
    //**************************************
    
    
    CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
    frameForCheckView.origin.x=10;
    frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    
    [_amountViewSingle setFrame:frameForCheckView];
    
    //  [_scrollForDynamicView addSubview:_amountViewSingle];
    
    
    CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
    frameForbeforeImageView.origin.x=10;
    frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
    
    [_beforeImageView setFrame:frameForbeforeImageView];
    
    // [_scrollForDynamicView addSubview:_beforeImageView];
    
    if (!(arrOfBeforeImageAll.count==0)) {
        
        CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
        frameForbeforeImageViewThumnNail.origin.x=10;
        frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
        
        //   [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
        
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        //   [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
        
    }else{
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        //  [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
    }
    
    
    
    //  _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, _lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y+600);
    
    
    //  [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y+600)];
    
    // [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+arrOfWorkOrderOtherChemicalList.count*150)];
    
    
    if (arrResponseInspection.count==0) {
        
        //   _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, _scrollForDynamicView.frame.size.height+arrOfProductMaster.count*150+200+_amountViewSingle.frame.size.height+_lastViewAfterImage.frame.size.height+_paymentTypeView.frame.size.height+_paymentInfoView.frame.size.height-arrSetHeightZeroOther.count*100+250);
        
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+arrOfProductMaster.count*150+200+_amountViewSingle.frame.size.height+_lastViewAfterImage.frame.size.height+_paymentTypeView.frame.size.height+_paymentInfoView.frame.size.height+heightForServiceJobDescriptions-arrSetHeightZeroOther.count*100+250)];
        
    }
    
    [self performSelector:@selector(actionOpenCloseChemicalListAtStartingOtherOnlyOnce:) withObject:nil afterDelay:0.0];
    
    [self defaultViewSettingForAmount];
    
    
}


// Change for iOS 10

//============================================================================
//============================================================================
#pragma mark- ---------------------Open Close MainView METHODS-----------------
//============================================================================
//============================================================================
-(void)actionOpenCloseChemicalList:(id)sender
{
    
    [self HideALlTextFieldsOpenedChemicalList];
    
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    BOOL isPlus;
    isPlus=NO;
    CGFloat heightToPlusOrMinus=0.0;
    
    UIButton *btn = (UIButton *)sender;
    int tagggg;
    if (btn==nil) {
        tagggg=0;
    }else{
        
        tagggg=btn.tag-5555;
        
    }
    
    UIButton *btnTempp=[arrayOfButtonsMainChemical objectAtIndex:tagggg];
    
    UILabel *lblTempp=[arrayProductName objectAtIndex:tagggg];
    
    [arrayOfButtonsMainChemical replaceObjectAtIndex:tagggg withObject:btnTempp];
    
    UIView *viewTempp=[arrayViewsChemical objectAtIndex:tagggg];
    
    if (viewTempp.frame.size.height==0) {
        
        isPlus=YES;
        
        UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[tagggg];
        [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
        NSString *strTempHeightLast=arrOfHeightsViewsChemical[tagggg];
        CGFloat hiTempLast=[strTempHeightLast floatValue];
        
        [viewTempp setFrame:CGRectMake(0, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, [UIScreen mainScreen].bounds.size.width, hiTempLast)];
        
        heightToPlusOrMinus=viewTempp.frame.size.height;
        
        //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTempp.frame.size.height)];
        NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
        
        [arrSelectedChemicals addObject:strTagg];
        NSArray *arrTempp=[dictChemicalList valueForKey:@"ProductMasters"];
        int indexxxx=[strTagg intValue];
        indexxxx=indexxxx-5555;
        NSDictionary *dictOfData=arrTempp[indexxxx];
        [arrOfSelectedChemicalToSendToNextView addObject:dictOfData];
        
        [viewTempp setHidden:NO];
    } else {
        
        isPlus=NO;
        
        UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[tagggg];
        [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
        NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
        
        [arrSelectedChemicals removeObject:strTagg];
        int indexxxx=[strTagg intValue];
        indexxxx=indexxxx-5555;
        NSArray *arrTempp=[dictChemicalList valueForKey:@"ProductMasters"];
        NSDictionary *dictOfData=arrTempp[indexxxx];
        [arrOfSelectedChemicalToSendToNextView removeObject:dictOfData];
        
        //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTempp.frame.size.height)];
        heightToPlusOrMinus=viewTempp.frame.size.height;
        
        [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
        [viewTempp setHidden:YES];
    }
    [arrayViewsChemical replaceObjectAtIndex:tagggg withObject:viewTempp];
    
    for (int k=tagggg+1; k<arrayViewsChemical.count; k++) {
        
        
        UIButton *btnTemppNew=[arrayOfButtonsMainChemical objectAtIndex:k];
        UIButton *btnTemppNewHidden=[arrayOfButtonsMainChemicalHidden objectAtIndex:k];
        UILabel *lblTemppNew=[arrayProductName objectAtIndex:k];
        UIView *viewTemppNew=[arrayViewsChemical objectAtIndex:k];
        UIView *viewTemppNewLasttt;
        CGFloat hiTempLast;
        if (k==0) {
            viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
        } else {
            viewTemppNewLasttt=[arrayViewsChemical objectAtIndex:k-1];
            NSString *strTempHeightLast=arrOfHeightsViewsChemical[k-1];
            hiTempLast =[strTempHeightLast floatValue];
            
        }
        [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
        
        [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
        [btnTemppNew addTarget:self action:@selector(actionOpenCloseChemicalList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfButtonsMainChemical replaceObjectAtIndex:k withObject:btnTemppNew];
        
        [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
        [btnTemppNewHidden addTarget:self action:@selector(actionOpenCloseChemicalList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfButtonsMainChemicalHidden replaceObjectAtIndex:k withObject:btnTemppNewHidden];
        
        
        [viewTemppNew setFrame:CGRectMake(0, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
        if (viewTemppNew.frame.size.height==0) {
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[k];//uncheck.png
            [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            [viewTemppNew setHidden:YES];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTemppNew.frame.size.height)];
            // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
            // NSDictionary *dictOfData=arrTempp[k];
            // [arrOfSelectedChemicalToSendToNextView removeObject:dictOfData];
            
        }else{
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[k];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            [viewTemppNew setHidden:NO];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTemppNew.frame.size.height)];
            //  NSArray *arrTempp=[dictChemicalList valueForKey:@""];
            // NSDictionary *dictOfData=arrTempp[k];
            // [arrOfSelectedChemicalToSendToNextView addObject:dictOfData];
            
        }
        [arrayViewsChemical replaceObjectAtIndex:k withObject:viewTemppNew];
    }
    
    
    UIView *viewChemicalListLast=arrayViewsChemical[arrayViewsChemical.count-1];
    
    
    CGRect frameForOtherChemicalList=CGRectMake(10, viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
    frameForOtherChemicalList.origin.x=10;
    frameForOtherChemicalList.origin.y=viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+80;
    
    [_viewOtherChemicalList setFrame:frameForOtherChemicalList];
    
    
    CGRect frameForAddChemicalList=CGRectMake(10, _viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+10, self.view.frame.size.width-20, _viewAddOtherProductList.frame.size.height);
    frameForAddChemicalList.origin.x=10;
    frameForAddChemicalList.origin.y=_viewOtherChemicalList.frame.origin.y;//+_viewOtherChemicalList.frame.size.height+80;
    
    [_viewAddOtherProductList setFrame:frameForAddChemicalList];
    
    
    // New change  Service Job Descriptions
    
#pragma mark- Service Job Descriptions View Settings
    
    
    CGRect frameForServiceJobDescriptions=CGRectMake(0, _viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+10, self.view.frame.size.width, heightForServiceJobDescriptions);
    frameForServiceJobDescriptions.origin.x=0;
    frameForServiceJobDescriptions.origin.y=_viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+80;
    
    [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
    
    
    // New change to change positions
    
    //**************************************
#pragma mark- Payment Info View Settings
    //**************************************
    
    
    CGRect frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
    frameForPaymentInfo.origin.x=10;
    frameForPaymentInfo.origin.y=_viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+80;
    
    [_paymentInfoView setFrame:frameForPaymentInfo];
    
    
    //**************************************
#pragma mark- Payment Type View Settings
    //**************************************
    
    
    CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
    frameForPaymentType.origin.x=10;
    frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
    
    [_paymentTypeView setFrame:frameForPaymentType];
    
    
    //**************************************
#pragma mark- Amount And Check View Settings
    //**************************************
    
    
    CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
    frameForCheckView.origin.x=10;
    frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    [_amountViewSingle setFrame:frameForCheckView];
    
    
    //**************************************
#pragma mark- After Image View Settings
    //**************************************
    
    
    //Yaha Change Kiya
    CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
    frameForbeforeImageView.origin.x=10;
    frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
    
    [_beforeImageView setFrame:frameForbeforeImageView];
    
    [_scrollForDynamicView addSubview:_beforeImageView];
    
    if (!(arrOfBeforeImageAll.count==0)) {
        
        CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
        frameForbeforeImageViewThumnNail.origin.x=10;
        frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
        
        [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
        
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
        
    }else{
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
    }
    
    if (isPlus)
    {
        //Nilind 21 Feb Add 200
        // [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+heightToPlusOrMinus)];
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+heightToPlusOrMinus+100)];
        
        //End
        //  NSLog(@"Height Plus Ho ri hai ===%@",_scrollForDynamicView);
        
    }else
    {
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-heightToPlusOrMinus)];
        
        //  NSLog(@"Height Minus Ho ri hai  ===%@",_scrollForDynamicView);
        
    }
    
    if (arrayOfButtonsMainChemicalOther.count==0) {
        
        [labelChemicalHeadGlobal setHidden:YES];
        
    } else {
        
        [labelChemicalHeadGlobal setHidden:NO];
        
        UIButton *btnTemppFirst=[arrayOfButtonsMainChemicalOther objectAtIndex:0];
        CGFloat HeightFirst=btnTemppFirst.frame.origin.y;
        
        UIButton *btnTemppLast=[arrayOfButtonsMainChemical objectAtIndex:arrayOfButtonsMainChemical.count-1];
        CGFloat HeightLast=btnTemppLast.frame.origin.y;
        
        CGFloat valueToSet=HeightFirst-HeightLast;
        
        if (!forFirstTimeToOpen) {
            forFirstTimeToOpen=YES;
            if (isPlus) {
                
                for (int k1=0; k1<arrayOfButtonsMainChemicalOther.count; k1++) {
                    
                    UIButton *btnTempp=[arrayOfButtonsMainChemicalOther objectAtIndex:k1];
                    
                    [btnTempp setFrame:CGRectMake(btnTempp.frame.origin.x, btnTempp.frame.origin.y-valueToSet+30+190, btnTempp.frame.size.width, btnTempp.frame.size.height)];
                    
                    [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:k1 withObject:btnTempp];
                    
                    
                    UIButton *btnTemppHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:k1];
                    
                    [btnTemppHidden setFrame:CGRectMake(btnTemppHidden.frame.origin.x, btnTempp.frame.origin.y-10, btnTemppHidden.frame.size.width, btnTemppHidden.frame.size.height)];
                    
                    [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:k1 withObject:btnTemppHidden];
                    
                    UILabel *lblTemppNew=[arrayProductNameOther objectAtIndex:k1];
                    
                    [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, lblTemppNew.frame.origin.y-valueToSet+30+190, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                    
                    [arrayProductNameOther replaceObjectAtIndex:k1 withObject:lblTemppNew];
                    
                    UIView *viewTemppNew=[arrayViewsChemicalOther objectAtIndex:k1];
                    
                    [viewTemppNew setFrame:CGRectMake(viewTemppNew.frame.origin.x, viewTemppNew.frame.origin.y-valueToSet+30+190, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
                    
                    [arrayViewsChemicalOther replaceObjectAtIndex:k1 withObject:viewTemppNew];
                    
                }
                
            }else{
                
                for (int k1=0; k1<arrayOfButtonsMainChemicalOther.count; k1++) {
                    
                    UIButton *btnTempp=[arrayOfButtonsMainChemicalOther objectAtIndex:k1];
                    
                    [btnTempp setFrame:CGRectMake(btnTempp.frame.origin.x, btnTempp.frame.origin.y-140, btnTempp.frame.size.width, btnTempp.frame.size.height)];
                    
                    [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:k1 withObject:btnTempp];
                    
                    UIButton *btnTemppHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:k1];
                    
                    [btnTemppHidden setFrame:CGRectMake(btnTemppHidden.frame.origin.x, btnTempp.frame.origin.y-10, btnTemppHidden.frame.size.width, btnTemppHidden.frame.size.height)];
                    
                    [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:k1 withObject:btnTemppHidden];
                    
                    UILabel *lblTemppNew=[arrayProductNameOther objectAtIndex:k1];
                    
                    [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, lblTemppNew.frame.origin.y-140, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                    
                    [arrayProductNameOther replaceObjectAtIndex:k1 withObject:lblTemppNew];
                    
                    UIView *viewTemppNew=[arrayViewsChemicalOther objectAtIndex:k1];
                    
                    [viewTemppNew setFrame:CGRectMake(viewTemppNew.frame.origin.x, viewTemppNew.frame.origin.y-140, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
                    
                    [arrayViewsChemicalOther replaceObjectAtIndex:k1 withObject:viewTemppNew];
                    
                }
            }
        }
        
        forFirstTimeToOpen=NO;
        
        [self performSelector:@selector(actionOpenCloseChemicalListAtStartingOtherNeww:) withObject:nil afterDelay:0.0];
        
    }
    [self setScrollHeightonClick];
}


-(void)actionOpenCloseChemicalListOther:(id)sender
{
    [self HideALlTextFieldsOpenedChemicalList];
    
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    BOOL isPlus;
    isPlus=NO;
    CGFloat heightToPlusOrMinus=0.0;
    
    UIButton *btn = (UIButton *)sender;
    int tagggg;
    if (btn==nil) {
        tagggg=0;
    }else{
        
        tagggg=btn.tag-55555;
        
    }
    
    UIButton *btnTempp=[arrayOfButtonsMainChemicalOther objectAtIndex:tagggg];
    
    [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:tagggg withObject:btnTempp];
    
    UIButton *btnTemppHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:tagggg];
    
    [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:tagggg withObject:btnTemppHidden];
    
    
    UIView *viewTempp=[arrayViewsChemicalOther objectAtIndex:tagggg];
    
    if (viewTempp.frame.size.height==0) {
        
        isPlus=YES;
        
        UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[tagggg];
        [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
        NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[tagggg];
        CGFloat hiTempLast=[strTempHeightLast floatValue];
        
        [viewTempp setFrame:CGRectMake(0, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, [UIScreen mainScreen].bounds.size.width, hiTempLast)];
        
        heightToPlusOrMinus=viewTempp.frame.size.height;
        
        //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTempp.frame.size.height)];
        NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
        
        [arrSelectedChemicalsOther addObject:strTagg];
        int indexxxx=[strTagg intValue];
        indexxxx=indexxxx-55555;
        NSDictionary *dictOfData=arrOfSavedChemicalListOtherAllValues[indexxxx];
        
        if (strWorkOrderId.length==0) {
            strWorkOrderId=strGlobalWorkOrderId;
        }
        
        NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"WoOtherProductId"]],
                                   @"true",
                                   strWorkOrderId,
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductName"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"UnitId"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"TargetId"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"DefaultPercentage"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductAmount"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"EPARegNumber"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ApplicationMethodId"]],nil];
        NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                 @"WoOtherProductId",
                                 @"Checked",
                                 @"WorkorderId",
                                 @"ProductName",
                                 @"UnitId",
                                 @"TargetId",
                                 @"DefaultPercentage",
                                 @"ProductAmount",
                                 @"EPARegNumber",
                                 @"ApplicationMethodId",nil];
        
        NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
        
        
        [arrOfSelectedChemicalToSendToNextViewOther addObject:dict_ToSendLeadInfo];
        [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:indexxxx withObject:dict_ToSendLeadInfo];
        
        [viewTempp setHidden:NO];
        
    } else {
        
        isPlus=NO;
        
        UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[tagggg];
        [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
        NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
        
        [arrSelectedChemicalsOther removeObject:strTagg];
        int indexxxx=[strTagg intValue];
        indexxxx=indexxxx-55555;
        NSDictionary *dictOfData=arrOfSavedChemicalListOtherAllValues[indexxxx];
        
        if (strWorkOrderId.length==0) {
            strWorkOrderId=strGlobalWorkOrderId;
        }
        
        NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"WoOtherProductId"]],
                                   @"false",
                                   strWorkOrderId,
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductName"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"UnitId"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"TargetId"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"DefaultPercentage"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductAmount"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"EPARegNumber"]],
                                   [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ApplicationMethodId"]],nil];
        NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                 @"WoOtherProductId",
                                 @"Checked",
                                 @"WorkorderId",
                                 @"ProductName",
                                 @"UnitId",
                                 @"TargetId",
                                 @"DefaultPercentage",
                                 @"ProductAmount",
                                 @"EPARegNumber",
                                 @"ApplicationMethodId",nil];
        
        NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
        
        [arrOfSelectedChemicalToSendToNextViewOther removeObject:dict_ToSendLeadInfo];
        [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:indexxxx withObject:dict_ToSendLeadInfo];
        
        //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTempp.frame.size.height)];
        heightToPlusOrMinus=viewTempp.frame.size.height;
        
        [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
        [viewTempp setHidden:YES];
    }
    [arrayViewsChemicalOther replaceObjectAtIndex:tagggg withObject:viewTempp];
    
    for (int k=tagggg+1; k<arrayViewsChemicalOther.count; k++) {
        
        
        UIButton *btnTemppNew=[arrayOfButtonsMainChemicalOther objectAtIndex:k];
        UIButton *btnTemppNewHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:k];
        UILabel *lblTemppNew=[arrayProductNameOther objectAtIndex:k];
        UIView *viewTemppNew=[arrayViewsChemicalOther objectAtIndex:k];
        UIView *viewTemppNewLasttt;
        CGFloat hiTempLast;
        if (k==0) {
            viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
        } else {
            viewTemppNewLasttt=[arrayViewsChemicalOther objectAtIndex:k-1];
            NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[k-1];
            hiTempLast =[strTempHeightLast floatValue];
            
        }
        [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
        
        [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
        [btnTemppNew addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
        [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:k withObject:btnTemppNew];
        
        
        [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
        [btnTemppNewHidden addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
        [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:k withObject:btnTemppNewHidden];
        
        [viewTemppNew setFrame:CGRectMake(0, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
        if (viewTemppNew.frame.size.height==0) {
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[k];//uncheck.png
            [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            [viewTemppNew setHidden:YES];
            
        }else{
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[k];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            [viewTemppNew setHidden:NO];
            
            
        }
        [arrayViewsChemicalOther replaceObjectAtIndex:k withObject:viewTemppNew];
    }
    
    //**************************************
#pragma mark- Chemical List other View Settings
    //**************************************
    
    UIView *viewChemicalListLast=arrayViewsChemicalOther[arrayViewsChemicalOther.count-1];
    
    
    CGRect frameForOtherChemical=CGRectMake(10, viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
    frameForOtherChemical.origin.x=10;
    frameForOtherChemical.origin.y=viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+80;
    
    [_viewOtherChemicalList setFrame:frameForOtherChemical];
    
    // New change to change positions
    //**************************************
#pragma mark- Add Other Chemical List View Settings
    //**************************************
    
    
    CGRect frameForOtherChemicalList=CGRectMake(10, _viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+10, self.view.frame.size.width-20, _viewAddOtherProductList.frame.size.height);
    frameForOtherChemicalList.origin.x=10;
    frameForOtherChemicalList.origin.y=_viewOtherChemicalList.frame.origin.y;//+_viewOtherChemicalList.frame.size.height+80;
    
    [_viewAddOtherProductList setFrame:frameForOtherChemicalList];
    
    // New change to change positions
    
    //**************************************
#pragma mark- _viewServiceJobDescriptions View Settings
    //**************************************
    
    CGRect frameForServiceJobDescriptions=CGRectMake(0, _viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+10, self.view.frame.size.width, heightForServiceJobDescriptions);
    frameForServiceJobDescriptions.origin.x=0;
    frameForServiceJobDescriptions.origin.y=_viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+80;
    
    [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
    
    //**************************************
#pragma mark- Payment Info View Settings
    //**************************************
    
    CGRect frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
    frameForPaymentInfo.origin.x=10;
    frameForPaymentInfo.origin.y=_viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+80;
    
    [_paymentInfoView setFrame:frameForPaymentInfo];
    
    
    //**************************************
#pragma mark- Payment Type View Settings
    //**************************************
    
    
    CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
    frameForPaymentType.origin.x=10;
    frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
    
    [_paymentTypeView setFrame:frameForPaymentType];
    
    
    //**************************************
#pragma mark- Amount And Check View Settings
    //**************************************
    
    
    CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
    frameForCheckView.origin.x=10;
    frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    [_amountViewSingle setFrame:frameForCheckView];
    
    
    //**************************************
#pragma mark- After Image View Settings
    //**************************************
    
    
    //Yaha Change Kiya
    CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
    frameForbeforeImageView.origin.x=10;
    frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
    
    [_beforeImageView setFrame:frameForbeforeImageView];
    
    [_scrollForDynamicView addSubview:_beforeImageView];
    
    if (!(arrOfBeforeImageAll.count==0)) {
        
        CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
        frameForbeforeImageViewThumnNail.origin.x=10;
        frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
        
        [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
        
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
        
    }else{
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
    }
    
    if (isPlus) {
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+heightToPlusOrMinus)];
        
        //  NSLog(@"Height Plus Ho ri hai ===%@",_scrollForDynamicView);
        
    }else
    {
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-heightToPlusOrMinus)];
        
        //  NSLog(@"Height Minus Ho ri hai  ===%@",_scrollForDynamicView);
        
    }
    
    [self defaultViewSettingForAmount];
    
    [self setScrollHeightonClick];
}

-(void)actionOpenCloseChemicalListAtStarting:(id)sender
{
    [self HideALlTextFieldsOpenedChemicalList];
    
    if (arrayOfButtonsMainChemical.count==0) {
        
    } else {
        
        BOOL isPlus;
        isPlus=NO;
        CGFloat heightToPlusOrMinus=0.0;
        
        UIButton *btn = (UIButton *)sender;
        int tagggg;
        if (btn==nil) {
            tagggg=0;
        }else{
            
            tagggg=btn.tag-5555;
            
        }
        
        UIButton *btnTempp=[arrayOfButtonsMainChemical objectAtIndex:tagggg];
        
        UILabel *lblTempp=[arrayProductName objectAtIndex:tagggg];
        
        [arrayOfButtonsMainChemical replaceObjectAtIndex:tagggg withObject:btnTempp];
        
        UIView *viewTempp=[arrayViewsChemical objectAtIndex:tagggg];
        
        if (!(viewTempp.frame.size.height==0)) {
            
            isPlus=YES;
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[tagggg];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            NSString *strTempHeightLast=arrOfHeightsViewsChemical[tagggg];
            CGFloat hiTempLast=[strTempHeightLast floatValue];
            
            [viewTempp setFrame:CGRectMake(0, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, [UIScreen mainScreen].bounds.size.width, hiTempLast)];
            
            heightToPlusOrMinus=viewTempp.frame.size.height;
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTempp.frame.size.height)];
            NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
            
            [arrSelectedChemicals addObject:strTagg];
            NSArray *arrTempp=[dictChemicalList valueForKey:@"ProductMasters"];
            int indexxxx=[strTagg intValue];
            indexxxx=indexxxx-5555;
            NSDictionary *dictOfData=arrTempp[indexxxx];
            [arrOfSelectedChemicalToSendToNextView addObject:dictOfData];
            
            [viewTempp setHidden:NO];
        } else {
            
            isPlus=NO;
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[tagggg];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
            
            [arrSelectedChemicals removeObject:strTagg];
            int indexxxx=[strTagg intValue];
            indexxxx=indexxxx-5555;
            NSArray *arrTempp=[dictChemicalList valueForKey:@"ProductMasters"];
            NSDictionary *dictOfData=arrTempp[indexxxx];
            [arrOfSelectedChemicalToSendToNextView removeObject:dictOfData];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTempp.frame.size.height)];
            heightToPlusOrMinus=viewTempp.frame.size.height;
            
            [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
            [viewTempp setHidden:YES];
        }
        [arrayViewsChemical replaceObjectAtIndex:tagggg withObject:viewTempp];
        
        for (int k=tagggg+1; k<arrayViewsChemical.count; k++) {
            
            
            UIButton *btnTemppNew=[arrayOfButtonsMainChemical objectAtIndex:k];
            UIButton *btnTemppNewHidden=[arrayOfButtonsMainChemicalHidden objectAtIndex:k];
            UILabel *lblTemppNew=[arrayProductName objectAtIndex:k];
            UIView *viewTemppNew=[arrayViewsChemical objectAtIndex:k];
            UIView *viewTemppNewLasttt;
            CGFloat hiTempLast;
            if (k==0) {
                viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
            } else {
                viewTemppNewLasttt=[arrayViewsChemical objectAtIndex:k-1];
                NSString *strTempHeightLast=arrOfHeightsViewsChemical[k-1];
                hiTempLast =[strTempHeightLast floatValue];
                
            }
            
            if (k==1){
                
                viewTemppNewLasttt=[arrayViewsChemical objectAtIndex:k-1];
                UIView *viewTemppNewLastttNewJugad;
                viewTemppNewLastttNewJugad=[arrayViewsChemical objectAtIndex:k-1];
                NSString *strTempHeightLast=arrOfHeightsViewsChemical[k-1];
                hiTempLast =[strTempHeightLast floatValue];
                
                [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                
                [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
                
                [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
                
            }else{
                
                [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                
                [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
                
                [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
                
            }
            [btnTemppNew addTarget:self action:@selector(actionOpenCloseChemicalList:) forControlEvents:UIControlEventTouchDown];
            [arrayOfButtonsMainChemical replaceObjectAtIndex:k withObject:btnTemppNew];
            
            [btnTemppNewHidden addTarget:self action:@selector(actionOpenCloseChemicalList:) forControlEvents:UIControlEventTouchDown];
            [arrayOfButtonsMainChemicalHidden replaceObjectAtIndex:k withObject:btnTemppNewHidden];
            
            [viewTemppNew setFrame:CGRectMake(0, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
            if (viewTemppNew.frame.size.height==0) {
                
                UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[k];//uncheck.png
                [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                
                [viewTemppNew setHidden:YES];
                
                //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTemppNew.frame.size.height)];
                // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
                // NSDictionary *dictOfData=arrTempp[k];
                // [arrOfSelectedChemicalToSendToNextView removeObject:dictOfData];
                
            }else{
                UIButton *btnCheckUncheck=arrayOfButtonsMainChemical[k];
                [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                
                [viewTemppNew setHidden:NO];
                
                //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTemppNew.frame.size.height)];
                // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
                // NSDictionary *dictOfData=arrTempp[k];
                // [arrOfSelectedChemicalToSendToNextView addObject:dictOfData];
                
            }
            [arrayViewsChemical replaceObjectAtIndex:k withObject:viewTemppNew];
        }
        
        
        // New change to change positions
        
        //**************************************
#pragma mark- Payment Info View Settings
        //**************************************
        
        UIView *viewChemicalListLast=arrayViewsChemical[arrayViewsChemical.count-1];
        
        
        CGRect frameForPaymentInfo=CGRectMake(10, viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
        frameForPaymentInfo.origin.x=10;
        frameForPaymentInfo.origin.y=viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+80;
        
        [_viewOtherChemicalList setFrame:frameForPaymentInfo];
        
        if (isPlus) {
            
            [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+heightToPlusOrMinus)];
            
            NSLog(@"Height Plus Ho ri hai ===%@",_scrollForDynamicView);
            
        }else
        {
            
            [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-heightToPlusOrMinus)];
            
            NSLog(@"Height Minus Ho ri hai  ===%@",_scrollForDynamicView);
            
        }
    }
    
    [self otherchemicalListDynamicViewCreations:dictChemicalList];
    
    [self setScrollHeightonClick];
}

-(void)actionOpenCloseChemicalListAtStartingOther:(id)sender
{
    
    [self HideALlTextFieldsOpenedChemicalList];
    
    if (arrayOfButtonsMainChemicalOther.count==0) {
        
    } else {
        
        BOOL isPlus;
        isPlus=NO;
        CGFloat heightToPlusOrMinus=0.0;
        
        UIButton *btn = (UIButton *)sender;
        int tagggg;
        if (btn==nil) {
            tagggg=0;
        }else{
            
            tagggg=btn.tag-55555;
            
        }
        
        UIButton *btnTempp=[arrayOfButtonsMainChemicalOther objectAtIndex:tagggg];
        
        [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:tagggg withObject:btnTempp];
        
        UIButton *btnTemppHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:tagggg];
        
        [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:tagggg withObject:btnTemppHidden];
        
        UIView *viewTempp=[arrayViewsChemicalOther objectAtIndex:tagggg];
        
        if (!(viewTempp.frame.size.height==0)) {
            
            isPlus=YES;
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[tagggg];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[tagggg];
            CGFloat hiTempLast=[strTempHeightLast floatValue];
            
            [viewTempp setFrame:CGRectMake(0, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, [UIScreen mainScreen].bounds.size.width, hiTempLast)];
            
            heightToPlusOrMinus=viewTempp.frame.size.height;
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTempp.frame.size.height)];
            NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
            
            [arrSelectedChemicalsOther addObject:strTagg];
            int indexxxx=[strTagg intValue];
            indexxxx=indexxxx-55555;
            NSDictionary *dictOfData=arrOfSavedChemicalListOtherAllValues[indexxxx];
            
            if (strWorkOrderId.length==0) {
                strWorkOrderId=strGlobalWorkOrderId;
            }
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"WoOtherProductId"]],
                                       @"true",
                                       strWorkOrderId,
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductName"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"UnitId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"TargetId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"DefaultPercentage"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductAmount"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"EPARegNumber"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ApplicationMethodId"]],nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoOtherProductId",
                                     @"Checked",
                                     @"WorkorderId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"TargetId",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethodId",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrOfSelectedChemicalToSendToNextViewOther addObject:dict_ToSendLeadInfo];
            [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:indexxxx withObject:dict_ToSendLeadInfo];
            
            [viewTempp setHidden:NO];
        } else {
            
            isPlus=NO;
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[tagggg];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
            
            [arrSelectedChemicalsOther removeObject:strTagg];
            int indexxxx=[strTagg intValue];
            indexxxx=indexxxx-55555;
            NSDictionary *dictOfData=arrOfSavedChemicalListOtherAllValues[indexxxx];
            
            if (strWorkOrderId.length==0) {
                strWorkOrderId=strGlobalWorkOrderId;
            }
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"WoOtherProductId"]],
                                       @"false",
                                       strWorkOrderId,
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductName"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"UnitId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"TargetId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"DefaultPercentage"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductAmount"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"EPARegNumber"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ApplicationMethodId"]],nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoOtherProductId",
                                     @"Checked",
                                     @"WorkorderId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"TargetId",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethodId",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrOfSelectedChemicalToSendToNextViewOther removeObject:dict_ToSendLeadInfo];
            [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:indexxxx withObject:dict_ToSendLeadInfo];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTempp.frame.size.height)];
            heightToPlusOrMinus=viewTempp.frame.size.height;
            
            [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
            [viewTempp setHidden:YES];
        }
        [arrayViewsChemicalOther replaceObjectAtIndex:tagggg withObject:viewTempp];
        
        for (int k=tagggg+1; k<arrayViewsChemicalOther.count; k++) {
            
            
            UIButton *btnTemppNew=[arrayOfButtonsMainChemicalOther objectAtIndex:k];
            UIButton *btnTemppNewHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:k];
            UILabel *lblTemppNew=[arrayProductNameOther objectAtIndex:k];
            UIView *viewTemppNew=[arrayViewsChemicalOther objectAtIndex:k];
            UIView *viewTemppNewLasttt;
            CGFloat hiTempLast;
            if (k==0) {
                viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
            } else {
                viewTemppNewLasttt=[arrayViewsChemicalOther objectAtIndex:k-1];
                NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[k-1];
                hiTempLast =[strTempHeightLast floatValue];
                
            }
            
            if (k==1){
                
                viewTemppNewLasttt=[arrayViewsChemicalOther objectAtIndex:k-1];
                UIView *viewTemppNewLastttNewJugad;
                viewTemppNewLastttNewJugad=[arrayViewsChemicalOther objectAtIndex:k-1];
                NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[k-1];
                hiTempLast =[strTempHeightLast floatValue];
                
                [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                
                [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
                
                [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
                
                
            }else{
                
                [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                
                [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
                
                [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
                
            }
            [btnTemppNew addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
            [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:k withObject:btnTemppNew];
            
            [btnTemppNewHidden addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
            [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:k withObject:btnTemppNewHidden];
            
            [viewTemppNew setFrame:CGRectMake(0, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
            if (viewTemppNew.frame.size.height==0) {
                
                UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[k];//uncheck.png
                [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                
                [viewTemppNew setHidden:YES];
                
                //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTemppNew.frame.size.height)];
                // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
                // NSDictionary *dictOfData=arrTempp[k];
                // [arrOfSelectedChemicalToSendToNextView removeObject:dictOfData];
                
            }else{
                UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[k];
                [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                
                [viewTemppNew setHidden:NO];
                
                //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTemppNew.frame.size.height)];
                // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
                // NSDictionary *dictOfData=arrTempp[k];
                // [arrOfSelectedChemicalToSendToNextView addObject:dictOfData];
                
            }
            [arrayViewsChemicalOther replaceObjectAtIndex:k withObject:viewTemppNew];
        }
        
        //**************************************
#pragma mark- Chemical List other View Settings
        //**************************************
        
        UIView *viewChemicalListLast=arrayViewsChemicalOther[arrayViewsChemicalOther.count-1];
        
        
        CGRect frameForOtherChemical=CGRectMake(10, viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
        frameForOtherChemical.origin.x=10;
        frameForOtherChemical.origin.y=viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+80;
        
        [_viewOtherChemicalList setFrame:frameForOtherChemical];
        
        // New change to change positions
        //**************************************
#pragma mark- Add Other Chemical List View Settings
        //**************************************
        
        
        CGRect frameForOtherChemicalList=CGRectMake(10, _viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+10, self.view.frame.size.width-20, _viewAddOtherProductList.frame.size.height);
        frameForOtherChemicalList.origin.x=10;
        frameForOtherChemicalList.origin.y=_viewOtherChemicalList.frame.origin.y;//+_viewOtherChemicalList.frame.size.height+80;
        
        [_viewAddOtherProductList setFrame:frameForOtherChemicalList];
        
        //**************************************
#pragma mark- _viewServiceJobDescriptions View Settings
        //**************************************
        
        
        CGRect frameForServiceJobDescriptions=CGRectMake(0, _viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+10, self.view.frame.size.width, heightForServiceJobDescriptions);
        frameForServiceJobDescriptions.origin.x=0;
        frameForServiceJobDescriptions.origin.y=_viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+80;
        
        [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
        
        //**************************************
#pragma mark- Payment Info View Settings
        //**************************************
        
        
        CGRect frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
        frameForPaymentInfo.origin.x=10;
        frameForPaymentInfo.origin.y=_viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+80;
        
        [_paymentInfoView setFrame:frameForPaymentInfo];
        
        
        //**************************************
#pragma mark- Payment Type View Settings
        //**************************************
        
        
        CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
        frameForPaymentType.origin.x=10;
        frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
        
        [_paymentTypeView setFrame:frameForPaymentType];
        
        
        //**************************************
#pragma mark- Amount And Check View Settings
        //**************************************
        
        
        CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
        frameForCheckView.origin.x=10;
        frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
        
        [_amountViewSingle setFrame:frameForCheckView];
        
        
        //Yaha Change kiyaa
        CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
        frameForbeforeImageView.origin.x=10;
        frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
        
        [_beforeImageView setFrame:frameForbeforeImageView];
        
        [_scrollForDynamicView addSubview:_beforeImageView];
        
        if (!(arrOfBeforeImageAll.count==0)) {
            
            CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
            frameForbeforeImageViewThumnNail.origin.x=10;
            frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
            
            [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
            
            [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
            
            
            CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
            frameForAfterImage.origin.x=10;
            frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
            
            
            [_lastViewAfterImage setFrame:frameForAfterImage];
            
            [_scrollForDynamicView addSubview:_lastViewAfterImage];
            
            
        }else{
            
            CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
            frameForAfterImage.origin.x=10;
            frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
            
            
            [_lastViewAfterImage setFrame:frameForAfterImage];
            
            [_scrollForDynamicView addSubview:_lastViewAfterImage];
            
        }
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+arrOfWorkOrderOtherChemicalList.count*190)];
    }
}

-(void)actionOpenCloseChemicalListAtStartingOtherNeww:(id)sender
{
    [self HideALlTextFieldsOpenedChemicalList];
    
    if (arrayOfButtonsMainChemicalOther.count==0) {
        
    } else {
        
        BOOL isPlus;
        isPlus=NO;
        CGFloat heightToPlusOrMinus=0.0;
        
        UIButton *btn = (UIButton *)sender;
        int tagggg;
        if (btn==nil) {
            tagggg=0;
        }else{
            
            tagggg=btn.tag-55555;
            
        }
        
        UIButton *btnTempp=[arrayOfButtonsMainChemicalOther objectAtIndex:tagggg];
        
        UIButton *btnTemppHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:tagggg];
        
        [labelChemicalHeadGlobal setFrame:CGRectMake(labelChemicalHeadGlobal.frame.origin.x, btnTempp.frame.origin.y-labelChemicalHeadGlobal.frame.size.height-10, labelChemicalHeadGlobal.frame.size.width, labelChemicalHeadGlobal.frame.size.height)];
        
        [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:tagggg withObject:btnTempp];
        
        [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:tagggg withObject:btnTemppHidden];
        
        UIView *viewTempp=[arrayViewsChemicalOther objectAtIndex:tagggg];
        
        if (!(viewTempp.frame.size.height==0)) {
            
            isPlus=YES;
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[tagggg];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[tagggg];
            CGFloat hiTempLast=[strTempHeightLast floatValue];
            
            [viewTempp setFrame:CGRectMake(0, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, [UIScreen mainScreen].bounds.size.width, hiTempLast)];
            
            heightToPlusOrMinus=viewTempp.frame.size.height;
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTempp.frame.size.height)];
            NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
            
            [arrSelectedChemicalsOther addObject:strTagg];
            int indexxxx=[strTagg intValue];
            indexxxx=indexxxx-55555;
            NSDictionary *dictOfData=arrOfSavedChemicalListOtherAllValues[indexxxx];
            
            if (strWorkOrderId.length==0) {
                strWorkOrderId=strGlobalWorkOrderId;
            }
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"WoOtherProductId"]],
                                       @"true",
                                       strWorkOrderId,
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductName"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"UnitId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"TargetId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"DefaultPercentage"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductAmount"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"EPARegNumber"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ApplicationMethodId"]],nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoOtherProductId",
                                     @"Checked",
                                     @"WorkorderId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"TargetId",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethodId",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrOfSelectedChemicalToSendToNextViewOther addObject:dict_ToSendLeadInfo];
            [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:indexxxx withObject:dict_ToSendLeadInfo];
            
            [viewTempp setHidden:NO];
        } else {
            
            isPlus=NO;
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[tagggg];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
            
            [arrSelectedChemicalsOther removeObject:strTagg];
            int indexxxx=[strTagg intValue];
            indexxxx=indexxxx-55555;
            NSDictionary *dictOfData=arrOfSavedChemicalListOtherAllValues[indexxxx];
            
            if (strWorkOrderId.length==0) {
                strWorkOrderId=strGlobalWorkOrderId;
            }
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"WoOtherProductId"]],
                                       @"false",
                                       strWorkOrderId,
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductName"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"UnitId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"TargetId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"DefaultPercentage"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductAmount"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"EPARegNumber"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ApplicationMethodId"]],nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoOtherProductId",
                                     @"Checked",
                                     @"WorkorderId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"TargetId",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethodId",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrOfSelectedChemicalToSendToNextViewOther removeObject:dict_ToSendLeadInfo];
            [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:indexxxx withObject:dict_ToSendLeadInfo];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTempp.frame.size.height)];
            heightToPlusOrMinus=viewTempp.frame.size.height;
            
            [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
            [viewTempp setHidden:YES];
        }
        [arrayViewsChemicalOther replaceObjectAtIndex:tagggg withObject:viewTempp];
        
        for (int k=tagggg+1; k<arrayViewsChemicalOther.count; k++) {
            
            
            UIButton *btnTemppNew=[arrayOfButtonsMainChemicalOther objectAtIndex:k];
            UIButton *btnTemppNewHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:k];
            UILabel *lblTemppNew=[arrayProductNameOther objectAtIndex:k];
            UIView *viewTemppNew=[arrayViewsChemicalOther objectAtIndex:k];
            UIView *viewTemppNewLasttt;
            CGFloat hiTempLast;
            if (k==0) {
                viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
            } else {
                viewTemppNewLasttt=[arrayViewsChemicalOther objectAtIndex:k-1];
                NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[k-1];
                hiTempLast =[strTempHeightLast floatValue];
                
            }
            
            if (k==1){
                
                viewTemppNewLasttt=[arrayViewsChemicalOther objectAtIndex:k-1];
                UIView *viewTemppNewLastttNewJugad;
                viewTemppNewLastttNewJugad=[arrayViewsChemicalOther objectAtIndex:k-1];
                NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[k-1];
                hiTempLast =[strTempHeightLast floatValue];
                
                [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                
                [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
                
                [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
                
                
            }else{
                
                [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                
                [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
                
                [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
                
            }
            [btnTemppNew addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
            [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:k withObject:btnTemppNew];
            
            
            [btnTemppNewHidden addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
            [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:k withObject:btnTemppNewHidden];
            
            [viewTemppNew setFrame:CGRectMake(0, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
            if (viewTemppNew.frame.size.height==0) {
                
                UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[k];//uncheck.png
                [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                
                [viewTemppNew setHidden:YES];
                
                //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTemppNew.frame.size.height)];
                // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
                // NSDictionary *dictOfData=arrTempp[k];
                // [arrOfSelectedChemicalToSendToNextView removeObject:dictOfData];
                
            }else{
                UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[k];
                [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                
                [viewTemppNew setHidden:NO];
                
                //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTemppNew.frame.size.height)];
                // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
                // NSDictionary *dictOfData=arrTempp[k];
                // [arrOfSelectedChemicalToSendToNextView addObject:dictOfData];
                
            }
            [arrayViewsChemicalOther replaceObjectAtIndex:k withObject:viewTemppNew];
        }
        
        //**************************************
#pragma mark- Chemical List other View Settings
        //**************************************
        
        UIView *viewChemicalListLast=arrayViewsChemicalOther[arrayViewsChemicalOther.count-1];
        
        
        CGRect frameForOtherChemical=CGRectMake(10, viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
        frameForOtherChemical.origin.x=10;
        frameForOtherChemical.origin.y=viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+80;
        
        [_viewOtherChemicalList setFrame:frameForOtherChemical];
        
        // New change to change positions
        //**************************************
#pragma mark- Add Other Chemical List View Settings
        //**************************************
        
        
        CGRect frameForOtherChemicalList=CGRectMake(10, _viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+10, self.view.frame.size.width-20, _viewAddOtherProductList.frame.size.height);
        frameForOtherChemicalList.origin.x=10;
        frameForOtherChemicalList.origin.y=_viewOtherChemicalList.frame.origin.y;//+_viewOtherChemicalList.frame.size.height+80;
        
        [_viewAddOtherProductList setFrame:frameForOtherChemicalList];
        
        
        //**************************************
#pragma mark- _viewServiceJobDescriptions View Settings
        //**************************************
        
        
        CGRect frameForServiceJobDescriptions=CGRectMake(10, _viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+10, self.view.frame.size.width, heightForServiceJobDescriptions);
        frameForServiceJobDescriptions.origin.x=10;
        frameForServiceJobDescriptions.origin.y=_viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+80;
        
        [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
        
        //**************************************
#pragma mark- Payment Info View Settings
        //**************************************
        
        
        CGRect frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
        frameForPaymentInfo.origin.x=10;
        frameForPaymentInfo.origin.y=_viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+80;
        
        [_paymentInfoView setFrame:frameForPaymentInfo];
        
        
        //**************************************
#pragma mark- Payment Type View Settings
        //**************************************
        
        
        CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
        frameForPaymentType.origin.x=10;
        frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
        
        [_paymentTypeView setFrame:frameForPaymentType];
        
        
        //**************************************
#pragma mark- Amount And Check View Settings
        //**************************************
        
        
        CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
        frameForCheckView.origin.x=10;
        frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
        
        [_amountViewSingle setFrame:frameForCheckView];
        
        
        //Yaha Change kiyaa
        CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
        frameForbeforeImageView.origin.x=10;
        frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
        
        [_beforeImageView setFrame:frameForbeforeImageView];
        
        [_scrollForDynamicView addSubview:_beforeImageView];
        
        if (!(arrOfBeforeImageAll.count==0)) {
            
            CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
            frameForbeforeImageViewThumnNail.origin.x=10;
            frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
            
            [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
            
            [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
            
            
            CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
            frameForAfterImage.origin.x=10;
            frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
            
            
            [_lastViewAfterImage setFrame:frameForAfterImage];
            
            [_scrollForDynamicView addSubview:_lastViewAfterImage];
            
            
        }else{
            
            CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
            frameForAfterImage.origin.x=10;
            frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
            
            
            [_lastViewAfterImage setFrame:frameForAfterImage];
            
            [_scrollForDynamicView addSubview:_lastViewAfterImage];
            
        }
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y)];
    }
}

-(void)actionOpenCloseChemicalListAtStartingOtherOnlyOnce:(id)sender
{
    
    [self HideALlTextFieldsOpenedChemicalList];
    
    if (arrayOfButtonsMainChemicalOther.count==0) {
        
    } else {
        
        BOOL isPlus;
        isPlus=NO;
        CGFloat heightToPlusOrMinus=0.0;
        
        UIButton *btn = (UIButton *)sender;
        int tagggg;
        if (btn==nil) {
            tagggg=0;
        }else{
            
            tagggg=btn.tag-55555;
            
        }
        
        UIButton *btnTempp=[arrayOfButtonsMainChemicalOther objectAtIndex:tagggg];
        
        [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:tagggg withObject:btnTempp];
        
        UIButton *btnTemppHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:tagggg];
        
        [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:tagggg withObject:btnTemppHidden];
        
        
        UIView *viewTempp=[arrayViewsChemicalOther objectAtIndex:tagggg];
        
        if (!(viewTempp.frame.size.height==0)) {
            
            isPlus=YES;
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[tagggg];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            
            NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[tagggg];
            CGFloat hiTempLast=[strTempHeightLast floatValue];
            
            [viewTempp setFrame:CGRectMake(0, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, [UIScreen mainScreen].bounds.size.width, hiTempLast)];
            
            heightToPlusOrMinus=viewTempp.frame.size.height;
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTempp.frame.size.height)];
            NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
            
            [arrSelectedChemicalsOther addObject:strTagg];
            int indexxxx=[strTagg intValue];
            indexxxx=indexxxx-55555;
            NSDictionary *dictOfData=arrOfSavedChemicalListOtherAllValues[indexxxx];
            
            if (strWorkOrderId.length==0) {
                strWorkOrderId=strGlobalWorkOrderId;
            }
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"WoOtherProductId"]],
                                       @"true",
                                       strWorkOrderId,
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductName"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"UnitId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"TargetId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"DefaultPercentage"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductAmount"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"EPARegNumber"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ApplicationMethodId"]],nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoOtherProductId",
                                     @"Checked",
                                     @"WorkorderId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"TargetId",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethodId",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrOfSelectedChemicalToSendToNextViewOther addObject:dict_ToSendLeadInfo];
            [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:indexxxx withObject:dict_ToSendLeadInfo];
            
            [viewTempp setHidden:NO];
        } else {
            
            isPlus=NO;
            
            UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[tagggg];
            [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            NSString *strTagg=[NSString stringWithFormat:@"%ld",(long)btnCheckUncheck.tag];
            
            [arrSelectedChemicalsOther removeObject:strTagg];
            int indexxxx=[strTagg intValue];
            indexxxx=indexxxx-55555;
            NSDictionary *dictOfData=arrOfSavedChemicalListOtherAllValues[indexxxx];
            
            if (strWorkOrderId.length==0) {
                strWorkOrderId=strGlobalWorkOrderId;
            }
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"WoOtherProductId"]],
                                       @"false",
                                       strWorkOrderId,
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductName"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"UnitId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"TargetId"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"DefaultPercentage"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ProductAmount"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"EPARegNumber"]],
                                       [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"ApplicationMethodId"]],nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoOtherProductId",
                                     @"Checked",
                                     @"WorkorderId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"TargetId",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethodId",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrOfSelectedChemicalToSendToNextViewOther removeObject:dict_ToSendLeadInfo];
            [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:indexxxx withObject:dict_ToSendLeadInfo];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTempp.frame.size.height)];
            heightToPlusOrMinus=viewTempp.frame.size.height;
            
            [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
            [viewTempp setHidden:YES];
        }
        [arrayViewsChemicalOther replaceObjectAtIndex:tagggg withObject:viewTempp];
        
        for (int k=tagggg+1; k<arrayViewsChemicalOther.count; k++) {
            
            
            UIButton *btnTemppNew=[arrayOfButtonsMainChemicalOther objectAtIndex:k];
            UIButton *btnTemppNewHidden=[arrayOfButtonsMainChemicalOtherHidden objectAtIndex:k];
            UILabel *lblTemppNew=[arrayProductNameOther objectAtIndex:k];
            UIView *viewTemppNew=[arrayViewsChemicalOther objectAtIndex:k];
            UIView *viewTemppNewLasttt;
            CGFloat hiTempLast;
            if (k==0) {
                viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
            } else {
                viewTemppNewLasttt=[arrayViewsChemicalOther objectAtIndex:k-1];
                NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[k-1];
                hiTempLast =[strTempHeightLast floatValue];
                
            }
            
            if (k==1){
                
                viewTemppNewLasttt=[arrayViewsChemicalOther objectAtIndex:k-1];
                UIView *viewTemppNewLastttNewJugad;
                viewTemppNewLastttNewJugad=[arrayViewsChemicalOther objectAtIndex:k-1];
                NSString *strTempHeightLast=arrOfHeightsViewsChemicalOther[k-1];
                hiTempLast =[strTempHeightLast floatValue];
                
                [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                
                [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
                
                [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLastttNewJugad.frame.size.height+15+30-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
                
            }else{
                
                [lblTemppNew setFrame:CGRectMake(lblTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, lblTemppNew.frame.size.width, lblTemppNew.frame.size.height)];
                
                [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
                
                [btnTemppNewHidden setFrame:CGRectMake(btnTemppNewHidden.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5-10, btnTemppNewHidden.frame.size.width, btnTemppNewHidden.frame.size.height)];
                
            }
            [btnTemppNew addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
            [arrayOfButtonsMainChemicalOther replaceObjectAtIndex:k withObject:btnTemppNew];
            
            
            [btnTemppNewHidden addTarget:self action:@selector(actionOpenCloseChemicalListOther:) forControlEvents:UIControlEventTouchDown];
            [arrayOfButtonsMainChemicalOtherHidden replaceObjectAtIndex:k withObject:btnTemppNewHidden];
            
            [viewTemppNew setFrame:CGRectMake(0, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
            if (viewTemppNew.frame.size.height==0) {
                
                UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[k];//uncheck.png
                [btnCheckUncheck setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                
                [viewTemppNew setHidden:YES];
                
                //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTemppNew.frame.size.height)];
                // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
                // NSDictionary *dictOfData=arrTempp[k];
                // [arrOfSelectedChemicalToSendToNextView removeObject:dictOfData];
                
            }else{
                UIButton *btnCheckUncheck=arrayOfButtonsMainChemicalOther[k];
                [btnCheckUncheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                
                [viewTemppNew setHidden:NO];
                
                //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTemppNew.frame.size.height)];
                // NSArray *arrTempp=[dictChemicalList valueForKey:@""];
                // NSDictionary *dictOfData=arrTempp[k];
                // [arrOfSelectedChemicalToSendToNextView addObject:dictOfData];
                
            }
            [arrayViewsChemicalOther replaceObjectAtIndex:k withObject:viewTemppNew];
        }
        
        //**************************************
#pragma mark- Chemical List other View Settings
        //**************************************
        
        UIView *viewChemicalListLast=arrayViewsChemicalOther[arrayViewsChemicalOther.count-1];
        
        
        CGRect frameForOtherChemical=CGRectMake(10, viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+10, self.view.frame.size.width-20, _viewOtherChemicalList.frame.size.height);
        frameForOtherChemical.origin.x=10;
        frameForOtherChemical.origin.y=viewChemicalListLast.frame.origin.y+viewChemicalListLast.frame.size.height+80;
        
        [_viewOtherChemicalList setFrame:frameForOtherChemical];
        
        // New change to change positions
        //**************************************
#pragma mark- Add Other Chemical List View Settings
        //**************************************
        
        
        CGRect frameForOtherChemicalList=CGRectMake(10, _viewOtherChemicalList.frame.origin.y+_viewOtherChemicalList.frame.size.height+10, self.view.frame.size.width-20, _viewAddOtherProductList.frame.size.height);
        frameForOtherChemicalList.origin.x=10;
        frameForOtherChemicalList.origin.y=_viewOtherChemicalList.frame.origin.y;//+_viewOtherChemicalList.frame.size.height+80;
        
        [_viewAddOtherProductList setFrame:frameForOtherChemicalList];
        
        
        //**************************************
#pragma mark- _viewServiceJobDescriptions View Settings
        //**************************************
        
        
        CGRect frameForServiceJobDescriptions=CGRectMake(0, _viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+10, self.view.frame.size.width, heightForServiceJobDescriptions);
        frameForServiceJobDescriptions.origin.x=0;
        frameForServiceJobDescriptions.origin.y=_viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+80;
        
        [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
        
        //**************************************
#pragma mark- Payment Info View Settings
        //**************************************
        
        
        CGRect frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
        frameForPaymentInfo.origin.x=10;
        frameForPaymentInfo.origin.y=_viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+80;
        
        [_paymentInfoView setFrame:frameForPaymentInfo];
        
        
        //**************************************
#pragma mark- Payment Type View Settings
        //**************************************
        
        
        CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
        frameForPaymentType.origin.x=10;
        frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
        
        [_paymentTypeView setFrame:frameForPaymentType];
        
        
        //**************************************
#pragma mark- Amount And Check View Settings
        //**************************************
        
        
        CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
        frameForCheckView.origin.x=10;
        frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
        
        [_amountViewSingle setFrame:frameForCheckView];
        
        
        //Yaha Change kiyaa
        CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
        frameForbeforeImageView.origin.x=10;
        frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
        
        [_beforeImageView setFrame:frameForbeforeImageView];
        
        [_scrollForDynamicView addSubview:_beforeImageView];
        
        if (!(arrOfBeforeImageAll.count==0)) {
            
            CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
            frameForbeforeImageViewThumnNail.origin.x=10;
            frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
            
            [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
            
            [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
            
            
            CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
            frameForAfterImage.origin.x=10;
            frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
            
            
            [_lastViewAfterImage setFrame:frameForAfterImage];
            
            [_scrollForDynamicView addSubview:_lastViewAfterImage];
            
            
        }else{
            
            CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
            frameForAfterImage.origin.x=10;
            frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
            
            
            [_lastViewAfterImage setFrame:frameForAfterImage];
            
            [_scrollForDynamicView addSubview:_lastViewAfterImage];
            
        }
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+190)];
    }
}

-(void)HideALlTextFieldsOpenedChemicalList{
    
    [_txtAddOtherProductName resignFirstResponder];
    [_txtAddOtherProductAmount resignFirstResponder];
    [_txtAddOtherProductEPARegNo resignFirstResponder];
    [_txtAddOtherProductPercentage resignFirstResponder];
    
    for (int k=0; k<arrOfTxtFieldsChemicalList.count; k++) {
        if ([[arrOfTxtFieldsChemicalList objectAtIndex:k] isKindOfClass:[UITextField class]]) {
            
            UITextField *textFeild=arrOfTxtFieldsChemicalList[k];
            [textFeild resignFirstResponder];
            
        }
        if ([[arrOfTxtFieldsChemicalList objectAtIndex:k] isKindOfClass:[UITextView class]]) {
            
            UITextView *textVieww=arrOfTxtFieldsChemicalList[k];
            [textVieww resignFirstResponder];
            
        }
    }
    
}

-(void)actionUnitList:(id)sender
{
    [self HideALlTextFieldsOpenedChemicalList];
    
    UIButton *btn = (UIButton *)sender;
    arrDataTblView=[[NSMutableArray alloc]init];
    isMultipleSelect=NO;
    isUnitOther=NO;
    isTargetOther=NO;
    isAppMethodInfoOther=NO;
    isUnit=YES;
    isTarget=NO;
    isAppMethodInfo=NO;
    isUnitOtherMain=NO;
    isTargetOtherMain=NO;
    isAppMethodInfoOtherMain=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    int taggg=btn.tag;
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"UnitMasters"];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrDataTblView addObject:dictData];
            
        }
    }
    
    // [arrDataTblView addObjectsFromArray:[dictChemicalList valueForKey:@"UnitMasters"]];
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=taggg;
        [self tableLoad:taggg];
    }
}
-(void)actionUnitListOther:(id)sender
{
    [self HideALlTextFieldsOpenedChemicalList];
    
    UIButton *btn = (UIButton *)sender;
    arrDataTblView=[[NSMutableArray alloc]init];
    isMultipleSelect=NO;
    isUnitOther=YES;
    isTargetOther=NO;
    isAppMethodInfoOther=NO;
    isUnit=NO;
    isTarget=NO;
    isAppMethodInfo=NO;
    isUnitOtherMain=NO;
    isTargetOtherMain=NO;
    isAppMethodInfoOtherMain=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    int taggg=btn.tag;
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"UnitMasters"];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrDataTblView addObject:dictData];
            
        }
    }
    
    // [arrDataTblView addObjectsFromArray:[dictChemicalList valueForKey:@"UnitMasters"]];
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=taggg;
        [self tableLoad:taggg];
    }
}

-(void)actionTargetList:(id)sender
{
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    
    [self HideALlTextFieldsOpenedChemicalList];
    UIButton *btn = (UIButton *)sender;
    arrDataTblView=[[NSMutableArray alloc]init];
    isMultipleSelect=NO;
    isUnitOther=NO;
    isTargetOther=NO;
    isAppMethodInfoOther=NO;
    isUnit=NO;
    isTarget=YES;
    isAppMethodInfo=NO;
    isUnitOtherMain=NO;
    isTargetOtherMain=NO;
    isAppMethodInfoOtherMain=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    int taggg=btn.tag;
    indexMultipleSelectTosetValue=taggg-20000;
    NSArray *ArrTempp=[arrTargetId[taggg-20000] componentsSeparatedByString:@","];
    [arraySelecteValues addObjectsFromArray:ArrTempp];
    if (arraySelecteValues.count>0) {
        [arraySelecteValues removeObjectAtIndex:0];
    }
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"TargetMasters"];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        NSString *strTargettIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
        BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
        if ([strDepartmentIddd isEqualToString:strDepartmentId] && (isActiveeee)) {
            
            [arrDataTblView addObject:dictData];
            
        }else if([ArrTempp containsObject:strTargettIddd]){
            
            [arrDataTblView addObject:dictData];
            
        }
    }
    
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=taggg;
        [self tableLoad:taggg];
    }
}
-(void)actionTargetListOther:(id)sender
{
    
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    
    [self HideALlTextFieldsOpenedChemicalList];
    UIButton *btn = (UIButton *)sender;
    arrDataTblView=[[NSMutableArray alloc]init];
    isMultipleSelect=NO;
    isUnitOther=NO;
    isTargetOther=YES;
    isAppMethodInfoOther=NO;
    isUnit=NO;
    isTarget=NO;
    isAppMethodInfo=NO;
    isUnitOtherMain=NO;
    isTargetOtherMain=NO;
    isAppMethodInfoOtherMain=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    int taggg=btn.tag;
    indexMultipleSelectTosetValue=taggg-200000;
    NSArray *ArrTempp=[arrTargetIdOther[taggg-200000] componentsSeparatedByString:@","];
    [arraySelecteValues addObjectsFromArray:ArrTempp];
    if (arraySelecteValues.count>0) {
        [arraySelecteValues removeObjectAtIndex:0];
    }
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"TargetMasters"];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        NSString *strTargettIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
        BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
        if ([strDepartmentIddd isEqualToString:strDepartmentId] && (isActiveeee)) {
            
            [arrDataTblView addObject:dictData];
            
        }else if([ArrTempp containsObject:strTargettIddd]){
            
            [arrDataTblView addObject:dictData];
            
        }
        
    }
    
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=taggg;
        [self tableLoad:taggg];
    }
}

-(void)actionAppMethodInfoList:(id)sender
{
    
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    
    [self HideALlTextFieldsOpenedChemicalList];
    UIButton *btn = (UIButton *)sender;
    arrDataTblView=[[NSMutableArray alloc]init];
    isMultipleSelect=NO;
    isUnitOther=NO;
    isTargetOther=NO;
    isAppMethodInfoOther=NO;
    isUnit=NO;
    isTarget=NO;
    isAppMethodInfo=YES;
    isUnitOtherMain=NO;
    isTargetOtherMain=NO;
    isAppMethodInfoOtherMain=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    int taggg=btn.tag;
    indexMultipleSelectTosetValue=taggg-40000;
    NSArray *ArrTempp=[arrAppMethodInfoId[taggg-40000] componentsSeparatedByString:@","];
    [arraySelecteValues addObjectsFromArray:ArrTempp];
    if (arraySelecteValues.count>0) {
        [arraySelecteValues removeObjectAtIndex:0];
    }
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"ApplicationMethodMasters"];
    
    NSArray *objValueControls=[NSArray arrayWithObjects:
                               @"0",
                               @"Select Method",
                               @"00001",nil];
    
    NSArray *objKeyControls=[NSArray arrayWithObjects:
                             @"ApplicationMethodId",
                             @"ApplicationMethodName",
                             @"DepartmentId",nil];
    
    NSDictionary *dictAddExtra=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
    
    //[arrDataTblView addObject:dictAddExtra];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        NSString *strTargettIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
        BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
        if ([strDepartmentIddd isEqualToString:strDepartmentId] && (isActiveeee)) {
            
            [arrDataTblView addObject:dictData];
            
        }else if([ArrTempp containsObject:strTargettIddd]){
            
            [arrDataTblView addObject:dictData];
            
        }
        
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=taggg;
        [self tableLoad:taggg];
    }
    
}

-(void)actionAppMethodInfoListOther:(id)sender
{
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    
    [self HideALlTextFieldsOpenedChemicalList];
    UIButton *btn = (UIButton *)sender;
    arrDataTblView=[[NSMutableArray alloc]init];
    isMultipleSelect=NO;
    isUnitOther=NO;
    isTargetOther=NO;
    isAppMethodInfoOther=YES;
    isUnit=NO;
    isTarget=NO;
    isAppMethodInfo=NO;
    isUnitOtherMain=NO;
    isTargetOtherMain=NO;
    isAppMethodInfoOtherMain=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    int taggg=btn.tag;
    indexMultipleSelectTosetValue=taggg-400000;
    NSArray *ArrTempp=[arrAppMethodInfoIdOther[taggg-400000] componentsSeparatedByString:@","];
    [arraySelecteValues addObjectsFromArray:ArrTempp];
    if (arraySelecteValues.count>0) {
        [arraySelecteValues removeObjectAtIndex:0];
    }
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"ApplicationMethodMasters"];
    
    NSArray *objValueControls=[NSArray arrayWithObjects:
                               @"0",
                               @"Select Method",
                               @"00001",nil];
    
    NSArray *objKeyControls=[NSArray arrayWithObjects:
                             @"ApplicationMethodId",
                             @"ApplicationMethodName",
                             @"DepartmentId",nil];
    
    NSDictionary *dictAddExtra=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
    
    //[arrDataTblView addObject:dictAddExtra];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        NSString *strTargettIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
        BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
        if ([strDepartmentIddd isEqualToString:strDepartmentId] && (isActiveeee)) {
            
            [arrDataTblView addObject:dictData];
            
        }else if([ArrTempp containsObject:strTargettIddd]){
            
            [arrDataTblView addObject:dictData];
            
        }
        
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=taggg;
        [self tableLoad:taggg];
    }
    
}

#pragma mark- --------------------------------------------------------------------------------------------------------Final Save Chemicals

//yesEditedSomething=YES;
-(void)finalSaveChemicalList
{
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrSelectedChemicals];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    arrSelectedChemicals=nil;
    arrSelectedChemicals=[[NSMutableArray alloc]init];
    
    [arrSelectedChemicals addObjectsFromArray:arrayWithoutDuplicates];
    
    NSMutableArray *arrTempProduuctDetail=[[NSMutableArray alloc]init];
    
    if (arrOfWorkOrderProductDetails.count==0) {
        
        for (int k=0; k<arrSelectedChemicals.count; k++) {
            
            NSString *strWOPId,*strPId,*strPName,*strUId,*strTId,*strDPercent,*strPAmount,*strEPAReg,*strAppMethodId;
            
            NSString *strTags=[NSString stringWithFormat:@"%@",arrSelectedChemicals[k]];
            int tags=[strTags intValue];
            tags=tags-5555;
            
            NSArray *arrOfProductMaster=[dictChemicalList valueForKey:@"ProductMasters"];
            
            NSMutableArray *arrTempValues=[[NSMutableArray alloc]init];
            
            //arrOfProductMaster
            for (int k=0; k<arrOfProductMaster.count; k++) {
                
                NSDictionary *dictData=arrOfProductMaster[k];
                NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
                
                NSString *strIsActiveProduct=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsActive"]];
                
                if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
                    
                    if ([strIsActiveProduct isEqualToString:@"true"] || [strIsActiveProduct isEqualToString:@"1"]) {
                        
                        [arrTempValues addObject:dictData];
                        
                    }
                    
                }
            }
            arrOfProductMaster=arrTempValues;
            arrTempValues=nil;
            arrTempValues=[[NSMutableArray alloc]init];
            
            NSDictionary *dictList=arrOfProductMaster[tags];
            strPId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductId"]];
            strPName=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductName"]];
            
            //EPAReg k liye........................
            for (int j=0; j<arrEPARegNumber.count; j++) {
                
                NSString *strAmount=[NSString stringWithFormat:@"%@",arrEPARegNumber[j]];
                NSArray *arrtemp=[strAmount componentsSeparatedByString:@","];
                NSString *strTagAmount=arrtemp[0];
                int tagAmount=[strTagAmount intValue];
                tagAmount=tagAmount-35000;
                if (tagAmount==tags) {
                    strEPAReg=arrtemp[1];
                    break;
                    
                }else{
                    
                    strEPAReg=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"EPARegNumber"]];
                }
                
            }
            if (arrEPARegNumber.count==0) {
                
                strEPAReg=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"EPARegNumber"]];
                
            }
            
            //Amount k liye........................
            for (int j=0; j<arrTxtAmount.count; j++) {
                
                NSString *strAmount=[NSString stringWithFormat:@"%@",arrTxtAmount[j]];
                NSArray *arrtemp=[strAmount componentsSeparatedByString:@","];
                NSString *strTagAmount=arrtemp[0];
                int tagAmount=[strTagAmount intValue];
                tagAmount=tagAmount-25000;
                if (tagAmount==tags) {
                    strPAmount=arrtemp[1];
                    break;
                    
                }else{
                    
                    strPAmount=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductAmount"]];
                }
                
            }
            if (arrTxtAmount.count==0) {
                strPAmount=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductAmount"]];
            }
            
            //percent k liye........................
            for (int j=0; j<arrTxtPercent.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrTxtPercent[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-30000;
                if (tagPercent==tags) {
                    strDPercent=arrtemp[1];
                    break;
                    
                }else{
                    
                    strDPercent=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"DefaultPercentage"]];
                }
                
            }
            
            if (arrTxtPercent.count==0) {
                strDPercent=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"DefaultPercentage"]];
            }
            
            //Unit k liye........................
            for (int j=0; j<arrUnitId.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrUnitId[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-15000;
                if (tagPercent==tags) {
                    strUId=arrtemp[1];
                    break;
                    
                }else{
                    
                    strUId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"UnitId"]];
                }
                
            }
            if (arrUnitId.count==0) {
                strUId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"UnitId"]];
            }
            
            //Target k liye........................
            for (int j=0; j<arrTargetId.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrTargetId[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-20000;
                if (tagPercent==tags) {
                    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
                    [arrMutableTemp addObjectsFromArray:arrtemp];
                    [arrMutableTemp removeObjectAtIndex:0];
                    
                    strTId=[arrMutableTemp componentsJoinedByString:@","];;
                    break;
                    
                }else{
                    
                    strTId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"TargetId"]];
                }
                
            }
            if (arrTargetId.count==0) {
                strTId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"TargetId"]];
            }
            //AppMethodInfo k liye........................
            for (int j=0; j<arrAppMethodInfoId.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrAppMethodInfoId[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-40000;
                if (tagPercent==tags) {
                    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
                    [arrMutableTemp addObjectsFromArray:arrtemp];
                    [arrMutableTemp removeObjectAtIndex:0];
                    
                    strAppMethodId=[arrMutableTemp componentsJoinedByString:@","];;
                    break;
                    
                }else{
                    
                    strAppMethodId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ApplicationMethodId"]];
                }
                
            }
            if (arrAppMethodInfoId.count==0) {
                strAppMethodId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ApplicationMethodId"]];
            }
            if (strWOPId.length==0) {
                strWOPId=@"";
            }
            if (strPId.length==0) {
                strPId=@"";
            }
            if (strPName.length==0) {
                strPName=@"";
            }
            if (strUId.length==0) {
                strUId=@"";
            }
            if ([strUId isEqualToString:@"0"]) {
                strUId=@"";
            }
            if (strTId.length==0) {
                strTId=@"";
            }
            if (strDPercent.length==0) {
                strDPercent=@"";
            }
            if (strPAmount.length==0) {
                strPAmount=@"";
            }
            if (strAppMethodId.length==0) {
                strAppMethodId=@"";
            }
            if (strEPAReg.length==0) {
                strEPAReg=@"";
            }
            
            
            
            //Final Savingggggg
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       strWOPId,
                                       @"true",
                                       strPId,
                                       strPName,
                                       strUId,
                                       strTId,
                                       strDPercent,
                                       strPAmount,
                                       strEPAReg,
                                       strAppMethodId,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoProductId",
                                     @"Checked",
                                     @"ProductId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"Targets",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethods",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrTempProduuctDetail addObject:dict_ToSendLeadInfo];
            
        }
        
        
        
    }else{
        BOOL yesFound;
        yesFound=NO;
        
        for (int k=0; k<arrSelectedChemicals.count; k++){
            
            NSString *strWOPId,*strPId,*strPName,*strUId,*strTId,*strDPercent,*strPAmount,*strEpaReg,*strAppMethodInfoIDd;
            
            NSString *strTags=[NSString stringWithFormat:@"%@",arrSelectedChemicals[k]];
            int tags=[strTags intValue];
            tags=tags-5555;
            
            NSArray *arrOfProductMaster=[dictChemicalList valueForKey:@"ProductMasters"];
            
            NSMutableArray *arrTempValues=[[NSMutableArray alloc]init];
            
            //arrOfProductMaster
            for (int k=0; k<arrOfProductMaster.count; k++) {
                
                NSDictionary *dictData=arrOfProductMaster[k];
                NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
                
                NSString *strIsActiveProduct=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsActive"]];
                
                if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
                    
                    if ([strIsActiveProduct isEqualToString:@"true"] || [strIsActiveProduct isEqualToString:@"1"]) {
                        
                        [arrTempValues addObject:dictData];
                        
                    }
                    
                }
            }
            arrOfProductMaster=arrTempValues;
            arrTempValues=nil;
            arrTempValues=[[NSMutableArray alloc]init];
            
            NSDictionary *dictList=arrOfProductMaster[tags];
            strPId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductId"]];
            strPName=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductName"]];
            
            
            //EPAReg k liye........................
            for (int j=0; j<arrEPARegNumber.count; j++) {
                
                NSString *strAmount=[NSString stringWithFormat:@"%@",arrEPARegNumber[j]];
                NSArray *arrtemp=[strAmount componentsSeparatedByString:@","];
                NSString *strTagAmount=arrtemp[0];
                int tagAmount=[strTagAmount intValue];
                tagAmount=tagAmount-35000;
                if (tagAmount==tags) {
                    strEpaReg=arrtemp[1];
                    break;
                    
                }else{
                    
                    strEpaReg=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"EPARegNumber"]];
                }
                
            }
            if (arrEPARegNumber.count==0) {
                
                strEpaReg=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"EPARegNumber"]];
                
            }
            
            
            //Amount k liye........................
            for (int j=0; j<arrTxtAmount.count; j++) {
                
                NSString *strAmount=[NSString stringWithFormat:@"%@",arrTxtAmount[j]];
                NSArray *arrtemp=[strAmount componentsSeparatedByString:@","];
                NSString *strTagAmount=arrtemp[0];
                int tagAmount=[strTagAmount intValue];
                tagAmount=tagAmount-25000;
                if (tagAmount==tags) {
                    strPAmount=arrtemp[1];
                    break;
                    
                }else{
                    
                    strPAmount=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductAmount"]];
                }
                
            }
            if (arrTxtAmount.count==0) {
                strPAmount=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductAmount"]];
                
            }
            
            //percent k liye........................
            for (int j=0; j<arrTxtPercent.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrTxtPercent[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-30000;
                if (tagPercent==tags) {
                    strDPercent=arrtemp[1];
                    break;
                    
                }else{
                    
                    strDPercent=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"DefaultPercentage"]];
                }
                
            }
            if (arrTxtPercent.count==0) {
                
                strDPercent=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"DefaultPercentage"]];
                
            }
            //Unit k liye........................
            for (int j=0; j<arrUnitId.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrUnitId[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-15000;
                if (tagPercent==tags) {
                    strUId=arrtemp[1];
                    break;
                    
                }else{
                    
                    strUId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"UnitId"]];
                }
                
            }
            
            if (arrUnitId.count==0) {
                strUId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"UnitId"]];
                
            }
            //Target k liye........................
            for (int j=0; j<arrTargetId.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrTargetId[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-20000;
                if (tagPercent==tags) {
                    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
                    [arrMutableTemp addObjectsFromArray:arrtemp];
                    [arrMutableTemp removeObjectAtIndex:0];
                    
                    strTId=[arrMutableTemp componentsJoinedByString:@","];;
                    break;
                    
                }else{
                    
                    strTId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"TargetId"]];
                }
                
            }
            if (arrTargetId.count==0) {
                strTId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"TargetId"]];
                
            }
            //AppMethodInfo k liye........................
            for (int j=0; j<arrAppMethodInfoId.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrAppMethodInfoId[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-40000;
                if (tagPercent==tags) {
                    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
                    [arrMutableTemp addObjectsFromArray:arrtemp];
                    [arrMutableTemp removeObjectAtIndex:0];
                    
                    strAppMethodInfoIDd=[arrMutableTemp componentsJoinedByString:@","];
                    break;
                    
                }else{
                    
                    strAppMethodInfoIDd=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ApplicationMethodId"]];
                }
                
            }
            if (arrAppMethodInfoId.count==0) {
                
                strAppMethodInfoIDd=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ApplicationMethodId"]];
                
            }
            
            if (strWOPId.length==0) {
                strWOPId=@"";
            }
            if (strPId.length==0) {
                strPId=@"";
            }
            if (strPName.length==0) {
                strPName=@"";
            }
            if (strUId.length==0) {
                strUId=@"";
            }
            if ([strUId isEqualToString:@"0"]) {
                strUId=@"";
            }
            if (strTId.length==0) {
                strTId=@"";
            }
            if (strDPercent.length==0) {
                strDPercent=@"";
            }
            if (strPAmount.length==0) {
                strPAmount=@"";
            }
            if (strEpaReg.length==0) {
                strEpaReg=@"";
            }
            if (strAppMethodInfoIDd.length==0) {
                strAppMethodInfoIDd=@"";
            }
            
            
            
            //Final Savingggggg
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       strWOPId,
                                       @"true",
                                       strPId,
                                       strPName,
                                       strUId,
                                       strTId,
                                       strDPercent,
                                       strPAmount,
                                       strEpaReg,
                                       strAppMethodInfoIDd,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoProductId",
                                     @"Checked",
                                     @"ProductId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"Targets",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethods",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrTempProduuctDetail addObject:dict_ToSendLeadInfo];
            
        }
    }
    
    if (arrTempProduuctDetail.count==0) {
        [arrTempProduuctDetail addObjectsFromArray:arrOfWorkOrderProductDetails];
    }
    
    //Updating Selected Chemical List To Database
    [matchesProductDetail setValue:arrTempProduuctDetail forKey:@"chemicalList"];
    NSError *error;
    [context save:&error];
    
    arrOfSelectedChemicalToSendToNextView=[[NSMutableArray alloc]init];
    
    [arrOfSelectedChemicalToSendToNextView addObjectsFromArray:arrTempProduuctDetail];
    
    NSLog(@"Final Selected Chemical List ====%@",arrTempProduuctDetail);
    if (arrSelectedChemicals.count==0) {
        arrOfWorkOrderProductDetails=nil;
    } else {
        
    }
    
    
}
-(void)finalSaveChemicalListOther
{
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrSelectedChemicalsOther];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    arrSelectedChemicalsOther=nil;
    arrSelectedChemicalsOther=[[NSMutableArray alloc]init];
    
    [arrSelectedChemicalsOther addObjectsFromArray:arrayWithoutDuplicates];
    
    NSMutableArray *arrTempProduuctDetail=[[NSMutableArray alloc]init];
    
    if (arrOfWorkOrderOtherChemicalList.count==0) {
        
        for (int k=0; k<arrSelectedChemicalsOther.count; k++) {
            
            NSString *strWOPId,*strPId,*strPName,*strUId,*strTId,*strDPercent,*strPAmount,*strEPAReg,*strAppMethodId;
            
            NSString *strTags=[NSString stringWithFormat:@"%@",arrSelectedChemicalsOther[k]];
            int tags=[strTags intValue];
            tags=tags-55555;
            
            NSDictionary *dictList=arrOfSavedChemicalListOtherAllValues[tags];
            strPId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"WoOtherProductId"]];
            strPName=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductName"]];
            
            //EPAReg k liye........................
            for (int j=0; j<arrEPARegNumberOther.count; j++) {
                
                NSString *strAmount=[NSString stringWithFormat:@"%@",arrEPARegNumberOther[j]];
                NSArray *arrtemp=[strAmount componentsSeparatedByString:@","];
                NSString *strTagAmount=arrtemp[0];
                int tagAmount=[strTagAmount intValue];
                tagAmount=tagAmount-350000;
                if (tagAmount==tags) {
                    strEPAReg=arrtemp[1];
                    break;
                    
                }else{
                    
                    strEPAReg=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"EPARegNumber"]];
                }
                
            }
            if (arrEPARegNumberOther.count==0) {
                
                strEPAReg=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"EPARegNumber"]];
                
            }
            
            //Amount k liye........................
            for (int j=0; j<arrTxtAmountOther.count; j++) {
                
                NSString *strAmount=[NSString stringWithFormat:@"%@",arrTxtAmountOther[j]];
                NSArray *arrtemp=[strAmount componentsSeparatedByString:@","];
                NSString *strTagAmount=arrtemp[0];
                int tagAmount=[strTagAmount intValue];
                tagAmount=tagAmount-250000;
                if (tagAmount==tags) {
                    strPAmount=arrtemp[1];
                    break;
                    
                }else{
                    
                    strPAmount=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductAmount"]];
                }
                
            }
            if (arrTxtAmountOther.count==0) {
                strPAmount=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductAmount"]];
            }
            
            //percent k liye........................
            for (int j=0; j<arrTxtPercentOther.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrTxtPercentOther[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-300000;
                if (tagPercent==tags) {
                    strDPercent=arrtemp[1];
                    break;
                    
                }else{
                    
                    strDPercent=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"DefaultPercentage"]];
                }
                
            }
            
            if (arrTxtPercentOther.count==0) {
                strDPercent=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"DefaultPercentage"]];
            }
            
            //Unit k liye........................
            for (int j=0; j<arrUnitIdOther.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrUnitIdOther[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-150000;
                if (tagPercent==tags) {
                    strUId=arrtemp[1];
                    break;
                    
                }else{
                    
                    strUId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"UnitId"]];
                }
                
            }
            if (arrUnitIdOther.count==0) {
                strUId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"UnitId"]];
            }
            
            //Target k liye........................
            for (int j=0; j<arrTargetIdOther.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrTargetIdOther[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-200000;
                if (tagPercent==tags) {
                    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
                    [arrMutableTemp addObjectsFromArray:arrtemp];
                    [arrMutableTemp removeObjectAtIndex:0];
                    
                    strTId=[arrMutableTemp componentsJoinedByString:@","];
                    break;
                    
                }else{
                    
                    strTId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"TargetId"]];
                }
                
            }
            if (arrTargetIdOther.count==0) {
                strTId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"TargetId"]];
            }
            //AppMethodInfo k liye........................
            for (int j=0; j<arrAppMethodInfoIdOther.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrAppMethodInfoIdOther[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-400000;
                if (tagPercent==tags) {
                    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
                    [arrMutableTemp addObjectsFromArray:arrtemp];
                    [arrMutableTemp removeObjectAtIndex:0];
                    
                    strAppMethodId=[arrMutableTemp componentsJoinedByString:@","];;
                    break;
                    
                }else{
                    
                    strAppMethodId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ApplicationMethodId"]];
                }
                
            }
            if (arrAppMethodInfoIdOther.count==0) {
                strAppMethodId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ApplicationMethodId"]];
            }
            if (strWOPId.length==0) {
                strWOPId=@"";
            }
            if (strPId.length==0) {
                strPId=@"";
            }
            if (strPName.length==0) {
                strPName=@"";
            }
            if (strUId.length==0) {
                strUId=@"";
            }
            if ([strUId isEqualToString:@"0"]) {
                strUId=@"";
            }
            if (strTId.length==0) {
                strTId=@"";
            }
            if (strDPercent.length==0) {
                strDPercent=@"";
            }
            if (strPAmount.length==0) {
                strPAmount=@"";
            }
            if (strAppMethodId.length==0) {
                strAppMethodId=@"";
            }
            if (strEPAReg.length==0) {
                strEPAReg=@"";
            }
            
            
            
            //Final Savingggggg
            if (strWorkOrderId.length==0) {
                strWorkOrderId=strGlobalWorkOrderId;
            }
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       strWOPId,
                                       [NSString stringWithFormat:@"%@",[dictList valueForKey:@"Checked"]],
                                       strWorkOrderId,
                                       strPName,
                                       strUId,
                                       strTId,
                                       strDPercent,
                                       strPAmount,
                                       strEPAReg,
                                       strAppMethodId,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoOtherProductId",
                                     @"Checked",
                                     @"WorkorderId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"Targets",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethods",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrTempProduuctDetail addObject:dict_ToSendLeadInfo];
            
            [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:tags withObject:dict_ToSendLeadInfo];
        }
        
        
        
    }else{
        BOOL yesFound;
        yesFound=NO;
        
        for (int k=0; k<arrSelectedChemicalsOther.count; k++){
            
            NSString *strWOPId,*strPId,*strPName,*strUId,*strTId,*strDPercent,*strPAmount,*strEpaReg,*strAppMethodInfoIDd;
            
            NSString *strTags=[NSString stringWithFormat:@"%@",arrSelectedChemicalsOther[k]];
            int tags=[strTags intValue];
            tags=tags-55555;
            
            NSDictionary *dictList=arrOfSavedChemicalListOtherAllValues[tags];
            strPId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"WoOtherProductId"]];
            strPName=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductName"]];
            
            
            //EPAReg k liye........................
            for (int j=0; j<arrEPARegNumberOther.count; j++) {
                
                NSString *strAmount=[NSString stringWithFormat:@"%@",arrEPARegNumberOther[j]];
                NSArray *arrtemp=[strAmount componentsSeparatedByString:@","];
                NSString *strTagAmount=arrtemp[0];
                int tagAmount=[strTagAmount intValue];
                tagAmount=tagAmount-350000;
                if (tagAmount==tags) {
                    strEpaReg=arrtemp[1];
                    break;
                    
                }else{
                    
                    strEpaReg=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"EPARegNumber"]];
                }
                
            }
            if (arrEPARegNumberOther.count==0) {
                
                strEpaReg=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"EPARegNumber"]];
                
            }
            
            
            //Amount k liye........................
            for (int j=0; j<arrTxtAmountOther.count; j++) {
                
                NSString *strAmount=[NSString stringWithFormat:@"%@",arrTxtAmountOther[j]];
                NSArray *arrtemp=[strAmount componentsSeparatedByString:@","];
                NSString *strTagAmount=arrtemp[0];
                int tagAmount=[strTagAmount intValue];
                tagAmount=tagAmount-250000;
                if (tagAmount==tags) {
                    strPAmount=arrtemp[1];
                    break;
                    
                }else{
                    
                    strPAmount=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductAmount"]];
                }
                
            }
            if (arrTxtAmountOther.count==0) {
                strPAmount=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ProductAmount"]];
                
            }
            
            //percent k liye........................
            for (int j=0; j<arrTxtPercentOther.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrTxtPercentOther[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-300000;
                if (tagPercent==tags) {
                    strDPercent=arrtemp[1];
                    break;
                    
                }else{
                    
                    strDPercent=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"DefaultPercentage"]];
                }
                
            }
            if (arrTxtPercentOther.count==0) {
                
                strDPercent=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"DefaultPercentage"]];
                
            }
            //Unit k liye........................
            for (int j=0; j<arrUnitIdOther.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrUnitIdOther[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-150000;
                if (tagPercent==tags) {
                    strUId=arrtemp[1];
                    break;
                    
                }else{
                    
                    strUId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"UnitId"]];
                }
                
            }
            
            if (arrUnitIdOther.count==0) {
                strUId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"UnitId"]];
                
            }
            //Target k liye........................
            for (int j=0; j<arrTargetIdOther.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrTargetIdOther[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-200000;
                if (tagPercent==tags) {
                    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
                    [arrMutableTemp addObjectsFromArray:arrtemp];
                    [arrMutableTemp removeObjectAtIndex:0];
                    
                    strTId=[arrMutableTemp componentsJoinedByString:@","];;
                    break;
                    
                }else{
                    
                    strTId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"TargetId"]];
                }
                
            }
            if (arrTargetIdOther.count==0) {
                strTId=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"TargetId"]];
                
            }
            //AppMethodInfo k liye........................
            for (int j=0; j<arrAppMethodInfoIdOther.count; j++) {
                
                NSString *strPercent=[NSString stringWithFormat:@"%@",arrAppMethodInfoIdOther[j]];
                NSArray *arrtemp=[strPercent componentsSeparatedByString:@","];
                NSString *strTagPercent=arrtemp[0];
                int tagPercent=[strTagPercent intValue];
                tagPercent=tagPercent-400000;
                if (tagPercent==tags) {
                    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
                    [arrMutableTemp addObjectsFromArray:arrtemp];
                    [arrMutableTemp removeObjectAtIndex:0];
                    
                    strAppMethodInfoIDd=[arrMutableTemp componentsJoinedByString:@","];;
                    break;
                    
                }else{
                    
                    strAppMethodInfoIDd=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ApplicationMethodId"]];
                }
                
            }
            if (arrAppMethodInfoIdOther.count==0) {
                
                strAppMethodInfoIDd=[NSString stringWithFormat:@"%@",[dictList valueForKey:@"ApplicationMethodId"]];
                
            }
            
            if (strWOPId.length==0) {
                strWOPId=@"";
            }
            if (strPId.length==0) {
                strPId=@"";
            }
            if (strPName.length==0) {
                strPName=@"";
            }
            if (strUId.length==0) {
                strUId=@"";
            }
            if ([strUId isEqualToString:@"0"]) {
                strUId=@"";
            }
            if (strTId.length==0) {
                strTId=@"";
            }
            if (strDPercent.length==0) {
                strDPercent=@"";
            }
            if (strPAmount.length==0) {
                strPAmount=@"";
            }
            if (strEpaReg.length==0) {
                strEpaReg=@"";
            }
            if (strAppMethodInfoIDd.length==0) {
                strAppMethodInfoIDd=@"";
            }
            
            
            
            //Final Savingggggg
            
            if (strWorkOrderId.length==0) {
                strWorkOrderId=strGlobalWorkOrderId;
            }
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       strWOPId,
                                       [NSString stringWithFormat:@"%@",[dictList valueForKey:@"Checked"]],
                                       strWorkOrderId,
                                       strPName,
                                       strUId,
                                       strTId,
                                       strDPercent,
                                       strPAmount,
                                       strEpaReg,
                                       strAppMethodInfoIDd,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"WoOtherProductId",
                                     @"Checked",
                                     @"WorkorderId",
                                     @"ProductName",
                                     @"UnitId",
                                     @"Targets",
                                     @"DefaultPercentage",
                                     @"ProductAmount",
                                     @"EPARegNumber",
                                     @"ApplicationMethods",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrTempProduuctDetail addObject:dict_ToSendLeadInfo];
            
            [arrOfSavedChemicalListOtherAllValues replaceObjectAtIndex:tags withObject:dict_ToSendLeadInfo];
        }
    }
    
    if (arrTempProduuctDetail.count==0) {
        [arrTempProduuctDetail addObjectsFromArray:arrOfWorkOrderOtherChemicalList];
    }
    
    //Updating Selected Chemical List To Database
    [matchesProductDetail setValue:arrOfSavedChemicalListOtherAllValues forKey:@"otherChemicalList"];
    NSError *error;
    [context save:&error];
    
    arrOfSelectedChemicalToSendToNextViewOther=[[NSMutableArray alloc]init];
    
    [arrOfSelectedChemicalToSendToNextViewOther addObjectsFromArray:arrTempProduuctDetail];
    
    NSLog(@"Final Selected Chemical List ====%@",arrTempProduuctDetail);
    if (arrSelectedChemicalsOther.count==0) {
        arrOfWorkOrderOtherChemicalList=nil;
    } else {
        
    }
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Fetch Work Order and Payment Info METHODS-----------------
//============================================================================
//============================================================================

-(void)fetchWorkOrderFromDataBase{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        strGlobalWorkOrderId=[matchesWorkOrder valueForKey:@"workorderId"];
        strWorkOrderStatuss=[matchesWorkOrder valueForKey:@"workorderStatus"];
        
        if ([strWorkOrderStatuss caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strWorkOrderStatuss caseInsensitiveCompare:@"Completed"] == NSOrderedSame) {
            
            strWorkOrderStatuss=@"Complete";
            
        }
        
        strPrimaryServiceSysName=[matchesWorkOrder valueForKey:@"primaryServiceSysName"];
        strCategorySysName=[matchesWorkOrder valueForKey:@"categorySysName"];
        
        
        _txtInvoicePayment.text=[matchesWorkOrder valueForKey:@"invoiceAmount"];
        _txtProductionPayment.text=[matchesWorkOrder valueForKey:@"productionAmount"];
        _txtPreviousBalance.text=[matchesWorkOrder valueForKey:@"previousBalance"];
        _txtTax.text=[matchesWorkOrder valueForKey:@"tax"];
        
        
        //for service job description start
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [[matchesWorkOrder valueForKey:@"serviceJobDescription"] dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
         _textView_ServiceJobDescriptions.attributedText = attributedString;
        
        //_textView_ServiceJobDescriptions.text=[matchesWorkOrder valueForKey:@"serviceJobDescription"];
        
        strGlobalServiceJobDescriptionId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceJobDescriptionId"]];
        strGlobalServiceJobDescriptionsText=[matchesWorkOrder valueForKey:@"serviceJobDescription"];
        
        arrPickerServiceJobDescriptions=[[NSMutableArray alloc]init];
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
        NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
        
        if ([dictdata isKindOfClass:[NSString class]]) {
            
            //[global AlertMethod:Alert :NoDataAvailableServiceJobDescriptions];
            
        }else{
            
            NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceJobDescriptionTemplate"];
            if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                    
                    NSDictionary *dictData=[arrOfServiceJobDescriptionTemplate objectAtIndex:k];
                    
                    if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]] isEqualToString:strGlobalServiceJobDescriptionId]) {
                        
                        [_btnServiceJobDescriptionTitle setTitle:[dictData valueForKey:@"Title"] forState:UIControlStateNormal];
                        
                    }
                }
            }
        }
        
        
        //for service job description start
        
        
        float total=[[matchesWorkOrder valueForKey:@"invoiceAmount"] floatValue]+[[matchesWorkOrder valueForKey:@"previousBalance"] floatValue]+[[matchesWorkOrder valueForKey:@"tax"] floatValue];
        NSString* formattedNumber = [NSString stringWithFormat:@"%.02f", total];
        _txtTotalDue.text=[NSString stringWithFormat:@"%@",formattedNumber];//[matchesWorkOrder valueForKey:@"workorderId"];
        
        if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
            
            [_btnAddGraphhh setEnabled:NO];
            [_btnCash setEnabled:NO];
            [_btnCheck setEnabled:NO];
            [_btnCreditCard setEnabled:NO];
            [_btnBill setEnabled:NO];
            [_btnAutoCharger setEnabled:NO];
            [_btnPaymentPending setEnabled:NO];
            [_btnNoCharge setEnabled:NO];
            [_txtAmount setEnabled:NO];
            [_txt_DrivingLicenseNo setEnabled:NO];
            [_txtCheckNo setEnabled:NO];
            [_txtAmountSingle setEnabled:NO];
            
            
            [_btnHiddenCash setEnabled:NO];
            [_btnHiddenCheck setEnabled:NO];
            [_btnHiddenCreditCard setEnabled:NO];
            [_btnHiddenBill setEnabled:NO];
            [_btnHiddenAutoChargeCustomer setEnabled:NO];
            [_btnHiddenPaymentPending setEnabled:NO];
            [_btnHiddenNoCharge setEnabled:NO];
            [_btnServiceJobDescriptionTitle setEnabled:NO];
            [_textView_ServiceJobDescriptions setEditable:NO];
            
        }else{
            
            [_btnAddGraphhh setEnabled:YES];
            [_btnCash setEnabled:YES];
            [_btnCheck setEnabled:YES];
            [_btnCreditCard setEnabled:YES];
            [_btnBill setEnabled:YES];
            [_btnAutoCharger setEnabled:YES];
            [_btnPaymentPending setEnabled:YES];
            [_btnNoCharge setEnabled:YES];
            [_txtAmount setEnabled:YES];
            [_txt_DrivingLicenseNo setEnabled:YES];
            [_txtCheckNo setEnabled:YES];
            [_txtAmountSingle setEnabled:YES];
            
            [_btnHiddenCash setEnabled:YES];
            [_btnHiddenCheck setEnabled:YES];
            [_btnHiddenCreditCard setEnabled:YES];
            [_btnHiddenBill setEnabled:YES];
            [_btnHiddenAutoChargeCustomer setEnabled:YES];
            [_btnHiddenPaymentPending setEnabled:YES];
            [_btnHiddenNoCharge setEnabled:YES];
            [_btnServiceJobDescriptionTitle setEnabled:YES];
            [_textView_ServiceJobDescriptions setEditable:YES];
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    [self fetchPaymentInfoFromDataBase];
    
    [self fetchServiceJobDescriptionsFromMaster];
    
}

-(void)fetchPaymentInfoFromDataBase{
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    requestPaymentInfo = [[NSFetchRequest alloc] init];
    [requestPaymentInfo setEntity:entityPaymentInfo];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestPaymentInfo setPredicate:predicate];
    
    sortDescriptorPaymentInfo = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsPaymentInfo = [NSArray arrayWithObject:sortDescriptorPaymentInfo];
    
    [requestPaymentInfo setSortDescriptors:sortDescriptorsPaymentInfo];
    
    self.fetchedResultsControllerPaymentInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestPaymentInfo managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerPaymentInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerPaymentInfo performFetch:&error];
    arrAllObjPaymentInfo = [self.fetchedResultsControllerPaymentInfo fetchedObjects];
    if ([arrAllObjPaymentInfo count] == 0)
    {
        
    }
    else
    {
        matchesPaymentInfo=arrAllObjPaymentInfo[0];
        isPaymentInfo=YES;
        [self setValuesForPaymentInfo];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    [self fetchProductDetailFromDataBase];
    
}


-(void)fetchProductDetailFromDataBase{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityProductDetail=[NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context];
    requestProductDetail = [[NSFetchRequest alloc] init];
    [requestProductDetail setEntity:entityProductDetail];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestProductDetail setPredicate:predicate];
    
    sortDescriptorProductDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsProductDetail = [NSArray arrayWithObject:sortDescriptorProductDetail];
    
    [requestProductDetail setSortDescriptors:sortDescriptorsProductDetail];
    
    self.fetchedResultsControllerProductDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestProductDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerProductDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerProductDetail performFetch:&error];
    arrAllObjProductDetail = [self.fetchedResultsControllerProductDetail fetchedObjects];
    if ([arrAllObjProductDetail count] == 0)
    {
        
    }
    else
    {
        matchesProductDetail=arrAllObjProductDetail[0];
        
        NSArray *arrWOPD=[matchesProductDetail valueForKey:@"chemicalList"];
        
        NSArray *arrWOOPD=[matchesProductDetail valueForKey:@"otherChemicalList"];
        
        [arrOfWorkOrderProductDetails addObjectsFromArray:arrWOPD];
        
        if (arrOfWorkOrderOtherChemicalList==nil) {
            arrOfWorkOrderOtherChemicalList=[[NSMutableArray alloc]init];
        }
        
        [arrOfWorkOrderOtherChemicalList addObjectsFromArray:arrWOOPD];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    [self fetchImageDetailFromDataBase];
    
}

-(void)fetchImageDetailFromDataBase{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        matchesImageDetail=arrAllObjImageDetail[0];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
//============================================================================
//============================================================================
#pragma mark- ---------------------Set Values Payment Info -----------------
//============================================================================
//============================================================================

-(void)setValuesForPaymentInfo{
    
    NSString* woPaymentId=[matchesPaymentInfo valueForKey:@"woPaymentId"];
    NSString* workorderId=[matchesPaymentInfo valueForKey:@"workorderId"];
    strGlobalWorkOrderId=workorderId;
    NSString* paymentMode=[matchesPaymentInfo valueForKey:@"paymentMode"];
    NSString* paidAmount=[matchesPaymentInfo valueForKey:@"paidAmount"];
    NSString* checkNo=[matchesPaymentInfo valueForKey:@"checkNo"];
    NSString* drivingLicenseNo=[matchesPaymentInfo valueForKey:@"drivingLicenseNo"];
    NSString* expirationDate=[matchesPaymentInfo valueForKey:@"expirationDate"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:expirationDate];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        strGlobalDateToShow=expirationDate;
        
    } else {
        
        expirationDate=finalTime;
        strGlobalDateToShow=finalTime;
        
    }
    
    NSString* checkFrontImagePath=[matchesPaymentInfo valueForKey:@"checkFrontImagePath"];
    if (checkFrontImagePath.length>0) {
        [arrOFImagesName addObject:checkFrontImagePath];
    }
    NSString* checkBackImagePath=[matchesPaymentInfo valueForKey:@"checkBackImagePath"];
    if (checkBackImagePath.length>0) {
        [arrOfCheckBackImage addObject:checkBackImagePath];
    }
    NSString* createdDate=[matchesPaymentInfo valueForKey:@"createdDate"];
    NSString* createdBy=[matchesPaymentInfo valueForKey:@"createdBy"];
    NSString* modifiedDate=[matchesPaymentInfo valueForKey:@"modifiedDate"];
    NSString* modifiedBy=[matchesPaymentInfo valueForKey:@"modifiedBy"];
    NSString* userName=[matchesPaymentInfo valueForKey:@"userName"];
    
    _txtAmount.text=paidAmount;
    _txtAmountSingle.text=paidAmount;
    _txtCheckNo.text=checkNo;
    _txt_DrivingLicenseNo.text=[matchesPaymentInfo valueForKey:@"drivingLicenseNo"];
    [_btnCheckExpDate setTitle:expirationDate forState:UIControlStateNormal];
    strGlobalPaymentMode=paymentMode;
    
    /*
     if ([paymentMode caseInsensitiveCompare:@"Cash"] == NSOrderedSame || paymentMode.length==0) {
     
     [self performSelector:@selector(btnCashSetting) withObject:nil afterDelay:5.0];
     [self btnCashSetting];
     
     } else if ([paymentMode caseInsensitiveCompare:@"CreditCard"] == NSOrderedSame){
     
     [self performSelector:@selector(btnCreditCArdSetting) withObject:nil afterDelay:5.0];
     [self btnCreditCArdSetting];
     
     }else if ([paymentMode caseInsensitiveCompare:@"NoCharge"] == NSOrderedSame){
     
     [self performSelector:@selector(btnNoChargeSetting) withObject:nil afterDelay:5.0];
     [self btnNoChargeSetting];
     
     }
     else if ([paymentMode caseInsensitiveCompare:@"Check"] == NSOrderedSame){
     
     [self performSelector:@selector(btnCheckSetting) withObject:nil afterDelay:7.0];
     [self btnCheckSetting];
     
     }
     else if ([paymentMode caseInsensitiveCompare:@"PaymentPending"] == NSOrderedSame){
     
     [self performSelector:@selector(btnPaymentPending) withObject:nil afterDelay:5.0];
     [self btnPaymentPending];
     
     }
     else if ([paymentMode caseInsensitiveCompare:@"AutoChargeCustomer"] == NSOrderedSame){
     
     [self performSelector:@selector(btnAutoChargeSetting) withObject:nil afterDelay:5.0];
     [self btnAutoChargeSetting];
     
     
     }
     else {
     
     [self performSelector:@selector(btnBillSetting) withObject:nil afterDelay:5.0];
     [self btnBillSetting];
     
     }
     */
}

-(void)finalSavePaymentInfo
{
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        
        
        if (_txtCheckNo.text.length==0)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter Check #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        //        else if (_txt_DrivingLicenseNo.text.length==0)
        //        {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Driving License #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //            [alert show];
        //
        //        }
        //        else if (_btnCheckExpDate.titleLabel.text.length==0)
        //        {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Check Expiration Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //            [alert show];
        //
        //        }else if (arrOFImagesName.count==0) {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Front Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //            [alert show];
        //
        //        }else if (arrOfCheckBackImage.count==0) {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Back Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //            [alert show];
        //
        //        }
        else{
            
            if (isPaymentInfo) {
                
                NSString *strTypePaymentToSend;
                
                if ([strGlobalPaymentMode isEqualToString:@"Credit Card"]) {
                    
                    strTypePaymentToSend=@"CreditCard";
                    
                } else if ([strGlobalPaymentMode isEqualToString:@"No Charge"]){
                    
                    strTypePaymentToSend=@"NoCharge";
                    
                }
                else if ([strGlobalPaymentMode isEqualToString:@"Auto Charge Customer"]){
                    
                    strTypePaymentToSend=@"AutoChargeCustomer";
                    
                }
                else if ([strGlobalPaymentMode isEqualToString:@"Payment Pending"]){
                    
                    strTypePaymentToSend=@"PaymentPending";
                    
                }else{
                    
                    strTypePaymentToSend=strGlobalPaymentMode;
                    
                }
                [matchesPaymentInfo setValue:strTypePaymentToSend forKey:@"paymentMode"];
                [matchesPaymentInfo setValue:_txtAmount.text forKey:@"paidAmount"];
                [matchesPaymentInfo setValue:_txtCheckNo.text forKey:@"checkNo"];
                [matchesPaymentInfo setValue:_txt_DrivingLicenseNo.text forKey:@"drivingLicenseNo"];
                [matchesPaymentInfo setValue:_btnCheckExpDate.titleLabel.text forKey:@"expirationDate"];
                if (!(arrOFImagesName.count==0)) {
                    [matchesPaymentInfo setValue:arrOFImagesName[0] forKey:@"checkFrontImagePath"];
                }
                if (!(arrOfCheckBackImage.count==0)) {
                    [matchesPaymentInfo setValue:arrOfCheckBackImage[0] forKey:@"checkBackImagePath"];
                }
                
                
                NSError *error;
                [context save:&error];
                
            } else {
                
                [self setPaymentInfoFirstTime:@"Check"];
                
            }
        }
    } else {
        if (isPaymentInfo) {
            
            NSString *strTypePaymentToSend;
            
            if ([strGlobalPaymentMode isEqualToString:@"Credit Card"]) {
                
                strTypePaymentToSend=@"CreditCard";
                
            } else if ([strGlobalPaymentMode isEqualToString:@"No Charge"]){
                
                strTypePaymentToSend=@"NoCharge";
                
            }
            else if ([strGlobalPaymentMode isEqualToString:@"Auto Charge Customer"]){
                
                strTypePaymentToSend=@"AutoChargeCustomer";
                
            }
            else if ([strGlobalPaymentMode isEqualToString:@"Payment Pending"]){
                
                strTypePaymentToSend=@"PaymentPending";
                
            }else{
                
                strTypePaymentToSend=strGlobalPaymentMode;
                
            }
            
            [matchesPaymentInfo setValue:strTypePaymentToSend forKey:@"paymentMode"];
            [matchesPaymentInfo setValue:_txtAmountSingle.text forKey:@"paidAmount"];
            NSError *error;
            [context save:&error];
            
        }
        else
        {
            
            [self setPaymentInfoFirstTime:@"NoCheck"];
            
        }
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------******** Payment Info Save *******----------------
//============================================================================
//============================================================================


-(void)setPaymentInfoFirstTime :(NSString*)type{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    
    PaymentInfoServiceAuto *objPaymentInfoServiceAuto = [[PaymentInfoServiceAuto alloc]initWithEntity:entityPaymentInfoServiceAuto insertIntoManagedObjectContext:context];
    
    objPaymentInfoServiceAuto.woPaymentId=@"";
    
    objPaymentInfoServiceAuto.workorderId=strGlobalWorkOrderId;
    
    objPaymentInfoServiceAuto.paymentMode=strGlobalPaymentMode;
    
    if ([type isEqualToString:@"Check"]) {
        
        objPaymentInfoServiceAuto.paidAmount=_txtAmount.text;
        objPaymentInfoServiceAuto.checkNo=_txtCheckNo.text;
        objPaymentInfoServiceAuto.drivingLicenseNo=@"";
        objPaymentInfoServiceAuto.expirationDate=_btnCheckExpDate.titleLabel.text;
        if (!(arrOFImagesName.count==0)) {
            objPaymentInfoServiceAuto.checkFrontImagePath=arrOFImagesName[0];
        }else{
            objPaymentInfoServiceAuto.checkFrontImagePath=@"";
        }
        if (!(arrOfCheckBackImage.count==0)) {
            objPaymentInfoServiceAuto.checkBackImagePath=arrOfCheckBackImage[0];
        }else{
            objPaymentInfoServiceAuto.checkBackImagePath=@"";
        }
    } else {
        
        objPaymentInfoServiceAuto.paidAmount=_txtAmountSingle.text;
        objPaymentInfoServiceAuto.checkNo=@"";
        objPaymentInfoServiceAuto.drivingLicenseNo=@"";
        objPaymentInfoServiceAuto.expirationDate=@"";
        objPaymentInfoServiceAuto.checkFrontImagePath=@"";
        objPaymentInfoServiceAuto.checkBackImagePath=@"";
        
    }
    
    objPaymentInfoServiceAuto.createdDate=@"";
    
    objPaymentInfoServiceAuto.createdBy=@"";
    
    objPaymentInfoServiceAuto.modifiedDate=@"";
    
    objPaymentInfoServiceAuto.modifiedBy=@"";
    
    objPaymentInfoServiceAuto.userName=strUserName;
    
    NSError *error;
    [context save:&error];
    
}

-(void)updateModifyDate{
    
    //============================================================================
    //============================================================================
#pragma mark- ---------******** Modify Date Save *******----------------
    //============================================================================
    //============================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityModifyDateServiceAuto];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        
        NSString *strCurrentDateFormatted = [global modifyDateService];
        
        [matchesServiceModifyDate setValue:strCurrentDateFormatted forKey:@"modifyDate"];
        
        [global fetchWorkOrderFromDataBaseForServiceToUpdateModifydate:strWorkOrderId :strCurrentDateFormatted];
        
        //Final Save
        NSError *error;
        [context save:&error];
    }
}
-(void)updateZSyncWorkOrder{
    
    [_matchesWorkOrderZSync setValue:@"yes" forKey:@"zSync"];
    //Final Save
    NSError *error;
    [context save:&error];
    
}
-(void)goToNextViewww{
    
    [self updateServiceJobDescription];
    
    BOOL isCompulsory =[self isCompulsoryImage];
    
    if (isCompulsory) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take atleast one before image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }else{
        
        [self goToNextViewDirectInvoice];
        
    }
}

-(void)goToNextViewDirectInvoice{
    
    [self updateServiceJobDescription];
    
    // strWorkOrderStatuss=@"InComplete";
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    InvoiceAppointmentView
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"InvoiceAppointmentView"];
    
    if (finalJsonDict.count==0) {
        if (arrResponseInspection.count==0) {
            
        } else {
            
            objByProductVC.dictJsonDynamicForm=[arrResponseInspection objectAtIndex:0];
            
        }
        
        
    } else {
        objByProductVC.dictJsonDynamicForm=finalJsonDict;
        
    }
    objByProductVC.workOrderDetail=matchesWorkOrder;
    objByProductVC.paymentInfoDetail=matchesPaymentInfo;
    objByProductVC.dictOfChemicalList=dictChemicalList;
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"])
    {
        objByProductVC.arrOfChemicalListOther=arrOfWorkOrderOtherChemicalList;
        objByProductVC.arrOfChemicalList=arrOfWorkOrderProductDetails;
        objByProductVC.arrChemicalList=arrOfWorkOrderProductDetails;//[dictChemicalList valueForKey:@""];
        
    }
    else
    {
        
        objByProductVC.arrOfChemicalListOther=arrOfSelectedChemicalToSendToNextViewOther;
        objByProductVC.arrOfChemicalList=arrOfSelectedChemicalToSendToNextView;
        objByProductVC.arrChemicalList=arrOfSelectedChemicalToSendToNextView;//[dictChemicalList valueForKey:@""];
        
    }
    objByProductVC.strPaymentModes=strGlobalPaymentMode;
    objByProductVC.strDepartmentIdd=strDepartmentId;
    if ([strGlobalPaymentMode isEqualToString:@"Check"]) {
        objByProductVC.strPaidAmount=_txtAmount.text;
        
    } else {
        objByProductVC.strPaidAmount=_txtAmountSingle.text;
        
    }
    
    objByProductVC.arrAllObjImageDetail=arrAllObjImageDetail;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

- (IBAction)action_ServiceDocuments:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceDocumentsViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDocumentsViewController"];
    objByProductVC.strAccountNo=[matchesWorkOrder valueForKey:@"accountNo"];
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------******** Before Image *******----------------
//============================================================================
//============================================================================

-(void)methodForImageOnViewWillAppear{
    
    NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
    BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreview"];
    if (yesFromDeleteImage) {
        
        yesEditedSomething=YES;
        
        [defsSS setBool:NO forKey:@"yesDeletedImageInPreview"];
        [defsSS synchronize];
        
        NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfBeforeImageAll.count; k++) {
            
            NSDictionary *dictdat=arrOfBeforeImageAll[k];
            
            NSString *strImageName;
            
            if ([dictdat isKindOfClass:[NSString class]]) {
                
                strImageName=arrOfBeforeImageAll[k];
                
            } else {
                
                strImageName=[dictdat valueForKey:@"woImagePath"];
                
            }
            NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImages"];
            BOOL yesFoundName;
            yesFoundName=NO;
            
            for (int j=0; j<arrOfImagesLeft.count; j++) {
                
                NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                
                if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                    
                    [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                    
                }
            }
            //            if (yesFoundName) {
            //
            //                [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
            //
            //            }
        }
        if (!(arrTempBeforeImage.count==0)) {
            // arrOfBeforeImageAll=nil;
            // arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            [arrOfBeforeImageAll removeObjectsInArray:arrTempBeforeImage];
        }
        
        yesEditedSomething=YES;
        arrOfImageCaptionGraph=nil;
        arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
        arrOfImageDescriptionGraph=nil;
        arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
        
        NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
        [defsnew setObject:nil forKey:@"DeletedImages"];
        [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
        [defsnew setObject:nil forKey:@"imageCaptionGraph"];
        [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
        [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
        [defsnew synchronize];
        
        //[self saveImageToCoreData];;
        
        
        if (arrOfBeforeImageAll.count==0) {
            
            [_beforeImageThumbNailView removeFromSuperview];
            
            [self defaultViewSettingForAmount];
            
        }else{
            
            [self defaultViewSettingForAmount];
            
        }
    }
    
    
    //Graph
    //Change For Image Description  yesEditedImageDescription
    BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
    if (yesEditedImageCaptionGraph) {
        
        yesEditedSomething=YES;
        arrOfImageCaptionGraph=nil;
        arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
        NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
        [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
        [defsnew setObject:nil forKey:@"imageCaptionGraph"];
        [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
        [defsnew synchronize];
        
        //[self saveImageToCoreData];;
        
    }
    
    //Graph
    //Change For Image Description  yesEditedImageDescription
    BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
    if (yesEditedImageDescriptionGraph) {
        
        yesEditedSomething=YES;
        arrOfImageDescriptionGraph=nil;
        arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
        NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
        [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
        [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
        [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
        [defsnew synchronize];
        
        //[self saveImageToCoreData];;
        
    }
    
    //Change in image Captions
    BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
    if (yesEditedImageCaption) {
        
        yesEditedSomething=YES;
        arrOfImageCaption=nil;
        arrOfImageCaption=[[NSMutableArray alloc]init];
        NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
        [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
        [defsnew setObject:nil forKey:@"imageCaption"];
        [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
        [defsnew synchronize];
        
        //[self saveImageToCoreData];;
        
    }
    
    
    //Change in image Description
    BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
    if (yesEditedImageDescription) {
        
        yesEditedSomething=YES;
        arrOfImageDescription=nil;
        arrOfImageDescription=[[NSMutableArray alloc]init];
        NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
        [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
        [defsnew setObject:nil forKey:@"imageDescription"];
        [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
        [defsnew synchronize];
        
        //[self saveImageToCoreData];;
        
    }
    
    
    
    [self downloadingImagesThumbNailCheck];
    
    //Graph
    [self downloadGraphImage];
    
    
}
//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [_imageCollectionView reloadData];
        
    }else if (arrOfImagesDetail.count==0){
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [_imageCollectionView reloadData];
        
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1) {
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:YES];
            [_buttonImg3 setHidden:YES];
        } else if (arrOfImagess.count==2){
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:YES];
        }else{
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:NO];
        }
        if (arrOfImagess.count==0) {
            
            [_buttonImg1 setHidden:YES];
            [_buttonImg2 setHidden:YES];
            [_buttonImg3 setHidden:YES];
            
        }
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionView addObjectsFromArray:arrOfImagess];
        [_imageCollectionView reloadData];
        
        [self downloadImages:arrOfImagess];
        
        /*
         dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
         dispatch_async(myQueue, ^{
         
         [self downloadImages:arrOfImagess];
         
         dispatch_async(dispatch_get_main_queue(), ^{
         // Update the UI
         });
         });
         */
        
        if (arrOfBeforeImageAll.count==0) {
            
            [_buttonImg1 setHidden:YES];
            [_buttonImg2 setHidden:YES];
            [_buttonImg3 setHidden:YES];
            
        }
        
    }
}

-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImage:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
            
        }
        
        [_imageCollectionView reloadData];
        
    }
    
    
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionView.count-1) {
        
        [_imageCollectionView reloadData];
        
    }
    
    [_imageCollectionView reloadData];
    
    //  [self ShowFirstImage : name : indexx];
    
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name :(int)indexxx {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (indexxx==0) {
        
        [_buttonImg1 setImage:image forState:UIControlStateNormal];
        
    } else if(indexxx==1) {
        
        [_buttonImg2 setImage:image forState:UIControlStateNormal];
        
        
    }else{
        
        [_buttonImg3 setImage:image forState:UIControlStateNormal];
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}

//============================================================================
//============================================================================
#pragma mark -----------------------Action Button ImageView-------------------------------
//============================================================================
//============================================================================

- (IBAction)action_ButtonImg1:(id)sender {
    
    [self goingToPreview : @"0"];
    
}

- (IBAction)action_ButtonImg2:(id)sender {
    
    [self goingToPreview : @"1"];
    
}

- (IBAction)action_ButtonImg3:(id)sender {
    
    [self goingToPreview : @"2"];
    
}

- (IBAction)actionOnBeforeImage:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if ([strWorkOrderStatuss caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strWorkOrderStatuss caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strWorkOrderStatuss caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                                  
                                  
                              }else{
                                  
                                  if (arrOfBeforeImageAll.count<10)
                                  {
                                      
                                      isFromBeforeImage=YES;
                                      
                                      NSLog(@"The CApture Image.");
                                      
                                      NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                      
                                      BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                                      
                                      if (isfirstTimeAudio) {
                                          
                                          [defs setBool:NO forKey:@"firstCamera"];
                                          [defs synchronize];
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }else{
                                          BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                          
                                          if (isCameraPermissionAvailable) {
                                              
                                              UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                              imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                              imagePickController.delegate=(id)self;
                                              imagePickController.allowsEditing=TRUE;
                                              [self presentViewController:imagePickController animated:YES completion:nil];
                                              
                                              
                                          }else{
                                              
                                              UIAlertController *alert= [UIAlertController
                                                                         alertControllerWithTitle:@"Alert"
                                                                         message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction * action)
                                                                    {
                                                                        
                                                                        
                                                                        
                                                                    }];
                                              [alert addAction:yes];
                                              UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                           NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                           [[UIApplication sharedApplication] openURL:url];
                                                                       } else {
                                                                           
                                                                           //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                           //                                     [alert show];
                                                                           
                                                                       }
                                                                       
                                                                   }];
                                              [alert addAction:no];
                                              [self presentViewController:alert animated:YES completion:nil];
                                          }
                                      }
                                  }
                                  else
                                  {
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                      [alert show];
                                  }
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if ([strWorkOrderStatuss caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strWorkOrderStatuss caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strWorkOrderStatuss caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                                 
                                 
                             }else{
                                 
                                 if (arrOfBeforeImageAll.count<10)
                                 {
                                     isFromBeforeImage=YES;
                                     
                                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                     
                                     BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                                     
                                     if (isfirstTimeAudio) {
                                         
                                         [defs setBool:NO forKey:@"firstGallery"];
                                         [defs synchronize];
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }else{
                                         BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                         
                                         if (isCameraPermissionAvailable) {
                                             
                                             UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                             imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                             imagePickController.delegate=(id)self;
                                             imagePickController.allowsEditing=TRUE;
                                             [self presentViewController:imagePickController animated:YES completion:nil];
                                             
                                             
                                         }else{
                                             
                                             UIAlertController *alert= [UIAlertController
                                                                        alertControllerWithTitle:@"Alert"
                                                                        message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       
                                                                       
                                                                   }];
                                             [alert addAction:yes];
                                             UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                        handler:^(UIAlertAction * action)
                                                                  {
                                                                      
                                                                      if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                          NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                          [[UIApplication sharedApplication] openURL:url];
                                                                      } else {
                                                                          
                                                                          //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                          //                                     [alert show];
                                                                          
                                                                      }
                                                                      
                                                                  }];
                                             [alert addAction:no];
                                             [self presentViewController:alert animated:YES completion:nil];
                                         }
                                     }
                                 }else{
                                     
                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                     [alert show];
                                 }
                             }
                             
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)goingToPreview :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentView
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaption;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescription;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

-(void)fetchImageDetailFromDataBaseNewForBeforeImage
{
    
    
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageLattitude=[[NSMutableArray alloc]init];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetail.count; j++) {
            
            matchesImageDetail=arrAllObjImageDetail[j];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Before"]) {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetail valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetail valueForKey:@"longitude"];
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAll addObject:dict_ToSendLeadInfo];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
                
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageLongitude addObject:strLong];
                    
                }
                
                
            }
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
    [self deleteBeforeImagesBeforeSaving];
    
    //Graph
    [self deleteGraphImagesBeforeSaving];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrOfBeforeImageAll.count; k++)
    {
        
        if ([arrOfBeforeImageAll[k] isKindOfClass:[NSString class]]) {
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfBeforeImageAll objectAtIndex:k]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            NSError *error1;
            [context save:&error1];
            
        }else{
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            NSDictionary *dictData=[arrOfBeforeImageAll objectAtIndex:k];
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
            
        }
    }
    
    
    //Graph
    for (int k=0; k<arrOfImagenameCollewctionViewGraph.count; k++)
    {
        // Image Detail Entity
        entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
        
        ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
        
        if ([arrOfImagenameCollewctionViewGraph[k] isKindOfClass:[NSString class]])
        {
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfImagenameCollewctionViewGraph objectAtIndex:k]];
            objImageDetail.woImageType=@"Graph";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
        }
        else
        {
            NSDictionary *dictData=[arrOfImagenameCollewctionViewGraph objectAtIndex:k];
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Graph";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    
    //........................................................................
}

-(void)deleteBeforeImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Before"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

//============================================================================
#pragma mark- ------------Action Add Other Product Details------------------
//============================================================================

- (IBAction)action_AddOtherProductUnit:(id)sender {
    
    [self HideALlTextFieldsOpenedChemicalList];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    /*
     isMultipleSelect=NO;
     isUnitOtherMain=YES;
     isTargetOtherMain=NO;
     isAppMethodInfoOtherMain=NO;
     isUnit=NO;
     isTarget=NO;
     isAppMethodInfo=NO;
     isUnitOther=NO;
     */
    
    isMultipleSelect=NO;
    isUnitOther=NO;
    isTargetOther=NO;
    isAppMethodInfoOther=NO;
    isUnit=NO;
    isTarget=NO;
    isAppMethodInfo=NO;
    isUnitOtherMain=YES;
    isTargetOtherMain=NO;
    isAppMethodInfoOtherMain=NO;
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"UnitMasters"];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId]) {
            
            [arrDataTblView addObject:dictData];
            
        }
    }
    
    // [arrDataTblView addObjectsFromArray:[dictChemicalList valueForKey:@"UnitMasters"]];
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=0;
        [self tableLoad:0];
        
    }
    
}

- (IBAction)action_AddOtherProductMethod:(id)sender {
    
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    
    [self HideALlTextFieldsOpenedChemicalList];
    arrDataTblView=[[NSMutableArray alloc]init];
    
    /*
     isMultipleSelect=NO;
     isTargetOtherMain=NO;
     isAppMethodInfoOtherMain=YES;
     isUnitOtherMain=NO;
     isUnit=NO;
     isTarget=NO;
     isAppMethodInfo=NO;
     */
    
    isMultipleSelect=NO;
    isUnitOther=NO;
    isTargetOther=NO;
    isAppMethodInfoOther=NO;
    isUnit=NO;
    isTarget=NO;
    isAppMethodInfo=NO;
    isUnitOtherMain=NO;
    isTargetOtherMain=NO;
    isAppMethodInfoOtherMain=YES;
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    if ([_btnAppMethodOtherMain.titleLabel.text isEqualToString:@"Select"]) {
        
        strGlobalAppMethodIdOther=@"";
        
    }
    
    NSArray *ArrTempp;
    
    if (strGlobalAppMethodIdOther.length>0) {
        
        ArrTempp=[strGlobalAppMethodIdOther componentsSeparatedByString:@","];
        [arraySelecteValues addObjectsFromArray:ArrTempp];
        
    }
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"ApplicationMethodMasters"];
    
    NSArray *objValueControls=[NSArray arrayWithObjects:
                               @"0",
                               @"Select Method",
                               @"00001",nil];
    
    NSArray *objKeyControls=[NSArray arrayWithObjects:
                             @"ApplicationMethodId",
                             @"ApplicationMethodName",
                             @"DepartmentId",nil];
    
    NSDictionary *dictAddExtra=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
    
    //[arrDataTblView addObject:dictAddExtra];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        NSString *strTargettIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
        BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
        if ([strDepartmentIddd isEqualToString:strDepartmentId] && (isActiveeee)) {
            
            [arrDataTblView addObject:dictData];
            
        }else if([ArrTempp containsObject:strTargettIddd]){
            
            [arrDataTblView addObject:dictData];
            
        }
        
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=0;
        [self tableLoad:0];
    }
    
}

- (IBAction)action_AddOtherProductTarget:(id)sender {
    
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    
    [self HideALlTextFieldsOpenedChemicalList];
    arrDataTblView=[[NSMutableArray alloc]init];
    
    /*
     isMultipleSelect=NO;
     isTargetOtherMain=YES;
     isAppMethodInfoOtherMain=NO;
     isUnitOtherMain=NO;
     isUnit=NO;
     isTarget=NO;
     isAppMethodInfo=NO;
     */
    
    isMultipleSelect=NO;
    isUnitOther=NO;
    isTargetOther=NO;
    isAppMethodInfoOther=NO;
    isUnit=NO;
    isTarget=NO;
    isAppMethodInfo=NO;
    isUnitOtherMain=NO;
    isTargetOtherMain=YES;
    isAppMethodInfoOtherMain=NO;
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    if ([_btnTargetOtherMain.titleLabel.text isEqualToString:@"Select"]) {
        
        strGlobalTargetIdOther=@"";
        
    }
    
    NSArray *ArrTempp;
    if (strGlobalTargetIdOther.length>0) {
        
        ArrTempp=[strGlobalTargetIdOther componentsSeparatedByString:@","];
        [arraySelecteValues addObjectsFromArray:ArrTempp];
        
    }
    
    
    NSArray *arrOfTargetMasterTemp=[dictChemicalList valueForKey:@"TargetMasters"];
    
    for (int k=0; k<arrOfTargetMasterTemp.count; k++) {
        
        NSDictionary *dictData=arrOfTargetMasterTemp[k];
        NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
        NSString *strTargettIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
        BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
        
        if ([strDepartmentIddd isEqualToString:strDepartmentId] && (isActiveeee)) {
            
            [arrDataTblView addObject:dictData];
            
        }else if([ArrTempp containsObject:strTargettIddd]){
            
            [arrDataTblView addObject:dictData];
            
        }
        
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=0;
        [self tableLoad:0];
    }
    
}

- (IBAction)action_AddOtherProductFinally:(id)sender {
    
    int tempText=[_txtAddOtherProductPercentage.text intValue];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    
    if (_txtAddOtherProductName.text.length==0) {
        
        [global AlertMethod:@"Alert!" :@"Please enter product name"];
        
    }else if ([[_txtAddOtherProductName.text stringByTrimmingCharactersInSet: set] length] == 0)
    {
        [global AlertMethod:@"Alert!" :@"Please enter product name"];
    }
    else if (tempText>100) {
        
        [global AlertMethod:@"Alert!" :@"Please enter percentage in between 100"];
        
    } else if ([_btnUnitOtherMain.titleLabel.text isEqualToString:@"Select" ]) {
        
        [global AlertMethod:@"Alert!" :@"Please select Unit"];
        
    }
    /*
     else if ([_btnTargetOtherMain.titleLabel.text isEqualToString:@"Select" ]){
     
     [global AlertMethod:@"Alert!" :@"Please select target"];
     
     }
     */
    else{
        
        NSMutableArray *arrTempProduuctDetail=[[NSMutableArray alloc]init];
        
        NSString *strDPercent,*strPAmount,*strEPAReg,*strAppMethodId;
        
        strDPercent=_txtAddOtherProductPercentage.text;
        if (strDPercent.length==0) {
            strDPercent=@"";
        }
        strPAmount=_txtAddOtherProductAmount.text;
        if (strPAmount.length==0) {
            strPAmount=@"";
        }
        strAppMethodId=strGlobalAppMethodIdOther;
        if (strAppMethodId.length==0) {
            strAppMethodId=@"";
        }
        strEPAReg=_txtAddOtherProductEPARegNo.text;
        if (strEPAReg.length==0) {
            strEPAReg=@"";
        }
        
        if (strWorkOrderId.length==0) {
            strWorkOrderId=strGlobalWorkOrderId;
        }
        
        if ([_btnTargetOtherMain.titleLabel.text isEqualToString:@"Select"]) {
            
            strGlobalTargetIdOther=@"";
            
        }
        
        if (strGlobalTargetIdOther.length==0) {
            
            strGlobalTargetIdOther=@"";
            
        }
        
        //Final Savingggggg
        
        NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                   @"",
                                   @"true",
                                   strWorkOrderId,
                                   _txtAddOtherProductName.text,
                                   strGlobalUnitIdOther,
                                   strGlobalTargetIdOther,
                                   strDPercent,
                                   strPAmount,
                                   strEPAReg,
                                   strAppMethodId,nil];
        NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                 @"WoOtherProductId",
                                 @"Checked",
                                 @"WorkorderId",
                                 @"ProductName",
                                 @"UnitId",
                                 @"Targets",
                                 @"DefaultPercentage",
                                 @"ProductAmount",
                                 @"EPARegNumber",
                                 @"ApplicationMethods",nil];
        
        NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
        
        [arrTempProduuctDetail addObject:dict_ToSendLeadInfo];
        
        if (arrOfWorkOrderOtherChemicalList==nil) {
            arrOfWorkOrderOtherChemicalList=[[NSMutableArray alloc]init];
        }
        
        [arrOfWorkOrderOtherChemicalList addObject:dict_ToSendLeadInfo];
        
        [arrOfSavedChemicalListOtherAllValues addObject:dict_ToSendLeadInfo];
        _txtAddOtherProductName.text=@"";
        _txtAddOtherProductPercentage.text=@"";
        _txtAddOtherProductAmount.text=@"";
        _txtAddOtherProductEPARegNo.text=@"";
        strGlobalUnitIdOther=@"";
        strGlobalTargetIdOther=@"";
        [_btnUnitOtherMain setTitle:@"Select" forState:UIControlStateNormal];
        [_btnTargetOtherMain setTitle:@"Select" forState:UIControlStateNormal];
        [_btnAppMethodOtherMain setTitle:@"Select" forState:UIControlStateNormal];
        
        
        [self otherchemicalListDynamicViewCreationsOnlyOnce:dictChemicalList :arrTempProduuctDetail];
    }
}

-(void)addFrameIfNothingIsPresent{
    
    CGRect frameForServiceJobDescriptions;
    frameForServiceJobDescriptions=CGRectMake(10, _viewAddOtherProductList.frame.origin.y+_viewAddOtherProductList.frame.size.height+10, self.view.frame.size.width, heightForServiceJobDescriptions);
    frameForServiceJobDescriptions.origin.x=0;
    frameForServiceJobDescriptions.origin.y=10;
    [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
    
    [_scrollForDynamicView addSubview:_viewServiceJobDescriptions];
    
    CGRect frameForPaymentInfo;
    frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
    frameForPaymentInfo.origin.x=10;
    frameForPaymentInfo.origin.y=10;
    [_paymentInfoView setFrame:frameForPaymentInfo];
    
    [_scrollForDynamicView addSubview:_paymentInfoView];
    
    
    CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
    frameForPaymentType.origin.x=10;
    if (_paymentInfoView.frame.size.height==0) {
        frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
        
    }else{//MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
        if (MainViewFormChemicalOther.frame.size.height==0) {
            
            frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20;
            
        }else{
            frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20;
        }
    }
    
    
    [_paymentTypeView setFrame:frameForPaymentType];
    
    [_scrollForDynamicView addSubview:_paymentTypeView];
    
    
}
- (IBAction)actionOnTempChemicalList:(id)sender
{
    
    strWorkOrderStatuss=@"InComplete";
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ChemicalListPreview
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChemicalListPreview"];
    
    
    if (finalJsonDict.count==0) {
        if (arrResponseInspection.count==0) {
            
        } else {
            
            objByProductVC.dictJsonDynamicForm=[arrResponseInspection objectAtIndex:0];
            
        }
        
        
    } else {
        objByProductVC.dictJsonDynamicForm=finalJsonDict;
        
    }
    objByProductVC.workOrderDetail=matchesWorkOrder;
    objByProductVC.paymentInfoDetail=matchesPaymentInfo;
    objByProductVC.dictOfChemicalList=dictChemicalList;
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
        objByProductVC.arrOfChemicalListOther=arrOfWorkOrderOtherChemicalList;
        objByProductVC.arrOfChemicalList=arrOfWorkOrderProductDetails;
        objByProductVC.arrChemicalList=arrOfWorkOrderProductDetails;//[dictChemicalList valueForKey:@""];
        
    }
    else {
        objByProductVC.arrOfChemicalListOther=arrOfSelectedChemicalToSendToNextViewOther;
        objByProductVC.arrOfChemicalList=arrOfSelectedChemicalToSendToNextView;
        objByProductVC.arrChemicalList=arrOfSelectedChemicalToSendToNextView;//[dictChemicalList valueForKey:@""];
        
        
    }
    objByProductVC.strPaymentModes=strGlobalPaymentMode;
    objByProductVC.strDepartmentIdd=strDepartmentId;
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        objByProductVC.strPaidAmount=_txtAmount.text;
        
    }
    else
    {
        
        objByProductVC.strPaidAmount=_txtAmountSingle.text;
        
    }
    
    objByProductVC.arrAllObjImageDetail=arrAllObjImageDetail;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    //[self.navigationController pushViewController:objByProductVC animated:YES];
    
    /*    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
     bundle: nil];
     ChemicalListPreview
     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChemicalListPreview"];
     [self.navigationController pushViewController:objByProductVC animated:YES];*/
    
    
}
-(void)fetchDepartmentNameBySysName
{
    
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    
    
    //Branch Master Fetch
    
    NSArray *arrBranchMaster;
    arrBranchMaster=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    
    NSMutableArray *arrDeptVal,*arrDeptKey;
    arrDeptVal=[[NSMutableArray alloc]init];
    arrDeptKey=[[NSMutableArray alloc]init];
    
    NSDictionary *dictMasterKeyValue;
    for (int i=0; i<arrBranchMaster.count; i++)
    {
        NSDictionary *dict=[arrBranchMaster objectAtIndex:i];
        NSArray *arrDepartments=[dict valueForKey:@"Departments"];
        for (int k=0; k<arrDepartments.count; k++)
        {
            NSDictionary *dict1=[arrDepartments objectAtIndex:k];
            [arrDeptVal addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"Name"]]];
            [arrDeptKey addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"SysName"]]];
        }
        
    }
    dictMasterKeyValue=[NSDictionary dictionaryWithObjects:arrDeptVal forKeys:arrDeptKey];
    dictDeptNameByKey=dictMasterKeyValue;
    NSLog(@"dictMasterKeyValue>>%@",dictMasterKeyValue);
}
-(void)setScrollHeightonClick{
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_viewAddEquipment.frame.size.height+_viewAddEquipment.frame.origin.y)];
    
}

-(void)goToEquipments{
    
    /*
     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
     bundle: nil];
     EquipmentsViewController
     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"EquipmentsViewController"];
     objByProductVC.strStatusWo=strWorkOrderStatuss;
     [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
     
     */
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    EquipmentsViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"EquipmentsViewController"];
    objByProductVC.strStatusWo=strWorkOrderStatuss;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)setEditableNoForCompleteWO{
    
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
        [_btnAddOtherProductMain setEnabled:NO];
        [_btnAppMethodOtherMain setEnabled:NO];
        [_btnTargetOtherMain setEnabled:NO];
        [_btnUnitOtherMain setEnabled:NO];
        [_txtAddOtherProductName setEnabled:NO];
        [_txtAddOtherProductAmount setEnabled:NO];
        [_txtAddOtherProductEPARegNo setEnabled:NO];
        [_txtAddOtherProductPercentage setEnabled:NO];
        
    } else {
        [_btnAddOtherProductMain setEnabled:YES];
        [_btnAppMethodOtherMain setEnabled:YES];
        [_btnTargetOtherMain setEnabled:YES];
        [_btnUnitOtherMain setEnabled:YES];
        [_txtAddOtherProductName setEnabled:YES];
        [_txtAddOtherProductAmount setEnabled:YES];
        [_txtAddOtherProductEPARegNo setEnabled:YES];
        [_txtAddOtherProductPercentage setEnabled:YES];
    }
}
-(BOOL)isCompulsoryImage{
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isCompulsory=[defsss boolForKey:@"isCompulsoryBeforeImageService"];
    if (isCompulsory) {
        
        if (arrOfBeforeImageAll.count==0) {
            
            isCompulsory=YES;
            
        } else {
            
            isCompulsory=NO;
            
        }
        
    } else {
        
        isCompulsory=NO;
    }
    if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"]) {
        
        isCompulsory=NO;
        
    }
    
    return isCompulsory;
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
-(void)customPicker
{
    myPickerView = [[UIPickerView alloc]init];
    myPickerView.dataSource =self;
    myPickerView.delegate =self;
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.frame=CGRectMake(0,CGRectGetMaxY(self.view.frame)-260,[UIScreen mainScreen].bounds.size.width,260 );
    
    myPickerView.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1.0];
    [self.view addSubview:myPickerView];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done)];
    
    //[doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [doneButton setTintColor:[UIColor whiteColor]];
    
    toolBar = [[UIToolbar alloc]initWithFrame:
               CGRectMake(0,CGRectGetMaxY(self.view.frame)-300,[UIScreen mainScreen].bounds.size.width,40)];
    
    [toolBar setBarStyle:UIBarStyleBlack];
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    [self.view addSubview:toolBar];
}
-(void)done
{
    [myPickerView removeFromSuperview];
    [toolBar removeFromSuperview];
}
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return [arrPickerServiceJobDescriptions count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component
{
    
    NSDictionary *dictData=[arrPickerServiceJobDescriptions objectAtIndex:row];
    NSString *strServiceJobDescription = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
    _textView_ServiceJobDescriptions.text=strServiceJobDescription;
    strGlobalServiceJobDescriptionsText = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
    strGlobalServiceJobDescriptionId = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]];
    
}
/*- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
 (NSInteger)row forComponent:(NSInteger)component
 {
 NSString *title = [arrPicker objectAtIndex:row];
 NSAttributedString *attString =
 [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
 
 return attString;
 // return [arrPicker objectAtIndex:row];
 }*/
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSDictionary *dictData=[arrPickerServiceJobDescriptions objectAtIndex:row];
    
    NSString *title = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Title"]];
    
    strGlobalServiceJobDescriptionsText = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
    
    strGlobalServiceJobDescriptionId = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]];
    
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
}

#pragma mark- Service Job Description Method

- (IBAction)action_ServiceJobDescriptionTitle:(id)sender {
    
    
    if (arrPickerServiceJobDescriptions.count==0) {
        
        [global AlertMethod:Alert :NoDataAvailableServiceJobDescriptions];
        
    }else{
        
        tblData.tag=1874919421;
        [self tableLoad:1874919421];
        
    }
}

-(void)fetchServiceJobDescriptionsFromMaster{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    if ([dictdata isKindOfClass:[NSString class]]) {
        
        
    }else{
        
        arrPickerServiceJobDescriptions=nil;
        arrPickerServiceJobDescriptions=[[NSMutableArray alloc]init];
        
        NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceJobDescriptionTemplate"];
        if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
            
            NSDictionary *dictTemp=@{@"CompanyId":@"",@"IsActive":@"",@"ServiceJobDescription":@"",@"ServiceJobDescriptionId":@"",@"Title":@"Select"};
            
            [arrPickerServiceJobDescriptions addObject:dictTemp];
            
            for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                
                NSDictionary *dictData=[arrOfServiceJobDescriptionTemplate objectAtIndex:k];
                
                BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
                
                if (isActiveeee) {
                    
                    [arrPickerServiceJobDescriptions addObject:dictData];
                    
                }
            }
            
        }else{
            
            
        }
    }
}
-(void)updateServiceJobDescription{
    
    [matchesWorkOrder setValue:strGlobalServiceJobDescriptionId forKey:@"serviceJobDescriptionId"];
    // [matchesWorkOrder setValue:[self getHTML] forKey:@"serviceJobDescription"];
    [matchesWorkOrder setValue:_textView_ServiceJobDescriptions.text forKey:@"serviceJobDescription"];
    
    NSError *error;
    [context save:&error];
    
}
- (NSString *)getHTML {
    NSDictionary *exportParams = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
    NSData *htmlData = [_textView_ServiceJobDescriptions.attributedText dataFromRange:NSMakeRange(0, _textView_ServiceJobDescriptions.attributedText.length) documentAttributes:exportParams error:nil];
    return [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
}

-(void)addEquipmentView{
    
    CGFloat yXisForBtnCheckUncheck=0.0;
    yXisForBtnCheckUncheck=MainViewForm.frame.size.height+MainViewForm.frame.origin.y+50;
    
    CGRect frameForEquipmentView;
    frameForEquipmentView=CGRectMake(0, yXisForBtnCheckUncheck+10, self.view.frame.size.width, 50);
    frameForEquipmentView.origin.x=0;
    frameForEquipmentView.origin.y=yXisForBtnCheckUncheck+80;
    [_viewAddEquipment setFrame:frameForEquipmentView];
    [_scrollForDynamicView addSubview:_viewAddEquipment];
    
    _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, _viewAddEquipment.frame.size.height+_viewAddEquipment.frame.origin.y+600);
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_viewAddEquipment.frame.size.height+_viewAddEquipment.frame.origin.y+600)];
    
    [self setScrollHeightonClick];
    
}

-(void)noChemicalListMethod{
    
    
    CGFloat yXisForBtnCheckUncheck=0.0;
    yXisForBtnCheckUncheck=MainViewForm.frame.size.height+MainViewForm.frame.origin.y+50;
    
    // Service job descriptions Changes
    
    CGRect frameForServiceJobDescriptions;
    frameForServiceJobDescriptions=CGRectMake(0, yXisForBtnCheckUncheck+10, self.view.frame.size.width, heightForServiceJobDescriptions);
    frameForServiceJobDescriptions.origin.x=0;
    frameForServiceJobDescriptions.origin.y=yXisForBtnCheckUncheck+80;
    [_viewServiceJobDescriptions setFrame:frameForServiceJobDescriptions];
    
    [_scrollForDynamicView addSubview:_viewServiceJobDescriptions];
    
    
    CGRect frameForPaymentInfo;
    frameForPaymentInfo=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10, self.view.frame.size.width-20, _paymentInfoView.frame.size.height);
    frameForPaymentInfo.origin.x=10;
    frameForPaymentInfo.origin.y=_viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+80;
    [_paymentInfoView setFrame:frameForPaymentInfo];
    
    [_scrollForDynamicView addSubview:_paymentInfoView];
    
    //**************************************
#pragma mark- Payment Type View Settings
    //**************************************
    
    
    CGRect frameForPaymentType=CGRectMake(10, _paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
    frameForPaymentType.origin.x=10;
    if (_paymentInfoView.frame.size.height==0) {
        frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+10;
        
    }else{//MainViewFormChemical.frame.origin.y+MainViewFormChemical.frame.size.height+80
        if (MainViewFormChemicalOther.frame.size.height==0) {
            
            frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20;
            
        }else{
            frameForPaymentType.origin.y=_paymentInfoView.frame.origin.y+_paymentInfoView.frame.size.height+20;
        }
    }
    
    
    [_paymentTypeView setFrame:frameForPaymentType];
    
    [_scrollForDynamicView addSubview:_paymentTypeView];
    
    
    
    //**************************************
#pragma mark- Amount And Check View Settings
    //**************************************
    
    
    CGRect frameForCheckView=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+20, self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
    frameForCheckView.origin.x=10;
    frameForCheckView.origin.y=_paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10;
    
    
    [_amountViewSingle setFrame:frameForCheckView];
    
    [_scrollForDynamicView addSubview:_amountViewSingle];
    
    
    CGRect frameForbeforeImageView=CGRectMake(10, _amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+20, self.view.frame.size.width-20, _beforeImageView.frame.size.height);
    frameForbeforeImageView.origin.x=10;
    frameForbeforeImageView.origin.y=_amountViewSingle.frame.origin.y+_amountViewSingle.frame.size.height+10;
    
    [_beforeImageView setFrame:frameForbeforeImageView];
    
    [_scrollForDynamicView addSubview:_beforeImageView];
    
    if (!(arrOfBeforeImageAll.count==0)) {
        
        CGRect frameForbeforeImageViewThumnNail=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _beforeImageThumbNailView.frame.size.height);
        frameForbeforeImageViewThumnNail.origin.x=10;
        frameForbeforeImageViewThumnNail.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        [_beforeImageThumbNailView setFrame:frameForbeforeImageViewThumnNail];
        
        [_scrollForDynamicView addSubview:_beforeImageThumbNailView];
        
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageThumbNailView.frame.origin.y+_beforeImageThumbNailView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
        
    }else{
        
        CGRect frameForAfterImage=CGRectMake(10, _beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+20, self.view.frame.size.width-20, _lastViewAfterImage.frame.size.height);
        frameForAfterImage.origin.x=10;
        frameForAfterImage.origin.y=_beforeImageView.frame.origin.y+_beforeImageView.frame.size.height+10;
        
        
        [_lastViewAfterImage setFrame:frameForAfterImage];
        
        [_scrollForDynamicView addSubview:_lastViewAfterImage];
        
    }
    
    _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, _lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y+600);
    
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_lastViewAfterImage.frame.size.height+_lastViewAfterImage.frame.origin.y+600)];
    
    [self setScrollHeightonClick];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //Graph
    if (collectionView==_collectionViewGraph) {
        
        return arrOfImagenameCollewctionViewGraph.count;
        
    } else {
        
        return arrOfImagenameCollewctionView.count;
        
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView==_imageCollectionView) {
        
        
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        //    cell.selected=YES;
        //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        
        NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        return cell;
        
    }else{
        
        //Graph
        static NSString *identifier = @"ServiceGraphCollectionViewCell";
        
        ServiceGraphCollectionViewCell *cell = (ServiceGraphCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        //    cell.selected=YES;
        //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        NSString *str;
        if ([[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictDataa=[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
            str = [dictDataa valueForKey:@"woImagePath"];
            
        } else {
            
            str = [arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
        }
        
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        return cell;
        
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_collectionViewGraph) {
        
        //Graph
        NSString *str;
        if ([[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictDataa=[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
            str = [dictDataa valueForKey:@"woImagePath"];
            
        } else {
            
            str = [arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    } else {
        
        NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        [self goingToPreview : strIndex];
        
    }
    
}

- (IBAction)action_History:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Task-Activity"
                                                             bundle: nil];
    ServiceHistory_iPhoneVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistory_iPhoneVC"];
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];

    /*UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceHistoryMechanical
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistoryMechanicaliPhone"];
    [self.navigationController pushViewController:objByProductVC animated:NO];*/
    
}

//Graph
-(void)deleteGraphImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Graph"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

//Graph
-(void)fetchGraphImageDetailFromDataBase{
    
    
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    arrOfBeforeImageAllGraph=nil;
    arrOfBeforeImageAllGraph=[[NSMutableArray alloc]init];
    
    arrOfImagenameCollewctionViewGraph=nil;
    arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    NSArray *arrAllObjImageDetailNew = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetailNew count] == 0)
    {
        
        
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetailNew.count; j++) {
            
            NSManagedObject *matchesImageDetailNew=arrAllObjImageDetailNew[j];
            NSString *woImageType=[matchesImageDetailNew valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Graph"]) {
                
                NSString *companyKey=[matchesImageDetailNew valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetailNew valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetailNew valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetailNew valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetailNew valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetailNew valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetailNew valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetailNew valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetailNew valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetailNew valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetailNew valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetailNew valueForKey:@"longitude"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy ,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAllGraph addObject:dict_ToSendLeadInfo];
                [arrOfImagenameCollewctionViewGraph addObject:dict_ToSendLeadInfo];
                
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
            }
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


//Graph
-(void)downloadGraphImage
{
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    NSArray *arrOfImagesDetail=arrOfBeforeImageAllGraph;
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
        
    }else if (arrOfImagesDetail.count==0){
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionViewGraph addObjectsFromArray:arrOfImagess];
        
        [self downloadImagesGraph:arrOfImagess];
        
    }
}

//Graph
//For Graph Download Code
//============================================================================
//============================================================================
#pragma mark -----------------------Download Graph Images-------------------------------
//============================================================================
//============================================================================

-(void)downloadImagesGraph :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImageGraph:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1Graph: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    [_collectionViewGraph reloadData];
}

-(void)ShowFirstImageGraph :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1Graph: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionViewGraph.count-1) {
        
        [_collectionViewGraph reloadData];
        
    }
    
    [_collectionViewGraph reloadData];
    
    //    [self ShowFirstImage : name : indexx];
    
}

//Graph
-(void)goingToPreviewGraph :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAllGraph;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        GraphImagePreviewViewController
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewController"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strWorkOrderId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        objImagePreviewSalesAuto.strFromWhere=@"Service";
        objImagePreviewSalesAuto.statusOfWorkOrder=[NSString stringWithFormat:@"%@",strWorkOrderStatuss];
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}

//Graph
- (IBAction)action_GraphImage:(id)sender {
    
    [_collectionViewGraph reloadData];
    
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewFooter.frame.origin.y-_viewGraphImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewGraphImage.frame.size.height);
    [_viewGraphImage setFrame:frameFor_view_BeforeImageInfo];
    [self.view addSubview:_viewGraphImage];
    
}
//Graph
- (IBAction)action_AddGraph:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"firstGraphImage"];
    [defs setBool:YES forKey:@"servicegraph"];
    [defs setBool:YES forKey:@"isForNewGraph"];// Akshay 21 May 2019
    [defs synchronize];
    
    /*  UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
     EditGraphViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewController"];
     objSignViewController.strLeadId=strWorkOrderId;
     objSignViewController.strCompanyKey=strCompanyKey;
     objSignViewController.strUserName=strUserName;
     [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
    
    
    // Akshay 21 may 2019
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
    objSignViewController.strLeadId=[matchesWorkOrder valueForKey:@"workorderId"];
    objSignViewController.strCompanyKey=strCompanyKey;
    objSignViewController.strUserName=strUserName;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    //////////////////
    
}
//Graph
- (IBAction)action_CloseGraphView:(id)sender {
    
    [_viewGraphImage removeFromSuperview];
    
}

- (IBAction)action_AddEquipMentView:(id)sender {
    
    [self goToAddEquipment];
    
}

-(void)goToAddEquipment{
    
    [self updateServiceJobDescription];
    
    BOOL isCompulsory =[self isCompulsoryImage];
    
    if (isCompulsory) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take atleast one before image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }else{
        NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
        
        BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
        isEquipEnabled = YES;
        if (isEquipEnabled) {
            
            NSString *strStatusToSend=strWorkOrderStatuss;
            
            // strWorkOrderStatuss=@"InComplete";
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                                     bundle: nil];
            EquipmentsViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"EquipmentsViewControllerNewServicePestFlow"];
            
            if (finalJsonDict.count==0) {
                if (arrResponseInspection.count==0) {
                    
                } else {
                    objByProductVC.dictJsonDynamicFormEquip=[arrResponseInspection objectAtIndex:0];
                }
                
            } else {
                objByProductVC.dictJsonDynamicFormEquip=finalJsonDict;
                
            }
            objByProductVC.workOrderDetailEquip=matchesWorkOrder;
            objByProductVC.paymentInfoDetailEquip=matchesPaymentInfo;
            objByProductVC.dictOfChemicalListEquip=dictChemicalList;
            if ([strWorkOrderStatuss isEqualToString:@"Complete"] || [strWorkOrderStatuss isEqualToString:@"Completed"])
            {
                objByProductVC.arrOfChemicalListOtherEquip=arrOfWorkOrderOtherChemicalList;
                objByProductVC.arrOfChemicalListEquip=arrOfWorkOrderProductDetails;
                objByProductVC.arrChemicalListEquip=arrOfWorkOrderProductDetails;//[dictChemicalList valueForKey:@""];
                
            }
            else
            {
                
                objByProductVC.arrOfChemicalListOtherEquip=arrOfSelectedChemicalToSendToNextViewOther;
                objByProductVC.arrOfChemicalListEquip=arrOfSelectedChemicalToSendToNextView;
                objByProductVC.arrChemicalListEquip=arrOfSelectedChemicalToSendToNextView;//[dictChemicalList valueForKey:@""];
                
            }
            objByProductVC.strPaymentModesEquip=strGlobalPaymentMode;
            objByProductVC.strDepartmentIddEquip=strDepartmentId;
            if ([strGlobalPaymentMode isEqualToString:@"Check"]) {
                objByProductVC.strPaidAmountEquip=_txtAmount.text;
                
            } else {
                objByProductVC.strPaidAmountEquip=_txtAmountSingle.text;
                
            }
            
            objByProductVC.arrAllObjImageDetailEquip=arrAllObjImageDetail;
            objByProductVC.strStatusWo=strStatusToSend;
            // [self.navigationController pushViewController:objByProductVC animated:NO];
            [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
            
        }else{
            
            // [self goToNextViewDirectInvoice];
            
        }
    }
    
}
- (IBAction)actionOnNotesHistory:(id)sender
{
    //Nilind
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strTypeOfService=@"service";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
    
    //End
}

#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ClockInOutViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

@end

