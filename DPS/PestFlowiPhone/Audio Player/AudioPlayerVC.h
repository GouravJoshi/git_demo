//
//  AudioPlayerVC.h
//  DPS
//
//  Created by Saavan Patidar on 29/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioPlayerVC : UIViewController

@property (strong, nonatomic)  NSString *strAudioName;


@end

NS_ASSUME_NONNULL_END
