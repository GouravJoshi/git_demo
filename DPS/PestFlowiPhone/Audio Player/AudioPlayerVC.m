//
//  AudioPlayerVC.m
//  DPS
//
//  Created by Saavan Patidar on 29/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import "AudioPlayerVC.h"
#import "AllImportsViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioPlayerVC ()
{
    Global *global;
    AVPlayerViewController * moviePlayer1;
}
@end

@implementation AudioPlayerVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    global = [[Global alloc] init];
    
    [self performSelector:@selector(playAudio) withObject:nil afterDelay:0.2];
    
}

- (IBAction)action_Back:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
- (IBAction)action_PlayAgain:(id)sender {
    
    [self playAudio];
    
}

-(void)playAudio{
    
    _strAudioName=[global strNameBackSlashIssue:_strAudioName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",_strAudioName]];
    
    moviePlayer1 = [[AVPlayerViewController alloc] init];
    moviePlayer1.player = [AVPlayer playerWithURL:[NSURL fileURLWithPath:path]];
    moviePlayer1.entersFullScreenWhenPlaybackBegins = true;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [self presentViewController:moviePlayer1 animated:YES completion:^{
          [moviePlayer1.player play];
    }];
    [moviePlayer1.player play];
    
}



@end
