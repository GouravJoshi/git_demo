//
//  EditCaptionDescriptionVC.swift
//  DPS
//
//  Created by Saavan Patidar on 06/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class EditCaptionDescriptionVC: UIViewController {
    
    // MARK: - -----------------------------------IBOutlets-----------------------------------
    
    @IBOutlet var lblTitleHeader: UILabel!
    @IBOutlet var txtViewCaption: UITextView!
    @IBOutlet var txtViewDescription: UITextView!
    @IBOutlet var btnSave: UIButton!
    
    // MARK: - -----------------------------------Global Variables-----------------------------------
    var arrayOfImages = NSArray ()
    var strWoId = NSString ()
    var strWoLeadId = NSString ()
    var strModuleType = NSString ()
    var strImageId = NSString ()
    var strHeaderTitle = NSString ()
    var strWoStatus = NSString ()
    
    //Nilind
    
    var strMobileTargetImageId = String ()
    var strTargetImageDetailId = String ()
    // MARK: - -----------------------------------Life Cycle-----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //buttonRoundedThemeColor(sender: btnSave)
        
        if (strWoStatus == "Completed" || strWoStatus == "Complete")
        {
            
            txtViewDescription.isEditable = false
            txtViewCaption.isEditable = false
            btnSave.isHidden = true
            
        } else {
            
            txtViewDescription.isEditable = true
            txtViewCaption.isEditable = true
            btnSave.isHidden = false
            txtViewCaption.becomeFirstResponder()
            
        }
        
        self.fetchImageFromDB()
        
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        if strModuleType == "ServicePestFlow" {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("imageCaption")
            arrOfKeys.add("imageDescription")
            
            arrOfValues.add(txtViewCaption.text)
            arrOfValues.add(txtViewDescription.text)
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && woImageId == %@", strWoId, strHeaderTitle, strImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        }
        else if strModuleType == "WdoFlow" {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("leadImageCaption")
            arrOfKeys.add("descriptionImageDetail")
            
            arrOfValues.add(txtViewCaption.text)
            arrOfValues.add(txtViewDescription.text)
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadImageId == %@", strWoId, strHeaderTitle, strImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        }
        else if strModuleType as String == flowTypeWdoSalesService {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("imageCaption")
            arrOfKeys.add("imageDescription")
            
            arrOfValues.add(txtViewCaption.text)
            arrOfValues.add(txtViewDescription.text)
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && woImageId == %@", strWoId, strHeaderTitle, strImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("leadImageCaption")
                arrOfKeys.add("descriptionImageDetail")
                
                arrOfValues.add(txtViewCaption.text)
                arrOfValues.add(txtViewDescription.text)
                
                let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strWoLeadId))
                
                var strStageNameLocal = ""
                var strStatusNameLocal = ""
                
                if(arrayLeadDetail.count > 0)
                {
                    
                    let objWdoLeadDetail = arrayLeadDetail.firstObject as! NSManagedObject
                    strStageNameLocal = "\(objWdoLeadDetail.value(forKey: "stageSysName")!)"
                    strStatusNameLocal = "\(objWdoLeadDetail.value(forKey: "statusSysName")!)"
                    
                }
                
                if(strStatusNameLocal.lowercased() == "complete" && strStageNameLocal.lowercased() == "won")
                {
                    
                    // Not Add in Db As Lead complete
                    
                }else{
                    
                    let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadImageId == %@", strWoLeadId, strHeaderTitle, strImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        Global().updateSalesModifydate(strWoLeadId as String)
                        
                    }
                    
                }
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        }
        else if strModuleType as String == flowTypeWdoProblemId {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("imageCaption")
            arrOfKeys.add("imageDescription")
            
            arrOfValues.add(txtViewCaption.text)
            arrOfValues.add(txtViewDescription.text)
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && woImageId == %@", strWoId, strImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("leadImageCaption")
                arrOfKeys.add("descriptionImageDetail")
                
                arrOfValues.add(txtViewCaption.text)
                arrOfValues.add(txtViewDescription.text)
                
                let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strWoLeadId))
                
                var strStageNameLocal = ""
                var strStatusNameLocal = ""
                
                if(arrayLeadDetail.count > 0)
                {
                    
                    let objWdoLeadDetail = arrayLeadDetail.firstObject as! NSManagedObject
                    strStageNameLocal = "\(objWdoLeadDetail.value(forKey: "stageSysName")!)"
                    strStatusNameLocal = "\(objWdoLeadDetail.value(forKey: "statusSysName")!)"
                    
                }
                
                if(strStatusNameLocal.lowercased() == "complete" && strStageNameLocal.lowercased() == "won")
                {
                    
                    // Not Add in Db As Lead complete
                    
                }else{
                    
                    let tempProblemId = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && woImageId == %@", strWoId, strImageId))

                    if tempProblemId.count > 0 {
                        
                        let objTemp = tempProblemId[0] as! NSManagedObject
                        
                        var tempImagePath = "\(objTemp.value(forKey: "woImagePath")!)"
                        
                        tempImagePath = tempImagePath.replacingOccurrences(of: "\\Documents\\ProblemIdentificationImages\\", with: "")
                        
                        let isSuccess =  getDataFromDbToUpdate(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadImagePath == %@", strWoLeadId, "Target", tempImagePath), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess {
                            
                            //Update Modify Date In Work Order DB
                            Global().updateSalesModifydate(strWoLeadId as String)
                            
                        }
                        
                    }
                    
                }
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        }
        else if strModuleType == "Target" {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("leadImageCaption")
            arrOfKeys.add("descriptionImageDetail")
            
            arrOfValues.add(txtViewCaption.text)
            arrOfValues.add(txtViewDescription.text)
            
            
            var isSuccess = Bool()
            
            if strTargetImageDetailId.count > 0
            {
                isSuccess =  getDataFromDbToUpdate(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && targetImageDetailId == %@", strWoId, strHeaderTitle, strTargetImageDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            else
            {
                isSuccess =  getDataFromDbToUpdate(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && mobileTargetImageId == %@", strWoId, strHeaderTitle, strMobileTargetImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            
            
            //let isSuccess =  getDataFromDbToUpdate(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && targetImageDetailId == %@", strWoId, strHeaderTitle, strImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                //updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                Global().updateSalesModifydate(strWoLeadId as String)
                
                dismiss(animated: false)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        }
        else if strModuleType == "SalesFlow" {
            
            
            let arrCRMImage  = getDataFromCoreDataBaseArray(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && leadMediaId = %@", strWoLeadId,strImageId))

            if arrCRMImage.count > 0 // CRM Image Object
            {
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("cRMImageCaption")
                arrOfKeys.add("cRMImageDescription")
                //arrOfKeys.add("isImageSyncforMobile")
                
                arrOfValues.add(txtViewCaption.text)
                arrOfValues.add(txtViewDescription.text)
                //arrOfValues.add("false")
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && leadMediaId == %@", strWoId, strImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    //updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    Global().updateSalesModifydate(strWoLeadId as String)
                    
                    dismiss(animated: false)
                    
                } else {
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            else
            {
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("leadImageCaption")
                arrOfKeys.add("descriptionImageDetail")
                arrOfKeys.add("isImageSyncforMobile")
                
                arrOfValues.add(txtViewCaption.text)
                arrOfValues.add(txtViewDescription.text)
                arrOfValues.add("false")
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadImageId == %@", strWoId, strHeaderTitle, strImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    //updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    Global().updateSalesModifydate(strWoLeadId as String)
                    
                    dismiss(animated: false)
                    
                } else {
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            

            
        }
    }
    
    // MARK: - -----------------------------------Functions-----------------------------------
    
    func fetchImageFromDB() {
        
        // Before  Graph  After
        
        if strModuleType == "ServicePestFlow" {
            
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && woImageId == %@", strWoId, strHeaderTitle, strImageId))
            
        }
        else if strModuleType == "WdoFlow" {
            
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadImageId == %@", strWoId, strHeaderTitle, strImageId))
            
        }
        else if strModuleType as String == flowTypeWdoSalesService {
            
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && woImageId == %@", strWoId, strHeaderTitle, strImageId))

        }
        else if strModuleType as String == flowTypeWdoProblemId {
            
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && woImageId == %@", strWoId, strImageId))
            
        }
        else if strModuleType == "Target"
        {
            if strTargetImageDetailId.count > 0
            {
                arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && targetImageDetailId == %@", strWoId, strHeaderTitle, strTargetImageDetailId))

            }
            else
            {
                arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && mobileTargetImageId == %@", strWoId, strHeaderTitle, strMobileTargetImageId))

            }
            
           // arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && targetImageDetailId == %@", strWoId, strHeaderTitle, strImageId))
            
        }
        else if strModuleType == "SalesFlow" {
            
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadImageId == %@", strWoId, strHeaderTitle, strImageId))
            
            if arrayOfImages.count == 0
            {
                arrayOfImages  = getDataFromCoreDataBaseArray(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && leadMediaId == %@", strWoId,strImageId))
            }

        }
        if arrayOfImages.count > 0 {
            
            let objTemp = arrayOfImages[0] as! NSManagedObject
            
            if strModuleType == "ServicePestFlow"
            {
                txtViewCaption.text = (objTemp.value(forKey: "imageCaption") as! String)
                txtViewDescription.text = (objTemp.value(forKey: "imageDescription") as! String)
            }
            else if strModuleType == "WdoFlow"
            {
                txtViewCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String)
                txtViewDescription.text = (objTemp.value(forKey: "descriptionImageDetail") as! String)
            }
            else if strModuleType as String == flowTypeWdoSalesService
            {
                txtViewCaption.text = (objTemp.value(forKey: "imageCaption") as! String)
                txtViewDescription.text = (objTemp.value(forKey: "imageDescription") as! String)
            }
            else if strModuleType as String == flowTypeWdoProblemId
            {
                txtViewCaption.text = (objTemp.value(forKey: "imageCaption") as! String)
                txtViewDescription.text = (objTemp.value(forKey: "imageDescription") as! String)
            }
            else if strModuleType == "Target"
            {
                txtViewCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String)
                txtViewDescription.text = (objTemp.value(forKey: "descriptionImageDetail") as! String)
            }
            else if strModuleType == "SalesFlow"
            {
                if objTemp.isKind(of: ImageDetail.self)
                {
                    txtViewCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String)
                    txtViewDescription.text = (objTemp.value(forKey: "descriptionImageDetail") as! String)
                }
                else
                {
                    txtViewCaption.text = (objTemp.value(forKey: "cRMImageCaption") as! String)
                    txtViewDescription.text = (objTemp.value(forKey: "cRMImageDescription") as! String)
                }

            }
            
        } else {
            
            dismiss(animated: false)
            
        }
        
    }
    
}

extension EditCaptionDescriptionVC : UITextViewDelegate{
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if range.location == 0 && text == " " {
            return false
        }
        return true
        
    }
    
}
