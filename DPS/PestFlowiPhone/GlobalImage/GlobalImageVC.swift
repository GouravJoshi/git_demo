//
//  GlobalImageVC.swift
//  DPS
//
//  Created by Saavan Patidar on 06/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos

class GlobalImageVC: UIViewController,UIDocumentPickerDelegate {

    // MARK: - -----------------------------------IBOutlets-----------------------------------
    
    @IBOutlet var tblViewImages: UITableView!
    @IBOutlet var lblTitleHeader: UILabel!
    @IBOutlet var btnAddImage: UIButton!

    // MARK: - -----------------------------------Global Variables-----------------------------------
    var arrayOfImages = NSArray ()
    @objc  var strHeaderTitle = NSString ()
    @objc var strWoId = NSString ()
    @objc var strWoLeadId = NSString ()
    @objc var strModuleType = NSString ()
    @objc var strWoStatus = NSString ()
    var imagePicker = UIImagePickerController()

    @objc var strTargetId = NSString ()
    @objc var dictTarget = NSDictionary ()

    
    //Nilind
    var strTargetImageDetailId = String ()
    var strMobileTargetImageId = String ()
    
    var isImagePick = true
    var strCRMImageId = ""
    
    // MARK: - -----------------------------------Life Cycle-----------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitleHeader.text = strHeaderTitle as String + " Images"
        tblViewImages.estimatedRowHeight = 50.0
        tblViewImages.tableFooterView = UIView()
        
        //if strWoStatus == "Completed"
        if ((strWoStatus == "Completed") || (strWoStatus == "Complete"))
        {
            
            btnAddImage.isEnabled = false
            
        } else {
            
            btnAddImage.isEnabled = true
            
        }
       // btnAddImage.isEnabled = true
        // Do any additional setup after loading the view.
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if nsud.bool(forKey: "IgnoreCameraPermissions") {

            
            
        }else{
            
            alertForPermissions()
            
        }

    }
    
    func alertForPermissions() {
        
        if checkPhotoLibraryPermission() {
            DispatchQueue.main.async {
                self.gotoSettings()
            }
        }else{
            if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
                // Already Authorized
            } else {
                AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                   if granted == true {
                       // User granted
                   } else {
                    DispatchQueue.main.async {
                        self.gotoSettings()
                    }
                }
               })
            }
        }
        
    }
    
    func checkPhotoLibraryPermission() -> Bool
      {
        
        var goToSettings = false
        
          let status = PHPhotoLibrary.authorizationStatus()
          switch status {
          case .authorized: break
              //handle authorized status
          case .denied, .restricted :
            goToSettings = true

            break
                  //handle denied
                   case .notDetermined:
                      // ask for permissions
                      PHPhotoLibrary.requestAuthorization { status in switch status {
                      case .authorized:
                        
                        break
                      // as above
                      case .denied, .restricted:
                        goToSettings = true

                        break
                      // as above
                      case .notDetermined:
                        
                        goToSettings = true
                        
                        break // won't happen but still
                      case .limited:
                          break
                      @unknown default:
                          break
                      }
                      
                      }
                      
          case .limited:
              break
          @unknown default:
              break
          }
        
        return goToSettings
          
      }
    
    func gotoSettings() {
        
        let alert = UIAlertController(title: nil, message: "Camera/Photos permissions not allowed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                
                nsud.set(false, forKey: "IgnoreCameraPermissions")
                nsud.synchronize()
                
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Never ask again", style: .default) { action in
            
            nsud.set(true, forKey: "IgnoreCameraPermissions")
            nsud.synchronize()
            
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            
            nsud.set(false, forKey: "IgnoreCameraPermissions")
            nsud.synchronize()
            
        })
        self.present(alert, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("WILL APPEAR CALLED")
        //fetchImageFromDB()
        methodForEditImageSales()
        do {
            sleep(1)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                  self.fetchImageFromDB()
                  
              }
        
        //goToSelectionView()
        
    }
    func methodForEditImageSales()  {
        
        if nsud.bool(forKey: "fromEditImageSales") == true && strCRMImageId != ""
        {
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
                        
            arrOfKeys = [
                "isImageSyncforMobile",
                "isImageEdited"
            ]
            
            
            arrOfValues = [
                "false",
                "true"
            ]
            
            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && leadMediaId == %@", strWoId, strCRMImageId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            
            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
            nsud.setValue(false, forKey: "fromEditImageSales")
            nsud.synchronize()
            
            Global().updateSalesModifydate(strWoLeadId as String)
        }
    }

    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_AddImage(_ sender: Any) {
        
        var limitCount = 10
        
        if (strModuleType == "SalesFlow")
        {
            limitCount = 25
        }
        
        if arrayOfImages.count < limitCount {
            
            self.isImagePick = true
            
            if strHeaderTitle == "Graph"
            {
                if(strModuleType == "SalesFlow")
                {
                    if DeviceType.IS_IPAD
                    {
                        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
                        
                        alert.addAction(UIAlertAction(title: "Detail Graph", style: .default , handler:{ (UIAlertAction)in
                            
                            
                            var arrTemp = NSArray()
                            
                            var chkNewGrpah = Bool()
                            
                            chkNewGrpah = false
                            
                            arrTemp = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@  && isNewGraph == %@", self.strWoId, self.strHeaderTitle,"true"))
                            
                            if arrTemp.count > 0
                            {
                                chkNewGrpah = true
                            }
                            if chkNewGrpah == true
                            {
                                let alertCOntroller = UIAlertController(title: alertMessage, message: "New graph already exist, Do you want to update it ?", preferredStyle: .alert)
                                
                                let alertAction = UIKit.UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                                    
                                    self.goToNewGraph()
                                })
                                
                                let alertActionNo = UIKit.UIAlertAction(title: "No", style: .cancel, handler: { (action) in
                                    
                                    
                                })
                                
                                alertCOntroller.addAction(alertActionNo)
                                alertCOntroller.addAction(alertAction)
                                self.present(alertCOntroller, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                self.goToNewGraph()
                            }
                            //self.goToNewGraph()
                        }))
                        
                        
                        alert.addAction(UIAlertAction(title: "Graph w free form", style: .default , handler:{ (UIAlertAction)in
                            
                            self.goToAddGraph()
                        }))
                        
                        
                        
                        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                        }))
                        
                        alert.popoverPresentationController?.sourceView = self.btnAddImage
                        
                        self.present(alert, animated: true, completion: {
                        })
                    }
                    else
                    {
                        //self.goToAddGraph()
                        
                        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
                        
                        alert.addAction(UIAlertAction(title: "Detail Graph", style: .default , handler:{ (UIAlertAction)in
                            
                            
                            var arrTemp = NSArray()
                            
                            var chkNewGrpah = Bool()
                            
                            chkNewGrpah = false
                            
                            arrTemp = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@  && isNewGraph == %@", self.strWoId, self.strHeaderTitle,"true"))
                            
                            if arrTemp.count > 0
                            {
                                chkNewGrpah = true
                            }
                            if chkNewGrpah == true
                            {
                                let alertCOntroller = UIAlertController(title: alertMessage, message: "New graph already exist, Do you want to update it ?", preferredStyle: .alert)
                                
                                let alertAction = UIKit.UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                                    
                                    self.goToNewGraph()
                                })
                                
                                let alertActionNo = UIKit.UIAlertAction(title: "No", style: .cancel, handler: { (action) in
                                    
                                    
                                })
                                
                                alertCOntroller.addAction(alertActionNo)
                                alertCOntroller.addAction(alertAction)
                                self.present(alertCOntroller, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                self.goToNewGraph()
                            }
                            //self.goToNewGraph()
                        }))
                        
                        
                        alert.addAction(UIAlertAction(title: "Graph w free form", style: .default , handler:{ (UIAlertAction)in
                            
                            self.goToAddGraph()
                        }))
                        
                        
                        
                        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                        }))
                        
                        alert.popoverPresentationController?.sourceView = self.btnAddImage
                        
                        self.present(alert, animated: true, completion: {
                        })
                    }
                }
                else
                {
                    self.goToAddGraph()
                }

            }
            else
            {
                
                if DeviceType.IS_IPAD
                {
                    
                    let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
                        self.imagePicker = UIImagePickerController()
                        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                            print("Button capture")
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .photoLibrary
                            self.imagePicker.allowsEditing = false
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                    }))
                    
                    
                    alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
                        self.imagePicker = UIImagePickerController()
                        
                        if UIImagePickerController.isSourceTypeAvailable(.camera){
                            print("Button capture")
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .camera
                            self.imagePicker.allowsEditing = false
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                    }))
                    
                    
                    alert.addAction(UIAlertAction(title: "More...", style: .default , handler:{ (UIAlertAction)in
                     let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypeImage),String(kUTTypePDF)], in: UIDocumentPickerMode.import)
                                                documentPicker.delegate = self
                                              documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                                              documentPicker.popoverPresentationController?.sourceView = self.view

                                              self.present(documentPicker, animated: true, completion: nil)

                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                    }))
                    //alert.popoverPresentationController?.sourceRect = sender.frame
                    alert.popoverPresentationController?.sourceView = self.view
                    
                    self.present(alert, animated: true, completion: {
                    })
                    
                }else{
                    
                    let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
                    
                    alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
                        self.imagePicker = UIImagePickerController()
                        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                            print("Button capture")
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .photoLibrary
                            self.imagePicker.allowsEditing = false
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                    }))
                    
                    
                    alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
                        self.imagePicker = UIImagePickerController()
                        
                        if UIImagePickerController.isSourceTypeAvailable(.camera){
                            print("Button capture")
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .camera
                            self.imagePicker.allowsEditing = false
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                    }))
                    
                    
                    alert.addAction(UIAlertAction(title: "More...", style: .default , handler:{ (UIAlertAction)in
                     let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypeImage),String(kUTTypePDF)], in: UIDocumentPickerMode.import)
                                                documentPicker.delegate = self
                                              documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                                              documentPicker.popoverPresentationController?.sourceView = self.view

                                              self.present(documentPicker, animated: true, completion: nil)

                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                    }))
                    //alert.popoverPresentationController?.sourceRect = sender.frame
                    alert.popoverPresentationController?.sourceView = self.view
                    
                    self.present(alert, animated: true, completion: {
                    })
                    
                }

                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Max \(limitCount) no. of images can be chosen", viewcontrol: self)
            
        }
        
    }
    
    // MARK: - -----------------------------------Functions-----------------------------------
    
    func goToSelectionView()  {
        
        let array = NSMutableArray()
        
        let dict = NSMutableDictionary()
        let dict1 = NSMutableDictionary()
        let dict2 = NSMutableDictionary()
        let dict3 = NSMutableDictionary()
        let dict4 = NSMutableDictionary()
        let dict5 = NSMutableDictionary()

        dict.setValue("Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan Saavan", forKey: "Name")
        dict1.setValue("Patidar", forKey: "Name")
        dict2.setValue("Test", forKey: "Name")
        dict3.setValue("Mukesh", forKey: "Name")
        dict4.setValue("iphone", forKey: "Name")
        dict5.setValue("work", forKey: "Name")

        array.add(dict)
        array.add(dict1)
        array.add(dict2)
        array.add(dict3)
        array.add(dict4)
        array.add(dict5)

        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionVC = storyboardIpad.instantiateViewController(withIdentifier: "SelectionVC") as! SelectionVC
        vc.strTitle = "Select"
        vc.strTag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.delegate = self
        vc.aryList = array
        present(vc, animated: false, completion: {})
        
    }

    func goToCaptionDescriptions(strWoImageId : String)  {
        
        var storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        var testController = storyboardIpad.instantiateViewController(withIdentifier: "EditCaptionDescriptionVC") as? EditCaptionDescriptionVC
        
        if DeviceType.IS_IPAD {
            
             storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
             testController = storyboardIpad.instantiateViewController(withIdentifier: "EditCaptionDescriptioniPadVC") as? EditCaptionDescriptionVC
            
        }
        
        testController?.strWoId = self.strWoId
        testController?.strModuleType = self.strModuleType
        testController?.strImageId = strWoImageId as NSString
        testController?.strHeaderTitle = strHeaderTitle
        testController?.strWoStatus = strWoStatus
        testController?.strWoLeadId = strWoLeadId
        
        //Nilind
        testController?.strTargetImageDetailId = strTargetImageDetailId
        testController?.strMobileTargetImageId = strMobileTargetImageId
        
                
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAddGraph()  {
        
        // Akshay
        if(strModuleType == "ServicePestFlow")
        {
            nsud.set(true, forKey: "firstGraphImage")
            nsud.set(true, forKey: "servicegraph")
            nsud.set(true, forKey: "isForNewGraph")
            nsud.synchronize()
        }
        else if(strModuleType == "WdoFlow")
        {
            nsud.set(true, forKey: "firstGraphImage")
            nsud.set(false, forKey: "servicegraph")
            nsud.set(false, forKey: "isForNewGraph")
            nsud.synchronize()
        }
        else if(strModuleType as String == flowTypeWdoSalesService)
        {
            nsud.set(true, forKey: "firstGraphImage")
            nsud.set(true, forKey: "servicegraph")
            nsud.set(true, forKey: "isForNewGraph")
            nsud.synchronize()
        }
        else if(strModuleType == "SalesFlow")
        {
            nsud.set(true, forKey: "firstGraphImage")
            nsud.set(false, forKey: "servicegraph")
            nsud.set(false, forKey: "isForNewGraph")
            nsud.synchronize()
        }
        ////

        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        var strUserName = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            
            strUserName = "\(value)"
        }
        
        var storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        
        if DeviceType.IS_IPAD{
            
            storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
            
        }
        
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GraphDrawingBoardViewController") as? GraphDrawingBoardViewController
        testController?.strLeadId = self.strWoId as String
        testController?.strUserName = strUserName
        testController?.strCompanyKey = strCompanyKey
        testController?.strModuleType = strModuleType as String
        testController?.strWdoLeadId = strWoLeadId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func fetchImageFromDB() {
        
        // Before  Graph  After
        if strModuleType == "ServicePestFlow" {
            
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, strHeaderTitle))

        }
        else if strModuleType == "WdoFlow" {
            
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", strWoId, strHeaderTitle))
            
        }
        else if strModuleType as String == flowTypeWdoSalesService {
            
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, strHeaderTitle))

        }
        else if strModuleType == "Target" {
            
            let strLeadCommercialTargetId = "\(dictTarget.value(forKey: "leadCommercialTargetId") ?? "")"
            let strMobileTargetId = "\(dictTarget.value(forKey: "mobileTargetId") ?? "")"
            
            if strLeadCommercialTargetId.count > 0
            {
                arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadCommercialTargetId = %@", strWoId, strHeaderTitle, strLeadCommercialTargetId))
            }
            else
            {
                arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && mobileTargetId == %@", strWoId, strHeaderTitle, strMobileTargetId))
            }
        }
        else if strModuleType == "SalesFlow" {
            
            let arrCRMImage  = getDataFromCoreDataBaseArray(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@", strWoId))
            
            let arrSalesImage = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", strWoId, strHeaderTitle))

            let arr = NSMutableArray()
            
            if strHeaderTitle != "Graph"
            {
                for item in arrCRMImage
                {
                    let obj = item as! NSManagedObject
                    arr.add(obj)
                    
                }
            }

            for item in arrSalesImage
            {
                let obj = item as! NSManagedObject
                arr.add(obj)
                
            }

            arrayOfImages = arr as NSArray
            
            //arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", strWoId, strHeaderTitle))
            
        }
        //arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, strHeaderTitle))
        
        if arrayOfImages.count > 0 {
            
            tblViewImages.isHidden = false
            tblViewImages.reloadData()
            
        } else {
            
            tblViewImages.isHidden = true
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)

        }
        
    }
    
    func SaveImageToDb(image : UIImage) {
        
        if strModuleType == "ServicePestFlow" {
            
            let strImageName = "\\Documents\\UploadImages\\Img"  + "\(strHeaderTitle)" + "\(getUniqueValueForId())" + ".jpg"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("woImagePath")
            arrOfKeys.add("woImageType")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("woImageId")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("imageCaption")
            arrOfKeys.add("imageDescription")
            arrOfKeys.add("latitude")
            arrOfKeys.add("longitude")
            
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            var strCompanyKey = String()
            var strUserName = String()
            var strEmployeeid = String() // EmployeeId
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                
                strCompanyKey = "\(value)"
            }
            
            if let value = dictLoginData.value(forKeyPath: "Company.Username") {
                
                strUserName = "\(value)"
            }
            
            if let value = dictLoginData.value(forKey: "EmployeeId") {
                
                strEmployeeid = "\(value)"
                
            }
            
            let coordinate = Global().getLocation()
            let latitude = "\(coordinate.latitude)"
            let longitude = "\(coordinate.longitude)"
            
            let woImageId = "Mobile" + getUniqueValueForId()
            let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
            
            
            arrOfValues.add("") //createdBy
            arrOfValues.add("") //createdDate
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strImageName)
            arrOfValues.add(strHeaderTitle)
            arrOfValues.add(strEmployeeid)
            arrOfValues.add(woImageId) // woImageId
            arrOfValues.add(modifiedDate ?? "")
            arrOfValues.add(strWoId)
            arrOfValues.add("") // imageCaption
            arrOfValues.add("") // imageDescription
            arrOfValues.add(latitude) // latitude
            arrOfValues.add(longitude) //  longitude
            
            saveDataInDB(strEntity: "ImageDetailsServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            let imageResized = Global().resizeImageGloballl(image)
            
            saveImageDocumentDirectory(strFileName: strImageName, image: imageResized!)
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
            goToCaptionDescriptions(strWoImageId: woImageId )
            
        }
        else if strModuleType == "WdoFlow" {
            
            let strImageName = "Img"  + "\(strHeaderTitle)" + "\(getUniqueValueForId())" + ".jpg"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("leadImagePath")
            arrOfKeys.add("leadImageType")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("leadImageId")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("leadId")
            arrOfKeys.add("leadImageCaption")
            arrOfKeys.add("descriptionImageDetail")
            arrOfKeys.add("latitude")
            arrOfKeys.add("longitude")
            arrOfKeys.add("isAddendum")
            
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            var strCompanyKey = String()
            var strUserName = String()
            var strEmployeeid = String() // EmployeeId
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                
                strCompanyKey = "\(value)"
            }
            
            if let value = dictLoginData.value(forKeyPath: "Company.Username") {
                
                strUserName = "\(value)"
            }
            
            if let value = dictLoginData.value(forKey: "EmployeeId") {
                
                strEmployeeid = "\(value)"
            }
            
            let coordinate = Global().getLocation()
            let latitude = "\(coordinate.latitude)"
            let longitude = "\(coordinate.longitude)"
            
            let woImageId = "Mobile" + getUniqueValueForId()
            let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
            
            
            arrOfValues.add("") //createdBy
            arrOfValues.add("") //createdDate
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strImageName)
            arrOfValues.add(strHeaderTitle)
            arrOfValues.add(strEmployeeid)
            arrOfValues.add(woImageId) // woImageId
            arrOfValues.add(modifiedDate ?? "")
            arrOfValues.add(strWoId)
            arrOfValues.add("") // imageCaption
            arrOfValues.add("") // imageDescription
            arrOfValues.add(latitude) // latitude
            arrOfValues.add(longitude) //  longitude
            arrOfValues.add("false")
            
            saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            let imageResized = Global().resizeImageGloballl(image)
            
            saveImageDocumentDirectory(strFileName: strImageName, image: imageResized!)
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
            goToCaptionDescriptions(strWoImageId: woImageId )
            
        }
        else if strModuleType as String == flowTypeWdoSalesService {
            
            let strImageName = "\\Documents\\UploadImages\\Img"  + "\(strHeaderTitle)" + "\(getUniqueValueForId())" + ".jpg"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("woImagePath")
            arrOfKeys.add("woImageType")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("woImageId")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("imageCaption")
            arrOfKeys.add("imageDescription")
            arrOfKeys.add("latitude")
            arrOfKeys.add("longitude")
            
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            var strCompanyKey = String()
            var strUserName = String()
            var strEmployeeid = String() // EmployeeId
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                
                strCompanyKey = "\(value)"
            }
            
            if let value = dictLoginData.value(forKeyPath: "Company.Username") {
                
                strUserName = "\(value)"
            }
            
            if let value = dictLoginData.value(forKey: "EmployeeId") {
                
                strEmployeeid = "\(value)"
            }
            
            let coordinate = Global().getLocation()
            let latitude = "\(coordinate.latitude)"
            let longitude = "\(coordinate.longitude)"
            
            let woImageId = "Mobile" + getUniqueValueForId()
            let modifiedDate =  Global().modifyDate()
            
            
            arrOfValues.add("") //createdBy
            arrOfValues.add("") //createdDate
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strImageName)
            arrOfValues.add(strHeaderTitle)
            arrOfValues.add(strEmployeeid)
            arrOfValues.add(woImageId) // woImageId
            arrOfValues.add(modifiedDate ?? "")
            arrOfValues.add(strWoId)
            arrOfValues.add("") // imageCaption
            arrOfValues.add("") // imageDescription
            arrOfValues.add(latitude) // latitude
            arrOfValues.add(longitude) //  longitude
            
            saveDataInDB(strEntity: "ImageDetailsServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
            let imageResized = Global().resizeImageGloballl(image)
            
            saveImageDocumentDirectory(strFileName: strImageName, image: imageResized!)
            
            
            // Save Image To Sales Auto Also for WDO New FLow
            
            let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
            
            let arrOfKeysSales = NSMutableArray()
            let arrOfValuesSales = NSMutableArray()
            
            arrOfKeysSales.add("createdBy")
            arrOfKeysSales.add("createdDate")
            arrOfKeysSales.add("companyKey")
            arrOfKeysSales.add("userName")
            arrOfKeysSales.add("leadImagePath")
            arrOfKeysSales.add("leadImageType")
            arrOfKeysSales.add("modifiedBy")
            arrOfKeysSales.add("leadImageId")
            arrOfKeysSales.add("modifiedDate")
            arrOfKeysSales.add("leadId")
            arrOfKeysSales.add("leadImageCaption")
            arrOfKeysSales.add("descriptionImageDetail")
            arrOfKeysSales.add("latitude")
            arrOfKeysSales.add("longitude")
            
            arrOfValuesSales.add("") //createdBy
            arrOfValuesSales.add("") //createdDate
            arrOfValuesSales.add(strCompanyKey)
            arrOfValuesSales.add(strUserName)
            arrOfValuesSales.add(strImageNameSales)
            
            if strHeaderTitle == "Graph"{
                
                arrOfValuesSales.add(strHeaderTitle)

            }else{

                arrOfValuesSales.add("Before")

            }
            
            arrOfValuesSales.add(strEmployeeid)
            arrOfValuesSales.add(woImageId) // woImageId
            arrOfValuesSales.add(modifiedDate ?? "")
            arrOfValuesSales.add(strWoLeadId)
            arrOfValuesSales.add("") // imageCaption
            arrOfValuesSales.add("") // imageDescription
            arrOfValuesSales.add(latitude) // latitude
            arrOfValuesSales.add(longitude) //  longitude
            
            let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strWoLeadId))
            
            var strStageNameLocal = ""
            var strStatusNameLocal = ""
            
            if(arrayLeadDetail.count > 0)
            {
                
                let objWdoLeadDetail = arrayLeadDetail.firstObject as! NSManagedObject
                strStageNameLocal = "\(objWdoLeadDetail.value(forKey: "stageSysName")!)"
                strStatusNameLocal = "\(objWdoLeadDetail.value(forKey: "statusSysName")!)"
                
            }
            
            if(strStatusNameLocal.lowercased() == "complete" && strStageNameLocal.lowercased() == "won")
            {
                
                // Not Add in Db As Lead complete
                
            }else{
                
                saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeysSales, arrayOfValue: arrOfValuesSales)
                
                saveImageDocumentDirectory(strFileName: strImageNameSales, image: imageResized!)
                
                Global().updateSalesModifydate(strWoLeadId as String)

            }
            
            
            goToCaptionDescriptions(strWoImageId: woImageId )
            
        }
        else if strModuleType == "Target"
        {
            strMobileTargetImageId =  "\(getUniqueValueForId())"
            
            strTargetImageDetailId = ""

            let strImageName = "Img" + "\(strWoLeadId)" + "\(strHeaderTitle)" + "\(getUniqueValueForId())" + ".jpg"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("leadImagePath")
            arrOfKeys.add("leadImageType")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("targetImageDetailId")//TargetImageDetailId  leadImageId
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("leadId")
            arrOfKeys.add("leadImageCaption")
            arrOfKeys.add("descriptionImageDetail")
            arrOfKeys.add("latitude")
            arrOfKeys.add("longitude")
            arrOfKeys.add("targetSysName")
            
            arrOfKeys.add("mobileTargetId")
            arrOfKeys.add("mobileTargetImageId")
            arrOfKeys.add("leadCommercialTargetId")
            //arrOfKeys.add("isInternalUsage")
            
            
            
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            var strCompanyKey = String()
            var strUserName = String()
            var strEmployeeid = String() // EmployeeId
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                
                strCompanyKey = "\(value)"
            }
            
            if let value = dictLoginData.value(forKeyPath: "Company.Username") {
                
                strUserName = "\(value)"
            }
            
            if let value = dictLoginData.value(forKey: "EmployeeId") {
                
                strEmployeeid = "\(value)"
            }
            
            let coordinate = Global().getLocation()
            let latitude = "\(coordinate.latitude)"
            let longitude = "\(coordinate.longitude)"
            
            let woImageId = "Mobile" + getUniqueValueForId()
            let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
            
            
            arrOfValues.add("") //createdBy
            arrOfValues.add("") //createdDate
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strImageName)
            arrOfValues.add(strHeaderTitle)
            arrOfValues.add(strEmployeeid)
            arrOfValues.add("") // targetImageDetailId
            arrOfValues.add(modifiedDate ?? "")
            arrOfValues.add(strWoId)
            arrOfValues.add("") // imageCaption
            arrOfValues.add("") // imageDescription
            arrOfValues.add(latitude) // latitude
            arrOfValues.add(longitude) //  longitude
            arrOfValues.add(strTargetId) //  longitude
            
            arrOfValues.add("\(dictTarget.value(forKey: "mobileTargetId") ?? "")")
            
            //arrOfValues.add("\(dictTarget.value(forKey: "mobileTargetImageId") ?? "")")
            arrOfValues.add("\(strMobileTargetImageId)")

            
            arrOfValues.add("\(dictTarget.value(forKey: "leadCommercialTargetId") ?? "")")
            //arrOfValues.add("false")//IsInternalUsage
            
            saveDataInDB(strEntity: "TargetImageDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            let imageResized = Global().resizeImageGloballl(image)
            
            saveImageDocumentDirectory(strFileName: strImageName, image: imageResized!)
            
            //let arrayOfImagesSales = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", self.strWoLeadId, self.strHeaderTitle))
            
            
            
            
            //Update Modify Date In Work Order DB
            //updateServicePestModifyDate(strWoId: self.strWoId as String)
            
            Global().updateSalesModifydate(strWoLeadId as String)
            
            goToCaptionDescriptions(strWoImageId: woImageId )
            
        }
        else if strModuleType == "SalesFlow"
        {
            
            let strImageName = "Img" + "\(strWoLeadId)" + "\(strHeaderTitle)" + "\(getUniqueValueForId())" + ".jpg"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("leadImagePath")
            arrOfKeys.add("leadImageType")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("leadImageId")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("leadId")
            arrOfKeys.add("leadImageCaption")
            arrOfKeys.add("descriptionImageDetail")
            arrOfKeys.add("latitude")
            arrOfKeys.add("longitude")
            arrOfKeys.add("isAddendum")
            arrOfKeys.add("isImageSyncforMobile")
            arrOfKeys.add("isInternalUsage")
            
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            var strCompanyKey = String()
            var strUserName = String()
            var strEmployeeid = String() // EmployeeId
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                
                strCompanyKey = "\(value)"
            }
            
            if let value = dictLoginData.value(forKeyPath: "Company.Username") {
                
                strUserName = "\(value)"
            }
            
            if let value = dictLoginData.value(forKey: "EmployeeId") {
                
                strEmployeeid = "\(value)"
            }
            
            let coordinate = Global().getLocation()
            let latitude = "\(coordinate.latitude)"
            let longitude = "\(coordinate.longitude)"
            
            let woImageId = "Mobile" + getUniqueValueForId()
            let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
            
            
            arrOfValues.add("") //createdBy
            arrOfValues.add("") //createdDate
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strImageName)
            arrOfValues.add(strHeaderTitle)
            arrOfValues.add(strEmployeeid)
            arrOfValues.add(woImageId) // woImageId
            arrOfValues.add(modifiedDate ?? "")
            arrOfValues.add(strWoId)
            arrOfValues.add("") // imageCaption
            arrOfValues.add("") // imageDescription
            arrOfValues.add(latitude) // latitude
            arrOfValues.add(longitude) //  longitude
            arrOfValues.add("false")
            arrOfValues.add("false")
            arrOfValues.add("false")//IsInternalUsage
            saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            let imageResized = Global().resizeImageGloballl(image)
            
            saveImageDocumentDirectory(strFileName: strImageName, image: imageResized!)
            
            //Update Modify Date In Work Order DB
            //updateServicePestModifyDate(strWoId: self.strWoId as String)
            
            Global().updateSalesModifydate(strWoLeadId as String)
            
            goToCaptionDescriptions(strWoImageId: woImageId )
            
        }
        
        
    }
    
    @objc func actionOnDownloadImage(sender: UIButton!)
    {
        
        let image: UIImage = UIImage(named: "NoImage.jpg")!
        var imageView = UIImageView(image: image)
        
        if strModuleType == "ServicePestFlow" {
            
            let objTemp = arrayOfImages[sender.tag] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "woImagePath")
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                
                
            }
            
            
        }else if strModuleType as String == flowTypeWdoSalesService {
            
            let objTemp = arrayOfImages[sender.tag] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "woImagePath")
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                
                
            }
            
            
        }
        else if strModuleType as String == "Target" {
            
            let objTemp = arrayOfImages[sender.tag] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "leadImagePath")
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                
                
            }
            
            
        }
        else if strModuleType as String == "SalesFlow" {
            
            let objTemp = arrayOfImages[sender.tag] as! NSManagedObject
            
            if objTemp.isKind(of: ImageDetail.self) //Image Detail
            {
                let strImagePath = objTemp.value(forKey: "leadImagePath")
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
                
                if image != nil  {
                    
                    imageView = UIImageView(image: image)
                    
                }else {
                    
                    
                    
                }
            }
            else //CRM Image Detail
            {
                let strImagePath = objTemp.value(forKey: "path")
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
                
                if image != nil  {
                    
                    imageView = UIImageView(image: image)
                    
                }else {
                    
                    
                    
                }
            }
            

        }
        else{
            
            let objTemp = arrayOfImages[sender.tag] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "leadImagePath")
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                
                
            }
            
        }
        let imgToShare = imageView.image
        
        if DeviceType.IS_IPAD {
            
            let activityVC = UIActivityViewController(activityItems: [imgToShare!], applicationActivities: nil)
            //present(activityVC, animated: true, completion: nil)
            if let popOver = activityVC.popoverPresentationController {
              popOver.sourceView = sender as UIView
              //popOver.sourceRect = sender.bounds
              //popOver.permittedArrowDirections = UIPopoverArrowDirection.down
            }
            present(activityVC, animated: true)

            //Completion handler
            activityVC.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed:
            Bool, arrayReturnedItems: [Any]?, error: Error?) in
                if completed {
                    print("share completed")
                    //return
                } else {
                    print("cancel")
                }
                if let shareError = error {
                    print("error while sharing: \(shareError.localizedDescription)")
                }
            }
            
            
        } else {
           
            if (imgToShare != nil) {
                let vc = UIActivityViewController(activityItems: [imgToShare!], applicationActivities: [])
                present(vc, animated: true)
            }
            
        }
        
    }
    
    @objc func actionOnAddInAddendum(sender: UIButton!)
    {
        print("Button clicked")
        
        var isImageDetailObject = false
        
        let objTemp = arrayOfImages[sender.tag] as! NSManagedObject
        
        if objTemp.isKind(of: ImageDetail.self) //Image Detail
        {
            isImageDetailObject = true
        }
        else
        {
            isImageDetailObject = false
        }
        
        
        let dict = getMutableDictionaryFromNSManagedObject(obj: arrayOfImages.object(at: sender.tag) as! NSManagedObject)
        
        
        print("\(dict)")
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String()
        var strIsAddendum = String()
        var strImageName = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey")
        {
            
            strCompanyKey = "\(value)"
        }
        else
        {
            strCompanyKey = ""
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username")
        {
            
            strUserName = "\(value)"
        }
        else
        {
            strUserName = ""
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId")
        {
            
            strEmployeeid = "\(value)"
            
        }
        else
        {
            strEmployeeid = ""
        }
        
        // Update Code for Addendum Image
        
        strIsAddendum = "\(dict.value(forKey: "isAddendum") ?? "")"
        
        if isImageDetailObject
        {
            strImageName = "\(dict.value(forKey: "leadImagePath") ?? "")"
        }
        else
        {
            strImageName = "\(dict.value(forKey: "path") ?? "")"
        }
        
       /* if strIsAddendum == "true"
        {
            strIsAddendum = "false"
        }
        else
        {
            strIsAddendum = "true"
        }*/
        
        if sender.currentImage == UIImage(named: "check_box_2New.png")
        {
            sender.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
            strIsAddendum = "false"
        }
        else
        {
            sender.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            strIsAddendum = "true"
        }
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        if isImageDetailObject
        {
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("isAddendum")
            
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strEmployeeid)
            arrOfValues.add(Global().modifyDate())
            arrOfValues.add(strIsAddendum)
        }
        else
        {
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("isAddendum")
            
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strIsAddendum)
        }

        
      
        if isImageDetailObject
        {
            let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadImagePath == %@", strWoId, strHeaderTitle,strImageName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                tblViewImages.reloadData()
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        else
        {
            let isSuccess =  getDataFromDbToUpdate(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && path == %@", strWoId, strUserName,strImageName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            if isSuccess
            {
                tblViewImages.reloadData()
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }

       
    }
    
    @objc func actionOnAddToInternal(sender: UIButton!)
    {
        print("Button clicked")
        
        var isImageDetailObject = false
        
        let objTemp = arrayOfImages[sender.tag] as! NSManagedObject
        
        if objTemp.isKind(of: ImageDetail.self) //Image Detail
        {
            isImageDetailObject = true
        }
        else
        {
            isImageDetailObject = false
        }
        
        
        let dict = getMutableDictionaryFromNSManagedObject(obj: arrayOfImages.object(at: sender.tag) as! NSManagedObject)
         print("\(dict)")
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String()
        var strIsAddToInternal = String()
        var strImageName = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey")
        {
            
            strCompanyKey = "\(value)"
        }
        else
        {
            strCompanyKey = ""
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username")
        {
            
            strUserName = "\(value)"
        }
        else
        {
            strUserName = ""
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId")
        {
            
            strEmployeeid = "\(value)"
            
        }
        else
        {
            strEmployeeid = ""
        }
        
        // Update Code for Addendum Image
        
        strIsAddToInternal = "\(dict.value(forKey: "isInternalUsage") ?? "")"
        
        
        strImageName = isImageDetailObject ? "\(dict.value(forKey: "leadImagePath") ?? "")" :  "\(dict.value(forKey: "path") ?? "")"
        
        if sender.currentImage == UIImage(named: "check_box_2New.png")
        {
            sender.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
            strIsAddToInternal = "false"
        }
        else
        {
            sender.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            strIsAddToInternal = "true"
        }
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        if isImageDetailObject
        {
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("isInternalUsage")
            
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strEmployeeid)
            arrOfValues.add(Global().modifyDate())
            arrOfValues.add(strIsAddToInternal)
        }
        else
        {
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("isInternalUsage")
            
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strIsAddToInternal)
        }

        
        if isImageDetailObject
        {
            let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadImagePath == %@", strWoId, strHeaderTitle,strImageName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                tblViewImages.reloadData()
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        else
        {
            let isSuccess =  getDataFromDbToUpdate(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && path == %@", strWoId, strUserName,strImageName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            if isSuccess
            {
                tblViewImages.reloadData()
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }

       

    }
    
    func goToNewGraphNew()
    {
        let showItemStoryboard = UIStoryboard(name: "SalesGraphiPad", bundle: nil)
        let showItemNavController = showItemStoryboard.instantiateViewController(withIdentifier: "NewGraphSalesVC") as! NewGraphSalesVC
        showItemNavController.strLeadId = strWoId
        present(showItemNavController, animated: true, completion: nil)
    }
    func goToNewGraph()
    {
        let storyboardIpad = UIStoryboard.init(name: "SalesGraphiPad", bundle: nil)
        let vc: NewGraphSalesVC = storyboardIpad.instantiateViewController(withIdentifier: "NewGraphSalesVC") as! NewGraphSalesVC
        
        //vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
       // vc.modalTransitionStyle = .coverVertical
        
        vc.strLeadId = strWoId
        present(vc, animated: true, completion: nil)

        //vc.delegate = self
        
        
        //self.navigationController?.pushViewController(vc, animated: false
       // )
        //present(vc, animated: false, completion: {})
    }
    // MARK: - -----------------------------------UIDocumentPickerViewController Delegate Method-----------------------------------

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
           guard let myURL = urls.first else {
                      return
                  }
          
                  print("import result : \(myURL)")
          
                  do {
          
                      let data = try Data(contentsOf: myURL)
                      let uiImage: UIImage = UIImage(data: data)!
          
                      self.SaveImageToDb(image: uiImage)
          
          
                  } catch _ as NSError  {
          
                  }catch {
                  }
      }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
         print("view was cancelled")
         //dismiss(animated: true, completion: nil)
     }
    
    func documentMenu(_ documentMenu: UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
    }
    
    
    /*public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        
        print("import result : \(myURL)")
        
        do {
            
            let data = try Data(contentsOf: myURL)
            let uiImage: UIImage = UIImage(data: data)!
            
            self.SaveImageToDb(image: uiImage)
            
            
        } catch _ as NSError  {
            
        }catch {
        }
        
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
     
     func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        //dismiss(animated: true, completion: nil)
    }*/
    

}


// MARK: - -------------------------------------------UITableViewDelegate-------------------------------------------

extension GlobalImageVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return arrayOfImages.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GlobalImageVC", for: indexPath as IndexPath) as! tblCell
        tblViewImages.separatorColor = UIColor.theme()
        // imageCaption  imageDescription
        
        if strModuleType == "ServicePestFlow" {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            
            cell.lbl_GlobalImageViewCaption.text = (objTemp.value(forKey: "imageCaption") as! String).count>0 ? "Caption :" + (objTemp.value(forKey: "imageCaption") as! String) : "Caption :"
            cell.lbl_GlobalImageViewDescriptions.text = (objTemp.value(forKey: "imageDescription") as! String).count>0 ? "Description :" + (objTemp.value(forKey: "imageDescription") as! String) : "Description :"

            let strImagePath = objTemp.value(forKey: "woImagePath") as! String
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                cell.imgView_GlobalImageVC.image = image
                cell.activityIndicatorImage.isHidden = true
                
            }else {
                
                let defsLogindDetail = UserDefaults.standard

                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                    
                    strURL = "\(value)"
                    
                }
                
                strURL = strURL + "\(strImagePath)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)

                // https://tsrs.pestream.com////Documents//UploadImages////Before_20190807021931051_Banner.png
                cell.activityIndicatorImage.isHidden = false
                cell.activityIndicatorImage.startAnimating()

                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgView_GlobalImageVC.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                            
                            cell.imgView_GlobalImageVC.image = image
                            cell.activityIndicatorImage.isHidden = true
                            cell.activityIndicatorImage.stopAnimating()
                            
                        }}
                    
                    }

            }
            
            
        }
        if strModuleType == "WdoFlow"
        {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            
            cell.lbl_GlobalImageViewCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String).count>0 ? "Caption :" + (objTemp.value(forKey: "leadImageCaption") as! String) : "Caption :"
            cell.lbl_GlobalImageViewDescriptions.text = (objTemp.value(forKey: "descriptionImageDetail") as! String).count>0 ? "Description :" + (objTemp.value(forKey: "descriptionImageDetail") as! String) : "Description :"
            
            let strImagePath = objTemp.value(forKey: "leadImagePath") as! String
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                cell.imgView_GlobalImageVC.image = image
                cell.activityIndicatorImage.isHidden = true
                
            }else {
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") {
                    
                    strURL = "\(value)"
                    
                }
                
                strURL = strURL + "\(strImagePath)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                cell.activityIndicatorImage.isHidden = false
                cell.activityIndicatorImage.startAnimating()
                
                // https://tsrs.pestream.com////Documents//UploadImages////Before_20190807021931051_Banner.png
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgView_GlobalImageVC.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                            
                            cell.imgView_GlobalImageVC.image = image
                            cell.activityIndicatorImage.isHidden = true
                            cell.activityIndicatorImage.stopAnimating()
                            
                        }}
                    
                }
            }
            
            cell.lbl_GlobalImageViewCaption.isEnabled = true
            cell.lbl_GlobalImageViewDescriptions.isEnabled = true
            
        }
        
        if strModuleType as String == flowTypeWdoSalesService {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            
            cell.lbl_GlobalImageViewCaption.text = (objTemp.value(forKey: "imageCaption") as! String).count>0 ? "Caption :" + (objTemp.value(forKey: "imageCaption") as! String) : "Caption :"
            cell.lbl_GlobalImageViewDescriptions.text = (objTemp.value(forKey: "imageDescription") as! String).count>0 ? "Description :" + (objTemp.value(forKey: "imageDescription") as! String) : "Description :"
            
            let strImagePath = objTemp.value(forKey: "woImagePath") as! String
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                cell.imgView_GlobalImageVC.image = image
                cell.activityIndicatorImage.isHidden = true
                
            }else {
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                    
                    strURL = "\(value)"
                    
                }
                
                strURL = strURL + "\(strImagePath)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                cell.activityIndicatorImage.isHidden = false
                cell.activityIndicatorImage.startAnimating()

                // https://tsrs.pestream.com////Documents//UploadImages////Before_20190807021931051_Banner.png
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgView_GlobalImageVC.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                            
                            cell.imgView_GlobalImageVC.image = image
                            cell.activityIndicatorImage.isHidden = true
                            cell.activityIndicatorImage.stopAnimating()
                            
                        }}
                    
                }
                
            }
            
            
        }
        
        if strModuleType == "Target"
        {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            

            cell.lbl_GlobalImageViewCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String).count>0 ? "Caption :" + (objTemp.value(forKey: "leadImageCaption") as! String) : "Caption :"
            cell.lbl_GlobalImageViewDescriptions.text = (objTemp.value(forKey: "descriptionImageDetail") as! String).count>0 ? "Description :" + (objTemp.value(forKey: "descriptionImageDetail") as! String) : "Description :"
            
            let strImagePath = objTemp.value(forKey: "leadImagePath") as! String
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                cell.imgView_GlobalImageVC.image = image
                cell.activityIndicatorImage.isHidden = true
                
            }else {
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")
                {
                    
                    strURL = "\(value)"
                    
                }
                //strURL = strURL + "\(strImagePath)"
                
                strURL = strURL + "Documents/Targetimages/" + "\(strImagePath)"

                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                cell.activityIndicatorImage.isHidden = false
                cell.activityIndicatorImage.startAnimating()
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgView_GlobalImageVC.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                            
                            cell.imgView_GlobalImageVC.image = image
                            cell.activityIndicatorImage.isHidden = true
                            cell.activityIndicatorImage.stopAnimating()
                            
                        }}
                    
                }
            }
        }
        if strModuleType == "SalesFlow"
        {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            if objTemp .isKind(of: ImageDetail.self) // Image Detail
            {
                
                cell.lbl_GlobalImageViewCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String).count>0 ? "Caption :" + (objTemp.value(forKey: "leadImageCaption") as! String) : "Caption :"
                cell.lbl_GlobalImageViewDescriptions.text = (objTemp.value(forKey: "descriptionImageDetail") as! String).count>0 ? "Description :" + (objTemp.value(forKey: "descriptionImageDetail") as! String) : "Description :"
                
                cell.btnAddendum.tag = indexPath.row
                
                if (("\(objTemp.value(forKey: "isAddendum") ?? "")".caseInsensitiveCompare("true") == .orderedSame) || ("\(objTemp.value(forKey: "isAddendum") ?? "")" == "1"))
                {
                    cell.btnAddendum.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
                }
                else
                {
                    cell.btnAddendum.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
                    
                }
                cell.btnAddendum.addTarget(self, action: #selector(actionOnAddInAddendum), for: .touchUpInside)
                
                //For Add To Internal Start
                
                cell.btnAddToInternal.tag = indexPath.row
                
                if (("\(objTemp.value(forKey: "isInternalUsage") ?? "")".caseInsensitiveCompare("true") == .orderedSame) || ("\(objTemp.value(forKey: "isInternalUsage") ?? "")" == "1"))
                {
                    cell.btnAddToInternal.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
                }
                else
                {
                    cell.btnAddToInternal.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
                    
                }
                cell.btnAddToInternal.addTarget(self, action: #selector(actionOnAddToInternal), for: .touchUpInside)
                
                //End
                
                let strImagePath = objTemp.value(forKey: "leadImagePath") as! String
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
                
                if isImageExists!  {
                    
                    cell.imgView_GlobalImageVC.image = image
                    cell.activityIndicatorImage.isHidden = true
                    
                }else {
                    
                    let defsLogindDetail = UserDefaults.standard
                    
                    let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                    
                    var strURL = String()
                    
                    //if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")
                    if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")
                    {
                        
                        strURL = "\(value)"
                        
                    }
                    
                    if strImagePath.contains("Documents")
                    {
                        strURL = strURL + "\(strImagePath)"
                    }
                    else
                    {
                         strURL = strURL + "/Documents/" + "\(strImagePath)"
                    }
                    
                    strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                    
                    cell.activityIndicatorImage.isHidden = false
                    cell.activityIndicatorImage.startAnimating()
                    // https://tsrs.pestream.com////Documents//UploadImages////Before_20190807021931051_Banner.png
                    
                    let image: UIImage = UIImage(named: "NoImage.jpg")!
                    
                    cell.imgView_GlobalImageVC.image = image
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string:strURL)
                        let data = try? Data(contentsOf: url!)
                        
                        if data != nil && (data?.count)! > 0 {
                            
                            let image: UIImage = UIImage(data: data!)!
                            
                            DispatchQueue.main.async {
                                
                                saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                                
                                cell.imgView_GlobalImageVC.image = image
                                cell.activityIndicatorImage.isHidden = true
                                cell.activityIndicatorImage.stopAnimating()
                                
                            }}
                        
                    }
                }
                
            }
            else
            {
                
                
                let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
                
                cell.lbl_GlobalImageViewCaption.text = (objTemp.value(forKey: "cRMImageCaption") as! String).count>0 ? "Caption :" + (objTemp.value(forKey: "cRMImageCaption") as! String) : "Caption :"
                cell.lbl_GlobalImageViewDescriptions.text = (objTemp.value(forKey: "cRMImageDescription") as! String).count>0 ? "Description :" + (objTemp.value(forKey: "cRMImageDescription") as! String) : "Description :"
                
                cell.btnAddendum.tag = indexPath.row
                
                if (("\(objTemp.value(forKey: "isAddendum") ?? "")".caseInsensitiveCompare("true") == .orderedSame) || ("\(objTemp.value(forKey: "isAddendum") ?? "")" == "1"))
                {
                    cell.btnAddendum.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
                }
                else
                {
                    cell.btnAddendum.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
                    
                }
                cell.btnAddendum.addTarget(self, action: #selector(actionOnAddInAddendum), for: .touchUpInside)
                
                //For Add To Internal Start
                
                cell.btnAddToInternal.tag = indexPath.row
                
                if (("\(objTemp.value(forKey: "isInternalUsage") ?? "")".caseInsensitiveCompare("true") == .orderedSame) || ("\(objTemp.value(forKey: "isInternalUsage") ?? "")" == "1"))
                {
                    cell.btnAddToInternal.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
                }
                else
                {
                    cell.btnAddToInternal.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
                    
                }
                cell.btnAddToInternal.addTarget(self, action: #selector(actionOnAddToInternal), for: .touchUpInside)
                
                //End
                
               
                let strImagePath = objTemp.value(forKey: "path") as! String
                //var strImagePath = objTemp.value(forKey: "path") as! String
                //strImagePath = strImagePath.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                //strImagePath = strImagePath.replacingOccurrences(of: "/", with: "\\", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
                
                if isImageExists!  {
                    
                    cell.imgView_GlobalImageVC.image = image
                    cell.activityIndicatorImage.isHidden = true
                    
                }else {
                    
                    let defsLogindDetail = UserDefaults.standard
                    
                    let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                    
                    var strURL = String()
                    
                    //if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")
                    if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")
                    {
                        
                        strURL = "\(value)"
                        
                    }
                    
//                    if strImagePath.contains("Documents")
//                    {
//                        strURL = strURL + "\(strImagePath)"
//                    }
//                    else
//                    {
//                         strURL = strURL + "/Documents/" + "\(strImagePath)"
//                    }
                    
                    strURL = strURL + "/Documents/" + "\(strImagePath)"
                    strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                    
                    cell.activityIndicatorImage.isHidden = false
                    cell.activityIndicatorImage.startAnimating()
                    // https://tsrs.pestream.com////Documents//UploadImages////Before_20190807021931051_Banner.png
                    
                    let image: UIImage = UIImage(named: "NoImage.jpg")!
                    
                    cell.imgView_GlobalImageVC.image = image
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string:strURL)
                        let data = try? Data(contentsOf: url!)
                        
                        if data != nil && (data?.count)! > 0 {
                            
                            let image: UIImage = UIImage(data: data!)!
                            
                            DispatchQueue.main.async {
                                
                                saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                                
                                cell.imgView_GlobalImageVC.image = image
                                cell.activityIndicatorImage.isHidden = true
                                cell.activityIndicatorImage.stopAnimating()
                                
                            }}
                        
                    }
                }
            }
            
        }
        if strModuleType == "SalesFlow"
        {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            if objTemp .isKind(of: ImageDetail.self) // Image Detail
            {
                cell.btnAddendum.isHidden = false
                cell.lblIncludeInAddendum.isHidden = false
                
                cell.btnAddToInternal.isHidden = false
                cell.lblAddToInternal.isHidden = false
                
                if ((strWoStatus == "Completed") || (strWoStatus == "Complete"))
                {
                    cell.btnAddendum.isEnabled = false
                    cell.btnAddToInternal.isEnabled = false
                }
            }
            else
            {
//                cell.btnAddendum.isHidden = true
//                cell.lblIncludeInAddendum.isHidden = true
//
//                cell.btnAddToInternal.isHidden = true
//                cell.lblAddToInternal.isHidden = true
                
                
                cell.btnAddendum.isHidden = false
                cell.lblIncludeInAddendum.isHidden = false
                
                cell.btnAddToInternal.isHidden = false
                cell.lblAddToInternal.isHidden = false
                
                if ((strWoStatus == "Completed") || (strWoStatus == "Complete"))
                {
                    cell.btnAddendum.isEnabled = false
                    cell.btnAddToInternal.isEnabled = false
                }
            }

        }
        else
        {
            cell.btnAddendum.isHidden = true
            cell.lblIncludeInAddendum.isHidden = true
            
            cell.btnAddToInternal.isHidden = true
            cell.lblAddToInternal.isHidden = true
        }
        
        cell.btnDownload.tag = indexPath.row
        cell.btnDownload.addTarget(self, action: #selector(actionOnDownloadImage), for: .touchUpInside)
        //actionOnDownloadImage
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if strModuleType as String == flowTypeWdoSalesService
        {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            let strIsProblemGraph = "\(objTemp.value(forKey: "isProblemIdentifaction") ?? "")"
            
            if strIsProblemGraph == "true" || strIsProblemGraph == "1" || strIsProblemGraph == "True"{
                
                return false
                
            }else{
                
                return true

            }
            
        }else{
            
            return true

        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        let objTemp = self.arrayOfImages[indexPath.row] as! NSManagedObject
        
        if self.strModuleType == "SalesFlow"
        {
            if objTemp.isKind(of: ImageDetail.self) //Image Detail
            {
                 strCRMImageId = ""
            }
            else
            {
                strCRMImageId = "\(objTemp.value(forKey: "leadMediaId") ?? "")"
            }
        }

        //if self.strWoStatus == "Completed"
        if ((self.strWoStatus == "Completed") || (self.strWoStatus == "Complete"))
        {
            
            let edit = UITableViewRowAction(style: .normal, title: "View") { (action, indexPath) in
                
                var strWoImageId = ""
                if self.strModuleType == "ServicePestFlow"
                {
                    strWoImageId = objTemp.value(forKey: "woImageId") as! String
                }
                else if self.strModuleType as String == flowTypeWdoSalesService
                {
                    strWoImageId = objTemp.value(forKey: "woImageId") as! String
                }
                else if self.strModuleType as String == "Target"
                {
                    self.strTargetImageDetailId = objTemp.value(forKey: "targetImageDetailId") as! String
                    self.strMobileTargetImageId = objTemp.value(forKey: "mobileTargetImageId") as! String
                    
                    strWoImageId = objTemp.value(forKey: "targetImageDetailId") as! String
                }
                else if self.strModuleType as String == "SalesFlow"
                {
                    if objTemp.isKind(of: ImageDetail.self) //Image Detail
                    {
                        strWoImageId = objTemp.value(forKey: "leadImageId") as! String
                    }
                    else //CRM mage Detail
                    {
                        strWoImageId = objTemp.value(forKey: "leadMediaId") as! String
                    }
                }
                else
                {
                    strWoImageId = objTemp.value(forKey: "leadImageId") as! String
                }
                
                self.goToCaptionDescriptions(strWoImageId: strWoImageId )
                
            }
            
            edit.backgroundColor = UIColor.lightGray
            
            return [edit]
            
        }
        else
        {
            
            let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
                // delete item at indexPath
                
                let alert = UIAlertController(title: AlertDelete, message: AlertDeleteMsg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                    
                    deleteDataFromDB(obj: objTemp)
                    
                    if self.strModuleType as String == flowTypeWdoSalesService
                    {

                        let arrayOfImagesSales = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", self.strWoLeadId, self.strHeaderTitle))

                        let objTempSales = arrayOfImagesSales[indexPath.row] as! NSManagedObject

                        deleteDataFromDB(obj: objTempSales)
                        
                    }
                    
                    self.fetchImageFromDB()
                    
                    if self.strModuleType as String == "Target" || self.strModuleType as String == "SalesFlow" || self.strModuleType as String == flowTypeWdoSalesService
                    {
                        Global().updateSalesModifydate(self.strWoLeadId as String)
                    }
                                        
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    
                    
                    
                }))
                
                alert.popoverPresentationController?.sourceView = self.view
                
                self.present(alert, animated: true, completion: {
                })
                
            }
            
            if DeviceType.IS_IPAD
            {
                let edit = UITableViewRowAction(style: .destructive, title: "Edit") { (action, indexPath) in // default
                               // Edit item at indexPath
                               
                               let alert = UIAlertController(title: "", message: "Please Select an Option to edit", preferredStyle: .alert)//actionSheet //alert
                               
                               let strTitle = "Edit " + "\(self.strHeaderTitle) Image"
                               
                               alert.addAction(UIAlertAction(title: strTitle, style: .default , handler:{ (UIAlertAction)in //default  //destructive
                                   
                                   if self.strModuleType == "ServicePestFlow"
                                   {
                                       let strWoImagePath = objTemp.value(forKey: "woImagePath")
                                       nsud.set(strWoImagePath, forKey: "editImagePath")
                                       nsud.synchronize()
                                   }
                                   else if self.strModuleType == "WdoFlow"
                                   {
                                       let strWoImagePath = objTemp.value(forKey: "leadImagePath")
                                       nsud.set(strWoImagePath, forKey: "editImagePath")
                                       nsud.synchronize()
                                   }
                                   else if self.strModuleType as String == flowTypeWdoSalesService
                                   {
                                       let strWoImagePath = objTemp.value(forKey: "woImagePath")
                                       nsud.set(strWoImagePath, forKey: "editImagePath")
                                       nsud.synchronize()
                                   }
                                   else if self.strModuleType as String == "Target"
                                   {
                                       let strWoImagePath = objTemp.value(forKey: "leadImagePath")
                                       nsud.set(strWoImagePath, forKey: "editImagePath")
                                       nsud.synchronize()
                                   }
                                   else if self.strModuleType as String == "SalesFlow"
                                   {
                                       if objTemp.isKind(of: ImageDetail.self) //Image Detail
                                       {
                                           let strWoImagePath = objTemp.value(forKey: "leadImagePath")
                                           nsud.set(strWoImagePath, forKey: "editImagePath")
                                           nsud.synchronize()
                                       }
                                       else
                                       {
                                           let strWoImagePath = objTemp.value(forKey: "path")
                                           nsud.set(strWoImagePath, forKey: "editImagePath")
                                           nsud.synchronize()
                                       }

                                       print("Edit graph 1")

                                   }
                                   
                                   if DeviceType.IS_IPAD
                                   {
                                       // MechanicaliPad
                                       if objTemp.isKind(of: ImageDetail.self) //Image Detail
                                       {
                                           if "\(objTemp.value(forKey: "isNewGraph") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                                           {
                                               //nsud.set(true, forKey: "graphDebug")
                                               nsud.synchronize()
                                               self.goToNewGraph()
                                           }
                                           else
                                           {
                                               let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
                                               let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController
                                               testController?.strModuleType = self.strModuleType as String
                                               testController?.strWdoLeadId = self.strWoLeadId as String
                                               testController?.modalPresentationStyle = .fullScreen
                                               self.present(testController!, animated: false, completion: nil)
                                           }
                                       }
                                       else
                                       {
                                           
                                           let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
                                           let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController
                                           testController?.strModuleType = self.strModuleType as String
                                           testController?.strWdoLeadId = self.strWoLeadId as String
                                           testController?.modalPresentationStyle = .fullScreen
                                           self.present(testController!, animated: false, completion: nil)
                                       }

                                       
                                   }
                                   else
                                   {
                                       
                                       let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
                                       let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController
                                       testController?.strModuleType = self.strModuleType as String
                                       testController?.strWdoLeadId = self.strWoLeadId as String
                                               testController?.modalPresentationStyle = .fullScreen
                                       self.present(testController!, animated: false, completion: nil)
                                       
                                   }
                                   
                               }))
                               
                               
                               alert.addAction(UIAlertAction(title: "Edit Caption/Description", style: .default , handler:{ (UIAlertAction)in
                                   
                                   var strWoImageId = ""
                                   if self.strModuleType == "ServicePestFlow"
                                   {
                                       strWoImageId = objTemp.value(forKey: "woImageId") as! String
                                   }
                                   else if self.strModuleType == "WdoFlow"
                                   {
                                       strWoImageId = objTemp.value(forKey: "leadImageId") as! String
                                   }
                                   else if self.strModuleType as String == flowTypeWdoSalesService
                                   {
                                       strWoImageId = objTemp.value(forKey: "woImageId") as! String
                                   }
                                   else if self.strModuleType as String == "Target"
                                   {
                                       strWoImageId = objTemp.value(forKey: "targetImageDetailId") as! String
                                       
                                       self.strTargetImageDetailId = objTemp.value(forKey: "targetImageDetailId") as! String
                                       self.strMobileTargetImageId = objTemp.value(forKey: "mobileTargetImageId") as! String
                                   }
                                   else if self.strModuleType == "SalesFlow"
                                   {
                                       if objTemp.isKind(of: ImageDetail.self) //Image Detail
                                       {
                                           strWoImageId = objTemp.value(forKey: "leadImageId") as! String
                                       }
                                       else //CRM Image Detail
                                       {
                                           strWoImageId = objTemp.value(forKey: "leadMediaId") as! String
                                       }
                                       print("Edit graph 2")
                                   }
                                   self.goToCaptionDescriptions(strWoImageId: strWoImageId)
                                   
                               }))
                               
                               
                               alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                               }))
                               //alert.popoverPresentationController?.sourceRect = sender.frame
                               alert.popoverPresentationController?.sourceView = self.view
                               
                               self.present(alert, animated: true, completion: {
                                   
                                   
                                   
                               })
                               
                           }
                           
                           edit.backgroundColor = UIColor.lightGray
                
                if self.strModuleType == "SalesFlow"
                {
                    if objTemp.isKind(of: ImageDetail.self) //Image Detail
                    {
                        return [delete, edit]
                    }
                    else //CRM Image Detail
                    {
                        return [delete, edit]
                    }
                    
                }
                else
                {
                    return [delete, edit]
                }
                 
            }
            else // iPhone
            {
                let edit = UITableViewRowAction(style: .destructive, title: "Edit") { (action, indexPath) in
                    // Edit item at indexPath
                    
                    let alert = UIAlertController(title: "", message: "Please Select an Option to edit", preferredStyle: .actionSheet)
                    
                    let strTitle = "Edit " + "\(self.strHeaderTitle) Image"
                    
                    alert.addAction(UIAlertAction(title: strTitle, style: .default , handler:{ (UIAlertAction)in
                        
                        if self.strModuleType == "ServicePestFlow"
                        {
                            let strWoImagePath = objTemp.value(forKey: "woImagePath")
                            nsud.set(strWoImagePath, forKey: "editImagePath")
                            nsud.synchronize()
                        }
                        else if self.strModuleType == "WdoFlow"
                        {
                            let strWoImagePath = objTemp.value(forKey: "leadImagePath")
                            nsud.set(strWoImagePath, forKey: "editImagePath")
                            nsud.synchronize()
                        }
                        else if self.strModuleType as String == flowTypeWdoSalesService
                        {
                            let strWoImagePath = objTemp.value(forKey: "woImagePath")
                            nsud.set(strWoImagePath, forKey: "editImagePath")
                            nsud.synchronize()
                        }
                        else if self.strModuleType as String == "Target"
                        {
                            let strWoImagePath = objTemp.value(forKey: "leadImagePath")
                            nsud.set(strWoImagePath, forKey: "editImagePath")
                            nsud.synchronize()
                        }
                        else if self.strModuleType as String == "SalesFlow"
                        {
                            if objTemp.isKind(of: ImageDetail.self) //Image Detail
                            {
                                let strWoImagePath = objTemp.value(forKey: "leadImagePath")
                                nsud.set(strWoImagePath, forKey: "editImagePath")
                                nsud.synchronize()
                            }
                            else //CRM Image Detail
                            {
                                let strWoImagePath = objTemp.value(forKey: "path")
                                nsud.set(strWoImagePath, forKey: "editImagePath")
                                nsud.synchronize()
                            }
                        }
                        
                        if self.strModuleType == "SalesFlow"
                        {
                            if "\(objTemp.value(forKey: "isNewGraph") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                            {
                                //nsud.set(true, forKey: "graphDebug")
                                nsud.synchronize()
                                self.goToNewGraph()
                            }
                            else
                            {
                                let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
                                let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController
                                testController?.strModuleType = self.strModuleType as String
                                testController?.strWdoLeadId = self.strWoLeadId as String
                                testController?.modalPresentationStyle = .fullScreen
                                self.present(testController!, animated: false, completion: nil)
                            }
                        }
                        else
                        {
                            if DeviceType.IS_IPAD {
                                // MechanicaliPad
                                
                                let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
                                let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController
                                testController?.strModuleType = self.strModuleType as String
                                testController?.strWdoLeadId = self.strWoLeadId as String
                                testController?.modalPresentationStyle = .fullScreen
                                self.present(testController!, animated: false, completion: nil)
                                
                            }else{
                                
                                let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
                                let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController
                                testController?.strModuleType = self.strModuleType as String
                                testController?.strWdoLeadId = self.strWoLeadId as String
                                testController?.modalPresentationStyle = .fullScreen
                                self.present(testController!, animated: false, completion: nil)
                                
                            }
                        }

                    }))
                    
                    
                    alert.addAction(UIAlertAction(title: "Edit Caption/Description", style: .default , handler:{ (UIAlertAction)in
                        
                        var strWoImageId = ""
                        if self.strModuleType == "ServicePestFlow"
                        {
                            strWoImageId = objTemp.value(forKey: "woImageId") as! String
                        }
                        else if self.strModuleType == "WdoFlow"
                        {
                            strWoImageId = objTemp.value(forKey: "leadImageId") as! String
                        }
                        else if self.strModuleType as String == flowTypeWdoSalesService
                        {
                            strWoImageId = objTemp.value(forKey: "woImageId") as! String
                        }
                        else if self.strModuleType as String == "Target"
                        {
                            strWoImageId = objTemp.value(forKey: "targetImageDetailId") as! String
                            
                            self.strTargetImageDetailId = objTemp.value(forKey: "targetImageDetailId") as! String
                            self.strMobileTargetImageId = objTemp.value(forKey: "mobileTargetImageId") as! String
                        }
                        else if self.strModuleType == "SalesFlow"
                        {
                            if objTemp.isKind(of: ImageDetail.self) //Image Detail
                            {
                                strWoImageId = objTemp.value(forKey: "leadImageId") as! String
                            }
                            else //CRM Image Detail
                            {
                                strWoImageId = objTemp.value(forKey: "leadMediaId") as! String
                            }
                        }
                        
                        if self.strModuleType as String == "Target" || self.strModuleType as String == "SalesFlow" || self.strModuleType as String == flowTypeWdoSalesService
                        {
                            //Global().updateSalesModifydate(self.strWoLeadId as String)
                        }
                        
                        self.goToCaptionDescriptions(strWoImageId: strWoImageId)
                        
                    }))
                    
                    
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                    }))
                    //alert.popoverPresentationController?.sourceRect = sender.frame
                    alert.popoverPresentationController?.sourceView = self.view
                    
                    self.present(alert, animated: true, completion: {
                        
                    })
                    
                }
                
                edit.backgroundColor = UIColor.lightGray
                
                if self.strModuleType == "SalesFlow"
                {
                    if objTemp.isKind(of: ImageDetail.self) //Image Detail
                    {
                        return [delete, edit]
                    }
                    else //CRM Image Detail
                    {
                        return [delete, edit]
                    }
                    
                }
                else
                {
                    return [delete, edit]
                }
                
            }
            
        
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if strModuleType == "SalesFlow"
        {
            return tableView.rowHeight
        }
        else
        {
            return 95
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let image: UIImage = UIImage(named: "NoImage.jpg")!
        var imageView = UIImageView(image: image)
        
        if strModuleType == "ServicePestFlow" {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "woImagePath")
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                
                
            }
            
            
        }else if strModuleType as String == flowTypeWdoSalesService {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "woImagePath")
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                
                
            }
            
            
        }
        else if strModuleType as String == "Target" {
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "leadImagePath")
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                
                
            }
            
            
        }
        else if strModuleType as String == "SalesFlow" {
            
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            if objTemp .isKind(of: ImageDetail.self) // Image Detail
            {
                let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
                
                let strImagePath = objTemp.value(forKey: "leadImagePath")
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
                
                if image != nil  {
                    
                    imageView = UIImageView(image: image)
                    
                }else {
                    
                    
                    
                }
            }
            else
            {
                let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
                
                let strImagePath = objTemp.value(forKey: "path")
                //                var strImagePath = objTemp.value(forKey: "path") as! String
                //                strImagePath = strImagePath.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                //                strImagePath = strImagePath.replacingOccurrences(of: "/", with: "\\", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
                
                if image != nil  {
                    
                    imageView = UIImageView(image: image)
                    
                }else {
                    
                    
                    
                }
            }
        }
        else{
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "leadImagePath")
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                
                
            }
            
        }
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
        testController!.img = imageView.image!
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func reloadWithAnimation() {
        
        tblViewImages.reloadData()
        let tableViewHeight = tblViewImages.bounds.size.height
        let cells = tblViewImages.visibleCells
        
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.5, delay: 0.08 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
    
    
    
}


// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension GlobalImageVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in

        })
        
        if isImagePick == true {
            
            isImagePick = false
            self.SaveImageToDb(image: (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!)

        }
        
        //self.SaveImageToDb(image: (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!)
        
    }
}

/*

// MARK:-
// MARK:- ---------PopUpDelegate

extension GlobalImageVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        
        if(tag == 1){
           
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: dictData.value(forKey: "Name") as! String, viewcontrol: self)
            
        }
    }
}
 
 */

// MARK:-
// MARK:- ---------TextFiledDelegate
