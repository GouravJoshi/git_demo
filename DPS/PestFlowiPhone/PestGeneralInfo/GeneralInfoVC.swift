//
//  GeneralInfoVC.swift
//  DPS
//  peSTReam iOS Code
//  Created by Akshay Hastekar on 27/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  Saavan Patidar Changes

//  Saavan Patidar 2021
//  2021


import UIKit

class GeneralInfoVC: UIViewController
{
    
    // MARK: -  ----- Outlet  -----
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnClock: UIButton!
    
    @IBOutlet weak var btnServiceDocs: UIButton!
    
    @IBOutlet weak var btnGeneralInfo: UIButton!
    
    @IBOutlet weak var btnInspection: UIButton!
    
    @IBOutlet weak var btnServiceDetail: UIButton!
    
    @IBOutlet weak var btnInvoice: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    // MARK: -  ----- View Customer Info Outlet  -----
    
    @IBOutlet weak var btnShowCustomerInfo: UIButton!
    @IBOutlet weak var viewCustomerInfo: CardView!
    @IBOutlet weak var const_ViewCustomerInfo_H: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var btnNoEmail: UIButton!
    @IBOutlet weak var txtFldPrimaryEmailCustInfo: UITextField!
    @IBOutlet weak var txtFldSecondaryEmailCustInfo: UITextField!
    @IBOutlet weak var txtFldPrimaryPhoneCustInfo: UITextField!
    @IBOutlet weak var txtFldSecondaryPhoneCustInfo: UITextField!
    @IBOutlet weak var txtFldCellNoCustInfo: UITextField!
    @IBOutlet weak var txtViewAccountAlert: UITextView!
    
    @IBOutlet weak var btnPrimaryEmailCustInfo: UIButton!
    @IBOutlet weak var btnSecondaryEmailCustInfo: UIButton!
    @IBOutlet weak var btnPrimaryPhoneCustInfo: UIButton!
    @IBOutlet weak var btnSecondaryPhoneCustInfo: UIButton!
    @IBOutlet weak var btnCellNoCustInfo: UIButton!
    
    // MARK: -  ----- View Work Order Info Outlet  -----
    @IBOutlet weak var viewWorkOrderInfo: CardView!
    @IBOutlet weak var btnShowWorkOrderInfo: UIButton!
    @IBOutlet weak var const_ViewWorkOrderInfo_H: NSLayoutConstraint!
    @IBOutlet weak var btnCollectPayment: UIButton!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblServices: UILabel!
    @IBOutlet weak var lblEmployeeNo: UILabel!
    @IBOutlet weak var lblEmployeeName: UILabel!
    @IBOutlet weak var lblKeyMap: UILabel!
    @IBOutlet weak var lblRoutNo: UILabel!
    @IBOutlet weak var lblRoutName: UILabel!
    @IBOutlet weak var lblScheduleStartDateTime: UILabel!
    @IBOutlet weak var lblScheduleEndDateTime: UILabel!
    @IBOutlet weak var lblEarliestStartTime: UILabel!
    @IBOutlet weak var lblLatestStartTime: UILabel!
    @IBOutlet weak var lblFromTime: UILabel!
    @IBOutlet weak var lblToTime: UILabel!
    
    @IBOutlet weak var lblDrivceTime: UILabel!
    
    
    
    // MARK: -  ----- View Billing Contact & Address Outlet  -----
    
    @IBOutlet weak var viewBillingContactAddress: CardView!
    @IBOutlet weak var btnShowBillingContactAddress: UIButton!
    @IBOutlet weak var constViewBillingContactAddress_H: NSLayoutConstraint!
    
    @IBOutlet weak var btnSelectBillingContactAddress: UIButton!
    @IBOutlet weak var lblContactNameBillingContact: UILabel!
    @IBOutlet weak var lblMapCodeBillingContact: UILabel!
    @IBOutlet weak var lblPrimaryEmailBillingContact: UILabel!
    @IBOutlet weak var lblSecondaryEmailBillingContact: UILabel!
    @IBOutlet weak var lblPrimaryPhoneBillingContact: UILabel!
    @IBOutlet weak var lblSecondaryPhoneBillingContact: UILabel!
    @IBOutlet weak var btnAddressBillingContact: UIButton!
    @IBOutlet weak var lblCountyBillingContact: UILabel!
    @IBOutlet weak var lblSchoolDistrictBillingContact: UILabel!
    
    @IBOutlet weak var btnCellBillingContact: UIButton!
    
    @IBOutlet weak var lblCellBillingContact: UILabel!
    // MARK: -  ----- View Service Contact & Address Outlet  -----
    @IBOutlet weak var viewServiceContactAddress: CardView!
    @IBOutlet weak var btnShowServiceContactAddress: UIButton!
    @IBOutlet weak var constViewServiceContactAddress_H: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewServiceContactAddress: UIImageView!
    @IBOutlet weak var btnUploadEditImageServiceContact: UIButton!
    
    @IBOutlet weak var btnSelectServiceContactAddress: UIButton!
    @IBOutlet weak var lblContactNameServiceContact: UILabel!
    @IBOutlet weak var lblMapCodeServiceContact: UILabel!
    @IBOutlet weak var lblPrimaryEmailServiceContact: UILabel!
    @IBOutlet weak var lblSecondaryEmailServiceContact: UILabel!
    @IBOutlet weak var lblPrimaryPhoneServiceContact: UILabel!
    @IBOutlet weak var lblSecondaryPhoneServiceContact: UILabel!
    @IBOutlet weak var btnAddressServiceContact: UIButton!
    @IBOutlet weak var lblCountyServiceContact: UILabel!
    @IBOutlet weak var lblSchoolDistrictServiceContact: UILabel!
    
    @IBOutlet weak var btnCellServiceContact: UIButton!
    
    @IBOutlet weak var lblCellServiceContact: UILabel!
    
    @IBOutlet weak var lblGateCodeServiceContact: UILabel!
    @IBOutlet weak var lblNotesServiceContact: UILabel!
    
    @IBOutlet weak var txtViewDirectionServiceContact: UITextView!
    // MARK: -  ----- View Upload Image Outlet  -----
    
    @IBOutlet weak var viewUploadImage: CardView!
    @IBOutlet weak var btnShowUploadImage: UIButton!
    @IBOutlet weak var constViewUploadImage_H: NSLayoutConstraint!
    
    
    // MARK: -  ----- View Other Outlet  -----
    
    @IBOutlet weak var viewOther: CardView!
    @IBOutlet weak var btnShowViewOther: UIButton!
    @IBOutlet weak var const_ViewOther_H: NSLayoutConstraint!
    @IBOutlet weak var txtViewServiceDescription: UITextView!
    @IBOutlet weak var txtViewServiceInstruction: UITextView!
    @IBOutlet weak var txtViewAttribute: UITextView!
    @IBOutlet weak var txtViewTarget: UITextView!
    @IBOutlet weak var txtViewSetupInstruction: UITextView!
    
    // MARK: -  ----- View Technician Comment Outlet  -----
    
    @IBOutlet weak var viewTechnicianComment: CardView!
    @IBOutlet weak var btnShowViewTechnicianComment: UIButton!
    @IBOutlet weak var constViewTechnicianComment_H: NSLayoutConstraint!
    @IBOutlet weak var txtViewTechnicianComment: UITextView!
    @IBOutlet weak var btnServiceStatus: UIButton!
    @IBOutlet weak var btnResetReason: UIButton!
    @IBOutlet weak var viewResetReason: UIView!
    @IBOutlet weak var constViewResetReason: NSLayoutConstraint!
    @IBOutlet weak var txtViewResetReason: UITextView!
    
    // MARK: -  ----- View Time Range Outlet -----
    
    @IBOutlet weak var viewTimeRange: CardView!
    @IBOutlet weak var btnShowTimeRange: UIButton!
    @IBOutlet weak var const_ViewTimeRange_H: NSLayoutConstraint!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var lblStartTimeValue: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    
    @objc var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var isCollectPayment = false
    
    // MARK: -  ----- Variable Declaration -----
    
    // MARK: -  --- String ---
    
    var strEmpID = String()
    var strEmpName = String()
    var strFromEmail = String()
    var strResetId = String()
    var strServicePocId = String()
    var strBillingPocId = String()
    var strServicesStatus = String()
    var strDepartmentType = String()
    // MARK: -  --- Array ---
    
    var arrData = NSMutableArray()
    var arrServiceAddressContactDetails = NSMutableArray()
    var arrBillingAddressContactDetails = NSMutableArray()
    
    // MARK: -  --- Dictionary ---
    
    var dictLoginData = NSDictionary()
    var dictServiceContactData = NSDictionary()
    var dictBillingContactData = NSDictionary()
    
    // MARK: -  --- Bool ---
    
    var isNoEmail = Bool()
    var isResetReason = Bool()
    var yesEditedSomething = Bool()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            isNoEmail = false
            isResetReason = false
            yesEditedSomething = false
            strResetId = ""
            strServicePocId = ""
            strBillingPocId = ""
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
            
            strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
            
            strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            
            strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            
            let strServiceReportType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
            
            if strServiceReportType == "CompanyEmail"
            {
                strFromEmail = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
            }
            else
            {
                strFromEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
            }
            
            fetchServiceAddressContactDetails()
            fetchBillingAddressContactDetails()
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
            
            if arryOfData.count > 0 {
                
                objWorkorderDetail = arryOfData[0] as! NSManagedObject
                
                assignValues()
                
            }
            
            //btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            btnNoEmail.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            btnShowCustomerInfo.setImage(UIImage(named: "hide"), for: .normal)
            btnShowWorkOrderInfo.setImage(UIImage(named: "hide"), for: .normal)
            btnShowBillingContactAddress.setImage(UIImage(named: "hide"), for: .normal)
            btnShowServiceContactAddress.setImage(UIImage(named: "hide"), for: .normal)
            // btnShowUploadImage.setImage(UIImage(named: "hide"), for: .normal)
            btnShowViewOther.setImage(UIImage(named: "hide"), for: .normal)
            btnShowViewTechnicianComment.setImage(UIImage(named: "hide"), for: .normal)
            btnShowTimeRange.setImage(UIImage(named: "hide"), for: .normal)
            //end
            
            setBorderAtLoad()
            btnCollectPayment.isEnabled = false
            
            // Saving Version No
            
            global.updateWoVersionNo(strWoId as String)
            
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !Check_Login_Session_expired() {
            methodForDisable()
            getClockStatus()
            btnCollectPayment.isEnabled = false
            btnCollectPayment.isEnabled = false
            
            if (strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
            
                btnSave.isEnabled = false
                
            }
            
            btnCollectPayment.isEnabled = false
                    
        }
       
    }
    // MARK: -  ------------------------------ Action ------------------------------
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        //dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func actionOnServiceDoc(_ sender: Any)
    {
        goToServiceDocument()
    }
    
    @IBAction func actionOnClock(_ sender: Any)
    {
        if !isInternetAvailable()
        {
            Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
        }
        else
        {
            let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
            let clockInOutVC = storyboardIpad.instantiateViewController(withIdentifier: "ClockInOutViewController") as? ClockInOutViewController
            self.navigationController?.pushViewController(clockInOutVC!, animated: false)
        }
        
    }
    
    @IBAction func action_Image(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Before")
        
    }
    
    @IBAction func action_ServiceHistory(_ sender: Any) {
        
        self.goToServiceHistory()
        
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: Any) {
        
        self.goToCustomerSalesDocuments()
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")
        
        
    }
    
    @IBAction func action_NotesHistory(_ sender: Any) {
        
        self.goToNotesHistory()
        
    }
    
    
    @IBAction func actionOnSave(_ sender: Any)
    {
        
        saveData()
        
    }
    
    // MARK: -  -----Actions View Customer Info   -----
    
    @IBAction func actionShowCustomerInfo(_ sender: Any)
    {
        showHideCustomerInfo()
        
    }
    
    @IBAction func actionOnNoEmail(_ sender: Any)
    {
        if (btnNoEmail.backgroundImage(for: .normal) == UIImage(named: "check_box_1.png"))
        {
            btnNoEmail.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            
            isNoEmail = true
            
            txtFldPrimaryEmailCustInfo.text = ""
        }
        else
        {
            btnNoEmail.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            isNoEmail = false
            
        }
    }
    
    @IBAction func actionOnPrimaryEmailCustInfo(_ sender: Any)
    {
        if (txtFldPrimaryEmailCustInfo.text?.count)! > 0
        {
            global.emailComposer(txtFldPrimaryEmailCustInfo.text, "", "", self)
        }
    }
    
    @IBAction func actionOnSecondaryEmailCustInfo(_ sender: Any)
    {
        if (txtFldSecondaryEmailCustInfo.text?.count)! > 0
        {
            global.emailComposer(txtFldSecondaryEmailCustInfo.text, "", "", self)
        }
        
    }
    
    @IBAction func actionOnPrimaryPhoneCustInfo(_ sender: Any)
    {
        if ((txtFldPrimaryPhoneCustInfo.text?.count)!>0)
        {
            global.calling(txtFldPrimaryPhoneCustInfo.text)
            
        }
    }
    
    @IBAction func actionOnSecondaryPhoneCustInfo(_ sender: Any)
    {
        if ((txtFldSecondaryPhoneCustInfo.text?.count)!>0)
        {
            global.calling(txtFldSecondaryPhoneCustInfo.text)
            
        }
    }
    
    @IBAction func actionOnCellNoCustInfo(_ sender: Any)
    {
        if ((txtFldCellNoCustInfo.text?.count)!>0)
        {
            global.calling(txtFldCellNoCustInfo.text)
            
        }
    }
    // MARK: -  -----Actions View Work Order Info   -----
    
    @IBAction func actionShowWorkOrderInfo(_ sender: Any)
    {
        showHideWorkOrderInfo()
        
    }
    
    @IBAction func actionOnCollectPayment(_ sender: Any)
    {
        /*
        if (btnCollectPayment.backgroundImage(for: .normal) == UIImage(named: "check_box_1.png"))
        {
            btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            isCollectPayment = true
            
        }
        else
        {
            btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            isCollectPayment = false
        }
        */
    }
    
    // MARK: -  -----Actions View Billing Contact & Address   -----
    
    @IBAction func actionShowBillingContactAddress(_ sender: Any)
    {
        
        showHideBillingContactAddress()
        
    }
    
    @IBAction func actionOnSelectBillingContact(_ sender: Any)
    {
        let arrOfData = arrBillingAddressContactDetails
        
        if arrOfData.count > 0
        {
            btnSelectBillingContactAddress.tag = 43
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No record found", viewcontrol: self)
        }
        
        
    }
    
    @IBAction func actionOnPrimaryEmailBillingContact(_ sender: Any)
    {
        if (lblPrimaryEmailBillingContact.text?.count)! > 0
        {
            global.emailComposer(lblPrimaryEmailBillingContact.text, "", "", self)
        }
    }
    
    @IBAction func actionOnSecondaryEmailBililngContact(_ sender: Any)
    {
        if (lblSecondaryEmailBillingContact.text?.count)! > 0
        {
            global.emailComposer(lblSecondaryEmailBillingContact.text, "", "", self)
        }
        
        
    }
    
    @IBAction func actionOnPrimaryPhoneBillingContact(_ sender: Any)
    {
        // global.emailComposer(lblPrimaryPhoneBillingContact.text, "", "", self)
        if ((lblPrimaryPhoneBillingContact.text?.count)!>0)
        {
            global.calling(lblPrimaryPhoneBillingContact.text)
            
        }
        
    }
    
    @IBAction func actionOnSecondaryPhoneBillingContact(_ sender: Any)
    {
        // global.emailComposer(lblSecondaryPhoneBillingContact.text, "", "", self)
        if ((lblSecondaryPhoneBillingContact.text?.count)!>0)
        {
            global.calling(lblSecondaryPhoneBillingContact.text)
            
        }
        
    }
    
    @IBAction func actionOnAddressBillingContact(_ sender: Any)
    {
        global.redirect(onAppleMap: self, btnAddressBillingContact.titleLabel?.text)
    }
    
    @IBAction func actionOnCellNoBillingContact(_ sender: Any)
    {
        if ((lblCellBillingContact.text?.count)!>0)
        {
            global.calling(lblCellBillingContact.text)
            
        }
    }
    
    
    // MARK: -  -----Actions View Service Contact & Address   -----
    
    @IBAction func actionOnUploadEditImageServiceContact(_ sender: Any)
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            //  BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    @IBAction func actionShowServiceContactAddress(_ sender: Any)
    {
        
        showHideServiceContactAddress()
        
    }
    
    @IBAction func actionOnSelectServiceContact(_ sender: Any)
    {
        let arrOfData = arrServiceAddressContactDetails
        if arrOfData.count > 0
        {
            btnSelectServiceContactAddress.tag = 40
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No record found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnPrimaryEmailServiceContact(_ sender: Any)
    {
        if (lblPrimaryEmailServiceContact.text?.count)! > 0
        {
            global.emailComposer(lblPrimaryEmailServiceContact.text, "", "", self)
        }
        
    }
    
    @IBAction func actionOnSecondaryEmailServiceContact(_ sender: Any)
    {
        if (lblSecondaryEmailServiceContact.text?.count)! > 0
        {
            global.emailComposer(lblSecondaryEmailServiceContact.text, "", "", self)
        }
        
    }
    
    @IBAction func actionOnPrimaryPhoneServiceContact(_ sender: Any)
    {
        //global.emailComposer(lblPrimaryPhoneServiceContact.text, "", "", self)
        if ((lblPrimaryPhoneServiceContact.text?.count)!>0)
        {
            global.calling(lblPrimaryPhoneServiceContact.text)
            
        }
        
    }
    
    @IBAction func actionOnSecondaryPhoneServiceContact(_ sender: Any)
    {
        //global.emailComposer(lblSecondaryPhoneServiceContact.text, "", "", self)
        if ((lblSecondaryPhoneServiceContact.text?.count)!>0)
        {
            global.calling(lblSecondaryPhoneServiceContact.text)
            
        }
        
    }
    
    @IBAction func actionOnAddressServiceContact(_ sender: Any)
    {
        global.redirect(onAppleMap: self, btnAddressServiceContact.titleLabel?.text)
        
    }
    
    @IBAction func actionOnCellNoServiceContact(_ sender: Any)
    {
        if ((lblCellServiceContact.text?.count)!>0)
        {
            global.calling(lblCellServiceContact.text)
            
        }
    }
    
    // MARK: -  -----Actions View Upload Image   -----
    
    
    @IBAction func actionShowUploadImage(_ sender: Any)
    {
        
        //showHideUploadImage()
        
    }
    
    
    // MARK: -  -----Actions View Other  -----
    
    @IBAction func actionShowOther(_ sender: Any)
    {
        
        showHideOther()
        
    }
    
    
    // MARK: -  -----Actions View Technician Comment   -----
    
    @IBAction func actionShowTechnicianComment(_ sender: Any)
    {
        
        showHideTechnicianComment()
    }
    
    @IBAction func actionOnServiceStatus(_ sender: Any)
    {
        /* let arrOfData = arrData
         btnServiceStatus.tag = 42
         gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")*/
        
        let alert = UIAlertController(title: "", message: "Please choose status", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Incomplete", style: .default , handler:{ (UIAlertAction)in
            
            //self.goToAreaView()
            self.btnServiceStatus.setTitle("Incomplete", for: .normal)
            self.showHideResetReason()
            self.isResetReason = false
            
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Reset", style: .default , handler:{ (UIAlertAction)in
            
            self.btnServiceStatus.setTitle("Reset", for: .normal)
            self.showHideResetReason()
            self.isResetReason = true
            // self.goToAddDeviceView()
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            self.showHideResetReason()
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    @IBAction func actionOnResetReason(_ sender: Any)
    {
        
        if (nsud.value(forKey: "MasterServiceAutomation") != nil) {

            let dictDetailServiceMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            
            if nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
                
                if dictDetailServiceMaster.value(forKey: "ResetReasons") is NSArray {
                    
                    let arrOfLogType = (dictDetailServiceMaster.value(forKey: "ResetReasons") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    
                    if arrOfLogType.count > 0
                    {
                        let arrOfData = arrOfLogType
                        if arrOfData.count > 0
                        {
                            btnResetReason.tag = 41
                            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)
                    }
                    
                }
                
            }
            
        }else{

            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)

        }
        
    }
    
    // MARK: -  -----Actions View Time Range    -----
    
    @IBAction func actionReset(_ sender: Any)
    {
        viewResetReason.isHidden = false
        
        //self.strServicesStatus = "Reset"
        viewResetReason.frame = self.view.frame
        self.view.addSubview(self.viewResetReason)
        
    }
    @IBAction func actionCancleResetReason(_ sender: UIButton)
    {
        
        if(sender.tag == 1){ // For Reset Click
            
            isResetReason = true
            self.strServicesStatus = "Reset"

            saveData()
            
        }else{
            
            btnResetReason.setTitle("---Select---", for: .normal)
            strResetId = ""
            txtViewResetReason.text = ""
            self.strServicesStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
            isResetReason = false
            self.viewResetReason.removeFromSuperview()

        }
        
        //self.viewResetReason.removeFromSuperview()
        
    }
    
    
    @IBAction func actionShowTimeRange(_ sender: Any)
    {
        
        showHideTimeRange()
    }
    
    @IBAction func actionOnStartTime(_ sender: Any)
    {
        /* btnStartTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime")*/
        
        let str = "\(lblStartTimeValue.titleLabel?.text ?? "")"
        
        if str == ""
        {
            getStartTimeIn()
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want to reset start time", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Yes-Reset", style: .default , handler:{ (nil)in
                
                self.getStartTimeIn()
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (nil)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    func getStartTimeIn()
    {
        yesEditedSomething = true
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        lblStartTimeValue.setTitle(str, for: .normal)
    }
    
    // MARK: -  ----- Function  -----
    
    func saveData()
    {
        
        if strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed"
        {
            self.goToInspectionView()
        }
        else
        {
            trimTextField()
            
            if ((global.checkIfCommaSepartedEmailsAreValid(txtFldPrimaryEmailCustInfo.text!) == false) && ((txtFldPrimaryEmailCustInfo.text?.count)!>0 ) )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid Primary Email", viewcontrol: self)
            }
            else if ((global.checkIfCommaSepartedEmailsAreValid(txtFldSecondaryEmailCustInfo.text!) == false) && ((txtFldSecondaryEmailCustInfo.text?.count)!>0 ) )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid Secondary Email", viewcontrol: self)
            }
            else if("\(lblStartTimeValue.titleLabel?.text ?? "")" == "")
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter the Start Time", viewcontrol: self)
            }
            else
            {
                
                if isResetReason == true
                {
                    // let strResetReason = "\(btnResetReason.titleLabel?.text ?? "")"
                    if (strResetId == "")
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select reset reason", viewcontrol: self)
                    }
                    else
                    {
                        if checkImage() == true
                        {
                            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one before image", viewcontrol: self)
                        }
                        else
                        {
                            
                            updateWorkOrderDetail()
                            saveEmailToCoreData()
                            
                            self.goToSendMailOnReset()
                            
                        }
                    }
                }
                else
                {
                    if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one before image", viewcontrol: self)
                    }
                    else
                    {
                        updateWorkOrderDetail()
                        saveEmailToCoreData()
                        self.goToInspectionView()
                    }
                }
                
            }
            
        }
        
    }
    
    func goToSendMailOnReset() {
        
        var image :UIImage?
        let currentLayer = UIApplication.shared.keyWindow!.layer
        let currentScale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(currentLayer.frame.size, false, currentScale);
        guard let currentContext = UIGraphicsGetCurrentContext() else {return}
        currentLayer.render(in: currentContext)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.viewResetReason.removeFromSuperview()
        
        nsud.set(true, forKey: "isFromSurveyToSendEmail")
        nsud.synchronize()
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceSendMailViewController") as? ServiceSendMailViewController
        testController?.strBackRoundImage = image
        self.navigationController?.pushViewController(testController!, animated: false)
        
        //        NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
        //        [defsss setBool:YES forKey:@"isFromSurveyToSendEmail"];
        //        [defsss synchronize];
        //
        //
        //        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
        //            bundle: nil];
        //        ServiceSendMailViewController
        //        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewController"];
        //        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
    func checkImage() -> Bool
    {
        let chkImage = nsud.bool(forKey: "isCompulsoryBeforeImageService")
        
        if chkImage == true
        {
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, "Before"))
            
            if arryOfData.count > 0
            {
                
                return false
                
            }
            else
            {
                return true
            }
            
        }
        else
        {
            
            return false
            
        }
        
    }
    
    
    func getClockStatus()
    {
        var strClockStatus = String()
        
        if !isInternetAvailable()
        {
            
            strClockStatus = global.fetchClockInOutDetailCoreData()
            
            if strClockStatus == "" || strClockStatus.count == 0 || strClockStatus == "<null>"
            {
                btnClock.setImage(UIImage(named: "white"), for: .normal)
            }
            else if strClockStatus == "WorkingTime"
            {
                btnClock.setImage(UIImage(named: "green"), for: .normal)
                
            }
            else if strClockStatus == "BreakTime"
            {
                btnClock.setImage(UIImage(named: "orange"), for: .normal)
            }
            else
            {
                btnClock.setImage(UIImage(named: "red"), for: .normal)
                
            }
        }
        else
        {
            DispatchQueue.global(qos: .background).async
                {
                    strClockStatus = self.global.getCurrentTimerOfClock()
                    
                    DispatchQueue.main.async
                        {
                            if strClockStatus == "" || strClockStatus.count == 0 || strClockStatus == "<null>"
                            {
                                self.btnClock.setImage(UIImage(named: "white"), for: .normal)
                            }
                            else if strClockStatus == "WorkingTime"
                            {
                                self.btnClock.setImage(UIImage(named: "green"), for: .normal)
                                
                            }
                            else if strClockStatus == "BreakTime"
                            {
                                self.btnClock.setImage(UIImage(named: "orange"), for: .normal)
                            }
                            else
                            {
                                self.btnClock.setImage(UIImage(named: "red"), for: .normal)
                                
                            }
                            
                    }
            }
        }
        
    }
    
    
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func saveEmailToCoreData()
    {
        
        let strEmailPrimary = txtFldPrimaryEmailCustInfo.text
        
        let strEmailScondary = txtFldSecondaryEmailCustInfo.text
        
        
        var arrEmailPrimary = strEmailPrimary!.components(separatedBy: ",") as NSArray
        
        //let conciseUniqueValues = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
        arrEmailPrimary = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrEmailPrimary)")
        
        
        var arrEmailSecondary = strEmailScondary!.components(separatedBy: ",") as NSArray
        
        arrEmailSecondary = arrEmailSecondary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        let arrEmailToSave = NSMutableArray()
        
        
        for i in 0 ..< arrEmailPrimary.count
        {
            let strPrimaryEmail = "\(arrEmailPrimary[i])"
            var chkEmailExist = Bool()
            chkEmailExist = false
            
            for j in 0 ..< arryOfData.count
            {
                
                let  objEmailDetail = arryOfData[j] as! NSManagedObject
                let strEmailId = "\(objEmailDetail.value(forKey: "emailId")!)"
                if strEmailId == strPrimaryEmail
                {
                    
                    chkEmailExist = true
                    break
                    
                }
                else
                {
                    
                    chkEmailExist = false
                    
                }
                
            }
            
            if chkEmailExist == false
            {
                
                arrEmailToSave.add(strPrimaryEmail)
                
            }
            
        }
        for i in 0 ..< arrEmailSecondary.count
        {
            let strSecondaryEmail = "\(arrEmailSecondary[i])"
            
            if strSecondaryEmail.count > 0
            {
                var chkEmailExist = Bool()
                chkEmailExist = false
                
                for j in 0 ..< arryOfData.count
                {
                    
                    let  objEmailDetail = arryOfData[j] as! NSManagedObject
                    let strEmailId = "\(objEmailDetail.value(forKey: "emailId")!)"
                    if strEmailId == strSecondaryEmail
                    {
                        
                        chkEmailExist = true
                        break
                        
                    }
                    else
                    {
                        
                        chkEmailExist = false
                        
                    }
                }
                
                if chkEmailExist == false
                {
                    
                    arrEmailToSave.add(strSecondaryEmail)
                    
                }
            }
        }
        
        //NSArray *uniqueArray = [[NSSet setWithArray:duplicateArray] allObjects];
        
        /*   let conciseUniqueValues = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
         print("Unique array \(conciseUniqueValues)")*/
        
        //conciseUniqueValues = [5, 6, 9, 7, 4]
        
        for i in 0 ..< arrEmailToSave.count
        {
            let strEmailToSave = "\(arrEmailToSave[i])"
            
            if strEmailToSave.count > 0
            {
                
                var arrOfKeys = NSMutableArray()
                var arrOfValues = NSMutableArray()
                
                arrOfKeys = ["createdBy",
                             "createdDate",
                             "emailId",
                             "isCustomerEmail",
                             "isMailSent",
                             "companyKey",
                             "modifiedBy",
                             "modifiedDate",
                             "subject",
                             "userName",
                             "workorderId",
                             "woInvoiceMailId"]
                
                arrOfValues = ["0",
                               "0",
                               strEmailToSave,
                               "true",
                               "true",
                               strCompanyKey,
                               "",
                               global.getCurrentDate(),
                               "",
                               strUserName,
                               strWoId,
                               ""]
                
                saveDataInDB(strEntity: "EmailDetailServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            
        }
        
        
    }
    func changeStringDateToGivenFormat(strDate: String , strRequiredFormat: String) -> String
    {
        let strTimeIn  = strDate
        
        var dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var date1 = Date()
        var strDateFinal = String()
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        }
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        
        return ""
        
    }
    
    
    
    func fetchServiceAddressContactDetails()
    {
        //ServiceAddressPOCDetailDcs
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceAddressPOCDetailDcs", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0
        {
            
            let  objServiceAddressContactDetails = arryOfData[0] as! NSManagedObject
            
            arrServiceAddressContactDetails = objServiceAddressContactDetails.value(forKey: "pocDetails")  as! NSMutableArray
            
        }
        else
        {
            
            // showAlertWithoutAnyAction(strtitle: alertMessage, assignstrMessage: alertNoWo, viewcontrol: self)
            
            //self.back()
            
        }
    }
    func fetchBillingAddressContactDetails()
    {
        //ServiceAddressPOCDetailDcs
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "BillingAddressPOCDetailDcs", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0
        {
            let  objBillingAddressContactDetails = arryOfData[0] as! NSManagedObject
            
            arrBillingAddressContactDetails = objBillingAddressContactDetails.value(forKey: "pocDetails") as!  NSMutableArray
            
        }
        else
        {
            
            // showAlertWithoutAnyAction(strtitle: alertMessage, assignstrMessage: alertNoWo, viewcontrol: self)
            
            //self.back()
            
        }
    }
    
    
    func goToInspectionView()
    {
        
        let isNPMATermite = "\(objWorkorderDetail.value(forKey: "isNPMATermite")!)"

        if isNPMATermite == "true" || isNPMATermite == "True" || isNPMATermite == "1" {
            
            //self.goToNPMAInspectionView()
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)

        }else{
            
            if strDepartmentType == "Termite"
            {
                
                let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "TermiteSelectStateViewController") as? TermiteSelectStateViewController
                testController?.strWorkOrder = strWoId as String
                testController?.strWorkOrderStatus = strWorkOrderStatus
                testController?.strTermiteStateType = "\(objWorkorderDetail.value(forKey: "termiteStateType") ?? "")"
                
                self.navigationController?.pushViewController(testController!, animated: false)
            }
            else
            {
                let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "InspectionVC") as? InspectionVC
                testController?.matchesWorkOrderZSync = objWorkorderDetail
                self.navigationController?.pushViewController(testController!, animated: false)
            }
            
        }
        
    }
    
    func goToNPMAInspectionView()
    {
        
        let storyboardIpad = UIStoryboard.init(name: "NPMA", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "NPMA_Inspection_iPadVC") as? NPMA_Inspection_iPadVC
        testController?.strWoId = strWoId
        self.navigationController?.pushViewController(testController!, animated: false)
        
    }
    
    func goToServiceDocument() {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        //self.navigationController?.pushViewController(objServiceDocumentsVC!, animated: false)
        objServiceDocumentsVC?.modalPresentationStyle = .fullScreen
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    func assignValues()
    {
        
        print("WorkOrderDetails \(objWorkorderDetail)")
        
        
        strDepartmentType = "\(objWorkorderDetail.value(forKey: "departmentType") ?? "")"//[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"departmentType"]];

        
        // IsCollectPayment  isCollectPayment
        
        let strIsCollectPayment = "\(objWorkorderDetail.value(forKey: "isCollectPayment") ?? "")"
        
        if strIsCollectPayment == "1" || strIsCollectPayment == "true" || strIsCollectPayment == "True" {
            
            isCollectPayment = true
            btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            
        }else{
            
            isCollectPayment = false
            btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            
        }
        
        
        strWorkOrderStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
        
        nsud.set(strWorkOrderStatus, forKey: "workOrderStatus")
        nsud.synchronize()
        
        strServicePocId = "\(objWorkorderDetail.value(forKey: "servicePOCId") ?? "")"
        strBillingPocId = "\(objWorkorderDetail.value(forKey: "billingPOCId") ?? "")"
        
        
        //Service Address Image Download
        strServiceAddImageName = "\(objWorkorderDetail.value(forKey: "serviceAddressImagePath") ?? "")"
        
        if strServiceAddImageName.count > 0
        {
            downloadServiceAddImage()
        }
        
        
        lblCustomerName.text = global.strFullName(for: objWorkorderDetail)//"\(objWorkorderDetail.value(forKey: "") ?? "")"
        
        lblCompanyName.text = "\(objWorkorderDetail.value(forKey: "companyName") ?? "")"
        
        txtFldPrimaryEmailCustInfo.text = "\(objWorkorderDetail.value(forKey: "primaryEmail") ?? "")"
        btnPrimaryEmailCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "primaryEmail") ?? "")", for: .normal)
        
        
        txtFldSecondaryEmailCustInfo.text = "\(objWorkorderDetail.value(forKey: "secondaryEmail") ?? "")"
        btnSecondaryEmailCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "secondaryEmail") ?? "")", for: .normal)
        
        
        
        txtFldPrimaryPhoneCustInfo.text = "\(objWorkorderDetail.value(forKey: "primaryPhone") ?? "")"
        btnPrimaryPhoneCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "primaryPhone") ?? "")", for: .normal)
        
        
        txtFldSecondaryPhoneCustInfo.text = "\(objWorkorderDetail.value(forKey: "secondaryPhone") ?? "")"
        btnSecondaryPhoneCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "secondaryPhone") ?? "")", for: .normal)
        
        txtFldCellNoCustInfo.text = "\(objWorkorderDetail.value(forKey: "cellNo") ?? "")"
        btnCellNoCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "cellNo") ?? "")", for: .normal)
        
        lblCategory.text = "\(objWorkorderDetail.value(forKey: "categoryName") ?? "")"
        
        lblServices.text = "\(objWorkorderDetail.value(forKey: "services") ?? "")"
        
        lblEmployeeNo.text = "\(objWorkorderDetail.value(forKey: "employeeNo") ?? "")"
        
        lblEmployeeName.text = strEmpName
        
        lblKeyMap.text = "\(objWorkorderDetail.value(forKey: "keyMap") ?? "")"
        
        lblRoutNo.text = "\(objWorkorderDetail.value(forKey: "routeNo") ?? "")"
        
        lblRoutName.text = "\(objWorkorderDetail.value(forKey: "routeName") ?? "")"
        
        
        lblScheduleStartDateTime.text = "\(objWorkorderDetail.value(forKey: "scheduleOnStartDateTime") ?? "")"
        
        if (lblScheduleStartDateTime.text?.count)! > 0
        {
            lblScheduleStartDateTime.text =  changeStringDateToGivenFormat(strDate:  lblScheduleStartDateTime.text!, strRequiredFormat: "MM/dd/yyyy hh:mm a")
        }
        else
        {
            lblScheduleStartDateTime.text =  " "
            
        }
        
        
        
        lblScheduleEndDateTime.text = "\(objWorkorderDetail.value(forKey: "scheduleOnEndDateTime") ?? "")"
        
        if (lblScheduleEndDateTime.text?.count)! > 0
        {
            lblScheduleEndDateTime.text =  changeStringDateToGivenFormat(strDate:  lblScheduleEndDateTime.text!, strRequiredFormat: "MM/dd/yyyy hh:mm a")
        }
        else
        {
            lblScheduleEndDateTime.text =  " "
            
        }
        
        
        var strEarliestStartTime = "\(objWorkorderDetail.value(forKey: "earliestStartTimeStr") ?? "")"
        
        /*  if strEarliestStartTime.count > 0
         {
         strEarliestStartTime =  changeStringDateToGivenFormat(strDate:  strEarliestStartTime, strRequiredFormat: "MM/dd/yyyy hh:mm a")
         }
         else
         {
         strEarliestStartTime =  " "
         
         }*/
        
        var strLatestStartTime = "\(objWorkorderDetail.value(forKey: "latestStartTimeStr") ?? "")"
        
        /* if strLatestStartTime.count > 0
         {
         strLatestStartTime =  changeStringDateToGivenFormat(strDate:  strLatestStartTime, strRequiredFormat: "MM/dd/yyyy hh:mm a")
         }
         else
         {
         strLatestStartTime =  ""
         
         }*/
        
        
        if strEarliestStartTime.count < 1 {
            
            strEarliestStartTime = ""
            
        }
        if strLatestStartTime.count < 1 {
            
            strLatestStartTime = ""
            
        }
        
        lblEarliestStartTime.text =  strEarliestStartTime + " - " + strLatestStartTime
        
        if strEarliestStartTime.count < 1 || strLatestStartTime.count < 1 {
            
            lblEarliestStartTime.text = ""
            
        }
        
        
        lblFromTime.text = " " //"\(objWorkorderDetail.value(forKey: "EmployeeNo") ?? "")"
        
        lblToTime.text = " " //"\(objWorkorderDetail.value(forKey: "EmployeeNo") ?? "")"
        
        lblDrivceTime.text =  "\(objWorkorderDetail.value(forKey: "driveTimeStr") ?? "")"
        
        //Billing
        
        var strBillingContactName = String()
        strBillingContactName = "\(objWorkorderDetail.value(forKey: "billingFirstName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "billingMiddleName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "billingLastName") ?? "")"
        lblContactNameBillingContact.text = strBillingContactName
        
        lblMapCodeBillingContact.text = "\(objWorkorderDetail.value(forKey: "billingMapCode") ?? "")"
        lblPrimaryEmailBillingContact.text = "\(objWorkorderDetail.value(forKey: "billingPrimaryEmail") ?? "")"
        lblSecondaryEmailBillingContact.text = "\(objWorkorderDetail.value(forKey: "billingSecondaryEmail") ?? "")"
        lblPrimaryPhoneBillingContact.text = "\(objWorkorderDetail.value(forKey: "billingPrimaryPhone") ?? "")"
        lblSecondaryPhoneBillingContact.text = "\(objWorkorderDetail.value(forKey: "billingSecondaryPhone") ?? "")"
        lblCellBillingContact.text = "\(objWorkorderDetail.value(forKey: "billingCellNo") ?? "")"
        btnAddressBillingContact.setTitle( global.strCombinedAddressBilling(for: objWorkorderDetail), for: .normal)
        
        lblCountyBillingContact.text = "\(objWorkorderDetail.value(forKey: "billingCounty") ?? "")"
        lblSchoolDistrictBillingContact.text = "\(objWorkorderDetail.value(forKey: "billingSchoolDistrict") ?? "")"
        
        
        //Service
        
        var strServiceContactName = String()
        strServiceContactName = "\(objWorkorderDetail.value(forKey: "serviceFirstName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "serviceMiddleName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "serviceLastName") ?? "")"
        lblContactNameServiceContact.text = strServiceContactName
        
        lblMapCodeServiceContact.text = "\(objWorkorderDetail.value(forKey: "serviceMapCode") ?? "")"
        lblPrimaryEmailServiceContact.text = "\(objWorkorderDetail.value(forKey: "servicePrimaryEmail") ?? "")"
        lblSecondaryEmailServiceContact.text = "\(objWorkorderDetail.value(forKey: "serviceSecondaryEmail") ?? "")"
        lblPrimaryPhoneServiceContact.text = "\(objWorkorderDetail.value(forKey: "servicePrimaryPhone") ?? "")"
        lblSecondaryPhoneServiceContact.text = "\(objWorkorderDetail.value(forKey: "serviceSecondaryPhone") ?? "")"
        btnAddressServiceContact.setTitle( global.strCombinedAddressService(for: objWorkorderDetail), for: .normal) // Service Address
        
        lblCellServiceContact.text = "\(objWorkorderDetail.value(forKey: "serviceCellNo") ?? "")"
        
        lblGateCodeServiceContact.text = "\(objWorkorderDetail.value(forKey: "serviceGateCode") ?? "")"
        
        lblNotesServiceContact.text = "\(objWorkorderDetail.value(forKey: "serviceNotes") ?? "")"
        
        txtViewDirectionServiceContact.text = "\(objWorkorderDetail.value(forKey: "direction") ?? "")"
        
        lblCountyServiceContact.text = "\(objWorkorderDetail.value(forKey: "serviceCounty") ?? "")"
        lblSchoolDistrictServiceContact.text = "\(objWorkorderDetail.value(forKey: "serviceSchoolDistrict") ?? "")"
        
        
        txtViewServiceDescription.text  = "\(objWorkorderDetail.value(forKey: "serviceDescription") ?? "")"
        
        txtViewServiceInstruction.text = "\(objWorkorderDetail.value(forKey: "serviceInstruction") ?? "")"
        
        txtViewTarget.text = "\(objWorkorderDetail.value(forKey: "targets") ?? "")"
        
        txtViewAttribute.text = "\(objWorkorderDetail.value(forKey: "atributes") ?? "")"
        
        txtViewSetupInstruction.text = "\(objWorkorderDetail.value(forKey: "specialInstruction") ?? "")"
        
        txtViewTechnicianComment.text = "\(objWorkorderDetail.value(forKey: "technicianComment") ?? "")"
        self.strServicesStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
        //  btnServiceStatus.setTitle("\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")", for: .normal) //workorderStatus
        
        
        // lblStartTimeValue.setTitle("\(objWorkorderDetail.value(forKey: "TimeIn") ?? "")", for: .normal)
        
        
        let strTimeIn  = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
        
        lblStartTimeValue.setTitle(changeStringDateToGivenFormat(strDate: strTimeIn, strRequiredFormat: "MM/dd/yyyy hh:mm a"), for: .normal)
        
        
        
        
        
        
        nsud.set("", forKey: "lblThirdPartyAccountNo")
        nsud.synchronize()
        
        var strThirdPartyAccountNo = String()
        strThirdPartyAccountNo = "\(objWorkorderDetail.value(forKey: "thirdPartyAccountNo") ?? "")"
        if strThirdPartyAccountNo.count > 0
        {
            lblTitle.text = "A/C #: " + strThirdPartyAccountNo + " Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
            
            nsud.set(lblTitle.text, forKey: "lblThirdPartyAccountNo")
            nsud.synchronize()
        }
        else
        {
            lblTitle.text = "A/C #: " + "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")" + " Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        }
        
        nsud.set(lblTitle.text, forKey: "lblName")
        nsud.set("\(objWorkorderDetail.value(forKey: "accountNo")!)", forKey: "workOrderAccountNo")
        nsud.set("\(objWorkorderDetail.value(forKey: "accountNo")!)", forKey: "AccNoService")
        nsud.set("\(objWorkorderDetail.value(forKey: "departmentId")!)", forKey: "DepartmentIdService")
        nsud.synchronize()
        
        
        // For Reset
        
        let dictDetailServiceMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        let arrOfLogType = dictDetailServiceMaster.value(forKey: "ResetReasons") as! NSArray
        strResetId = "\(objWorkorderDetail.value(forKey: "resetId") ?? "")"
        if arrOfLogType.count > 0
        {
            for i in 0 ..< arrOfLogType.count
            {
                let dict = arrOfLogType[i] as! NSDictionary
                
                if strResetId == "\(dict.value(forKey: "ResetId")!)"
                {
                    btnResetReason.setTitle(dict["ResetReason"] as? String, for: .normal)
                    break
                }
                
            }
        }
        
        
        
        if(strWorkOrderStatus.caseInsensitiveCompare("Reset") == ComparisonResult.orderedSame)
        {
            isResetReason = true
        }
        else if(strWorkOrderStatus.caseInsensitiveCompare("InComplete") == ComparisonResult.orderedSame)
        {
            isResetReason = false
        }
        else
        {
            isResetReason = false
        }
        
        
        let strWoNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        
        if nsud.value(forKey: "ListOfUnsignedDoc") is NSArray
        {
            let arrOfUnsignedDocs = nsud.value(forKey: "ListOfUnsignedDoc") as! NSArray
            
            if arrOfUnsignedDocs .contains(strWoNo)
            {
                btnServiceDocs.setImage(UIImage(named: "document_3"), for: .normal)
            }
            else
            {
                btnServiceDocs.setImage(UIImage(named: "document_2"), for: .normal)
                
            }
        }
        else
        {
            btnServiceDocs.setImage(UIImage(named: "document_2"), for: .normal)
            
        }
        
        if arrBillingAddressContactDetails.count != 0
        {
            for item in arrBillingAddressContactDetails
            {
                let dict = item as! NSDictionary
                
                let strId = "\(dict.value(forKey: "ContactId") ?? "")"
                
                if strBillingPocId == strId
                {
                    let strName = "\(dict.value(forKey: "FirstName") ?? "")" + " " + "\(dict.value(forKey: "MiddleName") ?? "")" + " " + "\(dict.value(forKey: "LastName") ?? "")"
                    btnSelectBillingContactAddress.setTitle(strName, for: .normal)
                    
                    break
                }
                
            }
        }
        
        if arrServiceAddressContactDetails.count != 0
        {
            for item in arrBillingAddressContactDetails
            {
                let dict = item as! NSDictionary
                
                let strId = "\(dict.value(forKey: "ContactId") ?? "")"
                
                if strServicePocId == strId
                {
                    
                    let strName = "\(dict.value(forKey: "FirstName") ?? "")" + " "  + "\(dict.value(forKey: "MiddleName") ?? "")" + " " + "\(dict.value(forKey: "LastName") ?? "")"
                    btnSelectServiceContactAddress.setTitle(strName, for: .normal)
                    
                    break
                }
                
            }
        }
        
        txtViewAccountAlert.text = "\(objWorkorderDetail.value(forKey: "accountDescription") ?? "")"

    }
    func downloadServiceAddImage()
    {
        
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strServiceAddImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strServiceAddImageName)
        
        if isImageExists!
        {
            imgViewServiceContactAddress.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strServiceAddImageUrl = String()
            
            strServiceAddImageUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            //let strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            
            var strUrl = String()//= strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            
            if strServiceAddImageName.contains("Documents")
            {
                strUrl = strServiceAddImageUrl  + strServiceAddImageName
            }
            else
            {
                strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            }
            
            strUrl  = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            let nsUrl = URL(string: strUrl)
            
           // imgViewServiceContactAddress.load(url: nsUrl! as URL , strImageName: strServiceAddImageName)
            
            
            
           // self.imgView.imageFromServerURL(urlString: strUrl)
            
            //let nsUrl = URL(fileURLWithPath: strUrl)
            
            self.imgViewServiceContactAddress.imageFromServerURL(urlString: strUrl)
        }
        
    }
    func methodForDisable()
    {
        if (strWorkOrderStatus == "Complete" || strWorkOrderStatus == "Completed" || strWorkOrderStatus == "complete" || strWorkOrderStatus == "completed" || strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
        {
            txtViewAccountAlert.isEditable = false
            txtViewServiceDescription.isEditable = false
            txtViewServiceInstruction.isEditable = false
            txtViewSetupInstruction.isEditable = false
            txtViewAttribute.isEditable = false
            txtViewTechnicianComment.isEditable = false
            txtViewTarget.isEditable = false
            
            txtFldPrimaryEmailCustInfo.isHidden = true
            txtFldSecondaryEmailCustInfo.isHidden = true
            txtFldPrimaryPhoneCustInfo.isHidden = true
            txtFldSecondaryPhoneCustInfo.isHidden = true
            txtFldCellNoCustInfo.isHidden = true
            
            btnPrimaryEmailCustInfo.isHidden = false
            btnSecondaryEmailCustInfo.isHidden = false
            btnPrimaryPhoneCustInfo.isHidden = false
            btnSecondaryPhoneCustInfo.isHidden = false
            btnCellNoCustInfo.isHidden = false
            
            btnNoEmail.isEnabled = false
            btnCollectPayment.isEnabled = false
            btnSelectServiceContactAddress.isEnabled = false
            btnSelectBillingContactAddress.isEnabled = false
            btnUploadEditImageServiceContact.isEnabled = false
            // btnServiceStatus.isEnabled = false
            btnResetReason.isEnabled = false
            lblStartTimeValue.isEnabled = false
            btnStartTime.isEnabled = false
            btnReset.isEnabled = false
            
        }
        else
        {
            
            btnReset.isEnabled = true
            txtViewAccountAlert.isEditable = false
            txtViewServiceDescription.isEditable = false
            txtViewServiceInstruction.isEditable = false
            txtViewSetupInstruction.isEditable = false
            txtViewAttribute.isEditable = false
            txtViewTechnicianComment.isEditable = false
            txtViewTarget.isEditable = false
            
            txtFldPrimaryEmailCustInfo.isHidden = false
            txtFldSecondaryEmailCustInfo.isHidden = false
            txtFldPrimaryPhoneCustInfo.isHidden = false
            txtFldSecondaryPhoneCustInfo.isHidden = false
            txtFldCellNoCustInfo.isHidden = false
            
            btnPrimaryEmailCustInfo.isHidden = true
            btnSecondaryEmailCustInfo.isHidden = true
            btnPrimaryPhoneCustInfo.isHidden = true
            btnSecondaryPhoneCustInfo.isHidden = true
            btnCellNoCustInfo.isHidden = true
            
            btnNoEmail.isEnabled = true
            btnCollectPayment.isEnabled = true
            btnSelectServiceContactAddress.isEnabled = true
            btnSelectBillingContactAddress.isEnabled = true
            btnUploadEditImageServiceContact.isEnabled = true
            //  btnServiceStatus.isEnabled = true
            btnResetReason.isEnabled = true
            lblStartTimeValue.isEnabled = true
            btnStartTime.isEnabled = true
            
        }
        txtViewDirectionServiceContact.isEditable = false
    }
    func updateWorkOrderDetail()
    {
        
        
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        //Key Array
        arrOfKeys.add("PrimaryEmail")
        arrOfKeys.add("SecondaryEmail")
        arrOfKeys.add("PrimaryPhone")
        arrOfKeys.add("SecondaryPhone")
        arrOfKeys.add("CellNo")
        //arrOfKeys.add("AccountAlert")
        
        arrOfKeys.add("ServiceFirstName")
        arrOfKeys.add("ServiceMiddleName")
        arrOfKeys.add("ServiceLastName")
        arrOfKeys.add("ServiceMapCode")
        arrOfKeys.add("ServicePrimaryEmail")
        arrOfKeys.add("ServiceSecondaryEmail")
        arrOfKeys.add("ServicePrimaryPhone")
        arrOfKeys.add("ServiceSecondaryPhone")
        arrOfKeys.add("ServicesAddress1")
        arrOfKeys.add("ServiceAddress2")
        arrOfKeys.add("ServiceCity")
        arrOfKeys.add("ServiceState")
        arrOfKeys.add("ServiceZipcode")
        arrOfKeys.add("ServiceCounty")
        arrOfKeys.add("ServiceSchoolDistrict")
        
        arrOfKeys.add("BillingFirstName")
        arrOfKeys.add("BillingMiddleName")
        arrOfKeys.add("BillingLastName")
        arrOfKeys.add("BillingMapCode")
        arrOfKeys.add("BillingPrimaryEmail")
        arrOfKeys.add("BillingSecondaryEmail")
        arrOfKeys.add("BillingPrimaryPhone")
        arrOfKeys.add("BillingSecondaryPhone")
        arrOfKeys.add("BillingAddress1")
        arrOfKeys.add("BillingAddress2")
        arrOfKeys.add("BillingCity")
        arrOfKeys.add("BillingState")
        arrOfKeys.add("BillingZipcode")
        arrOfKeys.add("BillingCounty")
        arrOfKeys.add("BillingSchoolDistrict")
        
        arrOfKeys.add("ServiceDescription")
        arrOfKeys.add("Targets")
        arrOfKeys.add("Atributes")
        arrOfKeys.add("ServiceInstruction")
        
        arrOfKeys.add("SpecialInstruction")
        arrOfKeys.add("TechnicianComment")
        
        arrOfKeys.add("SpecialInstruction")
        arrOfKeys.add("ServiceAddressImagePath")
        arrOfKeys.add("TimeIn")
        arrOfKeys.add("resetId")
        arrOfKeys.add("resetDescription")
        arrOfKeys.add("workorderStatus")
        
        // collect Payment
        arrOfKeys.add("isCollectPayment")
        
        
        //Value Array
        
        arrOfValues.add(txtFldPrimaryEmailCustInfo.text ?? "")
        arrOfValues.add(txtFldSecondaryEmailCustInfo.text ?? "")
        arrOfValues.add(txtFldPrimaryPhoneCustInfo.text ?? "")
        arrOfValues.add(txtFldSecondaryPhoneCustInfo.text ?? "")
        arrOfValues.add(txtFldCellNoCustInfo.text ?? "")
        //arrOfValues.add(txtViewAccountAlert.text ?? "")
        
        //Service POC
        if dictServiceContactData.count > 0
        {
            arrOfValues.add("\(dictServiceContactData.value(forKey: "FirstName") ?? "")")
            arrOfValues.add("\(dictServiceContactData.value(forKey: "MiddleName") ?? "")")
            arrOfValues.add("\(dictServiceContactData.value(forKey: "LastName") ?? "")")
        }
        else
        {
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceFirstName") ?? "")")
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceMiddleName") ?? "")")
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceLastName") ?? "")")
        }
        
        arrOfValues.add(lblMapCodeServiceContact.text ?? "")
        
        arrOfValues.add(lblPrimaryEmailServiceContact.text ?? "")
        arrOfValues.add(lblSecondaryEmailServiceContact.text ?? "")
        arrOfValues.add(lblPrimaryPhoneServiceContact.text ?? "")
        
        arrOfValues.add(lblSecondaryPhoneServiceContact.text ?? "")
        
        
        arrOfValues.add(objWorkorderDetail.value(forKey: "servicesAddress1") ?? "")
        arrOfValues.add(objWorkorderDetail.value(forKey: "serviceAddress2") ?? "")
        arrOfValues.add(objWorkorderDetail.value(forKey: "serviceCity") ?? "")
        arrOfValues.add(objWorkorderDetail.value(forKey: "serviceState") ?? "")
        arrOfValues.add(objWorkorderDetail.value(forKey: "serviceZipcode") ?? "")
        
        arrOfValues.add(lblCountyServiceContact.text ?? "")
        arrOfValues.add(lblSchoolDistrictServiceContact.text ?? "")
        
        
        //Billing POC
        
        if dictBillingContactData.count > 0
        {
            arrOfValues.add("\(dictBillingContactData.value(forKey: "FirstName") ?? "")")
            arrOfValues.add("\(dictBillingContactData.value(forKey: "MiddleName") ?? "")")
            arrOfValues.add("\(dictBillingContactData.value(forKey: "LastName") ?? "")")
        }
        else
        {
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "billingFirstName") ?? "")")
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "billingMiddleName") ?? "")")
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "billingLastName") ?? "")")
        }
        
        
        arrOfValues.add(lblMapCodeBillingContact.text ?? "")
        
        arrOfValues.add(lblPrimaryEmailBillingContact.text ?? "")
        arrOfValues.add(lblSecondaryEmailBillingContact.text ?? "")
        arrOfValues.add(lblPrimaryPhoneBillingContact.text ?? "")
        arrOfValues.add(lblSecondaryPhoneBillingContact.text ?? "")
        
        
        arrOfValues.add(objWorkorderDetail.value(forKey: "billingAddress1") ?? "")
        arrOfValues.add(objWorkorderDetail.value(forKey: "billingAddress2") ?? "")
        arrOfValues.add(objWorkorderDetail.value(forKey: "billingCity") ?? "")
        arrOfValues.add(objWorkorderDetail.value(forKey: "billingState") ?? "")
        arrOfValues.add(objWorkorderDetail.value(forKey: "billingZipcode") ?? "")
        
        
        arrOfValues.add(lblCountyBillingContact.text ?? "")
        arrOfValues.add(lblSchoolDistrictBillingContact.text ?? "")
        
        
        
        arrOfValues.add(txtViewServiceDescription.text ?? "")
        arrOfValues.add(txtViewTarget.text ?? "")
        arrOfValues.add(txtViewAttribute.text ?? "")
        arrOfValues.add(txtViewServiceInstruction.text ?? "")
        
        arrOfValues.add(txtViewServiceInstruction.text ?? "")
        arrOfValues.add(txtViewTechnicianComment.text ?? "")
        
        arrOfValues.add(txtViewSetupInstruction.text ?? "")
        arrOfValues.add(strServiceAddImageName )
        arrOfValues.add(lblStartTimeValue.titleLabel?.text ?? "" )
        
        arrOfValues.add(strResetId)
        arrOfValues.add(txtViewResetReason.text ?? "")
        arrOfValues.add("\(self.strServicesStatus)")
        
        if isCollectPayment {
            
            arrOfValues.add("true")
            
        } else {
            
            arrOfValues.add("false")
            
        }
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            if yesEditedSomething
            {
                
                updateModifyDate()
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    func trimTextField()
    {
        txtFldPrimaryEmailCustInfo.text = txtFldPrimaryEmailCustInfo.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        txtFldSecondaryEmailCustInfo.text = txtFldSecondaryEmailCustInfo.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        txtFldPrimaryPhoneCustInfo.text = txtFldPrimaryPhoneCustInfo.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtFldSecondaryPhoneCustInfo.text = txtFldSecondaryPhoneCustInfo.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
    }
    func tempDict()
    {
        arrData = NSMutableArray()
        
        let objValue = ["Jon","1"]
        let objKey = ["Name","IsActive"]
        
        let dict_ToSend = NSDictionary(objects:objValue, forKeys:objKey as [NSCopying]) as Dictionary
        
        let objValue1 = ["Rock","1"]
        let objKey1 = ["Name","IsActive"]
        
        let dict_ToSend1 = NSDictionary(objects:objValue1, forKeys:objKey1 as [NSCopying]) as Dictionary
        
        let objValue2 = ["Bravo","1"]
        let objKey2 = ["Name","IsActive"]
        
        let dict_ToSend2 = NSDictionary(objects:objValue2, forKeys:objKey2 as [NSCopying]) as Dictionary
        
        arrData.add(dict_ToSend)
        arrData.add(dict_ToSend1)
        arrData.add(dict_ToSend2)
        
        
    }
    func setBorderAtLoad()
    {
        buttonRound(sender: btnSave)
        buttonRound(sender: btnUploadEditImageServiceContact)
        
        showHideCustomerInfo()
        showHideWorkOrderInfo()
        showHideBillingContactAddress()
        showHideServiceContactAddress()
        //showHideUploadImage()
        showHideOther()
        showHideTechnicianComment()
        showHideTimeRange()
        
        setColorBorderForView(item: txtFldPrimaryEmailCustInfo)
        setColorBorderForView(item: txtFldSecondaryEmailCustInfo)
        setColorBorderForView(item: txtFldPrimaryPhoneCustInfo)
        setColorBorderForView(item: txtFldSecondaryPhoneCustInfo)
        setColorBorderForView(item: txtFldCellNoCustInfo)
        
        
        setColorBorderForView(item: txtViewAccountAlert)
        setColorBorderForView(item: txtViewDirectionServiceContact)
        setColorBorderForView(item: txtViewServiceDescription)
        setColorBorderForView(item: txtViewServiceInstruction)
        setColorBorderForView(item: txtViewTarget)
        setColorBorderForView(item: txtViewAttribute)
        setColorBorderForView(item: txtViewSetupInstruction)
        setColorBorderForView(item: txtViewTechnicianComment)
        setColorBorderForView(item: txtViewResetReason)
        
        
        
        setColorBorderForView(item: lblStartTimeValue)
        setColorBorderForView(item: btnStartTime)
        setColorBorderForView(item: btnSelectBillingContactAddress)
        setColorBorderForView(item: btnSelectServiceContactAddress)
        setColorBorderForView(item: btnResetReason)
        
        setColorBorderForView(item: btnReset)
        // setColorBorderForView(item: btnServiceStatus)
        //setColorBorderForView(item: btnSave)
        
        setColorBorderForView(item: btnPrimaryEmailCustInfo)
        setColorBorderForView(item: btnSecondaryEmailCustInfo)
        setColorBorderForView(item: btnPrimaryPhoneCustInfo)
        setColorBorderForView(item: btnSecondaryPhoneCustInfo)
        setColorBorderForView(item: btnCellNoCustInfo)
        
        
        txtFldCellNoCustInfo.setLeftPaddingPoints(5)
        txtFldPrimaryEmailCustInfo.setLeftPaddingPoints(5)
        txtFldSecondaryEmailCustInfo.setLeftPaddingPoints(5)
        txtFldPrimaryPhoneCustInfo.setLeftPaddingPoints(5)
        txtFldSecondaryPhoneCustInfo.setLeftPaddingPoints(5)
        
        /* btnSelectBillingContactAddress.layer.borderWidth = 0.0
         btnSelectServiceContactAddress.layer.borderWidth = 0.0
         btnServiceStatus.layer.borderWidth = 0.0
         btnResetReason.layer.borderWidth = 0.0*/
        
        
        
    }
    func setColorBorder(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    func setColorBorderForView(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanicaliPhone") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//                testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
                testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strModuleType = "ServicePestFlow"
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Reset" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "reset"{
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    func showHideCustomerInfo()
    {
        if btnShowCustomerInfo.currentImage == UIImage(named: "hide") //show_icon
        {
            btnShowCustomerInfo.setImage(UIImage(named: "show_icon"), for: .normal)
            const_ViewCustomerInfo_H.constant = 0
            viewCustomerInfo.isHidden = true
            
        }
        else
        {
            btnShowCustomerInfo.setImage(UIImage(named: "hide"), for: .normal)
            const_ViewCustomerInfo_H.constant = 555
            viewCustomerInfo.isHidden = false
        }
        
    }
    func showHideWorkOrderInfo()
    {
        if btnShowWorkOrderInfo.currentImage == UIImage(named: "hide") //show_icon
        {
            btnShowWorkOrderInfo.setImage(UIImage(named: "show_icon"), for: .normal)
            const_ViewWorkOrderInfo_H.constant = 0
            viewWorkOrderInfo.isHidden = true
            
        }
        else
        {
            btnShowWorkOrderInfo.setImage(UIImage(named: "hide"), for: .normal)
            const_ViewWorkOrderInfo_H.constant = 385
            viewWorkOrderInfo.isHidden = false
            
        }
        
    }
    func showHideBillingContactAddress()
    {
        if btnShowBillingContactAddress.currentImage == UIImage(named: "hide") //show_icon
        {
            btnShowBillingContactAddress.setImage(UIImage(named: "show_icon"), for: .normal)
            constViewBillingContactAddress_H.constant = 0
            viewBillingContactAddress.isHidden = true
            
        }
        else
        {
            btnShowBillingContactAddress.setImage(UIImage(named: "hide"), for: .normal)
            constViewBillingContactAddress_H.constant = 395
            
            viewBillingContactAddress.isHidden = false
            
        }
        
    }
    func showHideServiceContactAddress()
    {
        if btnShowServiceContactAddress.currentImage == UIImage(named: "hide") //show_icon
        {
            btnShowServiceContactAddress.setImage(UIImage(named: "show_icon"), for: .normal)
            constViewServiceContactAddress_H.constant = 0
            viewServiceContactAddress.isHidden = true
            
            
        }
        else
        {
            btnShowServiceContactAddress.setImage(UIImage(named: "hide"), for: .normal)
            constViewServiceContactAddress_H.constant = 601
            viewServiceContactAddress.isHidden = false
            
            
        }
        
    }
    func showHideUploadImage()
    {
        if btnShowUploadImage.currentImage == UIImage(named: "hide") //show_icon
        {
            btnShowUploadImage.setImage(UIImage(named: "show_icon"), for: .normal)
            constViewUploadImage_H.constant = 0
            viewUploadImage.isHidden = true
            
        }
        else
        {
            btnShowUploadImage.setImage(UIImage(named: "hide"), for: .normal)
            constViewUploadImage_H.constant = 100
            viewUploadImage.isHidden = false
            
        }
    }
    func showHideOther()
    {
        if btnShowViewOther.currentImage == UIImage(named: "hide") //show_icon
        {
            btnShowViewOther.setImage(UIImage(named: "show_icon"), for: .normal)
            const_ViewOther_H.constant = 0
            viewOther.isHidden = true
            
        }
        else
        {
            btnShowViewOther.setImage(UIImage(named: "hide"), for: .normal)
            const_ViewOther_H.constant = 685
            viewOther.isHidden = false
            
        }
    }
    func showHideTechnicianComment()
    {
        if btnShowViewTechnicianComment.currentImage == UIImage(named: "hide") //show_icon
        {
            btnShowViewTechnicianComment.setImage(UIImage(named: "show_icon"), for: .normal)
            constViewTechnicianComment_H.constant = 0
            viewTechnicianComment.isHidden = true
            viewResetReason.isHidden = true
            
        }
        else
        {
            btnShowViewTechnicianComment.setImage(UIImage(named: "hide"), for: .normal)
            
            constViewTechnicianComment_H.constant = 120
            //  showHideResetReason()
            viewTechnicianComment.isHidden = false
            
        }
    }
    func showHideResetReason()
    {
        let strServicStatus = "\(self.strServicesStatus)"
        
        if strServicStatus == "Reset"
        {
            constViewResetReason.constant = 190
            viewResetReason.isHidden = false
            constViewTechnicianComment_H.constant = 391
        }
        else
        {
            constViewResetReason.constant = 0
            viewResetReason.isHidden = true
            constViewTechnicianComment_H.constant = 391 - 190
        }
        
    }
    func showHideTimeRange()
    {
        if btnShowTimeRange.currentImage == UIImage(named: "hide") //show_icon
        {
            btnShowTimeRange.setImage(UIImage(named: "show_icon"), for: .normal)
            const_ViewTimeRange_H.constant = 0 + 55
            //viewTimeRange.isHidden = true
            viewTimeRange.isHidden = false
            
        }
        else
        {
            btnShowTimeRange.setImage(UIImage(named: "hide"), for: .normal)
            const_ViewTimeRange_H.constant = 55
            viewTimeRange.isHidden = false
            
        }
    }
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            //vc.handleSelection = self
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    func isValidEmail(checkString : String) -> Bool
    {
        /* var stricterFilter = Bool()
         stricterFilter = false
         let stricterFilterString = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$" as String
         
         let laxString = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$" as String
         
         let emailRegex = stricterFilter ? stricterFilterString : laxString
         
         var emailTest = NSPredicate()
         
         emailTest = NSPredicate(format: "SELF MATCHES = %@", emailRegex)
         
         return emailTest.evaluate(with: checkString)*/
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: checkString)
        
        
    }
    func alertForPrimaryEmailSaving()
    {
        
    }
    /*
     
     -(void)alertForPrimaryEmailSaving{
     
     [viewBackGround removeFromSuperview];
     [tblData removeFromSuperview];
     
     NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
     
     UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Email-Id"
     message: strMessage
     preferredStyle:UIAlertControllerStyleAlert];
     [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
     textField.placeholder = @"Enter Email-Id Here...";
     textField.tag=7;
     textField.delegate=self;
     textField.textColor = [UIColor blackColor];
     textField.clearButtonMode = UITextFieldViewModeWhileEditing;
     textField.borderStyle = UITextBorderStyleRoundedRect;
     textField.keyboardType=UIKeyboardTypeDefault;
     }];
     //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
     //        textField.placeholder = @"password";
     //        textField.textColor = [UIColor blueColor];
     //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
     //        textField.borderStyle = UITextBorderStyleRoundedRect;
     //        textField.secureTextEntry = YES;
     //    }];
     [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
     NSArray * textfields = alertController.textFields;
     UITextField * txtHistoricalDays = textfields[0];
     if (txtHistoricalDays.text.length>0) {
     
     txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
     
     NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
     NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
     
     if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
     {
     
     [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
     
     }else if ([txtHistoricalDays.text containsString:@" "])
     {
     
     [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
     
     }
     else{
     
     if ([arrOfPrimaryEmailNew containsObject:txtHistoricalDays.text]) {
     
     [self performSelector:@selector(AlertViewForAlreadyExitPrimaryEmail) withObject:nil afterDelay:0.2];
     
     } else {
     
     [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
     isNoEmail=NO;
     [arrOfPrimaryEmailNew addObject:txtHistoricalDays.text];
     
     [arrOfPrimaryEmailAlreadySaved addObject:txtHistoricalDays.text];
     
     NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
     
     [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
     
     [self tablViewForPrimaryEmailIds];
     
     }
     }
     
     } else {
     
     [self performSelector:@selector(AlertViewForEmptyPrimaryEmail) withObject:nil afterDelay:0.2];
     
     }
     }]];
     [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
     
     [self tablViewForPrimaryEmailIds];
     
     }]];
     [self presentViewController:alertController animated:YES completion:nil];
     
     }
     */
    
    
    
    
    
    
}

// MARK: --------------------- Tableview Delegate --------------

extension GeneralInfoVC : CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int)
    {
        if tag == 43
        {
            yesEditedSomething = true
            dictBillingContactData = dictData
            strBillingPocId = "\((dictBillingContactData.value(forKey: "ContactId") ?? ""))"
            
            btnSelectBillingContactAddress.setTitle(dictData["Name"] as? String, for: .normal)
            
            let strName = "\(dictBillingContactData.value(forKey: "FirstName") ?? "")" + " " + "\(dictBillingContactData.value(forKey: "MiddleName") ?? "")" + " " + "\(dictBillingContactData.value(forKey: "LastName") ?? "")"
            
            btnSelectBillingContactAddress.setTitle(strName, for: .normal)
            lblContactNameBillingContact.text = strName
            
            lblPrimaryEmailBillingContact.text = "\(dictBillingContactData.value(forKey: "PrimaryEmail") ?? "") "
            lblSecondaryEmailBillingContact.text = "\(dictBillingContactData.value(forKey: "SecondaryEmail") ?? "") "
            lblPrimaryPhoneBillingContact.text = "\(dictBillingContactData.value(forKey: "PrimaryPhone") ?? "") "
            lblSecondaryPhoneBillingContact.text = "\(dictBillingContactData.value(forKey: "SecondaryPhone") ?? "") "
            
            //btnAddressBillingContact.setTitle(global.strCombinedAddressBilling(dictBillingContactData), for: .normal)
            //btnAddressBillingContact.setTitle( global.strCombinedAddressBilling(for: objWorkorderDetail), for: .normal)
            
            
            
            
        }
        else if tag == 40
        {
            yesEditedSomething = true
            dictServiceContactData = dictData
            
            //btnSelectServiceContactAddress.setTitle(dictData["Name"] as? String, for: .normal)
            strServicePocId = "\((dictBillingContactData.value(forKey: "ContactId") ?? ""))"
            
            
            btnSelectServiceContactAddress.setTitle(dictData["Name"] as? String, for: .normal)
            
            let strName = "\(dictServiceContactData.value(forKey: "FirstName") ?? "") " + " " + "\(dictServiceContactData.value(forKey: "MiddleName") ?? "") " + " " +  "\(dictServiceContactData.value(forKey: "LastName") ?? "")"
            
            btnSelectServiceContactAddress.setTitle(strName, for: .normal)
            
            lblContactNameServiceContact.text = strName
            
            
            lblPrimaryEmailServiceContact.text = "\(dictBillingContactData.value(forKey: "PrimaryEmail") ?? "") "
            lblSecondaryEmailServiceContact.text = "\(dictServiceContactData.value(forKey: "SecondaryEmail") ?? "") "
            lblPrimaryPhoneServiceContact.text = "\(dictServiceContactData.value(forKey: "PrimaryPhone") ?? "") "
            lblSecondaryPhoneServiceContact.text = "\(dictServiceContactData.value(forKey: "SecondaryPhone") ?? "") "
            
            //btnAddressServiceContact.setTitle(global.strCombinedAddressBilling(dictServiceContactData), for: .normal)
            //btnAddressServiceContact.setTitle( global.strCombinedAddressService(for: objWorkorderDetail), for: .normal)
            
        }
        else if tag == 41
        {
            yesEditedSomething = true
            btnResetReason.setTitle(dictData["ResetReason"] as? String, for: .normal)
            strResetId = "\(dictData.value(forKey: "ResetId") ?? "")"
            
            isResetReason = true
            
        }
        else if tag == 42
        {
            // btnServiceStatus.setTitle(dictData["Name"] as? String, for: .normal)
            // btnResetReason.setTitle(dictData["Name"] as? String, for: .normal)
        }
        
    }
    
    
    
}

// MARK: --------------------- DatePicker Delegate --------------

extension GeneralInfoVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if tag == 10
        {
            
            lblStartTimeValue.setTitle(strDate, for: .normal)
            //strFromTimeNonBillable = strDate
            yesEditedSomething = true
            
        }
    }
    
    
}

// MARK: --------------------- ImageView Delegate --------------


extension GeneralInfoVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            imgViewServiceContactAddress.contentMode = .scaleAspectFit
            imgViewServiceContactAddress.image = pickedImage
            //strServiceAddImageName = "\\Documents\\UploadImages\\Img"  + "ServiceAddImg" + "\(getUniqueValueForId())" + ".jpg"
            
            strServiceAddImageName = "\\Documents\\CustomerAddressImages\\Img"  + "ServiceAddImg" + "\(getUniqueValueForId())" + ".jpg" // UploadImages
            
            saveImageDocumentDirectory(strFileName: strServiceAddImageName, image: pickedImage)
            yesEditedSomething = true
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
// MARK: --------------------- Text Field Delegate --------------

extension GeneralInfoVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if ( textField == txtFldPrimaryEmailCustInfo || textField == txtFldSecondaryEmailCustInfo )
        {
            yesEditedSomething = true
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 200)
            
        }
            
        else if ( textField == txtFldPrimaryPhoneCustInfo || textField == txtFldSecondaryPhoneCustInfo || textField == txtFldCellNoCustInfo )
        {
            yesEditedSomething = true
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            
        }
        else
        {
            
            return true
            
        }
    }
    
}
// MARK: --------------------- Text View Delegate --------------

extension GeneralInfoVC : UITextViewDelegate
{
    
    
}
