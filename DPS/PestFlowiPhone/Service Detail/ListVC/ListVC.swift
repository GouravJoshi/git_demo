//
//  ListVC.swift
//  DPS
//
//  Created by Saavan Patidar on 12/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import AVKit

class ListVC: UIViewController{

    // MARK: - ----IBOutlet
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnAdd: UIButton!

    // MARK: - ----Variable
    
    var strWoId = NSString ()
    var aryList = NSMutableArray()
    var strTitle = String()
    var strType = String()
    var imagePicker = UIImagePickerController()
    var objWorkorderDetail = NSManagedObject()
    var arrOfAllData = NSArray()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var strAssociationType = String ()
    var strFrom = String ()
    var strConditionName = String ()
    var strConditionId = String ()
    var strURL = String()
    var strFromDeviceName = String()
    var strDeviceId = String ()
    var strMobileDeviceId = String ()
    var arrOfFavProducts = [[String: Any]]()
    
    // MARK: - ----Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        getFavProductsList()

        // Do any additional setup after loading the view.
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        if strAddressAreaName.count > 0 {
            
            lblTitle.text = strAddressAreaName
            
        } else {
            
            lblTitle.text = enumArea
            
        }
        
        if strFromDeviceName.count > 0 {
            
            lblTitle.text = strFromDeviceName
            
        }
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            btnAdd.isHidden = true
            
        }
        
        if strFrom == "Condition"{

            lblTitle.text = strConditionName
            
        }
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
            
            strURL = "\(value)"
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.fetchDataFrmDB()
        
        // AudioNameService  yesAudio
        
        if (nsud.value(forKey: "yesAudio") != nil) {
            
            let isAudio = nsud.bool(forKey: "yesAudio")
            
            if isAudio {
                
                //let audioName = nsud.value(forKey: "AudioNameService")
                
            }
            
        }
        
        // Dismiss ANd goto Invoice View
        if (nsud.value(forKey: "dismissView") != nil) {
            
            let isDismissView = nsud.bool(forKey: "dismissView")
            
            if isDismissView {
                
                self.back()
                
            }
            
        }
        
    }
    

    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }

    @IBAction func action_Add(_ sender: Any) {
        
        if (strType == enumMaterial) {
            
            self.goToMaterialView(strForUpdate: "No" , strServiceProductId: "" ,strMobileServiceProductId: "", isFav: false, isWeCameToViewOnly: false)
            
        } else if (strType == enumPest){
            
            self.goToPestsView(strForUpdate: "No", strServiceAddressPestId: "", strMobileServiceAddressPestId: "")
            
        } else if (strType == enumCondition){
            
            self.goToConditionView(strForUpdate: "No", strsaConduciveConditionId: "", strMobileSAConduciveConditionId: "")

        } else if (strType == enumComment){
            
            self.goToCommentView(strForUpdate: "No", strServiceCommentId: "", strMobileServiceCommentId: "")
            
        } else {
            
            self.goToSelectDocument()
            
        }
        
    }
    
    
    @objc func btnFavAction(sender: UIButton!){
        
        var strIsFav = ""
        let objArea = arrOfAllData[sender.tag] as! NSManagedObject
        
        let strProductMasterId = "\(objArea.value(forKey: "productMasterId") ?? "0")"
        
        let index = IndexPath(item: sender.tag, section: 0)
        let cell = self.tvlist.cellForRow(at: index) as! MaterialListCell
        
        if cell.btnFav.currentImage == UIImage(named: "fav"){
            cell.btnFav.setImage(UIImage(named: "unfav"), for: .normal)
            strIsFav = "false"
            
        }
        else if cell.btnFav.currentImage == UIImage(named: "unfav"){
            cell.btnFav.setImage(UIImage(named: "fav"), for: .normal)
            strIsFav = "true"
        }
        
        callWebServiceToMarkProductFavourite(strProductMasterId: strProductMasterId, strFav: strIsFav)
    }
    

    // MARK: - -----------------------------------APICalling -----------------------------------
    
    func callWebServiceToMarkProductFavourite(strProductMasterId: String, strFav: String)
    {
        let loaderLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loaderLocal, animated: true, completion: nil)
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.markProdufctFav
        
        
//        {
//        "WorkOrderProductFavoriteExtDcs": [
//        {
//        "WorkOrderProductFavoriteId": "",
//        "ServiceProductId": "11246",
//        "EmployeeId": "143035",
//        "IsFavorite": "true",
//        "IsActive": "true",
//        "CompanyId": "3",
//        "WorkOrderId": "822613"
//        }
//        ]
//        }
//
        
        let innerParam = ["WorkOrderProductFavoriteId":"",
                     "CompanyId": strCompanyId,
                     "EmployeeId": strEmployeeId,
                     "ServiceProductId": strProductMasterId,
                     "IsFavorite" : strFav,
                     "IsActive": "true",
                     "WorkOrderId" : strWoId
                     ] as [String: Any]
    
        let param = ["WorkOrderProductFavoriteExtDcs" : [innerParam]] as [String: Any]
            
        WebService.postRequestWithHeaders(dictJson: param as NSDictionary , url: strURL, responseStringComing: "") { (response, status) in
            
            loaderLocal.dismiss(animated: false) {

                self.tvlist.reloadData()
                self.getFavProductsList()
                self.tvlist.reloadData()
                if strFav == "true"{
                    self.view.makeToast("Product marked favourite successfully")
                }
                else{
                    self.view.makeToast("Product marked unfavourite successfully")
                }
            }
        }
        
    }
    
    func getFavProductsList(){
            
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        let strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"

        let strURL = String(format:  "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.getFavProductList , strCompanyId, strEmployeeId)
            
            
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
                if(status)
                {
                    self.arrOfFavProducts.removeAll()
                    self.arrOfFavProducts = response.value(forKey: "data") as! [[String:Any]]
                    tvlist.reloadData()
                }
            }
    }
    // MARK: - -----------------------------------Functions -----------------------------------

    func fetchDataFrmDB()  {
        
        if (strType == enumMaterial) {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType))
            
            if strAssociationType == "Device"{
                
                var idLocal = ""
                
                if strDeviceId.count > 0 && strDeviceId != "0" {

                    idLocal = strDeviceId
                    
                }else{
                    
                    idLocal = strMobileDeviceId
                    
                }
                
                arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, idLocal,Global().getCompanyKey(), strAssociationType))
                
            }
            
        } else if (strType == enumPest){
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType))
            
            if strAssociationType == "Device"{
                
                var idLocal = ""
                
                if strDeviceId.count > 0 && strDeviceId != "0" {
                    
                    idLocal = strDeviceId
                    
                }else{
                    
                    idLocal = strMobileDeviceId
                    
                }
                
                arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, idLocal,Global().getCompanyKey(), strAssociationType))

            }
            
        } else if (strType == enumCondition){
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@", strWoId, strAddressAreaId,Global().getCompanyKey()))
            
        } else if (strType == enumComment){
            
            if strFrom == "Condition"{
                
                arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditionComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@ && companyKey == %@", strWoId, strConditionId,Global().getCompanyKey()))

            }
            else{
                
                arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType))
                
                if strAssociationType == "Device"{
                    
                    var idLocal = ""
                    
                    if strDeviceId.count > 0 && strDeviceId != "0" {
                        
                        idLocal = strDeviceId
                        
                    }else{
                        
                        idLocal = strMobileDeviceId
                        
                    }
                    
                    arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, idLocal,Global().getCompanyKey(), strAssociationType))

                }
                
            }
            
        } else {
            
            if strFrom == "Condition" {
                
                arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@ && companyKey == %@", strWoId, strConditionId,Global().getCompanyKey()))

                
            }else{
                
                arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@", strWoId, strAddressAreaId,Global().getCompanyKey()))
                
            }
            
        }
        
        tvlist.reloadData()
        
    }
    
    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func goToAreaView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddAreaVC") as? AddAreaVC
        testController?.strWoId = strWoId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAddDeviceView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDeviceVC") as? AddDeviceVC
        testController?.strWoId = strWoId
        nsud.setValue("true", forKey: "CameToAddDevice")
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToMaterialView(strForUpdate:String , strServiceProductId:String , strMobileServiceProductId:String, isFav: Bool, isWeCameToViewOnly: Bool)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddMaterialsVC") as? AddMaterialsVC
        testController?.strWoId = strWoId
        testController?.strForUpdate = strForUpdate
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strAssociationType = strAssociationType
        testController?.strServiceProductId = strServiceProductId
        testController?.strMobileServiceProductId = strMobileServiceProductId
        testController?.strDeviceId = strDeviceId
        testController?.strMobileDeviceId = strMobileDeviceId
        testController?.isWeCameToViewOnly = isWeCameToViewOnly
        testController?.isFav = isFav
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToPestsView(strForUpdate:String , strServiceAddressPestId:String , strMobileServiceAddressPestId:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddPestsVC") as? AddPestsVC
        testController?.strWoId = strWoId
        testController?.strForUpdate = strForUpdate
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strAssociationType = strAssociationType
        testController?.strServiceAddressPestId = strServiceAddressPestId
        testController?.strMobileServiceAddressPestId = strMobileServiceAddressPestId
        testController?.strDeviceId = strDeviceId
        testController?.strMobileDeviceId = strMobileDeviceId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToConditionView(strForUpdate:String , strsaConduciveConditionId:String , strMobileSAConduciveConditionId:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddConditionsVC") as? AddConditionsVC
        testController?.strWoId = strWoId
        testController?.strForUpdate = strForUpdate
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strAssociationType = strAssociationType
        testController?.strServiceConditionId = strsaConduciveConditionId
        testController?.strMobileServiceConditionId = strMobileSAConduciveConditionId
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCommentView(strForUpdate:String , strServiceCommentId:String , strMobileServiceCommentId:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddCommentVC") as? AddCommentVC
        testController?.strWoId = strWoId
        testController?.strForUpdate = strForUpdate
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strAssociationType = strAssociationType
        testController?.strServiceCommentId = strServiceCommentId
        testController?.strMobileServiceCommentId = strMobileServiceCommentId
        if strFrom == "Condition" {
            
            testController?.strFrom = strFrom
            testController?.strConditionId = strConditionId
            testController?.strConditionCommentId = strServiceCommentId
            testController?.strMobileConditionCommentId = strMobileServiceCommentId
            
        }
        testController?.strDeviceId = strDeviceId
        testController?.strMobileDeviceId = strMobileDeviceId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToRecordView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
        testController?.strFromWhere = "ServiceInvoice"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToSelectDocument()  {
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDocumentVC") as? AddDocumentVC
        testController?.strWoId = strWoId as String
        testController?.strFrom = strFrom
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strConditionId = strConditionId
        testController?.strAssociationType = strAssociationType
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToImageView(image : UIImage) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
        testController!.img = image
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToPdfView(strPdfName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func playAudioo(strAudioName : String) {
       
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }

}
// MARK: -


// MARK: - ----------------UITableViewDelegate

extension ListVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOfAllData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if (strType == enumMaterial) {
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "MaterialListCell", for: indexPath as IndexPath) as! MaterialListCell
            
            let objArea = arrOfAllData[indexPath.row] as! NSManagedObject

            cell.lblTitle.text = "\(objArea.value(forKey: "serviceProductName") ?? "N/A")"
            
            let strSubTitle = "Qty - \(objArea.value(forKey: "quantity") ?? "")  " + "EPA Reg. # - \(objArea.value(forKey: "ePARegNo") ?? "") "
            
            cell.lblSubTitle.text = strSubTitle
            
            if arrOfFavProducts.count > 0{
                for i in 0..<arrOfFavProducts.count{
                    let strFavProductId = arrOfFavProducts[i]["ServiceProductId"] as? NSNumber ?? 0
                    let strProductMasterId = "\(objArea.value(forKey: "productMasterId") ?? "0")"
                    if String(describing: strFavProductId) == strProductMasterId {
                        let strIsFav = arrOfFavProducts[i]["IsFavorite"] as? Bool ?? false
                        if strIsFav == true{
                        cell.btnFav.setImage(UIImage(named: "fav"), for: .normal)
                        break
                        }
                        else{
                            cell.btnFav.setImage(UIImage(named: "unfav"), for: .normal)
                            
                        }
                    }
                    else{
                        cell.btnFav.setImage(UIImage(named: "unfav"), for: .normal)
                        
                    }
                }
            }
            cell.btnFav.tag = indexPath.row
            cell.btnFav.addTarget(self, action: #selector(btnFavAction), for: .touchUpInside)

            
            return cell

        } else if (strType == enumPest){
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "PestsListCell", for: indexPath as IndexPath) as! PestsListCell

            let objArea = arrOfAllData[indexPath.row] as! NSManagedObject
            
            cell.lblTitle.text = "\(objArea.value(forKey: "targetName") ?? "N/A")"
            
            let pestStatus = "\(objArea.value(forKey: "pestStatus") ?? "")"
            var strSubTitle = String()
            
            if pestStatus == "1" {
                
                strSubTitle = "\(objArea.value(forKey: "quantity") ?? "")  Captured"

            } else if pestStatus == "2" {
                
                strSubTitle = "\(objArea.value(forKey: "quantity") ?? "")  Sighted"
                
            } else{
                
                strSubTitle = "Evidence: \(objArea.value(forKey: "evidence") ?? "")"

            }
            
            cell.lblSubTitle.text = strSubTitle

            return cell
            
        } else if (strType == enumCondition){
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ConditionsListCell", for: indexPath as IndexPath) as! ConditionsListCell
            
            let dict = arrOfAllData.object(at: indexPath.row) as! NSManagedObject
            
            cell.lblTitle.text = "\(dict.value(forKey: "sAConduciveConditionName") ?? "N/A")"
            
            let dateFormat = changeStringDateToGivenFormat(strDate: "\(dict.value(forKey: "strCreatedDate") ?? "N/A")", strRequiredFormat: "MM/dd/yyyy")
            
            cell.lblDate.text = "\(dict.value(forKey: "strCreatedDate") ?? "N/A")"
            cell.lblDate.text = dateFormat

            
            if("\(dict.value(forKey: "conditionStatus")!)" == "1")
            {
                cell.lblStatus.text =   "Status: Open"
            }
            else if("\(dict.value(forKey: "conditionStatus")!)" == "2")
            {
                cell.lblStatus.text = "Status: Void"
            }
            else if("\(dict.value(forKey: "conditionStatus")!)" == "3")
            {
                cell.lblStatus.text = "Status: Resolve"
            }
            
            if("\(dict.value(forKey: "priorityId")!)" == "1")
            {
                cell.lblPriority.text = "Low"
                
                cell.lblPriority.textColor = UIColor.green
                
            }
            else if("\(dict.value(forKey: "priorityId")!)" == "2")
            {
                cell.lblPriority.text = "Medium"
                
                cell.lblPriority.textColor = UIColor.orange

            }
            else if("\(dict.value(forKey: "priorityId")!)" == "3")
            {
                
                cell.lblPriority.text = "High"
                
                cell.lblPriority.textColor = UIColor.red

            }
            
            return cell
            
        } else if (strType == enumComment){
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "CommentListCell", for: indexPath as IndexPath) as! CommentListCell
            //serviceCommentDetail
            let objArea = arrOfAllData[indexPath.row] as! NSManagedObject
            
            if strFrom == "Condition" {
                
                cell.lblTitle.text = "\(objArea.value(forKey: "commentDetail") ?? "N/A")"

            }else{
                
                cell.lblTitle.text = "\(objArea.value(forKey: "serviceCommentDetail") ?? "N/A")"
                
            }
            //cell.lblTitle.text = "\(objArea.value(forKey: "serviceCommentDetail") ?? "N/A")"
            
            return cell
            
        } else {
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "DocumentListCell", for: indexPath as IndexPath) as! DocumentListCell
            
            let objArea = arrOfAllData[indexPath.row] as! NSManagedObject
            
            var strDocPathLocal = String()
            var strDocTypeLocal = String()
            
            if strFrom == "Condition" {
                
                cell.lblTitle.text = "\(objArea.value(forKey: "documnetTitle") ?? "N/A")"
                strDocPathLocal = "\(objArea.value(forKey: "documentPath") ?? "")"
                strDocTypeLocal = "\(objArea.value(forKey: "documentType") ?? "")"
                
            }else{
                
                cell.lblTitle.text = "\(objArea.value(forKey: "serviceAddressDocName") ?? "N/A")"
                strDocPathLocal = "\(objArea.value(forKey: "serviceAddressDocPath") ?? "")"
                strDocTypeLocal = "\(objArea.value(forKey: "documentType") ?? "")"
                
            }
//            strDocTypeLocal = "audio"
//            strDocTypeLocal = "image"
//            strDocTypeLocal = "pdf"
            
            if strDocTypeLocal == "image" {
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)

                if image != nil && strImagePath?.count != 0 && isImageExists! {
                    
                    let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                    
                    let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                    
                    cell.imgView.image = image

                    // kch nhi krna
                    
                }else {
                    
                    strURL = strURL + "\(strDocPathLocal)"
                    
                    strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                    
                    let image: UIImage = UIImage(named: "NoImage.jpg")!
                    
                    cell.imgView.image = image
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string:self.strURL)
                        let data = try? Data(contentsOf: url!)
                        
                        if data != nil && (data?.count)! > 0 {
                            
                            let image: UIImage = UIImage(data: data!)!
                            
                            DispatchQueue.main.async {
                                
                                saveImageDocumentDirectory(strFileName: strImagePath! , image: image)
                                
                                cell.imgView.image = image
                                
                            }}
                        
                    }
                    
                }

                
            } else if strDocTypeLocal == "pdf" {
                
                let image: UIImage = UIImage(named: "exam")!
                
                cell.imgView.image = image
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                
                if isImageExists!  {
                    
                    // kch nhi krna
                    
                }else {
                    
                    strURL = strURL + "\(strDocPathLocal)"
                    
                    strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string:self.strURL)
                        let data = try? Data(contentsOf: url!)
                        
                        if data != nil && (data?.count)! > 0 {
                            
                            DispatchQueue.main.async {
                                
                                savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                                
                            }}
                        
                    }
                    
                }
                
            } else if strDocTypeLocal == "audio" {
                
                let image: UIImage = UIImage(named: "audio")!
                
                cell.imgView.image = image
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                
                if isImageExists!  {
                    
                    // kch nhi krna
                    
                }else {
                    
                    strURL = strURL + "\(strDocPathLocal)"
                    
                    strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string:self.strURL)
                        let data = try? Data(contentsOf: url!)
                        
                        if data != nil && (data?.count)! > 0 {
                            
                            DispatchQueue.main.async {
                                
                                savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                                
                            }}
                        
                    }
                    
                }
                
            }else if strDocTypeLocal == "video" {
                
                let image: UIImage = UIImage(named: "video")!
                
                cell.imgView.image = image
                
            }
            
            else {
                
                strDocTypeLocal = "image"
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)

                if image != nil && strImagePath?.count != 0 && isImageExists! {
                    
                    let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                    
                    let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                    
                    cell.imgView.image = image

                    // kch nhi krna
                    
                }else {
                    
                    strURL = strURL + "\(strDocPathLocal)"
                    
                    strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                    
                    let image: UIImage = UIImage(named: "NoImage.jpg")!
                    
                    cell.imgView.image = image
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string:self.strURL)
                        let data = try? Data(contentsOf: url!)
                        
                        if data != nil && (data?.count)! > 0 {
                            
                            let image: UIImage = UIImage(data: data!)!
                            
                            DispatchQueue.main.async {
                                
                                saveImageDocumentDirectory(strFileName: strImagePath! , image: image)
                                
                                cell.imgView.image = image
                                
                            }}
                        
                    }
                    
                }

                
            }
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (strType == enumMaterial){
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                
            }else{
                
            let objArea = arrOfAllData[indexPath.row] as! NSManagedObject
            let strServiceProductIdLocal = objArea.value(forKey: "serviceProductId") as! String
            let strMobileServiceProductIdLocal = objArea.value(forKey: "mobileServiceProductId") as! String
            //let idd = strServiceProductIdLocal.count > 0 ? strServiceProductIdLocal : strMobileServiceProductIdLocal
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "MaterialListCell", for: indexPath as IndexPath) as! MaterialListCell
                
                var isFav = Bool()
                if cell.btnFav.currentImage == UIImage(named: "fav"){
                    
                    isFav = true
                    
                }
                else if cell.btnFav.currentImage == UIImage(named: "unfav"){
                    
                    isFav = false
                }
                self.goToMaterialView(strForUpdate: "Yes", strServiceProductId: strServiceProductIdLocal, strMobileServiceProductId: strMobileServiceProductIdLocal, isFav: isFav, isWeCameToViewOnly: true)
                
            }
            
        } else if (strType == enumPest){
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                
            }else{
                
            let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
            let strServiceProductIdLocal = objArea.value(forKey: "serviceAddressPestId") as! String
            let strMobileServiceProductIdLocal = objArea.value(forKey: "mobileServiceAddressPestId") as! String
            
            self.goToPestsView(strForUpdate: "Yes", strServiceAddressPestId: strServiceProductIdLocal, strMobileServiceAddressPestId: strMobileServiceProductIdLocal)
                
            }
            
        } else if (strType == enumCondition){
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                
            }else{
                
                let objArea = arrOfAllData[indexPath.row] as! NSManagedObject
                let strsaConduciveConditionIdLocal = objArea.value(forKey: "sAConduciveConditionId") as! String
                let strMobileSAConduciveConditionIdLocal = objArea.value(forKey: "mobileSAConduciveConditionId") as! String
                
                self.goToConditionView(strForUpdate: "Yes", strsaConduciveConditionId: strsaConduciveConditionIdLocal, strMobileSAConduciveConditionId: strMobileSAConduciveConditionIdLocal)
                
                
                
            }
            
        }else if (strType == enumComment){
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                
            }else{
                
            let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
            var strServiceCommentIdLocal =  String()
            var strMobileServiceCommentIdLocal = String()
            
            if strFrom == "Condition"{
            
                 strServiceCommentIdLocal = objArea.value(forKey: "sAConditionCommentId") as! String
                 strMobileServiceCommentIdLocal = objArea.value(forKey: "sAConditionCommentId") as! String
            
            }else{
                
                 strServiceCommentIdLocal = objArea.value(forKey: "serviceCommentId") as! String
                 strMobileServiceCommentIdLocal = objArea.value(forKey: "mobileServiceCommentId") as! String
                
            }
                
            self.goToCommentView(strForUpdate: "Yes", strServiceCommentId: strServiceCommentIdLocal, strMobileServiceCommentId: strMobileServiceCommentIdLocal)
                
            }
            
        } else {
            
            //documentType
            
            var strDocPathLocal = String()
            var strDocTypeLocal = String()
            
            if self.strFrom == "Condition" {
                
                // documentPath
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                 strDocPathLocal = "\(objArea.value(forKey: "documentPath") ?? "")"
                 strDocTypeLocal = "\(objArea.value(forKey: "documentType") ?? "")"

            }else{
                
                // serviceAddressDocPath
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                 strDocPathLocal = "\(objArea.value(forKey: "serviceAddressDocPath") ?? "")"
                 strDocTypeLocal = "\(objArea.value(forKey: "documentType") ?? "")"

            }
            
//            strDocTypeLocal = "audio"
//            strDocTypeLocal = "image"
//            strDocTypeLocal = "pdf"
            
            if strDocTypeLocal == "image" {
                
                let image1: UIImage = UIImage(named: "NoImage.jpg")!
                var imageView = UIImageView(image: image1)
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                
                if image != nil  {
                    
                    imageView = UIImageView(image: image)
                    
                }else {
                    
                    imageView = UIImageView(image: image1)

                }
                
                let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
                testController!.img = imageView.image!
                        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
                
            } else if strDocTypeLocal == "pdf" {
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)

                goToPdfView(strPdfName: strImagePath!)
                
            } else if strDocTypeLocal == "audio" {
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)

                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                
                if isImageExists!  {
                    
                    playAudioo(strAudioName: strImagePath!)
                    
                }else {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    
                }
                
            }else if strDocTypeLocal == "video" {
                let strVideoPath = Global().strDocName(fromPath: strDocPathLocal)
                
                
                let isVideoExists: Bool? = checkIfImageExistAtPath(strFileName: strVideoPath!)
                
                if isVideoExists!  {
                    
                    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                    let documentsDirectory = paths[0]
                    let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strVideoPath!)")
                    print(path)
                    let player = AVPlayer(url: path)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                    
                }else {
                    
                    var vedioUrl = strURL + "\(strDocPathLocal)"

                    vedioUrl = vedioUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)

                    let player = AVPlayer(url: URL(string: vedioUrl)!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            return false
            
        }else{
            
            return true
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if (strType == enumMaterial){
            
            let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
                // delete item at indexPath
                
                let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                    
                    let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                    let strServiceProductIdLocal = objArea.value(forKey: "serviceProductId") as! String
                    let strMobileServiceProductIdLocal = objArea.value(forKey: "mobileServiceProductId") as! String
                    
                    if strServiceProductIdLocal.count > 0 && strServiceProductIdLocal != "0" {
                        
                        deleteAllRecordsFromDB(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && serviceProductId == %@ && companyKey == %@", self.strWoId, strServiceProductIdLocal,Global().getCompanyKey()))

                    }else {
                        
                        deleteAllRecordsFromDB(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceProductId == %@ && companyKey == %@", self.strWoId, strMobileServiceProductIdLocal,Global().getCompanyKey()))

                    }
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    self.fetchDataFrmDB()

                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    
                    
                }))
                
                alert.popoverPresentationController?.sourceView = self.view
                
                self.present(alert, animated: true, completion: {
                })
                
            }
            
            let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
                
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                let strServiceProductIdLocal = objArea.value(forKey: "serviceProductId") as! String
                let strMobileServiceProductIdLocal = objArea.value(forKey: "mobileServiceProductId") as! String
                //let idd = strServiceProductIdLocal.count > 0 ? strServiceProductIdLocal : strMobileServiceProductIdLocal
                
                self.goToMaterialView(strForUpdate: "Yes" , strServiceProductId: strServiceProductIdLocal , strMobileServiceProductId: strMobileServiceProductIdLocal, isFav: false, isWeCameToViewOnly: false)
                
            }
            
            edit.backgroundColor = UIColor.lightGray
            
            return [delete, edit]
            
            
        } else if (strType == enumPest){
            
            let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
                // delete item at indexPath
                
                let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                    
                    let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                    let strServiceProductIdLocal = objArea.value(forKey: "serviceAddressPestId") as! String
                    let strMobileServiceProductIdLocal = objArea.value(forKey: "mobileServiceAddressPestId") as! String
                    
                    if strServiceProductIdLocal.count > 0 && strServiceProductIdLocal != "0" {
                        
                        deleteAllRecordsFromDB(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && serviceAddressPestId == %@ && companyKey == %@", self.strWoId, strServiceProductIdLocal,Global().getCompanyKey()))
                        
                    }else {
                        
                        deleteAllRecordsFromDB(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressPestId == %@ && companyKey == %@", self.strWoId, strMobileServiceProductIdLocal,Global().getCompanyKey()))
                        
                    }
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    self.fetchDataFrmDB()

                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    
                    
                }))
                
                alert.popoverPresentationController?.sourceView = self.view
                
                self.present(alert, animated: true, completion: {
                })
                
            }
            
            let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
                
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                let strServiceProductIdLocal = objArea.value(forKey: "serviceAddressPestId") as! String
                let strMobileServiceProductIdLocal = objArea.value(forKey: "mobileServiceAddressPestId") as! String
                
                self.goToPestsView(strForUpdate: "Yes", strServiceAddressPestId: strServiceProductIdLocal, strMobileServiceAddressPestId: strMobileServiceProductIdLocal)
                
            }
            
            edit.backgroundColor = UIColor.lightGray
            
            return [delete, edit]
            
            
        } else if (strType == enumCondition){
            
            // delete nahi lagana hai condition me
            
            /*  let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
             // delete item at indexPath
             
             let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
             
             alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
             
             
             self.fetchDataFrmDB()
             
             }))
             
             alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
             
             
             }))
             
             alert.popoverPresentationController?.sourceView = self.view
             
             self.present(alert, animated: true, completion: {
             })
             
             }
             */
            let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
                
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                let strsaConduciveConditionIdLocal = objArea.value(forKey: "sAConduciveConditionId") as! String
                let strMobileSAConduciveConditionIdLocal = objArea.value(forKey: "mobileSAConduciveConditionId") as! String
                //let idd = strServiceProductIdLocal.count > 0 ? strServiceProductIdLocal : strMobileServiceProductIdLocal
                
                
                
                self.goToConditionView(strForUpdate: "Yes", strsaConduciveConditionId: strsaConduciveConditionIdLocal, strMobileSAConduciveConditionId: strMobileSAConduciveConditionIdLocal)
                
                
                
            }
            
            edit.backgroundColor = UIColor.lightGray
            
            //            return [delete, edit]
            return [edit]
            
        }else if (strType == enumComment){
            
            let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
                // delete item at indexPath
                
                let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                    
                    if self.strFrom == "Condition" {
                        
                        let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                        let strServiceConditionIdLocal = objArea.value(forKey: "sAConditionCommentId") as! String
                        
                        deleteAllRecordsFromDB(strEntity: "ServiceConditionComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@ && companyKey == %@ && sAConditionCommentId == %@", self.strWoId, self.strConditionId,Global().getCompanyKey(), strServiceConditionIdLocal))

                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        self.fetchDataFrmDB()
                        
                    }else{
                        
                        let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                        let strServiceCommentIdLocal = objArea.value(forKey: "serviceCommentId") as! String
                        let strMobileServiceCommentIdLocal = objArea.value(forKey: "mobileServiceCommentId") as! String
                        
                        if strServiceCommentIdLocal.count > 0 && strServiceCommentIdLocal != "0" {
                            
                            deleteAllRecordsFromDB(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && serviceCommentId == %@ && companyKey == %@", self.strWoId, strServiceCommentIdLocal,Global().getCompanyKey()))
                            
                        }else {
                            
                            deleteAllRecordsFromDB(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceCommentId == %@ && companyKey == %@", self.strWoId, strMobileServiceCommentIdLocal,Global().getCompanyKey()))
                            
                        }
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        self.fetchDataFrmDB()
                        
                    }

                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    
                    
                }))
                
                alert.popoverPresentationController?.sourceView = self.view
                
                self.present(alert, animated: true, completion: {
                })
                
            }
            
            let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
                
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                var strServiceCommentIdLocal =  String()
                var strMobileServiceCommentIdLocal = String()
                
                if self.strFrom == "Condition"{
                    
                    strServiceCommentIdLocal = objArea.value(forKey: "sAConditionCommentId") as! String
                    strMobileServiceCommentIdLocal = objArea.value(forKey: "sAConditionCommentId") as! String
                    
                }else{
                    
                    strServiceCommentIdLocal = objArea.value(forKey: "serviceCommentId") as! String
                    strMobileServiceCommentIdLocal = objArea.value(forKey: "mobileServiceCommentId") as! String
                    
                }
                
                self.goToCommentView(strForUpdate: "Yes", strServiceCommentId: strServiceCommentIdLocal, strMobileServiceCommentId: strMobileServiceCommentIdLocal)
                
            }
            
            edit.backgroundColor = UIColor.lightGray
            
            return [edit]
            
            
        } else {
            
            let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
                // delete item at indexPath
                
                let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                    
                    if self.strFrom == "Condition" {
                        
                        // documentPath
                        let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                        let strServiceConditionIdLocal = objArea.value(forKey: "sAConditionDocumentId") as! String
                        let strDocPathLocal = objArea.value(forKey: "documentPath") as! String

                        deleteAllRecordsFromDB(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@ && companyKey == %@ && sAConditionDocumentId == %@", self.strWoId, self.strConditionId,Global().getCompanyKey(), strServiceConditionIdLocal))
                        
                        // deleting data from Document Directory also
                        let strDocNameToRemove = Global().strDocName(fromPath: strDocPathLocal)

                        removeImageFromDirectory(itemName: strDocNameToRemove!)
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        self.fetchDataFrmDB()
                        
                    }else{
                        // serviceAddressDocPath
                        let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                        let strServiceCommentIdLocal = objArea.value(forKey: "serviceAddressDocId") as! String
                        let strMobileServiceCommentIdLocal = objArea.value(forKey: "mobileServiceAddressDocId") as! String
                        let strDocPathLocal = objArea.value(forKey: "serviceAddressDocPath") as! String

                        if strServiceCommentIdLocal.count > 0 && strServiceCommentIdLocal != "0" { //serviceAddressAreaId
                            
                            deleteAllRecordsFromDB(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId == %@ && serviceAddressDocId == %@ && companyKey == %@ && mobileServiceAddressAreaId == %@", self.strWoId, strServiceCommentIdLocal,Global().getCompanyKey() , self.strAddressAreaId))
                            
                        }else {
                            
                            deleteAllRecordsFromDB(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressDocId == %@ && companyKey == %@ && mobileServiceAddressAreaId == %@", self.strWoId, strMobileServiceCommentIdLocal,Global().getCompanyKey() , self.strAddressAreaId))
                            
                        }
                        
                        // deleting data from Document Directory also
                        let strDocNameToRemove = Global().strDocName(fromPath: strDocPathLocal)
                        
                        removeImageFromDirectory(itemName: strDocNameToRemove!)
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        self.fetchDataFrmDB()
                        
                    }
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    
                    
                }))
                
                alert.popoverPresentationController?.sourceView = self.view
                
                self.present(alert, animated: true, completion: {
                })
                
            }
            
            let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
                
                self.goToSelectDocument()
                
            }
            
            edit.backgroundColor = UIColor.lightGray
            
            return [delete]
            
            
        }

        
    }
    
}

// MARK: - ----------------Cell
// MARK: -

class MaterialListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnFav: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}


class PestsListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}

class ConditionsListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPriority: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}


class CommentListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}


class DocumentListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewAddendum: UIView!
    @IBOutlet weak var btnAddendum: UIButton!
    @IBOutlet weak var btnAddToInternal: UIButton!
    @IBOutlet weak var lblAddToInternal: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}


// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension ListVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        //let resizedImage = Global().resizeImageGlobal((info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!, "")
        
        //self.SaveImageToDb(image: (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!)
        
    }
}
