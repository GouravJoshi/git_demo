//
//  InspectDeviceVC.swift
//  DPS
//
//  Created by Saavan Patidar on 19/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

// MARK:- -----------------------------------Selction From Table view On Pop Up -----------------------------------

extension InspectDeviceVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(dictData: NSDictionary, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 37 {
            
            if dictData["DeviceReplacementReason"] as? String  == strSelectString {
                
                dictDataOfReasonSelected = NSDictionary ()
                
                txtFldReason.text = ""
                
            }else{
                
                dictDataOfReasonSelected = dictData
                
                txtFldReason.text = dictData["DeviceReplacementReason"] as? String

            }
            
        }
        
    }
    
}


class InspectDeviceVC: UIViewController {

    // MARK: - -----------------------------------Ib-Outlets -----------------------------------

    @IBOutlet var btnSlectActions: UIButton!
    @IBOutlet var btnActivity: UIButton!
    @IBOutlet weak var const_ReplacedView_H: NSLayoutConstraint!
    @IBOutlet weak var txtFldReason: ACFloatingTextField!
    @IBOutlet weak var txtFldBarcode: ACFloatingTextField!
    @IBOutlet weak var const_TxtFldBarcode_H: NSLayoutConstraint!
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblTitleHeader: UILabel!

    // MARK: - -----------------------------------Global Variables -----------------------------------

    @objc var strWoId = NSString ()
    var arrOfDevice = NSArray()
    var strMobileDeviceId = String ()
    var strDeviceId = String ()
    var isActivity = false
    var strActionType = String ()
    var objWorkorderDetail = NSManagedObject()
    var dictDataOfReasonSelected = NSDictionary ()
    var isInspectDeviceFirstTime = false
    var strDeviceReplacmentId = String()
    var strAreaDeviceNameToDisplay = String ()

    // MARK: - -----------------------------------life Cycle -----------------------------------

    override func viewDidLoad() {
        
        super.viewDidLoad()

        lblTitleHeader.text = strAreaDeviceNameToDisplay
        
        self.btnSlectActions.setTitle("No Action", for: .normal)
        self.const_ReplacedView_H.constant = 0
        self.const_TxtFldBarcode_H.constant = 0
        self.txtFldReason.text = ""
        buttonRound(sender: btnScan)

        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        self.setValuesToUpdate()

        if isWoCompleted(objWo: objWorkorderDetail) {
            
            btnSave.isHidden = true
            btnActivity.isEnabled = false
            btnSlectActions.isEnabled = false
            txtFldReason.isEnabled = false
            txtFldBarcode.isEnabled = false
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if (nsud.value(forKey: "SaveDeviceDynamicData") != nil) {
            
            let isSaveDeviceDynamicData = nsud.bool(forKey: "SaveDeviceDynamicData")
            
            if isSaveDeviceDynamicData {
                
                nsud.set(false, forKey: "SaveDeviceDynamicData")
                nsud.synchronize()
                
                var strIddd = String()
                
                if strDeviceId.count > 0 && strDeviceId != "0" {
                    
                    strIddd = strDeviceId
                    
                }else {
                    
                    strIddd = strMobileDeviceId

                }
                
            //let arryOfDeviceInspectionData = getDataFromCoreDataBaseArray(strEntity: "DeviceInspectionDynamicForm", predicate: NSPredicate(format: "workOrderId == %@ && serviceDeviceId == %@", strWoId,strIddd))
                
              let arryOfDeviceInspectionData = getDataFromCoreDataBaseArray(strEntity: "DeviceInspectionDynamicForm", predicate: NSPredicate(format: "serviceDeviceId == %@ && companyKey == %@ && workOrderId == %@", strIddd,Global().getCompanyKey(),strWoId))

                
                if arryOfDeviceInspectionData.count > 0 {
                    
                    // Device Inspection Exist
                    let arrDeviceInspectionData = nsud.value(forKey: "DeviceDynamicData") as! NSArray
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("arrFinalInspection")
                    arrOfValues.add(arrDeviceInspectionData)

                    let isSuccess = getDataFromDbToUpdate(strEntity: "DeviceInspectionDynamicForm", predicate: NSPredicate(format: "workOrderId == %@ && serviceDeviceId == %@", strWoId,strIddd), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        
                        
                    }
                    
                }else{
                    
                    let arrDeviceInspectionData = nsud.value(forKey: "DeviceDynamicData") as! NSArray
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("arrFinalInspection")
                    arrOfKeys.add("isSentToServer")
                    arrOfKeys.add("serviceDeviceId")//serviceDeviceId
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add(arrDeviceInspectionData)
                    arrOfValues.add("Yes")
                    
                    if strDeviceId.count > 0 && strDeviceId != "0" {
                        
                        arrOfValues.add(strDeviceId)
                        
                    }else {
                        
                        arrOfValues.add(strMobileDeviceId)
                        
                    }
                    
                    saveDataInDB(strEntity: "DeviceInspectionDynamicForm", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }

                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        }
        
        if (nsud.value(forKey: "fromScanner") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "fromScanner")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "fromScanner")
                nsud.synchronize()
                
                txtFldBarcode.text = nsud.value(forKey: "scannedCode") as? String
                
            }
            
        }
        
    }
    
    // MARK: - -----------------------------------Actions -----------------------------------
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if strDeviceId.count > 0 && strDeviceId != "0" {
            
            self.saveDeviceDB(strDeviceId: strDeviceId)
            
        }else {
            
            self.saveDeviceDB(strDeviceId: strMobileDeviceId)

        }
        
    }
    
    @IBAction func action_Activity(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if sender.currentImage == UIImage(named: "check_box_2.png") {
            
            sender.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            isActivity = false
            
        } else {
            
            sender.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            isActivity = true
            
        }
        
    }
    
    @IBAction func action_SelectActions(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "Please Select Action", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "No Action", style: .default , handler:{ (UIAlertAction)in
            
            self.btnSlectActions.setTitle("No Action", for: .normal)
            self.const_ReplacedView_H.constant = 0
            self.const_TxtFldBarcode_H.constant = 0
            self.txtFldReason.text = ""
            self.strActionType = "1"
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Replaced", style: .default , handler:{ (UIAlertAction)in
            
            self.btnSlectActions.setTitle("Replaced", for: .normal)
            self.const_ReplacedView_H.constant = 112
            self.const_TxtFldBarcode_H.constant = 50
            self.txtFldReason.placeholder = "Reason*"
            self.txtFldReason.addTarget(self, action: #selector(self.selectReason), for: .allEvents)
            self.txtFldReason.text = ""
            self.strActionType = "2"

        }))
        
        
        alert.addAction(UIAlertAction(title: "Removed", style: .default , handler:{ (UIAlertAction)in
            
            self.btnSlectActions.setTitle("Removed", for: .normal)
            self.const_ReplacedView_H.constant = 60
            self.const_TxtFldBarcode_H.constant = 0
            self.txtFldReason.placeholder = "Comment"
            self.txtFldReason.removeTarget(self, action: #selector(self.selectReason), for: .allEvents)
            self.txtFldReason.text = ""
            self.strActionType = "3"

        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    @IBAction func action_Barcode(_ sender: Any) {
        
        self.view.endEditing(true)
        self.goToScannerView()
        
    }
    
    @IBAction func action_DeviceInspection(_ sender: Any) {
        
        self.goToDeviceInspectionView()
        
    }
    
    
    // MARK: - -----------------------------------Functions -----------------------------------

    func goToDeviceInspectionView()  {
        
        var  arrOfAllData = NSArray()
        
        if strDeviceId.count > 0 && strDeviceId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "companyKey == %@ && deviceId == %@",Global().getCompanyKey(), strDeviceId))
            
        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "companyKey == %@ && mobileDeviceId == %@",Global().getCompanyKey(), strMobileDeviceId))
            
        }
        
        var deviceSysNameLocal = String()
        
        if arrOfAllData.count > 0 {
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            let deviceTypeId = "\(objMaterial.value(forKey: "devieTypeId") ?? "")"
        
            var array = NSMutableArray()

            array = getDataFromServiceAutoMasters(strTypeOfMaster: "deviceTypeMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                if array.count > 0 {
                    
                    for k1 in 0 ..< array.count {
                        
                        let dictData = array[k1] as! NSDictionary
                        
                        // to add which target groups to be shown for product
                        
                        if deviceTypeId == "\(dictData.value(forKey: "DeviceTypeId") ?? "")"  {
                            
                            deviceSysNameLocal = "\(dictData.value(forKey: "DeviceTypeSysName") ?? "")"
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
            }
            
            
        }
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DeviceDynamicInspectionVC") as? DeviceDynamicInspectionVC
        testController?.matchesWorkOrderZSync = objWorkorderDetail
        testController?.strDeviceTypeSysName = deviceSysNameLocal
        
        if strDeviceId.count > 0 && strDeviceId != "0" {
            
            testController?.strDeviceId = strDeviceId
            
            if strMobileDeviceId.count < 1{
                
                strMobileDeviceId = ""
                
            }
            
            testController?.strMobileDeviceId = strMobileDeviceId
            
        } else {
            
            if strMobileDeviceId.count < 1{
                
                strMobileDeviceId = ""
                
            }
            
            testController?.strMobileDeviceId = strMobileDeviceId
            
            if strDeviceId.count < 1{
                
                strDeviceId = ""
                
            }
            
            testController?.strDeviceId = strDeviceId
            
        }
        
        testController?.strHeaderName = strAreaDeviceNameToDisplay
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func saveDeviceDB(strDeviceId : String) {
        
        
        if strActionType == "1" {
            
            self.saveInspectionDB(strDeviceIdd: strDeviceId)
            
        } else if strActionType == "2" {
            
            if (txtFldReason.text?.count)! <= 0{
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_DeviceReason, viewcontrol: self)
                
            }else{
                
                self.saveInspectionDB(strDeviceIdd: strDeviceId)

            }
            
        }else{
            
            if (txtFldReason.text?.count)! <= 0{
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_DeviceComment, viewcontrol: self)
                
            }else{
                
                self.saveInspectionDB(strDeviceIdd: strDeviceId)

            }
            
        }
        

        
    }
    
    func saveInspectionDB(strDeviceIdd : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        if strActionType == "2" {
            
            strDeviceReplacmentId = "\(dictDataOfReasonSelected.value(forKey: "DeviceReplacementReasonMasterId") ?? "")"
            
        } else {
            
            strDeviceReplacmentId = ""
            
        }
        
        arrOfKeys.add("actionId")
        arrOfKeys.add("activity")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("userName")
        arrOfKeys.add("mobileServiceDeviceId")
        arrOfKeys.add("isActive")
        arrOfKeys.add("deviceInspectionId")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("companyId")
        arrOfKeys.add("isDeletedDevice")

        
        arrOfValues.add(strActionType)
        if isActivity {
            
            arrOfValues.add(true)
            
        }else{
            
            arrOfValues.add(false)
            
        }
        arrOfValues.add(strWoId)
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strDeviceIdd)
        arrOfValues.add(true)
        arrOfValues.add(Global().getReferenceNumber())
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getCompanyId())
        arrOfValues.add(true)

        if strActionType == "1" {
            
            arrOfKeys.add("comments")
            arrOfKeys.add("barcode")
            arrOfKeys.add("deviceReplacementReasonId")
            arrOfKeys.add("deviceReplacementReasonName")

            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add("")

        }
        if strActionType == "2" {
            
            arrOfKeys.add("comments")
            arrOfKeys.add("barcode")
            arrOfKeys.add("deviceReplacementReasonId") // deviceReplacementReasonName
            arrOfKeys.add("deviceReplacementReasonName")
            
            arrOfValues.add("")
            arrOfValues.add(txtFldBarcode.text!.count > 0 ? txtFldBarcode.text! : "")
            arrOfValues.add(strDeviceReplacmentId)
            arrOfValues.add(txtFldReason.text!)

        }
        if strActionType == "3" {
            
            arrOfKeys.add("comments")
            arrOfKeys.add("barcode")
            arrOfKeys.add("deviceReplacementReasonId")
            arrOfKeys.add("deviceReplacementReasonName")

            arrOfValues.add(txtFldReason.text!)
            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add("")

        }


        if isInspectDeviceFirstTime {
            
            saveDataInDB(strEntity: "ServiceDeviceInspections", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            dismiss(animated: false)

        } else {
    
            if strDeviceId.count > 0 && strDeviceId != "0" {
                
                let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "mobileServiceDeviceId == %@ && companyKey == %@ && workOrderId == %@", strDeviceId,Global().getCompanyKey() ,strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    dismiss(animated: false)
                    
                } else {
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                    
                }
                
            }else{
                
                let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "mobileServiceDeviceId == %@ && companyKey == %@  && workOrderId == %@", strMobileDeviceId,Global().getCompanyKey() , strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    dismiss(animated: false)
                    
                } else {
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                    
                }
                
            }
            
        }
    
    }
    
    func setValuesOnFirstTime() {
        
        self.btnSlectActions.setTitle("No Action", for: .normal)
        self.const_ReplacedView_H.constant = 0
        self.const_TxtFldBarcode_H.constant = 0
        self.txtFldReason.text = ""
        self.strActionType = "1"
        
        isActivity = false
        btnActivity.setImage(UIImage(named: "check_box_1.png"), for: .normal)

    }
    
    func setValuesToUpdate() {
        
        var  arrOfAllData = NSArray()
        
        if strDeviceId.count > 0 && strDeviceId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "companyKey == %@ && mobileServiceDeviceId == %@  && workOrderId == %@",Global().getCompanyKey(), strDeviceId , strWoId))

        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "companyKey == %@ && mobileServiceDeviceId == %@ && workOrderId == %@",Global().getCompanyKey(), strMobileDeviceId , strWoId))

        }

        if arrOfAllData.count > 0 {
            
            isInspectDeviceFirstTime = false
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            if "\(objMaterial.value(forKey: "actionId") ?? "")" == "1" {
                
                self.btnSlectActions.setTitle("No Action", for: .normal)
                self.const_ReplacedView_H.constant = 0
                self.const_TxtFldBarcode_H.constant = 0
                self.txtFldReason.text = ""
                self.strActionType = "1"

            }else if "\(objMaterial.value(forKey: "actionId") ?? "")" == "2" {
                
                self.btnSlectActions.setTitle("Replaced", for: .normal)
                self.const_ReplacedView_H.constant = 112
                self.const_TxtFldBarcode_H.constant = 50
                self.txtFldReason.placeholder = "Reason*"
                self.txtFldReason.addTarget(self, action: #selector(self.selectReason), for: .allEvents)
                self.txtFldReason.text = "\(objMaterial.value(forKey: "deviceReplacementReasonName") ?? "")"
                self.strActionType = "2"
                strDeviceReplacmentId = "\(objMaterial.value(forKey: "deviceReplacementReasonId") ?? "")"
                self.txtFldBarcode.text = "\(objMaterial.value(forKey: "barcode") ?? "")"

            }else {
                // removed
                
                self.btnSlectActions.setTitle("Removed", for: .normal)
                self.const_ReplacedView_H.constant = 60
                self.const_TxtFldBarcode_H.constant = 0
                self.txtFldReason.placeholder = "Comment"
                self.txtFldReason.removeTarget(self, action: #selector(self.selectReason), for: .allEvents)
                self.txtFldReason.text = "\(objMaterial.value(forKey: "comments") ?? "")"
                self.strActionType = "3"

            }
            
            let isActivityLocal = objMaterial.value(forKey: "activity") as! Bool
            
            if isActivityLocal {
                
                isActivity = true
                btnActivity.setImage(UIImage(named: "check_box_2.png"), for: .normal)

            }else{
                
                isActivity = false
                btnActivity.setImage(UIImage(named: "check_box_1.png"), for: .normal)

            }
            
        }else{
            
            isInspectDeviceFirstTime = true
            self.setValuesOnFirstTime()
            
        }
        
        
    }
    
    func goToScannerView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "QRScannerController") as? QRScannerController
        testController?.strFromWhere = "AddArea"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    @objc func selectReason(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "DeviceReplacementReasonMasters" , strBranchId: "")
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "DeviceReplacementReason", array: array)
            
            gotoPopViewWithArray(sender: textField, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    func gotoPopViewWithArray(sender: UITextField , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSelectionOnEditOpportunity = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    

}


// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension InspectDeviceVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtFldBarcode  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        }
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}
