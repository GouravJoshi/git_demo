//
//  DeviceDetailVC.swift
//  DPS
//
//  Created by Saavan Patidar on 13/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class DeviceDetailVC: UIViewController {

    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblTitleHeader: UILabel!
    @IBOutlet weak var viewAddButton: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var btnChecklist: UIButton!
    @IBOutlet weak var btnOptionMenu: UIButton!

    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnStatics: UIButton!

    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var strParentAddressAreaId = String ()
    var strParentAddressAreaName = String ()
    var strMobileDeviceId = String ()
    var strDeviceId = String ()
    var strDeviceName = String ()
    var strAreaNameToDisplay = String ()
    var strButtonTitleOfStartDate = ""
    let global = Global()
    // MARK: - -----------------------------------Life Cycle -----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        
        // Adding Add Button
        
        if DeviceType.IS_IPHONE_X {
            
            viewAddButton.frame = CGRect(x: UIScreen.main.bounds.size.width-80, y: UIScreen.main.bounds.size.height-90-80, width: 80, height: 80)
            
        }else{
            
            viewAddButton.frame = CGRect(x: UIScreen.main.bounds.size.width-80, y: UIScreen.main.bounds.size.height-65-80, width: 80, height: 80)
            
        }
        
        //self.view.addSubview(viewAddButton)
        
        //buttonRound(sender: btnSave)
        
        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            strCompanyKey = "\(value)"
        }
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            strUserName = "\(value)"
        }
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        self.setValuesOnViewLoad()
        
        var  arrOfAllData = NSArray()
        
        if strDeviceId.count > 0 && strDeviceId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "companyKey == %@ && deviceId == %@",Global().getCompanyKey(), strDeviceId))

        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "companyKey == %@ && mobileDeviceId == %@",Global().getCompanyKey(), strMobileDeviceId))

        }

        if arrOfAllData.count > 0 {
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            strDeviceName = "\(objMaterial.value(forKey: "deviceName") ?? "")"
            
        }
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Dismiss ANd goto Invoice View
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            btnStartTime.isEnabled = false
            self.btnGallery.setBackgroundImage(UIImage(named: "galleryGray"), for: .normal)
            self.btnStatics.setBackgroundImage(UIImage(named: "staticsGray"), for: .normal)
            self.btnGallery.isEnabled  = false
            self.btnStatics.isEnabled = false
        }
        else{
            btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
            btnChecklist.layer.borderWidth = 1.0
            btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)
            
            strButtonTitleOfStartDate = UserDefaults.standard.value(forKey: "StartStopButtonTitle") as! String
            if strButtonTitleOfStartDate == "Start"{
                setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Pause")
            }
            else if strButtonTitleOfStartDate == "Pause"{
                setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Start")
            }
        }
        if (nsud.value(forKey: "dismissView") != nil) {
            
            let isDismissView = nsud.bool(forKey: "dismissView")
            
            if isDismissView {
                
                self.back()
                
            }
            
        }
        
        tvlist.reloadData()
        
    }
    
    // MARK: - -----------------------------------Actions -----------------------------------
    
    
    @IBAction func action_Clock(_ sender: Any) {
        self.showOtherOptionsActionSheet()

    }
    @IBAction func btnChecklistAction(_ sender: Any) {
        goToInspectionView()
    }
    
    @IBAction func btnStartTimeAction(_ sender: Any){
        /* btnStartTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime")*/
        
        let str = "\(objWorkorderDetail.value(forKey: "TimeIn") ?? "")" //"\(lblStartTimeValue.titleLabel?.text ?? "")"
        
        if str == ""
        {
            self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: false)
            strButtonTitleOfStartDate = "Pause"
        }
        else
        {
            var alertTitle = ""
            if strButtonTitleOfStartDate == "Start"{
                alertTitle = "Start"
            }
            else if strButtonTitleOfStartDate == "Pause"{
                alertTitle = "Pause"
            }
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want \(alertTitle) time", preferredStyle: UIAlertController.Style.alert)
            

            alert.addAction(UIAlertAction(title: alertTitle, style: .default , handler:{ (nil)in
                
                if self.strButtonTitleOfStartDate == "Start"{
                    self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: true)
                }
                else if self.strButtonTitleOfStartDate == "Pause"{
                    self.getStartTimeIn(isStartTime: false, isStrTimeInAvial: true)
                }
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (nil)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        nsud.set(true, forKey: "dismissView")
        nsud.synchronize()
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_Barcode(_ sender: Any) {
        
        self.goToScannerView()
        
    }
    
    @IBAction func action_Image(_ sender: Any) {
        
        self.goToGlobalmage(strType: "After")
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")
        
        
    }
   
    
    @IBAction func action_Add(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Area", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAreaView()
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Device", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAddDeviceView()
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    // MARK: - -----------------------------------Functions -----------------------------------
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func saveTimeInDB(startTime : String){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("TimeIn")
        arrOfValues.add(startTime)

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    //MARK: - Duration calculations
    func getDuration(strStartDate: String, strPauseDate: String) -> String{
        
        if strStartDate.count == 0 {
            
            return "\(0)"

        }else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            
           
            var startDate = dateFormatter.date(from: strStartDate)
            if startDate == nil{
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.local
                dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm:ss"
                startDate = dateFormatter2.date(from: strStartDate)
            }
            let pauseDate = dateFormatter.date(from: strPauseDate)!
            let resultedOffSet = pauseDate.offsetFrom(date: startDate!)
            
            let result = getDurationInterval(strOffSet: resultedOffSet)
            print(result)
            return result
            
        }
        
    }
    
    func getDurationInterval(strOffSet : String) -> String{
        let resultantOffSet = strOffSet
        
        let arr = resultantOffSet.components(separatedBy: " ")
        var strDay = "0"
        var strHr = "0"
        var strMin = "0"
        var strSec = "0"
        
        if resultantOffSet.contains("d"){
            strDay = arr[0].components(separatedBy: "d")[0]
            strHr = arr[1].components(separatedBy: "h")[0]
            strMin = arr[2].components(separatedBy: "m")[0]
            strSec = arr[3].components(separatedBy: "s")[0]

        }
        else if resultantOffSet.contains("h"){
            strHr = arr[0].components(separatedBy: "h")[0]
            strMin = arr[1].components(separatedBy: "m")[0]
            strSec = arr[2].components(separatedBy: "s")[0]

        }
        else if arr.count == 1{
            strSec = arr[0].components(separatedBy: "s")[0]
        }
        else{
           strMin = arr[0].components(separatedBy: "m")[0]
           strSec = arr[1].components(separatedBy: "s")[0]
        }
        
        strDay = strDay.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strDay = strDay.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strHr = strHr.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strHr = strHr.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strMin = strMin.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strMin = strMin.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strSec = strSec.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strSec = strSec.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        let dayInt = Int(strDay)!
        let hrInt = Int(strHr)!
        let minInt = Int(strMin)!
        let secInt = Int(strSec)!

        print(dayInt)
        print(hrInt)
        print(minInt)
        print(secInt)

        var dayConvertsInHours = Int()
        if dayInt != 0{
          dayConvertsInHours = dayInt * 24
        }
        let result = (dayConvertsInHours * 60 * 60) + (hrInt * 60 * 60) + (minInt * 60) + secInt  //INSECONDS
         //String(minInt) + " Min " + String(secInt) + " secs" // 2 Min 12 secs
        //
        return String(result)
    }
    
    
    
    func getStartTimeIn(isStartTime: Bool,  isStrTimeInAvial: Bool)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        
        let strtemp = "\(objWorkorderDetail.value(forKey: "TimeIn") ?? "")"
        var strDate = ""
        if strtemp != ""{
                        
            strDate  = changeStringDateToGivenFormat(strDate: strtemp, strRequiredFormat: "MM/dd/yyyy hh:mm:ss a")
        }
        if isStrTimeInAvial == false{
            self.saveTimeInDB(startTime: str)
            setStartButtonTextWithDateTime(strTempDateAndTime: str, strTimeType: "Start")
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
        }
        if strButtonTitleOfStartDate == "Start"{
            

            let obj =  fetchStartTimeWithStartStatus()
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
            
            strButtonTitleOfStartDate = "Pause"

            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
            
            

        }
        else if strButtonTitleOfStartDate == "Pause"{
            
            let obj =  fetchStartTimeWithStartStatus()
            
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
                       
            strButtonTitleOfStartDate = "Start"

            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "PAUSE", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Pause")

            
        }
        
    }
    
    
     func fetchStartTimeWithStartStatus() -> NSMutableArray{
         
         let objTimeSheetLastIndex = NSMutableArray()
         var queryParam  = ""
         if strButtonTitleOfStartDate == "Start"{
             queryParam = "PAUSE"
         }
         if strButtonTitleOfStartDate == "Pause"{
             queryParam = "START"
         }
         let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@  && status == %@", strWoId, queryParam))
     
         //Featching all the data from DB for start stop
         let tempArr = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
 
         let tempDict = NSMutableArray()
         for index in 0..<tempArr.count{
             let dict = tempArr[index] as! NSManagedObject
             tempDict.add(dict)
         }
 
         for j in 0..<tempDict.count{
             let tempIndex = tempDict[j] as! NSManagedObject
             print("Start Time: \(tempIndex.value(forKey: "startTime") as! String))")
             print("Stop Time: \(tempIndex.value(forKey: "stopTime") as! String))")
             print("Duration: \(tempIndex.value(forKey: "duration") as! String))")
             print("status: \(tempIndex.value(forKey: "status") as! String))")
             print("status: \(tempIndex.value(forKey: "subject") as! String))")
 
         }
         
         if arryOfData.count > 0 {
                 let dict = arryOfData.lastObject as! NSManagedObject
                 objTimeSheetLastIndex.add(dict)
         }
         return objTimeSheetLastIndex
     }
     
     func saveStartStopTimeInTimeSheet(strStartTime: String, strStopTime: String, strStatus: String, strDuration: String, isStart: Bool){
         
         let arrOfKeys = NSMutableArray()
         let arrOfValues = NSMutableArray()

         if isStart == true{
             arrOfKeys.add("companyKey")
             arrOfKeys.add("workorderId")
             arrOfKeys.add("userName")
             arrOfKeys.add("startTime")
             arrOfKeys.add("stopTime")
             arrOfKeys.add("duration")
             arrOfKeys.add("status")
             arrOfKeys.add("subject")
             
             arrOfValues.add(strCompanyKey)
             arrOfValues.add(strWoId)
             arrOfValues.add(strUserName)
             arrOfValues.add(strStartTime)
             arrOfValues.add(strStopTime)
             arrOfValues.add(strDuration)
             arrOfValues.add(strStatus)
             arrOfValues.add(strStatus)

             saveDataInDB(strEntity: "GeneralInfoWorkorderTimeSheet", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
         }
         else if isStart == false{
             
             arrOfKeys.add("stopTime")
             arrOfKeys.add("duration")
             arrOfKeys.add("status")
             
             arrOfValues.add(strStopTime)
             arrOfValues.add(strDuration)
             arrOfValues.add(strStatus)
             
             let isSuccess = getDataFromDbToUpdate(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && startTime == %@", strWoId, strStartTime), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
             
             if isSuccess
             {
                 print("Success")
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                UserDefaults.standard.setValue(strButtonTitleOfStartDate, forKey: "StartStopButtonTitle")

                if strButtonTitleOfStartDate == "Start"{

                    nsud.set(true, forKey: "dismissViewAfterPauseAndRedirectToGI")
                    nsud.synchronize()
                    
                    dismiss(animated: false)
                    
                }
                
             }
             else {
                 showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
             }
         }
      
     }
     
     func setStartButtonTextWithDateTime(strTempDateAndTime: String, strTimeType: String){
         
 //        btnStartTime?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
 //        var buttonText = NSString()
 //        if strTimeType == "Start" {
 //            buttonText = "Pause"//\nST:" + strTempDateAndTime as NSString
 //        }
 //        else{
 //            buttonText = "Start"//\nST:" + strTempDateAndTime as NSString
 //        }
 //        //"hello\nthere"
 //
 //           //getting the range to separate the button title strings
 //           let newlineRange: NSRange = buttonText.range(of: "\n")
 //
 //           //getting both substrings
 //           var substring1 = ""
 //           var substring2 = ""
 //
 //           if(newlineRange.location != NSNotFound) {
 //               substring1 = buttonText.substring(to: newlineRange.location)
 //               substring2 = buttonText.substring(from: newlineRange.location)
 //           }
 //
 //           //assigning diffrent fonts to both substrings
 //           let font1: UIFont = UIFont(name: "Arial", size: 14.0)!
 //           let attributes1 = [NSMutableAttributedString.Key.font: font1]
 //           let attrString1 = NSMutableAttributedString(string: substring1, attributes: attributes1)
 //
 //           let font2: UIFont = UIFont(name: "Arial", size: 9.0)!
 //           let attributes2 = [NSMutableAttributedString.Key.font: font2]
 //           let attrString2 = NSMutableAttributedString(string: substring2, attributes: attributes2)
 //
 //           //appending both attributed strings
 //           attrString1.append(attrString2)
 //
 //           //assigning the resultant attributed strings to the button
 //           btnStartTime.titleLabel?.textAlignment = NSTextAlignment.center
 //
         if strTimeType == "Start" {
             btnStartTime.setTitle("Pause", for: .normal)
             btnStartTime.backgroundColor = hexStringToUIColor(hex: "9A9A9A")
         }
         else{
             btnStartTime.setTitle("Start", for: .normal)
             btnStartTime.backgroundColor = hexStringToUIColor(hex: "36A085")
         }
         
          //  btnStartTime.setAttributedTitle(attrString1, for: [])
     }
     
    func goToInspectionView()
    {
        
        let testController = UIStoryboard.init(name: "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "InspectionChecklistCopyVC") as! InspectionChecklistCopyVC
        
        let strTempAcountNumber = objWorkorderDetail.value(forKey: "accountNo") as? String
        if strTempAcountNumber != ""{
            testController.strAccountNumber  = strTempAcountNumber ?? ""
        }
        testController.matchesWorkOrderZSync = objWorkorderDetail
        testController.isFromNewPestFlow = "NO"
        testController.modalPresentationStyle = .fullScreen
        self.present(testController, animated: false, completion: nil)
    }
    
    func showOtherOptionsActionSheet()  {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let currentSetupAction = UIAlertAction(title: "Current Setup", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCurrentSetupAction()
        })
        let customerSalesDocumentsAction = UIAlertAction(title: "Customer Sales Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.goToCustomerSalesDocuments()
        })
        
        let notesHistoryAction = UIAlertAction(title: "Notes History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.goToNotesHistory()
        })
        let serviceHistoryAction = UIAlertAction(title: "Service History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.goToServiceHistory()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(currentSetupAction)
        optionMenu.addAction(customerSalesDocumentsAction)
        optionMenu.addAction(notesHistoryAction)
        optionMenu.addAction(serviceHistoryAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.btnOptionMenu
        self.present(optionMenu, animated: true, completion: {
        })
        
    }
    
    func openCurrentSetupAction(){
        
    }
    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func setValuesOnViewLoad() {
        
        lblTitleHeader.text = nsud.value(forKey: "lblName") as? String
        
        lblTitleHeader.text = strAreaNameToDisplay
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanicaliPhone") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.strTypeOfService = "service"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.strAccountNo = objWorkorderDetail.value(forKey: "accountNo") as? String
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        // workorderStatus
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strModuleType = "ServicePestFlow"
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAreaView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddAreaVC") as? AddAreaVC
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAddDeviceView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDeviceVC") as? AddDeviceVC
        testController?.strWoId = strWoId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToInspectDeviceView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "InspectDeviceVC") as? InspectDeviceVC
        testController?.strWoId = strWoId
        testController?.strDeviceId = strDeviceId
        testController?.strMobileDeviceId = strMobileDeviceId
        
        let strAreaDeviceName = strAreaNameToDisplay  + "\n" + strDeviceName
        
        testController?.strAreaDeviceNameToDisplay = strAreaDeviceName
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToScannerView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "QRScannerController") as? QRScannerController
        testController?.strFromWhere = "AddArea"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToListView(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ListVC") as? ListVC
        testController?.strType = strType
        testController?.strWoId = strWoId
        testController?.strAddressAreaName = strParentAddressAreaName
        testController?.strAddressAreaId = strParentAddressAreaId
        testController?.strAssociationType = "Device"
        //testController?.strFromDeviceName = strDeviceName
        if strDeviceId.count > 0 && strDeviceId != "0" {

            testController?.strDeviceId = strDeviceId
            testController?.strMobileDeviceId = ""
            
        }else{
            
            testController?.strDeviceId = ""
            testController?.strMobileDeviceId = strMobileDeviceId
            
        }
        let strAreaDeviceName = strAreaNameToDisplay  + "\n" + strDeviceName
        
        testController?.strFromDeviceName = strAreaDeviceName
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    
    @objc func filterForDevice(btn: UIButton) {
        
        
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "All", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Due Today", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Not Inspected", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Skipped", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }

    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DeviceDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 50
            
        } else {
            
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            //
            
            let strHeader = strDeviceName
            
            
            let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let lblHeader = UILabel()
            lblHeader.frame = CGRect(x:  10, y: 0, width: tableView.frame.width-60, height: 50)
            lblHeader.text = strHeader
            lblHeader.font = UIFont.systemFont(ofSize: 20)
            vw.addSubview(lblHeader)
            
            let btn = UIButton(frame: CGRect(x: tableView.frame.width-55, y: 0, width: 50, height: 50))
            btn.setImage(UIImage(named: "advanceFilter.png"), for: .normal)
            
            btn.addTarget(self, action: #selector(filterForDevice), for: .touchUpInside)
            //vw.addSubview(btn)
            
            return vw
            
        } else {
            
            let strHeader = ""
            
            
            let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 0))
            //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let lblHeader = UILabel()
            lblHeader.frame = vw.frame
            lblHeader.text = strHeader
            lblHeader.font = UIFont.systemFont(ofSize: 20)
            vw.addSubview(lblHeader)
            
            return vw
            
        }
        
    }
    
    // set view for footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 0
            
        } else {
            
            return 5
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "DeviceDetail1Cell", for: indexPath as IndexPath) as! DeviceDetail1Cell
            
            cell.lblTitle.text = "Device Name"
            cell.lblSubTitle.text = "Device.............."
            
            tvlist.separatorColor = UIColor.clear
            
            return cell
            
        } else {
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "DeviceDetailCell", for: indexPath as IndexPath) as! DeviceDetailCell
            
            if indexPath.row == 0{
                
                var  arrOfAllData = NSArray()
                
                if strDeviceId.count > 0 && strDeviceId != "0" {
                    
                    arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "companyKey == %@ && mobileServiceDeviceId == %@  && workOrderId == %@",Global().getCompanyKey(), strDeviceId , strWoId))
                    
                } else {
                    
                    arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "companyKey == %@ && mobileServiceDeviceId == %@ && workOrderId == %@",Global().getCompanyKey(), strMobileDeviceId , strWoId))
                    
                }
                
                if arrOfAllData.count > 0 {
                 
                    cell.lblSubTitle.text = "Device Inspected: Yes"
                    
                }else{
                    
                    cell.lblSubTitle.text = "Device Inspected: No"

                }
                cell.lblTitle.text = enumInspectDevice
                //cell.lblSubTitle.text = ""
                cell.imgView.image = UIImage(named: "inspect_device_1")

            } else if indexPath.row == 1{
                
                var  arrOfAllData = NSArray()
                
                if strDeviceId.count > 0 && strDeviceId != "0" {

                      arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strDeviceId,Global().getCompanyKey(), enumDevice))

                    
                }else{
                    
                      arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strMobileDeviceId,Global().getCompanyKey(), enumDevice))
                    
                }
                
                let strNo = "\(arrOfAllData.count)"
                
                let strlblSubTitle = "\(strNo) " + enumMaterial
                
                cell.lblTitle.text = enumMaterial
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "chemical_1")

            } else if indexPath.row == 2{
                
                var  arrOfAllData = NSArray()
                
                if strDeviceId.count > 0 && strDeviceId != "0" {
                    
                    arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strDeviceId,Global().getCompanyKey(), enumDevice))
                    
                    
                }else{
                    
                    arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strMobileDeviceId,Global().getCompanyKey(), enumDevice))
                    
                }
                
                let strNo = "\(arrOfAllData.count)"
                
                let strlblSubTitle = "\(strNo) " + enumPest
                
                cell.lblTitle.text = enumPest
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "pest_1")

            }  else if indexPath.row == 3{
                
                var  arrOfAllData = NSArray()
                
                if strDeviceId.count > 0 && strDeviceId != "0" {
                    
                    arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strDeviceId,Global().getCompanyKey(), enumDevice))
                    
                }else{
                    
                    arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, strMobileDeviceId,Global().getCompanyKey(), enumDevice))
                    
                }
                
                let strNo = "\(arrOfAllData.count)"
                
                let strlblSubTitle = "\(strNo) " + enumComment
                
                cell.lblTitle.text = enumComment
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "message_1")

            }
            else {
            
            var idToCheck = ""
            
            if strDeviceId.count > 0 && strDeviceId != "0"{
                
                idToCheck = strDeviceId
                
            }else if strMobileDeviceId.count > 0 && strMobileDeviceId != "0"{
                
                idToCheck = strMobileDeviceId
                
            }else {
                
                idToCheck = ""
                
            }
            
            let arrOfAllData =  getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId, Global().getCompanyKey()))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumDocument
            
                cell.lblTitle.text = enumDocument
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "attatch_1")

            }
            tvlist.separatorColor = UIColor.clear
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            
            
        }else{
            
            if indexPath.row == 0{
                
                self.goToInspectDeviceView()
                
            }else if indexPath.row == 1{
                
                self.goToListView(strType: enumMaterial)
                
            }else if indexPath.row == 2{
                
                self.goToListView(strType: enumPest)
                
            } else if indexPath.row == 3{
                
                self.goToListView(strType: enumComment)
                
            }else {
                
                self.goToListView(strType: enumDocument)
                
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if indexPath.section == 0 {
            
            return true
            
        }else{
            
            return false
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
            })
            
        }
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            self.goToAreaView()
            
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [delete, edit]
        
    }
    
    
}

// MARK: - ----------------Cell
// MARK: -

class DeviceDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}


class DeviceDetail1Cell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}
