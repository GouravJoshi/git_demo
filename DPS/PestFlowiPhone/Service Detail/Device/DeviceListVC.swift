//
//  DeviceListVC.swift
//  DPS
//
//  Created by Saavan Patidar on 19/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class DeviceListVC: UIViewController {

    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblTitleHeader: UILabel!
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet weak var viewAddButton: UIView!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var btnChecklist: UIButton!
    @IBOutlet weak var btnOptionMenu: UIButton!

    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnStatics: UIButton!

    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var arrOfDevice = NSArray()
    var strAreaNameToDisplay = String ()
    var strButtonTitleOfStartDate = ""
    let global = Global()

    // MARK: - -----------------------------------Life Cycle -----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
       // buttonRound(sender: btnScan)

        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
       
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            strCompanyKey = "\(value)"
        }
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            strUserName = "\(value)"
        }
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        self.setValuesOnViewLoad()
        
        // Adding Add Button
        
        if DeviceType.IS_IPHONE_X {
            
            viewAddButton.frame = CGRect(x: UIScreen.main.bounds.size.width-80, y: UIScreen.main.bounds.size.height-90-80, width: 80, height: 80)
            
        }else{
            
            viewAddButton.frame = CGRect(x: UIScreen.main.bounds.size.width-80, y: UIScreen.main.bounds.size.height-65-80, width: 80, height: 80)
            
        }
        
        //self.view.addSubview(viewAddButton)
        
        if !isWoCompleted(objWo: objWorkorderDetail) {
            
            self.view.addSubview(viewAddButton)

        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            btnStartTime.isEnabled = false
            self.btnGallery.setBackgroundImage(UIImage(named: "galleryGray"), for: .normal)
            self.btnStatics.setBackgroundImage(UIImage(named: "staticsGray"), for: .normal)
            self.btnGallery.isEnabled  = false
            self.btnStatics.isEnabled = false
            
        }
        else{
            btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
            btnChecklist.layer.borderWidth = 1.0
            btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)
            
            
            strButtonTitleOfStartDate = UserDefaults.standard.value(forKey: "StartStopButtonTitle") as! String
            if strButtonTitleOfStartDate == "Start"{
                setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Pause")
            }
            else if strButtonTitleOfStartDate == "Pause"{
                setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Start")
            }
        }
        self.fetchDeviceList()

        // If From Add Device View to Inspect  Device.
        if (nsud.value(forKey: "DeviceAdded") != nil) {
            
            let isAreaAdded = nsud.bool(forKey: "DeviceAdded")
            
            if isAreaAdded {
                
                nsud.set(false, forKey: "DeviceAdded")
                nsud.synchronize()
                
                self.goToDeviceDetailView(strDeviceid: "", strMobileDeviceId: nsud.value(forKey: "DeviceAddedDeviceId") as! String)
                
            }
            
        }
        
        // Dismiss ANd goto Invoice View
        if (nsud.value(forKey: "dismissView") != nil) {
            
            let isDismissView = nsud.bool(forKey: "dismissView")
            
            if isDismissView {
                
                self.back()
                
            }
            
        }
        
        // Dismiss ANd goto Invoice View
        if (nsud.value(forKey: "dismissViewAfterPauseAndRedirectToGI") != nil) {
            
            let isDismissView = nsud.bool(forKey: "dismissViewAfterPauseAndRedirectToGI")
            
            if isDismissView {
                
                self.back()
                
            }
            
        }
        
    }
    
    
    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Clock(_ sender: Any) {
        self.showOtherOptionsActionSheet()

    }
    @IBAction func btnChecklistAction(_ sender: Any) {
        goToInspectionView()
    }
    
    @IBAction func btnStartTimeAction(_ sender: Any){
        /* btnStartTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime")*/
        
        let str = "\(objWorkorderDetail.value(forKey: "TimeIn") ?? "")" //"\(lblStartTimeValue.titleLabel?.text ?? "")"
        
        if str == ""
        {
            self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: false)
            strButtonTitleOfStartDate = "Pause"
        }
        else
        {
            var alertTitle = ""
            if strButtonTitleOfStartDate == "Start"{
                alertTitle = "Start"
            }
            else if strButtonTitleOfStartDate == "Pause"{
                alertTitle = "Pause"
            }
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want \(alertTitle) time", preferredStyle: UIAlertController.Style.alert)
            

            alert.addAction(UIAlertAction(title: alertTitle, style: .default , handler:{ (nil)in
                
                if self.strButtonTitleOfStartDate == "Start"{
                    self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: true)
                }
                else if self.strButtonTitleOfStartDate == "Pause"{
                    self.getStartTimeIn(isStartTime: false, isStrTimeInAvial: true)
                }
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (nil)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func action_Add(_ sender: Any) {
        
        self.goToAddDeviceView(strForUpdate: "No", strMobileDeviceId: "", strDeviceId: "")
        
    }
    
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        nsud.set(true, forKey: "dismissView")
        nsud.synchronize()
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_Image(_ sender: Any) {
        
        self.goToGlobalmage(strType: "After")
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")
        
        
    }
 
    @IBAction func action_Filter(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "All", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Due Today", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Not Inspected", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Skipped", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    // MARK: - -----------------------------------Functions -----------------------------------
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func saveTimeInDB(startTime : String){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("TimeIn")
        arrOfValues.add(startTime)

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    //MARK: - Duration calculations
    func getDuration(strStartDate: String, strPauseDate: String) -> String{
        
        if strStartDate.count == 0 {
            
            return "\(0)"

        }else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            
           
            var startDate = dateFormatter.date(from: strStartDate)
            if startDate == nil{
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.local
                dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm:ss"
                startDate = dateFormatter2.date(from: strStartDate)
            }
            let pauseDate = dateFormatter.date(from: strPauseDate)!
            let resultedOffSet = pauseDate.offsetFrom(date: startDate!)
            
            let result = getDurationInterval(strOffSet: resultedOffSet)
            print(result)
            return result
            
        }
        
    }
    
    func getDurationInterval(strOffSet : String) -> String{
        let resultantOffSet = strOffSet
        
        let arr = resultantOffSet.components(separatedBy: " ")
        var strDay = "0"
        var strHr = "0"
        var strMin = "0"
        var strSec = "0"
        
        if resultantOffSet.contains("d"){
            strDay = arr[0].components(separatedBy: "d")[0]
            strHr = arr[1].components(separatedBy: "h")[0]
            strMin = arr[2].components(separatedBy: "m")[0]
            strSec = arr[3].components(separatedBy: "s")[0]

        }
        else if resultantOffSet.contains("h"){
            strHr = arr[0].components(separatedBy: "h")[0]
            strMin = arr[1].components(separatedBy: "m")[0]
            strSec = arr[2].components(separatedBy: "s")[0]

        }
        else if arr.count == 1{
            strSec = arr[0].components(separatedBy: "s")[0]
        }
        else{
           strMin = arr[0].components(separatedBy: "m")[0]
           strSec = arr[1].components(separatedBy: "s")[0]
        }
        
        strDay = strDay.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strDay = strDay.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strHr = strHr.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strHr = strHr.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strMin = strMin.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strMin = strMin.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strSec = strSec.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strSec = strSec.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        let dayInt = Int(strDay)!
        let hrInt = Int(strHr)!
        let minInt = Int(strMin)!
        let secInt = Int(strSec)!

        print(dayInt)
        print(hrInt)
        print(minInt)
        print(secInt)

        var dayConvertsInHours = Int()
        if dayInt != 0{
          dayConvertsInHours = dayInt * 24
        }
        let result = (dayConvertsInHours * 60 * 60) + (hrInt * 60 * 60) + (minInt * 60) + secInt  //INSECONDS
         //String(minInt) + " Min " + String(secInt) + " secs" // 2 Min 12 secs
        //
        return String(result)
    }
    
    
    
    func getStartTimeIn(isStartTime: Bool,  isStrTimeInAvial: Bool)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        
        let strtemp = "\(objWorkorderDetail.value(forKey: "TimeIn") ?? "")"
        var strDate = ""
        if strtemp != ""{
                        
            strDate  = changeStringDateToGivenFormat(strDate: strtemp, strRequiredFormat: "MM/dd/yyyy hh:mm:ss a")
        }
        if isStrTimeInAvial == false{
            self.saveTimeInDB(startTime: str)
            setStartButtonTextWithDateTime(strTempDateAndTime: str, strTimeType: "Start")
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
        }
        if strButtonTitleOfStartDate == "Start"{
            

            let obj =  fetchStartTimeWithStartStatus()
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
            
            strButtonTitleOfStartDate = "Pause"

            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
            
            

        }
        else if strButtonTitleOfStartDate == "Pause"{
            
            let obj =  fetchStartTimeWithStartStatus()
            
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
                        
            strButtonTitleOfStartDate = "Start"

            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "PAUSE", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Pause")

            
        }
        
    }
    
    
     func fetchStartTimeWithStartStatus() -> NSMutableArray{
         
         let objTimeSheetLastIndex = NSMutableArray()
         var queryParam  = ""
         if strButtonTitleOfStartDate == "Start"{
             queryParam = "PAUSE"
         }
         if strButtonTitleOfStartDate == "Pause"{
             queryParam = "START"
         }
         let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@  && status == %@", strWoId, queryParam))
     
         //Featching all the data from DB for start stop
         let tempArr = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
 
         let tempDict = NSMutableArray()
         for index in 0..<tempArr.count{
             let dict = tempArr[index] as! NSManagedObject
             tempDict.add(dict)
         }
 
         for j in 0..<tempDict.count{
             let tempIndex = tempDict[j] as! NSManagedObject
             print("Start Time: \(tempIndex.value(forKey: "startTime") as! String))")
             print("Stop Time: \(tempIndex.value(forKey: "stopTime") as! String))")
             print("Duration: \(tempIndex.value(forKey: "duration") as! String))")
             print("status: \(tempIndex.value(forKey: "status") as! String))")
             print("status: \(tempIndex.value(forKey: "subject") as! String))")
 
         }
         
         if arryOfData.count > 0 {
                 let dict = arryOfData.lastObject as! NSManagedObject
                 objTimeSheetLastIndex.add(dict)
         }
         return objTimeSheetLastIndex
     }
     
     func saveStartStopTimeInTimeSheet(strStartTime: String, strStopTime: String, strStatus: String, strDuration: String, isStart: Bool){
         
         let arrOfKeys = NSMutableArray()
         let arrOfValues = NSMutableArray()

         if isStart == true{
             arrOfKeys.add("companyKey")
             arrOfKeys.add("workorderId")
             arrOfKeys.add("userName")
             arrOfKeys.add("startTime")
             arrOfKeys.add("stopTime")
             arrOfKeys.add("duration")
             arrOfKeys.add("status")
             arrOfKeys.add("subject")
             
             arrOfValues.add(strCompanyKey)
             arrOfValues.add(strWoId)
             arrOfValues.add(strUserName)
             arrOfValues.add(strStartTime)
             arrOfValues.add(strStopTime)
             arrOfValues.add(strDuration)
             arrOfValues.add(strStatus)
             arrOfValues.add(strStatus)

             saveDataInDB(strEntity: "GeneralInfoWorkorderTimeSheet", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
         }
         else if isStart == false{
             
             arrOfKeys.add("stopTime")
             arrOfKeys.add("duration")
             arrOfKeys.add("status")
             
             arrOfValues.add(strStopTime)
             arrOfValues.add(strDuration)
             arrOfValues.add(strStatus)
             
             let isSuccess = getDataFromDbToUpdate(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && startTime == %@", strWoId, strStartTime), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
             
             if isSuccess
             {
                 print("Success")
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                 UserDefaults.standard.setValue(strButtonTitleOfStartDate, forKey: "StartStopButtonTitle")

                if strButtonTitleOfStartDate == "Start"{

                    nsud.set(true, forKey: "dismissViewAfterPauseAndRedirectToGI")
                    nsud.synchronize()
                    
                    dismiss(animated: false)
                    
                }
                
             }
             else {
                 showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
             }
         }
      
     }
     
     func setStartButtonTextWithDateTime(strTempDateAndTime: String, strTimeType: String){
         
 //        btnStartTime?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
 //        var buttonText = NSString()
 //        if strTimeType == "Start" {
 //            buttonText = "Pause"//\nST:" + strTempDateAndTime as NSString
 //        }
 //        else{
 //            buttonText = "Start"//\nST:" + strTempDateAndTime as NSString
 //        }
 //        //"hello\nthere"
 //
 //           //getting the range to separate the button title strings
 //           let newlineRange: NSRange = buttonText.range(of: "\n")
 //
 //           //getting both substrings
 //           var substring1 = ""
 //           var substring2 = ""
 //
 //           if(newlineRange.location != NSNotFound) {
 //               substring1 = buttonText.substring(to: newlineRange.location)
 //               substring2 = buttonText.substring(from: newlineRange.location)
 //           }
 //
 //           //assigning diffrent fonts to both substrings
 //           let font1: UIFont = UIFont(name: "Arial", size: 14.0)!
 //           let attributes1 = [NSMutableAttributedString.Key.font: font1]
 //           let attrString1 = NSMutableAttributedString(string: substring1, attributes: attributes1)
 //
 //           let font2: UIFont = UIFont(name: "Arial", size: 9.0)!
 //           let attributes2 = [NSMutableAttributedString.Key.font: font2]
 //           let attrString2 = NSMutableAttributedString(string: substring2, attributes: attributes2)
 //
 //           //appending both attributed strings
 //           attrString1.append(attrString2)
 //
 //           //assigning the resultant attributed strings to the button
 //           btnStartTime.titleLabel?.textAlignment = NSTextAlignment.center
 //
         if strTimeType == "Start" {
             btnStartTime.setTitle("Pause", for: .normal)
             btnStartTime.backgroundColor = hexStringToUIColor(hex: "9A9A9A")
         }
         else{
             btnStartTime.setTitle("Start", for: .normal)
             btnStartTime.backgroundColor = hexStringToUIColor(hex: "36A085")
         }
         
          //  btnStartTime.setAttributedTitle(attrString1, for: [])
     }
     
    func goToInspectionView()
    {
       
        let testController = UIStoryboard.init(name: "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "InspectionChecklistCopyVC") as! InspectionChecklistCopyVC
        
        let strTempAcountNumber = objWorkorderDetail.value(forKey: "accountNo") as? String
        if strTempAcountNumber != ""{
            testController.strAccountNumber  = strTempAcountNumber ?? ""
        }
        testController.matchesWorkOrderZSync = objWorkorderDetail
        testController.isFromNewPestFlow = "NO"
        testController.modalPresentationStyle = .fullScreen
        self.present(testController, animated: false, completion: nil)
    }
    
    func showOtherOptionsActionSheet()  {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let currentSetupAction = UIAlertAction(title: "Current Setup", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCurrentSetupAction()
        })
        let customerSalesDocumentsAction = UIAlertAction(title: "Customer Sales Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.goToCustomerSalesDocuments()
        })
        
        let notesHistoryAction = UIAlertAction(title: "Notes History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.goToNotesHistory()
        })
        let serviceHistoryAction = UIAlertAction(title: "Service History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.goToServiceHistory()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(currentSetupAction)
        optionMenu.addAction(customerSalesDocumentsAction)
        optionMenu.addAction(notesHistoryAction)
        optionMenu.addAction(serviceHistoryAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.btnOptionMenu
        self.present(optionMenu, animated: true, completion: {
        })
        
    }
    
    func openCurrentSetupAction(){
        
    }
    
    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func setValuesOnViewLoad() {
        
        lblTitleHeader.text = nsud.value(forKey: "lblName") as? String
        
        lblTitleHeader.text = strAreaNameToDisplay
        
    }
    func fetchDeviceList() {
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                
                arrOfDevice = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressExist")

            }else{

                arrOfDevice = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressExist")

            }
            
        }else{
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{

                arrOfDevice = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "Yes")

            }else{

                arrOfDevice = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressblank")
                
            }

        }

        tvlist.reloadData()
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanicaliPhone") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.strTypeOfService = "service"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.strAccountNo = objWorkorderDetail.value(forKey: "accountNo") as? String
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        // workorderStatus
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strModuleType = "ServicePestFlow"
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAreaView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddAreaVC") as? AddAreaVC
        testController?.strWoId = strWoId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAddDeviceView(strForUpdate : String , strMobileDeviceId : String , strDeviceId : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDeviceVC") as? AddDeviceVC
        testController?.strWoId = strWoId
        testController?.strParentAddressAreaId = strAddressAreaId
        testController?.strParentAddressAreaName = strAddressAreaName
        testController?.strForUpdate = strForUpdate
        testController?.strMobileDeviceId = strMobileDeviceId
        testController?.strDeviceId = strDeviceId
        
        if strForUpdate.lowercased() == "Yes".lowercased(){
            nsud.setValue("false", forKey:  "CameToAddDevice")
        }
        else{
            nsud.setValue("true", forKey:  "CameToAddDevice")
        }
             
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToScannerView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "QRScannerController") as? QRScannerController
        testController?.strFromWhere = "AddArea"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToListView(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ListVC") as? ListVC
        testController?.strType = strType
        testController?.strWoId = strWoId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToDeviceDetailView(strDeviceid : String , strMobileDeviceId : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DeviceDetailVC") as? DeviceDetailVC
        testController?.strWoId = strWoId
        testController?.strParentAddressAreaName = strAddressAreaName
        testController?.strParentAddressAreaId = strAddressAreaId
        testController?.strDeviceId = strDeviceid
        testController?.strMobileDeviceId = strMobileDeviceId
        testController?.strAreaNameToDisplay = strAreaNameToDisplay
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    @objc func filterForDevice(btn: UIButton) {
        
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "All", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Due Today", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Not Inspected", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Skipped", style: .default , handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DeviceListVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOfDevice.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "DeviceListCell", for: indexPath as IndexPath) as! DeviceListCell
        
        let objArea = arrOfDevice[indexPath.row] as! NSManagedObject
        
        cell.lblTitle.text = "\(objArea.value(forKey: "deviceName") ?? "N/A")"
        cell.lblSubTitle.text = "\(objArea.value(forKey: "deviceDescription") ?? "N/A")"
        
        let isInspected =  getDeivceInspectedData(strDeviceId: "\(objArea.value(forKey: "deviceId") ?? "")", strMobileDeviceId: "\(objArea.value(forKey: "mobileDeviceId") ?? "")")
        
        if isInspected == true{
            cell.lblInspected.backgroundColor = UIColor(hex: "36A085")
        }
        else{
            cell.lblInspected.backgroundColor = UIColor(hex: "9A9A9A")
        }
        tvlist.separatorColor = UIColor.clear
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65 //UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objArea = arrOfDevice[indexPath.row] as! NSManagedObject
        let strDeviceIdIdLocal = objArea.value(forKey: "deviceId") as! String
        let strMobileDeviceIdIdLocal = objArea.value(forKey: "mobileDeviceId") as! String
        
        self.goToDeviceDetailView(strDeviceid: strDeviceIdIdLocal, strMobileDeviceId: strMobileDeviceIdIdLocal)
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if !isWoCompleted(objWo: objWorkorderDetail) {
            
            return true
            
        }else{
            
            return false
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                
                let objArea = self.arrOfDevice[indexPath.row] as! NSManagedObject
                let strDeviceIdLocal = objArea.value(forKey: "deviceId") as! String
                let strMobileDeviceIdLocal = objArea.value(forKey: "mobileDeviceId") as! String
                
                // isDeletedDevice ko true krna hai yaha par
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("isDeletedDevice")
                arrOfValues.add(true)
                
                if strDeviceIdLocal.count > 0 && strDeviceIdLocal != "0" {
                    
                    //let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && deviceId == %@ && companyKey == %@", self.strWoId, strDeviceIdLocal,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDevices", predicate: NSPredicate(format: "deviceId == %@ && companyKey == %@", strDeviceIdLocal,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                    //deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && deviceId == %@ && companyKey == %@", self.strWoId, strDeviceIdLocal,Global().getCompanyKey()))
                    
                }else {
                    
                    //let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && mobileDeviceId == %@ && companyKey == %@", self.strWoId, strMobileDeviceIdLocal,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDevices", predicate: NSPredicate(format: "mobileDeviceId == %@ && companyKey == %@", strMobileDeviceIdLocal,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                    //deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && mobileDeviceId == %@ && companyKey == %@", self.strWoId, strMobileDeviceIdLocal,Global().getCompanyKey()))
                    
                }
                
                self.fetchDeviceList()
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
            })
            
        }
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            let objArea = self.arrOfDevice[indexPath.row] as! NSManagedObject
            let strDeviceIdLocal = objArea.value(forKey: "deviceId") as! String
            let strMobileDeviceIdLocal = objArea.value(forKey: "mobileDeviceId") as! String
            
            if strDeviceIdLocal.count > 0 && strDeviceIdLocal != "0" {
                
                self.goToAddDeviceView(strForUpdate: "Yes", strMobileDeviceId: "", strDeviceId: strDeviceIdLocal)

            }else {
                
                self.goToAddDeviceView(strForUpdate: "Yes", strMobileDeviceId: strMobileDeviceIdLocal, strDeviceId: "")

            }
            
            //self.goToAddDeviceView(strForUpdate: "Yes", strMobileDeviceId: "", strDeviceId: "")
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [edit]
        
    }
    
    func getDeivceInspectedData(strDeviceId: String, strMobileDeviceId: String) -> Bool{
        
        var  arrOfAllData = NSArray()

        if strDeviceId.count > 0 && strDeviceId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "companyKey == %@ && mobileServiceDeviceId == %@  && workOrderId == %@",Global().getCompanyKey(), strDeviceId , strWoId))
            
        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "companyKey == %@ && mobileServiceDeviceId == %@ && workOrderId == %@",Global().getCompanyKey(), strMobileDeviceId , strWoId))
            
        }
        
        if arrOfAllData.count > 0 {
         
            // "Device Inspected: Yes"
            return true
            
        }else{
            
            //"Device Inspected: No"
            return false

        }
        
    }
    
}

// MARK: - ----------------Cell
// MARK: -

class DeviceListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblInspected: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}


