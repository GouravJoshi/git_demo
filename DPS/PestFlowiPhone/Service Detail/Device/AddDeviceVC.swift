//
//  AddDeviceVC.swift
//  DPS
//
//  Created by Saavan Patidar on 13/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  jsdofjsdkf
//sdfsdfsdf

import UIKit

// MARK:- -----------------------------------Selction From Table view On Pop Up -----------------------------------

extension AddDeviceVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(dictData: NSDictionary, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 38 {
            
            if dictData["DeviceTypeName"] as? String  == strSelectString {
                
                dictOfDeviceTypeSelected = NSDictionary ()
                
                txtType.text = ""
                
            }else{
                
                dictOfDeviceTypeSelected = dictData
                
                txtType.text = dictOfDeviceTypeSelected["DeviceTypeName"] as? String
                
            }
            
        }
        
    }
    
}


class AddDeviceVC: UIViewController {
    
    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var txtName: ACFloatingTextField!
    @IBOutlet weak var txtBarCode: ACFloatingTextField!
    @IBOutlet weak var txtArea: ACFloatingTextField!
    @IBOutlet weak var txtType: ACFloatingTextField!
    @IBOutlet weak var txtDescription: ACFloatingTextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnAddArea: UIButton!
    @IBOutlet weak var btnBarcode: UIButton!
    @IBOutlet weak var lblTitleHeader: UILabel!
    @IBOutlet weak var tblDocument: UITableView!
    @IBOutlet weak var viewDocumentHeader: UIView!
    
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc var strWoId = NSString ()
    var strWoNo = NSString ()
    @objc var objWorkorderDetail = NSManagedObject()
    var dictOfDeviceTypeSelected = NSDictionary ()
    var strFrom = String()
    var strParentAddressAreaId = String ()
    var strParentAddressAreaName = String ()
    var strForUpdate = String ()
    var arrOfDevice = NSArray()
    var strMobileDeviceId = String ()
    var strDeviceId = String ()
    var arrOfAllData = NSArray()
    var strURL = String()
    // MARK: - -----------------------------------Life - Cycle -----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let strToAddDevice = nsud.value(forKey: "CameToAddDevice")
        if strToAddDevice as! String == "true"{
            
            viewDocumentHeader.isHidden = true
            tblDocument.isHidden = true

        }
        else{
            
            viewDocumentHeader.isHidden = false
            tblDocument.isHidden = false
        }
        
        txtArea.addTarget(self, action: #selector(selectAreaFunction), for: .allEvents)
        txtType.addTarget(self, action: #selector(selectType), for: .allEvents)
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        if strParentAddressAreaName.count > 0 {
            
            txtArea.text = strParentAddressAreaName
            
        }
        
        if strFrom == "Scanner"{
            
            txtBarCode.text = nsud.value(forKey: "scannedCode") as? String
            
        }else{
            
            
            
        }
        
        if strForUpdate == "Yes" {
            
            self.setValuesToUpdate()
            
            lblTitleHeader.text = "Edit Device"
            
        } else {
            
            lblTitleHeader.text = "Add Device"
            
        }
        
        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
            
            strURL = "\(value)"
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchDataFrmDB()
        
        if (nsud.value(forKey: "fromScanner") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "fromScanner")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "fromScanner")
                nsud.synchronize()
                
                txtBarCode.text = nsud.value(forKey: "scannedCode") as? String
                
            }
            
        }
        
        if (nsud.value(forKey: "fromArea") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "fromArea")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "fromArea")
                nsud.synchronize()
                
                txtArea.text = nsud.value(forKey: "strSelectedAreaName") as? String
                strParentAddressAreaId = (nsud.value(forKey: "strSelectedAreaId") as? String)!
                strParentAddressAreaName = (nsud.value(forKey: "strSelectedAreaName") as? String)!
                
            }
            
        }
        
        
    }
    
    
    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        nsud.setValue("false", forKey: "CameToAddDevice")
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if strForUpdate == "Yes" {
            
            if strDeviceId.count > 0 && strDeviceId != "0" {
                
                self.saveDeviceDB(strMobileDeviceId: "", strDeviceId: strDeviceId)
                
            }else{
                
                self.saveDeviceDB(strMobileDeviceId: strMobileDeviceId, strDeviceId: "0")
                
            }
            
        } else {
            
            self.saveDeviceDB(strMobileDeviceId: Global().getReferenceNumber(), strDeviceId: "0")
            
        }
        
    }
    
    @IBAction func action_Barcode(_ sender: Any) {
        
        self.view.endEditing(true)
        self.goToScannerView()
        
    }
    
    @IBAction func action_SelectArea(_ sender: Any) {
        
        self.view.endEditing(true)
        self.goToSelectAreaView()
        
    }
    
    
    // MARK: - -----------------------------------Functions -----------------------------------// mobileAddressAreaId
    
    func checkIfDeviceAlreadyAddedOnServiceAddressId() -> Bool {
        
        let  arrOfDeviceLocal = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedDevice == %@ && deviceName == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(),"0",txtName.text!))
        
        if arrOfDeviceLocal.count > 0 {
            
            if strForUpdate == "Yes" {
                
                // addressAreaId mobileAddressAreaId
                
                for k in 0 ..< arrOfDeviceLocal.count {
                    
                    if arrOfDeviceLocal[k] is NSManagedObject {
                        
                        let dictOfData = arrOfDeviceLocal[k] as! NSManagedObject
                        
                        
                        let strId = "\(dictOfData.value(forKey: "deviceId") ?? "")"
                        let strMobileId = "\(dictOfData.value(forKey: "mobileDeviceId") ?? "")"
                        
                        if strId != "0" && strId.count > 0 {
                            
                            if strDeviceId.count > 0 && strDeviceId != "0" {
                                
                                if strId == strDeviceId {
                                    
                                    
                                    return false
                                    
                                }
                                
                            } else {
                                
                                if strId == strMobileDeviceId {
                                    
                                    return false
                                    
                                }
                                
                            }
                            
                        }else{
                            
                            if strDeviceId.count > 0 && strDeviceId != "0" {
                                
                                if strMobileId == strDeviceId {
                                    
                                    
                                    return false
                                    
                                }
                                
                            } else {
                                
                                if strMobileId == strMobileDeviceId {
                                    
                                    return false
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                
            }
            
            return true
            
        } else {
            
            return false
            
        }
        
    }
    
    func setValuesToUpdate() {
        
        var  arrOfAllData = NSArray()
        
        if strDeviceId.count > 0 && strDeviceId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "companyKey == %@ && mobileAddressAreaId == %@ && deviceId == %@",Global().getCompanyKey(), strParentAddressAreaId, strDeviceId))
            
        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "companyKey == %@ && mobileAddressAreaId == %@ && mobileDeviceId == %@",Global().getCompanyKey(), strParentAddressAreaId, strMobileDeviceId))
        }
        
        if arrOfAllData.count > 0 {
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            txtName.text = "\(objMaterial.value(forKey: "deviceName") ?? "")"
            
            // Barcode
            
            txtBarCode.text = "\(objMaterial.value(forKey: "barocode") ?? "")"
            
            // deviceDescription
            
            txtDescription.text = "\(objMaterial.value(forKey: "deviceDescription") ?? "")"
            
            // strParentAddressAreaName
            
            txtArea.text = strParentAddressAreaName
            
            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "deviceTypeMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "DeviceTypeId") ?? "")" == "\(objMaterial.value(forKey: "devieTypeId") ?? "")"{
                        
                        dictOfDeviceTypeSelected  = dictData
                        
                        txtType.text = dictOfDeviceTypeSelected["DeviceTypeName"] as? String
                        
                        break
                        
                    }
                    
                }
                
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            
            self.back()
            
        }
        
    }
    
    func saveDeviceDB(strMobileDeviceId : String , strDeviceId : String) {
        
        if txtName.text!.count < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_EnterDeviceName, viewcontrol: self)
            
        } else if txtType.text!.count < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectType, viewcontrol: self)
            
        } else {
            
            // check if device already exist
            
            let isDeviceExist = self.checkIfDeviceAlreadyAddedOnServiceAddressId()
            
            if isDeviceExist {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_DeviceExist, viewcontrol: self)
                
            }else{
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("mobileDeviceId")
                arrOfKeys.add("deviceId")
                arrOfKeys.add("mobileAddressAreaId")
                arrOfKeys.add("workOrderId")
                arrOfKeys.add("serviceAddressId")
                arrOfKeys.add("deviceName")
                arrOfKeys.add("barocode")
                arrOfKeys.add("companyId")
                arrOfKeys.add("deviceDescription")
                arrOfKeys.add("deviceSysName")
                arrOfKeys.add("devieTypeId")
                arrOfKeys.add("isActive")
                arrOfKeys.add("action")
                arrOfKeys.add("activity")
                arrOfKeys.add("parentAreaName")
                //arrOfKeys.add("deviceReplacementReasonMasterId")
                //arrOfKeys.add("deviceReplacementReason")
                //arrOfKeys.add("deviceReplacementReasonDetail")
                
                //deviceReplacementReasonMasterId
                //deviceReplacementReason
                //deviceReplacementReasonDetail
                
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strMobileDeviceId)
                arrOfValues.add(strDeviceId)
                arrOfValues.add(strParentAddressAreaId)
                arrOfValues.add(strWoId)
                arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")")
                arrOfValues.add(txtName.text!)
                arrOfValues.add(txtBarCode.text!.count > 0 ? txtBarCode.text! : "")
                arrOfValues.add("\(dictOfDeviceTypeSelected.value(forKey: "CompanyId") ?? "")")
                arrOfValues.add(txtDescription.text!)
                arrOfValues.add("\(dictOfDeviceTypeSelected.value(forKey: "DeviceSysName") ?? "")")
                arrOfValues.add("\(dictOfDeviceTypeSelected.value(forKey: "DeviceTypeId") ?? "")")
                arrOfValues.add(true)
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add(strParentAddressAreaName.count > 0 ? strParentAddressAreaName : "")
                //arrOfValues.add("")
                //arrOfValues.add("")
                //arrOfValues.add("")
                
                
                if strForUpdate == "Yes" {
                    
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    arrOfValues.add(Global().getEmployeeId())
                    arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy HH:mm:ss", ""))
                    
                    if strDeviceId.count > 0 && strDeviceId != "0" {
                        
                        //let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && deviceId == %@ && companyKey == %@", strWoId, strDeviceId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDevices", predicate: NSPredicate(format: "deviceId == %@ && companyKey == %@", strDeviceId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess {
                            
                            //Update Modify Date In Work Order DB
                            updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
                            dismiss(animated: false)
                            
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        
                    }else{
                        
                        //let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && mobileDeviceId == %@ && companyKey == %@", strWoId, strMobileDeviceId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDevices", predicate: NSPredicate(format: "mobileDeviceId == %@ && companyKey == %@", strMobileDeviceId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess {
                            
                            //Update Modify Date In Work Order DB
                            updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
                            dismiss(animated: false)
                            
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        
                    }
                    
                    
                } else {
                    
                    arrOfKeys.add("isDeletedDevice")
                    arrOfValues.add(false)
                    
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    arrOfValues.add(Global().getEmployeeId())
                    arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                    arrOfValues.add(Global().getEmployeeId())
                    arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy HH:mm:ss", ""))
                    
                    saveDataInDB(strEntity: "ServiceDevices", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    nsud.set(strMobileDeviceId, forKey: "DeviceAddedDeviceId")
                    nsud.set(true, forKey: "DeviceAdded")
                    nsud.synchronize()
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    saveWoDevice(strMobileServiceDeviceIdDB: strMobileDeviceId)
                    
                    dismiss(animated: false)
                    
                }
                
            }
            
        }
        
    }
    
    func saveWoDevice(strMobileServiceDeviceIdDB : String)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("mobileServiceDeviceId")
        arrOfKeys.add("serviceDeviceId")
        arrOfKeys.add("wOServiceDeviceId")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add(strMobileServiceDeviceIdDB)
        arrOfValues.add("0")
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "WOServiceDevices", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func back()  {
        nsud.setValue("false", forKey: "CameToAddDevice")
        dismiss(animated: false)
        
    }
    
    @objc func selectAreaFunction(textField: UITextField) {
        
        self.goToSelectAreaView()
        
    }
    
    @objc func selectType(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "deviceTypeMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "DeviceTypeName", array: array)
            
            gotoPopViewWithArray(sender: textField, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
        
    }
    
    func goToScannerView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "QRScannerController") as? QRScannerController
        testController?.strFromWhere = "AddArea"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToSelectAreaView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SelectAreaVC") as? SelectAreaVC
        testController?.strWoId = strWoId
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func gotoPopViewWithArray(sender: UITextField , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSelectionOnEditOpportunity = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
            
        }
    }
    
}


// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension AddDeviceVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtName  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "CHARNUMBER", limitValue: 100)
            
        } else if textField == txtBarCode  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        } else if textField == txtArea  {
            
            return false
            
        } else if textField == txtType  {
            
            return false
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}


// MARK: - ----------Add Documents

extension AddDeviceVC {
    @IBAction func action_AddDocument(_ sender: Any) {
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddAreaDocumentVC") as? AddAreaDocumentVC
        testController?.strWoId = strWoId as String
        testController?.strWoNo = strWoNo as String
        testController?.strFrom = "Device"
        
        if strDeviceId.count > 0 && strDeviceId != "0" {
            
             testController?.strConditionId = strDeviceId
            
        } else {
            
            testController?.strConditionId = strMobileDeviceId
            
        }
        
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    
    
    func fetchDataFrmDB()  {
        
        var strTempDeviceId = ""
        if strDeviceId.count > 0 && strDeviceId != "0" {
            
            strTempDeviceId = strDeviceId
            
        } else {
            
            strTempDeviceId = strMobileDeviceId
            
        }
        
        arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@ && deviceId == %@", strWoId, Global().getCompanyKey(), strTempDeviceId))
        print(arrOfAllData)
        tblDocument.reloadData()
        
    }
    
    func goToRecordView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
        testController?.strFromWhere = "ServiceInvoice"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToSelectDocument()  {
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDocumentVC") as? AddDocumentVC
        testController?.strWoId = strWoId as String
        testController?.strFrom = strFrom
        //        testController?.strAddressAreaId = strAddressAreaId
        //        testController?.strAddressAreaName = strAddressAreaName
        //        testController?.strConditionId = strConditionId
        //        testController?.strAssociationType = strAssociationType
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToImageView(image : UIImage) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
        testController!.img = image
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToPdfView(strPdfName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func playAudioo(strAudioName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
}

// MARK: - ----------------UITableViewDelegate
extension AddDeviceVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOfAllData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblDocument.dequeueReusableCell(withIdentifier: "DocumentListCell", for: indexPath as IndexPath) as! DocumentListCell
        
        let objArea = arrOfAllData[indexPath.row] as! NSManagedObject
        
        var strDocPathLocal = String()
        var strDocTypeLocal = String()
        
        cell.lblTitle.text = "\(objArea.value(forKey: "documnetTitle") ?? "N/A")"
        strDocPathLocal = "\(objArea.value(forKey: "documentPath") ?? "")"
        strDocTypeLocal = "\(objArea.value(forKey: "documentType") ?? "")"
        
        if strDocTypeLocal == "image" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if image != nil && strImagePath?.count != 0 && isImageExists! {
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                
                cell.imgView.image = image
                
                // kch nhi krna
                
            }else {
                
                strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgView.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath! , image: image)
                            
                            cell.imgView.image = image
                            
                        }}
                    
                }
                
            }
            
            
        } else if strDocTypeLocal == "pdf" {
            
            let image: UIImage = UIImage(named: "exam")!
            
            cell.imgView.image = image
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                // kch nhi krna
                
            }else {
                
                strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        DispatchQueue.main.async {
                            
                            savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                            
                        }}
                    
                }
                
            }
            
        } else if strDocTypeLocal == "audio" {
            
            let image: UIImage = UIImage(named: "audio")!
            
            cell.imgView.image = image
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                // kch nhi krna
                
            }else {
                
                strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        DispatchQueue.main.async {
                            
                            savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                            
                        }}
                    
                }
                
            }
            
        }else if strDocTypeLocal == "video" {
            let image: UIImage = UIImage(named: "video")!
            cell.imgView.image = image
        }
        else{
         
            
            strDocTypeLocal = "image"
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                
                if image != nil && strImagePath?.count != 0 && isImageExists! {
                    
                    let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                    
                    let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                    
                    cell.imgView.image = image
                    
                    // kch nhi krna
                    
                }else {
                    
                    strURL = strURL + "\(strDocPathLocal)"
                    
                    strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                    
                    let image: UIImage = UIImage(named: "NoImage.jpg")!
                    
                    cell.imgView.image = image
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string:self.strURL)
                        let data = try? Data(contentsOf: url!)
                        
                        if data != nil && (data?.count)! > 0 {
                            
                            let image: UIImage = UIImage(data: data!)!
                            
                            DispatchQueue.main.async {
                                
                                saveImageDocumentDirectory(strFileName: strImagePath! , image: image)
                                
                                cell.imgView.image = image
                                
                            }}
                        
                    }
                    
                }
                
                
            }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //documentType
        
        var strDocPathLocal = String()
        var strDocTypeLocal = String()
        
        
        // documentPath
        let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
        strDocPathLocal = "\(objArea.value(forKey: "documentPath") ?? "")"
        strDocTypeLocal = "\(objArea.value(forKey: "documentType") ?? "")"
        
        
        if strDocTypeLocal == "image" {
            
            let image1: UIImage = UIImage(named: "NoImage.jpg")!
            var imageView = UIImageView(image: image1)
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                imageView = UIImageView(image: image1)
                
            }
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        } else if strDocTypeLocal == "pdf" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            goToPdfView(strPdfName: strImagePath!)
            
        } else if strDocTypeLocal == "audio" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                playAudioo(strAudioName: strImagePath!)
                
            }else {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                
            }
            
        }else if strDocTypeLocal == "video" {
            let strVideoPath = Global().strDocName(fromPath: strDocPathLocal)
            
            
            let isVideoExists: Bool? = checkIfImageExistAtPath(strFileName: strVideoPath!)
            
            if isVideoExists!  {
                
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let documentsDirectory = paths[0]
                let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strVideoPath!)")
                print(path)
                let player = AVPlayer(url: path)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
                
            }else {
                
                var vedioUrl = strURL + "\(strDocPathLocal)"
                
                vedioUrl = vedioUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let player = AVPlayer(url: URL(string: vedioUrl)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            return false
            
        }else{
            
            return true
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                
                //if self.strFrom == "Condition" {
                
                // documentPath
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                let strServiceConditionIdLocal = objArea.value(forKey: "serviceDeviceDocumentId") as! String
                let strDocPathLocal = objArea.value(forKey: "documentPath") as! String
                
                deleteAllRecordsFromDB(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@ && serviceDeviceDocumentId == %@", self.strWoId,Global().getCompanyKey(), strServiceConditionIdLocal))
                
                // deleting data from Document Directory also
                let strDocNameToRemove = Global().strDocName(fromPath: strDocPathLocal)
                
                removeImageFromDirectory(itemName: strDocNameToRemove!)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                self.fetchDataFrmDB()
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: {
            })
        }
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            self.goToSelectDocument()
            
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [delete]
    }
}
