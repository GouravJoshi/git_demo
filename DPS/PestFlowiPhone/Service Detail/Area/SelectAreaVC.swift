//
//  SelectAreaVC.swift
//  DPS
//
//  Created by Saavan Patidar on 09/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class SelectAreaVC: UIViewController {

    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewForArea: UIView!
    @IBOutlet weak var scrollViewArea: UIScrollView!

    // MARK: - -----------------------------------Global Variables -----------------------------------
    var aryButtonsArea = NSMutableArray()
    var index = -1
    var strWoId = NSString ()
    var strParentAddressAreaId = String ()
    var strParentAddressAreaName = String ()
    var arrOfSubAreas = NSArray()
    var arrOfArea = NSArray()
    var strMobileAddressAreaId = String ()
    var strAddressAreaId = String ()
    var indexOnDidSelect = -1
    var objWorkorderDetail = NSManagedObject()
    var strCurrentAddressId = ""
    var strCurrentMobileParentAreaId = ""
    let arrTempCount = NSMutableArray()

    // MARK: - -----------------------------------Life - Cycle -----------------------------------

    override func viewDidLoad() {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            dismiss(animated: false)
            
        }
        
        tvlist.estimatedRowHeight = 50.0
        tvlist.tableFooterView = UIView()
        
        super.viewDidLoad()
        
        if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
            
            arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && addressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", strAddressAreaId,Global().getCompanyKey(),"0"))
            
        } else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0" {
            
            arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", strMobileAddressAreaId,Global().getCompanyKey(),"0"))
            
        } else  {
            
            arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0"))
            
        }

        if strCurrentAddressId != ""{
            let tempArrSubArea = NSMutableArray()
            for i in 0..<arrOfSubAreas.count{
                let obj = arrOfSubAreas[i] as! NSManagedObject
                if  "\(obj.value(forKey: "mobileAddressAreaId") ?? "")" == strCurrentAddressId || "\(obj.value(forKey: "addressAreaId") ?? "")" == strCurrentAddressId {
                   print("not Add Anytihng")
                }
                else{
                    if  "\(obj.value(forKey: "mobileParentAddressAreaId") ?? "")" == strCurrentAddressId {
                        print("ye to child hai")
                    }
                    else{
                        tempArrSubArea.add(arrOfSubAreas[i])
                        print("add kardo")
                    }
                }
            }

            arrOfSubAreas = NSArray()
            arrOfSubAreas = tempArrSubArea
        }
        
        
        //now going to itrate child child
        var arrOfNoArea1 = NSMutableArray()
        
        if strCurrentAddressId.count > 0 && strCurrentAddressId != "0" {
            
            arrOfNoArea1 = getAreaCount(strIdd: strCurrentAddressId, strWoId: strWoId as String)
            
        } else {
            
            arrOfNoArea1 = getAreaCount(strIdd: strCurrentAddressId, strWoId: strWoId as String)
            
        }
        
        print(arrOfNoArea1)
        let tempDict = arrOfSubAreas.mutableCopy() as! NSMutableArray
        if arrOfNoArea1.count > 0{
            for i in 0..<arrOfNoArea1.count{
                let parentId = arrOfNoArea1[i] as! String
                for j in 0..<arrOfSubAreas.count{
                    let obj = arrOfSubAreas[j] as! NSManagedObject
                    if  "\(obj.value(forKey: "mobileAddressAreaId") ?? "")" == parentId || "\(obj.value(forKey: "addressAreaId") ?? "")" == parentId
                    {
                        tempDict.removeObject(at: j)
                        tempDict.remove(arrOfSubAreas[j])
                        break
                    }
                }
            }
            
            arrOfSubAreas = NSArray()
            arrOfSubAreas = tempDict
        }
        else{
            print("")
        }
        if(arrOfArea.count > 0){
            
            if aryButtonsArea.count > 1 {
                
                aryButtonsArea.add(arrOfArea)
                
                self.addDynamicButtons()
                
                let objArea = arrOfArea[0] as! NSManagedObject
                
                var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
                let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
                
                if strAddressAreaIdLocal == "0"{
                    
                    strAddressAreaIdLocal = ""
                    
                }
                
                let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
                
                strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
                strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
                
                strAddressAreaId = strAddressAreaIdLocal
                strMobileAddressAreaId = strMobileAddressAreaIdLocal
                
                arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@",  "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", idd,Global().getCompanyKey(), false))
                
            }
            
        }else{
            
            let array = NSMutableArray()
            
            let dict = NSMutableDictionary()
            dict.setValue("Area", forKey: "addressAreaName")
            array.add(dict)
            
            aryButtonsArea.add(array)
            
            self.addDynamicButtons()
            
        }
        
        tvlist.reloadData()
        
    }
    
    func getAreaCount(strIdd : String , strWoId : String) -> NSMutableArray {
        
        if strIdd == "S" {
            
            return arrTempCount
            
        }
        
        var arrOfNoArea = NSArray()
        
        arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, strIdd,Global().getCompanyKey(),"0"))
        
        if strIdd == "" {
            
            arrOfNoArea = NSArray()
            
        }
        
        if arrOfNoArea.count > 0 {
            
            for k in 0 ..< arrOfNoArea.count {
                
                let dictData = arrOfNoArea[k] as! NSManagedObject
                
                var strAddressAreaIdLocal = dictData.value(forKey: "addressAreaId") as! String
                let strMobileAddressAreaIdLocal = dictData.value(forKey: "mobileAddressAreaId") as! String
                
                if strAddressAreaIdLocal == "0" {
                    
                    strAddressAreaIdLocal = ""
                    
                }
                
                let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
                
                arrTempCount.add(idd)
                
                let arrTempCountttttt =  getAreaCount(strIdd: idd, strWoId: strWoId)
                
            }
            
        }else{
            
            let arrTempCountttttt = getAreaCount(strIdd: "S", strWoId: strWoId)
            
        }
        
        return arrTempCount
        
    }
    
    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        
        nsud.set(true, forKey: "FromArea")
        nsud.synchronize()
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        if index >= 0 {
            
            let objArea = arrOfSubAreas[index] as! NSManagedObject
            
            var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
            let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
            
            if strAddressAreaIdLocal == "0"{
                
                strAddressAreaIdLocal = ""
                
            }
            
            let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
            let strServiceAddressNameLocal = objArea.value(forKey: "addressAreaName") as! String
            
            nsud.set(idd, forKey: "strSelectedAreaId")
            nsud.set(strServiceAddressNameLocal, forKey: "strSelectedAreaName")
            nsud.set(true, forKey: "fromArea")
            nsud.synchronize()
            
            self.view.endEditing(true)
            dismiss(animated: false)
            
        } else if indexOnDidSelect >= 0 {
            
            let objArea = arrOfArea[0] as! NSManagedObject
            
            var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
            let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
            
            if strAddressAreaIdLocal == "0"{
                
                strAddressAreaIdLocal = ""
                
            }
            
            let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
            let strServiceAddressNameLocal = objArea.value(forKey: "addressAreaName") as! String

            nsud.set(idd, forKey: "strSelectedAreaId")
            nsud.set(strServiceAddressNameLocal, forKey: "strSelectedAreaName")
            nsud.set(true, forKey: "fromArea")
            nsud.synchronize()
            
            self.view.endEditing(true)
            dismiss(animated: false)
            
        } else{
            
            nsud.set("", forKey: "strSelectedAreaId")
            nsud.set("", forKey: "strSelectedAreaName")
            nsud.set(true, forKey: "fromArea")
            nsud.synchronize()
            
            self.view.endEditing(true)
            dismiss(animated: false)
            
            //showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectArea, viewcontrol: self)
            
        }
        
    }

    
    // MARK: - -----------------------------------Functions -----------------------------------

    func fetchAreas(strType : String) {
      
      arrOfArea = NSArray()
        
      if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
      
        arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && addressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", strAddressAreaId,Global().getCompanyKey(),"0"))
      
      } else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0" {
        
        arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@",  "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", strMobileAddressAreaId,Global().getCompanyKey(), "0"))
        
      } else  {
        
        arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@",  "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", Global().getCompanyKey(),"0"))
        
        }
        
        if strCurrentAddressId != ""{
         //   arrOfSubAreas =  getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0"))
            let tempArrSubArea = NSMutableArray()
            for i in 0..<arrOfSubAreas.count{
                let obj = arrOfSubAreas[i] as! NSManagedObject
                if  "\(obj.value(forKey: "mobileAddressAreaId") ?? "")" == strCurrentAddressId || "\(obj.value(forKey: "addressAreaId") ?? "")" == strCurrentAddressId {
                   print("not Add Anytihng")
                }
                else{
                    if  "\(obj.value(forKey: "mobileParentAddressAreaId") ?? "")" == strCurrentAddressId {
                        print("ye to child hai")
                    }
                    else{
                        tempArrSubArea.add(arrOfSubAreas[i])
                        print("add kardo")
                    }
                }
            }
            
            arrOfSubAreas = NSArray()
            arrOfSubAreas = tempArrSubArea
        }
        
        //now going to itrate child child
        var arrOfNoArea1 = NSMutableArray()
        
        if strCurrentAddressId.count > 0 && strCurrentAddressId != "0" {
            
            arrOfNoArea1 = getAreaCount(strIdd: strCurrentAddressId, strWoId: strWoId as String)
            
        } else {
            
            arrOfNoArea1 = getAreaCount(strIdd: strCurrentAddressId, strWoId: strWoId as String)
            
        }
        
        print(arrOfNoArea1)
        let tempDict = arrOfSubAreas.mutableCopy() as! NSMutableArray
        if arrOfNoArea1.count > 0{
            for i in 0..<arrOfNoArea1.count{
                let parentId = arrOfNoArea1[i] as! String
                for j in 0..<arrOfSubAreas.count{
                    let obj = arrOfSubAreas[j] as! NSManagedObject
                    if  "\(obj.value(forKey: "mobileAddressAreaId") ?? "")" == parentId || "\(obj.value(forKey: "addressAreaId") ?? "")" == parentId
                    {
                        tempDict.removeObject(at: j)
                        tempDict.remove(arrOfSubAreas[j])
                        break
                    }
                }
            }
            
            arrOfSubAreas = NSArray()
            arrOfSubAreas = tempDict
        }
        else{
            print("")
        }
        
        if(arrOfArea.count > 0){
            
            if strType == "Add"{
                
                aryButtonsArea.add(arrOfArea)
                
            }
            
            self.addDynamicButtons()
            
            let objArea = arrOfArea[0] as! NSManagedObject
            
            var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
            let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
            
            if strAddressAreaIdLocal == "0"{
                
                strAddressAreaIdLocal = ""
                
            }
            
            let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
            
            strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
            strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
            
            strAddressAreaId = strAddressAreaIdLocal
            strMobileAddressAreaId = strMobileAddressAreaIdLocal
            
            arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", idd,Global().getCompanyKey(), "0"))

        }
        
        tvlist.reloadData()
        
    }
    
    func addDynamicButtons()  {
        
        let subViews = self.scrollViewArea.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        var xPostion = 5
        
        for index in 0..<aryButtonsArea.count {
            
            var strTitle = String()
            
            if index == 0 {
                
                strTitle = enumArea + " > "
                
            }else{
                
                let arrOfAreaLocal = aryButtonsArea[index] as! NSArray
                
                let objArea = arrOfAreaLocal[0] as! NSManagedObject
                
                strTitle = "\(objArea.value(forKey: "addressAreaName") ?? "")" + " > "
                
            }
            
            //            let arrOfAreaLocal = aryButtonsArea[index] as! NSArray
            //
            //            let objArea = arrOfAreaLocal[0] as! NSManagedObject
            //
            //            let strTitle = "\(objArea.value(forKey: "addressAreaName") ?? "")" + " > "
            
            let width = strTitle.width(withConstraintedHeight: 50, font: UIFont.systemFont(ofSize: 18))
            
            let frame1 = CGRect(x: xPostion, y: 5, width: Int(width), height: 50 )
            
            let button = UIButton(frame: frame1)
            button.tag = index
            button.setTitle(strTitle, for: .normal)
            button.backgroundColor = UIColor.clear
            button.titleLabel?.textColor = UIColor.theme()
            button.setTitleColor(UIColor.theme(), for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
            button.addTarget(self, action: #selector(pressAreaButton(_:)), for: .touchUpInside)
            
            self.scrollViewArea.addSubview(button)
            
            xPostion = xPostion + Int(width) + 5
            
        }
        
        scrollViewArea.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 56)
        
        scrollViewArea.contentSize = CGSize(width: xPostion, height: 56)
        
        if Int(UIScreen.main.bounds.size.width) > xPostion {
            
        }else{
            
            let bottomOffset = CGPoint(x: scrollViewArea.contentSize.width - scrollViewArea.bounds.size.width, y: 0)
            scrollViewArea.setContentOffset(bottomOffset, animated: true)
            
        }
        
    }
    
    @objc func pressAreaButton(_ button: UIButton) {
        
        index = -1;
        
        print("Button with tag: \(button.tag) clicked!")
        
        if button.tag == 0 {
            
            let subRange = NSMakeRange(button.tag + 1, aryButtonsArea.count - 1)
            
            aryButtonsArea.removeObjects(in: subRange)
            
            addDynamicButtons()
            
            strAddressAreaId = ""
            strMobileAddressAreaId = ""
            strParentAddressAreaId = ""
            strParentAddressAreaName = ""
            
            /*
            
            let arrOfAreaLocal = aryButtonsArea[button.tag] as! NSArray
            
            let objArea = arrOfAreaLocal[0] as! NSManagedObject
            
            strAddressAreaId = objArea.value(forKey: "addressAreaId") as! String
            strMobileAddressAreaId = objArea.value(forKey: "mobileAddressAreaId") as! String
            
            strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
            strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
 
           */
            
            //self.fetchAreas(strType: "Remove")
            
        } else if button.tag == aryButtonsArea.count - 1 {
            
            
            
        } else{
            
            let subRange = NSMakeRange(button.tag + 1, aryButtonsArea.count - button.tag - 1)
            
            aryButtonsArea.removeObjects(in: subRange)
            
            addDynamicButtons()
            
            let arrOfAreaLocal = aryButtonsArea[button.tag] as! NSArray
            
            let objArea = arrOfAreaLocal[0] as! NSManagedObject
            
            strAddressAreaId = objArea.value(forKey: "addressAreaId") as! String
            strMobileAddressAreaId = objArea.value(forKey: "mobileAddressAreaId") as! String
            
            strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
            strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
            
            self.fetchAreas(strType: "Remove")
            
        }
        
    }
    
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SelectAreaVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        vw.backgroundColor = UIColor.lightTextColorTimeSheet()
        
        let lblHeader = UILabel()
        lblHeader.frame = vw.frame
        lblHeader.text = enumArea
        lblHeader.font = UIFont.systemFont(ofSize: 17)
        vw.addSubview(lblHeader)
        
        return vw
    }
    
    // set view for footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOfSubAreas.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "SelectAreaVCCell", for: indexPath as IndexPath) as! SelectAreaVCCell
        
        let objArea = arrOfSubAreas[indexPath.row] as! NSManagedObject

        cell.lblTitle.text = "\(objArea.value(forKey: "addressAreaName")!)"
        
        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(pressRadioButton(_:)), for: .touchUpInside)
        
        if index == indexPath.row {
            
            cell.btnRadio.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            
        }else{
            
            cell.btnRadio.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            
        }
        

        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        index = -1;
        
        indexOnDidSelect = indexPath.row
        
        self.view.endEditing(true)
        
        let objArea = arrOfSubAreas[indexPath.row] as! NSManagedObject
        
        strAddressAreaId = objArea.value(forKey: "addressAreaId") as! String
        strMobileAddressAreaId = objArea.value(forKey: "mobileAddressAreaId") as! String
        
        strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
        strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
        
       // self.fetchAreas(strType: "Add")
        
    }
    
    @objc func pressRadioButton(_ button: UIButton) {
        
        print("RAdio Button with tag: \(button.tag) clicked!")
        
        index = button.tag
        
        tvlist.reloadData()
        
//        let indexPath = NSIndexPath(row: index, section: 0)
//        tvlist.reloadRows(at: [indexPath as IndexPath], with: UITableView.RowAnimation.top)
        
    }
    
}
// MARK: - ----------------SlectionCell
// MARK: -
class SelectAreaVCCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
}


extension String {
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
}

