//
//  AreaDetailVC.swift
//  DPS
//
//  Created by Saavan Patidar on 13/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class AreaDetailVC: UIViewController {

    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblTitleHeader: UILabel!
    @IBOutlet weak var viewAddButton: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var scrollViewArea: UIScrollView!
    @IBOutlet weak var btnBarcode: UIButton!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var btnChecklist: UIButton!
    @IBOutlet weak var btnOptionMenu: UIButton!

    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnStatics: UIButton!

    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var arrOfArea = NSArray()
    var strParentAddressAreaId = String ()
    var strParentAddressAreaName = String ()
    var arrOfSubAreas = NSArray()
    var strMobileAddressAreaId = String ()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var aryButtonsArea = NSMutableArray()
    //var index = -1
    var arrTempCount = NSMutableArray()
    var strButtonTitleOfStartDate = ""
    let global = Global()
    // MARK: - -----------------------------------Life Cycle -----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        
        //buttonRound(sender: btnSave)
        
        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
       
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            strCompanyKey = "\(value)"
        }
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            strUserName = "\(value)"
        }
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        self.setValuesOnViewLoad()
        
        
        // Adding Add Button
        
        if DeviceType.IS_IPHONE_X {
            
            viewAddButton.frame = CGRect(x: UIScreen.main.bounds.size.width-80, y: UIScreen.main.bounds.size.height-90-80, width: 80, height: 80)
            
        }else{
            
            viewAddButton.frame = CGRect(x: UIScreen.main.bounds.size.width-80, y: UIScreen.main.bounds.size.height-65-80, width: 80, height: 80)
            
        }
        
        let isWoComplete = isWoCompleted(objWo: objWorkorderDetail)
        
        if !isWoComplete {
            
            self.view.addSubview(viewAddButton)
            btnBarcode.isHidden = false
            
        }else{
            
            btnBarcode.isHidden = true
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            btnStartTime.isEnabled = false
            self.btnGallery.setBackgroundImage(UIImage(named: "galleryGray"), for: .normal)
            self.btnStatics.setBackgroundImage(UIImage(named: "staticsGray"), for: .normal)
            self.btnGallery.isEnabled  = false
            self.btnStatics.isEnabled = false
        }
        else{
        btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
        btnChecklist.layer.borderWidth = 1.0
        btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)
        
        strButtonTitleOfStartDate = UserDefaults.standard.value(forKey: "StartStopButtonTitle") as! String
        if strButtonTitleOfStartDate == "Start"{
            setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Pause")
        }
        else if strButtonTitleOfStartDate == "Pause"{
            setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Start")
        }
        }
        self.fetchAreaSubArea()

        // If From Sanner View to Add Device.
        if (nsud.value(forKey: "fromScanner") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "fromScanner")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "fromScanner")
                nsud.synchronize()
                
                self.goToAddDeviceView(strfrom: "Scanner")
                
            }
            
        }
        
        // If From Add Device View to Inspect  Device.
        if (nsud.value(forKey: "DeviceAdded") != nil) {
            
            let isAreaAdded = nsud.bool(forKey: "DeviceAdded")
            
            if isAreaAdded {
                
                nsud.set(false, forKey: "DeviceAdded")
                nsud.synchronize()
                
                self.goToDeviceDetailView(strMobileDeviceId: nsud.value(forKey: "DeviceAddedDeviceId") as! String)

            }
            
        }
        
        // Dismiss ANd goto Invoice View
        if (nsud.value(forKey: "dismissView") != nil) {
            
            let isDismissView = nsud.bool(forKey: "dismissView")
            
            if isDismissView {
                
                self.back()
                
            }
            
        }
        
    }
    
    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Clock(_ sender: Any) {
        self.showOtherOptionsActionSheet()

    }
    @IBAction func btnChecklistAction(_ sender: Any) {
        goToInspectionView()
    }
    
    @IBAction func btnStartTimeAction(_ sender: Any){
        /* btnStartTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime")*/
        
        let str = "\(objWorkorderDetail.value(forKey: "TimeIn") ?? "")" //"\(lblStartTimeValue.titleLabel?.text ?? "")"
        
        if str == ""
        {
            self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: false)
            strButtonTitleOfStartDate = "Pause"
        }
        else
        {
            var alertTitle = ""
            if strButtonTitleOfStartDate == "Start"{
                alertTitle = "Start"
            }
            else if strButtonTitleOfStartDate == "Pause"{
                alertTitle = "Pause"
            }
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want \(alertTitle) time", preferredStyle: UIAlertController.Style.alert)
            

            alert.addAction(UIAlertAction(title: alertTitle, style: .default , handler:{ (nil)in
                
                if self.strButtonTitleOfStartDate == "Start"{
                    self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: true)
                }
                else if self.strButtonTitleOfStartDate == "Pause"{
                    self.getStartTimeIn(isStartTime: false, isStrTimeInAvial: true)
                }
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (nil)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        nsud.set(true, forKey: "dismissView")
        nsud.synchronize()
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_Barcode(_ sender: Any) {
        
        self.goToScannerView()
        
    }
    
    @IBAction func action_Image(_ sender: Any) {
        
        self.goToGlobalmage(strType: "After")
        
    }
    
  
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")
        
        
    }
   
    @IBAction func action_Add(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Area", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAreaView(strType: "Add", strMobileId: "", strServiceId: "")
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Device", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAddDeviceView(strfrom: "")
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    // MARK: - -----------------------------------Functions -----------------------------------
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func saveTimeInDB(startTime : String){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("TimeIn")
        arrOfValues.add(startTime)

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    //MARK: - Duration calculations
    func getDuration(strStartDate: String, strPauseDate: String) -> String{
        
        if strStartDate.count == 0 {
            
            return "\(0)"

        }else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            
           
            var startDate = dateFormatter.date(from: strStartDate)
            if startDate == nil{
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.local
                dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm:ss"
                startDate = dateFormatter2.date(from: strStartDate)
            }
            let pauseDate = dateFormatter.date(from: strPauseDate)!
            let resultedOffSet = pauseDate.offsetFrom(date: startDate!)
            
            let result = getDurationInterval(strOffSet: resultedOffSet)
            print(result)
            return result
            
        }
        
    }
    
    func getDurationInterval(strOffSet : String) -> String{
        let resultantOffSet = strOffSet
        
        let arr = resultantOffSet.components(separatedBy: " ")
        var strDay = "0"
        var strHr = "0"
        var strMin = "0"
        var strSec = "0"
        
        if resultantOffSet.contains("d"){
            strDay = arr[0].components(separatedBy: "d")[0]
            strHr = arr[1].components(separatedBy: "h")[0]
            strMin = arr[2].components(separatedBy: "m")[0]
            strSec = arr[3].components(separatedBy: "s")[0]

        }
        else if resultantOffSet.contains("h"){
            strHr = arr[0].components(separatedBy: "h")[0]
            strMin = arr[1].components(separatedBy: "m")[0]
            strSec = arr[2].components(separatedBy: "s")[0]

        }
        else if arr.count == 1{
            strSec = arr[0].components(separatedBy: "s")[0]
        }
        else{
           strMin = arr[0].components(separatedBy: "m")[0]
           strSec = arr[1].components(separatedBy: "s")[0]
        }
        
        strDay = strDay.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strDay = strDay.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strHr = strHr.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strHr = strHr.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strMin = strMin.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strMin = strMin.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strSec = strSec.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strSec = strSec.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        let dayInt = Int(strDay)!
        let hrInt = Int(strHr)!
        let minInt = Int(strMin)!
        let secInt = Int(strSec)!

        print(dayInt)
        print(hrInt)
        print(minInt)
        print(secInt)

        var dayConvertsInHours = Int()
        if dayInt != 0{
          dayConvertsInHours = dayInt * 24
        }
        let result = (dayConvertsInHours * 60 * 60) + (hrInt * 60 * 60) + (minInt * 60) + secInt  //INSECONDS
         //String(minInt) + " Min " + String(secInt) + " secs" // 2 Min 12 secs
        //
        return String(result)
    }
    
    
    
    func getStartTimeIn(isStartTime: Bool,  isStrTimeInAvial: Bool)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        
        let strtemp = "\(objWorkorderDetail.value(forKey: "TimeIn") ?? "")"
        var strDate = ""
        if strtemp != ""{
                        
            strDate  = changeStringDateToGivenFormat(strDate: strtemp, strRequiredFormat: "MM/dd/yyyy hh:mm:ss a")
        }
        if isStrTimeInAvial == false{
            self.saveTimeInDB(startTime: str)
            setStartButtonTextWithDateTime(strTempDateAndTime: str, strTimeType: "Start")
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
        }
        if strButtonTitleOfStartDate == "Start"{
            

            let obj =  fetchStartTimeWithStartStatus()
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
            
            strButtonTitleOfStartDate = "Pause"

            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
            
            

        }
        else if strButtonTitleOfStartDate == "Pause"{
            
            let obj =  fetchStartTimeWithStartStatus()
            
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
                     
            strButtonTitleOfStartDate = "Start"

            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "PAUSE", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Pause")

            
        }
        
    }
    
    
     func fetchStartTimeWithStartStatus() -> NSMutableArray{
         
         let objTimeSheetLastIndex = NSMutableArray()
         var queryParam  = ""
         if strButtonTitleOfStartDate == "Start"{
             queryParam = "PAUSE"
         }
         if strButtonTitleOfStartDate == "Pause"{
             queryParam = "START"
         }
         let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@  && status == %@", strWoId, queryParam))
     
         //Featching all the data from DB for start stop
         let tempArr = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
 
         let tempDict = NSMutableArray()
         for index in 0..<tempArr.count{
             let dict = tempArr[index] as! NSManagedObject
             tempDict.add(dict)
         }
 
         for j in 0..<tempDict.count{
             let tempIndex = tempDict[j] as! NSManagedObject
             print("Start Time: \(tempIndex.value(forKey: "startTime") as! String))")
             print("Stop Time: \(tempIndex.value(forKey: "stopTime") as! String))")
             print("Duration: \(tempIndex.value(forKey: "duration") as! String))")
             print("status: \(tempIndex.value(forKey: "status") as! String))")
             print("status: \(tempIndex.value(forKey: "subject") as! String))")
 
         }
         
         if arryOfData.count > 0 {
                 let dict = arryOfData.lastObject as! NSManagedObject
                 objTimeSheetLastIndex.add(dict)
         }
         return objTimeSheetLastIndex
     }
     
     func saveStartStopTimeInTimeSheet(strStartTime: String, strStopTime: String, strStatus: String, strDuration: String, isStart: Bool){
         
         let arrOfKeys = NSMutableArray()
         let arrOfValues = NSMutableArray()

         if isStart == true{
             arrOfKeys.add("companyKey")
             arrOfKeys.add("workorderId")
             arrOfKeys.add("userName")
             arrOfKeys.add("startTime")
             arrOfKeys.add("stopTime")
             arrOfKeys.add("duration")
             arrOfKeys.add("status")
             arrOfKeys.add("subject")
             
             arrOfValues.add(strCompanyKey)
             arrOfValues.add(strWoId)
             arrOfValues.add(strUserName)
             arrOfValues.add(strStartTime)
             arrOfValues.add(strStopTime)
             arrOfValues.add(strDuration)
             arrOfValues.add(strStatus)
             arrOfValues.add(strStatus)

             saveDataInDB(strEntity: "GeneralInfoWorkorderTimeSheet", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
         }
         else if isStart == false{
             
             arrOfKeys.add("stopTime")
             arrOfKeys.add("duration")
             arrOfKeys.add("status")
             
             arrOfValues.add(strStopTime)
             arrOfValues.add(strDuration)
             arrOfValues.add(strStatus)
             
             let isSuccess = getDataFromDbToUpdate(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && startTime == %@", strWoId, strStartTime), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
             
             if isSuccess
             {
                 print("Success")
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                 UserDefaults.standard.setValue(strButtonTitleOfStartDate, forKey: "StartStopButtonTitle")
                
                if strButtonTitleOfStartDate == "Start"{

                    nsud.set(true, forKey: "dismissViewAfterPauseAndRedirectToGI")
                    nsud.synchronize()
                    
                    dismiss(animated: false)
                    
                }

             }
             else {
                 showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
             }
         }
      
     }
     
     func setStartButtonTextWithDateTime(strTempDateAndTime: String, strTimeType: String){
         
 //        btnStartTime?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
 //        var buttonText = NSString()
 //        if strTimeType == "Start" {
 //            buttonText = "Pause"//\nST:" + strTempDateAndTime as NSString
 //        }
 //        else{
 //            buttonText = "Start"//\nST:" + strTempDateAndTime as NSString
 //        }
 //        //"hello\nthere"
 //
 //           //getting the range to separate the button title strings
 //           let newlineRange: NSRange = buttonText.range(of: "\n")
 //
 //           //getting both substrings
 //           var substring1 = ""
 //           var substring2 = ""
 //
 //           if(newlineRange.location != NSNotFound) {
 //               substring1 = buttonText.substring(to: newlineRange.location)
 //               substring2 = buttonText.substring(from: newlineRange.location)
 //           }
 //
 //           //assigning diffrent fonts to both substrings
 //           let font1: UIFont = UIFont(name: "Arial", size: 14.0)!
 //           let attributes1 = [NSMutableAttributedString.Key.font: font1]
 //           let attrString1 = NSMutableAttributedString(string: substring1, attributes: attributes1)
 //
 //           let font2: UIFont = UIFont(name: "Arial", size: 9.0)!
 //           let attributes2 = [NSMutableAttributedString.Key.font: font2]
 //           let attrString2 = NSMutableAttributedString(string: substring2, attributes: attributes2)
 //
 //           //appending both attributed strings
 //           attrString1.append(attrString2)
 //
 //           //assigning the resultant attributed strings to the button
 //           btnStartTime.titleLabel?.textAlignment = NSTextAlignment.center
 //
         if strTimeType == "Start" {
             btnStartTime.setTitle("Pause", for: .normal)
             btnStartTime.backgroundColor = hexStringToUIColor(hex: "9A9A9A")
         }
         else{
             btnStartTime.setTitle("Start", for: .normal)
             btnStartTime.backgroundColor = hexStringToUIColor(hex: "36A085")
         }
         
          //  btnStartTime.setAttributedTitle(attrString1, for: [])
     }
     
    func goToInspectionView()
    {
        
        let testController = UIStoryboard.init(name: "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "InspectionChecklistCopyVC") as! InspectionChecklistCopyVC
        
        let strTempAcountNumber = objWorkorderDetail.value(forKey: "accountNo") as? String
        if strTempAcountNumber != ""{
            testController.strAccountNumber  = strTempAcountNumber ?? ""
        }
        testController.isFromNewPestFlow = "NO"
        testController.matchesWorkOrderZSync = objWorkorderDetail
        testController.modalPresentationStyle = .fullScreen
        self.present(testController, animated: false, completion: nil)
    }
    
    func showOtherOptionsActionSheet()  {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let currentSetupAction = UIAlertAction(title: "Current Setup", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCurrentSetupAction()
        })
        let customerSalesDocumentsAction = UIAlertAction(title: "Customer Sales Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.goToCustomerSalesDocuments()
        })
        
        let notesHistoryAction = UIAlertAction(title: "Notes History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.goToNotesHistory()
        })
        let serviceHistoryAction = UIAlertAction(title: "Service History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.goToServiceHistory()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(currentSetupAction)
        optionMenu.addAction(customerSalesDocumentsAction)
        optionMenu.addAction(notesHistoryAction)
        optionMenu.addAction(serviceHistoryAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.btnOptionMenu
        self.present(optionMenu, animated: true, completion: {
        })
        
    }
    
    func openCurrentSetupAction(){
        
    }
    func back()  {
        
        dismiss(animated: false)

    }
    func addDynamicButtons()  {
        
        let subViews = self.scrollViewArea.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        var xPostion = 10
        
        for index in 0..<aryButtonsArea.count {
            
            var strTitle = String()
            
            if index == 0 {
                
                strTitle = enumArea + " > "
                
            }else{
                
                let arrOfAreaLocal = aryButtonsArea[index] as! NSArray
                
                let objArea = arrOfAreaLocal[0] as! NSManagedObject
                
                strTitle = "\(objArea.value(forKey: "addressAreaName") ?? "")" + " > "
                
            }
            
//            let arrOfAreaLocal = aryButtonsArea[index] as! NSArray
//
//            let objArea = arrOfAreaLocal[0] as! NSManagedObject
//
//            let strTitle = "\(objArea.value(forKey: "addressAreaName") ?? "")" + " > "
            
            let width = strTitle.width(withConstraintedHeight: 50, font: UIFont.systemFont(ofSize: 18))
            
            let frame1 = CGRect(x: xPostion, y: 5, width: Int(width), height: 50 )
            
            let button = UIButton(frame: frame1)
            button.tag = index
            button.setTitle(strTitle, for: .normal)
            button.backgroundColor = UIColor.clear
            button.titleLabel?.textColor = UIColor.theme()
            button.setTitleColor(UIColor.theme(), for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
            button.addTarget(self, action: #selector(pressAreaButton(_:)), for: .touchUpInside)
            //button.setImage(UIImage(named: "Img_complete_2"), for: .normal)
            //button.setBackgroundImage(UIImage(named: "Img_complete_2"), for: .normal)
            
            
            self.scrollViewArea.addSubview(button)
            
            xPostion = xPostion + Int(width) + 5
            
        }
        
        scrollViewArea.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 56)
        
        scrollViewArea.contentSize = CGSize(width: xPostion, height: 56)
        
        if Int(UIScreen.main.bounds.size.width) > xPostion {
            
        }else{
            
            let bottomOffset = CGPoint(x: scrollViewArea.contentSize.width - scrollViewArea.bounds.size.width, y: 0)
            scrollViewArea.setContentOffset(bottomOffset, animated: true)
            
        }
        
    }
    
    
    @objc func pressAreaButton(_ button: UIButton) {
        
        print("Button with tag: \(button.tag) clicked!")
        
        if button.tag == 0 {
            
            let subRange = NSMakeRange(button.tag + 1, aryButtonsArea.count - 1)
            
            aryButtonsArea.removeObjects(in: subRange)
            
            addDynamicButtons()
            
            strAddressAreaId = ""
            strMobileAddressAreaId = ""
            strParentAddressAreaId = ""
            strParentAddressAreaName = ""
            strAddressAreaName = ""
            
            /*
            let arrOfAreaLocal = aryButtonsArea[button.tag] as! NSArray
            
            let objArea = arrOfAreaLocal[0] as! NSManagedObject
            
            strAddressAreaId = objArea.value(forKey: "addressAreaId") as! String
            strMobileAddressAreaId = objArea.value(forKey: "mobileAddressAreaId") as! String
            
            strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
            strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
            */
            
            self.fetchAreaSubArea()
            
        } else if button.tag == aryButtonsArea.count - 1 {
            
            
            
        } else{
            
            let subRange = NSMakeRange(button.tag + 1, aryButtonsArea.count - button.tag - 1)
            
            aryButtonsArea.removeObjects(in: subRange)
            
            addDynamicButtons()
            
            let arrOfAreaLocal = aryButtonsArea[button.tag] as! NSArray
            
            let objArea = arrOfAreaLocal[0] as! NSManagedObject
            
            strAddressAreaId = objArea.value(forKey: "addressAreaId") as! String
            strMobileAddressAreaId = objArea.value(forKey: "mobileAddressAreaId") as! String
            strAddressAreaName = objArea.value(forKey: "addressAreaName") as! String

            strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
            strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
            
            self.fetchAreaSubArea()
            
        }
        
    }
    
    func setValuesOnViewLoad() {
        
        //tvlist.setContentOffset(.zero, animated: true)
        
        lblTitleHeader.text = nsud.value(forKey: "lblName") as? String
        arrOfArea = NSArray()
        
        if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "addressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strAddressAreaId,Global().getCompanyKey(), "0"))

                //arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: strAddressAreaId, strMobileAddressAreaId: strMobileAddressAreaId , strWoStatus: "Complete")

            }else{
                
                //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && addressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", strAddressAreaId,Global().getCompanyKey(), "0"))

                arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: strAddressAreaId, strMobileAddressAreaId: strMobileAddressAreaId , strWoStatus: "InComplete")

            }
            
            //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), "0"))
            
        } else if strMobileAddressAreaId.count > 0 {
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "mobileAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strMobileAddressAreaId,Global().getCompanyKey(),"0"))

                //arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: strAddressAreaId, strMobileAddressAreaId: strMobileAddressAreaId , strWoStatus: "Complete")

            }else{
                
                //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", strMobileAddressAreaId,Global().getCompanyKey(),"0"))

                arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: strAddressAreaId, strMobileAddressAreaId: strMobileAddressAreaId , strWoStatus: "InComplete")

            }
            
            //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, strMobileAddressAreaId,Global().getCompanyKey(),"0"))
            
        } else  {
            
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, "",Global().getCompanyKey(),"0"))

                arrOfSubAreas = WebService().fetchSubAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strParentAddressAreaId: "")

            }else{
                
                //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", "",Global().getCompanyKey(),"0"))
                
                arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", "",Global().getCompanyKey(),"0"))

            }

            //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, "",Global().getCompanyKey(),"0"))
            
        }

        if(arrOfArea.count > 0){
            
            aryButtonsArea.add(arrOfArea)
            
            self.addDynamicButtons()
            
            if aryButtonsArea.count > 1{
                
                let objArea = arrOfArea[0] as! NSManagedObject
                
                var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
                let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
                
                if strAddressAreaIdLocal == "0"{
                    
                    strAddressAreaIdLocal = ""
                    
                }
                
                let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
                
                strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
                strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
                
                //strAddressAreaId = idd
                //strAddressAreaName = objArea.value(forKey: "addressAreaName") as! String

                
                if isWoCompleted(objWo: objWorkorderDetail) {
                    
                    arrOfSubAreas = WebService().fetchSubAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strParentAddressAreaId: idd)

                    //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", idd,Global().getCompanyKey(),"0"))

                }else{
                    
                    arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", idd,Global().getCompanyKey(),"0"))

                }
                
                //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, idd,Global().getCompanyKey(),"0"))
                
            }

        }else{
            
            let array = NSMutableArray()

            let dict = NSMutableDictionary()
            dict.setValue("Area", forKey: "addressAreaName")
            array.add(dict)

            aryButtonsArea.add(array)

            self.addDynamicButtons()
            
        }
        
        tvlist.reloadData()
        
    }
    
    func fetchAreaSubArea() {
        
        arrOfArea = NSArray()
        
        if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "addressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strAddressAreaId,Global().getCompanyKey(), "0"))

                //arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: strAddressAreaId, strMobileAddressAreaId: strMobileAddressAreaId , strWoStatus: "Complete")

            }else{
                
                //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && addressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", strAddressAreaId,Global().getCompanyKey(), "0"))

                arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: strAddressAreaId, strMobileAddressAreaId: strMobileAddressAreaId , strWoStatus: "InComplete")

            }
            
            //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), "0"))
            
        } else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0" {
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "mobileAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strMobileAddressAreaId,Global().getCompanyKey(),"0"))

                //arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: strAddressAreaId, strMobileAddressAreaId: strMobileAddressAreaId , strWoStatus: "Complete")

            }else{
                
                //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", strMobileAddressAreaId,Global().getCompanyKey(),"0"))

                arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: strAddressAreaId, strMobileAddressAreaId: strMobileAddressAreaId , strWoStatus: "InComplete")

            }
            
            //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, strMobileAddressAreaId,Global().getCompanyKey(),"0"))
            
        } else  {
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, "",Global().getCompanyKey(),"0"))
                
                arrOfSubAreas = WebService().fetchSubAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strParentAddressAreaId: "")

            }else{
                
                arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", "",Global().getCompanyKey(),"0"))

            }
            
            //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", "",Global().getCompanyKey(),"0"))

            
            //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, "",Global().getCompanyKey(),"0"))
            
        }
        
        if(arrOfArea.count > 0){
            
            if aryButtonsArea.count > 1{

            let objArea = arrOfArea[0] as! NSManagedObject
            
            var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
            let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
            
                if strAddressAreaIdLocal == "0"{
                    
                    strAddressAreaIdLocal = ""
                    
                }
                
            let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
            
            strParentAddressAreaId = objArea.value(forKey: "mobileParentAddressAreaId") as! String
            strParentAddressAreaName = objArea.value(forKey: "parentAreaName") as! String
                
                //strAddressAreaId = idd
                //strAddressAreaName = objArea.value(forKey: "addressAreaName") as! String
            
                if isWoCompleted(objWo: objWorkorderDetail) {
                    
                    //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", idd,Global().getCompanyKey(),"0"))

                    arrOfSubAreas = WebService().fetchSubAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strParentAddressAreaId: idd)

                }else{
                    
                    arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", idd,Global().getCompanyKey(),"0"))
                    
                }
                
            //arrOfSubAreas = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, idd,Global().getCompanyKey(),"0"))
                
            }
            
        }
        
        tvlist.setContentOffset(.zero, animated: true)
        tvlist.reloadData()
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanicaliPhone") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.strTypeOfService = "service"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.strAccountNo = objWorkorderDetail.value(forKey: "accountNo") as? String
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        // workorderStatus
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strModuleType = "ServicePestFlow"
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAreaView(strType : String , strMobileId : String , strServiceId : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddAreaVC") as? AddAreaVC
        testController?.strWoId = strWoId
        //testController?.strParentAddressAreaName = strParentAddressAreaName
        //testController?.strParentAddressAreaId = strParentAddressAreaId
        
        if strType == "Add" {
            
            if aryButtonsArea.count > 1 {
                
                if(arrOfArea.count > 0){
                    
                    let objArea = arrOfArea[0] as! NSManagedObject
                    var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
                    let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
                    let strParentAddressAreaNameLocal = objArea.value(forKey: "addressAreaName") as! String
                    
                    if strAddressAreaIdLocal == "0"{
                        
                        strAddressAreaIdLocal = ""
                        
                    }
                    let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
                    
                    testController?.strParentAddressAreaName = strParentAddressAreaNameLocal
                    testController?.strParentAddressAreaId = idd
                    
                }
                
            }
            
        }
        else if strType == "Update"{
            nsud.setValue("false", forKey:  "CameToAddArea")
        }
        testController?.strMobileAddressAreaId = strMobileId
        testController?.strAddressAreaId = strServiceId
        testController?.strType = strType
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAddDeviceView(strfrom : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDeviceVC") as? AddDeviceVC
        testController?.strWoId = strWoId
        testController?.strFrom = strfrom
        testController?.strParentAddressAreaName = strParentAddressAreaName
        testController?.strParentAddressAreaId = strParentAddressAreaId
        if aryButtonsArea.count > 1 {
            
            if(arrOfArea.count > 0){
                
                let objArea = arrOfArea[0] as! NSManagedObject
                var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
                let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
                let strParentAddressAreaNameLocal = objArea.value(forKey: "addressAreaName") as! String
                
                if strAddressAreaIdLocal == "0"{
                    
                    strAddressAreaIdLocal = ""
                    
                }
                
                let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
                
                testController?.strParentAddressAreaName = strParentAddressAreaNameLocal
                testController?.strParentAddressAreaId = idd
                
            }
            
        }
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToScannerView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "QRScannerController") as? QRScannerController
        testController?.strFromWhere = "AddArea"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToListView(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ListVC") as? ListVC
        testController?.strType = strType
        testController?.strWoId = strWoId
        testController?.strAssociationType = enumArea
        
        if(arrOfArea.count > 0){

            let objArea = arrOfArea[0] as! NSManagedObject
            
            var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
            let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
            let strMobileAddressAreaNameLocal = objArea.value(forKey: "addressAreaName") as! String
            
            if strAddressAreaIdLocal == "0"{
                
                strAddressAreaIdLocal = ""
                
            }
            
            let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
            
            testController?.strAddressAreaId = idd
            testController?.strAddressAreaName = strMobileAddressAreaNameLocal
            
        }

                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToDeviceView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DeviceListVC") as? DeviceListVC
        //testController?.strForUpdate = "No"
        testController?.strWoId = strWoId
        
        if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
            
            testController?.strAddressAreaId = strAddressAreaId
            
        } else {
            
            testController?.strAddressAreaId = strMobileAddressAreaId
            
        }
        
        testController?.strAddressAreaName = strAddressAreaName
        
        testController?.strAreaNameToDisplay = strAddressAreaName
        
        if strAddressAreaName.count > 0 {
            
            testController?.strAreaNameToDisplay = strAddressAreaName
            
        } else {
            
            testController?.strAreaNameToDisplay = enumArea
            
        }
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToDeviceDetailView(strMobileDeviceId : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DeviceDetailVC") as? DeviceDetailVC
        testController?.strWoId = strWoId
        testController?.strMobileDeviceId = strMobileDeviceId
        testController?.strParentAddressAreaId = strParentAddressAreaId
        testController?.strParentAddressAreaName = strParentAddressAreaName
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func updateMarkAsInspected(isInspectedd : Bool , strType : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("isInspected")
        arrOfValues.add(isInspectedd)
        
        if strType == "FirstTime" {
            
            arrOfKeys.add("isInspectingFirstTime")
            arrOfValues.add(false)
            
        }
        
        if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
            
            //let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@", strWoId, strAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "addressAreaId == %@ && companyKey == %@", strAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        } else {
            
            //let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileAddressAreaId == %@ && companyKey == %@", strWoId, strMobileAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "mobileAddressAreaId == %@ && companyKey == %@", strMobileAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        }
        
        self.fetchAreaSubArea()
        
    }
    
    @objc func markAsInspected(sender: UIButton) {
        
        if sender.currentImage == UIImage(named: "check_box_2.png") {
            
            sender.setImage(UIImage(named: "check_box_1.png"), for: .normal)

            self.updateMarkAsInspected(isInspectedd: false, strType: "")
            
        } else {
            
            self.updateMarkAsInspected(isInspectedd: true, strType: "")
            
            sender.setImage(UIImage(named: "check_box_2.png"), for: .normal)

        }
        
    }
    
    
    func getAreaCount(strIdd : String , strWoId : String) -> NSMutableArray {
        
        if strIdd == "S" {
            
            return arrTempCount
            
        }
        
        let arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, strIdd,Global().getCompanyKey(),"0"))
        
        if arrOfNoArea.count > 0 {
            
            for k in 0 ..< arrOfNoArea.count {
                
                let dictData = arrOfNoArea[k] as! NSManagedObject
                
                var strAddressAreaIdLocal = dictData.value(forKey: "addressAreaId") as! String
                let strMobileAddressAreaIdLocal = dictData.value(forKey: "mobileAddressAreaId") as! String
                
                if strAddressAreaIdLocal == "0"{
                    
                    strAddressAreaIdLocal = ""
                    
                }
                
                let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
                
                arrTempCount.add("tt")
                
                let arrTempCountttttt =  getAreaCount(strIdd: idd, strWoId: strWoId)
                
                //return arrTempCount
                
            }
            
        }else{
            
            let arrTempCountttttt = getAreaCount(strIdd: "S", strWoId: strWoId)
            
            //return arrTempCount
            
        }
        
        return arrTempCount
        
    }
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AreaDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 50
            
        } else {
            
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
            var isInspected = false
            
            var strHeader = String()
            
            if( aryButtonsArea.count > 1){
                
                if( arrOfArea.count > 0){
                    
                    let objArea = arrOfArea[0] as! NSManagedObject
                    
                    //strHeader = "\(objArea.value(forKey: "NoOfAreas")!) Area " + "\(objArea.value(forKey: "NoOfDevices")!) Device " + "\(objArea.value(forKey: "NoOfConditions")!) Conditions"
                    
                    strHeader = objArea.value(forKey: "addressAreaName") as! String
                    
                    isInspected = objArea.value(forKey: "isInspected") as! Bool
                    
                }else{
                    
                    
                }

            }else{
                
                strHeader = enumArea
                isInspected = false
                
            }

            
            let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let btn = UIButton(frame: CGRect(x: tableView.frame.width-40, y: 7, width: 36, height: 36))
            
            if isInspected {
                
                btn.setImage(UIImage(named: "check_box_2.png"), for: .normal)
                
            }else{
                
                btn.setImage(UIImage(named: "check_box_1.png"), for: .normal)
                
            }
            
            if !isWoCompleted(objWo: objWorkorderDetail) {
                
                btn.addTarget(self, action: #selector(markAsInspected), for: .touchUpInside)

            }
            
            //btn.addTarget(self, action: #selector(markAsInspected), for: .touchUpInside)
            
            if( aryButtonsArea.count > 1){
            vw.addSubview(btn)
            }
                
            let lbl = UILabel(frame: CGRect(x: btn.frame.origin.x-80, y: 0, width: 80, height: 50))
            lbl.text = "Inspected"
            lbl.font = UIFont.boldSystemFont(ofSize: 16)
            lbl.textColor = UIColor.theme()
            
            if( aryButtonsArea.count > 1){
            vw.addSubview(lbl)
            }
            
            let widthh = UIScreen.main.bounds.size.width - (lbl.frame.size.width + btn.frame.size.width + 20)
            
            let lblHeader = UILabel()
            lblHeader.frame = CGRect(x: 10, y: 0, width: widthh, height: 50)
            lblHeader.text = strHeader
            lblHeader.font = UIFont.systemFont(ofSize: 20)
            vw.addSubview(lblHeader)
            
            return vw
            
        } else {
            
            let strHeader = ""
            
            
            let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 0))
            //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let lblHeader = UILabel()
            lblHeader.frame = vw.frame
            lblHeader.text = strHeader
            lblHeader.font = UIFont.systemFont(ofSize: 20)
            vw.addSubview(lblHeader)
            
            return vw
            
        }

    }
    
    // set view for footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return arrOfSubAreas.count
            
        } else {
            
            return 6
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "AreaDetail1Cell", for: indexPath as IndexPath) as! AreaDetail1Cell
            
            let objArea = arrOfSubAreas[indexPath.row] as! NSManagedObject
            
            var strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
            let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
            
            if strAddressAreaIdLocal == "0"{
                
                strAddressAreaIdLocal = ""
                
            }
            
            let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal

            var arrOfNoDevices = NSArray()
            var arrOfNoConditions = NSArray()
            var arrOfNoArea = NSArray()
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                arrOfNoDevices = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && mobileAddressAreaId == %@ && companyKey == %@ && isDeletedDevice == %@", strWoId, idd,Global().getCompanyKey(), "0"))

            }else{
                
                arrOfNoDevices = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileAddressAreaId == %@ && companyKey == %@ && isDeletedDevice == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", idd,Global().getCompanyKey(), "0"))

            }
            
            //arrOfNoDevices = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@ && isDeletedDevice == %@", strWoId, idd,Global().getCompanyKey(), "0"))
            
            arrOfNoConditions = getDataFromCoreDataBaseArray(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@", strWoId, idd,Global().getCompanyKey()))
            
            arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, idd,Global().getCompanyKey(),"0"))
            
            var arrOfNoArea1 = NSMutableArray()

            arrTempCount = NSMutableArray()

            arrOfNoArea1 = getAreaCount(strIdd: idd, strWoId: strWoId as String)
            
            let strNoOfArea = "\(arrOfNoArea1.count)"
            let strNoOfDevices = "\(arrOfNoDevices.count)"
            let strNoOfConditions = "\(arrOfNoConditions.count)"
            
            let strlblSubTitle = "\(strNoOfArea) Area, " + "\(strNoOfDevices) Device, " + "\(strNoOfConditions) Conditions"

            cell.lblTitle.text = objArea.value(forKey: "addressAreaName") as? String
            cell.lblSubTitle.text = strlblSubTitle
            
            tvlist.separatorColor = UIColor.clear

            return cell
            
        } else {
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "AreaDetailCell", for: indexPath as IndexPath) as! AreaDetailCell
            
           if indexPath.row == 0{
            
            var arrOfNoDevices = NSArray()
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "Complete" , strToCheckBlankAddressId: "No")

                if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                    
                    arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String,strAddressDeviceId: "" ,strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressExist")

                }else{
                    
                    if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0"{
                        
                        arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: strMobileAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressExist")

                    }else{
                        
                        arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: "" , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressblankOnComplete")

                    }
                    
                }
                
            }else{
                
                if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                    
                    arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: strAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "No")

                }else{
                    
                    if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0"{
                        
                        arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: strMobileAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "No")

                    }else{
                        
                        arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: strMobileAddressAreaId , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressblank")

                    }
                    
                }
                
            }
            
            let strNoOfDevices = "\(arrOfNoDevices.count)"
            
            let strlblSubTitle = "\(strNoOfDevices) " + enumDevice
            
                cell.lblTitle.text = enumDevice
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "device_1")

            } else if indexPath.row == 1{
            
            var idToCheck = ""
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                
                idToCheck = strAddressAreaId
                
            }else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0"{
                
                idToCheck = strMobileAddressAreaId
                
            }else {
                
                idToCheck = ""
                
            }
            
            let  arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, idToCheck,Global().getCompanyKey(), enumArea))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumMaterial
            
                cell.lblTitle.text = enumMaterial
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "chemical_1")

            }else if indexPath.row == 2{
            
            var idToCheck = ""
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                
                idToCheck = strAddressAreaId
                
            }else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0"{
                
                idToCheck = strMobileAddressAreaId
                
            }else {
                
                idToCheck = ""
                
            }
            
            let  arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, idToCheck,Global().getCompanyKey(), enumArea))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumPest
            
                cell.lblTitle.text = enumPest
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "pest_1")

            } else if indexPath.row == 3{
            
            var idToCheck = ""
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                
                idToCheck = strAddressAreaId
                
            }else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0"{
                
                idToCheck = strMobileAddressAreaId
                
            }else {
                
                idToCheck = ""
                
            }
            
            let  arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@", strWoId, idToCheck,Global().getCompanyKey()))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumCondition
            
                cell.lblTitle.text = enumCondition
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "task_1")

            } else if indexPath.row == 4{
            // saavan
            
            var idToCheck = ""
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                
                idToCheck = strAddressAreaId
                
            }else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0"{
                
                idToCheck = strMobileAddressAreaId
                
            }else {
                
                idToCheck = ""
                
            }
            
            let  arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, idToCheck,Global().getCompanyKey(), enumArea))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumComment
            
                cell.lblTitle.text = enumComment
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "message_1")

            }else {
            
            var idToCheck = ""
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                
                idToCheck = strAddressAreaId
                
            }else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0"{
                
                idToCheck = strMobileAddressAreaId
                
            }else {
                
                idToCheck = ""
                
            }
            
            let arrOfAllData =  getDataFromCoreDataBaseArray(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@", strWoId, idToCheck,Global().getCompanyKey()))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumDocument
            
                cell.lblTitle.text = enumDocument
                cell.lblSubTitle.text = strlblSubTitle
                cell.imgView.image = UIImage(named: "attatch_1")

            }
            
            tvlist.separatorColor = UIColor.clear
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {

            let objArea = arrOfSubAreas[indexPath.row] as! NSManagedObject
            
            strAddressAreaId = objArea.value(forKey: "addressAreaId") as! String
            strMobileAddressAreaId = objArea.value(forKey: "mobileAddressAreaId") as! String
            strAddressAreaName = objArea.value(forKey: "addressAreaName") as! String

            //isInspectingFirstTime
            
            if objArea.value(forKey: "isInspectingFirstTime") as! Bool {
                
                updateMarkAsInspected(isInspectedd: true, strType: "FirstTime")
                
            }
            
            self.setValuesOnViewLoad()
            
        }else{
            
            if indexPath.row == 0{
                
                self.goToDeviceView()
                
            } else if indexPath.row == 1{
                
                self.goToListView(strType: enumMaterial)
                
            }else if indexPath.row == 2{
                
                self.goToListView(strType: enumPest)
                
            } else if indexPath.row == 3{
                
                self.goToListView(strType: enumCondition)
                
            } else if indexPath.row == 4{
                
                self.goToListView(strType: enumComment)
                
            }else {
                
                self.goToListView(strType: enumDocument)
                
            }
            
        }
        
    }
    
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     
        if indexPath.section == 0 {
            
            if !isWoCompleted(objWo: objWorkorderDetail) {
                
                return true

            }else{
                
                return false

            }
            
        }else{
            
            return false
            
        }
     
     }
     
     func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
     
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                
                let objArea = self.arrOfSubAreas[indexPath.row] as! NSManagedObject
                
                let strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
                let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
                
                if strAddressAreaIdLocal.count > 0 && strAddressAreaIdLocal != "0" {
                    
                    // isDeletedArea ko true krna hai yaha par
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("isDeletedArea")
                    arrOfValues.add(true)
                    
                    //let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@", self.strWoId, strAddressAreaIdLocal,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "addressAreaId == %@ && companyKey == %@", strAddressAreaIdLocal,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                    if isSuccess {
                        
                        
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                    /*
                     
                    // Deleting Main Area
                    deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@", self.strWoId, strAddressAreaIdLocal,Global().getCompanyKey()))
                    
                    // Deleting Sub Area Area
                    deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@", self.strWoId, strAddressAreaIdLocal,Global().getCompanyKey()))
                     
                     */

                } else {
                    
                    
                    // isDeletedArea ko true krna hai yaha par
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("isDeletedArea")
                    arrOfValues.add(true)
                    
                    //let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileAddressAreaId == %@ && companyKey == %@", self.strWoId, strMobileAddressAreaIdLocal,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "mobileAddressAreaId == %@ && companyKey == %@", strMobileAddressAreaIdLocal,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                    if isSuccess {
                        
                        
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                    /*
                    
                    // Deleting Main Area
                    deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileAddressAreaId == %@ && companyKey == %@", self.strWoId, strMobileAddressAreaIdLocal,Global().getCompanyKey()))
                    
                    // Deleting Sub Area Area
                    deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@", self.strWoId, strMobileAddressAreaIdLocal,Global().getCompanyKey()))

                    */
                    
                }
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                self.setValuesOnViewLoad()
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
            })
            
        }
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            
            let objArea = self.arrOfSubAreas[indexPath.row] as! NSManagedObject
            
            let strAddressAreaIdLocal = objArea.value(forKey: "addressAreaId") as! String
            let strMobileAddressAreaIdLocal = objArea.value(forKey: "mobileAddressAreaId") as! String
            
            self.goToAreaView(strType: "Update", strMobileId: strMobileAddressAreaIdLocal, strServiceId: strAddressAreaIdLocal)
            
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [edit]
     
     }
    
    
}

// MARK: - ----------------Cell
// MARK: -

class AreaDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}


class AreaDetail1Cell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}
