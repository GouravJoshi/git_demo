//
//  AddAreaVC.swift
//  DPS
//
//  Created by Saavan Patidar on 09/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
// Saavan S

import UIKit

class AddAreaVC: UIViewController {
    
    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var txtTitle: ACFloatingTextField!
    @IBOutlet weak var txtBarCode: ACFloatingTextField!
    @IBOutlet weak var txtArea: ACFloatingTextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnAddArea: UIButton!
    @IBOutlet weak var btnBarcode: UIButton!
    @IBOutlet weak var lblTitleHeader: UILabel!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var viewDocumentHeader: UIView!

    @IBOutlet weak var tblDocument: UITableView!
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    var strWoId = NSString ()
    var strWoNo = NSString ()
    var strParentAddressAreaId = String ()
    var strParentAddressAreaName = String ()
    var strType = String ()
    var strMobileAddressAreaId = String ()
    var strAddressAreaId = String ()
    var arrOfArea = NSArray()
    var objWorkorderDetail = NSManagedObject()
    var arrTempCount = NSMutableArray()
    var arrOfAllData = NSArray()
    var strFrom = String ()
    var strURL = String()
    
    // MARK: - -----------------------------------Life - Cycle -----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBorderColor(item: txtViewDesc)
        let strToAddDevice = nsud.value(forKey: "CameToAddArea")
        if strToAddDevice as? String ?? "" == "true"{
            
            viewDocumentHeader.isHidden = true
            tblDocument.isHidden = true

        }
        else{
            
            viewDocumentHeader.isHidden = false
            tblDocument.isHidden = false
        }
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            nsud.setValue("false", forKey: "CameToAddArea")
            dismiss(animated: false)
            
        }
        
        // Do any additional setup after loading the view.
        
        //txtArea.addTarget(self, action: #selector(selectAreaFunction), for: .allEvents)
        
        if strParentAddressAreaName.count > 0 {
            
            txtArea.text = strParentAddressAreaName
            
        }
        
        if strType == "Update" {
            
            self.setValuesOnUpdateArea()
            
            lblTitleHeader.text = "Edit Area"
            
        }else{
            
            lblTitleHeader.text = "Add Area"
            
        }
        
        
        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
            
            strURL = "\(value)"
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchDataFrmDB()
        
        if (nsud.value(forKey: "fromScanner") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "fromScanner")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "fromScanner")
                nsud.synchronize()
                
                txtBarCode.text = nsud.value(forKey: "scannedCode") as? String
                
            }
            
        }
        
        if (nsud.value(forKey: "fromArea") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "fromArea")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "fromArea")
                nsud.synchronize()
                
                var arrOfNoArea1 = NSMutableArray()
                
                arrTempCount = NSMutableArray()
                
                if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
                    
                    arrOfNoArea1 = getAreaCount(strIdd: strAddressAreaId, strWoId: strWoId as String)
                    
                } else {
                    
                    arrOfNoArea1 = getAreaCount(strIdd: strMobileAddressAreaId, strWoId: strWoId as String)
                    
                }
                
                // Check If Same Parent Area selected as of area
                
                if (nsud.value(forKey: "strSelectedAreaId") as? String)! == strAddressAreaId {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_AreaAndParentAreaSame, viewcontrol: self)
                    
                }else if (nsud.value(forKey: "strSelectedAreaId") as? String)! == strMobileAddressAreaId{
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_AreaAndParentAreaSame, viewcontrol: self)
                    
                }else if arrTempCount.contains((nsud.value(forKey: "strSelectedAreaId") as? String)!) {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_ParentArea, viewcontrol: self)
                    
                }else{
                    
                    txtArea.text = nsud.value(forKey: "strSelectedAreaName") as? String
                    strParentAddressAreaId = (nsud.value(forKey: "strSelectedAreaId") as? String)!
                    strParentAddressAreaName = (nsud.value(forKey: "strSelectedAreaName") as? String)!
                    
                }
                
                //                txtArea.text = nsud.value(forKey: "strSelectedAreaName") as? String
                //                strParentAddressAreaId = (nsud.value(forKey: "strSelectedAreaId") as? String)!
                //                strParentAddressAreaName = (nsud.value(forKey: "strSelectedAreaName") as? String)!
                
                
            }
            
        }
        
        
    }
    
    
    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        nsud.setValue("false", forKey: "CameToAddArea")
        dismiss(animated: false)
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        txtViewDesc.endEditing(true)
        
        if strType == "Update" {
            
            let objArea = arrOfArea[0] as! NSManagedObject
            
            if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
                
                self.saveArea(strMobileAddressAreaId: objArea.value(forKey: "addressAreaId") as! String)
                
            } else {
                
                self.saveArea(strMobileAddressAreaId: objArea.value(forKey: "mobileAddressAreaId") as! String)
                
            }
            
        } else {
            
            self.saveArea(strMobileAddressAreaId: Global().getReferenceNumber())
            
        }
        
        //        nsud.set(true, forKey: "AreaAdded")
        //        nsud.synchronize()
        //        dismiss(animated: false)
        
    }
    
    @IBAction func action_Barcode(_ sender: Any) {
        
        self.view.endEditing(true)
        self.goToScannerView()
        
    }
    
    @IBAction func action_SelectArea(_ sender: Any) {
        
        self.view.endEditing(true)
        self.goToSelectAreaView()
        
    }
    
    
    // MARK: - -----------------------------------Functions -----------------------------------
    
    func checkIfAreaAlreadyAddedOnServiceAddressId() -> Bool {
        
        let  arrOfAreaLocal = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@ && addressAreaName == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(),"0",txtTitle.text!))
        
        if arrOfAreaLocal.count > 0 {
            
            if strType == "Update" {
                
                // addressAreaId mobileAddressAreaId
                
                for k in 0 ..< arrOfAreaLocal.count {
                    
                    if arrOfAreaLocal[k] is NSManagedObject {
                        
                        let dictOfData = arrOfAreaLocal[k] as! NSManagedObject
                        
                        
                        let strId = "\(dictOfData.value(forKey: "addressAreaId") ?? "")"
                        let strMobileId = "\(dictOfData.value(forKey: "mobileAddressAreaId") ?? "")"
                        
                        if strId != "0" && strId.count > 0 {
                            
                            if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
                                
                                if strId == strAddressAreaId {
                                    
                                    
                                    return false
                                    
                                }
                                
                            } else {
                                
                                if strId == strMobileAddressAreaId {
                                    
                                    return false
                                    
                                }
                                
                            }
                            
                        }else{
                            
                            if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
                                
                                if strMobileId == strAddressAreaId {
                                    
                                    
                                    return false
                                    
                                }
                                
                            } else {
                                
                                if strMobileId == strMobileAddressAreaId {
                                    
                                    return false
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                
            }
            
            return true
            
        } else {
            
            return false
            
        }
        
    }
    
    func setValuesOnUpdateArea() {
        
        if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
            
            arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "addressAreaId == %@ && companyKey == %@", strAddressAreaId,Global().getCompanyKey()))

        } else {
            
            arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "mobileAddressAreaId == %@ && companyKey == %@", strMobileAddressAreaId,Global().getCompanyKey()))

        }
        
        if arrOfArea.count > 0 {
            
            let objArea = arrOfArea[0] as! NSManagedObject

            txtTitle.text = objArea.value(forKey: "addressAreaName") as? String
            txtBarCode.text = objArea.value(forKey: "barcode") as? String
            txtViewDesc.text = objArea.value(forKey: "serviceAreaDescription") as? String
            strParentAddressAreaId = (objArea.value(forKey: "mobileParentAddressAreaId") as? String)!
            strParentAddressAreaName = (objArea.value(forKey: "parentAreaName") as? String)!
            //parentAddressAreaId
            txtArea.text = strParentAddressAreaName

        }
        
    }
    
    @objc func selectAreaFunction(textField: UITextField) {
        
        self.goToSelectAreaView()
        
    }
    
    func goToScannerView()  {
        
        self.view.endEditing(true)
        txtViewDesc.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "QRScannerController") as? QRScannerController
        testController?.strFromWhere = "AddArea"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToSelectAreaView()  {
        
        self.view.endEditing(true)
        
        txtViewDesc.endEditing(true)
        
        self.view.endEditing(true)

        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SelectAreaVC") as? SelectAreaVC
        testController?.strWoId = strWoId
        if strMobileAddressAreaId.count > 0 || strMobileAddressAreaId != ""{
            testController?.strCurrentAddressId = strMobileAddressAreaId
        }
        else{
            testController?.strCurrentAddressId = strAddressAreaId
        }
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func saveArea(strMobileAddressAreaId : String) {
        
        if (txtTitle.text?.count)! > 0 {
            
            // check if device already exist
            
            let isAreaExist = self.checkIfAreaAlreadyAddedOnServiceAddressId()
            
            if isAreaExist {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_AreaExist, viewcontrol: self)
                
            }else{
             
                //let strMobileAddressAreaId = Global().getReferenceNumber()!
                let strAddressAreaName = txtTitle.text!
                let strParentAddressAreaId = self.strParentAddressAreaId
                let strParentAddressAreaName = self.strParentAddressAreaName
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("mobileAddressAreaId")
                arrOfKeys.add("addressAreaName")
                arrOfKeys.add("mobileParentAddressAreaId")
                arrOfKeys.add("workOrderId")
                arrOfKeys.add("barcode")
                arrOfKeys.add("serviceAddressId")
                arrOfKeys.add("isActive")
                arrOfKeys.add("noOfAreas")
                arrOfKeys.add("noOfDevices")
                arrOfKeys.add("noOfConditions")
                arrOfKeys.add("parentAreaName")
                arrOfKeys.add("serviceAreaDescription")
                
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strMobileAddressAreaId)
                arrOfValues.add(strAddressAreaName)
                arrOfValues.add(strParentAddressAreaId.count > 0 ? strParentAddressAreaId : "")
                arrOfValues.add(strWoId)
                arrOfValues.add((txtBarCode.text?.count)! > 0 ? txtBarCode.text! : "")
                arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")")
                arrOfValues.add(true)
                
                arrOfValues.add("0")
                arrOfValues.add("0")
                arrOfValues.add("0")
                arrOfValues.add(strParentAddressAreaName.count > 0 ? strParentAddressAreaName : "")
                arrOfValues.add((txtViewDesc.text?.count)! > 0 ? txtViewDesc.text! : "")
                
                if strType == "Update" {
                    
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    arrOfValues.add(Global().getEmployeeId())
                    arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy HH:mm:ss", ""))
                    
                    if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
                        
                        //let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@", strWoId, strAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "addressAreaId == %@ && companyKey == %@", strAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                        if isSuccess {
                            nsud.setValue("false", forKey: "CameToAddArea")
                            dismiss(animated: false)
                            
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        
                    } else {
                        
                        //let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileAddressAreaId == %@ && companyKey == %@", strWoId, strMobileAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "mobileAddressAreaId == %@ && companyKey == %@", strMobileAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                        if isSuccess {
                            nsud.setValue("false", forKey: "CameToAddArea")
                            dismiss(animated: false)
                            
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        
                    }
                    
                }else{
                    
                    arrOfKeys.add("isInspected")
                    arrOfKeys.add("isInspectingFirstTime")
                    arrOfKeys.add("addressAreaId")
                    arrOfKeys.add("isDeletedArea")
                    
                    arrOfValues.add(false)
                    arrOfValues.add(true)
                    arrOfValues.add("0")
                    arrOfValues.add(false)
                    
                    
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    arrOfValues.add(Global().getEmployeeId())
                    arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                    arrOfValues.add(Global().getEmployeeId())
                    arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy HH:mm:ss", ""))
                    
                    saveDataInDB(strEntity: "ServiceAreas", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    // Saving Info In WO Service Area Table
                    
                    saveWoArea(strMobileServiceAreaIdDB: strMobileAddressAreaId)
                    nsud.setValue("false", forKey: "CameToAddArea")
                    dismiss(animated: false)
                    
                }
                
                //nsud.set(true, forKey: "AreaAdded")
                //nsud.synchronize()
                //dismiss(animated: false)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: strWoId as String)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_AreaTitle, viewcontrol: self)
            
        }
        
    }
    
    func saveWoArea(strMobileServiceAreaIdDB : String)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("mobileServiceAreaId")
        arrOfKeys.add("serviceAreaId")
        arrOfKeys.add("wOServiceAreaId")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add(strMobileServiceAreaIdDB)
        arrOfValues.add("0")
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "WOServiceAreas", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    
    func getAreaCount(strIdd : String , strWoId : String) -> NSMutableArray {
        
        if strIdd == "S" {
            
            return arrTempCount
            
        }
        
        var arrOfNoArea = NSArray()
        
        arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@ && isDeletedArea == %@", strWoId, strIdd,Global().getCompanyKey(),"0"))

        if strIdd == "" {
            
            arrOfNoArea = NSArray()
            
        }
        
        if arrOfNoArea.count > 0 {
            
            for k in 0 ..< arrOfNoArea.count {
                
                let dictData = arrOfNoArea[k] as! NSManagedObject
                
                var strAddressAreaIdLocal = dictData.value(forKey: "addressAreaId") as! String
                let strMobileAddressAreaIdLocal = dictData.value(forKey: "mobileAddressAreaId") as! String
                
                if strAddressAreaIdLocal == "0" {
                    
                    strAddressAreaIdLocal = ""
                    
                }
                
                let idd = strAddressAreaIdLocal.count > 0 ? strAddressAreaIdLocal : strMobileAddressAreaIdLocal
                
                arrTempCount.add(idd)
                
                let arrTempCountttttt =  getAreaCount(strIdd: idd, strWoId: strWoId)
                
            }
            
        }else{
            
            let arrTempCountttttt = getAreaCount(strIdd: "S", strWoId: strWoId)
            
        }
        
        return arrTempCount
        
    }

    
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension AddAreaVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtTitle  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "CHARNUMBER", limitValue: 100)
            
        } else if textField == txtBarCode  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        } else if textField == txtArea  {
            
            return false
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}

// MARK: - ----------Add Documents

extension AddAreaVC {
    @IBAction func action_AddDocument(_ sender: Any) {
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddAreaDocumentVC") as? AddAreaDocumentVC
        testController?.strWoId = strWoId as String
        testController?.strWoNo = strWoNo as String
        testController?.strFrom = "Area"
        
        if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
            testController?.strConditionId = strAddressAreaId
        }
        else{
            testController?.strConditionId = strMobileAddressAreaId
        }
        
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    
    func fetchDataFrmDB()  {
        var strAreId = ""
        if strAddressAreaId.count > 0 && strAddressAreaId != "0" {
            strAreId = strAddressAreaId
        }
        else{
            strAreId = strMobileAddressAreaId
        }
        arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@ && addressAreaId == %@", strWoId, Global().getCompanyKey(), strAreId))
        print(arrOfAllData)
        tblDocument.reloadData()
        
    }
    
    func goToRecordView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
        testController?.strFromWhere = "ServiceInvoice"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToSelectDocument()  {
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDocumentVC") as? AddDocumentVC
        testController?.strWoId = strWoId as String
        testController?.strFrom = strFrom
        testController?.strAddressAreaId = strAddressAreaId
        //        testController?.strAddressAreaName = strAddressAreaName
        //        testController?.strConditionId = strConditionId
        //        testController?.strAssociationType = strAssociationType
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToImageView(image : UIImage) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
        testController!.img = image
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToPdfView(strPdfName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func playAudioo(strAudioName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
}

// MARK: - ----------------UITableViewDelegate
extension AddAreaVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOfAllData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblDocument.dequeueReusableCell(withIdentifier: "DocumentListCell", for: indexPath as IndexPath) as! DocumentListCell
        
        let objArea = arrOfAllData[indexPath.row] as! NSManagedObject
        
        var strDocPathLocal = String()
        var strDocTypeLocal = String()
        
        
        cell.lblTitle.text = "\(objArea.value(forKey: "documnetTitle") ?? "N/A")"
        strDocPathLocal = "\(objArea.value(forKey: "documentPath") ?? "")"
        strDocTypeLocal = "\(objArea.value(forKey: "documentType") ?? "")"
        
        
        if strDocTypeLocal == "image" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if image != nil && strImagePath?.count != 0 && isImageExists! {
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                
                cell.imgView.image = image
                
                // kch nhi krna
                
            }else {
                
                strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgView.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath! , image: image)
                            
                            cell.imgView.image = image
                            
                        }}
                    
                }
                
            }
            
            
        } else if strDocTypeLocal == "pdf" {
            
            let image: UIImage = UIImage(named: "exam")!
            
            cell.imgView.image = image
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                // kch nhi krna
                
            }else {
                
                strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        DispatchQueue.main.async {
                            
                            savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                            
                        }}
                    
                }
                
            }
            
        } else if strDocTypeLocal == "audio" {
            
            let image: UIImage = UIImage(named: "audio")!
            
            cell.imgView.image = image
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                // kch nhi krna
                
            }else {
                
                strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        DispatchQueue.main.async {
                            
                            savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                            
                        }}
                    
                }
                
            }
            
        }else if strDocTypeLocal == "video" {
            
            let image: UIImage = UIImage(named: "video")!
            
            cell.imgView.image = image
            
        }
        else{
            
            strDocTypeLocal = "image"
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if image != nil && strImagePath?.count != 0 && isImageExists! {
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                
                cell.imgView.image = image
                
                // kch nhi krna
                
            }else {
                
                strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgView.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath! , image: image)
                            
                            cell.imgView.image = image
                            
                        }}
                    
                }
                
            }
            
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //documentType
        
        var strDocPathLocal = String()
        var strDocTypeLocal = String()
        
        
        // documentPath
        let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
        strDocPathLocal = "\(objArea.value(forKey: "documentPath") ?? "")"
        strDocTypeLocal = "\(objArea.value(forKey: "documentType") ?? "")"
        
        
        if strDocTypeLocal == "image" {
            
            let image1: UIImage = UIImage(named: "NoImage.jpg")!
            var imageView = UIImageView(image: image1)
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                imageView = UIImageView(image: image1)
                
            }
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        } else if strDocTypeLocal == "pdf" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            goToPdfView(strPdfName: strImagePath!)
            
        } else if strDocTypeLocal == "audio" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                playAudioo(strAudioName: strImagePath!)
                
            }else {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                
            }
            
        }else if strDocTypeLocal == "video" {
            let strVideoPath = Global().strDocName(fromPath: strDocPathLocal)
            
            
            let isVideoExists: Bool? = checkIfImageExistAtPath(strFileName: strVideoPath!)
            
            if isVideoExists!  {
                
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let documentsDirectory = paths[0]
                let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strVideoPath!)")
                print(path)
                let player = AVPlayer(url: path)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
                
            }else {
                
                var vedioUrl = strURL + "\(strDocPathLocal)"
                
                vedioUrl = vedioUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let player = AVPlayer(url: URL(string: vedioUrl)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
                
            }
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            return false
            
        }else{
            
            return true
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                
                
                // documentPath
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                let strServiceConditionIdLocal = objArea.value(forKey: "serviceAreaDocumentId") as! String
                let strDocPathLocal = objArea.value(forKey: "documentPath") as! String
                
                deleteAllRecordsFromDB(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@ && serviceAreaDocumentId == %@", self.strWoId,Global().getCompanyKey(), strServiceConditionIdLocal))
                
                // deleting data from Document Directory also
                let strDocNameToRemove = Global().strDocName(fromPath: strDocPathLocal)
                
                removeImageFromDirectory(itemName: strDocNameToRemove!)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                self.fetchDataFrmDB()
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
            })
            
        }
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            self.goToSelectDocument()
            
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [delete]
    }
}
