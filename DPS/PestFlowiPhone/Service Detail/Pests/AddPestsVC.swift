//
//  AddPestsVC.swift
//  DPS
//
//  Created by Saavan Patidar on 12/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

/*
[Description("Evidence")]
Evidence = 3,
[Description("Sighted")]
Sighted = 2,
[Description("Captured")]
Captured = 1,
*/


import UIKit

class AddPestsVC: UIViewController {

    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var txtQty: ACFloatingTextField!
    @IBOutlet weak var txtTargets: ACFloatingTextField!
    @IBOutlet var btnSlectActions: UIButton!

    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc var strWoId = NSString ()
    @objc var objWorkorderDetail = NSManagedObject()
    var strForUpdate = String ()
    var strPestStatus = String ()
    var strMobileServiceAddressPestId = String ()
    var strServiceAddressPestId = String ()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var strAssociationType = String ()
    var dictDataOfTargetSelected = NSDictionary ()
    var strDeviceId = String ()
    var strMobileDeviceId = String ()
    
    // MARK: - -----------------------------------Life - Cycle -----------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtTargets.addTarget(self, action: #selector(selectTargets), for: .allEvents)

        // Do any additional setup after loading the view.
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        if strForUpdate == "Yes" {
            
            self.setValuesToUpdate()
            
        } else {
            
            self.btnSlectActions.setTitle("Captured", for: .normal)
            self.txtQty.placeholder = "Quantity Caught*"
            self.strPestStatus = "1"
            
            self.perform(#selector(selectGroupMasters), with: nil, afterDelay: 0.1)
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
    }
    
    
    // MARK: - -----------------------------------Actions -----------------------------------
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        
        //ServicePests
        
        if strForUpdate == "Yes" {
            
            if strServiceAddressPestId.count > 0 && strServiceAddressPestId != "0" {
                
                self.savePestDB(strMobileServiceAddressPestId: "", strServiceAddressPestId: strServiceAddressPestId)

            }else{
                
                self.savePestDB(strMobileServiceAddressPestId: strMobileServiceAddressPestId, strServiceAddressPestId: "0")

            }
            
        } else {
            
            self.savePestDB(strMobileServiceAddressPestId: Global().getReferenceNumber(), strServiceAddressPestId: "0")
            
        }
        
    }
    
    @IBAction func action_SelectActions(_ sender: Any) {
        
        self.view.endEditing(true)

        let alert = UIAlertController(title: "", message: "Please Select Action", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Captured", style: .default , handler:{ (UIAlertAction)in
            
            self.btnSlectActions.setTitle("Captured", for: .normal)
            self.txtQty.placeholder = "Quantity Caught*"
            self.strPestStatus = "1"
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Sighted", style: .default , handler:{ (UIAlertAction)in
          
            self.btnSlectActions.setTitle("Sighted", for: .normal)
            self.txtQty.placeholder = "Quantity Sighted*"
            self.strPestStatus = "2"

        }))
        
        
        alert.addAction(UIAlertAction(title: "Evidence", style: .default , handler:{ (UIAlertAction)in
            
            self.btnSlectActions.setTitle("Evidence", for: .normal)
            self.txtQty.placeholder = "Evidence*"
            self.strPestStatus = "3"

            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }

    // MARK: - -----------------------------------Functions -----------------------------------
    
    @objc func selectTargets(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        self.view.endEditing(true)
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "targetGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            self.goToSelectionVC(array: array, arrayList: array , strTitleSelctionVC: "Targets Groups" , strTitleSelctionClass: "Structural Condition" , strTagg: 104)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    func savePestDB(strMobileServiceAddressPestId : String , strServiceAddressPestId : String) {
        
        if txtTargets.text!.count < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectTarget, viewcontrol: self)
            
        } else if txtQty.text!.count < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectQty, viewcontrol: self)
            
        } else {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("mobileServiceAddressPestId")
            arrOfKeys.add("serviceAddressPestId")
            arrOfKeys.add("mobileAssociationId")
            arrOfKeys.add("associationType")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("serviceAddressId")
            arrOfKeys.add("pestStatus")
            arrOfKeys.add("quantity")
            arrOfKeys.add("targetGroupId")
            arrOfKeys.add("targetId")
            arrOfKeys.add("targetName")
            arrOfKeys.add("parentAreaName")
            arrOfKeys.add("evidence")

            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strMobileServiceAddressPestId)
            arrOfValues.add(strServiceAddressPestId)
            
            if strAssociationType == "Device"{
                
                if strDeviceId.count > 0 && strDeviceId != "0" {
                    
                    arrOfValues.add(strDeviceId)
                    
                }else{
                    
                    arrOfValues.add(strMobileDeviceId)
                    
                }
                
            }else{
                
                arrOfValues.add(strAddressAreaId)
                
            }
            
            arrOfValues.add(strAssociationType)
            arrOfValues.add(strWoId)
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")")
            arrOfValues.add(strPestStatus)
            arrOfValues.add(txtQty.text!)
            arrOfValues.add("\(dictDataOfTargetSelected.value(forKey: "TargetGroupId") ?? "")")
            arrOfValues.add("\(dictDataOfTargetSelected.value(forKey: "TargetId") ?? "")")
            arrOfValues.add("\(dictDataOfTargetSelected.value(forKey: "TargetName") ?? "")")
            arrOfValues.add(strAddressAreaName.count > 0 ? strAddressAreaName : "")
            
            if self.strPestStatus == "3" {
                
                arrOfValues.add(txtQty.text!)

            }else{
                
                arrOfValues.add("")

            }
            
            if strForUpdate == "Yes" {
                
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifyDate")
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                if strServiceAddressPestId.count > 0 && strServiceAddressPestId != "0" {

                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && serviceAddressPestId == %@ && companyKey == %@ && associationType == %@", strWoId, strServiceAddressPestId,Global().getCompanyKey() , strAssociationType), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }else{
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressPestId == %@ && companyKey == %@ && associationType == %@", strWoId, strMobileServiceAddressPestId,Global().getCompanyKey() , strAssociationType), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }

                
            } else {
                
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifyDate")
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                saveDataInDB(strEntity: "ServicePests", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            }
            
        }

    }
    
    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func setValuesToUpdate() {
        
        var  arrOfAllData = NSArray()
        
        if strServiceAddressPestId.count > 0 && strServiceAddressPestId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@ && serviceAddressPestId == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType, strServiceAddressPestId))
            
        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@ && mobileServiceAddressPestId == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType, strMobileServiceAddressPestId))
            
        }
        
        if arrOfAllData.count > 0 {
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            // Pest Status
            
            let strPestStatusLocal = "\(objMaterial.value(forKey: "pestStatus") ?? "")"
            
            if strPestStatusLocal == "1"{
                
                self.btnSlectActions.setTitle("Captured", for: .normal)
                self.txtQty.placeholder = "Quantity Caught*"
                self.strPestStatus = "1"
                txtQty.text = "\(objMaterial.value(forKey: "quantity") ?? "")"
                
            }else if strPestStatusLocal == "2"{
                
                self.btnSlectActions.setTitle("Sighted", for: .normal)
                self.txtQty.placeholder = "Quantity Sighted*"
                self.strPestStatus = "2"
                txtQty.text = "\(objMaterial.value(forKey: "quantity") ?? "")"
                
            }else if strPestStatusLocal == "3"{
                
                self.btnSlectActions.setTitle("Evidence", for: .normal)
                self.txtQty.placeholder = "Evidence*"
                self.strPestStatus = "3"
                txtQty.text = "\(objMaterial.value(forKey: "evidence") ?? "")"
                
            }
            
            
            //txtQty.text = "\(objMaterial.value(forKey: "quantity") ?? "")"
            
            // EPARegNumber
            
            txtTargets.text = "\(objMaterial.value(forKey: "targetName") ?? "")"
            
            var mergedDict = NSDictionary()
            var dictDataProductCategory = NSDictionary()
            var dictDataProduct = NSDictionary()
            
            var array = NSMutableArray()

            array = getDataFromServiceAutoMasters(strTypeOfMaster: "targetGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "TargetGroupId") ?? "")" == "\(objMaterial.value(forKey: "targetGroupId") ?? "")"{
                        
                        dictDataProductCategory  = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "TargetMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "TargetId") ?? "")" == "\(objMaterial.value(forKey: "targetId") ?? "")"{
                        
                        dictDataProduct  = dictData
                        
                        break
                        
                    }
                    
                }
                  
            }
            
            mergedDict = dictDataProduct + dictDataProductCategory
            
            dictDataOfTargetSelected = mergedDict
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            
            self.back()
            
        }
        
    }
    
    func setValuesOnTargetSelection() {
        
        txtTargets.text = "\(dictDataOfTargetSelected.value(forKey: "TargetName") ?? "")"
        
    }
    
    func goToSelectionVC(array : NSMutableArray , arrayList : NSMutableArray , strTitleSelctionVC : String , strTitleSelctionClass : String , strTagg : Int)  {
        
        self.view.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionVC = storyboardIpad.instantiateViewController(withIdentifier: "SelectionVC") as! SelectionVC
        vc.strTitle = strTitleSelctionVC
        vc.strSelectedTitle = strTitleSelctionClass
        vc.strTag = strTagg
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.delegateDictData = self
        vc.aryList = array
        vc.aryForSelectionView = arrayList
        vc.strBranchId = objWorkorderDetail.value(forKey: "branchId") as! String
        present(vc, animated: false, completion: {})
        
    }
    
    @objc func selectGroupMasters() {
        
        if strForUpdate == "Yes" {
            
            
            
        } else {
            
            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "targetGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                self.goToSelectionVC(array: array, arrayList: array , strTitleSelctionVC: "Targets Groups" , strTitleSelctionClass: "Structural Condition" , strTagg: 104)
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Target Data is not Available!", viewcontrol: self)

                //self.back()

                
            }
            
        }
        
    }
    
}

extension AddPestsVC : SelectionVCDelegate
{
    
    func dimissViewww(tag: Int) {
        
        if(tag == 123){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }
        
    }
    
}


extension AddPestsVC : SelectionVcDictDataDelegate
{
    func dimissViewwwOnSelection(tag: Int, dictData: NSDictionary) {
        
        dictDataOfTargetSelected = dictData
        
        self.setValuesOnTargetSelection()
        
        if(tag == 123){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }
        
    }
    
}
