//
//  AddCommentVC.swift
//  DPS
//
//  Created by Saavan Patidar on 12/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class AddCommentVC: UIViewController {

    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    
    @IBOutlet weak var txtViewComment: UITextView!
    
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc var strWoId = NSString ()
    var strMobileServiceCommentId = String ()
    var strServiceCommentId = String ()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var strAssociationType = String ()
    var objWorkorderDetail = NSManagedObject()
    var strForUpdate = String ()
    var strFrom = String ()
    var strMobileConditionCommentId = String ()
    var strConditionCommentId = String ()
    var strConditionId = String ()
    var strDeviceId = String ()
    var strMobileDeviceId = String ()
    
    // MARK: - -----------------------------------Life - Cycle -----------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setBorderColor(item: txtViewComment)
        
        txtViewComment.becomeFirstResponder()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        if strForUpdate == "Yes" {
            
            if strFrom == "Condition"{
                
                self.setValuesToUpdateConditonView()

            }else{
                
                self.setValuesToUpdate()

            }
            
        } else {
            
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    
    // MARK: - -----------------------------------Actions -----------------------------------
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        
        //ServicePests
        
        if strForUpdate == "Yes" {
            
            if strServiceCommentId.count > 0 && strServiceCommentId != "0" {
                
                if strFrom == "Condition"{

                    self.saveConditionCommentDB(strMobileConditionCommentId: "", strConditionCommentId: strConditionCommentId)

                }else{

                    self.saveCommentDB(strMobileServiceCommentId: "", strServiceCommentId: strServiceCommentId)

                }
                
            }else{
                
                if strFrom == "Condition"{
                    
                    self.saveConditionCommentDB(strMobileConditionCommentId: strMobileConditionCommentId, strConditionCommentId: "0")

                }else{

                    self.saveCommentDB(strMobileServiceCommentId: strMobileServiceCommentId, strServiceCommentId: "0")

                }
                
            }
            
        } else {
            
            if strFrom == "Condition"{
                
                self.saveConditionCommentDB(strMobileConditionCommentId: Global().getReferenceNumber(), strConditionCommentId: "")

            }else{
                
                self.saveCommentDB(strMobileServiceCommentId: Global().getReferenceNumber(), strServiceCommentId: "")

            }
            
        }
        
    }
    
    // MARK: - -----------------------------------Functions -----------------------------------

    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func setValuesToUpdate() {
        
        var  arrOfAllData = NSArray()
        
        if strServiceCommentId.count > 0 && strServiceCommentId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@ && serviceCommentId == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType, strServiceCommentId))
            
        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@ && mobileServiceCommentId == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType, strMobileServiceCommentId))
            
        }
        
        if arrOfAllData.count > 0 {
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            txtViewComment.text = "\(objMaterial.value(forKey: "serviceCommentDetail") ?? "")"
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            
            self.back()
            
        }
        
    }
    
    func setValuesToUpdateConditonView() {
        
        var  arrOfAllData = NSArray()
        
        if strConditionCommentId.count > 0 && strServiceCommentId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditionComments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@ && sAConditionCommentId == %@ && mobileSAConditionId == %@", strWoId,Global().getCompanyKey(), strConditionCommentId,strConditionId))
            
        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditionComments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@ && sAConditionCommentId == %@ && mobileSAConditionId == %@", strWoId,Global().getCompanyKey(), strMobileConditionCommentId,strConditionId))
            
        }
        
        if arrOfAllData.count > 0 {
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            txtViewComment.text = "\(objMaterial.value(forKey: "commentDetail") ?? "")"
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            
            self.back()
            
        }
        
    }
    
    func saveCommentDB(strMobileServiceCommentId : String , strServiceCommentId : String) {
        
        if txtViewComment.text!.count < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_EnterComment, viewcontrol: self)
            
        } else {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("mobileServiceCommentId")
            arrOfKeys.add("serviceCommentId")
            arrOfKeys.add("mobileAssociationId")
            arrOfKeys.add("associationType")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("serviceAddressId")
            arrOfKeys.add("serviceCommentDetail")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strMobileServiceCommentId)
            arrOfValues.add(strServiceCommentId)
            
            if strAssociationType == "Device"{
                
                if strDeviceId.count > 0 && strDeviceId != "0" {
                    
                    arrOfValues.add(strDeviceId)
                    
                }else{
                    
                    arrOfValues.add(strMobileDeviceId)
                    
                }
                
            }else{
                
                arrOfValues.add(strAddressAreaId)
                
            }
            
            arrOfValues.add(strAssociationType)
            arrOfValues.add(strWoId)
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")")
            arrOfValues.add(txtViewComment.text!)

            
            if strForUpdate == "Yes" {
                
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifyDate")
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                if strServiceCommentId.count > 0 && strServiceCommentId != "0" {
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && serviceCommentId == %@ && companyKey == %@ && associationType == %@", strWoId, strServiceCommentId,Global().getCompanyKey() , strAssociationType), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }else{
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceCommentId == %@ && companyKey == %@ && associationType == %@", strWoId, strMobileServiceCommentId,Global().getCompanyKey() , strAssociationType), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }
                
                
            } else {
                
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifyDate")
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                saveDataInDB(strEntity: "ServiceComments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            }
            
        }
        
    }
    
    
    func saveConditionCommentDB(strMobileConditionCommentId : String , strConditionCommentId : String) {
        
        if txtViewComment.text!.count < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_EnterComment, viewcontrol: self)
            
        } else {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("sAConditionCommentId")
            arrOfKeys.add("mobileSAConditionId")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("commentDetail")
            //arrOfKeys.add("parentAreaName")

            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strMobileConditionCommentId)
            arrOfValues.add(strConditionId)
            arrOfValues.add(strWoId)
            arrOfValues.add(txtViewComment.text!)
            //arrOfValues.add(strAddressAreaName.count > 0 ? strAddressAreaName : "")
            
            if strForUpdate == "Yes" {
                
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifyDate")
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                if strConditionCommentId.count > 0 && strServiceCommentId != "0" {
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceConditionComments", predicate: NSPredicate(format: "workOrderId == %@ && sAConditionCommentId == %@ && companyKey == %@ && mobileSAConditionId == %@", strWoId, strConditionCommentId,Global().getCompanyKey(),strConditionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }else{
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceConditionComments", predicate: NSPredicate(format: "workOrderId == %@ && sAConditionCommentId == %@ && companyKey == %@ && mobileSAConditionId == %@", strWoId, strMobileConditionCommentId,Global().getCompanyKey(), strConditionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }
                
                
            } else {
                
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifyDate")
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                saveDataInDB(strEntity: "ServiceConditionComments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            }
            
        }
        
    }
}
