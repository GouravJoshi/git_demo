//
//  AddConditionsVC.swift
//  DPS
//
//  Created by Saavan Patidar on 12/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

// MARK:- -----------------------------------Selction From Table view On Pop Up -----------------------------------

extension AddConditionsVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(dictData: NSDictionary, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 35 {
            
            if dictData["ResponsibilityName"] as? String  == strSelectString {
                
                dictOfResponsibilitySelected = NSDictionary ()
                
                txtResponsibility.text = ""
                
            }else{
                
                dictOfResponsibilitySelected = dictData
                txtResponsibility.text = dictOfResponsibilitySelected["ResponsibilityName"] as? String
                
            }
     
        } else if tag == 36 {
            
            if dictData["Text"] as? String  == strSelectString {
                
                dictOfPrioritySelected = NSDictionary ()
                
                txtPriority.text = ""
                
            }else{
                
                dictOfPrioritySelected = dictData
                txtPriority.text = dictOfPrioritySelected["Text"] as? String
                
            }

        }
        
    }
    
}

class AddConditionsVC: UIViewController {
    
    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var txtResponsibility: ACFloatingTextField!
    @IBOutlet weak var txtPriority: ACFloatingTextField!
    @IBOutlet weak var txtCondition: ACFloatingTextField!
    @IBOutlet weak var txtViewDetails: UITextView!
    @IBOutlet weak var txtViewCorrectiveActions: UITextView!
    @IBOutlet weak var btnOpen: UIButton!
    @IBOutlet weak var btnVoid: UIButton!
    @IBOutlet weak var btnResolve: UIButton!
    @IBOutlet weak var const_StatusView_H: NSLayoutConstraint!
    
    @IBOutlet weak var const_AddDoc_H: NSLayoutConstraint!
    @IBOutlet weak var const_AddComment_H: NSLayoutConstraint!
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc var strWoId = NSString ()
    var dictOfResponsibilitySelected = NSDictionary ()
    var dictOfPrioritySelected = NSDictionary ()
    @objc var objWorkorderDetail = NSManagedObject()
    var strForUpdate = String ()
    
    // Akshay
    var strAddressAreaId = String()
    var strAddressAreaName = String()
    var strAssociationType = String()
    var strServiceConditionId = String()
    var strMobileServiceConditionId = String()
    var dictDataOfConditionSelected = NSDictionary ()
    var strStatus = String()
    var arrayOfCorrectiveActionsSelected = NSMutableArray()
    var strCorrectiveActionIds = String()
    var strCorrectiveActionName = String()
    
    
    
    // MARK: - -----------------------------------Life - Cycle -----------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        strStatus = "1"
        
        setBorderColor(item: txtViewDetails)
        setBorderColor(item: txtViewCorrectiveActions)
        
        self.alltextFldActions()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        if strForUpdate == "Yes" {
            
            // const_StatusView_H.constant = 50
            const_AddDoc_H.constant = 56
            const_AddComment_H.constant = 56
            
            self.setValuesToUpdate()
            
        } else {
            
            //const_StatusView_H.constant = 0
            const_AddDoc_H.constant = 0
            const_AddComment_H.constant = 0
            
            self.perform(#selector(selectGroupMasters), with: nil, afterDelay: 0.1)
            
        }
        
        
        //txtViewCorrectiveActions.addTarget(self, action: #selector(selectCorrectiveActions), for: .allEvents)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    
    // MARK: - -----------------------------------Actions -----------------------------------
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if strForUpdate == "Yes" {
            
            if strServiceConditionId.count > 0 && strServiceConditionId != "0" {
                
                self.saveConditionDB(strMobileServiceConditionId: "", strServiceConditionId: strServiceConditionId)
                
            }else{
                
                self.saveConditionDB(strMobileServiceConditionId: strMobileServiceConditionId, strServiceConditionId: "0")
                
            }
            
        } else {
            
            self.saveConditionDB(strMobileServiceConditionId: Global().getReferenceNumber(), strServiceConditionId: "0")
        }
        
    }
    
    @IBAction func action_SelectDetails(_ sender: Any) {
        
        self.view.endEditing(true)
        //self.goToSelection(strTags: 1 , strTitleToShow: "Details")
        
    }
    
    @IBAction func action_SelectCorrectiveActions(_ sender: Any) {
        
        self.view.endEditing(true)
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "correctiveActionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        //array = getDataFromServiceAutoMasters(strTypeOfMaster: "correctiveActionMasters" , strBranchId: "0")

        if array.count > 0 {
            
            self.goToSelection(strTags: 101 , strTitleToShow: "Corrective Actions" , array: array)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @IBAction func action_SelectOpen(_ sender: Any) {
        
        self.view.endEditing(true)
        btnOpen.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnVoid.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnResolve.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        strStatus = "1"
        
    }
    
    @IBAction func action_SelectVoid(_ sender: Any) {
        
        self.view.endEditing(true)
        btnOpen.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnVoid.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnResolve.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        strStatus = "2"
        
    }
    
    @IBAction func action_SelectResolve(_ sender: Any) {
        
        self.view.endEditing(true)
        btnOpen.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnVoid.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnResolve.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        strStatus = "3"
        
    }
    
    @IBAction func action_AddDocuments(_ sender: UIButton) {
        
        self.goToListView(strType: "Documents")

/*
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDocumentVC") as? AddDocumentVC
        testController?.strWoId = "\(strWoId)"
        testController?.strFrom = "Condition"
        
        if strServiceConditionId.count > 0 {

            testController?.strConditionId = strServiceConditionId

        }else{

            testController?.strConditionId = strMobileServiceConditionId

        }
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
*/
        
    }
    
    
    @IBAction func action_Comment(_ sender: UIButton) {
        
        self.goToListView(strType: "Comments")
        
        /*
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddCommentVC") as? AddCommentVC
        testController?.strWoId = "\(strWoId)" as NSString
        testController?.strFrom = "Condition"
        
        if strServiceConditionId.count > 0 {
            
            testController?.strConditionId = strServiceConditionId
            
        }else{
            
            testController?.strConditionId = strMobileServiceConditionId
            
        }
        
        //testController?.strAddressAreaName = strAddressAreaName
        //testController?.strAddressAreaId = strAddressAreaId
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
 */
 
    }
    
    // MARK: - -----------------------------------Functions -----------------------------------
    
    @objc func selectCorrectiveActions(textField: UITextField) {

        
        
    }
    
    func goToListView(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ListVC") as? ListVC
        testController?.strType = strType
        testController?.strWoId = strWoId
        testController?.strFrom = "Condition"
        
        if strServiceConditionId.count > 0 && strServiceConditionId != "0" {
            
             testController?.strConditionId = strServiceConditionId
            
        } else {
            
            testController?.strConditionId = strMobileServiceConditionId
            
        }
        
        testController?.strConditionName = txtCondition.text!
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func setValuesToUpdate() {
        
        var  arrOfAllData = NSArray()
        
        if strServiceConditionId.count > 0 && strServiceConditionId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@ && sAConduciveConditionId == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strServiceConditionId))
            
        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@ && mobileSAConduciveConditionId == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strMobileServiceConditionId))
            
        }
        
        if arrOfAllData.count > 0 {
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            
            // status
            if("\(objMaterial.value(forKey: "conditionStatus") ?? "")" == "1")
            {
                self.view.endEditing(true)
                btnOpen.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                btnVoid.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                btnResolve.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                strStatus = "1"
            }
            else if("\(objMaterial.value(forKey: "conditionStatus") ?? "")" == "2")
            {
                self.view.endEditing(true)
                btnOpen.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                btnVoid.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                btnResolve.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                strStatus = "2"
            }
            else if("\(objMaterial.value(forKey: "conditionStatus") ?? "")" == "3")
            {
                self.view.endEditing(true)
                btnOpen.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                btnVoid.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                btnResolve.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                strStatus = "3"
            }
            
            // Responsbility
            txtResponsibility.text = "\(objMaterial.value(forKey: "responsibilityName") ?? "")"
            
            
            // priority
            
            if("\(objMaterial.value(forKey: "priorityId") ?? "")" == "1")
            {
                txtPriority.text = "Low"
                
            }
            else if("\(objMaterial.value(forKey: "priorityId") ?? "")" == "2")
            {
                txtPriority.text = "Medium"
            }
            else if("\(objMaterial.value(forKey: "priorityId") ?? "")" == "3")
            {
                txtPriority.text = "High"
            }
            
            // condition name
            txtCondition.text = "\(objMaterial.value(forKey: "sAConduciveConditionName") ?? "")"
            
            // details
            
            txtViewDetails.text = "\(objMaterial.value(forKey: "conduciveConditiondDetail") ?? "")"
            
            // corrective actions
            
            if("\(objMaterial.value(forKey: "strCorrectiveActionIds") ?? "")".count > 0)
            {
                // corrective action master
                var arrayCorrectiveActionMaster = NSMutableArray()
                
                arrayCorrectiveActionMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "correctiveActionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
                
                let ids = "\(objMaterial.value(forKey: "strCorrectiveActionIds") ?? "")"
                arrayOfCorrectiveActionsSelected.removeAllObjects()
                
                var strNameLocal = ""
                
                if(ids.count > 0)
                {
                    
                    for itemAction in arrayCorrectiveActionMaster
                    {
                        if(ids.contains("\((itemAction as! NSDictionary).value(forKey: "CorrectiveActionId")!)"))
                        {
                            arrayOfCorrectiveActionsSelected.add(itemAction as! NSDictionary)
                            
                            var str = String()
                            
                            str = "\((itemAction as! NSDictionary).value(forKey: "CorrectiveActionName") ?? "")"
                            
                            strNameLocal = strNameLocal + " " + str
                            
//                            if strNameLocal.length < 1 {
//
//                                strNameLocal = "\((itemAction as! NSDictionary).value(forKey: "CorrectiveActionName")!)" as! NSMutableString
//
//                            }else{
//
//                                strNameLocal = strNameLocal.appending("\((itemAction as! NSDictionary).value(forKey: "CorrectiveActionName")!)") as! NSMutableString
//
//                            }
                            
                        }
                        
                    }
                    
                    strCorrectiveActionIds = "\(ids)"
                    
                    strCorrectiveActionName = strNameLocal as String
                    
                    //self.txtViewCorrectiveActions.text = strNameLocal as String
                    
                }
                
                for item in arrayOfCorrectiveActionsSelected
                {
                    if(self.txtViewCorrectiveActions.text.count == 0)
                    {
                        self.txtViewCorrectiveActions.text = "\((item as! NSDictionary).value(forKey: "CorrectiveActionName")!)"
                    }
                    else
                    {
                        self.txtViewCorrectiveActions.text =  self.txtViewCorrectiveActions.text + "\n\((item as! NSDictionary).value(forKey: "CorrectiveActionName")!)"
                    }
                    
                }
                
            }
            
            // responsbility master --------------
            
            var arrayRespMaster = NSMutableArray()
            
            arrayRespMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "responsibilityMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            for item in arrayRespMaster
            {
                if("\((item as! NSDictionary).value(forKey: "ResponsibilityId")!)" == "\(objMaterial.value(forKey: "responsibilityId") ?? "")")
                {
                    dictOfResponsibilitySelected = item as! NSDictionary
                    break
                }
            }
            //------------------------------------
            
            // priority master --------------
            
            var arrayPriorityMaster = NSMutableArray()
            
            arrayPriorityMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "Prioritys" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            for item in arrayPriorityMaster
            {
                if("\((item as! NSDictionary).value(forKey: "Value")!)" == "\(objMaterial.value(forKey: "priorityId") ?? "")")
                {
                    dictOfPrioritySelected = item as! NSDictionary
                    break
                }
                
            }
            //------------------------------------
            
            // conducive condition group master and conducive condition master --------------
            
            var arrayConditionGroupMaster = NSMutableArray()
            
            arrayConditionGroupMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            
            
            var dictConditionGroup = NSDictionary()
            var dictCondition = NSDictionary()
            var dictMerge = NSDictionary()
            
            for item in arrayConditionGroupMaster
            {
                if("\((item as! NSDictionary).value(forKey: "ConditionGroupId")!)" == "\(objMaterial.value(forKey: "conditionGroupId") ?? "")")
                {
                    dictConditionGroup = item as! NSDictionary
                    break
                }
            }
            
            var arrayConditionMaster = NSMutableArray()
            
            arrayConditionMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            for item in arrayConditionMaster
            {
                if("\((item as! NSDictionary).value(forKey: "ConditionId")!)" == "\(objMaterial.value(forKey: "conduciveConditionId") ?? "")")
                {
                    dictCondition = item as! NSDictionary
                    break
                }
            }
            
            dictMerge = dictConditionGroup + dictCondition
            dictDataOfConditionSelected = dictMerge
            
            //------------------------------------
            
        }
    }
    
    func goToSelectionVC(array : NSMutableArray , arrayList : NSMutableArray , strTitleSelctionVC : String , strTitleSelctionClass : String , strTagg : Int)  {
        
        self.view.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionVC = storyboardIpad.instantiateViewController(withIdentifier: "SelectionVC") as! SelectionVC
        vc.strTitle = strTitleSelctionVC
        vc.strSelectedTitle = strTitleSelctionClass
        vc.strTag = strTagg
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.delegateDictData = self
        vc.aryList = array
        vc.aryForSelectionView = arrayList
        vc.strBranchId = objWorkorderDetail.value(forKey: "branchId") as! String
        present(vc, animated: false, completion: {})
        
    }
    
    func goToSelection(strTags : Int , strTitleToShow : String , array : NSMutableArray)  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionClass = storyboardIpad.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitleToShow
        vc.strTag = strTags
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = array
        vc.arySelectedTargets = arrayOfCorrectiveActionsSelected
        present(vc, animated: false, completion: {})
        
    }
    
    func alltextFldActions() {
        
        txtResponsibility.addTarget(self, action: #selector(selectResponsibility), for: .allEvents)
        txtPriority.addTarget(self, action: #selector(selectPriority), for: .allEvents)

    }
    
    @objc func selectGroupMasters() {
        
        if strForUpdate == "Yes" {
            
            
            
        } else {
            
            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                self.goToSelectionVC(array: array, arrayList: array , strTitleSelctionVC: "Condition Groups" , strTitleSelctionClass: "Structural Condition" , strTagg: 101)
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Condition Data is not Available!", viewcontrol: self)

                //self.back()
                
            }
            
        }
        
    }
    
    @objc func selectResponsibility(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "responsibilityMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "ResponsibilityName", array: array)

            gotoPopViewWithArray(sender: textField, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @objc func selectPriority(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "Prioritys" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "Text", array: array)

            gotoPopViewWithArray(sender: textField, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    func gotoPopViewWithArray(sender: UITextField , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSelectionOnEditOpportunity = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func setValuesOnConditionSelection()
    {
        
        if(dictDataOfConditionSelected.count > 0)
        {
            
            txtCondition.text = "\(dictDataOfConditionSelected.value(forKey: "ConditionName")!)"
            
            txtViewDetails.text = "\(dictDataOfConditionSelected.value(forKey: "Description")!)"

            // corrective actions
            
            if("\(dictDataOfConditionSelected.value(forKey: "CorrectiveActionId") ?? "")".count > 0)
            {
                // corrective action master
                var arrayCorrectiveActionMaster = NSMutableArray()
                
                arrayCorrectiveActionMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "correctiveActionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
                
                let ids = "\(dictDataOfConditionSelected.value(forKey: "CorrectiveActionId") ?? "")"
                arrayOfCorrectiveActionsSelected.removeAllObjects()
                
                var strNameLocal = ""
                
                if(ids.count > 0)
                {
                    
                    for itemAction in arrayCorrectiveActionMaster
                    {
                        if(ids.contains("\((itemAction as! NSDictionary).value(forKey: "CorrectiveActionId")!)"))
                        {
                            arrayOfCorrectiveActionsSelected.add(itemAction as! NSDictionary)
                            
                            var str = String()

                            str = "\((itemAction as! NSDictionary).value(forKey: "CorrectiveActionName") ?? "")"
                            
                            strNameLocal = strNameLocal + " " + str
                            
//                            if strNameLocal.length < 1 {
//
//                                strNameLocal = "\((itemAction as! NSDictionary).value(forKey: "CorrectiveActionName")!)" as! NSMutableString
//
//                            }else{
//
//                                strNameLocal = strNameLocal.appending("\((itemAction as! NSDictionary).value(forKey: "CorrectiveActionName")!)") as! NSMutableString
//
//                            }
                            
                        }
                        
                    }
                    
                    strCorrectiveActionIds = "\(ids)"
                    
                    strCorrectiveActionName = strNameLocal as String
                    
                    //self.txtViewCorrectiveActions.text = strNameLocal as String
                    
                }
                
                for item in arrayOfCorrectiveActionsSelected
                {
                    if(self.txtViewCorrectiveActions.text.count == 0)
                    {
                        self.txtViewCorrectiveActions.text = "\((item as! NSDictionary).value(forKey: "CorrectiveActionName")!)"
                    }
                    else
                    {
                        self.txtViewCorrectiveActions.text =  self.txtViewCorrectiveActions.text + "\n\((item as! NSDictionary).value(forKey: "CorrectiveActionName")!)"
                    }
                    
                }
                
            }
            
            
            // Responsibility  ResponsibilityId
            
            var array = NSMutableArray()

            array = getDataFromServiceAutoMasters(strTypeOfMaster: "responsibilityMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "ResponsibilityId") ?? "")" == "\(dictDataOfConditionSelected.value(forKey: "ResponsibilityId") ?? "")"{
                        
                        txtResponsibility.text = dictData["ResponsibilityName"] as? String
                        
                        dictOfResponsibilitySelected = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            // Priority    Severity
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "Prioritys" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "Text") ?? "")" == "\(dictDataOfConditionSelected.value(forKey: "Severity") ?? "")"{
                        
                        txtPriority.text = dictData["Text"] as? String
                        
                        dictOfPrioritySelected = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    // save to local db
    func saveConditionDB(strMobileServiceConditionId : String , strServiceConditionId : String) {
        
        if(dictOfResponsibilitySelected.count == 0)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select responsbility", viewcontrol: self)
            
        }
        else if(dictOfPrioritySelected.count == 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select priority", viewcontrol: self)
            
        }
            
        else
        {
            
            if strCorrectiveActionIds.count < 0 {
                
                strCorrectiveActionIds = "s"
                
            }
            
            // yaha par save krna hai database mai
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            // keys
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("mobileSAConduciveConditionId")
            arrOfKeys.add("sAConduciveConditionId")
            arrOfKeys.add("responsibilityId")
            arrOfKeys.add("responsibilityName")
            arrOfKeys.add("serviceAddressId")
            arrOfKeys.add("priorityId")
            arrOfKeys.add("conduciveConditionId")
            arrOfKeys.add("conduciveConditiondDetail")
            arrOfKeys.add("correctiveActionId")
            arrOfKeys.add("correctiveActionName")
            arrOfKeys.add("conditionStatus")
            arrOfKeys.add("mobileServiceAddressAreaId")
            arrOfKeys.add("parentAreaName")
            arrOfKeys.add("sAConduciveConditionName")
            arrOfKeys.add("conditionGroupId")
            arrOfKeys.add("strCorrectiveActionIds")
            arrOfKeys.add("isActive")

            
            // values
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(strMobileServiceConditionId)
            arrOfValues.add(strServiceConditionId)
            arrOfValues.add("\(dictOfResponsibilitySelected.value(forKey: "ResponsibilityId") ?? "")")
            arrOfValues.add("\(dictOfResponsibilitySelected.value(forKey: "ResponsibilityName") ?? "")")
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")")
            arrOfValues.add("\(dictOfPrioritySelected.value(forKey: "Value") ?? "")")
            arrOfValues.add("\(dictDataOfConditionSelected.value(forKey: "ConditionId") ?? "")")
            
            arrOfValues.add(txtViewDetails.text.count > 0 ? txtViewDetails.text! : "")
            arrOfValues.add("0")
            arrOfValues.add(strCorrectiveActionName)
            arrOfValues.add(strStatus)
            arrOfValues.add(strAddressAreaId)
            arrOfValues.add(strAddressAreaName)
            arrOfValues.add("\(dictDataOfConditionSelected.value(forKey: "ConditionName") ?? "")")
            arrOfValues.add("\(dictDataOfConditionSelected.value(forKey: "ConditionGroupId") ?? "")")
            arrOfValues.add(strCorrectiveActionIds)
            arrOfValues.add(true)

            
            if strForUpdate == "Yes" {
                
                arrOfKeys.add("modifiedBy")
                arrOfValues.add(Global().getEmployeeId()) //employee id pass karna h
                
                arrOfKeys.add("modifyDate")
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                if strServiceConditionId.count > 0 && strServiceConditionId != "0" {
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && sAConduciveConditionId == %@ && companyKey == %@", strWoId, strServiceConditionId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }else{
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && mobileSAConduciveConditionId == %@ && companyKey == %@", strWoId, strMobileServiceConditionId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }
                
                
            }else{
                
                arrOfKeys.add("createdDate")
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                arrOfKeys.add("createdBy")
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                arrOfKeys.add("modifiedBy")
                arrOfValues.add(Global().getEmployeeId())
                
                arrOfKeys.add("modifyDate")
                arrOfValues.add(Global().getEmployeeId())
                
                arrOfKeys.add("strCreatedDate")
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                arrOfKeys.add("strModifyDate")
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                saveDataInDB(strEntity: "ServiceConditions", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            }
            
        }
    }
    
}


// MARK:-
// MARK:- ---------PopUpDelegate

extension AddConditionsVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        
        if(tag == 101){
            
            if dictData.value(forKey: "CorrectiveActions") is NSArray {
                
                arrayOfCorrectiveActionsSelected = NSMutableArray()
                
                let arrDataTemp = dictData.value(forKey: "CorrectiveActions") as! NSArray
                
                for item in arrDataTemp
                {
                    
                    arrayOfCorrectiveActionsSelected.add(item)

                }
                
            }
            
//            // if same object does not contain then we will add object
//            if(arrayOfCorrectiveActionsSelected.contains(dictData) == false)
//            {
//                arrayOfCorrectiveActionsSelected.add(dictData)
//            }
            
            self.txtViewCorrectiveActions.text = ""
            
            let ids_local = NSMutableArray()// temporary variable
            let name_local = NSMutableArray()// temporary variable
            
            for item in arrayOfCorrectiveActionsSelected
            {
                if(self.txtViewCorrectiveActions.text.count == 0)
                {
                    self.txtViewCorrectiveActions.text = "\((item as! NSDictionary).value(forKey: "CorrectiveActionName")!)"
                }
                else
                {
                    
                    self.txtViewCorrectiveActions.text =  self.txtViewCorrectiveActions.text + "\n\((item as! NSDictionary).value(forKey: "CorrectiveActionName")!)"
                }
                
                ids_local.add("\((item as! NSDictionary).value(forKey: "CorrectiveActionId")!)")
                name_local.add("\((item as! NSDictionary).value(forKey: "CorrectiveActionName")!)")
                
            }
            
            strCorrectiveActionIds = ids_local.count > 0 ? ids_local.componentsJoined(by: ",") : ""
            
            strCorrectiveActionName = name_local.count > 0 ? name_local.componentsJoined(by: ",") : ""
            
        }
    }
}

extension AddConditionsVC : SelectionVCDelegate
{
    func dimissViewww(tag: Int) {
        
        if(tag == 123){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }
        
    }
    
}

extension AddConditionsVC : SelectionVcDictDataDelegate
{
    func dimissViewwwOnSelection(tag: Int, dictData: NSDictionary) {
        
        dictDataOfConditionSelected = dictData
        
        self.setValuesOnConditionSelection()
        
        print(dictData)
        
        if(tag == 123){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }
        
    }
    
}
