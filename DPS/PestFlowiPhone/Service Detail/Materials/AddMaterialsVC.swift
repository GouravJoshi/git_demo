//
//  AddMaterialsVC.swift
//  DPS
//
//  Created by Saavan Patidar on 12/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit


// MARK:- -----------------------------------Selction From Table view On Pop Up -----------------------------------

extension AddMaterialsVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(dictData: NSDictionary, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 29 {
            
            if dictData["UnitName"] as? String  == strSelectString {
                
                dictDataOfUnitOfMeasurementSelected = NSDictionary ()
                
                txtUnitOfMeasurement.text = ""
                strUnitOfMesurmentId = ""

                
            }else{
                
                dictDataOfUnitOfMeasurementSelected = dictData
                
                txtUnitOfMeasurement.text = dictData["UnitName"] as? String
                strUnitOfMesurmentId = dictData["UnitId"] as? String ?? ""
                chemicalCalcualtion(strFQ: txtQty.text ?? "")
            }
            
        } else  if tag == 30 {
            
            if dictData["ApplicationMethodName"] as? String  == strSelectString {
                
                dictDataOfMethodSelected = NSDictionary ()
                
                txtMethod.text = ""
                
            }else{
                
                dictDataOfMethodSelected = dictData
                
                txtMethod.text = dictData["ApplicationMethodName"] as? String
                
            }
    
        } else  if tag == 31 {
            
            if dictData["Name"] as? String  == strSelectString {
                
                dictDataOfEquipmentSelected = NSDictionary ()
                
                txtEquipment.text = ""
                
            }else{
                
                dictDataOfEquipmentSelected = dictData
                
                txtEquipment.text = dictData["Name"] as? String
                
            }
    
        }else  if tag == 32 {
            
            if dictData["ApplicationRateName"] as? String  == strSelectString {
                
                dictDataOfRateSelected = NSDictionary ()
                
                txtRate.text = ""
                
            }else{
                
                dictDataOfRateSelected = dictData
                
                txtRate.text = dictData["ApplicationRateName"] as? String
                
            }

        }else  if tag == 33 {
            
            if dictData["Text"] as? String  == strSelectString {
                
                dictDataOfWindDirectionSelected = NSDictionary ()
                
                txtWindDirection.text = ""
                
            }else{
                
                dictDataOfWindDirectionSelected = dictData
                
                txtWindDirection.text = dictData["Text"] as? String
                
            }
   
        }else  if tag == 34 {
            
            if dictData["Text"] as? String  == strSelectString {
                
                dictDataOfSkyConditionSelected = NSDictionary ()
                
                txtSkyCondition.text = ""
                
            }else{
                
                dictDataOfSkyConditionSelected = dictData
                
                txtSkyCondition.text = dictData["Text"] as? String
                
            }
            
        }else  if tag == 39 {
            
            if dictData["Name"] as? String  == strSelectString {
                
                dictDataOfConcentrationSelected = NSDictionary ()
                
                txtConcentration.text = ""
                
            }else{
                
                dictDataOfConcentrationSelected = dictData
                
                txtConcentration.text = dictData["Name"] as? String
                chemicalCalcualtion(strFQ: txtQty.text ?? "")
                
            }

        }
        
    }
    
}


class AddMaterialsVC: UIViewController {

    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var txtQty: ACFloatingTextField!
    @IBOutlet weak var txtUnitOfMeasurement: ACFloatingTextField!
    @IBOutlet weak var btnUnitOfMeasurement: UIButton!

    @IBOutlet weak var txtConcentration: ACFloatingTextField!
    @IBOutlet weak var btnConcentration: UIButton!

    @IBOutlet weak var txtUndilutedQty: ACFloatingTextField!
    
    @IBOutlet weak var txtTargets: ACFloatingTextField!
    @IBOutlet weak var btnTargets: UIButton!

    @IBOutlet weak var txtAreas: ACFloatingTextField!
    @IBOutlet weak var btnArea: UIButton!

    @IBOutlet weak var txtViewComment: UITextView!
    @IBOutlet weak var txtMethod: ACFloatingTextField!
    @IBOutlet weak var btnMethod: UIButton!

    @IBOutlet weak var txtLot: ACFloatingTextField!
    @IBOutlet weak var txtEquipment: ACFloatingTextField!
    @IBOutlet weak var btnEquipment: UIButton!

    @IBOutlet weak var txtRate: ACFloatingTextField!
    @IBOutlet weak var btnRate: UIButton!

    @IBOutlet weak var txtSquareFt: ACFloatingTextField!
    @IBOutlet weak var txtCubicFt: ACFloatingTextField!
    @IBOutlet weak var txtLinearFT: ACFloatingTextField!
    @IBOutlet weak var txtWindVelocity: ACFloatingTextField!
    @IBOutlet weak var txtAirTemprature: ACFloatingTextField!
    
    @IBOutlet weak var txtWindDirection: ACFloatingTextField!
    @IBOutlet weak var btnWindDirection: UIButton!

    @IBOutlet weak var txtSkyCondition: ACFloatingTextField!
    @IBOutlet weak var btnSkyCondition: UIButton!

    @IBOutlet weak var txtEpaRegNo: ACFloatingTextField!
    @IBOutlet weak var const_WetherConditionH: NSLayoutConstraint!
    @IBOutlet weak var lblHeaderProductName: UILabel!
    @IBOutlet weak var btnMarkProductFav: UIButton!
    @IBOutlet weak var lblEPANo: UILabel!
    @IBOutlet weak var viewContain: UIView!
    // MARK: - -----------------------------------Global Variables -----------------------------------
    var strWoId = NSString ()
    var objWorkorderDetail = NSManagedObject()
    var strForUpdate = String ()
    var strMobileServiceProductId = String ()
    var strServiceProductId = String ()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var strAssociationType = String ()
    var dictDataOfProductSelected = NSDictionary ()
    var dictDataOfUnitOfMeasurementSelected = NSDictionary ()
    var dictDataOfMethodSelected = NSDictionary ()
    var dictDataOfEquipmentSelected = NSDictionary ()
    var dictDataOfRateSelected = NSDictionary ()
    var dictDataOfWindDirectionSelected = NSDictionary ()
    var dictDataOfSkyConditionSelected = NSDictionary ()
    var dictDataOfConcentrationSelected = NSDictionary ()
    var dictDataOfTargetsSelected = NSDictionary ()
    var arySelectedTargets = NSMutableArray()
    var aryTargetsId = NSMutableArray()
    var aryTargetsToBeShown = NSMutableArray()
    var aryTargetsGroupsToBeShown = NSMutableArray()
    var isToShowWetherCondition = false
    var strDeviceId = String ()
    var strMobileDeviceId = String ()
    var isWeCameToViewOnly = false
    var isFav = false
    var strProductId = ""
    
    var strUnitOfMesurmentId = String()
    
    // MARK: - -----------------------------------Life - Cycle -----------------------------------


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnUnitOfMeasurement.setTitle("", for: .normal)
        btnConcentration.setTitle("", for: .normal)
        btnTargets.setTitle("", for: .normal)
        btnMethod.setTitle("", for: .normal)
        btnEquipment.setTitle("", for: .normal)
        btnRate.setTitle("", for: .normal)
        btnWindDirection.setTitle("", for: .normal)
        btnSkyCondition.setTitle("", for: .normal)
        btnArea.setTitle("", for: .normal)
        if isWeCameToViewOnly == false{
            btnMarkProductFav.isHidden = true
        }
        else{
           btnMarkProductFav.isHidden = false
            if isFav == true{
                btnMarkProductFav.setImage(UIImage(named: "fav"), for: .normal)
            }
            else{
                btnMarkProductFav.setImage(UIImage(named: "unfav"), for: .normal)
            }
        }
        
        
        setBorderColor(item: txtViewComment)
        
        self.alltextFldActions()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
            let strIsWhetherShow = "\(objWorkorderDetail.value(forKey: "isWhetherShow") ?? "")"
            
            if strIsWhetherShow == "1" || strIsWhetherShow == "true" || strIsWhetherShow == "True" {
                
                isToShowWetherCondition = true
                
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        if strForUpdate == "Yes" {
            
            self.setValuesOnUpdate()
            
        } else {
            
            self.perform(#selector(selectGroupMasters), with: nil, afterDelay: 0.1)
            
        }
        
         txtAreas.text = strAddressAreaName
        
        /*
        // Checking If to show wheteher condtion or not
        
        if (nsud.value(forKey: "LeadDetailMaster") != nil) {
            
            if nsud.value(forKey: "LeadDetailMaster") is NSDictionary {
                
                let dictData = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
                
                if dictData.value(forKey: "BranchMasters") is NSArray {

                    let arrData = dictData.value(forKey: "BranchMasters") as! NSArray
                    
                    for k in 0 ..< arrData.count {
                        
                        let dictDataBranch = arrData[k] as! NSDictionary
                        
                        let strBranchSysName = "\(dictDataBranch.value(forKey: "SysName") ?? "")"
                        
                        if strBranchSysName == Global().getEmployeeBranchSysName() {
                            
                            let strIsWhetherShow = "\(dictDataBranch.value(forKey: "IsWhetherShow") ?? "")"

                            if strIsWhetherShow == "1" || strIsWhetherShow == "true" || strIsWhetherShow == "True" {
                                
                                isToShowWetherCondition = true
                                
                            }
                            
                            break
                        }
                        
                    }
                    
                }
                
            }

        }
        */
        
        if isToShowWetherCondition {
            
            const_WetherConditionH.constant = 496
            
        } else {
            
            const_WetherConditionH.constant = 0
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if (nsud.value(forKey: "fromArea") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "fromArea")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "fromArea")
                nsud.synchronize()
                
                txtAreas.text = nsud.value(forKey: "strSelectedAreaName") as? String
                strAddressAreaId = (nsud.value(forKey: "strSelectedAreaId") as? String)!
                strAddressAreaName = (nsud.value(forKey: "strSelectedAreaName") as? String)!
                strAssociationType = "Area"
                
            }
            
        }
        
    }
    

    // MARK: - -----------------------------------Actions -----------------------------------
    
    //RUCHIKA MARK : Product Fav
    
    @IBAction func btnMarkProductFavAction(_ sender: Any) {
        
        
    }
     
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_concentrtion(_ sender: UIButton) {
        
        selectConcentration()
    }
    
    @IBAction func action_unitOfMesurment(_ sender: UIButton) {
        
        selectUnitOfMeasurement()
    }
    @IBAction func action_Method(_ sender: UIButton) {
        
        selectMethod()
    }
    
    @IBAction func action_Equipment(_ sender: UIButton) {
        
        selectEquipment()
    }
    
    @IBAction func action_Area(_ sender: UIButton) {
        
        selectAreas()
    }
    
    @IBAction func action_Rate(_ sender: UIButton) {
        
        selectRate()
    }
    @IBAction func action_Target(_ sender: UIButton) {
        
        selectTargets()
    }
   
    @IBAction func action_Wind_Direction(_ sender: UIButton) {
        
        selectWindDirection()
    }
    
    @IBAction func action_Sky_Condition(_ sender: UIButton) {
        
        selectSkyCondition()
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
       
        if strForUpdate == "Yes" {
            
            if strServiceProductId.count > 0 && strServiceProductId != "0" {

                self.saveMaterialDB(strMobileServiceProductId: "", strServiceProductId: strServiceProductId)
                
            }else{
                
                self.saveMaterialDB(strMobileServiceProductId: strMobileServiceProductId, strServiceProductId: "0")
                
            }
            
        } else {
            
            self.saveMaterialDB(strMobileServiceProductId: Global().getReferenceNumber(), strServiceProductId: "0")

            
        }
        
        
    }
    
    @IBAction func action_SelectComment(_ sender: Any) {
        
        self.view.endEditing(true)
        
        //self.goToSelection(strTags: 1 , strTitleToShow: "Comment")
        
    }
    
    // MARK: - -----------------------------------Call webservice -----------------------------------
    
    func callWebServiceToGetTheUndilutedQauntity(strConcentration: String, strProductId : String, strQuantity: String, strUnitOfMeasurment: String )
        {
                
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

            let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"

      //  Api/Workorder/GetUndilutedQuantity?ProductId=10103&Quantity=0.2&UnitOfMeasure=30053&Concentration=3&CompanyId=2043

            let strURL = String(format:  "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  URL.ServiceNewPestFlow.getChemicalCalculation , strProductId, strQuantity, strUnitOfMeasurment, strConcentration, strCompanyId)
                
                
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "CC") { [self] (response, status) in
                    if(status)
                    {
                        print(response)
                        txtUndilutedQty.text = String(describing: response["data"] as?  NSNumber ?? 0)

                    }
                }
        }
    
    // MARK: - -----------------------------------Functions -----------------------------------
    
    func saveMaterialDB(strMobileServiceProductId : String , strServiceProductId : String) {
        
//        else if (lblEPANo.text?.count)! < 1 {
//
//            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectEPAReg, viewcontrol: self)
//
//        }

        
        if (txtQty.text?.count)! < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectQty, viewcontrol: self)
            
        }else if (txtUnitOfMeasurement.text?.count)! < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectUnitOfMeasurement, viewcontrol: self)
            
        }else if (txtConcentration.text?.count)! < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectConcentration, viewcontrol: self)
            
        }else if (txtUndilutedQty.text?.count)! < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectUndilutedQty, viewcontrol: self)
            
        }
        else{
            
            // yaha par save krna hai database mai
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("undilutedUnit")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("mobileServiceProductId")
            arrOfKeys.add("serviceProductId")
            arrOfKeys.add("mobileAssociationId")
            arrOfKeys.add("associationType")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("productMasterId")
            arrOfKeys.add("serviceAddressId")
            arrOfKeys.add("productCategoryMasterId")
            arrOfKeys.add("quantity")
            arrOfKeys.add("unitId")
            arrOfKeys.add("concentration")
            arrOfKeys.add("undilutedQty")
            arrOfKeys.add("ePARegNo")
            arrOfKeys.add("applicationMethodId")
            arrOfKeys.add("appplicationEquipmentId")
            arrOfKeys.add("lotNo")
            arrOfKeys.add("applicationRateId")
            arrOfKeys.add("productComment")
            arrOfKeys.add("serviceProductName")
            
            arrOfKeys.add("squareft")
            arrOfKeys.add("cubicft")
            arrOfKeys.add("linearft")
            arrOfKeys.add("windVelocity")
            arrOfKeys.add("airTemperature")
            arrOfKeys.add("windDirection")
            arrOfKeys.add("skyCondition")
            
            
            arrOfKeys.add("strTargetIds")
            arrOfKeys.add("activeIngredient")

            
            arrOfValues.add("\(dictDataOfProductSelected.value(forKey: "UndilutedUnit") ?? "")")
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strMobileServiceProductId)
            arrOfValues.add(strServiceProductId)
            
            if strAssociationType == "Device"{
                
                if strDeviceId.count > 0 && strDeviceId != "0" {
                    
                    arrOfValues.add(strDeviceId)
                    
                }else{
                    
                    arrOfValues.add(strMobileDeviceId)
                    
                }
                
            }else{
                
                arrOfValues.add(strAddressAreaId)
                
            }
            arrOfValues.add(strAssociationType)
            arrOfValues.add(strWoId)
            arrOfValues.add("\(dictDataOfProductSelected.value(forKey: "ProductId") ?? "")")
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")")
            arrOfValues.add("\(dictDataOfProductSelected.value(forKey: "ProductCategoryId") ?? "")")
            arrOfValues.add(txtQty.text!)
            arrOfValues.add("\(dictDataOfUnitOfMeasurementSelected.value(forKey: "UnitId") ?? "")")
            arrOfValues.add(txtConcentration.text!)
            arrOfValues.add(txtUndilutedQty.text!)
            arrOfValues.add(lblEPANo.text!)
            arrOfValues.add("\(dictDataOfMethodSelected.value(forKey: "ApplicationMethodId") ?? "")")
            arrOfValues.add("\(dictDataOfEquipmentSelected.value(forKey: "ItemMasterId") ?? "")")
            arrOfValues.add(txtLot.text!)
            arrOfValues.add("\(dictDataOfRateSelected.value(forKey: "ApplicationRateId") ?? "")")
            arrOfValues.add(txtViewComment.text.count > 0 ? txtViewComment.text! : "")
            arrOfValues.add("\(dictDataOfProductSelected.value(forKey: "ProductName") ?? "")")
            
            if isToShowWetherCondition {
                
                arrOfValues.add(txtSquareFt.text!)
                arrOfValues.add(txtCubicFt.text!)
                arrOfValues.add(txtLinearFT.text!)
                arrOfValues.add(txtWindVelocity.text!)
                arrOfValues.add(txtAirTemprature.text!)
                
                if dictDataOfWindDirectionSelected.count > 0 {
                    
                    arrOfValues.add("\(dictDataOfWindDirectionSelected.value(forKey: "Value") ?? "")")

                }else{
                    
                    arrOfValues.add("")

                }
                if dictDataOfSkyConditionSelected.count > 0 {
                    
                    arrOfValues.add("\(dictDataOfSkyConditionSelected.value(forKey: "Value") ?? "")")
                    
                }else{
                    
                    arrOfValues.add("")
                    
                }
                
            }else{
                
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")

            }

            
            let aryTemptargetId = NSMutableArray()
            
            if arySelectedTargets.count > 0{
                
                arySelectedTargets = arySelectedTargets.reduce([], { $0.contains($1) ? $0 : $0 + [$1] }) as! NSMutableArray
                
            }
            
            if arySelectedTargets.count > 0 {
                
                for k in 0 ..< arySelectedTargets.count {
                    
                    let dictData = arySelectedTargets[k] as! NSDictionary
                    aryTemptargetId.add("\(dictData.value(forKey: "TargetId") ?? "")")
                    
                }
                
            }
            
            let strTempTargetId = aryTemptargetId.componentsJoined(by: ",")
            
            arrOfValues.add(strTempTargetId)
            arrOfValues.add("\(dictDataOfProductSelected.value(forKey: "ActiveIngredientPercent") ?? "")")

            if strForUpdate == "Yes" {
                
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifyDate")
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                if strServiceProductId.count > 0 && strServiceProductId != "0" {

                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && serviceProductId == %@ && companyKey == %@ && associationType == %@", strWoId, strServiceProductId,Global().getCompanyKey() , strAssociationType), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }else{
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceProductId == %@ && companyKey == %@ && associationType == %@", strWoId, strMobileServiceProductId,Global().getCompanyKey() , strAssociationType), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        dismiss(animated: false)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }

                
            }else{
                
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifyDate")
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                saveDataInDB(strEntity: "ServiceProducts", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                dismiss(animated: false)
                
            }


        }
        
    }

    func back()  {
        
        dismiss(animated: false)
        
    }
    
    
    func setValuesOnProductSelection() {
        
        // Product name
        
        lblHeaderProductName.text = "\(dictDataOfProductSelected.value(forKey: "ProductName") ?? "")"
        
        // Targets
        
        let strTargetsIds = "\(dictDataOfProductSelected.value(forKey: "StrTargetId") ?? "")"
        
        let arrTargetTemp = strTargetsIds.components(separatedBy: ",")
        
        aryTargetsToBeShown.addObjects(from: arrTargetTemp)
        
        aryTargetsId.addObjects(from: arrTargetTemp)
        
        // EPARegNumber
        
        lblEPANo.text = "\(dictDataOfProductSelected.value(forKey: "EPARegNumber") ?? "")"
        
        // UndilutedUnit hi ActiveIngredientPercent hi UndilutedUnit
        
        //txtUndilutedQty.text = "\(dictDataOfProductSelected.value(forKey: "ActiveIngredientPercent") ?? "")"
        
        // UndilutedUnit

        txtConcentration.text = "\(dictDataOfProductSelected.value(forKey: "FirstDilutedPercent") ?? "")"
        
        if (txtConcentration.text?.count)! <= 0 {
            
            txtConcentration.text = "\(dictDataOfProductSelected.value(forKey: "SecondDilutedPercent") ?? "")"
            
        }
        
        if (txtConcentration.text?.count)! <= 0 {
            
            txtConcentration.text = "\(dictDataOfProductSelected.value(forKey: "ThirdDilutedPercent") ?? "")"
            
        }
        
        //Area
        
        txtAreas.text = strAddressAreaName
        
        var array = NSMutableArray()
        
        let aryTempGroupId = NSMutableArray()
        
        // Targets k liye
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "TargetMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        var strTargetNameToShow = ""
        
        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                //if aryTargetsId.contains("\(dictData.value(forKey: "TargetId") ?? "")"){
                if "\(dictData.value(forKey: "TargetId") ?? "")" == "\(dictDataOfProductSelected.value(forKey: "TargetId") ?? "")" {

                    arySelectedTargets.add(dictData)
                    
                    var str = String()
                    
                    str = "\(dictData.value(forKey: "TargetName") ?? "")"
                    
                    if strTargetNameToShow.count > 0{
                        
                        strTargetNameToShow = strTargetNameToShow + ", " + str
                        
                    }else{
                        
                        strTargetNameToShow = strTargetNameToShow + "" + str
                        
                    }
                    
//                    if strTargetNameToShow.length > 0{
//
//                        strTargetNameToShow = strTargetNameToShow.appending(", \(dictData.value(forKey: "TargetName") ?? "")") as! NSMutableString
//
//                    }else{
//
//                        strTargetNameToShow.append("\(dictData.value(forKey: "TargetName") ?? "")")
//
//                    }
                    
                    //break
                    
                }
                
                if aryTargetsToBeShown.contains("\(dictData.value(forKey: "TargetId") ?? "")") {
                    
                    aryTempGroupId.add("\(dictData.value(forKey: "TargetGroupId") ?? "")")
                    
                }
                
            }
            
        }
        
        txtTargets.text = strTargetNameToShow as String
        
        
        // For For Showing Only Targets Groups Which Are in Product Master
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "targetGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            for k1 in 0 ..< array.count {
                
                let dictData = array[k1] as! NSDictionary
                
                // to add which target groups to be shown for product
                
                if aryTempGroupId.contains("\(dictData.value(forKey: "TargetGroupId") ?? "")") {
                    
                    aryTargetsGroupsToBeShown.add(dictData)
                    
                }
                
            }
            
        }
        
        // ApplicationMethodId

        //array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationMethodMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationMethodMasters" , strBranchId: "")

        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary  //ApplicationMethodId ApplicationMethodId
                
                if "\(dictData.value(forKey: "ApplicationMethodId") ?? "")" == "\(dictDataOfProductSelected.value(forKey: "ApplicationMethodId") ?? "")"{
                    
                    txtMethod.text = dictData["ApplicationMethodName"] as? String
                    
                    dictDataOfMethodSelected = dictData
                    
                    break
                    
                }
                
            }
            
        }
        
        //  ApplicationEquipmentId  ItemMasterId
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "InventoryItems" , strBranchId: "1") // Yaha branch ka check nhi hai yaha type ka check hai
        
        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary  // ItemMasterId  ApplicationEquipmentId
                
                if "\(dictData.value(forKey: "ItemMasterId") ?? "")" == "\(dictDataOfProductSelected.value(forKey: "ApplicationEquipmentId") ?? "")"{
                    
                    txtEquipment.text = dictData["Name"] as? String
                    
                    dictDataOfEquipmentSelected = dictData
                    
                    break
                    
                }
                
            }
            
        }
        
        // ApplicationRateId ApplicationRateId
        
        
        //array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationRateMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationRateMasters" , strBranchId: "")

        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary  // ApplicationRateId  ApplicationRateId
                
                if "\(dictData.value(forKey: "ApplicationRateId") ?? "")" == "\(dictDataOfProductSelected.value(forKey: "ApplicationRateId") ?? "")"{
                    
                    txtRate.text = dictData["ApplicationRateName"] as? String
                    
                    dictDataOfRateSelected = dictData
                    
                    break
                    
                }
                
            }
            
        }
        
        // UnitId
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")
        
        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary // UnitId UnitId
                
                if "\(dictData.value(forKey: "UnitId") ?? "")" == "\(dictDataOfProductSelected.value(forKey: "UnitId") ?? "")"{
                    
                    txtUnitOfMeasurement.text = dictData["UnitName"] as? String
                    strUnitOfMesurmentId = dictData["UnitId"] as? String ?? ""
                    
                    if "\(dictDataOfProductSelected.value(forKey: "DilutionCalculation") ?? "")".lowercased() == "pre-diluted".lowercased(){
                        txtConcentration.text = "\(dictDataOfProductSelected.value(forKey: "VolumeWeightOfUnit") ?? "")"
                    }
                      
                    dictDataOfUnitOfMeasurementSelected = dictData
                    
                    break
                    
                }
                
            }
            
        }
        
        
    }
    
    func setValuesOnUpdate() {
        
        var  arrOfAllData = NSArray()
        
        if strServiceProductId.count > 0 && strServiceProductId != "0" {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@ && serviceProductId == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType, strServiceProductId))
            
        } else {
            
            arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@ && mobileServiceProductId == %@", strWoId, strAddressAreaId,Global().getCompanyKey(), strAssociationType, strMobileServiceProductId))
            
        }
        
        if arrOfAllData.count > 0 {
            
            let objMaterial = arrOfAllData[0] as! NSManagedObject
            
            // Targets
            
            let strTargetsIds = "\(objMaterial.value(forKey: "strTargetIds") ?? "")"
            
            let arrTargetTemp = strTargetsIds.components(separatedBy: ",")
            
            aryTargetsToBeShown.addObjects(from: arrTargetTemp)

            aryTargetsId.addObjects(from: arrTargetTemp)
            
            txtQty.text = "\(objMaterial.value(forKey: "quantity") ?? "")"

            // EPARegNumber
            
            lblEPANo.text = "\(objMaterial.value(forKey: "ePARegNo") ?? "")"
            
            // UndilutedUnit
            
            txtUndilutedQty.text = "\(objMaterial.value(forKey: "undilutedQty") ?? "")"
            
            // FirstDilutedPercent
            
            txtConcentration.text = "\(objMaterial.value(forKey: "concentration") ?? "")"
            
            // Comment
            
            txtViewComment.text = "\(objMaterial.value(forKey: "productComment") ?? "")"
            
            // squareft
            
            txtSquareFt.text = "\(objMaterial.value(forKey: "squareft") ?? "")"
            
            // cubicft
            
            txtCubicFt.text = "\(objMaterial.value(forKey: "cubicft") ?? "")"
            
            // linearft
            
            txtLinearFT.text = "\(objMaterial.value(forKey: "linearft") ?? "")"
            
            // windVelocity
            
            txtWindVelocity.text = "\(objMaterial.value(forKey: "windVelocity") ?? "")"
            
            // airTemperature
            
            txtAirTemprature.text = "\(objMaterial.value(forKey: "airTemperature") ?? "")"
            
            // windDirection
            
            //txtWindDirection.text = "\(objMaterial.value(forKey: "windDirection") ?? "")"
            
            // skyCondition
            
            //txtSkyCondition.text = "\(objMaterial.value(forKey: "skyCondition") ?? "")"
            
            // lotNo
            
            txtLot.text = "\(objMaterial.value(forKey: "lotNo") ?? "")"
            
            //Area
            
            txtAreas.text = strAddressAreaName
            
            var array = NSMutableArray()
            
            let aryTempGroupId = NSMutableArray()

            // Targets k liye
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "TargetMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            var strTargetNameToShow = ""

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if aryTargetsId.contains("\(dictData.value(forKey: "TargetId") ?? "")"){
                        
                        arySelectedTargets.add(dictData)
                        
                        var str = String()
                        
                        str = "\(dictData.value(forKey: "TargetName") ?? "")"
                        
                        if strTargetNameToShow.count > 0{
                            
                            strTargetNameToShow = strTargetNameToShow + ", " + str

                        }else{
                            
                            strTargetNameToShow = strTargetNameToShow + "" + str

                        }
                        
//                        if strTargetNameToShow.length > 0{
//
//                            strTargetNameToShow = strTargetNameToShow.appending(", \(dictData.value(forKey: "TargetName") ?? "")") as! NSMutableString
//
//                        }else{
//
//                            strTargetNameToShow.append("\(dictData.value(forKey: "TargetName") ?? "")")
//
//                        }
                        
                    }
                    
                    
                    if aryTargetsToBeShown.contains("\(dictData.value(forKey: "TargetId") ?? "")") {
                        
                        aryTempGroupId.add("\(dictData.value(forKey: "TargetGroupId") ?? "")")
                        
                    }
                    
                }
                
            }
            
            txtTargets.text = strTargetNameToShow as String

            // For For Showing Only Targets Groups Which Are in Product Master
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "targetGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                for k1 in 0 ..< array.count {
                    
                    let dictData = array[k1] as! NSDictionary
                    
                    // to add which target groups to be shown for product
                    
                    if aryTempGroupId.contains("\(dictData.value(forKey: "TargetGroupId") ?? "")") {
                        
                        aryTargetsGroupsToBeShown.add(dictData)
                        
                    }
                    
                }
                
            }
            
            
            // ApplicationMethodId

            //array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationMethodMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationMethodMasters" , strBranchId: "")

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "applicationMethodId") ?? "")" == "\(objMaterial.value(forKey: "applicationMethodId") ?? "")"{
                        
                        txtMethod.text = dictData["ApplicationMethodName"] as? String
                        
                        dictDataOfMethodSelected = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            //  ApplicationEquipmentId  ItemMasterId
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "InventoryItems" , strBranchId: "1")// Yaha branch ka check nhi hai yaha type ka check hai
            
            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary // ItemMasterId  appplicationEquipmentId
                    
                    if "\(dictData.value(forKey: "ItemMasterId") ?? "")" == "\(objMaterial.value(forKey: "appplicationEquipmentId") ?? "")"{
                        
                        txtEquipment.text = dictData["Name"] as? String
                        
                        dictDataOfEquipmentSelected = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            // ApplicationRateId ApplicationRateId
            
            //array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationRateMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationRateMasters" , strBranchId: "")

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary  // ApplicationRateId  applicationRateId
                    
                    if "\(dictData.value(forKey: "ApplicationRateId") ?? "")" == "\(objMaterial.value(forKey: "applicationRateId") ?? "")"{
                        
                        txtRate.text = dictData["ApplicationRateName"] as? String
                        
                        dictDataOfRateSelected = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            // UnitId
            
            //array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")

            
            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary  // UnitId unitId
                    
                    if "\(dictData.value(forKey: "UnitId") ?? "")" == "\(objMaterial.value(forKey: "unitId") ?? "")"{
                        
                        txtUnitOfMeasurement.text = dictData["UnitName"] as? String
                        strUnitOfMesurmentId = dictData["UnitId"] as? String ?? ""

                        dictDataOfUnitOfMeasurementSelected = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            
            //SkyConditions
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "SkyConditions" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "Value") ?? "")" == "\(objMaterial.value(forKey: "skyCondition") ?? "")"{
                        
                        dictDataOfSkyConditionSelected  = dictData
                        
                        txtSkyCondition.text = "\(dictDataOfSkyConditionSelected.value(forKey: "Text") ?? "")"
                        
                        break
                        
                    }
                    
                }
                
            }
            
            //windDirection
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "WindDirections" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "Value") ?? "")" == "\(objMaterial.value(forKey: "windDirection") ?? "")"{
                        
                        dictDataOfWindDirectionSelected  = dictData
                        
                        txtWindDirection.text = "\(dictDataOfWindDirectionSelected.value(forKey: "Text") ?? "")"
                        
                        break
                        
                    }
                    
                }
                
            }
            
            var mergedDict = NSDictionary()
            var dictDataProductCategory = NSDictionary()
            var dictDataProduct = NSDictionary()

            // productMasterId  productCategoryMasterId
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductCategoryMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary  // ProductCategoryMasterId  productCategoryMasterId
                    
                    if "\(dictData.value(forKey: "ProductCategoryId") ?? "")" == "\(objMaterial.value(forKey: "productCategoryMasterId") ?? "")"{
                        
                        dictDataProductCategory  = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary  // ProductMasterId  productMasterId
                    
                    if "\(dictData.value(forKey: "ProductId") ?? "")" == "\(objMaterial.value(forKey: "productMasterId") ?? "")"{
                        
                        dictDataProduct  = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            mergedDict = dictDataProduct + dictDataProductCategory
            
            dictDataOfProductSelected = mergedDict
            
            // Product name
            
            lblHeaderProductName.text = "\(dictDataOfProductSelected.value(forKey: "ProductName") ?? "")"
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            
            self.back()
            
        }
        
    }
    
    func goToSelectionVC(array : NSMutableArray , arrayList : NSMutableArray , strTitleSelctionVC : String , strTitleSelctionClass : String , strTagg : Int)  {
        
        self.view.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionVC = storyboardIpad.instantiateViewController(withIdentifier: "SelectionVC") as! SelectionVC
        vc.strTitle = strTitleSelctionVC
        vc.strSelectedTitle = strTitleSelctionClass
        vc.strTag = strTagg
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.delegateDictData = self
        vc.aryList = array
        vc.aryForSelectionView = arrayList
        vc.arySelectedTargets = arySelectedTargets
        vc.strBranchId = objWorkorderDetail.value(forKey: "branchId") as! String
        vc.aryTargetsToBeShownOnSelection = aryTargetsToBeShown
        present(vc, animated: false, completion: {})
        
    }
    
    func goToSelection(strTags : Int , strTitleToShow : String , array : NSMutableArray)  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionClass = storyboardIpad.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitleToShow
        vc.strTag = strTags
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = array
        present(vc, animated: false, completion: {})
        
    }
    
    func alltextFldActions() {
        
//        txtUnitOfMeasurement.addTarget(self, action: #selector(selectUnitOfMeasurement), for: .allEvents)
//        txtTargets.addTarget(self, action: #selector(selectTargets), for: .allEvents)
//        txtAreas.addTarget(self, action: #selector(selectAreas), for: .allEvents)
//        txtMethod.addTarget(self, action: #selector(selectMethod), for: .allEvents)
//        txtEquipment.addTarget(self, action: #selector(selectEquipment), for: .allEvents)
//        txtRate.addTarget(self, action: #selector(selectRate), for: .allEvents)
//        txtWindDirection.addTarget(self, action: #selector(selectWindDirection), for: .allEvents)
//        txtSkyCondition.addTarget(self, action: #selector(selectSkyCondition), for: .allEvents)
//        txtConcentration.addTarget(self, action: #selector(selectConcentration), for: .allEvents)

    }
    
    @objc func selectGroupMasters() {
        
        if strForUpdate == "Yes" {
            
            
            
        } else {
            
            var array = NSMutableArray()
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductCategoryMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            if array.count > 0 {
                
                self.goToSelectionVC(array: array, arrayList: array , strTitleSelctionVC: "Product Groups" , strTitleSelctionClass: "Structural Condition" , strTagg: 102)
                
            }else{
                
                //self.back()
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                
            }
            
        }
        
    }
    
    @objc func selectUnitOfMeasurement() {
        
        self.view.endEditing(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        //array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")
        if array.count > 0 {
            
            array = addSelectInArray(strName: "UnitName", array: array)
            
            gotoPopViewWithArray(tag: txtUnitOfMeasurement.tag, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }

    @objc func selectTargets() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        self.view.endEditing(true)

        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "targetGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        let strTargetsIds = "\(dictDataOfProductSelected.value(forKey: "StrTargetId") ?? "")"
        
        if strTargetsIds.count < 1 {
            
            //var arrayTargetMastersLocal = NSMutableArray()
            
            //arrayTargetMastersLocal = getDataFromServiceAutoMasters(strTypeOfMaster: "TargetMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            
            aryTargetsToBeShown.add("ShowAllTarget")
            
        }else{
            
            array = aryTargetsGroupsToBeShown
            
        }
        //array = aryTargetsGroupsToBeShown
        
        if array.count > 0 {
            
            self.goToSelectionVC(array: array, arrayList: array , strTitleSelctionVC: "Target Groups" , strTitleSelctionClass: "Structural Condition" , strTagg: 103)

        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @objc func selectAreas() {
        
        txtAreas.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        self.goToSelectAreaView()
        
    }
    
    @objc func selectMethod() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        //array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationMethodMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationMethodMasters" , strBranchId: "")

        // Show approved only logic
        
        if (array.count > 0) && (dictDataOfProductSelected.count > 0) {
            
            //StrAppMethodId "\(dictData.value(forKey: "TargetId") ?? "")"
            
            let arrTempToCheck = "\(dictDataOfProductSelected.value(forKey: "StrAppMethodId") ?? "")".components(separatedBy: ",")
            
            let tempArrayLocal = NSMutableArray()
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if arrTempToCheck.contains("\(dictData.value(forKey: "ApplicationMethodId") ?? "")") {

                    tempArrayLocal.add(dictData)
                    
                }
                
            }
            
            // Replacing the array with approved list to show.
            
            if tempArrayLocal.count > 0 {
                
                array = NSMutableArray()
                
                array = tempArrayLocal
                
            }
            
        }
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "ApplicationMethodName", array: array)

            gotoPopViewWithArray(tag: txtMethod.tag, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @objc func selectEquipment() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "InventoryItems" , strBranchId: "1")// Yaha branch ka check nhi hai yaha type ka check hai
        
        // Show approved only logic
        
        if (array.count > 0) && (dictDataOfProductSelected.count > 0) {
            
            //StrAppRateId
            
            let arrTempToCheck = "\(dictDataOfProductSelected.value(forKey: "StrAppEquipmentId") ?? "")".components(separatedBy: ",")
            
            let tempArrayLocal = NSMutableArray()
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if arrTempToCheck.contains("\(dictData.value(forKey: "ItemMasterId") ?? "")") {
                    
                    tempArrayLocal.add(dictData)
                    
                }
                
            }
            
            // Replacing the array with approved list to show.
            
            if tempArrayLocal.count > 0 {
                
                array = NSMutableArray()
                
                array = tempArrayLocal
                
            }
            
        }
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "Name", array: array)

            gotoPopViewWithArray(tag: txtEquipment.tag, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @objc func selectRate() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ApplicationRateMasters" , strBranchId: "")
        
        // Show approved only logic
        
        if (array.count > 0) && (dictDataOfProductSelected.count > 0) {
            
            //StrAppRateId
            
            let arrTempToCheck = "\(dictDataOfProductSelected.value(forKey: "StrAppRateId") ?? "")".components(separatedBy: ",")
            
            let tempArrayLocal = NSMutableArray()
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if arrTempToCheck.contains("\(dictData.value(forKey: "ApplicationRateId") ?? "")") {
                    
                    tempArrayLocal.add(dictData)
                    
                }
                
            }
            
            // Replacing the array with approved list to show.
            
            if tempArrayLocal.count > 0 {
                
                array = NSMutableArray()
                
                array = tempArrayLocal
                
            }
            
        }
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "ApplicationRateName", array: array)
            
            gotoPopViewWithArray(tag: txtRate.tag, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @objc func selectWindDirection() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "WindDirections" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "Text", array: array)

            gotoPopViewWithArray(tag: txtWindDirection.tag, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @objc func selectSkyCondition() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "SkyConditions" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "Text", array: array)

            gotoPopViewWithArray(tag: txtSkyCondition.tag, aryData: array , strTitle: "")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @objc func selectConcentration() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var array = NSMutableArray()
        
        let dict = NSMutableDictionary()
        let dict1 = NSMutableDictionary()
        let dict2 = NSMutableDictionary()
//        let dict3 = NSMutableDictionary()
//        let dict4 = NSMutableDictionary()
//        let dict5 = NSMutableDictionary()
        
        let fDP = "\(dictDataOfProductSelected.value(forKey: "FirstDilutedPercent") ?? "")"
        let sDP = "\(dictDataOfProductSelected.value(forKey: "SecondDilutedPercent") ?? "")"
        let tDP = "\(dictDataOfProductSelected.value(forKey: "ThirdDilutedPercent") ?? "")"
        
        if fDP.count > 0  {
            
            dict.setValue(fDP, forKey: "Name")
            
        }
        if sDP.count > 0  {
            
            dict1.setValue(sDP, forKey: "Name")

        }
        if tDP.count > 0  {
            
            dict2.setValue(tDP, forKey: "Name")

        }

//        dict3.setValue("Broken", forKey: "Name")
//        dict4.setValue("Inaccessible", forKey: "Name")
//        dict5.setValue("Missing", forKey: "Name")
        
        if dict.count > 0 {
            
            array.add(dict)
            
        }
        
        if dict1.count > 0 {
            
            array.add(dict1)
            
        }

        if dict2.count > 0 {
            
            array.add(dict2)
            
        }
        
//        array.add(dict3)
//        array.add(dict4)
//        array.add(dict5)
        
        if array.count > 0 {
            
            array = addSelectInArray(strName: "Name", array: array)

            gotoPopViewWithArray(tag: txtConcentration.tag, aryData: array , strTitle: "")
            
        }else{
            
            if "\(dictDataOfProductSelected.value(forKey: "DilutionCalculation") ?? "")".lowercased() != "pre-diluted".lowercased(){
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            }
            
        }
        
    }
    
    func goToSelectAreaView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SelectAreaVC") as? SelectAreaVC
        testController?.strWoId = strWoId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func gotoPopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSelectionOnEditOpportunity = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func chemicalCalcualtion(strFQ: String){
        
        if txtConcentration.text != "" && txtUnitOfMeasurement.text != "" && strFQ != "" {
                   
            print("\(dictDataOfProductSelected.value(forKey: "DilutionCalculation") ?? "")")
            
            if "\(dictDataOfProductSelected.value(forKey: "DilutionCalculation") ?? "")".lowercased() == "diluted".lowercased(){
                
                let strChemicalDilutedWater = "\(dictDataOfProductSelected.value(forKey: "ChemicalDilutedWater") ?? "")"
                
                if strChemicalDilutedWater == "1" || strChemicalDilutedWater == "true"{ //wattable
                    
                    //Value = (Concentration / ProductMasterData.MinimumDilutionPercent) * FinishedQty * ProductMasterData.ChemicalMixedRate;
                    
                    var chemicalMixedRate = ""
                    
                    if  dictDataOfProductSelected.value(forKey: "ChemicalMixedRate") is NSNumber{
                        chemicalMixedRate = String(describing: dictDataOfProductSelected.value(forKey: "ChemicalMixedRate") as! NSNumber)
                    }
                    else{
                        chemicalMixedRate = dictDataOfProductSelected.value(forKey: "ChemicalMixedRate") as? String ?? ""
                    }
                    
                    if chemicalMixedRate == ""{
                        chemicalMixedRate = "0.0"
                    }
                    
                    var MinimumDilutionPercent = ""
                    
                    
                    if  dictDataOfProductSelected.value(forKey: "MinimumDilutionPercent") is NSNumber{
                        MinimumDilutionPercent = String(describing: dictDataOfProductSelected.value(forKey: "MinimumDilutionPercent") as! NSNumber)
                    }
                    else{
                        MinimumDilutionPercent = dictDataOfProductSelected.value(forKey: "MinimumDilutionPercent") as? String ?? ""
                    }
                    
                    
                    if  MinimumDilutionPercent == ""{
                        MinimumDilutionPercent = "0.0"
                    }
                    
                    let subVAlue = Float(txtConcentration.text!)! / Float(String(describing: MinimumDilutionPercent))!
                    let finishedQuantity = Float(strFQ) ?? 0.0
                    
                    let value =  subVAlue * finishedQuantity * Float(chemicalMixedRate)!
                    
                    print(value)
                    
                    txtUndilutedQty.text = "\(value)"
                }
                
                else{ // liquid poweder
                    
                    //let convertedValue = FQ (operator) value
                    //((ConvertedValue / AIpercent) * Concentration);
                    // (3 / 9.5555555555) * 105.544
                    
                    var tempOperator = String()
                    var tempValue = String()

                    //Get operators from master
                    let arrOfOperatorsFromMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitOperators", strBranchId: "")
                    
                    let parentUnitNameShortName = getParentUnitShortName()
                    let conversionUnitShotName = getConversionUnitShortName()
                    
                    for i in 0..<arrOfOperatorsFromMaster.count{
                        
                        let obj = arrOfOperatorsFromMaster[i] as! NSDictionary
                        if parentUnitNameShortName ==  "\(obj.value(forKey: "UnitFrom") ?? "")"{
                            if conversionUnitShotName == "\(obj.value(forKey: "UnitTo") ?? "")"{
                                tempOperator = "\(obj.value(forKey: "Operator") ?? "")"
                                tempValue = "\(obj.value(forKey: "Value") ?? "")"
                                break
                            }
                        }
                    }
               
                    print(tempValue, tempOperator)
                    if tempValue != "" && tempOperator != tempOperator{
                        
                        var calculatedValue = Float()
                        let finishedQuantity = Float(strFQ) ?? 0.0
                        if tempOperator == "*"{
                            calculatedValue = Float(tempValue) ?? 0.0 * finishedQuantity
                        }
                        else if tempOperator == "/"{
                            calculatedValue = Float(tempValue) ?? 0.0  / finishedQuantity
                        }
                        else if tempOperator == "-"{
                            calculatedValue = Float(tempValue) ?? 0.0  - finishedQuantity

                        }
                        else if tempOperator == "+"{
                            calculatedValue = Float(tempValue) ?? 0.0  + finishedQuantity
                        }
                                                
                        let value = (calculatedValue / Float("\(dictDataOfProductSelected.value(forKey: "ActiveIngredientPercent") ?? "0.00")")!) * Float(txtConcentration.text!)!
                        
                        print(value)
                        txtUndilutedQty.text = "\(value)"
                    }
                    else{
                        
                        let calculatedValue = Float(strFQ) ?? 0.0
                        
                        let value = (calculatedValue / Float("\(dictDataOfProductSelected.value(forKey: "ActiveIngredientPercent") ?? "0.00")")!) * Float(txtConcentration.text!)!
                        
                        print(value)
                        txtUndilutedQty.text = "\(value)"
                        
                    }
                    
                    //  let value =  Float("\(dictDataOfProductSelected.value(forKey: "ActiveIngredientPercent") ?? "0.00")")! * Float(txtConcentration.text!)! ;
                    //}
                    
                }
            }
            else if "\(dictDataOfProductSelected.value(forKey: "DilutionCalculation") ?? "")".lowercased() == "DilutedFactor".lowercased(){
                
                // Value = FinishedQty * ProductMasterData.DilutionFactor;

                //var DilutionFactor = dictDataOfProductSelected.value(forKey: "DilutionFactor") as! String
                var dilutionFactor = ""
                
                if  dictDataOfProductSelected.value(forKey: "DilutionFactor") is NSNumber{
                    
                    dilutionFactor = String(describing: dictDataOfProductSelected.value(forKey: "DilutionFactor") as! NSNumber)
                }
                else{
                    dilutionFactor = dictDataOfProductSelected.value(forKey: "DilutionFactor") as? String ?? ""
                }
                
                if dilutionFactor == ""{
                    dilutionFactor = "0"
                }
                let finishedQuantity = Float(strFQ) ?? 0.0
                let value = finishedQuantity * Float(dilutionFactor)!
                
                print(value)
                
                txtUndilutedQty.text = "\(value)"

            }
            else if "\(dictDataOfProductSelected.value(forKey: "DilutionCalculation") ?? "")".lowercased() == "pre-diluted".lowercased(){
               // Value = FinishedQty * VolumeWeightOfUnit;

                var VolumeWeightOfUnit = ""
                
                if  dictDataOfProductSelected.value(forKey: "VolumeWeightOfUnit") is NSNumber{
                    VolumeWeightOfUnit = String(describing: dictDataOfProductSelected.value(forKey: "VolumeWeightOfUnit") as! NSNumber)
                }
                else{
                    VolumeWeightOfUnit = dictDataOfProductSelected.value(forKey: "VolumeWeightOfUnit") as? String ?? ""
                }
                
                if VolumeWeightOfUnit == "" || VolumeWeightOfUnit == "0"{
                    VolumeWeightOfUnit = "0"
                }
                
                let finishedQuantity = Float(strFQ) ?? 0.0
                let value = finishedQuantity * Float(VolumeWeightOfUnit)!
                
                print(value)
                
                txtUndilutedQty.text = "\(value)"
            }
        }
        else{
            txtUndilutedQty.text = ""
        }
    }
}


// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension AddMaterialsVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if textField == txtQty  {
            
            let completeText = (textField.text ?? "") + string
            let chk = decimalValidation(textField: textField, range: range, string: string)
            if chk == true{
                print("new added: \((textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string)")
                let entredText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
                chemicalCalcualtion(strFQ: entredText)
                
            }
            return chk
            
        } else if textField == txtLot  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        } else if textField == txtSquareFt  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)

        } else if textField == txtCubicFt  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        } else if textField == txtLinearFT  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        } else if textField == txtWindVelocity  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        } else if textField == txtAirTemprature  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        } else if textField == txtEpaRegNo  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        } else if textField == txtTargets  {
            
            return false
            
        } else if textField == txtUndilutedQty  {
            
            return false
            
        } else if textField == txtConcentration  {
            
            return false
            
        }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtConcentration{
            txtConcentration.resignFirstResponder()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtConcentration{
            txtConcentration.resignFirstResponder()
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func getParentUnitShortName() -> String{
        
        var parentUnitNameShortName = String()
        let array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary // UnitId
            
            if "\(dictData.value(forKey: "UnitId") ?? "")" == "\(dictDataOfProductSelected.value(forKey: "UnitId") ?? "")"{
                
                let temp = dictData["UnitName"] as? String ?? ""
                
                let arrOfUnitAbbreviationMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitAbbreviationMaster", strBranchId: "")
                
                for i in 0..<arrOfUnitAbbreviationMaster.count{
                    
                    let objAbbrivation = arrOfUnitAbbreviationMaster[i] as! NSDictionary // UnitId UnitId
                    
                    if "\(objAbbrivation.value(forKey: "FullName") ?? "")".lowercased() == temp.lowercased(){
                         parentUnitNameShortName = "\(dictData.value(forKey: "ShortName") ?? "")"
                        
                    }
                }
                
            }
        }
        return parentUnitNameShortName
    }
    
    func getConversionUnitShortName() -> String{
        
        var parentUnitNameShortName = String()
        let array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary // UnitId
            
            if "\(dictData.value(forKey: "UnitId") ?? "")" == strUnitOfMesurmentId {
                
                let temp = dictData["UnitName"] as? String ?? ""
                
                let arrOfUnitAbbreviationMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitAbbreviationMaster", strBranchId: "")
                
                for i in 0..<arrOfUnitAbbreviationMaster.count{
                    
                    let objAbbrivation = arrOfUnitAbbreviationMaster[i] as! NSDictionary // UnitId UnitId
                    
                    if "\(objAbbrivation.value(forKey: "FullName") ?? "")".lowercased() == temp.lowercased(){
                         parentUnitNameShortName = "\(dictData.value(forKey: "ShortName") ?? "")"
                        
                    }
                }
                
            }
        }
        return parentUnitNameShortName
    }
    
}


// MARK:-
// MARK:- ---------PopUpDelegate

extension AddMaterialsVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        
        if(tag == 102){
            
            print(dictData)
            
        }
        
    }
}

extension AddMaterialsVC : SelectionVCDelegate
{
    
    func dimissViewww(tag: Int) {
        
        if(tag == 123){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }
        
    }
    
}

extension AddMaterialsVC : SelectionVcDictDataDelegate
{
    func dimissViewwwOnSelection(tag: Int, dictData: NSDictionary) {
        
        //dictDataOfProductSelected = dictData
        
        //self.setValuesOnProductSelection()
        
        print(dictData)
        strProductId =  "\(dictData.value(forKey: "ProductId") ?? "")"
        if(tag == 123){
            
            dictDataOfProductSelected = dictData
            
            self.setValuesOnProductSelection()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }else  if(tag == 124){
            
            dictDataOfTargetsSelected = dictData
            
            if dictData.value(forKey: "Targets") is NSArray {
                
                let aryTargetsSelected = dictData.value(forKey: "Targets") as! NSArray

                for k in 0 ..< aryTargetsSelected.count {
                    
                    let dictData = aryTargetsSelected[k] as! NSDictionary
                    
                    arySelectedTargets.add(dictData)
                    
                }
                
            }
            
            if arySelectedTargets.count > 0{
                
                arySelectedTargets = arySelectedTargets.reduce([], { $0.contains($1) ? $0 : $0 + [$1] }) as! NSMutableArray
                
            }
            
            
            var strTargetNameToShow = ""
            
            if arySelectedTargets.count > 0 {
                
                for k in 0 ..< arySelectedTargets.count {
                    
                    let dictData = arySelectedTargets[k] as! NSDictionary
                    
                    var str = String()
                    
                    str = "\(dictData.value(forKey: "TargetName") ?? "")"
                    
                    if strTargetNameToShow.count > 0{
                        
                        strTargetNameToShow = strTargetNameToShow + ", " + str
                        
                    }else{
                        
                        strTargetNameToShow = strTargetNameToShow + "" + str
                        
                    }
                    /*
                    if strTargetNameToShow.length > 0{
                        
                        strTargetNameToShow = strTargetNameToShow.appending(", \(dictData.value(forKey: "TargetName") ?? "")") as! NSMutableString
                        
                    }else{
                        
                        strTargetNameToShow.append("\(dictData.value(forKey: "TargetName") ?? "")")
                        
                    }
                    */
                    
                }
                
            }
            
            txtTargets.text = strTargetNameToShow as String
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.dismiss(animated: false){}
                
            })
            
        }
        
    }
    
}

extension AddMaterialsVC : UIScrollViewDelegate{
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        print("did scrolllllllll start.....")
        viewContain.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { [self] in
                 viewContain.isUserInteractionEnabled = true
             })

    }
    
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        print("did scrolllllllll start.....")
//        viewContain.isUserInteractionEnabled = false
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: { [self] in
//                 viewContain.isUserInteractionEnabled = true
//             })
//
//    }
}
