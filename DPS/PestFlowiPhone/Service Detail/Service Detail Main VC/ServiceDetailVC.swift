//
//  ServiceDetailVC.swift
//  DPS
//  peSTream Buil Link
//  Created by Saavan Patidar on 13/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
import CoreData

class ServiceDetailVC: UIViewController {

    // MARK: - -----------------------------------IB - Outlets -----------------------------------
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblTitleHeader: UILabel!
    @IBOutlet weak var viewAddButton: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnBarcode: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnChecklist: UIButton!
    @IBOutlet weak var btnStatics: UIButton!
    @IBOutlet weak var btnClock: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var btnOptionMenu: UIButton!

    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc  var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var arrOfArea = NSArray()
    @objc var isFromServicePestNewFlow = Bool()
    var strButtonTitleOfStartDate = ""
    let global = Global()

    // MARK: - -----------------------------------Life Cycle -----------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        //self.deleteAllDB()
        
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        
        //buttonRound(sender: btnSave)

        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary

        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            strCompanyKey = "\(value)"
        }
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            strUserName = "\(value)"
        }
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()

        }
        
        self.setValuesOnViewLoad()
        
        // Adding Add Button
        
        if DeviceType.IS_IPHONE_X {
            
            viewAddButton.frame = CGRect(x: UIScreen.main.bounds.size.width-80, y: UIScreen.main.bounds.size.height-90-105, width: 80, height: 80)
            
        }else{
            
            viewAddButton.frame = CGRect(x: UIScreen.main.bounds.size.width-80, y: UIScreen.main.bounds.size.height-65-105, width: 80, height: 80)
            
        }
        
        let isWoComplete = isWoCompleted(objWo: objWorkorderDetail)

        if !isWoComplete {
            
            self.view.addSubview(viewAddButton)
            btnBarcode.isHidden = false

        }else{
            
            btnBarcode.isHidden = true
            
        }
        
        // To Enter all Areas and Device on Service Address to individual Work Order
        
        if !isWoCompleted(objWo: objWorkorderDetail) {

            deleteAllRecordsFromDB(strEntity: "WOServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
            deleteAllRecordsFromDB(strEntity: "WOServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
            
            let arrOfAreaOnServiceAddress = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0"))

            let arrOfDeviceOnServiceAddress = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedDevice == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(),"0"))

            for k in 0 ..< arrOfAreaOnServiceAddress.count {
                
                let dictData = arrOfAreaOnServiceAddress[k] as! NSManagedObject
                
                let strAreaIdLocal = "\(dictData.value(forKey: "addressAreaId") ?? "")"
                let strMobileAreaIdLocal = "\(dictData.value(forKey: "mobileAddressAreaId") ?? "")"
                
                
                if strAreaIdLocal.count > 0 && strAreaIdLocal != "0" {
                    
                    saveWoArea(strMobileServiceAreaIdDB: "0" , strServiceAreaIdDB: strAreaIdLocal)
                    
                }else{
                    
                    saveWoArea(strMobileServiceAreaIdDB: strMobileAreaIdLocal , strServiceAreaIdDB: "0")
                    
                }
                
            }
            
            for k in 0 ..< arrOfDeviceOnServiceAddress.count {
                
                let dictData = arrOfDeviceOnServiceAddress[k] as! NSManagedObject
                
                let strDeviceIdLocal = "\(dictData.value(forKey: "deviceId") ?? "")"
                let strMobileDeviceIdLocal = "\(dictData.value(forKey: "mobileDeviceId") ?? "")"
                
                
                if strDeviceIdLocal.count > 0 && strDeviceIdLocal != "0" {
                    
                    saveWoDevice(strMobileServiceDeviceIdDB: "0" , strServiceDeviceIdDB: strDeviceIdLocal)
                    
                }else{
                    
                    saveWoDevice(strMobileServiceDeviceIdDB: strMobileDeviceIdLocal , strServiceDeviceIdDB: "0")

                }
                
            }
            
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@", strWoId, "",Global().getCompanyKey()))
        
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            btnStartTime.isEnabled = false
            self.btnGallery.setBackgroundImage(UIImage(named: "galleryGray"), for: .normal)
            self.btnStatics.setBackgroundImage(UIImage(named: "staticsGray"), for: .normal)
            self.btnGallery.isEnabled  = false
            self.btnStatics.isEnabled = false
        }
        else{
            btnChecklist.layer.borderColor = UIColor(hex: "36A085")?.cgColor
            btnChecklist.layer.borderWidth = 1.0
            btnChecklist.setTitleColor(hexStringToUIColor(hex: "36A085"), for: .normal)
            
            strButtonTitleOfStartDate = UserDefaults.standard.value(forKey: "StartStopButtonTitle") as! String
            
            if strButtonTitleOfStartDate == "Start"{
                setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Pause")
            }
            else if strButtonTitleOfStartDate == "Pause"{
                setStartButtonTextWithDateTime(strTempDateAndTime: "", strTimeType: "Start")
            }
        }
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            //arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && parentAddressAreaId == %@ && companyKey == %@", strWoId, "",Global().getCompanyKey()))
            
            arrOfArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String, strAddressAreaId: "" , strMobileAddressAreaId: "" , strWoStatus: "Complete")

        }else{
            
            arrOfArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && mobileParentAddressAreaId == %@ && companyKey == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")", "",Global().getCompanyKey()))
            
        }
        
        tvlist.reloadData()
        
        // If From Sanner View to Add Device.
        if (nsud.value(forKey: "fromScanner") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "fromScanner")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "fromScanner")
                nsud.synchronize()
                
                self.goToAddDeviceView(strFrom: "Scanner")
                
            }
            
        }
        
        
        // If From Add Device View to Inspect  Device.
        if (nsud.value(forKey: "DeviceAdded") != nil) {
            
            let isAreaAdded = nsud.bool(forKey: "DeviceAdded")
            
            if isAreaAdded {
                
                nsud.set(false, forKey: "DeviceAdded")
                nsud.synchronize()
                
                self.goToDeviceDetailView(strMobileDeviceId: nsud.value(forKey: "DeviceAddedDeviceId") as! String)
                
            }
            
        }
        
        // If From Add Area View to Inspect  Area.
        if (nsud.value(forKey: "AreaAdded") != nil) {
            
            let isAreaAdded = nsud.bool(forKey: "AreaAdded")
            
            if isAreaAdded {
                
                nsud.set(false, forKey: "AreaAdded")
                nsud.synchronize()
                
                self.goToAreaDetailView()
                
            }
            
        }
        
        // Dismiss And goto Invoice View
        if (nsud.value(forKey: "dismissView") != nil) {
            
            let isDismissView = nsud.bool(forKey: "dismissView")
            
            if isDismissView {
                
                nsud.set(false, forKey: "dismissView")
                nsud.synchronize()
                
                self.goToInvoiceView()
                
            }
            
        }
        
        // Dismiss And goto Invoice View
        if (nsud.value(forKey: "dismissViewAfterPauseAndRedirectToGI") != nil) {
            
            let isDismissView = nsud.bool(forKey: "dismissViewAfterPauseAndRedirectToGI")
            
            if isDismissView {
                
                nsud.set(false, forKey: "dismissViewAfterPauseAndRedirectToGI")
                nsud.synchronize()
                
                switch UIDevice.current.userInterfaceIdiom {
                case .phone:
                    navigationController?.popToViewController(ofClass: GenralInfoVC.self)
                case .pad:
                    navigationController?.popToViewController(ofClass: GeneralInfoNewPestiPad.self)
                default:
                    break
                }
                
            }
            
        }
        
        
    }

    // MARK: - -----------------------------------Actions -----------------------------------
    
    
    @IBAction func btnChecklistAction(_ sender: Any) {
        goToInspectionView()
    }
    
    @IBAction func btnStartTimeAction(_ sender: Any){
        /* btnStartTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime")*/
        
        let str = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")" //"\(lblStartTimeValue.titleLabel?.text ?? "")"
        
        if str == ""
        {
            self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: false)
            strButtonTitleOfStartDate = "Pause"
        }
        else
        {
            var alertTitle = ""
            if strButtonTitleOfStartDate == "Start"{
                alertTitle = "Start"
            }
            else if strButtonTitleOfStartDate == "Pause"{
                alertTitle = "Pause"
            }
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want \(alertTitle) time", preferredStyle: UIAlertController.Style.alert)
            

            alert.addAction(UIAlertAction(title: alertTitle, style: .default , handler:{ (nil)in
                
                if self.strButtonTitleOfStartDate == "Start"{
                    self.getStartTimeIn(isStartTime: true, isStrTimeInAvial: true)
                }
                else if self.strButtonTitleOfStartDate == "Pause"{
                    self.getStartTimeIn(isStartTime: false, isStrTimeInAvial: true)
                }
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (nil)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    @IBAction func action_Clock(_ sender: Any) {
        self.showOtherOptionsActionSheet()

    }
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.goToInvoiceView()
        
    }
    
    @IBAction func action_Barcode(_ sender: Any) {
        
        self.goToScannerView()
        
    }
    
    @IBAction func action_Image(_ sender: Any) {
        
       self.goToGlobalmage(strType: "After")
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")

    }
    @IBAction func action_Add(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Area", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAreaView()
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Device", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAddDeviceView(strFrom: "")
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.btnAdd
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    // MARK: - -----------------------------------Functions -----------------------------------
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func saveTimeInDB(startTime : String){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("TimeIn")
        arrOfValues.add(startTime)

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    //MARK: - Duration calculations
    func getDuration(strStartDate: String, strPauseDate: String) -> String{
        
        if strStartDate.count == 0 {
            
            return "\(0)"

        }else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            
           
            var startDate = dateFormatter.date(from: strStartDate)
            if startDate == nil{
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.local
                dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm:ss"
                startDate = dateFormatter2.date(from: strStartDate)
            }
            let pauseDate = dateFormatter.date(from: strPauseDate)!
            let resultedOffSet = pauseDate.offsetFrom(date: startDate!)
            
            let result = getDurationInterval(strOffSet: resultedOffSet)
            print(result)
            return result
            
        }
        
    }
    
    func getDurationInterval(strOffSet : String) -> String{
        let resultantOffSet = strOffSet
        
        let arr = resultantOffSet.components(separatedBy: " ")
        var strDay = "0"
        var strHr = "0"
        var strMin = "0"
        var strSec = "0"
        
        if resultantOffSet.contains("d"){
            strDay = arr[0].components(separatedBy: "d")[0]
            strHr = arr[1].components(separatedBy: "h")[0]
            strMin = arr[2].components(separatedBy: "m")[0]
            strSec = arr[3].components(separatedBy: "s")[0]

        }
        else if resultantOffSet.contains("h"){
            strHr = arr[0].components(separatedBy: "h")[0]
            strMin = arr[1].components(separatedBy: "m")[0]
            strSec = arr[2].components(separatedBy: "s")[0]

        }
        else if arr.count == 1{
            strSec = arr[0].components(separatedBy: "s")[0]
        }
        else{
           strMin = arr[0].components(separatedBy: "m")[0]
           strSec = arr[1].components(separatedBy: "s")[0]
        }
        
        strDay = strDay.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strDay = strDay.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strHr = strHr.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strHr = strHr.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strMin = strMin.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strMin = strMin.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        strSec = strSec.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strSec = strSec.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)

        let dayInt = Int(strDay)!
        let hrInt = Int(strHr)!
        let minInt = Int(strMin)!
        let secInt = Int(strSec)!

        print(dayInt)
        print(hrInt)
        print(minInt)
        print(secInt)

        var dayConvertsInHours = Int()
        if dayInt != 0{
          dayConvertsInHours = dayInt * 24
        }
        let result = (dayConvertsInHours * 60 * 60) + (hrInt * 60 * 60) + (minInt * 60) + secInt  //INSECONDS
         //String(minInt) + " Min " + String(secInt) + " secs" // 2 Min 12 secs
        //
        return String(result)
    }
        
    func getStartTimeIn(isStartTime: Bool,  isStrTimeInAvial: Bool)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        
        let strtemp = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
        var strDate = ""
        if strtemp != ""{
                        
            strDate  = changeStringDateToGivenFormat(strDate: strtemp, strRequiredFormat: "MM/dd/yyyy hh:mm:ss a")
        }
        if isStrTimeInAvial == false{
            self.saveTimeInDB(startTime: str)
            setStartButtonTextWithDateTime(strTempDateAndTime: str, strTimeType: "Start")
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
        }
        if strButtonTitleOfStartDate == "Start"{
            

            let obj =  fetchStartTimeWithStartStatus()
            if obj.count > 0 {
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
            
            strButtonTitleOfStartDate = "Pause"

            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "START", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Start")
            }
            

        }
        else if strButtonTitleOfStartDate == "Pause"{
            
            let obj =  fetchStartTimeWithStartStatus()
            if obj.count > 0 {
            let subObj = obj[0] as! NSManagedObject
            let strDuration  = getDuration(strStartDate: subObj.value(forKey: "startTime") as! String, strPauseDate: str)
                  
            strButtonTitleOfStartDate = "Start"

            saveStartStopTimeInTimeSheet(strStartTime: subObj.value(forKey: "startTime") as! String, strStopTime: str, strStatus: "COMPLETE", strDuration: strDuration, isStart: false)
            
            saveStartStopTimeInTimeSheet(strStartTime: str, strStopTime: "", strStatus: "PAUSE", strDuration: "", isStart: true)
            
            setStartButtonTextWithDateTime(strTempDateAndTime: strDate, strTimeType: "Pause")
            }
            
        }
        
    }
    
    
     func fetchStartTimeWithStartStatus() -> NSMutableArray{
         
         let objTimeSheetLastIndex = NSMutableArray()
         var queryParam  = ""
         if strButtonTitleOfStartDate == "Start"{
             queryParam = "PAUSE"
         }
         if strButtonTitleOfStartDate == "Pause"{
             queryParam = "START"
         }
         let arryOfData = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@  && status == %@", strWoId, queryParam))
     
         //Featching all the data from DB for start stop
 //        let tempArr = getDataFromCoreDataBaseArray(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@", strWoId))
 //
 //        let tempDict = NSMutableArray()
 //        for index in 0..<tempArr.count{
 //            let dict = tempArr[index] as! NSManagedObject
 //            tempDict.add(dict)
 //        }
 //
 //        for j in 0..<tempDict.count{
 //            let tempIndex = tempDict[j] as! NSManagedObject
 //            print("Start Time: \(tempIndex.value(forKey: "startTime") as! String))")
 //            print("Stop Time: \(tempIndex.value(forKey: "stopTime") as! String))")
 //            print("Duration: \(tempIndex.value(forKey: "duration") as! String))")
 //            print("status: \(tempIndex.value(forKey: "status") as! String))")
 //            print("status: \(tempIndex.value(forKey: "subject") as! String))")
 //
 //        }
         
         if arryOfData.count > 0 {
                 let dict = arryOfData.lastObject as! NSManagedObject
                 objTimeSheetLastIndex.add(dict)
         }
         return objTimeSheetLastIndex
     }
     
     func saveStartStopTimeInTimeSheet(strStartTime: String, strStopTime: String, strStatus: String, strDuration: String, isStart: Bool){
         
         let arrOfKeys = NSMutableArray()
         let arrOfValues = NSMutableArray()

         if isStart == true{
             arrOfKeys.add("companyKey")
             arrOfKeys.add("workorderId")
             arrOfKeys.add("userName")
             arrOfKeys.add("startTime")
             arrOfKeys.add("stopTime")
             arrOfKeys.add("duration")
             arrOfKeys.add("status")
             arrOfKeys.add("subject")
             
             arrOfValues.add(strCompanyKey)
             arrOfValues.add(strWoId)
             arrOfValues.add(strUserName)
             arrOfValues.add(strStartTime)
             arrOfValues.add(strStopTime)
             arrOfValues.add(strDuration)
             arrOfValues.add(strStatus)
             arrOfValues.add(strStatus)

             saveDataInDB(strEntity: "GeneralInfoWorkorderTimeSheet", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
         }
         else if isStart == false{
             
             arrOfKeys.add("stopTime")
             arrOfKeys.add("duration")
             arrOfKeys.add("status")
             
             arrOfValues.add(strStopTime)
             arrOfValues.add(strDuration)
             arrOfValues.add(strStatus)
             
             let isSuccess = getDataFromDbToUpdate(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && startTime == %@", strWoId, strStartTime), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
             
             if isSuccess
             {
                 print("Success")
                updateModifyDate()
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                UserDefaults.standard.setValue(strButtonTitleOfStartDate, forKey: "StartStopButtonTitle")

                if strButtonTitleOfStartDate == "Start"{
                    switch UIDevice.current.userInterfaceIdiom {
                    case .phone:
                        navigationController?.popToViewController(ofClass: GenralInfoVC.self)
                    case .pad:
                        navigationController?.popToViewController(ofClass: GeneralInfoNewPestiPad.self)
                    default:
                        break
                    }
                }
                
             }
             else {
                 showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
             }
         }
      
     }
     
     func setStartButtonTextWithDateTime(strTempDateAndTime: String, strTimeType: String){
         
 //        btnStartTime?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
 //        var buttonText = NSString()
 //        if strTimeType == "Start" {
 //            buttonText = "Pause"//\nST:" + strTempDateAndTime as NSString
 //        }
 //        else{
 //            buttonText = "Start"//\nST:" + strTempDateAndTime as NSString
 //        }
 //        //"hello\nthere"
 //
 //           //getting the range to separate the button title strings
 //           let newlineRange: NSRange = buttonText.range(of: "\n")
 //
 //           //getting both substrings
 //           var substring1 = ""
 //           var substring2 = ""
 //
 //           if(newlineRange.location != NSNotFound) {
 //               substring1 = buttonText.substring(to: newlineRange.location)
 //               substring2 = buttonText.substring(from: newlineRange.location)
 //           }
 //
 //           //assigning diffrent fonts to both substrings
 //           let font1: UIFont = UIFont(name: "Arial", size: 14.0)!
 //           let attributes1 = [NSMutableAttributedString.Key.font: font1]
 //           let attrString1 = NSMutableAttributedString(string: substring1, attributes: attributes1)
 //
 //           let font2: UIFont = UIFont(name: "Arial", size: 9.0)!
 //           let attributes2 = [NSMutableAttributedString.Key.font: font2]
 //           let attrString2 = NSMutableAttributedString(string: substring2, attributes: attributes2)
 //
 //           //appending both attributed strings
 //           attrString1.append(attrString2)
 //
 //           //assigning the resultant attributed strings to the button
 //           btnStartTime.titleLabel?.textAlignment = NSTextAlignment.center
 //
         if strTimeType == "Start" {
             btnStartTime.setTitle("Pause", for: .normal)
             btnStartTime.backgroundColor = hexStringToUIColor(hex: "9A9A9A")
         }
         else{
             btnStartTime.setTitle("Start", for: .normal)
             btnStartTime.backgroundColor = hexStringToUIColor(hex: "36A085")
         }
         
          //  btnStartTime.setAttributedTitle(attrString1, for: [])
     }
     
    func goToInspectionView()
    {
        
        let testController = UIStoryboard.init(name: "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "InspectionChecklistCopyVC") as! InspectionChecklistCopyVC
        
        testController.isFromNewPestFlow = "YES"
        let strTempAcountNumber = objWorkorderDetail.value(forKey: "accountNo") as? String
        if strTempAcountNumber != ""{
            testController.strAccountNumber  = strTempAcountNumber ?? ""
        }
        testController.matchesWorkOrderZSync = objWorkorderDetail
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    
    func showOtherOptionsActionSheet()  {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let currentSetupAction = UIAlertAction(title: "Current Setup", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCurrentSetupAction()
        })
        let customerSalesDocumentsAction = UIAlertAction(title: "Customer Sales Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openCustomerSalesDocument()
        })
        
        let notesHistoryAction = UIAlertAction(title: "Notes History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openNotesHistory()
        })
        let serviceHistoryAction = UIAlertAction(title: "Service History", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openServiceHistory()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(currentSetupAction)
        optionMenu.addAction(customerSalesDocumentsAction)
        optionMenu.addAction(notesHistoryAction)
        optionMenu.addAction(serviceHistoryAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.btnOptionMenu
        self.present(optionMenu, animated: true, completion: {
        })
        
    }
    
    func openCurrentSetupAction(){
        
    }
    
    func openCustomerSalesDocument(){
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func openNotesHistory(){
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = false;
        testController!.strAccccountId = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        
    }
    
    func openServiceHistory(){
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
    
    func saveWoArea(strMobileServiceAreaIdDB : String , strServiceAreaIdDB : String)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("mobileServiceAreaId")
        arrOfKeys.add("serviceAreaId")
        arrOfKeys.add("wOServiceAreaId")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add(strMobileServiceAreaIdDB)
        arrOfValues.add(strServiceAreaIdDB)
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "WOServiceAreas", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func saveWoDevice(strMobileServiceDeviceIdDB : String , strServiceDeviceIdDB : String)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("mobileServiceDeviceId")
        arrOfKeys.add("serviceDeviceId")
        arrOfKeys.add("wOServiceDeviceId")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add(strMobileServiceDeviceIdDB)
        arrOfValues.add(strServiceDeviceIdDB)
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "WOServiceDevices", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func deleteAllDB()  {
        
        deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceConditionComments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", self.strWoId,Global().getCompanyKey()))
        
    }
    
    func setValuesOnViewLoad() {
        
        lblTitleHeader.text = nsud.value(forKey: "lblName") as? String
        
    }
    
    func back()  {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func goToInvoiceView()  {
        
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            
            let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom

            // 2. check the idiom
            switch (deviceIdiom) {

            case .pad:
                let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlowiPad", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "InvoiceInfoiPadVC") as? InvoiceInfoiPadVC
                testController?.strWoId = self.strWoId as String
                self.navigationController?.pushViewController(testController!, animated: false)

            case .phone:
                let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "InvoiceVC") as? InvoiceVC
                testController?.strWoId = self.strWoId as String
                self.navigationController?.pushViewController(testController!, animated: false)
                            
            
            default:
                print("Unspecified UI idiom")

            }

            
           
//            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
//            let testController = storyboardIpad.instantiateViewController(withIdentifier: "InvoiceDetailVC") as? InvoiceDetailVC
//            testController?.strWoId = strWoId
//            self.navigationController?.pushViewController(testController!, animated: false)
            
        }else{
            
            let errorMsg = checkIfAreaDeviceInspected()
            
            if errorMsg.count > 0 {
                
                //showAlertWithoutAnyAction(strtitle: Alert, strMessage: errorMsg, viewcontrol: self)
                
                let alert = UIAlertController(title: Alert, message: errorMsg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes-Continue", style: .default , handler:{ (UIAlertAction)in
                    
                    let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom

                    // 2. check the idiom
                    switch (deviceIdiom) {

                    case .pad:
                        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlowiPad", bundle: nil)
                        let testController = storyboardIpad.instantiateViewController(withIdentifier: "InvoiceInfoiPadVC") as? InvoiceInfoiPadVC
                        testController?.strWoId = self.strWoId as String
                        self.navigationController?.pushViewController(testController!, animated: false)

                    case .phone:
                        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
                        let testController = storyboardIpad.instantiateViewController(withIdentifier: "InvoiceVC") as? InvoiceVC
                        testController?.strWoId = self.strWoId as String
                        self.navigationController?.pushViewController(testController!, animated: false)
                                    
                    
                    default:
                        print("Unspecified UI idiom")

                    }
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    
                    
                    
                }))
                alert.popoverPresentationController?.sourceView = self.view
                
                self.present(alert, animated: true, completion: {
                    
                    
                })
                
                
            }else{
                

                let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom

                // 2. check the idiom
                switch (deviceIdiom) {

                case .pad:
                    let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlowiPad", bundle: nil)
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "InvoiceInfoiPadVC") as? InvoiceInfoiPadVC
                    testController?.strWoId = self.strWoId as String
                    self.navigationController?.pushViewController(testController!, animated: false)

                case .phone:
                    let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "InvoiceVC") as? InvoiceVC
                    testController?.strWoId = self.strWoId as String
                    self.navigationController?.pushViewController(testController!, animated: false)
                                
                
                default:
                    print("Unspecified UI idiom")

                }
                
            }
            
        }
    }

    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Task-Activity", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPhoneVC") as? ServiceHistory_iPhoneVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanicaliPhone") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.strTypeOfService = "service"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewController") as? ServiceDocumentsViewController
        testController?.strAccountNo = objWorkorderDetail.value(forKey: "accountNo") as? String
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        // workorderStatus
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strModuleType = "ServicePestFlow"
        if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAreaView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddAreaVC") as? AddAreaVC
        testController?.strWoId = strWoId
        nsud.setValue("true", forKey:  "CameToAddArea")

                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAddDeviceView(strFrom : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDeviceVC") as? AddDeviceVC
        testController?.strWoId = strWoId
        testController?.strFrom = strFrom
        nsud.setValue("true", forKey:  "CameToAddDevice")
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAreaDetailView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AreaDetailVC") as? AreaDetailVC
        testController?.strWoId = strWoId
        
        if(arrOfArea.count > 0){
            
            //let objArea = arrOfArea[0] as! NSManagedObject
            testController?.strParentAddressAreaName = ""
            testController?.strMobileAddressAreaId = ""
            testController?.strAddressAreaId = ""

                    testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)

        }else{
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_NoSubArea, viewcontrol: self)
            
        }
        
    }
    
    func goToScannerView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "QRScannerController") as? QRScannerController
        testController?.strFromWhere = "AddArea"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToListView(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ListVC") as? ListVC
        testController?.strType = strType
        testController?.strWoId = strWoId
        testController?.strAssociationType = "Area"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToDeviceView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DeviceListVC") as? DeviceListVC
        testController?.strWoId = strWoId
        testController?.strAreaNameToDisplay = enumArea
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }

    func goToDeviceDetailView(strMobileDeviceId : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DeviceDetailVC") as? DeviceDetailVC
        testController?.strWoId = strWoId
        testController?.strMobileDeviceId = strMobileDeviceId
        testController?.strAreaNameToDisplay = enumArea
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func checkIfAreaDeviceInspected() -> String {
        
        var errorMsg = ""
        
        var arrOfNoArea = NSArray()
        
        if isWoCompleted(objWo: objWorkorderDetail) {
            
            arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "WOServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
            
        }else{
            
            arrOfNoArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String , strAddressAreaId: "" , strMobileAddressAreaId: "" , strWoStatus: "InComplete")
            
        }
        
        let arrOfAreaLocal = arrOfNoArea
        
        let tempCount = NSMutableArray()
        
        if arrOfAreaLocal.count > 0 {
            
            for k in 0 ..< arrOfAreaLocal.count {
                
                let obj = arrOfNoArea[k] as! NSManagedObject
                
                // serviceAreaId
                
                if isWoCompleted(objWo: objWorkorderDetail) {
                    
                    let areaTemp = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@ && addressAreaId == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0" , "\(obj.value(forKey: "serviceAreaId") ?? "")"))
                    
                    if areaTemp.count > 0 {
                        
                        let objTemp = areaTemp[0] as! NSManagedObject
                        
                        let isInspected = objTemp.value(forKey: "isInspected") as! Bool
                        
                        if isInspected {
                            
                            tempCount.add(obj)
                            
                        }
                        
                    }
                    
                }else{
                    
                    let isInspected = obj.value(forKey: "isInspected") as! Bool
                    
                    if isInspected {
                        
                        tempCount.add(obj)
                        
                    }
                    
                }
                
            }
            
        }
        
        
        // To check if Device inspected
        
        //let arrOfDevices =  getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@ && isDeletedDevice == %@",strWoId,Global().getCompanyKey(),"0"))
        let arrOfDevices =  getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedDevice == %@","\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(),"0"))

        let tempCountDeviceInspection = NSMutableArray()
        //let tempCountDeviceReplaced = NSMutableArray()
        //let tempCountDeviceActivity = NSMutableArray()
        
        if arrOfDevices.count > 0 {
            
            for k in 0 ..< arrOfDevices.count {
                
                let obj = arrOfDevices[k] as! NSManagedObject
                
                var strDeviceIdLocal = "\(obj.value(forKey: "deviceId") ?? "")"
                
                if strDeviceIdLocal.count == 0 || strDeviceIdLocal == "0" {
                    
                    strDeviceIdLocal = "\(obj.value(forKey: "mobileDeviceId") ?? "")"
                    
                }
                
                let arrOfDevicesInspection =  getDataFromCoreDataBaseArray(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "mobileServiceDeviceId == %@ && companyKey == %@",strDeviceIdLocal,Global().getCompanyKey()))
                
                if arrOfDevicesInspection.count > 0 {
                    
                    tempCountDeviceInspection.add(strDeviceIdLocal)
                    
                }
                
            }
            
        }
        
        if tempCount.count < 1 && tempCountDeviceInspection.count < 1 {
            
            errorMsg = "No Area/Device Inspected. Do you won't to continue without Inspecting."
            
        }else if tempCount.count < 1 {
            
            errorMsg = "No Area Inspected. Do you won't to continue without Inspecting."
            
        }else if tempCountDeviceInspection.count < 1 {
            
            errorMsg = "No Device Inspected. Do you won't to continue without Inspecting."
            
        }
        
        return errorMsg
        
    }
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ServiceDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ServiceDetailCell", for: indexPath as IndexPath) as! ServiceDetailCell
        
        if indexPath.row == 0{
            
            var arrOfNoDevices = NSArray()
            var arrOfNoConditions = NSArray()
            var arrOfNoArea = NSArray()
            

            arrOfNoConditions = getDataFromCoreDataBaseArray(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))

            if isWoCompleted(objWo: objWorkorderDetail) {
                
                arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "WOServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))

                arrOfNoDevices = getDataFromCoreDataBaseArray(strEntity: "WOServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
                
            }else{
                
                arrOfNoArea = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && isDeletedArea == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey(), "0"))
                
                arrOfNoArea = WebService().fetchAreasFromAreaMasterViaWoId(strWoId: strWoId as String , strAddressAreaId: "" , strMobileAddressAreaId: "" , strWoStatus: "InComplete")
                
                arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: "" , strWoStatus: "InComplete" , strToCheckBlankAddressId: "No")
                
            }
            
            let strNoOfArea = "\(arrOfNoArea.count)"
            let strNoOfDevices = "\(arrOfNoDevices.count)"
            let strNoOfConditions = "\(arrOfNoConditions.count)"
            
            let strlblSubTitle = "\(strNoOfArea) Area, " + "\(strNoOfDevices) Device, " + "\(strNoOfConditions) Conditions"
            
            cell.lblSubTitle.text = strlblSubTitle
            
            cell.lblTitle.text = enumArea
            //cell.lblSubTitle.text = "5 Area, 0 Device, 0 Conditions"
            cell.imgView.image = UIImage(named: "route_1")
            
        } else if indexPath.row == 1{
            
            var arrOfNoDevices = NSArray()
            
            if isWoCompleted(objWo: objWorkorderDetail) {
                
                arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: "" , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressblankOnComplete")

            }else{
                
                arrOfNoDevices = WebService().fetchDeviceFromDeviceMasterViaWoId(strWoId: strWoId as String, strAddressDeviceId: "" , strMobileAddressDeviceId: "" , strAddressId: "" , strWoStatus: "InComplete" , strToCheckBlankAddressId: "serviceaddressblank")
                
            }
            
            let strNoOfDevices = "\(arrOfNoDevices.count)"
            
            let strlblSubTitle = "\(strNoOfDevices) " + enumDevice
            
            cell.lblTitle.text = enumDevice
            cell.lblSubTitle.text = strlblSubTitle
            cell.imgView.image = UIImage(named: "device_1")
            
        } else if indexPath.row == 2{
            
            let  arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, "",Global().getCompanyKey(), enumArea))

            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumMaterial

            cell.lblTitle.text = enumMaterial
            cell.lblSubTitle.text = strlblSubTitle
            cell.imgView.image = UIImage(named: "chemical_1")
            
        }else if indexPath.row == 3{
            
            let  arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, "",Global().getCompanyKey(), enumArea))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumPest
            
            cell.lblTitle.text = enumPest
            cell.lblSubTitle.text = strlblSubTitle
            cell.imgView.image = UIImage(named: "pest_1")
            
        } else if indexPath.row == 4{
            
            let  arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@", strWoId, "",Global().getCompanyKey()))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumCondition
            
            cell.lblTitle.text = enumCondition
            cell.lblSubTitle.text = strlblSubTitle
            cell.imgView.image = UIImage(named: "task_1")
            
        } else if indexPath.row == 5{
            
            let  arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && mobileAssociationId == %@ && companyKey == %@ && associationType == %@", strWoId, "",Global().getCompanyKey(), enumArea))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumComment
            
            cell.lblTitle.text = enumComment
            cell.lblSubTitle.text = strlblSubTitle
            cell.imgView.image = UIImage(named: "message_1")
            
        }else {
            
            let arrOfAllData =  getDataFromCoreDataBaseArray(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId == %@ && mobileServiceAddressAreaId == %@ && companyKey == %@", strWoId, "",Global().getCompanyKey()))
            
            let strNo = "\(arrOfAllData.count)"
            
            let strlblSubTitle = "\(strNo) " + enumDocument
            
            cell.lblTitle.text = enumDocument
            cell.lblSubTitle.text = strlblSubTitle
            cell.imgView.image = UIImage(named: "attatch_1")
            
        }
        
        tvlist.separatorColor = UIColor.clear
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            
            self.goToAreaDetailView()
            
        } else if indexPath.row == 1{
            
            self.goToDeviceView()
            
        } else if indexPath.row == 2{
            
            self.goToListView(strType: enumMaterial)
            
        }else if indexPath.row == 3{
            
            self.goToListView(strType: enumPest)

        } else if indexPath.row == 4{
            
            self.goToListView(strType: enumCondition)

        } else if indexPath.row == 5{
            
            self.goToListView(strType: enumComment)
            
        }else {
            
            self.goToListView(strType: enumDocument)

        }
        
    }
    
    /*
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
    }
    */
    
}

// MARK: - ----------------Cell
// MARK: -

class ServiceDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
    
}


extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = false) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}
