//
//  SalesPaymentViewController.h
//  DPS
//
//  Created by Saavan Patidar on 04/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//
//  Saavan Patidar 2021

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface SalesPaymentViewController : UIViewController<UIWebViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,NSFetchedResultsControllerDelegate>
{
    
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches,*matchesStandard,*matchesNonStandard,*matchesPaymentInfo;
    NSArray *arrAllObj,*arrAllObjStandard,*arrAllObjNonStandard,*arrAllObjPaymentInfo;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail;

}
- (IBAction)actionBack:(id)sender;
- (IBAction)action_PayNow:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewGeneralInfo;
@property (strong, nonatomic) IBOutlet UIView *viewPaymentInfo;
@property (strong, nonatomic) IBOutlet UIView *viewBillinfInfo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVieww;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblContactNO;
@property (strong, nonatomic) IBOutlet UILabel *lblPaymentAmount;
@property (strong, nonatomic) IBOutlet UIButton *btnSavedCards;
@property (strong, nonatomic) IBOutlet UIButton *btnNewCard;
- (IBAction)action_SavedCards:(id)sender;
- (IBAction)action_NewCard:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress1;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress2;
@property (strong, nonatomic) IBOutlet UILabel *lblCity;
@property (strong, nonatomic) IBOutlet UILabel *lblState;
@property (strong, nonatomic) IBOutlet UILabel *lblZipcode;
@property (strong, nonatomic) IBOutlet UILabel *lblCountry;
@property (strong, nonatomic) IBOutlet UIView *viewSavedCards;
- (IBAction)action_AddNewRowSavedCard:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddNewRowSavedCard;
- (IBAction)action_PayNowSavedCards:(id)sender;
- (IBAction)action_CancelSavedCards:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnPayNowSavedCards;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelSavedCard;
@property (strong, nonatomic) IBOutlet UITableView *tblViewSavedCards;
@property (strong, nonatomic) IBOutlet UIView *viewNewCard;
@property (strong, nonatomic) IBOutlet UIButton *btnNewCardSaveDetails;
- (IBAction)action_CheckNewCardSaveCardDetail:(id)sender;
- (IBAction)action_PayNowNewCard:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;

@end
