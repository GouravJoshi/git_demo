//
//  TexasTermiteServiceDetail+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 14/11/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface TexasTermiteServiceDetail : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TexasTermiteServiceDetail+CoreDataProperties.h"
