//
//  LegendGraphView.swift
//  DPS
//
//  Created by Navin Patidar on 23/09/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import UIKit
import AVFoundation

@objc class LegendGraphView: UIViewController {

    
    @objc func CreatFormLegend( viewLegend:UIView, controllor : UIViewController,OnResultBlock: @escaping (_ viewContain: UIView   ) -> Void) {
        //MARK: ---Userdefine filed-------
  
        var yAxis = 8 , heightCalculate = 0
        var xAxis = 18
        let strFont = DeviceType.IS_IPAD ? 18.0 : 14.0
        let strMODE = DeviceType.IS_IPAD ? 3 : 2

        //MARK: ----- Title
        for item in viewLegend.subviews {
            if(item is UILabel){
                item.removeFromSuperview()
            }
        }
        for (headerIndex, item) in getGraphLegendCategory().enumerated() {
            print("headerIndex---------------\(headerIndex)")
            if((item as AnyObject) is NSDictionary){
                var dictMAIN = NSMutableDictionary()
                dictMAIN = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let LegendCategoryName = "\(dictMAIN.value(forKey: "LegendCategoryName") ?? "")"
                let lblSectionDetailTitle = UILabel()
                lblSectionDetailTitle.text = LegendCategoryName
                lblSectionDetailTitle.frame = CGRect(x: 8.0, y: CGFloat(yAxis), width: (viewLegend.frame.width) - 16, height: 30)
                lblSectionDetailTitle.numberOfLines = 0
               // lblSectionDetailTitle.textColor = hexStringToUIColor(hex: appThemeColor)
                lblSectionDetailTitle.font = UIFont.boldSystemFont(ofSize: strFont)
                viewLegend.addSubview(lblSectionDetailTitle)
                yAxis = yAxis + 30 
                
                let aryOption = dictMAIN.value(forKey: "GraphLegendMasterExtDcs") as! NSArray
                for (subIndex, item) in aryOption.enumerated() {
                    if( (item as AnyObject) is NSDictionary){
                        let optionDict = (item as AnyObject) as! NSDictionary
                        let strLabel = "\(optionDict.value(forKey: "LegendMasterName") ?? "")"
                        let lblLegendTitle = UILabel()
                        lblLegendTitle.text = "• " + strLabel
                        lblLegendTitle.numberOfLines = 0
                        //lblLegendTitle.textColor = hexStringToUIColor(hex: appThemeColor)
                        lblLegendTitle.font = UIFont.systemFont(ofSize: strFont)
                        let heightstr = self.estimatedHeightOfLabel(text: lblLegendTitle.text!,  width: Int((viewLegend.frame.width))/strMODE - 16 + 18, font: Int(strFont))
                        if(DeviceType.IS_IPAD){
                            if(subIndex%strMODE) == 0{
                                lblLegendTitle.frame = CGRect(x: CGFloat(xAxis), y: CGFloat(yAxis), width: (viewLegend.frame.width)/CGFloat(strMODE) - 16, height: heightstr)
                                heightCalculate = Int(heightstr)
                                if aryOption.count - 1 ==  subIndex{
                                    if(Int(heightstr) > heightCalculate){
                                        heightCalculate = Int(heightstr)
                                    }
                                    yAxis = yAxis + Int(heightCalculate)
                                    xAxis = 18
                                    //yAxis = yAxis + Int(30.0)
                                }else{
                                    if(Int(heightstr) > heightCalculate){
                                        heightCalculate = Int(heightstr)
                                    }
                                    xAxis = xAxis + Int((viewLegend.frame.width)/CGFloat(strMODE))
                                }
                            }else if(subIndex%strMODE) == 1{
                                lblLegendTitle.frame = CGRect(x:CGFloat(xAxis), y: CGFloat(yAxis), width: (viewLegend.frame.width)/CGFloat(strMODE) - 16, height:heightstr)
                                    if aryOption.count - 1 ==  subIndex{
                                            if(Int(heightstr) > heightCalculate){
                                                heightCalculate = Int(heightstr)
                                            }
                                            yAxis = yAxis + Int(heightCalculate)
                                            xAxis = 18
                                        }else{
                                            if(Int(heightstr) > heightCalculate){
                                                heightCalculate = Int(heightstr)
                                            }
                                            xAxis = xAxis + Int((viewLegend.frame.width)/CGFloat(strMODE))
                                        }
                            }else if(subIndex%strMODE) == 2{
                                lblLegendTitle.frame = CGRect(x: CGFloat(xAxis), y: CGFloat(yAxis), width: (viewLegend.frame.width)/CGFloat(strMODE) - 16, height: heightstr)
                            
                               // yAxis = yAxis + Int(30.0)
                                if(Int(heightstr) > heightCalculate){
                                    heightCalculate = Int(heightstr)
                                }
                                yAxis = yAxis + Int(heightCalculate)
                                xAxis = 18
                            }
                            
                        }else{
                            if(subIndex%strMODE) == 0{
                                lblLegendTitle.frame = CGRect(x: CGFloat(xAxis), y: CGFloat(yAxis), width: (viewLegend.frame.width)/CGFloat(strMODE) - 16, height: heightstr)
                                heightCalculate = Int(heightstr)
                                if aryOption.count - 1 ==  subIndex{
                                    if(Int(heightstr) > heightCalculate){
                                        heightCalculate = Int(heightstr)
                                    }
                                    yAxis = yAxis + Int(heightCalculate)
                                    xAxis = 18
                                    //yAxis = yAxis + Int(30.0)
                                }else{
                                    if(Int(heightstr) > heightCalculate){
                                        heightCalculate = Int(heightstr)
                                    }
                                    xAxis = xAxis + Int((viewLegend.frame.width)/CGFloat(strMODE))
                                }
                            }else if(subIndex%strMODE) == 1{
                                lblLegendTitle.frame = CGRect(x: CGFloat(xAxis), y: CGFloat(yAxis), width: (viewLegend.frame.width)/CGFloat(strMODE) - 16, height: heightstr)
                            
                               // yAxis = yAxis + Int(30.0)
                                if(Int(heightstr) > heightCalculate){
                                    heightCalculate = Int(heightstr)
                                }
                                yAxis = yAxis + Int(heightCalculate)
                                xAxis = 18
                            }
                            
                        }
                        
                        
                       
                        
                        
                        viewLegend.addSubview(lblLegendTitle)
                    }
                }
        }
            let lblseparate = UILabel()
            lblseparate.frame = CGRect(x: 18.0, y: CGFloat(yAxis + 5), width: (viewLegend.frame.width) - 16, height: 1)
            lblseparate.backgroundColor = .lightGray
            lblseparate.alpha = 0.8
            viewLegend.addSubview(lblseparate)
            yAxis = yAxis + Int(lblseparate.frame.height) + 12
    }
        viewLegend.tag = yAxis
        OnResultBlock(viewLegend)

 }
    func estimatedHeightOfLabel(text: String, width : Int , font : Int) -> CGFloat {
        
        let size = CGSize(width: width , height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(font))]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight + 8
    }

}
func getGraphLegendCategory() -> NSArray {
   // return NSArray()
    if(nsud.value(forKey: "MasterServiceAutomation") != nil){
        if nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
            let dict = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            if(dict.value(forKey: "GraphLegendCategoryExtDcs") is NSArray){
              
                return dict.value(forKey: "GraphLegendCategoryExtDcs") as! NSArray
            }
        }
    }
    return NSArray()
}

