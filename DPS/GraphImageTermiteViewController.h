//
//  GraphImageTermiteViewController.h
//  DPS
//
//  Created by Saavan Patidar on 17/11/17.
//  Copyright © 2017 Saavan. All rights reserved.
////

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface GraphImageTermiteViewController : UIViewController
{
    IBOutlet UIImageView *imgView;
    IBOutlet UILabel *lblCount;
    
    NSMutableArray *arrayImages;
    
    int activeIndex;
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityTask;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entityCurrentService,
    *entitySoldServiceStandardDetail;
    
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    NSEntityDescription *entityTermiteTexas;

}

- (IBAction)action_DeleteImage:(id)sender;
- (IBAction)action_Back:(id)sender;
@property(nonatomic,strong) NSMutableArray *arrOfImages;
@property(nonatomic,strong) NSDictionary *dictOfWorkOrdersImagePreview;
@property (strong, nonatomic) IBOutlet UILabel *imageName;
@property (strong, nonatomic) NSMutableArray *arrayOfImages;
@property (strong, nonatomic) NSString *imagePreviewName;
@property (strong, nonatomic) NSString *indexOfImage;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property(nonatomic,strong) NSString *statusOfWorkOrder;
@property (weak, nonatomic) IBOutlet UITextView *textViewImageCaption;
@property (weak, nonatomic) IBOutlet UIButton *btnEditCaption;
- (IBAction)action_EditCaption:(id)sender;
@property (strong, nonatomic) NSMutableArray *arrOfImageCaptionsSaved;
@property (strong, nonatomic) NSMutableArray *arrOfImageDescriptionSaved;
- (IBAction)action_EditImage:(id)sender;
- (IBAction)action_EditDescription:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textViewImageDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnEditDescription;
@property (strong, nonatomic) NSString *strLeadId;
@property (strong, nonatomic) NSString *strCompanyKey;
@property (strong, nonatomic) NSString *strUserName;
@property (strong, nonatomic) NSString *strFromWhere;

@end
