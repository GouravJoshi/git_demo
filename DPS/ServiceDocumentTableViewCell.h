//
//  ServiceDocumentTableViewCell.h
//  DPS
//
//  Created by Saavan Patidar on 20/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavan 2021

#import <UIKit/UIKit.h>

@interface ServiceDocumentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_Title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Description;
@property (strong, nonatomic) IBOutlet UILabel *lbl_TitleHeader;

@end
