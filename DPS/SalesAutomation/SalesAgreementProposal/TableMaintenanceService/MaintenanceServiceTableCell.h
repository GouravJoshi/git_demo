//
//  MaintenanceServiceTableCell.h
//  DPS
//
//  Created by Rakesh Jain on 23/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaintenanceServiceTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblServiceNameMaintenance;
@property (weak, nonatomic) IBOutlet UILabel *lblFrequencyMaintenance;
@property (weak, nonatomic) IBOutlet UILabel *lblInitialPriceMaintenance;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenancePriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblRenewalService;
@property (weak, nonatomic) IBOutlet UILabel *lblLineRenewalSeperator1;
@property (weak, nonatomic) IBOutlet UILabel *lblLineRenewalSeperator2;
@end
