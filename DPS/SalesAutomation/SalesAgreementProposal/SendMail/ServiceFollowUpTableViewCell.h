//
//  ServiceFollowUpTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 10/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceFollowUpTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDeptName;
@property (weak, nonatomic) IBOutlet UIButton *btnFollowUp;
@property (weak, nonatomic) IBOutlet UITextView *txtViewService;

@end
