//
//  ProposalFollowUpTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 10/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProposalFollowUpTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblServiceProposal;
@property (weak, nonatomic) IBOutlet UIButton *btnProposalFollowUp;
@property (weak, nonatomic) IBOutlet UITextView *txtViewProposal;

@end
