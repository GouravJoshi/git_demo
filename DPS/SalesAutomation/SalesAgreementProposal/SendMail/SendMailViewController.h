//
//  SendMailViewController.h
//  DPS
//
//  Created by Rakesh Jain on 02/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

//
//  SendMailViewController.h
//  DPS
//
//  Created by Rakesh Jain on 02/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ServiceFollowUpDcs+CoreDataClass.h"
#import "ServiceFollowUpDcs+CoreDataProperties.h"
#import "ProposalFollowUpDcs+CoreDataClass.h"
#import "ProposalFollowUpDcs+CoreDataProperties.h"
#import "AllImportsViewController.h"
// Clark Pest Data Base

#import "LeadCommercialScopeExtDc+CoreDataClass.h"
#import "LeadCommercialScopeExtDc+CoreDataProperties.h"

#import "LeadCommercialTargetExtDc+CoreDataClass.h"
#import "LeadCommercialTargetExtDc+CoreDataProperties.h"

#import "LeadCommercialDiscountExtDc+CoreDataClass.h"
#import "LeadCommercialDiscountExtDc+CoreDataProperties.h"

#import "LeadCommercialInitialInfoExtDc+CoreDataClass.h"
#import "LeadCommercialInitialInfoExtDc+CoreDataProperties.h"

#import "LeadCommercialMaintInfoExtDc+CoreDataClass.h"
#import "LeadCommercialMaintInfoExtDc+CoreDataProperties.h"

#import "LeadCommercialTermsExtDc+CoreDataClass.h"
#import "LeadCommercialTermsExtDc+CoreDataProperties.h"

#import "LeadCommercialDetailExtDc+CoreDataClass.h"
#import "LeadCommercialDetailExtDc+CoreDataProperties.h"

#import "AppointmentView.h"
#import "ClarkPestSalesAgreementProposaliPhone.h"

@interface SendMailViewController : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIScrollViewDelegate>
{
    
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,
    *entityDocumentsDetail,
    *entityCurrentService,
    *entityServiceFollowUp,
    *entityProposalFollowUp,*entityLeadAgreementChecklistSetups,*entityElectronicAuthorizedForm,*entityLeadAppliedDiscounts,*entityRenewalServiceDetail,*entityTagDetail;
    
    NSEntityDescription *entityModifyDate;
    NSFetchRequest *requestModifyDate;
    NSSortDescriptor *sortDescriptorModifyDate;
    NSArray *sortDescriptorsModifyDate;
    NSManagedObject *matchesModifyDate;
    NSArray *arrAllObjModifyDate;
    
    NSEntityDescription *entitySalesDynamic;
    NSFetchRequest *requestNewSalesDynamic;
    NSSortDescriptor *sortDescriptorSalesDynamic;
    NSArray *sortDescriptorsSalesDynamic;
    NSManagedObject *matchesSalesDynamic;
    NSArray *arrAllObjSalesDynamic;
    //Clark Pest
    NSEntityDescription *entityLeadCommercialScopeExtDc,*entityLeadCommercialTargetExtDc,*entityLeadCommercialMaintInfoExtDc,*entityLeadCommercialInitialInfoExtDc,*entityLeadCommercialDiscountExtDc,*entityLeadCommercialDetailExtDc,*entityLeadCommercialTermsExtDc,*entityLeadMarketingContentExtDc;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerModifyDate;
@property (weak, nonatomic) IBOutlet UILabel *lblFromValue;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailId;
- (IBAction)actionOnAdd:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblEmailData;
- (IBAction)actionOnSendEmail:(id)sender;
- (IBAction)actionOnContinueToSetup:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewSendMail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;
@property (strong, nonatomic) IBOutlet UIButton *btnSurveyy;
- (IBAction)actionOnInitialSetup:(id)sender;
- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_View_T;
@property (weak, nonatomic) IBOutlet UIButton *btnInitialSetupDummy;

- (IBAction)actionOnInitialSetupDummy:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnInitialSetup;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnSendMail;
-(void)syncCall;
@property(weak,nonatomic) NSString *strForSendProposal;

//Nilind 10 Feb
@property (strong, nonatomic) IBOutlet UIView *viewForFollowUp;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceFollowUp;
@property (weak, nonatomic) IBOutlet UITableView *tblServiceFollowUp;
@property (weak, nonatomic) IBOutlet UILabel *lblProposalFollowUp;
@property (weak, nonatomic) IBOutlet UITableView *tblProposalFollowUp;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_tableService_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_tableProposal_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ScrollView_H;

@property (weak, nonatomic) IBOutlet UIScrollView *_scrollViewSendMail;
@property (weak, nonatomic) IBOutlet UIButton *btnForFollowUp;

- (IBAction)actionOnFollowUp:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblDocuments;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTableEmail_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableDocument_H;

@property (weak, nonatomic) NSString *isCustomerPresent;
@property (strong, nonatomic) NSString *strFromWhere;

@property (weak, nonatomic) IBOutlet UILabel *lblSendDocument;
@property (weak, nonatomic) IBOutlet UITextField *txtSubjectEmail;


//End
@end
