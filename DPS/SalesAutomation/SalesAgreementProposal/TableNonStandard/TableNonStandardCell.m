//
//  TableNonStandardCell.m
//  DPS
//
//  Created by Rakesh Jain on 23/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "TableNonStandardCell.h"

@implementation TableNonStandardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
