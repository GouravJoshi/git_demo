//
//  TableNonStandardCell.h
//  DPS
//
//  Created by Rakesh Jain on 23/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableNonStandardCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblServiceName;
@property (weak, nonatomic) IBOutlet UILabel *lblInitialPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForBillingFrequency;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceFreqName;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintPriceValue;

@end
