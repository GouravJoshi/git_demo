//
//  InitialSetupTableViewCell.h
//  DPS Change
//
//  Created by Rakesh Jain on 10/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InitialSetupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAccNo;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderNo;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderDate;

@end
