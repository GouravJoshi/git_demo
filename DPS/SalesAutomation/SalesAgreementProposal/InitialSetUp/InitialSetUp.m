//
//  InitialSetUp.m
//  DPS
//
//  Created by Rakesh Jain on 24/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "InitialSetUp.h"
#import "InitialSetupTableViewCell.h"
#import "AppointmentView.h"
#import "Global.h"
#import "DejalActivityView.h"
#import "SendMailViewController.h"
#import "DPS-Swift.h"

@interface InitialSetUp ()
{
    NSString *strLeadId;
    UIView *viewBackGround;
    UITableView *tblData;
    NSMutableArray *arrServiceName,*arrPreferTime,*arrNonPreferTime,*arrServiceRoute,*arrServiceTime,*arrOfSelectedServiceName,*arrOfServiceDepartmentName,*arrOfDictAllDataOfService,*arrOfPreferableSysName,*arrOfNonPreferableSysName,*arrofRangeTimeId;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    NSString *strDate,*isPrimarySelectedService,*strSelectedTimeIn24Hrs;
    BOOL chkForDate;
    NSMutableArray *arrSourceSelectedValue;
    BOOL  isSelectEnable,chkServiceTime;
    NSDictionary *dictServiceName,*dictServiceSYSNAME;
    double totalServicePrice;
    
    //Nilind 02 Dec
    
    NSMutableArray *arrServiceSysName,*arrSysName;
    NSDictionary *dictDusration;
    NSString *strSalesAutoMainUrl,*strCompanyKey;
    
    
    NSString *strAccountNo,*strAddress1,*strZipcode,*strCountrySysName,*strStateSysName,*strCityName;
    Global *global;
    NSMutableArray *arrRout,*arrRouteSysName;
    NSDictionary *dictRout;
    NSArray *arrOtherRout,*arrSuggestedRout;
    BOOL entry;
    NSTimer *_timer;
    NSMutableData *responseData;
    NSMutableArray *arrRoutOther,*arrRoutSuggested,*arrOfViewAppointments,*arrRouteNumber,*arrRouteNumberSelected,*arrRouteSysNameSelected;
    NSArray *arrOfResponseRouteAppointments;
    NSString *strSelectedRoutes;
    int indexxRouteSelected;
    NSString *strTypeOfTime,*isLockTime,*islockRoute,*strSelectedPreferableSysName,*strSeletcedNonPreferableSysName,*strSelectedRangeTimeId;
    //................
    NSDictionary *dictMasters;
    
    //Nilind 28 Dec
    
    NSMutableArray *temArrServiceName;
    NSMutableArray *temArrServiceSysName;
    NSString *strTitile;
    UILabel *lblOk;
    //...........
}

@end

@implementation InitialSetUp

- (void)viewDidLoad
{
    
    //Nilind 18 Jan
    
    _const_PreferableView_H.constant=0;
    _const_ViewMain_H.constant=890-143;
    _btnPreferableTime.hidden=YES;
    _btnNewPreferableTime.hidden=YES;
    _lblPreferable.hidden=YES;
    _lblNonPreferable.hidden=YES;
    strSelectedRangeTimeId = @"";
    
    _txtServicePrice.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePrice.layer.borderWidth=1.0;
    _txtServicePrice.layer.cornerRadius=5.0;
    
    _txtServiceProdValue.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceProdValue.layer.borderWidth=1.0;
    _txtServiceProdValue.layer.cornerRadius=5.0;
    
    _txtServiceDuration.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceDuration.layer.borderWidth=1.0;
    _txtServiceDuration.layer.cornerRadius=5.0;
    
    _txtServiceDate.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceDate.layer.borderWidth=1.0;
    _txtServiceDate.layer.cornerRadius=5.0;
    
    _txtServiceTime.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceTime.layer.borderWidth=1.0;
    _txtServiceTime.layer.cornerRadius=5.0;
    
    
    
    //.text=[NSString stringWithFormat:@"Lead#:%@, A/C#:%@",[matchesGeneralInfo valueForKey:@"leadNumber"],[matchesGeneralInfo valueForKey:@"accountNo"]];
    _txtServiceDuration.textAlignment=NSTextAlignmentLeft;
    
    SendMailViewController *objSendMailViewController=[[SendMailViewController alloc]init];
    [objSendMailViewController syncCall];
    NSUserDefaults *defsFromInitialSetup=[NSUserDefaults standardUserDefaults];
    [defsFromInitialSetup setBool:YES forKey:@"FromInitialSetup"];
    [defsFromInitialSetup synchronize];
    
    //............
    
    [self leadTitleFetch];
    
    NSUserDefaults *defsdictMasters=[NSUserDefaults standardUserDefaults];
    dictMasters=[defsdictMasters valueForKey:@"MasterSalesAutomation"];
    
    strSelectedRoutes=@"";
    isLockTime=@"false";
    islockRoute=@"false";
    strSelectedTimeIn24Hrs=@"";
    indexxRouteSelected=0;
    strTypeOfTime=@"1";
    [super viewDidLoad];
    responseData=[[NSMutableData alloc]init];
    global = [[Global alloc] init];
    entry=YES;
    
    [_btnServiceRoute setTitle:@"--Select Route--" forState:UIControlStateNormal];
    [_btnInitialService setTitle:@"--Select Initial Service--" forState:UIControlStateNormal];
    [_btnPreferableTime setTitle:@"--Select Preferable Time--" forState:UIControlStateNormal];
    [_btnNewPreferableTime setTitle:@"--Select Non-Preferable Time--" forState:UIControlStateNormal];
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    arrServiceSysName=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    arrSourceSelectedValue=[[NSMutableArray alloc]init];
    isSelectEnable=NO;
    totalServicePrice=0;
    isPrimarySelectedService=@"";
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    
    //Nilind 3'rd Jan
    /*  NSUserDefaults *defs12=[NSUserDefaults standardUserDefaults];
     NSString *str;
     str=[defs12 valueForKey:@"fromInitialSetupClick"];
     
     if ([str isEqualToString:@"fromInitialSetupClick"])
     {
     strLeadId=[defs12 valueForKey:@"ForInitialSetUpLeadId"];
     
     [defs12 setValue:@"1234" forKey:@"fromInitialSetupClick"];
     [defs12 synchronize];
     
     }*/
    
    //.............
    
    
    arrServiceName=[[NSMutableArray alloc]init];
    arrOfServiceDepartmentName=[[NSMutableArray alloc]init];
    arrOfSelectedServiceName=[[NSMutableArray alloc]init];
    arrOfDictAllDataOfService=[[NSMutableArray alloc]init];
    arrServiceRoute=[[NSMutableArray alloc]init];
    arrRoutOther=[[NSMutableArray alloc]init];
    arrRoutSuggested=[[NSMutableArray alloc]init];
    //arrServiceRoute=[NSMutableArray arrayWithObjects:@"John",@"Smith",@"James",@"Rafel", nil];
    arrPreferTime=[[NSMutableArray alloc]init];
    arrOfNonPreferableSysName=[[NSMutableArray alloc]init];
    arrOfPreferableSysName=[[NSMutableArray alloc]init];
    arrofRangeTimeId=[[NSMutableArray alloc]init];

    
    //arrPreferTime=[NSMutableArray arrayWithObjects:@"08:00 AM - 10:00 AM",@"10:00 AM - 12:00 PM",@"12:00 PM - 02:00 PM",@"02:00 PM - 04:00 PM",@"04:00 PM - 06:00 PM",@"06:00 PM - 08:00 PM",@"08:00 PM - 10:00 PM",@"10:00 PM - 12:00 PM", nil];
    //arrNonPreferTime=[NSMutableArray arrayWithObjects:@"08:00 AM - 10:00 AM",@"10:00 AM - 12:00 PM",@"12:00 PM - 02:00 PM",@"02:00 PM - 04:00 PM",@"04:00 PM - 06:00 PM",@"06:00 PM - 08:00 PM",@"08:00 PM - 10:00 PM",@"10:00 PM - 12:00 PM", nil];
    
    
    arrRout=[[NSMutableArray alloc]init];
    
    arrRouteNumber=[[NSMutableArray alloc]init];
    arrRouteNumberSelected=[[NSMutableArray alloc]init];
    arrRouteSysNameSelected=[[NSMutableArray alloc]init];
    
    _txtViewServiceInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewServiceInstruction.layer.borderWidth=1.0;
    
    _txtViewSetupInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewSetupInstruction.layer.borderWidth=1.0;
    
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    
    //...............
    
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.dataSource=self;
    tblData.delegate=self;
    
#pragma mark- Depratment Master
    
    [_imgChkBoxSpecificTime setImage:[UIImage imageNamed:@"check_box_2.png"]];
    [_imgChkBoxRangeTime setImage:[UIImage imageNamed:@"check_box_1.png"]];
    //_viewTwoPreferableTime.hidden=YES;
    _viewTwoPreferableTime.hidden=NO;
    //  [_viewTwoPreferableTime setUserInteractionEnabled:NO];
    
    [self serviceName];
    [self serviceSYSNAME];
    [self fetchFromCoreDataStandard];
    
    [self performSelector:@selector(methodCalling) withObject:nil afterDelay:1.0];
    
    //Nilind 06 Jan
    
    UITapGestureRecognizer *viewTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    viewTapGestureRecognizer.numberOfTapsRequired = 1;
    viewTapGestureRecognizer.enabled = YES;
    viewTapGestureRecognizer.cancelsTouchesInView = NO;
    [_viewTwoPreferableTime addGestureRecognizer:viewTapGestureRecognizer];
    //..........
    
    
    
}
- (void)viewTap:(UITapGestureRecognizer *)gesture
{
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    //Nilind 31 Dec
    
    /* if(strLeadId.length==0||[strLeadId isEqual:[NSNull null]])
     {
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     strLeadId=[defs valueForKey:@"ForInitialSetUpLeadId"];
     }*/
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    //.............
    
}
-(void)methodCalling
{
    
    [_activityIndicator startAnimating];
    [_activityIndicator setHidden:NO];
    
    [self salesFetch];
    [self rangeTimeApi];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionOnBack:(id)sender
{
    [self endEditing];
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    [defsBack setBool:NO forKey:@"isFromBackSendMail"];
    [defsBack synchronize];

    [self goToAppointment];
    //[self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:NO];
    
    /* for (UIViewController *controller in self.navigationController.viewControllers)
     {
     if ([controller isKindOfClass:[AppointmentView class]])
     {
     [self.navigationController popToViewController:controller animated:NO];
     break;
     }
     }*/
}

- (IBAction)actionOnInitialSetUp:(id)sender
{
     [self endEditing];
    UIButton *btn=(UIButton *)sender;
    if (btn.selected==NO)
    {
        [_imgChkBoxInitialSetup setImage:[UIImage imageNamed:@"check_box_2.png"]];
        btn.selected=YES;
        _viewInitialService.hidden=YES;
        
    }
    else
    {
        [_imgChkBoxInitialSetup setImage:[UIImage imageNamed:@"check_box_1.png"]];
        btn.selected=NO;
        _viewInitialService.hidden=NO;
    }
}

- (IBAction)actionOnSpecificTime:(id)sender
{
     [self endEditing];
    strTypeOfTime=@"1";
    [_imgChkBoxSpecificTime setImage:[UIImage imageNamed:@"check_box_2.png"]];
    [_imgChkBoxRangeTime setImage:[UIImage imageNamed:@"check_box_1.png"]];
    // _viewTwoPreferableTime.hidden=YES;
    
    //[_viewTwoPreferableTime setUserInteractionEnabled:NO];
    
    _const_PreferableView_H.constant=0;
    _const_ViewMain_H.constant=890-143;
    _btnPreferableTime.hidden=YES;
    _btnNewPreferableTime.hidden=YES;
    _lblPreferable.hidden=YES;
    _lblNonPreferable.hidden=YES;
    
    
}

- (IBAction)actionOnRangeTime:(id)sender
{
     [self endEditing];
    strTypeOfTime=@"2";
    [_imgChkBoxRangeTime setImage:[UIImage imageNamed:@"check_box_2.png"]];
    [_imgChkBoxSpecificTime setImage:[UIImage imageNamed:@"check_box_1.png"]];
    _viewTwoPreferableTime.hidden=NO;
    // [_viewTwoPreferableTime setUserInteractionEnabled:YES];
    _const_PreferableView_H.constant=143;
    _const_ViewMain_H.constant=890;
    _btnPreferableTime.hidden=NO;
    _btnNewPreferableTime.hidden=NO;
    _lblPreferable.hidden=NO;
    _lblNonPreferable.hidden=NO;
    
}
- (IBAction)actionOnInitialService:(id)sender
{
     [self endEditing];
    lblOk.hidden=NO;
    //NSMutableArray *
    temArrServiceName=[[NSMutableArray alloc]init];
    //NSMutableArray *
    temArrServiceSysName=[[NSMutableArray alloc]init];
    /* //Working 27 Dec
     for (int k=0; k<arrServiceSysName.count; k++)
     {
     
     NSString *strServiceSysName=arrServiceSysName[k];
     
     NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
     for (int i=0; i<arrCtgry.count; i++)
     {
     NSDictionary *dict=[arrCtgry objectAtIndex:i];
     NSArray *arrServices=[dict valueForKey:@"Services"];
     for (int j=0; j<arrServices.count; j++)
     {
     NSDictionary *dict=[arrServices objectAtIndex:j];
     NSString *strServiceSysNameToCompare=[dict valueForKey:@"InitialServiceSysName"];
     if ([strServiceSysNameToCompare isEqualToString:strServiceSysName]) {
     
     [temArrServiceName addObject:[dict valueForKey:@"Name"]];
     [temArrServiceSysName addObject:[dict valueForKey:@"SysName"]];
     }
     }
     }
     }
     */
    //Nilind 27 Dec
    
    NSMutableArray *arrInitial;
    arrInitial=[[NSMutableArray alloc]init];
    if (arrServiceSysName.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No service available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }
    else
    {
        for (int k=0; k<arrServiceSysName.count; k++)
        {
            
            NSString *strServiceSysName=arrServiceSysName[k];
            
            NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
            for (int i=0; i<arrCtgry.count; i++)
            {
                NSDictionary *dict=[arrCtgry objectAtIndex:i];
                NSArray *arrServices=[dict valueForKey:@"Services"];
                for (int j=0; j<arrServices.count; j++)
                {
                    NSDictionary *dict=[arrServices objectAtIndex:j];
                    NSString *strServiceSysNameToCompare=[dict valueForKey:@"SysName"];
                    //[arrInitial addObject:strServiceSysName];
                    if ([strServiceSysNameToCompare isEqualToString:strServiceSysName])
                    {
                        
                        //[temArrServiceName addObject:[dict valueForKey:@"Name"]];
                        [arrInitial addObject:[dict valueForKey:@"InitialServiceSysName"]];
                    }
                }
            }
        }
        NSLog(@"ArrInitial>>%@",arrInitial);
        /*arrServiceName=nil;
         arrServiceSysName=nil;
         arrServiceName=[[NSMutableArray alloc]init];
         arrServiceSysName=[[NSMutableArray alloc]init];
         
         arrServiceName=temArrServiceName;
         arrServiceSysName=temArrServiceSysName;
         
         tblData.tag=10;
         [self tableLoad:tblData.tag];*/
        
        /*  if (arrServiceSysName.count==0)
         {
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No service available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
         [alert show];
         }
         else
         {*/
        //  NSMutableArray *arrInitial;
        //  arrInitial=[[NSMutableArray alloc]init];
        
        for(int l=0;l<arrInitial.count;l++)
        {
            if([[arrInitial objectAtIndex:l]isEqualToString:@""])
            {
                [arrInitial removeObjectAtIndex:l];
            }
        }
        for (int k=0; k<arrInitial.count; k++)
        {
            
            NSString *strServiceSysName=arrInitial[k];
            
            NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
            for (int i=0; i<arrCtgry.count; i++)
            {
                NSDictionary *dict=[arrCtgry objectAtIndex:i];
                NSArray *arrServices=[dict valueForKey:@"Services"];
                for (int j=0; j<arrServices.count; j++)
                {
                    NSDictionary *dict=[arrServices objectAtIndex:j];
                    NSString *strServiceSysNameToCompare=[dict valueForKey:@"SysName"];
                    //[arrInitial addObject:strServiceSysName];
                    if ([strServiceSysNameToCompare isEqualToString:strServiceSysName])
                    {
                        
                        [temArrServiceName addObject:[dict valueForKey:@"Name"]];
                        [temArrServiceSysName addObject:[dict valueForKey:@"SysName"]];
                    }
                }
            }
        }
        NSLog(@"ArrtemArrServiceName>>%@",temArrServiceName);
        /* arrServiceName=nil;
         arrServiceSysName=nil;
         arrServiceName=[[NSMutableArray alloc]init];
         arrServiceSysName=[[NSMutableArray alloc]init];
         
         arrServiceName=temArrServiceName;
         arrServiceSysName=temArrServiceSysName;*/
        
        
        //23 Mrach
        
        NSArray * uniquetemArrServiceName = [[NSOrderedSet orderedSetWithArray:temArrServiceName] array];
        temArrServiceName=[[NSMutableArray alloc]init];
        
        for (int i=0; i<uniquetemArrServiceName.count; i++)
        {
            [temArrServiceName addObject:[uniquetemArrServiceName objectAtIndex:i]];
        }
        
        NSArray * uniquetemArrServiceSysName = [[NSOrderedSet orderedSetWithArray:temArrServiceSysName] array];
        temArrServiceSysName=[[NSMutableArray alloc]init];
        
        for (int i=0; i<uniquetemArrServiceSysName.count; i++)
        {
            [temArrServiceSysName addObject:[uniquetemArrServiceSysName objectAtIndex:i]];
        }
        
        //End
        
        tblData.tag=10;
        [self tableLoad:tblData.tag];
    }
    //..........................
    
    /*  //Working
     arrServiceName=nil;
     arrServiceSysName=nil;
     arrServiceName=[[NSMutableArray alloc]init];
     arrServiceSysName=[[NSMutableArray alloc]init];
     
     arrServiceName=temArrServiceName;
     arrServiceSysName=temArrServiceSysName;
     
     tblData.tag=10;
     [self tableLoad:tblData.tag];*/
}

- (IBAction)actionOnServiceDate:(id)sender
{
     [self endEditing];
    strTitile=@"SELECT DATE";
    chkForDate=YES;
    chkServiceTime=NO;
    [self addPickerViewDateTo];
}
- (IBAction)actionOnServiceRoute:(id)sender
{
     [self endEditing];
    lblOk.hidden=NO;
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    //_timer = [NSTimer scheduledTimerWithTimeInterval:10.0f
    //                                              target:self
    //                                            selector:@selector(_timerFired:)
    //                                            userInfo:nil
    //                                             repeats:YES];
    // [self routApi];
    
    tblData.tag=20;
    [self tableLoad:tblData.tag];
}
- (void)_timerFired:(NSTimer *)timer {
    NSLog(@"ping");
    // [DejalBezelActivityView removeView];
}
- (IBAction)actionOnPreferableTime:(id)sender
{
     [self endEditing];
    
    if( [_imgChkBoxRangeTime.image isEqual:[UIImage imageNamed: @"check_box_2.png"]])
    {
        
        tblData.tag=30;
        [self tableLoad:tblData.tag];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select range time above" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
    lblOk.hidden=YES;
}
- (IBAction)actionOnNewPreferableTime:(id)sender
{
    [self endEditing];
    
    if( [_imgChkBoxRangeTime.image isEqual:[UIImage imageNamed: @"check_box_2.png"]])
    {
        tblData.tag=40;
        [self tableLoad:tblData.tag];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select range time above" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    lblOk.hidden=YES;
}
- (IBAction)actionOnLockRout:(id)sender
{
     [self endEditing];
    UIButton *btn=(UIButton *)sender;
    if (btn.selected==NO)
    {
        [_imgChkBoxLockRout setImage:[UIImage imageNamed:@"check_box_2.png"]];
        btn.selected=YES;
        islockRoute=@"true";
    }
    else
    {
        [_imgChkBoxLockRout setImage:[UIImage imageNamed:@"check_box_1.png"]];
        btn.selected=NO;
        islockRoute=@"false";
    }
}
- (IBAction)actionOnLockTime:(id)sender
{
     [self endEditing];
    UIButton *btn=(UIButton *)sender;
    if (btn.selected==NO)
    {
        [_imgChkBoxLockTime setImage:[UIImage imageNamed:@"check_box_2.png"]];
        btn.selected=YES;
        isLockTime=@"true";
    }
    else
    {
        [_imgChkBoxLockTime setImage:[UIImage imageNamed:@"check_box_1.png"]];
        btn.selected=NO;
        isLockTime=@"false";
    }
    
}
#pragma mark- ACTION VIEW APPOINTMENT
- (IBAction)actionOnViewAppointment:(id)sender
{
     [self endEditing];
    if (arrSourceSelectedValue.count==0) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"ALert" message:@"Please select Route" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else if (_txtServiceDate.text.length==0){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"ALert" message:@"Please select service date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
        
        [self performSelector:@selector(apiViewAppointment) withObject:nil afterDelay:1.0];
    }
}

- (IBAction)actionOnSaveContinue:(id)sender
{
     [self endEditing];
    _btnSaveContinue.enabled = NO;
    
    [UIView animateWithDuration:3 animations:^{
        
    } completion:^(BOOL finished) {
        _btnSaveContinue.enabled = YES;
        
    }
     ];
    
    
    NSString *errorMessage=[self validationCheck];
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    }
    else
    {
        //        [_activityIndicator startAnimating];
        //        [_activityIndicator setHidden:NO];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Saving Initial Setup..."];
        
        [self performSelector:@selector(sendToServerSaveSetup) withObject:nil afterDelay:0.5];
        
        //   [self sendToServerSaveSetup];
        
        
    }
}
//============================================================================
#pragma mark- TABLEVIEW DELEGATE METHOD
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_tblAppointment)
    {
        return arrOfViewAppointments.count;
    }else if (tblData.tag==10)
    {
        //return arrServiceName.count;
        return temArrServiceName.count;
    }
    else if (tblData.tag==20)
    {
        //return arrServiceRoute.count;
        return  arrRout.count;
    }
    else if (tblData.tag==30)
    {
        return arrPreferTime.count;
    }
    else if (tblData.tag==40)
    {
        return arrNonPreferTime.count;
    }
    else if (tblData.tag==50)
    {
        return arrPreferTime.count;
    }
    else
        return 0;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag==1)
    {
        static NSString *identifier=@"cellAppointment";
        InitialSetupTableViewCell *cell=[_tblAppointment dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[InitialSetupTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrOfViewAppointments.count==0)) {
            
            NSLog(@"IndexPath====%ld",(long)indexPath.row);
            
            NSDictionary *dictData=arrOfViewAppointments[indexPath.row];
            
            cell.lblAccNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"AccountNumber"]];
            cell.lblName.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
            cell.lblAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@",
                                  
                                  [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServicesAddress1"]],[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceCity"]],[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceState"]],[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceZipcode"]]];
            
            
            cell.lblOrderNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WorkOrderNo"]];
            
            NSString *dateString = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"CreatedDate"]];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];//@"yyyy-MM-dd'T'HH:mm:ss.sss"
            NSDate *date = [dateFormatter dateFromString:dateString];
            // Convert date object into desired format
            [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];//hh:mm
            NSString *newDateString = [dateFormatter stringFromDate:date];
            cell.lblOrderDate.text=newDateString;//[global modifyDate];//[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CreatedDate"]];
            
            
        }
        return cell;
        
    }
    else
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        
        // InitialSetupTableViewCell *cellAppoint=[_tblAppointment]
        
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (tblData.tag==10)
        {
            
            cell.textLabel.text=[temArrServiceName objectAtIndex:indexPath.row];//arrServiceName
            cell.detailTextLabel.text=[arrOfServiceDepartmentName objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            
            NSString *str=[temArrServiceName objectAtIndex:indexPath.row];//arrServiceName
            
            long index=100000;
            
            for (int k=0; k<arrOfSelectedServiceName.count; k++)
            {
                if ([arrOfSelectedServiceName containsObject:str])
                {
                    index=indexPath.row;
                    break;
                }
            }
            
            if (indexPath.row==index)
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
        }
        else if (tblData.tag==20)
        {
            cell.textLabel.text=[arrRout objectAtIndex:indexPath.row];//[arrServiceRoute objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            //
            //        NSString *str=[arrRoutSuggested objectAtIndex:indexPath.row];
            long index12=100000;
            //
            //        for (int k=0; k<arrOfSelectedServiceName.count; k++) {
            //            if ([arrOfSelectedServiceName containsObject:str]) {
            //                index12=indexPath.row;
            //                break;
            //            }
            //        }
            
            
            for (int k=0; k<arrRoutSuggested.count; k++)
            {
                if ([arrRoutSuggested containsObject:cell.textLabel.text])
                {
                    index12=indexPath.row;
                    break;
                    //[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
            if (indexPath.row==index12) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
            
            
        }
        else if (tblData.tag==30)
        {
            
            cell.textLabel.text=[arrPreferTime objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        else if (tblData.tag==40)
        {
            
            cell.textLabel.text=[arrNonPreferTime objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        else if (tblData.tag==50)
        {
            
            cell.textLabel.text=[arrPreferTime objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
        {
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        return cell;
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger i;
    i=tblData.tag;
    isSelectEnable=NO;
    switch (i)
    {
            
        case 10:
        {
            
            isSelectEnable=YES;
            
            if (isPrimarySelectedService.length==0) {
                
                isPrimarySelectedService=[arrOfServiceDepartmentName objectAtIndex:indexPath.row];
                
            }
            
            if ([isPrimarySelectedService isEqualToString:[arrOfServiceDepartmentName objectAtIndex:indexPath.row]])
            {
                
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    [arrOfSelectedServiceName addObject:[temArrServiceName objectAtIndex:indexPath.row]];//arrServiceName
                    [arrSysName addObject:[temArrServiceSysName objectAtIndex:indexPath.row]];//arrServiceSysName
                    
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    [arrOfSelectedServiceName removeObject:[temArrServiceName objectAtIndex:indexPath.row]];//arrServiceName
                    [arrSysName removeObject:[temArrServiceSysName objectAtIndex:indexPath.row]];//arrServiceSysName
                }
                
                NSString *joinedComponents = [arrOfSelectedServiceName componentsJoinedByString:@","];
                if (joinedComponents.length==0)
                {
                    
                    [_btnInitialService setTitle:@"" forState:UIControlStateNormal];
                    _txtServicePrice.text=@"";
                    _txtServiceProdValue.text=@"";
                    isPrimarySelectedService=@"";
                }
                else
                {
                    
                    [_btnInitialService setTitle:joinedComponents forState:UIControlStateNormal];
                    
                    [self getTotalServicePrice];
                }
                
            } else {
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Selection must contain same department name."
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                      }];
                [alert addAction:yes];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            
            break;
        }
        case 20:
        {
            isSelectEnable=YES;
            
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                // [arrSourceSelectedValue addObject:[arrServiceRoute objectAtIndex:indexPath.row]];
                [arrSourceSelectedValue addObject:[arrRout objectAtIndex:indexPath.row]];
                [arrRouteNumberSelected addObject:[arrRouteNumber objectAtIndex:indexPath.row]];
                [arrRouteSysNameSelected addObject:[arrRouteNumber objectAtIndex:indexPath.row]];
                [arrRoutSuggested addObject:[arrRout objectAtIndex:indexPath.row]];
                
                
                
            }
            else
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                // [arrSourceSelectedValue removeObject:[arrServiceRoute objectAtIndex:indexPath.row]];
                [arrSourceSelectedValue removeObject:[arrRout objectAtIndex:indexPath.row]];
                [arrRouteNumberSelected removeObject:[arrRouteNumber objectAtIndex:indexPath.row]];
                [arrRoutSuggested removeObject:[arrRout objectAtIndex:indexPath.row]];
                [arrRouteSysNameSelected removeObject:[arrRouteNumber objectAtIndex:indexPath.row]];
                
            }
            
            //  [_btnServiceRoute setTitle:[arrServiceRoute objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            [_btnServiceRoute setTitle:[arrRout objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            break;
        }
        case 30:
        {
            [_btnPreferableTime setTitle:[arrPreferTime objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            strSelectedPreferableSysName=arrOfPreferableSysName[indexPath.row];
            strSelectedRangeTimeId =  arrofRangeTimeId[indexPath.row];

            break;
        }
        case 40:
        {
            [_btnNewPreferableTime setTitle:[arrNonPreferTime objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            strSeletcedNonPreferableSysName=arrOfNonPreferableSysName[indexPath.row];
            break;
        }
        case 50:
        {
            // [_btnServiceTime setTitle:[arrPreferTime objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            break;
        }
            
        default:
            break;
    }
    
    if (isSelectEnable)
    {
        
    }
    else
    {
        [tblData removeFromSuperview];
        [viewBackGround removeFromSuperview];
    }
    //[viewBackGround removeFromSuperview];
    //[tblData removeFromSuperview];
    // [DejalActivityView removeView];
}

//.......................................................
#pragma mark- Tableview Delegate
//.......................................................
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // [_txtInitialPrice resignFirstResponder];
    //[_txtDiscount resignFirstResponder];
    [textField resignFirstResponder];
    return YES;
}

//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    [self setValueForParticipants];
    
    //Nilind 02 Dec
    NSLog(@"arrOfSelectedServiceName>>>>%@",arrOfSelectedServiceName);
    NSLog(@"isPrimarySelectedService>>>>%@",isPrimarySelectedService);
    NSLog(@"arrSysName>>>>%@",arrSysName);
    if (arrSysName.count==0)
    {
        _txtServiceDuration.text=@"";
        
    }
    else
    {
        //Nilind 09 Jan
        
        NSString *strPrimaryServiceSysNameForInitialService;
        strPrimaryServiceSysNameForInitialService=[arrSysName objectAtIndex:0];
        NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
        for (int i=0; i<arrCtgry.count; i++)
        {
            NSDictionary *dict=[arrCtgry objectAtIndex:i];
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strServiceSysNameToCompare=[dict valueForKey:@"InitialServiceSysName"];
                //[arrInitial addObject:strServiceSysName];
                if ([strPrimaryServiceSysNameForInitialService isEqualToString:strServiceSysNameToCompare])
                {
                    
                    strPrimaryServiceSysNameForInitialService=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                    
                    break;
                }
            }
        }
        
        
        //End
        _txtServiceDuration.text=[dictDusration valueForKey:strPrimaryServiceSysNameForInitialService];//[arrSysName objectAtIndex:0]];
    }
    NSLog(@"%@",dictDusration);
    
    //.......................
}
- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewBackGround removeFromSuperview];
    
    [tblData removeFromSuperview];
    [self setValueForParticipants];
    NSLog(@"isPrimarySelectedService>>>>%@",isPrimarySelectedService);
    
}


-(void)setValueForParticipants
{
    if (arrSourceSelectedValue.count==0)
    {
        [_btnServiceRoute setTitle:@"--Select Route--" forState:UIControlStateNormal];
        //strSourceSysName=@"";
    }
    else
    {
        //        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:yourArray];
        //        NSArray *arrayWithoutDuplicates = [orderedSet array];
        
        NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
        [_btnServiceRoute setTitle:joinedComponents forState:UIControlStateNormal];
        // NSString *joinedComponentsSysName = [arrSourceSelectedId componentsJoinedByString:@","];
        //strSourceSysName=joinedComponentsSysName;
        
    }
}


//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [viewBackGround removeFromSuperview];
//    [tblData removeFromSuperview];
//
//}
-(void)setTableFrame:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    
    lblOk=[[UILabel alloc]init];
    lblOk.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    lblOk.textColor=[UIColor whiteColor];
    lblOk.text=@"OK";
    lblOk.textAlignment=NSTextAlignmentCenter;
    lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-80, tblData.frame.origin.y+tblData.frame.size.height+2, 160, 40);
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-60, tblData.frame.origin.y+tblData.frame.size.height+2, 120, 40);
    }
    lblOk.layer.borderWidth=1.0;
    lblOk.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    lblOk.layer.cornerRadius=5.0;
    [viewBackGround addSubview:lblOk];
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            [self setTableFrame:i];
            break;
        }
        case 40:
        {
            [self setTableFrame:i];
            break;
        }
        case 50:
        {
            [self setTableFrame:i];
            break;
        }
            
            
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}
//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    
    if (chkForDate==YES)
    {
        [pickerDate setMinimumDate:[NSDate date]];
        pickerDate.datePickerMode =UIDatePickerModeDate;
    }
    else
    {
        pickerDate.datePickerMode =UIDatePickerModeTime;
        
        if (chkServiceTime==YES)
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
            [pickerDate setLocale:locale];
        }
        else
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
            [pickerDate setLocale:locale];
        }
        
    }
    //pickerDate.datePickerMode =UIDatePickerModeTime;  //UIDatePickerModeDate;
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=strTitile;//@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    if (chkForDate==YES)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        strDate = [dateFormat stringFromDate:pickerDate.date];
        _txtServiceDate.text=strDate;
        _txtServiceDate.textAlignment=NSTextAlignmentLeft;
        chkForDate=NO;
    }
    else
    {
        if (chkServiceTime==YES)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            // [dateFormat setDateFormat:@"hh:mm:a"];
            [dateFormat setDateFormat:@"HH:mm"];
            strDate = [dateFormat stringFromDate:pickerDate.date];
            _txtServiceTime.text=strDate;
            
            NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
            [dateFormat1 setDateFormat:@"HH:mm"];
            strSelectedTimeIn24Hrs=[dateFormat1 stringFromDate:pickerDate.date];
            
            chkForDate=YES;
            chkServiceTime=NO;
            _txtServiceTime.textAlignment=NSTextAlignmentLeft;
        }
        else
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"HH:mm"];
            strDate = [dateFormat stringFromDate:pickerDate.date];
            _txtServiceDuration.text=strDate;
            _txtServiceDuration.textAlignment=NSTextAlignmentLeft;
            chkForDate=YES;
        }
        
        
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}


- (IBAction)actionOnBackAppointment:(id)sender
{
     [self endEditing];
    [_viewAppointment removeFromSuperview];
}

- (IBAction)actionOnSkip:(id)sender
{
     [self endEditing];
    [self goToAppointment];
}
- (IBAction)actionOnServiceTime:(id)sender
{
     [self endEditing];
    //tblData.tag=50;
    // chkForDate=NO;
    //[self tableLoad:tblData.tag];
    strTitile=@"SELECT TIME";
    chkForDate=NO;
    chkServiceTime=YES;
    [self addPickerViewDateTo];
}
-(void)fetchFromCoreDataStandard
{
    
    
    
    
    if(strLeadId.length==0||[strLeadId isEqual:[NSNull null]])
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        //strLeadId=[defs valueForKey:@"ForInitialSetUpLeadId"];
        strLeadId=[defs valueForKey:@"LeadId"];
    }
    
    //.............
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrServiceName=[[NSMutableArray alloc]init];
    arrServiceSysName=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            if ([[matches valueForKey:@"isSold"] isEqualToString:@"true"]) {
                
                if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] isEqualToString:@"(null)"])
                {
                    [arrServiceName addObject: @""];
                }
                else
                {
                    [arrServiceName addObject: [dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                }
                
                //Nilind 02 Dec
                
                if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] isEqualToString:@"(null)"])
                {
                    [arrServiceSysName addObject: @""];
                }
                else
                {
                    [arrServiceSysName addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]];
                }
                
                //..............
                
                [arrOfDictAllDataOfService addObject:matches];
                
            }
        }
        
        
        
        
        [self findDepartmentNameForService];
    }
}
-(void)findDepartmentNameForService
{
    NSMutableArray *name,*sysName;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSString *strDepartmentId;
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            strDepartmentId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DepartmentId"]];
            
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]];
                
                for (int q=0; q<arrServiceName.count; q++) {
                    
                    if ([strNamee isEqualToString:[arrServiceName objectAtIndex:q]]) {
                        
                        [arrOfServiceDepartmentName addObject:strDepartmentId];
                        
                    }
                }
            }
        }
    }
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
}

-(void)serviceName
{
    NSMutableArray *name,*sysName;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                
            }
        }
    }
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
}
-(void)serviceSYSNAME
{
    NSMutableArray *name,*sysName,*esitimatedDuration;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    esitimatedDuration=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                
                [esitimatedDuration addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"InitialEstimatedDuration"]]];
                
            }
        }
    }
    dictServiceSYSNAME = [NSDictionary dictionaryWithObjects:sysName forKeys:name];
    
    dictDusration = [NSDictionary dictionaryWithObjects:esitimatedDuration forKeys:sysName];
}

-(void)getTotalServicePrice{
    
    totalServicePrice=0;
    
    //Nilind 28 Dec
    
    NSMutableArray *arrSoldService;
    arrSoldService=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSelectedServiceName.count; k++)
    {
        
        NSString *strServiceSysName=[dictServiceSYSNAME valueForKey:[NSString stringWithFormat:@"%@",[arrOfSelectedServiceName objectAtIndex:k]]];//arrOfSelectedServiceName[k];
        
        NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
        for (int i=0; i<arrCtgry.count; i++)
        {
            NSDictionary *dict=[arrCtgry objectAtIndex:i];
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strServiceSysNameToCompare=[dict valueForKey:@"InitialServiceSysName"];
                if ([strServiceSysNameToCompare isEqualToString:strServiceSysName]) {
                    
                    [arrSoldService addObject:[dict valueForKey:@"Name"]];
                    //[temArrServiceSysName addObject:[dict valueForKey:@"SysName"]];
                }
            }
        }
    }
    
    
    //........
    
    for (int k=0; k<arrSoldService.count; k++)//arrOfSelectedServiceName
    {
        
        
        NSString *strNameee=[dictServiceSYSNAME valueForKey:[NSString stringWithFormat:@"%@",[arrSoldService objectAtIndex:k]]];//arrOfSelectedServiceName
        
        for (int l=0; l<arrOfDictAllDataOfService.count; l++) {
            
            NSManagedObject *objTemp=arrOfDictAllDataOfService[l];
            
            NSString *strServiceSysName=[objTemp valueForKey:@"serviceSysName"];
            
            if ([strNameee isEqualToString:strServiceSysName]) {
                
                // double maintenanceValue=[[objTemp valueForKey:@"maintenancePrice"] doubleValue];
                
                //Nilind 02 Dec
                
                double initialPrice=[[objTemp valueForKey:@"totalInitialPrice"] doubleValue];//initialPrice
                double discount=[[objTemp valueForKey:@"discount"] doubleValue];
                
                double finalPrice=initialPrice-discount;
                //.............
                
                
                // totalServicePrice=maintenanceValue+totalServicePrice;
                
                totalServicePrice=finalPrice+totalServicePrice;
                
                _txtServicePrice.text=[NSString stringWithFormat:@"%.2f",totalServicePrice];
                _txtServiceProdValue.text=[NSString stringWithFormat:@"%.2f",totalServicePrice];
                
            }
        }
    }
}
-(void)getTotalServicePriceMinus{
    
    totalServicePrice=0;
    
    for (int k=0; k<arrOfSelectedServiceName.count; k++)//arrOfSelectedServiceName
    {
        
        NSString *strNameee=[dictServiceSYSNAME valueForKey:[NSString stringWithFormat:@"%@",[arrOfSelectedServiceName objectAtIndex:k]]];
        
        for (int l=0; l<arrOfDictAllDataOfService.count; l++) {
            
            NSManagedObject *objTemp=arrOfDictAllDataOfService[l];
            
            NSString *strServiceSysName=[objTemp valueForKey:@"serviceSysName"];
            
            if ([strNameee isEqualToString:strServiceSysName]) {
                
                double maintenanceValue=[[objTemp valueForKey:@"maintenancePrice"] doubleValue];
                
                totalServicePrice=totalServicePrice-maintenanceValue;
                
                _txtServicePrice.text=[NSString stringWithFormat:@"%.2f",totalServicePrice];
                _txtServiceProdValue.text=[NSString stringWithFormat:@"%.2f",totalServicePrice];
                
            }
        }
    }
}
#pragma mark- ROUTE API CALLING

-(void)routApi
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
        [_activityIndicator stopAnimating];
        [_activityIndicator setHidden:YES];
    }
    else
    {
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesSuggestRoute];
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"CustomerAddressId",
             @"accountNumber",
             @"companyKey",
             @"Address1",
             @"Address2",
             @"Zipcode",
             @"CountryId",
             @"CountrySysName",
             @"StateId",
             @"StateSysName",
             @"CityName",
             @"CitySysName",
             nil];
        value=[NSArray arrayWithObjects:
               @"",
               strAccountNo,
               strCompanyKey,
               strAddress1,
               @"",
               strZipcode,
               @"",
               strCountrySysName,
               @"",
               strStateSysName,
               strCityName,
               @"",
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil) {
            
            
        } else {
            
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
          //  NSString *jsonResponse = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

            
            if (dictionary.count==0 || dictionary==nil) {
                
            } else {
                
                arrRout=[[NSMutableArray alloc]init];
                arrRouteNumber=[[NSMutableArray alloc]init];
                arrRouteSysName=[[NSMutableArray alloc]init];
                
                arrOtherRout=[dictionary valueForKey:@"OtherRoute"];
                arrSuggestedRout=[dictionary valueForKey:@"SuggestedRoute"];
                for (int i=0; i<arrSuggestedRout.count; i++)
                {
                    NSDictionary *dict=[arrSuggestedRout objectAtIndex:i];
                    [arrRout addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteName"]]];
                    [arrRouteNumber addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRouteNumberSelected addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRouteSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRouteSysNameSelected addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    
                    [arrRoutSuggested addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteName"]]];
                    [arrSourceSelectedValue addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteName"]]];
                }
                if (!(arrSourceSelectedValue.count==0)) {
                    
                    NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
                    [_btnServiceRoute setTitle:joinedComponents forState:UIControlStateNormal];
                    
                }
                for (int i=0; i<arrOtherRout.count; i++)
                {
                    NSDictionary *dict=[arrOtherRout objectAtIndex:i];
                    [arrRout addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteName"]]];
                    [arrRouteNumber addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRouteSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    
                }
                
            }
        }
        [DejalActivityView removeView];
        [_activityIndicator stopAnimating];
        [_activityIndicator setHidden:YES];
    }
}

#pragma mark -SalesAuto Fetch Core Data


-(void)salesFetch
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        
        matches=arrAllObj[0];
        
        strAccountNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountNo"]];
        strAddress1=[NSString stringWithFormat:@"%@",[matches valueForKey:@"servicesAddress1"]];
        strZipcode=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceZipcode"]];
        strCountrySysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceCountry"]];
        
        strStateSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceState"]];
        strCityName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceCity"]];
        _lblTitle.text=[NSString stringWithFormat:@"Opportunity #: %@, A/C #: %@",[matches valueForKey:@"leadNumber"],[matches valueForKey:@"accountNo"]];
    }
    
    
}
//..........................

#pragma mark- RANGE TIME API CALLING

-(void)rangeTimeApi
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        [_activityIndicator stopAnimating];
        [_activityIndicator setHidden:YES];
    }
    else
    {
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlSalesRangeofTime,strCompanyKey];
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        NSURL *url = [NSURL URLWithString:strUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
        if (jsonData==nil) {
            
            
        } else {
            
            NSArray* responseArr = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (responseArr.count==0) {
                
            } else {
                arrPreferTime=[[NSMutableArray alloc]init];
                arrNonPreferTime=[[NSMutableArray alloc]init];
                
                arrOfNonPreferableSysName=[[NSMutableArray alloc]init];
                arrOfPreferableSysName=[[NSMutableArray alloc]init];
                arrofRangeTimeId=[[NSMutableArray alloc]init];

                for (int i=0;i<responseArr.count; i++)
                {
                    NSDictionary *dict=[responseArr objectAtIndex:i];
                    [arrPreferTime addObject:[NSString stringWithFormat:@"%@-%@",[dict valueForKey:@"StartInterval"],[dict valueForKey:@"EndInterval"]]];
                    [arrNonPreferTime addObject:[NSString stringWithFormat:@"%@-%@",[dict valueForKey:@"StartInterval"],[dict valueForKey:@"EndInterval"]]];
                    [arrOfPreferableSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                    [arrOfNonPreferableSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                    [arrofRangeTimeId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RangeofTimeId"]]];

                    
                }
            }
        }
        [self routApi];
    }
}

-(void)apiViewAppointment
{
    //http://service.dps.com/api/MobileAppToCore/GetViewAppointmentAsync
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
        
    }
    else
    {
        
        NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
        
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesViewAppointment];
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"serviceDate",
             @"routeNumbers",
             @"companyKey",
             nil];
        value=[NSArray arrayWithObjects:
               _txtServiceDate.text,
               joinedComponents,
               strCompanyKey,
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil) {
            
            [global AlertMethod:@"ALert" :@"No appointments available"];
            
        } else {
            
            NSArray *arrResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (arrResponse.count==0 || arrResponse==nil) {
                
                [global AlertMethod:@"ALert" :@"No appointments available"];
                
            } else {
                arrOfResponseRouteAppointments=arrResponse;
                indexxRouteSelected=0;
                arrOfViewAppointments=nil;
                arrOfViewAppointments=[[NSMutableArray alloc]init];
                
                strSelectedRoutes=arrSourceSelectedValue[indexxRouteSelected];
                _lblOne.text=strSelectedRoutes;
                for (int k=0; k<arrOfResponseRouteAppointments.count; k++) {
                    
                    NSDictionary *dictData=arrOfResponseRouteAppointments[k];
                    
                    NSString *strRouteNo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RouteNo"]];
                    if ([strRouteNo isEqualToString:arrRouteNumberSelected[indexxRouteSelected]]) {
                        
                        [arrOfViewAppointments addObject:dictData];
                        
                    }
                }
                
                _viewAppointment.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
                [self.view addSubview:_viewAppointment];
                tblData.tag=1999;
                [_tblAppointment reloadData];
                
            }
        }
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
    }
}

- (IBAction)action_ForwardRouteNames:(id)sender {
    
     [self endEditing];
    arrOfViewAppointments=nil;
    arrOfViewAppointments=[[NSMutableArray alloc]init];
    
    if (indexxRouteSelected+1<arrRouteNumberSelected.count) {
        
        indexxRouteSelected++;
        
        strSelectedRoutes=arrSourceSelectedValue[indexxRouteSelected];
        _lblOne.text=strSelectedRoutes;
        
        for (int k=0; k<arrOfResponseRouteAppointments.count; k++) {
            
            NSDictionary *dictData=arrOfResponseRouteAppointments[k];
            
            NSString *strRouteNo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RouteNo"]];
            if ([strRouteNo isEqualToString:arrRouteNumberSelected[indexxRouteSelected]]) {
                
                [arrOfViewAppointments addObject:dictData];
                
            }
        }
        [_tblAppointment reloadData];
        
    }
}

- (IBAction)action_BackRouteNames:(id)sender {
    
     [self endEditing];
    arrOfViewAppointments=nil;
    arrOfViewAppointments=[[NSMutableArray alloc]init];
    
    if (indexxRouteSelected-1<arrRouteNumberSelected.count) {
        
        indexxRouteSelected--;
        strSelectedRoutes=arrSourceSelectedValue[indexxRouteSelected];
        _lblOne.text=strSelectedRoutes;
        
        for (int k=0; k<arrOfResponseRouteAppointments.count; k++) {
            
            NSDictionary *dictData=arrOfResponseRouteAppointments[k];
            
            NSString *strRouteNo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RouteNo"]];
            if ([strRouteNo isEqualToString:arrRouteNumberSelected[indexxRouteSelected]]) {
                
                [arrOfViewAppointments addObject:dictData];
                
            }
        }
        [_tblAppointment reloadData];
        
    }
}

-(void)sendToServerSaveSetup
{
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    //UrlSalesSaveInitialSetupnGenerateWorkOrder
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
        
    }
    else
    {
        
        // NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
        
        NSString *strUrl,*strSetupInstruction,*strInitialServiceInstruction,*strPreferableTime,*strNonPreferableTime,*strPrimaryServiceSysName,*strRouteSysNAme,*strInitialServiceSysNames,*strAccountNoss,*strLeadNumber;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesSaveInitialSetup];
        NSArray *key,*value;
        
        strSetupInstruction=_txtViewSetupInstruction.text;
        strInitialServiceInstruction=_txtViewServiceInstruction.text;
        strPreferableTime=_btnPreferableTime.titleLabel.text;
        strNonPreferableTime=_btnNewPreferableTime.titleLabel.text;
        
        if (strSetupInstruction.length==0) {
            strSetupInstruction=@"";
        }
        if (strInitialServiceInstruction.length==0) {
            strInitialServiceInstruction=@"";
        }
        if (strPreferableTime.length==0) {
            strPreferableTime=@"";
        }
        if([strPreferableTime isEqual: [NSNull null]])
        {
            strPreferableTime=@"";
        }
        if ([strPreferableTime isEqualToString:@"--Select Preferable Time--"]) {
            strPreferableTime=@"";
        }
        
        if ([strNonPreferableTime isEqualToString:@"--Select Non-Preferable Time--"]) {
            strNonPreferableTime=@"";
        }
        if (strNonPreferableTime.length==0) {
            strNonPreferableTime=@"";
        }
        if([strNonPreferableTime isEqual: [NSNull null]])
        {
            strNonPreferableTime=@"";
        }
        
        if([strSelectedPreferableSysName isEqual:[NSNull null]] || strSelectedPreferableSysName.length==0)
        {
            strPreferableTime=@"";
        }
        else
        {
            strPreferableTime=strSelectedPreferableSysName;
            
        }
        
        if([strSeletcedNonPreferableSysName isEqual:[NSNull null]] || strSeletcedNonPreferableSysName.length==0)
        {
            strNonPreferableTime=@"";
        }
        else
        {
            
            strNonPreferableTime=strSeletcedNonPreferableSysName;
        }
        
        
        
        strPrimaryServiceSysName=[NSString stringWithFormat:@"%@",arrSysName[0]];
        
        //Nilind 30 Dec
        
        NSString *strSoldSerivce;
        strSoldSerivce=strPrimaryServiceSysName;
        
        
        NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
        for (int i=0; i<arrCtgry.count; i++)
        {
            NSDictionary *dict=[arrCtgry objectAtIndex:i];
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strServiceSysNameToCompare=[dict valueForKey:@"InitialServiceSysName"];
                //[arrInitial addObject:strServiceSysName];
                if ([strServiceSysNameToCompare isEqualToString:strSoldSerivce])
                {
                    
                    //[temArrServiceName addObject:[dict valueForKey:@"Name"]];
                    //[arrInitial addObject:[dict valueForKey:@"InitialServiceSysName"]];
                    strPrimaryServiceSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                    break;
                }
            }
        }
        
        
        
        //..............................
        
        NSMutableArray *arrOfInitialServiceSysNames=[[NSMutableArray alloc]init];
        
        //Nilind 30 Dec
        
        if (arrSysName.count==1)  //working 30 DEC
        {
            
            strInitialServiceSysNames=[arrSysName objectAtIndex:0];
            
        }
        else
        {
            
            for (int k=0; k<arrSysName.count; k++)
            {
                
                if (k==0)
                {
                    
                }
                else
                {
                    
                    [arrOfInitialServiceSysNames addObject:[NSString stringWithFormat:@"%@",arrSysName[k]]];
                    
                }
            }
            strInitialServiceSysNames=[arrOfInitialServiceSysNames componentsJoinedByString:@","];
            
        }
        
        //..................
        
        
        strRouteSysNAme=arrRouteSysNameSelected[0];
        
        if (strInitialServiceSysNames.length==0) {
            strInitialServiceSysNames=@"";
        }
        if (strRouteSysNAme.length==0) {
            strRouteSysNAme=@"";
        }
        
        if (strPrimaryServiceSysName.length==0) {
            strPrimaryServiceSysName=@"";
        }
        
        NSUserDefaults *defsAccountNo=[NSUserDefaults standardUserDefaults];
        strAccountNoss=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"AccountNos"]];
        strLeadNumber=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"LeadNumber"]];
        
        if (strAccountNoss.length==0 || [strAccountNoss isEqualToString:@"(null)"]) {
            strAccountNoss=@"";
        }
        if (strLeadNumber.length==0 || [strLeadNumber isEqualToString:@"(null)"]) {
            strLeadNumber=@"";
        }
        
        
        key=[NSArray arrayWithObjects:
             @"PrimaryServiceSysName",
             @"AccountNumber",
             @"companykey",
             @"SetupStartDate",
             @"LeadNo",
             @"Typeoftime",
             @"NonPreferablerangeSysName",
             @"PreferablerangeSysName",
             @"RouteSysName",
             @"InitialServicePrice",
             @"InitialServiceDuration",
             @"InitialServiceDate",
             @"InitialServiceSysNames",
             @"InitialServiceTime",
             @"LockInitialRoute",
             @"LockInitialtime",
             @"InitialServiceInstruction",
             @"SetupInstruction",
             @"RangeofTimeId",
             nil];
        
        
        value=[NSArray arrayWithObjects:
               strPrimaryServiceSysName,          //priya_service_2,strPrimaryServiceSysName
               strAccountNoss,
               strCompanyKey,
               _txtServiceDate.text,
               strLeadNumber,
               strTypeOfTime,
               strNonPreferableTime,//NIL
               strPreferableTime,//nil
               strRouteSysNAme,
               _txtServicePrice.text,
               _txtServiceDuration.text,
               _txtServiceDate.text,
               strInitialServiceSysNames,       //"priyaserivce1",strInitialServiceSysNames
               strSelectedTimeIn24Hrs,
               islockRoute,
               isLockTime,
               strInitialServiceInstruction,
               strSetupInstruction,
               strSelectedRangeTimeId,
               nil];
        
        
        
        /*  key=[NSArray arrayWithObjects:
         @"PrimaryServiceSysName",
         @"AccountNumber",
         @"companykey",
         @"SetupStartDate",
         @"LeadNo",
         @"Typeoftime",
         @"NonPreferablerangeSysName",
         @"PreferablerangeSysName",
         @"RouteSysName",
         @"InitialServicePrice",
         @"InitialServiceDuration",
         @"InitialServiceDate",
         @"InitialServiceSysNames",
         @"InitialServiceTime",
         @"LockInitialRoute",
         @"LockInitialtime",
         @"InitialServiceInstruction",
         @"SetupInstruction",
         nil];
         
         
         value=[NSArray arrayWithObjects:
         @"priyaserivce1",
         @"10015",
         @"Automation",
         @"12/22/2016",
         strLeadId,
         strTypeOfTime,
         strNonPreferableTime,
         strPreferableTime,
         @"Route12e",
         @"100.0",
         @"23:55:00",
         @"12/22/2016",
         strInitialServiceSysNames,
         strSelectedTimeIn24Hrs,
         @"true",
         @"true",
         strInitialServiceInstruction,
         strSetupInstruction,
         nil];
         */
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        // NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"FINAL FORM JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            
            
        }
        else
        {
            
            NSArray *arrResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (arrResponse.count==0 || arrResponse==nil)
            {
                
            } else
            {
                //Nilind 28 Dec
                NSLog(@"Save & Continue Response >>%@",arrResponse);
                [self initialSetupStatusUpdate];
                // [self goToAppointment];
                
                //........
            }
            //            [_activityIndicator stopAnimating];
            //            [_activityIndicator setHidden:YES];
        }
        //[DejalActivityView removeView];
    }
    
    
}
-(void)sendToServerSaveSetupAndGenerateWorkOrder
{
    //   [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
        
    }
    else
    {
        
        // NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
        
        NSString *strUrl,*strSetupInstruction,*strInitialServiceInstruction,*strPreferableTime,*strNonPreferableTime,*strPrimaryServiceSysName,*strRouteSysNAme,*strInitialServiceSysNames,*strAccountNoss,*strLeadNumber;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesSaveInitialSetupnGenerateWorkOrder];
        NSArray *key,*value;
        
        strSetupInstruction=_txtViewSetupInstruction.text;
        strInitialServiceInstruction=_txtViewServiceInstruction.text;
        strPreferableTime=_btnPreferableTime.titleLabel.text;
        strNonPreferableTime=_btnNewPreferableTime.titleLabel.text;
        
        if (strSetupInstruction.length==0) {
            strSetupInstruction=@"";
        }
        if (strInitialServiceInstruction.length==0) {
            strInitialServiceInstruction=@"";
        }
        if (strPreferableTime.length==0) {
            strPreferableTime=@"";
        }
        if([strPreferableTime isEqual: [NSNull null]])
        {
            strPreferableTime=@"";
        }
        if ([strPreferableTime isEqualToString:@"--Select Preferable Time--"])
        {
            strPreferableTime=@"";
        }
        
        if ([strNonPreferableTime isEqualToString:@"--Select Non-Preferable Time--"]) {
            strNonPreferableTime=@"";
        }
        if (strNonPreferableTime.length==0) {
            strNonPreferableTime=@"";
        }
        if([strNonPreferableTime isEqual: [NSNull null]])
        {
            strNonPreferableTime=@"";
        }
        
        if([strSelectedPreferableSysName isEqual:[NSNull null]] || strSelectedPreferableSysName.length==0)
        {
            strPreferableTime=@"";
        }
        else
        {
            strPreferableTime=strSelectedPreferableSysName;
            
        }
        
        if([strSeletcedNonPreferableSysName isEqual:[NSNull null]] || strSeletcedNonPreferableSysName.length==0)
        {
            strNonPreferableTime=@"";
        }
        else
        {
            
            strNonPreferableTime=strSeletcedNonPreferableSysName;
        }
        
        
        
        strPrimaryServiceSysName=[NSString stringWithFormat:@"%@",arrSysName[0]];
        //Nilind 30 Dec
        
        NSString *strSoldSerivce;
        strSoldSerivce=strPrimaryServiceSysName;
        
        
        NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
        for (int i=0; i<arrCtgry.count; i++)
        {
            NSDictionary *dict=[arrCtgry objectAtIndex:i];
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strServiceSysNameToCompare=[dict valueForKey:@"InitialServiceSysName"];
                //[arrInitial addObject:strServiceSysName];
                if ([strServiceSysNameToCompare isEqualToString:strSoldSerivce])
                {
                    
                    //[temArrServiceName addObject:[dict valueForKey:@"Name"]];
                    //[arrInitial addObject:[dict valueForKey:@"InitialServiceSysName"]];
                    strPrimaryServiceSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                    
                    break;
                }
            }
        }
        
        
        
        //..............................
        
        NSMutableArray *arrOfInitialServiceSysNames=[[NSMutableArray alloc]init];
        
        //Nilind 30 Dec
        
        if (arrSysName.count==1)  //working 30 DEC
        {
            
            strInitialServiceSysNames=[arrSysName objectAtIndex:0];
            
        }
        else
        {
            
            for (int k=0; k<arrSysName.count; k++)
            {
                
                if (k==0)
                {
                    
                }
                else
                {
                    
                    [arrOfInitialServiceSysNames addObject:[NSString stringWithFormat:@"%@",arrSysName[k]]];
                    
                }
            }
            strInitialServiceSysNames=[arrOfInitialServiceSysNames componentsJoinedByString:@","];
            
        }
        //..................
        
        strRouteSysNAme=arrRouteSysNameSelected[0];
        
        if (strInitialServiceSysNames.length==0) {
            strInitialServiceSysNames=@"";
        }
        if (strRouteSysNAme.length==0) {
            strRouteSysNAme=@"";
        }
        
        if (strPrimaryServiceSysName.length==0) {
            strPrimaryServiceSysName=@"";
        }
        
        NSUserDefaults *defsAccountNo=[NSUserDefaults standardUserDefaults];
        strAccountNoss=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"AccountNos"]];
        strLeadNumber=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"LeadNumber"]];
        
        if (strAccountNoss.length==0 || [strAccountNoss isEqualToString:@"(null)"]) {
            strAccountNoss=@"";
        }
        if (strLeadNumber.length==0 || [strLeadNumber isEqualToString:@"(null)"]) {
            strLeadNumber=@"";
        }
        //        strAccountNoss=@"10014";
        //        strLeadNumber=@"1675";
        // strInitialServiceSysNames=@"SignaturePestControl";
        
        
        
        key=[NSArray arrayWithObjects:
             @"PrimaryServiceSysName",
             @"AccountNumber",
             @"companykey",
             @"SetupStartDate",
             @"LeadNo",
             @"Typeoftime",
             @"NonPreferablerangeSysName",
             @"PreferablerangeSysName",
             @"RouteSysName",
             @"InitialServicePrice",
             @"InitialServiceDuration",
             @"InitialServiceDate",
             @"InitialServiceSysNames",
             @"InitialServiceTime",
             @"LockInitialRoute",
             @"LockInitialtime",
             @"InitialServiceInstruction",
             @"SetupInstruction",
             @"RangeofTimeId",
             nil];
        
        
        value=[NSArray arrayWithObjects:
               strPrimaryServiceSysName,
               strAccountNoss,
               strCompanyKey,
               _txtServiceDate.text,
               strLeadNumber,
               strTypeOfTime,
               strNonPreferableTime,
               strPreferableTime,
               strRouteSysNAme,
               _txtServicePrice.text,
               _txtServiceDuration.text,
               _txtServiceDate.text,
               strInitialServiceSysNames,
               strSelectedTimeIn24Hrs,
               islockRoute,
               isLockTime,
               strInitialServiceInstruction,
               strSetupInstruction,
               strSelectedRangeTimeId,
               nil];
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        // NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"FINAL FORM JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
        }
        else
        {
            
            NSArray *arrResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (arrResponse.count==0 || arrResponse==nil)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Unable to generate workorder" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
                [DejalBezelActivityView removeView];
                [DejalActivityView removeView];
            }
            else
            {
                //Nilind 28 Dec
                NSLog(@"Save & Generate Work order Response >>%@",arrResponse);
                [self initialSetupStatusUpdate];
                //[self goToAppointment];
                
                //........
            }
        }
        
    }
}

- (IBAction)action_SaveGenrateWorkOrder:(id)sender
{
     [self endEditing];
    _btnSaveGenerateWorkOrder.enabled = NO;
    
    [UIView animateWithDuration:3 animations:^{
        
    } completion:^(BOOL finished) {
        _btnSaveGenerateWorkOrder.enabled = YES;
        
    }
     ];
    
    NSString *errorMessage=[self validationCheck];
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Generating Work Order..."];
        
        //        [_activityIndicator startAnimating];
        //        [_activityIndicator setHidden:NO];
        
        [self performSelector:@selector(sendToServerSaveSetupAndGenerateWorkOrder) withObject:nil afterDelay:0.5];
        
        //  [self sendToServerSaveSetupAndGenerateWorkOrder];
        //[self goToAppointment];
        
    }
}

//============================================================================
//============================================================================
#pragma mark ---------------- Validation Check Method--------------------------
//============================================================================
//============================================================================

-(NSString *)validationCheck
{
    NSString *errorMessage;
    
    [_btnPreferableTime setTitle:@"--Select Preferable Time--" forState:UIControlStateNormal];
    [_btnNewPreferableTime setTitle:@"--Select Non-Preferable Time--" forState:UIControlStateNormal];
    
    if ([strTypeOfTime isEqualToString:@"2"]) {
        
        if ([_btnNewPreferableTime.titleLabel.text isEqualToString:_btnPreferableTime.titleLabel.text]) {
            
            errorMessage=@"Please select Different Preferable and Non-Preferable Time";
            
        }
        
        if ([_btnNewPreferableTime.titleLabel.text isEqualToString:@"--Select Non-Preferable Time--"]) {
            
            errorMessage=@"Please select Non-Preferable Time";
            
        }
        
        if ([_btnPreferableTime.titleLabel.text isEqualToString:@"--Select Preferable Time--"]) {
            
            errorMessage=@"Please select Preferable Time";
            
        }
        
    }
    
    if ([_btnInitialService.titleLabel.text isEqualToString:@"--Select Initial Service--"]) {
        
        errorMessage=@"Please select Initial Service";
        
    }
    else if (_txtServicePrice.text.length==0) {
        
        errorMessage=@"Please enter service price";
        
    }
    else if (_txtServiceProdValue.text.length==0) {
        
        errorMessage=@"Please enter service production value";
        
    }
    else if (_txtServiceDuration.text.length==0) {
        
        errorMessage=@"Please select Initial Service";
        
    }
    else if (_txtServiceDate.text.length==0) {
        
        errorMessage=@"Please select Service Date";
        
    }
    else if (_txtServiceTime.text.length==0) {
        
        errorMessage=@"Please select Service Time";
        
    }
    else if (arrSourceSelectedValue.count==0) {
        
        errorMessage=@"Please select Route";
        
    }
    else if (arrSourceSelectedValue.count>1) {
        
        errorMessage=@"Please select only one Route";
        
    }
    return errorMessage;
}

-(void)goToAppointmentOld{
    
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppointmentView *objAppointmentView=[storyBoard instantiateViewControllerWithIdentifier:@"AppointmentView"];
    [self.navigationController pushViewController:objAppointmentView animated:NO];
    
}
-(void)goToAppointment
{
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Appointment" bundle:nil];
        AppointmentVC *objAppointmentView=[storyBoard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objAppointmentView animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];AppointmentView*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
#pragma mark- -----------------API FOR INITIAL SETUP STATUS--------------------------
//Nilind 30 Dec

-(void)initialSetupStatusUpdate
{
    //http://tsas.stagingsoftware.com//api/MobileToSaleAuto/UpdateLeadInitialSetup?leadId=675&IsInitialSetup=true
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        //[self salesFetch];
    }
    else
    {
        
        
        NSString *strUrl,*strInitialSetupStatus;
        strInitialSetupStatus=@"true";
        strUrl = [NSString stringWithFormat:@"%@%@%@&IsInitialSetup=%@",strSalesAutoMainUrl,UrlSalesInitialSetupStatusUpdate,strLeadId,strInitialSetupStatus];
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        NSLog(@"Sales Lead URl-----%@",strUrl);
        
        
        
        // NSURL *url = [NSURL URLWithString:strUrl];
        
        // NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        //        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        //        [request setHTTPBody: requestData];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSString *responseString    = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 NSLog(@"responseString %@",responseString);
                 [DejalBezelActivityView removeView];
                 [DejalActivityView removeView];
                 [self goToAppointment];
                 
             }
         }];
    }
}
-(void)leadTitleFetch
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAll = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesAll;
    if (arrAll.count==0)
    {
        
    }
    else
    {
        
        matchesAll=arrAll[0];
        _lblTitle.text=[NSString stringWithFormat:@"Opportunity #: %@, A/C #: %@",[matchesAll valueForKey:@"leadNumber"],[matchesAll valueForKey:@"accountNo"]];
        
        _lblNameTitle.text=[NSString stringWithFormat:@"%@ %@ %@",[matchesAll valueForKey:@"firstName"],[matchesAll valueForKey:@"middleName"],[matchesAll valueForKey:@"lastName"]];
        _lblTitle.font=[UIFont boldSystemFontOfSize:14];
        _lblNameTitle.font=[UIFont boldSystemFontOfSize:14];
        
    }
    
    
}
-(NSString *)modifyDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString* finalTime = [dateFormatter stringFromDate:[NSDate date]];
    return finalTime;
}
//.............................
#pragma mark- Textview  Delegate

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        [_txtViewSetupInstruction resignFirstResponder];
        [_txtViewServiceInstruction resignFirstResponder];
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==_txtServicePrice)
    {
 
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@".0123456789" :_txtServicePrice.text];
            
            if (isNuberOnly)
            {
                return YES;
            }
            else
            {
                return NO;
            }
       
    }
    else if(textField==_txtServiceProdValue)
    {
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@".0123456789" :_txtServiceProdValue.text];
        
        if (isNuberOnly)
        {
            return YES;
        }
        else
        {
            return NO;
        }
        
    }
    else
    {
        return YES;
    }
}
-(void)endEditing
{
    [self.view endEditing:YES];
}
@end
