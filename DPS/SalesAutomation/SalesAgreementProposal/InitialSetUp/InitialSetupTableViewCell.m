//
//  InitialSetupTableViewCell.m
//  DPS
//
//  Created by Rakesh Jain on 10/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "InitialSetupTableViewCell.h"

@implementation InitialSetupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
