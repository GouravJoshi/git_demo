//
//  AgreementCheckTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 01/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
////////

#import <UIKit/UIKit.h>

@interface AgreementCheckTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxAgreement;
@property (weak, nonatomic) IBOutlet UILabel *lblAgreementService;

@end
