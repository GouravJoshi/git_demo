//
//  GenerateWorkOrder.m
//  DPS
//
//  Created by Rakesh Jain on 04/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "GenerateWorkOrder.h"
#import "InitialSetUp.h"
#import "GenrateWorkorderTableViewCell.h"
#import "AppointmentView.h"
#import "Global.h"
#import "DejalActivityView.h"
#import "SendMailViewController.h"
#import "DynamicViewTableViewCell.h"
#import "DPS-Swift.h"

@interface GenerateWorkOrder ()
{
    NSString *strLeadId;
    UIView *viewBackGround;
    UITableView *tblData;
    NSMutableArray *arrServiceName,*arrPreferTime,*arrNonPreferTime,*arrServiceRoute,*arrServiceTime,*arrOfSelectedServiceName,*arrOfServiceDepartmentName,*arrOfDictAllDataOfService,*arrOfPreferableSysName,*arrOfNonPreferableSysName;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    NSString *strDate,*isPrimarySelectedService,*strSelectedTimeIn24Hrs;
    BOOL chkForDate;
    NSMutableArray *arrSourceSelectedValue;
    BOOL  isSelectEnable,chkServiceTime;
    NSDictionary *dictServiceName,*dictServiceSYSNAME;
    double totalServicePrice;
    
    //Nilind 02 Dec
    
    NSMutableArray *arrServiceSysName,*arrSysName;
    NSDictionary *dictDusration;
    NSString *strSalesAutoMainUrl,*strCompanyKey;
    
    
    NSString *strAccountNo,*strAddress1,*strZipcode,*strCountrySysName,*strStateSysName,*strCityName;
    Global *global;
    NSMutableArray *arrRout,*arrRouteSysName;
    NSDictionary *dictRout;
    NSArray *arrOtherRout,*arrSuggestedRout;
    BOOL entry;
    NSTimer *_timer;
    NSMutableData *responseData;
    NSMutableArray *arrRoutOther,*arrRoutSuggested,*arrOfViewAppointments,*arrRouteNumber,*arrRouteNumberSelected,*arrRouteSysNameSelected,*arrRoutSelectedId,*arrRoutId;
    NSArray *arrOfResponseRouteAppointments;
    NSString *strSelectedRoutes;
    int indexxRouteSelected;
    NSString *strTypeOfTime,*isLockTime,*islockRoute,*strSelectedPreferableSysName,*strSeletcedNonPreferableSysName;
    //................
    NSDictionary *dictMasters;
    
    //Nilind 28 Dec
    
    NSMutableArray *temArrServiceName;
    NSMutableArray *temArrServiceSysName;
    NSString *strTitile;
    UILabel *lblOk;
    //...........
    
    //Nilind 04thMay
    
    NSMutableArray *arrOnTimeSoldStandard,*arrOneTimeSoldNonStandard,*arrSelectedOneTime;
    BOOL chkOneTime,chkSelect;
    NSString *strClickedService,*strDidSelectService;;
    
    //End
    
    //nilind 10 May
    NSMutableArray *arrPriceProduction,*arrDuration;
    NSMutableArray *globalArrayDict;
    NSMutableDictionary *dictMutable;
    NSInteger sectionNo;
    NSIndexPath *getIndex;
    NSMutableDictionary *tempDict;
    BOOL chkForCellDuration;
    NSString *strForDuration;
    NSInteger tagForDuration;
    NSMutableArray * captureAllSecondsArray;
    NSString *strDuration;
    NSString *strTax,*strCreatedBy,*strEmployeeNo;
    NSMutableArray *arrTargets;
    NSMutableArray *arrAttribute,*arrSelectedTargetId,*arrSelectedAttributeId;
    NSMutableArray *arrSelectedTarget,*arrSelectedAttribute,*arrSelectedTargetName,*arrSelectedAttributeName;
    
    
}
@end

@implementation GenerateWorkOrder

- (void)viewDidLoad
{
    [super viewDidLoad];
    _txtTotal.enabled=NO;
    _txtTotalDuration.enabled=NO;
    _txtGrandTotal.enabled=NO;
    
    //Nilind 04th May
    chkForCellDuration=NO;
    sectionNo=0;tagForDuration=0;
    strForDuration=@"";
    strDuration=@"00:00";
    tempDict=[[NSMutableDictionary alloc]init];
    globalArrayDict=[[NSMutableArray alloc]init];
    arrSelectedOneTime=[[NSMutableArray alloc]init];
    chkOneTime=YES;chkSelect=YES;
    globalArrayDict=[[NSMutableArray alloc]init];
    dictMutable=[[NSMutableDictionary alloc]init];
    arrSelectedTargetId=[[NSMutableArray alloc]init];
    arrSelectedAttributeId=[[NSMutableArray alloc]init];
    arrSelectedTarget=[[NSMutableArray alloc]init];
    arrSelectedAttribute=[[NSMutableArray alloc]init];
    //End
    //Nilind 18 Jan
    
    _const_PreferableView_H.constant=0;
    _const_ViewMain_H.constant=1243-143;
    _btnPreferableTime.hidden=YES;
    _btnNewPreferableTime.hidden=YES;
    _lblPreferable.hidden=YES;
    _lblNonPreferable.hidden=YES;
    
    _txtServicePrice.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePrice.layer.borderWidth=1.0;
    _txtServicePrice.layer.cornerRadius=5.0;
    
    _txtServiceProdValue.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceProdValue.layer.borderWidth=1.0;
    _txtServiceProdValue.layer.cornerRadius=5.0;
    
    _txtServiceDuration.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceDuration.layer.borderWidth=1.0;
    _txtServiceDuration.layer.cornerRadius=5.0;
    
    _txtServiceDate.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceDate.layer.borderWidth=1.0;
    _txtServiceDate.layer.cornerRadius=5.0;
    
    _txtServiceTime.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceTime.layer.borderWidth=1.0;
    _txtServiceTime.layer.cornerRadius=5.0;
    
    _txtServiceToTime.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceToTime.layer.borderWidth=1.0;
    _txtServiceToTime.layer.cornerRadius=5.0;
    
    
    
    //.text=[NSString stringWithFormat:@"Lead#:%@, A/C#:%@",[matchesGeneralInfo valueForKey:@"leadNumber"],[matchesGeneralInfo valueForKey:@"accountNo"]];
    _txtServiceDuration.textAlignment=NSTextAlignmentLeft;
    
#pragma mark - TEMP COMMENT 04 MAY
    SendMailViewController *objSendMailViewController=[[SendMailViewController alloc]init];
    [objSendMailViewController syncCall];
    NSUserDefaults *defsFromInitialSetup=[NSUserDefaults standardUserDefaults];
    [defsFromInitialSetup setBool:YES forKey:@"FromInitialSetup"];
    [defsFromInitialSetup synchronize];
    //............
    
    
    NSUserDefaults *defsdictMasters=[NSUserDefaults standardUserDefaults];
    dictMasters=[defsdictMasters valueForKey:@"MasterSalesAutomation"];
    
    strSelectedRoutes=@"";
    isLockTime=@"false";
    islockRoute=@"false";
    strSelectedTimeIn24Hrs=@"";
    indexxRouteSelected=0;
    strTypeOfTime=@"1";
    
    responseData=[[NSMutableData alloc]init];
    global = [[Global alloc] init];
    entry=YES;
    
    [_btnServiceRoute setTitle:@"--Select Route--" forState:UIControlStateNormal];
    [_btnInitialService setTitle:@"--Select Initial Service--" forState:UIControlStateNormal];
    [_btnPreferableTime setTitle:@"--Select Preferable Time--" forState:UIControlStateNormal];
    [_btnNewPreferableTime setTitle:@"--Select Non-Preferable Time--" forState:UIControlStateNormal];
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    arrServiceSysName=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    arrSourceSelectedValue=[[NSMutableArray alloc]init];
    isSelectEnable=NO;
    totalServicePrice=0;
    isPrimarySelectedService=@"";
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    
    
    arrServiceName=[[NSMutableArray alloc]init];
    arrOfServiceDepartmentName=[[NSMutableArray alloc]init];
    arrOfSelectedServiceName=[[NSMutableArray alloc]init];
    arrOfDictAllDataOfService=[[NSMutableArray alloc]init];
    arrServiceRoute=[[NSMutableArray alloc]init];
    arrRoutOther=[[NSMutableArray alloc]init];
    arrRoutSuggested=[[NSMutableArray alloc]init];
    
    arrPreferTime=[[NSMutableArray alloc]init];
    arrOfNonPreferableSysName=[[NSMutableArray alloc]init];
    arrOfPreferableSysName=[[NSMutableArray alloc]init];
    
    
    
    arrRout=[[NSMutableArray alloc]init];
    
    arrRouteNumber=[[NSMutableArray alloc]init];
    arrRouteNumberSelected=[[NSMutableArray alloc]init];
    arrRouteSysNameSelected=[[NSMutableArray alloc]init];
    arrRoutSelectedId=[[NSMutableArray alloc]init];
    arrRoutId=[[NSMutableArray alloc]init];
    
    _txtViewServiceInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewServiceInstruction.layer.borderWidth=1.0;
    
    _txtViewSetupInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewSetupInstruction.layer.borderWidth=1.0;
    
    _txtViewDirections.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewDirections.layer.borderWidth=1.0;
    
    _txtViewSpecialInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewSpecialInstruction.layer.borderWidth=1.0;
    
    _txtViewAccountInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAccountInstruction.layer.borderWidth=1.0;
    
    _txtViewOtherInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewOtherInstruction.layer.borderWidth=1.0;
    
    
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    
    //...............
    
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.dataSource=self;
    tblData.delegate=self;
    
#pragma mark- Depratment Master
    
    [_imgChkBoxSpecificTime setImage:[UIImage imageNamed:@"check_box_2.png"]];
    [_imgChkBoxRangeTime setImage:[UIImage imageNamed:@"check_box_1.png"]];
    //_viewTwoPreferableTime.hidden=YES;
    _viewTwoPreferableTime.hidden=NO;
    //  [_viewTwoPreferableTime setUserInteractionEnabled:NO];
    
    [self serviceName];
    [self serviceSYSNAME];
    [self leadTitleFetch];
    [self fetchOneTime];
    [self fetchFromCoreDataStandard];
    
    [self performSelector:@selector(methodCalling) withObject:nil afterDelay:1.0];
    
    //Nilind 06 Jan
    
    //    UITapGestureRecognizer *viewTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    //    viewTapGestureRecognizer.numberOfTapsRequired = 1;
    //    viewTapGestureRecognizer.enabled = YES;
    //    viewTapGestureRecognizer.cancelsTouchesInView = NO;
    //    [_viewTwoPreferableTime addGestureRecognizer:viewTapGestureRecognizer];
    //..........
    
    [self addViews];
    
    
}
- (void)viewTap:(UITapGestureRecognizer *)gesture
{
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    //Nilind 31 Dec
    
    /* if(strLeadId.length==0||[strLeadId isEqual:[NSNull null]])
     {
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     strLeadId=[defs valueForKey:@"ForInitialSetUpLeadId"];
     }*/
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    //.............
    
}
-(void)methodCalling
{
    
    [_activityIndicator startAnimating];
    [_activityIndicator setHidden:NO];
    
    [self salesFetch];
    [self rangeTimeApi];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionOnBack:(id)sender
{
    [self endEditing];
    [self goToAppointment];
    //[self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:NO];
    
    /* for (UIViewController *controller in self.navigationController.viewControllers)
     {
     if ([controller isKindOfClass:[AppointmentView class]])
     {
     [self.navigationController popToViewController:controller animated:NO];
     break;
     }
     }*/
}

- (IBAction)actionOnInitialSetUp:(id)sender
{
    [self endEditing];
    UIButton *btn=(UIButton *)sender;
    if (btn.selected==NO)
    {
        [_imgChkBoxInitialSetup setImage:[UIImage imageNamed:@"check_box_2.png"]];
        btn.selected=YES;
        _viewInitialService.hidden=YES;
        
    }
    else
    {
        [_imgChkBoxInitialSetup setImage:[UIImage imageNamed:@"check_box_1.png"]];
        btn.selected=NO;
        _viewInitialService.hidden=NO;
    }
}

- (IBAction)actionOnSpecificTime:(id)sender
{
    [self endEditing];
    strTypeOfTime=@"1";
    [_imgChkBoxSpecificTime setImage:[UIImage imageNamed:@"check_box_2.png"]];
    [_imgChkBoxRangeTime setImage:[UIImage imageNamed:@"check_box_1.png"]];
    // _viewTwoPreferableTime.hidden=YES;
    
    //[_viewTwoPreferableTime setUserInteractionEnabled:NO];
    
    _const_PreferableView_H.constant=0;
    _const_ViewMain_H.constant=1243-143;
    _btnPreferableTime.hidden=YES;
    _btnNewPreferableTime.hidden=YES;
    _lblPreferable.hidden=YES;
    _lblNonPreferable.hidden=YES;
    
    
}

- (IBAction)actionOnRangeTime:(id)sender
{
    [self endEditing];
    strTypeOfTime=@"2";
    [_imgChkBoxRangeTime setImage:[UIImage imageNamed:@"check_box_2.png"]];
    [_imgChkBoxSpecificTime setImage:[UIImage imageNamed:@"check_box_1.png"]];
    _viewTwoPreferableTime.hidden=NO;
    // [_viewTwoPreferableTime setUserInteractionEnabled:YES];
    _const_PreferableView_H.constant=143;
    _const_ViewMain_H.constant=1243;
    _btnPreferableTime.hidden=NO;
    _btnNewPreferableTime.hidden=NO;
    _lblPreferable.hidden=NO;
    _lblNonPreferable.hidden=NO;
    
}
- (IBAction)actionOnInitialService:(id)sender
{
    [self endEditing];
    lblOk.hidden=NO;
    /*
     temArrServiceName=[[NSMutableArray alloc]init];
     
     temArrServiceSysName=[[NSMutableArray alloc]init];
     NSMutableArray *arrInitial;
     arrInitial=[[NSMutableArray alloc]init];
     if (arrServiceSysName.count==0)
     {
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No service available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
     [alert show];
     
     }
     else
     {
     for (int k=0; k<arrServiceSysName.count; k++)
     {
     
     NSString *strServiceSysName=arrServiceSysName[k];
     
     NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
     for (int i=0; i<arrCtgry.count; i++)
     {
     NSDictionary *dict=[arrCtgry objectAtIndex:i];
     NSArray *arrServices=[dict valueForKey:@"Services"];
     for (int j=0; j<arrServices.count; j++)
     {
     NSDictionary *dict=[arrServices objectAtIndex:j];
     NSString *strServiceSysNameToCompare=[dict valueForKey:@"SysName"];
     //[arrInitial addObject:strServiceSysName];
     if ([strServiceSysNameToCompare isEqualToString:strServiceSysName])
     {
     
     //[temArrServiceName addObject:[dict valueForKey:@"Name"]];
     [arrInitial addObject:[dict valueForKey:@"InitialServiceSysName"]];
     }
     }
     }
     }
     NSLog(@"ArrInitial>>%@",arrInitial);
     
     
     for(int l=0;l<arrInitial.count;l++)
     {
     if([[arrInitial objectAtIndex:l]isEqualToString:@""])
     {
     [arrInitial removeObjectAtIndex:l];
     }
     }
     for (int k=0; k<arrInitial.count; k++)
     {
     
     NSString *strServiceSysName=arrInitial[k];
     
     NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
     for (int i=0; i<arrCtgry.count; i++)
     {
     NSDictionary *dict=[arrCtgry objectAtIndex:i];
     NSArray *arrServices=[dict valueForKey:@"Services"];
     for (int j=0; j<arrServices.count; j++)
     {
     NSDictionary *dict=[arrServices objectAtIndex:j];
     NSString *strServiceSysNameToCompare=[dict valueForKey:@"SysName"];
     //[arrInitial addObject:strServiceSysName];
     if ([strServiceSysNameToCompare isEqualToString:strServiceSysName])
     {
     
     [temArrServiceName addObject:[dict valueForKey:@"Name"]];
     [temArrServiceSysName addObject:[dict valueForKey:@"SysName"]];
     }
     }
     }
     }
     NSLog(@"ArrtemArrServiceName>>%@",temArrServiceName);
     
     tblData.tag=10;
     [self tableLoad:tblData.tag];
     }
     
     */
    //..........................
    
    //Nilind 04 May
    tblData.tag=10;
    [self tableLoad:tblData.tag];
    
    //End
    
}

- (IBAction)actionOnServiceDate:(id)sender
{
    [self endEditing];
    strTitile=@"SELECT DATE";
    chkForDate=YES;
    chkServiceTime=YES;
    [self addPickerViewDateTo];
}

- (IBAction)actionOnServiceRoute:(id)sender
{
    [self endEditing];
    lblOk.hidden=NO;
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    //_timer = [NSTimer scheduledTimerWithTimeInterval:10.0f
    //                                              target:self
    //                                            selector:@selector(_timerFired:)
    //                                            userInfo:nil
    //                                             repeats:YES];
    // [self routApi];
    
    tblData.tag=20;
    [self tableLoad:tblData.tag];
}
- (void)_timerFired:(NSTimer *)timer {
    NSLog(@"ping");
    // [DejalBezelActivityView removeView];
}
- (IBAction)actionOnPreferableTime:(id)sender
{
    [self endEditing];
    
    if( [_imgChkBoxRangeTime.image isEqual:[UIImage imageNamed: @"check_box_2.png"]])
    {
        
        tblData.tag=30;
        [self tableLoad:tblData.tag];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select range time above" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
    lblOk.hidden=YES;
}
- (IBAction)actionOnNewPreferableTime:(id)sender
{
    [self endEditing];
    if( [_imgChkBoxRangeTime.image isEqual:[UIImage imageNamed: @"check_box_2.png"]])
    {
        tblData.tag=40;
        [self tableLoad:tblData.tag];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select range time above" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    lblOk.hidden=YES;
}
- (IBAction)actionOnLockRout:(id)sender
{
    [self endEditing];
    UIButton *btn=(UIButton *)sender;
    if (btn.selected==NO)
    {
        [_imgChkBoxLockRout setImage:[UIImage imageNamed:@"check_box_2.png"]];
        btn.selected=YES;
        islockRoute=@"true";
    }
    else
    {
        [_imgChkBoxLockRout setImage:[UIImage imageNamed:@"check_box_1.png"]];
        btn.selected=NO;
        islockRoute=@"false";
    }
}
- (IBAction)actionOnLockTime:(id)sender
{
    [self endEditing];
    UIButton *btn=(UIButton *)sender;
    if (btn.selected==NO)
    {
        [_imgChkBoxLockTime setImage:[UIImage imageNamed:@"check_box_2.png"]];
        btn.selected=YES;
        isLockTime=@"true";
    }
    else
    {
        [_imgChkBoxLockTime setImage:[UIImage imageNamed:@"check_box_1.png"]];
        btn.selected=NO;
        isLockTime=@"false";
    }
    
}
#pragma mark- ACTION VIEW APPOINTMENT
- (IBAction)actionOnViewAppointment:(id)sender
{
    [self endEditing];
    
    if (arrSourceSelectedValue.count==0) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"ALert" message:@"Please select Route" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else if (_txtServiceDate.text.length==0){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"ALert" message:@"Please select service date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
        
        [self performSelector:@selector(apiViewAppointment) withObject:nil afterDelay:1.0];
    }
}

- (IBAction)actionOnSaveContinue:(id)sender
{
    [self endEditing];
    _btnSaveContinue.enabled = NO;
    
    [UIView animateWithDuration:3 animations:^{
        
    } completion:^(BOOL finished) {
        _btnSaveContinue.enabled = YES;
        
    }
     ];
    
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Generating Workorder..."];
    
    [self performSelector:@selector(generateWorkorderAPI) withObject:nil afterDelay:0.0];
    
    /* NSString *errorMessage=[self validationCheck];
     errorMessage=@"";
     if (errorMessage)
     {
     NSString *strTitle = Alert;
     [global AlertMethod:strTitle :errorMessage];
     [DejalBezelActivityView removeView];
     
     
     }
     else
     {
     
     [DejalBezelActivityView activityViewForView:self.view withLabel:@"Saving Initial Setup..."];
     
     [self performSelector:@selector(generateWorkorderAPI) withObject:nil afterDelay:0.0];
     
     }*/
}
//============================================================================
#pragma mark- TABLEVIEW DELEGATE METHOD
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==10)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==10)
    {
        if (section==0)
        {
            return @"Standard Service";
        }
        else if (section==1)
        {
            return @"Non Standard Service";
        }
    }
    return @"";
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_tblAppointment)
    {
        return arrOfViewAppointments.count;
    }
    else if(tableView==_tblViewDynamic)
    {
        return arrSelectedOneTime.count;
    }
    else if (tblData.tag==10)
    {
        //return arrServiceName.count;
        if (section==0)
        {
            return arrOnTimeSoldStandard.count;
        }
        if (section==1)
        {
            return arrOneTimeSoldNonStandard.count;
        }
        return 2;
        
    }
    else if (tblData.tag==20)
    {
        //return arrServiceRoute.count;
        return  arrRout.count;
    }
    else if (tblData.tag==30)
    {
        return arrPreferTime.count;
    }
    else if (tblData.tag==40)
    {
        return arrNonPreferTime.count;
    }
    else if (tblData.tag==50)
    {
        return arrPreferTime.count;
    }
    else if (tblData.tag==60)
    {
        return arrTargets.count;
    }
    else if (tblData.tag==70)
    {
        return arrAttribute.count;
    }
    else
        return 0;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag==1)
    {
        static NSString *identifier=@"cellAppointment";
        GenrateWorkorderTableViewCell *cell=[_tblAppointment dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[GenrateWorkorderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrOfViewAppointments.count==0))
        {
            
            NSLog(@"IndexPath====%ld",(long)indexPath.row);
            
            NSDictionary *dictData=arrOfViewAppointments[indexPath.row];
            
            cell.lblAccNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"AccountNumber"]];
            cell.lblName.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
            //cell.lblAddress.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServicesAddress1"]];
            cell.lblAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@",
                                  
                                  [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServicesAddress1"]],[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceCity"]],[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceState"]],[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceZipcode"]]];
            cell.lblOrderNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WorkOrderNo"]];
            
            NSString *dateString = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"CreatedDate"]];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.sss"];
            NSDate *date = [dateFormatter dateFromString:dateString];
            // Convert date object into desired format
            [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm"];
            NSString *newDateString = [dateFormatter stringFromDate:date];
            cell.lblOrderDate.text=newDateString;//[global modifyDate];//[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CreatedDate"]];
            
            
        }
        return cell;
        
    }
    else if(tableView==_tblViewDynamic)
    {
        static NSString *identifier=@"DynamicViewTableViewCell";
        DynamicViewTableViewCell *cell=[_tblViewDynamic dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[DynamicViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (arrSelectedOneTime.count==0)
        {
            
        }
        else
        {
            NSDictionary *dict;
            dict=[globalArrayDict objectAtIndex:indexPath.row];
            
            cell.lblServiceName.text=[dict valueForKey:@"ServiceName"];
            cell.txtQuantity.text=[dict valueForKey:@"quantity"];
            cell.txtServicePrice.text=[dict valueForKey:@"price"];
            cell.txtProductionValue.text=[dict valueForKey:@"productionValue"];            cell.txtDuration.text=[dict valueForKey:@"duration"];
            if ([[dict valueForKey:@"primary"] isEqualToString:@"false"])
            {
                cell.imgForPrimary.image=[UIImage imageNamed:@"check_box_1.png"];
            }
            else
            {
                cell.imgForPrimary.image=[UIImage imageNamed:@"check_box_2.png"];
            }
            if ([[dict valueForKey:@"tax"] isEqualToString:@"false"])
            {
                cell.imgForTax.image=[UIImage imageNamed:@"check_box_1.png"];
            }
            else
            {
                cell.imgForTax.image=[UIImage imageNamed:@"check_box_2.png"];
            }
            
            
        }
        
        cell.btnPrimary.tag=indexPath.row;
        [cell.btnPrimary addTarget:self
                            action:@selector(buttonClickedPrimaryService:) forControlEvents:UIControlEventTouchDown];
        cell.btnTax.tag=indexPath.row;
        [cell.btnTax addTarget:self
                        action:@selector(buttonClickedTax:) forControlEvents:UIControlEventTouchDown];
        
        cell.txtServicePrice.tag=indexPath.row;
        cell.btnDuration.tag=indexPath.row;
        [cell.btnDuration addTarget:self
                             action:@selector(buttonClickedDuration:) forControlEvents:UIControlEventTouchDown];
        
        
        return cell;
        
    }
    else
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        
        
        
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (tblData.tag==10)
        {
            //Nilind 04 May
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            if (indexPath.section==0)
            {
                cell.textLabel.text=[arrOnTimeSoldStandard objectAtIndex:indexPath.row];
            }
            if (indexPath.section==1)
            {
                cell.textLabel.text=[arrOneTimeSoldNonStandard objectAtIndex:indexPath.row];
                
            }
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            //End
            
            /* cell.textLabel.text=[temArrServiceName objectAtIndex:indexPath.row];//arrServiceName
             cell.detailTextLabel.text=[arrOfServiceDepartmentName objectAtIndex:indexPath.row];*/
            
            /*NSString *str=[temArrServiceName objectAtIndex:indexPath.row];//arrServiceName
             
             long index=100000;
             
             for (int k=0; k<arrOfSelectedServiceName.count; k++)
             {
             if ([arrOfSelectedServiceName containsObject:str])
             {
             index=indexPath.row;
             break;
             }
             }
             if (indexPath.row==index)
             {
             [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
             }
             else
             {
             [cell setAccessoryType:UITableViewCellAccessoryNone];
             }*/
            
        }
        else if (tblData.tag==20)
        {
            cell.textLabel.text=[arrRout objectAtIndex:indexPath.row];//[arrServiceRoute objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            //
            //        NSString *str=[arrRoutSuggested objectAtIndex:indexPath.row];
            long index12=100000;
            //
            //        for (int k=0; k<arrOfSelectedServiceName.count; k++) {
            //            if ([arrOfSelectedServiceName containsObject:str]) {
            //                index12=indexPath.row;
            //                break;
            //            }
            //        }
            
            
            for (int k=0; k<arrRoutSuggested.count; k++)
            {
                if ([arrRoutSuggested containsObject:cell.textLabel.text])
                {
                    index12=indexPath.row;
                    break;
                    //[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
            }
            if (indexPath.row==index12)
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
            
            
        }
        else if (tblData.tag==30)
        {
            
            cell.textLabel.text=[arrPreferTime objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        else if (tblData.tag==40)
        {
            
            cell.textLabel.text=[arrNonPreferTime objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        else if (tblData.tag==50)
        {
            cell.textLabel.text=[arrPreferTime objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        else if (tblData.tag==60)
        {
            NSDictionary *dict=[arrTargets objectAtIndex:indexPath.row];
            
            cell.textLabel.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]];
            
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            
            long index12=123456;
            
            for (int k=0; k<arrSelectedTarget.count; k++)
            {
                NSDictionary *dict=[arrSelectedTarget objectAtIndex:k];
                if ([[dict valueForKey:@"Name"] isEqualToString:cell.textLabel.text])
                {
                    index12=indexPath.row;
                    break;
                }
            }
            if (indexPath.row==index12)
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
            
        }
        else if (tblData.tag==70)
        {
            NSDictionary *dict=[arrAttribute objectAtIndex:indexPath.row];
            cell.textLabel.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]];
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            
            long index12=1234567;
            
            for (int k=0; k<arrSelectedAttribute.count; k++)
            {
                NSDictionary *dict=[arrSelectedAttribute objectAtIndex:k];
                
                if ([[dict valueForKey:@"Name"] isEqualToString:cell.textLabel.text])
                {
                    index12=indexPath.row;
                    break;
                }
            }
            if (indexPath.row==index12)
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
        }
        if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
        {
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        return cell;
    }
    
}
-(void)buttonClickedPrimaryService:(UIButton*)btn
{
    [self endEditing];
    UIButton *button=(UIButton *)btn;
    NSIndexPath *indexpath = [_tblViewDynamic indexPathForCell:(DynamicViewTableViewCell *)btn.superview.superview];
    // NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    DynamicViewTableViewCell *tappedCell = (DynamicViewTableViewCell *)[_tblViewDynamic cellForRowAtIndexPath:indexpath];
    
    NSMutableDictionary *dict;
    dict=[[NSMutableDictionary alloc]init];
    dict=[globalArrayDict objectAtIndex:indexpath.row];
    
    if ([tappedCell.imgForPrimary.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        [tappedCell.imgForPrimary setImage:[UIImage imageNamed:@"check_box_1.png"]];
        [dict setObject:@"false" forKey:@"primary"];
        [dict setObject:@"false" forKey:@"IsPrimary"];
    }
    else
    {
        [tappedCell.imgForPrimary setImage:[UIImage imageNamed:@"check_box_2.png"]];
        [dict setObject:@"true" forKey:@"primary"];
        [dict setObject:@"true" forKey:@"IsPrimary"];
        
    }
    for (int i=0; i<globalArrayDict.count; i++)
    {
        NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
        dictData=[globalArrayDict objectAtIndex:i];
        if (i == indexpath.row)
        {
            //IsPrimary
            //[dictData setObject:@"true" forKey:@"IsPrimary"];
        }
        else
        {
            [dictData setObject:@"false" forKey:@"IsPrimary"];
            [dictData setObject:@"false" forKey:@"primary"];
        }
    }
    strClickedService=tappedCell.lblServiceName.text;
    [_tblViewDynamic reloadData];
    [self calculateTotal];
}
-(void)buttonClickedTax:(UIButton*)btn
{
    [self endEditing];
    UIButton *button=(UIButton *)btn;
    NSIndexPath *indexpath = [_tblViewDynamic indexPathForCell:(DynamicViewTableViewCell *)btn.superview.superview];
    
    //NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    DynamicViewTableViewCell *tappedCell = (DynamicViewTableViewCell *)[_tblViewDynamic cellForRowAtIndexPath:indexpath];
    NSMutableDictionary *dict;
    dict=[[NSMutableDictionary alloc]init];
    dict=[globalArrayDict objectAtIndex:indexpath.row];
    
    if ([tappedCell.imgForTax.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        [tappedCell.imgForTax setImage:[UIImage imageNamed:@"check_box_1.png"]];
        [dict setObject:@"false" forKey:@"tax"];
    }
    else
    {
        [tappedCell.imgForTax setImage:[UIImage imageNamed:@"check_box_2.png"]];
        [dict setObject:@"true" forKey:@"tax"];
        
    }
    
    
    
    strClickedService=tappedCell.lblServiceName.text;
    [_tblViewDynamic reloadData];
    [self calculateTotal];
    
}
-(void)buttonClickedDuration:(UIButton*)btn
{
    [self endEditing];
    tagForDuration=btn.tag;
    strTitile=@"SELECT TIME";
    strForDuration=@"duration";
    chkForCellDuration=YES;
    chkForDate=NO;
    [self addPickerViewDateTo];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblViewDynamic)
    {
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [arrSelectedOneTime removeObjectAtIndex:indexPath.row];
                                     [globalArrayDict removeObjectAtIndex:indexPath.row];
                                     [_tblViewDynamic reloadData];
                                     [self calculateTotal];
                                     [self addViews];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblViewDynamic)
    {
        //tempDict=[[NSMutableDictionary alloc]init];
        //tempDict=[globalArrayDict objectAtIndex:indexPath.row];
        // getIndex=indexPath;
        getIndex = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        NSLog(@"getindex>>%ld",(long)getIndex.row);
        NSLog(@"indexpath>>%ld",(long)indexPath.row);
        
        //        DynamicViewTableViewCell *tappedCell = (DynamicViewTableViewCell *)[_tblViewDynamic cellForRowAtIndexPath:indexPath];
        //0
        
        //        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        //        dict=[globalArrayDict objectAtIndex:indexPath.row];
        //        [dict setObject:tappedCell.txtServicePrice.text forKey:@"price"];
        //        [dict setObject:tappedCell.txtProductionValue.text forKey:@"productionValue"];
        //
        //        [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"duration"]] forKey:@"duration"];
        //        [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]] forKey:@"SysName"];
        //
        //        [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"quantity"]] forKey:@"quantity"];
        //         [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"primary"]] forKey:@"primary"];
        //         [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"tax"]] forKey:@"tax"];
        //        [globalArrayDict replaceObjectAtIndex:indexPath.row withObject:dict];
        //        [self calculateTotal];
    }
    else
    {
        NSInteger i;
        i=tblData.tag;
        isSelectEnable=NO;
        switch (i)
        {
                
            case 10:
            {
                NSString *str;
                if(indexPath.section==0)
                {
                    [_btnInitialService setTitle:[arrOnTimeSoldStandard objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                    str=[arrOnTimeSoldStandard objectAtIndex:indexPath.row];
                    [self getTargetId:[dictServiceSYSNAME valueForKey:str]];
                    [self getAttributeId:[dictServiceSYSNAME valueForKey:str]];
                    
                }
                else if (indexPath.section==1)
                {
                    [_btnInitialService setTitle:[arrOneTimeSoldNonStandard objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                    str=[arrOneTimeSoldNonStandard objectAtIndex:indexPath.row] ;
                }
                
                BOOL chkAdd;
                chkAdd=NO;
                for (int i=0; i<arrSelectedOneTime.count; i++)
                {
                    if ([arrSelectedOneTime containsObject:str])
                    {
                        chkAdd=YES;
                    }
                    else
                    {
                        chkAdd=NO;
                    }
                }
                if (chkAdd==YES)
                {
                    [global AlertMethod:@"Alert!!" :@"Already added this service"];
                }
                else
                {
                    strDidSelectService=str;
                    sectionNo=indexPath.section;
                    [arrSelectedOneTime addObject:str];
                    [self addViews];
                    [self getTotalServicePrice];
                    [_tblViewDynamic reloadData];
                    [self calculateTotal];
                }
                break;
            }
            case 20:
            {
                isSelectEnable=YES;
                
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
                if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
                {
                    [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                    // [arrSourceSelectedValue addObject:[arrServiceRoute objectAtIndex:indexPath.row]];
                    [arrSourceSelectedValue addObject:[arrRout objectAtIndex:indexPath.row]];
                    [arrRouteNumberSelected addObject:[arrRouteNumber objectAtIndex:indexPath.row]];
                    [arrRouteSysNameSelected addObject:[arrRouteNumber objectAtIndex:indexPath.row]];
                    [arrRoutSuggested addObject:[arrRout objectAtIndex:indexPath.row]];
                    [arrRoutSelectedId addObject:[arrRoutId objectAtIndex:indexPath.row]];
                    
                    
                    
                }
                else
                {
                    [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                    // [arrSourceSelectedValue removeObject:[arrServiceRoute objectAtIndex:indexPath.row]];
                    [arrSourceSelectedValue removeObject:[arrRout objectAtIndex:indexPath.row]];
                    [arrRouteNumberSelected removeObject:[arrRouteNumber objectAtIndex:indexPath.row]];
                    [arrRoutSuggested removeObject:[arrRout objectAtIndex:indexPath.row]];
                    [arrRouteSysNameSelected removeObject:[arrRouteNumber objectAtIndex:indexPath.row]];
                    [arrRoutSelectedId removeObject:[arrRoutId objectAtIndex:indexPath.row]];
                }
                
                //  [_btnServiceRoute setTitle:[arrServiceRoute objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                [_btnServiceRoute setTitle:[arrRout objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                break;
            }
            case 30:
            {
                [_btnPreferableTime setTitle:[arrPreferTime objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                strSelectedPreferableSysName=arrOfPreferableSysName[indexPath.row];
                break;
            }
            case 40:
            {
                [_btnNewPreferableTime setTitle:[arrNonPreferTime objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                strSeletcedNonPreferableSysName=arrOfNonPreferableSysName[indexPath.row];
                break;
            }
            case 50:
            {
                // [_btnServiceTime setTitle:[arrPreferTime objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                break;
            }
            case 60:
            {
                isSelectEnable=YES;
                
                NSDictionary *dict=[arrTargets objectAtIndex:indexPath.row];
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
                
                if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
                {
                    [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                    [arrSelectedTarget addObject:dict];
                    //[arrSelectedTargetId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetId"]]];
                    //[arrSelectedTarget addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                }
                else
                {
                    [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                    [arrSelectedTarget removeObject:dict];
                    //[arrSelectedTargetId removeObjectAtIndex:indexPath.row];
                    // [arrSelectedTarget removeObjectAtIndex:indexPath.row];
                    
                }
                
                [_btnTarget setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]  forState:UIControlStateNormal];//Name
                break;
            }
            case 70:
            {
                isSelectEnable=YES;
                
                NSDictionary *dict=[arrAttribute objectAtIndex:indexPath.row];
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
                
                if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
                {
                    [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                    [arrSelectedAttribute addObject:dict];
                    // [arrSelectedAttributeId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"AttributeId"]]];
                    //[arrSelectedAttribute addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                }
                else
                {
                    [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                    [arrSelectedAttribute removeObject:dict];
                    //[arrSelectedAttributeId removeObjectAtIndex:indexPath.row];
                    //[arrSelectedAttribute removeObjectAtIndex:indexPath.row];
                }
                
                [_btnAttribute setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]  forState:UIControlStateNormal];//Name
                break;
            }
                
            default:
                break;
        }
        
        if (isSelectEnable)
        {
            
        }
        else
        {
            [tblData removeFromSuperview];
            [viewBackGround removeFromSuperview];
        }
    }
    //[viewBackGround removeFromSuperview];
    //[tblData removeFromSuperview];
    // [DejalActivityView removeView];
}

//.......................................................
#pragma mark- Tableview Delegate
//.......................................................
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // [_txtInitialPrice resignFirstResponder];
    //[_txtDiscount resignFirstResponder];
    [textField resignFirstResponder];
    return YES;
}
//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    if(tblData.tag==20)
    {
        NSLog(@"tag 20 calledd");
        [self setValueForParticipants];
    }
    else if (tblData.tag==60)
    {
        NSLog(@"tag 60 for target  calledd");
        [self setValueForTargets];
        
    }
    else if (tblData.tag==70)
    {
        NSLog(@"tag 70 for attribute  calledd");
        [self setValueForAttributes];
    }
    //Nilind 02 Dec
    NSLog(@"arrOfSelectedServiceName>>>>%@",arrOfSelectedServiceName);
    NSLog(@"isPrimarySelectedService>>>>%@",isPrimarySelectedService);
    NSLog(@"arrSysName>>>>%@",arrSysName);
    if (arrSysName.count==0)
    {
        _txtServiceDuration.text=@"";
        
    }
    else
    {
        //Nilind 09 Jan
        
        NSString *strPrimaryServiceSysNameForInitialService;
        strPrimaryServiceSysNameForInitialService=[arrSysName objectAtIndex:0];
        NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
        for (int i=0; i<arrCtgry.count; i++)
        {
            NSDictionary *dict=[arrCtgry objectAtIndex:i];
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strServiceSysNameToCompare=[dict valueForKey:@"InitialServiceSysName"];
                //[arrInitial addObject:strServiceSysName];
                if ([strPrimaryServiceSysNameForInitialService isEqualToString:strServiceSysNameToCompare])
                {
                    
                    strPrimaryServiceSysNameForInitialService=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                    
                    break;
                }
            }
        }
        
        
        //End
        _txtServiceDuration.text=[dictDusration valueForKey:strPrimaryServiceSysNameForInitialService];//[arrSysName objectAtIndex:0]];
    }
    NSLog(@"%@",dictDusration);
    
    //.......................
}
- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    
    [self setValueForParticipants];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    NSLog(@"isPrimarySelectedService>>>>%@",isPrimarySelectedService);
    
}


-(void)setValueForParticipants
{
    if (arrSourceSelectedValue.count==0)
    {
        [_btnServiceRoute setTitle:@"--Select Route--" forState:UIControlStateNormal];
    }
    else
    {
        
        NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
        NSLog(@"joinedComponents>>%@",joinedComponents);
        [_btnServiceRoute setTitle:joinedComponents forState:UIControlStateNormal];
    }
}

-(void)setValueForTargets
{
    if (arrSelectedTarget.count==0)
    {
        [_btnTarget setTitle:@"--Select Target--" forState:UIControlStateNormal];
        
    }
    else
    {
        NSMutableArray *arrSelected;
        arrSelected=[[NSMutableArray alloc]init];
        arrSelectedTargetId=[[NSMutableArray alloc]init];
        for (int i=0; i<arrSelectedTarget.count; i++)
        {
            NSDictionary *dict=[arrSelectedTarget objectAtIndex:i];
            [arrSelected addObject:[dict valueForKey:@"Name"]];
            [arrSelectedTargetId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetId"]]];
            
        }
        
        
        NSString *joinedComponents = [arrSelected componentsJoinedByString:@","];
        NSLog(@"Selected Target>>%@",joinedComponents);
        [_btnTarget setTitle:joinedComponents forState:UIControlStateNormal];
    }
}
-(void)setValueForAttributes
{
    if (arrSelectedAttribute.count==0)
    {
        [_btnAttribute setTitle:@"--Select Attribute--" forState:UIControlStateNormal];
    }
    else
    {
        NSMutableArray *arrSelected;
        arrSelected=[[NSMutableArray alloc]init];
        arrSelectedAttributeId=[[NSMutableArray alloc]init];
        for (int i=0; i<arrSelectedAttribute.count; i++)
        {
            NSDictionary *dict=[arrSelectedAttribute objectAtIndex:i];
            [arrSelected addObject:[dict valueForKey:@"Name"]];
            [arrSelectedAttributeId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"AttributeId"]]];
            
        }
        
        NSString *joinedComponents = [arrSelected componentsJoinedByString:@","];
        NSLog(@"Selected Attribute>>%@",joinedComponents);
        [_btnAttribute setTitle:joinedComponents forState:UIControlStateNormal];
    }
}

-(void)setTableFrame:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    
    lblOk=[[UILabel alloc]init];
    lblOk.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    lblOk.textColor=[UIColor whiteColor];
    lblOk.text=@"OK";
    lblOk.textAlignment=NSTextAlignmentCenter;
    lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-80, tblData.frame.origin.y+tblData.frame.size.height+2, 160, 40);
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-60, tblData.frame.origin.y+tblData.frame.size.height+2, 120, 40);
    }
    lblOk.layer.borderWidth=1.0;
    lblOk.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    lblOk.layer.cornerRadius=5.0;
    [viewBackGround addSubview:lblOk];
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            [self setTableFrame:i];
            break;
        }
        case 40:
        {
            [self setTableFrame:i];
            break;
        }
        case 50:
        {
            [self setTableFrame:i];
            break;
        }
        case 60:
        {
            [self setTableFrame:i];
            break;
        }
            
        case 70:
        {
            [self setTableFrame:i];
            break;
        }
            
            
            
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}
//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    
    if (chkForDate==YES)
    {
        [pickerDate setMinimumDate:[NSDate date]];
        pickerDate.datePickerMode =UIDatePickerModeDate;
    }
    else
    {
        pickerDate.datePickerMode =UIDatePickerModeTime;
        
        if (chkServiceTime==YES)
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
            [pickerDate setLocale:locale];
        }
        else
        {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
            [pickerDate setLocale:locale];
        }
        
    }
    //pickerDate.datePickerMode =UIDatePickerModeTime;  //UIDatePickerModeDate;
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=strTitile;//@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    
    if (chkForCellDuration==YES)
    {
        [btnDone addTarget: self action: @selector(setDateDoneForDuration:)forControlEvents: UIControlEventTouchDown];
    }
    else
    {
        [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    }
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
-(void)tapDetectedOnBackGroundView
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateDoneForDuration:(id)sender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tagForDuration inSection:0];
    
    DynamicViewTableViewCell *tappedCell = (DynamicViewTableViewCell *)[_tblViewDynamic cellForRowAtIndexPath:indexPath];
    chkForCellDuration=NO;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    // [dateFormat setDateFormat:@"hh:mm:a"];
    [dateFormat setDateFormat:@"HH:mm"];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    tappedCell.txtDuration.text=strDate;
    
    //Nilind 15 May
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    dict=[globalArrayDict objectAtIndex:tagForDuration];
    NSLog(@"globalArrayDict>>>.%@",globalArrayDict);
    [dict setObject:tappedCell.txtDuration.text forKey:@"duration"];
    [_tblViewDynamic reloadData];
    [self calculateTotal];
    //End
    
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"HH:mm"];
    strSelectedTimeIn24Hrs=[dateFormat1 stringFromDate:pickerDate.date];
    
    // chkForDate=NO;
    // chkServiceTime=NO;
    // _txtServiceTime.textAlignment=NSTextAlignmentLeft;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    // _txtTotalDuration.text=@"00:00";
    //_txtTotalDuration.text= [self addTime:_txtTotalDuration.text :[NSString stringWithFormat:@"%@",tappedCell.txtDuration.text]];
    
    
}
-(void)setDateOnDone:(id)sender
{
    if (chkForDate==YES)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        strDate = [dateFormat stringFromDate:pickerDate.date];
        _txtServiceDate.text=strDate;
        _txtServiceDate.textAlignment=NSTextAlignmentLeft;
        chkForDate=NO;
    }
    else
    {
        if (chkServiceTime==YES)
        {
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            // [dateFormat setDateFormat:@"hh:mm:a"];
            [dateFormat setDateFormat:@"HH:mm"];
            strDate = [dateFormat stringFromDate:pickerDate.date];
            _txtServiceTime.text=strDate;
            
            NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
            [dateFormat1 setDateFormat:@"HH:mm"];
            strSelectedTimeIn24Hrs=[dateFormat1 stringFromDate:pickerDate.date];
            
            chkForDate=YES;
            chkServiceTime=NO;
            _txtServiceTime.textAlignment=NSTextAlignmentLeft;
            
            
        }
        else
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"HH:mm"];
            strDate = [dateFormat stringFromDate:pickerDate.date];
            //_txtServiceDuration.text=strDate;
            //_txtServiceDuration.textAlignment=NSTextAlignmentLeft;
            _txtServiceToTime.text=strDate;
            _txtServiceToTime.textAlignment=NSTextAlignmentLeft;
            
            //chkForDate=YES;
        }
        
        
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}


- (IBAction)actionOnBackAppointment:(id)sender
{
    [self endEditing];
    [_viewAppointment removeFromSuperview];
}

- (IBAction)actionOnSkip:(id)sender
{
    [self endEditing];
    [self goToAppointment];
}
- (IBAction)actionOnServiceTime:(id)sender
{
    [self endEditing];
    //tblData.tag=50;
    // chkForDate=NO;
    //[self tableLoad:tblData.tag];
    strTitile=@"SELECT TIME";
    chkForDate=NO;
    chkServiceTime=YES;
    [self addPickerViewDateTo];
}
- (IBAction)actionOnServiceToTime:(id)sender
{
    [self endEditing];
    strTitile=@"SELECT TIME";
    chkForDate=NO;
    chkServiceTime=NO;
    [self addPickerViewDateTo];
}
-(void)fetchFromCoreDataStandard
{
    
    
    
    
    if(strLeadId.length==0||[strLeadId isEqual:[NSNull null]])
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        //strLeadId=[defs valueForKey:@"ForInitialSetUpLeadId"];
        strLeadId=[defs valueForKey:@"LeadId"];
    }
    
    //.............
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrServiceName=[[NSMutableArray alloc]init];
    arrServiceSysName=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            if ([[matches valueForKey:@"isSold"] isEqualToString:@"true"]) {
                
                
                if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] isEqualToString:@"(null)"])
                {
                    [arrServiceName addObject: @""];
                }
                else
                {
                    [arrServiceName addObject: [dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                }
                
                //Nilind 02 Dec
                
                if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] isEqualToString:@"(null)"])
                {
                    [arrServiceSysName addObject: @""];
                }
                else
                {
                    [arrServiceSysName addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]];
                }
                
                
                //..............
                
                [arrOfDictAllDataOfService addObject:matches];
                
            }
        }
        
        [self findDepartmentNameForService];
    }
}
-(void)findDepartmentNameForService
{
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSString *strDepartmentId;
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            strDepartmentId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DepartmentId"]];
            
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]];
                
                for (int q=0; q<arrServiceName.count; q++) {
                    
                    if ([strNamee isEqualToString:[arrServiceName objectAtIndex:q]]) {
                        
                        [arrOfServiceDepartmentName addObject:strDepartmentId];
                        
                    }
                }
            }
        }
    }
}

-(void)serviceName
{
    NSMutableArray *name,*sysName;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                
            }
        }
    }
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
}
-(void)serviceSYSNAME
{
    NSMutableArray *name,*sysName,*esitimatedDuration;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    esitimatedDuration=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                
                [esitimatedDuration addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"InitialEstimatedDuration"]]];
                
            }
        }
    }
    dictServiceSYSNAME = [NSDictionary dictionaryWithObjects:sysName forKeys:name];
    
    dictDusration = [NSDictionary dictionaryWithObjects:esitimatedDuration forKeys:sysName];
}

-(void)getTotalServicePrice
{
    
    arrPriceProduction=[[NSMutableArray alloc]init];
    arrDuration=[[NSMutableArray alloc]init];
    NSString *strServiceSysName=strDidSelectService;
    
    if (sectionNo==0)
    {
        strServiceSysName=[dictServiceSYSNAME valueForKey:strDidSelectService];
        NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
        
        
        for (int i=0; i<arrCtgry.count; i++)
        {
            NSDictionary *dict=[arrCtgry objectAtIndex:i];
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                
                NSString *strServiceSysNameToCompare=[dict valueForKey:@"SysName"];
                if ([strServiceSysNameToCompare isEqualToString:strServiceSysName])
                {
                    dictMutable=[[NSMutableDictionary alloc]init];
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"InitialPrice"]] forKey:@"price"];
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"InitialPrice"]] forKey:@"productionValue"];
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"InitialEstimatedDuration"]] forKey:@"duration"];
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]] forKey:@"SysName"];
                    
                    [dictMutable setObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]] forKey:@"ServiceName"];
                    
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",@"1"]forKey:@"quantity"];
                    
                    [dictMutable setObject:@"false" forKey:@"primary"];
#pragma mark- TEMP CHANGE
                    if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsTaxable"]] isEqualToString:@"1"])
                    {
                        [dictMutable setObject:@"true" forKey:@"tax"];
                        
                    }
                    else
                    {
                        [dictMutable setObject:@"false" forKey:@"tax"];
                    }
                    [dictMutable setObject:@"true" forKey:@"IsStandard"];
                    [dictMutable setObject:@"false" forKey:@"IsPrimary"];
                    [globalArrayDict addObject:dictMutable];
                }
            }
        }
    }
    else if (sectionNo==1)
    {
        [self fetchSoldNonStandard];
        NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        if (arrAllObj12.count==0)
        {
        }else
        {
            
            NSManagedObject *matches21;
            for (int k=0; k<arrAllObj12.count; k++)
            {
                matches21=arrAllObj12[k];
                NSLog(@"Lead IDDDD====%@",[matches21 valueForKey:@"leadId"]);
                if ([[matches21 valueForKey:@"ServiceName"] isEqualToString:strServiceSysName])
                {
                    dictMutable=[[NSMutableDictionary alloc]init];
                    
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",[matches21 valueForKey:@"InitialPrice"]] forKey:@"price"];
                    
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",[matches21 valueForKey:@"InitialPrice"]] forKey:@"productionValue"];
                    
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",@"00:30"] forKey:@"duration"];
                    
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",[matches21 valueForKey:@"ServiceName"]] forKey:@"SysName"];
                    
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",[matches21 valueForKey:@"ServiceName"]] forKey:@"ServiceName"];
                    
                    [dictMutable setObject:[NSString stringWithFormat:@"%@",@"1"]forKey:@"quantity"];
                    
                    [dictMutable setObject:@"false" forKey:@"primary"];
                    
                    [dictMutable setObject:@"false" forKey:@"tax"];
                    
                    [dictMutable setObject:@"false" forKey:@"IsStandard"];
                    
                    [dictMutable setObject:@"false" forKey:@"IsPrimary"];
                    
                    [globalArrayDict addObject:dictMutable];
                    
                }
            }
        }
        
        
    }
    
    
}
-(void)getTargetId:(NSString *)string
{
    NSString *strTarget;
    NSMutableArray *arrTempTarget,*arrTargetId;
    arrTargetId=[[NSMutableArray alloc]init];
    arrTempTarget=[[NSMutableArray alloc]init];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasterss=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasterss valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                if ([string isEqualToString:[dict valueForKey:@"SysName"]])
                {
                    strTarget=[dict valueForKey:@"ServiceTargets"];
                }
            }
        }
    }
    NSArray *arr=[dictMasterss valueForKey:@"Targets"];
    for (int i=0; i<arr.count; i++)
    {
        NSDictionary *dict=[arr objectAtIndex:i];
        {
            [arrTempTarget addObject:dict];
        }
    }
    if(strTarget.length==0|| [strTarget isEqual:nil])
    {
    }
    else
    {
        NSArray *targetId = [strTarget componentsSeparatedByString:@","];
        for(int i=0;i<targetId.count;i++)
        {
            for (int j=0; j<arrTempTarget.count; j++)
            {
                NSDictionary *dict=[arrTempTarget objectAtIndex:j];
                NSString *str1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetId"]];
                NSString *str2=[NSString stringWithFormat:@"%@",[targetId objectAtIndex:i]];
                if ([str1 isEqualToString:str2])
                {
                    [arrTargetId addObject:[dict valueForKey:@"Name"]];
                    [arrSelectedTarget addObject:dict];
                }
                
            }
        }
        NSString *str;
        str=[arrTargetId componentsJoinedByString:@","];
        [_btnTarget setTitle:str forState:UIControlStateNormal];
    }
}
-(void)getAttributeId:(NSString *)string
{
    NSString *strAttribute;
    NSMutableArray *arrTempAttribute,*arrAttributeId;
    arrTempAttribute=[[NSMutableArray alloc]init];
    arrAttributeId=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasterss=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasterss valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                if ([string isEqualToString:[dict valueForKey:@"SysName"]])
                {
                    strAttribute=[dict valueForKey:@"ServiceAttributes"];
                }
            }
        }
    }
    NSArray *arr=[dictMasterss valueForKey:@"Attributes"];
    for (int i=0; i<arr.count; i++)
    {
        NSDictionary *dict=[arr objectAtIndex:i];
        {
            [arrTempAttribute addObject:dict];
        }
    }
    
    if(strAttribute.length==0|| [strAttribute isEqual:nil])
    {
    }
    else
    {
        NSArray *targetId = [strAttribute componentsSeparatedByString:@","];
        for(int i=0;i<targetId.count;i++)
        {
            for (int j=0; j<targetId.count; j++)
            {
                NSDictionary *dict=[arrTempAttribute objectAtIndex:j];
                NSString *str1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AttributeId"]];
                NSString *str2=[NSString stringWithFormat:@"%@",[targetId objectAtIndex:i]];
                
                if ([str1 isEqualToString:str2])
                {
                    [arrAttributeId addObject:[dict valueForKey:@"Name"]];
                    [arrSelectedAttribute addObject:dict];
                }
                
            }
        }
        NSString *str;
        str=[arrAttributeId componentsJoinedByString:@","];
        [_btnAttribute setTitle:str forState:UIControlStateNormal];
    }
}
-(void)getTotalServicePriceMinus{
    
    totalServicePrice=0;
    
    for (int k=0; k<arrOfSelectedServiceName.count; k++)//arrOfSelectedServiceName
    {
        
        NSString *strNameee=[dictServiceSYSNAME valueForKey:[NSString stringWithFormat:@"%@",[arrOfSelectedServiceName objectAtIndex:k]]];
        
        for (int l=0; l<arrOfDictAllDataOfService.count; l++) {
            
            NSManagedObject *objTemp=arrOfDictAllDataOfService[l];
            
            NSString *strServiceSysName=[objTemp valueForKey:@"serviceSysName"];
            
            if ([strNameee isEqualToString:strServiceSysName]) {
                
                double maintenanceValue=[[objTemp valueForKey:@"maintenancePrice"] doubleValue];
                
                totalServicePrice=totalServicePrice-maintenanceValue;
                
                _txtServicePrice.text=[NSString stringWithFormat:@"%.2f",totalServicePrice];
                _txtServiceProdValue.text=[NSString stringWithFormat:@"%.2f",totalServicePrice];
                
            }
        }
    }
}
#pragma mark- ROUTE API CALLING

-(void)routApi
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
        [_activityIndicator stopAnimating];
        [_activityIndicator setHidden:YES];
    }
    else
    {
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesSuggestRoute];
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"CustomerAddressId",
             @"accountNumber",
             @"companyKey",
             @"Address1",
             @"Address2",
             @"Zipcode",
             @"CountryId",
             @"CountrySysName",
             @"StateId",
             @"StateSysName",
             @"CityName",
             @"CitySysName",
             nil];
        value=[NSArray arrayWithObjects:
               @"",
               strAccountNo,
               strCompanyKey,
               strAddress1,
               @"",
               strZipcode,
               @"",
               strCountrySysName,
               @"",
               strStateSysName,
               strCityName,
               @"",
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End

        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil) {
            
            
        }
        else
        {
            
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (dictionary.count==0 || dictionary==nil) {
                
            } else {
                
                arrRout=[[NSMutableArray alloc]init];
                
                arrRouteNumber=[[NSMutableArray alloc]init];
                arrRouteSysName=[[NSMutableArray alloc]init];
                
                arrOtherRout=[dictionary valueForKey:@"OtherRoute"];
                arrSuggestedRout=[dictionary valueForKey:@"SuggestedRoute"];
                for (int i=0; i<arrSuggestedRout.count; i++)
                {
                    NSDictionary *dict=[arrSuggestedRout objectAtIndex:i];
                    [arrRout addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteName"]]];
                    [arrRouteNumber addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRouteNumberSelected addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRouteSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRouteSysNameSelected addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    
                    [arrRoutSuggested addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteName"]]];
                    [arrSourceSelectedValue addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteName"]]];
                    [arrRoutSelectedId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteId"]]];
                    [arrRoutId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteId"]]];
                }
                if (!(arrSourceSelectedValue.count==0)) {
                    
                    NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
                    [_btnServiceRoute setTitle:joinedComponents forState:UIControlStateNormal];
                    
                }
                for (int i=0; i<arrOtherRout.count; i++)
                {
                    NSDictionary *dict=[arrOtherRout objectAtIndex:i];
                    [arrRout addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteName"]]];
                    [arrRouteNumber addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRouteSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]]];
                    [arrRoutId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteId"]]];
                    
                }
                
            }
        }
        [DejalActivityView removeView];
        [_activityIndicator stopAnimating];
        [_activityIndicator setHidden:YES];
    }
}

#pragma mark -SalesAuto Fetch Core Data


-(void)salesFetch
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        
        matches=arrAllObj[0];
        
        strAccountNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountNo"]];
        strAddress1=[NSString stringWithFormat:@"%@",[matches valueForKey:@"servicesAddress1"]];
        strZipcode=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceZipcode"]];
        strCountrySysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceCountry"]];
        
        strStateSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceState"]];
        strCityName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceCity"]];
        _lblTitle.text=[NSString stringWithFormat:@"Opportunity #: %@, A/C #: %@",[matches valueForKey:@"leadNumber"],[matches valueForKey:@"accountNo"]];
    }
    
    
}
//..........................

#pragma mark- RANGE TIME API CALLING

-(void)rangeTimeApi
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        [_activityIndicator stopAnimating];
        [_activityIndicator setHidden:YES];
    }
    else
    {
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlSalesRangeofTime,strCompanyKey];
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        NSURL *url = [NSURL URLWithString:strUrl];
        //NSURLRequest *request = [NSURLRequest requestWithURL:url]; //temp 
        
        NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
        
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End


        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
        if (jsonData==nil) {
            
            
        } else {
            
            NSArray* responseArr = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (responseArr.count==0) {
                
            } else {
                arrPreferTime=[[NSMutableArray alloc]init];
                arrNonPreferTime=[[NSMutableArray alloc]init];
                
                arrOfNonPreferableSysName=[[NSMutableArray alloc]init];
                arrOfPreferableSysName=[[NSMutableArray alloc]init];
                
                for (int i=0;i<responseArr.count; i++)
                {
                    NSDictionary *dict=[responseArr objectAtIndex:i];
                    [arrPreferTime addObject:[NSString stringWithFormat:@"%@-%@",[dict valueForKey:@"StartInterval"],[dict valueForKey:@"EndInterval"]]];
                    [arrNonPreferTime addObject:[NSString stringWithFormat:@"%@-%@",[dict valueForKey:@"StartInterval"],[dict valueForKey:@"EndInterval"]]];
                    [arrOfPreferableSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                    [arrOfNonPreferableSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                    
                }
            }
        }
        [self routApi];
    }
}

-(void)apiViewAppointment
{
    //http://service.dps.com/api/MobileAppToCore/GetViewAppointmentAsync
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
        
    }
    else
    {
        
        NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
        
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesViewAppointment];
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"serviceDate",
             @"routeNumbers",
             @"companyKey",
             nil];
        value=[NSArray arrayWithObjects:
               _txtServiceDate.text,
               joinedComponents,
               strCompanyKey,
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil) {
            
            [global AlertMethod:@"ALert" :@"No appointments available"];
            
        } else {
            
            NSArray *arrResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (arrResponse.count==0 || arrResponse==nil) {
                
                [global AlertMethod:@"ALert" :@"No appointments available"];
                
            } else {
                arrOfResponseRouteAppointments=arrResponse;
                indexxRouteSelected=0;
                arrOfViewAppointments=nil;
                arrOfViewAppointments=[[NSMutableArray alloc]init];
                
                strSelectedRoutes=arrSourceSelectedValue[indexxRouteSelected];
                _lblOne.text=strSelectedRoutes;
                for (int k=0; k<arrOfResponseRouteAppointments.count; k++) {
                    
                    NSDictionary *dictData=arrOfResponseRouteAppointments[k];
                    
                    NSString *strRouteNo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RouteNo"]];
                    if ([strRouteNo isEqualToString:arrRouteNumberSelected[indexxRouteSelected]]) {
                        
                        [arrOfViewAppointments addObject:dictData];
                        
                    }
                }
                
                _viewAppointment.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
                [self.view addSubview:_viewAppointment];
                tblData.tag=1999;
                [_tblAppointment reloadData];
                
            }
        }
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
    }
}

- (IBAction)action_ForwardRouteNames:(id)sender {
    
    [self endEditing];
    arrOfViewAppointments=nil;
    arrOfViewAppointments=[[NSMutableArray alloc]init];
    
    if (indexxRouteSelected+1<arrRouteNumberSelected.count) {
        
        indexxRouteSelected++;
        
        strSelectedRoutes=arrSourceSelectedValue[indexxRouteSelected];
        _lblOne.text=strSelectedRoutes;
        
        for (int k=0; k<arrOfResponseRouteAppointments.count; k++) {
            
            NSDictionary *dictData=arrOfResponseRouteAppointments[k];
            
            NSString *strRouteNo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RouteNo"]];
            if ([strRouteNo isEqualToString:arrRouteNumberSelected[indexxRouteSelected]]) {
                
                [arrOfViewAppointments addObject:dictData];
                
            }
        }
        [_tblAppointment reloadData];
        
    }
}

- (IBAction)action_BackRouteNames:(id)sender {
    
    [self endEditing];
    arrOfViewAppointments=nil;
    arrOfViewAppointments=[[NSMutableArray alloc]init];
    
    if (indexxRouteSelected-1<arrRouteNumberSelected.count) {
        
        indexxRouteSelected--;
        strSelectedRoutes=arrSourceSelectedValue[indexxRouteSelected];
        _lblOne.text=strSelectedRoutes;
        
        for (int k=0; k<arrOfResponseRouteAppointments.count; k++) {
            
            NSDictionary *dictData=arrOfResponseRouteAppointments[k];
            
            NSString *strRouteNo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RouteNo"]];
            if ([strRouteNo isEqualToString:arrRouteNumberSelected[indexxRouteSelected]]) {
                
                [arrOfViewAppointments addObject:dictData];
                
            }
        }
        [_tblAppointment reloadData];
        
    }
}

-(void)sendToServerSaveSetup
{
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    //UrlSalesSaveInitialSetupnGenerateWorkOrder
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
        
    }
    else
    {
        
        // NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
        
        NSString *strUrl,*strSetupInstruction,*strInitialServiceInstruction,*strPreferableTime,*strNonPreferableTime,*strPrimaryServiceSysName,*strRouteSysNAme,*strInitialServiceSysNames,*strAccountNoss,*strLeadNumber;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesSaveInitialSetup];
        NSArray *key,*value;
        
        strSetupInstruction=_txtViewSetupInstruction.text;
        strInitialServiceInstruction=_txtViewServiceInstruction.text;
        strPreferableTime=_btnPreferableTime.titleLabel.text;
        strNonPreferableTime=_btnNewPreferableTime.titleLabel.text;
        
        if (strSetupInstruction.length==0) {
            strSetupInstruction=@"";
        }
        if (strInitialServiceInstruction.length==0) {
            strInitialServiceInstruction=@"";
        }
        if (strPreferableTime.length==0) {
            strPreferableTime=@"";
        }
        if([strPreferableTime isEqual: [NSNull null]])
        {
            strPreferableTime=@"";
        }
        if ([strPreferableTime isEqualToString:@"--Select Preferable Time--"]) {
            strPreferableTime=@"";
        }
        
        if ([strNonPreferableTime isEqualToString:@"--Select Non-Preferable Time--"]) {
            strNonPreferableTime=@"";
        }
        if (strNonPreferableTime.length==0) {
            strNonPreferableTime=@"";
        }
        if([strNonPreferableTime isEqual: [NSNull null]])
        {
            strNonPreferableTime=@"";
        }
        
        if([strSelectedPreferableSysName isEqual:[NSNull null]] || strSelectedPreferableSysName.length==0)
        {
            strPreferableTime=@"";
        }
        else
        {
            strPreferableTime=strSelectedPreferableSysName;
            
        }
        
        if([strSeletcedNonPreferableSysName isEqual:[NSNull null]] || strSeletcedNonPreferableSysName.length==0)
        {
            strNonPreferableTime=@"";
        }
        else
        {
            
            strNonPreferableTime=strSeletcedNonPreferableSysName;
        }
        
        
        
        strPrimaryServiceSysName=[NSString stringWithFormat:@"%@",arrSysName[0]];
        
        //Nilind 30 Dec
        
        NSString *strSoldSerivce;
        strSoldSerivce=strPrimaryServiceSysName;
        
        
        NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
        for (int i=0; i<arrCtgry.count; i++)
        {
            NSDictionary *dict=[arrCtgry objectAtIndex:i];
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strServiceSysNameToCompare=[dict valueForKey:@"InitialServiceSysName"];
                //[arrInitial addObject:strServiceSysName];
                if ([strServiceSysNameToCompare isEqualToString:strSoldSerivce])
                {
                    
                    //[temArrServiceName addObject:[dict valueForKey:@"Name"]];
                    //[arrInitial addObject:[dict valueForKey:@"InitialServiceSysName"]];
                    strPrimaryServiceSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                    break;
                }
            }
        }
        
        
        
        //..............................
        
        NSMutableArray *arrOfInitialServiceSysNames=[[NSMutableArray alloc]init];
        
        //Nilind 30 Dec
        
        if (arrSysName.count==1)  //working 30 DEC
        {
            
            strInitialServiceSysNames=[arrSysName objectAtIndex:0];
            
        }
        else
        {
            
            for (int k=0; k<arrSysName.count; k++)
            {
                
                if (k==0)
                {
                    
                }
                else
                {
                    
                    [arrOfInitialServiceSysNames addObject:[NSString stringWithFormat:@"%@",arrSysName[k]]];
                    
                }
            }
            strInitialServiceSysNames=[arrOfInitialServiceSysNames componentsJoinedByString:@","];
            
        }
        
        //..................
        
        
        strRouteSysNAme=arrRouteSysNameSelected[0];
        
        if (strInitialServiceSysNames.length==0) {
            strInitialServiceSysNames=@"";
        }
        if (strRouteSysNAme.length==0) {
            strRouteSysNAme=@"";
        }
        
        if (strPrimaryServiceSysName.length==0) {
            strPrimaryServiceSysName=@"";
        }
        
        NSUserDefaults *defsAccountNo=[NSUserDefaults standardUserDefaults];
        strAccountNoss=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"AccountNos"]];
        strAccountNoss=[strAccountNoss stringByReplacingOccurrencesOfString:@"Ac#:" withString:@""];
        
        strLeadNumber=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"LeadNumber"]];
        strLeadNumber=[strLeadNumber stringByReplacingOccurrencesOfString:@"Opportunity#:" withString:@""];
        
        if (strAccountNoss.length==0 || [strAccountNoss isEqualToString:@"(null)"]) {
            strAccountNoss=@"";
        }
        if (strLeadNumber.length==0 || [strLeadNumber isEqualToString:@"(null)"]) {
            strLeadNumber=@"";
        }
        
        key=[NSArray arrayWithObjects:
             @"PrimaryServiceSysName",
             @"AccountNumber",
             @"companykey",
             @"SetupStartDate",
             @"LeadNo",
             @"Typeoftime",
             @"NonPreferablerangeSysName",
             @"PreferablerangeSysName",
             @"RouteSysName",
             @"InitialServicePrice",
             @"InitialServiceDuration",
             @"InitialServiceDate",
             @"InitialServiceSysNames",
             @"InitialServiceTime",
             @"LockInitialRoute",
             @"LockInitialtime",
             @"InitialServiceInstruction",
             @"SetupInstruction",
             nil];
        
        
        value=[NSArray arrayWithObjects:
               strPrimaryServiceSysName,          //priya_service_2,strPrimaryServiceSysName
               strAccountNoss,
               strCompanyKey,
               _txtServiceDate.text,
               strLeadNumber,
               strTypeOfTime,
               strNonPreferableTime,//NIL
               strPreferableTime,//nil
               strRouteSysNAme,
               _txtServicePrice.text,
               _txtServiceDuration.text,
               _txtServiceDate.text,
               strInitialServiceSysNames,       //"priyaserivce1",strInitialServiceSysNames
               strSelectedTimeIn24Hrs,
               islockRoute,
               isLockTime,
               strInitialServiceInstruction,
               strSetupInstruction,
               nil];
        
        
        
        /*  key=[NSArray arrayWithObjects:
         @"PrimaryServiceSysName",
         @"AccountNumber",
         @"companykey",
         @"SetupStartDate",
         @"LeadNo",
         @"Typeoftime",
         @"NonPreferablerangeSysName",
         @"PreferablerangeSysName",
         @"RouteSysName",
         @"InitialServicePrice",
         @"InitialServiceDuration",
         @"InitialServiceDate",
         @"InitialServiceSysNames",
         @"InitialServiceTime",
         @"LockInitialRoute",
         @"LockInitialtime",
         @"InitialServiceInstruction",
         @"SetupInstruction",
         nil];
         
         
         value=[NSArray arrayWithObjects:
         @"priyaserivce1",
         @"10015",
         @"Automation",
         @"12/22/2016",
         strLeadId,
         strTypeOfTime,
         strNonPreferableTime,
         strPreferableTime,
         @"Route12e",
         @"100.0",
         @"23:55:00",
         @"12/22/2016",
         strInitialServiceSysNames,
         strSelectedTimeIn24Hrs,
         @"true",
         @"true",
         strInitialServiceInstruction,
         strSetupInstruction,
         nil];
         */
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        // NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"FINAL FORM JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            
            
        }
        else
        {
            
            NSArray *arrResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (arrResponse.count==0 || arrResponse==nil)
            {
                
            } else
            {
                //Nilind 28 Dec
                NSLog(@"Save & Continue Response >>%@",arrResponse);
                [self initialSetupStatusUpdate];
                // [self goToAppointment];
                
                //........
            }
            //            [_activityIndicator stopAnimating];
            //            [_activityIndicator setHidden:YES];
        }
        //[DejalActivityView removeView];
    }
    
    
}
-(void)sendToServerSaveSetupAndGenerateWorkOrder
{
    //   [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
        
    }
    else
    {
        
        // NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
        
        NSString *strUrl,*strSetupInstruction,*strInitialServiceInstruction,*strPreferableTime,*strNonPreferableTime,*strPrimaryServiceSysName,*strRouteSysNAme,*strInitialServiceSysNames,*strAccountNoss,*strLeadNumber;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesSaveInitialSetupnGenerateWorkOrder];
        NSArray *key,*value;
        
        strSetupInstruction=_txtViewSetupInstruction.text;
        strInitialServiceInstruction=_txtViewServiceInstruction.text;
        strPreferableTime=_btnPreferableTime.titleLabel.text;
        strNonPreferableTime=_btnNewPreferableTime.titleLabel.text;
        
        if (strSetupInstruction.length==0) {
            strSetupInstruction=@"";
        }
        if (strInitialServiceInstruction.length==0) {
            strInitialServiceInstruction=@"";
        }
        if (strPreferableTime.length==0) {
            strPreferableTime=@"";
        }
        if([strPreferableTime isEqual: [NSNull null]])
        {
            strPreferableTime=@"";
        }
        if ([strPreferableTime isEqualToString:@"--Select Preferable Time--"])
        {
            strPreferableTime=@"";
        }
        
        if ([strNonPreferableTime isEqualToString:@"--Select Non-Preferable Time--"]) {
            strNonPreferableTime=@"";
        }
        if (strNonPreferableTime.length==0) {
            strNonPreferableTime=@"";
        }
        if([strNonPreferableTime isEqual: [NSNull null]])
        {
            strNonPreferableTime=@"";
        }
        
        if([strSelectedPreferableSysName isEqual:[NSNull null]] || strSelectedPreferableSysName.length==0)
        {
            strPreferableTime=@"";
        }
        else
        {
            strPreferableTime=strSelectedPreferableSysName;
            
        }
        
        if([strSeletcedNonPreferableSysName isEqual:[NSNull null]] || strSeletcedNonPreferableSysName.length==0)
        {
            strNonPreferableTime=@"";
        }
        else
        {
            
            strNonPreferableTime=strSeletcedNonPreferableSysName;
        }
        
        
        
        strPrimaryServiceSysName=[NSString stringWithFormat:@"%@",arrSysName[0]];
        //Nilind 30 Dec
        
        NSString *strSoldSerivce;
        strSoldSerivce=strPrimaryServiceSysName;
        
        
        NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
        for (int i=0; i<arrCtgry.count; i++)
        {
            NSDictionary *dict=[arrCtgry objectAtIndex:i];
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *strServiceSysNameToCompare=[dict valueForKey:@"InitialServiceSysName"];
                //[arrInitial addObject:strServiceSysName];
                if ([strServiceSysNameToCompare isEqualToString:strSoldSerivce])
                {
                    
                    //[temArrServiceName addObject:[dict valueForKey:@"Name"]];
                    //[arrInitial addObject:[dict valueForKey:@"InitialServiceSysName"]];
                    strPrimaryServiceSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                    
                    break;
                }
            }
        }
        
        
        
        //..............................
        
        NSMutableArray *arrOfInitialServiceSysNames=[[NSMutableArray alloc]init];
        
        //Nilind 30 Dec
        
        if (arrSysName.count==1)  //working 30 DEC
        {
            
            strInitialServiceSysNames=[arrSysName objectAtIndex:0];
            
        }
        else
        {
            
            for (int k=0; k<arrSysName.count; k++)
            {
                
                if (k==0)
                {
                    
                }
                else
                {
                    
                    [arrOfInitialServiceSysNames addObject:[NSString stringWithFormat:@"%@",arrSysName[k]]];
                    
                }
            }
            strInitialServiceSysNames=[arrOfInitialServiceSysNames componentsJoinedByString:@","];
            
        }
        //..................
        
        strRouteSysNAme=arrRouteSysNameSelected[0];
        
        if (strInitialServiceSysNames.length==0) {
            strInitialServiceSysNames=@"";
        }
        if (strRouteSysNAme.length==0) {
            strRouteSysNAme=@"";
        }
        
        if (strPrimaryServiceSysName.length==0) {
            strPrimaryServiceSysName=@"";
        }
        
        NSUserDefaults *defsAccountNo=[NSUserDefaults standardUserDefaults];
        strAccountNoss=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"AccountNos"]];
        strLeadNumber=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"LeadNumber"]];
        
        if (strAccountNoss.length==0 || [strAccountNoss isEqualToString:@"(null)"]) {
            strAccountNoss=@"";
        }
        if (strLeadNumber.length==0 || [strLeadNumber isEqualToString:@"(null)"]) {
            strLeadNumber=@"";
        }
        //        strAccountNoss=@"10014";
        //        strLeadNumber=@"1675";
        // strInitialServiceSysNames=@"SignaturePestControl";
        
        
        
        key=[NSArray arrayWithObjects:
             @"PrimaryServiceSysName",
             @"AccountNumber",
             @"companykey",
             @"SetupStartDate",
             @"LeadNo",
             @"Typeoftime",
             @"NonPreferablerangeSysName",
             @"PreferablerangeSysName",
             @"RouteSysName",
             @"InitialServicePrice",
             @"InitialServiceDuration",
             @"InitialServiceDate",
             @"InitialServiceSysNames",
             @"InitialServiceTime",
             @"LockInitialRoute",
             @"LockInitialtime",
             @"InitialServiceInstruction",
             @"SetupInstruction",
             nil];
        
        
        value=[NSArray arrayWithObjects:
               strPrimaryServiceSysName,
               strAccountNoss,
               strCompanyKey,
               _txtServiceDate.text,
               strLeadNumber,
               strTypeOfTime,
               strNonPreferableTime,
               strPreferableTime,
               strRouteSysNAme,
               _txtServicePrice.text,
               _txtServiceDuration.text,
               _txtServiceDate.text,
               strInitialServiceSysNames,
               strSelectedTimeIn24Hrs,
               islockRoute,
               isLockTime,
               strInitialServiceInstruction,
               strSetupInstruction,
               nil];
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        // NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"FINAL FORM JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            
            
        }
        else
        {
            
            NSArray *arrResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (arrResponse.count==0 || arrResponse==nil)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Unable to generate workorder" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
                [DejalBezelActivityView removeView];
                [DejalActivityView removeView];
            }
            else
            {
                //Nilind 28 Dec
                NSLog(@"Save & Generate Work order Response >>%@",arrResponse);
                [self initialSetupStatusUpdate];
                //[self goToAppointment];
                
                //........
            }
        }
        
    }
    
    
    
}

- (IBAction)action_SaveGenrateWorkOrder:(id)sender
{
    [self endEditing];
    
    _btnSaveContinue.enabled = NO;
    
    [UIView animateWithDuration:3 animations:^{
        
    } completion:^(BOOL finished) {
        _btnSaveContinue.enabled = YES;
        
    }
     ];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Generating Workorder..."];
    
    [self performSelector:@selector(generateWorkorderAPI) withObject:nil afterDelay:0.0];
    
    /*
     _btnSaveGenerateWorkOrder.enabled = NO;
     
     [UIView animateWithDuration:3 animations:^{
     
     } completion:^(BOOL finished) {
     _btnSaveGenerateWorkOrder.enabled = YES;
     
     }
     ];
     
     NSString *errorMessage=[self validationCheck];
     if (errorMessage) {
     NSString *strTitle = Alert;
     [global AlertMethod:strTitle :errorMessage];
     [DejalBezelActivityView removeView];
     }
     else
     {
     [DejalBezelActivityView activityViewForView:self.view withLabel:@"Generating Work Order..."];
     
     
     [self performSelector:@selector(sendToServerSaveSetupAndGenerateWorkOrder) withObject:nil afterDelay:0.5];
     
     
     
     }*/
}


//============================================================================
//============================================================================
#pragma mark ---------------- Validation Check Method--------------------------
//============================================================================
//============================================================================

-(NSString *)validationCheck
{
    NSString *errorMessage;
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    for (int i=0; i<globalArrayDict.count; i++)
    {
        NSDictionary *dict=[globalArrayDict objectAtIndex:i];
        if ([[dict valueForKey:@"IsPrimary"] isEqualToString:@"true"])
        {
            [arr addObject:@"1"];
        }
        
    }
    
    if (arr.count==0)
    {
        
        errorMessage=@"Atlease one Primary Service must be selected";
        
    }
    else if (_txtServiceDate.text.length==0) {
        
        errorMessage=@"Please select date";
        
    }
    else if (_txtServiceTime.text.length==0) {
        
        errorMessage=@"Please select from time";
        
    }
    else if (_txtServiceToTime.text.length==0) {
        
        errorMessage=@"Please select to time";
        
    }
    else if([_txtServiceTime.text isEqualToString:_txtServiceToTime.text])
    {
        errorMessage=@"From time to time can not be same";
    }
    else if (arrSourceSelectedValue.count==0) {
        
        errorMessage=@"Please select Route";
        
    }
    else if (arrSourceSelectedValue.count>1) {
        
        errorMessage=@"Please select only one Route";
        
    }
    return errorMessage;
}

-(void)goToAppointmentOld{
    

    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppointmentView *objAppointmentView=[storyBoard instantiateViewControllerWithIdentifier:@"AppointmentView"];
    [self.navigationController pushViewController:objAppointmentView animated:NO];
    
}
-(void)goToAppointment
{
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Appointment" bundle:nil];
        AppointmentVC *objAppointmentView=[storyBoard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objAppointmentView animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];AppointmentView*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
#pragma mark- -----------------API FOR INITIAL SETUP STATUS--------------------------
//Nilind 30 Dec

-(void)initialSetupStatusUpdate
{
    //http://tsas.stagingsoftware.com//api/MobileToSaleAuto/UpdateLeadInitialSetup?leadId=675&IsInitialSetup=true
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        //[self salesFetch];
    }
    else
    {
        
        
        NSString *strUrl,*strInitialSetupStatus;
        strInitialSetupStatus=@"true";
        strUrl = [NSString stringWithFormat:@"%@%@%@&IsInitialSetup=%@",strSalesAutoMainUrl,UrlSalesInitialSetupStatusUpdate,strLeadId,strInitialSetupStatus];
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        NSLog(@"Sales Lead URl-----%@",strUrl);
        
        
        
        // NSURL *url = [NSURL URLWithString:strUrl];
        // NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        //        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        //        [request setHTTPBody: requestData];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSString *responseString    = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 NSLog(@"responseString %@",responseString);
                 [DejalBezelActivityView removeView];
                 [DejalActivityView removeView];
                 [self goToAppointment];
                 
             }
         }];
    }
}
-(void)leadTitleFetch
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAll = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesAll;
    if (arrAll.count==0)
    {
        
    }
    else
    {
        
        matchesAll=arrAll[0];
        _lblTitle.text=[NSString stringWithFormat:@"Opportunity #: %@, A/C #: %@",[matchesAll valueForKey:@"leadNumber"],[matchesAll valueForKey:@"accountNo"]];
        
        _lblNameTitle.text=[NSString stringWithFormat:@"%@ %@ %@",[matchesAll valueForKey:@"firstName"],[matchesAll valueForKey:@"middleName"],[matchesAll valueForKey:@"lastName"]];
        _lblTitle.font=[UIFont boldSystemFontOfSize:14];
        _lblNameTitle.font=[UIFont boldSystemFontOfSize:14];
        strTax=[NSString stringWithFormat:@"%@",[matchesAll valueForKey:@"tax"]];
        strCreatedBy=[NSString stringWithFormat:@"%@",[matchesAll valueForKey:@"createdBy"]];
        
        
    }
    
    
}
-(NSString *)modifyDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString* finalTime = [dateFormatter stringFromDate:[NSDate date]];
    return finalTime;
}
//.............................
-(void)fetchOneTime
{
    [self fetchSoldStandard];
    [self fetchSoldNonStandard];
}
-(void)fetchSoldStandard
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOnTimeSoldStandard=[[NSMutableArray alloc]init];
    NSManagedObject *matches21;
    
    if (arrAllObj12.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj12.count; k++)
        {
            matches21=arrAllObj12[k];
            NSLog(@"Lead IDDDD====%@",[matches21 valueForKey:@"leadId"]);
            if([[matches21 valueForKey:@"serviceFrequency"] isEqualToString:@"OneTime"]||[[matches21 valueForKey:@"serviceFrequency"] isEqualToString:@"One Time"])
                
            {
                [arrOnTimeSoldStandard addObject:[NSString stringWithFormat:@"%@",[dictServiceName valueForKey:[matches21 valueForKey:@"serviceSysName"]]]];
            }
        }
        
    }
    
}

-(void)fetchSoldNonStandard
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOneTimeSoldNonStandard=[[NSMutableArray alloc]init];
    if (arrAllObj12.count==0)
    {
    }else
    {
        
        NSManagedObject *matches21;
        for (int k=0; k<arrAllObj12.count; k++)
        {
            matches21=arrAllObj12[k];
            NSLog(@"Lead IDDDD====%@",[matches21 valueForKey:@"leadId"]);
            [arrOneTimeSoldNonStandard addObject:[matches21 valueForKey:@"serviceName"]];
        }
    }
}
-(void)removeFromView
{
    [_viewOneInitailService removeFromSuperview];
    [_viewDynamicForService removeFromSuperview];
    [_viewTotal removeFromSuperview];
    [_viewSetupDescription removeFromSuperview];
    [_viewThreeLockRout removeFromSuperview];
    [_viewSaveContinue removeFromSuperview];
}
-(void)addViews
{
    [_viewDynamicForService removeFromSuperview];
    tblData.tag=101;
    _viewOneInitailService.frame=CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width-10, _viewOneInitailService.frame.size.height);
    
    [_scrollView addSubview:_viewOneInitailService];
    
    //  _viewDynamicForService.frame=CGRectMake(_viewOneInitailService.frame.origin.x, CGRectGetMaxY(_viewOneInitailService.frame)+5,_viewOneInitailService.frame.size.width,arrSelectedOneTime.count*300);
    
    //  [_scrollView addSubview:_viewDynamicForService];
    
    if (arrSelectedOneTime.count==0)
    {
        chkOneTime=YES;
        
        
        _viewServiceDetail.frame=CGRectMake(_viewOneInitailService.frame.origin.x, CGRectGetMaxY(_viewOneInitailService.frame)+5,_viewOneInitailService.frame.size.width, _viewServiceDetail.frame.size.height);
        [_scrollView addSubview:_viewServiceDetail];
        
        _viewSetupDescription.frame=CGRectMake(_viewOneInitailService.frame.origin.x, CGRectGetMaxY(_viewServiceDetail.frame)+5,_viewOneInitailService.frame.size.width, _viewSetupDescription.frame.size.height);
        [_scrollView addSubview:_viewSetupDescription];
        
    }
    else
    {
        /* _viewDynamicForService.frame=CGRectMake(_viewOneInitailService.frame.origin.x, CGRectGetMaxY(_viewOneInitailService.frame)+5,_viewOneInitailService.frame.size.width,arrSelectedOneTime.count*300);
         
         [_scrollView addSubview:_viewDynamicForService];
         _viewTotal.frame=CGRectMake(_viewDynamicForService.frame.origin.x, CGRectGetMaxY(_viewDynamicForService.frame)+5,_viewDynamicForService.frame.size.width,_viewTotal.frame.size.height);
         
         [_scrollView addSubview:_viewTotal];*/
        
        _viewDynamicForService.frame=CGRectMake(_viewOneInitailService.frame.origin.x, CGRectGetMaxY(_viewOneInitailService.frame)+5,_viewOneInitailService.frame.size.width,arrSelectedOneTime.count*220);
        
        [_scrollView addSubview:_viewDynamicForService];
        
        _viewTotal.frame=CGRectMake(_viewDynamicForService.frame.origin.x, CGRectGetMaxY(_viewDynamicForService.frame)+5,_viewDynamicForService.frame.size.width,_viewTotal.frame.size.height);
        
        [_scrollView addSubview:_viewTotal];
        
        _viewServiceDetail.frame=CGRectMake(_viewOneInitailService.frame.origin.x, CGRectGetMaxY(_viewTotal.frame)+5,_viewOneInitailService.frame.size.width, _viewServiceDetail.frame.size.height);
        [_scrollView addSubview:_viewServiceDetail];
        
        
        _viewSetupDescription.frame=CGRectMake(_viewDynamicForService.frame.origin.x, CGRectGetMaxY(_viewServiceDetail.frame)+5,_viewTotal.frame.size.width, _viewSetupDescription.frame.size.height);
        [_scrollView addSubview:_viewSetupDescription];
        
    }
    // _viewSetupDescription.frame=CGRectMake(_viewDynamicForService.frame.origin.x, CGRectGetMaxY(_viewDynamicForService.frame)+5,_viewOneInitailService.frame.size.width, _viewSetupDescription.frame.size.height);
    
    
    // [_scrollView addSubview:_viewSetupDescription];
    
    
    _viewThreeLockRout.frame=CGRectMake(_viewSetupDescription.frame.origin.x, CGRectGetMaxY(_viewSetupDescription.frame)+5,_viewSetupDescription.frame.size.width, _viewThreeLockRout.frame.size.height);
    
    [_scrollView addSubview:_viewThreeLockRout];
    
    
    _viewSaveContinue.frame=CGRectMake(_viewThreeLockRout.frame.origin.x, CGRectGetMaxY(_viewThreeLockRout.frame)+5,_viewThreeLockRout.frame.size.width, _viewSaveContinue.frame.size.height);
    
    [_scrollView addSubview:_viewSaveContinue];
    
    
    [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewSaveContinue.frame)+5+_viewSaveContinue.frame.size.height+150)]   ;
    
}

/*-(void)textFieldDidEndEditing:(UITextField *)textField
 {
 
 NSInteger row = textField.tag;
 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
 
 DynamicViewTableViewCell *tappedCell = (DynamicViewTableViewCell *)[_tblViewDynamic cellForRowAtIndexPath:indexPath];
 
 if(textField==tappedCell.txtServicePrice)
 {
 NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
 dict=[globalArrayDict objectAtIndex:row];
 NSLog(@"globalArrayDict>>>.%@",globalArrayDict);
 //dict=tempDict;
 [dict setObject:tappedCell.txtServicePrice.text forKey:@"price"];
 [dict setObject:tappedCell.txtProductionValue.text forKey:@"productionValue"];
 NSLog(@"globalArrayDict>>>>>>>>>%@",globalArrayDict);
 // [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"duration"]] forKey:@"duration"];
 // [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]] forKey:@"SysName"];
 
 // [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"quantity"]] forKey:@"quantity"];
 //[dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"primary"]] forKey:@"primary"];
 // [dict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"tax"]] forKey:@"tax"];
 //[globalArrayDict replaceObjectAtIndex:getIndex.row withObject:dict];
 [self calculateTotal];
 }
 
 
 }*/

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger row = textField.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    DynamicViewTableViewCell *tappedCell = (DynamicViewTableViewCell *)[_tblViewDynamic cellForRowAtIndexPath:indexPath];
    
    if(textField==tappedCell.txtServicePrice)
    {
        
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        dict=[globalArrayDict objectAtIndex:row];
        NSLog(@"globalArrayDict>>>.%@",globalArrayDict);
        if([string isEqualToString:@""])
        {
            NSString *str1 =tappedCell.txtServicePrice.text;
            
            NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
            [dict setObject:newString forKey:@"price"];
            [dict setObject:newString forKey:@"productionValue"];
        }
        else
        {
            [dict setObject:[NSString stringWithFormat:@"%@%@",tappedCell.txtServicePrice.text,string] forKey:@"price"];
            [dict setObject:[NSString stringWithFormat:@"%@%@",tappedCell.txtServicePrice.text,string] forKey:@"productionValue"];
            
        }
        
        // [dict setObject:tappedCell.txtProductionValue.text forKey:@"productionValue"];
        
        NSLog(@"globalArrayDict>>>>>>>>>%@",globalArrayDict);
        [_tblViewDynamic reloadData];
        [tappedCell.txtServicePrice canBecomeFirstResponder];
        
        [self calculateTotal];
    }
    if (textField==_txtTax)
    {
        if([string isEqualToString:@""])
        {
            NSString *str1 =_txtTax.text;
            NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
            string=[NSString stringWithFormat:@"%@%@",newString,string];
            
        }
        else
        {
            string=[NSString stringWithFormat:@"%@%@",_txtTax.text,string];
        }
        [self caluclateTax:string];
    }
    return  YES;
}
-(void)calculateTotal
{
    double totalPrice,totalDuration,totalProductionValue,totalTax,grandTotal,taxTotal;
    totalPrice=0;totalProductionValue=0;totalDuration=0;totalTax=0,grandTotal=0;taxTotal=0;
    strDuration=@"00:00";
    
    for (int i=0; i<globalArrayDict.count; i++)
    {
        NSDictionary *dict=[globalArrayDict objectAtIndex:i];
        totalPrice=totalPrice+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"price"]] doubleValue];
        
        totalProductionValue=totalPrice+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"price"]] doubleValue];
        
        _txtTotalDuration.text= [self addTime:strDuration : [NSString stringWithFormat:@"%@",[dict valueForKey:@"duration"]]];
        
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"tax"]] isEqualToString:@"true"])
        {
            taxTotal=taxTotal+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"price"]] doubleValue];
            totalTax=totalTax+taxTotal*([strTax doubleValue]/100);
        }
        
    }
    grandTotal=grandTotal+totalPrice+totalTax;
    _txtTotal.text=[NSString stringWithFormat:@"%.2f",totalPrice];
    _txtTax.text=[NSString stringWithFormat:@"%.2f",totalTax];
    _txtGrandTotal.text=[NSString stringWithFormat:@"%.2f",grandTotal];
    //_txtTotalDuration.text=[NSString stringWithFormat:@"%@",strDuration];
    
}
-(void)caluclateTax:(NSString*)val
{
    double tax,total;
    tax=0;
    tax = [[NSString stringWithFormat:@"%@",val] doubleValue];
    
    total= [[NSString stringWithFormat:@"%@",_txtTotal.text] doubleValue]+tax;//*(tax/100);
    _txtGrandTotal.text=[NSString stringWithFormat:@"%.2f",total];
    //[[NSString stringWithFormat:@"%@",_txtTotal.text] doubleValue]+
    
}

#pragma mark- ******************* DATE TIME CALCULATION *******************

-(NSString*) addTime :(NSString*)strDate1 : (NSString *)strDate2
{
    
    NSMutableArray *recordedArray ;//;= [[NSMutableArray alloc] initWithObjects:strDate1,strDate2,nil];
    recordedArray=[[NSMutableArray alloc]init];
    [recordedArray addObject:strDate1];
    [recordedArray addObject:strDate2];
    
    captureAllSecondsArray = [[NSMutableArray alloc] init];// Define NSMutableArray * captureAllSecondsArray in your .h file.
    
    for (int i = 0; i<[recordedArray count]; i++)
    {
        [captureAllSecondsArray addObject:[self dateToSecondConvert:[recordedArray objectAtIndex:i]]];
    }
    
    
    int sumOfArray = 0;
    for (NSNumber * n in captureAllSecondsArray)
    {
        sumOfArray += [n intValue];
    }
    //int sumOfArray = 0;
    //sumOfArray =[strDate1 intValue]+[strDate2 intValue];
    
    NSString *iCanUseThis= [NSString stringWithFormat:@"%@", [self timeFormatted:sumOfArray]];
    strDuration=iCanUseThis;
    NSLog(@"Sum of array time in HH:mm:ss is--> %@",iCanUseThis);
    return iCanUseThis;
    
    
    
}

- (NSNumber *)dateToSecondConvert:(NSString *)string
{
    
    NSArray *components = [string componentsSeparatedByString:@":"];
    
    NSInteger hours   = [[components objectAtIndex:0] integerValue];
    NSInteger minutes = [[components objectAtIndex:1] integerValue];
    // NSInteger seconds = [[components objectAtIndex:2] integerValue];
    
    NSNumber *secondsNumber = [NSNumber numberWithInteger:(hours * 60 * 60) + (minutes * 60) + 0];
    
    return secondsNumber;
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    
    //int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    // NSString *totalTime = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];//seconds
    NSString *totalTime = [NSString stringWithFormat:@"%02d:%02d",hours, minutes];
    
    NSLog(@"TOTAL TIME%@",totalTime);
    return totalTime;
}
#pragma mark- *************** API FOR GENERATE WORK ORDER ***************
-(void)generateWorkorderAPI
{
    //[self performSelector:@selector(generateWorkOrder) withObject:nil afterDelay:0.5];
    NSString *errorMessage=[self validationCheck];
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Generating Work Order..."];
        
        [self performSelector:@selector(generateWorkOrder) withObject:nil afterDelay:0.5];
        
    }
    
}
-(void)generateWorkOrder
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
        
    }
    else
    {
        
        NSString *strTargetId = [arrSelectedTargetId componentsJoinedByString:@","];
        NSString *strAttributedId = [arrSelectedAttributeId componentsJoinedByString:@","];
        NSString *strUrl,*strSetupInstruction,*strInitialServiceInstruction,*strPreferableTime,*strNonPreferableTime,*strPrimaryServiceSysName,*strRouteId,*strInitialServiceSysNames,*strAccountNoss,*strLeadNumber;
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,@"api/WorkOrder/AddOneTimeWorkOrderForMobile"];
        //tcr.stagingsoftware.com/api/WorkOrder/AddUpdate
        NSArray *key,*value;
        
        
        strRouteId=arrRoutSelectedId[0];
        
        if (strInitialServiceSysNames.length==0) {
            strInitialServiceSysNames=@"";
        }
        if (strRouteId.length==0) {
            strRouteId=@"";
        }
        
        if (strPrimaryServiceSysName.length==0) {
            strPrimaryServiceSysName=@"";
        }
        if (strTargetId.length==0)
        {
            strTargetId=@"";
            
        }
        if (strAttributedId.length==0)
        {
            strAttributedId=@"";
        }
        NSUserDefaults *defsAccountNo=[NSUserDefaults standardUserDefaults];
        strAccountNoss=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"AccountNos"]];
        strLeadNumber=[NSString stringWithFormat:@"%@",[defsAccountNo valueForKey:@"LeadNumber"]];
        
        if (strAccountNoss.length==0 || [strAccountNoss isEqualToString:@"(null)"]) {
            strAccountNoss=@"";
        }
        if (strLeadNumber.length==0 || [strLeadNumber isEqualToString:@"(null)"]) {
            strLeadNumber=@"";
        }
        
        NSMutableArray *arrDictSend=[[NSMutableArray alloc]init];
        for (int i=0; i<globalArrayDict.count; i++)
        {
            NSDictionary *dict=[globalArrayDict objectAtIndex:i];
            NSMutableDictionary *dictLocal=[[NSMutableDictionary alloc]init];
            
            [dictLocal setObject:[dict valueForKey:@"SysName"] forKey:@"ServiceSysName"];
            [dictLocal setObject:[dict valueForKey:@"ServiceName"] forKey:@"ServiceName"];
            [dictLocal setObject:[dict valueForKey:@"quantity"] forKey:@"Quantity"];
            [dictLocal setObject:[dict valueForKey:@"IsPrimary"] forKey:@"IsPrimary"];
            [dictLocal setObject:[dict valueForKey:@"duration"] forKey:@"EstimatedTimeReq"];
            [dictLocal setObject:[dict valueForKey:@"IsStandard"] forKey:@"IsStandard"];
            [dictLocal setObject:[dict valueForKey:@"tax"] forKey:@"IsTaxable"];
            [arrDictSend addObject:dictLocal];
            
        }
        
        key=[NSArray arrayWithObjects:
             @"ServiceDate",
             @"companykey",
             @"AccountDescription",
             @"OtherInstruction",
             @"ServiceStartEndTime",
             @"ServicesTarget",
             @"RouteMasterId",
             @"IsActive",
             @"LeadNumber",// @"LeadNo",
             @"TaxPercent",
             @"IsInitial",
             @"FrequencySysName",
             @"ServiceInstructions",
             @"InternalInstruction",
             @"AccountNumber",
             @"ModifiedBy",//EMP NO
             @"TaxAmount",
             @"LockTime",
             @"ModifiedDate",
             @"InvoiceAmount", //GRAND TOTAL
             @"SpecialInstructions",
             @"ServicesAttribute",
             @"IsAutoGenerated",
             @"CreatedBy",
             @"CreatedDate",
             @"ServiceStartStartTime",
             @"LockTechnician",
             @"Directions",
             @"WorkOrderServices", // ARRAY OF DICTIONARY
             nil];
        
        value=[NSArray arrayWithObjects:
               _txtServiceDate.text, //ServiceDate
               strCompanyKey,  //companykey
               @"",  //AccountDescription
               _txtViewOtherInstruction.text, //OtherInstruction
               _txtServiceToTime.text, //ServiceStartEndTime
               strTargetId, //ServicesTarget
               strRouteId,//RouteMasterId
               @"true",//isActive
               strLeadNumber, //LeadNumber
               strTax, //TaxPercent
               @"false",//IsInitial
               @"OneTime",//FrequencySysName
               _txtViewServiceInstruction.text, //ServiceInstructions
               @"", //InternalInstruction
               strAccountNoss,//accountno
               strEmployeeNo,//modified by //STRcREATEDBY
               _txtTax.text, //TaxAmount
               isLockTime, //LockTime
               [global modifyDate] , //ModifiedDate
               _txtGrandTotal.text, //InvoiceAmount
               _txtViewSpecialInstruction.text, //SpecialInstructions
               strAttributedId,//ServicesAttribute
               @"false",//IsAutoGenerated
               strCreatedBy,//CreatedBy
               _txtServiceDate.text,//CreatedDate
               _txtServiceTime.text,//ServiceStartStartTime
               islockRoute,//LockTechnician
               _txtViewDirections.text,//Directions
               arrDictSend,
               nil];
        
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"FINAL FORM JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            
            
        }
        else
        {
            [DejalBezelActivityView removeView];
            [DejalActivityView removeView];
            NSString *string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"RESPONSE FOR WORK ORDER %@",string);
            if(string.length==0 || [string isEqualToString:@""] || [string isEqual:nil])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Unable to generate workorder" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
                [DejalBezelActivityView removeView];
                [DejalActivityView removeView];
            }
            else
            {
                [self initialSetupStatusUpdate];
                
            }
            
            /*  NSArray *arrResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if (arrResponse.count==0 || arrResponse==nil)
             {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Unable to generate workorder" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [alert show];
             
             [DejalBezelActivityView removeView];
             [DejalActivityView removeView];
             }
             else
             {
             //Nilind 28 Dec
             NSLog(@"Save & Generate Work order Response >>%@",arrResponse);
             //[self initialSetupStatusUpdate];
             //[self goToAppointment];
             
             //........
             }*/
        }
        
    }
    
    
    
}

- (IBAction)actionOnServiceTarget:(id)sender
{
    [self endEditing];
    
    arrTargets=[[NSMutableArray alloc]init];
    NSMutableArray *name,*sysName;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasterss=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arr=[dictMasterss valueForKey:@"Targets"];
    for (int i=0; i<arr.count; i++)
    {
        NSDictionary *dict=[arr objectAtIndex:i];
        {
            [arrTargets addObject:dict];
        }
    }
    tblData.tag=60;
    [self tableLoad:tblData.tag];
    
}

- (IBAction)actionOnServiceAttribute:(id)sender
{
    [self endEditing];
    arrAttribute=[[NSMutableArray alloc]init];
    NSMutableArray *name,*sysName;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasterss=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arr=[dictMasterss valueForKey:@"Attributes"];
    for (int i=0; i<arr.count; i++)
    {
        NSDictionary *dict=[arr objectAtIndex:i];
        {
            [arrAttribute addObject:dict];
        }
    }
    tblData.tag=70;
    [self tableLoad:tblData.tag];
    
}
#pragma mark- Textview  Delegate

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        [_txtViewSetupInstruction resignFirstResponder];
        [_txtViewServiceInstruction resignFirstResponder];
        [_txtViewDirections resignFirstResponder];
        [_txtViewOtherInstruction resignFirstResponder];
        [_txtViewAccountInstruction resignFirstResponder];
        [_txtViewSpecialInstruction resignFirstResponder];
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}
-(void)endEditing
{
    [self.view endEditing:YES];
}
@end


