//
//  DynamicViewTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 06/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>///

@interface DynamicViewTableViewCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UITextField *txtService;
@property (weak, nonatomic) IBOutlet UITextField *txtQuantity;
@property (weak, nonatomic) IBOutlet UITextField *txtProductionValue;
@property (weak, nonatomic) IBOutlet UITextField *txtDuration;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimary;
@property (weak, nonatomic) IBOutlet UIButton *btnTax;
@property (weak, nonatomic) IBOutlet UIImageView *imgForPrimary;
@property (weak, nonatomic) IBOutlet UIImageView *imgForTax;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceName;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePrice;
@property (weak, nonatomic) IBOutlet UIButton *btnDuration;

@end
