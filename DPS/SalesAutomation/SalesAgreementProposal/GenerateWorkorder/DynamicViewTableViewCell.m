//
//  DynamicViewTableViewCell.m
//  DPS
//
//  Created by Rakesh Jain on 06/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "DynamicViewTableViewCell.h"

@implementation DynamicViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
