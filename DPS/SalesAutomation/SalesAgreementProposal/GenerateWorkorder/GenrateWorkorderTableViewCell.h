//
//  GenrateWorkorderTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 04/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenrateWorkorderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAccNo;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderNo;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderDate;
@end
