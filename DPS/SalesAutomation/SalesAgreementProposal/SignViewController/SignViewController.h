//
//  SignViewController.h
//  DemoTable
//
//  Created by Rakesh Jain on 08/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignViewController : UIViewController
// SIGNATURE CODE
@property (weak, nonatomic) IBOutlet UIView *viewSignature;
- (IBAction)actionOnSaveSign:(id)sender;
- (IBAction)actionOnClearSign:(id)sender;
- (IBAction)actionOnExitSign:(id)sender;
@property (nonatomic, strong) UIImageView *mySignatureImage;
@property (nonatomic, assign) CGPoint lastContactPoint1, lastContactPoint2, currentPoint;
@property (nonatomic, assign) CGRect imageFrame;
@property (nonatomic, assign) BOOL fingerMoved;
@property (weak, nonatomic) IBOutlet UILabel *lblInpectorName;
@property(weak,nonatomic)NSString *strType;
@property (weak, nonatomic) IBOutlet UILabel *lblPleaseSignBelow;

@end
