//
//  SalesTermsConditionVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 29/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//


import UIKit


class SalesTermsTableViewCellNew:UITableViewCell
{
    
    @IBOutlet weak var lblServiceName: UILabel!
    
    @IBOutlet weak var lblServiceTermsCondition: UILabel!
    
    @IBOutlet weak var txtTerms: UITextView!
}

class SalesTermsConditionVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var tblTermsCondition: UITableView!
    
    @objc  var arrGeneralTermsConditionName = NSMutableArray()
    @objc  var arrGeneralTermsConditionDesc = NSMutableArray()
    
    @objc var arrStandardServiceTermsConditionName = NSMutableArray()
    @objc var arrStandardServiceTermsConditionDesc = NSMutableArray()
    
    @objc var arrNonStandardServiceTermsConditionName = NSMutableArray()
    @objc var arrNonStandardServiceTermsConditionDesc = NSMutableArray()
    
    @objc var strFrom = String()
    
    var heisghtStanSerivce = Bool()
    var heisghtNonStanSerivce = Bool()


    override func viewDidLoad()
    {
        super.viewDidLoad()
        print("\(arrGeneralTermsConditionName)")
        print("\(arrStandardServiceTermsConditionName)")
        print("\(arrStandardServiceTermsConditionDesc)")
        print("\(arrNonStandardServiceTermsConditionName)")
        
        heisghtStanSerivce = true
        
        for item in arrStandardServiceTermsConditionDesc
        {
            let str = item as! String
            
            if str.count > 0
            {
                heisghtStanSerivce = false
                break
            }
        }
        
        heisghtNonStanSerivce = true
        
        for item in arrNonStandardServiceTermsConditionDesc
        {
            let str = item as! String
            
            if str.count > 0
            {
                heisghtNonStanSerivce = false
                break
            }
        }
        
        tblTermsCondition.reloadData()
    }
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    // MARK: -  ------------------------------ UITableview delegate and data source  ------------------------------

    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return arrGeneralTermsConditionName.count
        }
        else if section == 1
        {
            return arrStandardServiceTermsConditionName.count
        }
        else if section == 2
        {
            return arrNonStandardServiceTermsConditionName.count
        }
        else
        {
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalesTermsTableViewCellNew") as! SalesTermsTableViewCellNew
        
        if indexPath.section == 0
        {
            var strDesc = String()
            
            if strFrom == "Residential"
            {
                strDesc = "\(arrGeneralTermsConditionName.object(at: indexPath.row))"
                cell.lblServiceName.isHidden = true
            }
            else
            {
                let dict = arrGeneralTermsConditionName.object(at: indexPath.row) as! NSDictionary
                cell.lblServiceName.text = "\(dict.value(forKey: "TermsTitle") ?? "")"
                strDesc = "\(dict.value(forKey: "TermsConditions") ?? "")"
                cell.lblServiceName.isHidden = false
            }
            
            if strDesc.count == 0 || strDesc == ""
            {
                cell.lblServiceTermsCondition.text = " "
                //cell.txtTerms.text = " "
            }
            else
            {
                 
//                 let data = Data("\(strDesc)".utf8)
//
//                 if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                 {
//                     cell.lblServiceTermsCondition.attributedText = attributedString
//                 }
                
                cell.lblServiceTermsCondition.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)

 
            }
            
        }
        else if indexPath.section == 1
        {
            cell.lblServiceName.text = "\(arrStandardServiceTermsConditionName.object(at: indexPath.row))"
            
            let strDesc = "\(arrStandardServiceTermsConditionDesc.object(at: indexPath.row))"
            
            if strDesc.count == 0 || strDesc == ""
            {
                cell.lblServiceTermsCondition.text = " "
                cell.lblServiceName.isHidden = true
                cell.lblServiceTermsCondition.isHidden = true
            }
            else
            {
                
                cell.lblServiceName.isHidden = false
                cell.lblServiceTermsCondition.isHidden = false
                
//                let data = Data("\(strDesc)".utf8)
//
//                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                {
//                    cell.lblServiceTermsCondition.attributedText = attributedString
//                }
                
                cell.lblServiceTermsCondition.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)

                
            }
        }
        else if indexPath.section == 2
        {
            
            
            cell.lblServiceName.text = "\(arrNonStandardServiceTermsConditionName.object(at: indexPath.row))"
            
            let strDesc = "\(arrNonStandardServiceTermsConditionDesc.object(at: indexPath.row))"

            
            if strDesc.count == 0 || strDesc == ""
            {
                cell.lblServiceTermsCondition.text = " "
                
                cell.lblServiceName.isHidden = true
                cell.lblServiceTermsCondition.isHidden = true
            }
            else
            {
                cell.lblServiceName.isHidden = false
                cell.lblServiceTermsCondition.isHidden = false
                
//                let data = Data("\(strDesc)".utf8)
//
//                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                {
//                    cell.lblServiceTermsCondition.attributedText = attributedString
//                }
                
                cell.lblServiceTermsCondition.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)

                
            }
        }
        
        cell.lblServiceTermsCondition.numberOfLines = 500
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
        viewheader.backgroundColor = UIColor.darkGray//UIColor.init(red: 246/255, green: 246/255, blue: 246/255, alpha: 1)
        
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: viewheader.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
        if(section == 0)
        {
            lbl.text = "A. General Terms And Conditions:"
            lbl.textColor = UIColor.black
        }
        if(section == 1)
        {
            lbl.text = "B. Standard Service Terms And Conditions:"
            lbl.textColor = UIColor.black
        }
        else if(section == 2)
        {
            lbl.text = "C. Non Standard Service Terms And Conditions:"
            lbl.textColor = UIColor.black
        }
        
        lbl.textColor = UIColor.white
        
        lbl.font = UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 22 : 16)
        
        viewheader.addSubview(lbl)
        
        return viewheader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        if section == 0  //General Terms
        {
            if arrGeneralTermsConditionName.count > 0
            {
                return (DeviceType.IS_IPAD ? 50 : 40)
            }
            else
            {
                return 0
            }
        }
        else if section == 1  //Standard Service Terms
        {
            if arrStandardServiceTermsConditionName.count > 0 && heisghtStanSerivce == false
            {
                return (DeviceType.IS_IPAD ? 50 : 40)
            }
            else
            {
                return 0
            }
        }
        else if section == 2  //Non Standard Service Terms
        {
            if arrNonStandardServiceTermsConditionName.count > 0 && heisghtNonStanSerivce == false
            {
                return (DeviceType.IS_IPAD ? 50 : 40)
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return UITableView.automaticDimension
        }
        else if indexPath.section == 1
        {
          /* let strDesc = "\(arrStandardServiceTermsConditionDesc.object(at: indexPath.row))"
            
            if strDesc.count == 0 || strDesc == ""
            {
                return 0

            }
            else
            {
                return UITableView.automaticDimension

            }*/
            return UITableView.automaticDimension
        }
        else if indexPath.section == 2
        {
            /*let strDesc = "\(arrNonStandardServiceTermsConditionDesc.object(at: indexPath.row))"
            
            if strDesc.count == 0 || strDesc == ""
            {
                return 0

            }
            else
            {
                return UITableView.automaticDimension

            }*/
            return UITableView.automaticDimension
        }
        else
        {
            return 0
        }
    }

}
