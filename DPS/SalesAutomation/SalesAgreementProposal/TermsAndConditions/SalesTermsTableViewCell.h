//
//  SalesTermsTableViewCell.h
//  DPS
//
//  Created by Akshay Hastekar on 28/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SalesTermsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblServiceName;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceTermsCondition;

@end

NS_ASSUME_NONNULL_END
