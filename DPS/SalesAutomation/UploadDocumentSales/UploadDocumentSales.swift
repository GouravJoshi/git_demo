//
//  UploadDocumentSales.swift
//  DPS
//
//  Created by Nilind Sharma on 10/06/20.
//  Copyright © 2020 Saavan. All rights reserved.
//


import UIKit
import MobileCoreServices
import AVFoundation
import AVKit

class UploadDocumentSales: UIViewController{

    // MARK: - ----IBOutlet
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnAdd: UIButton!

    // MARK: - ----Variable
    
@objc var strWoId = NSString ()
    var aryList = NSMutableArray()
    var strTitle = String()
    var strType = String()
    var imagePicker = UIImagePickerController()
    var objWorkorderDetail = NSManagedObject()
    var arrOfAllData = NSArray()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var strAssociationType = String ()
    var strFrom = String ()
    var strConditionName = String ()
    var strConditionId = String ()
    var strURL = String()
    var strFromDeviceName = String()
    var strDeviceId = String ()
    var strMobileDeviceId = String ()
    
    // MARK: - ----Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        
        // Do any additional setup after loading the view.
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        

        
        if ("\(objWorkorderDetail.value(forKey: "statusSysName") ?? "")" == "Complete" && "\(objWorkorderDetail.value(forKey: "stageSysName") ?? "")" == "Won")
        {
            
            btnAdd.isHidden = true
            
        }
        

        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")//ServiceAutoModule
        {
            
            strURL = "\(value)"
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.fetchDataFrmDB()
        
        // AudioNameService  yesAudio
        
        if (nsud.value(forKey: "yesAudio") != nil) {
            
            let isAudio = nsud.bool(forKey: "yesAudio")
            
            if isAudio {
                
                //let audioName = nsud.value(forKey: "AudioNameService")
                
            }
            
        }
        
        // Dismiss ANd goto Invoice View
        if (nsud.value(forKey: "dismissView") != nil) {
            
            let isDismissView = nsud.bool(forKey: "dismissView")
            
            if isDismissView {
                
                self.back()
                
            }
            
        }
        
    }
    

    // MARK: - -----------------------------------Actions -----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        
        self.back()
        
    }

    @IBAction func action_Add(_ sender: Any) {
        

            
            self.goToSelectDocument()
            
        
        
    }
    
    // MARK: - -----------------------------------Functions -----------------------------------

    func fetchDataFrmDB()
    {
        
        arrOfAllData = getDataFromCoreDataBaseArray(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && docType == %@ && companyKey == %@", strWoId, "Other", Global().getCompanyKey()))
        
        tvlist.reloadData()
        
    }
    
    func back()  {
        
        dismiss(animated: false)
        
    }
    
    func goToAreaView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddAreaVC") as? AddAreaVC
        testController?.strWoId = strWoId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAddDeviceView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDeviceVC") as? AddDeviceVC
        testController?.strWoId = strWoId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToMaterialView(strForUpdate:String , strServiceProductId:String , strMobileServiceProductId:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddMaterialsVC") as? AddMaterialsVC
        testController?.strWoId = strWoId
        testController?.strForUpdate = strForUpdate
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strAssociationType = strAssociationType
        testController?.strServiceProductId = strServiceProductId
        testController?.strMobileServiceProductId = strMobileServiceProductId
        testController?.strDeviceId = strDeviceId
        testController?.strMobileDeviceId = strMobileDeviceId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToPestsView(strForUpdate:String , strServiceAddressPestId:String , strMobileServiceAddressPestId:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddPestsVC") as? AddPestsVC
        testController?.strWoId = strWoId
        testController?.strForUpdate = strForUpdate
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strAssociationType = strAssociationType
        testController?.strServiceAddressPestId = strServiceAddressPestId
        testController?.strMobileServiceAddressPestId = strMobileServiceAddressPestId
        testController?.strDeviceId = strDeviceId
        testController?.strMobileDeviceId = strMobileDeviceId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToConditionView(strForUpdate:String , strsaConduciveConditionId:String , strMobileSAConduciveConditionId:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddConditionsVC") as? AddConditionsVC
        testController?.strWoId = strWoId
        testController?.strForUpdate = strForUpdate
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strAssociationType = strAssociationType
        testController?.strServiceConditionId = strsaConduciveConditionId
        testController?.strMobileServiceConditionId = strMobileSAConduciveConditionId
        
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCommentView(strForUpdate:String , strServiceCommentId:String , strMobileServiceCommentId:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddCommentVC") as? AddCommentVC
        testController?.strWoId = strWoId
        testController?.strForUpdate = strForUpdate
        testController?.strAddressAreaId = strAddressAreaId
        testController?.strAddressAreaName = strAddressAreaName
        testController?.strAssociationType = strAssociationType
        testController?.strServiceCommentId = strServiceCommentId
        testController?.strMobileServiceCommentId = strMobileServiceCommentId
        if strFrom == "Condition" {
            
            testController?.strFrom = strFrom
            testController?.strConditionId = strConditionId
            testController?.strConditionCommentId = strServiceCommentId
            testController?.strMobileConditionCommentId = strMobileServiceCommentId
            
        }
        testController?.strDeviceId = strDeviceId
        testController?.strMobileDeviceId = strMobileDeviceId
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToRecordView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
        testController?.strFromWhere = "ServiceInvoice"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToSelectDocument()  {
        
        self.view.endEditing(true)
        
        if DeviceType.IS_IPAD
        {
            let storyboardIpad = UIStoryboard.init(name: "SalesMainiPad", bundle: nil)
                   let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDocumentSales_VCiPad") as? AddDocumentSales_VC
                   testController?.strWoId = strWoId as String
                   /*testController?.strFrom = strFrom
                   testController?.strAddressAreaId = strAddressAreaId
                   testController?.strAddressAreaName = strAddressAreaName
                   testController?.strConditionId = strConditionId
                   testController?.strAssociationType = strAssociationType*/
                           testController?.modalPresentationStyle = .fullScreen
                   self.present(testController!, animated: false, completion: nil)
        }
        else
        {
            let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
                   let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDocumentSales_VC") as? AddDocumentSales_VC
                   testController?.strWoId = strWoId as String
                   /*testController?.strFrom = strFrom
                   testController?.strAddressAreaId = strAddressAreaId
                   testController?.strAddressAreaName = strAddressAreaName
                   testController?.strConditionId = strConditionId
                   testController?.strAssociationType = strAssociationType*/
                           testController?.modalPresentationStyle = .fullScreen
                   self.present(testController!, animated: false, completion: nil)
        }
       
        
    }
    @objc func actionOnAddInAddendum(sender: UIButton!)
    {
        print("Button clicked")
        
        let dict = getMutableDictionaryFromNSManagedObject(obj: arrOfAllData.object(at: sender.tag) as! NSManagedObject)
        
         print("\(dict)")
        
        let strDocTypeLocal = getDocFormatType(strDocFileName: "\(dict.value(forKey: "fileName") ?? "")")
        
        if strDocTypeLocal.caseInsensitiveCompare("pdf") == .orderedSame
        {
           
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            var strCompanyKey = String()
            var strUserName = String()
            var strEmployeeid = String()
            var strIsAddendum = String()
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey")
            {
                
                strCompanyKey = "\(value)"
            }
            else
            {
                strCompanyKey = ""
            }
            
            if let value = dictLoginData.value(forKeyPath: "Company.Username")
            {
                
                strUserName = "\(value)"
            }
            else
            {
                strUserName = ""
            }
            
            if let value = dictLoginData.value(forKey: "EmployeeId")
            {
                
                strEmployeeid = "\(value)"
                
            }
            else
            {
                strEmployeeid = ""
            }
            
            // Update Code for Addendum Image
            
            strIsAddendum = "\(dict.value(forKey: "isAddendum") ?? "")"
            
            
            
            if sender.currentImage == UIImage(named: "check_box_2New.png")
            {
                sender.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
                strIsAddendum = "false"
            }
            else
            {
                sender.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
                strIsAddendum = "true"
            }
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("isAddendum")
            
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strEmployeeid)
            arrOfValues.add(Global().modifyDate())
            arrOfValues.add(strIsAddendum)
            
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && leadDocumentId == %@ && title == %@", strWoId, "\(dict.value(forKey: "leadDocumentId") ?? "")","\(dict.value(forKey: "title") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                tvlist.reloadData()
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Only pdf can be add for addendum document", viewcontrol: self)

        }
        
    }
    @objc func actionOnAddToInternal(sender: UIButton!)
    {
        print("Button clicked")
        
        let dict = getMutableDictionaryFromNSManagedObject(obj: arrOfAllData.object(at: sender.tag) as! NSManagedObject)
        
        print("\(dict)")
                
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String()
        var strIsAddToInternal = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey")
        {
            
            strCompanyKey = "\(value)"
        }
        else
        {
            strCompanyKey = ""
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username")
        {
            
            strUserName = "\(value)"
        }
        else
        {
            strUserName = ""
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId")
        {
            
            strEmployeeid = "\(value)"
            
        }
        else
        {
            strEmployeeid = ""
        }
        
        // Update Code for Addendum Image
        
        strIsAddToInternal = "\(dict.value(forKey: "isInternalUsage") ?? "")"
        
        
        
        if sender.currentImage == UIImage(named: "check_box_2New.png")
        {
            sender.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
            strIsAddToInternal = "false"
        }
        else
        {
            sender.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            strIsAddToInternal = "true"
        }
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("isInternalUsage")
        
        
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strEmployeeid)
        arrOfValues.add(Global().modifyDate())
        arrOfValues.add(strIsAddToInternal)
        
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && leadDocumentId == %@ && title == %@", strWoId, "\(dict.value(forKey: "leadDocumentId") ?? "")","\(dict.value(forKey: "title") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            tvlist.reloadData()
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
        
    }
    
    
    func goToImageView(image : UIImage) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
        testController!.img = image
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToPdfView(strPdfName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func playAudioo(strAudioName : String) {
       
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func getDocFormatType(strDocFileName: String) -> String
    {
        var strDocTypeLocal = String()
         
        let filename: NSString = strDocFileName as NSString
        let pathExtention = filename.pathExtension
        
        if pathExtention.caseInsensitiveCompare("jpg") == .orderedSame || pathExtention.caseInsensitiveCompare("png") == .orderedSame || pathExtention.caseInsensitiveCompare("gif") == .orderedSame || pathExtention.caseInsensitiveCompare("jpeg") == .orderedSame || pathExtention.caseInsensitiveCompare("tiff") == .orderedSame || pathExtention.caseInsensitiveCompare("jpg") == .orderedSame
        {
            strDocTypeLocal = "image"
        }
        else if pathExtention.caseInsensitiveCompare("pdf") == .orderedSame || pathExtention.caseInsensitiveCompare("doc") == .orderedSame || pathExtention.caseInsensitiveCompare("docx") == .orderedSame || pathExtention.caseInsensitiveCompare("xls") == .orderedSame || pathExtention.caseInsensitiveCompare("odt") == .orderedSame || pathExtention.caseInsensitiveCompare("ppt") == .orderedSame || pathExtention.caseInsensitiveCompare("txt") == .orderedSame || pathExtention.caseInsensitiveCompare("pptx") == .orderedSame || pathExtention.caseInsensitiveCompare("html") == .orderedSame || pathExtention.caseInsensitiveCompare("htm") == .orderedSame || pathExtention.caseInsensitiveCompare("ods") == .orderedSame || pathExtention.caseInsensitiveCompare("xlsx") == .orderedSame
        {
            strDocTypeLocal = "pdf"
        }
        else if pathExtention.caseInsensitiveCompare("mp4") == .orderedSame || pathExtention.caseInsensitiveCompare("mp3") == .orderedSame || pathExtention.caseInsensitiveCompare("wav") == .orderedSame || pathExtention.caseInsensitiveCompare("wma") == .orderedSame || pathExtention.caseInsensitiveCompare("aac") == .orderedSame || pathExtention.caseInsensitiveCompare("flac") == .orderedSame
        {
            strDocTypeLocal = "audio"
        }
        else if pathExtention.caseInsensitiveCompare("mp4") == .orderedSame || pathExtention.caseInsensitiveCompare("3gp") == .orderedSame || pathExtention.caseInsensitiveCompare("ogg") == .orderedSame || pathExtention.caseInsensitiveCompare("wmv") == .orderedSame || pathExtention.caseInsensitiveCompare("webm") == .orderedSame || pathExtention.caseInsensitiveCompare("flv") == .orderedSame || pathExtention.caseInsensitiveCompare("avi") == .orderedSame || pathExtention.caseInsensitiveCompare("mpeg") == .orderedSame
        {
            strDocTypeLocal = "video"
        }
        else
        {
            strDocTypeLocal = "video"
        }
        return strDocTypeLocal
    }

}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension UploadDocumentSales : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOfAllData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "DocumentListCell", for: indexPath as IndexPath) as! DocumentListCell
        
        let objDocument = arrOfAllData[indexPath.row] as! NSManagedObject
        
        var strDocPathLocal = String()
        var strDocTypeLocal = String()
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")//ServiceAutoModule
        {
            
            strURL = "\(value)"
            
        }
        
        
        cell.lblTitle.text = "\(objDocument.value(forKey: "title") ?? "N/A")"
        strDocPathLocal = "\(objDocument.value(forKey: "fileName") ?? "")"
        strDocTypeLocal = "\(objDocument.value(forKey: "docFormatType") ?? "")"
        
        strDocTypeLocal = getDocFormatType(strDocFileName: strDocPathLocal)

        cell.btnAddendum.tag = indexPath.row
        
        if ("\(objWorkorderDetail.value(forKey: "statusSysName") ?? "")" == "Complete" && "\(objWorkorderDetail.value(forKey: "stageSysName") ?? "")" == "Won")
        {
            cell.btnAddendum.isEnabled = false
        }
        
        if (("\(objDocument.value(forKey: "isAddendum") ?? "")".caseInsensitiveCompare("true") == .orderedSame) || ("\(objDocument.value(forKey: "isAddendum") ?? "")" == "1"))
        {
            cell.btnAddendum.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        }
        else
        {
            cell.btnAddendum.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
            
        }
        cell.btnAddendum.addTarget(self, action: #selector(actionOnAddInAddendum), for: .touchUpInside)
        
        //For Add To Internal Start
        cell.btnAddToInternal.tag = indexPath.row
        
        if ("\(objWorkorderDetail.value(forKey: "statusSysName") ?? "")" == "Complete" && "\(objWorkorderDetail.value(forKey: "stageSysName") ?? "")" == "Won")
        {
            cell.btnAddToInternal.isEnabled = false
        }
        
        if (("\(objDocument.value(forKey: "isInternalUsage") ?? "")".caseInsensitiveCompare("true") == .orderedSame) || ("\(objDocument.value(forKey: "isInternalUsage") ?? "")" == "1"))
        {
            cell.btnAddToInternal.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        }
        else
        {
            cell.btnAddToInternal.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
            
        }
        cell.btnAddToInternal.addTarget(self, action: #selector(actionOnAddToInternal), for: .touchUpInside)
        //End
        

        if strDocTypeLocal == "pdf"
        {
            cell.viewAddendum.isHidden = false
        }
        else
        {
            cell.viewAddendum.isHidden = false
        }
        // cell.viewAddendum.isHidden = true
        
        //            strDocTypeLocal = "audio"
        //            strDocTypeLocal = "image"
        //            strDocTypeLocal = "pdf"
        //image
        
        
        if strDocTypeLocal == "image" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if image != nil && strImagePath?.count != 0 && isImageExists! {
                
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                
                cell.imgView.image = image
                
                // kch nhi krna
                
            }
            else
            {
                
               /* if strDocPathLocal.contains("Documents")
                {
                    strURL = strURL + "uploadimages" + "\(strDocPathLocal)"
                }
                else
                {
                    strURL = strURL + "/Documents/uploadimages" + "\(strDocPathLocal)"
                }*/
                strURL = strURL + "/Documents/uploadimages/" + "\(strImagePath ?? "")"
                //strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgView.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath! , image: image)
                            
                            cell.imgView.image = image
                            
                        }}
                    
                }
                
            }
            
            
        } else if strDocTypeLocal == "pdf" {
            
            let image: UIImage = UIImage(named: "exam")!
            
            cell.imgView.image = image
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                // kch nhi krna
                
            }
            else
            {
                
                /*if strDocPathLocal.contains("Documents")
                {
                    strURL = strURL + "\(strDocPathLocal)"
                }
                else
                {
                    strURL = strURL + "/Documents/uploadimages" + "\(strDocPathLocal)"
                }*/
                //strURL = strURL + "\(strDocPathLocal)"
                strURL = strURL + "/Documents/UploadImages/" + "\(strImagePath ?? "")"

                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        DispatchQueue.main.async {
                            
                            savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                            
                        }}
                    
                }
                
            }
            
        } else if strDocTypeLocal == "audio" {
            
            let image: UIImage = UIImage(named: "audio")!
            
            cell.imgView.image = image
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                // kch nhi krna
                
            }else {
                
                
               /* if strDocPathLocal.contains("Documents")
                {
                    strURL = strURL + "\(strDocPathLocal)"
                }
                else
                {
                     strURL = strURL + "/Documents/" + "\(strDocPathLocal)"
                }*/
                
                //strURL = strURL + "\(strDocPathLocal)"
                strURL = strURL + "/Documents/uploadimages/" + "\(strImagePath ?? "")"

                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:self.strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        DispatchQueue.main.async {
                            
                            savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                            
                        }}
                    
                }
                
            }
            
        }else if strDocTypeLocal == "video" {
            
            let image: UIImage = UIImage(named: "video")!
            
            cell.imgView.image = image
            
        }
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        //documentType
        
        var strDocPathLocal = String()
        var strDocTypeLocal = String()
        
        let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
        strDocPathLocal = "\(objArea.value(forKey: "fileName") ?? "")"
        strDocTypeLocal = "\(objArea.value(forKey: "docFormatType") ?? "")"
        
        //            strDocTypeLocal = "audio"
        //            strDocTypeLocal = "image"
        //            strDocTypeLocal = "pdf"
        strDocTypeLocal = getDocFormatType(strDocFileName: strDocPathLocal)
        
        if strDocTypeLocal == "image" {
            
            let image1: UIImage = UIImage(named: "NoImage.jpg")!
            var imageView = UIImageView(image: image1)
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            if image != nil  {
                
                imageView = UIImageView(image: image)
                
            }else {
                
                imageView = UIImageView(image: image1)
                
            }
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        } else if strDocTypeLocal == "pdf" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            goToPdfView(strPdfName: strImagePath!)
            
            
        } else if strDocTypeLocal == "audio" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                
                playAudioo(strAudioName: strImagePath!)
                
            }else {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                
            }
            
        }else if strDocTypeLocal == "video" {
            let strVideoPath = Global().strDocName(fromPath: strDocPathLocal)
            
            
            let isVideoExists: Bool? = checkIfImageExistAtPath(strFileName: strVideoPath!)
            
            if isVideoExists!  {
                
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let documentsDirectory = paths[0]
                let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strVideoPath!)")
                print(path)
                let player = AVPlayer(url: path)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
                
            }else {
                
                var vedioUrl = strURL + "\(strDocPathLocal)"
                
                vedioUrl = vedioUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let player = AVPlayer(url: URL(string: vedioUrl)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
                
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        
        if ("\(objWorkorderDetail.value(forKey: "statusSysName") ?? "")" == "Complete" && "\(objWorkorderDetail.value(forKey: "stageSysName") ?? "")" == "Won")
        {
            
            return false
            
        }else{
            
            return true
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                
                
                let objArea = self.arrOfAllData[indexPath.row] as! NSManagedObject
                let strLeadDocumentId = objArea.value(forKey: "leadDocumentId") as! String
                let strDocPathLocal = objArea.value(forKey: "fileName") as! String
                
                    
                deleteAllRecordsFromDB(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && leadDocumentId == %@ && docType == %@ && fileName == %@", self.strWoId, strLeadDocumentId,"Other", strDocPathLocal))

                
                // deleting data from Document Directory also
                let strDocNameToRemove = Global().strDocName(fromPath: strDocPathLocal)
                
                removeImageFromDirectory(itemName: strDocNameToRemove!)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                self.fetchDataFrmDB()
                    
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
            })
            
        }
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            self.goToSelectDocument()
            
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [delete]
        
        
    }
    
}


// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension UploadDocumentSales : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        //let resizedImage = Global().resizeImageGlobal((info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!, "")
        
        //self.SaveImageToDb(image: (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!)
        
    }
}
