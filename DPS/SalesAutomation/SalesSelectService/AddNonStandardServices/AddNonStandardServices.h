//
//  AddNonStandardServices.h
//  DPS
//
//  Created by Rakesh Jain on 16/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UITextView+Placeholder.h"

@interface AddNonStandardServices : UIViewController<NSFetchedResultsControllerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail;
    
}
@property (weak, nonatomic) IBOutlet UIView *viewNonStandard;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;

@property (weak, nonatomic) IBOutlet UITextField *txtServiceName;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtInitialPriceNonStandard;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountNonStandard;
- (IBAction)actionOnAddNonStandard:(id)sender;
- (IBAction)actionOnCancelNonStandard:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNonStandardService;
- (IBAction)actionOnDepartment:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnDepartment;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_View_T;
@property (weak, nonatomic) IBOutlet UITextView *txtViewServiceDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountPer;
@property (weak, nonatomic) IBOutlet UIButton *btnBillingFrequency;
- (IBAction)actionOnBillingFrequency:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewTermsCondition;
@property (weak, nonatomic) IBOutlet UITextField *txtMaintenancePriceNonStan;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceFrequency;
- (IBAction)actionOnServiceFrequency:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnEditDescription;
- (IBAction)actionOnEditDescription:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEditTermsCondition;
- (IBAction)actionOnEditTermsCondition:(id)sender;
@property(strong,nonatomic) NSManagedObject *matchesServiceToBeEdited;
@property(weak, nonatomic)NSString *strForEdit,*strServiceIdForEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNew;

@end
