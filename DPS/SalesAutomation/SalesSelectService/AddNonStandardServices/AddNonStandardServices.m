//
//  AddNonStandardServices.m
//  DPS
//
//  Created by Rakesh Jain on 16/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "AddNonStandardServices.h"
#import "SoldServiceNonStandardDetail.h"
#import "Global.h"
#import "DPS-Swift.h"

typedef enum {
    descNonStan=1,
    termsNonStan,
    noType
    
} EditType;

@interface AddNonStandardServices ()
{
    NSString *strLeadId;
    Global *global;
    NSDictionary *dictSalesLeadMaster;
    UITableView *tblData;
    UIView *viewBackGround;
    NSMutableArray *arrDepartment;
    NSString *strDeptSysName;
    NSString *strBranchSysName;
    NSMutableArray *arrName,*arrSysName;
    NSString *strCompanyKey,*strEmployeeNo,*strUserName;
    BOOL isEditedInSalesAuto;
    UILabel *lbl;
    NSMutableArray *arrBillingFreq, *arrServiceFreq;
    NSDictionary *dictMasters,*dictDeptNameFromSysname;
    NSString *strBillingFreqSysName,*strServiceFreqSysName,*strServiceFreqName;
    
    EditType editType;
    
    NSString *strHtmlTermsConditionNonStan, *strHtmlDescriptionNonStan,*strSoldServiceNonStandId;
}
@end

@implementation AddNonStandardServices

- (void)viewDidLoad
{
    
    //Nilind 16 Nov
    isEditedInSalesAuto=NO;
    strSoldServiceNonStandId = @"";
    strServiceFreqSysName =@"";
    strBillingFreqSysName = @"";
    strServiceFreqName=@"";
    editType = descNonStan;
    strHtmlTermsConditionNonStan = @"";
    strHtmlDescriptionNonStan = @"";
    /* lbl=[[UILabel alloc]init];
     lbl.frame=CGRectMake(_txtInitialPriceNonStandard.frame.origin.x, _txtInitialPriceNonStandard.frame.origin.y-_txtInitialPriceNonStandard.frame.size.height+10, 200 , 30);
     lbl.font=[UIFont systemFontOfSize:12];
     lbl.text=@"Initial Price";
     [_viewNonStandard addSubview:lbl];*/
    
    NSUserDefaults *defsLogin=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLogin=[defsLogin valueForKey:@"LoginDetails"];
    
    strCompanyKey =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.CompanyKey"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeeNumber"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.Username"]];
    
    
    //.....................
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    dictMasters=[defs valueForKey:@"MasterSalesAutomation"];

   // _txtViewServiceDescription.text= @"Enter Description..!!";
   // _txtViewServiceDescription.textColor =[UIColor lightGrayColor];
    
    global = [[Global alloc] init];
    [self salesFetch];
    [super viewDidLoad];
    
    //Nilind 4 Oct
    arrDepartment=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    //NSDictionary *dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    
    /*NSArray *arrDept=[dictSalesLeadMaster valueForKey:@"DepartmentMasters"];
     for (int i=0; i<arrDept.count; i++)
     {
     NSDictionary *dict=[arrDept objectAtIndex:i];
     [arrDepartment addObject:[dict valueForKey:@"Name"]];
     }*/
    NSMutableArray *arrDept;
    
    arrName=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    arrDept=[[NSMutableArray alloc]init];
    NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    for (int i=0;i<arrDeptName.count; i++)
    {
        NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
        NSArray *arry=[dictBranch valueForKey:@"Departments"];
        for (int j=0; j<arry.count; j++)
        {
            NSDictionary *dict=[arry objectAtIndex:j];
            if ([strBranchSysName isEqualToString:[dictBranch valueForKey:@"SysName"]])
            {
                [arrDepartment addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                //                [arrName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [arrSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
            }
            
        }
        
    }
    dictDeptNameFromSysname = [NSDictionary dictionaryWithObjects:arrDepartment forKeys:arrSysName];

    // dictForDepartment = [NSDictionary dictionaryWithObjects:arrName forKeys:arrSysName];
    //............................
    
    
    
    //Dynamic Table
    
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.dataSource=self;
    tblData.delegate=self;
    
    //Nilind
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    
    //...............
    
    
    //.........................
    
    
    
    
    
    //borderl color
    _txtServiceName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceName.layer.borderWidth=1.0;
    
    
    _txtServiceDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServiceDescription.layer.borderWidth=1.0;
    
    _txtInitialPriceNonStandard.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtInitialPriceNonStandard.layer.borderWidth=1.0;
    
    _txtDiscountNonStandard.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtDiscountNonStandard.layer.borderWidth=1.0;
    
    _txtViewServiceDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewServiceDescription.layer.borderWidth=1.0;
    
    _txtMaintenancePriceNonStan.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtMaintenancePriceNonStan.layer.borderWidth=1.0;
    
    //Nilind 24 Jan
    
    _txtServiceName.layer.cornerRadius=5.0;
    _txtInitialPriceNonStandard.layer.cornerRadius=5.0;
    _txtDiscountNonStandard.layer.cornerRadius=5.0;
    _txtViewServiceDescription.layer.cornerRadius=5.0;
    
    _txtDiscountPer.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtDiscountPer.layer.borderWidth=1.0;
    _txtDiscountPer.layer.cornerRadius=5.0;
    _txtMaintenancePriceNonStan.layer.cornerRadius = 5.0;
    //End
    
    _viewNonStandard.layer.borderColor=[[UIColor colorWithRed:149.0f/255 green:158.0f/255 blue:174.0f/255 alpha:1] CGColor];
    
    _viewNonStandard.layer.borderWidth=1.0;
    _viewNonStandard.layer.cornerRadius=15.0;
    
    // _lblNonStandardService.hidden=YES;
    _lblNonStandardService.backgroundColor=[UIColor clearColor];
    //end
    
    // Do any additional setup after loading the view.
    //Dynamic Table
    
    [self fetchFromCoreData];
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        _const_View_T.constant=0;
    }
    [self setDefaultBillingFreq];
    
    _txtViewTermsCondition.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewTermsCondition.layer.borderWidth=1.0;
    _txtViewTermsCondition.layer.cornerRadius=5.0;
    _txtViewTermsCondition.placeholder = @"Enter Terms and Conditions";
    _txtViewTermsCondition.placeholderColor = [UIColor lightGrayColor]; // optional
    
    
    _txtViewServiceDescription.text = @"";
    _txtViewServiceDescription.placeholder = @"Enter Description";
    _txtViewServiceDescription.placeholderColor = [UIColor lightGrayColor]; //optional
    [self setTextCorner];
    
    _txtViewServiceDescription.editable = NO;
    _txtViewTermsCondition.editable = NO;
    
    if ([_strForEdit isEqualToString:@"EditNonStandard"])
    {
        editType = noType;
        [self editServiceDetail];
        [_btnAdd setTitle:@"Update" forState:UIControlStateNormal];
        //_btnAddNew.titleLabel.text = @"Update";

    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [self manageHtmlContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionOnAddNonStandard:(id)sender
{
    [self endEditing];
    if([_btnDepartment.titleLabel.text isEqualToString:@"--Select Department--"])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Select Department" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if([_btnServiceFrequency.titleLabel.text isEqualToString:@"--Select Service Frequency--"])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Select Service Frequency" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if([_btnBillingFrequency.titleLabel.text isEqualToString:@"--Select Billing Frequency--"])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Select Billing Frequency" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if(_txtServiceName.text.length==0)
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Service name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if([_txtViewServiceDescription.text isEqualToString:@"Enter Description..!!"])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter discription" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
    else if(_txtInitialPriceNonStandard.text.length==0)
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Initial price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    /* else if (_txtDiscountNonStandard.text.length==0)
     {
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Discount" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
     [alert show];
     }*/
    else if([_txtDiscountNonStandard.text longLongValue]>[_txtInitialPriceNonStandard.text longLongValue] )
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Discount can't be greater than Inital price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
    else
    {
        
        if ([_strForEdit isEqualToString:@"EditNonStandard"])
        {
            [self updateServiceDetails];
        }
        else
        {
            [self updateSoldServiceNonStandardDetail];
        }
        
        if (isEditedInSalesAuto==YES)
        {
            NSLog(@"Global modify date called in AddNonStandard");
            [global updateSalesModifydate:strLeadId];
        }
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setObject:_txtServiceName.text forKey:@"serviceNameNonStandard"];
        [defs setObject:_txtServiceDescription.text forKey:@"serviceDescriptionNonStandard"];
        [defs setObject:_txtInitialPriceNonStandard.text forKey:@"initailPriceNonStandard"];
        [defs setObject:_txtDiscountNonStandard.text forKey:@"discountNonStandard"];
        
        [defs setBool:YES forKey:@"backNonStandard"];
        [defs synchronize];
        
        _txtInitialPriceNonStandard.text=@"";
        _txtDiscountNonStandard.text=@"";
        _txtServiceName.text=@"";
        _txtServiceDescription.text=@"";
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
    
}

- (IBAction)actionOnCancelNonStandard:(id)sender
{
    [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"backNonStandard"];
    [defs synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark- 3 Aug
-(void)updateSoldServiceNonStandardDetail
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    isEditedInSalesAuto=YES;
    //**** For Lead Detail *****//
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    
    SoldServiceNonStandardDetail *objentitySoldServiceNonStandardDetail = [[SoldServiceNonStandardDetail alloc]initWithEntity:entitySoldServiceNonStandardDetail insertIntoManagedObjectContext:context];
    objentitySoldServiceNonStandardDetail.leadId=strLeadId;
    objentitySoldServiceNonStandardDetail.createdBy=@"";
    objentitySoldServiceNonStandardDetail.createdDate=@"";
    objentitySoldServiceNonStandardDetail.departmentSysname=strDeptSysName;
    objentitySoldServiceNonStandardDetail.discount=_txtDiscountNonStandard.text;
    
    objentitySoldServiceNonStandardDetail.discountPercentage=[self getNumberStringNew:_txtDiscountPer.text];
    
    objentitySoldServiceNonStandardDetail.initialPrice=_txtInitialPriceNonStandard.text;
    
    objentitySoldServiceNonStandardDetail.isSold=@"false";
    
    if ([_txtMaintenancePriceNonStan.text isEqualToString:@""]|| _txtMaintenancePriceNonStan.text.length == 0)
    {
        objentitySoldServiceNonStandardDetail.maintenancePrice= @"00.00";
        
    }
    else
    {
        objentitySoldServiceNonStandardDetail.maintenancePrice=_txtMaintenancePriceNonStan.text;
        
    }
    objentitySoldServiceNonStandardDetail.modifiedBy=@"";
    objentitySoldServiceNonStandardDetail.modifiedDate=[global modifyDate];
    objentitySoldServiceNonStandardDetail.modifiedInitialPrice=@"";
    objentitySoldServiceNonStandardDetail.modifiedMaintenancePrice=@"";
    objentitySoldServiceNonStandardDetail.serviceDescription= strHtmlDescriptionNonStan;//_txtViewServiceDescription.text;//_txtServiceDescription.text;
    objentitySoldServiceNonStandardDetail.serviceFrequency=strServiceFreqName;//@"";
    
    objentitySoldServiceNonStandardDetail.serviceName=_txtServiceName.text;
    objentitySoldServiceNonStandardDetail.soldServiceNonStandardId=@"" ;
    
    objentitySoldServiceNonStandardDetail.userName=strUserName;
    objentitySoldServiceNonStandardDetail.companyKey=strCompanyKey;
    
    objentitySoldServiceNonStandardDetail.billingFrequencyPrice=@"";
    objentitySoldServiceNonStandardDetail.billingFrequencySysName=strBillingFreqSysName;
    if([_txtViewTermsCondition.text isEqualToString:@"Enter Terms and Conditions"])
    {
        objentitySoldServiceNonStandardDetail.nonStdServiceTermsConditions=@"";
        
    }
    else
    {
        objentitySoldServiceNonStandardDetail.nonStdServiceTermsConditions=strHtmlTermsConditionNonStan;//_txtViewTermsCondition.text;
    }
    objentitySoldServiceNonStandardDetail.frequencySysName = strServiceFreqSysName;
    
    objentitySoldServiceNonStandardDetail.internalNotes=@"";
    objentitySoldServiceNonStandardDetail.isTaxable=@"true";
    objentitySoldServiceNonStandardDetail.soldServiceNonStandardId = [global getReferenceNumber];
    
    NSError *error1;
    [context save:&error1];
    
    [self fetchFromCoreData];
}
#pragma mark- 31 Aug Nilind
#pragma mark- FETCH CORE DATA
-(void)fetchFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        }
    }
}
#pragma mark- TEXTFIELD DELEGATE METHOD
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    /*
     
     if(textField==_txtInitialPriceNonStandard)
     {
     
     lbl.frame=CGRectMake(_txtInitialPriceNonStandard.frame.origin.x, _txtInitialPriceNonStandard.frame.origin.y-_txtInitialPriceNonStandard.frame.size.height+10, 200 , 30);
     lbl.font=[UIFont systemFontOfSize:12];
     lbl.text=@"Initial Price";
     }
     else if(textField==_txtDiscountNonStandard)
     {
     
     //[lbl removeFromSuperview];
     lbl.frame=CGRectMake(_txtDiscountNonStandard.frame.origin.x, _txtDiscountNonStandard.frame.origin.y-_txtDiscountNonStandard.frame.size.height+10, 150 , 30);
     lbl.font=[UIFont systemFontOfSize:12];
     lbl.text=@"Discount";
     //[_viewTextFiled addSubview:lbl];
     
     
     }
     else if(textField==_txtDiscountPer)
     {
     lbl.frame=CGRectMake(_txtDiscountPer.frame.origin.x, _txtDiscountPer.frame.origin.y-_txtDiscountPer.frame.size.height+10, 150 , 30);
     lbl.font=[UIFont systemFontOfSize:12];
     lbl.text=@"Discount %";
     // [_viewTextFiled addSubview:lbl];
     
     }
     */
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField==_txtDiscountNonStandard)
    {
        NSLog(@"TXT DISCOUNT RETURN");
        double initial,discountPer;
        initial=[_txtInitialPriceNonStandard.text doubleValue];
        discountPer=([_txtDiscountNonStandard.text doubleValue])*100/initial;
        //discountPer=round(discountPer*100)/100;
        _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
        
        // NSString *strPer=@"%";
        // _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f %@",discountPer,strPer];
    }
    if (textField==_txtDiscountPer)
    {
        NSLog(@"TXT DISCOUNT RETURN");
        double initial,discountPer;
        initial=[_txtInitialPriceNonStandard.text doubleValue];
        discountPer=(initial*[[self getNumberStringNew:_txtDiscountPer.text] doubleValue])/100;
        //discountPer=round(discountPer*100)/100;
        
        _txtDiscountNonStandard.text=[NSString stringWithFormat:@"%.2f",discountPer];
    }
    if (textField==_txtDiscountPer)
    {
        NSLog(@"TXT DISCOUNT PERCENT RETURN");
        
    }
    if (textField==_txtMaintenancePriceNonStan)
    {
        NSLog(@"Maintenance Price  RETURN");
        
    }
    
    [_txtDiscountPer resignFirstResponder];
    [_txtServiceName resignFirstResponder];
    [_txtServiceDescription resignFirstResponder];
    [_txtInitialPriceNonStandard resignFirstResponder];
    [_txtDiscountNonStandard resignFirstResponder];
    [_txtMaintenancePriceNonStan resignFirstResponder];
    return YES;
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_txtDiscountNonStandard)
    {
        NSLog(@"TXT DISCOUNT RETURN");
        double initial,discountPer;
        initial=[_txtInitialPriceNonStandard.text doubleValue];
        discountPer=([_txtDiscountNonStandard.text doubleValue])*100/initial;
        //discountPer=round(discountPer*100)/100;
        if (initial==0 || initial<0)
        {
            discountPer=0;
        }

        _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
        
        //NSString *strPer=@"%";
        // _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f %@",discountPer,strPer];
        
        
    }
    if (textField==_txtDiscountPer)
    {
        NSLog(@"TXT DISCOUNT RETURN");
        double initial,discountPer;
        initial=[_txtInitialPriceNonStandard.text doubleValue];
        discountPer=(initial*[[self getNumberStringNew:_txtDiscountPer.text] doubleValue])/100;
        //discountPer=round(discountPer*100)/100;
        
        _txtDiscountNonStandard.text=[NSString stringWithFormat:@"%.2f",discountPer];
        
        /*NSString *strPerNew=@"%";
         double discPer;
         discPer=[[self getNumberStringNew:_txtDiscountPer.text]doubleValue];
         _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f %@",discPer,strPerNew];*/
        
    }
    
    else

    {
        double initial,discountPer;
        initial=[_txtInitialPriceNonStandard.text doubleValue];
        discountPer=([_txtDiscountNonStandard.text doubleValue])*100/initial;
        //discountPer=round(discountPer*100)/100;
        if (initial==0 || initial<0)
        {
            discountPer=0;
        }

        _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
        
        //NSString *strPer=@"%";
        //_txtDiscountPer.text=[NSString stringWithFormat:@"%.2f %@",discountPer,strPer];
        
        /*NSString *strPerNew=@"%";
         double discPer;
         discPer=[[self getNumberStringNew:_txtDiscountPer.text]doubleValue];
         _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f %@",discPer,strPerNew];*/
    }
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==_txtDiscountNonStandard)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtDiscountNonStandard.text];
        
        if (isNuberOnly) {
            
            if ([string isEqualToString:@""])
            {
                double initial,discountPer;
                NSString *strDiscount;
                strDiscount=[_txtDiscountNonStandard.text stringByReplacingCharactersInRange:range withString:string];
                initial=[_txtInitialPriceNonStandard.text doubleValue];
                discountPer=([strDiscount doubleValue])*100/initial;
                if (initial==0 || initial<0)
                {
                    discountPer=0;
                }
                
                _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
            }
            else
            {
                double initial,discountPer;
                NSString *strDiscount;
                strDiscount=[NSString stringWithFormat:@"%@%@",_txtDiscountNonStandard.text,string];
                initial=[_txtInitialPriceNonStandard.text doubleValue];
                discountPer=([strDiscount doubleValue])*100/initial;
                if (initial==0 || initial<0)
                {
                    discountPer=0;
                }
                
                _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
            }
            return YES;
        } else {
            return NO;
        }
        
    }
    else if(textField==_txtDiscountPer)
    {
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtDiscountPer.text];
        
        if (isNuberOnly) {
            
            if ([string isEqualToString:@""])
            {
                double initial,discount;
                initial=[_txtInitialPriceNonStandard.text doubleValue];
                
                NSString *strDiscountPer;
                NSString *strText=_txtDiscountPer.text;
                strDiscountPer=[strText stringByReplacingCharactersInRange:range withString:string];
                
                discount=(initial*[strDiscountPer doubleValue])/100;
                _txtDiscountNonStandard.text=[NSString stringWithFormat:@"%.2f",discount];
                
            }
            else
            {
                double initial,discount;
                initial=[_txtInitialPriceNonStandard.text doubleValue];
                
                NSString *strDiscountPer;
                strDiscountPer=[NSString stringWithFormat:@"%@%@",_txtDiscountPer.text,string];
                
                
                discount=(initial*[strDiscountPer doubleValue])/100;
                _txtDiscountNonStandard.text=[NSString stringWithFormat:@"%.2f",discount];
            }
            return YES;
        } else {
            return NO;
        }
        
    }
    else if (textField==_txtInitialPriceNonStandard)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtInitialPriceNonStandard.text];
        
        if (isNuberOnly) {
            
            if ([string isEqualToString:@""])
            {
                double initial,discountPer;
                NSString *strInitial;
                strInitial=[_txtInitialPriceNonStandard.text stringByReplacingCharactersInRange:range withString:string];
                initial=[strInitial doubleValue];
                discountPer=([_txtDiscountNonStandard.text doubleValue])*100/initial;
                if (initial==0 || initial<0)
                {
                    discountPer=0;
                }
                
                _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
            }
            else
            {
                double initial,discountPer;
                NSString *strInitial;
                strInitial=[NSString stringWithFormat:@"%@%@",_txtInitialPriceNonStandard.text,string];
                initial=[strInitial doubleValue];
                discountPer=([_txtDiscountNonStandard.text doubleValue])*100/initial;
                if (initial==0 || initial<0)
                {
                    discountPer=0;
                }
                
                _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
                
            }
            return YES;
        } else {
            return NO;
        }
        
    }
    else if (textField==_txtMaintenancePriceNonStan)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtMaintenancePriceNonStan.text];
        
        if (isNuberOnly) {
            
            
            
            return YES;
        } else {
            return NO;
        }
    }
    
    return YES;
}
/*{
    if(textField==_txtDiscountNonStandard)
    {
        if ([string isEqualToString:@""])
        {
            double initial,discountPer;
            NSString *strDiscount;
            strDiscount=[_txtDiscountNonStandard.text stringByReplacingCharactersInRange:range withString:string];
            initial=[_txtInitialPriceNonStandard.text doubleValue];
            discountPer=([strDiscount doubleValue])*100/initial;
            if (initial==0 || initial<0)
            {
                discountPer=0;
            }

            _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
        }
        else
        {
            double initial,discountPer;
            NSString *strDiscount;
            strDiscount=[NSString stringWithFormat:@"%@%@",_txtDiscountNonStandard.text,string];
            initial=[_txtInitialPriceNonStandard.text doubleValue];
            discountPer=([strDiscount doubleValue])*100/initial;
            if (initial==0 || initial<0)
            {
                discountPer=0;
            }

            _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
        }
    }
    else if(textField==_txtDiscountPer)
    {
        if ([string isEqualToString:@""])
        {
            double initial,discount;
            initial=[_txtInitialPriceNonStandard.text doubleValue];
            
            NSString *strDiscountPer;
            NSString *strText=_txtDiscountPer.text;
            strDiscountPer=[strText stringByReplacingCharactersInRange:range withString:string];
            
            discount=(initial*[strDiscountPer doubleValue])/100;
            _txtDiscountNonStandard.text=[NSString stringWithFormat:@"%.2f",discount];
            
        }
        else
        {
            double initial,discount;
            initial=[_txtInitialPriceNonStandard.text doubleValue];
            
            NSString *strDiscountPer;
            strDiscountPer=[NSString stringWithFormat:@"%@%@",_txtDiscountPer.text,string];
            
            
            discount=(initial*[strDiscountPer doubleValue])/100;
            _txtDiscountNonStandard.text=[NSString stringWithFormat:@"%.2f",discount];
        }
    }
    else if (textField==_txtInitialPriceNonStandard)
    {
        if ([string isEqualToString:@""])
        {
            double initial,discountPer;
            NSString *strInitial;
            strInitial=[_txtInitialPriceNonStandard.text stringByReplacingCharactersInRange:range withString:string];
            initial=[strInitial doubleValue];
            discountPer=([_txtDiscountNonStandard.text doubleValue])*100/initial;
            if (initial==0 || initial<0)
            {
                discountPer=0;
            }

            _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
        }
        else
        {
            double initial,discountPer;
            NSString *strInitial;
            strInitial=[NSString stringWithFormat:@"%@%@",_txtInitialPriceNonStandard.text,string];
            initial=[strInitial doubleValue];
            discountPer=([_txtDiscountNonStandard.text doubleValue])*100/initial;
            if (initial==0 || initial<0)
            {
                discountPer=0;
            }

            _txtDiscountPer.text=[NSString stringWithFormat:@"%.2f",discountPer];
            
        }
    }
    
    
    return YES;
}*/

-(NSString*)getNumberStringNew:(NSString*)str
{
    NSString *value;
    NSString *strDiscount = [str stringByReplacingOccurrencesOfString:@"%" withString:@""];
    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    value=[NSString stringWithFormat:@"%@",strDiscount];
    double valueDiscount=[value doubleValue];
    value=[NSString stringWithFormat:@"%.2f",valueDiscount];
    return value;
}

- (IBAction)actionOnDepartment:(id)sender
{
    [self endEditing];
        //[self tableLoad];
        tblData.tag=10;
        [self tableLoad:tblData.tag];
   
    // [self tableLoad];
}
//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_txtDiscountPer resignFirstResponder];
    [_txtServiceName resignFirstResponder];
    [_txtServiceDescription resignFirstResponder];
    [_txtInitialPriceNonStandard resignFirstResponder];
    [_txtDiscountNonStandard resignFirstResponder];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
}
//============================================================================
#pragma mark- TABLEVIEW DELEGATE METHOD
//tblRecord Tag=0
//tblNonStandardService Tag=1
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==10)
    {
        return [arrDepartment count];
    }
    else if(tableView.tag==20)
    {
        return [arrBillingFreq count];
    }
    else if(tableView.tag==30)
    {
        return [arrServiceFreq count];
    }
    else
        
        return arrDepartment.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if(tableView.tag==10)
    {
        cell.textLabel.text=[arrDepartment objectAtIndex:indexPath.row];
        
    }
    
    else if(tableView.tag==20)
    {
        NSDictionary *dict=[arrBillingFreq objectAtIndex:indexPath.row];
        
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]];
    }
    else if(tableView.tag==30)
    {
        NSDictionary *dict=[arrServiceFreq objectAtIndex:indexPath.row];
        
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]];
    }
    
    
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==10)
    {
        [_btnDepartment setTitle:[arrDepartment objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        if (arrSysName.count==0)
        {
        }
        else
        {
            strDeptSysName=[NSString stringWithFormat:@"%@",[arrSysName objectAtIndex:indexPath.row]];
        }
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }
    
    else if (tableView.tag==20)
    {
        NSDictionary *dict=[arrBillingFreq objectAtIndex:indexPath.row];
        [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
        strBillingFreqSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }
    else if (tableView.tag==30)
    {
        NSDictionary *dict=[arrServiceFreq objectAtIndex:indexPath.row];
        [_btnServiceFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
        strServiceFreqSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        strServiceFreqName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]];

        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        [self forOneTimeFreq];
    }
    else
    {
        [_btnDepartment setTitle:[arrDepartment objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        if (arrSysName.count==0)
        {
        }
        else
        {
            strDeptSysName=[NSString stringWithFormat:@"%@",[arrSysName objectAtIndex:indexPath.row]];
        }
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }
}

-(void)tableLoad
{
    
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}

- (IBAction)actionOnBack:(id)sender
{
    [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"backNonStandard"];
    [defs synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
//Nilind 25 Oct
//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    // [self.view setFrame:CGRectMake(0,-150,self.view.frame.size.width,self.view.frame.size.height)];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        
        [_txtViewServiceDescription resignFirstResponder];
        [_txtViewTermsCondition resignFirstResponder];

        return NO;
    }
    
    /*if ([_txtViewServiceDescription.textColor isEqual:[UIColor lightGrayColor]])
    {
        _txtViewServiceDescription.text = @"";
        _txtViewServiceDescription.textColor =[UIColor blackColor];
    }*/
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    
    return YES;
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
   /* if (textView == _txtViewServiceDescription)
    {
        editType = descNonStan;
        NSLog(@"EditTpye is %u",editType);
        [self goToHtmlEditorView:strHtmlDescriptionNonStan];
    }
    if (textView == _txtViewTermsCondition)
    {
        editType = termsNonStan;
        NSLog(@"EditTpye is %u",editType);
        [self goToHtmlEditorView:strHtmlTermsConditionNonStan];
    }*/
    
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    // [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    /*if(textView==_txtViewServiceDescription)
    {
        if ([_txtViewServiceDescription.text isEqualToString:@""])
        {
            textView.text = @"Enter Description..!!";
            
            textView.textColor =[UIColor lightGrayColor];
        }
    }*/
    
    
}
//Nilind 14 Nov

#pragma mark - SALES FETCH
-(void)salesFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            strBranchSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"branchSysName"]];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        }
    }
}
- (IBAction)actionOnBillingFrequency:(id)sender
{
    [self endEditing];
    arrBillingFreq=[[NSMutableArray alloc]init];
    NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
    if (arrTempFreq.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Billing Frequency Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }
    else
    {
        for (int i=0; i<arrTempFreq.count;i++)
        {
            NSDictionary *dict=[arrTempFreq objectAtIndex:i];
            [arrBillingFreq addObject:dict];
            
        }
        /*tblData.tag=20;
         [self tableLoad:tblData.tag];*/
        
        if ([_btnServiceFrequency.currentTitle caseInsensitiveCompare:@"OneTime"]==NSOrderedSame||[_btnServiceFrequency.currentTitle caseInsensitiveCompare:@"One Time"]==NSOrderedSame)
        {
            arrBillingFreq=[[NSMutableArray alloc]init];
            for (int i=0; i<arrTempFreq.count;i++)
            {
                NSDictionary *dict=[arrTempFreq objectAtIndex:i];
                
                if ([[dict valueForKey:@"SysName"]isEqualToString:@"OneTime"]||[[dict valueForKey:@"SysName"]isEqualToString:@"oneTime"])
                {
                    [arrBillingFreq addObject:dict];
                }
            }
        }
        
        //02 Sept 2020
        
        NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
        arrTemp = [self getFrequencyAsPerType:arrBillingFreq FrequencyType:@"Billing"];
        arrBillingFreq = [[NSMutableArray alloc]init];
        [arrBillingFreq addObjectsFromArray:arrTemp];
        
        //End
        
        if (arrBillingFreq.count==0)
        {
            [global AlertMethod:@"Alert!" :@"No Billing Frequency Available"];
        }
        else
        {
            tblData.tag=20;
            [self tableLoad:tblData.tag];
        }
    }
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            [self setTableFrame:i];
            break;
        }
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
    
    [tblData scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}
-(void)setTableFrame:(NSInteger)btntag
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
    
}
-(void)setDefaultBillingFreq
{
    arrBillingFreq=[[NSMutableArray alloc]init];
    NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
    [arrBillingFreq addObjectsFromArray:arrTempFreq];
    for (int i=0; i<arrBillingFreq.count; i++)
    {
        NSDictionary *dict=[arrBillingFreq objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]] isEqualToString:@"Monthly"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]] isEqualToString:@"monthly"])
        {
            [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
            strBillingFreqSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
            break;
        }
    }
}
//...............

- (IBAction)actionOnServiceFrequency:(id)sender
{
    [self endEditing];
    arrServiceFreq=[[NSMutableArray alloc]init];
    NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
    
    if (arrTempFreq.count==0)
    {
       /* UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Billing Frequency Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];*/
        [global displayAlertController:@"Alert!" :@"No Service Frequency Available" :self];
        
    }
    else
    {
        for (int i=0; i<arrTempFreq.count;i++)
        {
            NSDictionary *dict=[arrTempFreq objectAtIndex:i];
            [arrServiceFreq addObject:dict];
            
        }
        
        //02 Sept 2020
        
        NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
        arrTemp = [self getFrequencyAsPerType:arrServiceFreq FrequencyType:@"Service"];
        arrServiceFreq = [[NSMutableArray alloc]init];
        [arrServiceFreq addObjectsFromArray:arrTemp];
        
        //End
        if (arrServiceFreq.count==0)
        {
            [global displayAlertController:@"Alert!" :@"No Service Frequency Available" :self];
        }
        else
        {
            tblData.tag=30;
            [self tableLoad:tblData.tag];
        }
    }
    
}
-(void)forOneTimeFreq
{
    if ([_btnServiceFrequency.currentTitle caseInsensitiveCompare:@"OneTime"]==NSOrderedSame||[_btnServiceFrequency.currentTitle caseInsensitiveCompare:@"One Time"]==NSOrderedSame)
    {
        
        arrBillingFreq=[[NSMutableArray alloc]init];
        NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
        if (arrTempFreq.count==0)
        {
            /*UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Billing Frequency Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [alert show];*/
            [global displayAlertController:@"Alert!" :@"No Billing Frequency Available" :self];
            
        }
        else
        {
            for (int i=0; i<arrTempFreq.count;i++)
            {
                NSDictionary *dict=[arrTempFreq objectAtIndex:i];
                [arrBillingFreq addObject:dict];
                
            }
            
            if ([_btnServiceFrequency.currentTitle caseInsensitiveCompare:@"OneTime"]==NSOrderedSame||[_btnServiceFrequency.currentTitle caseInsensitiveCompare:@"One Time"]==NSOrderedSame)
            {
                arrBillingFreq=[[NSMutableArray alloc]init];
                for (int i=0; i<arrTempFreq.count;i++)
                {
                    NSDictionary *dict=[arrTempFreq objectAtIndex:i];
                    
                    
                    if ([[dict valueForKey:@"SysName"]isEqualToString:@"OneTime"]||[[dict valueForKey:@"SysName"]isEqualToString:@"oneTime"])
                    {
                        [arrBillingFreq addObject:dict];
                        [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
                        strBillingFreqSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                        break;
                    }
                }
            }
            
        }
    }
    
}
-(void)endEditing
{
    [self.view endEditing:YES];
}
#pragma mark: ----------------- Frequency Type Setup ---------------

-(NSMutableArray *)getFrequencyAsPerType:(NSMutableArray *)arrFreq FrequencyType:(NSString *)strTypeFreq
{
    NSMutableArray *arrTempFreq = [[NSMutableArray alloc]init];
    
    for(int i=0;i<arrFreq.count;i++)
    {
        NSDictionary *dict = [arrFreq objectAtIndex:i];
        NSString *strType = [NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyType"]];
        if ([strType isEqualToString:strTypeFreq] || [strType isEqualToString:@"Both"])
        {
            [arrTempFreq addObject:dict];
        }
    }
    
    //Temp
    NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
    arrTemp = [self sortArrayByType:arrTempFreq Type:@"FrequencyName"];
    arrTempFreq = [[NSMutableArray alloc]init];
    [arrTempFreq addObjectsFromArray:arrTemp];
    //
    
    return arrTempFreq;
}

-(NSMutableArray *)sortArrayByType:(NSMutableArray *)arr Type:(NSString *)strType
{
     NSMutableArray *arrTemp = (NSMutableArray*)arr;
    
     NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"FrequencyName"
         ascending:YES];
     NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
     NSArray *sortedArray = [arrTemp sortedArrayUsingDescriptors:sortDescriptors];
     NSLog(@"%@",sortedArray);
    
    
    NSMutableArray *arrFinal = [[NSMutableArray alloc]init];
    [arrFinal addObjectsFromArray:sortedArray];
    
    return  arrFinal;
}
-(void)setTextCorner
{
    _txtInitialPriceNonStandard.layer.masksToBounds = YES;
    _txtMaintenancePriceNonStan.layer.masksToBounds = YES;
    _txtDiscountNonStandard.layer.masksToBounds = YES;
    _txtDiscountPer.layer.masksToBounds = YES;
    _txtServiceName.layer.masksToBounds = YES;
}

- (IBAction)actionOnEditDescription:(id)sender
{
    editType = descNonStan;
    NSLog(@"EditTpye is %u",editType);
    [self goToHtmlEditorView:strHtmlDescriptionNonStan];
    
}
- (IBAction)actionOnEditTermsCondition:(id)sender
{
    editType = termsNonStan;
    NSLog(@"EditTpye is %u",editType);
    [self goToHtmlEditorView:strHtmlTermsConditionNonStan];

}

-(void)manageHtmlContent
{
    
    NSUserDefaults *nsud = [NSUserDefaults standardUserDefaults];

    if ([nsud valueForKey:@"EditedHtmlContents"] != nil)
    {
        
        BOOL isScannedCode = [nsud boolForKey:@"EditedHtmlContents"];
        if (isScannedCode)
        {
            [nsud setBool:NO forKey:@"EditedHtmlContents"];
            [nsud synchronize];
        }
    }
    if ([nsud valueForKey:@"EditedHtmlContents"] != nil)
    {
        NSString *strHtmlContent;
        strHtmlContent = [NSString stringWithFormat:@"%@",[nsud valueForKey:@"htmlContents"]];
        
        NSAttributedString *attributedStringHTML = [[NSAttributedString alloc]
                                                initWithData: [strHtmlContent dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        
        
        if (editType == descNonStan)
        {
            _txtViewServiceDescription.attributedText = attributedStringHTML;
            strHtmlDescriptionNonStan = strHtmlContent;
        }
        else if (editType == termsNonStan)
        {
            _txtViewTermsCondition.attributedText = attributedStringHTML;
            strHtmlTermsConditionNonStan = strHtmlContent;
            
        }
        
        [nsud setValue:@"" forKey:@"htmlContents"];
        [nsud synchronize];
    }
    
}
-(void)goToHtmlEditorView: (NSString *)htmlString
{
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Appointment" bundle:nil];
    
    HTMLEditorVC *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"HTMLEditorVC"];
    
    objSalesAutomationAgreementProposal.strHtml=htmlString;
    
    objSalesAutomationAgreementProposal.strfrom=@"sales";
    objSalesAutomationAgreementProposal.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:objSalesAutomationAgreementProposal animated:NO completion:nil];
    //[self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:NO];
  
}
#pragma mark: ----------------- Edit Price ---------------

-(void)editServiceDetail
{
    if ([_strForEdit isEqualToString:@"EditNonStandard"])
    {
        [_btnDepartment setEnabled:NO];
        //[_txtServiceName setEnabled:NO];
        
        if ([_matchesServiceToBeEdited isKindOfClass:[NSManagedObject class]])
        {

            strDeptSysName=[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"departmentSysname"]];
            [_btnDepartment setTitle:[dictDeptNameFromSysname valueForKey:strDeptSysName] forState:UIControlStateNormal];
            
            _txtServiceName.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceName"]];
            
            strServiceFreqSysName = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"frequencySysName"]];
            
            strServiceFreqName = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceFrequency"]];
            [_btnServiceFrequency setTitle:strServiceFreqName forState:UIControlStateNormal];
            
            strSoldServiceNonStandId = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"soldServiceNonStandardId"]];
            strBillingFreqSysName = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"billingFrequencySysName"]];
            
            for (int i=0; i<arrBillingFreq.count;i++)
            {
                NSDictionary *dict = [arrBillingFreq objectAtIndex:i];
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]
                     isEqualToString:strBillingFreqSysName])
                {
                    [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]] forState:UIControlStateNormal];
                    break;
                }
            }
            
            
            _txtInitialPriceNonStandard.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"initialPrice"]];
            _txtMaintenancePriceNonStan.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"maintenancePrice"]];
          
            
            _txtDiscountNonStandard.text = [NSString stringWithFormat:@"%.2f",[[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"discount"]]floatValue]];
            
            
            _txtDiscountPer.text = [NSString stringWithFormat:@"%.2f",[[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"discountPercentage"]]floatValue]];
            
            
            if([[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceDescription"]] isEqualToString:@""] || [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceDescription"]].length == 0 )
            {
                _txtViewServiceDescription.text = @"";
                strHtmlDescriptionNonStan = @"";
            }
            else
            {
                strHtmlDescriptionNonStan = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceDescription"]];
                
                _txtViewServiceDescription.attributedText = [[NSAttributedString alloc]
                                            initWithData: [strHtmlDescriptionNonStan dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
                
                 
            }
            
            if([[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"nonStdServiceTermsConditions"]] isEqualToString:@""] || [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"nonStdServiceTermsConditions"]].length == 0 )
            {
                _txtViewTermsCondition.text = @"";
                strHtmlTermsConditionNonStan = @"";
            }
            else
            {
                strHtmlTermsConditionNonStan = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"nonStdServiceTermsConditions"]];
                
                _txtViewTermsCondition.attributedText =  [[NSAttributedString alloc]
                                                          initWithData: [strHtmlTermsConditionNonStan dataUsingEncoding:NSUnicodeStringEncoding]
                                                          options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                          documentAttributes: nil
                                                          error: nil
                                                          ];
            }
        }
    }

}
-(void)updateServiceDetails
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceNonStandardId=%@",strLeadId,strSoldServiceNonStandId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesServiceUpdate;
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matchesServiceUpdate=arrAllObj[i];
            
            [matchesServiceUpdate setValue:strDeptSysName forKey:@"departmentSysname"];
            
            [matchesServiceUpdate setValue:strServiceFreqName forKey:@"serviceFrequency"];
            
            [matchesServiceUpdate setValue:strServiceFreqSysName forKey:@"frequencySysName"];
            
            [matchesServiceUpdate setValue:strBillingFreqSysName forKey:@"billingFrequencySysName"];

            
            [matchesServiceUpdate setValue:_txtServiceName.text forKey:@"serviceName"];

            [matchesServiceUpdate setValue:_txtInitialPriceNonStandard.text forKey:@"initialPrice"];
            [matchesServiceUpdate setValue:_txtMaintenancePriceNonStan.text forKey:@"maintenancePrice"];
            [matchesServiceUpdate setValue:_txtDiscountNonStandard.text forKey:@"discount"];
            [matchesServiceUpdate setValue:_txtDiscountPer.text forKey:@"discountPercentage"];
            
            [matchesServiceUpdate setValue:strHtmlDescriptionNonStan forKey:@"serviceDescription"];
            [matchesServiceUpdate setValue:strHtmlTermsConditionNonStan forKey:@"nonStdServiceTermsConditions"];

        }
        [context save:&error1];
    }
}
@end
