//
//  SelectServiceCouponDiscountTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 27/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectServiceCouponDiscountTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCouponName;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountValue;
@property (weak, nonatomic) IBOutlet UILabel *lbCouponDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;

@end
