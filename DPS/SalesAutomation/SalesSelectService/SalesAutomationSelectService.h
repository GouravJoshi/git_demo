//
//  SalesAutomationSelectService.h
//  DPS Change
//
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SalesAutomationSelectService : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,UITextFieldDelegate,UIActionSheetDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,*entityLeadAppliedDiscounts, *entityRenewalSerivceDetail;
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnGlobalSync;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewLeads;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblRecord;
@property (weak, nonatomic) IBOutlet UITableView *tblNonStandardService;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTable_H;
- (IBAction)actionOnAdd:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cons_View_H;
@property (weak, nonatomic) IBOutlet UIView *viewForNonStandardService;
@property (weak, nonatomic) IBOutlet UIView *viewForStandardService;
@property (weak, nonatomic) IBOutlet UIButton *btnStandardService;
@property (weak, nonatomic) IBOutlet UIButton *btnNonStandardService;

- (IBAction)actionOnStandardService:(id)sender;

- (IBAction)actionOnNonStandardService:(id)sender;
- (IBAction)actionOnAddNonStandardService:(id)sender;
- (IBAction)actionOnCategory1:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory1;
- (IBAction)actionCategory2:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory2;
- (IBAction)actionOnCategory3:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory3;
- (IBAction)actionOnSaveContinue:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
#pragma mark- VIEWSTANDARD

@property (weak, nonatomic) IBOutlet UITextField *txtInitialPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscount;

@property (weak, nonatomic) IBOutlet UITextField *txtMaintenance;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenanceServiceStandard;
@property (weak, nonatomic) IBOutlet UILabel *lblBundleServiceStandard;

#pragma mark- VIEW NONSTANDARD
@property (weak, nonatomic) IBOutlet UITextField *txtServiceName;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtInitialPriceNonStandard;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountNonStandard;
@property (weak, nonatomic) IBOutlet UILabel *lblNameAcount;
- (IBAction)actionOnGlobalSync:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
- (IBAction)actionOnChkBoxDiscount:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxDiscount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Discount_H;
- (IBAction)actionOnDiscountCheckBox:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableStan_H;

//Nilind 08 June

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewDiscountNonStan_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableNonStan_H;
- (IBAction)actionOnCheckBoxNonStan:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxDiscountNonStan;

//View For Final Save
@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewService;

@property (weak, nonatomic) IBOutlet UIView *viewForFinalSave;

//View For After Image
@property (strong, nonatomic) IBOutlet UIView *viewForAfterImage;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAfterImage;
- (IBAction)actionOnAfterImgView:(id)sender;
- (IBAction)actionAddAfterImages:(id)sender;
- (IBAction)actionOnCancelAfterImage:(id)sender;

//View For Graph Image change
@property (strong, nonatomic) IBOutlet UIView *viewForGraphImage;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewGraphImage;
- (IBAction)actionOnAddGraphImage:(id)sender;

- (IBAction)actionOnCancelBeforeImage:(id)sender;
- (IBAction)actionOnGraphImageFooter:(id)sender;

#pragma mark- New Bundle Change
@property (weak, nonatomic) IBOutlet UITableView *tblBundle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTblBundle_H;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewStan_H;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;
@property (weak, nonatomic) IBOutlet UIButton *btnMarkAsLost;
- (IBAction)actionOnBtnMarkAsLost:(id)sender;


// discount
@property (weak, nonatomic) IBOutlet UITableView *tblCouponDiscount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableDiscount_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewDiscountCoupon_H;
- (IBAction)actionOnApplyDiscount:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnApplyDiscount;
@property (weak, nonatomic) IBOutlet UITextField *txtApplyDiscount;

- (IBAction)actionOnChemicalSensitivityList:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSendProposal;
- (IBAction)actionOnSendProposal:(id)sender;

//Renewal
@property (weak, nonatomic) IBOutlet UITableView *tblRenewalService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_tblRenewalService_H;
@property (weak, nonatomic) IBOutlet UIView *viewRenewalService;
@property (weak, nonatomic) IBOutlet UIButton *btnAddService;
- (IBAction)actionOnAddService:(id)sender;

@end
