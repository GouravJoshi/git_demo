//
//  SelectServiceCouponDiscountTableViewCell.m
//  DPS
//
//  Created by Rakesh Jain on 27/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "SelectServiceCouponDiscountTableViewCell.h"

@implementation SelectServiceCouponDiscountTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
