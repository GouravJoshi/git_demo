//
//  SalesAutomationServiceTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 28/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesAutomationServiceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblFrequency;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceType;

@property (weak, nonatomic) IBOutlet UILabel *lblInitialPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenanceValue;
- (IBAction)actionOnButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *btnImgStandardService;
@property (weak, nonatomic) IBOutlet UIButton *btnPlusService;
//temp

@property (weak, nonatomic) IBOutlet UILabel *lblServiceSysName;
@property (weak, nonatomic) IBOutlet UILabel *lblUnit;
@property (weak, nonatomic) IBOutlet UILabel *lblFinalInitialPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblFinalMaintPrice;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_viewUnit_H;
@property (weak, nonatomic) IBOutlet UITextField *txtStanDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingFrequency;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPercent;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountPercent;
@property (weak, nonatomic) IBOutlet UITextField *txtUnit;
@property (weak, nonatomic) IBOutlet UIView *viewForParameterPrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewParameter_H;

@end
