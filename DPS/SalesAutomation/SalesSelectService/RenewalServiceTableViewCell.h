//
//  RenewalServiceTableViewCell.h
//  DPS
//
//  Created by Nilind Sharma on 07/05/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RenewalServiceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblRenewalService;
@property (weak, nonatomic) IBOutlet UILabel *lblRenewalFrequency;
@property (weak, nonatomic) IBOutlet UITextField *txtRenewalPrice;
@property (weak, nonatomic) IBOutlet UITextView *txtViewRenewalServiceDesc;

@end

NS_ASSUME_NONNULL_END
