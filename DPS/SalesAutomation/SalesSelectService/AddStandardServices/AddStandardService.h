//
//  AddStandardService.h
//  DPS
//
//  Created by Rakesh Jain on 16/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AddStandardService : UIViewController<NSFetchedResultsControllerDelegate,UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,
    *entityRenewalServiceDetail,*entityLeadAppliedDiscounts;
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;

@property (weak, nonatomic) IBOutlet UIView *viewStandardServices;
@property (weak, nonatomic) IBOutlet UILabel *lblStandardService;

@property (weak, nonatomic) IBOutlet UIButton *btnCategory1;

@property (weak, nonatomic) IBOutlet UIButton *btnCategory2;

@property (weak, nonatomic) IBOutlet UIButton *btnCategory3;
@property (weak, nonatomic) IBOutlet UITextField *txtInitialPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscount;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountPercent;
@property (weak, nonatomic) IBOutlet UITextField *txtMaintenance;



#pragma mark- ACTION
- (IBAction)actionOnCategory1:(id)sender;
- (IBAction)actionOnCategory2:(id)sender;
- (IBAction)actionOnCategory3:(id)sender;
//........................................
- (IBAction)actionOnCategory:(id)sender;
- (IBAction)actionOnCategoryService:(id)sender;
- (IBAction)actionOnCategoryPackage:(id)sender;
- (IBAction)actionOnCategoryFrequency:(id)sender;
- (IBAction)actionOnAdd:(id)sender;

- (IBAction)actionOnCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory;
@property (weak, nonatomic) IBOutlet UIButton *btnService;
@property (weak, nonatomic) IBOutlet UIButton *btnPackage;
@property (weak, nonatomic) IBOutlet UIButton *btnFrequency;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constView_T;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Quantity_H;
@property (weak, nonatomic) IBOutlet UITextField *txtQuantity;
- (IBAction)actionOnAddBundle:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBillingFrequency;
- (IBAction)actionOnBillingFrequency:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewTextFiled;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ButtonPackage_H;

//Nilind 28 Sept 19
@property(weak, nonatomic)NSString *serviceType,*strForEdit,*strServiceIdForEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnAddBundle;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBundle;
@property (weak, nonatomic) IBOutlet UILabel *lblAddStandardService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Add_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_AddBundle_H;


@property (weak, nonatomic) IBOutlet UIView *viewBundle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewBundle_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewAddStandardService_H;

@property (weak, nonatomic) IBOutlet UIButton *btnSelectBundle;
- (IBAction)actionOnSelectBundle:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingFreqBundle;
- (IBAction)actionOnBillingFreqBundle:(id)sender;
- (IBAction)actionOnAddBundleNew:(id)sender;
@property(strong,nonatomic) NSManagedObject *matchesServiceToBeEdited;

@end
