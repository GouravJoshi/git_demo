//
//  SalesAutomationTableViewCellSecond.h
//  DPS
//
//  Created by Rakesh Jain on 29/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesAutomationTableViewCellSecond : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDepartmentValue;

@property (weak, nonatomic) IBOutlet UILabel *lblServiceType;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblInitialPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenanceValue;
@property (weak, nonatomic) IBOutlet UIButton *btnImageNonStandardService;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewNonStandardService;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountNonStan;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountPercent;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPercent;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingFreqValue;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceFreqValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintPriceValue;
@property (weak, nonatomic) IBOutlet UIButton *btnTaxableNonStan;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewTaxableNonStan;
@end
