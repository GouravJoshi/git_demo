//
//  AddPlusService.h
//  DPS
//
//  Created by Rakesh Jain on 07/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AddPlusService : UIViewController<UITextFieldDelegate,NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,*entityRenewalServiceDetail,*entityLeadAppliedDiscounts;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;

@property (weak, nonatomic) IBOutlet UIView *viewStandardServices;
@property (weak, nonatomic) IBOutlet UILabel *lblStandardService;


@property (weak, nonatomic) IBOutlet UITextField *txtInitialPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscount;

@property (weak, nonatomic) IBOutlet UITextField *txtMaintenance;


@property (weak, nonatomic) IBOutlet UIButton *btnService;
@property (weak, nonatomic) IBOutlet UIButton *btnPackage;
@property (weak, nonatomic) IBOutlet UIButton *btnFrequency;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

- (IBAction)actionOnBack:(id)sender;

- (IBAction)actionOnCategoryService:(id)sender;
- (IBAction)actionOnCategoryPackage:(id)sender;
- (IBAction)actionOnCategoryFrequency:(id)sender;

- (IBAction)actionOnAdd:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
@property(weak,nonatomic)NSDictionary *dictPlusService;
@property(strong,nonatomic)NSArray *arrPlusService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_View_T;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountPercent;
@property (weak, nonatomic) IBOutlet UIButton *btnBillingFrequency;
- (IBAction)actionOnBillingFrequency:(id)sender;
@property(strong,nonatomic)NSString *strFreqName,*strBillingFreqName,*strForEdit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Quantity_H;
@property (weak, nonatomic) IBOutlet UITextField *txtQuantity;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_BtnPackage_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_BtnPackage_T;
@property(strong,nonatomic) NSManagedObject *matchesServiceToBeEdited;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;


@end

