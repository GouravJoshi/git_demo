//
//  ElectronicAuthorizedForm+CoreDataClass.h
//  
//
//  Created by Akshay Hastekar on 01/05/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ElectronicAuthorizedForm : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ElectronicAuthorizedForm+CoreDataProperties.h"
