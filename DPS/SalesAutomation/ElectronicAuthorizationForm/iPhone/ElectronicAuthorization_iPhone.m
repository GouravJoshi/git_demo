//
//  ElectronicAuthorization_iPhone.m
//  DPS
//
//  Created by Akshay Hastekar on 08/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#define ACCEPTABLE_CHARACTERS @"0123456789"


#import "ElectronicAuthorization_iPhone.h"
#import "Global.h"
#import "AllImportsViewController.h"


@interface ElectronicAuthorization_iPhone ()

@end

@implementation ElectronicAuthorization_iPhone
{
    BOOL isAtFreqOfService;
    BOOL isEFTCheckingDraft;
    Global *global;
    AppDelegate *appDelegate;
    NSManagedObjectContext *context,*contextSubmitData;
    NSEntityDescription *entityLeadDetail;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSString *strEmployeeNo,*strLeadId;
    NSArray *arrAllObj;
    NSManagedObject *matches;
    NSDictionary *dictData;
    UIDatePicker *pickerDate;
    UIView *viewForDate,*viewBackGround;
    NSString *strDateString;
    NSString *strCustSignaturePath;
    BOOL isSignatureImage;
    NSDictionary *dictLeadDetails;
    NSString *strLoggedInID;
    NSString *strCompanyKey;
    BOOL isSignUpTermsAccepted;
    NSEntityDescription *entityElectronicAuthorizedForm;
    NSMutableArray *arrayState;
    UIButton *buttonBackground,*buttonOK;
    UITableView *tblData;
    NSDictionary *dictState;
    NSString *strAccountNumber;
    NSString *strLeadNumber,*strServiceUrlMainSales;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isAtFreqOfService = YES;
    isEFTCheckingDraft = YES;
    isSignUpTermsAccepted = NO;
    global = [[Global alloc]init];
    strAccountNumber = @"";
    strLeadNumber = @"";
    
    _heightMainContainerView.constant = _heightMainContainerView.constant-(_heightLabelDayOfMonth.constant+_heightTextViewTitle.constant+_heightTextViewPreNotes.constant+_heightTextViewTerms.constant+_heightTextViewPostNotes.constant);
    
    _heightLabelDayOfMonth.constant = 0.0;
    _heightTextViewTitle.constant = 0.0;
    _heightTextViewPreNotes.constant = 0.0;
    _heightTextViewTerms.constant = 0.0;
    _heightTextViewPostNotes.constant = 0.0;
    
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.dataSource=self;
    tblData.delegate=self;
    [self fetchJsonState];
    
    [self configureUI:_textViewPreNotes];
    [self configureUI:_textViewTerms];
    [self configureUI:_textViewPostNotes];
    [self configureUI:_textFieldFirstName];
    [self configureUI:_textFieldMiddleName];
    [self configureUI:_textFieldLastName];
    [self configureUI:_textFieldAddressLine1];
    [self configureUI:_textFieldAddressLine2];
    [self configureUI:_textFieldCity];
    [self configureUI:_textFieldZipcode];
    [self configureUI:_textFieldBestContactPhone];
    [self configureUI:_textFieldEmail];
    [self configureUI:_textFielddayOfMonth];
    [self configureUI:_textFieldBankName];
    [self configureUI:_textFieldAccountNumber];
    [self configureUI:_textFieldRoutingNumber];
    // [self configureUI:_buttonSubmit];
    [self configureUI:_buttonSelectDate];
    [self configureUI:_buttonSelectState];
    
    // add padding
    
    [self addPaddingInTextFields:_textFieldFirstName];
    [self addPaddingInTextFields:_textFieldMiddleName];
    [self addPaddingInTextFields:_textFieldLastName];
    [self addPaddingInTextFields:_textFieldAddressLine1];
    [self addPaddingInTextFields:_textFieldAddressLine2];
    [self addPaddingInTextFields:_textFieldCity];
    [self addPaddingInTextFields:_textFieldZipcode];
    [self addPaddingInTextFields:_textFieldBestContactPhone];
    [self addPaddingInTextFields:_textFieldEmail];
    [self addPaddingInTextFields:_textFielddayOfMonth];
    [self addPaddingInTextFields:_textFieldBankName];
    [self addPaddingInTextFields:_textFieldAccountNumber];
    [self addPaddingInTextFields:_textFieldRoutingNumber];
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:@"no" forKey:@"isFromElectronicAuthorizedForm"];
    [userDef synchronize];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    //    strEmpId      =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.CompanyId"];
    
    NSString *strPhoneNo;
    strPhoneNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.PrimaryPhone"]];
    [_btnPhoneNo setTitle:strPhoneNo forState:UIControlStateNormal];
    if (strPhoneNo.length==0|| [strPhoneNo isEqualToString:@""])
    {
        strPhoneNo=@"Us at office";
        [_btnPhoneNo setTitle:strPhoneNo forState:UIControlStateNormal];
    }
    
    
    strServiceUrlMainSales=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
    strLoggedInID = [NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    strLeadId=[defsLogindDetail valueForKey:@"LeadId"];
    
    NSString *strCompanyLogoURL=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.LogoImagePath"]];
    
    NSString* webStringURL = [strCompanyLogoURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSURL *urlImage = [NSURL URLWithString:webStringURL];
    
    [_imageViewCompanyLogo sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"profile.png"] options:SDWebImageRefreshCached];
    
    [self salesFetch];
    [self fetchElectronicAuthorizedFormDataFromLocalDB];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    
    if([[userDef valueForKey:@"isFromElectronicAuthorizedForm"] isEqualToString:@"yes"])
    {
        NSString *strImageName;
        strImageName=[userDef valueForKey:@"imagePathElectronic"];
        if(strImageName.length>0)
        {
            strCustSignaturePath = strImageName;
            UIImage *img = [self loadImage:strImageName];
            _imageViewCustomerSignature.image=img;
            isSignatureImage = YES;
        }
        [userDef setValue:@"no" forKey:@"isFromElectronicAuthorizedForm"];
        [userDef synchronize];
    }
    else
    {
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        [userDef setValue:@"" forKey:@"imagePathElectronic"];
        [userDef synchronize];
        
        isSignatureImage = NO;
    }
}

#pragma mark  - UIButton action

- (IBAction)actionOnBack:(id)sender
{
    [self endEditing];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionOnState:(id)sender
{
    [self endEditing];
    if(arrayState.count>0)
    {
        [self setTableFrame:0];
        [tblData reloadData];
    }
}

- (IBAction)actionOnEFTCheckingDraft:(id)sender
{
    [self endEditing];
    if(isEFTCheckingDraft==NO)
    {
        isEFTCheckingDraft = YES;
        [_buttonEFTCheckingDraft setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        _heightViewBankDetails.constant = 220.0;
        _heightMainContainerView.constant = _heightMainContainerView.constant+_heightViewBankDetails.constant;
        
    }
}
- (IBAction)actionOnCreditCard:(id)sender
{
    [self endEditing];
    if(isEFTCheckingDraft==YES)
    {
        isEFTCheckingDraft = NO;
        [_buttonCreditCard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonEFTCheckingDraft setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        _heightMainContainerView.constant = _heightMainContainerView.constant-_heightViewBankDetails.constant;
        _heightViewBankDetails.constant = 0.0;
    }
}

- (IBAction)actionOnCall:(id)sender
{
    [self endEditing];
    if ([_btnPhoneNo.currentTitle isEqualToString:@"Us at office"])
    {
        
    }
    else
    {
        [global calling:_btnPhoneNo.titleLabel.text];
    }
}

- (IBAction)actionOnAtFreqOfService:(id)sender
{
    [self endEditing];
    if(isAtFreqOfService==NO)
    {
        isAtFreqOfService=YES;
        [_buttonAtFreqOfService setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonEasyMonthlyPayment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
        _heightMainContainerView.constant = _heightMainContainerView.constant-_heightLabelDayOfMonth.constant;
        _heightLabelDayOfMonth.constant = 0.0;
        
    }
}
- (IBAction)actionOnEasyMonthlyPayments:(id)sender
{
    [self endEditing];
    if(isAtFreqOfService==YES)
    {
        isAtFreqOfService=NO;
        [_buttonEasyMonthlyPayment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonAtFreqOfService setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        _heightLabelDayOfMonth.constant = 40.0;
        _heightMainContainerView.constant = _heightMainContainerView.constant+_heightLabelDayOfMonth.constant;
        
    }
}

- (IBAction)actionOnSelectDate:(id)sender
{
    [self endEditing];
    [self addPickerViewDateTo];
}


- (IBAction)actionOnCustomerSignature:(id)sender
{
    [self endEditing];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:@"yes" forKey:@"isFromElectronicAuthorizedForm"];
    
    [userDef synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    ServiceSignViewController *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"ServiceSignViewController"];
    
    // objSignViewController.strType=@"inspector";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}
- (IBAction)actionOnCheckMarkSignUpTerms:(id)sender
{
    [self endEditing];
    if(isSignUpTermsAccepted)
    {
        isSignUpTermsAccepted = NO;
        [_buttonCheckMarkSignUpTerms setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    }
    else
    {
        isSignUpTermsAccepted = YES;
        [_buttonCheckMarkSignUpTerms setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
    }
}


- (IBAction)actionOnSubmit:(id)sender
{
    
    [global updateSalesModifydate:strLeadId];
    
    [self disableButton];
    
    [self endEditing];
    [self trimTextField];
    
    if(isSignatureImage)
    {
        
        if([[dictLeadDetails valueForKey:@"statusSysName"] isEqualToString:@"Complete"])
        {
            Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
            NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
            if (netStatusWify1== NotReachable)
            {
                [global AlertMethod:Alert :@"Internet connection is reqiured to fill payment authorization form"];
                return;
            }
            else
            {
                contextSubmitData = [appDelegate managedObjectContext];
                if([self getDataDict]!=nil)
                {
                    NSDictionary *dict = [self getDataDict];
                    
                    entityElectronicAuthorizedForm = [NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
                    
                    ElectronicAuthorizedForm *objMechanicalPOGeneric = [[ElectronicAuthorizedForm alloc] initWithEntity:entityElectronicAuthorizedForm insertIntoManagedObjectContext:contextSubmitData];
                    
                    objMechanicalPOGeneric.accountNo = [dict valueForKey:@"AccountNo"];
                    objMechanicalPOGeneric.leadNo = [dict valueForKey:@"LeadNo"];
                    objMechanicalPOGeneric.title = [dict valueForKey:@"Title"];
                    
                    objMechanicalPOGeneric.preNotes = [dict valueForKey:@"PreNotes"];
                    objMechanicalPOGeneric.postNotes = [dict valueForKey:@"PostNotes"];
                    objMechanicalPOGeneric.terms = [dict valueForKey:@"Terms"];
                    objMechanicalPOGeneric.termsSignUp = [dict valueForKey:@"TermsSignUp"];
                    
                    objMechanicalPOGeneric.frequency = [dict valueForKey:@"Frequency"];
                    objMechanicalPOGeneric.monthlyDate = [dict valueForKey:@"MonthlyDate"];
                    objMechanicalPOGeneric.paymentMethod = [dict valueForKey:@"PaymentMethod"];            objMechanicalPOGeneric.bankName = [dict valueForKey:@"BankName"];            objMechanicalPOGeneric.bankAccountNo = [NSString stringWithFormat:@"%@",[dict valueForKey:@"BankAccountNo"]];
                    // [dictData valueForKey:@"BankAccountNo"];
                    objMechanicalPOGeneric.routingNo = [dict valueForKey:@"RoutingNo"];
                    objMechanicalPOGeneric.signaturePath = [dict valueForKey:@"SignaturePath"];            objMechanicalPOGeneric.firstName = [dict valueForKey:@"FirstName"];
                    objMechanicalPOGeneric.middleName = [dict valueForKey:@"MiddleName"];            objMechanicalPOGeneric.lastName = [dict valueForKey:@"LastName"];
                    objMechanicalPOGeneric.address1 = [dict valueForKey:@"Address1"];
                    objMechanicalPOGeneric.address2 = [dict valueForKey:@"Address2"];            objMechanicalPOGeneric.stateName = [dict valueForKey:@"StateName"];
                    objMechanicalPOGeneric.cityName = [dict valueForKey:@"CityName"];
                    objMechanicalPOGeneric.zipCode =[NSString stringWithFormat:@"%@",[dict valueForKey:@"Zipcode"]];
                    objMechanicalPOGeneric.phone = [dict valueForKey:@"Phone"];
                    objMechanicalPOGeneric.email = [dict valueForKey:@"Email"];
                    objMechanicalPOGeneric.date = [dict valueForKey:@"Date"];
                    objMechanicalPOGeneric.createdBy = [dict valueForKey:@"CreatedBy"];
                    objMechanicalPOGeneric.signatureName = [dict valueForKey:@"SignatureName"];
                    objMechanicalPOGeneric.stateId = [dict valueForKey:@"StateId"];
                    objMechanicalPOGeneric.leadId = strLeadId;
                    
                    NSError *error2;
                    [contextSubmitData save:&error2];
                    
                    
                    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
                    
                    [self performSelector:@selector(uploadSignatureImageOnServer) withObject:nil afterDelay:0.1];
                    //[self uploadSignatureImageOnServer];
                }
            }
        }
        else
        {
            // save data offline
            contextSubmitData = [appDelegate managedObjectContext];
            if([self getDataDict]!=nil)
            {
                NSDictionary *dict = [self getDataDict];
                
                entityElectronicAuthorizedForm = [NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
                
                ElectronicAuthorizedForm *objMechanicalPOGeneric = [[ElectronicAuthorizedForm alloc] initWithEntity:entityElectronicAuthorizedForm insertIntoManagedObjectContext:contextSubmitData];
                
                objMechanicalPOGeneric.accountNo = [dict valueForKey:@"AccountNo"];
                objMechanicalPOGeneric.leadNo = [dict valueForKey:@"LeadNo"];
                objMechanicalPOGeneric.title = [dict valueForKey:@"Title"];
                
                objMechanicalPOGeneric.preNotes = [dict valueForKey:@"PreNotes"];
                objMechanicalPOGeneric.postNotes = [dict valueForKey:@"PostNotes"];
                objMechanicalPOGeneric.terms = [dict valueForKey:@"Terms"];
                objMechanicalPOGeneric.termsSignUp = [dict valueForKey:@"TermsSignUp"];
                
                objMechanicalPOGeneric.frequency = [dict valueForKey:@"Frequency"];
                objMechanicalPOGeneric.monthlyDate = [dict valueForKey:@"MonthlyDate"];
                objMechanicalPOGeneric.paymentMethod = [dict valueForKey:@"PaymentMethod"];
                objMechanicalPOGeneric.bankName = [dict valueForKey:@"BankName"];
                objMechanicalPOGeneric.bankAccountNo = [NSString stringWithFormat:@"%@",[dict valueForKey:@"BankAccountNo"]];
                // [dictData valueForKey:@"BankAccountNo"];
                objMechanicalPOGeneric.routingNo = [dict valueForKey:@"RoutingNo"];
                objMechanicalPOGeneric.signaturePath = [dict valueForKey:@"SignaturePath"];
                objMechanicalPOGeneric.firstName = [dict valueForKey:@"FirstName"];
                objMechanicalPOGeneric.middleName = [dict valueForKey:@"MiddleName"];
                objMechanicalPOGeneric.lastName = [dict valueForKey:@"LastName"];
                objMechanicalPOGeneric.address1 = [dict valueForKey:@"Address1"];
                objMechanicalPOGeneric.address2 = [dict valueForKey:@"Address2"];
                objMechanicalPOGeneric.stateName = [dict valueForKey:@"StateName"];
                objMechanicalPOGeneric.cityName = [dict valueForKey:@"CityName"];
                objMechanicalPOGeneric.zipCode =[NSString stringWithFormat:@"%@",[dict valueForKey:@"Zipcode"]];
                objMechanicalPOGeneric.phone = [dict valueForKey:@"Phone"];
                objMechanicalPOGeneric.email = [dict valueForKey:@"Email"];
                objMechanicalPOGeneric.date = [dict valueForKey:@"Date"];
                objMechanicalPOGeneric.createdBy = [dict valueForKey:@"CreatedBy"];
                objMechanicalPOGeneric.signatureName = [dict valueForKey:@"SignatureName"];
                objMechanicalPOGeneric.stateId = [dict valueForKey:@"StateId"];
                objMechanicalPOGeneric.leadId = strLeadId;
                
                NSError *error2;
                [contextSubmitData save:&error2];
                
                [self.navigationController popViewControllerAnimated:YES];
                [global AlertMethod:@"Message" :@"Payment authorization form saved successfully"];
                
                //                 [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
                //                 [self uploadSignatureImageOnServer];
            }
            //}
            //        else
            //        {
            
            //}
        }
    }
    else
    {
        [global AlertMethod:Alert :@"Customer signature is required."];
    }
}
-(void)actionOnBackground:(id)sender
{
   
    [tblData removeFromSuperview];
    [buttonBackground removeFromSuperview];
}

- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)tapDetectedOnBackGroundView
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}

#pragma mark  - UITextField delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(!(textField.text.length>0)&&[string isEqualToString:@" "])
    {
        return NO;
    }
    if(textField == _textFieldAccountNumber||textField == _textFieldBestContactPhone||textField == _textFieldZipcode)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if(textField==_textFieldAccountNumber)
        {
            if([string isEqualToString:filtered]==YES)
            {
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
                
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 15;
            }
        }
        if(textField==_textFieldBestContactPhone)
        {
            if([string isEqualToString:filtered]==YES)
            {
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
                
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 15;
            }
        }
        if(textField == _textFieldZipcode)
        {
            if([string isEqualToString:filtered]==YES)
            {
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
                
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 5;
            }
        }
        return [string isEqualToString:filtered];
    }
    if(textField==_textFielddayOfMonth)
    {
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :2 :(int)textField.text.length :@"0123456789" :_textFielddayOfMonth.text];
        
        if (isNuberOnly) {
            return YES;
        } else {
            return NO;
        }
        
        //
        
    }
    return YES;
}
#pragma mark - Data show and submit related methods

- (UIImage*)loadImage:(NSString *)imageName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDateString = [dateFormat stringFromDate:pickerDate.date];
    [_buttonSelectDate setTitle:strDateString forState:UIControlStateNormal];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)salesFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj12.count>0)
    {
        NSManagedObject *matches12=arrAllObj12[0];
        
        NSArray *keys = [[[matches12 entity] attributesByName] allKeys];
        dictLeadDetails = [matches12 dictionaryWithValuesForKeys:keys];
        
        if(dictLeadDetails.count>0)
        {
            _textFieldFirstName.text = [dictLeadDetails valueForKey:@"firstName"];
            _textFieldMiddleName.text = [dictLeadDetails valueForKey:@"middleName"];
            _textFieldLastName.text = [dictLeadDetails valueForKey:@"lastName"];
            _textFieldAddressLine1.text = [dictLeadDetails valueForKey:@"billingAddress1"];
            _textFieldAddressLine2.text = [dictLeadDetails valueForKey:@"billingAddress2"];
            [_buttonSelectState setTitle:[dictLeadDetails valueForKey:@"billingState"] forState:UIControlStateNormal];
            _textFieldCity.text = [dictLeadDetails valueForKey:@"billingCity"];
            _textFieldZipcode.text = [dictLeadDetails valueForKey:@"billingZipcode"];
            if([[dictLeadDetails valueForKey:@"billingCellNo"] length]>0)
            {
                _textFieldBestContactPhone.text = [NSString stringWithFormat:@"%@",[[dictLeadDetails valueForKey:@"billingCellNo"] stringByReplacingOccurrencesOfString:@"-" withString:@""]];
            }
            
            _textFieldEmail.text = [dictLeadDetails valueForKey:@"billingPrimaryEmail"];
            
            for(NSDictionary *dict in arrayState)
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]] isEqualToString:[dictLeadDetails valueForKey:@"billingState"]])
                {
                    dictState = dict;
                    break;
                }
            }
            
            [self resignKeyboard];
        }
    }
}

/*-(void)getHTMLData
 {
 NSString *strUrl = [NSString stringWithFormat:@"http://tcrs.stagingsoftware.com/api/MobileAppToCore/GetAuthorizationFormBySysNameAsync?companyKey=automation&FormSysName=ElectronicAuthorizationForm"];
 
 NSURL *url = [NSURL URLWithString:strUrl];
 
 NSURLSession *session = [NSURLSession sharedSession];
 [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
 NSURLSessionDataTask *data = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
 
 NSError *erro = nil;
 
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
 
 [DejalBezelActivityView removeView];
 
 });
 
 if (data!=nil)
 {
 NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
 
 if (json.count>0)
 {
 NSLog(@"%@",json);
 dictData = json;
 [self showData:json];
 }
 }
 else
 {
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
 
 [self.navigationController popViewControllerAnimated:YES];
 [global AlertMethod:Alert :@"Something went wrong."];
 
 });
 
 }
 }];
 [data resume];
 }*/

-(void)uploadSignatureImageOnServer
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strImageName = [defs valueForKey:@"imagePathElectronic"];
    strCustSignaturePath = strImageName;
    UIImage *img = _imageViewCustomerSignature.image;
    
    NSData *imageData  = UIImageJPEGRepresentation(img,.1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainSales,@"api/File/UploadSignatureAsync"];
    //URLCUSTOMERSIGNATUREUPLOAD
    //api/File/UploadSignatureAsync
    //NSString *urlString = [NSString stringWithFormat:@"http://tcrms.stagingsoftware.com/%@",@"api/File/UploadSignatureAsync"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSError *error = Nil;
    
    id jsonObject =[NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&error];
    
    
    if ([jsonObject isKindOfClass:[NSArray class]])
    {
        [self syncDataOnServer];
    }
    else
    {
        [self syncDataOnServer];
    }
}

-(void)showData:(NSDictionary*)data
{
    NSString *htmlStringTitle = [data valueForKey:@"Title"];
    NSString *htmlStringPreNotes = [data valueForKey:@"PreNotes"];
    NSString *htmlStringTerms = [data valueForKey:@"Terms"];
    NSString *htmlStringPostNotes = [data valueForKey:@"PostNotes"];
    
    NSAttributedString *attributedStringTitle = [[NSAttributedString alloc]
                                                 initWithData: [htmlStringTitle dataUsingEncoding:NSUnicodeStringEncoding]
                                                 options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                 documentAttributes: nil
                                                 error: nil
                                                 ];
    
    NSAttributedString *attributedStringPreNotes = [[NSAttributedString alloc]
                                                    initWithData: [htmlStringPreNotes dataUsingEncoding:NSUnicodeStringEncoding]
                                                    options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                    documentAttributes: nil
                                                    error: nil
                                                    ];
    
    NSAttributedString *attributedStringTerms = [[NSAttributedString alloc]
                                                 initWithData: [htmlStringTerms dataUsingEncoding:NSUnicodeStringEncoding]
                                                 options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                 documentAttributes: nil
                                                 error: nil
                                                 ];
    
    NSAttributedString *attributedStringPostNames = [[NSAttributedString alloc]
                                                     initWithData: [htmlStringPostNotes dataUsingEncoding:NSUnicodeStringEncoding]
                                                     options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                     documentAttributes: nil
                                                     error: nil
                                                     ];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        _textViewTitle.attributedText = attributedStringTitle;
        _textViewPreNotes.attributedText = attributedStringPreNotes;
        _textViewTerms.attributedText = attributedStringTerms;
        _textViewPostNotes.attributedText = attributedStringPostNames;
        
        CGFloat heightOfTextTitle =   [self heightForAttributedString:attributedStringTitle maxWidth:_textViewTitle.frame.size.width];
        
        if(heightOfTextTitle<60.0)
        {
            _heightTextViewTitle.constant = 60.0;
        }
        else
        {
            _heightTextViewTitle.constant = heightOfTextTitle;
        }
        _heightMainContainerView.constant = _heightMainContainerView.constant+_heightTextViewTitle.constant;
        
        CGFloat heightOfTextPreNotes =   [self heightForAttributedString:attributedStringPreNotes maxWidth:_textViewPreNotes.frame.size.width]+40;
        
        _heightTextViewPreNotes.constant = heightOfTextPreNotes;
        _heightMainContainerView.constant = _heightMainContainerView.constant+_heightTextViewPreNotes.constant;
        
        CGFloat heightOfTextTerms =   [self heightForAttributedString:attributedStringTerms maxWidth:_textViewTerms.frame.size.width]+40;
        
        _heightTextViewTerms.constant = heightOfTextTerms;
        _heightMainContainerView.constant = _heightMainContainerView.constant+_heightTextViewTerms.constant;
        
        CGFloat heightOfTextPostNotes =   [self heightForAttributedString:attributedStringPostNames maxWidth:_textViewPostNotes.frame.size.width]+40;
        
        _heightTextViewPostNotes.constant = heightOfTextPostNotes;
        _heightMainContainerView.constant = _heightMainContainerView.constant+_heightTextViewPostNotes.constant;
    });
}
-(void)syncDataOnServer
{
    if([self getDataDict]!=nil)
    {
        [self submitDataOnServer:[self getDataDict]];
    }
}

-(NSDictionary*)getDataDict
{
    NSString *strFirstName = @"";
    NSString *strMiddleName = @"";
    NSString *strLastName = @"";
    NSString *strAddressLine1 = @"";
    NSString *strAddressLine2 = @"";
    NSString *strStateID = @"";
    NSString *strCity = @"";
    NSString *strZipCode = @"";
    NSString *strBestContactPhone = @"";
    NSString *strEmail = @"";
    NSString *strBankName = @"";
    NSString *strBankAccNumber = @"";
    NSString *strRoutingNumber = @"";
    NSString *strPaymentMethod = @"";
    
    NSString *strTitle = @"";
    NSString *strPreNotes = @"";
    NSString *strPostNotes = @"";
    NSString *strTerms = @"";
    NSString *strFrequency = @"";
    NSString *strDayOfMonth = @"";
    NSString *strDate = @"";
    NSString *strIsSignTermsAccepted = @"";
    
    if(isSignUpTermsAccepted)
    {
        strIsSignTermsAccepted = @"true";
    }
    else
    {
        strIsSignTermsAccepted = @"false";
    }
    
    if(dictLeadDetails.count>0)
    {
        strAccountNumber = [NSString stringWithFormat:@"%@",[dictLeadDetails valueForKey:@"accountNo"]];
        strLeadNumber = [NSString stringWithFormat:@"%@",[dictLeadDetails valueForKey:@"leadNumber"]];
    }
    if(dictData.count>0)
    {
        strTitle = [dictData valueForKey:@"Title"];
        strPreNotes = [dictData valueForKey:@"PreNotes"];
        strPostNotes = [dictData valueForKey:@"PostNotes"];
        strTerms = [dictData valueForKey:@"Terms"];
    }
    if(_textFieldFirstName.text.length>0)
    {
        strFirstName = _textFieldFirstName.text;
    }
    
    if(_textFieldMiddleName.text.length>0)
    {
        strMiddleName = _textFieldMiddleName.text;
    }
    
    if(_textFieldLastName.text.length>0)
    {
        strLastName = _textFieldLastName.text;
    }
    
    if(_textFieldAddressLine1.text.length>0)
    {
        strAddressLine1 = _textFieldAddressLine1.text;
    }
    
    if(_textFieldAddressLine2.text.length>0)
    {
        strAddressLine2 = _textFieldAddressLine2.text;
    }
    if(dictState.count>0)
    {
        strStateID = [NSString stringWithFormat:@"%@",[dictState valueForKey:@"StateId"]];
    }
    if(_textFieldCity.text.length>0)
    {
        strCity = _textFieldCity.text;
    }
    if(_textFieldZipcode.text.length>0)
    {
        strZipCode =[NSString stringWithFormat:@"%@",_textFieldZipcode.text] ;
    }
    else
    {
        strZipCode = @"";
    }
    
    if(_textFieldBestContactPhone.text.length>0)
    {
        if([_textFieldBestContactPhone.text containsString:@"-"])
        {
            strBestContactPhone = [NSString stringWithFormat:@"%@",[_textFieldBestContactPhone.text stringByReplacingOccurrencesOfString:@"-" withString:@""]];
        }
        else
        {
            strBestContactPhone = _textFieldBestContactPhone.text;
        }
    }
    
    if(_textFieldEmail.text.length>0)
    {
        
        BOOL isValid = [global checkIfCommaSepartedEmailsAreValid:_textFieldEmail.text];
        
        if (!isValid) {
            
            [global AlertMethod:Alert :@"Please enter valid email id."];
            return nil;
            
        }else{
            
            strEmail = _textFieldEmail.text;

        }
        
        //strEmail = _textFieldEmail.text;
    }
    if(isEFTCheckingDraft==YES)
    {
        strPaymentMethod = @"EFT";
        if(_textFieldBankName.text.length>0)
        {
            strBankName = _textFieldBankName.text;
        }
        else
        {
            [global AlertMethod:Alert :@"Please enter bank name."];
            return nil;
        }
        
        if (_textFieldAccountNumber.text.length>0)
        {
            strBankAccNumber = [NSString stringWithFormat:@"%@",_textFieldAccountNumber.text];
        }
        else
        {
            [global AlertMethod:Alert :@"Please enter account number."];
            return nil;
        }
        
        if (_textFieldRoutingNumber.text.length>0)
        {
            strRoutingNumber = [NSString stringWithFormat:@"%@",_textFieldRoutingNumber.text];
        }
        else
        {
            [global AlertMethod:Alert :@"Please enter routing number."];
            return nil;
        }
    }
    else
    {
        strPaymentMethod = @"CreditCard";
    }
    
    if(isAtFreqOfService)
    {
        strFrequency = @"Service";
    }
    else
    {
        if(!(_textFielddayOfMonth.text.length>0))
        {
            [global AlertMethod:Alert :@"Please enter day of month."];
            return nil;
        }
        else
        {
            strDayOfMonth = [NSString stringWithFormat:@"%@",_textFielddayOfMonth.text];
            strFrequency = @"Monthly";
        }
    }
    if(![_buttonSelectDate.titleLabel.text isEqualToString:@"Select"])
    {
        strDate = _buttonSelectDate.titleLabel.text;
    }
    else
    {
        [global AlertMethod:Alert :@"Please select date."];
        return nil;
    }
    
    NSDictionary *dict = @{@"AccountNo":strAccountNumber,
                           @"LeadNo":strLeadNumber,
                           @"Title":strTitle,
                           @"PreNotes":strPreNotes,
                           @"PostNotes":strPostNotes,
                           @"Terms":strTerms,
                           @"TermsSignUp":strIsSignTermsAccepted,
                           @"Frequency":strFrequency,
                           @"MonthlyDate":strDayOfMonth,
                           @"PaymentMethod":strPaymentMethod,
                           @"BankName":strBankName,
                           @"BankAccountNo":strBankAccNumber,
                           @"RoutingNo":strRoutingNumber,
                           @"SignaturePath":strCustSignaturePath,
                           @"FirstName":strFirstName,
                           @"MiddleName":strMiddleName,
                           @"LastName":strLastName,
                           @"Address1":strAddressLine1,
                           @"Address2":strAddressLine2,
                           @"StateId":strStateID,
                           @"CityName":strCity,
                           @"Zipcode":strZipCode,
                           @"Phone":strBestContactPhone,
                           @"Email":strEmail,
                           @"Date":strDate,
                           @"CreatedBy":strLoggedInID
                           };
    NSLog(@"%@",dict);
    return dict;
}

-(void)submitDataOnServer:(NSDictionary *)jsonDict
{
    // NSString *strUrl = [NSString stringWithFormat:@"http://tcrms.stagingsoftware.com/api/LeadNowAppToSalesProcess/SaveElectronicAuthorizationFormAsync?companyKey=%@",strCompanyKey];//http://192.168.0.78:801/
    
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@/api/LeadNowAppToSalesProcess/SaveElectronicAuthorizationFormAsync?companyKey=%@",strServiceUrlMain,strCompanyKey];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:Nil];
    
    
    [global getServerResponseForUrlPostMethod:strUrl :jsonData withCallback:^(BOOL success, NSDictionary *response, NSError *error) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [DejalBezelActivityView removeView];
            
        });
        
        if(error==nil)
        {
            NSLog(@"%@",response);
            if([[response valueForKey:@"isSuccess"] boolValue]==true||[[response valueForKey:@"isSuccess"] integerValue]==1||[[response valueForKey:@"isSuccess"] isEqualToString:@"true"]||[[response valueForKey:@"isSuccess"] isEqualToString:@"True"])
            {
                if([[response valueForKey:@"Messages"] count]>0)
                {
                    [global AlertMethod:@"Message" :[[response valueForKey:@"Messages"] objectAtIndex:0]];
                }
                else
                {
                    [global AlertMethod:@"Message" :@"Payment authorization form saved successfully"];
                }
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                if([[response valueForKey:@"Messages"] count]>0)
                {
                    [global AlertMethod:@"Message" :[[response valueForKey:@"Messages"] objectAtIndex:0]];
                }
                else
                {
                    [global AlertMethod:@"Message" :@"Something went wrong."];
                }
            }
        }
        else
        {
            [global AlertMethod:@"Message" :@"Something went wrong"];
        }
    }];
}

-(void)fetchJsonState
{
    arrayState=[[NSMutableArray alloc]init];
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:StateJson];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSArray *arrOfState = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    [arrayState addObjectsFromArray:arrOfState];
}

-(void)fetchElectronicAuthorizedFormDataFromLocalDB
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityElectronicAuthorizedForm=[NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityElectronicAuthorizedForm];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj12.count>0)
    {
        NSManagedObject *matches12=arrAllObj12[0];
        
        NSArray *keys = [[[matches12 entity] attributesByName] allKeys];
        NSDictionary *dictElectronicFormDetails = [matches12 dictionaryWithValuesForKeys:keys];
        
        if(dictElectronicFormDetails.count>0)
        {
            strAccountNumber = [dictElectronicFormDetails valueForKey:@"accountNumber"];
            strLeadNumber = [dictElectronicFormDetails valueForKey:@"leadNo"];
            _textFieldFirstName.text = [dictElectronicFormDetails valueForKey:@"firstName"];
            _textFieldMiddleName.text = [dictElectronicFormDetails valueForKey:@"middleName"];
            _textFieldLastName.text = [dictElectronicFormDetails valueForKey:@"lastName"];
            _textFieldAddressLine1.text = [dictElectronicFormDetails valueForKey:@"address1"];
            _textFieldAddressLine2.text = [dictElectronicFormDetails valueForKey:@"address2"];
            
            for(NSDictionary *dict in arrayState)
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"StateId"]] isEqualToString:[dictElectronicFormDetails valueForKey:@"stateId"]])
                {
                    dictState = dict;
                    [_buttonSelectState setTitle:[dict valueForKey:@"Name"] forState:UIControlStateNormal];
                    break;
                }
            }
            
            if([[dictElectronicFormDetails valueForKey:@"termsSignUp"] isEqualToString:@"true"])
            {
                [_buttonCheckMarkSignUpTerms setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            }
            else
            {
                [_buttonCheckMarkSignUpTerms setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            }
            
            if([[dictElectronicFormDetails valueForKey:@"frequency"] isEqualToString:@"Service"])
            {
                [_buttonAtFreqOfService setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                [_buttonEasyMonthlyPayment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                isAtFreqOfService = YES;
                
                _heightMainContainerView.constant = _heightMainContainerView.constant-_heightLabelDayOfMonth.constant;
                _heightLabelDayOfMonth.constant = 0.0;
                
            }
            else
            {
                [_buttonEasyMonthlyPayment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                [_buttonAtFreqOfService setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                isAtFreqOfService = NO;
                
                _textFielddayOfMonth.text = [dictElectronicFormDetails valueForKey:@"monthlyDate"];
                _heightLabelDayOfMonth.constant = 35.0;
                _heightMainContainerView.constant = _heightMainContainerView.constant+_heightLabelDayOfMonth.constant;
                
            }
            
            if([[dictElectronicFormDetails valueForKey:@"paymentMethod"] isEqualToString:@"EFT"])
            {
                [_buttonEFTCheckingDraft setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                [_buttonCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                isEFTCheckingDraft = YES;
                
                _textFieldBankName.text = [dictElectronicFormDetails valueForKey:@"bankName"];
                _textFieldAccountNumber.text = [dictElectronicFormDetails valueForKey:@"bankAccountNo"];
                _textFieldRoutingNumber.text = [dictElectronicFormDetails valueForKey:@"routingNo"];
                
                _heightViewBankDetails.constant = 220.0;
                _heightMainContainerView.constant = _heightMainContainerView.constant+_heightViewBankDetails.constant;
            }
            else
            {
                [_buttonEFTCheckingDraft setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                [_buttonCreditCard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                isEFTCheckingDraft = NO;
                
                _heightMainContainerView.constant = _heightMainContainerView.constant-_heightViewBankDetails.constant;
                _heightViewBankDetails.constant = 0.0;
            }
            
            NSString *strDate=[NSString stringWithFormat:@"%@",[dictElectronicFormDetails valueForKey:@"date"]];
            strDate=[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:strDate];
            
            if ([strDate isEqualToString:@""]||[strDate isEqual:nil]||strDate.length==0)
            {
                strDate=[NSString stringWithFormat:@"%@",[dictElectronicFormDetails valueForKey:@"date"]];
            }
            [_buttonSelectDate setTitle:strDate forState:UIControlStateNormal];
            _textFieldCity.text = [dictElectronicFormDetails valueForKey:@"cityName"];
            _textFieldZipcode.text = [dictElectronicFormDetails valueForKey:@"zipCode"];
            _textFieldBestContactPhone.text = [dictElectronicFormDetails valueForKey:@"phone"];
            _textFieldEmail.text = [dictElectronicFormDetails valueForKey:@"email"];
            
            NSDictionary *dictTemp = @{@"Title":[dictElectronicFormDetails valueForKey:@"title"],
                                       @"PreNotes":[dictElectronicFormDetails valueForKey:@"preNotes"],
                                       @"Terms":[dictElectronicFormDetails valueForKey:@"terms"],
                                       @"PostNotes":[dictElectronicFormDetails valueForKey:@"postNotes"]
                                       };
            [self showData:dictTemp];
            
            strLoggedInID = [dictElectronicFormDetails valueForKey:@"createdBy"];
            
            NSString *strimageName = [dictElectronicFormDetails valueForKey:@"signaturePath"];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strimageName]];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
            if(fileExists)
            {
                UIImage* image = [UIImage imageWithContentsOfFile:path];
                _imageViewCustomerSignature.image = image;
            }
            
            [self resignKeyboard];
            
            _viewMainContainer.userInteractionEnabled = false;
            _buttonSubmit.hidden = YES;
        }
    }
    else
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        NSDictionary *dictHTMLData = [dictMasters valueForKey:@"ElectronicAuthorizationFormMaster"];
        dictData = dictHTMLData;
        [self showData:dictHTMLData];
        _viewMainContainer.userInteractionEnabled = true;
        _buttonSubmit.hidden = NO;
        
        
        // [self getHTMLData];
    }
}


#pragma mark - UITableView delegate and data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(arrayState.count>0)
    {
        return arrayState.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = [[arrayState objectAtIndex:indexPath.row] valueForKey:@"Name"];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [_buttonSelectState setTitle:[[arrayState objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
    
    dictState = [arrayState objectAtIndex:indexPath.row];
    [tblData removeFromSuperview];
    [buttonBackground removeFromSuperview];
}

#pragma mark- UI related methods

-(void)setTableFrame:(int)tag
{
    if(tag==102)
    {
        buttonBackground=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        buttonBackground.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
        
        [buttonBackground addTarget:self action:@selector(actionOnBackground:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: buttonBackground];
        
        tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
        
        tblData.layer.borderWidth = 1.0;
        tblData.layer.borderColor = [[UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0] CGColor];
        
        if([UIScreen mainScreen].bounds.size.height>1000)
        {
            tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
        }
        
        buttonOK = [[UIButton alloc] initWithFrame:CGRectMake((tblData.frame.size.width/2)-50, CGRectGetMaxY(tblData.frame)+10, 100, 50)];
        
        buttonOK.backgroundColor = [UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0];
        [buttonOK setTitle:@"OK" forState:UIControlStateNormal];
        //        buttonOK.titleLabel.text = @"OK";
        buttonOK.titleLabel.textColor = [UIColor whiteColor];
        buttonOK.titleLabel.font = [UIFont systemFontOfSize:22.0];
        [buttonOK addTarget:self action:@selector(actionOnOk_Source:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self.view addSubview:tblData];
        [self.view addSubview:buttonOK];
        
    }
    else
    {
        buttonBackground=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        buttonBackground.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
        
        [buttonBackground addTarget:self action:@selector(actionOnBackground:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: buttonBackground];
        
        tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
        
        tblData.layer.borderWidth = 1.0;
        tblData.layer.borderColor = [[UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0] CGColor];
        
        if([UIScreen mainScreen].bounds.size.height>1000)
        {
            tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
        }
        
        [self.view addSubview:tblData];
    }
}

-(void)configureUI:(UIView*)view
{
    view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 2.0;
}

-(void)addPaddingInTextFields:(UITextField*)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMaximumDate:[NSDate date]];
    
    if (!(strDateString.length==0))
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strDateString];
        
        if (dateToSett==nil)
        {
            
        }
        else
        {
            pickerDate.date =dateToSett;
        }
    }
    
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(20, lblLine.frame.origin.y+5, 100, 40)];
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+10, btnClose.frame.origin.y, 100,40)];
    
    //    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    //    {
    //        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
    //    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
-(void)resignKeyboard
{
    [_textFieldFirstName resignFirstResponder];
    [_textFieldMiddleName resignFirstResponder];
    [_textFieldLastName resignFirstResponder];
    [_textFieldAddressLine1 resignFirstResponder];
    [_textFieldAddressLine2 resignFirstResponder];
    [_textFieldCity resignFirstResponder];
    [_textFieldZipcode resignFirstResponder];
    [_textFieldBestContactPhone resignFirstResponder];
    [_textFieldEmail resignFirstResponder];
}

#pragma mark - Email validation method
-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - method to calculate height of attribute string
- (CGFloat)heightForAttributedString:(NSAttributedString *)text maxWidth:(CGFloat)maxWidth
{
    if ([text isKindOfClass:[NSString class]] && !text.length) {
        // no text means no height
        return 0;
    }
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGSize size = [text boundingRectWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX) options:options context:nil].size;
    
    CGFloat height = ceilf(size.height) + 1; // add 1 point as padding
    
    return height;
}

/*
  BOOL isValid = [global checkIfCommaSepartedEmailsAreValid:_txtFld_PrimaryEmail.text];
 */
-(void)trimTextField
{
    _textFieldEmail.text= [_textFieldEmail.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
}
-(void)endEditing
{
    [self.view endEditing:YES];
}

-(void)disableButton
{
    _buttonSubmit.enabled = NO;
    NSLog(@"button disable");
    [self performSelector:@selector(enableButton) withObject:nil afterDelay:3.0];
    
}
-(void)enableButton
{
     NSLog(@"button enable");
    _buttonSubmit.enabled = YES;
}

@end
