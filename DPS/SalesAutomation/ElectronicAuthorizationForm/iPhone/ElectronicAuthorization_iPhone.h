//
//  ElectronicAuthorization_iPhone.h
//  DPS
//
//  Created by Akshay Hastekar on 08/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ServiceSignViewController.h"



@interface ElectronicAuthorization_iPhone : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textViewPreNotes;
@property (weak, nonatomic) IBOutlet UITextField *textFieldFirstName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAddressLine1;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAddressLine2;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCity;
@property (weak, nonatomic) IBOutlet UITextField *textFieldZipcode;
@property (weak, nonatomic) IBOutlet UITextField *textFieldBestContactPhone;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFielddayOfMonth;
@property (weak, nonatomic) IBOutlet UITextField *textFieldBankName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAccountNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldRoutingNumber;
@property (weak, nonatomic) IBOutlet UITextView *textViewPostNotes;
@property (weak, nonatomic) IBOutlet UIButton *buttonSubmit;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectDate;
@property (weak, nonatomic) IBOutlet UIButton *buttonAtFreqOfService;
@property (weak, nonatomic) IBOutlet UIButton *buttonEasyMonthlyPayment;
@property (weak, nonatomic) IBOutlet UIButton *buttonEFTCheckingDraft;
@property (weak, nonatomic) IBOutlet UIButton *buttonCreditCard;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectState;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCustomerSignature;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCompanyLogo;
@property (weak, nonatomic) IBOutlet UIButton *buttonCheckMarkSignUpTerms;
@property (weak, nonatomic) IBOutlet UITextView *textViewTerms;
@property (weak, nonatomic) IBOutlet UITextView *textViewTitle;
@property (weak, nonatomic) IBOutlet UIView *viewMainContainer;

// constraints outlets //
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextViewPreNotes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLabelDayOfMonth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewBankDetails;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMainContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextViewTerms;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextViewPostNotes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextViewTitle;


@property(strong,nonatomic)NSManagedObject *matchesGeneralInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnPhoneNo;


@end
