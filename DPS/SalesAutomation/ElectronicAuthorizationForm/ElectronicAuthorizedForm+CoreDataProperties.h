//
//  ElectronicAuthorizedForm+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 01/05/18.
//
//

#import "ElectronicAuthorizedForm+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ElectronicAuthorizedForm (CoreDataProperties)

+ (NSFetchRequest<ElectronicAuthorizedForm *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *accountNo;
@property (nullable, nonatomic, copy) NSString *leadNo;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *preNotes;
@property (nullable, nonatomic, copy) NSString *postNotes;
@property (nullable, nonatomic, copy) NSString *terms;
@property (nullable, nonatomic, copy) NSString *termsSignUp;
@property (nullable, nonatomic, copy) NSString *frequency;
@property (nullable, nonatomic, copy) NSString *monthlyDate;
@property (nullable, nonatomic, copy) NSString *paymentMethod;
@property (nullable, nonatomic, copy) NSString *bankName;
@property (nullable, nonatomic, copy) NSString *bankAccountNo;
@property (nullable, nonatomic, copy) NSString *routingNo;
@property (nullable, nonatomic, copy) NSString *signaturePath;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *middleName;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, copy) NSString *address1;
@property (nullable, nonatomic, copy) NSString *address2;
@property (nullable, nonatomic, copy) NSString *stateName;
@property (nullable, nonatomic, copy) NSString *cityName;
@property (nullable, nonatomic, copy) NSString *zipCode;
@property (nullable, nonatomic, copy) NSString *phone;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *signatureName;
@property (nullable, nonatomic, copy) NSString *stateId;
@property (nullable, nonatomic, copy) NSString *leadId;


@end

NS_ASSUME_NONNULL_END
