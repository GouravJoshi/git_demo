//
//  ElectronicAuthorizedForm+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 01/05/18.
//
//

#import "ElectronicAuthorizedForm+CoreDataProperties.h"

@implementation ElectronicAuthorizedForm (CoreDataProperties)

+ (NSFetchRequest<ElectronicAuthorizedForm *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ElectronicAuthorizedForm"];
}

@dynamic accountNo;
@dynamic leadNo;
@dynamic title;
@dynamic preNotes;
@dynamic postNotes;
@dynamic terms;
@dynamic termsSignUp;
@dynamic frequency;
@dynamic monthlyDate;
@dynamic paymentMethod;
@dynamic bankName;
@dynamic bankAccountNo;
@dynamic routingNo;
@dynamic signaturePath;
@dynamic firstName;
@dynamic middleName;
@dynamic lastName;
@dynamic address1;
@dynamic address2;
@dynamic stateName;
@dynamic cityName;
@dynamic zipCode;
@dynamic phone;
@dynamic email;
@dynamic date;
@dynamic createdBy;
@dynamic signatureName;
@dynamic stateId;
@dynamic leadId;
@end
