//
//  ImagePreviewSalesAuto.h
//  DPS
//
//  Created by Rakesh Jain on 23/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
/////

#import <UIKit/UIKit.h>

@interface ImagePreviewSalesAuto : UIViewController
{
    IBOutlet UIImageView *imgView;
    IBOutlet UILabel *lblCount;
    NSMutableArray *arrayImages;
    int activeIndex;
}
- (IBAction)action_Back:(id)sender;
@property(nonatomic,strong) NSMutableArray *arrOfImages;
@property(nonatomic,strong) NSDictionary *dictOfWorkOrdersImagePreview;
@property (strong, nonatomic) IBOutlet UILabel *imageName;
@property (strong, nonatomic) NSMutableArray *arrayOfImages;
@property (strong, nonatomic) NSString *imagePreviewName,*strFromSalesAuto;
@property(nonatomic,strong) NSString *statusOfWorkOrder;
- (IBAction)actionOnDelete:(id)sender;
@property (strong, nonatomic) NSString *indexOfImage;
//Nilind 06 Jan

@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
- (IBAction)actionEditImage:(id)sender;

//Nilind 07 June
@property (weak, nonatomic) IBOutlet UITextView *textViewImageCaption;
@property (weak, nonatomic) IBOutlet UIButton *btnEditCaption;
- (IBAction)action_EditCaption:(id)sender;
@property (strong, nonatomic) NSMutableArray *arrOfImageCaptionsSaved;
@property (strong, nonatomic) NSMutableArray *arrOfImageDescriptionSaved;
//- (IBAction)action_EditImage:(id)sender;
- (IBAction)action_EditDescription:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textViewImageDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnEditDescription;
//@property (strong, nonatomic) IBOutlet UIButton *btnEditImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewEditDesc_H;
@property(strong,nonatomic)NSString *strForCheckImage;
@end
