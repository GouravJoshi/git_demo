//
//  GlobalSyncViewController.m
//  DPS
//
//  Created by Rakesh Jain on 12/01/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "LeadDetail.h"
#import "PaymentInfo.h"
#import "LeadPreference.h"
#import "SoldServiceNonStandardDetail.h"
#import "SoldServiceStandardDetail.h"
#import "ImageDetail.h"
#import "EmailDetail.h"
#import "DocumentsDetail.h"
#import "DocumentsDetail.h"
#import "BeginAuditView.h"
#import "SalesAutomationAgreementProposal.h"
#import "InitialSetUp.h"
#import "AppointmentView.h"

#import "CurrentService.h"
#import "SalesAutomationSelectService.h"
#import "SalesAutomationServiceSummary.h"
#import "SaleAutomationGeneralInfo.h"
#import "SalesAutomationInspection.h"
#import "SalesDynamicInspection.h"
#import "GlobalSyncViewController.h"
#import "AllImportsViewController.h"
#import "DPS-Swift.h"

@interface GlobalSyncViewController ()
{
    NSString *strLeadId,*strLeadName,*strSalesAutoMainUrl,*strAudioNameGlobal;
    NSMutableArray *arrEmail,*arrFinalJson,*arrUpdateLeadId,*arrOfAllImagesToSendToServer,*arrOfAllSignatureImagesToSendToServer;
    UITableViewCell *cell;
    NSMutableArray *arr,*arr2;
    BOOL chk,temp,flag;
    int val;
    NSMutableArray *arrFinalSend;
    
    NSMutableArray  *arrFinalLeadDetail,
    *arrFinalPaymenyInfo,
    *arrFinalImageDetail,
    *arrFinalEmailDetail,
    *arrFinalDocumentsDetail,
    *arrFinalSoldStandard,
    *arrFinalLeadPreference,
    *arrFinalSoldNonStandard
    ,*arrPrimarySecondaryMail;
    
    
    NSMutableDictionary *dictFinal;
    //......
    Global *global;
    
    NSMutableArray *arrImageName,*arrImageSend,*arrResponseInspection;
    NSString *strModifyDateToSendToServerAll;
    int indexToSendImage,indexToSendImageSign;
    NSMutableArray *arrSaveEmail,*arrDuplicateMail,*arrSoldCount;
    
    NSMutableArray *arrMailChecked;
    
    NSMutableArray *arrCheckEmail,*arrUncheckEmail;
    BOOL chkClickStatus,lock;
    NSString *strCompanyKey,*strEmployeeNo,*strUserName;
    BOOL chkStatus;
    NSMutableArray *arrCheckImage;
    NSString *strServiceUrlMainSales,*strElectronicSign;

    NSMutableArray *arrAllTargetImages,*arrUploadOtherDocument,*arrUploadOtherDocumentId;
    NSMutableArray *arrUploadGraphXML;

}
@end

@implementation GlobalSyncViewController

- (void)viewDidLoad
{
    //Nilind 16 Nov
    [super viewDidLoad];
    arrCheckImage=[[NSMutableArray alloc]init];
    
    //  _viewSendMail.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    // _viewSendMail.layer.borderWidth=1.0;
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionOnSendEmail:(id)sender
{
    // [self updateLeadIdDetail];
    //[self syncCall];
    
}



// 2nd Sept
#pragma mark -SalesAuto Fetch Core Data

-(void)salesEmailFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    arrEmail=[[NSMutableArray alloc]init];
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrEmail addObject:[matches valueForKey:@"emailId"]];
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"emailId"]);
        
    }
    chk=YES;
    //[_tblEmailData reloadData];
    
}

-(void)finalMails
{
    /* arrFinalSend=[[NSMutableArray alloc]init];
     for (int i=0; i<arr.count; i++)
     {
     int value;
     value=[[arr objectAtIndex:i]intValue];
     [arrFinalSend addObject:[arrEmail objectAtIndex:value]];
     
     }
     NSLog(@"ArrFinalSend >>%@",arrFinalSend);*/
}
#pragma mark- 13 Sept Nilind
#pragma mark- FETCHING ALL RECORDS CORE DATA
-(void)fetchLeadDetail
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadDetail"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
            
            NSArray *arrLeadDetailKey;
            //arrLeadDetailKey=[[NSMutableArray alloc]init];
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrLeadDetailKey);
            
            NSLog(@"all keys %@",arrLeadDetailValue);
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                //[arrLeadDetailValue addObject:str];
                if([[arrLeadDetailKey objectAtIndex:i]isEqualToString:@"billingCountry"]||[[arrLeadDetailKey objectAtIndex:i]isEqualToString:@"serviceCountry"])
                {
                    [arrLeadDetailValue addObject:[str stringByReplacingOccurrencesOfString:@" " withString:@""]];
                }
                else
                {
                    [arrLeadDetailValue addObject:str];
                }
                
            }
            NSDictionary *dictLeadDetail;
            dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            NSLog(@"LeadDetail%@",dictLeadDetail);
            //[dictFinal setObject:dictLeadDetail forKey:@"LeadDetail"];
            
            
            
            int indexToRemove=-1;
            int indexToRemoveDate=-1;
            int indexToReplaceModifyDate=-1;
            int indexToReplaceScheduleDated=-1;
            
            for (int k=0; k<arrLeadDetailKey.count; k++) {
                
                NSString *strKeyLeadId=arrLeadDetailKey[k];
                //dateModified
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"dateModified"]) {
                    
                    indexToRemoveDate=k;
                    
                }
                
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"zdateScheduledStart"]) {
                    
                    indexToReplaceScheduleDated=k;
                    
                }
                
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrLeadDetailKey];
            [arrKeyTemp removeObjectAtIndex:indexToRemoveDate];
            // [arrKeyTemp removeObjectAtIndex:indexToRemove-1];
            arrLeadDetailKey=arrKeyTemp;
            [arrLeadDetailValue removeObjectAtIndex:indexToRemoveDate];
            //  [arrLeadDetailValue removeObjectAtIndex:indexToRemove-1];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceScheduleDated-1 withObject:@""];
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
            [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
            
            if ([_strFromWhere isEqualToString:@"WDO"] || [_strFromWhere isEqualToString:@"WDOPendingApproval"] ){
                
                // From WDO Set isMailSend to True to send mails
                
                //[dictTemp setValue:@"true" forKey:@"isMailSend"];
                
            }else {

                
                
            }
            
            NSLog(@"%@",dictTemp);
            [dictFinal setObject:dictTemp forKey:@"LeadDetail"];
        }
    }
}
-(void)fetchPaymentInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrCheckImage=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [self savePaymentInfo];
        //[dictFinal setObject:@"" forKey:@"PaymentInfo"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSString *strCustomerImage=[matches valueForKey:@"customerSignature"];
            NSString *strSalesPersonImage=[matches valueForKey:@"salesSignature"];
            NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
            NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
            
            if (!(strCustomerImage.length==0)) {
                
                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerImage];//salesSignature
                
            }
            if (!(strSalesPersonImage.length==0)) {
                
                [arrOfAllSignatureImagesToSendToServer addObject:strSalesPersonImage];//salesSignature
                
            }
            
            if (!(strCheckFrontImage.length==0))
            {
                
                [arrCheckImage addObject:strCheckFrontImage];//salesSignature
                
            }
            if (!(strCheckBackImage.length==0))
            {
                
                [arrCheckImage addObject:strCheckBackImage];//salesSignature
                
            }
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"PaymentInfo%@",dictPaymentInfo);
            [dictFinal setObject:dictPaymentInfo forKey:@"PaymentInfo"];
        }
    }
}
-(void)fetchImageDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    arrUploadGraphXML = [[NSMutableArray alloc]init];

    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImageDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matches valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                //Nilind 11 Nov
                /*NSString *result;
                 NSRange equalRange = [strImageName rangeOfString:@"/" options:NSBackwardsSearch];
                 if (equalRange.location != NSNotFound) {
                 result = [strImageName substringFromIndex:equalRange.location + equalRange.length];
                 }
                 if (result.length==0) {
                 NSRange equalRange = [strImageName rangeOfString:@"\\" options:NSBackwardsSearch];
                 if (equalRange.location != NSNotFound) {
                 result = [strImageName substringFromIndex:equalRange.location + equalRange.length];
                 }
                 }
                 [arrOfAllImagesToSendToServer addObject:result];*/
                //...................
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isNewGraph"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[matches valueForKey:@"isNewGraph"]] isEqualToString:@"True"] || [[NSString stringWithFormat:@"%@",[matches valueForKey:@"isNewGraph"]] isEqualToString:@"1"])
            {
                NSString *strXMLName = [matches valueForKey:@"leadXmlPath"];
                if (!(strXMLName.length==0))
                {
                    [arrUploadGraphXML addObject:strXMLName];
                }

            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}
-(void)fetchEmailDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    [request setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSMutableDictionary *dictPaymentInfo;
            dictPaymentInfo=[[NSMutableDictionary alloc]init];
            dictPaymentInfo = [NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];

            if ([_strFromWhere isEqualToString:@"WDO"] || [_strFromWhere isEqualToString:@"WDOPendingApproval"] ){
                
                // From WDO Set isMailSend to True to send mails
                
                //[dictPaymentInfo setValue:@"false" forKey:@"isMailSent"];

            }else {
                
                //[dictPaymentInfo setValue:@"false" forKey:@"isMailSent"];
                
            }
            
            //[dictPaymentInfo setValue:@"false" forKey:@"isMailSent"];

            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}
-(void)fetchLeadContactDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityContactNumberDetail= [NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context];
    [request setEntity:entityContactNumberDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"LeadContactDetailExtSerDc"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"LeadContactDetailExtSerDc%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"LeadContactDetailExtSerDc"];
        
    }
}

-(void)fetchDocumentsDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrDocumentsDetail forKey:@"DocumentsDetail"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrDocumentsDetail addObject:dictPaymentInfo];
            NSLog(@"DocumentsDetail%@",arrDocumentsDetail);
        }
        [dictFinal setObject:arrDocumentsDetail forKey:@"DocumentsDetail"];
    }
}
-(void)fetchLeadPreference
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadPreference= [NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
    [request setEntity:entityLeadPreference];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadPreference"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictLeadPreference;
            dictLeadPreference = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadPreference%@",dictLeadPreference);
            [dictFinal setObject:dictLeadPreference forKey:@"LeadPreference"];
        }
        
        
    }
}
-(void)fetchSoldServiceStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrSoldServiceStandardDetail;
    arrSoldServiceStandardDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrSoldServiceStandardDetail forKey:@"SoldServiceStandardDetail"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        arrSoldCount=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //Nilind 09 Jan
            if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isSold"]]isEqualToString:@"true"])
            {
                [arrSoldCount addObject:[NSString stringWithFormat:@"%d",k]];
            }
            //End
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                //NSString *str;
                id str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil])// || str.length==0 )
                {
                    str=@"";
                }
                if([str isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrPaymentInfoValue addObject:str];
                
                /* NSString *str;
                 str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                 
                 BOOL isTrue=[global isStringNullNilBlank:str];
                 if (isTrue) {
                 str=@"";
                 }
                 
                 [arrPaymentInfoValue addObject:str];*/
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictSoldServiceStandardDetail;
            dictSoldServiceStandardDetail = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictSoldServiceStandardDetail);
            [arrSoldServiceStandardDetail addObject:dictSoldServiceStandardDetail];
            NSLog(@"SoldServiceStandardDetail%@",arrSoldServiceStandardDetail);
        }
        [dictFinal setObject:arrSoldServiceStandardDetail forKey:@"SoldServiceStandardDetail"];
        
    }
}
-(void)fetchSoldServiceNonStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceNonStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrSoldServiceNonStandardDetail;
    arrSoldServiceNonStandardDetail=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrSoldServiceNonStandardDetail forKey:@"SoldServiceNonStandardDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictSoldServiceNonStandardDetail;
            dictSoldServiceNonStandardDetail = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictSoldServiceNonStandardDetail);
            [arrSoldServiceNonStandardDetail addObject:dictSoldServiceNonStandardDetail];
            NSLog(@"SoldServiceNonStandardDetail%@",arrSoldServiceNonStandardDetail);
        }
        [dictFinal setObject:arrSoldServiceNonStandardDetail forKey:@"SoldServiceNonStandardDetail"];
        
    }
}
-(void)fetchTagDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTagDetail=[NSEntityDescription entityForName:@"LeadTagExtDcs" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityTagDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadTagExtDcs"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadTagExtDcs%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadTagExtDcs"];
    }
}

-(void)sendingleadToServer
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
    requestModifyDate = [[NSFetchRequest alloc] init];
    [requestModifyDate setEntity:entityModifyDate];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadIdd = %@ AND userName = %@",strLeadId,strUsername];
    
    [requestModifyDate setPredicate:predicate];
    
    sortDescriptorModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsModifyDate = [NSArray arrayWithObject:sortDescriptorModifyDate];
    
    [requestModifyDate setSortDescriptors:sortDescriptorsModifyDate];
    
    self.fetchedResultsControllerModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerModifyDate performFetch:&error];
    arrAllObjModifyDate = [self.fetchedResultsControllerModifyDate fetchedObjects];
    if ([arrAllObjModifyDate count] == 0)
    {
#pragma mark- NILIND 18 Oct
        //Nilind
        //strModifyDateToSendToServerAll=[global modifyDate];
        //..........
    }
    else
    {
        matchesModifyDate=arrAllObjModifyDate[0];
        strModifyDateToSendToServerAll=[matchesModifyDate valueForKey:@"modifyDate"];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    
    indexToSendImage=0;
    indexToSendImageSign=0;
    
    [self fetchLeadDetail];
    [self fetchPaymentInfo];
    [self fetchImageDetail];
    [self fetchEmailDetail];
    [self fetchDocumentsDetail];
    [self fetchLeadPreference];
    [self fetchSoldServiceStandardDetail];
    [self fetchSoldServiceNonStandardDetail];
    [self fetchCurrentServices];
    [self fetchElectronicAuthorizedForm];
    [self fetchForAppliedDiscountFromCoreData];
    [self fetchLeadContactDetail];
    [self fetchRenewalServiceDetail];
    [self fetchTagDetail];
    //For  ClarkPest
    
    [self fetchScopeFromCoreData];
    [self fetchTargetFromCoreData];
    [self fetchLeadCommercialInitialInfoFromCoreData];
    [self fetchLeadCommercialMaintInfoFromCoreData];
    [self fetchLeadCommercialDetailExtDcFromCoreData];
    [self fetchMultiTermsFromCoreDataClarkPest];
    [self fetchForAppliedDiscountFromCoreDataClarkPest];
    [self fetchSalesMarketingContentFromCoreDataClarkPest];
    
    NSLog(@"Final Dictionary >>>>>>%@",dictFinal);
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        if (arrOfAllImagesToSendToServer.count==0) {
            
            if (arrOfAllSignatureImagesToSendToServer.count==0) {
                
                if (strAudioNameGlobal.length==0 || [strAudioNameGlobal isEqualToString:@"(null)"]) {
                    
                    [self fialJson];
                    
                } else {
                    
                    [self uploadAudio:strAudioNameGlobal];
                    
                }
                
                
            } else {
                
                [self uploadingAllImages:0];
                
            }
            
            
        }
        else
        {
            
            [self uploadingAllImages:0];
            
        }
    }
}
-(void)fetchRenewalServiceDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityRenewalServiceDetail= [NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
    [request setEntity:entityRenewalServiceDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrRenewalServiceDetail;
    arrRenewalServiceDetail=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrRenewalServiceDetail forKey:@"RenewalServiceExtDcs"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);

            
            NSArray *arrRenewalServiceKey;
            NSMutableArray *arrRenewalServiceValue;
            
            arrRenewalServiceValue=[[NSMutableArray alloc]init];
            arrRenewalServiceKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrRenewalServiceKey);
            
            NSLog(@"all keys %@",arrRenewalServiceValue);
            for (int i=0; i<arrRenewalServiceKey.count; i++)
            {
                //NSString *str;
                id str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrRenewalServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrRenewalServiceKey objectAtIndex:i]]];
                if([str isEqual:nil])// || str.length==0 )
                {
                    str=@"";
                }
                if([str isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrRenewalServiceValue addObject:str];
                
                /* NSString *str;
                 str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                 
                 BOOL isTrue=[global isStringNullNilBlank:str];
                 if (isTrue) {
                 str=@"";
                 }
                 
                 [arrPaymentInfoValue addObject:str];*/
            }
            
            
            NSDictionary *dictSoldServiceStandardDetail;
            dictSoldServiceStandardDetail = [NSDictionary dictionaryWithObjects:arrRenewalServiceValue forKeys:arrRenewalServiceKey];
            
            NSLog(@"%@",dictSoldServiceStandardDetail);
            [arrRenewalServiceDetail addObject:dictSoldServiceStandardDetail];
            NSLog(@"RenewalServiceExtDcs%@",arrRenewalServiceDetail);
        }
        [dictFinal setObject:arrRenewalServiceDetail forKey:@"RenewalServiceExtDcs"];
    }
}
#pragma mark- FINAL JSON SENDING

-(void)fialJson
{
    for (int i=0; i<arrCheckImage.count; i++)
    {
        [self uploadImageCheckImage:[arrCheckImage objectAtIndex:i]];
    }
    for (int i=0; i<arrAllTargetImages.count; i++)
    {
        [self uploadTargetImage:[arrAllTargetImages objectAtIndex:i]];
    }
    if ([strElectronicSign isKindOfClass:[NSString class]])
    {
        [self uploadSignatureImageOnServer:strElectronicSign];
        
    }
    //For OtherDocument Upload
       [self fetchOtherDocument];
       for (int i=0; i<arrUploadOtherDocument.count; i++)
       {
           [self uploadOtherDocuments:[arrUploadOtherDocument objectAtIndex:i] DocumentId:[arrUploadOtherDocumentId objectAtIndex:i]];           //[self uploadOtherDocumentImage:[arrUploadOtherDocument objectAtIndex:i]];
       }
    
    //For Graph XML Upload
    for (int i=0; i<arrUploadGraphXML.count; i++)
    {
        [self uploadGraphXml:[arrUploadGraphXML objectAtIndex:i]];
    }
    
    [self finalJsonAfterImageSync];
    ///api/MobileToSaleAuto/UpdateLeadDetail
}
-(void)finalJsonAfterImageSync
{
    
    if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
    {
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    }
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        
        if ([NSJSONSerialization isValidJSONObject:dictFinal])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictFinal options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Update Task JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesFinalUpdate];
        
        
        //============================================================================
        //============================================================================
        NSDictionary *dictTemp;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     //Sending Dynamic Json
                     NSLog(@"fianl response %@",response);
                     
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"startLoader" object:self];
                     
                     if ([_strFromWhere isEqualToString:@"WDOPendingApproval"]){

                         [[NSNotificationCenter defaultCenter] postNotificationName:@"startLoaderWdoPendingSalesAuto" object:self];

                         
                     }

                     //  [DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         
                         [global updateSalesZSYNC:strLeadId :@"no"];
                         //Nilind 05 Dec
                         // Save target in DB
                         
                         NSArray *arrTargets=[response valueForKey:@"leadCommercialTargetExtDcs"];
                         
                         if ([arrTargets isKindOfClass:[NSArray class]]){
                             
                             NSString *strTempLeadId=[NSString stringWithFormat:@"%@",[response valueForKey:@"LeadId"]];
                             
                             WebService *objWebService = [[WebService alloc] init];
                             
                             [objWebService savingTargetsWithArrTarget:arrTargets strLeadId:strTempLeadId];
                             
                         }
                         //End
                         
                         /*   if ([_strWhereToSync isEqualToString:@"general"])
                          {
                          
                          }
                          else if ([_strForSendProposal isEqualToString:@"inspection"])
                          {
                          }
                          else if ([_strForSendProposal isEqualToString:@"selectservice"])
                          {
                          
                          }
                          else if ([_strForSendProposal isEqualToString:@"servicesummary"])
                          {
                          }
                          
                          else if ([_strForSendProposal isEqualToString:@"agreement"])
                          {
                          }
                          
                          
                          
                          
                          if ([_strForSendProposal isEqualToString:@"forSendProposal"])
                          {
                          _strForSendProposal=@"abc";
                          UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];AppointmentView*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
                          [self.navigationController pushViewController:objByProductVC animated:NO];
                          }
                          else
                          {
                          UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"Appointment completed successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                          [alert show];
                          
                          
                          
                          NSString *strStatus;
                          strStatus=[self fetchLead];
                          if ([strStatus isEqualToString:@"false"])
                          {
                          //Nilind 09 Jan
                          if (arrSoldCount.count ==0)
                          {
                          UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];AppointmentView*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
                          [self.navigationController pushViewController:objByProductVC animated:NO];
                          }
                          //End
                          else
                          {
                          int index = 0;
                          NSArray *arrstack=self.navigationController.viewControllers;
                          for (int k1=0; k1<arrstack.count; k1++) {
                          if ([[arrstack objectAtIndex:k1] isKindOfClass:[SalesAutomationAgreementProposal class]]) {
                          index=k1;
                          //break;
                          }
                          }
                          SalesAutomationAgreementProposal *myController = (SalesAutomationAgreementProposal *)[self.navigationController.viewControllers objectAtIndex:index];
                          
                          [self.navigationController popToViewController:myController animated:NO];
                          
                          }
                          }
                          else
                          {
                          UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];AppointmentView*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
                          [self.navigationController pushViewController:objByProductVC animated:NO];
                          }
                          }
                          */
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
    }
}
#pragma mark- FINAL IMAGE SEND

-(void)uploadingAllImages :(int)indexToSendimages{
    
    if (arrOfAllImagesToSendToServer.count==indexToSendimages) {
        
        [self uploadingAllImagesSignature:0];
        
    } else {
        
        [self uploadImage:arrOfAllImagesToSendToServer[indexToSendimages]];
        
    }
}

-(void)uploadingAllImagesSignature :(int)indexToSendimages{
    
    if (arrOfAllSignatureImagesToSendToServer.count==indexToSendimages) {
        
        if (strAudioNameGlobal.length==0 || [strAudioNameGlobal isEqualToString:@"(null)"]) {
            
            [self fialJson];
            
        } else {
            
            [self uploadAudio:strAudioNameGlobal];
            
        }
        
        
    } else {
        
        [self uploadImageSignature:arrOfAllSignatureImagesToSendToServer[indexToSendimages]];
        
    }
    
}
// Nilind 21 Sept
//============================================================================
//============================================================================
#pragma mark- Upload Image METHOD
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName
{
    if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
    {
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    }
    
    if ([_strFromWhere isEqualToString:@"WDO"] || [_strFromWhere isEqualToString:@"WDOPendingApproval"]){

        strImageName = [global splitString:strImageName :@"\\"];
        
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    if (imagee == nil){
        
    }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        NSLog(@"Image Sent");
        
    }
    
    indexToSendImage++;
    [self uploadingAllImages:indexToSendImage];
    
}

-(void)uploadImageSignature :(NSString*)strImageName
{
    if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
    {
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    if (imagee == nil){
        
    }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesSignatureImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        NSLog(@"Image Sent");
        
    }
    
    indexToSendImageSign++;
    [self uploadingAllImagesSignature:indexToSendImageSign];
    
}
#pragma mark- UPLOAD CHECK IMAGE
//============================================================================
//============================================================================
-(void)uploadImageCheckImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
        
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
            NSString *strCheckUrl;
            strCheckUrl=@"/api/File/UploadCheckImagesAsync";
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,strCheckUrl];//UrlSalesImageUpload];//
            
            //http://192.168.0.218:3333//api/File/UploadAsync
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Image Sent");
            }
            //[self HudView:strrr];
            
        }
        
    }
    
}

//...........................................................
// Nilind 24 Sept
//============================================================================
//============================================================================
#pragma mark- TEXT FIELD DELEGATE
//============================================================================
//============================================================================
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtEmailId resignFirstResponder];
    return YES;
}


//============================================================================
//============================================================================
#pragma mark- Dynamic Form Send Method
//============================================================================
//============================================================================


-(void)FetchFromCoreDataToSendSalesDynamic
{
    //Nilind 27 Dec
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    
    // changes by saavan for WDO syncing
    
    if ([_strFromWhere isEqualToString:@"WDO"] || [_strFromWhere isEqualToString:@"WDOPendingApproval"]){
        
        strLeadId  = [defsLead valueForKey:@"WdoLeadId"];
        
    }
    
    //......
    
    //Nilind 2 Jan
    
    /*if(strLeadId.length==0 || [strLeadId isEqual:[NSNull null]])
     {
     strLeadId=[defsLead valueForKey:@"ForInitialSetUpLeadId"];
     }
     
     //............
     
     //Nilind 3'rd Jan
     NSUserDefaults *defs12=[NSUserDefaults standardUserDefaults];
     NSString *str;
     str=[defs12 valueForKey:@"fromInitialSetupClick"];
     
     if ([str isEqualToString:@"fromInitialSetupClick"])
     {
     strLeadId=[defs12 valueForKey:@"ForInitialSetUpLeadId"];
     }*/
    //.............
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    requestNewSalesDynamic = [[NSFetchRequest alloc] init];
    [requestNewSalesDynamic setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND userName = %@",strLeadId,strUsername];
    
    [requestNewSalesDynamic setPredicate:predicate];
    
    sortDescriptorSalesDynamic = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsSalesDynamic = [NSArray arrayWithObject:sortDescriptorSalesDynamic];
    
    [requestNewSalesDynamic setSortDescriptors:sortDescriptorsSalesDynamic];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewSalesDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObjSalesDynamic = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObjSalesDynamic count] == 0)
    {
        [self sendingleadToServer];
    }
    else
    {
        matchesSalesDynamic=nil;
        
        matchesSalesDynamic=arrAllObjSalesDynamic[0];
        NSArray *arrTemp = [matchesSalesDynamic valueForKey:@"arrFinalInspection"];
        arrResponseInspection =[[NSMutableArray alloc]init];
        [arrResponseInspection addObjectsFromArray:arrTemp];
        
        [self sendingFinalDynamicJsonToServer];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)sendingFinalDynamicJsonToServer
{
    
    if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
    {
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    }
    
    
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrResponseInspection,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"InspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesInspectionDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 //[DejalBezelActivityView removeView];
                 [self sendingleadToServer];
                 
                 if (success)
                 {
                     //                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     //                     if ([str containsString:@"Success"]) {
                     //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"Data Synced Successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     //                         [alert show];
                     //                     }else{
                     //
                     //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong,Some data didn't synced properly.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     //                         [alert show];
                     //
                     //                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
    
}

- (IBAction)actionOnInitialSetup:(id)sender
{
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    InitialSetUp *objInitialSetUp=[storyBoard instantiateViewControllerWithIdentifier:@"InitialSetUp"];
    // [self presentViewController:objInitialSetUp animated:YES completion:nil];
    [self.navigationController pushViewController:objInitialSetUp animated:NO];
}
- (IBAction)actionOnBack:(id)sender
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[SalesAutomationAgreementProposal class]]) {
            index=k1;
            //break;
        }
    }
    SalesAutomationAgreementProposal *myController = (SalesAutomationAgreementProposal *)[self.navigationController.viewControllers objectAtIndex:index];
    // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    [self.navigationController popToViewController:myController animated:NO];
    
    
}
- (IBAction)actionOnAdd:(id)sender

{
    if (_txtEmailId.text.length==0)
        
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter email id to add" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        BOOL chkEmail;
        chkEmail=NO;
        for (int i=0; i<arrEmail.count; i++)
        {
            if ([_txtEmailId.text isEqualToString:[arrEmail objectAtIndex:i]])
            {
                chkEmail=YES;
            }
        }
        if (chkEmail==YES)
        {
            chkEmail=NO;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Email Id already exist" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            BOOL emailChk;
            emailChk=[self NSStringIsValidEmail:_txtEmailId.text];
            if (emailChk==NO)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Invalid email type" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            else
            {
                
                chk=YES;
                [arrEmail addObject:_txtEmailId.text];
                [arr addObject:[NSString stringWithFormat:@"%lu",(unsigned long)arrEmail.count-1]];
                [_txtEmailId resignFirstResponder];
                [self salesEmailFetchForSave];
                //[_tblEmailData reloadData];
                [_tblEmailData scrollRectToVisible:CGRectMake(_tblEmailData.contentSize.width - 1,_tblEmailData.contentSize.height - 1, 1, 1) animated:YES];
                _txtEmailId.text=@"";
            }
        }
    }
    [self finalMails];
}

#pragma mark- SAVE EMAIL TO CORE DATA 12Oct Nilind


-(void)salesEmailFetchForSave
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [requestNew setSortDescriptors:sortDescriptors];
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrDuplicateMail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrDuplicateMail addObject:[matches valueForKey:@"emailId"]];
        }
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"emailId"]);
    }
    NSMutableArray *arrFinalMail;
    arrFinalMail=[[NSMutableArray alloc]init];
    arrFinalMail=arrEmail;
    for (int i=0; i<arrFinalMail.count; i++)
    {
        for (int j=0; j<arrDuplicateMail.count; j++)
        {
            if ([[arrFinalMail objectAtIndex:i] isEqualToString:[arrDuplicateMail objectAtIndex:j]])
            {
                [arrFinalMail removeObjectAtIndex:i];
            }
        }
    }
    [self saveEmailToCoreData:arrFinalMail];
    
}

-(void)saveEmailToCoreData:(NSMutableArray *)arrEmailDetail
{
    for (int j=0; j<arrEmailDetail.count; j++)
    {
        //Email Detail Entity
        entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
        EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
        objEmailDetail.createdBy=@"0";;
        
        objEmailDetail.createdDate=@"0";
        
        objEmailDetail.emailId=[arrEmailDetail objectAtIndex:j];
        
        objEmailDetail.isCustomerEmail=@"true";
        
        objEmailDetail.isMailSent=@"true";
        
        objEmailDetail.leadMailId=@"";
        objEmailDetail.isDefaultEmail=@"false";

        objEmailDetail.modifiedBy=@"";
        
        objEmailDetail.modifiedDate=[global modifyDate];
        
        objEmailDetail.subject=@"";
        
        objEmailDetail.leadId=strLeadId;
        objEmailDetail.userName=strUserName;
        objEmailDetail.companyKey=strCompanyKey;
        
        NSError *error1;
        
        [context save:&error1];
        
    }
    [self salesEmailFetch];
    [_tblEmailData reloadData];
    
}

//Nilind 17 Oct
-(void)updateMailStatus:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    [request setEntity:entityEmailDetail];
    
    NSError *error1 = nil;
    matches=indexValue;
    if (chkClickStatus==YES)
    {
        [matches setValue:@"true" forKey:@"isMailSent"];
    }
    else
    {
        [matches setValue:@"false" forKey:@"isMailSent"];
    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    [context save:&error1];
    [self salesEmailFetch];
}

//....................................
//....................................
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
//------------------------------------------

- (IBAction)actionOnInitialSetupDummy:(id)sender
{
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    InitialSetUp *objInitialSetUp=[storyBoard instantiateViewControllerWithIdentifier:@"InitialSetUp"];
    [self presentViewController:objInitialSetUp animated:YES completion:nil];
}
//Nilind 21 Oct

-(void)updateLeadIdDetail
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        matches=arrAllObj[0];
        [matches setValue:@"Complete" forKey:@"statusSysName"];
        [context save:&error1];
        
    }
}
//Nilind 24 Oct
-(void)fetchPrimarySeconddaryMail
{
    
    
    arrPrimarySecondaryMail=[[NSMutableArray alloc]init];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            [arrPrimarySecondaryMail addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"primaryEmail"]]];
            [arrPrimarySecondaryMail addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"secondaryEmail"]]];
            
        }
    }
}

//Nilind 24 Oct

-(void)savePrimaryEmailToCoreData:(NSString *)str
{
    
    //Email Detail Entity
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
    objEmailDetail.createdBy=@"0";;
    
    objEmailDetail.createdDate=@"0";
    
    objEmailDetail.emailId=str;
    
    objEmailDetail.isCustomerEmail=@"true";
    
    objEmailDetail.isMailSent=@"true";
    
    objEmailDetail.leadMailId=@"";
    objEmailDetail.isDefaultEmail=@"false";

    objEmailDetail.modifiedBy=@"";
    
    objEmailDetail.modifiedDate=[global modifyDate];
    
    objEmailDetail.subject=@"";
    
    objEmailDetail.leadId=strLeadId;
    
    objEmailDetail.userName=strUserName;
    objEmailDetail.companyKey=strCompanyKey;
    
    NSError *error1;
    
    [context save:&error1];
    // [self salesEmailFetch];
}
//..................
#pragma mark-  Fetch Current Services
-(void)fetchCurrentServices
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityCurrentService=[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityCurrentService];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES                                                                                                                                                                                                                                                                                                                              ];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //    NSManagedObject *matchesNew;
    NSMutableArray *arrCurrentServiceValue;
    
    arrCurrentServiceValue=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrCurrentServiceValue forKey:@"CurrentService"];
    }else
    {
        NSMutableArray *arrCurrentService=[[NSMutableArray alloc]init];
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrCurrentServiceKey;
            arrCurrentServiceKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrCurrentServiceKey);
            
            NSLog(@"all keys %@",arrCurrentServiceValue);
            for (int i=0; i<arrCurrentServiceKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]];
                
                [arrCurrentServiceValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrCurrentServiceKey.count; k++) {
                
                NSString *strKeyLeadId=arrCurrentServiceKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrCurrentServiceKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrCurrentServiceKey=arrKeyTemp;
            [arrCurrentServiceValue removeObjectAtIndex:indexToRemove];
            
            [arrCurrentServiceValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictCurrentService;
            dictCurrentService = [NSDictionary dictionaryWithObjects:arrCurrentServiceValue forKeys:arrCurrentServiceKey];
            
            NSLog(@"%@",dictCurrentService);
            [arrCurrentService addObject:dictCurrentService];
            NSLog(@"EmailDetail%@",arrCurrentService);
        }
        [dictFinal setObject:arrCurrentService forKey:@"CurrentService"];
    }
}
//Nilind 27 Dec
-(void)syncCall:(NSString *)str
{
    [self viewDidLoadContent];
    _strWhereToSync=str;
    NSLog(@"strFromSync>>>>%@",str);
    
    
    // Change by saavan for WDO Syncing
    _strFromWhere = str;
    
    
    /* NSUserDefaults *defsLogin=[NSUserDefaults standardUserDefaults];
     NSDictionary *dictLogin=[defsLogin valueForKey:@"LoginDetails"];
     
     strCompanyKey =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.CompanyKey"]];
     strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeeNumber"]];
     strUserName       =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.Username"]];
     
     NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
     NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
     strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];*/
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Data saved offline" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
            
            NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
            
            if ([strAppointmentFlow isEqualToString:@"New"])
            {
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
                AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
                
            }
            else
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
                AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
            
        }else{
            
            NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
            
            NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
            
            if ([strAppointmentFlow isEqualToString:@"New"])
            {
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment"bundle: nil];
                AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
                
            }
            else
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];
                AppointmentView *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
            
        }
        
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];
//        AppointmentView*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
//        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    else
    {
        
        [self FetchFromCoreDataToSendSalesDynamic];
        
    }
    
}

//...............

//Nilind 04 Jan

-(NSString *)fetchLead
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSString *str;
    if (arrAllObj12.count==0)
    {
        
    }
    else
    {
        
        NSManagedObject *matches12=arrAllObj12[0];
        
        str=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"isInitialSetupCreated"]];//isInitialSetupCreated
        
    }
    return str;
    
}

//============================================================================
//============================================================================
#pragma mark- Upload Audio METHOD
//============================================================================
//============================================================================


-(void)uploadAudio :(NSString*)strAudioName
{
    
    if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
    {
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    }
    
    
    NSRange equalRange = [strAudioName rangeOfString:@"\\" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange.location + equalRange.length];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    NSData *audioData;
    
    audioData = [NSData dataWithContentsOfFile:path];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlAudioUploadAsync];
    
    // setting up the request object now
    NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
    [request1 setURL:[NSURL URLWithString:urlString]];
    [request1 setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:audioData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request1 setHTTPBody:body];
    
    // now lets make the connection to the web
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Audio Sent");
    }
    
    [self fialJson];
    
    NSLog(@"Audio Sent Return Message is = = = %@",returnString);
}
-(void)viewDidLoadContent
{
    NSUserDefaults *defsLogin=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLogin=[defsLogin valueForKey:@"LoginDetails"];
    
    strCompanyKey =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.CompanyKey"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeeNumber"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.Username"]];
    
    
    NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
    chkStatus=[dfsStatus boolForKey:@"status"];
    
    
    //.....................
    
    
    
    
    lock=YES;
    
    
    // _tblEmailData.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    // _tblEmailData.layer.borderWidth=1.0;
    
    NSUserDefaults *defsIsService=[NSUserDefaults standardUserDefaults];
    [defsIsService setBool:NO forKey:@"isServiceSurvey"];
    [defsIsService setBool:NO forKey:@"isMechanicalSurvey"];
    [defsIsService setBool:NO forKey:@"isNewSalesSurvey"];
    [defsIsService synchronize];
    
    indexToSendImage=0;
    indexToSendImageSign=0;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    arrImageName=[[NSMutableArray alloc]init];
    //...........
    global = [[Global alloc] init];
    //...........
    
    chk=NO;temp=YES;flag=YES;
    val=0;
    
    // Mutable Array  Declaration
    
    arrEmail=[[NSMutableArray alloc]init];
    arr = [NSMutableArray array];
    arrFinalSend=[[NSMutableArray alloc]init];
    arrFinalJson=[[NSMutableArray alloc]init];
    
    dictFinal=[[NSMutableDictionary alloc]init];
    arrUpdateLeadId=[[NSMutableArray alloc]init];
    arrImageSend=[[NSMutableArray alloc]init];
    arrSaveEmail=[[NSMutableArray alloc]init];
    arrDuplicateMail=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    arrMailChecked=[[NSMutableArray alloc]init];
    arrPrimarySecondaryMail=[[NSMutableArray alloc]init];
    arrSoldCount=[[NSMutableArray alloc]init];
    //..........................................................
    
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    strLeadName=[defsLead valueForKey:@"defsLeadName"];
    
    // _lblFromValue.text=@"explore@quacito.com";//strLeadName;
    
    //Nilind 3'rd Jan
    /* NSUserDefaults *defs12=[NSUserDefaults standardUserDefaults];
     NSString *str;
     str=[defs12 valueForKey:@"fromInitialSetupClick"];
     
     if ([str isEqualToString:@"fromInitialSetupClick"])
     {
     strLeadId=[defs12 valueForKey:@"ForInitialSetUpLeadId"];
     }*/
    //.............
    
    
    
    // Email Fetch
    
    //Nilind 24 Oct
    [self salesEmailFetch];
    [self fetchPrimarySeconddaryMail];
    for (int i=0; i<arrPrimarySecondaryMail.count; i++)
    {
        if ([arrEmail containsObject:[arrPrimarySecondaryMail objectAtIndex:i]])
        {
        }
        else
        {
            NSString *str;
            str=[arrPrimarySecondaryMail objectAtIndex:i];
            if(str.length>3)
            {
                [self savePrimaryEmailToCoreData:str];
            }
        }
        // }
    }
    [self salesEmailFetch];
    //........................................................
    
    
    
    [self finalMails];
    
    NSString *strIsGGQIntegration=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsGGQIntegration"]];
    NSString *strSurveyId=[defsLead valueForKey:@"SurveyID"];
    
    
    //_btnInitialSetupDummy.hidden=YES;
    if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveyId.length>0))
    {
        //   [_btnSurveyy setHidden:NO];
        //  _btnInitialSetupDummy.hidden=YES;
        // _btnInitialSetup.hidden=NO;
        // _btnInitialSetup.hidden=YES;
        
    }
    else
    {
        [_btnSurveyy setHidden:YES];
        
        //_btnInitialSetup.hidden=YES;
        
        _btnInitialSetupDummy.hidden=NO;
        _btnInitialSetupDummy.hidden=YES;
    }
    //Nilind 17 Oct
    
    arrCheckEmail=[[NSMutableArray alloc]init];
    arrUncheckEmail=[[NSMutableArray alloc]init];
    
    
}
-(void)savePaymentInfo
{
    // For Payment Info
    //PaymentInfo Entity
    
    
    
    entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    PaymentInfo *objentityPaymentInfo = [[PaymentInfo alloc]initWithEntity:entityPaymentInfo insertIntoManagedObjectContext:context];
    
    objentityPaymentInfo.leadId=strLeadId;
    
    objentityPaymentInfo.leadPaymentDetailId=@"";
    objentityPaymentInfo.paymentMode=@"";
    objentityPaymentInfo.amount=@"";
    objentityPaymentInfo.checkNo= @"";
    objentityPaymentInfo.licenseNo=@"";
    
    objentityPaymentInfo.expirationDate=@"";
    objentityPaymentInfo.specialInstructions=@"";
    objentityPaymentInfo.agreement=@"";
    objentityPaymentInfo.proposal=@"";
    objentityPaymentInfo.customerSignature=@"";
    
    objentityPaymentInfo.salesSignature=@"";
    objentityPaymentInfo.createdBy=@"";
    objentityPaymentInfo.createdDate=@"";
    objentityPaymentInfo.modifiedBy=@"";
    objentityPaymentInfo.modifiedDate=[global modifyDate];
    
    //Nilind 16 Nov
    
    objentityPaymentInfo.companyKey=strCompanyKey;
    objentityPaymentInfo.userName=strUserName;
    
    //.............
    NSError *error1;
    [context save:&error1];
    
    [self fetchPaymentInfo];
    //........................................................................
}
-(void)fetchElectronicAuthorizedForm
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityElectronicAuthorizedForm = [NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
    [request setEntity:entityElectronicAuthorizedForm];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //arrCheckImage=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"ElectronicAuthorizationForm"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
                
                NSString  *strCustomerImage=[matches valueForKey:@"signaturePath"];
                if (!(strCustomerImage.length==0)) {
                    
                    strElectronicSign=strCustomerImage;
                    
                }
                NSArray *arrElectronicAuthorizedFormKey;
                NSMutableArray *arrElectronicAuthorizedFormValue;
                
                arrElectronicAuthorizedFormValue=[[NSMutableArray alloc]init];
                arrElectronicAuthorizedFormKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrElectronicAuthorizedFormKey);
                
                NSLog(@"all keys %@",arrElectronicAuthorizedFormValue);
                for (int i=0; i<arrElectronicAuthorizedFormKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrElectronicAuthorizedFormKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrElectronicAuthorizedFormKey objectAtIndex:i]]];
                    
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                    
                    [arrElectronicAuthorizedFormValue addObject:str];
                }
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                NSDictionary *dictPaymentInfo;
                dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrElectronicAuthorizedFormValue forKeys:arrElectronicAuthorizedFormKey];
                NSLog(@"ElectronicAuthorizationForm%@",dictPaymentInfo);
                [dictFinal setObject:dictPaymentInfo forKey:@"ElectronicAuthorizationForm"];
            }
            
        }
    }
}
//Upload Electronic Signature Image
-(void)uploadSignatureImageOnServer:(NSString *)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
        
    }
    else
    {
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainSalesProcess=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"]];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        NSString *strCheckUrl;
        strCheckUrl=@"api/File/UploadSignatureAsync";
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainSalesProcess,strCheckUrl];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
            
        }
        
    }
    
    
}
-(void)fetchForAppliedDiscountFromCoreData
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadAppliedDiscounts= [NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    [request setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadAppliedDiscount;
    arrLeadAppliedDiscount=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadAppliedDiscount forKey:@"LeadAppliedDiscounts"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                if(![str isKindOfClass:[NSString class]])
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadAppliedDiscounts%@",dictPaymentInfo);
            [arrLeadAppliedDiscount addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadAppliedDiscount forKey:@"LeadAppliedDiscounts"];
    }
}
#pragma mark- ----------- Fetch Record For Clark Pest -------------

#pragma mark- ------ Fetch Scope --------

-(void)fetchScopeFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialScopeExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialScopeExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialScopeExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialScopeExtDc"];
    }
    
}
#pragma mark- ------- Fetch Target --------

-(void)fetchTargetFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTargetExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTargetExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSMutableArray *arrImage;
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *strLeadCommercialTargetId, *strMobileTargetId;
                strLeadCommercialTargetId = [matches valueForKey:@"leadCommercialTargetId"];
                strMobileTargetId = [matches valueForKey:@"mobileTargetId"];
                
                arrImage = [[NSMutableArray alloc]init];
                arrImage = [self fetchTargetImageDetail:strLeadCommercialTargetId MobileTargetId:strMobileTargetId];
            }
            
//            NSDictionary *dictPaymentInfo;
//            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            
            NSMutableDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSMutableDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            [dictPaymentInfo setObject:arrImage forKey:@"targetImageDetailExtDc"];
            NSLog(@"LeadCommercialTargetExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTargetExtDc"];
    }
    [self fetchAllTargetImages];
    
}

#pragma mark- ------- Fetch LeadCommercialInitialInfoFromCoreData --------

-(void)fetchLeadCommercialInitialInfoFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialInitialInfoExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    // arrAllObj=[[NSArray alloc]init];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialInitialInfoExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialInitialInfoExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialInitialInfoExtDc"];
    }
    
    
    
}
#pragma mark- ------- Fetch LeadCommercialMaintInfoFromCoreData --------

-(void)fetchLeadCommercialMaintInfoFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialMaintInfoExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialMaintInfoExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialMaintInfoExtDc"];
    }
    
    
}
#pragma mark- ------- Fetch MultiTermsFromCoreDataClarkPest --------

-(void)fetchMultiTermsFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTermsExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTermsExtDc"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialTermsExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTermsExtDc"];
        
    }
}

#pragma mark- ------- Fetch LeadCommercialDiscountExtDc --------

-(void)fetchForAppliedDiscountFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialDiscountExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                if(![str isKindOfClass:[NSString class]])
                {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialDiscountExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialDiscountExtDc"];
    }
    
}
#pragma mark- ------- Fetch LeadCommercialDetailExtDc --------


-(void)fetchLeadCommercialDetailExtDcFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadCommercialDetailExtDc"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictLeadPreference;
            dictLeadPreference = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadCommercialDetailExtDc%@",dictLeadPreference);
            [dictFinal setObject:dictLeadPreference forKey:@"LeadCommercialDetailExtDc"];
        }
        
        
    }
    
    
}
#pragma mark- ------- Fetch Sales Marketing Content FromCoreDataClarkPest --------

-(void)fetchSalesMarketingContentFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadMarketingContentExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadMarketingContentExtDc"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadMarketingContentExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadMarketingContentExtDc"];
    }
}
#pragma mark: ------------- Fetch Target Image ------------------

-(NSMutableArray *)fetchTargetImageDetail:(NSString *)strLeadCommercialTargetId MobileTargetId:(NSString *)strMobileTargetId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    
    if (strLeadCommercialTargetId.length > 0)
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && leadCommercialTargetId=%@" ,strLeadId, strLeadCommercialTargetId];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && mobileTargetId=%@",strLeadId,strMobileTargetId];
    }
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
                // [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesTargetImage entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictPaymentInfo];
            NSLog(@"TargetImageDetailImageDetail%@",arrImageDetail);
        }
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    return arrImageDetail;
}
-(void)fetchAllTargetImages
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    arrAllTargetImages = [[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
                [arrAllTargetImages addObject:strImageName];
                
            }
        }
        
        
    }
    
}
-(void)uploadTargetImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        //NSString *strCheckUrl;
        //strCheckUrl=@"/api/File/UploadCheckImagesAsync";
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlUploadTargetImage];//UrlSalesImageUpload];//
        //NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesImageUpload];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
            
        }
        
    }
    
    
}
//============================================================================
#pragma mark- --------------- Upload Other Document METHOD -------------------
//============================================================================
//============================================================================
-(void)uploadOtherDocumentImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
            
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,@"/api/File/UploadOtherDocsAsync"];
            //http://192.168.0.218:3333//api/File/UploadAsync
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Document Sent");
            }
            NSLog(@"Document Sent");
            
        }
    
    }
}

-(void)uploadOtherDocuments :(NSString*)strDocName DocumentId:(NSString*)strDocId
{
    NSString* ServerUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,@"/api/File/UploadOtherDocsAsync"];
    
    if ((strDocName.length==0) && [strDocName isEqualToString:@"(null)"]) {
        
    } else {
        
        strDocName = [global strDocNameFromPath:strDocName];
        
        NSRange equalRange = [strDocName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strDocName = [strDocName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strDocName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@",ServerUrl];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strDocName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response On Document Upload = = = =  = = %@",returnString);
        
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Document Sent");
        }
        if (returnString.length > 0)
        {
            [self updateOtherDocumentSyncStatus:strDocId];
        }
        //[{"Name":"pdfOther337825_2020618163749.pdf","Extension":".pdf","RelativePath":"UploadImages\\","Length":500337}]
        
        NSLog(@"Document Sent");
        
    }
}
-(void)fetchOtherDocument
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@",strLeadId,@"Other"];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];

    NSArray *arrAllObjOtherDoucment = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    arrUploadOtherDocument = [[NSMutableArray alloc]init];
    arrUploadOtherDocumentId = [[NSMutableArray alloc]init];

    if (arrAllObjOtherDoucment.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjOtherDoucment.count; k++)
        {
            NSManagedObject *matchesTemp = [arrAllObjOtherDoucment objectAtIndex:k];
            if ([[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isToUpload"]] isEqualToString:@"true"])
            {
                [arrUploadOtherDocument addObject:[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"fileName"]]];
                [arrUploadOtherDocumentId addObject:[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"leadDocumentId"]]];

            }
        }
    }
}

-(void)updateOtherDocumentSyncStatus : (NSString *)strDocId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@ && leadDocumentId=%@",strLeadId,@"Other",strDocId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjTemp = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObjTemp.count==0)
    {
        
    }else
    {
        NSManagedObject *matchesTemp=arrAllObjTemp[0];
        [matchesTemp setValue:@"false" forKey:@"isToUpload"];
        [context save:&error1];
    }
}

-(BOOL)checkOtherDocumentSyncStatus:(NSString *)strDocId
{
    BOOL chkStatus;
    chkStatus = NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@ && leadDocumentId = %@",strLeadId,@"Other",strDocId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];

    NSArray *arrAllObjOtherDoucment = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    if (arrAllObjOtherDoucment.count==0)
    {
        
    }else
    {
        NSManagedObject *matchesTemp = [arrAllObjOtherDoucment objectAtIndex:0];
        if([[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"True"] || [[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"1"])
        {
            chkStatus = YES;
        }
        else
        {
            chkStatus = NO;
        }
    }
    return chkStatus;
}

-(void)uploadGraphXml :(NSString*)strXmlName
{
    
    if ((strXmlName.length==0) && [strXmlName isEqualToString:@"(null)"]) {
        
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        /*strXmlName = [global strDocNameFromPath:strXmlName];

        NSRange equalRange = [strXmlName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strXmlName = [strXmlName substringFromIndex:equalRange.location + equalRange.length];
        }*/
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strXmlName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlUploadXMLSales];

        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strXmlName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response On Graph XML Upload = = = =  = = %@",returnString);
        
        if ([returnString isEqualToString:@"OK"])
        {
            
            NSLog(@"Graph XML Sent");
            
        }
       
    }
    
}
@end



