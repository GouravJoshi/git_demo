//
//  GlobalSyncViewController.h
//  DPS
//
//  Created by Rakesh Jain on 12/01/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface GlobalSyncViewController : UIViewController<NSFetchedResultsControllerDelegate,UITextFieldDelegate>
{
    
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,*entityDocumentsDetail,*entityCurrentService,*entityElectronicAuthorizedForm,*entityLeadAppliedDiscounts,*entityContactNumberDetail,*entityRenewalServiceDetail,*entityTagDetail;
    
    //Clark Pest
    NSEntityDescription *entityLeadCommercialScopeExtDc,*entityLeadCommercialTargetExtDc,*entityLeadCommercialMaintInfoExtDc,*entityLeadCommercialInitialInfoExtDc,*entityLeadCommercialDiscountExtDc,*entityLeadCommercialDetailExtDc,*entityLeadCommercialTermsExtDc,*entityLeadMarketingContentExtDc;
    
    NSEntityDescription *entityModifyDate;
    NSFetchRequest *requestModifyDate;
    NSSortDescriptor *sortDescriptorModifyDate;
    NSArray *sortDescriptorsModifyDate;
    NSManagedObject *matchesModifyDate;
    NSArray *arrAllObjModifyDate;
    
    NSEntityDescription *entitySalesDynamic;
    NSFetchRequest *requestNewSalesDynamic;
    NSSortDescriptor *sortDescriptorSalesDynamic;
    NSArray *sortDescriptorsSalesDynamic;
    NSManagedObject *matchesSalesDynamic;
    NSArray *arrAllObjSalesDynamic;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerModifyDate;
@property (weak, nonatomic) IBOutlet UILabel *lblFromValue;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailId;

@property (weak, nonatomic) IBOutlet UITableView *tblEmailData;


@property (weak, nonatomic) IBOutlet UIView *viewSendMail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;
@property (strong, nonatomic) IBOutlet UIButton *btnSurveyy;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_View_T;
@property (weak, nonatomic) IBOutlet UIButton *btnInitialSetupDummy;

-(void)syncCall:(NSString *)str;
@property(weak,nonatomic) NSString *strForSendProposal;
@property(weak,nonatomic)NSString *strWhereToSync;
@property(strong,nonatomic)NSString *strFromWhere;

@end


