//
//  SalesAutoServiceSummaryTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 01/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesAutoServiceSummaryTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblServiceNameType;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceDescriptionValue;

@property (weak, nonatomic) IBOutlet UILabel *lblInitialPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountValue;
@property (weak, nonatomic) IBOutlet UILabel *lblFinalInitiaPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenance;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenanceValue;

@property (weak, nonatomic) IBOutlet UILabel *lblFrequencyValue;

@property (weak, nonatomic) IBOutlet UILabel *lblFrequency;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Desc_H;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDesc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrnt_TaxtView_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstr_lblNewDesc_H;
@property (weak, nonatomic) IBOutlet UILabel *lblNewDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitBasedFinalInitialPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitBasedFinalInitialPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitBasedFinalMaintPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitBaseFinalMaintPriceVal;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingFrequency;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscoutnPercent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblUnitBasedInitialPrice_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_SeperatorLineRenewalSerivce_H;
@property (weak, nonatomic) IBOutlet UILabel *lblRenewalService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblRenewalService_H;
@property (weak, nonatomic) IBOutlet UIView *viewUnitPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblLineRenewalSeperator1;
@property (weak, nonatomic) IBOutlet UILabel *lblLineRenewalSeperator2;

@end
