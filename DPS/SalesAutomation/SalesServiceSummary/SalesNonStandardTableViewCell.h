//
//  SalesNonStandardTableViewCell.h
//  DPS change
//
//  Created by Rakesh Jain on 11/01/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesNonStandardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNonStanServiceNameType;

@property (weak, nonatomic) IBOutlet UILabel *lblNonStanDepartmentName;

@property (weak, nonatomic) IBOutlet UILabel *lblNonStanNewDesc;

@property (weak, nonatomic) IBOutlet UILabel *lblNonStanInitialPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblNonStanDiscountValue;
@property (weak, nonatomic) IBOutlet UILabel *lblNonStanFinalInitiaPriceValue;

@property (weak, nonatomic) IBOutlet UILabel *lblNonStanFrequencyValue;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblDescNonStan_H;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPercent;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingFreqValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintPriceValue;


@end
