//  changes changes Changes
//  DPS changes Nilind changes changes final changes changes changes
//  Created by Rakesh Jain on 19/07/16. changes changes changes changes
//  Copyright © 2016 Saavan. All rights reserved. change DONES changes
//changes May 2019 Nilind s Nilind Sharma Nilind Sharma Nilind Nilind 


#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "CurrentService.h"
#import <MessageUI/MessageUI.h>


@interface SaleAutomationGeneralInfo : UIViewController<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIDocumentInteractionControllerDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityTask;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entityCurrentService,
    *entitySoldServiceStandardDetail,
    *entityContactNumberDetail;
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewLeads;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
@property(strong,nonatomic)NSManagedObject *matchesGeneralInfo;

- (IBAction)actoinBack:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtLead;

@property (strong, nonatomic) IBOutlet UITextField *txtCustomerName;
@property (strong, nonatomic) IBOutlet UITextField *txtScheduleStartDate;
@property (strong, nonatomic) IBOutlet UITextField *txtAccount;

@property (strong, nonatomic) IBOutlet UITextField *txtPrimaryEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtSecondayEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPrimaryPhone;

@property (strong, nonatomic) IBOutlet UITextField *txtSecondaryPhone;

@property (strong, nonatomic) IBOutlet UIButton *btnStatus;
- (IBAction)actionOnStatus:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnStage;
- (IBAction)actionOnStage:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnPriority;
- (IBAction)actionOnPriority:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnVisibility;

- (IBAction)actionOnVisibility:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSource;

- (IBAction)actionOnSource:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnInspector;
- (IBAction)actionOnInspector:(id)sender;



@property (weak, nonatomic) IBOutlet UITextField *txtCityBillingAddress;

@property (weak, nonatomic) IBOutlet UITextField *txtZipCodeBillingAddress;


///sa/sadf/



@property (weak, nonatomic) IBOutlet UITextField *txtCityServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtZipcodeServiceAddress;
#pragma mark- Outlet ServiceRequired
@property (weak, nonatomic) IBOutlet UITextView *txtViewNotes;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtTagServiceRequired;
- (IBAction)actionOnAttachImage:(id)sender;

- (IBAction)actionOnSaveContinue:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
@property(strong,nonatomic)NSDictionary *dictSaleAuto;
- (IBAction)actionOnSave:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAdd1ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAdd2ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAdd1BillingAddress;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAdd2BillingAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnCountryServiceAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnStateServiceAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnCountryBillingAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnStateBillingAddress;


- (IBAction)actionOnCountryServiceAddress:(id)sender;

- (IBAction)actionOnStateServiceAddress:(id)sender;
- (IBAction)actionOnCountryBillingAddress:(id)sender;
- (IBAction)actionOnStateBillingAddress:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
- (IBAction)actionOnCheckBox:(id)sender;
- (IBAction)actionOnUploadDocument:(id)sender;

//Nilind 30 Sept
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyName;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextView *txtViewCurrentServices;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLblCurrenService_H;

//................................
//Nilind 4 Oct


@property (weak, nonatomic) IBOutlet UITextField *txtBranchName;

//...............................
@property (weak, nonatomic) IBOutlet UILabel *lblNameAcount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ScrollLead_H;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewDetail;
@property(strong,nonatomic)NSString *strFromAppoint;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImages;
@property (weak, nonatomic) IBOutlet UIButton *btnAddGraphImageFooter;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImageFooter;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadDocument;

//Nilind 06 Jan

@property (weak, nonatomic) IBOutlet UILabel *lblBranchName;

@property (weak, nonatomic) IBOutlet UIButton *buttonImg1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImg2;
@property (weak, nonatomic) IBOutlet UIButton *buttonImg3;
- (IBAction)actionOn_buttonImg1:(id)sender;
- (IBAction)actionOn_buttonImg2:(id)sender;
- (IBAction)actionOn_buttonImg3:(id)sender;
//.............

@property (strong, nonatomic) IBOutlet UIButton *btnGlobalSync;
- (IBAction)actionOnGlobalSync:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
//Nilind 08 Feb
@property (weak, nonatomic) IBOutlet UITextField *txtAreaSqFt;
@property (weak, nonatomic) IBOutlet UITextField *txtBedroom;
@property (weak, nonatomic) IBOutlet UITextField *txtBathroom;
@property (weak, nonatomic) IBOutlet UITextField *txtLotSizeSqFt;

@property (weak, nonatomic) IBOutlet UITextField *txtYearBuil;
- (IBAction)actionOnGetPropertyInfo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnGetPropertyInfo;
@property (weak, nonatomic) IBOutlet UITextField *txtNoOfStory;
@property (weak, nonatomic) IBOutlet UITextField *txtShrubArea;
@property (weak, nonatomic) IBOutlet UITextField *txtTurfArea;

//End

//Saavan Changes For Email Id
@property (strong, nonatomic) IBOutlet UIButton *btnPrimaryEmail;
- (IBAction)action_PrimaryEmail:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSecondaryEmail;
- (IBAction)action_SecondaryEmail:(id)sender;
//End Of Changes by Saavan
- (IBAction)action_AddGraph:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraph;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewGraph;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_BtnSavePriority;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAccountDesc;

//Nilind 04 Aug ////////
//Nilind 03 August change
@property (weak, nonatomic) IBOutlet UITextField *txtMapCodeServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtGateCodeServiceNotes;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAddressNotesServiceAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDirectionServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtMapCodeBillingAddress;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAddImage;
@property (weak, nonatomic) IBOutlet UITextField *txtLinearSqFt;
@property (weak, nonatomic) IBOutlet UITextField *txtCellNo;
//Service POC
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCPrimaryEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCSecondaryEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCCellNo;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCPrimaryPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCSecondaryPhone;

//Billing POC Change
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCPrimaryEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCSecondaryEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCCellNo;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCPrimaryPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCSecondaryPhone;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;

//Service POC
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constServicePOCDetail_H;
@property (weak, nonatomic) IBOutlet UIView *viewServicePOCDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnShowServicePOCDetail;
- (IBAction)actionOnShowServicePOCDetail:(id)sender;
- (IBAction)actionOnEditServicePOCDetail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEditServicePOCDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnSendMailServicePOC;
- (IBAction)actionOnSendMailServicePOC:(id)sender;

//Billing POC
@property (weak, nonatomic) IBOutlet UIView *viewBillingPOCDetail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constantBillingPOCDetail_H;
@property (weak, nonatomic) IBOutlet UIButton *btnEditBillingPOCDetail;
- (IBAction)actionOnEditBillingPOCDetail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnShowBillingPOCDetail;
- (IBAction)actionOnShowBillingPOCDetail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSendMailBillingPOC;
- (IBAction)actionOnSendMailBillingPOC:(id)sender;
- (IBAction)actionOnCallBillingPOCCell:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constView1FisrtName_H;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constView1FullName_H;
@property (weak, nonatomic) IBOutlet UILabel *lblView1FullName;

@property (strong, nonatomic) IBOutlet UIView *view2;

@property (weak, nonatomic) IBOutlet UIButton *btnCellNo;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondaryPhone;
- (IBAction)actionOnCellNo:(id)sender;

- (IBAction)actionCallPrimaryPhone:(id)sender;
- (IBAction)actionCallSecondaryPhone:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *view3;
@property (strong, nonatomic) IBOutlet UIView *view4;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constView3ServiceAddress_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblView3NewSerivceAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constView3newlblServiceAddress_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constServiceContacAllName_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constServiceContactFullName_H;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceContactFullName;

- (IBAction)actionOnNewServiceAddress:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactPrimaryEmail;
- (IBAction)actionServiceContactPrimaryEmail:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactSecondaryEmail;
- (IBAction)actionOnServiceContactSecondaryEmail:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactCell;
- (IBAction)actionOnServiceContactCell:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactPrimaryPhone;
- (IBAction)actionOnServiceContactPrimaryPhone:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactSecondaryPhone;
- (IBAction)actionOnServiceContactSecondaryPhone:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *view5;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constView5BillingAddress_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constView5NewBillingAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBillingContactAllName_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBillingContactFullName_H;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingContactFullName;

@property (strong, nonatomic) IBOutlet UIView *view6;

- (IBAction)actionOnBillingAddress:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactPrimaryEmail;
- (IBAction)actionBillingContactPrimaryEmail:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactSecondaryEmail;
- (IBAction)actionOnBillingContactSecondaryEmail:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactCell;
- (IBAction)actionOnBillingContactCell:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactPrimaryPhone;
- (IBAction)actionOnBillingContactPrimaryPhone:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactSecondaryPhone;
- (IBAction)actionOnBillingContactSecondaryPhone:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *view7;
- (IBAction)actionEditCustomerInfo:(id)sender;
- (IBAction)actionShowCustomerInfo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEditCustomerInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnShowCustomerInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnEditServiceAddress;
- (IBAction)actionOnEditServiceAddress:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnShowServiceAddress;
- (IBAction)actionOnShowServiceAddress:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEditBillingAddress;
- (IBAction)actionOnEditBillingAddress:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnShowBillingAddress;
- (IBAction)actionOnShowBillingAddress:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblNewServiceAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblNewBillingAddress;


@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryEmailNew;
- (IBAction)actionOnPrimaryEmailNew:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewProblemescription;
@property (weak, nonatomic) IBOutlet UIButton *btnLeadTypeInside;
- (IBAction)actiononLeadTypeInside:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnLeadTypeField;
- (IBAction)actiononLeadTypeField:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceCounty;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceSchoolDistrict;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingCounty;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingSchoolDistrict;
- (IBAction)actionOnChemicalSensitivityList:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTaxCode;
- (IBAction)actionOnTaxCode:(id)sender;
//Drive Time

@property (weak, nonatomic) IBOutlet UITextField *txtDriveTime;
@property (weak, nonatomic) IBOutlet UITextField *txtEarliestStartTime;
@property (weak, nonatomic) IBOutlet UITextField *txtLatestStartTime;

@property (weak, nonatomic) IBOutlet UIButton *btnPropertyType;
- (IBAction)actionOnProprtyType:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewPropertyType_H;

@property (weak, nonatomic) IBOutlet UIButton *btnOppTypeResidential;
@property (weak, nonatomic) IBOutlet UIButton *btnOppTypeCommercial;
- (IBAction)actionOnResidential:(id)sender;
- (IBAction)actionOnCommercial:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnReopen;
- (IBAction)actionOnReopen:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceAddressSubType;
- (IBAction)actionOnServiceAddressSubType:(id)sender;

@end
