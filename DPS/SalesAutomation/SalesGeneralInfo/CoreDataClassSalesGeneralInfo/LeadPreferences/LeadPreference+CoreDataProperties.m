//
//  LeadPreference+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LeadPreference+CoreDataProperties.h"

@implementation LeadPreference (CoreDataProperties)

@dynamic createdBy;
@dynamic createdDate;
@dynamic leadPreferenceId;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic preferredDays;
@dynamic preferredEndTime;
@dynamic preferredStartDate;
@dynamic preferredStartTime;
@dynamic preferredTechnician;
@dynamic leadId;
@dynamic userName;
@dynamic companyKey;

@end
