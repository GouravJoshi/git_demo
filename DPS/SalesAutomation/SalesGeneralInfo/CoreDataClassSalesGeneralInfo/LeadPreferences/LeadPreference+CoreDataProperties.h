//
//  LeadPreference+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LeadPreference.h"

NS_ASSUME_NONNULL_BEGIN

@interface LeadPreference (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *leadPreferenceId;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *preferredDays;
@property (nullable, nonatomic, retain) NSString *preferredEndTime;
@property (nullable, nonatomic, retain) NSString *preferredStartDate;
@property (nullable, nonatomic, retain) NSString *preferredStartTime;
@property (nullable, nonatomic, retain) NSString *preferredTechnician;
@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;

@end

NS_ASSUME_NONNULL_END
