//
//  EmailDetail+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EmailDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface EmailDetail (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *emailId;
@property (nullable, nonatomic, retain) NSString *isCustomerEmail;
@property (nullable, nonatomic, retain) NSString *isMailSent;
@property (nullable, nonatomic, retain) NSString *leadMailId;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *subject;
@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;//isDefaultEmail
@property (nullable, nonatomic, retain) NSString *isDefaultEmail;//isDefaultEmail

@end

NS_ASSUME_NONNULL_END
