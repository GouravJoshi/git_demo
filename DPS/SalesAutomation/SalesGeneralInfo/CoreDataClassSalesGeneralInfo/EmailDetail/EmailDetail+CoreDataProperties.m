//
//  EmailDetail+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EmailDetail+CoreDataProperties.h"

@implementation EmailDetail (CoreDataProperties)

@dynamic createdBy;
@dynamic createdDate;
@dynamic emailId;
@dynamic isCustomerEmail;
@dynamic isMailSent;
@dynamic leadMailId;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic subject;
@dynamic leadId;
@dynamic userName;
@dynamic companyKey;
@dynamic isDefaultEmail;

@end
