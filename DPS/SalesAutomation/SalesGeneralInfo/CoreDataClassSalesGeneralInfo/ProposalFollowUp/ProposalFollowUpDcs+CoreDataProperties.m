//
//  ProposalFollowUpDcs+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 16/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "ProposalFollowUpDcs+CoreDataProperties.h"

@implementation ProposalFollowUpDcs (CoreDataProperties)

+ (NSFetchRequest<ProposalFollowUpDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ProposalFollowUpDcs"];
}

@dynamic proposalFollowupDate;
@dynamic proposalId;
@dynamic proposalLeadId;
@dynamic proposalNotes;
@dynamic proposalServiceId;
@dynamic proposalServiceName;
@dynamic proposalServiceSysName;

@end
