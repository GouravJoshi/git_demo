//
//  ProposalFollowUpDcs+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 16/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "ProposalFollowUpDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ProposalFollowUpDcs (CoreDataProperties)

+ (NSFetchRequest<ProposalFollowUpDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *proposalFollowupDate;
@property (nullable, nonatomic, copy) NSString *proposalId;
@property (nullable, nonatomic, copy) NSString *proposalLeadId;
@property (nullable, nonatomic, copy) NSString *proposalNotes;
@property (nullable, nonatomic, copy) NSString *proposalServiceId;
@property (nullable, nonatomic, copy) NSString *proposalServiceName;
@property (nullable, nonatomic, copy) NSString *proposalServiceSysName;

@end

NS_ASSUME_NONNULL_END
