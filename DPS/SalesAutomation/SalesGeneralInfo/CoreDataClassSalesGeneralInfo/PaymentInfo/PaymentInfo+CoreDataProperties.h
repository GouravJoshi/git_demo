//
//  PaymentInfo+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PaymentInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface PaymentInfo (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *leadPaymentDetailId;
@property (nullable, nonatomic, retain) NSString *paymentMode;
@property (nullable, nonatomic, retain) NSString *amount;
@property (nullable, nonatomic, retain) NSString *checkNo;
@property (nullable, nonatomic, retain) NSString *licenseNo;
@property (nullable, nonatomic, retain) NSString *expirationDate;
@property (nullable, nonatomic, retain) NSString *specialInstructions;
@property (nullable, nonatomic, retain) NSString *agreement;
@property (nullable, nonatomic, retain) NSString *proposal;
@property (nullable, nonatomic, retain) NSString *customerSignature;
@property (nullable, nonatomic, retain) NSString *salesSignature;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *checkBackImagePath;
@property (nullable, nonatomic, retain) NSString *checkFrontImagePath;
@property (nullable, nonatomic, retain) NSString *inspection;

@end

NS_ASSUME_NONNULL_END
