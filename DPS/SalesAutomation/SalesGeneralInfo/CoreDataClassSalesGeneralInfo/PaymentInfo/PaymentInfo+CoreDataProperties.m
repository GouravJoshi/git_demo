//
//  PaymentInfo+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PaymentInfo+CoreDataProperties.h"

@implementation PaymentInfo (CoreDataProperties)

@dynamic leadId;
@dynamic leadPaymentDetailId;
@dynamic paymentMode;
@dynamic amount;
@dynamic checkNo;
@dynamic licenseNo;
@dynamic expirationDate;
@dynamic specialInstructions;
@dynamic agreement;
@dynamic proposal;
@dynamic customerSignature;
@dynamic salesSignature;
@dynamic createdBy;
@dynamic createdDate;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic userName;
@dynamic companyKey;
@dynamic checkBackImagePath;
@dynamic checkFrontImagePath;
@dynamic inspection;
@end
