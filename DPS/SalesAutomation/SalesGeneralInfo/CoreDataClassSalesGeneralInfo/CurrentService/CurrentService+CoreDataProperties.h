//
//  CurrentService+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 01/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CurrentService.h"

NS_ASSUME_NONNULL_BEGIN

@interface CurrentService (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *serviceName;
@property (nullable, nonatomic, retain) NSString *technicianName;
@property (nullable, nonatomic, retain) NSString *frequency;
@property (nullable, nonatomic, retain) NSString *serviceDateTime;
@property (nullable, nonatomic, retain) NSString *serviceTimeSlot;
@property (nullable, nonatomic, retain) NSString *frequencyId;
@property (nullable, nonatomic, retain) NSString *serviceSysName;
@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *serviceId;
@property (nullable, nonatomic, retain) NSString *serviceKey;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;

@end

NS_ASSUME_NONNULL_END
