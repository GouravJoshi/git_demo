//
//  CurrentService+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 01/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CurrentService+CoreDataProperties.h"

@implementation CurrentService (CoreDataProperties)

@dynamic serviceName;
@dynamic technicianName;
@dynamic frequency;
@dynamic serviceDateTime;
@dynamic serviceTimeSlot;
@dynamic frequencyId;
@dynamic serviceSysName;
@dynamic leadId;
@dynamic serviceId;
@dynamic serviceKey;
@dynamic userName;
@dynamic companyKey;

@end
