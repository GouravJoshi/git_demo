//
//  LeadDetail+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 05/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LeadDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface LeadDetail (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *accountNo;
@property (nullable, nonatomic, retain) NSString *billedAmount;
@property (nullable, nonatomic, retain) NSString *billingAddress1;
@property (nullable, nonatomic, retain) NSString *billingAddress2;
@property (nullable, nonatomic, retain) NSString *billingCity;
@property (nullable, nonatomic, retain) NSString *billingCountry;
@property (nullable, nonatomic, retain) NSString *billingState;
@property (nullable, nonatomic, retain) NSString *billingZipcode;
@property (nullable, nonatomic, retain) NSString *branchSysName;
@property (nullable, nonatomic, retain) NSString *closeDate;
@property (nullable, nonatomic, retain) NSString *collectedAmount;
@property (nullable, nonatomic, retain) NSString *companyId;
@property (nullable, nonatomic, retain) NSString *companyMappingKey;
@property (nullable, nonatomic, retain) NSString *companyName;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *customerName;
@property (nullable, nonatomic, retain) NSString *departmentSysName;
@property (nullable, nonatomic, retain) NSString *descriptionLeadDetail;
@property (nullable, nonatomic, retain) NSString *divisionSysName;
@property (nullable, nonatomic, retain) NSString *estimatedValue;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *inspectionCompletedTime;
@property (nullable, nonatomic, retain) NSString *inspectionDate;
@property (nullable, nonatomic, retain) NSString *isAgreementGenerated;
@property (nullable, nonatomic, retain) NSString *isInspectionCompleted;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *latitude;
@property (nullable, nonatomic, retain) NSString *leadGeneralTermsConditions;
@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *leadName;
@property (nullable, nonatomic, retain) NSString *leadNumber;
@property (nullable, nonatomic, retain) NSString *longitude;
@property (nullable, nonatomic, retain) NSString *middleName;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *notes;
@property (nullable, nonatomic, retain) NSString *primaryEmail;
@property (nullable, nonatomic, retain) NSString *primaryPhone;
@property (nullable, nonatomic, retain) NSString *primaryServiceSysName;
@property (nullable, nonatomic, retain) NSString *prioritySysName;
@property (nullable, nonatomic, retain) NSString *salesRepId;
@property (nullable, nonatomic, retain) NSString *scheduleEndDate;
@property (nullable, nonatomic, retain) NSString *scheduleStartDate;
@property (nullable, nonatomic, retain) NSString *secondaryEmail;
@property (nullable, nonatomic, retain) NSString *secondaryPhone;
@property (nullable, nonatomic, retain) NSString *serviceAddress2;
@property (nullable, nonatomic, retain) NSString *serviceCity;
@property (nullable, nonatomic, retain) NSString *serviceCountry;
@property (nullable, nonatomic, retain) NSString *servicesAddress1;
@property (nullable, nonatomic, retain) NSString *serviceState;
@property (nullable, nonatomic, retain) NSString *serviceZipcode;
@property (nullable, nonatomic, retain) NSString *sourceSysName;
@property (nullable, nonatomic, retain) NSString *stageSysName;
@property (nullable, nonatomic, retain) NSString *statusSysName;
@property (nullable, nonatomic, retain) NSString *tags;
@property (nullable, nonatomic, retain) NSString *tax;
@property (nullable, nonatomic, retain) NSDate *dateModified;//dateScheduledStart
@property (nullable, nonatomic, retain) NSDate *zdateScheduledStart;
@property (nullable, nonatomic, retain) NSString *surveyID;
@property (nullable, nonatomic, retain) NSString *isProposalFromMobile;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *employeeNo_Mobile;
@property (nullable, nonatomic, retain) NSString*isInitialSetupCreated;
@property (nullable, nonatomic, retain) NSString*audioFilePath;
@property (nullable, nonatomic, retain) NSString *deviceName;
@property (nullable, nonatomic, retain) NSString *deviceVersion;
@property (nullable, nonatomic, retain) NSString *versionDate;
@property (nullable, nonatomic, retain) NSString *versionNumber;
@property (nullable, nonatomic, retain) NSString*iAgreeTerms;
@property (nullable, nonatomic, retain) NSString*isCustomerNotPresent;
@property (nullable, nonatomic, retain) NSString*isBillingAddressSame;
@property (nullable, nonatomic, retain) NSString*isAgreementSigned;
@property (nullable, nonatomic, retain) NSString*zSync;
@property (nullable, nonatomic, retain) NSString*area;
@property (nullable, nonatomic, retain) NSString*noOfBathroom;
@property (nullable, nonatomic, retain) NSString*noOfBedroom;
@property (nullable, nonatomic, retain) NSString*lotSizeSqFt;
@property (nullable, nonatomic, retain) NSString*yearBuilt;//isEmployeePresetSignature
@property (nullable, nonatomic, retain) NSString*isEmployeePresetSignature;
@property (nullable, nonatomic, retain) NSString *serviceAddressSubType;
@property (nullable, nonatomic, retain) NSString *serviceAddrTaxExemptionNo;
@property (nullable, nonatomic, retain) NSString *isServiceAddrTaxExempt;
@property (nullable, nonatomic, retain) NSString *taxableAmount;
@property (nullable, nonatomic, retain) NSString *taxAmount;
@property (nullable, nonatomic, retain) NSString *totalPrice;
//IsCouponApplied
@property (nullable, nonatomic, retain) NSString *isCouponApplied;
@property (nullable, nonatomic, retain) NSString *couponDiscount;
@property (nullable, nonatomic, retain) NSString *subTotalAmount;

@property (nullable, nonatomic, retain) NSString *serviceDirection;
@property (nullable, nonatomic, retain) NSString *serviceGateCode;
@property (nullable, nonatomic, retain) NSString *serviceNotes;

@property (nullable, nonatomic, retain) NSString *billingMapcode;
@property (nullable, nonatomic, retain) NSString *serviceMapCode;
@property (nullable, nonatomic, retain) NSString *accountDescription;//linearSqFt
@property (nullable, nonatomic, retain) NSString *linearSqFt;
@property (nullable, nonatomic, retain) NSString *surveyStatus;
@property (nullable, nonatomic, retain) NSString *isResendAgreementProposalMail;


@property (nullable, nonatomic, retain) NSString *cellNo;
@property (nullable, nonatomic, retain) NSString *billingFirstName;
@property (nullable, nonatomic, retain) NSString *billingMiddleName;
@property (nullable, nonatomic, retain) NSString *billingLastName;
@property (nullable, nonatomic, retain) NSString *billingPrimaryEmail;
@property (nullable, nonatomic, retain) NSString *billingSecondaryEmail;
@property (nullable, nonatomic, retain) NSString *billingCellNo;
@property (nullable, nonatomic, retain) NSString *billingPrimaryPhone;
@property (nullable, nonatomic, retain) NSString *billingSecondaryPhone;

@property (nullable, nonatomic, retain) NSString *serviceFirstName;
@property (nullable, nonatomic, retain) NSString *serviceMiddleName;
@property (nullable, nonatomic, retain) NSString *serviceLastName;
@property (nullable, nonatomic, retain) NSString *servicePrimaryEmail;
@property (nullable, nonatomic, retain) NSString *serviceSecondaryEmail;
@property (nullable, nonatomic, retain) NSString *serviceCellNo;
@property (nullable, nonatomic, retain) NSString *servicePrimaryPhone;
@property (nullable, nonatomic, retain) NSString *serviceSecondaryPhone;
//Nilind 8 Dec
@property (nullable, nonatomic, retain) NSString *onMyWaySentSMSTime;
@property (nullable, nonatomic, retain) NSString *onMyWaySentSMSTimeLatitude;
@property (nullable, nonatomic, retain) NSString *onMyWaySentSMSTimeLongitude;
@property (nullable, nonatomic, retain) NSString *strPreferredMonth;
@property (nullable, nonatomic, retain) NSString *salesType;
@property (nullable, nonatomic, retain) NSString *problemDescription;
@property (nullable, nonatomic, retain) NSString *accountElectronicAuthorizationFormSigned;
@property (nullable, nonatomic, retain) NSString *taxableMaintAmount;
@property (nullable, nonatomic, retain) NSString *taxMaintAmount;
@property (nullable, nonatomic, retain) NSString *billingCounty;
@property (nullable, nonatomic, retain) NSString *billingSchoolDistrict;
@property (nullable, nonatomic, retain) NSString *serviceCounty;
@property (nullable, nonatomic, retain) NSString *serviceSchoolDistrict;
@property (nullable, nonatomic, retain) NSString *noOfStory
;
@property (nullable, nonatomic, retain) NSString *shrubArea;
@property (nullable, nonatomic, retain) NSString *turfArea;
@property (nullable, nonatomic, retain) NSString *thirdPartyAccountNo;
@property (nullable, nonatomic, retain) NSString *taxSysName;
@property (nullable, nonatomic, retain) NSString *totalMaintPrice;
//For Commercial Flow
@property (nullable, nonatomic, retain) NSString *flowType;
@property (nullable, nonatomic, retain) NSString *initialServiceDate;
@property (nullable, nonatomic, retain) NSString *recurringServiceMonth;
@property (nullable, nonatomic, retain) NSString *subTotalMaintAmount;
@property (nullable, nonatomic, retain) NSString *proposalNotes;


@property (nullable, nonatomic, retain) NSString *driveTime;
@property (nullable, nonatomic, retain) NSString *earliestStartTime;
@property (nullable, nonatomic, retain) NSString *latestStartTime;

@property(nullable,nonatomic,retain)NSString *fromDate;
@property(nullable,nonatomic,retain)NSString *toDate;
@property(nullable,nonatomic,retain)NSString *modifyDate;


@property(nullable,nonatomic,retain)NSString *accountManagerId;
@property(nullable,nonatomic,retain)NSString *accountManagerName;
@property(nullable,nonatomic,retain)NSString *accountManagerNo;
@property(nullable,nonatomic,retain)NSString *accountManagerEmail;
@property(nullable,nonatomic,retain)NSString *accountManagerPrimaryPhone;
@property(nullable,nonatomic,retain)NSString *propertyType;
@property(nullable,nonatomic,retain)NSString *serviceAddressPropertyTypeSysName;
@property(nullable,nonatomic,retain)NSString *profileImage;
@property(nullable,nonatomic,retain)NSString *isSelectionAllowedForCustomer;
@property(nullable,nonatomic,retain)NSString *otherDiscount;
@property(nullable,nonatomic,retain)NSString *tipDiscount;
@property(nullable,nonatomic,retain)NSString *isCommLeadUpdated;
@property(nullable,nonatomic,retain)NSString *isMailSend;
@property(nullable,nonatomic,retain)NSString *isInvisible;//leadInspectionFee
@property(nullable,nonatomic,retain)NSString *leadInspectionFee;//leadInspectionFee IsTipEligible
@property(nullable,nonatomic,retain)NSString *isTipEligible;
@property(nullable,nonatomic,retain)NSString *needTipService;
@property(nullable,nonatomic,retain)NSString *isWdoLead;
@property(nullable,nonatomic,retain)NSString *isCommTaxUpdated;//accountName
@property(nullable,nonatomic,retain)NSString *accountName;//
@property(nullable,nonatomic,retain)NSString *buildingPermitAmount;
@property(nullable,nonatomic,retain)NSString *isReopen;
@property(nullable,nonatomic,retain)NSString *templateKey;
@property(nullable,nonatomic,retain)NSString *pdfPath;
@property(nullable,nonatomic,retain)NSString *smsReminders;
@property(nullable,nonatomic,retain)NSString *phoneReminders;
@property(nullable,nonatomic,retain)NSString *emailReminders;
@property(nullable,nonatomic,retain)NSString *timeRangeId;
@property(nullable,nonatomic,retain)NSString *serviceAddressImagePath;
@property(nullable,nonatomic,retain)NSString *scheduleTimeType;
@property(nullable,nonatomic,retain)NSString *scheduleDate;
@property(nullable,nonatomic,retain)NSString *scheduleTime;
@property(nullable,nonatomic,retain)NSString *billToLocationId;
@property(nullable,nonatomic,retain)NSString *cardAccountNumber;
@property(nullable,nonatomic,retain)NSString *creditCardType;
@property(nullable,nonatomic,retain)NSString *isPreview;

@end

NS_ASSUME_NONNULL_END
