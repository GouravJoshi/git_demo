//
//  ImageDetail+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ImageDetail+CoreDataProperties.h"

@implementation ImageDetail (CoreDataProperties)

@dynamic createdBy;
@dynamic createdDate;
@dynamic descriptionImageDetail;
@dynamic leadImageId;
@dynamic leadImagePath;
@dynamic leadImageType;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic leadId;
@dynamic userName;
@dynamic companyKey;
@dynamic leadImageCaption;
@dynamic latitude;
@dynamic longitude;
@dynamic isAddendum;
@dynamic isNewGraph;
@dynamic leadXmlPath;
@dynamic isImageSyncforMobile;
@dynamic isInternalUsage;

@end
