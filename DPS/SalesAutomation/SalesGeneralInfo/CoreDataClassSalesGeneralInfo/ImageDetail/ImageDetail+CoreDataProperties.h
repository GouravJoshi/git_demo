//
//  ImageDetail+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ImageDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface ImageDetail (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *descriptionImageDetail;
@property (nullable, nonatomic, retain) NSString *leadImageId;
@property (nullable, nonatomic, retain) NSString *leadImagePath;
@property (nullable, nonatomic, retain) NSString *leadImageType;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;//leadImageCaption
@property (nullable, nonatomic, retain) NSString *leadImageCaption;
@property (nullable, nonatomic, retain) NSString *latitude;
@property (nullable, nonatomic, retain) NSString *longitude;
@property (nullable, nonatomic, retain) NSString *isAddendum; //isNewGraph leadXmlPath
@property (nullable, nonatomic, retain) NSString *isNewGraph;
@property (nullable, nonatomic, retain) NSString *leadXmlPath;
@property (nullable, nonatomic, retain) NSString *isImageSyncforMobile;
@property (nullable, nonatomic, retain) NSString *isInternalUsage;


@end

NS_ASSUME_NONNULL_END
