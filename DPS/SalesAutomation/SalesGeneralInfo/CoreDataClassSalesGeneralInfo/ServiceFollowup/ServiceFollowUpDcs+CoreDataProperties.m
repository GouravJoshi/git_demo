//
//  ServiceFollowUpDcs+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 16/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "ServiceFollowUpDcs+CoreDataProperties.h"

@implementation ServiceFollowUpDcs (CoreDataProperties)

+ (NSFetchRequest<ServiceFollowUpDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ServiceFollowUpDcs"];
}

@dynamic id;
@dynamic notes;
@dynamic followupDate;
@dynamic leadId;
@dynamic departmentId;
@dynamic departmentName;
@dynamic serviceId;
@dynamic serviceName;
@dynamic serviceSysName;
@dynamic departmentSysName;

@end
