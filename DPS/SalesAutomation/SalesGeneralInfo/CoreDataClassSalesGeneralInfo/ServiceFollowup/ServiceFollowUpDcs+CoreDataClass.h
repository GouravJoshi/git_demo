//
//  ServiceFollowUpDcs+CoreDataClass.h
//  DPS
//
//  Created by Rakesh Jain on 16/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceFollowUpDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ServiceFollowUpDcs+CoreDataProperties.h"
