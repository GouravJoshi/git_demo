//
//  ServiceFollowUpDcs+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 16/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "ServiceFollowUpDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ServiceFollowUpDcs (CoreDataProperties)

+ (NSFetchRequest<ServiceFollowUpDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *id;
@property (nullable, nonatomic, copy) NSString *notes;
@property (nullable, nonatomic, copy) NSString *followupDate;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *departmentId;
@property (nullable, nonatomic, copy) NSString *departmentName;
@property (nullable, nonatomic, copy) NSString *serviceId;
@property (nullable, nonatomic, copy) NSString *serviceName;
@property (nullable, nonatomic, copy) NSString *serviceSysName;
@property (nullable, nonatomic, copy) NSString *departmentSysName;

@end

NS_ASSUME_NONNULL_END
