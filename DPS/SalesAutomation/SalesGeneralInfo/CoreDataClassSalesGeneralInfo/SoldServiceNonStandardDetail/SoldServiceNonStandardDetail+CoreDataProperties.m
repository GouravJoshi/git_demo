//
//  SoldServiceNonStandardDetail+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SoldServiceNonStandardDetail+CoreDataProperties.h"

@implementation SoldServiceNonStandardDetail (CoreDataProperties)

@dynamic leadId;
@dynamic createdBy;
@dynamic createdDate;
@dynamic departmentSysname;
@dynamic discount;
@dynamic initialPrice;
@dynamic isSold;
@dynamic maintenancePrice;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic modifiedInitialPrice;
@dynamic modifiedMaintenancePrice;
@dynamic serviceDescription;
@dynamic serviceFrequency;
@dynamic serviceName;
@dynamic soldServiceNonStandardId;
@dynamic userName;
@dynamic companyKey;
@dynamic discountPercentage;
@dynamic billingFrequencyPrice;
@dynamic billingFrequencySysName;
@dynamic nonStdServiceTermsConditions;
@dynamic frequencySysName;
@dynamic internalNotes;
@dynamic problemIdentificationId;
@dynamic wdoProblemIdentificationId;
@dynamic mobileId;
@dynamic isBidOnRequest;
@dynamic isTaxable;
@dynamic isLeadTest;
@dynamic isUnderWarranty;


@end
