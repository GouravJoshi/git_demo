//
//  SoldServiceNonStandardDetail+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 25/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SoldServiceNonStandardDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface SoldServiceNonStandardDetail (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *departmentSysname;
@property (nullable, nonatomic, retain) NSString *discount;
@property (nullable, nonatomic, retain) NSString *initialPrice;
@property (nullable, nonatomic, retain) NSString *isSold;
@property (nullable, nonatomic, retain) NSString *maintenancePrice;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *modifiedInitialPrice;
@property (nullable, nonatomic, retain) NSString *modifiedMaintenancePrice;
@property (nullable, nonatomic, retain) NSString *serviceDescription;
@property (nullable, nonatomic, retain) NSString *serviceFrequency;
@property (nullable, nonatomic, retain) NSString *serviceName;
@property (nullable, nonatomic, retain) NSString *soldServiceNonStandardId;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *discountPercentage;
@property (nullable, nonatomic, retain) NSString *billingFrequencyPrice;
@property (nullable, nonatomic, retain) NSString *billingFrequencySysName;
@property (nullable, nonatomic, retain) NSString *nonStdServiceTermsConditions;
@property (nullable, nonatomic, retain) NSString *frequencySysName;
@property (nullable, nonatomic, retain) NSString *internalNotes;
@property (nullable, nonatomic, retain) NSString *problemIdentificationId; //wdoProblemIdentificationId
@property (nullable, nonatomic, retain) NSString *wdoProblemIdentificationId;
@property (nullable, nonatomic, retain) NSString *mobileId;
@property (nullable, nonatomic, retain) NSString *isBidOnRequest;
@property (nullable, nonatomic, retain) NSString *isTaxable;
@property (nullable, nonatomic, retain) NSString *isLeadTest;
@property (nullable, nonatomic, retain) NSString *isUnderWarranty;

@end

NS_ASSUME_NONNULL_END
