//
//  DocumentsDetail+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 06/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DocumentsDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface DocumentsDetail (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *accountNo;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *descriptionDocument;
@property (nullable, nonatomic, retain) NSString *docType;
@property (nullable, nonatomic, retain) NSString *fileName;
@property (nullable, nonatomic, retain) NSString *leadDocumentId;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *scheduleStartDate;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *otherDocSysName;
@property (nullable, nonatomic, retain) NSString *docFormatType;
@property (nullable, nonatomic, retain) NSString *isAddendum;
@property (nullable, nonatomic, retain) NSString *isSync;
@property (nullable, nonatomic, retain) NSString *isToUpload;
@property (nullable, nonatomic, retain) NSString *inspectionFormBuilderMappingMasterId;
@property (nullable, nonatomic, retain) NSString *templateKey;
@property (nullable, nonatomic, retain) NSString *isInternalUsage;
@end

NS_ASSUME_NONNULL_END
