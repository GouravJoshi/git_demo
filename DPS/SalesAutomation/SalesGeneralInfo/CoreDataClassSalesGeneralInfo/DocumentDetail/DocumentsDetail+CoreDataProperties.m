//
//  DocumentsDetail+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 06/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DocumentsDetail+CoreDataProperties.h"

@implementation DocumentsDetail (CoreDataProperties)

@dynamic leadId;
@dynamic accountNo;
@dynamic createdBy;
@dynamic createdDate;
@dynamic descriptionDocument;
@dynamic docType;
@dynamic fileName;
@dynamic leadDocumentId;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic scheduleStartDate;
@dynamic title;
@dynamic userName;
@dynamic companyKey;
@dynamic otherDocSysName;
@dynamic docFormatType;
@dynamic isAddendum;
@dynamic isSync;
@dynamic isToUpload;
@dynamic inspectionFormBuilderMappingMasterId;
@dynamic templateKey;
@dynamic isInternalUsage;

@end
