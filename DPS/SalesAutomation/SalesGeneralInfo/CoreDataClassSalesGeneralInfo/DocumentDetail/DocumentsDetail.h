//
//  DocumentsDetail.h
//  DPS
//
//  Created by Rakesh Jain on 06/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DocumentsDetail : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "DocumentsDetail+CoreDataProperties.h"
