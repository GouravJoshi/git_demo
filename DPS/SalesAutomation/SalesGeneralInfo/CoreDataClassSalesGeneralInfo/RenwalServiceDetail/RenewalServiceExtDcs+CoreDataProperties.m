//
//  RenewalServiceExtDcs+CoreDataProperties.m
//  
//
//  Created by Nilind Sharma on 08/05/20.
//
//

#import "RenewalServiceExtDcs+CoreDataProperties.h"

@implementation RenewalServiceExtDcs (CoreDataProperties)

+ (NSFetchRequest<RenewalServiceExtDcs *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"RenewalServiceExtDcs"];
}

@dynamic companyKey;
@dynamic leadId;
@dynamic renewalAmount;
@dynamic renewalDescription;
@dynamic renewalFrequencySysName;
@dynamic renewalPercentage;
@dynamic renewalServiceId;
@dynamic soldServiceStandardId;
@dynamic userName;
@dynamic serviceId;

@end
