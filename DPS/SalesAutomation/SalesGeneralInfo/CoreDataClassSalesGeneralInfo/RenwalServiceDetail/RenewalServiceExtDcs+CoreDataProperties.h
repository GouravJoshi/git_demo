//
//  RenewalServiceExtDcs+CoreDataProperties.h
//  
//
//  Created by Nilind Sharma on 08/05/20.
//
//

#import "RenewalServiceExtDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface RenewalServiceExtDcs (CoreDataProperties)

+ (NSFetchRequest<RenewalServiceExtDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *renewalAmount;
@property (nullable, nonatomic, copy) NSString *renewalDescription;
@property (nullable, nonatomic, copy) NSString *renewalFrequencySysName;
@property (nullable, nonatomic, copy) NSString *renewalPercentage;
@property (nullable, nonatomic, copy) NSString *renewalServiceId;
@property (nullable, nonatomic, copy) NSString *soldServiceStandardId;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *serviceId;
@end

NS_ASSUME_NONNULL_END
