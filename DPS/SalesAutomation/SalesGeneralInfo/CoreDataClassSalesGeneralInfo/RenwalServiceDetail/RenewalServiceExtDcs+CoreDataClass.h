//
//  RenewalServiceExtDcs+CoreDataClass.h
//  
//
//  Created by Nilind Sharma on 08/05/20.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface RenewalServiceExtDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "RenewalServiceExtDcs+CoreDataProperties.h"
