//
//  LeadMarketingContentExtDc+CoreDataProperties.m
//  
//
//  Created by Rakesh Jain on 08/01/19.
//
//

#import "LeadMarketingContentExtDc+CoreDataProperties.h"

@implementation LeadMarketingContentExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadMarketingContentExtDc *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadMarketingContentExtDc"];
}

@dynamic leadId;
@dynamic userName;
@dynamic companyKey;
@dynamic contentSysName;

@end
