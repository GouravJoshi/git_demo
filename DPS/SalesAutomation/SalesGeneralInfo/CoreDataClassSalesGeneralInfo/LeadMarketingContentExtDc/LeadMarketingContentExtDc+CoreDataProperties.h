//
//  LeadMarketingContentExtDc+CoreDataProperties.h
//  
//
//  Created by Rakesh Jain on 08/01/19.
//
//

#import "LeadMarketingContentExtDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadMarketingContentExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadMarketingContentExtDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *contentSysName;

@end

NS_ASSUME_NONNULL_END
