//
//  LeadMarketingContentExtDc+CoreDataClass.h
//  
//
//  Created by Rakesh Jain on 08/01/19.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadMarketingContentExtDc : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "LeadMarketingContentExtDc+CoreDataProperties.h"
