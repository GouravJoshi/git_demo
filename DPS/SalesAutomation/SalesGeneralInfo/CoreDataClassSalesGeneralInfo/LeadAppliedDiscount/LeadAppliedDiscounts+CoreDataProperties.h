//
//  LeadAppliedDiscounts+CoreDataProperties.h
//  
//
//  Created by Rakesh Jain on 01/05/18.
//
//

#import "LeadAppliedDiscounts+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadAppliedDiscounts (CoreDataProperties)

+ (NSFetchRequest<LeadAppliedDiscounts *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadAppliedDiscountId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *serviceSysName;
@property (nullable, nonatomic, copy) NSString *discountSysName;
@property (nullable, nonatomic, copy) NSString *discountType;
@property (nullable, nonatomic, copy) NSString *discountCode;
@property (nullable, nonatomic, copy) NSString *discountAmount;
@property (nullable, nonatomic, copy) NSString *discountDescription;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *isDiscountPercent;
@property (nullable, nonatomic, copy) NSString *discountPercent;
@property (nullable, nonatomic, copy) NSString *soldServiceId;
@property (nullable, nonatomic, copy) NSString *serviceType;
@property (nullable, nonatomic, copy) NSString *appliedInitialDiscount;
@property (nullable, nonatomic, copy) NSString *appliedMaintDiscount;
@property (nullable, nonatomic, copy) NSString *applicableForInitial;
@property (nullable, nonatomic, copy) NSString *applicableForMaintenance;
@property (nullable, nonatomic, copy) NSString *isApplied;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *accountNo;

@end

NS_ASSUME_NONNULL_END
