//
//  LeadAppliedDiscounts+CoreDataProperties.m
//  
//
//  Created by Rakesh Jain on 01/05/18.
//
//

#import "LeadAppliedDiscounts+CoreDataProperties.h"

@implementation LeadAppliedDiscounts (CoreDataProperties)

+ (NSFetchRequest<LeadAppliedDiscounts *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadAppliedDiscounts"];
}

@dynamic leadAppliedDiscountId;
@dynamic leadId;
@dynamic serviceSysName;
@dynamic discountSysName;
@dynamic discountType;
@dynamic discountCode;
@dynamic discountAmount;
@dynamic discountDescription;
@dynamic isActive;
@dynamic userName;
@dynamic companyKey;
@dynamic isDiscountPercent;
@dynamic discountPercent;
@dynamic soldServiceId;
@dynamic serviceType;
@dynamic appliedMaintDiscount;
@dynamic appliedInitialDiscount;
@dynamic applicableForInitial;
@dynamic applicableForMaintenance;
@dynamic isApplied;
@dynamic name;
@dynamic accountNo;
@end
