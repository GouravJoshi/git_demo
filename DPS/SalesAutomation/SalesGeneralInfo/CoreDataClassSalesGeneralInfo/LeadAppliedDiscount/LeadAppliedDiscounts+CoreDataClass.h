//
//  LeadAppliedDiscounts+CoreDataClass.h
//  
//
//  Created by Rakesh Jain on 01/05/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadAppliedDiscounts : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "LeadAppliedDiscounts+CoreDataProperties.h"
