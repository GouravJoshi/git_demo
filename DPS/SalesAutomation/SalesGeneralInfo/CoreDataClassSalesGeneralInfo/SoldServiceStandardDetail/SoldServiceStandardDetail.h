//
//  SoldServiceStandardDetail.h
//  DPS
//
//  Created by Rakesh Jain on 01/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SoldServiceStandardDetail : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "SoldServiceStandardDetail+CoreDataProperties.h"
