//
//  SoldServiceStandardDetail+CoreDataProperties.h
//  DPS
//
//  Created by Rakesh Jain on 01/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SoldServiceStandardDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface SoldServiceStandardDetail (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *discount;
@property (nullable, nonatomic, retain) NSString *initialPrice;
@property (nullable, nonatomic, retain) NSString *isSold;
@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *maintenancePrice;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *modifiedInitialPrice;
@property (nullable, nonatomic, retain) NSString *modifiedMaintenancePrice;
@property (nullable, nonatomic, retain) NSString *packageId;
@property (nullable, nonatomic, retain) NSString *serviceFrequency;
@property (nullable, nonatomic, retain) NSString *serviceId;
@property (nullable, nonatomic, retain) NSString *serviceSysName;
@property (nullable, nonatomic, retain) NSString *soldServiceStandardId;
@property (nullable, nonatomic, retain) NSString *serviceTermsConditions;
@property (nullable, nonatomic, retain) NSString *isTBD;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *frequencySysName;
@property (nullable, nonatomic, retain) NSString *unit;
@property (nullable, nonatomic, retain) NSString *finalUnitBasedInitialPrice;
@property (nullable, nonatomic, retain) NSString *finalUnitBasedMaintPrice;
@property (nullable, nonatomic, retain) NSString *bundleDetailId;
@property (nullable, nonatomic, retain) NSString *bundleId;
@property (nullable, nonatomic, retain) NSString *billingFrequencySysName;
@property (nullable, nonatomic, retain) NSString *discountPercentage;
@property (nullable, nonatomic, retain) id additionalParameterPriceDcs;
@property (nullable, nonatomic, retain) NSString *totalInitialPrice;
@property (nullable, nonatomic, retain) NSString *totalMaintPrice;
@property (nullable, nonatomic, retain) NSString *billingFrequencyPrice;
@property (nullable, nonatomic, retain) NSString *servicePackageName;
//Clark Pest Change
@property (nullable, nonatomic, retain) NSString *serviceDescription;
@property (nullable, nonatomic, retain) NSString *isChangeServiceDesc;
@property (nullable, nonatomic, retain) NSString *isResidentialTaxable;
@property (nullable, nonatomic, retain) NSString *isCommercialTaxable;
@property (nullable, nonatomic, retain) NSString *internalNotes;
@property (nullable, nonatomic, retain) NSString *isBidOnRequest;
@property (nullable, nonatomic, retain) NSString *categorySysName;

@end

NS_ASSUME_NONNULL_END
