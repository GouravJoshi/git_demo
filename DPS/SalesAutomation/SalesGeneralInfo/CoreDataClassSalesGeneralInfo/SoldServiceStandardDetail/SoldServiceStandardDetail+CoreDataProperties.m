//
//  SoldServiceStandardDetail+CoreDataProperties.m
//  DPS
//
//  Created by Rakesh Jain on 01/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SoldServiceStandardDetail+CoreDataProperties.h"

@implementation SoldServiceStandardDetail (CoreDataProperties)

@dynamic createdBy;
@dynamic createdDate;
@dynamic discount;
@dynamic initialPrice;
@dynamic isSold;
@dynamic leadId;
@dynamic maintenancePrice;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic modifiedInitialPrice;
@dynamic modifiedMaintenancePrice;
@dynamic packageId;
@dynamic serviceFrequency;
@dynamic serviceId;
@dynamic serviceSysName;
@dynamic soldServiceStandardId;
@dynamic serviceTermsConditions;
@dynamic isTBD;
@dynamic userName;
@dynamic companyKey;
@dynamic frequencySysName;
@dynamic unit;
@dynamic finalUnitBasedMaintPrice;
@dynamic finalUnitBasedInitialPrice;
@dynamic bundleDetailId;
@dynamic bundleId;
@dynamic billingFrequencySysName;
@dynamic discountPercentage;
@dynamic additionalParameterPriceDcs;
@dynamic totalInitialPrice;
@dynamic totalMaintPrice;
@dynamic billingFrequencyPrice;
@dynamic servicePackageName;
@dynamic serviceDescription;
@dynamic isChangeServiceDesc;
@dynamic isResidentialTaxable;
@dynamic isCommercialTaxable;
@dynamic internalNotes;
@dynamic isBidOnRequest;
@dynamic categorySysName;

@end
