//
//  LeadContactDetailExtSerDc+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 31/01/20.
//
//

#import "LeadContactDetailExtSerDc+CoreDataProperties.h"

@implementation LeadContactDetailExtSerDc (CoreDataProperties)

+ (NSFetchRequest<LeadContactDetailExtSerDc *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"LeadContactDetailExtSerDc"];
}

@dynamic companyKey;
@dynamic contactNumber;
@dynamic createdBy;
@dynamic createdDate;
@dynamic isCustomerNumber;
@dynamic isMessageSent;
@dynamic leadId;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic subject;
@dynamic userName;
@dynamic leadContactId;
@dynamic isDefaultNumber;

@end
