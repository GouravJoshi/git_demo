//
//  LeadContactDetailExtSerDc+CoreDataClass.h
//  
//
//  Created by Akshay Hastekar on 31/01/20.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadContactDetailExtSerDc : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "LeadContactDetailExtSerDc+CoreDataProperties.h"
