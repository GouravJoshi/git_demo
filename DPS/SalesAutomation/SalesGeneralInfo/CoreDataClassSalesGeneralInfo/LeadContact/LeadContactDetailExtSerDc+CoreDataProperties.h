//
//  LeadContactDetailExtSerDc+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 31/01/20.
//
//

#import "LeadContactDetailExtSerDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadContactDetailExtSerDc (CoreDataProperties)

+ (NSFetchRequest<LeadContactDetailExtSerDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *contactNumber;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *isCustomerNumber;
@property (nullable, nonatomic, copy) NSString *isMessageSent;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *subject;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *leadContactId;
@property (nullable, nonatomic, copy) NSString *isDefaultNumber;

@end

NS_ASSUME_NONNULL_END
