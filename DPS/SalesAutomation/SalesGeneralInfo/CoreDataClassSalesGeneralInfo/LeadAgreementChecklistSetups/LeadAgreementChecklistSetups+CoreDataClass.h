//
//  LeadAgreementChecklistSetups+CoreDataClass.h
//  
//
//  Created by Rakesh Jain on 02/02/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadAgreementChecklistSetups : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "LeadAgreementChecklistSetups+CoreDataProperties.h"
