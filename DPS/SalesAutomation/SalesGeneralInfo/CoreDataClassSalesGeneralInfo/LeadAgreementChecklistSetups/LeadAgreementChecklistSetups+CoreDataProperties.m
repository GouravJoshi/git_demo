//
//  LeadAgreementChecklistSetups+CoreDataProperties.m
//  
//
//  Created by Rakesh Jain on 02/02/18.
//
//

#import "LeadAgreementChecklistSetups+CoreDataProperties.h"

@implementation LeadAgreementChecklistSetups (CoreDataProperties)

+ (NSFetchRequest<LeadAgreementChecklistSetups *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadAgreementChecklistSetups"];
}

@dynamic leadAgreementChecklistSetupId;
@dynamic leadId;
@dynamic agreementChecklistSysName;
@dynamic isActive;
@dynamic companyKey;
@dynamic userName;
@dynamic agreementChecklistId;
@end
