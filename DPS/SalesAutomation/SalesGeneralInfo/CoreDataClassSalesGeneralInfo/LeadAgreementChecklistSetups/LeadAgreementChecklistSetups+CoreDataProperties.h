//
//  LeadAgreementChecklistSetups+CoreDataProperties.h
//  
//
//  Created by Rakesh Jain on 02/02/18.
//
//

#import "LeadAgreementChecklistSetups+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadAgreementChecklistSetups (CoreDataProperties)

+ (NSFetchRequest<LeadAgreementChecklistSetups *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadAgreementChecklistSetupId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *agreementChecklistSysName;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *agreementChecklistId;
@end

NS_ASSUME_NONNULL_END
