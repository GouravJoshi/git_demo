
// New Changes changes Changes Changes
//  SaleAutomationGeneralInfo.m
//  changes new done by nilind changes changes
//  Created by Rakesh Jain on 19/07/16. change changes changes
//  Copyright © 2016 Saavan. All rights reserved. New changes Nilind  changes changes Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind

#import "SaleAutomationGeneralInfo.h"

#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "TotalLeads.h"
#import "TotalLeads+CoreDataProperties.h"
#import "Reachability.h"
#import "SalesAutomationSelectService.h"
#import "SalesAutomationInspection.h"
#import "LeadDetail.h"
#import "DocumentsDetail.h"
#import "ImagePreviewGeneralInfoAppointmentView.h"
#import "ImagePreviewSalesAuto.h"
#import "ImageDetail.h"
#import "SalesAutomationSelectService.h"
#import <QuartzCore/QuartzCore.h>
#import <sys/utsname.h>
#import "GlobalSyncViewController.h"
//#import "XMLReader.h"
#import "XMLConverter.h"
#import "EditImageViewController.h"
#import "EmailDetail.h"
#import "AllImportsViewController.h"

#import <CoreLocation/CoreLocation.h>
#import "LeadContactDetailExtSerDc+CoreDataClass.h"
#import "LeadContactDetailExtSerDc+CoreDataProperties.h"
#import "DPS-Swift.h"

@interface SaleAutomationGeneralInfo ()<NSXMLParserDelegate,CLLocationManagerDelegate>
{
    NSArray *arrayItems ;
    NSMutableArray *arrOfLeads;
    NSString *tyPeOfLead;
    UILabel *lblBorder,*lblBorder2;
    UIImageOrientation scrollOrientation;
    CGPoint lastPos;
    Global *global;
    NSArray *arrOfTotalLeads;
    NSMutableArray *arrOfleadsByType;
    UIButton *btnTemp;
    UILabel *lblTemp;
    int check;
    UITableView *tblData;
    NSMutableArray *arrData;
    NSString *strCompanyKey,*strEmpID,*strUserName;
    NSDictionary *dictMasters,*dictSalesLeadMaster;
    NSMutableArray *arrStatusMaster,*arrStageMaster,*arrUrgency,*arrVisibility,*arrSource,*arrInspector,*arrCountries,*arrCities,*arrState;
    NSString *strLeadId;
    UIDocumentInteractionController *documentInteractionController;
    NSMutableArray *arrImagePath;
    NSMutableArray *arrLocalImages,*arrOfImagenameCollewctionView;
    
    
    // For All Ids
    
    NSString *strInspectorId,*strPrioritySysName,*strStatusSysName,*strStageSysName,*strSourceSysName,*strCountryService,*strStateService,*strCountryBilling,*strStateBilling,*strStatusssId;
    
    //Nilind
    NSMutableArray *arrStageIdMaster,*arrSourceSelectedValue,*arrSourceSelectedId;
    NSMutableArray *arrIndex,*arrStageSysName;
    UIView *viewBackGround;
    NSString *strStatusIdForStage,*strLeadStageId;
    BOOL isSelectEnable,chkStage;
    //............
    NSMutableArray *arrAdd;
    NSArray *arrDataTblView;
    NSMutableArray *arrNoImage;
    
    //Nilind 1 Oct
    NSMutableArray *arrCurrentServiceName,*arrCurrentServiceSysname;
    //...........................
    
    //Nilind 4 Oct
    
    NSString *strBranchSysName;
    //...................................
    
    //Nilind 7 Nov
    
    NSString *strSalesAutoMainUrl;
    NSMutableData *responseData;
    //.............................................
    
    //Nilind 8 Nov
    
    NSString *strChkCity,*strChkZipcode,*strChkChkState,*strUpdateTax;
    
    BOOL chkForBranchName;
    int tagText;
    
    //Nilind 17 Nov
    
    BOOL chkForDuplicateImageSave;
    BOOL chkForSameBillingAddress;
    
    NSMutableArray *arrSourceChange;
    UILabel *lblOk;
    
    //Nilind 24 Jan
    BOOL isEditedInSalesAuto;
    //End
    NSMutableString *strMutable;
    NSMutableArray *arrayKey,*arrValue;
    NSMutableDictionary *dictXML;
    NSDictionary *dictZillow;
    
    //Saavan Changes For Email ID
    NSString *strStatusss;
    NSMutableArray *arrOfPrimaryEmailNew,*arrOfSecondaryEmailNew,*arrOfPrimaryEmailAlreadySaved,*arrOfSecondaryEmailAlreadySaved,*arrDataTblViewNew;
    
    //Nilind 07 Jun ImageCaption
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    NSMutableArray *arrOfImageCaption,*arrOfImageDescription,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph;
    // Nilind 23 Nov
    
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    
    BOOL view1EditName,view3EditServiceAdd,view5EditBillingAdd;
    BOOL chkEditViewCustomerInfo,chkEditViewServiceAdd,chkEditViewBillingAdd;
    
    NSString *strLeadType;
    
    NSString *strChkServiceCounty,*strChkServiceSchoolDistrict;
    
    NSMutableArray *arrTaxCode;
    NSString *strTaxCode,*strFlowType,*strTaxSysName;
    BOOL chkTaxCodeRequired;
    
    NSMutableArray *arrPropertyType;
    NSString *strProprtyTypeSysName,*strSelectedProprtyTypeSysName, *strLeadStatusGlobal , *strLeadNumber,*strServiceAddressSubType;
    
    NSMutableArray *arrCustomerContactNo,*arrServiceAddressSubType;;
}
@end

@implementation SaleAutomationGeneralInfo
@synthesize matchesGeneralInfo;
- (void)viewDidLoad
{
    [self tempPOC];
    strTaxSysName=@"";
    strTaxCode=@"";
    strProprtyTypeSysName=@"";
    strStageSysName = @"";
    //Saavan Changes For Email Id
    arrOfPrimaryEmailNew=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailNew=[[NSMutableArray alloc]init];
    arrOfPrimaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    
    
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    arrServiceAddressSubType = [[NSMutableArray alloc]init];
    
    [arrServiceAddressSubType addObject:@"Residential"];
    [arrServiceAddressSubType addObject:@"Commercial"];
#pragma mark- TEMP NILIND 14 MMARCH
    view1EditName=NO;view3EditServiceAdd=NO;view5EditBillingAdd=NO;
    
    chkEditViewBillingAdd=NO;chkEditViewServiceAdd=NO;chkEditViewCustomerInfo=NO;
    
    _constView1FisrtName_H.constant=0;
    _constView3ServiceAddress_H.constant=0;_constServiceContacAllName_H.constant=0;
    _constView5BillingAddress_H.constant=0;_constBillingContactAllName_H.constant=0;
    
    
    strFlowType=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"flowType"]];
    if ([strFlowType isEqualToString:@""] || strFlowType.length == 0)
    {
        strFlowType = @"Residential";
    }
    
    [self methodSetAllViews];
    // [self customerInfoUnEdit];
    
    //eND
    txtFieldCaption.delegate=self;
    txtViewImageDescription.delegate=self;
    //   [arrOfImagenameCollewctionView addObject:@"\\Documents\\UploadImages\\Img05112017120130.jpg"];
    
    
    //POC CHANGE
    _constServicePOCDetail_H.constant=0;
    _constantBillingPOCDetail_H.constant=0;
    
    _viewServicePOCDetail.userInteractionEnabled=NO;
    _viewBillingPOCDetail.userInteractionEnabled=NO;
    
    [_btnShowCustomerInfo setTitle:@"Show" forState:UIControlStateNormal];
    [_btnShowServicePOCDetail setTitle:@"Show" forState:UIControlStateNormal];
    [_btnShowBillingPOCDetail setTitle:@"Show" forState:UIControlStateNormal];
    
    
    [_btnShowCustomerInfo setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
    
    [_btnShowServiceAddress setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
    [_btnShowBillingAddress setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
    
    [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    [_btnEditServiceAddress setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    [_btnEditBillingAddress setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    //End
    
    global=[[Global alloc]init];
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    NSLog(@"*dLatitude : %@", latitude);
    NSLog(@"*dLongitude : %@",longitude);
    //End Of Changes by Saavan
    
    strMutable=[[NSMutableString alloc] init];
    arrayKey=[[NSMutableArray alloc]init];
    arrValue=[[NSMutableArray alloc]init];
    
    isEditedInSalesAuto=NO;
    
    _txtBranchName.hidden=YES;
    _lblBranchName.numberOfLines=5;
    chkForSameBillingAddress=NO;
    
    NSUserDefaults *defs5=[NSUserDefaults standardUserDefaults];
    [defs5 setBool:NO forKey:@"fromImagePrview"];
    [defs5 synchronize];
    chkForDuplicateImageSave=NO;
    
    _txtViewNotes.textColor=[UIColor blackColor];
    responseData = [NSMutableData data];
    chkForBranchName=NO;
    _txtBranchName.delegate = self; // delegate methods won't be called without this
    _txtBranchName.inputView = [[UIView alloc] init]; // UIView replaces keyboard
    
    isSelectEnable=NO;
    chkStage=NO;
    [super viewDidLoad];
    arrStatusMaster=[[NSMutableArray alloc]init];
    arrStageMaster=[[NSMutableArray alloc]init];
    arrUrgency=[[NSMutableArray alloc]init];
    arrVisibility=[[NSMutableArray alloc]init];
    arrSource=[[NSMutableArray alloc]init];
    arrInspector=[[NSMutableArray alloc]init];
    arrCountries=[[NSMutableArray alloc]init];
    arrState=[[NSMutableArray alloc]init];
    arrCities=[[NSMutableArray alloc]init];
    arrImagePath=[[NSMutableArray alloc]init];
    //NIlind
    arrStageIdMaster=[[NSMutableArray alloc]init];
    arrIndex=[[NSMutableArray alloc]init];
    arrSourceSelectedValue=[[NSMutableArray alloc]init];
    arrSourceSelectedId=[[NSMutableArray alloc]init];
    arrStageSysName=[[NSMutableArray alloc]init];
    arrLocalImages=[[NSMutableArray alloc]init];
    //..............
    arrNoImage=[[NSMutableArray alloc]init];
    arrCurrentServiceName=[[NSMutableArray alloc]init];
    arrCurrentServiceSysname=[[NSMutableArray alloc]init];
    
#pragma mark- GET ALL MASTERS
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    dictSalesLeadMaster=[defs valueForKey:@"LeadDetailMaster"];
    // NSLog(@"Response on getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    NSString *strAccountNo=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"thirdPartyAccountNo"]];
    if (strAccountNo.length==0 || [strAccountNo isEqualToString:@""] || [strAccountNo isEqualToString:@"(null)"])
    {
        strAccountNo=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"accountNo"]];
        
    }
    _lblNameAcount.text=[NSString stringWithFormat:@"Opportunity #: %@, A/C #: %@",[matchesGeneralInfo valueForKey:@"leadNumber"],strAccountNo];
    
    //_lblNameAcount.text=[NSString stringWithFormat:@"Lead #: %@, A/C #: %@",[matchesGeneralInfo valueForKey:@"leadNumber"],strAccountNo];
    
    
        
    //Nilind 18 Jan
    
    _lblNameTitle.text=[NSString stringWithFormat:@"%@ %@ %@",[matchesGeneralInfo valueForKey:@"firstName"],[matchesGeneralInfo valueForKey:@"middleName"],[matchesGeneralInfo valueForKey:@"lastName"]];
    [defs setValue:_lblNameTitle.text forKey:@"nameTitle"];
    [defs setValue:[matchesGeneralInfo valueForKey:@"leadNumber"] forKey:@"leadNumber"];
    _lblNameAcount.font=[UIFont boldSystemFontOfSize:14];
    _lblNameTitle.font=[UIFont boldSystemFontOfSize:14];
    //End
    
    _lblNameAcount.numberOfLines=5;
    
    //Nilind 8 Nov
    strChkChkState=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceState"]];
    strChkCity=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCity"]];
    strChkZipcode=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceZipcode"]];
    strChkServiceCounty=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCounty"]];
    strChkServiceSchoolDistrict=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceSchoolDistrict"]];
    //....................
    
    
    
    [defs setValue:_lblNameAcount.text forKey:@"lblName"];
    _lblNameAcount.textAlignment=NSTextAlignmentCenter;
    
    // Status Master Fetching
    NSArray *temparrStatusMaster=[dictSalesLeadMaster valueForKey:@"LeadStatusMasters"];
    for (int i=0; i<temparrStatusMaster.count; i++)
    {
        NSDictionary *dict=[temparrStatusMaster objectAtIndex:i];
        
        [arrStatusMaster addObject:[dict valueForKey:@"StatusName"]];
    }
    
    
    //Stage Master Fetching
    NSArray *temparrStageMaster=[dictSalesLeadMaster valueForKey:@"LeadStageMasters"];
    for (int i=0; i<temparrStageMaster.count; i++)
    {
        NSDictionary *dict=[temparrStageMaster objectAtIndex:i];
        
        [arrStageMaster addObject:[dict valueForKey:@"Name"]];
        
    }
    
    // Urgency Master Fetching
    NSArray *temparrUrgency=[dictSalesLeadMaster valueForKey:@"UrgencyMasters"];
    for (int i=0; i<temparrUrgency.count; i++)
    {
        NSDictionary *dict=[temparrUrgency objectAtIndex:i];
        
        [arrUrgency addObject:[dict valueForKey:@"Name"]];
    }
    
    //// Visibility Master Fetching
    //    NSArray *temparrVisibility=[dictMasters valueForKey:@"Visbilities"];
    //    for (int i=0; i<temparrVisibility.count; i++)
    //    {
    //        NSDictionary *dict=[temparrVisibility objectAtIndex:i];
    //
    //        [arrVisibility addObject:[dict valueForKey:@"Name"]];
    //    }
    
    // Source Master Fetching
    NSArray *temparrSource=[dictSalesLeadMaster valueForKey:@"SourceMasters"];
    for (int i=0; i<temparrSource.count; i++)
    {
        NSDictionary *dict=[temparrSource objectAtIndex:i];
        
        [arrSource addObject:[dict valueForKey:@"Name"]];
    }
    
    // GETTING ALL EMPLOYEE Inspector
    ////New Change for Employee 26 Sept
    //    NSArray *arrEmployee;
    //    arrEmployee=[dictMasters valueForKey:@"Employees"];
    //    if ([arrEmployee isEqual:@""])
    //    {
    //        NSLog(@"dsfdsfdsfdsfdsfdsf");
    //    }
    //    else
    //    {
    //        for (int i=0; i<arrEmployee.count; i++)
    //        {
    //            NSDictionary *dict=[arrEmployee objectAtIndex:i];
    //
    //            [arrInspector addObject:[dict valueForKey:@"FullName"]];
    //        }
    //    }
    //
    
    NSArray *arrEmployee;
    arrEmployee=[defs valueForKey:@"EmployeeList"];
    if ([arrEmployee isEqual:@""] || [arrEmployee isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"dsfdsfdsfdsfdsfdsf");
    }
    else
    {
        for (int i=0; i<arrEmployee.count; i++)
        {
            NSDictionary *dict=[arrEmployee objectAtIndex:i];
            
            [arrInspector addObject:[dict valueForKey:@"FullName"]];
        }
    }
    
    // Text border color
    _txtBranchName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    
    _txtBranchName.layer.borderWidth=1.0;
    
    
    _txtLead.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtLead.layer.borderWidth=1.0;
    
    
    
    
    _txtCustomerName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtCustomerName.layer.borderWidth=1.0;
    _txtScheduleStartDate.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtScheduleStartDate.layer.borderWidth=1.0;
    _txtAccount.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtAccount.layer.borderWidth=1.0;
    
    
    _txtPrimaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtPrimaryEmail.layer.borderWidth=1.0;
    _txtCellNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtCellNo.layer.borderWidth=1.0;
    
    _txtPrimaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtPrimaryPhone.layer.borderWidth=1.0;
    
    _txtSecondaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtSecondaryPhone.layer.borderWidth=1.0;
    
    _txtCityServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtCityServiceAddress.layer.borderWidth=1.0;
    
    _txtZipcodeServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtZipcodeServiceAddress.layer.borderWidth=1.0;
    
    
    _txtCityBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtCityBillingAddress.layer.borderWidth=1.0;
    
    _txtZipCodeBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtZipCodeBillingAddress.layer.borderWidth=1.0;
    
    _txtTagServiceRequired.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtTagServiceRequired.layer.borderWidth=1.0;
    
    
    
    _txtViewNotes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewNotes.layer.borderWidth=1.0;
    
    _txtViewDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewDescription.layer.borderWidth=1.0;
    
    _txtViewAdd1ServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdd1ServiceAddress.layer.borderWidth=1.0;
    
    _txtViewAdd2ServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdd2ServiceAddress.layer.borderWidth=1.0;
    
    
    
    _txtViewAdd1BillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdd1BillingAddress.layer.borderWidth=1.0;
    
    _txtViewAdd2BillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdd2BillingAddress.layer.borderWidth=1.0;
    
    _txtCompanyName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtCompanyName.layer.borderWidth=1.0;
    
    _txtFirstName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtFirstName.layer.borderWidth=1.0;
    
    _txtLastName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtLastName.layer.borderWidth=1.0;
    
    _txtMiddleName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtMiddleName.layer.borderWidth=1.0;
    
    _txtViewCurrentServices.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewCurrentServices.layer.borderWidth=1.0;
    
    //Nilind 08 Feb
    _txtAreaSqFt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtAreaSqFt.layer.borderWidth=1.0;
    
    _txtBedroom.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBedroom.layer.borderWidth=1.0;
    
    _txtBathroom.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBathroom.layer.borderWidth=1.0;
    
    _txtLotSizeSqFt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtLotSizeSqFt.layer.borderWidth=1.0;
    
    _txtYearBuil.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtYearBuil.layer.borderWidth=1.0;
    
    _txtLinearSqFt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtLinearSqFt.layer.borderWidth=1.0;
    
    _txtLinearSqFt.layer.cornerRadius=5.0;
    
    _txtAreaSqFt.layer.cornerRadius=5.0;
    _txtBedroom.layer.cornerRadius=5.0;
    _txtBathroom.layer.cornerRadius=5.0;
    _txtLotSizeSqFt.layer.cornerRadius=5.0;
    _txtYearBuil.layer.cornerRadius=5.0;
    
//    _txtAreaSqFt.placeholder=@"Area(Sq.Ft.)";
//    _txtBedroom.placeholder=@"No of Bedrooms";
//    _txtBathroom.placeholder=@"No of Bathrooms";
//    _txtLotSizeSqFt.placeholder=@"Lot Size(Sq.Ft.)";
//    _txtYearBuil.placeholder=@"Year Built";
    
    
    //End
    
    
    //Nilind 09 Jan
    _txtLead.layer.cornerRadius=5.0;
    _txtAccount.layer.cornerRadius=5.0;
    _txtCompanyName.layer.cornerRadius=5.0;
    _txtScheduleStartDate.layer.cornerRadius=5.0;
    _txtFirstName.layer.cornerRadius=5.0;
    _txtMiddleName.layer.cornerRadius=5.0;
    _txtLastName.layer.cornerRadius=5.0;
    _txtPrimaryEmail.layer.cornerRadius=5.0;
    _txtCellNo.layer.cornerRadius=5.0;
    _txtPrimaryPhone.layer.cornerRadius=5.0;
    _txtSecondaryPhone.layer.cornerRadius=5.0;
    _txtBranchName.layer.cornerRadius=5.0;
    _txtCityBillingAddress.layer.cornerRadius=5.0;
    _txtZipCodeBillingAddress.layer.cornerRadius=5.0;
    _txtCityServiceAddress.layer.cornerRadius=5.0;
    _txtZipcodeServiceAddress.layer.cornerRadius=5.0;
    _txtTagServiceRequired.layer.cornerRadius=5.0;
    
    _txtViewAdd1ServiceAddress.layer.cornerRadius=5.0;
    _txtViewAdd2ServiceAddress.layer.cornerRadius=5.0;
    _txtViewAdd1BillingAddress.layer.cornerRadius=5.0;
    _txtViewAdd2BillingAddress.layer.cornerRadius=5.0;
    _txtViewNotes.layer.cornerRadius=5.0;
    _txtViewDescription.layer.cornerRadius=5.0;
    
    
    _txtLead.layer.borderColor=[[UIColor whiteColor] CGColor];
    _txtAccount.layer.borderColor=[[UIColor whiteColor] CGColor];
    _txtCompanyName.layer.borderColor=[[UIColor whiteColor] CGColor];
    _txtScheduleStartDate.layer.borderColor=[[UIColor whiteColor] CGColor];
    //End
    
    //Nilind 04 Aug
    
    _txtMapCodeServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtMapCodeServiceAddress.layer.borderWidth=1.0;
    
    _txtMapCodeBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtMapCodeBillingAddress.layer.borderWidth=1.0;
    
    _txtGateCodeServiceNotes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtGateCodeServiceNotes.layer.borderWidth=1.0;
    
    
    _txtViewDirectionServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewDirectionServiceAddress.layer.borderWidth=1.0;
    
    _txtViewAddressNotesServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAddressNotesServiceAddress.layer.borderWidth=1.0;
    
    
    _txtMapCodeBillingAddress.layer.cornerRadius=5.0;
    _txtMapCodeServiceAddress.layer.cornerRadius=5.0;
    _txtGateCodeServiceNotes.layer.cornerRadius=5.0;
    _txtViewDirectionServiceAddress.layer.cornerRadius=5.0;
    _txtViewAddressNotesServiceAddress.layer.cornerRadius=5.0;
    
    _txtViewAccountDesc.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAccountDesc.layer.borderWidth=1.0;
    
    
    _txtViewAccountDesc.layer.cornerRadius=5.0;
    
    
    _txtViewProblemescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewProblemescription.layer.borderWidth=1.0;
    
    
    _txtViewProblemescription.layer.cornerRadius=5.0;
    
    
    //End
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpID        =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    chkTaxCodeRequired=[defsLogindDetail boolForKey:@"TaxCodeReq"];//[[dictLoginData valueForKey:@"TaxCodeReq"]boolValue];
    
    //NO NEED  [self FetchFromCoreDataToShowTaskList];
    
    
    //Response from appoint start******
    NSLog(@"APPOINTMENT RESPOSE DICTIONARY %@",_dictSaleAuto);
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    [defs2 setObject:_dictSaleAuto forKey:@"dictSalesLeadDetails"];
    [defs2 setValue:[matchesGeneralInfo valueForKey:@"accountNo"] forKey:@"leadAccountNo"];
    [defs2 synchronize];
    
    //end*******
    
    // Do any additional setup after loading the view.
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    arrData=[[NSMutableArray alloc]init];
    [arrData  addObject:@"test"];
    [arrData addObject:@"commercial"];
    [arrData addObject:@"residential"];
    
    tblData.dataSource=self;
    tblData.delegate=self;
    //Nilind 22 Septt
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    //..........................
    
    
    
    // Do any additional setup after loading the view.
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 100, 40);
    lblTemp.frame=CGRectMake(0, 0, 100, 40);
    
#pragma mark - temp comment
    
    
    global = [[Global alloc] init];
    [self getClockStatus];
    
    arrOfLeads=[[NSMutableArray alloc]init];
    
    //============================================================================
    //============================================================================
    arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Service Summary",@"Agreement", nil];
    _scrollViewLeads.delegate=self;
    CGFloat scrollWidth=arrOfLeads.count*110;
    
    [_scrollViewLeads setFrame:CGRectMake(0,_viewTop.frame.origin.y+_viewTop.frame.size.height,[UIScreen mainScreen].bounds.size.width,_scrollViewLeads.frame.size.height)];
    [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,60)];
    _scrollViewLeads.showsHorizontalScrollIndicator = NO;
    
    
    //============================================================================
    //============================================================================
    lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-2, 100, 2)];
    
    lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-2, _scrollViewLeads.frame.size.width+btnTemp.frame.size.width-35, 2)];
    
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-2, 100, 2);
    }
    lblBorder.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
    
    [_scrollViewLeads addSubview:lblBorder2];
    [_scrollViewLeads addSubview:lblBorder];
    //============================================================================
    //============================================================================
    _scrollViewLeads.backgroundColor=[UIColor whiteColor];
    [_scrollViewLeads addSubview:btnTemp];
    [_scrollViewLeads addSubview:lblTemp];
    [self addButtons];
    
    
#pragma mark- ASSIGING VALUES TO FORM
    
    _txtLead.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.LeadName"]];
    
    _txtCustomerName.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.CustomerName"]];
    _txtScheduleStartDate.text=[NSString stringWithFormat:@"%@",[global ChangeDateToLocalDateOther:[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.ScheduleStartDate"]]]];
    
    _txtAccount.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.AccountNo"]];
    _txtPrimaryEmail.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.PrimaryEmail"]];
    //_txtCellNo.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.CellNo"]];
    _txtPrimaryPhone.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.PrimaryPhone"]];
    _txtSecondaryPhone.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.SecondaryPhone"]];
    
    
    _txtViewAdd1ServiceAddress.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.ServicesAddress1"]];
    _txtViewAdd2ServiceAddress.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.ServicesAddress2"]];
    
    _txtViewAdd1BillingAddress.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.BillingAddress1"]];
    _txtViewAdd2BillingAddress.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.BillingAddress2"]];
    
    [_btnCountryServiceAddress setTitle:[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.ServiceCountry"]] forState:UIControlStateNormal];
    [_btnStateServiceAddress setTitle:[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.ServiceState"]] forState:UIControlStateNormal];
    _txtCityServiceAddress.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.ServiceCity"]];
    _txtZipcodeServiceAddress.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.ServiceZipcode"]];
    
    
    [_btnCountryBillingAddress setTitle:[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.BillingCountry"]] forState:UIControlStateNormal];
    [_btnStateBillingAddress setTitle:[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.BillingState"]] forState:UIControlStateNormal];
    _txtCityBillingAddress.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.BillingCity"]];
    _txtZipCodeBillingAddress.text=[NSString stringWithFormat:@"%@",[_dictSaleAuto valueForKeyPath:@"LeadDetail.BillingZipcode"]];
    
    [_btnStatus setTitle:[_dictSaleAuto valueForKeyPath:@"LeadDetail.StatusSysName"] forState:UIControlStateNormal];
    [_btnStage setTitle:[_dictSaleAuto valueForKeyPath:@"LeadDetail.StageSysName"] forState:UIControlStateNormal];
    [_btnPriority setTitle:[_dictSaleAuto valueForKeyPath:@"LeadDetail.PrioritySysName"] forState:UIControlStateNormal];
    [_btnSource setTitle:[_dictSaleAuto valueForKeyPath:@"LeadDetail.SourceSysName"] forState:UIControlStateNormal];
    
    strLeadId=[matchesGeneralInfo valueForKey:@"leadId"];
    //Nilind 2 Nov
    
    
    strStatusss=[matchesGeneralInfo valueForKey:@"statusSysName"];
    strStageSysName = [matchesGeneralInfo valueForKey:@"stageSysName"];
    
    NSUserDefaults *dfsStageStatus=[NSUserDefaults standardUserDefaults];
    if ([dfsStageStatus boolForKey:@"backFromInspectionNew"] == YES)
    {
        strStatusss= [dfsStageStatus valueForKey:@"leadStatusSales"];
        strStageSysName=[dfsStageStatus valueForKey:@"stageSysNameSales"];
    }
    
    
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        //Nilind 08 Feb
//        _txtAreaSqFt.inputView = [[UIView alloc] init];
//        _txtAreaSqFt.delegate = self;
//
//        _txtLotSizeSqFt.inputView = [[UIView alloc] init];
//        _txtLotSizeSqFt.delegate = self;
//
//        _txtBathroom.inputView = [[UIView alloc] init];
//        _txtBathroom.delegate = self;
//
//        _txtBedroom.inputView = [[UIView alloc] init];
//        _txtBedroom.delegate = self;
//
//        _txtYearBuil.inputView = [[UIView alloc] init];
//        _txtYearBuil.delegate = self;
        
        //End
        
        //Nilind 10 Nov
        _btnPriority.enabled=NO;
        _btnSource.enabled=NO;
        _btnInspector.enabled=NO;
        _btnGlobalSync.enabled=NO;
        _txtScheduleStartDate.inputView = [[UIView alloc] init];
        _txtScheduleStartDate.delegate = self;
        
        
//        _txtFirstName.inputView=[[UIView alloc]init];
//        _txtFirstName.delegate = self;
//
//        _txtLastName.inputView=[[UIView alloc]init];
//        _txtLastName.delegate = self;
//
//
//        _txtMiddleName.inputView=[[UIView alloc]init];
//        _txtMiddleName.delegate = self;
//
//
//        _txtLastName.inputView=[[UIView alloc]init];
//        _txtLastName.delegate = self;
//
//        _txtPrimaryEmail.inputView=[[UIView alloc]init];
//        _txtPrimaryEmail.delegate = self;
//
//        _txtCellNo.inputView=[[UIView alloc]init];
//        _txtCellNo.delegate = self;
//
//        _txtPrimaryPhone.inputView=[[UIView alloc]init];
//        _txtPrimaryPhone.delegate = self;
//
//        _txtSecondaryPhone.inputView=[[UIView alloc]init];
//        _txtSecondaryPhone.delegate = self;
        
        _txtViewAdd1ServiceAddress.editable=NO;
        _txtViewAdd2ServiceAddress.editable=NO;
        _btnCountryServiceAddress.enabled=NO;
        _btnStateServiceAddress.enabled=NO;
        _txtCityServiceAddress.enabled=NO;
        _txtZipcodeServiceAddress.enabled=NO;
        
        _btnCheckBox.enabled=NO;
        
        _txtViewAdd1BillingAddress.editable=NO;
        // _txtViewAdd1BillingAddress.delegate = self;
        
        _txtViewAdd2BillingAddress.editable=NO;
        _btnCountryBillingAddress.enabled=NO;
        _btnStateBillingAddress.enabled=NO;
        _txtCityBillingAddress.enabled=NO;
        _txtZipCodeBillingAddress.enabled=NO;
        
        _txtViewNotes.editable=NO;
        _txtViewDescription.editable=YES;
        _txtTagServiceRequired.enabled=YES;
        
        _btnAddImages.enabled=NO;
        _btnUploadDocument.enabled=NO;
        
        _btnGetPropertyInfo.enabled=NO;
        
        _btnAddGraph.enabled=NO;
       // _btnAddImageFooter.enabled=NO;
       // _btnAddGraphImglobalageFooter.enabled=NO;
        //..................................................
        
        NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
        [dfsStatus setBool:YES forKey:@"status"];
        [dfsStatus synchronize];
        
        
        //Billing POC
        [self textMethodForDisable:_txtBillingPOCFirstName];
        [self textMethodForDisable:_txtBillingPOCLastName];
        [self textMethodForDisable:_txtBillingPOCMiddleName];
        [self textMethodForDisable:_txtBillingPOCCellNo];
        [self textMethodForDisable:_txtBillingPOCPrimaryEmail];
        [self textMethodForDisable:_txtBillingPOCSecondaryEmail];
        [self textMethodForDisable:_txtBillingPOCPrimaryPhone];
        [self textMethodForDisable:_txtBillingPOCSecondaryPhone];
        
        //Service POC
        [self textMethodForDisable:_txtServicePOCFirstName];
        [self textMethodForDisable:_txtServicePOCLastName];
        [self textMethodForDisable:_txtServicePOCMiddleName];
        [self textMethodForDisable:_txtServicePOCCellNo];
        [self textMethodForDisable:_txtServicePOCPrimaryEmail];
        [self textMethodForDisable:_txtServicePOCSecondaryEmail];
        [self textMethodForDisable:_txtServicePOCPrimaryPhone];
        [self textMethodForDisable:_txtServicePOCSecondaryPhone];
        
        _txtViewProblemescription.editable=YES;
        _btnLeadTypeInside.enabled=NO;
        _btnLeadTypeField.enabled=NO;
        
        [self textMethodForDisable:_txtNoOfStory];
        [self textMethodForDisable:_txtShrubArea];
        [self textMethodForDisable:_txtTurfArea];
        [_btnTaxCode setEnabled:NO];
        
        if ([_btnEditBillingAddress.currentImage isEqual:[UIImage imageNamed:@"cancelEdit.png"]])
        {
           // [self enableBillingAddressDetail];
        }
    }
    else
    {
        NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
        [dfsStatus setBool:NO forKey:@"status"];
        [dfsStatus synchronize];
        
    }
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        _btnCheckBox.enabled = NO;
        _txtViewNotes.editable = NO;
        _txtViewAccountDesc.editable = NO;
        
        [self textMethodForDisable:_txtFirstName];
        [self textMethodForDisable:_txtMiddleName];
        [self textMethodForDisable:_txtLastName];
        [self textMethodForDisable:_txtPrimaryEmail];
        [self textMethodForDisable:_txtCellNo];
        [self textMethodForDisable:_txtPrimaryPhone];
        [self textMethodForDisable:_txtSecondaryPhone];
        [self textMethodForDisable:_txtNoOfStory];
        [self textMethodForDisable:_txtShrubArea];
        [self textMethodForDisable:_txtTurfArea];
        
    }
    else
    {
        _btnCheckBox.enabled = YES;
        _txtViewNotes.editable = YES;
        _txtViewAccountDesc.editable = YES;
    }
    //...............
    //Nilind 15 Nov
    
    [self fetchImageDetailFromDataBase];
    //...........
    
    
    [self fetchJsonCountry];
    [self fetchJsonState];
    [self fetchCurrentServices];
    [self assignValues];
    [self getTaxCode];
    
    NSString *strIsBillingAddress=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"isBillingAddressSame"]];
    
    if ([strIsBillingAddress isEqualToString:@"true"]|| [strIsBillingAddress isEqualToString:@"1"])
    {
        chkForSameBillingAddress=YES;
        _btnCheckBox.selected=YES;
        [_imgCheckBox setImage:[UIImage imageNamed:@"check_box_2.png"]];
        
        _txtViewAdd1BillingAddress.text=_txtViewAdd1ServiceAddress.text;
        _txtViewAdd2BillingAddress.text=_txtViewAdd2ServiceAddress.text;
        //[ _btnCountryBillingAddress setTitle:_btnCountryServiceAddress.titleLabel.text forState:UIControlStateNormal];
        //[ _btnStateBillingAddress setTitle:_btnStateServiceAddress.titleLabel.text forState:UIControlStateNormal];
        
        [ _btnCountryBillingAddress setTitle:_btnCountryServiceAddress.currentTitle forState:UIControlStateNormal];
        [ _btnStateBillingAddress setTitle:_btnStateServiceAddress.currentTitle forState:UIControlStateNormal];
        
        _txtCityBillingAddress.text=_txtCityServiceAddress.text;
        _txtZipCodeBillingAddress.text=_txtZipcodeServiceAddress.text;
        
    }
    else
    {
        _btnCheckBox.selected=NO;
        chkForSameBillingAddress=NO;
        [_imgCheckBox setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    
    
    
    NSString *strStatus,*strSource,*strStage,*strVisibility,*strUrgency,*strInspector;
    strStatus=_btnStatus.titleLabel.text;
    strSource=_btnSource.titleLabel.text;
    strStage=_btnStage.titleLabel.text;
    strVisibility=_btnVisibility.titleLabel.text;
    strUrgency=_btnPriority.titleLabel.text;
    strInspector=_btnInspector.titleLabel.text;
    
    if(strStatus.length==0)
    {
        [_btnStatus setTitle:@"Select" forState:UIControlStateNormal];
    }
    if(strSource.length==0)
    {
        [_btnSource setTitle:@"Select" forState:UIControlStateNormal];
    }
    if(strStage.length==0)
    {
        [_btnStage setTitle:@"Select" forState:UIControlStateNormal];
    }
    if (strVisibility.length==0)
    {
        [_btnVisibility setTitle:@"Select" forState:UIControlStateNormal];
    }
    if (strUrgency.length==0)
    {
        [_btnPriority setTitle:@"Select" forState:UIControlStateNormal];
    }
    if (strInspector.length==0)
    {
        [_btnInspector setTitle:@"Select" forState:UIControlStateNormal];
    }
    NSMutableArray *arrImageToSave,*arrImageToSend;
    arrImageToSave=[[NSMutableArray alloc]init];
    arrImageToSend=[[NSMutableArray alloc]init];
    [defs setObject:arrImageToSave forKey:@"arrImageToSend"];
    [defs setObject:arrImageToSend forKey:@"arrImageToSave"];
    [defs synchronize];
    
    
    [_btnInspector setEnabled:NO];
    
    //Nilind 10 Jan
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtFirstName.leftView = paddingView;
    _txtFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtMiddleName.leftView = paddingView1;
    _txtMiddleName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtLastName.leftView = paddingView2;
    _txtLastName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtPrimaryEmail.leftView = paddingView3;
    _txtPrimaryEmail.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtCellNo.leftView = paddingView4;
    _txtCellNo.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtPrimaryPhone.leftView = paddingView5;
    _txtPrimaryPhone.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtPrimaryPhone.leftView = paddingView6;
    _txtPrimaryPhone.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtSecondaryPhone.leftView = paddingView8;
    _txtSecondaryPhone.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtCityServiceAddress.leftView = paddingView9;
    _txtCityServiceAddress.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView10 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtZipcodeServiceAddress.leftView = paddingView10;
    _txtZipcodeServiceAddress.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtCityBillingAddress.leftView = paddingView7;
    _txtCityBillingAddress.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView11 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtZipCodeBillingAddress.leftView = paddingView11;
    _txtZipCodeBillingAddress.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView12 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtTagServiceRequired.leftView = paddingView12;
    _txtTagServiceRequired.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *paddingView13 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtAreaSqFt.leftView = paddingView13;
    _txtAreaSqFt.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView14 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtLotSizeSqFt.leftView = paddingView14;
    _txtLotSizeSqFt.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView15 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtBedroom.leftView = paddingView15;
    _txtBedroom.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView16 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtBathroom.leftView = paddingView16;
    _txtBathroom.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView17 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtYearBuil.leftView = paddingView17;
    _txtYearBuil.leftViewMode = UITextFieldViewModeAlways;
    
    //End
    
    //Service POC
    [self textMethod:_txtServicePOCFirstName];
    [self textMethod:_txtServicePOCMiddleName];
    [self textMethod:_txtServicePOCLastName];
    [self textMethod:_txtServicePOCCellNo];
    [self textMethod:_txtServicePOCPrimaryEmail];
    [self textMethod:_txtServicePOCSecondaryEmail];
    [self textMethod:_txtServicePOCPrimaryPhone];
    [self textMethod:_txtServicePOCSecondaryPhone];
    
    //Billing POC
    
    [self textMethod:_txtBillingPOCFirstName];
    [self textMethod:_txtBillingPOCMiddleName];
    [self textMethod:_txtBillingPOCLastName];
    [self textMethod:_txtBillingPOCCellNo];
    [self textMethod:_txtBillingPOCPrimaryEmail];
    [self textMethod:_txtBillingPOCSecondaryEmail];
    [self textMethod:_txtBillingPOCPrimaryPhone];
    [self textMethod:_txtBillingPOCSecondaryPhone];
    
    
    [self textMethod:_txtServiceCounty];
    [self textMethod:_txtServiceSchoolDistrict];
    
    [self textMethod:_txtBillingCounty];
    [self textMethod:_txtBillingSchoolDistrict];
    
    [self textMethod:_txtNoOfStory];
    [self textMethod:_txtShrubArea];
    [self textMethod:_txtTurfArea];
    
    [_buttonImg1 setHidden:YES];
    [_buttonImg2 setHidden:YES];
    [_buttonImg3 setHidden:YES];
    
    [self hideBorderColorInSection:1];
    [self hideBorderColorInSection:2];
    [self hideBorderColorInSection:3];
    
    
    [self textMethodNew:_txtDriveTime];
    [self textMethodNew:_txtLatestStartTime];
    [self textMethodNew:_txtEarliestStartTime];
    
    [self assignValues];
    [self getTaxCode];
    [self setTextCorner];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self assignValues];
    [self forDisable];
    
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    [self fetchImageDetailFromDataBaseForGraph];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        
        /*  UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        ////Akshay 22 may 2019
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        ////
        
    }
    else
    {
        
        //Nilind 07 June Image Caption
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        //BOOL isFromBack=[defsBack boolForKey:@"isFromBackServiceDynamci"];
        BOOL isFromBack=[defsBack boolForKey:@"backfromDynamicView"];
        if (isFromBack)
        {
            
            //arrOfBeforeImageAll=nil;
            //arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            arrImageLattitude=nil;
            arrImageLattitude=[[NSMutableArray alloc]init];
            arrImageLongitude=nil;
            arrImageLongitude=[[NSMutableArray alloc]init];
            
            arrImageGraphLattitude=nil;
            arrImageGraphLattitude=[[NSMutableArray alloc]init];
            arrImageGraphLongitude=nil;
            arrImageGraphLongitude=[[NSMutableArray alloc]init];
            
            
            
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            arrNoImage=[[NSMutableArray alloc]init];
            
            // [defsBack setBool:NO forKey:@"isFromBackServiceDynamci"];
            [defsBack setBool:NO forKey:@"backfromDynamicView"];
            
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseForGraph];
            [self fetchImageDetailFromDataBase];
            
        }
        
        
        
        //End
        
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreviewSales"];
        if (yesFromDeleteImage)
        {
            isEditedInSalesAuto=YES;
            NSLog(@"Database edited");
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreviewSales"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrNoImage.count; k++)
            {
                
                NSDictionary *dictdat=arrNoImage[k];
                NSString *strImageName;
                if([dictdat isKindOfClass:[NSDictionary class]])
                {
                    strImageName=[dictdat valueForKey:@"leadImagePath"];
                }
                else
                {
                    strImageName=arrNoImage[k];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImagesSales"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrNoImage[k]];
                        
                    }
                }
                //            if (yesFoundName) {
                //
                //                [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                //
                //            }
            }
            if (!(arrTempBeforeImage.count==0)) {
                //arrNoImage=nil;
                //  arrNoImage=[[NSMutableArray alloc]init];
                // [arrNoImage addObjectsFromArray:arrTempBeforeImage];
                [arrNoImage removeObjectsInArray:arrTempBeforeImage];
            }
            else if((arrTempBeforeImage.count==0))
            {
                arrNoImage=nil;
                arrNoImage=[[NSMutableArray alloc]init];
            }
            
            //nIlind 09 April
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImagesSales"];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            
            
            [defsnew synchronize];
            
            [self saveImageToCoreData];
            
            //End
            
        }
        else
        {
            
        }
        //Nilind 07 June Image Caption
        
        
        //Change in image Captions
        BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //End
        
        
        [self downloadingImagesThumbNailCheck];
        //Nilind 10 Jan
        //NSString *strStatusss1;
        strStatusss=[matchesGeneralInfo valueForKey:@"statusSysName"];
        
        NSUserDefaults *dfsStageStatus=[NSUserDefaults standardUserDefaults];
        if ([dfsStageStatus boolForKey:@"backFromInspectionNew"] == YES)
        {
            strStatusss= [dfsStageStatus valueForKey:@"leadStatusSales"];
            strStageSysName=[dfsStageStatus valueForKey:@"stageSysNameSales"];
        }
        
        
        if([_imgCheckBox.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
        {
            [_txtViewAdd1BillingAddress setEditable:NO];
            [_txtViewAdd2BillingAddress setEditable:NO];
            [ _btnCountryBillingAddress setEnabled:NO];
            
            [ _btnStateBillingAddress setEnabled:NO];
            [_txtCityBillingAddress setEnabled:NO];
            [_txtZipCodeBillingAddress setEnabled:NO];
        }
        else
        {
            if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            {
            }
            else
            {
                [_txtViewAdd1BillingAddress setEditable:YES];
                [_txtViewAdd2BillingAddress setEditable:YES];
                [ _btnCountryBillingAddress setEnabled:YES];
                
                [ _btnStateBillingAddress setEnabled:YES];
                [_txtCityBillingAddress setEnabled:YES];
                [_txtZipCodeBillingAddress setEnabled:YES];
            }
        }
    }
    //...........
    
    if (arrOfImagenameCollewctionView.count==0) {
        
        _const_BtnSavePriority.constant=0;
        [_collectionViewGraph reloadData];
        
        
    } else {
        
        _const_BtnSavePriority.constant=210;
        
        [_collectionViewGraph reloadData];
        
    }
    if (arrNoImage.count==0) {
        
        //_const_BtnSavePriority.constant=0;
        [_collectionViewAddImage reloadData];
        
        
    } else {
        
        // _const_BtnSavePriority.constant=210;
        
        [_collectionViewAddImage reloadData];
        
    }
    [_buttonImg1 setHidden:YES];
    [_buttonImg2 setHidden:YES];
    [_buttonImg3 setHidden:YES];
    
    
    
    
}
//============================================================================
#pragma mark- Action Button
//============================================================================
- (IBAction)actoinBack:(id)sender
{
    //    for (UIViewController *controller in self.navigationController.viewControllers)
    //    {
    //        if ([controller isKindOfClass:[DashBoardView class]])
    //        {
    //            [self.navigationController popToViewController:controller animated:NO];
    //            break;
    //        }
    //    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backfromDynamicView"];
    [defs setBool:YES forKey:@"backfromSalesGeneralInfo"];
    [defs synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}

//============================================================================
#pragma mark- DYNAMIC LABEL AND BUTTON METHOD
//============================================================================

-(void)addButtons
{
    for(int i =0;i<arrOfLeads.count;i++)
    {
        
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+40, 0, 30, 30)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 15;
        lbl.layer.borderWidth=1.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 100, 20);
            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 30, 30);
            lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor = [UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 100, 20);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 30, 30);
            lbl.textColor = [UIColor darkGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            
        }
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];
        btn.titleLabel.numberOfLines=2;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollViewLeads addSubview:btn];
        
        [_scrollViewLeads addSubview:lbl];
    }
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//============================================================================
//============================================================================
#pragma mark- ---------------------ACTION ON BUTTONS-----------------
//============================================================================
//============================================================================


- (IBAction)actionOnStatus:(id)sender
{
    tblData.tag=101;
    [self tableLoad:tblData.tag];
    lblOk.hidden=YES;
}
- (IBAction)actionOnStage:(id)sender
{
    tblData.tag=102;
    [self tableLoad:tblData.tag];
    lblOk.hidden=YES;
}
- (IBAction)actionOnPriority:(id)sender
{
    tblData.tag=103;
    [self tableLoad:tblData.tag];
    lblOk.hidden=YES;
}
- (IBAction)actionOnVisibility:(id)sender
{
    tblData.tag=104;
    [self tableLoad:tblData.tag];
    lblOk.hidden=YES;
}
- (IBAction)actionOnSource:(id)sender
{
    NSArray *arrSourceMaster=[dictSalesLeadMaster valueForKey:@"SourceMasters"];
    
    arrSourceChange=[[NSMutableArray alloc]init];
    for (int i=0; i<arrSourceMaster.count; i++)
    {
        NSDictionary *dict=[arrSourceMaster objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]]isEqualToString:@"1"])
        {
            [arrSourceChange addObject:dict];
        }
    }
    
    
    if(arrSourceChange.count==0)
    {
        [global AlertMethod:@"Alert!" :@"No Source available"];
    }
    else
    {
        tblData.tag=105;
        [self tableLoad:tblData.tag];
    }
    lblOk.hidden=NO;
}
- (IBAction)actionOnInspector:(id)sender
{
    tblData.tag=106;
    [self tableLoad:tblData.tag];
    NSArray *arrEmployee;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    arrEmployee=[defs valueForKey:@"EmployeeList"];
    lblOk.hidden=YES;
}
// Service Address Action
- (IBAction)actionOnCountryServiceAddress:(id)sender
{
    [self endEditing];
    /*if ([_btnCountryServiceAddress.titleLabel.text isEqualToString:@"India"])
     {
     NSLog(@"NO STATE");
     [_btnStateServiceAddress setTitle:@"N/A" forState:UIControlStateNormal];
     }*/
    
    //Nilind
    
    [self fetchJsonCountry];
    tblData.tag=107;
    [self tableLoad:tblData.tag];
    lblOk.hidden=YES;
    //..........................................................
    
}

- (IBAction)actionOnStateServiceAddress:(id)sender
{
    [self endEditing];
    /* if ([_btnCountryServiceAddress.titleLabel.text isEqualToString:@"India"])
     {
     NSLog(@"NO STATE");
     [_btnStateServiceAddress setTitle:@"N/A" forState:UIControlStateNormal];
     }
     else
     {
     tblData.tag=108;
     [self tableLoad:tblData.tag];
     }*/
    
    //Nilind 15 Sept
    
    [self fetchJsonState];
    tblData.tag=108;
    [self tableLoad:tblData.tag];
    lblOk.hidden=YES;
    //...................................................................
}

// Billing Address Action
- (IBAction)actionOnCountryBillingAddress:(id)sender
{
    //    if ([_btnCountryBillingAddress.titleLabel.text isEqualToString:@"India"])
    //    {
    //        NSLog(@"NO STATE");
    //        [_btnStateBillingAddress setTitle:@"N/A" forState:UIControlStateNormal];
    //    }
    [self fetchJsonCountry];
    tblData.tag=109;
    [self tableLoad:tblData.tag];
    lblOk.hidden=YES;
}

- (IBAction)actionOnStateBillingAddress:(id)sender
{
    [self endEditing];
    //    if ([_btnCountryBillingAddress.titleLabel.text isEqualToString:@"India"])
    //    {
    //        NSLog(@"NO STATE");
    //        [_btnStateBillingAddress setTitle:@"N/A" forState:UIControlStateNormal];
    //    }
    //    else
    //    {
    //        tblData.tag=110;
    //        [self tableLoad:tblData.tag];
    //    }
    [self fetchJsonState];
    tblData.tag=110;
    [self tableLoad:tblData.tag];
    lblOk.hidden=YES;
    
}

- (IBAction)actionOnUploadDocument:(id)sender
{
    [self endEditing];
    UIButton *button = (UIButton *)sender;
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"pdf"];
    
    if (URL)
    {
        // Initialize Document Interaction Controller
        self->documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [self->documentInteractionController setDelegate:self];
        
        // Present Open In Menu
        [self->documentInteractionController presentOpenInMenuFromRect:[button frame] inView:self.view animated:YES];
    }
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [searchPaths objectAtIndex:0];
    NSLog(@"%@",documentsPath);
    //[self listAllLocalFiles];
}
- (void)listAllLocalFiles{
    // Fetch directory path of document for local application.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // NSFileManager is the manager organize all the files on device.
    NSFileManager *manager = [NSFileManager defaultManager];
    // This function will return all of the files' Name as an array of NSString.
    NSArray *files = [manager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    // Log the Path of document directory.
    NSLog(@"Directory: %@", documentsDirectory);
    // For each file, log the name of it.
    for (NSString *file in files) {
        NSLog(@"File at: %@", file);
    }
}
- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}
#pragma mark- ---------------------ACTION SHEET DELEGATE METHOD---------------------

- (IBAction)actionOnAttachImage:(id)sender
{
    [self attachImage];
}

-(void)openGalleryy{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    /*  if (buttonIndex ==0)
     {
     BOOL chk;
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     chk=[defs boolForKey:@"fromImagePrview"];
     if (chk==YES)
     {
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     [defs setBool:NO forKey:@"fromImagePrview"];
     [defs synchronize];
     
     }
     else
     {
     [self fetchImageFromCoreDataStandard];
     }
     NSLog(@"The Image Preview.");
     //NSArray *arrOfImagesDetail=[_dictOfWorkOrders valueForKey:@"ImagesDetail"];
     if ([arrImagePath isKindOfClass:[NSString class]])
     {
     [global AlertMethod:Info :NoBeforeImg];
     }
     else if (arrImagePath.count==0)
     {
     [global AlertMethod:Info :NoBeforeImg];
     }
     else
     {
     NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
     //            for (int k=0; k<arrImagePath.count; k++) {
     //                NSDictionary *dict=arrOfImagesDetail[k];
     //                [arrOfImagess addObject:[dict valueForKey:@"WoImagePath"]];
     //            }
     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
     bundle: nil];
     ImagePreviewSalesAuto
     *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
     
     NSMutableOrderedSet *orderedSet = [NSMutableOrderedSet orderedSetWithArray:arrImagePath];
     NSMutableArray *arrayWithoutDuplicates = (NSMutableArray*)[orderedSet mutableCopy];
     NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
     arrTemp=(NSMutableArray*)arrayWithoutDuplicates;
     
     objImagePreviewSalesAuto.arrOfImages=arrImagePath;//
     objImagePreviewSalesAuto.strFromSalesAuto=@"strFromSalesAuto";
     // objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
     [self.navigationController pushViewController:objImagePreviewSalesAuto animated:NO];
     
     }
     }*/
    if (buttonIndex ==7)
    {
        
        
        NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        
        NSArray *arrOfImagesDetail=arrNoImage;
        
        arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dictData=arrOfImagesDetail[k];
                
                [arrOfBeforeImages addObject:dictData];
                
            }else{
                
                [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
                
            }
            
        }
        
        arrOfImagesDetail=arrOfBeforeImages;
        
        if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
            [global AlertMethod:Info :NoBeforeImg];
        }else if (arrOfImagesDetail.count==0){
            [global AlertMethod:Info :NoBeforeImg];
        }
        else {
            NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
            for (int k=0; k<arrOfImagesDetail.count; k++) {
                if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dict=arrOfImagesDetail[k];
                    [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
                }else{
                    
                    [arrOfImagess addObject:arrOfImagesDetail[k]];
                    
                }
            }
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                     bundle: nil];
            ImagePreviewSalesAuto
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
            objByProductVC.arrOfImages=arrOfImagess;
            //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
            // [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
            [self.navigationController pushViewController:objByProductVC animated:YES];
            
        }
    }
    else if (buttonIndex == 0)
    {
        NSLog(@"The CApture Image.");
        /*if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
         {
         UIImagePickerController *picker = [[UIImagePickerController alloc] init];
         picker.delegate = self;
         picker.allowsEditing = YES;
         picker.sourceType = UIImagePickerControllerSourceTypeCamera;
         [self presentViewController:picker animated:YES completion:NULL];
         }
         else
         {
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Your device does not support camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
         [alert show];
         }*/
        //Nilind 15 nov
        
        
        //arrOfBeforeImageAll
        
        if (arrNoImage.count<10)
        {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                         }
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //............
        
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"The Gallery.");
        if (arrNoImage.count<10)
        {
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                             //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                             //                                     [alert show];
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    // NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}
#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backfromDynamicView"];
    [defs synchronize];
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    
    NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@%@.jpg",strDate,strTime,strLeadId];
    
    [arrNoImage addObject:strImageNamess];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    // [self saveImage:chosenImage :strImageNamess];
    
    [self resizeImage:chosenImage :strImageNamess];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //Lat long code
       
       CLLocationCoordinate2D coordinate = [global getLocation] ;
       NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
       NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
       [arrImageLattitude addObject:latitude];
       [arrImageLongitude addObject:longitude];
    
    //imageCaption
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption)
    {
        
        [self alertViewCustom];
        
    }
    else
    {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        [self saveImageToCoreData];
        
    }
    
   
    
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollViewDetail.contentSize.height - _scrollViewDetail.bounds.size.height);
        [_scrollViewDetail setContentOffset:bottomOffset animated:YES];
        [self saveImageToCoreData];
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollViewDetail.contentSize.height - _scrollViewDetail.bounds.size.height);
    [_scrollViewDetail setContentOffset:bottomOffset animated:YES];
    [self saveImageToCoreData];
    
}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=8;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollViewDetail.contentSize.height - _scrollViewDetail.bounds.size.height);
            [_scrollViewDetail setContentOffset:bottomOffset animated:YES];
            
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollViewDetail.contentSize.height - _scrollViewDetail.bounds.size.height);
        [_scrollViewDetail setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
//.................................................................................
-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

- (IBAction)actionOnSaveContinue:(id)sender
{
}
- (IBAction)actionOnCancel:(id)sender {
    
    [self endEditing];
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
}
//============================================================================
//============================================================================
#pragma mark- Save & Remove Image  Methods
//============================================================================
//============================================================================

- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}


//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT FIELD DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==_txtTagServiceRequired)
    {
        // tagText=0;
        //[self.view setFrame:CGRectMake(0,-150,self.view.frame.size.width,self.view.frame.size.height)];
    }
    if (textField==_txtBranchName)
    {
        chkForBranchName=YES;
    }
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    return YES;
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *strStatusss2;
    strStatusss=[matchesGeneralInfo valueForKey:@"statusSysName"];
    
    NSUserDefaults *dfsStageStatus=[NSUserDefaults standardUserDefaults];
    if ([dfsStageStatus boolForKey:@"backFromInspectionNew"] == YES)
    {
        strStatusss= [dfsStageStatus valueForKey:@"leadStatusSales"];
        strStageSysName=[dfsStageStatus valueForKey:@"stageSysNameSales"];
    }
    
    
    if (range.location == 0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    
    if(textField==_txtTagServiceRequired)
    {
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            return  YES;
        }
    }
    if(textField==_txtPrimaryPhone)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtPrimaryPhone.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
            
            //
        }
    }
    if(textField==_txtSecondaryPhone)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtSecondaryPhone.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
            
            //
        }
    }
    if(textField==_txtAreaSqFt)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 4;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtAreaSqFt.text];
            
            if (isNuberOnly)
            {
                if([string isEqualToString:@""])
                {
                    
                    NSString *strTurf = [_txtAreaSqFt.text stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:string];
                    
                    _txtTurfArea.text=[self calculateTurfArea:_txtLotSizeSqFt.text Area:strTurf Story:_txtNoOfStory.text ShrubArea:_txtShrubArea.text];
                    
                    _txtLinearSqFt.text = [self calculateLinerSquareFt:strTurf Story:_txtNoOfStory.text];
                    
                }
                else
                {
                    NSString *strTurf = [NSString stringWithFormat:@"%@%@",_txtAreaSqFt.text,string];
                    
                    _txtTurfArea.text=[self calculateTurfArea:_txtLotSizeSqFt.text Area:strTurf Story:_txtNoOfStory.text ShrubArea:_txtShrubArea.text];
                    
                    _txtLinearSqFt.text = [self calculateLinerSquareFt:strTurf Story:_txtNoOfStory.text];
                    
                    
                }
                
                return YES;
            }
            else
            {
                return NO;
            }
        }
    }
    if(textField==_txtBedroom)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 4;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :100 :(int)textField.text.length :@".0123456789" :_txtBedroom.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
        }
    }
    if(textField==_txtBathroom)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 4;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :100 :(int)textField.text.length :@".0123456789" :_txtBathroom.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
        }
    }
    if(textField==_txtLotSizeSqFt)
    {
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :100 :(int)textField.text.length :@".0123456789" :_txtLotSizeSqFt.text];
            
            if (isNuberOnly)
            {
                if([string isEqualToString:@""])
                {
                    
                    NSString *strTurf = [_txtLotSizeSqFt.text stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:string];
                    
                    _txtTurfArea.text=[self calculateTurfArea:strTurf Area:_txtAreaSqFt.text Story:_txtNoOfStory.text ShrubArea:_txtShrubArea.text];
                }
                else
                {
                    NSString *strTurf = [NSString stringWithFormat:@"%@%@",_txtLotSizeSqFt.text,string];
                    
                    _txtTurfArea.text=[self calculateTurfArea:strTurf Area:_txtAreaSqFt.text Story:_txtNoOfStory.text ShrubArea:_txtShrubArea.text];
                    
                }
                
                return YES;
            }
            else
            {
                return NO;
            }
        }
    }
    if(textField==_txtLinearSqFt)
    {
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 4;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :100 :(int)textField.text.length :@".0123456789" :_txtLinearSqFt.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
        }
    }
    if(textField==_txtYearBuil)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 4;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :4 :(int)textField.text.length :@"0123456789" :_txtYearBuil.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
        }
    }
    //Turf Area Change
    
    if(textField==_txtNoOfStory)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 4;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtNoOfStory.text];
            
            if (isNuberOnly)
            {
                if([string isEqualToString:@""])
                {
                    
                    NSString *strTurf = [_txtNoOfStory.text stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:string];
                    
                    _txtTurfArea.text=[self calculateTurfArea:_txtLotSizeSqFt.text Area:_txtAreaSqFt.text Story:strTurf ShrubArea:_txtShrubArea.text];
                    
                    _txtLinearSqFt.text = [self calculateLinerSquareFt:_txtAreaSqFt.text Story:strTurf];
                    
                }
                else
                {
                    NSString *strTurf = [NSString stringWithFormat:@"%@%@",_txtNoOfStory.text,string];
                    
                    _txtTurfArea.text=[self calculateTurfArea:_txtLotSizeSqFt.text Area:_txtAreaSqFt.text Story:strTurf ShrubArea:_txtShrubArea.text];
                    
                    _txtLinearSqFt.text = [self calculateLinerSquareFt:_txtAreaSqFt.text Story:strTurf];
                    
                    
                }
                
                return YES;
            } else {
                return NO;
            }
        }
    }
    if(textField==_txtShrubArea)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 4;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@".0123456789" :_txtShrubArea.text];
            
            if (isNuberOnly)
            {
                if([string isEqualToString:@""])
                {
                    
                    NSString *strTurf = [_txtShrubArea.text stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:string];
                    
                    _txtTurfArea.text=[self calculateTurfArea:_txtLotSizeSqFt.text Area:_txtAreaSqFt.text Story:_txtNoOfStory.text ShrubArea:strTurf];
                }
                else
                {
                    NSString *strTurf = [NSString stringWithFormat:@"%@%@",_txtShrubArea.text,string];
                    
                    _txtTurfArea.text=[self calculateTurfArea:_txtLotSizeSqFt.text Area:_txtAreaSqFt.text Story:_txtNoOfStory.text ShrubArea:strTurf];
                    
                }
                
                return YES;
            }
            else
            {
                return NO;
            }
        }
    }
    if(textField==_txtTurfArea)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 4;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :50 :(int)textField.text.length :@".0123456789" :_txtTurfArea.text];
            
            if (isNuberOnly)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
    }
    //End
    if(textField==_txtBillingPOCPrimaryPhone)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 15;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtBillingPOCPrimaryPhone.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
        }
    }
    if(textField==_txtBillingPOCSecondaryPhone)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 15;*/
            
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtBillingPOCSecondaryPhone.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
        }
    }
    if(textField==_txtServicePOCPrimaryPhone)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 15;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtServicePOCPrimaryPhone.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
        }
    }
    if(textField==_txtServicePOCSecondaryPhone)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /*if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 15;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtServicePOCSecondaryPhone.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
            
        }
    }
    if(textField==_txtCellNo)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /*if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 15;*/
            
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtCellNo.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
            
            
            
        }
    }
    if(textField==_txtServicePOCCellNo)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 15;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtServicePOCCellNo.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
            
        }
    }
    if(textField==_txtBillingPOCCellNo)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /*if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 15;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :15 :(int)textField.text.length :@"0123456789" :_txtBillingPOCCellNo.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
        }
    }
    if(textField==_txtZipcodeServiceAddress)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 5;*/
            //_txtZipCodeBillingAddress
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :5 :(int)textField.text.length :@"0123456789" :_txtZipcodeServiceAddress.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
            
        }
    }
    if(textField==_txtZipCodeBillingAddress)
    {
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            /* if(range.length + range.location > textField.text.length)
             {
             return NO;
             }
             
             NSUInteger newLength = [textField.text length] + [string length] - range.length;
             return newLength <= 5;*/
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :5 :(int)textField.text.length :@"0123456789" :_txtZipCodeBillingAddress.text];
            
            if (isNuberOnly) {
                return YES;
            } else {
                return NO;
            }
            
            //
        }
    }
    
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        return true;
    }
    else
    {
        if (textField==_txtBranchName)
        {
            return false;
        }
        else
            return true;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    if (tagText==0)
    {
        //tagText=1;
        //[self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    }
    
    if (textField == _txtCityServiceAddress || textField == _txtZipcodeServiceAddress)
    {
        [self sameAsServiceAddress];
    }
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    //[self.view setFrame:CGRectMake(0,-150,self.view.frame.size.width,self.view.frame.size.height)];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        [_txtViewNotes resignFirstResponder];
        [_txtViewDescription resignFirstResponder];
        [_txtViewAccountDesc resignFirstResponder];
        
        [_txtViewAdd1ServiceAddress resignFirstResponder];
        [_txtViewAdd2ServiceAddress resignFirstResponder];
        [_txtViewAdd1BillingAddress resignFirstResponder];
        [_txtViewAdd2BillingAddress resignFirstResponder];
        [txtFieldCaption resignFirstResponder];
        [txtViewImageDescription resignFirstResponder];
        
        [_txtViewAddressNotesServiceAddress resignFirstResponder];
        [_txtViewDirectionServiceAddress resignFirstResponder];
        
        [_txtViewProblemescription resignFirstResponder];
        
        
        return NO;
    }
 
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    
    return YES;
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    /* if (textView==_txtViewNotes)
     {
     check=0;
     }
     if (textView==_txtViewDescription)
     {
     check=1;
     }
     if (textView==_txtViewAdd1ServiceAddress)
     {
     check=2;
     }
     if (textView==_txtViewAdd2ServiceAddress)
     {
     check=3;
     }
     if (textView==_txtViewAdd1BillingAddress)
     {
     check=4;
     }
     if (textView==_txtViewAdd2BillingAddress)
     {
     check=5;
     }*/
    
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    //[self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    /* if ([_txtViewNotes.text isEqualToString:@""])
     {
     textView.text = @"Enter Comment..!!";
     
     textView.textColor =[UIColor lightGrayColor];
     }
     if ([_txtViewDescription.text isEqualToString:@""])
     {
     _txtViewDescription.text =  @"Enter Description..!!";
     _txtViewDescription.textColor =[UIColor lightGrayColor];
     }
     if ([_txtViewAdd1ServiceAddress.text isEqualToString:@""])
     {
     _txtViewAdd1ServiceAddress.text =  @"Enter Address Line1..!!";
     _txtViewAdd1ServiceAddress.textColor =[UIColor GHJ];
     }
     if ([_txtViewAdd2ServiceAddress.text isEqualToString:@""])
     {
     _txtViewAdd2ServiceAddress.text =  @"Enter Address Line2..!!";
     _txtViewAdd2ServiceAddress.textColor =[UIColor lightGrayColor];
     }
     if ([_txtViewAdd1BillingAddress.text isEqualToString:@""])
     {
     _txtViewAdd1BillingAddress.text =  @"Enter Address Line1..!!";
     _txtViewAdd1BillingAddress.textColor =[UIColor lightGrayColor];
     }
     if ([_txtViewAdd2BillingAddress.text isEqualToString:@""])
     {
     _txtViewAdd2BillingAddress.text =  @"Enter Address Line2..!!";
     _txtViewAdd2BillingAddress.textColor =[UIColor lightGrayColor];
     }*/
    
    if (textView == _txtViewAdd1ServiceAddress || textView == _txtViewAdd2ServiceAddress)
    {
        [self sameAsServiceAddress];
    }
    
    
}
//============================================================================
//============================================================================
#pragma mark- --------------Actions Button Selecting----------------
//============================================================================
//============================================================================

-(void)setTableFrame
{
    
    //    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    //    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    //    {
    //        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    //    }
    //    [self.view addSubview:tblData];
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    //Nilind 19 Jan
    
    lblOk=[[UILabel alloc]init];
    lblOk.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    lblOk.textColor=[UIColor whiteColor];
    lblOk.text=@"OK";
    lblOk.textAlignment=NSTextAlignmentCenter;
    lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-80, tblData.frame.origin.y+tblData.frame.size.height+2, 160, 40);
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-60, tblData.frame.origin.y+tblData.frame.size.height+2, 120, 40);
    }
    lblOk.layer.borderWidth=1.0;
    lblOk.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    lblOk.layer.cornerRadius=5.0;
    [viewBackGround addSubview:lblOk];
    
    //.......
    
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
        case 103:
        {
            [self setTableFrame];
            break;
        }
        case 104:
        {
            [self setTableFrame];
            break;
        }
        case 105:
        {
            [self setTableFrame];
            break;
        }
        case 106:
        {
            [self setTableFrame];
            break;
        }
        case 107:
        {
            [self setTableFrame];
            break;
        }
        case 108:
        {
            [self setTableFrame];
            break;
        }
        case 109:
        {
            [self setTableFrame];
            break;
        }
            
        case 110:
        {
            [self setTableFrame];
            break;
        }
        case 111:
        {
            [self setTableFrame];
            break;
        }
        case 112:
        {
            [self setTableFrame];
            break;
        }
        case 113:
        {
            [self setTableFrame];
            break;
        }
        case 205:
        {
            [self setTableFrameForEmailIds :@"primary"];
            break;
        }
        case 206:
        {
            [self setTableFrameForEmailIds :@"secondary"];
            break;
        }
            
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tblData.tag==101)
    {
        return [arrStatusMaster count];
    }
    else if (tblData.tag==102)
    {
        return [arrStageMaster count];
    }
    else if (tblData.tag==103)
    {
        return [arrUrgency count];
    }
    else if (tblData.tag==104)
    {
        return [arrVisibility count];
    }
    else if (tblData.tag==105)
    {
        return [arrSourceChange count];//arrSource
    }
    else if (tblData.tag==106)
    {
        return [arrInspector count];
    }
    else if (tblData.tag==107)
    {
        return [arrCountries count];
    }
    else if (tblData.tag==108)
    {
        return [arrState count];
    }
    else if (tblData.tag==109)
    {
        return [arrCountries count];
    }
    else if (tblData.tag==110)
    {
        return [arrState count];
    }
    else if(tblData.tag==111)
    {
        return [arrTaxCode count];
    }
    else if(tblData.tag==112)
    {
        return [arrPropertyType count];
    }
    else if(tblData.tag==113)
    {
        return [arrServiceAddressSubType count];
    }
    else if (tblData.tag==205)
    {
        return [arrDataTblViewNew count];
    }
    else if (tblData.tag==206)
    {
        return [arrDataTblViewNew count];
    }
    else
    {
        return [arrData count];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if (tblData.tag==101)
    {
        cell.textLabel.text=[arrStatusMaster objectAtIndex:indexPath.row];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else if (tblData.tag==102)
    {
        cell.textLabel.text=[arrStageMaster objectAtIndex:indexPath.row];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else if (tblData.tag==103)
    {
        cell.textLabel.text=[arrUrgency objectAtIndex:indexPath.row];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else if (tblData.tag==104)
    {
        cell.textLabel.text=[arrVisibility objectAtIndex:indexPath.row];
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else if (tblData.tag==105)
    {
        
        
        NSDictionary *dictData=[arrSourceChange objectAtIndex:indexPath.row];//arrDataTblView
        cell.textLabel.text=[dictData valueForKey:@"Name"];
        NSString *str=[dictData valueForKey:@"SourceSysName"];//[dictData valueForKey:@"Name"];//
        
        long index=10000000;
        
        for (int k=0; k<arrSourceSelectedId.count; k++)//arrSourceSelectedValue
        {
            if ([arrSourceSelectedId containsObject:str])//arrSourceSelectedValue
            {
                index=indexPath.row;
                break;
            }
        }
        
        if (indexPath.row==index)
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        //...............
    }
    else if (tblData.tag==106)
    {
        cell.textLabel.text=[arrInspector objectAtIndex:indexPath.row];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    }
    else if (tblData.tag==107)
    {
        NSDictionary *dictData=[arrCountries objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"Name"];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        //cell.textLabel.text=[arrCountries objectAtIndex:indexPath.row];
    }
    else if (tblData.tag==108)
    {
        
        //cell.textLabel.text=[arrState objectAtIndex:indexPath.row];
        NSDictionary *dictData=[arrState objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"Name"];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        
    }
    else if (tblData.tag==109)
    {
        // cell.textLabel.text=[arrCountries objectAtIndex:indexPath.row];
        NSDictionary *dictData=[arrCountries objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"Name"];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else if (tblData.tag==110)
    {
        // cell.textLabel.text=[arrState objectAtIndex:indexPath.row];
        NSDictionary *dictData=[arrState objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"Name"];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    }
    else if (tblData.tag==111)
    {
        NSDictionary *dictData=[arrTaxCode objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"Name"];
        //[cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if ([strTaxCode isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]]])
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        
    }
    else if (tblData.tag==112)
    {
        NSDictionary *dictData=[arrPropertyType objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"Name"];
        
        if ([strSelectedProprtyTypeSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]]])
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        
    }
    else if (tblData.tag==113)
    {
        cell.textLabel.text=[arrServiceAddressSubType objectAtIndex:indexPath.row];
        
        if ([strServiceAddressSubType isEqualToString:[NSString stringWithFormat:@"%@",[arrServiceAddressSubType objectAtIndex:indexPath.row]]])
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
    else if (tblData.tag==205)
    {
        cell.textLabel.text=[arrDataTblViewNew objectAtIndex:indexPath.row];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    }
    else if (tblData.tag==206)
    {
        cell.textLabel.text=[arrDataTblViewNew objectAtIndex:indexPath.row];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    }
    
    else
    {
        cell.textLabel.text=[arrData objectAtIndex:indexPath.row];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    isSelectEnable=NO;
    chkStage=NO;
    NSInteger i;
    i=tblData.tag;
    switch (i)
    {
        case 101:
        {
            
            chkStage=YES;
            [_btnStatus setTitle:[arrStatusMaster objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            NSArray *temparrStatusMaster=[dictSalesLeadMaster valueForKey:@"LeadStatusMasters"];
            if (temparrStatusMaster.count==0)
            {
                
            }
            else
            {
                NSDictionary *dict=[temparrStatusMaster objectAtIndex:indexPath.row];
                strStatusSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                NSString *strStatusId;
                strStatusId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusId"]];
                strStatusIdForStage=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusId"]];
                
                // [self titleStage];
                //Nilind
                
                arrStageMaster=[[NSMutableArray alloc]init];
                arrStageSysName=[[NSMutableArray alloc]init];
                arrStageIdMaster=[[NSMutableArray alloc]init];
                
                NSArray *temparrStageMaster=[dictSalesLeadMaster valueForKey:@"LeadStageMasters"];
                for (int i=0; i<temparrStageMaster.count; i++)
                {
                    NSDictionary *dict=[temparrStageMaster objectAtIndex:i];
                    if ([strStatusId isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusId"]]])
                    {
                        [arrStageMaster addObject:[dict valueForKey:@"Name"]];
                        [arrStageSysName addObject:[dict valueForKey:@"SysName"]];
                        if (chkStage==YES)
                        {
                            chkStage=NO;
                            //  strStageSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                        }
                        
                    }
                }
                [_btnStage setTitle:[arrStageMaster objectAtIndex:0] forState:UIControlStateNormal];
                // [self findStageId:_btnStage.titleLabel.text];
                //...............
                
            }
            break;
        }
        case 102:
        {
            
            [_btnStage setTitle:[arrStageMaster objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            strStageSysName=[arrStageSysName objectAtIndex:indexPath.row];
            
            NSArray *temparrStageMaster=[dictSalesLeadMaster valueForKey:@"LeadStageMasters"];
            if (temparrStageMaster.count==0)
            {
            }
            else
            {
                
                
                // strStageSysName=[arrStageSysName objectAtIndex:indexPath.row];
                
            }
            // [self findStageId:_btnStage.titleLabel.text];
            break;
        }
        case 103:
        {
            [_btnPriority setTitle:[arrUrgency objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            NSArray *temparrUrgency=[dictSalesLeadMaster valueForKey:@"UrgencyMasters"];
            if (temparrUrgency.count==0)
            {
            }
            else
            {
                NSDictionary *dict=[temparrUrgency objectAtIndex:indexPath.row];
                strPrioritySysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
            }
            break;
        }
        case 104:
        {
            [_btnVisibility setTitle:[arrVisibility objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            break;
        }
        case 105:
        {
            isSelectEnable=YES;
            //[_btnSource setTitle:[arrSource objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            //     NSArray *temparrSource=[dictSalesLeadMaster valueForKey:@"SourceMasters"];
            NSDictionary *dict=[arrSourceChange objectAtIndex:indexPath.row];//arrSourceChange//temparrSource
            if (arrSourceChange.count==0)
            {
            }
            else
            {
#pragma mark: TEMP COMMENT
                //strSourceSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SourceSysName"]];
            }
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                [arrIndex addObject:[NSString stringWithFormat:@"%lu",(unsigned long)indexPath.row]];
                [arrSourceSelectedValue addObject:[dict valueForKey:@"Name"]];
                // [arrAdd addObject:[dict valueForKey:@"Name"]];
                [arrSourceSelectedId addObject:[dict valueForKey:@"SourceSysName"]];
                NSLog(@"ArrIndex After add>>%@",arrIndex);
                NSLog(@"arrSourceSelectedId After add>>%@",arrSourceSelectedId);
                
            }
            else
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
                [arrIndex removeObject:str];
                [arrSourceSelectedId removeObject:[dict valueForKey:@"SourceSysName"]];
                [arrSourceSelectedValue removeObject:[dict valueForKey:@"Name"]];
                // [arrAdd removeObject:[dict valueForKey:@"Name"]];
                NSLog(@"ArrIndex After Remove>>%@",arrIndex);
                NSLog(@"arrSourceSelectedId After remove>>%@",arrSourceSelectedId);
                
                
            }
            
            
            break;
        }
            //        case 106:
            //        {
            //            [_btnInspector setTitle:[arrInspector objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            //            //Nilind 14 Sept
            //            NSArray *arrEmployee;
            //            //NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            //            arrEmployee=[dictMasters valueForKey:@"Employees"];
            //            if(arrEmployee.count==0)
            //            {}
            //            else
            //            {
            //            NSDictionary *dict=[arrEmployee objectAtIndex:indexPath.row];
            //            strInspectorId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeId"]];
            //            }
            //            //...............................................
            //            break;
            //        }
        case 106:
        {
            [_btnInspector setTitle:[arrInspector objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            //Nilind 14 Sept
            NSArray *arrEmployee;
            /* //NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
             arrEmployee=[dictMasters valueForKey:@"Employees"];*/
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            arrEmployee=[defs valueForKey:@"EmployeeList"];
            if(arrEmployee.count==0)
            {}
            else
            {
                NSDictionary *dict=[arrEmployee objectAtIndex:indexPath.row];
                strInspectorId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeId"]];
            }
            //...............................................
            break;
        }
        case 107:
        {
            //[_btnCountryServiceAddress setTitle:[arrCountries objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            NSDictionary *dictData=[arrCountries objectAtIndex:indexPath.row];
            // cell.textLabel.text=[dictData valueForKey:@"Name"];
            [_btnCountryServiceAddress setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
            strCountryService=[dictData valueForKey:@"CountrySysName"];
            
            
            [self sameAsServiceAddress];
           
            break;
        }
        case 108:
        {
            //[_btnStateServiceAddress setTitle:[arrState objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            NSDictionary *dictData=[arrState objectAtIndex:indexPath.row];
            [_btnStateServiceAddress setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
            strStateService=[dictData valueForKey:@"StateSysName"];
            
            [self sameAsServiceAddress];
            
            break;
        }
        case 109:
        {
            // [_btnCountryBillingAddress setTitle:[arrCountries objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            NSDictionary *dictData=[arrCountries objectAtIndex:indexPath.row];
            [_btnCountryBillingAddress setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
            strCountryBilling=[dictData valueForKey:@"CountrySysName"];
            break;
        }
        case 110:
        {
            // [_btnStateBillingAddress setTitle:[arrState objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            NSDictionary *dictData=[arrState objectAtIndex:indexPath.row];
            [_btnStateBillingAddress setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
            strStateBilling=[dictData valueForKey:@"StateSysName"];
            break;
        }
        case 111:
        {
            NSDictionary *dictData=[arrTaxCode objectAtIndex:indexPath.row];
            /* [_btnTaxCode setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
             strTaxCode=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];*/
            
            strTaxCode=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
            
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else
            {
                strTaxCode = @"";
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                
            }
            
            if ([strTaxCode isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]]])
            {
                [_btnTaxCode setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
            }
            else
            {
                [_btnTaxCode setTitle:@"Select Tax Code" forState:UIControlStateNormal];
            }
            
            break;
        }
        case 112:
        {
            NSDictionary *dictData=[arrPropertyType objectAtIndex:indexPath.row];
            
            strSelectedProprtyTypeSysName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
            
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else
            {
                strSelectedProprtyTypeSysName = @"";
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                
            }
            
            if ([strSelectedProprtyTypeSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]]])
            {
                [_btnPropertyType setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
            }
            else
            {
                [_btnPropertyType setTitle:@"Property Type" forState:UIControlStateNormal];
            }
            
            break;
        }
        case 113:
        {
            
            strServiceAddressSubType=[NSString stringWithFormat:@"%@",[arrServiceAddressSubType objectAtIndex:indexPath.row]];
            [_btnServiceAddressSubType setTitle:strServiceAddressSubType forState:UIControlStateNormal];
            
            //            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            //            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            //            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            //            {
            //                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            //            }
            //            else
            //            {
            //                strServiceAddressSubType = @"";
            //                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            //            }
            //
            //            if ([strServiceAddressSubType isEqualToString:[NSString stringWithFormat:@"%@",[arrServiceAddressSubType objectAtIndex:indexPath.row]]])
            //            {
            //                [_btnServiceAddressSubType setTitle:strServiceAddressSubType forState:UIControlStateNormal];
            //            }
            //            else
            //            {
            //                [_btnServiceAddressSubType setTitle:@"--- Select ---" forState:UIControlStateNormal];
            //            }
            
            break;
        }
        case 205:
        {
            
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Edit Email-Id"
                                                                                      message: @""
                                                                               preferredStyle:UIAlertControllerStyleAlert];
            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                textField.placeholder = @"Enter Email-Id Here...";
                textField.tag=7;
                textField.delegate=self;
                textField.text=arrOfPrimaryEmailNew[indexPath.row];
                textField.textColor = [UIColor blackColor];
                textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                textField.borderStyle = UITextBorderStyleRoundedRect;
                textField.keyboardType=UIKeyboardTypeDefault;
            }];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                NSArray * textfields = alertController.textFields;
                UITextField * txtHistoricalDays = textfields[0];
                if (txtHistoricalDays.text.length>0) {
                    
                    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
                    
                    if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
                    {
                        
                        [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                        
                    }else{
                        
                        if ([arrOfPrimaryEmailNew containsObject:txtHistoricalDays.text]) {
                            
                            if ([arrOfPrimaryEmailNew[indexPath.row] isEqualToString:txtHistoricalDays.text]) {
                                
                                [arrOfPrimaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                
                                arrDataTblViewNew=[[NSMutableArray alloc]init];
                                [arrDataTblViewNew addObjectsFromArray:arrOfPrimaryEmailNew];
                                [tblData reloadData];
                                [self tablViewForPrimaryEmailIds];
                                
                            } else {
                                
                                //AlertViewForAlreadyEmailPresentOnEdit
                                [self performSelector:@selector(AlertViewForAlreadyEmailPresentOnEdit) withObject:nil afterDelay:0.2];
                                
                            }
                            
                        } else {
                            
                            
                            [arrOfPrimaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                            
                            arrDataTblView=[[NSMutableArray alloc]init];
                            [arrDataTblViewNew addObjectsFromArray:arrOfPrimaryEmailNew];
                            [tblData reloadData];
                            [self tablViewForPrimaryEmailIds];
                            
                        }
                    }
                } else {
                    
                    [self performSelector:@selector(AlertViewForEmptyEmailEdit) withObject:nil afterDelay:0.2];
                    
                }
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [self tablViewForPrimaryEmailIds];
                
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
            break;
        }
        case 206:
        {
            
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Edit Email-Id"
                                                                                      message: @""
                                                                               preferredStyle:UIAlertControllerStyleAlert];
            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                textField.placeholder = @"Enter Email-Id Here...";
                textField.tag=7;
                textField.delegate=self;
                textField.text=arrOfSecondaryEmailNew[indexPath.row];
                textField.textColor = [UIColor blackColor];
                textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                textField.borderStyle = UITextBorderStyleRoundedRect;
                textField.keyboardType=UIKeyboardTypeDefault;
            }];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                NSArray * textfields = alertController.textFields;
                UITextField * txtHistoricalDays = textfields[0];
                if (txtHistoricalDays.text.length>0) {
                    
                    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
                    
                    if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
                    {
                        
                        [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                        
                    }else{
                        
                        if ([arrOfSecondaryEmailNew containsObject:txtHistoricalDays.text]) {
                            
                            if ([arrOfSecondaryEmailNew[indexPath.row] isEqualToString:txtHistoricalDays.text]) {
                                
                                [arrOfSecondaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                
                                arrDataTblViewNew=[[NSMutableArray alloc]init];
                                [arrDataTblViewNew addObjectsFromArray:arrOfSecondaryEmailNew];
                                [tblData reloadData];
                                [self tablViewForSecondaryEmailIds];
                                
                            } else {
                                
                                
                                //AlertViewForAlreadyEmailPresentOnEdit
                                [self performSelector:@selector(AlertViewForAlreadyEmailPresentOnEdit) withObject:nil afterDelay:0.2];
                                
                            }
                            
                        } else {
                            
                            [arrOfSecondaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                            
                            arrDataTblView=[[NSMutableArray alloc]init];
                            [arrDataTblViewNew addObjectsFromArray:arrOfSecondaryEmailNew];
                            [tblData reloadData];
                            [self tablViewForSecondaryEmailIds];
                            
                        }
                    }
                    
                } else {
                    
                    [self performSelector:@selector(AlertViewForEmptyEmailEdit) withObject:nil afterDelay:0.2];
                    
                }
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [self tablViewForSecondaryEmailIds];
                
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
            
            break;
        }
            
        default:
            break;
    }
    if (isSelectEnable)
    {
        
    }
    else
    {
        [tblData removeFromSuperview];
        [viewBackGround removeFromSuperview];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==205) {
        
        return true;
        
    }else if (tableView.tag==206){
        
        return true;
        
    }else
        
        return false;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==205) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            [arrOfPrimaryEmailNew removeObjectAtIndex:indexPath.row];
            //arrDataTblView=[[NSMutableArray alloc]init];
            arrDataTblViewNew=[[NSMutableArray alloc]init];
            [arrDataTblViewNew addObjectsFromArray:arrOfPrimaryEmailNew];
            [tblData reloadData];
        }
    }else if (tableView.tag==206){
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            [arrOfSecondaryEmailNew removeObjectAtIndex:indexPath.row];
            //arrDataTblView=[[NSMutableArray alloc]init];
            arrDataTblViewNew=[[NSMutableArray alloc]init];
            [arrDataTblViewNew addObjectsFromArray:arrOfSecondaryEmailNew];
            [tblData reloadData];
            
        }
    }
}

#pragma mark- CoreDataFetch
-(void)FetchFromCoreDataToShowTaskList{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTask=[NSEntityDescription entityForName:@"SalesAutomationAppoint" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityTask];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"companyKey = %@",strCompanyKey];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerTaslList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTaslList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTaslList performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerTaslList fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        matches=arrAllObj[0];
        
        _dictSaleAuto = [matches valueForKey:@"appointResponseDict"];
        NSString *strUsername,*strEmpid,*strCompanykeey;
        strCompanykeey=[matches valueForKey:@"companyKey"];
        strUsername=[matches valueForKey:@"userName"];
        strEmpid=[matches valueForKey:@"empId"];
        NSLog(@"%@",_dictSaleAuto);
        NSLog(@"\nEmployeeID>>%@\nUserName>>%@\nCompanyKey>>%@",strEmpid,strUsername,strCompanykeey);
        
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

- (IBAction)actionOnSave:(id)sender
{
    [self endEditing];
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame)
    {
        NSUserDefaults *dfsStageStatus=[NSUserDefaults standardUserDefaults];
        [dfsStageStatus setBool:NO forKey:@"backFromInspectionNew"];
        [dfsStageStatus setValue:strStatusss forKey:@"leadStatusSales"];
        [dfsStageStatus setValue:strStageSysName forKey:@"stageSysNameSales"];
        [dfsStageStatus synchronize];
        
        
        UIStoryboard *storyBoar=[UIStoryboard storyboardWithName:@"SalesMainSelectService" bundle:nil];
        SalesAutomationInspection *objSaleAutoInspection=[storyBoar instantiateViewControllerWithIdentifier:@"SalesAutomationInspection"];
        objSaleAutoInspection.strBrnachSysName=[matchesGeneralInfo valueForKey:@"branchSysName"];
        objSaleAutoInspection.strDepartmentSysName=[matchesGeneralInfo valueForKey:@"departmentSysName"];
        [self.navigationController pushViewController:objSaleAutoInspection animated:YES];
    }
    else
    {
        [self trimTextField];
        
        BOOL chkMailPrimary,chkSecondaryEmail;
        chkMailPrimary=[self NSStringIsValidEmail:_txtPrimaryEmail.text];
        chkSecondaryEmail=[self NSStringIsValidEmail:_txtSecondayEmail.text];
        //  if(_txtPrimaryEmail.text.length==0)
        //  {
        //      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter  email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        //      [alert show];
        //  }
        //  else
        if (_txtLead.text.length==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter opportunity name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_txtFirstName.text.length==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter  first name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_txtLastName.text.length==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter  last name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        
        else if ([_btnInspector.titleLabel.text isEqualToString:@"Select"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Sales Representative not present, please sync master and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        
        else if ([_txtViewAdd1ServiceAddress.text isEqualToString:@"Enter Address Line1..!!"]||_txtViewAdd1ServiceAddress.text.length==0||[_txtViewAdd1ServiceAddress.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter service address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_btnStateServiceAddress.titleLabel.text.length==0||[_btnStateServiceAddress.titleLabel.text isEqualToString:@""]||[_btnStateServiceAddress.titleLabel.text isEqualToString:@"(null)"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select service state" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_btnStateServiceAddress.currentTitle.length==0||[_btnStateServiceAddress.currentTitle isEqualToString:@""]||[_btnStateServiceAddress.currentTitle isEqualToString:@"(null)"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select service state" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_txtCityServiceAddress.text.length==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter  service city" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //else if (_txtZipcodeServiceAddress.text.length>7 || _txtZipcodeServiceAddress.text.length<4)
        else if (_txtZipcodeServiceAddress.text.length == 0 )
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter service zipcode" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (!(_txtZipcodeServiceAddress.text.length == 5 ))
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Invalid service zipcode" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //Billing Address Condition
        
        else if ([_txtViewAdd1BillingAddress.text isEqualToString:@"Enter Address Line1..!!"]||_txtViewAdd1BillingAddress.text.length==0||[_txtViewAdd1BillingAddress.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter billing address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_btnCountryBillingAddress.currentTitle.length==0||[_btnCountryBillingAddress.currentTitle isEqualToString:@""]||[_btnCountryBillingAddress.currentTitle isEqualToString:@"(null)"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select billing country" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_btnStateBillingAddress.titleLabel.text.length==0||[_btnStateBillingAddress.titleLabel.text isEqualToString:@""]||[_btnStateBillingAddress.titleLabel.text isEqualToString:@"(null)"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select billing state" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_btnStateBillingAddress.currentTitle.length==0||[_btnStateBillingAddress.currentTitle isEqualToString:@""]||[_btnStateBillingAddress.currentTitle isEqualToString:@"(null)"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select billing state" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_txtCityBillingAddress.text.length==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter billing city" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if (_txtZipCodeBillingAddress.text.length == 0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter billing zipcode" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //else if (_txtZipCodeBillingAddress.text.length>7 || _txtZipCodeBillingAddress.text.length<4)
        else if (!(_txtZipCodeBillingAddress.text.length == 5))

        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Invalid billing zipcode" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //End
        
        else if ((_txtServicePOCPrimaryEmail.text.length>0)&&[self NSStringIsValidEmail:_txtServicePOCPrimaryEmail.text]==NO)
        {
            [global AlertMethod:@"Alert!" :@"Please enter valid Service POC Primary Email address"];
        }
        else if ((_txtServicePOCSecondaryEmail.text.length>0)&&[self NSStringIsValidEmail:_txtServicePOCSecondaryEmail.text]==NO)
        {
            [global AlertMethod:@"Alert!" :@"Please enter valid Service POC Secondary Email address"];
        }
        else if ((_txtBillingPOCPrimaryEmail.text.length>0)&&[self NSStringIsValidEmail:_txtBillingPOCPrimaryEmail.text]==NO)
        {
            [global AlertMethod:@"Alert!" :@"Please enter valid Billing POC Primary Email address"];
        }
        else if ((_txtBillingPOCSecondaryEmail.text.length>0)&&[self NSStringIsValidEmail:_txtBillingPOCSecondaryEmail.text]==NO)
        {
            [global AlertMethod:@"Alert!" :@"Please enter valid Billing POC Secondary Email address"];
        }
        /* else if (strTaxCode.length==0|| [strTaxCode isEqualToString:@""]||[_btnTaxCode.currentTitle isEqualToString:@"Select Tax Code"])
         {
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Taxcode under Service Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
         [alert show];
         }*/
        else
        {
            if (chkTaxCodeRequired==YES)
            {
                if (strTaxCode.length==0|| [strTaxCode isEqualToString:@""] || [_btnTaxCode.currentTitle isEqualToString:@"Select Tax Code"])
                {
                    //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Taxcode under Service Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [global displayAlertController:Alert :@"Please select Taxcode under Service Address" :self];
                    
                    //[alert show];
                }
                else
                {
                    [self finalSave];
                }
            }
            else
            {
                [self finalSave];
            }
        }
    }
}
-(void)finalSave
{
    BOOL chkTemp;
    chkTemp=NO;
    
    if (_txtPrimaryPhone.text.length>0)
    {
        if (_txtPrimaryPhone.text.length>15||_txtPrimaryPhone.text.length<6)
        {
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid Phone No." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            // [alert show];
            chkTemp=YES;
        }
    }
    if (_txtSecondaryPhone.text.length>0)
    {
        if (_txtSecondaryPhone.text.length>15||_txtSecondaryPhone.text.length<6)
        {
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid Secondary Phone No." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            //[alert show];
            chkTemp=YES;
        }
        
        
    }
    
    if (chkTemp==YES)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Invalid Primary or Secondary Phone No." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }
    else
    {
        chkTemp=NO;
        for (int i=0; i<arrLocalImages.count; i++)
        {
            //[self uploadImage:[arrLocalImages objectAtIndex:i]];
        }
        //Nilind 7 Nov
        // if (![strChkChkState isEqualToString:_btnStateServiceAddress.titleLabel.text]||![strChkCity isEqualToString:_txtCityServiceAddress.text]||![strChkZipcode isEqualToString:_txtZipcodeServiceAddress.text]||![strChkServiceCounty isEqualToString:_txtServiceCounty.text]||![strChkServiceSchoolDistrict isEqualToString:_txtServiceSchoolDistrict.text])
        if(![strTaxSysName isEqualToString:strTaxCode])
        {
            [self salesAutomationTaxUpdate];
        }
        
        //[self myFunction];
        //..........
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setObject:_btnInspector.titleLabel.text forKey:@"inspectorName"];
        if(_txtMiddleName.text.length>0)
        {
            [defs setValue:[NSString stringWithFormat:@"%@ %@ %@",_txtFirstName.text,_txtMiddleName.text,_txtLastName.text] forKey:@"nameTitle"];
        }
        else
        {
            [defs setValue:[NSString stringWithFormat:@"%@ %@",_txtFirstName.text,_txtLastName.text] forKey:@"nameTitle"];
        }
        [defs synchronize];
        _lblNameTitle.text=[NSString stringWithFormat:@"%@",[defs valueForKey:@"nameTitle"]];
        
        if(isEditedInSalesAuto==YES)
        {
            NSLog(@"Modify date called in General");
            [global updateSalesModifydate:strLeadId];
        }
        [self updateLeadIdDetail];
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
        }
        else
        {
            [self saveImageToCoreData];
            
        }
        
        
        [self saveEmailIdPrimaryToCoreData];
        [self fetchCustomerContact];
        
        UIStoryboard *storyBoar=[UIStoryboard storyboardWithName:@"SalesMainSelectService" bundle:nil];
        SalesAutomationInspection *objSaleAutoInspection=[storyBoar instantiateViewControllerWithIdentifier:@"SalesAutomationInspection"];
        objSaleAutoInspection.strBrnachSysName=[matchesGeneralInfo valueForKey:@"branchSysName"];
        objSaleAutoInspection.strDepartmentSysName=[matchesGeneralInfo valueForKey:@"departmentSysName"];
        [self.navigationController pushViewController:objSaleAutoInspection animated:YES];
        
        NSUserDefaults *dfsStageStatus=[NSUserDefaults standardUserDefaults];
        [dfsStageStatus setBool:NO forKey:@"backFromInspectionNew"];
        [dfsStageStatus setValue:strStatusss forKey:@"leadStatusSales"];
        [dfsStageStatus setValue:strStageSysName forKey:@"stageSysNameSales"];
        [dfsStageStatus synchronize];
        
    }
}

//- (IBAction)actionOnCountryServiceAddress:(id)sender {
//}
- (IBAction)actionOnCheckBox:(id)sender
{
    if ([_btnEditBillingAddress.currentImage isEqual:[UIImage imageNamed:@"cancelEdit.png"]])
    {
        [self endEditing];
        isEditedInSalesAuto=YES;
        NSLog(@"Database edited");
        if(_btnCheckBox.selected==NO)
        {
            _btnCheckBox.selected=YES;
            [_imgCheckBox setImage:[UIImage imageNamed:@"check_box_2.png"]];
            _txtViewAdd1BillingAddress.text=_txtViewAdd1ServiceAddress.text;
            _txtViewAdd2BillingAddress.text=_txtViewAdd2ServiceAddress.text;
            [ _btnCountryBillingAddress setTitle:_btnCountryServiceAddress.titleLabel.text forState:UIControlStateNormal];
            [ _btnStateBillingAddress setTitle:_btnStateServiceAddress.titleLabel.text forState:UIControlStateNormal];
            _txtCityBillingAddress.text=_txtCityServiceAddress.text;
            _txtZipCodeBillingAddress.text=_txtZipcodeServiceAddress.text;
            
            chkForSameBillingAddress=YES;
            
            
            
            //temp
            NSString *strServiceAddress = @"";
            if(_txtViewAdd1ServiceAddress.text.length>0)
            {
                _txtViewAdd1ServiceAddress.text=[_txtViewAdd1ServiceAddress.text stringByTrimmingCharactersInSet:
                                                 [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                strServiceAddress = [NSString stringWithFormat:@"%@",_txtViewAdd1ServiceAddress.text ];
            }
            if(_txtViewAdd2ServiceAddress.text.length>0)
            {
                
                _txtViewAdd2ServiceAddress.text=[_txtViewAdd2ServiceAddress.text stringByTrimmingCharactersInSet:
                                                 [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtViewAdd2ServiceAddress.text ];
            }
            
            if(_txtCityServiceAddress.text.length>0)
            {
                _txtCityServiceAddress.text=[_txtCityServiceAddress.text stringByTrimmingCharactersInSet:
                                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtCityServiceAddress.text ];
            }
            
            if(_btnStateServiceAddress.titleLabel.text.length>0&& ![_btnStateServiceAddress.titleLabel.text isEqualToString:@"State"])
            {
                strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_btnStateServiceAddress.titleLabel.text];
            }
            if(_txtZipcodeServiceAddress.text.length>0)
            {
                _txtZipcodeServiceAddress.text=[_txtZipcodeServiceAddress.text stringByTrimmingCharactersInSet:
                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtZipcodeServiceAddress.text ];
            }
            
            
            _lblNewBillingAddress.text = strServiceAddress;
            // _lblNewBillingAddress.attributedText=[self getUnderLineAttributedString:_lblNewBillingAddress.text];
            
        }
        else
        {
            
            [_imgCheckBox setImage:[UIImage imageNamed:@"check_box_1.png"]];
            _btnCheckBox.selected=NO;
            _txtViewAdd1BillingAddress.text=@"";
            _txtViewAdd2BillingAddress.text=@"";
            [ _btnCountryBillingAddress setTitle:@"" forState:UIControlStateNormal];
            [ _btnStateBillingAddress setTitle:@"" forState:UIControlStateNormal];
            _txtCityBillingAddress.text=@"";
            _txtZipCodeBillingAddress.text=@"";
            chkForSameBillingAddress=NO;
            
            _lblNewBillingAddress.text=@"";
            
        }
        //Nilind
        if([_imgCheckBox.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
        {
            [_txtViewAdd1BillingAddress setEditable:NO];
            [_txtViewAdd2BillingAddress setEditable:NO];
            [ _btnCountryBillingAddress setEnabled:NO];
            
            [ _btnStateBillingAddress setEnabled:NO];
            [_txtCityBillingAddress setEnabled:NO];
            [_txtZipCodeBillingAddress setEnabled:NO];
        }
        else
        {
            [_txtViewAdd1BillingAddress setEditable:YES];
            [_txtViewAdd2BillingAddress setEditable:YES];
            [ _btnCountryBillingAddress setEnabled:YES];
            
            [ _btnStateBillingAddress setEnabled:YES];
            [_txtCityBillingAddress setEnabled:YES];
            [_txtZipCodeBillingAddress setEnabled:YES];
        }
        
        [self setUnderLineText];
        
    }
    else
    {
        
    }
}

#pragma mark- 29 Aug
-(void)assignValues
{
    
    arrOfPrimaryEmailNew=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailNew=[[NSMutableArray alloc]init];
    arrOfPrimaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrCustomerContactNo=[[NSMutableArray alloc]init];
    NSLog(@"sadfhadsjkfhasdjkf djfhgajsdf %@",[matchesGeneralInfo valueForKey:@"leadId"]);
    
    strLeadId=[matchesGeneralInfo valueForKey:@"leadId"];
    strLeadNumber = [matchesGeneralInfo valueForKey:@"leadNumber"];
    
    _txtLead.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"leadName"]];
    strFlowType=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"flowType"]];
    strStageSysName=[matchesGeneralInfo valueForKey:@"stageSysName"];
    
    strServiceAddressSubType = [NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceAddressSubType"]];

    if (strServiceAddressSubType.length == 0 || [strServiceAddressSubType isEqualToString:@""])
    {
        strServiceAddressSubType = @"Residential";
    }
    [_btnServiceAddressSubType setTitle:strServiceAddressSubType forState:UIControlStateNormal];
    
    // Lead Name
    
    
    
    NSUserDefaults *defsLeadName=[NSUserDefaults standardUserDefaults];
    [defsLeadName setObject:_txtLead.text forKey:@"defsLeadName"];
    [defsLeadName setValue:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"tax"]] forKey:@"taxValue"];
    [defsLeadName setValue:strFlowType forKey:@"flowType"];
    
    [defsLeadName setValue:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"accountNo"]] forKey:@"strAcountNumber"];
    [defsLeadName setValue:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"inspectionDate"]] forKey:@"strScheduleInspectionDate"];
    [defsLeadName synchronize];
    
    //........................................
    
    
    //Nilind 08 Feb
#pragma mark- TEMP COMMENT
    _txtAreaSqFt.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"area"]];
    _txtLotSizeSqFt.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"lotSizeSqFt"]];
    _txtBathroom.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"noOfBathroom"]];
    _txtBedroom.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"noOfBedroom"]];
    _txtYearBuil.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"yearBuilt"]];
    NSString *strLinearSqFt=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"linearSqFt"]];
    if ([strLinearSqFt isEqualToString:@"(null)"]|| strLinearSqFt.length==0|| [strLinearSqFt isEqual:NULL])
    {
        strLinearSqFt=@"";
    }
    _txtLinearSqFt.text=strLinearSqFt;
    
    //End
    
    
    _txtCustomerName.text=[NSString stringWithFormat:@"%@ %@",[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"firstName"]],[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"lastName"]]];
    
    //Nilind 30 Sept
    
    _txtFirstName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"firstName"]];
    
    _txtMiddleName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"middleName"]];
    
    _txtLastName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"lastName"]];
    
    //...............................................................................
    //Nilind 13 Oct
    
    _txtCompanyName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"companyName"]];
    if ([NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"companyName"]].length==0)
    {
        _txtCompanyName.text=@"";
    }
    //...............................................................................
    
    
    
    //[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"customerName"]];
    _txtScheduleStartDate.text=[NSString stringWithFormat:@"%@",[self ChangeDateToLocalDateTime:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"scheduleStartDate"]]]];
    
    _txtAccount.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"accountNo"]];
    _txtPrimaryEmail.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"primaryEmail"]];
    //_txtSecondayEmail.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"secondaryEmail"]];
    _txtPrimaryPhone.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"primaryPhone"]];
    _txtSecondaryPhone.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"secondaryPhone"]];
    
    //Multiple Primary Email Saavan
    
    
    
    NSString *strTemPrimaryEmail=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"primaryEmail"]];
    NSString *strTemSecondaryEmail=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"secondaryEmail"]];
    
    NSArray *arrTempPrimaryEmail,*arrTempsecondaryEmail;
    if (!(strTemPrimaryEmail.length==0) &&!([strTemPrimaryEmail isEqualToString:@"(null)"])) {
        
        [_btnPrimaryEmail setTitle:[matchesGeneralInfo valueForKey:@"primaryEmail"] forState:UIControlStateNormal];
        arrTempPrimaryEmail=[strTemPrimaryEmail componentsSeparatedByString:@","];
        
    }else{
        
        [_btnPrimaryEmail setTitle:@"" forState:UIControlStateNormal];
        
    }
    
    if (!(strTemSecondaryEmail.length==0) &&!([strTemSecondaryEmail isEqualToString:@"(null)"])) {
        
        [_btnSecondaryEmail setTitle:[matchesGeneralInfo valueForKey:@"secondaryEmail"] forState:UIControlStateNormal];
        arrTempsecondaryEmail=[strTemSecondaryEmail componentsSeparatedByString:@","];
        
        
    }else{
        
        [_btnSecondaryEmail setTitle:@"" forState:UIControlStateNormal];
        
    }
    
    if (arrTempPrimaryEmail.count>0) {
        
        [arrOfPrimaryEmailNew addObjectsFromArray:arrTempPrimaryEmail];
        [arrOfPrimaryEmailAlreadySaved addObjectsFromArray:arrTempPrimaryEmail];
        
    }
    if (arrTempsecondaryEmail.count>0) {
        
        [arrOfSecondaryEmailNew addObjectsFromArray:arrTempsecondaryEmail];
        [arrOfSecondaryEmailAlreadySaved addObjectsFromArray:arrTempsecondaryEmail];
        
    }
    
    //End Multiple Primary Email Saavan
    
    _txtViewAdd1ServiceAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"servicesAddress1"]];
    _txtViewAdd2ServiceAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceAddress2"]];
    
    _txtViewAdd1BillingAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingAddress1"]];
    _txtViewAdd2BillingAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingAddress2"]];
    
    
    //Nilind 03 August
    
    _txtMapCodeServiceAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceMapCode"]];
    _txtMapCodeBillingAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingMapcode"]];
    _txtGateCodeServiceNotes.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceGateCode"]];
    
    _txtViewAddressNotesServiceAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceNotes"]];
    _txtViewDirectionServiceAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceDirection"]];
    _txtViewAccountDesc.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"accountDescription"]];
    //End
    
    //Nilind 17 Oct Country Fetch
    for (int i=0; i<arrCountries.count; i++)
    {
        NSDictionary *dictData=[arrCountries objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountrySysName"]]isEqualToString:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCountry"]]])
        {
            [_btnCountryServiceAddress setTitle:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]] forState:UIControlStateNormal];
        }
        if (_btnCountryServiceAddress.titleLabel.text.length==0 ||[_btnCountryServiceAddress.titleLabel.text isEqualToString:@"(null)"])
        {
            [_btnCountryServiceAddress setTitle:@"United States" forState:UIControlStateNormal];
        }
    }
    //............................
    
    //Nilind 17 Oct Country Fetch
    
    for (int i=0; i<arrState.count; i++)
    {
        NSDictionary *dictData=[arrState objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@", [dictData valueForKey:@"StateSysName"]]isEqualToString:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceState"]]])
        {
            [_btnStateServiceAddress setTitle:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]] forState:UIControlStateNormal];
        }
        
        
    }
    
    
    // [_btnStateServiceAddress setTitle:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceState"]] forState:UIControlStateNormal];
    
    //...................................
    
    _txtCityServiceAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCity"]];
    //_txtZipcodeServiceAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceZipcode"]];
    
    NSString *strZip=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceZipcode"]];
    strZip = [strZip stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //strZip=@"123456789";
    if(strZip.length>5)
    {
        strZip=[strZip substringToIndex:5];
        
    }
    _txtZipcodeServiceAddress.text=strZip;
    
    //Nilind 17 Oct Country Fetch
    
    for (int i=0; i<arrCountries.count; i++)
    {
        NSDictionary *dictData=[arrCountries objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountrySysName"]]isEqualToString:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingCountry"]]])
        {
            [_btnCountryBillingAddress setTitle:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]] forState:UIControlStateNormal];
        }
        if (_btnCountryBillingAddress.titleLabel.text.length==0 ||[_btnCountryBillingAddress.titleLabel.text isEqualToString:@"(null)"])
        {
            [_btnCountryBillingAddress setTitle:@"United States" forState:UIControlStateNormal];
        }
    }
    
    
    //  [_btnCountryBillingAddress setTitle:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingCountry"]] forState:UIControlStateNormal];
    
    //...................................
    
    
    //Nilind 17 Oct Country Fetch
    
    
    for (int i=0; i<arrState.count; i++)
    {
        NSDictionary *dictData=[arrState objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@", [dictData valueForKey:@"StateSysName"]]isEqualToString:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingState"]]])
        {
            [_btnStateBillingAddress setTitle:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]] forState:UIControlStateNormal];
        }
        
    }
    
    
    //[_btnStateBillingAddress setTitle:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingState"]] forState:UIControlStateNormal];
    
    //...........................
    
    
    _txtCityBillingAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingCity"]];
    
    //_txtZipCodeBillingAddress.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingZipcode"]];
    
    NSString *strZipBilling=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingZipcode"]];
    strZipBilling = [strZipBilling stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    //strZip=@"123456789";
    if(strZipBilling.length>5)
    {
        strZipBilling=[strZipBilling substringToIndex:5];
        
    }
    _txtZipCodeBillingAddress.text=strZipBilling;
    
    
    _btnStateBillingAddress.titleLabel.text=([ _btnStateBillingAddress.titleLabel.text isEqual:nil]) ? @"" :_btnStateBillingAddress.titleLabel.text  ;
    
    [_btnStatus setTitle:[matchesGeneralInfo valueForKey:@"statusSysName"] forState:UIControlStateNormal];
    [_btnStage setTitle:[matchesGeneralInfo valueForKey:@"stageSysName"] forState:UIControlStateNormal];
    [_btnPriority setTitle:[matchesGeneralInfo valueForKey:@"prioritySysName"] forState:UIControlStateNormal];
    [_btnSource setTitle:[matchesGeneralInfo valueForKey:@"sourceSysName"] forState:UIControlStateNormal];
    arrayItems = [_btnSource.titleLabel.text componentsSeparatedByString:@","];
    
    _txtViewNotes.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"notes"]];
    _txtViewDescription.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"descriptionLeadDetail"]];
    _txtTagServiceRequired.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"tags"]];
    // Nilind 14 Sept
    
    // Inspector Master Fetching
    strInspectorId=[matchesGeneralInfo valueForKey:@"salesRepId"];
    /*   NSArray *arrEmployee;
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     NSArray* arrEmployee12=[defs valueForKey:@"EmployeeList"];
     arrEmployee=[dictMasters valueForKey:@"Employees"];
     if ([arrEmployee isEqual:@""])
     {
     NSLog(@"dsfdsfdsfdsfdsfdsf");
     }
     else
     {
     for (int i=0; i<arrEmployee.count; i++)
     {
     NSDictionary *dict=[arrEmployee objectAtIndex:i];
     if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeId"]]isEqualToString:strInspectorId])
     {
     NSString *str;
     str=[dict valueForKey:@"FullName"];
     [_btnInspector setTitle:str forState:UIControlStateNormal];
     }
     }
     }*/
    NSArray *arrEmployee;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    arrEmployee=[defs valueForKey:@"EmployeeList"];
    
    if ([arrEmployee isEqual:@""]|| arrEmployee.count==0 || [arrEmployee isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"dsfdsfdsfdsfdsfdsf");
    }
    else
    {
        for (int i=0; i<arrEmployee.count; i++)
        {
            NSDictionary *dict=[arrEmployee objectAtIndex:i];
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeId"]]isEqualToString:strInspectorId])
            {
                NSString *str;
                str=[dict valueForKey:@"FullName"];
                [_btnInspector setTitle:str forState:UIControlStateNormal];
                NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
                [defs setObject:dict forKey:@"inspectorDetail"];
                [defs synchronize];
            }
        }
    }
    
    //.........................................................................
    
    // Urgency Master Fetching
    
    strPrioritySysName=[matchesGeneralInfo valueForKey:@"prioritySysName"];
    NSArray *temparrUrgency=[dictSalesLeadMaster valueForKey:@"UrgencyMasters"];
    for (int i=0; i<temparrUrgency.count; i++)
    {
        NSDictionary *dict=[temparrUrgency objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]isEqualToString:strPrioritySysName])
        {
            NSString *str;
            str=[dict valueForKey:@"Name"];
            [_btnPriority setTitle:str forState:UIControlStateNormal];
        }
    }
    
    //.........................................................................
    
    // Status Master Fetching
    
    strStatusSysName=[matchesGeneralInfo valueForKey:@"statusSysName"];
    NSArray *temparrStatusMaster=[dictSalesLeadMaster valueForKey:@"LeadStatusMasters"];
    for (int i=0; i<temparrStatusMaster.count; i++)
    {
        NSDictionary *dict=[temparrStatusMaster objectAtIndex:i];
        
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]isEqualToString:strStatusSysName])
        {
            NSString *str;
            str=[dict valueForKey:@"StatusName"];
            strStatusIdForStage=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusId"]];
            //strStatusssId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusId"]];
            
            [_btnStatus setTitle:str forState:UIControlStateNormal];
            break;
        }
    }
    
    //......................................................................
    
    //Stage Master Fetching
    
    
    
    // [self findStageId:strStageSysName];
    //temp comment
    /*
     NSArray *temparrStageMaster=[dictSalesLeadMaster valueForKey:@"LeadStageMasters"];
     for (int i=0; i<temparrStageMaster.count; i++)
     {
     NSDictionary *dict=[temparrStageMaster objectAtIndex:i];
     if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]isEqualToString:strStageSysName])
     {
     NSString *str;
     str=[dict valueForKey:@"Name"];
     [_btnStage setTitle:str forState:UIControlStateNormal];
     }
     }
     */ //temp comment
    //Nilind 22 Sept
    
    //Nilind
    
    arrStageMaster=[[NSMutableArray alloc]init];
    arrStageSysName=[[NSMutableArray alloc]init];
    NSArray *temparrStageMaster=[dictSalesLeadMaster valueForKey:@"LeadStageMasters"];
    for (int i=0; i<temparrStageMaster.count; i++)
    {
        NSDictionary *dict=[temparrStageMaster objectAtIndex:i];
        if ([strStatusIdForStage isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusId"]]])
        {
            [arrStageMaster addObject:[dict valueForKey:@"Name"]];
            [arrStageSysName addObject:[dict valueForKey:@"SysName"]];
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]isEqualToString:strStageSysName])
            {
                NSString *str;
                str=[dict valueForKey:@"Name"];
                strLeadStageId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"LeadStageId"]];
                [_btnStage setTitle:str forState:UIControlStateNormal];
            }
        }
        
    }
    
    //...................
    
    
    
    //......................................................................
    
    // Source Master Fetching
    
    /*
     
     strSourceSysName=[matchesGeneralInfo valueForKey:@"sourceSysName"];
     
     NSArray *temparrSource=[dictSalesLeadMaster valueForKey:@"SourceMasters"];
     
     for (int i=0; i<temparrSource.count; i++)
     {
     NSDictionary *dict=[temparrSource objectAtIndex:i];
     if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SourceSysName"]]isEqualToString:strSourceSysName])
     {
     NSString *str;
     str=[dict valueForKey:@"Name"];
     [_btnSource setTitle:str forState:UIControlStateNormal];
     }
     }*/
    
    // Nilind Temp
    
    strSourceSysName=[matchesGeneralInfo valueForKey:@"sourceSysName"];
    
    NSArray*arr= [strSourceSysName componentsSeparatedByString:@","];
    
    arrSourceSelectedValue = [[NSMutableArray alloc]init];
    arrSourceSelectedId = [[NSMutableArray alloc]init];
    NSArray *temparrSource=[dictSalesLeadMaster valueForKey:@"SourceMasters"];
    
    for (int i=0; i<temparrSource.count; i++)
    {
        NSDictionary *dict=[temparrSource objectAtIndex:i];
        
        for(int i=0;i<arr.count;i++)
        {
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SourceSysName"]]isEqualToString:[arr objectAtIndex:i]])
            {
                NSString *str;
                str=[dict valueForKey:@"Name"];
                [arrSourceSelectedValue addObject:str];
                [arrSourceSelectedId addObject:[dict valueForKey:@"SourceSysName"]];
                // [_btnSource setTitle:str forState:UIControlStateNormal];
                //Nilind
                //End
            }
        }
    }
    //arrSourceSelectedValue=arrAdd;
    NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
    [_btnSource setTitle:joinedComponents forState:UIControlStateNormal];
    
    //......................................................................
    for (int i=0; i<arrCurrentServiceName.count; i++)
    {
        _constLblCurrenService_H.constant=_constLblCurrenService_H.constant+10;
    }
    _lblCurrentService.text=[arrCurrentServiceName componentsJoinedByString:@","];
    
    //......................................................................
    
    //NIlind 4 Oct
    
    strBranchSysName=[matchesGeneralInfo valueForKey:@"branchSysName"];
    NSUserDefaults *defsBranch=[NSUserDefaults standardUserDefaults];
    [defsBranch setObject:strBranchSysName forKey:@"branchSysName"];
    [defsBranch synchronize];
    
    // Branch Master Fetching
    NSArray *temparrBranchMasters=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    for (int i=0; i<temparrBranchMasters.count; i++)
    {
        NSDictionary *dict=[temparrBranchMasters objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]isEqualToString:strBranchSysName])
        {
            NSString *str;
            str=[dict valueForKey:@"Name"];
            [_txtBranchName setText:str];
            [_lblBranchName setText:str];
            break;
        }
        
    }
    //New poc change
    
    _txtCellNo.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"cellNo"]];
    _txtBillingPOCFirstName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingFirstName"]];
    _txtBillingPOCLastName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingLastName"]];
    _txtBillingPOCMiddleName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingMiddleName"]];
    _txtBillingPOCPrimaryEmail.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingPrimaryEmail"]];
    _txtBillingPOCSecondaryEmail.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingSecondaryEmail"]];
    _txtBillingPOCCellNo.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingCellNo"]];
    _txtBillingPOCPrimaryPhone.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingPrimaryPhone"]];
    _txtBillingPOCSecondaryPhone.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"billingSecondaryPhone"]];
    
    _txtServicePOCFirstName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceFirstName"]];
    _txtServicePOCLastName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceLastName"]];
    _txtServicePOCMiddleName.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceMiddleName"]];
    _txtServicePOCPrimaryEmail.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"servicePrimaryEmail"]];
    _txtServicePOCSecondaryEmail.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceSecondaryEmail"]];
    _txtServicePOCCellNo.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCellNo"]];
    _txtServicePOCPrimaryPhone.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"servicePrimaryPhone"]];
    _txtServicePOCSecondaryPhone.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceSecondaryPhone"]];
    
    
    
    //......................................................................
    
    //Combination of Name
    NSString *strName=@"";
    if(_txtFirstName.text.length>0)
    {
        _txtFirstName.text=[_txtFirstName.text stringByTrimmingCharactersInSet:
                            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strName = [NSString stringWithFormat:@"%@",[_txtFirstName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    if(_txtMiddleName.text.length>0)
    {
        
        _txtMiddleName.text=[_txtMiddleName.text stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        strName = [NSString stringWithFormat:@"%@ %@",strName,[_txtMiddleName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    if(_txtLastName.text.length>0)
    {
        
        _txtLastName.text=[_txtLastName.text stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        strName = [NSString stringWithFormat:@"%@ %@",strName,[_txtLastName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    _lblView1FullName.text=[NSString stringWithFormat:@"%@",strName];
    [_btnPrimaryEmailNew setTitle:_btnPrimaryEmail.titleLabel.text forState:UIControlStateNormal];
    
    
    _txtViewProblemescription.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"problemDescription"]];
    
    
    strLeadType=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"salesType"]];
    if ([strLeadType caseInsensitiveCompare:@"field"] == NSOrderedSame)
    {
        [_btnLeadTypeField setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnLeadTypeInside setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnLeadTypeField setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnLeadTypeInside setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        
    }
    
    
    _txtServiceCounty.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"serviceCounty"]];
    _txtServiceSchoolDistrict.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"serviceSchoolDistrict"]];
    _txtBillingCounty.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"billingCounty"]];
    _txtBillingSchoolDistrict.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"billingSchoolDistrict"]];
    
    _txtNoOfStory.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"noOfStory"]];
    _txtShrubArea.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"shrubArea"]];
    _txtTurfArea.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKeyPath:@"turfArea"]];
    
    //Combination Service Address
    
    if(_btnStateServiceAddress.titleLabel.text.length==0|| [_btnStateServiceAddress.titleLabel.text isEqualToString:@"(null)"])
    {
        [ _btnStateServiceAddress setTitle:@"" forState:UIControlStateNormal];
    }
    if(_btnStateBillingAddress.titleLabel.text.length==0|| [_btnStateBillingAddress.titleLabel.text isEqualToString:@"(null)"])
    {
        [_btnStateBillingAddress setTitle:@"" forState:UIControlStateNormal];
    }
    
    
    
    [self setServiceAddressData];
    
    //Combination Billing Address
    
    [self setBillingAddressData];
    
    
    
    
    [self setUnderLineText];
    
    /*if ([_btnStateBillingAddress.titleLabel.text isEqualToString:@"(null)"])
     {
     _btnStateBillingAddress.titleLabel.text=@"";
     }
     if ([_btnStateServiceAddress.titleLabel.text isEqualToString:@"(null)"])
     {
     _btnStateServiceAddress.titleLabel.text=@"";
     }*/
    
    strTaxCode=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"taxSysName"]];
    strTaxSysName=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"taxSysName"]];
    
    [self getTaxCode];
    for (int i=0; i<arrTaxCode.count; i++)
    {
        NSDictionary *dict=[arrTaxCode objectAtIndex:i];
        if ([strTaxCode isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]])
        {
            [_btnTaxCode setTitle:[dict valueForKey:@"Name"] forState:UIControlStateNormal];
            break;
        }
    }
    if(_btnTaxCode.titleLabel.text.length==0|| [_btnTaxCode.titleLabel.text isEqualToString:@"(null)"])
    {
        [ _btnTaxCode setTitle:@"" forState:UIControlStateNormal];
    }
    
    _txtDriveTime.text=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"driveTime"]];
    
    
    _txtEarliestStartTime.text=[global ChangeDateToLocalTimeAkshay:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"earliestStartTime"]] type:@"time"];
    //[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"earliestStartTime"]];
    
    _txtLatestStartTime.text=[global ChangeDateToLocalTimeAkshay:[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"latestStartTime"]] type:@"time"];//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"latestStartTime"]];
    
    //        _txtTurfArea.text=[self calculateTurfArea:_txtLotSizeSqFt.text Area:_txtAreaSqFt.text Story:_txtNoOfStory.text ShrubArea:_txtShrubArea.text];
    [self setPropertyType];
    
    _txtLinearSqFt.text = [self calculateLinerSquareFt:_txtAreaSqFt.text Story:_txtNoOfStory.text];
    
    
    //Nilind 31 Jan 2019
    
    //Nilind
    NSString *strIsBillingAddress=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"isBillingAddressSame"]];
    
    if ([strIsBillingAddress isEqualToString:@"true"]|| [strIsBillingAddress isEqualToString:@"1"])
    {
        [_txtViewAdd1BillingAddress setEditable:NO];
        [_txtViewAdd2BillingAddress setEditable:NO];
        [ _btnCountryBillingAddress setEnabled:NO];
        
        [ _btnStateBillingAddress setEnabled:NO];
        [_txtCityBillingAddress setEnabled:NO];
        [_txtZipCodeBillingAddress setEnabled:NO];
    }
    else
    {
        [_txtViewAdd1BillingAddress setEditable:YES];
        [_txtViewAdd2BillingAddress setEditable:YES];
        [ _btnCountryBillingAddress setEnabled:YES];
        
        [ _btnStateBillingAddress setEnabled:YES];
        [_txtCityBillingAddress setEnabled:YES];
        [_txtZipCodeBillingAddress setEnabled:YES];
    }
    
   // [self sameAsServiceAddress];
    
    
    if ([strFlowType caseInsensitiveCompare:@"Residential"] == NSOrderedSame)
    {
        [_btnOppTypeResidential setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnOppTypeCommercial setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnOppTypeResidential setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnOppTypeCommercial setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        
    }
    
    
}

#pragma mark- 29 Aug
-(void)updateLeadIdDetail
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        matches=arrAllObj[0];
        
        NSString *strPrimaryToSet,*strSecondartToSet;
        
        if (_btnPrimaryEmail.currentTitle.length==0) {
            strPrimaryToSet=@"";
        } else {
            strPrimaryToSet=_btnPrimaryEmail.currentTitle;
        }
        
        if (_btnSecondaryEmail.currentTitle.length==0) {
            strSecondartToSet=@"";
        } else {
            strSecondartToSet=_btnSecondaryEmail.currentTitle;
        }
        
        [matches setValue:strPrimaryToSet forKey:@"primaryEmail"];
        [matches setValue:strSecondartToSet forKey:@"secondaryEmail"];
        
        //        [matches setValue:_txtPrimaryEmail.text forKey:@"primaryEmail"];
        //        [matches setValue:_txtSecondayEmail.text forKey:@"secondaryEmail"];
        
        
        [matches setValue:_txtPrimaryPhone.text forKey:@"primaryPhone"];
        [matches setValue:_txtSecondaryPhone.text forKey:@"secondaryPhone"];
        
        // [matches setValue:_btnStage.titleLabel.text forKey:@"stageSysName"];
        //[matches setValue:_btnStatus.titleLabel.text forKey:@"statusSysName"];
        //[matches setValue:_btnPriority.titleLabel.text forKey:@"prioritySysName"];
        //[matches setValue:_btnSource.titleLabel.text forKey:@"sourceSysName"];
        
        // Billing
        [matches setValue:_txtViewAdd1BillingAddress.text forKey:@"billingAddress1"];
        [matches setValue:_txtViewAdd2BillingAddress.text forKey:@"billingAddress2"];
        
        [matches setValue:_btnCountryBillingAddress.titleLabel.text forKey:@"billingCountry"];
        //[matches setValue:[_btnCountryBillingAddress.titleLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"billingCountry"];
        //[_btnCountryBillingAddress.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];[yourString stringByReplacingOccurrencesOfString:@" " withString:@""]
        
        
        [matches setValue:_btnStateBillingAddress.titleLabel.text forKey:@"billingState"];
        [matches setValue:_txtCityBillingAddress.text forKey:@"billingCity"];
        [matches setValue:_txtZipCodeBillingAddress.text forKey:@"billingZipcode"];
        //[mystr substringToIndex:3];
        //..................................................
        
        //Service
        
        [matches setValue:_txtViewAdd1ServiceAddress.text forKey:@"servicesAddress1"];
        [matches setValue:_txtViewAdd2ServiceAddress.text forKey:@"serviceAddress2"];
        [matches setValue:_btnCountryServiceAddress.titleLabel.text forKey:@"serviceCountry"];
        // [matches setValue:[_btnCountryServiceAddress.titleLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"serviceCountry"];
        
        [matches setValue:_btnStateServiceAddress.titleLabel.text forKey:@"serviceState"];
        
        [matches setValue:_txtCityServiceAddress.text forKey:@"serviceCity"];
        [matches setValue:_txtZipcodeServiceAddress.text forKey:@"serviceZipcode"];
        
        //..........................................................
        
        [matches setValue:_txtTagServiceRequired.text forKey:@"tags"];
        [matches setValue:_txtViewDescription.text forKey:@"descriptionLeadDetail"];
        [matches setValue:_txtViewNotes.text forKey:@"notes"];
        
        //Nilindn 14 sept
        [matches setValue:strInspectorId forKey:@"salesRepId"];
        [matches setValue:strPrioritySysName forKey:@"prioritySysName"];
        [matches setValue:strStatusSysName forKey:@"statusSysName"];
        [matches setValue:strStageSysName forKey:@"stageSysName"];
        [matches setValue:strSourceSysName forKey:@"sourceSysName"];
        //...............................................................
        
        //Nilind 24 Sept
        
        [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
        //...............................................................
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[global modifyDate]]];
        [matches setValue:newTime forKey:@"dateModified"];
        
        //Nilind 30 Sept
        
        [matches setValue:_txtFirstName.text forKey:@"firstName"];
        [matches setValue:_txtMiddleName.text forKey:@"middleName"];
        [matches setValue:_txtLastName.text forKey:@"lastName"];
        
        //...............................................................
        
        NSString *DeviceVersion = [[UIDevice currentDevice] systemVersion];
        NSString *VersionDatee = VersionDate;
        NSString *VersionNumberr = VersionNumber;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *DeviceName=[NSString stringWithCString:systemInfo.machine
                                                encoding:NSUTF8StringEncoding];
        
        NSLog(@"DeviceVersion===%@----VersionDatee===%@----VersionNumberr===%@----DeviceName===%@",DeviceVersion,VersionDatee,VersionNumberr,DeviceName);
        
        [matches setValue:VersionDatee forKey:@"versionDate"];
        [matches setValue:VersionNumberr forKey:@"versionNumber"];
        [matches setValue:DeviceVersion forKey:@"deviceVersion"];
        [matches setValue:DeviceName forKey:@"deviceName"];
        
        if (chkForSameBillingAddress==YES)
        {
            [matches setValue:@"true" forKey:@"isBillingAddressSame"];
        }
        else
        {
            [matches setValue:@"false" forKey:@"isBillingAddressSame"];
        }
        //Nilind 08 Feb
        [matches setValue:_txtAreaSqFt.text forKey:@"area"];
        [matches setValue:_txtLotSizeSqFt.text forKey:@"lotSizeSqFt"];
        [matches setValue:_txtBedroom.text forKey:@"noOfBedroom"];
        [matches setValue:_txtBathroom.text forKey:@"noOfBathroom"];
        [matches setValue:_txtYearBuil.text forKey:@"yearBuilt"];
        [matches setValue:_txtLinearSqFt.text forKey:@"linearSqFt"];
        [matches setValue:_txtViewAccountDesc.text forKey:@"accountDescription"];
        
        
        //End
        ////Yaha Par zSyn ko yes karna hai
        
        [matches setValue:_txtMapCodeServiceAddress.text forKey:@"serviceMapCode"];
        [matches setValue:_txtMapCodeBillingAddress.text forKey:@"billingMapcode"];
        [matches setValue:_txtViewDirectionServiceAddress.text forKey:@"serviceDirection"];
        [matches setValue:_txtGateCodeServiceNotes.text forKey:@"serviceGateCode"];
        [matches setValue:_txtViewAddressNotesServiceAddress.text forKey:@"serviceNotes"];
        
        [matches setValue:@"yes" forKey:@"zSync"];
        
        //New Update 17 Nov
        
        [matches setValue:_txtCellNo.text forKey:@"cellNo"];
        [matches setValue:_txtBillingPOCFirstName.text forKey:@"billingFirstName"];
        [matches setValue:_txtBillingPOCLastName.text forKey:@"billingLastName"];
        [matches setValue:_txtBillingPOCMiddleName.text forKey:@"billingMiddleName"];
        [matches setValue:_txtBillingPOCPrimaryEmail.text forKey:@"billingPrimaryEmail"];
        [matches setValue:_txtBillingPOCSecondaryEmail.text forKey:@"billingSecondaryEmail"];
        [matches setValue:_txtBillingPOCCellNo.text forKey:@"billingCellNo"];
        [matches setValue:_txtBillingPOCPrimaryPhone.text forKey:@"billingPrimaryPhone"];
        [matches setValue:_txtBillingPOCSecondaryPhone.text forKey:@"billingSecondaryPhone"];
        
        
        [matches setValue:_txtServicePOCFirstName.text forKey:@"serviceFirstName"];
        [matches setValue:_txtServicePOCLastName.text forKey:@"serviceLastName"];
        [matches setValue:_txtServicePOCMiddleName.text forKey:@"serviceMiddleName"];
        [matches setValue:_txtServicePOCPrimaryEmail.text forKey:@"servicePrimaryEmail"];
        [matches setValue:_txtServicePOCSecondaryEmail.text forKey:@"serviceSecondaryEmail"];
        [matches setValue:_txtServicePOCCellNo.text forKey:@"serviceCellNo"];
        [matches setValue:_txtServicePOCPrimaryPhone.text forKey:@"servicePrimaryPhone"];
        [matches setValue:_txtServicePOCSecondaryPhone.text forKey:@"serviceSecondaryPhone"];
        
        [matches setValue:_txtViewProblemescription.text forKey:@"problemDescription"];
        [matches setValue:strLeadType forKey:@"salesType"];
        
        [matches setValue:_txtServiceCounty.text forKey:@"serviceCounty"];
        [matches setValue:_txtServiceSchoolDistrict.text forKey:@"serviceSchoolDistrict"];
        [matches setValue:_txtBillingCounty.text forKey:@"billingCounty"];
        [matches setValue:_txtBillingSchoolDistrict.text forKey:@"billingSchoolDistrict"];
        
        [matches setValue:_txtNoOfStory.text forKey:@"noOfStory"];
        [matches setValue:_txtShrubArea.text forKey:@"shrubArea"];
        [matches setValue:_txtTurfArea.text forKey:@"turfArea"];
        
        [matches setValue:strSelectedProprtyTypeSysName forKey:@"serviceAddressPropertyTypeSysName"];
        
        [matches setValue:strFlowType forKey:@"flowType"];
        [matches setValue:_txtLead.text forKey:@"leadName"];
        [matches setValue:_txtCompanyName.text forKey:@"companyName"];
        [matches setValue:strServiceAddressSubType forKey:@"serviceAddressSubType"];

        [context save:&error1];
        
    }
    [self fetchForUpdate];
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    [defs setValue:strFlowType forKey:@"flowType"];
    [defs synchronize];
    
}
-(void)fetchForUpdate
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        }
    }
}
#pragma mark- 7 Sept Aug Nilind
#pragma mark- FETCH CORE DATA IMAGE
-(void)fetchImageFromCoreDataStandard
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    //arrImagePath=[[NSMutableArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            [arrImagePath addObject:[matches valueForKey:@"leadImagePath"]];
            
            
        }
        // [self fetchFromCoreDataStandard];
    }
}
//Nilind 15 Sept

-(void)fetchJsonCountry
{
    arrCountries=[[NSMutableArray alloc]init];
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:CountryJson];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSArray *arrOfCountry = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    // NSDictionary *dictOfCountry = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    // [arrCountries addObject:[NSString stringWithFormat:@"%@",[dictOfCountry valueForKey:@"Name"]]];
    
    
    
    [arrCountries addObjectsFromArray:arrOfCountry];
}
-(void)fetchJsonState
{
    arrState=[[NSMutableArray alloc]init];
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:StateJson];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSArray *arrOfState = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    // NSDictionary *dictOfState = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    //[arrState addObject:[dictOfState valueForKey:@"Name"]];
    [arrState addObjectsFromArray:arrOfState];
}
//............................................................
-(void)titleStage
{
    NSString *strId;
    NSMutableArray *arrStatusId;
    arrStatusId=[[NSMutableArray alloc]init];
    arrStageMaster=[[NSMutableArray alloc]init];
    NSArray *temparrStatusMaster=[dictSalesLeadMaster valueForKey:@"LeadStatusMasters"];
    for (int i=0; i<temparrStatusMaster.count; i++)
    {
        NSDictionary *dict=[temparrStatusMaster objectAtIndex:i];
        if ([_btnStatus.titleLabel.text isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusName"]]])
        {
            
            strId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusId"]];
            break;
        }
    }
    
    NSArray *temparrStageMaster=[dictSalesLeadMaster valueForKey:@"LeadStageMasters"];
    for (int i=0; i<temparrStageMaster.count; i++)
    {
        NSDictionary *dict=[temparrStageMaster objectAtIndex:i];
        if ([strId isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"StatusId"]]])
        {
            [arrStageMaster addObject:[dict valueForKey:@"Name"]];
        }
    }
    [_btnStage setTitle:[arrStageMaster objectAtIndex:0] forState:UIControlStateNormal];
}
-(void)findStageId:(NSString*)stageSysName
{
    NSArray *temparrStageMaster=[dictSalesLeadMaster valueForKey:@"LeadStageMasters"];
    if (temparrStageMaster.count==0)
    {
    }
    else
    {
        
        NSArray *temparrStageMaster=[dictSalesLeadMaster valueForKey:@"LeadStageMasters"];
        for (int i=0; i<temparrStageMaster.count; i++)
        {
            NSDictionary *dict=[temparrStageMaster objectAtIndex:i];
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]isEqualToString:stageSysName])
            {
                NSString *str;
                str=[dict valueForKey:@"Name"];
                // strStageSysName=[dict valueForKey:@"SysName"];
                [_btnStage setTitle:str forState:UIControlStateNormal];
            }
        }
        
        
    }
    
}
//Nilind 22 Sept

//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    if (tblData.tag==105)
    {
        [self setValueForParticipants];
        
    }
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    // [self setValueForParticipants];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (tblData.tag==105)
    {
        [self setValueForParticipants];
        
    }
    //[_txtPrimaryPhone resignFirstResponder];
    //[_txtSecondaryPhone resignFirstResponder];
    //[_txtZipCodeBillingAddress resignFirstResponder];
    // [_txtZipcodeServiceAddress resignFirstResponder];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    // [self setValueForParticipants];
}
-(void)setValueForParticipants
{
    /*  if (arrSourceSelectedValue.count==0)
     {
     [_btnSource setTitle:@"--Select Source--" forState:UIControlStateNormal];
     strSourceSysName=@"";
     }
     else
     {
     
     NSString *joinedComponents = [arrSourceSelectedValue componentsJoinedByString:@","];
     [_btnSource setTitle:joinedComponents forState:UIControlStateNormal];
     NSString *joinedComponentsSysName = [arrSourceSelectedId componentsJoinedByString:@","];
     strSourceSysName=joinedComponentsSysName;
     
     }*/
    if (arrSourceSelectedId.count==0)
    {
        [_btnSource setTitle:@"--Select Source--" forState:UIControlStateNormal];
        strSourceSysName=@"";
    }
    else
    {
        NSMutableArray *arrrr=[[NSMutableArray alloc]init];
        for (int i=0; i<arrSourceSelectedId.count; i++)
        {
            for (int j=0; j<arrSourceChange.count; j++)
            {
                NSDictionary *dict=[arrSourceChange objectAtIndex:j];
                if ([[arrSourceSelectedId objectAtIndex:i] isEqualToString:[dict valueForKey:@"SourceSysName"]])
                {
                    [arrrr addObject:[dict valueForKey:@"Name"]];
                }
            }
        }
        
        NSString *joinedComponents = [arrrr componentsJoinedByString:@","];
        [_btnSource setTitle:joinedComponents forState:UIControlStateNormal];
        NSString *joinedComponentsSysName = [arrSourceSelectedId componentsJoinedByString:@","];
        strSourceSysName=joinedComponentsSysName;
        
    }
}

//.................................................................
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    /* [_txtPrimaryPhone endEditing:YES];////
     [_txtSecondaryPhone endEditing:YES];
     [_txtZipCodeBillingAddress endEditing:YES];
     [_txtZipcodeServiceAddress endEditing:YES];*/
}
//Nilind 22 Sept

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
#pragma mark- Note
    /*
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL yesfromback=[defs boolForKey:@"backfromDynamicView"];
    if (yesfromback) {
        
        [defs setBool:NO forKey:@"backfromDynamicView"];
        [defs synchronize];
        
    }
    else
    {
        
        [self deleteImageFromCoreDataSalesInfo];
        
        for (int k=0; k<arrNoImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            if ([arrNoImage[k] isKindOfClass:[NSString class]])
            {
                
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[arrNoImage objectAtIndex:k]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                
                NSError *error1;
                [context save:&error1];
            }
            else
            {
                NSDictionary *dictData=[arrNoImage objectAtIndex:k];
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                NSError *error1;
                [context save:&error1];
                
            }
            
        }
        
        
        for (int k=0; k<arrOfImagenameCollewctionView.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            if ([arrOfImagenameCollewctionView[k] isKindOfClass:[NSString class]])
            {
                
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[arrOfImagenameCollewctionView objectAtIndex:k]];
                objImageDetail.leadImageType=@"Graph";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                NSError *error1;
                [context save:&error1];
            }
            else
            {
                NSDictionary *dictData=[arrOfImagenameCollewctionView objectAtIndex:k];
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=@"Graph";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                //Latlong code
                
                //NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
                objImageDetail.latitude=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"latitude"]];
                // NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
                objImageDetail.longitude=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"longitude"]];
                
                
                NSError *error1;
                [context save:&error1];
                
            }
        }
        
    }
    //........................................................................
    [self fetchForUpdate];
    
    [self fetchImageFromCoreDataStandard];*/
    
}
-(void)deleteImageFromCoreDataSalesInfo
{
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
    [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    [allDataImage setPredicate:predicateImage];
    
    [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage) {
        [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
}
-(void)fetchImageDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                [arrPaymentInfoValue addObject:str];
            }
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictPaymentInfo];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
    }
}
//Nilind 26 Sept
//============================================================================
#pragma mark- MODIFY DATE COMPARISION
//============================================================================
-(void)uploadImage :(NSString*)strImageName
{
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString* strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesImageUpload];
    //http://192.168.0.218:3333//api/File/UploadAsync
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Image Sent");
    }
    NSLog(@"Image Sent");
}

//============================================================================
#pragma mark-  DATE TIME
//============================================================================


-(NSString *)ChangeDateToLocalDateTime :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}
//========Nilind 1st Oct====================================
#pragma mark-  Fetch Current Services
-(void)fetchCurrentServices
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityCurrentService=[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityCurrentService];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES                                                                                                                                                                                                                                                                                                                              ];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //    NSManagedObject *matchesNew;
    arrCurrentServiceName=[[NSMutableArray alloc]init];
    arrCurrentServiceSysname=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            [arrCurrentServiceName addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceName"]]];
            [arrCurrentServiceSysname addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]] ];
            
        }
    }
    
}

//...........................................................
//....................................
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark- 7 Nov
//============================================================================
//============================================================================
#pragma mark- SalesAutomation API
//============================================================================
//============================================================================

-(void)salesAutomationTaxUpdate
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        //[self salesFetch];
    }
    else
    {
        
        NSString *strUrl;
        
        //strUrl = [NSString stringWithFormat:@"%@%@%@&ServiceCity=%@&ServiceState=%@&ServiceZipcode=%@",strSalesAutoMainUrl,UrlSalesTaxByServiceAddress,strCompanyKey,_txtCityServiceAddress.text,_btnStateServiceAddress.titleLabel.text,_txtZipcodeServiceAddress.text];//@"08831"];//
        // strUrl = [NSString stringWithFormat:@"%@%@%@&ServiceCity=%@&ServiceState=%@&ServiceZipcode=%@&ServiceCounty=%@&ServiceSchoolDistrict=%@",strSalesAutoMainUrl,UrlSalesTaxByServiceAddress,strCompanyKey,_txtCityServiceAddress.text,_btnStateServiceAddress.titleLabel.text,_txtZipcodeServiceAddress.text,_txtServiceCounty.text,_txtServiceSchoolDistrict.text];//@"08831"];//
        
        NSString *strNewTaxUrl=@"/api/MobileToSaleAuto/GetTaxByTaxCode?companyKey=";
        
        strUrl = [NSString stringWithFormat:@"%@%@%@&taxSysName=%@",strSalesAutoMainUrl,strNewTaxUrl,strCompanyKey,strTaxCode];
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        
        NSLog(@"Sales Lead URl-----%@",strUrl);
        
        // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
        
        NSURL *url = [NSURL URLWithString:strUrl];
        //NSURLRequest *request = [NSURLRequest requestWithURL:url]; //temp
        NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
        
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
        
        //End
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSString *responseString    = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 NSLog(@"responseString %@",responseString);
                 
                 strUpdateTax=[NSString stringWithFormat:@"%@",responseString];
                 
                 if (strUpdateTax.length==0|| strUpdateTax.length>10)
                 {
                     strUpdateTax=@"0";
                 }
                 
                 NSUserDefaults *defsLeadName=[NSUserDefaults standardUserDefaults];
                 [defsLeadName setValue:strUpdateTax forKey:@"taxValue"];
                 [defsLeadName synchronize];
                 
                 
                 //   NSDictionary *greeting = [NSJSONSerialization JSONObjectWithData:data
                 // options:0
                 // error:NULL];
                 //  NSLog(@"greetinggreeting%@",greeting);
                 [self updateLeadIdDetailTax];
                 
             }
         }];
    }
}

-(void)updateLeadIdDetailTax
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        matches=arrAllObj[0];
        [matches setValue:strUpdateTax forKey:@"tax"];
        [matches setValue:strTaxCode forKey:@"taxSysName"];
        strTaxSysName=strTaxCode;
        strChkChkState=_btnStateServiceAddress.titleLabel.text;
        strChkCity=_txtCityServiceAddress.text;
        strChkZipcode=_txtZipcodeServiceAddress.text;
        strChkServiceCounty=_txtServiceCounty.text;
        strChkServiceSchoolDistrict=_txtServiceSchoolDistrict.text;
        
        [context save:&error1];
        
    }
    [self fetchForUpdate];
    
}
//Nilind 15 Nov

-(void)fetchImageDetailFromDataBase
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" : leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                /*
                 [arrOfImagenameCollewctionView addObject:leadImagePath];
                 
                 //Nilind 07 June
                 
                 NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                 
                 if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                 
                 [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                 
                 }
                 else
                 {
                 
                 
                 [arrOfImageCaptionGraph addObject:strImageCaption];
                 
                 }
                 
                 
                 NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                 
                 if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                 
                 [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                 
                 } else {
                 
                 [arrOfImageDescriptionGraph addObject:strImageDescription];
                 
                 }
                 
                 
                 */
                //End
                
            }
            else
            {
                
                [arrNoImage addObject:dict_ToSendLeadInfo];
                
                //Nilind 07 June
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageLongitude addObject:strLong];
                    
                }
                //End
                
            }
            
            
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    }
    else
    {
    }
}
-(void)fetchImageDetailFromDataBaseForGraph
{
    
    arrOfImagenameCollewctionView=nil;
    
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" :leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                
                //[arrOfImagenameCollewctionView addObject:leadImagePath];
                [arrOfImagenameCollewctionView addObject:dict_ToSendLeadInfo];
                
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                
                
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
                
            }
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    } else {
    }
    
      // 21 April 2020 Comment
  //  [self downloadImagesGraphs:arrOfImagenameCollewctionView];
    
}



//...........

//Nilind 06 Jan
//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++)
    {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
        {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        //[global AlertMethod:Info :NoBeforeImg];
        // _const_ImgView_H.constant=0;
        //_const_SaveContinue_B.constant=162;
        [_buttonImg1 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        [_buttonImg2 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        [_buttonImg3 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        
    }else if (arrOfImagesDetail.count==0){
        //[global AlertMethod:Info :NoBeforeImg];
        //_const_ImgView_H.constant=0;
        //_const_SaveContinue_B.constant=162;
        [_buttonImg1 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        [_buttonImg2 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        [_buttonImg3 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++)
        {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1)
        {
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:NO];
            
            [_buttonImg2 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
            [_buttonImg3 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        }
        else if (arrOfImagess.count==2)
        {
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:NO];
            
            [_buttonImg3 setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        }
        else
        {
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:NO];
        }
        //_const_ImgView_H.constant=100;  temp comment
        //_const_SaveContinue_B.constant=62;
      
          // 21 April 2020 Comment
      //  [self downloadImages:arrOfImagess];
        
    }
}


-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            [self ShowFirstImage:str:k];
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    
}


-(void)downloadImagesGraphs :(NSArray*)arrOfImagesDownload
{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        // NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
        NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrOfImagesDownload objectAtIndex:k];
        }
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    [_collectionViewGraph reloadData];
    
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImage : name : indexx];
    
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name :(int)indexxx
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (indexxx==0)
    {
        
        [_buttonImg1 setImage:image forState:UIControlStateNormal];
        
    } else if(indexxx==1)
    {
        
        [_buttonImg2 setImage:image forState:UIControlStateNormal];
        
        
    }else{
        
        [_buttonImg3 setImage:image forState:UIControlStateNormal];
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}
//============================================================================
//============================================================================
#pragma mark -----------------------Action Button ImageView-------------------------------
//============================================================================
//============================================================================

- (IBAction)actionOn_buttonImg1:(id)sender
{
    if ([_buttonImg1.currentImage isEqual:[UIImage imageNamed:@"NoImage.jpg"]]) {
        
        
        
    } else {
        
        [self goingToPreview : @"0"];
        
    }
}

- (IBAction)actionOn_buttonImg2:(id)sender
{
    if ([_buttonImg2.currentImage isEqual:[UIImage imageNamed:@"NoImage.jpg"]]) {
        
        
        
    } else {
        
        [self goingToPreview : @"1"];
        
    }
}

- (IBAction)actionOn_buttonImg3:(id)sender
{
    if ([_buttonImg3.currentImage isEqual:[UIImage imageNamed:@"NoImage.jpg"]]) {
        
        
        
    } else {
        
        [self goingToPreview : @"2"];
        
    }
}
-(void)goingToPreview :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                 bundle: nil];
        ImagePreviewSalesAuto
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        
        //objImagePreviewSalesAuto.statusOfWorkOrder=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"statusSysName"]];
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"statusSysName"]];
            
        }
        else
        {
            objImagePreviewSalesAuto.statusOfWorkOrder = @"Open";
        }
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaption;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescription;
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}


-(void)goingToPreviewGraph :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfImagenameCollewctionView;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        GraphImagePreviewViewController
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewController"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strLeadId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        //objImagePreviewSalesAuto.statusOfWorkOrder=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"statusSysName"]];
        
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"statusSysName"]];
            
        }
        else
        {
            objImagePreviewSalesAuto.statusOfWorkOrder = @"Open";
        }
        
        
        
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}


-(NSArray *)activeSource:(NSArray *)arrayDict
{
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    // arrDataTblView=[dictSalesLeadMaster valueForKey:@"SourceMasters"];
    for (int i=0; i<arrayDict.count; i++)
    {
        NSDictionary *dict=[arrayDict objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"isActive"]]isEqualToString:@"true"])
        {
            [arr addObject:dict];
        }
    }
    return  arr;
    
}

- (IBAction)actionOnGlobalSync:(id)sender
{
    [self endEditing];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"Alert!":@"No Internet connection available"];
    }
    else
    {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
        [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];
        
    }
    
}
-(void)methodSync
{
    GlobalSyncViewController *objGlobalSyncViewController=[[GlobalSyncViewController alloc]init];
    NSString *str=@"general";
    
    [objGlobalSyncViewController syncCall:str];
}
-(void)stopLoader
{
    [DejalBezelActivityView removeView];
}

// 08 March 2021 Replaced Zillow with Melissa.com
-(void)zillowServiceTemp
{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strMelissaKey=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.MelissaKey"];
    
    NSString *strKey=@"TJXAP_9TGVGzzPWOqOZy5O**";

    if (strMelissaKey.length > 0) {
        
        strKey=strMelissaKey;
        
    }

    NSString *strUrl;
    NSString *strAddress;
    strAddress=_txtViewAdd1ServiceAddress.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"servicesAddress1"]];
    
    NSString *strState =_btnStateServiceAddress.titleLabel.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceState"]];
    if ([strState caseInsensitiveCompare:@"California"] == NSOrderedSame)
    {
        strState=@"CA";
    }
    if ([strState caseInsensitiveCompare:@"Texas"] == NSOrderedSame)
    {
        strState=@"TX";
    }
    
    NSString *strCity=_txtCityServiceAddress.text;
    NSString *strZipcode=_txtZipcodeServiceAddress.text;
    
    strUrl=[NSString stringWithFormat:@"%@",UrlMelissa];
    
    strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSMutableDictionary *dictMelissaRecords;
    dictMelissaRecords=[[NSMutableDictionary alloc]init];

    [dictMelissaRecords setObject:strAddress forKey:@"AddressLine1"];
    [dictMelissaRecords setObject:strCity forKey:@"City"];
    [dictMelissaRecords setObject:strState forKey:@"State"];
    [dictMelissaRecords setObject:strZipcode forKey:@"PostalCode"];
    
    NSMutableArray *arrTempRecord = [[NSMutableArray alloc] init];
    [arrTempRecord addObject:dictMelissaRecords];
    
    NSMutableDictionary *dictMelissa;
    dictMelissa=[[NSMutableDictionary alloc]init];

    [dictMelissa setObject:strKey forKey:@"CustomerId"];
    [dictMelissa setObject:@"Property V4 - LookupProperty" forKey:@"TransmissionReference"];
    [dictMelissa setObject:@"1" forKey:@"TotalRecords"];
    [dictMelissa setObject:@"GrpAll" forKey:@"Columns"];
    [dictMelissa setObject:arrTempRecord forKey:@"Records"];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictMelissa])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictMelissa options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Melissa.com JSON: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    //============================================================================
    //============================================================================
    NSDictionary *dictTemp;
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
            
            [DejalBezelActivityView removeView];

             //dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [DejalBezelActivityView removeView];

                if (success)
                 {
                     
                     if ([[response valueForKey:@"Records"] isKindOfClass:[NSArray class]]) {
                         
                         NSArray *arrRecords = [response valueForKey:@"Records"];
                         
                         if (arrRecords.count > 0) {
                             
                             NSDictionary *dictRecords = arrRecords[0];

                             if ([[dictRecords valueForKey:@"IntRoomInfo"] isKindOfClass:[NSDictionary class]]) {

                                 NSDictionary *dictIntRoomInfo = [dictRecords valueForKey:@"IntRoomInfo"];

                                 _txtBathroom.text=[NSString stringWithFormat:@"%@",[dictIntRoomInfo valueForKey:@"BathCount"]];
                                 _txtBedroom.text=[NSString stringWithFormat:@"%@",[dictIntRoomInfo valueForKey:@"BedroomsCount"]];
                                 
                                 if ([_txtBathroom.text isEqualToString:@"(null)"])
                                 {
                                     _txtBathroom.text=@"0";
                                 }
                                 if ([_txtBedroom.text isEqualToString:@"(null)"])
                                 {
                                     _txtBedroom.text=@"0";
                                 }
                                 
                             }else{
                                 
                                 _txtBathroom.text=@"0";
                                 _txtBedroom.text=@"0";
                                 
                             }
                             
                             if ([[dictRecords valueForKey:@"PropertyUseInfo"] isKindOfClass:[NSDictionary class]]) {

                                 NSDictionary *dictPropertyUseInfo = [dictRecords valueForKey:@"PropertyUseInfo"];

                                 _txtYearBuil.text=[NSString stringWithFormat:@"%@",[dictPropertyUseInfo valueForKey:@"YearBuilt"]];
                                 
                                 if ([_txtYearBuil.text isEqualToString:@"(null)"])
                                 {
                                     _txtYearBuil.text=@"0";
                                 }
                                 
                             }else{
                                 
                                 _txtYearBuil.text=@"0";
                                 
                             }
                             
                        
                             if ([[dictRecords valueForKey:@"PropertySize"] isKindOfClass:[NSDictionary class]]) {
                                 
                                 NSDictionary *dictPropertySize = [dictRecords valueForKey:@"PropertySize"];

                                 _txtLotSizeSqFt.text=[NSString stringWithFormat:@"%@",[dictPropertySize valueForKey:@"AreaLotSF"]];
                                 _txtAreaSqFt.text=[NSString stringWithFormat:@"%@",[dictPropertySize valueForKey:@"AreaBuilding"]];
                                 
                                 if ([_txtLotSizeSqFt.text isEqualToString:@"(null)"])
                                 {
                                     _txtLotSizeSqFt.text=@"0";
                                 }
                                 if ([_txtAreaSqFt.text isEqualToString:@"(null)"])
                                 {
                                     _txtAreaSqFt.text=@"0";
                                 }
                                 
                             }else{
                                 
                                 _txtLotSizeSqFt.text=@"0";
                                 _txtAreaSqFt.text=@"0";
                                 
                             }
                             
                         } else {
                             
                             NSString *strTitle = Alert;
                             NSString *strMsg = NoDataAvailableee;
                             [global AlertMethod:strTitle :strMsg];
                             
                         }
                       
                     } else {
                         
                         NSString *strTitle = Alert;
                         NSString *strMsg = NoDataAvailableee;
                         [global AlertMethod:strTitle :strMsg];
                         
                     }

                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                     [DejalBezelActivityView removeView];
                 }
             //});
         }];
    //});
    
    //============================================================================
    //============================================================================
    
    
}// 08 March 2021 Replaced Zillow with Melissa.com
-(void)zillowServiceTempOldd
{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strMelissaKey=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.MelissaKey"];
    
    NSString *strKey=@"TJXAP_9TGVGzzPWOqOZy5O**";

    if (strMelissaKey.length > 0) {
        
        strKey=strMelissaKey;
        
    }
    
    NSString *strUrl;
    NSString *strAddress;
    strAddress=_txtViewAdd1ServiceAddress.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"servicesAddress1"]];
    
    NSString *strState =_btnStateServiceAddress.titleLabel.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceState"]];
    if ([strState caseInsensitiveCompare:@"California"] == NSOrderedSame)
    {
        strState=@"CA";
    }
    if ([strState caseInsensitiveCompare:@"Texas"] == NSOrderedSame)
    {
        strState=@"TX";
    }
    
    NSString *strCity=_txtCityServiceAddress.text;
    NSString *strZipcode=_txtZipcodeServiceAddress.text;
    
    strUrl=[NSString stringWithFormat:@"%@",UrlMelissa];
    
    strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSMutableDictionary *dictMelissaRecords;
    dictMelissaRecords=[[NSMutableDictionary alloc]init];

    [dictMelissaRecords setObject:strAddress forKey:@"AddressLine1"];
    [dictMelissaRecords setObject:strCity forKey:@"City"];
    [dictMelissaRecords setObject:strState forKey:@"State"];
    [dictMelissaRecords setObject:strZipcode forKey:@"PostalCode"];
    
    NSMutableArray *arrTempRecord = [[NSMutableArray alloc] init];
    [arrTempRecord addObject:dictMelissaRecords];
    
    NSMutableDictionary *dictMelissa;
    dictMelissa=[[NSMutableDictionary alloc]init];

    [dictMelissa setObject:strKey forKey:@"CustomerId"];
    [dictMelissa setObject:@"Property V4 - LookupProperty" forKey:@"TransmissionReference"];
    [dictMelissa setObject:@"1" forKey:@"TotalRecords"];
    [dictMelissa setObject:@"GrpAll" forKey:@"Columns"];
    [dictMelissa setObject:arrTempRecord forKey:@"Records"];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictMelissa])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictMelissa options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Melissa.com JSON: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    //============================================================================
    //============================================================================
    NSDictionary *dictTemp;
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
            
            [DejalBezelActivityView removeView];

             //dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [DejalBezelActivityView removeView];

                if (success)
                 {
                     
                     if ([[response valueForKey:@"Records"] isKindOfClass:[NSArray class]]) {
                         
                         NSArray *arrRecords = [response valueForKey:@"Records"];
                         
                         if (arrRecords.count > 0) {
                             
                             NSDictionary *dictRecords = arrRecords[0];

                             if ([[dictRecords valueForKey:@"IntRoomInfo"] isKindOfClass:[NSDictionary class]]) {

                                 NSDictionary *dictIntRoomInfo = [dictRecords valueForKey:@"IntRoomInfo"];

                                 _txtBathroom.text=[NSString stringWithFormat:@"%@",[dictIntRoomInfo valueForKey:@"BathCount"]];
                                 _txtBedroom.text=[NSString stringWithFormat:@"%@",[dictIntRoomInfo valueForKey:@"BedroomsCount"]];
                                 
                                 if ([_txtBathroom.text isEqualToString:@"(null)"])
                                 {
                                     _txtBathroom.text=@"0";
                                 }
                                 if ([_txtBedroom.text isEqualToString:@"(null)"])
                                 {
                                     _txtBedroom.text=@"0";
                                 }
                                 
                             }else{
                                 
                                 _txtBathroom.text=@"0";
                                 _txtBedroom.text=@"0";
                                 
                             }
                             
                             if ([[dictRecords valueForKey:@"PropertyUseInfo"] isKindOfClass:[NSDictionary class]]) {

                                 NSDictionary *dictPropertyUseInfo = [dictRecords valueForKey:@"PropertyUseInfo"];

                                 _txtYearBuil.text=[NSString stringWithFormat:@"%@",[dictPropertyUseInfo valueForKey:@"YearBuilt"]];
                                 
                                 if ([_txtYearBuil.text isEqualToString:@"(null)"])
                                 {
                                     _txtYearBuil.text=@"0";
                                 }
                                 
                             }else{
                                 
                                 _txtYearBuil.text=@"0";
                                 
                             }
                             
                        
                             if ([[dictRecords valueForKey:@"PropertySize"] isKindOfClass:[NSDictionary class]]) {
                                 
                                 NSDictionary *dictPropertySize = [dictRecords valueForKey:@"PropertySize"];

                                 _txtLotSizeSqFt.text=[NSString stringWithFormat:@"%@",[dictPropertySize valueForKey:@"AreaLotSF"]];
                                 _txtAreaSqFt.text=[NSString stringWithFormat:@"%@",[dictPropertySize valueForKey:@"BasementAreaFinished"]];
                                 
                                 if ([_txtLotSizeSqFt.text isEqualToString:@"(null)"])
                                 {
                                     _txtLotSizeSqFt.text=@"0";
                                 }
                                 if ([_txtAreaSqFt.text isEqualToString:@"(null)"])
                                 {
                                     _txtAreaSqFt.text=@"0";
                                 }
                                 
                             }else{
                                 
                                 _txtLotSizeSqFt.text=@"0";
                                 _txtAreaSqFt.text=@"0";
                                 
                             }
                             
                         } else {
                             
                             NSString *strTitle = Alert;
                             NSString *strMsg = NoDataAvailableee;
                             [global AlertMethod:strTitle :strMsg];
                             
                         }
                       
                     } else {
                         
                         NSString *strTitle = Alert;
                         NSString *strMsg = NoDataAvailableee;
                         [global AlertMethod:strTitle :strMsg];
                         
                     }

                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                     [DejalBezelActivityView removeView];
                 }
             //});
         }];
    //});
    
    //============================================================================
    //============================================================================
    
    
}

//Nilind 09 Feb
-(void)zillowServiceTempOld
{
    
    NSString *strKey=@"X1-ZWz19zkb2lkumj_7zykg";//@"X1-ZWz1fn2iqwnc3v_5jgqp";
    NSString *strUrl;
    NSString *strAddress;
    strAddress=_txtViewAdd1ServiceAddress.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"servicesAddress1"]];
    
    // NSString *strCountry= [NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCountry"]];
    //http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=X1-ZWz19zkb2lkumj_7zykg&address=18715%20Rogers%20Lk%20&citystatezip=SanAntonio,TX,78258
    
    NSString *strState =_btnStateServiceAddress.titleLabel.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceState"]];
    if ([strState caseInsensitiveCompare:@"California"] == NSOrderedSame)
    {
        strState=@"CA";
    }
    if ([strState caseInsensitiveCompare:@"Texas"] == NSOrderedSame)
    {
        strState=@"TX";
    }
    
    NSString *strCity=_txtCityServiceAddress.text;// [NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCity"]];
    NSString *strZipcode=_txtZipcodeServiceAddress.text;// [NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceZipcode"]];
    
    strUrl=[NSString stringWithFormat:@"http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=%@&address=%@&citystatezip=%@",strKey,strAddress,[NSString stringWithFormat:@"%@,%@,%@",strCity,strState,strZipcode]];
    
    //strUrl = @"";
    NSLog(@"ZilloURL%@",strUrl);
    strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:strUrl];
    
    [XMLConverter convertXMLURL:url completion:^(BOOL success, NSDictionary *dictionary, NSError *error)
     {
         [DejalBezelActivityView removeView];
         NSLog(@"xml dictionary %@", success ? dictionary : error);
         dictZillow=dictionary;
         if ([[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.message.code"]] isEqualToString:@"508"])
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Information found on Zillow for service address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [alert show];
             _txtBathroom.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.bathrooms"]];
             _txtBedroom.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.bedrooms"]];
             _txtLotSizeSqFt.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.lotSizeSqFt"]];
             _txtYearBuil.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.yearBuilt"]];
             _txtAreaSqFt.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.finishedSqFt"]];
             if ([_txtBathroom.text isEqualToString:@"(null)"])
             {
                 _txtBathroom.text=@"0";
             }
             if ([_txtBedroom.text isEqualToString:@"(null)"])
             {
                 _txtBedroom.text=@"0";
             }
             if ([_txtLotSizeSqFt.text isEqualToString:@"(null)"])
             {
                 _txtLotSizeSqFt.text=@"0";
             }
             if ([_txtYearBuil.text isEqualToString:@"(null)"])
             {
                 _txtYearBuil.text=@"0";
             }
             if ([_txtAreaSqFt.text isEqualToString:@"(null)"])
             {
                 _txtAreaSqFt.text=@"0";
             }
             
         }
         else
         {
             
             NSArray *arrResults = [dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result"];
             
             if ([arrResults isKindOfClass:[NSArray class]])
             {
                 
                 if (arrResults.count>0)
                 {
                     
                     NSDictionary *dictDataResults = arrResults[0];
                     
                     _txtBathroom.text=[NSString stringWithFormat:@"%@",[dictDataResults valueForKey:@"bathrooms"]];
                     _txtBedroom.text=[NSString stringWithFormat:@"%@",[dictDataResults valueForKey:@"bedrooms"]];
                     _txtLotSizeSqFt.text=[NSString stringWithFormat:@"%@",[dictDataResults valueForKey:@"lotSizeSqFt"]];
                     _txtYearBuil.text=[NSString stringWithFormat:@"%@",[dictDataResults valueForKey:@"yearBuilt"]];
                     _txtAreaSqFt.text=[NSString stringWithFormat:@"%@",[dictDataResults valueForKey:@"finishedSqFt"]];
                     if ([_txtBathroom.text isEqualToString:@"(null)"])
                     {
                         _txtBathroom.text=@"0";
                     }
                     if ([_txtBedroom.text isEqualToString:@"(null)"])
                     {
                         _txtBedroom.text=@"0";
                     }
                     if ([_txtLotSizeSqFt.text isEqualToString:@"(null)"])
                     {
                         _txtLotSizeSqFt.text=@"0";
                     }
                     if ([_txtYearBuil.text isEqualToString:@"(null)"])
                     {
                         _txtYearBuil.text=@"0";
                     }
                     if ([_txtAreaSqFt.text isEqualToString:@"(null)"])
                     {
                         _txtAreaSqFt.text=@"0";
                     }
                     
                 }
                 else
                 {
                     
                     [global displayAlertController:Alert :@"No Information found on Zillow for service address" :self];
                     
                     _txtBathroom.text=@"0";
                     _txtBedroom.text=@"0";
                     _txtLotSizeSqFt.text=@"0";
                     _txtYearBuil.text=@"0";
                     _txtAreaSqFt.text=@"0";
                     
                 }
                 
             }
             else
             {
                 if ([dictZillow isKindOfClass:[NSDictionary class]])
                 {
                     _txtBathroom.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.bathrooms"]];
                     _txtBedroom.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.bedrooms"]];
                     _txtLotSizeSqFt.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.lotSizeSqFt"]];
                     _txtYearBuil.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.yearBuilt"]];
                     _txtAreaSqFt.text=[NSString stringWithFormat:@"%@",[dictZillow valueForKeyPath:@"SearchResults:searchresults.response.results.result.finishedSqFt"]];
                     
                     if ([_txtBathroom.text isEqualToString:@"(null)"])
                     {
                         _txtBathroom.text=@"0";
                     }
                     if ([_txtBedroom.text isEqualToString:@"(null)"])
                     {
                         _txtBedroom.text=@"0";
                     }
                     if ([_txtLotSizeSqFt.text isEqualToString:@"(null)"])
                     {
                         _txtLotSizeSqFt.text=@"0";
                     }
                     if ([_txtYearBuil.text isEqualToString:@"(null)"])
                     {
                         _txtYearBuil.text=@"0";
                     }
                     if ([_txtAreaSqFt.text isEqualToString:@"(null)"])
                     {
                         _txtAreaSqFt.text=@"0";
                     }
                 }
                 else
                 {
                     _txtBathroom.text=@"0";
                     _txtBedroom.text=@"0";
                     _txtLotSizeSqFt.text=@"0";
                     _txtYearBuil.text=@"0";
                     _txtAreaSqFt.text=@"0";
                 }
                 
             }
             
         }
         
         _txtTurfArea.text=[self calculateTurfArea:_txtLotSizeSqFt.text Area:_txtAreaSqFt.text Story:_txtNoOfStory.text ShrubArea:_txtShrubArea.text];
         
         _txtLinearSqFt.text = [self calculateLinerSquareFt:_txtAreaSqFt.text Story:_txtNoOfStory.text];
         
         /*NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
          NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
          NSLog(@"%@",jsonString1);*/
     }];
    
    
}
-(void)zillowService
{
    // http://www.zillow.com/webservice/GetZestimate.htm?zws-id=@"X1-ZWz1fn2iqwnc3v_5jgqp"&zpid=48749425
    //http://www.zillow.com/webservice/GetSearchResults.htm?zws-id=<ZWSID>&address=2114+Bigelow+Ave&citystatezip=Seattle%2C+WA
    NSString *strKey=@"X1-ZWz19zkb2lkumj_7zykg";//@"X1-ZWz1fn2iqwnc3v_5jgqp";
    NSString *strUrl;
    NSString *strAddress;
    strAddress= _txtViewAdd1ServiceAddress.text;
    //[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"servicesAddress1"]];
    
    // NSString *strCountry= [NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCountry"]];
    
    NSString *strState =_btnStateServiceAddress.titleLabel.text; //[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceState"]];
    
    NSString *strCity=_txtCityServiceAddress.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceCity"]];
    NSString *strZipcode=_txtZipcodeServiceAddress.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceZipcode"]];
    strUrl=[NSString stringWithFormat:@"http://www.zillow.com/webservice/GetSearchResults.htm?zws-id=%@&address=%@&citystatezip=%@",strKey,strAddress,[NSString stringWithFormat:@"%@,%@,%@",strCity,strState,strZipcode]];
    
    /*    NSXMLParser *xmlparser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]];
     [xmlparser setDelegate:self];
     [xmlparser parse];
     // - See more at: http://www.theappguruz.com/blog/xmlparsing-with-nsxmlparser-tutorial#sthash.tQ3R2C4b.dpuf
     
     
     
     //strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
     
     //    NSData *xmlData = [strUrl dataUsingEncoding:NSASCIIStringEncoding];
     //
     //    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
     //
     //    [xmlParser setDelegate:self];
     //
     //    [xmlParser parse];*/
    strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSError *error = nil;
    NSURLResponse * response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSData* jsonData = [NSData dataWithData:data];
    
    if (jsonData==nil)
    {
        
    }
    else
    {
        
        
        NSArray* responseArr = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        
        NSString *xml_Response=[[NSString alloc]initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        NSLog(@"XMl Data :-> %@",xml_Response);
        
        NSXMLParser *xmlParser;
        NSLog(@"%@",responseArr);
        if(xmlParser)
            
        {
            
        }
        
        xmlParser = [[NSXMLParser alloc] initWithData:jsonData];
        [xmlParser setDelegate:(id)self];
        [xmlParser setShouldResolveExternalEntities:YES];
        [xmlParser parse];
        
    }
    
    
}


//}
-(void)parserDidStartDocument:(NSXMLParser *)parser{
    // Initialize the neighbours data array.
    //self.arrNeighboursData = [[NSMutableArray alloc] init];
}
-(void)parserDidEndDocument:(NSXMLParser *)parser{
    // When the parsing has been finished then simply reload the table view.
    //[self.tblNeighbours reloadData];
}
-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    NSLog(@"xml parsing error %@", [parseError localizedDescription]);
    [DejalBezelActivityView removeView];
}
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    strMutable = [[NSMutableString alloc] init];
    
    NSLog(@"Element started %@",elementName);
    //self.currentElement=elementName;
    
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    NSLog(@"Element ended %@",elementName);
    NSString *trimmedString = [strMutable stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [arrValue addObject:trimmedString];
    [arrayKey addObject:elementName];
    // self.currentElement=@"";
    
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    // Store the found characters if only we're interested in the current element.
    // [strMutable appendString:string];
    [strMutable appendString:[[string stringByReplacingOccurrencesOfString:@"\n" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@" "]];
    
    
    
    NSLog(@"RESPONSE STRING %@",strMutable);
}

- (IBAction)actionOnGetPropertyInfo:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Unable to fetch, due to no internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [DejalBezelActivityView removeView];
    }
    else
    {
        // [self zillowServiceTemp];
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Details..."];
        [self performSelector:@selector(zillowServiceTemp) withObject:nil afterDelay:0.5];
        
        //   [DejalBezelActivityView removeView];
        // dictXML = [NSMutableDictionary dictionaryWithObjects: arrValue forKeys: arrayKey];
        //NSLog(@"arrayArticleNamesarrayArticleNames>>%@",dictZillow);
        
        //[self zillowService];
    }
}



//============================================================================
//============================================================================
#pragma mark- ------------New Methods For Emails--------------
//============================================================================
//============================================================================

- (IBAction)action_PrimaryEmail:(id)sender
{
    [self endEditing];
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        
        NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
        
        if (strMessage.length==0) {
            
        } else {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@""
                                       message:strMessage
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
    }
    else
    {
        
        [self tablViewForPrimaryEmailIds];
        
    }
    
}
- (IBAction)action_SecondaryEmail:(id)sender
{
    [self endEditing];
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        
        NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
        if (strMessage.length==0) {
            
        } else {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@""
                                       message:strMessage
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
    }else{
        
        [self tablViewForSecondaryEmailIds];
        
    }
    
}

-(void)alertForPrimaryEmailSaving{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Email-Id"
                                                                              message: strMessage
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Email-Id Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    //        textField.placeholder = @"password";
    //        textField.textColor = [UIColor blueColor];
    //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    //        textField.borderStyle = UITextBorderStyleRoundedRect;
    //        textField.secureTextEntry = YES;
    //    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        if (txtHistoricalDays.text.length>0) {
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
            {
                
                [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
                
            }else{
                
                if ([arrOfPrimaryEmailNew containsObject:txtHistoricalDays.text]) {
                    
                    [self performSelector:@selector(AlertViewForAlreadyExitPrimaryEmail) withObject:nil afterDelay:0.2];
                    
                } else {
                    
                    [arrOfPrimaryEmailNew addObject:txtHistoricalDays.text];
                    
                    [arrOfPrimaryEmailAlreadySaved addObject:txtHistoricalDays.text];
                    
                    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
                    
                    [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
                    [_btnPrimaryEmailNew setTitle:strMessage forState:UIControlStateNormal];
                    // [_btnPrimaryEmailNew setTitle:_btnPrimaryEmail.titleLabel.text forState:UIControlStateNormal];
                    
                    [self tablViewForPrimaryEmailIds];
                    
                }
            }
            
        } else {
            
            [self performSelector:@selector(AlertViewForEmptyPrimaryEmail) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self tablViewForPrimaryEmailIds];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)AlertViewForEmptyPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to add Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyExitPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Email-Id Already Present. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForEmptyEmailEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to edit Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)AlertViewForNonValidPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForNonValidEmailOnEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyEmailPresentOnEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Email-Id Already Present. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)AlertViewForEmptySecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to add Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForNonValidSecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyPresentSecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Emai-Id Already Saved. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)alertForSecondaryEmailSaving{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Email-Id"
                                                                              message: strMessage
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Email-Id Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    //        textField.placeholder = @"password";
    //        textField.textColor = [UIColor blueColor];
    //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    //        textField.borderStyle = UITextBorderStyleRoundedRect;
    //        textField.secureTextEntry = YES;
    //    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        if (txtHistoricalDays.text.length>0) {
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
            {
                
                [self performSelector:@selector(AlertViewForNonValidSecondaryEmail) withObject:nil afterDelay:0.2];
                
            }else{
                
                if ([arrOfSecondaryEmailNew containsObject:txtHistoricalDays.text]) {
                    
                    [self performSelector:@selector(AlertViewForAlreadyPresentSecondaryEmail) withObject:nil afterDelay:0.2];
                    
                    
                } else {
                    
                    [arrOfSecondaryEmailNew addObject:txtHistoricalDays.text];
                    
                    [arrOfSecondaryEmailAlreadySaved addObject:txtHistoricalDays.text];
                    
                    NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
                    
                    [_btnSecondaryEmail setTitle:strMessage forState:UIControlStateNormal];
                    
                    [self tablViewForSecondaryEmailIds];
                    
                }
            }
        } else {
            
            [self performSelector:@selector(AlertViewForEmptySecondaryEmail) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self tablViewForSecondaryEmailIds];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)tablViewForPrimaryEmailIds{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblViewNew=[[NSMutableArray alloc]init];
    
    [arrDataTblViewNew addObjectsFromArray:arrOfPrimaryEmailNew];
    
    if (arrDataTblViewNew.count==0) {
        //[global AlertMethod:Info :NoDataAvailableReset];
        //[self alertForPrimaryEmailSaving];
        tblData.tag=205;
        [self tableLoad:tblData.tag];
        
        
    }else{
        tblData.tag=205;
        [self tableLoad:tblData.tag];
    }
    
}
-(void)tablViewForSecondaryEmailIds{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblViewNew=[[NSMutableArray alloc]init];
    
    [arrDataTblViewNew addObjectsFromArray:arrOfSecondaryEmailNew];
    
    if (arrDataTblViewNew.count==0) {
        //[global AlertMethod:Info :NoDataAvailableReset];
        //[self alertForSecondaryEmailSaving];
        tblData.tag=206;
        [self tableLoad:tblData.tag];
        
    }else{
        tblData.tag=206;
        [self tableLoad:tblData.tag];
    }
    
}

-(void)setTableFrameForEmailIds :(NSString*)strType
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    UILabel *lblMsg=[[UILabel alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y-40, tblData.bounds.size.width, 30)];
    lblMsg.text=@"Swipe to delete & tap to edit";
    lblMsg.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    // [viewBackGround addSubview:lblMsg];
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 42)];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(methodOnDoneEditingEmailIds) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGround addSubview:btnDone];
    
    
    UIButton *btnAddNew=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.size.width/2+2+tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 42)];
    
    [btnAddNew setTitle:@"Add New" forState:UIControlStateNormal];
    [btnAddNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddNew.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    if ([strType isEqualToString:@"primary"]) {
        
        [btnAddNew addTarget:self action:@selector(alertForPrimaryEmailSaving) forControlEvents:UIControlEventTouchDown];
        
    } else {
        
        [btnAddNew addTarget:self action:@selector(alertForSecondaryEmailSaving) forControlEvents:UIControlEventTouchDown];
        
    }
    
    [viewBackGround addSubview:btnAddNew];
    
}
-(void)methodOnDoneEditingEmailIds{
    
    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
    
    if (strMessage.length==0) {
        
        strMessage=@"";
        
    }
    
    [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
    [_btnPrimaryEmailNew setTitle:strMessage forState:UIControlStateNormal];
    
    // [_btnPrimaryEmailNew setTitle:_btnPrimaryEmail.titleLabel.text forState:UIControlStateNormal];
    
    NSString *strMessageSec=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
    
    if (strMessageSec.length==0) {
        
        strMessageSec=@"";
        
    }
    
    [_btnSecondaryEmail setTitle:strMessageSec forState:UIControlStateNormal];
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
}

-(void)deleteEmailIdFromCoreDataNew :(NSString*)strEmailIdToDelete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND emailId=%@",strLeadId,strEmailIdToDelete];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

-(void)saveEmailIdPrimaryToCoreData
{
    
    for (int k=0; k<arrOfPrimaryEmailAlreadySaved.count; k++) {
        
        [self deleteEmailIdFromCoreDataNew:arrOfPrimaryEmailAlreadySaved[k]];
        
    }
    for (int k=0; k<arrOfPrimaryEmailNew.count; k++) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
        [request setEntity:entityEmailDetail];//leadId
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND emailId=%@",strLeadId,arrOfPrimaryEmailNew[k]];
        
        [request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [request setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        
        if (arrAllObj.count==0)
        {
            [self saveToCoreDataNewEmail :arrOfPrimaryEmailNew[k]];
            // [self saveToCoreDataNewEmailSecondary];
            
        }else
        {
            
            BOOL notMatched,notMatchedSecondaryEmail;
            
            notMatched=YES;
            notMatchedSecondaryEmail=YES;
            
            for (int k=0; k<arrAllObj.count; k++)
            {
                
                matches=arrAllObj[k];
                
                NSString *strEmailToCompare=[matches valueForKey:@"emailId"];
                
                if ([strEmailToCompare isEqualToString:arrOfPrimaryEmailNew[k]]) {
                    
                    notMatched=NO;
                    
                    [matches setValue:arrOfPrimaryEmailNew[k] forKey:@"emailId"];
                    NSError *error1;
                    [context save:&error1];
                    
                }
            }
            
            if (notMatched)
            {
                
                [self saveToCoreDataNewEmail:arrOfPrimaryEmailNew[k]];
                
            }
        }
    }
    
    [self saveEmailIdSecondaryToCoreData];
    
}

-(void)saveEmailIdSecondaryToCoreData
{
    
    for (int k=0; k<arrOfSecondaryEmailAlreadySaved.count; k++) {
        
        [self deleteEmailIdFromCoreDataNew:arrOfSecondaryEmailAlreadySaved[k]];
        
    }
    
    for (int k=0; k<arrOfSecondaryEmailNew.count; k++) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
        [request setEntity:entityEmailDetail];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND emailId=%@",strLeadId,arrOfSecondaryEmailNew[k]];
        
        [request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [request setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        
        if (arrAllObj.count==0)
        {
            [self saveToCoreDataNewEmail :arrOfSecondaryEmailNew[k]];
            // [self saveToCoreDataNewEmailSecondary];
            
        }else
        {
            
            BOOL notMatched,notMatchedSecondaryEmail;
            
            notMatched=YES;
            notMatchedSecondaryEmail=YES;
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strEmailToCompare=[matches valueForKey:@"emailId"];
                
                if ([strEmailToCompare isEqualToString:arrOfSecondaryEmailNew[k]]) {
                    
                    notMatched=NO;
                    
                    [matches setValue:arrOfSecondaryEmailNew[k] forKey:@"emailId"];
                    NSError *error1;
                    [context save:&error1];
                    
                }
            }
            
            if (notMatched) {
                
                [self saveToCoreDataNewEmail:arrOfSecondaryEmailNew[k]];
                
            }
        }
    }
}

-(void)saveToCoreDataNewEmail :(NSString*)strEmailTosave{
    
    if (strEmailTosave.length==0) {
        
    } else {
        
        entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
        
        EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
        
        objEmailDetail.createdBy=@"0";
        objEmailDetail.createdDate=@"0";
        objEmailDetail.emailId=strEmailTosave;
        objEmailDetail.isCustomerEmail=@"true";
        objEmailDetail.isMailSent=@"true";
        objEmailDetail.companyKey=strCompanyKey;
        objEmailDetail.modifiedBy=@"";
        objEmailDetail.modifiedDate=[global modifyDate];
        objEmailDetail.subject=@"";
        objEmailDetail.userName=strUserName;
        objEmailDetail.leadId=strLeadId;
        objEmailDetail.leadMailId=@"";
        objEmailDetail.isDefaultEmail=@"false";

        NSError *error1;
        [context save:&error1];
        
    }
}

- (IBAction)action_AddGraph:(id)sender {
    
    [self endEditing];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"firstGraphImage"];
    [defs setBool:NO forKey:@"servicegraph"];
    
    [defs synchronize];
    
    [self goToGlobalmage:@"Graph"];
    
    
    
    /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
     EditGraphViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewController"];
     objSignViewController.strLeadId=strLeadId;
     objSignViewController.strCompanyKey=strCompanyKey;
     objSignViewController.strUserName=strUserName;
     [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
    
    // 16/04/2020
  /*  UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
    objSignViewController.strLeadId=strLeadId;
    objSignViewController.strCompanyKey=strCompanyKey;
    objSignViewController.strUserName=strUserName;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _collectionViewGraph)
    {
        return arrOfImagenameCollewctionView.count;
        
    }
    else
    {
        return arrNoImage.count;
    }
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CellCollectionService";
    
    GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (collectionView == _collectionViewGraph)
    {
        
        NSString *str;// = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSDictionary *dictdat=[arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    else
    {
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_collectionViewGraph)
    {
        // NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *str;// = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSDictionary *dictdat=[arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    }
    else
    {
        //NSString *str = [arrNoImage objectAtIndex:indexPath.row];
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreview : strIndex];
            
        }
        
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    //return CGSizeMake(140, 140);
    return CGSizeMake(130, 130);
    
}
-(void)textMethod:(UITextField*)textField
{
    textField.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    textField.layer.borderWidth=1.0;
    textField.layer.cornerRadius=5.0;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
}
-(void)textMethodNew:(UITextField*)textField
{
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
}
-(void)textMethodForDisable:(UITextField*)textField
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    strStatusss=[NSString stringWithFormat:@"%@",[defs valueForKey:@"leadStatusSales"]];
    
    NSUserDefaults *dfsStageStatus=[NSUserDefaults standardUserDefaults];
    if ([dfsStageStatus boolForKey:@"backFromInspectionNew"] == YES)
    {
        strStatusss= [dfsStageStatus valueForKey:@"leadStatusSales"];
        strStageSysName=[dfsStageStatus valueForKey:@"stageSysNameSales"];
    }
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        textField.inputView=[[UIView alloc]init];
        textField.delegate = self;
    }
    else
    {
        
    }
    
}
- (IBAction)actionOnNotesHistory:(id)sender
{
    [self endEditing];
    //[self goToServiceHistory];
    [self goToHistory];
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ClockInOutViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
- (IBAction)actionOnShowServicePOCDetail:(id)sender
{
    [self endEditing];
    
    if(_constServicePOCDetail_H.constant==630)
    {
        _constServicePOCDetail_H.constant=0;
        [_btnShowServicePOCDetail setTitle:@"Show" forState:UIControlStateNormal];
        
        [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionLayoutSubviews animations:^{
            [self.view layoutIfNeeded];
        } completion:nil];
    }
    else
    {
        _constServicePOCDetail_H.constant=630;
        [_btnShowServicePOCDetail setTitle:@"Hide" forState:UIControlStateNormal];
        
        [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionLayoutSubviews animations:^{
            [self.view layoutIfNeeded];
        } completion:nil];
        
    }
}

- (IBAction)actionOnEditServicePOCDetail:(id)sender
{
    [self endEditing];
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        _viewServicePOCDetail.userInteractionEnabled=NO;
    }
    else
    {
        _viewServicePOCDetail.userInteractionEnabled=YES;
        
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)actionOnEditBillingPOCDetail:(id)sender
{
    [self endEditing];
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        _viewBillingPOCDetail.userInteractionEnabled=NO;
    }
    else
    {
        _viewBillingPOCDetail.userInteractionEnabled=YES;
        
    }
    
}
- (IBAction)actionOnShowBillingPOCDetail:(id)sender
{
    [self endEditing];
    if(_constantBillingPOCDetail_H.constant==630)
    {
        _constantBillingPOCDetail_H.constant=0;
        [_btnShowBillingPOCDetail setTitle:@"Show" forState:UIControlStateNormal];
        
        [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionLayoutSubviews animations:^{
            [self.view layoutIfNeeded];
        } completion:nil];
    }
    else
    {
        _constantBillingPOCDetail_H.constant=630;
        [_btnShowBillingPOCDetail setTitle:@"Hide" forState:UIControlStateNormal];
        
        [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionLayoutSubviews animations:^{
            [self.view layoutIfNeeded];
        } completion:nil];
    }
}
- (IBAction)actionOnSendMailServicePOC:(id)sender
{
    [self endEditing];
    if ([MFMailComposeViewController canSendMail])
    {
        NSString *strEmials=[NSString stringWithFormat:@"%@,%@",_txtServicePOCPrimaryEmail.text,_txtServicePOCSecondaryEmail.text];
        
        [global emailComposer:strEmials :@"" :@"" :self];
        
    }
    else
    {
        [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
        
        
    }
    
}
- (IBAction)actionOnSendMailBillingPOC:(id)sender
{
    [self endEditing];
    if ([MFMailComposeViewController canSendMail])
    {
        NSString *strEmials=[NSString stringWithFormat:@"%@,%@",_txtBillingPOCPrimaryEmail.text,_txtBillingPOCSecondaryEmail.text];
        
        [global emailComposer:strEmials :@"" :@"" :self];
        
    }
    else
    {
        [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
        
        
    }
    
}

- (IBAction)actionOnCallBillingPOCCell:(id)sender
{
    [self endEditing];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:09827269471"]];
}
-(void)tempPOC
{
    _btnEditBillingPOCDetail.hidden=YES;
    _btnShowBillingPOCDetail.hidden=YES;
    _btnSendMailBillingPOC.hidden=YES;
    
    _btnEditServicePOCDetail.hidden=YES;
    _btnShowServicePOCDetail.hidden=YES;
    _btnSendMailServicePOC.hidden=YES;
    
    _btnEditBillingPOCDetail.enabled=NO;
    _btnShowBillingPOCDetail.enabled=NO;
    _btnSendMailBillingPOC.enabled=NO;
    
    _btnEditServicePOCDetail.enabled=NO;
    _btnShowServicePOCDetail.enabled=NO;
    _btnSendMailServicePOC.enabled=NO;
    
    _constantBillingPOCDetail_H.constant=630;
    _constServicePOCDetail_H.constant=630;
}
-(void)methodSetAllViews
{
    if([strFlowType isEqualToString:@"commercial"]||[strFlowType isEqualToString:@"Commercial"])
    {
        _const_ViewPropertyType_H.constant = 82;
        //_view5.frame = CGRectMake(_view5.frame.origin.x, _view5.frame.origin.y, [UIScreen mainScreen].bounds.size.width, 1038+82);
        
    }
    else
    {
        // _const_ViewPropertyType_H.constant = 0;
        _const_ViewPropertyType_H.constant = 82;
        
        // _view5.frame = CGRectMake(_view5.frame.origin.x, _view5.frame.origin.y, [UIScreen mainScreen].bounds.size.width, 1038);
        
    }
    /*for(UIView *viewInScroll in _scrollViewDetail.subviews)
     {
     [viewInScroll removeFromSuperview];
     }*/
    
    CGRect frameFor_View1=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,_view1.frame.size.height);
    [_view1 setFrame:frameFor_View1];
    
    [_scrollViewDetail addSubview:_view1];
    
    CGRect frameFor_View2;
    if([_btnShowCustomerInfo.currentTitle isEqualToString:@"Show"])
    {
        //_view2.frame.size.height=861;
        frameFor_View2=CGRectMake(0, _view1.frame.origin.y+_view1.frame.size.height, [UIScreen mainScreen].bounds.size.width,0);
    }
    else
    {
        if (view1EditName==YES)
        {
            frameFor_View2=CGRectMake(0, _view1.frame.origin.y+_view1.frame.size.height, [UIScreen mainScreen].bounds.size.width,664-94+175 + 75);
        }
        else
        {
            frameFor_View2=CGRectMake(0, _view1.frame.origin.y+_view1.frame.size.height, [UIScreen mainScreen].bounds.size.width,664-32+175 + 75);
        }
    }
    [_view2 setFrame:frameFor_View2];
   // _view2.backgroundColor=[UIColor yellowColor];
    // _view3.backgroundColor=[UIColor redColor];
    
    [_scrollViewDetail addSubview:_view2];
    
    CGRect frameFor_View3=CGRectMake(0, _view2.frame.origin.y+_view2.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view3.frame.size.height);
    
    
    [_view3 setFrame:frameFor_View3];
    
    [_scrollViewDetail addSubview:_view3];
    
    
    CGRect frameFor_View4;
    if([_btnShowServiceAddress.currentTitle isEqualToString:@"Show"])
    {
        frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,0);
    }
    else
    {
        if (view3EditServiceAdd==YES)
        {
            
            frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,1938-90-65);
            
            if ([strFlowType isEqualToString:@"Commercial"] || [strFlowType isEqualToString:@"commercial"])
            {
                frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,1938-90-65+80);
            }
            else
            {
                // frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,1938-90-65);
                frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,1938-90-65+80);
                
                
            }
        }
        else
        {
            frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,1938-512-230);
            if ([strFlowType isEqualToString:@"Commercial"] || [strFlowType isEqualToString:@"commercial"])
            {
                frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,1938-512-230+80);
                
            }
            else
            {
                //  frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,1938-512-230);
                frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,1938-512-230+80);
                
                
            }
        }
    }
    [_view4 setFrame:frameFor_View4];
    
    [_scrollViewDetail addSubview:_view4];
    
    CGRect frameFor_View5=CGRectMake(0, _view4.frame.origin.y+_view4.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view5.frame.size.height);
    
    
    [_view5 setFrame:frameFor_View5];
    
    [_scrollViewDetail addSubview:_view5];
    
    CGRect frameFor_View6;
    if([_btnShowBillingAddress.currentTitle isEqualToString:@"Show"])
    {
        frameFor_View6=CGRectMake(0, _view5.frame.origin.y+_view5.frame.size.height, [UIScreen mainScreen].bounds.size.width,0);
    }
    else
    {
        if (view5EditBillingAdd==YES)
        {
            frameFor_View6=CGRectMake(0, _view5.frame.origin.y+_view5.frame.size.height, [UIScreen mainScreen].bounds.size.width,1582-90-65);
        }
        else
        {
            frameFor_View6=CGRectMake(0, _view5.frame.origin.y+_view5.frame.size.height, [UIScreen mainScreen].bounds.size.width,1582-512-230);
        }
    }
    
    [_view6 setFrame:frameFor_View6];
    
    [_scrollViewDetail addSubview:_view6];
    
    CGRect frameFor_View7=CGRectMake(0, _view6.frame.origin.y+_view6.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view7.frame.size.height);
    [_view7 setFrame:frameFor_View7];
    
    [_scrollViewDetail addSubview:_view7];
    
    [_scrollViewDetail setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view7.frame.size.height+_view7.frame.origin.y)];
}

-(void)customerInfoInitial
{
    
}
- (IBAction)actionEditCustomerInfo:(id)sender
{
    [self endEditing];
    
    if (_view2.frame.size.height==0)
    {
        if ([_btnShowCustomerInfo.currentTitle isEqualToString:@"Show"])
        {
            [_btnShowCustomerInfo setTitle:@"Hide" forState:UIControlStateNormal];
            [_btnShowCustomerInfo setImage:[UIImage imageNamed:@"showiPhoneOld.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_btnShowCustomerInfo setTitle:@"Show" forState:UIControlStateNormal];
            [_btnShowCustomerInfo setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
        }
        
        // enable customer view fields
     
        NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
        [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
        [_btnPrimaryEmailNew setTitle:strMessage forState:UIControlStateNormal];
        
        [_btnCellNo setTitle:_txtCellNo.text forState:UIControlStateNormal];
        [_btnPrimaryPhone setTitle:_txtPrimaryPhone.text forState:UIControlStateNormal];
        [_btnSecondaryPhone setTitle:_txtSecondaryPhone.text forState:UIControlStateNormal];
        
        NSString *strName=@"";
        if(_txtFirstName.text.length>0)
        {
            _txtFirstName.text=[_txtFirstName.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            strName = [NSString stringWithFormat:@"%@",[_txtFirstName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        if(_txtMiddleName.text.length>0)
        {
            _txtMiddleName.text=[_txtMiddleName.text stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            strName = [NSString stringWithFormat:@"%@ %@",strName,[_txtMiddleName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        if(_txtLastName.text.length>0)
        {
            _txtLastName.text=[_txtLastName.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            strName = [NSString stringWithFormat:@"%@ %@",strName,[_txtLastName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        _lblView1FullName.text=[NSString stringWithFormat:@"%@",strName];
        
        if (_constView1FisrtName_H.constant==94)
        {
            view1EditName=YES;
            
            _constView1FullName_H.constant=32;
            _constView1FisrtName_H.constant=0;
            [self hideBorderColorInSection:1];
            [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        else
        {
            view1EditName=NO;
            
            _constView1FullName_H.constant=0;
            
            _constView1FisrtName_H.constant=94;
            
            [self showBorderColorInSection:1];
            
            [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"cancelEdit.png"] forState:UIControlStateNormal];
        }
        [self methodSetAllViews];
       
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            [self hideBorderColorInSection:1];
        }
        [self setUnderLineText];
        
        
        //[global AlertMethod:@"Alert" :@"Please expand customer info to edit"];
    }
    else
    {
        NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
        [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
        [_btnPrimaryEmailNew setTitle:strMessage forState:UIControlStateNormal];
        
        [_btnCellNo setTitle:_txtCellNo.text forState:UIControlStateNormal];
        [_btnPrimaryPhone setTitle:_txtPrimaryPhone.text forState:UIControlStateNormal];
        [_btnSecondaryPhone setTitle:_txtSecondaryPhone.text forState:UIControlStateNormal];
        
        NSString *strName=@"";
        if(_txtFirstName.text.length>0)
        {
            _txtFirstName.text=[_txtFirstName.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            strName = [NSString stringWithFormat:@"%@",[_txtFirstName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        if(_txtMiddleName.text.length>0)
        {
            _txtMiddleName.text=[_txtMiddleName.text stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            strName = [NSString stringWithFormat:@"%@ %@",strName,[_txtMiddleName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        if(_txtLastName.text.length>0)
        {
            _txtLastName.text=[_txtLastName.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            strName = [NSString stringWithFormat:@"%@ %@",strName,[_txtLastName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        _lblView1FullName.text=[NSString stringWithFormat:@"%@",strName];
        
        if (_constView1FisrtName_H.constant==94)
        {
            view1EditName=YES;
            
            _constView1FullName_H.constant=32;
            _constView1FisrtName_H.constant=0;
            [self hideBorderColorInSection:1];
            [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        else
        {
            view1EditName=NO;
            
            _constView1FullName_H.constant=0;
            
            _constView1FisrtName_H.constant=94;
            
            [self showBorderColorInSection:1];
            
            [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"cancelEdit.png"] forState:UIControlStateNormal];
        }
        [self methodSetAllViews];
    }
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        [self hideBorderColorInSection:1];
    }

    [self setUnderLineText];
}
-(void)customerInfoUnEdit
{
    _txtCellNo.enabled=NO;
    _txtCellNo.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    _txtPrimaryPhone.enabled=NO;
    _txtPrimaryPhone.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    _txtSecondaryPhone.enabled=NO;
    _txtSecondaryPhone.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    _btnPriority.enabled=NO;
    _btnSource.enabled=NO;
    
}
-(void)customerInfoEditCase
{
    _txtCellNo.enabled=YES;
    _txtCellNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    
    _txtPrimaryPhone.enabled=YES;
    _txtPrimaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    
    _txtSecondaryPhone.enabled=YES;
    _txtSecondaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    
    _btnPriority.enabled=YES;
    _btnSource.enabled=YES;
}
- (IBAction)actionShowCustomerInfo:(id)sender
{
    [self endEditing];
    
    if ([_btnShowCustomerInfo.currentTitle isEqualToString:@"Show"])
    {
        [_btnShowCustomerInfo setTitle:@"Hide" forState:UIControlStateNormal];
        [_btnShowCustomerInfo setImage:[UIImage imageNamed:@"showiPhoneOld.png"] forState:UIControlStateNormal];
    }
    else
    {
        [_btnShowCustomerInfo setTitle:@"Show" forState:UIControlStateNormal];
        [_btnShowCustomerInfo setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
        [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    }
    
    [self methodSetAllViews];
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        [self hideBorderColorInSection:1];
    }
}
- (IBAction)actionOnEditServiceAddress:(id)sender
{
    [self endEditing];
    
    if (_view4.frame.size.height==0)
    {
        if ([_btnShowServiceAddress.currentTitle isEqualToString:@"Show"])
        {
            [_btnShowServiceAddress setTitle:@"Hide" forState:UIControlStateNormal];
            [_btnShowServiceAddress setImage:[UIImage imageNamed:@"showiPhoneOld.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_btnShowServiceAddress setTitle:@"Show" forState:UIControlStateNormal];
            [_btnShowServiceAddress setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
        }
        
        // enable service address fields
        
        [self setServiceAddressData];
        
        if (_constView3ServiceAddress_H.constant==515)
        {
            view3EditServiceAdd=NO;
            
            _constView3newlblServiceAddress_H.constant=90;
            _constView3ServiceAddress_H.constant=0;
            
            _constServiceContacAllName_H.constant=0;
            _constServiceContactFullName_H.constant=65;
            
            [self hideBorderColorInSection:2];
            // [_btnEditServiceAddress setTitle:@"Edit" forState:UIControlStateNormal];
            [_btnEditServiceAddress setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        else
        {
            view3EditServiceAdd=YES;
            
            _constView3newlblServiceAddress_H.constant=0;
            
            _constView3ServiceAddress_H.constant=515;
            
            _constServiceContacAllName_H.constant=230;
            _constServiceContactFullName_H.constant=0;
            
            [self showBorderColorInSection:2];
            //[_btnEditServiceAddress setTitle:@"Cancel" forState:UIControlStateNormal];
            [_btnEditServiceAddress setImage:[UIImage imageNamed:@"cancelEdit.png"] forState:UIControlStateNormal];
        }
        
        [self methodSetAllViews];
       
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            [self hideBorderColorInSection:2];
        }
        else if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            [self hideBorderColorInSection:2];
            if (view3EditServiceAdd)
            {
                [self enableServicePOCDetail];
            }
            
        }
        [self setUnderLineText];
        
        
        //[global AlertMethod:@"Alert" :@" Service Address info to edit"];
    }
    else
    {
        [self setServiceAddressData];
        
        if (_constView3ServiceAddress_H.constant==515)
        {
            view3EditServiceAdd=NO;
            
            _constView3newlblServiceAddress_H.constant=90;
            _constView3ServiceAddress_H.constant=0;
            
            _constServiceContacAllName_H.constant=0;
            _constServiceContactFullName_H.constant=65;
            
            [self hideBorderColorInSection:2];
            // [_btnEditServiceAddress setTitle:@"Edit" forState:UIControlStateNormal];
            [_btnEditServiceAddress setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        else
        {
            view3EditServiceAdd=YES;
            
            _constView3newlblServiceAddress_H.constant=0;
            
            _constView3ServiceAddress_H.constant=515;
            
            _constServiceContacAllName_H.constant=230;
            _constServiceContactFullName_H.constant=0;
            
            [self showBorderColorInSection:2];
            //[_btnEditServiceAddress setTitle:@"Cancel" forState:UIControlStateNormal];
            [_btnEditServiceAddress setImage:[UIImage imageNamed:@"cancelEdit.png"] forState:UIControlStateNormal];
        }
        
        [self methodSetAllViews];
    }
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        [self hideBorderColorInSection:2];
    }
    else if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        [self hideBorderColorInSection:2];
        if (view3EditServiceAdd)
        {
            [self enableServicePOCDetail];
        }
    }
    [self setUnderLineText];
}
- (IBAction)actionOnShowServiceAddress:(id)sender
{
    [self endEditing];
    
    if ([_btnShowServiceAddress.currentTitle isEqualToString:@"Show"])
    {
        [_btnShowServiceAddress setTitle:@"Hide" forState:UIControlStateNormal];
        [_btnShowServiceAddress setImage:[UIImage imageNamed:@"showiPhoneOld.png"] forState:UIControlStateNormal];
    }
    else
    {
        [_btnShowServiceAddress setTitle:@"Show" forState:UIControlStateNormal];
        [_btnShowServiceAddress setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
        [_btnEditServiceAddress setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    }
    [self methodSetAllViews];
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        [self hideBorderColorInSection:2];
    }
}
- (IBAction)actionOnEditBillingAddress:(id)sender
{
    [self endEditing];
    
    if (_view6.frame.size.height==0)
    {
        if ([_btnShowBillingAddress.currentTitle isEqualToString:@"Show"])
        {
            [_btnShowBillingAddress setTitle:@"Hide" forState:UIControlStateNormal];
            [_btnShowBillingAddress setImage:[UIImage imageNamed:@"showiPhoneOld.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_btnShowBillingAddress setTitle:@"Show" forState:UIControlStateNormal];
            [_btnShowBillingAddress setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
        }
        
        // enable billing address fields
      
            
        [self setBillingAddressData];
        
        if (_constView5BillingAddress_H.constant==512)
        {
            view5EditBillingAdd=NO;
            
            _constView5NewBillingAddress.constant=90;
            _constView5BillingAddress_H.constant=0;
            
            _constBillingContactFullName_H.constant=65;
            _constBillingContactAllName_H.constant=0;
            
            [self hideBorderColorInSection:3];
            // [_btnEditBillingAddress setTitle:@"Edit" forState:UIControlStateNormal];
            [_btnEditBillingAddress setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        else
        {
            view5EditBillingAdd=YES;
            
            _constView5NewBillingAddress.constant=0;
            
            _constView5BillingAddress_H.constant=512;
            
            _constBillingContactFullName_H.constant=0;
            _constBillingContactAllName_H.constant=230;
            
            [self showBorderColorInSection:3];
            // [_btnEditBillingAddress setTitle:@"Cancel" forState:UIControlStateNormal];
            [_btnEditBillingAddress setImage:[UIImage imageNamed:@"cancelEdit.png"] forState:UIControlStateNormal];
        }
        [self methodSetAllViews];
        
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
        {
            [self hideBorderColorInSection:3];
        }
        else if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            if ([_btnEditBillingAddress.currentImage isEqual:[UIImage imageNamed:@"cancelEdit.png"]])
            {
                [self showBorderColorInSection:3];
            }
            else
            {
                [self hideBorderColorInSection:3];
            }
        }
        else
        {
            if ([_btnEditBillingAddress.currentImage isEqual:[UIImage imageNamed:@"cancelEdit.png"]])
            {
                [self showBorderColorInSection:3];
            }
            else
            {
                [self hideBorderColorInSection:3];
            }
        }

        [self setUnderLineText];
        
    }
    else
    {
        [self setBillingAddressData];
        
        if (_constView5BillingAddress_H.constant==512)
        {
            view5EditBillingAdd=NO;
            
            _constView5NewBillingAddress.constant=90;
            _constView5BillingAddress_H.constant=0;
            
            _constBillingContactFullName_H.constant=65;
            _constBillingContactAllName_H.constant=0;
            
            [self hideBorderColorInSection:3];
            // [_btnEditBillingAddress setTitle:@"Edit" forState:UIControlStateNormal];
            [_btnEditBillingAddress setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        else
        {
            view5EditBillingAdd=YES;
            
            _constView5NewBillingAddress.constant=0;
            
            _constView5BillingAddress_H.constant=512;
            
            _constBillingContactFullName_H.constant=0;
            _constBillingContactAllName_H.constant=230;
            
            [self showBorderColorInSection:3];
            // [_btnEditBillingAddress setTitle:@"Cancel" forState:UIControlStateNormal];
            [_btnEditBillingAddress setImage:[UIImage imageNamed:@"cancelEdit.png"] forState:UIControlStateNormal];
        }
        [self methodSetAllViews];
    }
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        [self hideBorderColorInSection:3];
    }
    else if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        if ([_btnEditBillingAddress.currentImage isEqual:[UIImage imageNamed:@"cancelEdit.png"]])
        {
            [self showBorderColorInSection:3];
            
        }
        else
        {
            [self hideBorderColorInSection:3];
        }
    }
    else
    {
        if ([_btnEditBillingAddress.currentImage isEqual:[UIImage imageNamed:@"cancelEdit.png"]])
        {
            [self showBorderColorInSection:3];
        }
        else
        {
            [self hideBorderColorInSection:3];
        }
    }
    
    [self setUnderLineText];
    
    [self disableBillingAddress];
}
- (IBAction)actionOnShowBillingAddress:(id)sender
{
    [self endEditing];
    
    if ([_btnShowBillingAddress.currentTitle isEqualToString:@"Show"])
    {
        [_btnShowBillingAddress setTitle:@"Hide" forState:UIControlStateNormal];
        [_btnShowBillingAddress setImage:[UIImage imageNamed:@"showiPhoneOld.png"] forState:UIControlStateNormal];
    }
    else
    {
        [_btnShowBillingAddress setTitle:@"Show" forState:UIControlStateNormal];
        [_btnShowBillingAddress setImage:[UIImage imageNamed:@"hideiPhoneOld.png"] forState:UIControlStateNormal];
        [_btnEditBillingAddress setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    }
    [self methodSetAllViews];
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        [self hideBorderColorInSection:3];
    }
}
-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return center;
}

- (IBAction)actionOnNewServiceAddress:(id)sender
{
    [self endEditing];
    
    if(_lblNewServiceAddress.text.length>0)
    {
        [global redirectOnAppleMap:self :_lblNewServiceAddress.text];
        
    }
}
-(void)showBorderColorInSection:(NSInteger)section
{
    if(section==1)// customer
    {
        
        _txtFirstName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtFirstName.layer.borderWidth=1.0;
        
        _txtMiddleName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtMiddleName.layer.borderWidth=1.0;
        
        
        _txtLastName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtLastName.layer.borderWidth=1.0;
        
        
        _txtCustomerName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtCustomerName.layer.borderWidth=1.0;
        
        _txtScheduleStartDate.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtScheduleStartDate.layer.borderWidth=1.0;
        
        _txtAccount.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtAccount.layer.borderWidth=1.0;
        
        
        
        _txtPrimaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtPrimaryEmail.layer.borderWidth=1.0;
        
        _txtCellNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtCellNo.layer.borderWidth=1.0;
        
        
        
        _txtPrimaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtSecondaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtSecondaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtCompanyName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtCompanyName.layer.borderWidth=1.0;
        
        
        
        _txtBranchName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBranchName.layer.borderWidth=1.0;
        
        
        
        
        
        // disable buttons
        _btnPriority.enabled=YES;
        _btnSource.enabled=YES;
        _btnPrimaryEmail.enabled=YES;
        _txtLead.enabled=YES; //NO
        _txtFirstName.enabled=YES;
        _txtMiddleName.enabled=YES;
        _txtLastName.enabled=YES;
        _txtCustomerName.enabled=YES;
        _txtScheduleStartDate.enabled=NO;
        _txtAccount.enabled=NO;
        _txtPrimaryEmail.enabled=YES;
        _txtCellNo.enabled=YES;
        _txtPrimaryPhone.enabled=YES;
        _txtSecondaryPhone.enabled=YES;
        _txtCompanyName.enabled=YES; //NO
        _txtBranchName.enabled=YES;
        
        [_btnPriority setBackgroundImage:[UIImage imageNamed:@"drop_downButton.png"] forState:UIControlStateNormal];
        [_btnSource setBackgroundImage:[UIImage imageNamed:@"drop_downButton.png"] forState:UIControlStateNormal];
        [_btnInspector setBackgroundImage:[UIImage imageNamed:@"drop_downButton.png"] forState:UIControlStateNormal];
        
        [_btnCellNo setHidden:YES];
        [_btnPrimaryPhone setHidden:YES];
        [_btnSecondaryPhone setHidden:YES];
        _txtPrimaryPhone.hidden=NO;
        _txtCellNo.hidden=NO;
        _txtSecondaryPhone.hidden=NO;
        
        
        
        _btnPrimaryEmail.hidden=NO;
        _btnPrimaryEmailNew.hidden=YES;
        // _btnPrimaryEmailNew.hidden=YES;
        
        
        
        _txtLead.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtLead.layer.borderWidth=1.0;
        
        _txtAccount.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtAccount.layer.borderWidth=1.0;
        _txtCompanyName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtCompanyName.layer.borderWidth=1.0;
        
        _txtScheduleStartDate.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtScheduleStartDate.layer.borderWidth=1.0;
        
        
        _btnLeadTypeInside.enabled=YES;
        _btnLeadTypeField.enabled=YES;
        
        _btnOppTypeCommercial.enabled = NO; //YES
        _btnOppTypeResidential.enabled = NO; //YES
        
      
        _txtLead.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtLead.layer.borderWidth=1.0;
        
        _txtCompanyName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtCompanyName.layer.borderWidth=1.0;
        
    }
    
    else if (section==2)// service
        
    {
        
        _txtCityServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtCityServiceAddress.layer.borderWidth=1.0;
        
        
        
        
        _txtZipcodeServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtZipcodeServiceAddress.layer.borderWidth=1.0;
        
        
        
        
        _txtViewAdd1ServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtViewAdd1ServiceAddress.layer.borderWidth=1.0;
        
        
        _txtViewAdd2ServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtViewAdd2ServiceAddress.layer.borderWidth=1.0;
        
        
        
        _txtCityServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtCityServiceAddress.layer.borderWidth=1.0;
        
        
        
        _txtZipcodeServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtZipcodeServiceAddress.layer.borderWidth=1.0;
        
        
        _txtServicePOCFirstName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCFirstName.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCLastName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCLastName.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCMiddleName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCMiddleName.layer.borderWidth=1.0;
        
        
        _txtServicePOCCellNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCCellNo.layer.borderWidth=1.0;
        
        
        _txtServicePOCPrimaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCPrimaryEmail.layer.borderWidth=1.0;
        
        
        _txtServicePOCSecondaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCPrimaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        _txtServicePOCSecondaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtGateCodeServiceNotes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtMapCodeServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        _txtViewAddressNotesServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        _txtViewAddressNotesServiceAddress.layer.borderWidth=1.0;
        
        _txtViewDirectionServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtViewDirectionServiceAddress.layer.borderWidth=1.0;
        
        
        //eNABLE
        
        _txtCityServiceAddress.enabled=YES;
        _txtZipcodeServiceAddress.enabled=YES;
        _txtViewAdd1ServiceAddress.editable=YES;
        _txtViewAdd2ServiceAddress.editable=YES;
        _txtCityServiceAddress.enabled=YES;
        _txtZipcodeServiceAddress.enabled=YES;
        _txtServicePOCFirstName.enabled=YES;
        _txtServicePOCLastName.enabled=YES;
        _txtServicePOCMiddleName.enabled=YES;
        _txtServicePOCCellNo.enabled=YES;
        _txtServicePOCPrimaryEmail.enabled=YES;
        _txtServicePOCPrimaryPhone.enabled=YES;
        _txtServicePOCPrimaryPhone.enabled=YES;
        _txtServicePOCPrimaryPhone.enabled=YES;
        _txtServicePOCPrimaryPhone.enabled=YES;
        _txtServicePOCPrimaryPhone.enabled=YES;
        
        
        
        _txtMapCodeServiceAddress.enabled=YES;
        _txtGateCodeServiceNotes.enabled=YES;
        _txtViewAddressNotesServiceAddress.editable=YES;
        _txtViewDirectionServiceAddress.editable=YES;
        
        
        //TEMP
        
        [_btnServiceContactPrimaryEmail setHidden:YES];
        [_btnServiceContactSecondaryEmail setHidden:YES];
        [_btnServiceContactCell setHidden:YES];
        [_btnServiceContactPrimaryPhone setHidden:YES];
        [_btnServiceContactSecondaryPhone setHidden:YES];
        _txtServicePOCPrimaryEmail.hidden=NO;
        _txtServicePOCSecondaryEmail.hidden=NO;
        _txtServicePOCCellNo.hidden=NO;
        _txtServicePOCPrimaryPhone.hidden=NO;
        _txtServicePOCSecondaryPhone.hidden=NO;
        
        
        _txtServiceCounty.enabled=YES;
        _txtServiceSchoolDistrict.enabled=YES;
        
        _txtServiceCounty.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServiceCounty.layer.borderWidth=1.0;
        
        _txtServiceSchoolDistrict.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtServiceSchoolDistrict.layer.borderWidth=1.0;
        
        
        [_btnTaxCode setEnabled:YES];
        [_btnTaxCode setBackgroundImage:[UIImage imageNamed:@"drop_downButton.png"] forState:UIControlStateNormal];
        
        [_btnPropertyType setEnabled:YES];
        [_btnPropertyType setBackgroundImage:[UIImage imageNamed:@"drop_downButton.png"] forState:UIControlStateNormal];
        
        _btnServiceAddressSubType.enabled = YES;
        [_btnServiceAddressSubType setBackgroundImage:[UIImage imageNamed:@"drop_downButton.png"] forState:UIControlStateNormal];
        
        
    }
    
    else if (section==3)// billing
        
    {
        
        _txtCityBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtCityBillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtZipCodeBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtZipCodeBillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtViewAdd1BillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtViewAdd1BillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtViewAdd2BillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtViewAdd2BillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtMapCodeBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtMapCodeBillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCFirstName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingPOCFirstName.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCMiddleName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingPOCMiddleName.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCLastName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingPOCLastName.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCPrimaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingPOCPrimaryEmail.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCSecondaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingPOCSecondaryEmail.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCCellNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingPOCCellNo.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCPrimaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingPOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCSecondaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingPOCSecondaryPhone.layer.borderWidth=1.0;
        
        
        //ENABLE
        
        
        _txtCityBillingAddress.enabled=YES;
        _txtZipCodeBillingAddress.enabled=YES;
        _txtViewAdd1BillingAddress.editable=YES;
        _txtViewAdd2BillingAddress.editable=YES;
        _txtMapCodeBillingAddress.enabled=YES;
        _txtBillingPOCFirstName.enabled=YES;
        _txtBillingPOCMiddleName.enabled=YES;
        _txtBillingPOCLastName.enabled=YES;
        _txtBillingPOCPrimaryEmail.enabled=YES;
        _txtBillingPOCSecondaryEmail.enabled=YES;
        _txtBillingPOCCellNo.enabled=YES;
        _txtBillingPOCPrimaryPhone.enabled=YES;
        _txtBillingPOCSecondaryPhone.enabled=YES;
        
        
        
        
        //TEMP
        
        [_btnBillingContactPrimaryEmail setHidden:YES];
        [_btnBillingContactSecondaryEmail setHidden:YES];
        [_btnBillingContactCell setHidden:YES];
        [_btnBillingContactPrimaryPhone setHidden:YES];
        [_btnBillingContactSecondaryPhone setHidden:YES];
        
        
        _txtBillingPOCPrimaryEmail.hidden=NO;
        _txtBillingPOCSecondaryEmail.hidden=NO;
        _txtBillingPOCCellNo.hidden=NO;
        _txtBillingPOCPrimaryPhone.hidden=NO;
        _txtBillingPOCSecondaryPhone.hidden=NO;
        
        _txtBillingCounty.enabled=YES;
        _txtBillingSchoolDistrict.enabled=YES;
        
        _txtBillingCounty.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingCounty.layer.borderWidth=1.0;
        
        _txtBillingSchoolDistrict.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        
        _txtBillingSchoolDistrict.layer.borderWidth=1.0;
        
    }
    
}

-(void)enableServicePOCDetail
{
    
    _txtServicePOCFirstName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePOCFirstName.layer.borderWidth=1.0;
    
    _txtServicePOCLastName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePOCLastName.layer.borderWidth=1.0;
    
    _txtServicePOCMiddleName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePOCMiddleName.layer.borderWidth=1.0;
    
    _txtServicePOCCellNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePOCCellNo.layer.borderWidth=1.0;
    
    _txtServicePOCPrimaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePOCPrimaryEmail.layer.borderWidth=1.0;
    
    _txtServicePOCSecondaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
    
    _txtServicePOCPrimaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
    
    _txtServicePOCSecondaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
    
    ///////////////////////
    
    _txtServicePOCFirstName.enabled = YES;
    
    _txtServicePOCLastName.enabled = YES;
    
    _txtServicePOCMiddleName.enabled = YES;
    
    _txtServicePOCCellNo.enabled = YES;
    
    _txtServicePOCPrimaryEmail.enabled = YES;
    
    _txtServicePOCSecondaryEmail.enabled = YES;
    
    _txtServicePOCPrimaryPhone.enabled = YES;
    
    _txtServicePOCSecondaryPhone.enabled = YES;
    
    //eNABLE

    _txtServicePOCFirstName.enabled=YES;
    _txtServicePOCLastName.enabled=YES;
    _txtServicePOCMiddleName.enabled=YES;
    _txtServicePOCCellNo.enabled=YES;
    _txtServicePOCPrimaryEmail.enabled=YES;
    _txtServicePOCPrimaryPhone.enabled=YES;
    _txtServicePOCPrimaryPhone.enabled=YES;
    _txtServicePOCPrimaryPhone.enabled=YES;
    _txtServicePOCPrimaryPhone.enabled=YES;
    _txtServicePOCPrimaryPhone.enabled=YES;

    
    //TEMP
    
    [_btnServiceContactPrimaryEmail setHidden:YES];
    [_btnServiceContactSecondaryEmail setHidden:YES];
    [_btnServiceContactCell setHidden:YES];
    [_btnServiceContactPrimaryPhone setHidden:YES];
    [_btnServiceContactSecondaryPhone setHidden:YES];
    _txtServicePOCPrimaryEmail.hidden=NO;
    _txtServicePOCSecondaryEmail.hidden=NO;
    _txtServicePOCCellNo.hidden=NO;
    _txtServicePOCPrimaryPhone.hidden=NO;
    _txtServicePOCSecondaryPhone.hidden=NO;
}

-(void)enableBillingAddressDetail
{
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        _btnCheckBox.enabled = NO;
    }
    else
    {
        _btnCheckBox.enabled = YES;
    }
    
    _txtCityBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtCityBillingAddress.layer.borderWidth=1.0;
    
    _txtZipCodeBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtZipCodeBillingAddress.layer.borderWidth=1.0;
    
    _txtViewAdd1BillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdd1BillingAddress.layer.borderWidth=1.0;
    
    _txtViewAdd2BillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdd2BillingAddress.layer.borderWidth=1.0;
    
    _txtMapCodeBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtMapCodeBillingAddress.layer.borderWidth=1.0;
    
    _txtBillingPOCFirstName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCFirstName.layer.borderWidth=1.0;
    
    _txtBillingPOCMiddleName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCMiddleName.layer.borderWidth=1.0;
    
    _txtBillingPOCLastName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCLastName.layer.borderWidth=1.0;
    
    _txtBillingPOCPrimaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCPrimaryEmail.layer.borderWidth=1.0;
    
    _txtBillingPOCSecondaryEmail.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCSecondaryEmail.layer.borderWidth=1.0;
    
    _txtBillingPOCCellNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCCellNo.layer.borderWidth=1.0;
    
    _txtBillingPOCPrimaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCPrimaryPhone.layer.borderWidth=1.0;
    
    _txtBillingPOCSecondaryPhone.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCSecondaryPhone.layer.borderWidth=1.0;
    
    
    _txtMapCodeBillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtMapCodeBillingAddress.layer.borderWidth=1.0;
    
    
    _txtBillingPOCMiddleName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBillingPOCMiddleName.layer.borderWidth=1.0;
    
    _txtBillingPOCLastName.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];;
    _txtBillingPOCLastName.layer.borderWidth=1.0;
    
    ///////////
    
    
    _txtCityBillingAddress.enabled = YES;
    
    _txtZipCodeBillingAddress.enabled = YES;
    
    _txtViewAdd1BillingAddress.editable = YES;
    
    _txtViewAdd2BillingAddress.editable = YES;
    
    _txtMapCodeBillingAddress.enabled = YES;
    
    _txtBillingPOCFirstName.enabled = YES;
    
    _txtBillingPOCMiddleName.enabled = YES;
    
    _txtBillingPOCLastName.enabled = YES;
    
    _txtBillingPOCPrimaryEmail.enabled = YES;
    
    _txtBillingPOCSecondaryEmail.enabled = YES;
    
    _txtBillingPOCCellNo.enabled = YES;
    
    _txtBillingPOCPrimaryPhone.enabled = YES;
    
    _txtBillingPOCSecondaryPhone.enabled = YES;
    
    
    
    //ENABLE
    
    
    _txtCityBillingAddress.enabled=YES;
    _txtZipCodeBillingAddress.enabled=YES;
    _txtViewAdd1BillingAddress.editable=YES;
    _txtViewAdd2BillingAddress.editable=YES;
    _txtMapCodeBillingAddress.enabled=YES;
    _txtBillingPOCFirstName.enabled=YES;
    _txtBillingPOCMiddleName.enabled=YES;
    _txtBillingPOCLastName.enabled=YES;
    _txtBillingPOCPrimaryEmail.enabled=YES;
    _txtBillingPOCSecondaryEmail.enabled=YES;
    _txtBillingPOCCellNo.enabled=YES;
    _txtBillingPOCPrimaryPhone.enabled=YES;
    _txtBillingPOCSecondaryPhone.enabled=YES;
    
    
    
    
    //TEMP
    
    [_btnBillingContactPrimaryEmail setHidden:YES];
    [_btnBillingContactSecondaryEmail setHidden:YES];
    [_btnBillingContactCell setHidden:YES];
    [_btnBillingContactPrimaryPhone setHidden:YES];
    [_btnBillingContactSecondaryPhone setHidden:YES];
    
    
    _txtBillingPOCPrimaryEmail.hidden=NO;
    _txtBillingPOCSecondaryEmail.hidden=NO;
    _txtBillingPOCCellNo.hidden=NO;
    _txtBillingPOCPrimaryPhone.hidden=NO;
    _txtBillingPOCSecondaryPhone.hidden=NO;
    
    _txtBillingCounty.enabled=YES;
    _txtBillingSchoolDistrict.enabled=YES;
    
    _txtBillingCounty.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    
    _txtBillingCounty.layer.borderWidth=1.0;
    
    _txtBillingSchoolDistrict.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    
    _txtBillingSchoolDistrict.layer.borderWidth=1.0;
}

-(void)hideBorderColorInSection:(NSInteger)section

{
    
    if(section==1)// customer
        
    {
        
        //_txtLead.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        //_txtLead.layer.borderWidth=1.0;
        
        
        
        _txtFirstName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtFirstName.layer.borderWidth=1.0;
        
        
        
        _txtMiddleName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtMiddleName.layer.borderWidth=1.0;
        
        
        
        _txtLastName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtLastName.layer.borderWidth=1.0;
        
        
        
        
        
        _txtCustomerName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtCustomerName.layer.borderWidth=1.0;
        
        _txtScheduleStartDate.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtScheduleStartDate.layer.borderWidth=1.0;
        
        //_txtAccount.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        //_txtAccount.layer.borderWidth=1.0;
        
        
        
        _txtPrimaryEmail.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtPrimaryEmail.layer.borderWidth=1.0;
        
        _txtCellNo.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtCellNo.layer.borderWidth=1.0;
        
        
        
        _txtPrimaryPhone.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtSecondaryPhone.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtSecondaryPhone.layer.borderWidth=1.0;
        
        
        
        // _txtCompanyName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        //_txtCompanyName.layer.borderWidth=1.0;
        
        
        
        _txtBranchName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBranchName.layer.borderWidth=1.0;
        
        
        
        _btnPriority.enabled=NO;
        _btnSource.enabled=NO;
        _btnPrimaryEmail.enabled=NO;
        _txtLead.enabled=NO;
        _txtFirstName.enabled=NO;
        _txtMiddleName.enabled=NO;
        _txtLastName.enabled=NO;
        _txtCustomerName.enabled=NO;
        _txtScheduleStartDate.enabled=NO;
        _txtAccount.enabled=NO;
        _txtPrimaryEmail.enabled=NO;
        _txtCellNo.enabled=NO;
        _txtPrimaryPhone.enabled=NO;
        _txtSecondaryPhone.enabled=NO;
        _txtCompanyName.enabled=NO;
        _txtBranchName.enabled=NO;
        
        [_btnPriority setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnSource setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnInspector setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        [_btnCellNo setHidden:NO];
        [_btnPrimaryPhone setHidden:NO];
        [_btnSecondaryPhone setHidden:NO];
        _txtCellNo.hidden=YES;
        _txtPrimaryPhone.hidden=YES;
        _txtSecondaryPhone.hidden=YES;
        
        
        _btnPrimaryEmail.hidden=YES;
        _btnPrimaryEmailNew.hidden=NO;
        // _btnPrimaryEmailNew.hidden=YES;
        
        
        _txtLead.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtLead.layer.borderWidth=1.0;
        
        _txtAccount.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtAccount.layer.borderWidth=1.0;
        _txtCompanyName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtCompanyName.layer.borderWidth=1.0;
        
        _txtScheduleStartDate.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtScheduleStartDate.layer.borderWidth=1.0;
        
        _btnLeadTypeInside.enabled=NO;
        _btnLeadTypeField.enabled=NO;
        
        _btnOppTypeCommercial.enabled = NO;
        _btnOppTypeResidential.enabled = NO;
        
        
    }
    
    else if (section==2)// service
        
    {
        
        _txtViewAdd1ServiceAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtViewAdd1ServiceAddress.layer.borderWidth=1.0;
        
        
        
        _txtViewAdd2ServiceAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtViewAdd2ServiceAddress.layer.borderWidth=1.0;
        
        
        
        _txtCityServiceAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtCityServiceAddress.layer.borderWidth=1.0;
        
        
        
        _txtZipcodeServiceAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtZipcodeServiceAddress.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCFirstName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCFirstName.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCLastName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCLastName.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCMiddleName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCMiddleName.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCCellNo.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCCellNo.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCPrimaryEmail.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCPrimaryEmail.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCSecondaryEmail.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCPrimaryPhone.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtServicePOCSecondaryPhone.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtGateCodeServiceNotes.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtMapCodeServiceAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtServicePOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtViewAddressNotesServiceAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        _txtViewAddressNotesServiceAddress.layer.borderWidth=1.0;
        
        _txtViewDirectionServiceAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtViewDirectionServiceAddress.layer.borderWidth=1.0;
        
        
        
        
        //Disable
        
        _txtCityServiceAddress.enabled=NO;
        _txtZipcodeServiceAddress.enabled=NO;
        _txtViewAdd1ServiceAddress.editable=NO;
        _txtViewAdd2ServiceAddress.editable=NO;
        _txtCityServiceAddress.enabled=NO;
        _txtZipcodeServiceAddress.enabled=NO;
        _txtServicePOCFirstName.enabled=NO;
        _txtServicePOCLastName.enabled=NO;
        _txtServicePOCMiddleName.enabled=NO;
        _txtServicePOCCellNo.enabled=NO;
        _txtServicePOCPrimaryEmail.enabled=NO;
        _txtServicePOCPrimaryPhone.enabled=NO;
        _txtServicePOCPrimaryPhone.enabled=NO;
        _txtServicePOCPrimaryPhone.enabled=NO;
        _txtServicePOCPrimaryPhone.enabled=NO;
        _txtServicePOCPrimaryPhone.enabled=NO;
        
        _txtMapCodeServiceAddress.enabled=NO;
        _txtGateCodeServiceNotes.enabled=NO;
        _txtViewAddressNotesServiceAddress.editable=NO;
        _txtViewDirectionServiceAddress.editable=NO;
        
        
        
        //TEMP
        
        [_btnServiceContactPrimaryEmail setHidden:NO];
        [_btnServiceContactSecondaryEmail setHidden:NO];
        [_btnServiceContactCell setHidden:NO];
        [_btnServiceContactPrimaryPhone setHidden:NO];
        [_btnServiceContactSecondaryPhone setHidden:NO];
        
        
        _txtServicePOCPrimaryEmail.hidden=YES;
        _txtServicePOCSecondaryEmail.hidden=YES;
        _txtServicePOCCellNo.hidden=YES;
        _txtServicePOCPrimaryPhone.hidden=YES;
        _txtServicePOCSecondaryPhone.hidden=YES;
        
        
        _txtServiceCounty.enabled=NO;
        _txtServiceSchoolDistrict.enabled=NO;
        
        _txtServiceCounty.layer.borderColor=[[UIColor whiteColor] CGColor];
        _txtServiceCounty.layer.borderWidth=1.0;
        
        _txtServiceSchoolDistrict.layer.borderColor=[[UIColor whiteColor] CGColor];
        _txtServiceSchoolDistrict.layer.borderWidth=1.0;
        
        
        [_btnTaxCode setEnabled:NO];
        [_btnTaxCode setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnPropertyType setEnabled:NO];
        [_btnPropertyType setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        _btnServiceAddressSubType.enabled = NO;
        [_btnServiceAddressSubType setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    
    else if (section==3)// billing
        
    {
        
        _txtCityBillingAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtCityBillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtZipCodeBillingAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtZipCodeBillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtViewAdd1BillingAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtViewAdd1BillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtViewAdd2BillingAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtViewAdd2BillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtMapCodeBillingAddress.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtMapCodeBillingAddress.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCFirstName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBillingPOCFirstName.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCMiddleName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBillingPOCMiddleName.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCLastName.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBillingPOCLastName.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCPrimaryEmail.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBillingPOCPrimaryEmail.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCSecondaryEmail.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBillingPOCSecondaryEmail.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCCellNo.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBillingPOCCellNo.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCPrimaryPhone.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBillingPOCPrimaryPhone.layer.borderWidth=1.0;
        
        
        
        _txtBillingPOCSecondaryPhone.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        _txtBillingPOCSecondaryPhone.layer.borderWidth=1.0;
        
        
        
        //DISABLE
        
        
        _txtCityBillingAddress.enabled=NO;
        _txtZipCodeBillingAddress.enabled=NO;
        _txtViewAdd1BillingAddress.editable=NO;
        _txtViewAdd2BillingAddress.editable=NO;
        _txtMapCodeBillingAddress.enabled=NO;
        _txtBillingPOCFirstName.enabled=NO;
        _txtBillingPOCMiddleName.enabled=NO;
        _txtBillingPOCLastName.enabled=NO;
        _txtBillingPOCPrimaryEmail.enabled=NO;
        _txtBillingPOCSecondaryEmail.enabled=NO;
        _txtBillingPOCCellNo.enabled=NO;
        _txtBillingPOCPrimaryPhone.enabled=NO;
        _txtBillingPOCSecondaryPhone.enabled=NO;
        
        
        
        
        //TEMP
        
        [_btnBillingContactPrimaryEmail setHidden:NO];
        [_btnBillingContactSecondaryEmail setHidden:NO];
        [_btnBillingContactCell setHidden:NO];
        [_btnBillingContactPrimaryPhone setHidden:NO];
        [_btnBillingContactSecondaryPhone setHidden:NO];
        
        
        _txtBillingPOCPrimaryEmail.hidden=YES;
        _txtBillingPOCSecondaryEmail.hidden=YES;
        _txtBillingPOCCellNo.hidden=YES;
        _txtBillingPOCPrimaryPhone.hidden=YES;
        _txtBillingPOCSecondaryPhone.hidden=YES;
        
        _txtBillingCounty.enabled=NO;
        _txtBillingSchoolDistrict.enabled=NO;
        
        _txtBillingCounty.layer.borderColor=[[UIColor whiteColor] CGColor];
        _txtBillingCounty.layer.borderWidth=1.0;
        
        _txtBillingSchoolDistrict.layer.borderColor=[[UIColor whiteColor] CGColor];
        _txtBillingSchoolDistrict.layer.borderWidth=1.0;
    }
    
}

- (IBAction)actionOnCellNo:(id)sender
{
    [self endEditing];
    if (_txtCellNo.text.length>0)
    {
        [global calling:_txtCellNo.text];
    }
}

- (IBAction)actionCallPrimaryPhone:(id)sender
{
    [self endEditing];
    if (_txtPrimaryPhone.text.length>0)
    {
        [global calling:_txtPrimaryPhone.text];
    }
}

- (IBAction)actionCallSecondaryPhone:(id)sender
{
    [self endEditing];
    if (_txtSecondaryPhone.text.length>0)
    {
        [global calling:_txtSecondaryPhone.text];
        
    }
}

//Serivce POC
- (IBAction)actionServiceContactPrimaryEmail:(id)sender
{
    [self endEditing];
    if ((_txtServicePOCPrimaryEmail.text.length>0)&&[self NSStringIsValidEmail:_txtServicePOCPrimaryEmail.text]==NO)
    {
        [global AlertMethod:@"Alert!" :@"Please enter valid Service POC Primary Email address"];
    }
    else
    {
        if (_txtServicePOCPrimaryEmail.text.length>0)
        {
            if ([MFMailComposeViewController canSendMail])
            {
                [global emailComposer:_txtServicePOCPrimaryEmail.text :@"" :@"" :self];
            }
            else
            {
                [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
                
                
            }
            
        }
        else
        {
            
        }
        
    }
    
    
}
- (IBAction)actionOnServiceContactSecondaryEmail:(id)sender
{
    [self endEditing];
    if ((_txtServicePOCSecondaryEmail.text.length>0)&&[self NSStringIsValidEmail:_txtServicePOCSecondaryEmail.text]==NO)
    {
        [global AlertMethod:@"Alert!" :@"Please enter valid Service POC Primary Email address"];
    }
    else
    {
        if (_txtServicePOCSecondaryEmail.text.length>0)
        {
            
            if ([MFMailComposeViewController canSendMail])
            {
                [global emailComposer:_txtServicePOCSecondaryEmail.text :@"" :@"" :self];
            }
            else
            {
                [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
                
                
            }
        }
        else
        {
            
        }
        
    }
    
    
}
- (IBAction)actionOnServiceContactCell:(id)sender
{
    [self endEditing];
    if (_txtServicePOCCellNo.text.length>0)
    {
        [global calling:_txtServicePOCCellNo.text];
        
    }
    
}
- (IBAction)actionOnServiceContactPrimaryPhone:(id)sender
{
    [self endEditing];
    if (_txtServicePOCPrimaryPhone.text.length>0)
    {
        [global calling:_txtServicePOCPrimaryPhone.text];
        
    }
    
}
- (IBAction)actionOnServiceContactSecondaryPhone:(id)sender
{
    [self endEditing];
    if (_txtServicePOCSecondaryPhone.text.length>0)
    {
        [global calling:_txtServicePOCSecondaryPhone.text];
        
    }
    
}

//Billing POC

- (IBAction)actionBillingContactPrimaryEmail:(id)sender
{
    [self endEditing];
    if ((_txtBillingPOCPrimaryEmail.text.length>0)&&[self NSStringIsValidEmail:_txtBillingPOCPrimaryEmail.text]==NO)
    {
        [global AlertMethod:@"Alert!" :@"Please enter valid Service POC Primary Email address"];
    }
    else
    {
        if (_txtBillingPOCPrimaryEmail.text.length>0)
        {
            if ([MFMailComposeViewController canSendMail])
            {
                [global emailComposer:_txtBillingPOCPrimaryEmail.text :@"" :@"" :self];
            }
            else
            {
                [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
                
                
            }
        }
        else
        {
            
        }
    }
    
    
}
- (IBAction)actionOnBillingContactSecondaryEmail:(id)sender
{
    [self endEditing];
    if ((_txtBillingPOCSecondaryEmail.text.length>0)&&[self NSStringIsValidEmail:_txtBillingPOCSecondaryEmail.text]==NO)
    {
        [global AlertMethod:@"Alert!" :@"Please enter valid Service POC Primary Email address"];
    }
    else
    {
        if (_txtBillingPOCSecondaryEmail.text.length>0)
        {
            if ([MFMailComposeViewController canSendMail])
            {
                [global emailComposer:_txtBillingPOCSecondaryEmail.text :@"" :@"" :self];
            }
            else
            {
                [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
                
                
            }
            
        }
        else
        {
            
        }
        
    }
    
}
- (IBAction)actionOnBillingContactCell:(id)sender
{
    [self endEditing];
    if (_txtBillingPOCCellNo.text.length>0)
    {
        [global calling:_txtBillingPOCCellNo.text];
        
    }
    
    
}
- (IBAction)actionOnBillingContactPrimaryPhone:(id)sender
{
   [self endEditing];
    if (_txtBillingPOCPrimaryPhone.text.length>0)
    {
        [global calling:_txtBillingPOCPrimaryPhone.text];
        
    }
    
}
- (IBAction)actionOnBillingContactSecondaryPhone:(id)sender
{
    [self endEditing];
    if (_txtBillingPOCSecondaryPhone.text.length>0)
    {
        [global calling:_txtBillingPOCSecondaryPhone.text];
        
    }
    
}
- (IBAction)actionOnBillingAddress:(id)sender
{
    [self endEditing];
    if (_lblNewBillingAddress.text>0)
    {
        [global redirectOnAppleMap:self :_lblNewBillingAddress.text];
        
        
    }
}
- (IBAction)actionOnPrimaryEmailNew:(id)sender
{
    [self endEditing];
    NSString *strPrimaryEmailNew=_btnPrimaryEmail.titleLabel.text;
    
    //NSArray *arrEmailTemp = [self getSplit:strPrimaryEmailNew];
    //strPrimaryEmailNew = [arrEmailTemp componentsJoinedByString:@","];
    
    if (strPrimaryEmailNew.length==0)
    {
        [global AlertMethod:@"Alert!" :@"NO email address found"];
    }
    else
    {
        
        if ([MFMailComposeViewController canSendMail])
        {
            [global emailComposer:strPrimaryEmailNew :@"" :@"" :self];
        }
        else
        {
            [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
            
            
        }
        
        
        
    }
    
}
-(NSAttributedString*)getUnderLineAttributedString:(NSString*)strText
{
    
    if(![strText isKindOfClass:[NSString class]])
    {
        strText=@"";
    }
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:strText];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    
    return attributeString;
    
}
-(void)setUnderLineText
{
    
    //View Customer Info
    
    [_btnCellNo setAttributedTitle:[self getUnderLineAttributedString:_txtCellNo.text] forState:UIControlStateNormal];
    [_btnPrimaryPhone setAttributedTitle:[self getUnderLineAttributedString:_txtPrimaryPhone.text] forState:UIControlStateNormal];
    [_btnSecondaryPhone setAttributedTitle:[self getUnderLineAttributedString:_txtSecondaryPhone.text] forState:UIControlStateNormal];
    
    //if (_btnPrimaryEmail.titleLabel.text.length>0)
    //{
    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
    // [_btnPrimaryEmailNew setAttributedTitle:[self getUnderLineAttributedString:_btnPrimaryEmail.titleLabel.text] forState:UIControlStateNormal];
    [_btnPrimaryEmailNew setAttributedTitle:[self getUnderLineAttributedString:strMessage] forState:UIControlStateNormal];
    
    //}
    //Service Address
    
    _lblNewServiceAddress.attributedText=[self getUnderLineAttributedString:_lblNewServiceAddress.text];
    [_btnServiceContactPrimaryEmail setAttributedTitle:[self getUnderLineAttributedString:_txtServicePOCPrimaryEmail.text] forState:UIControlStateNormal];
    [_btnServiceContactSecondaryEmail setAttributedTitle:[self getUnderLineAttributedString:_txtServicePOCSecondaryEmail.text] forState:UIControlStateNormal];
    [_btnServiceContactCell setAttributedTitle:[self getUnderLineAttributedString:_txtServicePOCCellNo.text] forState:UIControlStateNormal];
    [_btnServiceContactPrimaryPhone setAttributedTitle:[self getUnderLineAttributedString:_txtServicePOCPrimaryPhone.text] forState:UIControlStateNormal];
    [_btnServiceContactSecondaryPhone setAttributedTitle:[self getUnderLineAttributedString:_txtServicePOCSecondaryPhone.text] forState:UIControlStateNormal];
    
    //BIlling Address
    
    _lblNewBillingAddress.attributedText=[self getUnderLineAttributedString:_lblNewBillingAddress.text];
    
    [_btnBillingContactPrimaryEmail setAttributedTitle:[self getUnderLineAttributedString:_txtBillingPOCPrimaryEmail.text] forState:UIControlStateNormal];
    [_btnBillingContactSecondaryEmail setAttributedTitle:[self getUnderLineAttributedString:_txtBillingPOCSecondaryEmail.text] forState:UIControlStateNormal];
    [_btnBillingContactCell setAttributedTitle:[self getUnderLineAttributedString:_txtBillingPOCCellNo.text] forState:UIControlStateNormal];
    [_btnBillingContactPrimaryPhone setAttributedTitle:[self getUnderLineAttributedString:_txtBillingPOCPrimaryPhone.text] forState:UIControlStateNormal];
    [_btnBillingContactSecondaryPhone setAttributedTitle:[self getUnderLineAttributedString:_txtBillingPOCSecondaryPhone.text] forState:UIControlStateNormal];
    
}
-(void)setServiceAddressData
{
    //POC
    [_btnServiceContactPrimaryEmail setTitle:_txtServicePOCPrimaryEmail.text forState:UIControlStateNormal];
    [_btnServiceContactSecondaryEmail setTitle:_txtServicePOCSecondaryEmail.text forState:UIControlStateNormal];
    [_btnServiceContactCell setTitle:_txtServicePOCCellNo.text forState:UIControlStateNormal];
    [_btnServiceContactPrimaryPhone setTitle:_txtServicePOCPrimaryPhone.text forState:UIControlStateNormal];
    [_btnServiceContactSecondaryPhone setTitle:_txtServicePOCSecondaryPhone.text forState:UIControlStateNormal];
    
    //END
    
    NSString *strServiceAddress = @"";
    if(_txtViewAdd1ServiceAddress.text.length>0)
    {
        _txtViewAdd1ServiceAddress.text=[_txtViewAdd1ServiceAddress.text stringByTrimmingCharactersInSet:
                                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        strServiceAddress = [NSString stringWithFormat:@"%@",_txtViewAdd1ServiceAddress.text ];
        
    }
    if(_txtViewAdd2ServiceAddress.text.length>0)
    {
        _txtViewAdd2ServiceAddress.text=[_txtViewAdd2ServiceAddress.text stringByTrimmingCharactersInSet:
                                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtViewAdd2ServiceAddress.text ];
    }
    
    if(_txtCityServiceAddress.text.length>0)
    {
        
        _txtCityServiceAddress.text=[_txtCityServiceAddress.text stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtCityServiceAddress.text ];
    }
    
    if(_btnStateServiceAddress.titleLabel.text.length>0&& ![_btnStateServiceAddress.titleLabel.text isEqualToString:@"State"]&&![_btnStateServiceAddress.titleLabel.text isEqualToString:@"(null)"])
    {
       // strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_btnStateServiceAddress.titleLabel.text];
    }
    
    if(_btnStateServiceAddress.currentTitle.length>0&& ![_btnStateServiceAddress.currentTitle isEqualToString:@"State"]&&![_btnStateServiceAddress.currentTitle isEqualToString:@"(null)"])
    {
        strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_btnStateServiceAddress.currentTitle];
    }
    
    
    if(_txtZipcodeServiceAddress.text.length>0)
    {
        _txtZipcodeServiceAddress.text=[_txtZipcodeServiceAddress.text stringByTrimmingCharactersInSet:
                                        [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtZipcodeServiceAddress.text ];
    }
    _lblNewServiceAddress.text=[NSString stringWithFormat:@"%@",strServiceAddress];
    
    
    _lblNewServiceAddress.attributedText=[self getUnderLineAttributedString:_lblNewServiceAddress.text];
    //For Service Poc Name
    
    NSString *strServicePOCName=@"";
    if(_txtServicePOCFirstName.text.length>0)
    {
        _txtServicePOCFirstName.text=[_txtServicePOCFirstName.text stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        strServicePOCName= [NSString stringWithFormat:@"%@",[_txtServicePOCFirstName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    if (_txtServicePOCMiddleName.text.length>0)
    {
        
        _txtServicePOCMiddleName.text=[_txtServicePOCMiddleName.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        strServicePOCName= [NSString stringWithFormat:@"%@ %@",strServicePOCName,[_txtServicePOCMiddleName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        
    }
    if (_txtServicePOCLastName.text.length>0)
    {
        
        _txtServicePOCLastName.text=[_txtServicePOCLastName.text stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strServicePOCName= [NSString stringWithFormat:@"%@ %@",strServicePOCName,[_txtServicePOCLastName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    _lblServiceContactFullName.text=[NSString stringWithFormat:@"%@",strServicePOCName];
    
}
-(void)setBillingAddressData
{
    //POC
    
    [_btnBillingContactPrimaryEmail setTitle:_txtBillingPOCPrimaryEmail.text forState:UIControlStateNormal];
    [_btnBillingContactSecondaryEmail setTitle:_txtBillingPOCSecondaryEmail.text forState:UIControlStateNormal];
    [_btnBillingContactCell setTitle:_txtBillingPOCCellNo.text forState:UIControlStateNormal];
    [_btnBillingContactPrimaryPhone setTitle:_txtBillingPOCPrimaryPhone.text forState:UIControlStateNormal];
    [_btnBillingContactSecondaryPhone setTitle:_txtBillingPOCSecondaryPhone.text forState:UIControlStateNormal];
    
    //End
    
    NSString *strBillingAddress = @"";
    if(_txtViewAdd1BillingAddress.text.length>0)
    {
        _txtViewAdd1BillingAddress.text=[_txtViewAdd1BillingAddress.text stringByTrimmingCharactersInSet:
                                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strBillingAddress = [NSString stringWithFormat:@"%@",_txtViewAdd1BillingAddress.text ];
    }
    if(_txtViewAdd2BillingAddress.text.length>0)
    {
        _txtViewAdd2BillingAddress.text=[_txtViewAdd2BillingAddress.text stringByTrimmingCharactersInSet:
                                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strBillingAddress = [NSString stringWithFormat:@"%@, %@",strBillingAddress,_txtViewAdd2BillingAddress.text ];
    }
    
    if(_txtCityBillingAddress.text.length>0)
    {
        
        _txtCityBillingAddress.text=[_txtCityBillingAddress.text stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strBillingAddress = [NSString stringWithFormat:@"%@, %@",strBillingAddress,_txtCityBillingAddress.text ];
    }
    
    if(_btnStateBillingAddress.titleLabel.text.length>0&& ![_btnStateBillingAddress.titleLabel.text isEqualToString:@"State"]&&![_btnStateBillingAddress.titleLabel.text isEqualToString:@"(null)"])
    {
       // strBillingAddress = [NSString stringWithFormat:@"%@, %@",strBillingAddress,_btnStateBillingAddress.titleLabel.text];
    }
    
    if(_btnStateBillingAddress.currentTitle.length>0&& ![_btnStateBillingAddress.currentTitle isEqualToString:@"State"]&&![_btnStateBillingAddress.currentTitle isEqualToString:@"(null)"])
    {
        strBillingAddress = [NSString stringWithFormat:@"%@, %@",strBillingAddress,_btnStateBillingAddress.currentTitle];
    }
    
    
    if(_txtZipCodeBillingAddress.text.length>0)
    {
        _txtZipCodeBillingAddress.text=[_txtZipCodeBillingAddress.text stringByTrimmingCharactersInSet:
                                        [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        strBillingAddress = [NSString stringWithFormat:@"%@, %@",strBillingAddress,_txtZipCodeBillingAddress.text ];
    }
    _lblNewBillingAddress.text=[NSString stringWithFormat:@"%@",strBillingAddress];
    _lblNewBillingAddress.attributedText=[self getUnderLineAttributedString:_lblNewBillingAddress.text];
    
    //Billing POC
    NSString *strBillingPOCName=@"";
    if(_txtBillingPOCFirstName.text.length>0)
    {
        _txtBillingPOCFirstName.text=[_txtBillingPOCFirstName.text stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strBillingPOCName= [NSString stringWithFormat:@"%@",[_txtBillingPOCFirstName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    if (_txtBillingPOCMiddleName.text.length>0)
    {
        _txtBillingPOCMiddleName.text=[_txtBillingPOCMiddleName.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strBillingPOCName= [NSString stringWithFormat:@"%@ %@",strBillingPOCName,[_txtBillingPOCMiddleName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        
    }
    if (_txtBillingPOCLastName.text.length>0)
    {
        _txtBillingPOCLastName.text=[_txtBillingPOCLastName.text stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        strBillingPOCName= [NSString stringWithFormat:@"%@ %@",strBillingPOCName,[_txtBillingPOCLastName.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    _lblBillingContactFullName.text=[NSString stringWithFormat:@"%@",strBillingPOCName];
}
- (IBAction)actiononLeadTypeInside:(id)sender
{
    isEditedInSalesAuto=YES;
    strLeadType=@"inside";
    [_btnLeadTypeInside setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnLeadTypeField setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}
- (IBAction)actiononLeadTypeField:(id)sender
{
    isEditedInSalesAuto=YES;
    strLeadType=@"field";
    [_btnLeadTypeField setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    [_btnLeadTypeInside setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}
- (IBAction)actionOnChemicalSensitivityList:(id)sender
{
    [self endEditing];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        [self updateLeadIdDetail];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                 bundle: nil];
        ChemicalSensitivityList
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChemicalSensitivityList"];
        objByProductVC.strFrom=@"sales";
        objByProductVC.strId=strLeadId;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
    //End
    
}
-(NSString *)calculateTurfArea:(NSString *)strLotSize Area:(NSString *)strArea Story:(NSString *)strNoOfStory ShrubArea:(NSString*)strShrubArea
{
    if([strLotSize isEqualToString:@""] || [strLotSize isEqual:nil] || strLotSize.length == 0)
    {
        return @"";
    }
    else
    {
        float turfArea;
        turfArea=0;
        NSString *strStoryMultiplier;
        strStoryMultiplier=@"0";
        NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictSalesLeadMasterForTurf=[defs2 valueForKey:@"MasterSalesAutomation"];
        if ([dictSalesLeadMasterForTurf isKindOfClass:[NSDictionary class]])
        {
            NSArray *arrTurfAreaStoryMultiplayer=[dictSalesLeadMasterForTurf valueForKey:@"TurfAreaStoryMultiplier"];
            for(int i=0;i<arrTurfAreaStoryMultiplayer.count;i++)
            {
                NSDictionary *dict=[arrTurfAreaStoryMultiplayer objectAtIndex:i];
                
                if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"NoOfStory"]]isEqualToString:strNoOfStory])
                {
                    strStoryMultiplier=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Multiplier"]];
                    break;
                }
            }
            
        }
        
        
        turfArea=[strLotSize floatValue]-(([strArea floatValue]*[strStoryMultiplier floatValue])-[strShrubArea floatValue]);
        
        if(turfArea<0)
        {
            turfArea=00.00;
        }
        
        return [NSString stringWithFormat:@"%.2f",turfArea];
    }
}

- (IBAction)actionOnTaxCode:(id)sender
{
    [self endEditing];
    [self getTaxCode];
    
    if (arrTaxCode.count==0)
    {
        [global AlertMethod:@"Alert!" :@"No Tax code available"];
    }
    else
    {
        tblData.tag=111;
        [self tableLoad:tblData.tag];
    }
}
-(void)getTaxCode
{
    arrTaxCode=[[NSMutableArray alloc]init];
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMasterForTurf=[defs2 valueForKey:@"MasterSalesAutomation"];
    if ([dictSalesLeadMasterForTurf isKindOfClass:[NSDictionary class]])
    {
        arrTaxCode=[dictSalesLeadMasterForTurf valueForKey:@"TaxMaster"];
    }
}

-(void)getPropertyType
{
    arrPropertyType=[[NSMutableArray alloc]init];
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMasterForTurf=[defs2 valueForKey:@"MasterSalesAutomation"];
    NSMutableArray *arrPropertyTemp;
    arrPropertyTemp = [[NSMutableArray alloc]init];
    if ([dictSalesLeadMasterForTurf isKindOfClass:[NSDictionary class]])
    {
        
        arrPropertyTemp=[dictSalesLeadMasterForTurf valueForKey:@"AddressPropertyTypeMaster"];
    }
    for (int i=0; i<arrPropertyTemp.count; i++)
    {
        NSDictionary *dict = [arrPropertyTemp objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]]isEqualToString:@"1"])
        {
            [arrPropertyType addObject:dict];
        }
    }
    
}
-(void)setPropertyType
{
    [self getPropertyType];
    
    strProprtyTypeSysName = [NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceAddressPropertyTypeSysName"]];
    strSelectedProprtyTypeSysName = [NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"serviceAddressPropertyTypeSysName"]];
    
    
    for (int i=0; i<arrPropertyType.count; i++)
    {
        NSDictionary *dict=[arrPropertyType objectAtIndex:i];
        if ([strProprtyTypeSysName isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]])
        {
            [_btnPropertyType setTitle:[dict valueForKey:@"Name"] forState:UIControlStateNormal];
            break;
        }
    }
    if(_btnPropertyType.titleLabel.text.length==0|| [_btnPropertyType.titleLabel.text isEqualToString:@"(null)"]||[_btnPropertyType.titleLabel.text isEqualToString:@"Property Type"])
    {
        [ _btnPropertyType setTitle:@"Property Type" forState:UIControlStateNormal];
    }
}
- (IBAction)actionOnProprtyType:(id)sender
{
    if (arrPropertyType.count==0)
    {
        [global AlertMethod:@"Alert!" :@"No property available"];
    }
    else
    {
        tblData.tag=112;
        [self tableLoad:tblData.tag];
    }
}
-(NSArray *)getSplit:(NSString*)test
{
    //NSString *test=@"arr1,arr2:arr3:arr4;arr5,arr6";
    NSString *sep = @",;:";
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:sep];
    NSArray *tempArr=[test componentsSeparatedByCharactersInSet:set];
    NSLog(@"Temp Array =%@",tempArr);
    return tempArr;
}
-(void)trimTextField
{
    
    _txtServicePOCPrimaryEmail.text= [_txtServicePOCPrimaryEmail.text stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtServicePOCSecondaryEmail.text= [_txtServicePOCSecondaryEmail.text stringByTrimmingCharactersInSet:
                                        [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtBillingPOCPrimaryEmail.text= [_txtBillingPOCPrimaryEmail.text stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _txtBillingPOCSecondaryEmail.text= [_txtBillingPOCSecondaryEmail.text stringByTrimmingCharactersInSet:
                                        [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
-(void)forDisable
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    strStatusss=[NSString stringWithFormat:@"%@",[defs valueForKey:@"leadStatusSales"]];
    strLeadStatusGlobal = strStatusss;
    NSUserDefaults *dfsStageStatus=[NSUserDefaults standardUserDefaults];
    if ([dfsStageStatus boolForKey:@"backFromInspectionNew"] == YES)
    {
        strStatusss= [dfsStageStatus valueForKey:@"leadStatusSales"];
        strStageSysName=[dfsStageStatus valueForKey:@"stageSysNameSales"];
    }
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        //Nilind 08 Feb
//        _txtAreaSqFt.inputView = [[UIView alloc] init];
//        _txtAreaSqFt.delegate = self;
//
//        _txtLotSizeSqFt.inputView = [[UIView alloc] init];
//        _txtLotSizeSqFt.delegate = self;
//
//        _txtBathroom.inputView = [[UIView alloc] init];
//        _txtBathroom.delegate = self;
//
//        _txtBedroom.inputView = [[UIView alloc] init];
//        _txtBedroom.delegate = self;
//
//        _txtYearBuil.inputView = [[UIView alloc] init];
//        _txtYearBuil.delegate = self;
        
        //End
        
        //Nilind 10 Nov
        _btnPriority.enabled=NO;
        _btnSource.enabled=NO;
        _btnInspector.enabled=NO;
        _btnGlobalSync.enabled=NO;
        _txtScheduleStartDate.inputView = [[UIView alloc] init];
        _txtScheduleStartDate.delegate = self;
        
        
//        _txtFirstName.inputView=[[UIView alloc]init];
//        _txtFirstName.delegate = self;
//        
//        _txtLastName.inputView=[[UIView alloc]init];
//        _txtLastName.delegate = self;
//        
//        
//        _txtMiddleName.inputView=[[UIView alloc]init];
//        _txtMiddleName.delegate = self;
//        
//        
//        _txtLastName.inputView=[[UIView alloc]init];
//        _txtLastName.delegate = self;
//        
//        _txtPrimaryEmail.inputView=[[UIView alloc]init];
//        _txtPrimaryEmail.delegate = self;
//        
//        _txtCellNo.inputView=[[UIView alloc]init];
//        _txtCellNo.delegate = self;
//        
//        _txtPrimaryPhone.inputView=[[UIView alloc]init];
//        _txtPrimaryPhone.delegate = self;
//        
//        _txtSecondaryPhone.inputView=[[UIView alloc]init];
//        _txtSecondaryPhone.delegate = self;
        
        _txtViewAdd1ServiceAddress.editable=NO;
        _txtViewAdd2ServiceAddress.editable=NO;
        _btnCountryServiceAddress.enabled=NO;
        _btnStateServiceAddress.enabled=NO;
        _txtCityServiceAddress.enabled=NO;
        _txtZipcodeServiceAddress.enabled=NO;
        
        _btnCheckBox.enabled=NO;
        
        _txtViewAdd1BillingAddress.editable=NO;
        // _txtViewAdd1BillingAddress.delegate = self;
        
        _txtViewAdd2BillingAddress.editable=NO;
        _btnCountryBillingAddress.enabled=NO;
        _btnStateBillingAddress.enabled=NO;
        _txtCityBillingAddress.enabled=NO;
        _txtZipCodeBillingAddress.enabled=NO;
        
        _txtViewNotes.editable=NO;
        _txtViewDescription.editable=YES;
        _txtTagServiceRequired.enabled=YES;
        
        _btnAddImages.enabled=NO;
        _btnAddGraph.enabled=NO;
        
       // _btnAddImageFooter.enabled=NO;
       // _btnAddGraphImageFooter.enabled=NO;
        _btnUploadDocument.enabled=NO;
        
        _btnGetPropertyInfo.enabled=NO;
        
        //..................................................
        
        NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
        [dfsStatus setBool:YES forKey:@"status"];
        [dfsStatus synchronize];
        
        
        
        //Billing POC
        [self textMethodForDisable:_txtBillingPOCFirstName];
        [self textMethodForDisable:_txtBillingPOCLastName];
        [self textMethodForDisable:_txtBillingPOCMiddleName];
        [self textMethodForDisable:_txtBillingPOCCellNo];
        [self textMethodForDisable:_txtBillingPOCPrimaryEmail];
        [self textMethodForDisable:_txtBillingPOCSecondaryEmail];
        [self textMethodForDisable:_txtBillingPOCPrimaryPhone];
        [self textMethodForDisable:_txtBillingPOCSecondaryPhone];
        
        //Service POC
        [self textMethodForDisable:_txtServicePOCFirstName];
        [self textMethodForDisable:_txtServicePOCLastName];
        [self textMethodForDisable:_txtServicePOCMiddleName];
        [self textMethodForDisable:_txtServicePOCCellNo];
        [self textMethodForDisable:_txtServicePOCPrimaryEmail];
        [self textMethodForDisable:_txtServicePOCSecondaryEmail];
        [self textMethodForDisable:_txtServicePOCPrimaryPhone];
        [self textMethodForDisable:_txtServicePOCSecondaryPhone];
        
        _txtViewProblemescription.editable=YES;
        _btnLeadTypeInside.enabled=NO;
        _btnLeadTypeField.enabled=NO;
        
        [self textMethodForDisable:_txtNoOfStory];
        [self textMethodForDisable:_txtShrubArea];
        [self textMethodForDisable:_txtTurfArea];
        
        
        [_btnTaxCode setEnabled:NO];
        
        _btnOppTypeCommercial.enabled = NO;
        _btnOppTypeResidential.enabled = NO;
        
        //btn rest status hidden false
        [_btnReopen setHidden:NO];
        
        _btnServiceAddressSubType.enabled = NO;
    }
    else
    {
        NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
        [dfsStatus setBool:NO forKey:@"status"];
        [dfsStatus synchronize];
        
        //btn rest status hidden true
        [_btnReopen setHidden:YES];

        
    }
    
    _btnOppTypeCommercial.enabled = NO;
    _btnOppTypeResidential.enabled = NO;
    
    /* [self calculate:@"4" :@"5" withCompltionHandler:^(NSString *result) {
     
     NSLog(@"%@",result);
     }];*/
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        _btnCheckBox.enabled = NO;
        _txtViewNotes.editable = NO;
        _txtViewAccountDesc.editable = NO;
        _txtViewDescription.editable=NO;
        _txtViewProblemescription.editable=NO;
        
        [self textMethodForDisable:_txtFirstName];
        [self textMethodForDisable:_txtMiddleName];
        [self textMethodForDisable:_txtLastName];
        [self textMethodForDisable:_txtPrimaryEmail];
        [self textMethodForDisable:_txtCellNo];
        [self textMethodForDisable:_txtPrimaryPhone];
        [self textMethodForDisable:_txtSecondaryPhone];
        [self textMethodForDisable:_txtNoOfStory];
        [self textMethodForDisable:_txtShrubArea];
        [self textMethodForDisable:_txtTurfArea];
    }
    else
    {
        _btnCheckBox.enabled = YES;
        _txtViewNotes.editable = YES;
        _txtViewAccountDesc.editable = YES;
        _txtViewDescription.editable=YES;
        _txtViewProblemescription.editable=YES;
    }
    
}
-(NSString *)calculateLinerSquareFt:(NSString *)strArea Story:(NSString *)strNoOfStory
{
    float lineraSqFtValue;
    lineraSqFtValue=0;
    NSString *strStoryMultiplier;
    strStoryMultiplier=@"0";
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMaster=[defs2 valueForKey:@"MasterSalesAutomation"];
    
    if ([dictSalesLeadMaster isKindOfClass:[NSDictionary class]])
    {
        NSArray *arrLinearSqFtMaster=[dictSalesLeadMaster valueForKey:@"LinearSqFtMasterExtServiceDc"];
        
        if ([arrLinearSqFtMaster isKindOfClass:[NSArray class]])
        {
            
            for(int i=0;i<arrLinearSqFtMaster.count;i++)
            {
                NSDictionary *dict=[arrLinearSqFtMaster objectAtIndex:i];
                
                float areaFrom = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"AreaFrom"]]floatValue];
                float areaTo = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"AreaTo"]]floatValue];
                
                
                //if (areaFrom <= [strArea floatValue] <= areaTo)
                if ((areaFrom <= [strArea floatValue]) && [strArea floatValue] <= areaTo)
                {
                    if ([strNoOfStory isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"NoOfStory"]]])
                    {
                        lineraSqFtValue = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"LinearSqFt"]]floatValue];
                        break;
                    }
                }
                
                
            }
            
        }
        
    }
    
    
    if (lineraSqFtValue > 0)
    {
        
    }
    else
    {
        lineraSqFtValue = [_txtLinearSqFt.text floatValue];
    }
    
    
    if(lineraSqFtValue<0)
    {
        //lineraSqFtValue=00.00;
    }
    
    return [NSString stringWithFormat:@"%.2f",lineraSqFtValue];
}

#pragma mark- -------------- Customer Contact Core Data -----------

-(void)fetchCustomerContact
{
    NSString *strTemPrimaryPhone= _txtPrimaryPhone.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"primaryPhone"]];
    NSString *strTemSecondaryPhone=_txtSecondaryPhone.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"secondaryPhone"]];
    NSString *strTemCellPhone=_txtCellNo.text;//[NSString stringWithFormat:@"%@",[matchesGeneralInfo valueForKey:@"cellNo"]];
    //strTemPrimaryPhone = @"123456,123456,897978988";
    //strTemSecondaryPhone = @"123456,54321,897978988";
    // strTemCellPhone = @"123456,54321,897978988";
    
    NSArray *arrTempPrimaryPhone,*arrTempSecondaryPhone,*arrTempellPhone;
    if (!(strTemPrimaryPhone.length==0) &&!([strTemPrimaryPhone isEqualToString:@"(null)"]))
    {
        arrTempPrimaryPhone=[strTemPrimaryPhone componentsSeparatedByString:@","];
    }
    if (!(strTemSecondaryPhone.length==0) &&!([strTemSecondaryPhone isEqualToString:@"(null)"]))
    {
        arrTempSecondaryPhone=[strTemSecondaryPhone componentsSeparatedByString:@","];
    }
    if (!(strTemCellPhone.length==0) &&!([strTemCellPhone isEqualToString:@"(null)"]))
    {
        arrTempellPhone=[strTemCellPhone componentsSeparatedByString:@","];
    }
    NSMutableArray *arrTempContactNo = [[NSMutableArray alloc]init];
    [arrTempContactNo addObjectsFromArray:arrTempPrimaryPhone];
    [arrTempContactNo addObjectsFromArray:arrTempSecondaryPhone];
    [arrTempContactNo addObjectsFromArray:arrTempellPhone];
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrTempContactNo];
    NSArray *arrTempContactNoWithoutDuplicates = [orderedSet array];
    NSLog(@"%@",arrTempContactNoWithoutDuplicates);
    
    for (int i=0; i<arrTempContactNoWithoutDuplicates.count; i++)
    {
        NSString *strNo = [NSString stringWithFormat:@"%@",[arrTempContactNoWithoutDuplicates objectAtIndex:i]];
        BOOL chk = [self fetchCustomerContactNoExist:strNo];
        if (chk == NO)
        {
            [self saveContactNumberToCoreData:strNo];
            
        }
    }
}


-(BOOL)fetchCustomerContactNoExist:(NSString *)strContactNo
{
    BOOL chkExistence;
    chkExistence = NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityContactNumberDetail= [NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context];
    [request setEntity:entityContactNumberDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjContact = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjContact.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjContact.count; k++)
        {
            NSManagedObject *matchesContact;
            
            matchesContact=arrAllObjContact[k];
            
            if ([[NSString stringWithFormat:@"%@",[matchesContact valueForKey:@"contactNumber"]] isEqualToString:strContactNo])
            {
                
                chkExistence = YES;
                break;
            }
            
        }
    }
    
    return chkExistence;
}

-(void)saveContactNumberToCoreData:(NSString*)strContactNo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityContactNumberDetail=[NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context];
    
    LeadContactDetailExtSerDc *objEmailDetail = [[LeadContactDetailExtSerDc alloc]initWithEntity:entityContactNumberDetail insertIntoManagedObjectContext:context];
    
    objEmailDetail.createdBy=strUserName;
    
    objEmailDetail.createdDate=[global getCurrentDate];
    
    objEmailDetail.contactNumber=strContactNo;
    
    objEmailDetail.leadContactId = @"";
    
    objEmailDetail.isDefaultNumber=@"true";
    
    objEmailDetail.isMessageSent=@"true";
    
    objEmailDetail.isCustomerNumber=@"true";
    
    objEmailDetail.modifiedBy=@"";
    
    objEmailDetail.modifiedDate=[global modifyDate];
    
    objEmailDetail.subject=@"";
    objEmailDetail.leadId=strLeadId;
    objEmailDetail.userName=strUserName;
    objEmailDetail.companyKey=strCompanyKey;
    
    NSError *error1;
    [context save:&error1];
}
-(void)stringLineDemo
{
    NSString *str1 = @"Hello";
    NSString *str2 = @"Shyam";
    NSString *str3 = @"Sharma";
    NSString *strNew = [NSString stringWithFormat:@"%@\n%@\n%@",str1,str2,str3];
    NSLog(@"%@",strNew);
    
}
-(void)endEditing
{
    [self.view endEditing:YES];
}
-(void)goToGlobalmage:(NSString *)strType
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"PestiPhone" bundle:nil];
    GlobalImageVC *objGlobalImageVC=[storyBoard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objGlobalImageVC.strHeaderTitle = strType;
    objGlobalImageVC.strWoId = strLeadId;
    objGlobalImageVC.strWoLeadId = strLeadId;
    objGlobalImageVC.strModuleType = @"SalesFlow";
    
    if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        
        objGlobalImageVC.strWoStatus = @"Complete";
        
    }
    else
    {
        objGlobalImageVC.strWoStatus = @"InComplete";
        
    }
    [self.navigationController presentViewController:objGlobalImageVC animated:NO completion:nil];
    
}
-(void)attachImage
{
    [self endEditing];
   // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* addImage = [UIAlertAction actionWithTitle:@"Add Images" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToGlobalmage:@"Before"];
        
    }];
    
    UIAlertAction* addDocument = [UIAlertAction actionWithTitle:@"Add Documents" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToUploadDocument];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addImage];
    [alert addAction:addDocument];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
  
}
-(void)goToUploadDocument
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    UploadDocumentSales *objUploadDocumentSales=[storyBoard instantiateViewControllerWithIdentifier:@"UploadDocumentSales"];
    objUploadDocumentSales.strWoId = strLeadId;

    [self.navigationController presentViewController:objUploadDocumentSales animated:NO completion:nil];
    
}
-(void)sameAsServiceAddress
{
    if (chkForSameBillingAddress == YES)
    {
        _txtViewAdd1BillingAddress.text=_txtViewAdd1ServiceAddress.text;
        _txtViewAdd2BillingAddress.text=_txtViewAdd2ServiceAddress.text;
        [ _btnCountryBillingAddress setTitle:_btnCountryServiceAddress.titleLabel.text forState:UIControlStateNormal];
        [ _btnStateBillingAddress setTitle:_btnStateServiceAddress.titleLabel.text forState:UIControlStateNormal];
        
        [ _btnCountryBillingAddress setTitle:_btnCountryServiceAddress.currentTitle forState:UIControlStateNormal];
        [ _btnStateBillingAddress setTitle:_btnStateServiceAddress.currentTitle forState:UIControlStateNormal];
        
        
        _txtCityBillingAddress.text=_txtCityServiceAddress.text;
        _txtZipCodeBillingAddress.text=_txtZipcodeServiceAddress.text;
        
        //temp
        NSString *strServiceAddress = @"";
        if(_txtViewAdd1ServiceAddress.text.length>0)
        {
            _txtViewAdd1ServiceAddress.text=[_txtViewAdd1ServiceAddress.text stringByTrimmingCharactersInSet:
                                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            strServiceAddress = [NSString stringWithFormat:@"%@",_txtViewAdd1ServiceAddress.text ];
        }
        if(_txtViewAdd2ServiceAddress.text.length>0)
        {
            
            _txtViewAdd2ServiceAddress.text=[_txtViewAdd2ServiceAddress.text stringByTrimmingCharactersInSet:
                                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtViewAdd2ServiceAddress.text ];
        }
        
        if(_txtCityServiceAddress.text.length>0)
        {
            _txtCityServiceAddress.text=[_txtCityServiceAddress.text stringByTrimmingCharactersInSet:
                                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtCityServiceAddress.text ];
        }
        
        if(_btnStateServiceAddress.titleLabel.text.length>0&& ![_btnStateServiceAddress.titleLabel.text isEqualToString:@"State"])
        {
            strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_btnStateServiceAddress.titleLabel.text];
        }
        if(_txtZipcodeServiceAddress.text.length>0)
        {
            _txtZipcodeServiceAddress.text=[_txtZipcodeServiceAddress.text stringByTrimmingCharactersInSet:
                                            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            strServiceAddress = [NSString stringWithFormat:@"%@, %@",strServiceAddress,_txtZipcodeServiceAddress.text ];
        }
        
        _lblNewBillingAddress.text = strServiceAddress;
    }
    
}
-(void)disableBillingAddress
{
    if([_imgCheckBox.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        [_txtViewAdd1BillingAddress setEditable:NO];
        [_txtViewAdd2BillingAddress setEditable:NO];
        [ _btnCountryBillingAddress setEnabled:NO];
        
        [ _btnStateBillingAddress setEnabled:NO];
        [_txtCityBillingAddress setEnabled:NO];
        [_txtZipCodeBillingAddress setEnabled:NO];
    }
    else
    {
        if([strStatusss caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
        }
        else
        {
            [_txtViewAdd1BillingAddress setEditable:YES];
            [_txtViewAdd2BillingAddress setEditable:YES];
            [ _btnCountryBillingAddress setEnabled:YES];
            
            [ _btnStateBillingAddress setEnabled:YES];
            [_txtCityBillingAddress setEnabled:YES];
            [_txtZipCodeBillingAddress setEnabled:YES];
        }
    }
}
- (IBAction)actionOnResidential:(id)sender
{
    
    [_btnOppTypeResidential setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnOppTypeCommercial setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    strFlowType = @"Residential";
}

- (IBAction)actionOnCommercial:(id)sender
{
    
    [_btnOppTypeResidential setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnOppTypeCommercial setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    strFlowType = @"Commercial";
  
}

#pragma mark: ------------- Email & Service History -------------

-(void)goToEmailHistory
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSString *strLeadNo = [defs valueForKey:@"leadNumber"];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"DashBoard" bundle:nil];
    EmailListNew_iPhoneVC *objEmailListNew_iPhoneVC=[storyBoard instantiateViewControllerWithIdentifier:@"EmailListNew_iPhoneVC"];
    objEmailListNew_iPhoneVC.refNo = strLeadNo;
    [self.navigationController pushViewController:objEmailListNew_iPhoneVC animated:NO];
    //[self.navigationController presentViewController:objEmailListNew_iPhoneVC animated:YES completion:nil];
}
-(void)goToServiceHistory
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strTypeOfService=@"sales";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
}
-(void)goToHistory
{
    [self endEditing];
   // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* addServiceHistory = [UIAlertAction actionWithTitle:@"Service History" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToServiceHistory];
        
    }];
    
    UIAlertAction* addEmailHistory = [UIAlertAction actionWithTitle:@"Email History" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToEmailHistory];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addServiceHistory];
    [alert addAction:addEmailHistory];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
  
}
-(void)setTextCorner
{
    _txtLead.layer.masksToBounds = YES;
    _txtAccount.layer.masksToBounds = YES;
    _txtCompanyName.layer.masksToBounds = YES;
    _txtScheduleStartDate.layer.masksToBounds = YES;
    _txtFirstName.layer.masksToBounds = YES;
    _txtMiddleName.layer.masksToBounds = YES;
    _txtLastName.layer.masksToBounds = YES;
    _txtPrimaryEmail.layer.masksToBounds = YES;
    _txtCellNo.layer.masksToBounds = YES;
    _txtPrimaryPhone.layer.masksToBounds = YES;
    _txtSecondaryPhone.layer.masksToBounds = YES;
    _txtBranchName.layer.masksToBounds = YES;
    _txtCityBillingAddress.layer.masksToBounds = YES;
    _txtZipCodeBillingAddress.layer.masksToBounds = YES;
    _txtCityServiceAddress.layer.masksToBounds = YES;
    _txtZipcodeServiceAddress.layer.masksToBounds = YES;
    _txtTagServiceRequired.layer.masksToBounds = YES;
    
    
    _txtMapCodeServiceAddress.layer.masksToBounds = YES;
    _txtGateCodeServiceNotes.layer.masksToBounds = YES;
    _txtServiceCounty.layer.masksToBounds = YES;
    _txtServiceSchoolDistrict.layer.masksToBounds = YES;
    _txtMapCodeBillingAddress.layer.masksToBounds = YES;
    _txtBillingCounty.layer.masksToBounds = YES;
    _txtBillingSchoolDistrict.layer.masksToBounds = YES;
    
    
    _txtServicePOCFirstName.layer.masksToBounds = YES;
    _txtServicePOCLastName.layer.masksToBounds = YES;
    _txtServicePOCMiddleName.layer.masksToBounds = YES;
    _txtServicePOCPrimaryEmail.layer.masksToBounds = YES;
    _txtServicePOCSecondaryEmail.layer.masksToBounds = YES;
    _txtServicePOCPrimaryPhone.layer.masksToBounds = YES;
    _txtServicePOCSecondaryPhone.layer.masksToBounds = YES;
    _txtServicePOCCellNo.layer.masksToBounds = YES;
    
    _txtBillingPOCFirstName.layer.masksToBounds = YES;
    _txtBillingPOCLastName.layer.masksToBounds = YES;
    _txtBillingPOCMiddleName.layer.masksToBounds = YES;
    _txtBillingPOCPrimaryEmail.layer.masksToBounds = YES;
    _txtBillingPOCSecondaryEmail.layer.masksToBounds = YES;
    _txtBillingPOCPrimaryPhone.layer.masksToBounds = YES;
    _txtBillingPOCSecondaryPhone.layer.masksToBounds = YES;
    _txtBillingPOCCellNo.layer.masksToBounds = YES;
    
    
    _txtAreaSqFt.layer.masksToBounds = YES;
    _txtBedroom.layer.masksToBounds = YES;
    _txtBathroom.layer.masksToBounds = YES;
    _txtLotSizeSqFt.layer.masksToBounds = YES;
    _txtYearBuil.layer.masksToBounds = YES;
    _txtLinearSqFt.layer.masksToBounds = YES;
    _txtShrubArea.layer.masksToBounds = YES;
    _txtNoOfStory.layer.masksToBounds = YES;
    _txtTurfArea.layer.masksToBounds = YES;

}
#pragma mark: ----------------- Reset Status -----------------

-(void)resetStatus
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert!"
                               message:@"Are you sure want to reopen opportunity"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
        [self callApiToResetStatus];
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel
                                               handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)callApiToResetStatus
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:Alert : ErrorInternetMsg];
    }
    else
    {
        
        NSString *strUrl = [NSString stringWithFormat:@"%@/api/mobiletosaleauto/ReopenOpportunity?CompanyKey=%@&leadnumber=%@",strSalesAutoMainUrl,strCompanyKey,strLeadNumber];
        
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait..."];
        [FTIndicator showProgressWithMessage:@"Please wait..."];
        //============================================================================
        //============================================================================
        
        NSString *strType=@"ResetStatus";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[DejalBezelActivityView removeView];
                    [FTIndicator dismissProgress];
                    if (success)
                    {
                        if ([[NSString stringWithFormat:@"%@",[response valueForKey:@"Response"]] isEqualToString:@"true"])
                        {
                            NSLog(@"TRUE");
                            [_btnReopen setHidden:YES];
                            [self updateLeadIdDetailForStatus];
                            [self updatePaymentInfoForStatus];
                            
                            [global AlertMethod:Alert : @"Status updated successfully"];
                        }
                        else
                        {
                            
                        }
                    }
                    else
                    {
                        NSString *strTitle = Alert;
                        NSString *strMsg = Sorry;
                        [global AlertMethod:strTitle :strMsg];
                    }
                });
            }];
        });
    }
    //============================================================================
    //============================================================================
}
-(void)updateLeadIdDetailForStatus
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        matches=arrAllObj[0];
        matchesGeneralInfo = matches;
        [matches setValue:@"Complete" forKey:@"statusSysName"];
        [matches setValue:@"CompletePending" forKey:@"stageSysName"];
        [matches setValue:@"true" forKey:@"isCustomerNotPresent"];
        [matches setValue:@"true" forKey:@"isReopen"];
        [matches setValue:@"false" forKey:@"isResendAgreementProposalMail"];
        [context save:&error1];
        
        strStatusss= @"Complete";//[dfsStageStatus valueForKey:@"leadStatusSales"];
        strStageSysName= @"CompletePending";// [dfsStageStatus valueForKey:@"stageSysNameSales"];
        
        NSUserDefaults *dfsStageStatus=[NSUserDefaults standardUserDefaults];
        [dfsStageStatus setValue:strStatusss forKey:@"leadStatusSales"];
        [dfsStageStatus setValue:strStageSysName forKey:@"stageSysNameSales"];
        [dfsStageStatus synchronize];

        
    }
    [self assignValues];
    [self forDisable];
}
-(void)updatePaymentInfoForStatus
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObjNew.count==0)
    {
        
    }
    else
    {
        NSManagedObject *matcheNew;
        matcheNew=arrAllObjNew[0];
        
        [matcheNew setValue:@"false" forKey:@"customerSignature"];
        [context save:&error1];
        
    }
}
- (IBAction)actionOnReopen:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:Alert : ErrorInternetMsg];
    }
    else
    {
        [self resetStatus];
    }
}
- (IBAction)actionOnServiceAddressSubType:(id)sender {
    
    if (arrServiceAddressSubType.count==0)
    {
        [global AlertMethod:@"Alert!" :@"No Address Subtype available"];
    }
    else
    {
        tblData.tag=113;
        [self tableLoad:tblData.tag];
    }
}
@end



