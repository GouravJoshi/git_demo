//
//  BeginAuditView.h
//  AuditApp
//
//  Created by Rakesh Jain on 06/02/14.
//  Copyright (c) 2014 Rakesh Jain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComboBox.h"
#import "dicObj.h"
#import "CMPopTipView.h"
@interface BeginAuditView : UIViewController<UITableViewDataSource,UITableViewDelegate,NSURLConnectionDelegate,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIPopoverControllerDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UIImageView *imgAds;
    int k;
    BOOL isFromImage;
    int countSwipe;
    IBOutlet UITextField *txtfldCome;
    IBOutlet UITextField *txtfldFeedback;
    IBOutlet UITextField *txtfldComment;
    IBOutlet UITextField *txtfldCell;
    IBOutlet UIScrollView *scrollView;
    int findId;
    NSMutableArray *arrayNumbers;
    ComboBox* combo1;
    IBOutlet UILabel *lblRating;
    IBOutlet UIButton *btnYes;
    IBOutlet UIButton *btnNo;
    IBOutlet UIButton *btnFloor;
    UIButton *btnActiveFindingImage;
    
    NSMutableArray *arrayDataStore;
    IBOutlet UITableView *tableMain;
    
    UITapGestureRecognizer *tapRecognizer;
    int radioNum;
    UITextField *txtPriority;
     UITableView *tblView;
    NSString *aId;
    NSURLConnection* connection;
    NSMutableData *responseData;
    
    NSMutableArray *arrayArticles;
    NSMutableArray *arrayFindings;
    NSMutableArray *arrayTblDis;
    NSMutableArray *arrayTblPrio;
    NSMutableDictionary *radioDictObjectKey;
    NSDictionary *dict;
    UITextField *txtPriorityTemp;
    NSMutableArray *arry;
   
    NSMutableArray *arrayGroupName;
    NSMutableArray *arrayViews;
   
    NSMutableArray *arrayNoneBool;
    IBOutlet UIActivityIndicatorView *activity;
    NSDictionary *jsonDictionary;
    int count;
    int boolCount;
    NSString *activeTitle;
    NSMutableArray *arrayPriority;
    BOOL isSendPressed;
    int indexOfRadiotitle;
    int activeTag;
    int cbIndexInArrayData;
    int uploadImageIndex;
    IBOutlet UIImageView *imgHdr;
    IBOutlet UILabel *lblSn;
    IBOutlet UILabel *lblDisHdr;
    IBOutlet UILabel *lblHdrPrio;
    IBOutlet UILabel *lblHdrAction;
    IBOutlet UIButton *btnSaveProgress;
    IBOutlet UIButton *btnMarkAsdone;
    IBOutlet UITableView *tblFindings;
    UIPopoverController *popoverController;
    UIActionSheet *actionSheet;
    
    NSMutableArray *arrayHeight;
  //  UIPickerView *pickerView;
    int QCount;
    NSString *ActivePriority;
    NSString *StringDescription;
    CGFloat animatedDistance ;
    NSString *strAppend;
    NSString *ans;
    NSString *q;
    NSString *sliderValuestr;
    NSString *myStringComasap;
    BOOL isCompleted;
    int qid;
    int gIndex;
    int CbIIndex;
    int TfiIndex;
    int TviIndex;
    int DdiIndex;
    int RbiIndex;
    NSString *finalstr;
    NSMutableDictionary *dic;
    NSMutableDictionary *dicQuestion;
    NSMutableDictionary *dicQuestion1;
    NSMutableArray *arr,*arrFian;
    NSMutableArray *arayData;
    NSMutableDictionary *dictAns;
    NSMutableArray *arrayQid;
    NSString *typeStr;
    NSMutableArray *arrayCBAns;
    NSMutableArray *arraySliderValue;
    NSMutableArray *arry2;
    NSMutableArray *arrayFindId;
    NSArray *CheckBoxChecked;
    NSArray *txtFieldCheckedValues;
    NSMutableArray *arrayDataTemp;
    NSMutableArray *arrayFindImages;
    BOOL isRadioBtn;
    BOOL isInTableView;
    BOOL isCheckBox;
    BOOL isNextQ;
    BOOL isSkipQ;
    
    
    dicObj *dicc;
    int qNo;
    int checkBoxCount;
    UIButton *btnCombo;
    UIAlertView *alert;
    int indexradio;
    BOOL isDeleteCalled;
    NSArray *dicAllvalueforkey;
    BOOL isMarkAsDonePresed;
    NSMutableArray *descArray;
    NSMutableArray *priorArray;
    NSMutableArray *arrayTitles;
    int noneIindex;
    
    UIAlertController *alertController;
    
    NSString *noneQno;
    NSString *noneRating;
    
    int CurrentTitle;
    int sIndex;
    
    int RCorunt;
    int ratingindex;
    int CheckComplition;
    
    BOOL isradioButton;
    float scrollHeight;
    int tesint;
    int MainScroll;
    BOOL isRemovedArticle;
    int mandCount;
    int mantCountForCheck;
    int swipCount;
    IBOutlet UIButton *btnNext;
    IBOutlet UIButton *btnPrev;
    IBOutlet UILabel *lblQno;
    IBOutlet UILabel *lblQCategory;
    IBOutlet UIButton *btnSkip;
    UIButton *btnMore;
    IBOutlet UIPageControl *pgControll;
    float buttonsY;
    UITextField *txtTempDiscription;
    UITextField *txtActivePriorityTemp;
    BOOL isChanged;
    int activeFindIndex;
    
}
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
-(IBAction)AnswerInYesOrNo:(id)sender;
-(IBAction)probleInArea:(id)sender;
-(IBAction)addArticle:(id)sender;
-(IBAction)removeArticle:(id)sender;
-(IBAction)sendData:(id)sender;
-(IBAction)markAsDone:(id)sender;
-(IBAction)checkv;
-(IBAction)back:(id)sender;
-(IBAction)next:(id)sender;
-(IBAction)gotoSetting;
-(IBAction)skipQuestion:(id)sender;
-(IBAction)CaptureImage:(id)sender;
@property(nonatomic,retain)NSString *activeTitle;
@property(nonatomic,readwrite)NSMutableArray *arrayTblDis;
@property(nonatomic,retain)NSMutableArray *arrayTblPrio;
@property(nonatomic,retain)NSMutableArray *arayData;
@property(nonatomic,retain)UIButton *btnActiveFindingImage;
@property(nonatomic,readwrite)UITableView *tblView;
@property(nonatomic,retain)UIPopoverController *popoverController;
@property (nonatomic, strong)	id	currentPopTipViewTarget;
@property(nonatomic)int activeTag;
@property (strong, nonatomic)  NSString *statusOfInitialSetup;
@property (strong, nonatomic)  NSString *strStageType;
@property(strong,nonatomic)NSString *isFromSendEmail;

@end
