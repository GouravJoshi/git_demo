//
//  CommentsTableViewCell.h
//  DPS
//  Created by Rakesh Jain on 27/06/16.
//  Copyright © 2016 Saavan. All rights reserved.


//  Saavan Patidar 2020

#import <UIKit/UIKit.h>

@interface CommentsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *_lbl_DescriptionComments;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EmpName;

@end
