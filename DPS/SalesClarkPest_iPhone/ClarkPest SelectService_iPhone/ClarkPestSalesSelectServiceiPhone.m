//
//  ClarkPestSalesSelectServiceiPhone.m
//  DPS
//
//  Created by Akshay Hastekar on 24/10/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "ClarkPestSalesSelectServiceiPhone.h"

#import "AllImportsViewController.h"
#import "SalesAutomationSelectServiceiPad.h"
#import "SalesAutomationServiceTableViewCell.h"
#import "SalesAutomationTableViewCellSecond.h"
#import "SalesAutomationServiceSummary.h"
#import "AddStandardService.h"
#import "AddNonStandardServices.h"
#import "Global.h"
#import "AddPlusService.h"
#import "GlobalSyncViewController.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
#import "SelectServiceCouponDiscountTableViewCell.h"

#import "ClarkPestSelectServiceTableViewCell.h"
#import "ClarkPestTableViewCellNonStandard.h"
#import "DPS-Swift.h"

///// initial and maint price cell class
@interface CellInitialPrice1:UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelInitialPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelInitialDescription;

@end

@implementation CellInitialPrice1

@end


@interface CellMaintPrice1:UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelMaintPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelServiceFrequency;
@property (weak, nonatomic) IBOutlet UILabel *labelMaintDescription;

@end

@implementation CellMaintPrice1

@end


@interface ClarkPestSalesSelectServiceiPhone ()

@end

@implementation ClarkPestSalesSelectServiceiPhone
{
    UIButton *btnTemp;
    UILabel *lblTemp,*lblBorder,*lblBorder2;
    NSArray *arrOfLeads;
    NSMutableArray *arrData,*arrData1,*arrCategory,*arrFinalIndexPath,*arrFinalIndexPathNonStan;
    UITableView *tblData;
    SalesAutomationServiceTableViewCell *cell1;
    SalesAutomationTableViewCellSecond *cell2;
    BOOL chkScroll,chkScroll2;
    NSMutableArray *arrInitialPrice,*arrDiscount,*arrMaintenancePrice,*arrFrequencyName,*arrServiceName,*arrStanIsSold,*arrSysName,*arrSysNameConditionalStandard;
    NSMutableArray *arrDiscountPer;
    
    NSMutableArray *arrNonStanInitialPrice,*arrNonStanDiscount,*arrNonStanServiceName,*arrNonStanServiceDesc,*arrNonStanIsSold;
    NSMutableArray *arrTem1Initial,*arrTem2Discount,*arrTem3Maintenance,*arrTemFreqName,*arrTemServiceName;
    NSMutableArray *arrNonStanTem1Initial,*arrNonStanTem2Discount;
    BOOL chkClickStan;
    NSString *strLeadId;
    BOOL chkBtnCheckBoxStan,chkBtnCheckBoxNonStan;
    Global *global;
    NSMutableArray *arrDepartmentName,*arrDepartmentSysName;
    //Nilind
    NSMutableArray *arrMasterId;
    NSDictionary *dictServiceName;
    //.............
    BOOL chkkkk,chkStatus;
    NSString *strBranchSysName;
    
    
    BOOL chkPlusService;
    BOOL chkBox,isEditedInSalesAuto;
    UIButton *btnAddRecord;
    NSDictionary *dictForDepartment;
    NSDictionary*dictFreqNameFromSysName;
    NSMutableArray *arrUnit,*arrFinalInitialPrice,*arrFinalMaintPrice;
    NSDictionary *dictQuantityStatus,*dictPackageName;
    NSMutableArray *arrPackageNameId;
    NSString *strCouponStatus,*strCouponStatusNonStan;
    ;
    BOOL isChkBoxEnable,isCheckBoxClickFirstTime;
    BOOL isCheckBoxClickFirstTimeNonStan,isChkBoxEnableNonStan;
    NSMutableArray *arrBillingFreqSysName;
    
    NSMutableArray *arrDiscountPerNonStan;
    
    
    //Nilind 14 Sept Image Change
    
    BOOL chkForDuplicateImageSave;
    
    NSMutableArray *arrNoImage;
    
    
    //Nilind 07 Jun ImageCaption
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    NSMutableArray *arrOfImageCaption,*arrOfImageDescription,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph;
    NSMutableArray *arrImagePath,*arrGraphImage;
    NSString *strLeadStatusGlobal,*strUserName,*strCompanyKey,*strServiceUrlMain;
    NSDictionary *dictBundleNameFromId;
    NSMutableArray *arrBundleId;
    
    //Bundle Code
    NSArray *arrMatchesBundle;
    NSArray *arrBundleRow;
    NSMutableArray *arrInitialPriceBundle,*arrDiscountPerBundle,*arrDiscountBundle,*arrMaintenancePriceBundle,*arrFrequencyNameBundle,*arrServiceNameBundle,*arrStanIsSoldBundle,*arrSysNameBundle,*arrUnitBundle,*arrFinalInitialPriceBundle,*arrFinalMaintPriceBundle,*arrPackageNameIdBundle,*arrBillingFreqSysNameBundle;
    NSMutableArray *arrTempBundelDetailId,*arrTempBundleServiceSysName;
    double totalInitialBundle,totalMaintBundle;
    NSString *strSoldStatusBundle;
    NSString *strBundleIdForTextField;
    NSMutableArray *arrSoldServiceStandardId;
    
    // Nilind 23 Nov
    
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    
    NSString *strStageSysName;
    NSMutableArray *arrRandomId;
    
    NSDictionary *dictServiceParmaterBasedStatus,*dictYearlyOccurFromFreqSysName;
    NSMutableArray *arrAdditionalParamterDcs;
    NSMutableArray *arrFreqSysName,*arrBillingFreqSysNameNonStan;
    NSMutableArray *arrTempCoupon;
    
    NSMutableArray *arrDiscountCoupon;
    NSDictionary *dictDiscountNameFromSysName;
    NSString *strAppliedDiscountServiceId;
    BOOL chkDirectServiceDelete;
    
    float appliedDiscountInitial;
    float appliedDiscountMaint;
    BOOL chkForSaveCoupon;
    NSMutableArray *arrSoldServiceStandardIdBundle;
    NSMutableArray *arrSoldServiceIdOfAllService;
    
    NSString *strAccountNoGlobal;
    NSDictionary *dictDiscountUsageFromSysName,*dictDiscountUsageFromCode;
    NSArray *arrAppliedCouponDetail;
    NSMutableArray *arrAllUnsoldServiceId;
    
    //Saavan Chnages on 9th july 2018
    float globalAmountInitialPrice,globalAmountMaintenancePrice;
    NSMutableArray *arrSoldServiceStandardIdForCoupon;
    
    //Clark Pest Code
    
    
    // akshay working for demo
    UIView *viewBackground;
    int selectionType; // 1 for Service, 2 for Scope and 3 for Target
    NSArray *arrayService;
    NSArray *arrayScope;
    NSArray *arrayTarget;
    NSArray *arrCategoryPopUp;
    
    
    NSArray *arrProposedFreq,*arrProposedMonth;
    UIView *viewBackGround;
    
    NSString *strCategorySysName,*strServiceSysName,*strScopeSysName,*strTargetSysName,*strFreqSysName,*strServiceDescription,*strScopeDescription,*strTargetDescription,*strServiceId;
    
    
    NSArray *arrSavedServiceDetail,*arrSavedScopeDetail,*arrSavedTargetDetail,*arrSavedInitialPriceDetail,*arrSavedMaintPriceDetail;
    UIView *viewForDate;
    UIView *viewBackGround1;
    UIDatePicker *pickerDate;
    NSString *strIsChangedServiceDesc,*strIsChangedScopeDesc,*strIsChangedTargetDesc,*strScopeName,*strTargetName;
    
    NSString *strHTML;
    NSInteger selectedIndexPath;
    UITextView *textViewHTML;
    NSArray *arrSoldCount;
    float subTotalInitialPrice,subTotalMaintPrice;
    BOOL chkForLost;
    NSString *strFlowType;
    
    //28 Sept
    
    NSArray *arrSavedNonStanServiceDetail;
    NSArray *arrSoldCountNonStan;
    
    
    //03 Oct
    
    //Clark Pest Changes For Template
    
    NSMutableArray *arrSelectedMultipleCondition;
    
    NSMutableArray *arrTermsOfService;
    NSMutableArray *arrValueForTermsOfServices;
    
    
    NSMutableArray *arrayLetterTemplateMaster,*arrCoverLetter,*arrIntroductionLetter,*arrMultipleTermsCondtions;
    NSDictionary *dictCoverLetter,*dictIntroLetter,*dictTermsOfService;
    NSString *strIsAgreementValidFor;
    
    NSDictionary *dictTitleFromTermId;
    NSString *isTaxApplicableInitialPrice,*isTaxApplicableMaintPrice;
    NSMutableArray *arrSelectedTermsConditionsDescriptions;
    BOOL isTermsOfService;
    BOOL isCreditCouponValueGreaterThanInitialPrice;
    BOOL isCreditValueGreaterThanMaintPrice;
    
    
    NSMutableArray *arrMarketingContent,*arrSelectedMarketingContent;
    NSDictionary *dictSalesContentTitleFromSysName;
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    chkForLost=NO;
    strIsChangedServiceDesc = @"false";
    strIsChangedScopeDesc = @"false";
    strIsChangedTargetDesc = @"false";
    strFreqSysName = @"Monthly";
    
    strScopeSysName=@"";
    strTargetSysName=@"";
    strServiceSysName=@"";
    strCategorySysName=@"";
    strServiceId=@"";
    
    chkDirectServiceDelete=NO;
    chkForSaveCoupon=NO;
    strBundleIdForTextField=@"0";
    appliedDiscountInitial=0;
    appliedDiscountMaint=0;
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"leadStatusSales"]];
    
    if ([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strLeadStatusGlobal caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
    {
        
        //   strLeadStatusGlobal=@"Complete";
        // [self disableUIElementsInCaseOfLeadStatusComplete];
        
    }
    
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    
    isEditedInSalesAuto=NO;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    isEditedInSalesAuto=NO;
    chkPlusService=NO;
    isChkBoxEnable=NO;
    isCheckBoxClickFirstTime=NO;
    chkBox=NO;
    isCheckBoxClickFirstTimeNonStan=NO;
    isChkBoxEnableNonStan=NO;
    //Nilind  2 Nov
    [self salesFetch];
    
    if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
    {
        strLeadStatusGlobal=@"Complete";
        [self disableUIElementsInCaseOfLeadStatusComplete];
    }
    
    
    
    //NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
    //chkStatus=[dfsStatus boolForKey:@"status"];
    if ([strCouponStatus isEqualToString:@"true"])
    {
        if (chkStatus==YES)
        {
            _const_Discount_H.constant=0;//0
            _const_TableStan_H.constant=_const_TableStan_H.constant+ _const_Discount_H.constant;
        }
        else
        {
            _const_Discount_H.constant=50-50;
            _const_TableStan_H.constant=_const_TableStan_H.constant- _const_Discount_H.constant;
        }
    }
    else
    {
        _const_Discount_H.constant=0;//0
        _const_TableStan_H.constant=_const_TableStan_H.constant+ _const_Discount_H.constant;
    }
    //_const_Discount_H.constant=50;
    
    [self getFrequencySysNameFromName];
    
    
    //..............
    
    chkkkk=NO;
    //Nilind
    
    arrMasterId=[[NSMutableArray alloc]init];
    [self serviceName];
    //..................
    
    _lblNameAcount.text=[defs valueForKey:@"lblName"];
    _lblNameTitle.text=[defs valueForKey:@"nameTitle"];
    _lblNameAcount.textAlignment=NSTextAlignmentCenter;
    _lblNameAcount.font=[UIFont boldSystemFontOfSize:14];
    _lblNameTitle.font=[UIFont boldSystemFontOfSize:14];
    
    global = [[Global alloc] init];
    [self getClockStatus];
    
    chkClickStan=YES;
    _btnCategory1.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _btnCategory1.layer.borderWidth=1.0;
    _btnCategory2.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _btnCategory2.layer.borderWidth=1.0;
    _btnCategory3.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _btnCategory3.layer.borderWidth=1.0;
    //Non StandardView Array creation of InitialPrice Discount and Maintenance
    arrNonStanTem1Initial=[[NSMutableArray alloc]init];
    arrNonStanTem2Discount=[[NSMutableArray alloc]init];
    // arrTem3Maintenance=[[NSMutableArray alloc]init];
    
    arrNonStanInitialPrice=[[NSMutableArray alloc]init];
    arrNonStanDiscount=[[NSMutableArray alloc]init];
    arrNonStanServiceName=[[NSMutableArray alloc]init];
    arrNonStanServiceDesc=[[NSMutableArray alloc]init];
    arrDepartmentName=[[NSMutableArray alloc]init];
    arrDepartmentSysName=[[NSMutableArray alloc]init];
    
    //StandardView Array creation of InitialPrice Discount and Maintenance
    
    arrTem1Initial=[[NSMutableArray alloc]init];
    arrTem2Discount=[[NSMutableArray alloc]init];
    arrTem3Maintenance=[[NSMutableArray alloc]init];
    arrTemFreqName=[[NSMutableArray alloc]init];
    arrTemServiceName=[[NSMutableArray alloc]init];
    
    arrInitialPrice=[[NSMutableArray alloc]init];
    arrDiscount=[[NSMutableArray alloc]init];
    arrMaintenancePrice=[[NSMutableArray alloc]init];
    
    arrFrequencyName=[[NSMutableArray alloc]init];
    arrServiceName=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    
    arrData=[[NSMutableArray alloc]init];
    arrData1=[[NSMutableArray alloc]init];
    arrCategory=[[NSMutableArray alloc]init];
    arrFinalIndexPath=[[NSMutableArray alloc]init];
    arrFinalIndexPathNonStan=[[NSMutableArray alloc]init];
    [arrCategory addObject:@"Residence"];
    [arrCategory addObject:@"Test"];
    [arrCategory addObject:@"Commercial"];
    //[_tblRecord setHidden:YES];
    
    _viewForNonStandardService.hidden=YES;
    _viewForStandardService.hidden=NO;
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 150+50, 50);
    lblTemp.frame=CGRectMake(0, 0, 150+50, 50);
    arrOfLeads=[[NSMutableArray alloc]init];
    arrSysNameConditionalStandard=[[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
    
    arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Agreement", nil];
    if ([strFlowType isEqualToString:@"Commercial"])
    {
        arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Configure Proposal",@"Agreement", nil];
    }
    
    _scrollViewLeads.delegate=self;
    CGFloat scrollWidth=arrOfLeads.count*110;
    [_scrollViewLeads setFrame:CGRectMake(0,_viewTop.frame.origin.y+_viewTop.frame.size.height,[UIScreen mainScreen].bounds.size.width,_scrollViewLeads.frame.size.height)];
    // [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,_scrollViewLeads.frame.size.height)];
    [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,60)];
    _scrollViewLeads.showsHorizontalScrollIndicator = NO;
    
    lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-2, 100*3+15, 2)];
    
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-3, 100*3+15, 2);
    }
    lblBorder.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-2, _scrollViewLeads.frame.size.width, 2)];
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
    
    [_scrollViewLeads addSubview:lblBorder2];
    [_scrollViewLeads addSubview:lblBorder];
    _scrollViewLeads.backgroundColor=[UIColor whiteColor];
    [_scrollViewLeads addSubview:btnTemp];
    [_scrollViewLeads addSubview:lblTemp];
    [self addButtons];
    //Dynamic Table
    
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.dataSource=self;
    tblData.delegate=self;
    
    
    btnAddRecord=[[UIButton alloc]init];
    btnAddRecord.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-80, _viewForStandardService.frame.origin.y+[UIScreen mainScreen].bounds.size.height-200, 60, 60);
    btnAddRecord.layer.cornerRadius=30;
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        btnAddRecord.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-80, _viewForStandardService.frame.origin.y+[UIScreen mainScreen].bounds.size.height-120-40-10, 40, 40);
        btnAddRecord.layer.cornerRadius=20;
    }
    if ([UIScreen mainScreen].bounds.size.height==568)
    {
        btnAddRecord.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-80, _viewForStandardService.frame.origin.y+[UIScreen mainScreen].bounds.size.height-150-10-10, 40, 40);
        btnAddRecord.layer.cornerRadius=20;
    }
    
    btnAddRecord.layer.borderWidth=1;
    btnAddRecord.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    // [btnAddRecord setTitle:@"ADD" forState:UIControlStateNormal];
    [btnAddRecord setImage:[UIImage imageNamed:@"add_white.png"] forState:UIControlStateNormal];
    btnAddRecord.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
   // [_viewForStandardService addSubview:btnAddRecord];
    [btnAddRecord addTarget:self action:@selector(addAction) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:btnAddRecord];
    
    [_btnNonStandardService setBackgroundColor:[UIColor lightGrayColor]];
    
    //Nilind 2 Nov
    if (chkStatus==YES)
    {
        [btnAddRecord setEnabled:NO];
        _btnGlobalSync.enabled=NO;
        _btnMarkAsLost.hidden=YES;
        _btnApplyDiscount.enabled=NO;
        _btnAddService.enabled=NO;
    }
    else
    {
        _btnMarkAsLost.hidden=NO;
    }
    if ([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
    {
        _btnMarkAsLost.hidden=YES;
    }
    
    //........................
    //Standar Data Fetch
    [self fetchDepartmentName];
    [self fetchFromCoreDataStandard];
    [self fetchFromCoreDataStandardNewSaavan];
    
    //Nilind 14 sEPT
    chkForDuplicateImageSave=NO;
    arrNoImage=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImagePath=[[NSMutableArray alloc]init];
    
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    
    [self fetchImageDetailFromDataBase];
    //_btnMarkAsLost.layer.borderWidth=1.0;
    
    // _btnMarkAsLost.layer.cornerRadius=5.0;
    //End
    
    _tblCouponDiscount.rowHeight=UITableViewAutomaticDimension;
    _tblCouponDiscount.estimatedRowHeight=50;
    _const_TableDiscount_H.constant=0;
    _const_ViewDiscountCoupon_H.constant=_const_ViewDiscountCoupon_H.constant-108;
    [self fetchForAppliedDiscountFromCoreData];
    
    //  arrCategoryPopUp = @[@"Category1",@"Category2",@"Category3",@"Category4",@"Category5",@"Category6",@"Category7",@"Category",@"Category9",@"Category"];
    
    // arrProposedFreq = @[@"Mothly",@"Yearly",@"Weekly",@"Bi-Annualy",@"EOM",@"One Time"];
    arrProposedMonth = @[@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December",];
    
    
    arrCategoryPopUp=[self getCategoryDeptWise];
    
    [self getTarget];
    [self getFrequency];
    
    _textViewDescription_PopUp.placeholder = @"Enter Description";
    _textViewDescription_PopUp.delegate = self;
    
    _btnSelectionCategory.layer.borderColor = [[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor];
    _btnSelectionCategory.layer.borderWidth = 1.0;
    _buttonSelectOptions_PopUp.layer.borderColor = [[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor];
    _buttonSelectOptions_PopUp.layer.borderWidth = 1.0;
    
    _textViewDescription_PopUp.layer.borderColor = [[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor];
    _textViewDescription_PopUp.layer.borderWidth = 1.0;
    _textViewDescription_PopUp.layer.cornerRadius = 2.0;
    
    _heightTblViewInitialPrice.constant = 0.0;
    _heightSubtotalInitialPrice.constant = 0.0;
    _heightTblViewMaintPrice.constant = 0.0;
    _heightSubtotalMaintPrice.constant = 0.0;
    
    [self setTabelBordeColor];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    //    NSString *strServiceDate = [dateFormat stringFromDate:[NSDate date]];
    //    [_buttonProposedInitialServiceDate setTitle:strServiceDate forState:UIControlStateNormal];
    _buttonProposedInitialServiceDate.layer.cornerRadius = 5.0;
    _buttonProposedInitialServiceDate.layer.borderWidth = 1.0;
    _buttonProposedInitialServiceDate.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    //[self setCornerRadius];
    
    // [_buttonProposedInitialServiceDate setTitle:@"Select Date" forState:UIControlStateNormal];
    [self setBorderTemplate];
    
    _txtViewIntroductionLetter.editable = NO;
    _txtViewTermsServices.editable = NO;
    
    
    self.tblService.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tblTarget.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tblScope.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tblNonStandardClarkPest.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

- (void)viewWillLayoutSubviews{
    
    float ftbl = _tblNonStandardClarkPest.contentSize.height;

    if (ftbl > _const_TblNonStan_H.constant) {
        
        _const_TblNonStan_H.constant = ftbl;
        
    }
    
}

-(void)setCornerRadius
{
    // _txtInitialPrice.layer.borderColor = [[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor];
    
    _txtFieldProposedInitialPrice.layer.borderWidth = 1.0;
    _txtFieldProposedInitialPrice.layer.cornerRadius = 4.0;
    
    _txtFieldProposedMaintPrice.layer.borderWidth = 1.0;
    _txtFieldProposedMaintPrice.layer.cornerRadius = 4.0;
    
    _textViewInitialDescription.layer.borderWidth = 1.0;
    _textViewInitialDescription.layer.cornerRadius = 4.0;
    
    _textViewMaintDescription.layer.borderWidth = 1.0;
    _textViewMaintDescription.layer.cornerRadius = 4.0;
    
    _btnProposedServiceFreq.layer.borderWidth = 1.0;
    _btnProposedServiceFreq.layer.cornerRadius = 4.0;
    
    _btnProposedServiceMonth.layer.borderWidth = 1.0;
    _btnProposedServiceMonth.layer.cornerRadius = 4.0;
    
    _buttonProposedInitialServiceDate.layer.borderWidth = 1.0;
    _buttonProposedInitialServiceDate.layer.cornerRadius = 4.0;
}
-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    [self fetchImageDetailFromDataBaseForGraph];
    
    isCheckBoxClickFirstTime=NO;
    isCheckBoxClickFirstTimeNonStan=NO;
    [self fetchFromCoreDataStandard];
    //temp 19 April
    [self fetchFromCoreDataStandardNewSaavan];
    //End
    
    [self serviceName];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    if([defs boolForKey:@"isFromBackServiceSummary"]==YES)
    {
        [self fetchForAppliedDiscountFromCoreData];
        // [self fetchForAppliedDiscountFromCoreDataFromBack];
    }
    //Nilind 15 Sept IMAGE FETCH COLLECTION VIEW  FOOTER
    
    
    [self getImageCollectionView];
    [self heightManage];
    
    /* if (chkNonStan==YES)
     {
     int sumNonStan=0;
     for (int i=0; i<arrNonStanServiceName.count; i++)
     {
     sumNonStan=sumNonStan+_tblNonStandardService.rowHeight;
     }
     _const_TableNonStan_H.constant=sumNonStan+50;
     [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableNonStan_H.constant)];
     [defs setBool:NO forKey:@"backNonStandard"];
     [defs synchronize];
     }*/
    [self fetchFromCoreDataStandardClarkPest];
    //Nilind
    [self fetchFromCoreDataNonStandardClarkPest];
    //
    [self fetchScopeFromCoreData];
    [self fetchTargetFromCoreData];
    [self fetchLeadCommercialInitialInfoFromCoreData];
    [self fetchLeadCommercialMaintInfoFromCoreData];
    [self fetchLead];
    
    _heightSubtotalInitialPrice.constant = 0;
    _heightSubtotalMaintPrice.constant = 0;
    
    //[_buttonProposedInitialServiceDate setTitle:@"Select Date" forState:UIControlStateNormal];
    
    // [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_viewInitialMaintDetail.frame.size.height+_const_TblService_H.constant+_const_TblScope_H.constant+_const_TblTarget_H.constant+  _heightTblViewInitialPrice.constant+_heightSubtotalInitialPrice.constant+_heightTblViewMaintPrice.constant+_heightSubtotalMaintPrice.constant)];
    [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TblService_H.constant+_const_TblScope_H.constant+_const_TblTarget_H.constant+ _const_TblNonStan_H.constant+ _viewCoverLetter.frame.size.height+_viewIntroductionLetter.frame.size.height+_viewForSalesMarketingContent.frame.size.height+_viewTermsOfService.frame.size.height + 150 + 350)];
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        [self disableUIElementsInCaseOfLeadStatusComplete];
    }
    
}
//============================================================================
#pragma mark- DYNAMIC LABEL AND BUTTON METHOD
//============================================================================

-(void)addButtons
{
    
    
    
    for(int i =0;i<arrOfLeads.count;i++)
    {
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+40, 0, 30, 30)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 15;
        lbl.layer.borderWidth=1.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 100, 20);
            //            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 30, 30);
            
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2-15, 5, 30, 30);
            lbl.backgroundColor=[UIColor redColor];//[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor =[UIColor darkGrayColor];// [UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 100, 20);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 30, 30);
            lbl.textColor = [UIColor darkGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        for(int j=0;j<=2;j++)
        {
            if (i==j)
            {
                lbl.backgroundColor= [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
                lbl.textColor = [UIColor whiteColor];
                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];
        btn.titleLabel.numberOfLines=2;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [_scrollViewLeads addSubview:btn];
        
        [_scrollViewLeads addSubview:lbl];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionOnBack:(id)sender
{
    [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isBackFromSelectService"];
    [defs synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}
//============================================================================
#pragma mark- TABLEVIEW DELEGATE METHOD
//tblRecord Tag=0
//tblNonStandardService Tag=1
//============================================================================
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==0)
    {
        NSArray *arrOther;
        if (arrAdditionalParamterDcs.count>0)
        {
            arrOther=[arrAdditionalParamterDcs objectAtIndex:indexPath.row];
            
        }
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]];
        if ([str isEqualToString:@"0"])
        {
            if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]isEqualToString:@"1"])
            {
                
                return 242-65+18+((arrOther.count)*(175))+50;
            }
            else
            {
                return 242-65+18;
                
            }
            //return 260;
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                //return 227-65;
                if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]isEqualToString:@"1"])
                {
                    
                    return 242-65+18+((arrOther.count)*175)+50;
                }
                else
                {
                    return 242-65+18;
                    
                }
                
            }
            else
            {
                if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]isEqualToString:@"1"])
                {
                    
                    return 242+18+((arrOther.count)*175)+50; //242
                }
                else
                {
                    return 242+18; //242
                    
                }
            }
            //return 260;
        }
    }
    else if (tableView.tag==2)
    {
        NSString *strBundellll=[arrBundleRow objectAtIndex:indexPath.section];
        if (strBundellll.length==0 || [strBundellll isEqualToString:@""] || [strBundellll isEqual:nil])
        {
            
        }
        else
        {
            [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:indexPath.section]];
        }
        
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]];
        if ([str isEqualToString:@"0"])
        {
            //return 227-65;
            return tableView.rowHeight-65+10;
            
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                //return 227-65;
                return tableView.rowHeight-65+10;
                
            }
            else
            {
                //return 227;
                return tableView.rowHeight+10;
            }
        }
        
    }
    else if (tableView.tag==101)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==300)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==301)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==302)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==303)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==304)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==305)
    {
        return UITableViewAutomaticDimension;
    }
    else
    {
        //return 190;
        return tableView.rowHeight;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView.tag==0)
    {
        return arrInitialPrice.count;
    }
    else if(tableView.tag==1)
    {
        return [arrNonStanInitialPrice count];//[arrData1 count];//[arrData count];
    }
    else if (tableView.tag==2)
    {
        int k=0;
        NSString *strBundleId=[arrBundleRow objectAtIndex:section];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        NSArray *arrTempBundle=[dictMasters valueForKey:@"ServiceBundles"];
        NSMutableArray *arrAllBundles;
        arrAllBundles=[[NSMutableArray alloc]init];
        for (int i=0; i<arrTempBundle.count;i++)
        {
            NSDictionary *dict=[arrTempBundle objectAtIndex:i];
            [arrAllBundles addObject:dict];
            
        }
        for (int i=0; i<arrAllBundles.count; i++)
        {
            NSDictionary *dict=[arrAllBundles objectAtIndex:i];
            if ([strBundleId isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceBundleId"]]])
            {
                NSArray *arrServiceBundle=[dict valueForKey:@"ServiceBundleDetails"];
                k=(int)arrServiceBundle.count;
                break;
            }
        }
        /*if (strBundleId.length==0 || [strBundleId isEqualToString:@""] || [strBundleId isEqual:nil])
         {
         
         }
         else
         {
         [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:section]];
         }*/
        return k;
    }
    else if (tableView.tag==300)
    {
        return arrSavedServiceDetail.count;
    }
    else if (tableView.tag==301)
    {
        return arrSavedScopeDetail.count;
    }
    else if (tableView.tag==302)
    {
        return arrSavedTargetDetail.count;
    }
    else if(tableView.tag==303)
    {
        return arrSavedInitialPriceDetail.count;
    }
    else if(tableView.tag==304)
    {
        return arrSavedMaintPriceDetail.count;
    }
    else if(tableView.tag==305)
    {
        return arrSavedNonStanServiceDetail.count;
    }
    else if (tableView.tag==70)
    {
        return arrCategoryPopUp.count;
    }
    else if (tableView.tag==71)
    {
        return arrayService.count;
    }
    else if (tableView.tag==72)
    {
        return arrayScope.count;
    }
    else if (tableView.tag==73)
    {
        return arrayTarget.count;
    }
    else if (tableView.tag==74)
    {
        return arrProposedFreq.count;
    }
    else if (tableView.tag==75)
    {
        return arrProposedMonth.count;
    }
    // Template Letter Tables
    else if (tableView.tag==10)
    {
        return arrCoverLetter.count;
    }
    else if (tableView.tag==11)
    {
        return arrIntroductionLetter.count;
    }
    /*else if (tableView.tag==12)
     {
     return arrMultipleTermsCondtions.count;
     }*/
    else if (tableView.tag==13)
    {
        return arrTermsOfService.count;
    }
    else if (tableView.tag==14)
    {
        return arrMarketingContent.count;
    }
    //End
    else
    {
        return 0;
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==0)
    {
        static NSString *identifier=@"cellId";
        // SalesAutomationServiceTableViewCell *
        cell1=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell1==nil)
        {
            cell1=[[SalesAutomationServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell1.lblFrequency.text=@"dsfdsfdsf";
            
        }
        
        
        if (chkStatus==YES)
        {
            cell1.btnImgStandardService.enabled=NO;
            cell1.btnPlusService.enabled=NO;
            cell1.txtUnit.enabled=NO;
            
        }
        cell1.btnPlusService.hidden=YES;
        chkkkk=NO;
        
        //cell1.backgroundColor=[UIColor lightGrayColor];
        cell1.btnImgStandardService.tag=indexPath.row;
        
        NSLog(@"cell1.btnImgStandardService.tag %ld",(long)cell1.btnImgStandardService.tag);
        
        NSLog(@"cell1.imgView.tag %ld",(long)cell1.imgView.tag);
        NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        if ([arrFinalIndexPath containsObject:str])
        {
            
            [cell1.imgView setImage:[UIImage imageNamed:@"check_box_2.png"]];
        }
        else
        {
            
            [cell1.imgView setImage:[UIImage imageNamed:@"check_box_1.png"]];
            
        }
        
        
        [cell1.btnImgStandardService addTarget:self
                                        action:@selector(buttonClickedStandardService:) forControlEvents:UIControlEventTouchDown];
        
        
        NSString *str12=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]];
        
        if ([str12 isEqualToString:@"0"]||[str isEqualToString:@"(null)"]||str12.length==0)
        {
            cell1.const_viewUnit_H.constant=0;
            
            
        }
        else
        {
            cell1.const_viewUnit_H.constant=65;
            
            
        }
        
        if ([[arrInitialPrice objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblInitialPriceValue.text=@"TBD";
        }
        else
        {
            cell1.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrInitialPrice objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrMaintenancePrice objectAtIndex:indexPath.row]isEqualToString:@"TBD"])
        {
            cell1.lblMaintenanceValue.text=@"TBD";
        }
        else
        {
            cell1.lblMaintenanceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrMaintenancePrice objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrFinalInitialPrice objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblFinalInitialPrice.text=@"TBD";
        }
        else
        {
            cell1.lblFinalInitialPrice.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalInitialPrice objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrFinalMaintPrice objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblFinalMaintPrice.text=@"TBD";
        }
        else
        {
            cell1.lblFinalMaintPrice.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalMaintPrice objectAtIndex:indexPath.row] doubleValue]];
        }
        
        
        
        //Nilind 25 May
        //End
        cell1.lblUnit.hidden=YES;
        //cell1.lblUnit.text=[arrUnit objectAtIndex:indexPath.row];
        cell1.txtUnit.text=[arrUnit objectAtIndex:indexPath.row];
        
        //End
        
        
        cell1.lblDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrDiscount objectAtIndex:indexPath.row]doubleValue]];
        
        cell1.lblDiscountPercent.text=[NSString stringWithFormat:@"%.2f",[[arrDiscountPer objectAtIndex:indexPath.row]doubleValue]];
        
        
        
        cell1.lblFrequency.text=[dictFreqNameFromSysName valueForKey:[arrFrequencyName objectAtIndex:indexPath.row]];
        
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysName objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        
        //Billing Frequency Formula Calculation
        
        NSArray *arrAdditionalPara=[arrAdditionalParamterDcs objectAtIndex:indexPath.row];
        
        float totalParaInitial=0.0,totalParaMaint=0.0;
        if(arrAdditionalPara.count>0)
        {
            for (int i=0; i<arrAdditionalPara.count; i++)
            {
                NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
                totalParaInitial=totalParaInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]floatValue];
                totalParaMaint=totalParaMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]floatValue];
                
            }
        }
        totalParaInitial=totalParaInitial+([[arrInitialPrice objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
        
        if (indexPath.row==0) {
            
            globalAmountInitialPrice=totalParaInitial;
            
        }
        
        totalParaInitial=totalParaInitial-[[arrDiscount objectAtIndex:indexPath.row]floatValue];
        
        totalParaMaint=totalParaMaint+([[arrMaintenancePrice objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
        
        if (indexPath.row==0) {
            
            globalAmountMaintenancePrice=totalParaMaint;
            
        }
        
        NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
        strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFreqSysName objectAtIndex:indexPath.row]]];
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysName objectAtIndex:indexPath.row]]];
        float totalBillingFreqCharge=0.0;
        
        if ([[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
        {
            
            totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            //strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
            
        }
        else
        {
            totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            //strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        //End
        cell1.lblBillingFrequency.text=strBillingFreq;
        //Nilind 02 June
        
        //cell1.lblServiceType.text=[arrServiceName objectAtIndex:indexPath.row];
        
        NSString *strServiceWithPackage,*strPackage;
        strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameId objectAtIndex:indexPath.row]]];
        if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
        {
            strPackage=@"NO Package";
        }
        strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",[arrServiceName objectAtIndex:indexPath.row],strPackage];
        
        //For Bundle
        
        /* NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
         if ([strBundle isEqualToString:@"0"])
         {
         }
         else
         {
         strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
         if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
         {
         strPackage=@"NO Package";
         }
         strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,[arrServiceName objectAtIndex:indexPath.row]];
         
         }*/
        
        //End
        
        
        cell1.lblServiceType.text=strServiceWithPackage;
        [cell1.lblServiceType sizeToFit];
        //End
        
        cell1.lblServiceSysName.text=[arrSysName objectAtIndex:indexPath.row];
        NSString *strSysNameService=[arrSysName objectAtIndex:indexPath.row];
        
        [self chkForPlusServiceButton:strSysNameService];
        if (chkkkk==YES)
        {
            cell1.btnPlusService.hidden=NO;
            
        }
        else
        {
            cell1.btnPlusService.hidden=YES;
        }
        
#pragma mark- change sysname array
        //Nilind 6 Oct
        
        cell1.btnPlusService.tag=indexPath.row;
        [cell1.btnPlusService addTarget:self action:@selector(plusService:) forControlEvents:UIControlEventTouchUpInside];
        cell1.txtStanDiscount.text=[arrDiscount objectAtIndex:indexPath.row];
        cell1.txtDiscountPercent.text=[arrDiscountPer objectAtIndex:indexPath.row];
        
        if (isCheckBoxClickFirstTime==YES)
        {
            if ([strCouponStatus isEqualToString:@"true"])
            {
                if (isChkBoxEnable==YES)
                {
                    cell1.txtStanDiscount.hidden=NO;
                    cell1.lblDiscountValue.hidden=YES;
                    cell1.txtDiscountPercent.hidden=NO;
                    cell1.lblDiscountPercent.hidden=YES;
                    cell1.txtStanDiscount.text=[arrDiscount objectAtIndex:indexPath.row];
                }
                else
                {
                    cell1.txtStanDiscount.hidden=YES;
                    cell1.lblDiscountValue.hidden=NO;
                    
                    cell1.txtDiscountPercent.hidden=YES;
                    cell1.lblDiscountPercent.hidden=NO;
                    if([cell1.lblInitialPriceValue.text isEqualToString:@"TBD"])
                    {
                        cell1.lblDiscountValue.text=@"TBD";
                        cell1.txtStanDiscount.text=@"TBD";
                        cell1.lblDiscountPercent.text=@"TBD";
                        cell1.txtDiscountPercent.text=@"TBD";
                        
                    }
                    else
                    {
                        cell1.lblDiscountValue.text=@"0";
                        cell1.txtStanDiscount.text=@"0";
                        cell1.lblDiscountPercent.text=@"0";
                        cell1.txtDiscountPercent.text=@"0";
                        
                    }
                }
            }
            else
            {
                cell1.txtStanDiscount.hidden=YES;
                cell1.lblDiscountValue.hidden=NO;
                cell1.txtDiscountPercent.hidden=YES;
                cell1.lblDiscountPercent.hidden=NO;
                
                
            }
            cell1.txtStanDiscount.tag=indexPath.row;
            cell1.txtDiscountPercent.tag=indexPath.row;
            
        }
        else
        {
            cell1.txtStanDiscount.hidden=YES;
            cell1.lblDiscountValue.hidden=NO;
            
            cell1.txtDiscountPercent.hidden=YES;
            cell1.lblDiscountPercent.hidden=NO;
        }
        cell1.txtUnit.tag=indexPath.row;
        
        //Temp Code
        /*cell1.txtStanDiscount.hidden=NO;
         cell1.lblDiscountValue.hidden=YES;
         
         cell1.txtDiscountPercent.hidden=NO;
         cell1.lblDiscountPercent.hidden=YES;*/
        
        
        if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]isEqualToString:@"1"])
        {
            //NSArray *arrOther=[self calculationForParamterBasedService:[arrSysName objectAtIndex:indexPath.row]];
            
            NSArray *arrOther=[arrAdditionalParamterDcs objectAtIndex:indexPath.row];
            if([arrOther isKindOfClass:[NSArray class]])
            {
                int heigthDynamic;
                heigthDynamic=0;
                float sumFinalInitialPrice = 0.0;
                float sumFinalMaintPrice = 0.0;
                int maxY=0;
                // UILabel* lbl;//=[[UILabel alloc]init];
                // UITextField* txt;//=[[UITextField alloc]init];
                
                //[cell1.viewForParameterPrice removeFromSuperview];
                
                
                
                //                dispatch_async (dispatch_get_main_queue(), ^{
                
                for(UIView *view in cell1.viewForParameterPrice.subviews)
                {
                    [view removeFromSuperview];
                }
                
                for (int i=0; i<arrOther.count; i++)
                {
                    UILabel* lbl;
                    UITextField* txt;
                    
                    NSDictionary *dictOther=[arrOther objectAtIndex:i];
                    NSArray *arrKey=[dictOther allKeys];
                    
                    if ([arrKey containsObject:@"AdditionalParameterName"])
                    {
                        
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25, maxY,cell1.viewForParameterPrice.frame.size.width, 20)];;
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(25, maxY,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        
                        lbl.text=[NSString stringWithFormat:@"%@ : %@",@"Field Name",[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"AdditionalParameterName"]]];
                        
                        lbl.font=[UIFont boldSystemFontOfSize:16];
                        
                        /*txt.frame=CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20);
                         
                         txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"Name"]];
                         txt.font=[UIFont systemFontOfSize:16];
                         txt.layer.borderColor=[[UIColor blackColor] CGColor];
                         txt.delegate=self;
                         txt.layer.borderWidth=1.0;
                         txt.textAlignment=NSTextAlignmentCenter;
                         
                         txt.tag=(10*i)+10+indexPath.row;*/
                        
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        // [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                    }
                    if ([arrKey containsObject:@"UnitName"] && [NSString stringWithFormat:@"%@",[dictOther valueForKey:@"UnitName"]].length>0)
                    {
                        
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        
                        lbl.text=[NSString stringWithFormat:@"%@",@"Unit Name"];
                        
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"UnitName"]];
                        txt.font=[UIFont systemFontOfSize:16];
                        //txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        txt.enabled=NO;
                        //txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=1000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        
                    }
                    if ([arrKey containsObject:@"UnitName"] && [NSString stringWithFormat:@"%@",[dictOther valueForKey:@"UnitName"]].length>0)
                    {
                        
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        lbl.text=[NSString stringWithFormat:@"%@",@"Enter Unit"];
                        
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"Unit"]];
                        txt.font=[UIFont systemFontOfSize:16];
                        txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=2000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        if (chkStatus==YES)
                        {
                            txt.enabled=NO;
                        }
                        else
                        {
                            txt.enabled=YES;
                            
                        }
                        txt.keyboardType=UIKeyboardTypeDecimalPad;
                        
                    }
                    if ([arrKey containsObject:@"InitialUnitPrice"])
                    {
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        
                        lbl.text=[NSString stringWithFormat:@"%@",@"Initial Unit Price"];
                        
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        
                        //txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]];
                        if([[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]] floatValue]<[[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]]floatValue])
                        {
                            txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]];
                        }
                        else
                        {
                            txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]];
                        }
                        
                        
                        txt.font=[UIFont systemFontOfSize:16];
                        // txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        //txt.layer.borderWidth=1.0;
                        txt.enabled=NO;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=3000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        
                    }
                    if ([arrKey containsObject:@"MaintUnitPrice"])
                    {
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        lbl.text=[NSString stringWithFormat:@"%@",@"Maint. Unit Price"];
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MaintUnitPrice"]];
                        txt.font=[UIFont systemFontOfSize:16];
                        txt.delegate=self;
                        txt.enabled=NO;
                        // txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=4000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        
                    }
                    if ([arrKey containsObject:@"FinalInitialUnitPrice"])
                    {
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        lbl.text=[NSString stringWithFormat:@"%@",@"Total Initial Price"];
                        //                        lbl.backgroundColor = [UIColor yellowColor];
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        // txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"FinalInitialUnitPrice"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"FinalInitialUnitPrice"]] floatValue]<[[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]]floatValue])
                        {
                            txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]];
                        }
                        else
                        {
                            txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"FinalInitialUnitPrice"]];
                        }
                        
                        
                        
                        txt.font=[UIFont systemFontOfSize:16];
                        txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        //txt.enabled=NO;
                        txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=5000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        
                        sumFinalInitialPrice=sumFinalInitialPrice + [txt.text floatValue];
                        
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        if (chkStatus==YES)
                        {
                            txt.enabled=NO;
                        }
                        else
                        {
                            txt.enabled=YES;
                            
                        }
                        txt.keyboardType=UIKeyboardTypeDecimalPad;
                        
                    }
                    if ([arrKey containsObject:@"FinalMaintUnitPrice"])
                    {
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        lbl.text=[NSString stringWithFormat:@"%@",@"Total Maint. Price"];
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"FinalMaintUnitPrice"]];
                        txt.font=[UIFont systemFontOfSize:16];
                        txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        //txt.enabled=NO;
                        txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=6000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        sumFinalMaintPrice=sumFinalMaintPrice + [txt.text floatValue];
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        if (chkStatus==YES)
                        {
                            txt.enabled=NO;
                        }
                        else
                        {
                            txt.enabled=YES;
                            
                        }
                        txt.keyboardType=UIKeyboardTypeDecimalPad;
                        
                    }
                    
                }
                
                
                
                UILabel* lblFinalInitialPrice=[[UILabel alloc]init];
                UILabel* lblValueFinalInitialPrice=[[UILabel alloc]init];
                
                lblFinalInitialPrice=[[UILabel alloc]init];
                lblValueFinalInitialPrice=[[UILabel alloc]init];
                lblFinalInitialPrice.frame=CGRectMake(10,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20);
                lblFinalInitialPrice.text=[NSString stringWithFormat:@"%@",@"Final Initial Price"];
                
                lblFinalInitialPrice.font=[UIFont boldSystemFontOfSize:16];
                
                lblValueFinalInitialPrice.frame=CGRectMake(cell1.txtUnit.frame.origin.x, lblFinalInitialPrice.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20);
                
                
                sumFinalInitialPrice=sumFinalInitialPrice+[[arrInitialPrice objectAtIndex:indexPath.row]floatValue]-[[arrDiscount objectAtIndex:indexPath.row]floatValue];
                lblValueFinalInitialPrice.text=[NSString stringWithFormat:@"$%.2f",sumFinalInitialPrice];
                
                lblValueFinalInitialPrice.font=[UIFont boldSystemFontOfSize:16];
                lblValueFinalInitialPrice.textAlignment=NSTextAlignmentCenter;
                [cell1.viewForParameterPrice addSubview:lblFinalInitialPrice];
                [cell1.viewForParameterPrice addSubview:lblValueFinalInitialPrice];
                maxY=CGRectGetMaxY(lblFinalInitialPrice.frame);
                cell1.const_ViewParameter_H.constant=maxY+10;
                
                
                UILabel* lblFinalMaintPrice=[[UILabel alloc]init];
                UILabel* lblValueFinalMaintPrice=[[UILabel alloc]init];
                
                lblFinalMaintPrice=[[UILabel alloc]init];
                lblValueFinalMaintPrice=[[UILabel alloc]init];
                
                lblFinalMaintPrice.frame=CGRectMake(10,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20);
                lblFinalMaintPrice.text=@"Final Maint. Price";
                
                lblFinalMaintPrice.font=[UIFont boldSystemFontOfSize:16];
                
                lblValueFinalMaintPrice.frame=CGRectMake(cell1.txtUnit.frame.origin.x, lblFinalMaintPrice.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20);
                
                
                
                sumFinalMaintPrice=sumFinalMaintPrice+[[arrMaintenancePrice objectAtIndex:indexPath.row]floatValue];
                
                lblValueFinalMaintPrice.text=[NSString stringWithFormat:@"$%.2f",sumFinalMaintPrice];
                
                lblValueFinalMaintPrice.font=[UIFont boldSystemFontOfSize:16];
                lblValueFinalMaintPrice.textAlignment=NSTextAlignmentCenter;
                [cell1.viewForParameterPrice addSubview:lblFinalMaintPrice];
                [cell1.viewForParameterPrice addSubview:lblValueFinalMaintPrice];
                maxY=CGRectGetMaxY(lblFinalMaintPrice.frame);
                
                cell1.const_ViewParameter_H.constant=maxY+10;
                //});
                
            }
            
            
        }
        else
        {
            cell1.const_ViewParameter_H.constant=0;
            
        }
        
        return cell1;
    }
    else if(tableView.tag==1)
    {
        static NSString *identifier=@"cellId2";
        // SalesAutomationTableViewCellSecond *
        cell2=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell2==nil)
        {
            cell2=[[SalesAutomationTableViewCellSecond alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (chkStatus==YES)
        {
            cell2.btnImageNonStandardService.enabled=NO;
        }
        cell2.btnImageNonStandardService.tag=indexPath.row;
        
        NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        if ([arrFinalIndexPathNonStan containsObject:str])
        {
            //[cell2.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            [cell2.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"]];
        }
        else
        {
            //[cell2.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            [cell2.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"]];
        }
        
        // cell2.backgroundColor=[UIColor lightGrayColor];
        [cell2.btnImageNonStandardService addTarget:self
                                             action:@selector(buttonClickedNonStandardService:) forControlEvents:UIControlEventTouchDown];
        
        cell2.lblServiceType.text=[arrNonStanServiceName objectAtIndex:indexPath.row];
        cell2.lblServiceDescription.text=[arrNonStanServiceDesc objectAtIndex:indexPath.row];
        
        cell2.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrNonStanInitialPrice objectAtIndex:indexPath.row] doubleValue]];
        
        cell2.lblDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrNonStanDiscount objectAtIndex:indexPath.row]doubleValue]];
        
        cell2.lblDiscountPercent.text=[NSString stringWithFormat:@"%.2f",[[arrDiscountPerNonStan objectAtIndex:indexPath.row]doubleValue]];
        
        
        if(arrDepartmentName.count==0)
        {
            cell2.lblDepartmentValue.text=@"N/A";
        }
        else
        {
            if ([NSString stringWithFormat:@"%@",[arrDepartmentName objectAtIndex:indexPath.row]].length==0)
            {
                cell2.lblDepartmentValue.text=@"N/A";
            }
            else
            {
                cell2.lblDepartmentValue.text=[NSString stringWithFormat:@"%@",[arrDepartmentName objectAtIndex:indexPath.row]];
                //cell2.lblDepartmentValue.text=[NSString stringWithFormat:@"%@",[dictForDepartment valueForKey:[arrDepartmentName objectAtIndex:indexPath.row]]];
                //[dictForDepartment valueForKey:[matches valueForKey:@"departmentSysname"]]
            }
        }
        
        //Nilind 08 June
        
        cell2.txtDiscountNonStan.text=[arrNonStanDiscount objectAtIndex:indexPath.row];
        cell2.txtDiscountPercent.text=[arrDiscountPerNonStan objectAtIndex:indexPath.row];
        
        if (isCheckBoxClickFirstTimeNonStan==YES)
        {
            if ([strCouponStatusNonStan isEqualToString:@"true"])
            {
                if (isChkBoxEnableNonStan==YES)
                {
                    cell2.txtDiscountNonStan.hidden=NO;
                    cell2.lblDiscountValue.hidden=YES;
                    cell2.txtDiscountNonStan.text=[arrNonStanDiscount objectAtIndex:indexPath.row];
                    
                    cell2.txtDiscountPercent.hidden=NO;
                    cell2.lblDiscountPercent.hidden=YES;
                    cell2.txtDiscountPercent.text=[arrDiscountPerNonStan objectAtIndex:indexPath.row];
                }
                else
                {
                    cell2.txtDiscountNonStan.hidden=YES;
                    cell2.lblDiscountValue.hidden=NO;
                    
                    cell2.txtDiscountPercent.hidden=YES;
                    cell2.lblDiscountPercent.hidden=NO;
                    
                    if([cell2.lblInitialPriceValue.text isEqualToString:@"TBD"])
                    {
                        cell2.lblDiscountValue.text=@"TBD";
                        cell2.txtDiscountNonStan.text=@"TBD";
                        
                        cell2.lblDiscountPercent.text=@"TBD";
                        cell2.txtDiscountPercent.text=@"TBD";
                    }
                    else
                    {
                        cell2.lblDiscountValue.text=@"0";
                        cell2.txtDiscountNonStan.text=@"0";
                        
                        cell2.lblDiscountPercent.text=@"0";
                        cell2.txtDiscountPercent.text=@"0";
                        
                    }
                }
            }
            else
            {
                cell2.txtDiscountNonStan.hidden=YES;
                cell2.lblDiscountValue.hidden=NO;
                
                cell2.txtDiscountPercent.hidden=YES;
                cell2.lblDiscountPercent.hidden=NO;
                
            }
            cell2.txtDiscountNonStan.tag=indexPath.row;
            cell2.txtDiscountPercent.tag=indexPath.row;
            
        }
        
        else
        {
            cell2.txtDiscountNonStan.hidden=YES;
            cell2.lblDiscountValue.hidden=NO;
            
            cell2.txtDiscountPercent.hidden=YES;
            cell2.lblDiscountPercent.hidden=NO;
            
        }
        cell2.txtDiscountNonStan.tag=indexPath.row;
        cell2.txtDiscountPercent.tag=indexPath.row;
        //End
        
        
        
        
        //.....................................................................
        
        double diff;
        diff=[[arrNonStanInitialPrice objectAtIndex:indexPath.row]doubleValue]-[[arrNonStanDiscount objectAtIndex:indexPath.row]doubleValue];
        cell2.lblMaintenanceValue.text=[NSString stringWithFormat:@"$%.2f",diff];
        
        if ([[arrNonStanIsSold objectAtIndex:indexPath.row] isEqualToString:@"true"])
        {
            //[cell2.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            [cell2.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"]];
        }
        else if ([[arrNonStanIsSold objectAtIndex:indexPath.row] isEqualToString:@"false"])
        {
            //[cell2.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            [cell2.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"]];
        }
        
        
        //Billing Freq Calculation
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        NSString *strBillingFreqYearOccurence;
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
        if([strBillingFreqYearOccurence isEqualToString:@"(null)"])
        {
            strBillingFreqYearOccurence=@"0";
        }
        float totalBillingFreqCharge=0.0;
        @try {
            totalBillingFreqCharge=([[arrNonStanInitialPrice objectAtIndex:indexPath.row]floatValue]-[[arrNonStanDiscount objectAtIndex:indexPath.row]floatValue]) /[strBillingFreqYearOccurence floatValue];
            // totalBillingFreqCharge=totalBillingFreqCharge-[[arrNonStanDiscount objectAtIndex:indexPath.row]floatValue];
            
        } @catch (NSException *exception) {
            totalBillingFreqCharge=0.0;
        } @finally {
            
        }
        //strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
        strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
        
        cell2.lblBillingFreqValue.text=strBillingFreq;
        //End
        
        
        return cell2;
    }
    else if (tableView.tag==2) //bUndle
    {
        static NSString *identifier=@"cellId";
        // SalesAutomationServiceTableViewCell *
        cell1=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell1==nil)
        {
            cell1=[[SalesAutomationServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell1.lblFrequency.text=@"dsfdsfdsf";
            
        }
        NSString *strBundellll=[arrBundleRow objectAtIndex:indexPath.section];
        if (strBundellll.length==0 || [strBundellll isEqualToString:@""] || [strBundellll isEqual:nil])
        {
            
        }
        else
        {
            [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:indexPath.section]];
        }
        
        if (chkStatus==YES)
        {
            cell1.btnImgStandardService.enabled=NO;
            cell1.btnPlusService.enabled=NO;
            cell1.txtUnit.enabled=NO;
            
        }
        cell1.btnPlusService.hidden=YES;
        chkkkk=NO;
        
        //cell1.backgroundColor=[UIColor lightGrayColor];
        cell1.btnImgStandardService.tag=indexPath.row;
        
        NSLog(@"cell1.btnImgStandardService.tag %ld",(long)cell1.btnImgStandardService.tag);
        
        NSLog(@"cell1.imgView.tag %ld",(long)cell1.imgView.tag);
        NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        /* if ([arrFinalIndexPath containsObject:str])
         {
         
         [cell1.imgView setImage:[UIImage imageNamed:@"check_box_2.png"]];
         }
         else
         {
         
         [cell1.imgView setImage:[UIImage imageNamed:@"check_box_1.png"]];
         
         }
         */
        
        //[cell1.btnImgStandardService addTarget:self
        //action:@selector(buttonClickedStandardService:) forControlEvents:UIControlEventTouchDown];
        
        
        NSString *str12=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]];
        if ([str12 isEqualToString:@"0"]||[str isEqualToString:@"(null)"]||str12.length==0)
        {
            cell1.const_viewUnit_H.constant=0;
            
            
        }
        else
        {
            cell1.const_viewUnit_H.constant=65;
            
            
        }
        
        if ([[arrInitialPriceBundle objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblInitialPriceValue.text=@"TBD";
        }
        else
        {
            cell1.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrInitialPriceBundle objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrMaintenancePriceBundle objectAtIndex:indexPath.row]isEqualToString:@"TBD"])
        {
            cell1.lblMaintenanceValue.text=@"TBD";
        }
        else
        {
            cell1.lblMaintenanceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrMaintenancePriceBundle objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrFinalInitialPriceBundle objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblFinalInitialPrice.text=@"TBD";
        }
        else
        {
            cell1.lblFinalInitialPrice.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalInitialPriceBundle objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrFinalMaintPriceBundle objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblFinalMaintPrice.text=@"TBD";
        }
        else
        {
            cell1.lblFinalMaintPrice.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalMaintPriceBundle objectAtIndex:indexPath.row] doubleValue]];
        }
        
        
        
        //Nilind 25 May
        //End
        cell1.lblUnit.hidden=YES;
        //cell1.lblUnit.text=[arrUnit objectAtIndex:indexPath.row];
        cell1.txtUnit.text=[arrUnitBundle objectAtIndex:indexPath.row];
        
        //End
        
        
        cell1.lblDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrDiscountBundle objectAtIndex:indexPath.row]doubleValue]];
        
        cell1.lblDiscountPercent.text=[NSString stringWithFormat:@"%.2f",[[arrDiscountPerBundle objectAtIndex:indexPath.row]doubleValue]];
        
        
        
        cell1.lblFrequency.text=[dictFreqNameFromSysName valueForKey:[arrFrequencyNameBundle objectAtIndex:indexPath.row]];
        
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        //Billing Freq Calculation
        
        float totalParaInitial=0.0,totalParaMaint=0.0;
        
        totalParaInitial=totalParaInitial+([[arrInitialPriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
        
        if (indexPath.row==0) {
            
            globalAmountInitialPrice=totalParaInitial;
            
        }
        
        totalParaInitial=totalParaInitial-[[arrDiscountBundle objectAtIndex:indexPath.row]floatValue];
        
        totalParaMaint=totalParaMaint+([[arrMaintenancePriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
        
        
        if (indexPath.row==0) {
            
            globalAmountMaintenancePrice=totalParaMaint;
            
        }
        
        NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
        strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFrequencyNameBundle objectAtIndex:indexPath.row]]];
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
        float totalBillingFreqCharge=0.0;
        
        if ([[arrFrequencyNameBundle objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFrequencyNameBundle objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
        {
            
            totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            //  strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        else
        {
            totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        cell1.lblBillingFrequency.text=strBillingFreq;
        
        //End
        
        //Nilind 02 June
        
        //cell1.lblServiceType.text=[arrServiceName objectAtIndex:indexPath.row];
        
        NSString *strServiceWithPackage,*strPackage;
        strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameIdBundle objectAtIndex:indexPath.row]]];
        if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
        {
            strPackage=@"";
        }
        strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",[arrServiceNameBundle objectAtIndex:indexPath.row],strPackage];
        
        //For Bundle
        
        NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
        if ([strBundle isEqualToString:@"0"])
        {
        }
        else
        {
            strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
            if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
            {
                strPackage=@"";
            }
            strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,[arrServiceNameBundle objectAtIndex:indexPath.row]];
            
        }
        
        //End
        
        
        cell1.lblServiceType.text=strServiceWithPackage;
        [cell1.lblServiceType sizeToFit];
        //End
        
        cell1.lblServiceSysName.text=[arrSysNameBundle objectAtIndex:indexPath.row];
        NSString *strSysNameService=[arrSysNameBundle objectAtIndex:indexPath.row];
        
        [self chkForPlusServiceButton:strSysNameService];
        if (chkkkk==YES)
        {
            //cell1.btnPlusService.hidden=NO;
            cell1.btnPlusService.hidden=YES;
            
        }
        else
        {
            cell1.btnPlusService.hidden=YES;
        }
        
#pragma mark- change sysname array
        //Nilind 6 Oct
        
        cell1.btnPlusService.tag=indexPath.row;
        [cell1.btnPlusService addTarget:self action:@selector(plusService:) forControlEvents:UIControlEventTouchUpInside];
        cell1.txtStanDiscount.text=[arrDiscountBundle objectAtIndex:indexPath.row];
        cell1.txtDiscountPercent.text=[arrDiscountPerBundle objectAtIndex:indexPath.row];
        
        if (isCheckBoxClickFirstTime==YES)
        {
            if ([strCouponStatus isEqualToString:@"true"])
            {
                if (isChkBoxEnable==YES)
                {
                    cell1.txtStanDiscount.hidden=NO;
                    cell1.lblDiscountValue.hidden=YES;
                    cell1.txtDiscountPercent.hidden=NO;
                    cell1.lblDiscountPercent.hidden=YES;
                    cell1.txtStanDiscount.text=[arrDiscountBundle objectAtIndex:indexPath.row];
                }
                else
                {
                    cell1.txtStanDiscount.hidden=YES;
                    cell1.lblDiscountValue.hidden=NO;
                    
                    cell1.txtDiscountPercent.hidden=YES;
                    cell1.lblDiscountPercent.hidden=NO;
                    if([cell1.lblInitialPriceValue.text isEqualToString:@"TBD"])
                    {
                        cell1.lblDiscountValue.text=@"TBD";
                        cell1.txtStanDiscount.text=@"TBD";
                        cell1.lblDiscountPercent.text=@"TBD";
                        cell1.txtDiscountPercent.text=@"TBD";
                        
                    }
                    else
                    {
                        cell1.lblDiscountValue.text=@"0";
                        cell1.txtStanDiscount.text=@"0";
                        cell1.lblDiscountPercent.text=@"0";
                        cell1.txtDiscountPercent.text=@"0";
                        
                    }
                }
            }
            else
            {
                cell1.txtStanDiscount.hidden=YES;
                cell1.lblDiscountValue.hidden=NO;
                cell1.txtDiscountPercent.hidden=YES;
                cell1.lblDiscountPercent.hidden=NO;
                
                
            }
            cell1.txtStanDiscount.tag=indexPath.row;
            cell1.txtDiscountPercent.tag=indexPath.row;
            
        }
        else
        {
            cell1.txtStanDiscount.hidden=YES;
            cell1.lblDiscountValue.hidden=NO;
            
            cell1.txtDiscountPercent.hidden=YES;
            cell1.lblDiscountPercent.hidden=NO;
        }
        cell1.txtUnit.tag=indexPath.row;
        return cell1;
    }
    else if (tableView.tag==101)
    {
        static NSString *identifier=@"SelectServiceCouponDiscountTableViewCell";
        SelectServiceCouponDiscountTableViewCell *cellCoupon=[_tblCouponDiscount dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[SelectServiceCouponDiscountTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSManagedObject *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
        
        cellCoupon.lblCouponName.text=[NSString stringWithFormat:@"%@",[dictDiscountNameFromSysName valueForKey:[dict valueForKey:@"discountSysName"]]];
        
        //  cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountAmount"]];
        
        
        cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"$%.2f",[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue]];
        
        cellCoupon.lbCouponDescription.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
        return cellCoupon;
        
    }
    else if (tableView.tag==300)
    {
        static NSString *identifier=@"ClarkPestSelectService";//
        ClarkPestSelectServiceTableViewCell *cellService=[_tblService dequeueReusableCellWithIdentifier:identifier];
        if (cellService==nil)
        {
            cellService=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellService.btnAddToAgreement.tag=indexPath.row;
        [cellService.btnAddToAgreement addTarget:self
                                          action:@selector(buttonClickedCheckBoxAgreementService:) forControlEvents:UIControlEventTouchDown];
        cellService.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        NSManagedObject *matchedServiceDetail=[arrSavedServiceDetail objectAtIndex:indexPath.row];
        cellService.lblServiceName.text=[dictServiceName valueForKey:[matchedServiceDetail valueForKey:@"serviceSysName"]] ;
        NSString *strDesc=[self convertHTML:[matchedServiceDetail valueForKey:@"serviceDescription"]];
        
        
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil]|| [strDesc isKindOfClass:[NSNull class]] || [strDesc isEqualToString:@"<null>"]||[strDesc isEqualToString:@"(null)"]||[strDesc isEqual:NULL])
        {
            cellService.lblDescription.text=@" ";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            //cellService.lblDescription.text=strDesc;
            cellService.lblDescription.attributedText = [global getAttributedString:[matchedServiceDetail valueForKey:@"serviceDescription"] WithFontStyle:[UIFont systemFontOfSize:14]];

        }
        [cellService.lblDescription sizeToFit];
        if ([[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"true"]||[[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"1"])
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        if ([[matchedServiceDetail valueForKey:@"isChangeServiceDesc"]isEqualToString:@"true"]||[[matchedServiceDetail valueForKey:@"isChangeServiceDesc"]isEqualToString:@"1"])
        {
            [cellService.btnIsChanged setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [cellService.btnIsChanged setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        cellService.lblInitialPrice.text = [NSString stringWithFormat:@"$ %@",[matchedServiceDetail valueForKey:@"initialPrice"]];
        cellService.lblMaintenancePrice.text = [NSString stringWithFormat:@"$ %@",[matchedServiceDetail valueForKey:@"maintenancePrice"]];
        cellService.lblFrequency.text = [NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"serviceFrequency"]];
        
        return cellService;
        
    }
    else if (tableView.tag==301)
    {
        static NSString *identifier=@"ClarkPestSelectServiceScope";
        ClarkPestSelectServiceTableViewCell *cellScope=[_tblScope dequeueReusableCellWithIdentifier:identifier];
        if (cellScope==nil)
        {
            cellScope=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellScope.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSManagedObject *matchedScopeDetail=[arrSavedScopeDetail objectAtIndex:indexPath.row];
        cellScope.lblServiceName.text=[matchedScopeDetail valueForKey:@"title"];
        //cellScope.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        
        
        
        
        NSString *strDesc=[self convertHTML:[matchedScopeDetail valueForKey:@"scopeDescription"]];
        
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellScope.lblDescription.text=@" ";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            //cellScope.lblDescription.text=strDesc;
            cellScope.lblDescription.attributedText = [global getAttributedString:[matchedScopeDetail valueForKey:@"scopeDescription"] WithFontStyle:[UIFont systemFontOfSize:14]];

        }
        if ([[matchedScopeDetail valueForKey:@"isChangedScopeDesc"]isEqualToString:@"true"]||[[matchedScopeDetail valueForKey:@"isChangedScopeDesc"]isEqualToString:@"1"])
        {
            [cellScope.btnIsChanged setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            
            
        }
        else
        {
            [cellScope.btnIsChanged setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        
        
        return cellScope;
        
    }
    else if (tableView.tag==302)
    {
        static NSString *identifier=@"ClarkPestSelectServiceTarget";
        ClarkPestSelectServiceTableViewCell *cellTarget=[_tblTarget dequeueReusableCellWithIdentifier:identifier];
        if (cellTarget==nil)
        {
            cellTarget=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellTarget.selectionStyle=UITableViewCellSelectionStyleNone;
        NSManagedObject *matchedTargetDetail=[arrSavedTargetDetail objectAtIndex:indexPath.row];
        cellTarget.lblServiceName.text=[matchedTargetDetail valueForKey:@"name"];
        //dgdf dfg dfgd fgcellTarget.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        
        NSString *strDesc=[self convertHTML:[matchedTargetDetail valueForKey:@"targetDescription"]];
        
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellTarget.lblDescription.text=@" ";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            //cellTarget.lblDescription.text=strDesc;
            cellTarget.lblDescription.attributedText = [global getAttributedString:[matchedTargetDetail valueForKey:@"targetDescription"] WithFontStyle:[UIFont systemFontOfSize:14]];

        }
        
        if ([[matchedTargetDetail valueForKey:@"isChangedTargetDesc"]isEqualToString:@"true"]||[[matchedTargetDetail valueForKey:@"isChangedTargetDesc"]isEqualToString:@"1"])
        {
            [cellTarget.btnIsChanged setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            
            
        }
        else
        {
            [cellTarget.btnIsChanged setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        cellTarget.btnAddTargetImages.tag=indexPath.row;
        [cellTarget.btnAddTargetImages addTarget:self
                                          action:@selector(buttonClickedAddTargetImages:) forControlEvents:UIControlEventTouchDown];
        cellTarget.textLabel.font=[UIFont systemFontOfSize:16];
        
        
        return cellTarget;
        
    }
    else if (tableView.tag==303)
    {
        static NSString *identifier=@"CellInitialPrice1";
        CellInitialPrice1 *cellInitialPrice=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellInitialPrice==nil)
        {
            cellInitialPrice=[[CellInitialPrice1 alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellInitialPrice.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSDictionary *dict = [arrSavedInitialPriceDetail objectAtIndex:indexPath.row];
        
        cellInitialPrice.labelInitialPrice.text=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:@"initialPrice"]floatValue]];
        NSString *strDesc=[dict valueForKey:@"initialDescription"];
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellInitialPrice.labelInitialDescription.text=@" ";
        }
        else
        {
            cellInitialPrice.labelInitialDescription.text=strDesc;
        }
        
        return cellInitialPrice;
        
    }
    else if (tableView.tag==304)
    {
        static NSString *identifier=@"CellMaintPrice1";
        CellMaintPrice1 *cellMaintPrice=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellMaintPrice==nil)
        {
            cellMaintPrice=[[CellMaintPrice1 alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellMaintPrice.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSDictionary *dict = [arrSavedMaintPriceDetail objectAtIndex:indexPath.row];
        
        cellMaintPrice.labelMaintPrice.text=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:@"maintenancePrice"]floatValue]];
        cellMaintPrice.labelServiceFrequency.text = [dict valueForKey:@"frequencySysName"];
        NSString *strDesc=[dict valueForKey:@"maintenanceDescription"];
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellMaintPrice.labelMaintDescription.text=@" ";
        }
        else
        {
            cellMaintPrice.labelMaintDescription.text=strDesc;
        }
        
        return cellMaintPrice;
        
    }
    else if (tableView.tag==305)
    {
        static NSString *identifier=@"ClarkPestTableViewCellNonStandard";
        ClarkPestTableViewCellNonStandard *cellService=[_tblNonStandardClarkPest dequeueReusableCellWithIdentifier:identifier];
        if (cellService==nil)
        {
            cellService=[[ClarkPestTableViewCellNonStandard alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellService.btnAddToAgreement.tag=indexPath.row;
        [cellService.btnAddToAgreement addTarget:self
                                          action:@selector(buttonClickedCheckBoxAgreementNonStanadrdService:) forControlEvents:UIControlEventTouchDown];
        cellService.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        NSManagedObject *matchedServiceDetail=[arrSavedNonStanServiceDetail objectAtIndex:indexPath.row];
        
        cellService.lblServiceName.text = [matchedServiceDetail valueForKey:@"serviceName"] ;
        
        //NSString *strDesc=[self convertHTML:[matchedServiceDetail valueForKey:@"serviceDescription"]];
        NSString *strDesc=[matchedServiceDetail valueForKey:@"serviceDescription"];

        
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil]|| [strDesc isKindOfClass:[NSNull class]] || [strDesc isEqualToString:@"<null>"]||[strDesc isEqualToString:@"(null)"]||[strDesc isEqual:NULL])
        {
            cellService.lblDescription.text=@" ";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            //cellService.lblDescription.text=strDesc;
            NSAttributedString *attributedStringHTML = [[NSAttributedString alloc]
                                                    initWithData: [[matchedServiceDetail valueForKey:@"serviceDescription"] dataUsingEncoding:NSUnicodeStringEncoding]
                                                    options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                    documentAttributes: nil
                                                    error: nil
                                                    ];
            
            cellService.lblDescription.attributedText=attributedStringHTML;
            
        }
        
        [cellService.lblDescription sizeToFit];
        
        if ([[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"true"]||[[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"1"])
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        cellService.lblInitialPrice.text = [NSString stringWithFormat:@"$ %@",[matchedServiceDetail valueForKey:@"initialPrice"]];
        cellService.lblMaintenancePrice.text = [NSString stringWithFormat:@"$ %@",[matchedServiceDetail valueForKey:@"maintenancePrice"]];
        
        if ([[NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"isBidOnRequest"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"isBidOnRequest"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"isBidOnRequest"]] isEqualToString:@"True"])

        {
            cellService.lblInitialPrice.text = @"Bid On Request";
            cellService.lblMaintenancePrice.text = @"Bid On Request";
        }
        
        
        cellService.lblFrequency.text = [NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"serviceFrequency"]];
        
        /*if ([[matchedServiceDetail valueForKey:@"isChangeServiceDesc"]isEqualToString:@"true"]||[[matchedServiceDetail valueForKey:@"isChangeServiceDesc"]isEqualToString:@"1"])
         {
         [cellService.btnIsChanged setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
         
         }
         else
         {
         [cellService.btnIsChanged setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
         
         }*/
        
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            cellService.btnAddToAgreement.enabled = NO;
        }
        else
        {
            cellService.btnAddToAgreement.enabled = YES;
            
        }
        
        return cellService;
        
    }
    else if(tableView.tag == 70)
    {
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        
        NSDictionary *dict=[arrCategoryPopUp objectAtIndex:indexPath.row];
        
        cell.textLabel.text=[dict valueForKey:@"Name"];
        return cell;
    }
    else if(tableView.tag == 71)
    {
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        NSDictionary *dict=[arrayService objectAtIndex:indexPath.row];
        cell.textLabel.text=[dict valueForKey:@"Name"];
        return cell;
    }
    else if (tableView.tag == 72)
    {
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        NSDictionary *dict=[arrayScope objectAtIndex:indexPath.row];
        cell.textLabel.text=[dict valueForKey:@"Title"];
        return cell;
    }
    else if (tableView.tag == 73)
    {
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        NSDictionary *dict=[arrayTarget objectAtIndex:indexPath.row];
        cell.textLabel.text=[dict valueForKey:@"Name"];
        // cell.textLabel.text=[arrayTarget objectAtIndex:indexPath.row];
        return cell;
        
    }
    else if (tableView.tag == 74)
    {
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        NSDictionary *dict=[arrProposedFreq objectAtIndex:indexPath.row];
        cell.textLabel.text=[dict valueForKey:@"FrequencyName"];
        return cell;
        
    }
    else if (tableView.tag == 75)
    {
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        
        cell.textLabel.text=[arrProposedMonth objectAtIndex:indexPath.row];
        return cell;
        
    }
    // Letter Template Changes
    else if (tableView.tag==10) //Cover Letter
    {
        static NSString *identifier=@"demo";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@",[[arrCoverLetter objectAtIndex:indexPath.row] valueForKey:@"TemplateName"]];
        if(dictCoverLetter.count>0)
        {//
            if([cellCoupon.textLabel.text isEqualToString:[dictCoverLetter valueForKey:@"TemplateName"]])
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else
        {
            cellCoupon.accessoryType = UITableViewCellAccessoryNone;
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==11) //Introduction Letter
    {
        static NSString *identifier=@"demo";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@",[[arrIntroductionLetter objectAtIndex:indexPath.row] valueForKey:@"TemplateName"]];
        
        if(dictIntroLetter.count>0)
        {
            if([cellCoupon.textLabel.text isEqualToString:[dictIntroLetter valueForKey:@"TemplateName"]])
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else
        {
            cellCoupon.accessoryType = UITableViewCellAccessoryNone;
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==13) //Terms of services
    {
        static NSString *identifier=@"demoTemrs service";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@",[[arrTermsOfService objectAtIndex:indexPath.row] valueForKey:@"Title"]];
        
        if(dictTermsOfService.count>0)
        {
            if([cellCoupon.textLabel.text isEqualToString:[dictTermsOfService valueForKey:@"Title"]])
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else
        {
            cellCoupon.accessoryType = UITableViewCellAccessoryNone;
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==14) //Marketing Content
    {
        static NSString *identifier=@"MarketingContent";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSDictionary *dict=[arrMarketingContent objectAtIndex:indexPath.row];
        NSString *strMultiTerms=[dict valueForKey:@"Title"];
        NSString *strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        // strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadCommercialTermsId"]];
        
        if([strTermsId isEqualToString:@"(null)"]|| [strTermsId isEqual:nil] || [strTermsId isKindOfClass:[NSNull class]])
        {
            strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"contentSysName"]];
            strMultiTerms=[dictTitleFromTermId valueForKey:[dict valueForKey:@"contentSysName"]];
            
        }
        
        cellCoupon.textLabel.text=strMultiTerms;
        
        BOOL chkExist;
        chkExist=NO;
        for (int i=0; i<arrSelectedMarketingContent.count; i++)
        {
            if ([[arrSelectedMarketingContent objectAtIndex:i]isEqualToString:strTermsId])
            {
                chkExist=YES;
                break;
            }
            else
            {
                chkExist=NO;
            }
        }
        if (chkExist==YES)
        {
            [cellCoupon setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cellCoupon setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            cellCoupon.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        return cellCoupon;
        
    }
    //End
    else
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.textLabel.text=[arrCategory objectAtIndex:indexPath.row];
        if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
        {
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        return cell;
    }
}
-(void)buttonClickedStandardService:(UIButton*)sender
{
    isEditedInSalesAuto=YES;
    [self fetchFromCoreDataStandard];
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    
    // NSIndexPath *indexPath1 = [_tblRecord indexPathForCell:(UITableViewCell *)sender.superview.superview];
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    SalesAutomationServiceTableViewCell *tappedCell = (SalesAutomationServiceTableViewCell *)[_tblRecord cellForRowAtIndexPath:indexpath];
    
    
    if ([tappedCell.imgView.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        [tappedCell.imgView setImage:[UIImage imageNamed:@"check_box_1.png"]];
        chkBtnCheckBoxStan=NO;
        
        //Old
        /* if (arrSysNameConditionalStandard.count>=indexpath.row) {
         
         [arrSysNameConditionalStandard removeObject:arrSysName[indexpath.row]];
         
         NSString *strPath=[NSString stringWithFormat:@"%ld",(long)indexpath.row];
         
         [arrFinalIndexPath removeObject:strPath];
         
         }*/
        if ([arrFinalIndexPath containsObject:[NSString stringWithFormat:@"%ld",(long)indexpath.row]]) {
            
            [arrSysNameConditionalStandard removeObject:arrSysName[indexpath.row]];
            
            NSString *strPath=[NSString stringWithFormat:@"%ld",(long)indexpath.row];
            
            [arrFinalIndexPath removeObject:strPath];
            
        }
        
        /*if (arrSysNameConditionalStandard.count>0) {
         
         [arrSysNameConditionalStandard removeObject:arrSysName[indexpath.row]];
         
         NSString *strPath=[NSString stringWithFormat:@"%ld",(long)indexpath.row];
         
         [arrFinalIndexPath removeObject:strPath];
         
         }*/
        
    }
    else
    {
        BOOL isMatched;
        int indexxMatched=9999999;
        isMatched=NO;
        //NSString *strPath=[NSString stringWithFormat:@"%ld",(long)indexpath.row];
        
        //[arrFinalIndexPath removeObject:strPath];
        for (int k1=0; k1<arrSysNameConditionalStandard.count; k1++) {
            
            NSString *strConditional=arrSysNameConditionalStandard[k1];
            NSString *strssysname=arrSysName[indexpath.row];
            
            if ([strConditional isEqualToString:strssysname]) {
                
                indexxMatched=k1;
                isMatched=YES;
                
            }
        }
        
        if (isMatched)
        {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:@"Service already sold do yo want to continue"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     NSString *strIndex=[NSString stringWithFormat:@"%@",arrFinalIndexPath[indexxMatched]];
                                     
                                     NSManagedObject *record = [arrAllObj objectAtIndex:[strIndex integerValue]];
                                     
                                     [self updateStadardNew:record];
                                     
                                     [arrFinalIndexPath removeObjectAtIndex:indexxMatched];
                                     [arrSysNameConditionalStandard removeObjectAtIndex:indexxMatched];
                                     
                                     chkBtnCheckBoxStan=YES;
                                     [arrFinalIndexPath addObject:[NSString stringWithFormat:@"%ld",(long)indexpath.row]];
                                     NSLog(@"Final Index array %@",arrFinalIndexPath);
                                     NSLog(@"Final IndexArray count %lu",(unsigned long)arrFinalIndexPath.count);
                                     //[tappedCell.btnImgStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
                                     [tappedCell.imgView setImage:[UIImage imageNamed:@"check_box_2.png"]];
                                     
                                     NSString *strSysNameConditional=arrSysName[indexpath.row];
                                     
                                     [arrSysNameConditionalStandard addObject:strSysNameConditional];
                                     //Nilind 28 Dec
                                     
                                     /* chkBox=YES;
                                      NSManagedObject *recordCheckBox = [arrAllObj objectAtIndex:indexpath.row];
                                      [self updateStadardForFinalSave:recordCheckBox];*/
                                     //............
                                     NSLog(@"arrFinalIndexPath>>%@",arrFinalIndexPath);
                                     
                                     [_tblRecord reloadData];
                                     
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            
            chkBtnCheckBoxStan=YES;
            [arrFinalIndexPath addObject:[NSString stringWithFormat:@"%ld",(long)indexpath.row]];
            NSLog(@"Final Index array %@",arrFinalIndexPath);
            NSLog(@"Final IndexArray count %lu",(unsigned long)arrFinalIndexPath.count);
            //[tappedCell.btnImgStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            [tappedCell.imgView setImage:[UIImage imageNamed:@"check_box_2.png"]];
            
            NSString *strSysNameConditional=arrSysName[indexpath.row];
            
            [arrSysNameConditionalStandard addObject:strSysNameConditional];
            
        }
    }
    
    NSManagedObject *record = [arrAllObj objectAtIndex:sender.tag];//[self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath1];
    [self updateStadard:record];
    if(isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in buttonClickedStandardService");
        [global updateSalesModifydate:strLeadId];
    }
}
-(void)updateStadardNew:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    
    NSError *error1 = nil;
    matches=indexValue;
    //    if (chkBtnCheckBoxStan==YES)
    //    {
    //        [matches setValue:@"true" forKey:@"isSold"];
    //    }
    //    else
    //    {
    [matches setValue:@"false" forKey:@"isSold"];
    //    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataStandard];
}
-(void)buttonClickedNonStandardService:(UIButton*)sender
{
    isEditedInSalesAuto=YES;
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexPath1 = [_tblNonStandardService indexPathForCell:(UITableViewCell *)sender.superview.superview];
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    SalesAutomationTableViewCellSecond *tappedCell = (SalesAutomationTableViewCellSecond *)[_tblNonStandardService cellForRowAtIndexPath:indexpath];
    //if ([tappedCell.btnImageNonStandardService.imageView.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    if ([tappedCell.imgViewNonStandardService.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        
        // [tappedCell.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [tappedCell.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"]];
        chkBtnCheckBoxNonStan=NO;
        for(int i=0;i<arrFinalIndexPathNonStan.count;i++)
        {
            if([[arrFinalIndexPathNonStan objectAtIndex:i]isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath1.row]])
            {
                [arrFinalIndexPathNonStan removeObjectAtIndex:i];
                
            }
            NSLog(@"After Removing New Final IndexArray%@",arrFinalIndexPathNonStan);
            NSLog(@"After Removing New Final IndexArray count %lu",(unsigned long)arrFinalIndexPathNonStan.count);
        }
        
    }
    else
    {
        chkBtnCheckBoxNonStan=YES;
        [arrFinalIndexPathNonStan addObject:[NSString stringWithFormat:@"%ld",(long)indexPath1.row]];
        NSLog(@"Final Index array %@",arrFinalIndexPathNonStan);
        NSLog(@"Final IndexArray count %lu",(unsigned long)arrFinalIndexPathNonStan.count);
        
        //[tappedCell.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [tappedCell.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    [self fetchFromCoreDataNonStandard];
    NSManagedObject *record = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath1];
    [self updateNonStadard:record];
    if(isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in buttonClickedNonStandardService");
        [global updateSalesModifydate:strLeadId];
    }
}

-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==300) // standard service
    {
        
        UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                              
                                               {
            
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
            
            AddStandardNonStandardServiceVC
            
            *objAddStandardNonStandardVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddStandardNonStandardServiceVC"];
            objAddStandardNonStandardVC.strWoId = strLeadId;
            objAddStandardNonStandardVC.strBranchSysName = strBranchSysName;
            objAddStandardNonStandardVC.strForEdit = @"EditStandard";
            NSManagedObject *matchedServiceDetail=[arrSavedServiceDetail objectAtIndex:indexPath.row];
            objAddStandardNonStandardVC.matchesServiceEdit = matchedServiceDetail;

            //strWoId
            [self.navigationController pushViewController:objAddStandardNonStandardVC animated:NO];
            
          /*  UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
            
            TextEditorNormal *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"TextEditorNormal"];
                        
            NSManagedObject *matchedServiceDetail=[arrSavedServiceDetail objectAtIndex:indexPath.row];

            NSString *strDesc=[matchedServiceDetail valueForKey:@"serviceDescription"];
            NSString *strId=[matchedServiceDetail valueForKey:@"soldServiceStandardId"];

            objSalesAutomationAgreementProposal.strHtml=strDesc;
            
            objSalesAutomationAgreementProposal.strLeadId = strLeadId;
            objSalesAutomationAgreementProposal.strServiceId = strId;
            
            objSalesAutomationAgreementProposal.strfrom=@"SalesCommercialService";
            objSalesAutomationAgreementProposal.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:objSalesAutomationAgreementProposal animated:NO completion:nil];*/
            
            
        }];
        
            UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                                     {
                
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Alert!"
                                              message:@"Are you sure want to delete"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                    
                    // [self fetchFromCoreDataStandardClarkPest];
                    NSManagedObject *dict=[arrSavedServiceDetail objectAtIndex:indexPath.row];
                    NSString *strSysName=[dict valueForKey:@"serviceSysName"];
                    [self deleteFromCoreDataSalesInfoClarkPest:strSysName];
                    [self fetchFromCoreDataStandardClarkPest];
                    
                    
                }];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Cancel"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                    
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alert addAction:ok];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
                
            }];
        
        rowActionDelete.backgroundColor=[UIColor redColor];
        rowActionEdit.backgroundColor=[UIColor grayColor];
        return @[rowActionDelete,rowActionEdit];
        return @[rowActionEdit];
    }
    else if (tableView.tag==301) // scope
    {
        
        UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit Description" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                              
                                               {
            
            UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
            
            TextEditorNormal *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"TextEditorNormal"];
            
            //objSalesAutomationAgreementProposal.strHtml=[arrNonStanServiceDesc objectAtIndex:indexPath.row];
            
            NSManagedObject *matchedServiceDetail=[arrSavedScopeDetail objectAtIndex:indexPath.row];

            NSString *strDesc=[matchedServiceDetail valueForKey:@"scopeDescription"];
            NSString *strId=[matchedServiceDetail valueForKey:@"scopeSysName"];

            objSalesAutomationAgreementProposal.strHtml=strDesc;
            
            objSalesAutomationAgreementProposal.strLeadId = strLeadId;
            objSalesAutomationAgreementProposal.strServiceId = strId;
            
            objSalesAutomationAgreementProposal.strfrom=@"SalesCommercialScope";
            objSalesAutomationAgreementProposal.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:objSalesAutomationAgreementProposal animated:NO completion:nil];
            
            
        }];
        
            UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                                     {
                
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Alert!"
                                              message:@"Are you sure want to delete"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                    
                    [self fetchScopeFromCoreData];
                    NSManagedObject *dict=[arrSavedScopeDetail objectAtIndex:indexPath.row];
                    NSString *strSysName=[dict valueForKey:@"scopeSysName"];
                    [self deleteScopeFromCoreDataSalesInfoClarkPest:strSysName];
                    [self fetchScopeFromCoreData];
                    
                }];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Cancel"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                    
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alert addAction:ok];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
                
            }];
        
        rowActionDelete.backgroundColor=[UIColor redColor];
        rowActionEdit.backgroundColor=[UIColor grayColor];
        return @[rowActionDelete,rowActionEdit];
        return @[rowActionEdit];
    }
    else if (tableView.tag==302) // target
    {
        
        UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit Description" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                              
                                               {
            
            UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
            
            TextEditorNormal *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"TextEditorNormal"];
            
            //objSalesAutomationAgreementProposal.strHtml=[arrNonStanServiceDesc objectAtIndex:indexPath.row];
            
            NSManagedObject *matchedServiceDetail=[arrSavedTargetDetail objectAtIndex:indexPath.row];

            NSString *strDesc=[matchedServiceDetail valueForKey:@"targetDescription"];
            NSString *strId=[matchedServiceDetail valueForKey:@"targetSysName"];

            objSalesAutomationAgreementProposal.strHtml=strDesc;
            
            objSalesAutomationAgreementProposal.strLeadId = strLeadId;
            objSalesAutomationAgreementProposal.strServiceId = strId;
            
            objSalesAutomationAgreementProposal.strfrom=@"SalesCommercialTarget";
            objSalesAutomationAgreementProposal.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:objSalesAutomationAgreementProposal animated:NO completion:nil];
            
            
        }];
        
            UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                                     {
                
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Alert!"
                                              message:@"Are you sure want to delete Target as well as images associated with this Target"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                    
                    [self fetchTargetFromCoreData];
                    NSManagedObject *dict=[arrSavedTargetDetail objectAtIndex:indexPath.row];
                    NSString *strSysName=[dict valueForKey:@"targetSysName"];
                    //[self deleteTargetFromCoreDataSalesInfoClarkPest:strSysName];
                    
                    [self deleteTargetFromCoreDataSalesInfoClarkPest:@"" TargetId:[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadCommercialTargetId"]] MobileId:[NSString stringWithFormat:@"%@",[dict valueForKey:@"mobileTargetId"]]];
                    
                    [self fetchTargetFromCoreData];
                    
                    //For Image
                    
                    WebService *objWebService = [[WebService alloc] init];
                    [objWebService deleteTargetImageDataFromDBWithStrLeadId:strLeadId strHeaderTitle:@"Target" strTargetSysName:strSysName];
                    
                    
                }];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Cancel"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                    
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alert addAction:ok];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }];
        
        rowActionDelete.backgroundColor=[UIColor redColor];
        rowActionEdit.backgroundColor=[UIColor grayColor];
        return @[rowActionDelete,rowActionEdit];
        return @[rowActionEdit];
    }
    else if (tableView.tag==305) //Non standard service
    {
        
        UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                              
                                               {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
            
            AddStandardNonStandardServiceVC
            
            *objAddStandardNonStandardVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddStandardNonStandardServiceVC"];
            objAddStandardNonStandardVC.strWoId = strLeadId;
            objAddStandardNonStandardVC.strBranchSysName = strBranchSysName;
            objAddStandardNonStandardVC.strForEdit = @"EditNonStandard";
            NSManagedObject *matchedServiceDetail=[arrSavedNonStanServiceDetail objectAtIndex:indexPath.row];
            objAddStandardNonStandardVC.matchesServiceEdit = matchedServiceDetail;
            [self.navigationController pushViewController:objAddStandardNonStandardVC animated:NO];
            
         /*   UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Appointment" bundle:nil];
            
            HTMLEditorVC *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"HTMLEditorVC"];
                        
            NSManagedObject *matchedServiceDetail=[arrSavedNonStanServiceDetail objectAtIndex:indexPath.row];

            NSString *strDesc=[matchedServiceDetail valueForKey:@"serviceDescription"];
            NSString *strId=[matchedServiceDetail valueForKey:@"soldServiceNonStandardId"];

            objSalesAutomationAgreementProposal.strHtml=strDesc;
            
            objSalesAutomationAgreementProposal.strLeadId = strLeadId;
            objSalesAutomationAgreementProposal.strServiceId = strId;
            
            objSalesAutomationAgreementProposal.strfrom=@"SalesCommercialNonStan";
            objSalesAutomationAgreementProposal.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:objSalesAutomationAgreementProposal animated:NO completion:nil];*/
            
            
        }];
        
            UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                                     {
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Alert!"
                                              message:@"Are you sure want to delete"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                    
                    NSManagedObject *dict=[arrSavedNonStanServiceDetail objectAtIndex:indexPath.row];
                    NSString *strsoldServiceNonStandardId=[dict valueForKey:@"soldServiceNonStandardId"];
                    [self deleteFromCoreDataSalesInfoNonStandardClarkPest:strsoldServiceNonStandardId];
                    [self fetchFromCoreDataNonStandardClarkPest];
                    
                    
                }];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Cancel"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                    
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alert addAction:ok];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
                
            }];
        
        rowActionDelete.backgroundColor=[UIColor redColor];
        rowActionEdit.backgroundColor=[UIColor grayColor];
        return @[rowActionDelete,rowActionEdit];
        return @[rowActionEdit];
    }
    else
    {
        return UITableViewRowActionStyleDefault;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==300 || tableView.tag==301 || tableView.tag==302 || tableView.tag==303 || tableView.tag==304 || tableView.tag==305)
    {
        if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
        {
            return NO;
        }
        else
        {
            return YES;
            
        }
    }
    
    else
    {
        return NO;
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==0)
    {
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     isEditedInSalesAuto=YES;
                                     NSLog(@"arrFinalIndexPath before delete ==%@",arrFinalIndexPath);
                                     NSLog(@"arrInitialPrice before delete ==%@",arrInitialPrice);
                                     NSLog(@"arrDiscount before delete ==%@",arrDiscount);
                                     NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
                                     chkDirectServiceDelete=YES;
                                     [self fetchFromCoreDataStandard];
                                     NSString *strId=[arrSoldServiceStandardId objectAtIndex:indexPath.row];
                                     [self deleteFromCoreDataSalesInfo:[arrSysName objectAtIndex:indexPath.row]:[arrSoldServiceStandardId objectAtIndex:indexPath.row]];
                                     
                                     [self fetchForAppliedDiscountServiceFromCoreData:strId];
                                     [self fetchFromCoreDataStandard];
                                     
                                     if ([arrFinalIndexPath containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                                         
                                         NSInteger index=[arrFinalIndexPath indexOfObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                                         
                                         [arrSysNameConditionalStandard removeObjectAtIndex:index];
                                         
                                         [arrFinalIndexPath removeObjectAtIndex:index];
                                         
                                     }
                                     
                                     [self fetchFromCoreDataStandardNewSaavan];
                                     
                                     [_tblRecord reloadData];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     chkDirectServiceDelete=NO;
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
            /*   NSLog(@"arrFinalIndexPath before delete ==%@",arrFinalIndexPath);
             NSLog(@"arrInitialPrice before delete ==%@",arrInitialPrice);
             NSLog(@"arrDiscount before delete ==%@",arrDiscount);
             NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
             
             
             NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath];
             [context deleteObject:managedObject];
             NSError *saveError = nil;
             [context save:&saveError];
             
             [self fetchFromCoreDataStandard];
             
             [_tblRecord reloadData];*/
        }
    }
    else if (tableView.tag==1)
    {
        
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     isEditedInSalesAuto=YES;
                                     NSLog(@"arrFinalIndexPath before delete==%@",arrFinalIndexPathNonStan);
                                     NSLog(@"arrInitialPrice before delete ==%@",arrNonStanInitialPrice);
                                     NSLog(@"arrDiscount before delete ==%@",arrNonStanDiscount);
                                     NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
                                     [self fetchFromCoreDataNonStandard];
                                     
                                     NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath];
                                     [context deleteObject:managedObject];
                                     NSError *saveError = nil;
                                     [context save:&saveError];
                                     
                                     [self fetchFromCoreDataNonStandard];
                                     
                                     [_tblNonStandardService reloadData];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            /* NSLog(@"arrFinalIndexPath before delete==%@",arrFinalIndexPathNonStan);
             NSLog(@"arrInitialPrice before delete ==%@",arrNonStanInitialPrice);
             NSLog(@"arrDiscount before delete ==%@",arrNonStanDiscount);
             NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
             
             
             NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath];
             [context deleteObject:managedObject];
             NSError *saveError = nil;
             [context save:&saveError];
             
             [self fetchFromCoreDataNonStandard];
             
             [_tblNonStandardService reloadData];*/
        }
    }
    else if (tableView.tag==101)
    {
        {
            
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [self fetchForAppliedDiscountFromCoreData];
                                     NSManagedObject *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
                                     
                                     [self deleteAppliedCouponFromCoreDataSalesInfo:dict:@"strDirect"];
                                     [self heightForDiscountCoupon];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
    }
    else if (tableView.tag==300)
    {
        
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 // [self fetchFromCoreDataStandardClarkPest];
                                 NSManagedObject *dict=[arrSavedServiceDetail objectAtIndex:indexPath.row];
                                 NSString *strSysName=[dict valueForKey:@"serviceSysName"];
                                 [self deleteFromCoreDataSalesInfoClarkPest:strSysName];
                                 [self fetchFromCoreDataStandardClarkPest];
                                 
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    else if (tableView.tag==301)
    {
        
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [self fetchScopeFromCoreData];
                                 NSManagedObject *dict=[arrSavedScopeDetail objectAtIndex:indexPath.row];
                                 NSString *strSysName=[dict valueForKey:@"scopeSysName"];
                                 [self deleteScopeFromCoreDataSalesInfoClarkPest:strSysName];
                                 [self fetchScopeFromCoreData];
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    else if (tableView.tag==302)
    {
        
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete Target as well as images associated with this Target"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [self fetchTargetFromCoreData];
                                 NSManagedObject *dict=[arrSavedTargetDetail objectAtIndex:indexPath.row];
                                 NSString *strSysName=[dict valueForKey:@"targetSysName"];
                                 //[self deleteTargetFromCoreDataSalesInfoClarkPest:strSysName];
                                 
                                 [self deleteTargetFromCoreDataSalesInfoClarkPest:@"" TargetId:[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadCommercialTargetId"]] MobileId:[NSString stringWithFormat:@"%@",[dict valueForKey:@"mobileTargetId"]]];
                                 
                                 [self fetchTargetFromCoreData];
                                 
                                 //For Image
                                 
                                 WebService *objWebService = [[WebService alloc] init];
                                 [objWebService deleteTargetImageDataFromDBWithStrLeadId:strLeadId strHeaderTitle:@"Target" strTargetSysName:strSysName];
                                 
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else if (tableView.tag==303)
    {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //[self fetchTargetFromCoreData];
                                 NSDictionary *dict=[arrSavedInitialPriceDetail objectAtIndex:indexPath.row];
                                 NSString *leadCommInitialInfoId=[dict valueForKey:@"leadCommInitialInfoId"];
                                 [self deleteLeadCommercialInitialInfoFromCoreData:leadCommInitialInfoId];
                                 [self fetchLeadCommercialInitialInfoFromCoreData];
                                 
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (tableView.tag==304)
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //[self fetchTargetFromCoreData];
                                 NSDictionary *dict=[arrSavedMaintPriceDetail objectAtIndex:indexPath.row];
                                 NSString *leadCommMaintInfoId=[dict valueForKey:@"leadCommMaintInfoId"];
                                 [self deleteLeadCommercialMaintInfoFromCoreData:leadCommMaintInfoId];
                                 [self fetchLeadCommercialMaintInfoFromCoreData];
                                 
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else if (tableView.tag == 305)
    {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 NSManagedObject *dict=[arrSavedNonStanServiceDetail objectAtIndex:indexPath.row];
                                 NSString *strsoldServiceNonStandardId=[dict valueForKey:@"soldServiceNonStandardId"];
                                 [self deleteFromCoreDataSalesInfoNonStandardClarkPest:strsoldServiceNonStandardId];
                                 [self fetchFromCoreDataNonStandardClarkPest];
                                 
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
}
-(void)addAction
{
    // show pop up
     [self endEditing];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select Options" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *actionTarget = [UIAlertAction actionWithTitle:@"Target" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // show pop up
        // selectionType = 3;
        selectionType = 1;
        _const_ViewPopUp_Category_H.constant=0;
        
        [self addPopUpView];
        
        
    }];
    UIAlertAction *actionScope = [UIAlertAction actionWithTitle:@"Scope" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // show pop up
        selectionType = 2;
        _const_ViewPopUp_Category_H.constant=0;
        
        [self addPopUpView];
        
        
    }];
    
    
    UIAlertAction *actionService = [UIAlertAction actionWithTitle:@"Service" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        
        /*selectionType = 3;
         
         _const_ViewPopUp_Category_H.constant=35;
         [self addPopUpView];*/
        
        
        [self goToAddStanNonStanService];
        
        
    }];
    
    UIAlertAction *actionDismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionTarget];
    [alert addAction:actionScope];
    [alert addAction:actionService];
    [alert addAction:actionDismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
    return;
    
    
    /* if (chkClickStan==YES)
     {
     UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
     AddStandardService *objAddStandardService=[storyBoard instantiateViewControllerWithIdentifier:@"AddStandardService"];
     [self.navigationController presentViewController:objAddStandardService animated:YES completion:nil];
     }
     
     else
     {
     
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     [defs setBool:NO forKey:@"backStan"];
     [defs synchronize];
     UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
     AddNonStandardServices *objAddNonStandardServices=[storyBoard instantiateViewControllerWithIdentifier:@"AddNonStandardServices"];
     [self.navigationController presentViewController:objAddNonStandardServices animated:YES completion:nil];
     }*/
    
}
- (IBAction)actionOnStandardService:(id)sender
{
    [self endEditing];
    [_btnNonStandardService setBackgroundColor:[UIColor lightGrayColor]];
    [_btnStandardService setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    _viewForNonStandardService.hidden=YES;
    _viewForStandardService.hidden=NO;
    chkClickStan=YES;
    [self fetchFromCoreDataStandard];
    [self heightManage];
    
}

- (IBAction)actionOnNonStandardService:(id)sender
{
    [self endEditing];
    [self discountStatusNonStan];
    [_btnStandardService setBackgroundColor:[UIColor lightGrayColor]];
    [_btnNonStandardService setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    
    [self fetchFromCoreDataNonStandard];
    
    //    [_btnStandardService setBackgroundColor:[UIColor lightGrayColor]];
    //    [_btnNonStandardService setBackgroundColor:[UIColor colorWithRed:236.0f/255 green:27.0f/255 blue:33.0f/255 alpha:1]];
    
    chkClickStan=NO;
    _viewForNonStandardService.hidden=NO;
    _viewForStandardService.hidden=YES;
    
    
    
    //Non Standard
    int sumNonStan=0;
    for (int i=0; i<arrNonStanServiceName.count; i++)
    {
        sumNonStan=sumNonStan+_tblNonStandardService.rowHeight;
    }
    _const_TableNonStan_H.constant=sumNonStan+50;
    [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableNonStan_H.constant)];
    
    // [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableStan_H.constant+_constTblBundle_H.constant+_const_TableNonStan_H.constant)];
}

- (IBAction)actionOnAddNonStandardService:(id)sender
{
    
}

- (IBAction)actionOnCategory1:(id)sender
{
    tblData.tag=10;
    [self tableLoad:tblData.tag];
}
- (IBAction)actionCategory2:(id)sender
{
    tblData.tag=20;
    [self tableLoad:tblData.tag];
}
- (IBAction)actionOnCategory3:(id)sender
{
    tblData.tag=30;
    [self tableLoad:tblData.tag];
}
-(void)setTableFrame:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    switch (i)
    {
            /*case 10:
             {
             tblData.frame=CGRectMake(_btnCategory1.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory1.frame.origin.y+35+150, [UIScreen mainScreen].bounds.size.width/3, 300);
             if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
             {
             tblData.frame=CGRectMake(_btnCategory1.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory1.frame.origin.y+35+150-15, [UIScreen mainScreen].bounds.size.width/3, 250);
             }
             
             break;
             }*/
        case 10:
        {
            [self loadFrame];
            break;
        }
        case 11:
        {
            [self loadFrame];
            break;
        }
        case 13:
        {
            [self loadFrame];
            break;
        }
        case 14:
        {
            [self loadFrame];
            break;
        }
        case 20:
        {
            tblData.frame=CGRectMake(_btnCategory2.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory2.frame.origin.y+35+150, [UIScreen mainScreen].bounds.size.width/3, 300);
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_btnCategory2.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory2.frame.origin.y+35+150-10, [UIScreen mainScreen].bounds.size.width/3, 250);
            }
            
            break;
        }
        case 30:
        {
            tblData.frame=CGRectMake(_btnCategory3.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory3.frame.origin.y+35+150, [UIScreen mainScreen].bounds.size.width/3, 300);
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_btnCategory3.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory3.frame.origin.y+35+150-10, [UIScreen mainScreen].bounds.size.width/3, 250);
            }
            break;
        }
        case 70:
        {
            tblData.frame=CGRectMake(_viewPopUp.frame.origin.x,_viewPopUp.frame.origin.y-20, _viewPopUp.frame.size.width, _viewPopUp.frame.size.height+25);
            
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_viewPopUp.frame.origin.x,_viewPopUp.frame.origin.y-20, _viewPopUp.frame.size.width, _viewPopUp.frame.size.height+25);
            }
            break;
        }
        case 71:
        {
            tblData.frame=CGRectMake(_viewPopUp.frame.origin.x,_viewPopUp.frame.origin.y-20, _viewPopUp.frame.size.width, _viewPopUp.frame.size.height+25);
            
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_viewPopUp.frame.origin.x,_viewPopUp.frame.origin.y-20, _viewPopUp.frame.size.width, _viewPopUp.frame.size.height+25);
            }
            break;
        }
        case 72:
        {
            tblData.frame=CGRectMake(_viewPopUp.frame.origin.x,_viewPopUp.frame.origin.y-20, _viewPopUp.frame.size.width, _viewPopUp.frame.size.height+25);
            
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_viewPopUp.frame.origin.x,_viewPopUp.frame.origin.y-20, _viewPopUp.frame.size.width, _viewPopUp.frame.size.height+25);
            }
            break;
        }
        case 73:
        {
            tblData.frame=CGRectMake(_viewPopUp.frame.origin.x,_viewPopUp.frame.origin.y-20, _viewPopUp.frame.size.width, _viewPopUp.frame.size.height+25);
            
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_viewPopUp.frame.origin.x,_viewPopUp.frame.origin.y-20, _viewPopUp.frame.size.width, _viewPopUp.frame.size.height+25);
            }
            break;
        }
        case 74:
        {
            viewBackGround1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            viewBackGround1.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
            UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
            singleTap1.numberOfTapsRequired = 1;
            [viewBackGround1 setUserInteractionEnabled:YES];
            [viewBackGround1 addGestureRecognizer:singleTap1];
            
            [self.view addSubview: viewBackGround1];
            
            tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
            }
            if([UIScreen mainScreen].bounds.size.height>1000)
            {
                tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
            }
            [self.view addSubview:viewBackGround1];
            [viewBackGround1 addSubview:tblData];
            break;
        }
        case 75:
        {
            
            viewBackGround1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            viewBackGround1.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
            
            UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
            singleTap1.numberOfTapsRequired = 1;
            [viewBackGround1 setUserInteractionEnabled:YES];
            [viewBackGround1 addGestureRecognizer:singleTap1];
            
            [self.view addSubview: viewBackGround1];
            
            tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
            }
            if([UIScreen mainScreen].bounds.size.height>1000)
            {
                tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
            }
            [self.view addSubview:viewBackGround1];
            [viewBackGround addSubview:tblData];
            break;
        }
        default:
            break;
    }
    
    
    
    [self.view addSubview:tblData];
    
}
-(void)loadFrame
{
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
}
-(void)setTableFrameNew
{
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    //Nilind 19 Jan
    UILabel *lblOk;
    lblOk=[[UILabel alloc]init];
    lblOk.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    lblOk.textColor=[UIColor whiteColor];
    lblOk.text=@"OK";
    lblOk.textAlignment=NSTextAlignmentCenter;
    lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-80, tblData.frame.origin.y+tblData.frame.size.height+2, 160, 40);
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-60, tblData.frame.origin.y+tblData.frame.size.height+2, 120, 40);
    }
    lblOk.layer.borderWidth=1.0;
    lblOk.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    lblOk.layer.cornerRadius=5.0;
    [viewBackGround addSubview:lblOk];
    
    //.......
    
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 11:
        {
            [self setTableFrame:i];
            break;
        }
        case 13:
        {
            [self setTableFrame:i];
            break;
        }
        case 14:
        {
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            [self setTableFrame:i];
            break;
        }
        case 70:
        {
            [self setTableFrame:i];
            break;
        }
        case 71:
        {
            [self setTableFrame:i];
            break;
        }
        case 72:
        {
            [self setTableFrame:i];
            break;
        }
        case 73:
        {
            [self setTableFrame:i];
            break;
        }
        case 74:
        {
            [self setTableFrame:i];
            break;
        }
        case 75:
        {
            [self setTableFrame:i];
            break;
        }
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger i;
    i=tblData.tag;
    
    if (tableView == _tblTarget || tableView == _tblScope || tableView == _tblService || tableView == _tblNonStandardClarkPest)
    {
        i=9999;
    }
    switch (i)
    {
            /*case 10:
             {
             [_btnCategory1 setTitle:[arrCategory objectAtIndex:indexPath.row] forState:UIControlStateNormal];
             break;
             }*/
        case 10:
        {
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                
                dictCoverLetter = [arrCoverLetter objectAtIndex:indexPath.row];
                [_btnCoverLetter setTitle:[NSString stringWithFormat:@"%@",[dictCoverLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
                [viewBackGround removeFromSuperview];
                [tblData removeFromSuperview];
            }
            else
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                dictCoverLetter = nil;
                [_btnCoverLetter setTitle:@"Select Cover Letter" forState:UIControlStateNormal];
                [viewBackGround removeFromSuperview];
                [tblData removeFromSuperview];
            }
            break;
        }
        case 11:
        {
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                dictIntroLetter = [arrIntroductionLetter objectAtIndex:indexPath.row];
                
                [_btnIntroductionLetter setTitle:[NSString stringWithFormat:@"%@",[dictIntroLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
                
                _txtViewIntroductionLetter.attributedText = [self getAttributedString:[dictIntroLetter valueForKey:@"TemplateContent"]];
                
                [_txtViewIntroductionLetter resignFirstResponder];
                [viewBackGround removeFromSuperview];
                [tblData removeFromSuperview];
                
            }
            else
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                
                dictIntroLetter = nil;
                
                [_btnIntroductionLetter setTitle:@"Select Introduction Letter" forState:UIControlStateNormal];
                
                _txtViewIntroductionLetter.attributedText = [self getAttributedString:@""];
                
                [_txtViewIntroductionLetter resignFirstResponder];
                [viewBackGround removeFromSuperview];
                [tblData removeFromSuperview];
            }
            break ;
        }
        case 13:
        {
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                
                dictTermsOfService = [arrTermsOfService objectAtIndex:indexPath.row];
                
                _txtViewTermsServices.attributedText =[self getAttributedString:[NSString stringWithFormat:@"%@",[dictTermsOfService valueForKey:@"Description"]]];
                _txtViewTermsServices.font=[UIFont systemFontOfSize:20];
                [_btnTermsOfServices setTitle:[NSString stringWithFormat:@"%@",[dictTermsOfService valueForKey:@"Title"]] forState:UIControlStateNormal];
                
                [viewBackGround removeFromSuperview];
                [tblData removeFromSuperview];
            }
            else
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                
                dictTermsOfService = nil;
                _txtViewTermsServices.text = @"";
                [_btnTermsOfServices setTitle:@"Select Terms of Services" forState:UIControlStateNormal];
                [viewBackGround removeFromSuperview];
                [tblData removeFromSuperview];
            }
            break ;
            
        }
        case 14:
        {
            if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            {
                
            }
            else
            {
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
                
                NSDictionary *dict=[arrMarketingContent objectAtIndex:indexPath.row];
                NSString *strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                
                if([strTermsId isEqualToString:@"(null)"]|| [strTermsId isEqual:nil] || [strTermsId isKindOfClass:[NSNull class]])
                {
                    strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"contentSysName"]];
                }
                
                if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
                {
                    [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                    
                    [arrSelectedMarketingContent addObject:strTermsId];
                }
                else
                {
                    [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                    if (arrSelectedMarketingContent.count==0)
                    {
                        
                    }
                    else
                    {
                        [arrSelectedMarketingContent removeObject:strTermsId];
                    }
                }
            }
            break ;
        }
            //End
        case 20:
        {
            [_btnCategory2 setTitle:[arrCategory objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            break;
        }
        case 30:
        {
            [_btnCategory3 setTitle:[arrCategory objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            break;
        }
        case 70:
        {
            NSDictionary *dict=[arrCategoryPopUp objectAtIndex:indexPath.row];
            [_btnSelectionCategory setTitle:[dict valueForKey:@"Name"] forState:UIControlStateNormal];
            strCategorySysName=[dict valueForKey:@"SysName"];
            [_buttonSelectOptions_PopUp setTitle:@"Select Service" forState:UIControlStateNormal];
            _textViewDescription_PopUp.text = @"";
            strHTML = @"";
            strServiceSysName = @"";
            break;
        }
        case 71:
        {
            selectedIndexPath = indexPath.row;
            NSDictionary *dict=[arrayService objectAtIndex:indexPath.row];
            [_buttonSelectOptions_PopUp setTitle:[dict valueForKey:@"Name"] forState:UIControlStateNormal];
            strServiceSysName=[dict valueForKey:@"SysName"];
            strServiceId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceMasterId"]];
            
            strHTML = [dict valueForKey:@"Description"];
            NSString *strDesc = [self convertHTML:strHTML];
            _textViewDescription_PopUp.text = strDesc;
            
            break;
        }
        case 72:
        {
            selectedIndexPath = indexPath.row;
            NSDictionary *dict=[arrayScope objectAtIndex:indexPath.row];
            [_buttonSelectOptions_PopUp setTitle:[dict valueForKey:@"Title"] forState:UIControlStateNormal];
            strScopeSysName=[dict valueForKey:@"SysName"];
            strScopeName=[dict valueForKey:@"Title"];
            strHTML = [dict valueForKey:@"Description"];
            NSString *strDesc = [self convertHTML:strHTML];
            _textViewDescription_PopUp.text = strDesc;
            [viewBackGround1 removeFromSuperview];
            [tblData removeFromSuperview];
            break;
        }
        case 73:
        {
            
            selectedIndexPath = indexPath.row;
            
            NSDictionary *dict=[arrayTarget objectAtIndex:indexPath.row];
            [_buttonSelectOptions_PopUp setTitle:[dict valueForKey:@"Name"] forState:UIControlStateNormal];
            strTargetSysName=[dict valueForKey:@"SysName"];
            strTargetName=[dict valueForKey:@"Name"];
            strHTML = [dict valueForKey:@"Description"];
            NSString *strDesc = [self convertHTML:strHTML];
            
            _textViewDescription_PopUp.text = strDesc;
            [viewBackGround1 removeFromSuperview];
            [tblData removeFromSuperview];
            break;
        }
        case 74:
        {
            NSDictionary *dict=[arrProposedFreq objectAtIndex:indexPath.row];
            
            [_btnProposedServiceFreq setTitle:[dict valueForKey:@"FrequencyName"] forState:UIControlStateNormal];
            strFreqSysName=[dict valueForKey:@"SysName"];
            
            [viewBackGround1 removeFromSuperview];
            [tblData removeFromSuperview];
            break;
        }
        case 75:
        {
            [_btnProposedServiceMonth setTitle:[arrProposedMonth objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            
            [viewBackGround1 removeFromSuperview];
            [tblData removeFromSuperview];
            break;
        }
        default:
            break;
    }
    
    // [tblData removeFromSuperview];
}

- (IBAction)actionOnSaveContinue:(id)sender
{
    [self endEditing];
    if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
    {
        
    }
    else
    {
        
        
    }
    // [self chekcBox];
    //  [self fetchFromCoreDataStandard];
    
    if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
    {
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
        ClarkPestConfigureProposaliPhone *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"ClarkPestConfigureProposaliPhone"];
        objSalesAutomationAgreementProposal.strFromSummary=@"FromSummary";
        [self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:YES];
    }
    else
    {
        
        [self saveImageToCoreData];
        [global updateSalesZSYNC:strLeadId :@"yes"];
        
        if(isEditedInSalesAuto==YES)
        {
            NSLog(@"Global modify date called in Select Service");
            [global updateSalesModifydate:strLeadId];
        }
        
        if(_txtFieldProposedInitialPrice.text.length>0)
        {
            NSDictionary *dict = @{@"initialPrice":[NSString stringWithFormat:@"%@",_txtFieldProposedInitialPrice.text],
                                   @"initialDescription":[NSString stringWithFormat:@"%@",_textViewInitialDescription.text.length>0 ? _textViewInitialDescription.text : @""]
                                   };
            [self saveLeadCommercialInitialInfoCoreData:dict];
            
        }
        
        if(_txtFieldProposedMaintPrice.text.length>0)
        {
            NSDictionary *dict = @{@"maintenancePrice":[NSString stringWithFormat:@"%@",_txtFieldProposedMaintPrice.text],
                                   @"frequencySysName":[NSString stringWithFormat:@"%@",_btnProposedServiceFreq.titleLabel.text],
                                   @"maintenanceDescription":[NSString stringWithFormat:@"%@",_textViewMaintDescription.text.length>0 ? _textViewMaintDescription.text : @""]
                                   };
            
            [self saveLeadCommercialMaintInfoCoreData:dict];
            
        }
        [self fetchLeadCommercialInitialInfoFromCoreData];
        [self fetchLeadCommercialMaintInfoFromCoreData];
        
        /* if(arrSavedInitialPriceDetail.count==0 || arrSavedMaintPriceDetail.count==0 ||arrSavedInitialPriceDetail==nil||arrSavedMaintPriceDetail==nil)
         {
         [global AlertMethod:Alert :@"Please enter Initial or Maintenance price"];
         return;
         }*/
        
        [self updateLeadIdDetail];
        [self fetchFromCoreDataStandardClarkPestSoldCount];
        [self fetchFromCoreDataNonStandardClarkPestSoldCount];
        
        if(arrSoldCount.count==0 && arrSoldCountNonStan.count==0)
        {
            [global displayAlertController:@"Alert" :@"To proceed, please add atleast one service to the agreement" :self];
        }
        else
        {
            [self saveNewAgreementDetails];
            [self fetchForCommercialAppliedDiscountFromCoreData];
            
            UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
            ClarkPestConfigureProposaliPhone *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"ClarkPestConfigureProposaliPhone"];
            objSalesAutomationAgreementProposal.strFromSummary=@"FromSummary";
            [self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:YES];
        }
        
    }
}

- (IBAction)actionOnCancel:(id)sender
{
    [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isBackFromSelectService"];
    [defs synchronize];
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)viewStandard
{
    UIView *viewStandard=[[UIView alloc]init];
    viewStandard.frame=CGRectMake(20, 50, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height-100);
    viewStandard.backgroundColor=[UIColor purpleColor];
    
    UILabel *lblTitle=[[UILabel alloc]init];
    lblTitle.frame=CGRectMake(0, 0,  viewStandard.frame.size.width, 35);
    lblTitle.textColor=[UIColor blueColor];
    lblTitle.text=@"VIEW STANDARD";
    lblTitle.backgroundColor=[UIColor lightGrayColor];
    [viewStandard addSubview:lblTitle];
    
    
    UIButton *btnCategory1=[[UIButton alloc]init];
    btnCategory1.frame=CGRectMake(10, lblTitle.frame.origin.y+lblTitle.frame.size.height+5, viewStandard.frame.size.width-10, 30);
    btnCategory1.backgroundColor=[UIColor blueColor];
    
    
    UIButton *btnCategory2=[[UIButton alloc]init];
    btnCategory2.frame=CGRectMake(10, btnCategory1.frame.origin.y+btnCategory1.frame.size.height+5, viewStandard.frame.size.width-10, 30);
    btnCategory2.backgroundColor=[UIColor blueColor];
    
    
    UIButton *btnCategory3=[[UIButton alloc]init];
    btnCategory3.frame=CGRectMake(10, btnCategory2.frame.origin.y+btnCategory2.frame.size.height+5, viewStandard.frame.size.width-10, 30);
    btnCategory3.backgroundColor=[UIColor blueColor];
    
    
    [viewStandard addSubview:btnCategory1];
    [viewStandard addSubview:btnCategory2];
    [viewStandard addSubview:btnCategory3];
    
    
    UITextField *txt1=[[UITextField alloc]init];
    txt1.frame=CGRectMake(5, btnCategory3.frame.origin.y+btnCategory3.frame.size.height+5, 50, 30);
    txt1.backgroundColor=[UIColor blueColor];
    
    UITextField *txt2=[[UITextField alloc]init];
    txt2.frame=CGRectMake(txt1.frame.origin.x+txt1.frame.size.width+ 5, btnCategory3.frame.origin.y+btnCategory3.frame.size.height+5, 50, 30);
    txt2.backgroundColor=[UIColor blueColor];
    
    UITextField *txt3=[[UITextField alloc]init];
    txt3.frame=CGRectMake(txt2.frame.origin.x+txt2.frame.size.width+ 5 ,btnCategory3.frame.origin.y+btnCategory3.frame.size.height+5, 50, 30);
    txt3.backgroundColor=[UIColor blueColor];
    
    [viewStandard addSubview:txt1];
    [viewStandard addSubview:txt2];
    [viewStandard addSubview:txt3];
    
    [self.view addSubview:viewStandard];
}
#pragma mark- 30 Aug Nilind
#pragma mark- FETCH CORE DATA
-(void)fetchFromCoreDataStandard
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrMatchesBundle= [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrInitialPrice=[[NSMutableArray alloc]init];
    arrDiscountPer=[[NSMutableArray alloc]init];
    
    arrDiscount=[[NSMutableArray alloc]init];
    arrMaintenancePrice=[[NSMutableArray alloc]init];
    arrFrequencyName=[[NSMutableArray alloc]init];
    arrServiceName=[[NSMutableArray alloc]init];
    arrStanIsSold=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    
    arrUnit=[[NSMutableArray alloc]init];
    arrFinalInitialPrice=[[NSMutableArray alloc]init];
    arrFinalMaintPrice=[[NSMutableArray alloc]init];
    
    arrPackageNameId=[[NSMutableArray alloc]init];
    arrBillingFreqSysName=[[NSMutableArray alloc]init];
    //arrBundleId=[[NSMutableArray alloc]init];
    NSMutableArray *arrBundleNew;
    arrBundleNew=[[NSMutableArray alloc]init];
    
    arrTempBundelDetailId=[[NSMutableArray alloc]init];
    arrTempBundleServiceSysName=[[NSMutableArray alloc]init];
    arrBundleRow=[[NSMutableArray alloc]init];
    arrSoldServiceStandardId=[[NSMutableArray alloc]init];
    arrRandomId=[[NSMutableArray alloc]init];
    arrAdditionalParamterDcs=[[NSMutableArray alloc]init];
    arrFreqSysName=[[NSMutableArray alloc]init];
    arrSoldServiceStandardIdForCoupon=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                [arrFrequencyName addObject:[matches valueForKey:@"serviceFrequency"]];
                [arrFreqSysName addObject:[matches valueForKey:@"frequencySysName"]];
                
                if ([[matches valueForKey:@"serviceSysName"]isEqualToString:@""])
                {
                    [arrServiceName addObject:@""];
                }
                else
                {
                    [arrServiceName addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                }
                
                [arrStanIsSold addObject: [matches valueForKey:@"isSold"]];
                
                [arrSysName addObject:[matches valueForKey:@"serviceSysName"]];
                
                //Nilind 6 oct
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isTBD"]]isEqualToString:@"true"])
                {
                    /* [arrInitialPrice addObject:@"TBD"];
                     [arrDiscountPer addObject:@"TBD"];
                     [arrDiscount addObject:@"TBD"];
                     [arrMaintenancePrice addObject:@"TBD"];
                     [arrFinalInitialPrice addObject:@"TBD"];
                     [arrFinalMaintPrice addObject:@"TBD"];*/
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]isEqualToString:@"TBD"])
                    {
                        [arrInitialPrice addObject:@"TBD"];
                    }
                    else
                    {
                        [arrInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
                        
                    }
                    
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"discount"]]isEqualToString:@"TBD"])
                    {
                        [arrDiscount addObject:@"TBD"];
                    }
                    else
                    {
                        [arrDiscount addObject:[matches valueForKey:@"discount"]];
                        
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]isEqualToString:@"TBD"])
                    {
                        [arrMaintenancePrice addObject:@"TBD"];
                    }
                    else
                    {
                        [arrMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
                        
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedInitialPrice"]]isEqualToString:@"TBD"])
                    {
                        [arrFinalInitialPrice addObject:@"TBD"];
                    }
                    else
                    {
                        [arrFinalInitialPrice addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                        
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedMaintPrice"]]isEqualToString:@"TBD"])
                    {
                        [arrFinalMaintPrice addObject:@"TBD"];
                    }
                    else
                    {
                        [arrFinalMaintPrice addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                        
                    }
                    
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"discountPercentage"]]isEqualToString:@"TBD"])
                    {
                        [arrDiscountPer addObject:@"TBD"];
                    }
                    else
                    {
                        [arrDiscountPer addObject:[matches valueForKey:@"discountPercentage"]];
                        
                    }
                    
                    
                }
                else
                {
                    [arrInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
                    [arrDiscount addObject:[matches valueForKey:@"discount"]];
                    [arrMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
                    [arrFinalInitialPrice addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                    [arrFinalMaintPrice addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                    [arrDiscountPer addObject:[matches valueForKey:@"discountPercentage"]];
                    
                }
                [arrUnit addObject:[matches valueForKey:@"unit"]];
                
                [arrPackageNameId addObject:[matches valueForKey:@"packageId"]];
                [arrBillingFreqSysName addObject:[matches valueForKey:@"billingFrequencySysName"]];
                [arrSoldServiceStandardId addObject:[matches valueForKey:@"soldServiceStandardId"]];
                
                //[arrBundleId addObject:[matches valueForKey:@"bundleId"]];
                // if([[matches valueForKey:@"additionalParameterPriceDcs"] isKindOfClass:[NSArray class]])
                // {
                [arrAdditionalParamterDcs addObject:[matches valueForKey:@"additionalParameterPriceDcs"]];
                // }
                
            }
            else
            {
                //[arrBundleId addObject:[matches valueForKey:@"bundleId"]];
                //[arrBundleRow addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]];
                [arrBundleNew addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]];
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleDetailId"]] isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleDetailId"]] isEqual:nil])
                {
                    
                }
                else
                {
                    //[arrTempBundleId addObject:[matches valueForKey:@"bundleId"]];
                    [arrTempBundelDetailId addObject:[matches valueForKey:@"bundleDetailId"]];
                    [arrTempBundleServiceSysName addObject:[matches valueForKey:@"serviceSysName"]];
                    
                    
                }
                
                
            }
            [arrSoldServiceStandardIdForCoupon addObject:[matches valueForKey:@"soldServiceStandardId"]];
            
        }
        arrBundleRow = [[NSSet setWithArray:arrBundleNew] allObjects];
        
        // [self fetchFromCoreDataStandard];
        [_tblBundle reloadData];
        [_tblRecord reloadData];
        //[self heightManage];
        
    }
    if(arrAllObj.count==0)
    {
        _lblMaintenanceServiceStandard.hidden=YES;
    }
    else
    {
        _lblMaintenanceServiceStandard.hidden=NO;
    }
    if(arrBundleRow.count==0)
    {
        _lblBundleServiceStandard.hidden=YES;
    }
    else
    {
        _lblBundleServiceStandard.hidden=NO;
        
    }
}
-(void)fetchFromCoreDataStandardNewSaavan
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,@"0"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrAllObjSaavan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrInitialPrice=[[NSMutableArray alloc]init];
    arrDiscount=[[NSMutableArray alloc]init];
    arrMaintenancePrice=[[NSMutableArray alloc]init];
    arrFrequencyName=[[NSMutableArray alloc]init];
    arrServiceName=[[NSMutableArray alloc]init];
    arrStanIsSold=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    //temp
    arrSysNameConditionalStandard=[[NSMutableArray alloc]init];
    arrFinalIndexPath=[[NSMutableArray alloc]init];
    //End
    if (arrAllObjSaavan.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSaavan.count; k++)
        {
            matches=arrAllObjSaavan[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            [arrFrequencyName addObject:[matches valueForKey:@"serviceFrequency"]];
            
            if ([[matches valueForKey:@"serviceSysName"]isEqualToString:@""])
            {
                [arrServiceName addObject:@""];
            }
            else
            {
                [arrServiceName addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
            }
            
            [arrStanIsSold addObject: [matches valueForKey:@"isSold"]];
            
            if ([[matches valueForKey:@"isSold"] isEqualToString:@"true"]) {
                
                NSString *strPath=[NSString stringWithFormat:@"%d",k];
                
                [arrFinalIndexPath addObject:strPath];
                
                NSString *strSysNameConditional=[matches valueForKey:@"serviceSysName"];
                
                [arrSysNameConditionalStandard addObject:strSysNameConditional];
                
            }else{
                
                // NSString *strPath=[NSString stringWithFormat:@"%d",k];
                
                
                
                // NSString *strSysNameConditional=[matches valueForKey:@"serviceSysName"];
                
                if (arrSysNameConditionalStandard.count>k) {
                    
                    [arrSysNameConditionalStandard removeObjectAtIndex:k];
                    [arrFinalIndexPath removeObjectAtIndex:k];
                }
            }
            [arrSysName addObject:[matches valueForKey:@"serviceSysName"]];
            
            //Nilind 6 oct
            
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isTBD"]]isEqualToString:@"true"])
            {
                [arrInitialPrice addObject:@"TBD"];
                [arrDiscount addObject:@"TBD"];
                [arrMaintenancePrice addObject:@"TBD"];
                [arrDiscountPer addObject:@"TBD"];
            }
            else
            {
                [arrInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
                [arrDiscount addObject:[matches valueForKey:@"discount"]];
                [arrDiscountPer addObject:[matches valueForKey:@"discountPercentage"]];
                [arrMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
            }
            
            //....................
            
        }
        // [self fetchFromCoreDataStandard];
        [_tblRecord reloadData];
    }
}

-(void)fetchFromCoreDataNonStandard
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrNonStanInitialPrice=[[NSMutableArray alloc]init];
    arrNonStanDiscount=[[NSMutableArray alloc]init];
    arrNonStanServiceName=[[NSMutableArray alloc]init];
    arrNonStanServiceDesc=[[NSMutableArray alloc]init];
    arrNonStanIsSold=[[NSMutableArray alloc]init];
    arrDepartmentSysName=[[NSMutableArray alloc]init];
    arrDiscountPerNonStan=[[NSMutableArray alloc]init];
    arrBillingFreqSysNameNonStan=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            [arrNonStanInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
            [arrNonStanDiscount addObject:[matches valueForKey:@"discount"]];
            [arrNonStanServiceName addObject:[matches valueForKey:@"serviceName"]];
            [arrNonStanServiceDesc addObject:[matches valueForKey:@"serviceDescription"]];
            [arrNonStanIsSold addObject:[matches valueForKey:@"isSold"]];
            
            //Nilind
            [arrDepartmentSysName addObject:[matches valueForKey:@"departmentSysname"]];
            [arrDiscountPerNonStan addObject:[matches valueForKey:@"discountPercentage"]];
            [arrBillingFreqSysNameNonStan addObject:[matches valueForKey:@"billingFrequencySysName"]];
            
            //......................................................................
            
        }
        
        NSDictionary *dictSalesLeadMaster;
        /*NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
         dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
         
         NSArray *arrDept=[dictSalesLeadMaster valueForKey:@"DepartmentMasters"];*/
        //        for (int i=0; i<arrDept.count; i++)
        //        {
        //            NSDictionary *dict=[arrDept objectAtIndex:i];
        //
        //            if ([[arrDepartmentSysName objectAtIndex:i] isEqualToString:[dict valueForKey:@"SysName"]])
        //            {
        //                [arrDepartmentName addObject:[dict valueForKey:@"Name"]];
        //            }
        //        }
        //Nilind
        // NSMutableArray *arrDept=[[NSMutableArray alloc]init];
        NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
        dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
        NSMutableArray *arrDept;
        NSMutableArray *arrName;//*arrSysName;
        arrName=[[NSMutableArray alloc]init];
        ///arrSysName=[[NSMutableArray alloc]init];//////
        arrDept=[[NSMutableArray alloc]init];
        NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
        for (int i=0;i<arrDeptName.count; i++)
        {
            NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
            NSArray *arry=[dictBranch valueForKey:@"Departments"];
            for (int j=0; j<arry.count; j++)
            {
                NSDictionary *dict=[arry objectAtIndex:j];
                if ([strBranchSysName isEqualToString:[dictBranch valueForKey:@"SysName"]])
                {
                    //  [arrDept addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                    [arrDept addObject:dict];
                    //                [arrName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                    //                [arrSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                }
                
            }
            
        }
        
        
        
        //............
        
        arrDepartmentName=arrDepartmentSysName;
        for (int i=0; i<arrDepartmentSysName.count; i++)
        {
            for (int k=0; k<arrDept.count; k++)
            {
                NSDictionary *dict=[arrDept objectAtIndex:k];
                if([[arrDepartmentSysName objectAtIndex:i] isEqualToString:[dict valueForKey:@"SysName"]])
                {
                    // [arrDepartmentName addObject:[dict valueForKey:@"Name"]];
                    [arrDepartmentName replaceObjectAtIndex:i withObject:[dict valueForKey:@"Name"]];
                }
            }
        }
        
        [_tblNonStandardService reloadData];
    }
}
//============================================================================
//============================================================================
#pragma mark- Sales Info CoreData Delete
//============================================================================
//============================================================================



-(void)deleteFromCoreDataSalesInfo:(NSString *)sysName : (NSString *)strSoldServiceStandardId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        NSString *strIdBundle,*strSysNameService,*strId;
        strIdBundle=[NSString stringWithFormat:@"%@",[data valueForKey:@"bundleId"]];
        strSysNameService=[NSString stringWithFormat:@"%@",[data valueForKey:@"serviceSysName"]];
        strId=[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceStandardId"]];
        if ([strIdBundle isEqualToString:@"0"]&&[strSysNameService isEqualToString:sysName]&&[strId isEqualToString:strSoldServiceStandardId])
        {
            [context deleteObject:data];
            break;
            
        }
    }
    NSError *saveError = nil;
    [context save:&saveError];
    [self heightManage];
    
    
}
/*-(void)deleteFromCoreDataSalesInfo
 {
 
 appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
 context = [appDelegate managedObjectContext];
 
 //  Delete Lead Detail Data
 
 NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
 NSFetchRequest *allData = [[NSFetchRequest alloc] init];
 [allData setPredicate:predicate];
 [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
 [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
 
 NSError * error = nil;
 NSArray * Data = [context executeFetchRequest:allData error:&error];
 //error handling goes here
 for (NSManagedObject * data in Data) {
 [context deleteObject:data];
 }
 NSError *saveError = nil;
 [context save:&saveError];
 
 }*/
//===============31August============================

#pragma mark- Update Sales Info CoreData Standard

//============================================================================
-(void)updateStadard:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    
    NSError *error1 = nil;
    matches=indexValue;
    if (chkBtnCheckBoxStan==YES)
    {
        [matches setValue:@"true" forKey:@"isSold"];
    }
    else
    {
        [matches setValue:@"false" forKey:@"isSold"];
    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataStandard];
}

-(void)updateNonStadard:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    
    NSError *error1 = nil;
    matches=indexValue;
    if (chkBtnCheckBoxNonStan==YES)
    {
        [matches setValue:@"true" forKey:@"isSold"];
    }
    else
    {
        [matches setValue:@"false" forKey:@"isSold"];
    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    [context save:&error1];
    
    [self fetchFromCoreDataNonStandard];
    
}
//===============6 Oct============================
-(void)plusService:(UIButton *)btn
{
    
    NSDictionary *dictChk;
    NSInteger tagPLusService;
    tagPLusService=btn.tag;
    NSLog(@"tagPLusService>>>%ld",(long)tagPLusService);
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tagPLusService inSection:0];
    
    SalesAutomationServiceTableViewCell *tappedCell = (SalesAutomationServiceTableViewCell *)[_tblRecord cellForRowAtIndexPath:indexpath];
    NSLog(@"%@",tappedCell.lblServiceSysName.text);
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //   NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    NSString *strSysNameService;
    BOOL chk;
    chk=NO;
    strSysNameService=tappedCell.lblServiceSysName.text;
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSMutableArray *arrPlusServiceDict;
    arrPlusServiceDict=[[NSMutableArray alloc]init];
    
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict1=[arrServices objectAtIndex:j];
                if ([strSysNameService isEqualToString:[dict1 valueForKey:@"ParentSysName"]])
                {
                    dictChk=dict;
                    NSLog(@"SERTVICE FOUND");
                    NSLog(@"TRUE");
                    NSLog(@"ParentSysName>>>>%@--%@",[dict1 valueForKey:@"ParentSysName"],[dict1 valueForKey:@"ServiceMasterId"]);
                    NSLog(@"Name>>>>%@",[dict1 valueForKey:@"Name"]);
                    chk=YES;
                    [arrPlusServiceDict addObject:dict1];
                }
                else
                {
                    
                }
            }
            
        }
        
    }
    if (arrPlusServiceDict.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Sorry,Plus Service Not Avaible" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
        AddPlusService *objAddPlusService=[storyBoard instantiateViewControllerWithIdentifier:@"AddPlusService"];
        objAddPlusService.arrPlusService=arrPlusServiceDict;
        
        objAddPlusService.strFreqName=tappedCell.lblFrequency.text;
        
        NSString *separatorString = @"/";
        NSString *myString = tappedCell.lblBillingFrequency.text;
        NSString *myNewString = [myString componentsSeparatedByString:separatorString].lastObject;
        objAddPlusService.strBillingFreqName=myNewString;
        
        
        NSLog(@"%@",arrPlusServiceDict);
        [self.navigationController presentViewController:objAddPlusService animated:YES completion:nil];
    }
    
    
}
-(void)serviceName
{
    NSMutableArray *name,*sysName,*qtyVal,*arrPackageId,*arrPackageName, *arrParamService;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    qtyVal=[[NSMutableArray alloc]init];
    arrPackageId=[[NSMutableArray alloc]init];
    arrPackageName=[[NSMutableArray alloc]init];
    
    
    
    arrParamService=[[NSMutableArray alloc]init];
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    // NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [qtyVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsUnitBasedService"]]];
                [arrParamService addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsParameterizedPriced"]]];
                
                NSArray *arrPackage=[dict valueForKey:@"ServicePackageDcs"];
                for (int k=0; k<arrPackage.count; k++)
                {
                    NSDictionary *dictPackage=[arrPackage objectAtIndex:k];
                    [arrPackageId addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"ServicePackageId"]]];
                    [arrPackageName addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"PackageName"]]];
                }
                
            }
        }
    }
    
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
    
    dictQuantityStatus = [NSDictionary dictionaryWithObjects:qtyVal forKeys:sysName];
    
    dictPackageName =[NSDictionary dictionaryWithObjects:arrPackageName forKeys:arrPackageId];
    dictServiceParmaterBasedStatus = [NSDictionary dictionaryWithObjects:arrParamService forKeys:sysName];
    
    
    NSLog(@"PaymentInfo%@",dictServiceName);
    NSLog(@"Package Detail %@",dictPackageName);
    
    //For Bundle
    NSArray *arrBundleData=[dictMasters valueForKey:@"ServiceBundles"];
    NSMutableArray *nameBundle,*keyBundleId;
    nameBundle=[[NSMutableArray alloc]init];
    keyBundleId=[[NSMutableArray alloc]init];
    for (int i=0; i<arrBundleData.count; i++)
    {
        NSDictionary *dict=[arrBundleData objectAtIndex:i];
        {
            [nameBundle addObject:[dict valueForKey:@"BundleName"]];
            [keyBundleId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceBundleId"]]];
        }
    }
    dictBundleNameFromId =[NSDictionary dictionaryWithObjects:nameBundle forKeys:keyBundleId];
    
    
    
    
    //For Discount
    
    NSArray *arrForDiscount;
    NSMutableArray *valNameDiscount,*keySysNameDiscount,*arrDiscountUsage,*arrDiscountCode;
    valNameDiscount=[[NSMutableArray alloc]init];
    keySysNameDiscount=[[NSMutableArray alloc]init];
    arrDiscountUsage=[[NSMutableArray alloc]init];
    arrDiscountCode=[[NSMutableArray  alloc]init];
    arrForDiscount=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
    if ([arrForDiscount isKindOfClass:[NSArray class]])
    {
        for (int i=0; i<arrForDiscount.count; i++)
        {
            NSDictionary *dict=[arrForDiscount objectAtIndex:i];
            {
                [valNameDiscount addObject:[dict valueForKey:@"Name"]];
                [keySysNameDiscount addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [arrDiscountUsage addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Usage"]]];
                [arrDiscountCode addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountCode"]]];
                
            }
        }
        dictDiscountNameFromSysName =[NSDictionary dictionaryWithObjects:valNameDiscount forKeys:keySysNameDiscount];
        dictDiscountUsageFromSysName =[NSDictionary dictionaryWithObjects:arrDiscountUsage forKeys:keySysNameDiscount];
        dictDiscountUsageFromCode =[NSDictionary dictionaryWithObjects:arrDiscountUsage forKeys:arrDiscountCode];
        
    }
    //For Sales Content Title
    NSArray *arrSalesContent=[dictMasters valueForKey:@"SalesMarketingContentMaster"];
    NSMutableArray *arrKeySysNameMarketingContent,*arrValueTitleMarketingContent;
    arrKeySysNameMarketingContent=[[NSMutableArray alloc]init];
    arrValueTitleMarketingContent=[[NSMutableArray alloc]init];
    
    if ([arrSalesContent isKindOfClass:[NSArray class]])
    {
        for (int i=0; i<arrSalesContent.count; i++)
        {
            NSDictionary *dict=[arrSalesContent objectAtIndex:i];
            {
                [arrValueTitleMarketingContent addObject:[dict valueForKey:@"Title"]];
                [arrKeySysNameMarketingContent addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
            }
        }
        dictSalesContentTitleFromSysName =[NSDictionary dictionaryWithObjects:arrValueTitleMarketingContent forKeys:arrKeySysNameMarketingContent];
        
    }
    
    [self getMultiTermsContionDetails];
}

-(void)chkForPlusServiceButton:(NSString *)strSysNameService
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict1=[arrServices objectAtIndex:j];
                if ([strSysNameService isEqualToString:[dict1 valueForKey:@"ParentSysName"]])
                {
                    chkkkk=YES;
                }
            }
            
        }
        
    }
}

//Nilind 14 Nov

#pragma mark - SALES FETCH
-(void)salesFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matches=arrAllObjSales[k];
            
            strFlowType=[NSString stringWithFormat:@"%@",[matches valueForKey:@"flowType"]];
            
            strBranchSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"branchSysName"]];
            strCouponStatus=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCouponApplied"]];
            strCouponStatusNonStan=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCouponApplied"]];
            
            strAccountNoGlobal=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountNo"]];
            
            NSString *strLeadStatus=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
            
            strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
            
            strStageSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]];
            
            // if ([strLeadStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strLeadStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
            if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
            {
                
                chkStatus=YES;
            }
            else
            {
                chkStatus=NO;
                
            }
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        }
    }
}


//...............
//Nilind 29 Dec

-(void)chekcBox
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request = [[NSFetchRequest alloc] init];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,@"0"];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSArray *arrAllObjStan;
    NSManagedObject *matchesStan;
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObjStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSArray *sortedArrayOfString = [arrFinalIndexPath sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [(NSString *)obj1 compare:(NSString *)obj2 options:NSNumericSearch];
    }];
    NSLog(@"sortedArrayOfString1%@",sortedArrayOfString);
    
    
    if (arrAllObjStan.count==0)
    {
    }
    else
    {
        
        for (int j=0; j<arrAllObjStan.count; j++)
        {
            matchesStan=[arrAllObjStan objectAtIndex:j];
            
            
            /*for (int i=0; i>arrFinalIndexPath.count; i++)
             {
             if([[sortedArrayOfString objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",j]])
             {
             [matches setValue:@"true" forKey:@"isSold"];
             }
             else
             {
             [matches setValue:@"false" forKey:@"isSold"];
             }
             }*/
            for (int i=0; i<arrFinalIndexPath.count; i++)
            {
                if([[NSString stringWithFormat:@"%d",j] isEqualToString:[sortedArrayOfString objectAtIndex:i]])
                {
                    [matchesStan setValue:@"true" forKey:@"isSold"];
                    break;
                }
                else
                {
                    [matchesStan setValue:@"false" forKey:@"isSold"];
                }
            }
            
            
        }
        
        
    }
    
    [matchesStan setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataNonStandard];
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Detemine if it's in editing mode
    
    if (chkStatus==YES)
    {
        if (_tblRecord.editing)
        {
            return UITableViewCellEditingStyleDelete;
        }
        
        return UITableViewCellEditingStyleNone;
    }
    else
    {
        return UITableViewCellEditingStyleDelete;
    }
}
//........
-(NSString *)fetchLead
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSString *str;
    if (arrAllObj12.count==0)
    {
    }
    else
    {
        NSManagedObject *matches12=arrAllObj12[0];
        str=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]];
        
        NSString *strDate=[global ChangeDateMechanicalEquipment:[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"initialServiceDate"]]];
        if([strDate isEqualToString:@"01/01/0001"]||strDate.length==0||[strDate isEqual:nil])
        {
            strDate=@"Select Date";
        }
        
        
        [_buttonProposedInitialServiceDate setTitle:strDate forState:UIControlStateNormal];
        
        
        
        
        if([[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"recurringServiceMonth"]] length]>0)
        {
            [_btnProposedServiceMonth setTitle:[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"recurringServiceMonth"]] forState:UIControlStateNormal];
        }
        else
        {
            [_btnProposedServiceMonth setTitle:@"January" forState:UIControlStateNormal];
        }
        
        strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]];
        strStageSysName=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"stageSysName"]];
        
    }
    return str;
}

- (IBAction)actionOnGlobalSync:(id)sender
{
    
    [self endEditing];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"Alert!":@"No Internet connection available"];
    }
    else
    {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
        [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];
        
    }}
-(void)methodSync
{
    GlobalSyncViewController *objGlobalSyncViewController=[[GlobalSyncViewController alloc]init];
    NSString *str=@"general";
    
    [objGlobalSyncViewController syncCall:str];
}
-(void)stopLoader
{
    [DejalBezelActivityView removeView];
}
//Nilind 20 Jan

-(void)fetchDepartmentName
{
    
    NSDictionary *dictSalesLeadMaster;
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    NSMutableArray *arrDept;
    NSMutableArray *arrName12,*arrSysName12;
    arrName12=[[NSMutableArray alloc]init];
    arrSysName12=[[NSMutableArray alloc]init];
    arrDept=[[NSMutableArray alloc]init];
    NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    for (int i=0;i<arrDeptName.count; i++)
    {
        NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
        NSArray *arry=[dictBranch valueForKey:@"Departments"];
        for (int j=0; j<arry.count; j++)
        {
            NSDictionary *dict=[arry objectAtIndex:j];
            
            [arrName12 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
            [arrSysName12 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
            
            
        }
        
    }
    dictForDepartment = [NSDictionary dictionaryWithObjects:arrName12 forKeys:arrSysName12];
}

//.......
//nilind 03 May
-(void)getFrequencySysNameFromName
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    NSMutableArray *arrName1,*arrSysName1,*arrYearlyOccurence;
    arrName1=[[NSMutableArray alloc]init];
    arrSysName1=[[NSMutableArray alloc]init];
    arrYearlyOccurence=[[NSMutableArray alloc]init];
    
    NSArray *arrFreq=[dictMasters valueForKey:@"Frequencies"];
    for (int i=0; i<arrFreq.count; i++)
    {
        NSDictionary *dict=[arrFreq objectAtIndex:i];
        [arrName1 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]];
        [arrSysName1 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        [arrYearlyOccurence addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"YearlyOccurrence"]]];
        
        
    }
    
    dictFreqNameFromSysName=[NSDictionary dictionaryWithObjects:arrName1 forKeys:arrSysName1];
    dictYearlyOccurFromFreqSysName=[NSDictionary dictionaryWithObjects:arrYearlyOccurence forKeys:arrSysName1];
    
    
    NSLog(@"dictFreqNameFromSysName>>%@",dictFreqNameFromSysName);
    
}
#pragma mark- ********* TEXT FIELD DELEGATE **********
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)hideKeyboard
{
    [self.view endEditing:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField==_txtFieldProposedInitialPrice || textField==_txtFieldProposedMaintPrice || textField==_txtFieldProposedServiceDate)
    {
        if(textField ==_txtFieldProposedInitialPrice)
        {
            NSString *firstLetter;
            if(_txtFieldProposedInitialPrice.text.length==0)
            {
                firstLetter=@"";
            }
            else
            {
                firstLetter = [_txtFieldProposedInitialPrice.text substringToIndex:1];
                
            }NSLog(@"%@",firstLetter);
            
            
            
            BOOL isNuberOnly;
            if ([firstLetter isEqualToString:@"-"])
            {
                isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtFieldProposedInitialPrice.text];
            }
            else
            {
                if ([string isEqualToString:@"-"])
                {
                    if ([firstLetter isEqualToString:@""])
                    {
                        isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789-" :_txtFieldProposedInitialPrice.text];
                    }
                    else
                    {
                        if (![firstLetter isEqualToString:@"-"] && range.location==0)
                        {
                            isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789-" :_txtFieldProposedInitialPrice.text];
                            
                        }
                        else
                        {
                            isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtFieldProposedInitialPrice.text];
                        }
                    }
                }
                else
                {
                    isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789-" :_txtFieldProposedInitialPrice.text];
                }
            }
            //BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789-" :_txtFieldProposedInitialPrice.text];
            return isNuberOnly;
        }
        /*{
         BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtFieldProposedInitialPrice.text];
         return isNuberOnly;
         }*/
        if(textField == _txtFieldProposedMaintPrice)
        {
            NSString *firstLetter;
            if(_txtFieldProposedMaintPrice.text.length==0)
            {
                firstLetter=@"";
            }
            else
            {
                firstLetter = [_txtFieldProposedMaintPrice.text substringToIndex:1];
                
            }NSLog(@"%@",firstLetter);
            
            
            
            BOOL isNuberOnly;
            if ([firstLetter isEqualToString:@"-"])
            {
                isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtFieldProposedMaintPrice.text];
            }
            else
            {
                if ([string isEqualToString:@"-"])
                {
                    if ([firstLetter isEqualToString:@""])
                    {
                        isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789-" :_txtFieldProposedMaintPrice.text];
                    }
                    else
                    {
                        if (![firstLetter isEqualToString:@"-"] && range.location==0)
                        {
                            isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789-" :_txtFieldProposedMaintPrice.text];
                            
                        }
                        else
                        {
                            
                            isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtFieldProposedMaintPrice.text];
                        }
                    }
                }
                else
                {
                    isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789-" :_txtFieldProposedMaintPrice.text];
                }
            }
            //BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789-" :_txtFieldProposedInitialPrice.text];
            return isNuberOnly;
        }
        /*{
         BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtFieldProposedMaintPrice.text];
         return isNuberOnly;
         }*/
        return YES;
    }
    else
    {
        return YES;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
//End
//Nilind 06 June

- (IBAction)actionOnChkBoxDiscount:(id)sender
{
    [self endEditing];
    
    isCheckBoxClickFirstTime=YES;
    isEditedInSalesAuto=YES;
    if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isChkBoxEnable=NO;
        [_imgChkBoxDiscount setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    else
    {
        isChkBoxEnable=YES;
        [_imgChkBoxDiscount setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    //[_tblRecord reloadData];
    
    //Nilind
    
    isCheckBoxClickFirstTimeNonStan=YES;
    isEditedInSalesAuto=YES;
    if ([_imgChkBoxDiscountNonStan.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isChkBoxEnableNonStan=NO;
        [_imgChkBoxDiscountNonStan setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    else
    {
        isChkBoxEnableNonStan=YES;
        [_imgChkBoxDiscountNonStan setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    //[_tblNonStandardService reloadData];
    [self finalUpdatedTableDataWithDiscount];
    [self finalUpdatedTableDataWithDiscountNonStan];
    [self fetchFromCoreDataStandard];
    [_tblRecord reloadData];
    [_tblBundle reloadData];
    [_tblNonStandardService reloadData];
    
    
}
- (IBAction)actionOnDiscountCheckBox:(id)sender
{
}
-(void)finalUpdatedTableDataWithDiscount
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matchesDiscoutUpdate=arrAllObj[i];
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                
                /*if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
                 {
                 
                 //[matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
                 //[matchesDiscoutUpdate setValue:[arrDiscountPer objectAtIndex:i] forKey:@"discountPercentage"];
                 }
                 else
                 {
                 [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
                 [matchesDiscoutUpdate setValue:@"0" forKey:@"discountPercentage"];
                 }
                 // [matchesDiscoutUpdate setValue:[arrFinalInitialPrice objectAtIndex:i] forKey:@"finalUnitBasedInitialPrice"];
                 //[matchesDiscoutUpdate setValue:[arrUnit objectAtIndex:i] forKey:@"unit"];*/
                [matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
                [matchesDiscoutUpdate setValue:[arrDiscountPer objectAtIndex:i] forKey:@"discountPercentage"];
            }
            else  //For Bundle
            {
                
                /*if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
                 {
                 
                 // [matchesDiscoutUpdate setValue:[arrDiscountBundle objectAtIndex:i] forKey:@"discount"];
                 //[matchesDiscoutUpdate setValue:[arrDiscountPerBundle objectAtIndex:i] forKey:@"discountPercentage"];
                 }
                 else
                 {
                 [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
                 [matchesDiscoutUpdate setValue:@"0" forKey:@"discountPercentage"];
                 }*/
                [matchesDiscoutUpdate setValue:[arrDiscountBundle objectAtIndex:i] forKey:@"discount"];
                [matchesDiscoutUpdate setValue:[arrDiscountPerBundle objectAtIndex:i] forKey:@"discountPercentage"];
            }
        }
        [context save:&error1];
    }
    
}
-(void)finalUpdatedTableDataWithBundleDiscount:(NSString *)strBundleIdForTextField1
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,strBundleIdForTextField1];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matchesDiscoutUpdate=arrAllObj[i];
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                
                if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
                {
                    
                    [matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
                    [matchesDiscoutUpdate setValue:[arrDiscountPer objectAtIndex:i] forKey:@"discountPercentage"];
                }
                else
                {
                    // [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
                    // [matchesDiscoutUpdate setValue:@"0" forKey:@"discountPercentage"];
                    
                    
                    [matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
                    [matchesDiscoutUpdate setValue:[arrDiscountPer objectAtIndex:i] forKey:@"discountPercentage"];
                }
                [matchesDiscoutUpdate setValue:[arrFinalInitialPrice objectAtIndex:i] forKey:@"finalUnitBasedInitialPrice"];
                [matchesDiscoutUpdate setValue:[arrFinalMaintPrice objectAtIndex:i] forKey:@"finalUnitBasedMaintPrice"];
                
                [matchesDiscoutUpdate setValue:[arrUnit objectAtIndex:i] forKey:@"unit"];
            }
            else  //For Bundle
            {
                
                NSString *strIdBundle,*strSysNameService;
                strIdBundle=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]];
                
                if ([strIdBundle isEqualToString:strBundleIdForTextField])
                {
                    if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
                    {
                        
                        [matchesDiscoutUpdate setValue:[arrDiscountBundle objectAtIndex:i] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[arrDiscountPerBundle objectAtIndex:i] forKey:@"discountPercentage"];
                    }
                    else
                    {
                        // [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
                        //[matchesDiscoutUpdate setValue:@"0" forKey:@"discountPercentage"];
                        [matchesDiscoutUpdate setValue:[arrDiscountBundle objectAtIndex:i] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[arrDiscountPerBundle objectAtIndex:i] forKey:@"discountPercentage"];
                    }
                    [matchesDiscoutUpdate setValue:[arrFinalInitialPriceBundle objectAtIndex:i] forKey:@"finalUnitBasedInitialPrice"];
                    [matchesDiscoutUpdate setValue:[arrFinalMaintPriceBundle objectAtIndex:i] forKey:@"finalUnitBasedMaintPrice"];
                    
                    [matchesDiscoutUpdate setValue:[arrUnitBundle objectAtIndex:i] forKey:@"unit"];
                    
                }
            }
        }
        [context save:&error1];
    }
    /*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
     requestNew = [[NSFetchRequest alloc] init];
     [requestNew setEntity:entitySoldServiceStandardDetail];
     NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
     
     [requestNew setPredicate:predicate];
     
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
     sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     
     [requestNew setSortDescriptors:sortDescriptors];
     
     self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
     [self.fetchedResultsControllerSalesInfo setDelegate:self];
     
     
     // Perform Fetch
     NSError *error1 = nil;
     [self.fetchedResultsControllerSalesInfo performFetch:&error1];
     arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
     NSManagedObject *matchesDiscoutUpdate;
     if (arrAllObj.count==0)
     {
     
     }else
     {
     for (int i=0; i<arrAllObj.count; i++)
     {
     matchesDiscoutUpdate=arrAllObj[i];
     
     if (isCheckBoxClickFirstTime==YES)
     {
     if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
     {
     
     [matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
     }
     else
     {
     [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
     }
     }
     }
     [context save:&error1];
     }
     */
    [_tblBundle reloadData];
    
}
- (IBAction)actionOnCheckBoxNonStan:(id)sender
{
    [self endEditing];
    isCheckBoxClickFirstTimeNonStan=YES;
    isEditedInSalesAuto=YES;
    if ([_imgChkBoxDiscountNonStan.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isChkBoxEnableNonStan=NO;
        [_imgChkBoxDiscountNonStan setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    else
    {
        isChkBoxEnableNonStan=YES;
        [_imgChkBoxDiscountNonStan setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    
    
    
    //Nilind
    
    isCheckBoxClickFirstTime=YES;
    isEditedInSalesAuto=YES;
    if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isChkBoxEnable=NO;
        [_imgChkBoxDiscount setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    else
    {
        isChkBoxEnable=YES;
        [_imgChkBoxDiscount setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    [self finalUpdatedTableDataWithDiscount];
    [self finalUpdatedTableDataWithDiscountNonStan];
    [_tblRecord reloadData];
    [_tblNonStandardService reloadData];
    
}
//Nilind 08 June
-(void)discountStatusNonStan
{
    //strCouponStatusNonStan=@"false";
    //strCouponStatusNonStan=@"true";
    
    if ([strCouponStatusNonStan isEqualToString:@"true"])
    {
        if (chkStatus==YES)
        {
            _constViewDiscountNonStan_H.constant=0;//0
            _const_TableNonStan_H.constant=_const_TableNonStan_H.constant+ _constViewDiscountNonStan_H.constant;
        }
        else
        {
            _constViewDiscountNonStan_H.constant=50-50;
            _const_TableNonStan_H.constant=_const_TableNonStan_H.constant- _constViewDiscountNonStan_H.constant;
        }
    }
    else
    {
        _constViewDiscountNonStan_H.constant=0;//0
        _const_TableNonStan_H.constant=_const_TableNonStan_H.constant+ _constViewDiscountNonStan_H.constant;
    }
    
    //_constViewDiscountNonStan_H.constant=50;
    
}

-(void)finalUpdatedTableDataWithDiscountNonStan
{
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray*arrAllObjNonStan;
    
    arrAllObjNonStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdateNonStan;
    if (arrAllObjNonStan.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjNonStan.count; i++)
        {
            matchesDiscoutUpdateNonStan=arrAllObjNonStan[i];
            
            /*if (isCheckBoxClickFirstTimeNonStan==YES)
             {
             if ([_imgChkBoxDiscountNonStan.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
             {
             
             [matchesDiscoutUpdateNonStan setValue:[arrNonStanDiscount objectAtIndex:i] forKey:@"discount"];
             }
             else
             {
             [matchesDiscoutUpdateNonStan setValue:@"0" forKey:@"discount"];
             }
             }*/
            if ([_imgChkBoxDiscountNonStan.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
            {
                
                [matchesDiscoutUpdateNonStan setValue:[arrNonStanDiscount objectAtIndex:i] forKey:@"discount"];
                [matchesDiscoutUpdateNonStan setValue:[arrDiscountPerNonStan objectAtIndex:i] forKey:@"discountPercentage"];
            }
            else
            {
                [matchesDiscoutUpdateNonStan setValue:@"0" forKey:@"discount"];
                [matchesDiscoutUpdateNonStan setValue:@"0" forKey:@"discountPercentage"];
            }
            
        }
        [context save:&error1];
    }
}
//Nilind 14 Sept
#pragma mark - ---------Nilind 14 Sept FOOTER CHANGE -----------

- (IBAction)actionAddAfterImages:(id)sender
{
    [self endEditing];
    if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        /* UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
         delegate:self
         cancelButtonTitle:@"Cancel"
         destructiveButtonTitle:nil
         otherButtonTitles:@"Capture New", @"Gallery", nil];
         
         [actionSheet showInView:self.view];*/
        [self newAlertMethodAddImage];
        
        
    }
}

- (IBAction)actionOnCancelAfterImage:(id)sender
{
    [self endEditing];
    [_viewForAfterImage removeFromSuperview];
    
}

- (IBAction)actionOnAfterImgView:(id)sender
{
  //  [self endEditing];
  //  [self goToGlobalmage:@"Before"];
    [self attachImage];
  
}
-(void)getImageCollectionView
{
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        /*  UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    }
    else
    {
        
        //Nilind 07 June Image Caption
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackServiceSummary"];
        
        if (isFromBack)
        {
            
            //arrOfBeforeImageAll=nil;
            //arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            arrNoImage=[[NSMutableArray alloc]init];
            
            arrImageLattitude=nil;
            arrImageLattitude=[[NSMutableArray alloc]init];
            arrImageLongitude=nil;
            arrImageLongitude=[[NSMutableArray alloc]init];
            
            arrImageGraphLattitude=nil;
            arrImageGraphLattitude=[[NSMutableArray alloc]init];
            arrImageGraphLongitude=nil;
            arrImageGraphLongitude=[[NSMutableArray alloc]init];
            
            
            [defsBack setBool:NO forKey:@"isFromBackServiceSummary"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseForGraph];
            
            [self fetchImageDetailFromDataBase];
            
        }
        
        
        
        //End
        
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreviewSales"];
        if (yesFromDeleteImage)
        {
            isEditedInSalesAuto=YES;
            NSLog(@"Database edited");
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreviewSales"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrNoImage.count; k++)
            {
                
                NSDictionary *dictdat=arrNoImage[k];
                NSString *strImageName;
                if([dictdat isKindOfClass:[NSDictionary class]])
                {
                    strImageName=[dictdat valueForKey:@"leadImagePath"];
                }
                else
                {
                    strImageName=arrNoImage[k];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImagesSales"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrNoImage[k]];
                        
                    }
                }
                
            }
            if (!(arrTempBeforeImage.count==0))
            {
                
                [arrNoImage removeObjectsInArray:arrTempBeforeImage];
            }
            else if((arrTempBeforeImage.count==0))
            {
                arrNoImage=nil;
                arrNoImage=[[NSMutableArray alloc]init];
            }
            
            //nIlind 09 April
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImagesSales"];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            
            
            [defsnew synchronize];
            
            [self saveImageToCoreData];
            
            //End
            
        }
        else
        {
            
        }
        //Nilind 07 June Image Caption
        
        
        //Change in image Captions
        BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //End
        
        
        [self downloadingImagesThumbNailCheck];
        //Nilind 10 Jan
        
    }
    //...........
    [_collectionViewAfterImage reloadData];
    [_collectionViewGraphImage reloadData];
    
}

#pragma mark- ---------- FETCH IMAGE ----------
-(void)fetchImageDetailFromDataBase
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //arrGraphImage=[[NSMutableArray alloc]init];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" : leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                /*
                 [arrOfImagenameCollewctionView addObject:leadImagePath];
                 
                 //Nilind 07 June
                 
                 NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                 
                 if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                 
                 [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                 
                 }
                 else
                 {
                 
                 
                 [arrOfImageCaptionGraph addObject:strImageCaption];
                 
                 }
                 
                 
                 NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                 
                 if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                 
                 [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                 
                 } else {
                 
                 [arrOfImageDescriptionGraph addObject:strImageDescription];
                 
                 }
                 
                 
                 */
                //End
                
            }
            else
            {
                
                [arrNoImage addObject:dict_ToSendLeadInfo];
                
                //Nilind 07 June
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageLongitude addObject:strLong];
                    
                }
                //End
                
            }
            
            
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    }
    else
    {
    }
}


-(void)openGalleryy{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera
{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==7)
    {
        
        NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        NSArray *arrOfImagesDetail=arrNoImage;
        
        arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dictData=arrOfImagesDetail[k];
                
                [arrOfBeforeImages addObject:dictData];
                
            }else{
                
                [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
                
            }
            
        }
        
        arrOfImagesDetail=arrOfBeforeImages;
        
        if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
            [global AlertMethod:Info :NoBeforeImg];
        }else if (arrOfImagesDetail.count==0){
            [global AlertMethod:Info :NoBeforeImg];
        }
        else {
            NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
            for (int k=0; k<arrOfImagesDetail.count; k++) {
                if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dict=arrOfImagesDetail[k];
                    [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
                }else{
                    
                    [arrOfImagess addObject:arrOfImagesDetail[k]];
                    
                }
            }
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                     bundle: nil];
            ImagePreviewSalesAuto
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
            objByProductVC.arrOfImages=arrOfImagess;
            
            [self.navigationController pushViewController:objByProductVC animated:YES];
            
        }
    }
    else if (buttonIndex == 0)
    {
        NSLog(@"The CApture Image.");
        
        
        if (arrNoImage.count<10)
        {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                         }
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 3 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //............
        
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"The Gallery.");
        if (arrNoImage.count<10)
        {
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         }
                                         else
                                         {
                                             
                                             
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 3 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    
}

//============================================================================

#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==_collectionViewAfterImage)
    {
        return arrNoImage.count;
    }
    else
    {
        return arrGraphImage.count;
    }
    
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_collectionViewAfterImage)
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    else
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *str = [arrNoImage objectAtIndex:indexPath.row];
    if (collectionView==_collectionViewAfterImage)
    {
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreview : strIndex];
            
        }
    }
    else
    {
        // NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *str;// = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrGraphImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    }
    
    
}
//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        
        
        
    }else if (arrOfImagesDetail.count==0){
        
        
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1)
        {
        }
        else if (arrOfImagess.count==2)
        {
            
        }
        else
        {
        }
        //_const_ImgView_H.constant=100;  temp comment
        //_const_SaveContinue_B.constant=62;
        
        //21 April 2020
        //[self downloadImages:arrOfImagess];
        
    }
}


-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            [self ShowFirstImage:str:k];
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    //NIlind 14 Sept
    
    [_collectionViewAfterImage reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImage : name : indexx];
    
}

- (UIImage*)loadImage :(NSString*)name :(int)indexxx
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (indexxx==0)
    {
        
        
    } else if(indexxx==1)
    {
        
        
        
    }else{
        
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}
-(void)goingToPreview :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                 bundle: nil];
        ImagePreviewSalesAuto
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        // objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
        
        if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
        }
        else
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=@"Open";
        }
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaption;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescription;
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backfromDynamicView"];
    [defs synchronize];
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    //NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
    NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@%@.jpg",strDate,strTime,strLeadId];
    
    
    
    [arrNoImage addObject:strImageNamess];
    //UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    // [self saveImage:chosenImage :strImageNamess];
    
    [self resizeImage:chosenImage :strImageNamess];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //Lat long code
       
       CLLocationCoordinate2D coordinate = [global getLocation] ;
       NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
       NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
       [arrImageLattitude addObject:latitude];
       [arrImageLongitude addObject:longitude];
    
    //imageCaption
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption) {
        
        [self alertViewCustom];
        
    } else {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        [self saveImageToCoreData];
        
    }
    
   
    
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender
{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
        // [_scrollViewService setContentOffset:bottomOffset animated:YES];
        [_scrollViewService setContentOffset:CGPointZero animated:YES];
        [self saveImageToCoreData];
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    //    CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
    //    [_scrollViewService setContentOffset:bottomOffset animated:YES];
    [self saveImageToCoreData];
    
}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=8;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
            [_scrollViewService setContentOffset:bottomOffset animated:YES];
            
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
        [_scrollViewService setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
//.................................................................................
-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    //CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    CGRect rect = CGRectMake(0.0, 0.0, image.size.width/2, image.size.height/2);
    
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}
#pragma mark- Save & Remove Image  Methods
//============================================================================
//============================================================================

- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
#pragma mark- Note
    
   /* NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL yesfromback=[defs boolForKey:@"backfromDynamicView"];
    if (yesfromback) {
        
        [defs setBool:NO forKey:@"backfromDynamicView"];
        [defs synchronize];
        
    }
    else
    {
        
        [self deleteImageFromCoreDataSalesInfo];
        
        for (int k=0; k<arrNoImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            if ([arrNoImage[k] isKindOfClass:[NSString class]])
            {
                
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[arrNoImage objectAtIndex:k]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                NSError *error1;
                [context save:&error1];
            }
            else
            {
                NSDictionary *dictData=[arrNoImage objectAtIndex:k];
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                NSError *error1;
                [context save:&error1];
                
            }
            
        }
        for (int k=0; k<arrGraphImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            NSDictionary *dict=[arrGraphImage objectAtIndex:k];
            if ([arrGraphImage[k] isKindOfClass:[NSDictionary class]])
            {
                
                objImageDetail.createdBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdBy"]];
                objImageDetail.createdDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdDate"]];
                objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
                objImageDetail.leadImageId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageId"]];
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageType"]];
                objImageDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"modifiedBy"]];
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                objImageDetail.leadImageCaption=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageCaption"]];
                objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
                
                
                //Latlong code
                
                //NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
                objImageDetail.latitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"latitude"]];
                // NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
                objImageDetail.longitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"longitude"]];
                
               
                NSError *error1;
                [context save:&error1];
            }
        }
        
        
        
    }
    */
    //........................................................................
    // [self fetchForUpdate];
    
    //[self fetchImageFromCoreDataStandard];
    
}
-(void)deleteImageFromCoreDataSalesInfo
{
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
    [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    [allDataImage setPredicate:predicateImage];
    
    [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage) {
        [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
}
#pragma mark -------------- ADDING GRAPH IMAGE ----------------

- (IBAction)actionOnAddGraphImage:(id)sender
{
    [self endEditing];
    if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"firstGraphImage"];
        [defs setBool:NO forKey:@"servicegraph"];
        
        [defs synchronize];
        
        /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         EditGraphViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewController"];
         objSignViewController.strLeadId=strLeadId;
         objSignViewController.strCompanyKey=strCompanyKey;
         objSignViewController.strUserName=strUserName;
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
        objSignViewController.strLeadId=strLeadId;
        objSignViewController.strCompanyKey=strCompanyKey;
        objSignViewController.strUserName=strUserName;
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    }
}

- (IBAction)actionOnCancelBeforeImage:(id)sender
{
    [self endEditing];
    [_viewForGraphImage removeFromSuperview];
}

- (IBAction)actionOnGraphImageFooter:(id)sender
{
    [self endEditing];
    [self goToGlobalmage:@"Graph"];
   /* CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewForFinalSave.frame.origin.y-_viewForGraphImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewForGraphImage.frame.size.height);
    [_viewForGraphImage setFrame:frameFor_view_BeforeImageInfo];
    [_collectionViewGraphImage reloadData];
    [self.view addSubview:_viewForGraphImage];*/
}
-(void)goingToPreviewGraph :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrGraphImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++)
    {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
        {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }
        else
        {
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++)
        {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        GraphImagePreviewViewController
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewController"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strLeadId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        //  objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
        
        if ([strLeadStatusGlobal isEqualToString:@"Complete"] &&  [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame)
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
        }
        else
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=@"Open";
        }
        
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
-(void)fetchImageDetailFromDataBaseForGraph
{
    
    arrGraphImage=nil;
    
    arrGraphImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" :leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                
                //[arrOfImagenameCollewctionView addObject:leadImagePath];
                [arrGraphImage addObject:dict_ToSendLeadInfo];
                
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
            }
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    } else {
    }
    
    [self downloadImagesGraphs:arrGraphImage];
    
}
-(void)downloadImagesGraphs :(NSArray*)arrOfImagesDownload
{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        // NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
        NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrOfImagesDownload objectAtIndex:k];
        }
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    [_collectionViewGraphImage reloadData];
    
}
#pragma mark- ----------Textview Delegate Method-----------


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView

{
    
    if(textView==_textViewDescription_PopUp)
        
    {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        
    }
    
    
    
    return YES;
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView

{
    
    if(textView==_textViewDescription_PopUp)
        
    {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
        
    }
    
    
    
    [self.view endEditing:YES];
    
    
    
    return YES;
    
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        [txtFieldCaption resignFirstResponder];
        [txtViewImageDescription resignFirstResponder];
        [_textViewDescription_PopUp resignFirstResponder];
        [_textViewInitialDescription resignFirstResponder];
        [_textViewMaintDescription resignFirstResponder];
        [_txtViewIntroductionLetter resignFirstResponder];
        [_txtViewTermsServices resignFirstResponder];
        return NO;
    }
    if(textView==_textViewDescription_PopUp)
    {
        if(selectionType==3)// service
        {
            /*
             
             strIsChangedServiceDesc = @"false";
             strIsChangedScopeDesc = @"false";
             strIsChangedTargetDesc = @"true";*/
            
            strIsChangedServiceDesc = @"true";
            strIsChangedScopeDesc = @"false";
            strIsChangedTargetDesc = @"false";
        }
        else if(selectionType==2)
        {
            strIsChangedServiceDesc = @"false";
            strIsChangedScopeDesc = @"true";
            strIsChangedTargetDesc = @"false";
        }
        else if(selectionType==1) //target
        {
            /*
             strIsChangedServiceDesc = @"true";
             strIsChangedScopeDesc = @"false";
             strIsChangedTargetDesc = @"false";*/
            
            strIsChangedServiceDesc = @"false";
            strIsChangedScopeDesc = @"false";
            strIsChangedTargetDesc = @"true";
        }
    }
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}
#pragma mark- NILIND 30 Oct bundle change

-(void)heightManage
{
    NSMutableArray *arrHeight;
    arrHeight=[[NSMutableArray alloc]init];
    [self fetchFromCoreDataStandard];
    [self fetchFromCoreDataNonStandard];
    for (int i=0; i<arrSysName.count; i++)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysName objectAtIndex:i]]];
        if ([str isEqualToString:@"0"])
        {
            [arrHeight addObject:[NSString stringWithFormat:@"%.2f",_tblRecord.rowHeight-55]];
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                [arrHeight addObject:[NSString stringWithFormat:@"%.2f",_tblRecord.rowHeight-55]];
            }
            else
            {
                [arrHeight addObject:[NSString stringWithFormat:@"%.2f",_tblRecord.rowHeight+20]];
            }
        }
    }
    double sum=0;
    for (int i=0; i<arrHeight.count; i++)
    {
        sum=sum+[[arrHeight objectAtIndex:i]doubleValue];
    }
    
    for(int i=0;i<arrAdditionalParamterDcs.count;i++)
    {
        NSArray *arr=[arrAdditionalParamterDcs objectAtIndex:i];
        if([arr isKindOfClass:[NSArray class]])
        {
            if (arr.count>0)
            {
                sum=sum+450;
            }
        }
        else
        {
            sum=0;
            
        }
    }
    //_tblRecord.backgroundColor=[UIColor purpleColor];
    _const_TableStan_H.constant=sum+10;//+(400*(arrAdditionalParamterDcs.count));
    //25*16=400
    NSMutableArray *arrBundleHeight;
    arrBundleHeight=[[NSMutableArray alloc]init];
    for (int i=0; i<arrTempBundleServiceSysName.count; i++)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrTempBundleServiceSysName objectAtIndex:i]]];
        if ([str isEqualToString:@"0"])
        {
            [arrBundleHeight addObject:[NSString stringWithFormat:@"%.2f",_tblBundle.rowHeight-65+10+10+15]];
            
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                //return 227-65;
                [arrBundleHeight addObject:[NSString stringWithFormat:@"%.2f",_tblBundle.rowHeight-65+10+10+15]];
                //return _tblBundle.rowHeight-65+10;
                
            }
            else
            {
                //return 227;
                [arrBundleHeight addObject:[NSString stringWithFormat:@"%.2f",_tblBundle.rowHeight+10+10+15]];
                
                //return _tblBundle.rowHeight+10;
            }
        }
        
    }
    int sumBundle=0;
    for (int i=0; i<arrBundleHeight.count; i++)
    {
        sumBundle=sumBundle+[[arrBundleHeight objectAtIndex:i]doubleValue];
    }
    
    _constTblBundle_H.constant=sumBundle+25;//(260)*arrBundleHeight.count;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    if([defs boolForKey:@"fromServiceSummaryScroll"]==YES)
    {
        
        [defs setBool:NO forKey:@"fromServiceSummaryScroll"];
        [defs synchronize];
        [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableStan_H.constant+_constTblBundle_H.constant+160+40+_const_ViewDiscountCoupon_H.constant)];
    }
    else
    {
        
        [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableStan_H.constant+_constTblBundle_H.constant)];
        
    }
    if([defs boolForKey:@"backStan"]==NO)
    {
        [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableStan_H.constant+_constTblBundle_H.constant+250+_const_ViewDiscountCoupon_H.constant)];
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        NSString *sectionName;
        sectionName=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        return sectionName;
    }
    else
    {
        return @"";
    }
    
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        NSString *sectionName;
        sectionName=[NSString stringWithFormat:@"%@ Footer",[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        return sectionName;
    }
    else
    {
        return @"";
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==2)
    {
        return arrBundleRow.count;
    }
    else
    {
        return 1;
        //return arrSoldServiceStandardId.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        return 35.0f;
    }
    else
    {
        return 0.0f;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        return 30.0f;
    }
    else
    {
        /*if (tableView==_tblRecord)
         {
         return 50;
         }*/
        return 0.0f;
    }
}
-(void)fetchFromCoreDataStandardForBundle:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    
    
    
    arrInitialPriceBundle=[[NSMutableArray alloc]init];
    arrDiscountPerBundle=[[NSMutableArray alloc]init];
    
    arrDiscountBundle=[[NSMutableArray alloc]init];
    arrMaintenancePriceBundle=[[NSMutableArray alloc]init];
    arrFrequencyNameBundle=[[NSMutableArray alloc]init];
    arrServiceNameBundle=[[NSMutableArray alloc]init];
    arrStanIsSoldBundle=[[NSMutableArray alloc]init];
    arrSysNameBundle=[[NSMutableArray alloc]init];
    
    arrUnitBundle=[[NSMutableArray alloc]init];
    arrFinalInitialPriceBundle=[[NSMutableArray alloc]init];
    arrFinalMaintPriceBundle=[[NSMutableArray alloc]init];
    
    arrPackageNameIdBundle=[[NSMutableArray alloc]init];
    arrBillingFreqSysNameBundle=[[NSMutableArray alloc]init];
    arrBundleId=[[NSMutableArray alloc]init];
    
    // arrBundleRow=[[NSMutableArray alloc]init];
    NSMutableArray *arrBundleNew;
    arrBundleNew=[[NSMutableArray alloc]init];
    
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                [arrFrequencyNameBundle addObject:[matches valueForKey:@"serviceFrequency"]];
                
                if ([[matches valueForKey:@"serviceSysName"]isEqualToString:@""])
                {
                    [arrServiceNameBundle addObject:@""];
                }
                else
                {
                    [arrServiceNameBundle addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                }
                
                [arrStanIsSoldBundle addObject: [matches valueForKey:@"isSold"]];
                
                [arrSysNameBundle addObject:[matches valueForKey:@"serviceSysName"]];
                
                //Nilind 6 oct
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isTBD"]]isEqualToString:@"true"])
                {
                    [arrInitialPriceBundle addObject:@"TBD"];
                    [arrDiscountPerBundle addObject:@"TBD"];
                    [arrDiscountBundle addObject:@"TBD"];
                    [arrMaintenancePriceBundle addObject:@"TBD"];
                    [arrFinalInitialPriceBundle addObject:@"TBD"];
                    [arrFinalMaintPriceBundle addObject:@"TBD"];
                }
                else
                {
                    [arrInitialPriceBundle addObject:[matches valueForKey:@"initialPrice"]];
                    [arrDiscountBundle addObject:[matches valueForKey:@"discount"]];
                    [arrMaintenancePriceBundle addObject:[matches valueForKey:@"maintenancePrice"]];
                    [arrFinalInitialPriceBundle addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                    [arrFinalMaintPriceBundle addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                    [arrDiscountPerBundle addObject:[matches valueForKey:@"discountPercentage"]];
                    
                }
                [arrUnitBundle addObject:[matches valueForKey:@"unit"]];
                
                [arrPackageNameIdBundle addObject:[matches valueForKey:@"packageId"]];
                [arrBillingFreqSysNameBundle addObject:[matches valueForKey:@"billingFrequencySysName"]];
                [arrBundleId addObject:[matches valueForKey:@"bundleId"]];
                
                
            }
        }
    }
    // [self heightManage];
}
/*-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 
 }*/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(tableView==_tblBundle)
    {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 35)];
        //headerView.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        headerView.backgroundColor = [UIColor colorWithRed:246.0/255 green:246.0/255 blue:246.0/255 alpha:1];
        
        //headerView.backgroundColor = [UIColor colorWithRed:211.0/255 green:175.0/255 blue:72.0/255 alpha:1];
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = section + 1000;
        button.frame = CGRectMake(_tblBundle.frame.origin.x+5, 2, 30, 30);
        
        [self fetchFromCoreDataStandardForSectionCheckBoxBundle:[arrBundleRow objectAtIndex:section]];
        if ([strSoldStatusBundle isEqualToString:@"true"])
        {
            [button setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [button setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            
        }
        
        [button addTarget:self action:@selector(checkBundleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:button];
        [button setTitle:[arrBundleRow objectAtIndex:section] forState:UIControlStateNormal];
        // button.titleLabel.textColor=[UIColor clearColor];
        
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x+button.frame.size.width+10, 0, 250, 35)];
        
        
        NSString *sectionName;
        sectionName=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        headerLabel.text =sectionName;
        headerLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        headerLabel.textColor = [UIColor blackColor];
        //headerLabel.backgroundColor = [UIColor lightGrayColor];
        [headerView addSubview:headerLabel];
        
        UIButton *buttonDelete = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonDelete.tag = section + 1000;
        buttonDelete.frame = CGRectMake(CGRectGetMaxX(headerView.frame)-70, 0, 35, 35);
        //[buttonDelete setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        //buttonDelete.backgroundColor=[UIColor redColor];
        [buttonDelete addTarget:self action:@selector(deleteBundleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:buttonDelete];
        [buttonDelete setTitle:[arrBundleRow objectAtIndex:section]  forState:UIControlStateNormal];
        buttonDelete.titleLabel.textColor=[UIColor clearColor];
        [buttonDelete setImage:[UIImage imageNamed:@"ic_action_delete.png"] forState:UIControlStateNormal];
        [buttonDelete setBackgroundImage:[UIImage imageNamed:@"ic_action_delete.png"] forState:UIControlStateNormal];
        if (chkStatus==YES)
        {
            button.enabled=NO;
            buttonDelete.enabled=NO;
        }
        else
        {
            button.enabled=YES;
            buttonDelete.enabled=YES;
            
        }
        return headerView;
    }
    else
    {
        UIView *headerView = [[UIView alloc] init];
        return headerView;
        
    }
}
- (IBAction)deleteBundleButtonPressed:(UIButton *)sender
{
    // NSInteger section = sender.tag - 1000;
    //UIButton *selectedButton = (UIButton *) sender;
    
    //[self deleteBundleButtonPressed:sender.titleLabel.text];
    NSLog(@"Section Bundle Id %@",sender.titleLabel.text);
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert!"
                               message:@"Are you sure want to delete Bundle"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              [self fetchFromCoreDataStandard];
                              [self fetchFromCoreDataStandardForBundleForDiscountCupon:[NSString stringWithFormat:@"%@",sender.titleLabel.text]];
                              [self deleteBundleFromCoreDataSalesInfo:[NSString stringWithFormat:@"%@",sender.titleLabel.text]];
                              [self fetchFromCoreDataStandard];
                              
                              
                              
                              for (int i=0;i< arrSoldServiceStandardIdBundle.count; i++)
                              {
                                  NSString *strId=[arrSoldServiceStandardIdBundle objectAtIndex:i];
                                  [self fetchForAppliedDiscountServiceFromCoreData:strId];
                              }
                              
                              [_tblBundle reloadData];
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             
                             
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)checkBundleButtonPressed:(UIButton *)sender
{
    NSInteger section = sender.tag - 1000;
    
    UIButton *selectedButton = (UIButton *) sender;
    NSLog(@"Section Bundle Id %@",selectedButton.titleLabel.text);
    if([selectedButton.currentImage isEqual:[UIImage imageNamed:@"check_box_2.png"]])
        //if(selectedButton.selected==YES)
    {
        [selectedButton setSelected:NO];
        [selectedButton setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [self updateBundle:selectedButton.titleLabel.text :@"false"];
        
        
    }
    else
    {
        [selectedButton setSelected:YES];
        [selectedButton setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [self updateBundle:selectedButton.titleLabel.text :@"true"];
    }
    
}
-(void)updateBundle: (NSString*)strButtonBundleId : (NSString*)status
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,strButtonBundleId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            //if([[matches valueForKey:@"bundleId"]isEqualToString:strButtonBundleId])
            //{
            [matches setValue:status forKey:@"isSold"];
            [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
            // [context save:&error1];
            //}
            
        }
        [context save:&error1];
    }
    
    [self fetchFromCoreDataStandard];
    
}
-(void)deleteBundleFromCoreDataSalesInfo:(NSString*)strBundleIdd
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    for (int i=0; i<Data.count; i++)
    {
        NSManagedObject *dataMatches=[Data objectAtIndex:i];
        if ([[dataMatches valueForKey:@"bundleId"]isEqualToString:strBundleIdd])
        {
            [context deleteObject:dataMatches];
            NSError *saveError = nil;
            [context save:&saveError];
            
        }
    }
    
    /*for (NSManagedObject * data in Data)
     {
     [context deleteObject:data];
     }*/
    
    //[self fetchFromCoreDataStandard];
    [self heightManage];
    //[self fetchFromCoreDataStandard];
    
    // [_tblBundle reloadData];
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    //if (tableView==_tblBundle)
    //{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 30)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(_tblBundle.frame.origin.x+5, 0, _tblBundle.frame.size.width/2, 30)];
    [self fetchFromCoreDataStandardForSectionCheckBoxBundle:[arrBundleRow objectAtIndex:section]];
    headerLabel.text =[NSString stringWithFormat:@"Total Initial Cost = $%.2f",totalInitialBundle];
    headerLabel.font=[UIFont boldSystemFontOfSize:12];
    headerLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabel];
    
    UILabel *headerLabelMaint = [[UILabel alloc] initWithFrame:CGRectMake(headerLabel.frame.origin.x+headerLabel.frame.size.width+120, 0,_tblBundle.frame.size.width/2, 30)];
    headerLabelMaint.text =[NSString stringWithFormat:@"Total Maint Cost = $%.2f",totalMaintBundle];
    CGSize expectedLabelSize = [ headerLabelMaint.text sizeWithFont:headerLabelMaint.font constrainedToSize:CGSizeMake(headerLabelMaint.frame.size.width, FLT_MAX) lineBreakMode:headerLabelMaint.lineBreakMode];
    headerLabelMaint.frame=CGRectMake(_tblBundle.frame.size.width -expectedLabelSize.width-10, 0, expectedLabelSize.width, 30);
    
    headerLabelMaint.font=[UIFont boldSystemFontOfSize:14];
    
    headerLabelMaint.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabelMaint.textColor = [UIColor blackColor];
    headerLabelMaint.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabelMaint];
    
    return headerView;
    //}
    /*  else if (tableView==_tblRecord)
     {
     
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblRecord.frame.size.width, 40)];
     headerView.backgroundColor = [UIColor yellowColor];
     
     /*UILabel *lbl;
     UITextField *txt;
     
     _viewFooter.frame=CGRectMake(_tblRecord.frame.origin.x, 0, _tblRecord.frame.size.width, 20);
     for (int i=0; i<3; i++)
     {
     _viewFooter=[[UIView alloc]init];
     lbl=[[UILabel alloc]init];
     txt=[[UITextField alloc]init];
     //_viewFooter.backgroundColor=[UIColor redColor];
     if(i==0)
     {
     _viewFooter.frame=CGRectMake(_tblRecord.frame.origin.x, 0, _tblRecord.frame.size.width, 20);
     lbl.frame=CGRectMake(10, 0,_viewFooter.frame.size.width/2, 20);
     lbl.text=[NSString stringWithFormat:@"Label %d",i];
     lbl.font=[UIFont boldSystemFontOfSize:14];
     
     txt.frame=CGRectMake(CGRectGetMaxX(lbl.frame), lbl.frame.origin.y,_viewFooter.frame.size.width/2-20, 20);
     
     txt.text=[NSString stringWithFormat:@"Text %d",i];
     txt.font=[UIFont boldSystemFontOfSize:14];
     txt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     txt.delegate=self;
     txt.layer.borderWidth=1.0;
     
     txt.layer.cornerRadius=5.0;
     
     txt.tag=(10*i)+10;
     
     [_viewFooter addSubview:lbl];
     [_viewFooter addSubview:txt];
     
     }
     else
     {
     _viewFooter.frame=CGRectMake(_tblRecord.frame.origin.x, (20+5)*(i), _tblRecord.frame.size.width, 20);
     
     lbl.frame=CGRectMake(10, 0,_viewFooter.frame.size.width/2, 20);
     lbl.text=[NSString stringWithFormat:@"Label %d",i];
     txt.frame=CGRectMake(CGRectGetMaxX(lbl.frame), lbl.frame.origin.y,_viewFooter.frame.size.width/2-20, 20);
     txt.text=[NSString stringWithFormat:@"Text %d",i];
     
     lbl.font=[UIFont boldSystemFontOfSize:14];
     txt.font=[UIFont boldSystemFontOfSize:14];
     
     txt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     txt.delegate=self;
     
     txt.layer.borderWidth=1.0;
     txt.tag=(10*i)+10;
     
     txt.layer.cornerRadius=5.0;
     [_viewFooter addSubview:lbl];
     [_viewFooter addSubview:txt];
     }
     
     [headerView addSubview:_viewFooter];
     
     }
     headerView.frame=CGRectMake(headerView.frame.origin.x, headerView.frame.origin.y, headerView.frame.size.width, 20* 3.5);
     
     return headerView;
     
     }
     else
     {
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 30)];
     return headerView;
     
     }*/
}
-(void)fetchFromCoreDataStandardForSectionCheckBoxBundle:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    strSoldStatusBundle=@"false";
    NSMutableArray *arrTotalInitialCostBundle,*arrTotalMaintCostBundle;
    arrTotalInitialCostBundle=[[NSMutableArray alloc]init];
    arrTotalMaintCostBundle=[[NSMutableArray alloc]init];
    totalInitialBundle=0;totalMaintBundle=0;
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                strSoldStatusBundle=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isSold"]];
                
                
                
                NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                if ([str isEqualToString:@"0"])
                {
                    totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                    totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                    
                }
                else
                {
                    if (str.length==0 || [str isEqualToString:@"(null)"])
                    {
                        totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                        totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                        
                    }
                    else
                    {
                        totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedInitialPrice"]]doubleValue];
                        
                        totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedMaintPrice"]]doubleValue];
                    }
                }
                
                //totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                //totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                //break;
            }
        }
    }
}
- (IBAction)actionOnNotesHistory:(id)sender
{
     [self endEditing];
     [self goToHistory];
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    [self endEditing];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ClockInOutViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
-(void)updateLeadIdDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        matches=arrAllObj[0];
        
        [matches setValue:_buttonProposedInitialServiceDate.titleLabel.text forKey:@"initialServiceDate"];
        
        if([_buttonProposedInitialServiceDate.currentTitle isEqualToString:@"Select Date"])
        {
            [matches setValue:@"" forKey:@"initialServiceDate"];
            
        }
        [matches setValue:_btnProposedServiceMonth.titleLabel.text forKey:@"recurringServiceMonth"];
        if(chkForLost==YES)
        {
            [matches setValue:@"Lost" forKey:@"stageSysName"];
            [matches setValue:@"Complete" forKey:@"statusSysName"];
        }
        
        
    }
    [context save:&error1];
}

- (IBAction)actionOnBtnMarkAsLost:(id)sender
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert!"
                                  message:@"Are you sure to mark Opportunity as lost?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             chkForLost=YES;
                             [self updateLeadIdDetail];
                             Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
                             NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
                             if (netStatusWify1== NotReachable)
                             {
                                 //[global AlertMethod:@"Alert!":@"No Internet connection available"];
                             }
                             else
                             {
                                 /*[DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
                                  
                                  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
                                  [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];*/
                                 
                             }
                            [self goToAppointment];
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"NO"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 chkForLost=YES;
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(NSArray*)calculationForParamterBasedService:(NSString *)strServiceSysNamePara
{
    NSMutableArray *arrOtherNonDefault;
    arrOtherNonDefault=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        
        NSArray *arrServicePara=[dict valueForKey:@"Services"];
        
        for(int j=0;j<arrServicePara.count;j++)
        {
            NSDictionary *dictForJ=[arrServicePara objectAtIndex:j];
            
            if ([strServiceSysNamePara isEqualToString:[NSString stringWithFormat:@"%@",[dictForJ valueForKey:@"SysName"]]])
            {
                NSArray *arrServiceParametersDcs=[dictForJ valueForKey:@"ServiceParametersDcs"];
                
                for (int k=0; k<arrServiceParametersDcs.count; k++)
                {
                    NSDictionary *dictForK=[arrServiceParametersDcs objectAtIndex:k];
                    
                    if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"0"])
                    {
                        [arrOtherNonDefault addObject:dictForK];
                    }
                }
                
                break;
                
            }
        }
        
        
    }
    return arrOtherNonDefault;
    
}


#pragma mark- final update parameter data
-(void)finalUpdatedParameterData:(NSMutableArray*) arrTempPara :(long)row
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrUpdate;
    arrUpdate = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesUpdate;
    if (arrUpdate.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrUpdate.count; i++)
        {
            matchesUpdate=arrUpdate[i];
            
            if (i==row)
            {
                [matchesUpdate setValue:arrTempPara forKey:@"additionalParameterPriceDcs"];
                [arrAdditionalParamterDcs replaceObjectAtIndex:i withObject:arrTempPara];
                
            }
        }
        [context save:&error1];
    }
    //[self fetchFromCoreDataStandard];
    // [_tblRecord reloadData];
    
}
#pragma mark- final update parameter data
-(void)finalUpdatedOnSaveForTotalInitialAndMaintPrice
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrUpdate;
    arrUpdate = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrSoldServiceIdOfAllService=[[NSMutableArray alloc]init];
    arrAllUnsoldServiceId=[[NSMutableArray alloc]init];
    NSManagedObject *matchesUpdate;
    if (arrUpdate.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrUpdate.count; k++)
        {
            matchesUpdate=arrUpdate[k];
            
            if([[matchesUpdate valueForKey:@"isSold"]isEqualToString:@"true"]||[[matchesUpdate valueForKey:@"isSold"]isEqualToString:@"True"])
            {
                [arrSoldServiceIdOfAllService addObject:[matchesUpdate valueForKey:@"soldServiceStandardId"]];
            }
            else
            {
                [arrAllUnsoldServiceId addObject:[matchesUpdate valueForKey:@"soldServiceStandardId"]];
            }
            
            if ([[NSString stringWithFormat:@"%@",[matchesUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                NSArray *arrAdditionalPara=[matchesUpdate valueForKey:@"additionalParameterPriceDcs"];
                
                float totalParaInitial=0.0,totalParaMaint=0.0;
                
                if(arrAdditionalPara.count>0)
                {
                    for (int i=0; i<arrAdditionalPara.count; i++)
                    {
                        NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
                        totalParaInitial=totalParaInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]floatValue];
                        totalParaMaint=totalParaMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]floatValue];
                        
                    }
                    //totalParaInitial=totalParaInitial+[[matchesUpdate valueForKey:@"initialPrice"]floatValue];
                    //totalParaMaint=totalParaMaint+[[matchesUpdate valueForKey:@"maintenancePrice"]floatValue];
                }
                totalParaInitial=totalParaInitial+([[matchesUpdate valueForKey:@"initialPrice"]floatValue])*[[matchesUpdate valueForKey:@"unit"]floatValue];
                
                //commented by saavan
                //totalParaInitial=totalParaInitial-[[matchesUpdate valueForKey:@"discount"]floatValue];
                
                totalParaMaint=totalParaMaint+[[matchesUpdate valueForKey:@"maintenancePrice"]floatValue]*[[matchesUpdate valueForKey:@"unit"]floatValue];
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalParaInitial] forKey:@"totalInitialPrice"];
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalParaMaint] forKey:@"totalMaintPrice"];
                
                
                //For Billing Price Calculation
                
                NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
                strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]]];
                
                strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"billingFrequencySysName"]]]];
                
                
                float totalBillingFreqCharge=0.0;
                
                if ([[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"OneTime"]||[[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"One Time"])
                {
                    
                    @try {
                        totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                        
                    } @catch (NSException *exception) {
                        totalBillingFreqCharge=0;
                    } @finally {
                        
                    }
                    
                    
                }
                else
                {
                    @try
                    {
                        totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                        
                    }
                    @catch (NSException *exception)
                    {
                        totalBillingFreqCharge=0;
                    }
                    @finally
                    {
                        
                    }
                    
                    
                }
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalBillingFreqCharge] forKey:@"billingFrequencyPrice"];
                //End
            }
            else
            {  //Bundle
                float totalParaInitial=0.0,totalParaMaint=0.0;
                totalParaInitial=totalParaInitial+([[matchesUpdate valueForKey:@"initialPrice"]floatValue])*[[matchesUpdate valueForKey:@"unit"]floatValue];
                totalParaMaint=totalParaMaint+[[matchesUpdate valueForKey:@"maintenancePrice"]floatValue]*[[matchesUpdate valueForKey:@"unit"]floatValue];
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalParaInitial] forKey:@"totalInitialPrice"];
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalParaMaint] forKey:@"totalMaintPrice"];
                
                
                //For Billing Price Calculation
                
                NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
                strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]]];
                
                strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"billingFrequencySysName"]]]];
                
                
                float totalBillingFreqCharge=0.0;
                
                if ([[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"OneTime"]||[[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"One Time"])
                {
                    @try {
                        totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                        
                    } @catch (NSException *exception) {
                        totalBillingFreqCharge=0;
                    } @finally {
                        
                    }
                    
                }
                else
                {
                    @try {
                        totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                        
                    } @catch (NSException *exception) {
                        totalBillingFreqCharge=0;
                    } @finally {
                        
                    }
                    
                    
                }
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalBillingFreqCharge] forKey:@"billingFrequencyPrice"];
                //End
                
                
            }
        }
        [context save:&error1];
    }
    
}
-(void)finalUpdatedNonStanOnSaveForBillilngFreqPrice
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray*arrAllObjNonStan;
    
    arrAllObjNonStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdateNonStan;
    if (arrAllObjNonStan.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjNonStan.count; i++)
        {
            matchesDiscoutUpdateNonStan=arrAllObjNonStan[i];
            
            //Billing Freq Calculation
            
            NSString *strBillingFreqYearOccurence;
            strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesDiscoutUpdateNonStan valueForKey:[NSString stringWithFormat:@"%@",@"billingFrequencySysName"]]]];
            
            if([strBillingFreqYearOccurence isEqualToString:@"(null)"])
            {
                strBillingFreqYearOccurence=@"0";
            }
            float totalBillingFreqCharge=0.0;
            @try {
                totalBillingFreqCharge=([[matchesDiscoutUpdateNonStan valueForKey:[NSString stringWithFormat:@"%@",@"initialPrice"]]floatValue]-[[matchesDiscoutUpdateNonStan valueForKey:@"discount"]floatValue]) /[strBillingFreqYearOccurence floatValue];
                
            } @catch (NSException *exception) {
                totalBillingFreqCharge=0.0;
            } @finally {
                
            }
            [matchesDiscoutUpdateNonStan setValue:[NSString stringWithFormat:@"%.5f",totalBillingFreqCharge] forKey:@"billingFrequencyPrice"];
            
        }
        [context save:&error1];
    }
}

- (IBAction)actionOnApplyDiscount:(id)sender
{
    [self fetchFromCoreDataStandard];
    if(_txtApplyDiscount.text.length==0)
    {
        [global AlertMethod:@"Alert!" :@"Please enter coupon code"];
        _txtApplyDiscount.text=@"";
    }
    else if(arrAllObj.count==0)
    {
        
        [global AlertMethod:@"Alert!" :@"Please add service first to apply coupon"];
        _txtApplyDiscount.text=@"";
        
    }
    /*else if(arrInitialPrice.count==0 && arrTempBundleServiceSysName.count>0)
     {
     
     [global AlertMethod:@"Alert!" :@"Please add normal service first to apply coupon"];
     _txtApplyDiscount.text=@"";
     
     }*/
    else
    {
        arrDiscountCoupon=[[NSMutableArray alloc]init];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        arrDiscountCoupon=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
        if([arrDiscountCoupon isKindOfClass:[NSArray class]])
        {
            arrTempCoupon=[[NSMutableArray alloc]init];
            
            NSString *strCouponDiscount=_txtApplyDiscount.text;
            
            //14 June Temp
            NSString *strDiscountUsage=[dictDiscountUsageFromCode valueForKey:strCouponDiscount];
            
            BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount:strDiscountUsage];
            //End
            //BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount];
            if (chkDiscountApplied==YES)
            {
                [global AlertMethod:@"Alert!" :@"Coupon already applied"];
            }
            else
            {
                BOOL chkValidDiscount;
                chkValidDiscount=NO;
                for(int i=0;i<arrDiscountCoupon.count;i++)
                {
                    
                    NSDictionary *dict=[arrDiscountCoupon objectAtIndex:i];
                    
                    if ([strCouponDiscount isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountCode"]]])
                    {
                        NSLog(@"Matched Discount");
                        
                        //Discount Validity Check
                        chkValidDiscount=[self discountValidityCheck:dict];
                        if (chkValidDiscount==YES)
                        {
                            NSLog(@"VALID DISCOUNT");
                            
                            
                            appliedDiscountMaint=0.0;
                            appliedDiscountInitial=0.0;
                            
                            
                            NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Coupon"];
                            
                            float totalDiscountAmoutAlreadyPresent=0.0;
                            
                            for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
                                
                                NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
                                
                                NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedInitialDiscount"]];
                                
                                float discountAmountPresentt=[strDiscountAmountPresent floatValue];
                                
                                if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"true"])
                                {
                                    
                                    
                                }else{
                                    
                                    totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
                                    
                                }
                                
                            }
                            
                            BOOL ifToSaveOrNot;
                            
                            ifToSaveOrNot=NO;
                            
                            NSString *strDiscountAmountTemp=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]];
                            
                            float discount=[strDiscountAmountTemp floatValue];
                            
                            NSString *strIsDiscountTypeNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
                            
                            if ([strIsDiscountTypeNew isEqualToString:@"1"] || [strIsDiscountTypeNew isEqualToString:@"true"] || [strIsDiscountTypeNew isEqualToString:@"True"]) {
                                
                                NSString *strDiscountPercenteNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]];
                                
                                float discountPercentValue=[strDiscountPercenteNew floatValue];
                                
                                if (discountPercentValue==100 || discountPercentValue>100)
                                {
                                    discountPercentValue=100.00;
                                    
                                }
                                
                                discount=(globalAmountInitialPrice*discountPercentValue)/100;
                                
                            }
                            
                            
                            if (!(globalAmountInitialPrice>=(totalDiscountAmoutAlreadyPresent+discount))) {
                                
                                
                                if (globalAmountInitialPrice<(totalDiscountAmoutAlreadyPresent+discount)) {
                                    
                                    float amountDiscountToApplyCredit=globalAmountInitialPrice-totalDiscountAmoutAlreadyPresent;
                                    
                                    NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
                                    
                                    [dictTempToSave addEntriesFromDictionary:dict];
                                    
                                    [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"DiscountAmount"];
                                    
                                    dict=dictTempToSave;
                                    
                                    ifToSaveOrNot=YES;
                                    
                                    if (amountDiscountToApplyCredit>0) {
                                        
                                        ifToSaveOrNot=YES;
                                        appliedDiscountInitial=amountDiscountToApplyCredit;
                                        
                                        
                                    }else{
                                        
                                        ifToSaveOrNot=NO;
                                        appliedDiscountInitial=0.0;
                                        
                                        
                                    }
                                    
                                }
                            }else{
                                
                                ifToSaveOrNot=YES;
                                
                            }
                            
                            
                            if (ifToSaveOrNot) {
                                
                                NSString *strDiscountPerCheck=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
                                if ([strDiscountPerCheck isEqualToString:@"1"]||[strDiscountPerCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                                {
                                    [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :@"0" CheckForDiscountPer:@"1" DiscountPerValue:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]];
                                }
                                else
                                {
                                    [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]]CheckForDiscountPer:@"0" DiscountPerValue:@"0"];
                                }
                                if (chkForSaveCoupon==YES)
                                {
                                    [self saveAppliedDiscountCoreData:dict];
                                    [self fetchForAppliedDiscountFromCoreData];
                                    
                                }
                                
                            }else{
                                
                                [global AlertMethod:Alert :@"Coupon can not be more then billing amount."];
                                
                            }
                            NSLog(@"VALID DISCOUNT");
                            
                            
                        }
                        else
                        {
                            NSLog(@"INVALID DISCOUNT");
                            //[global AlertMethod:@"Alert" :@"No valid coupon"];
                            
                        }
                        //[self heightManage];
                        break;
                        
                    }
                    else
                    {
                        chkValidDiscount=NO;
                    }
                    
                }
                if(chkValidDiscount==NO)// && chkForSaveCoupon==NO)
                {
                    [global AlertMethod:@"Alert" :@"Invalid coupon"];
                    
                }
                else
                {
                    if(chkForSaveCoupon==NO)
                    {
                        [global AlertMethod:@"Alert" :@"Invalid coupon"];
                        
                    }
                }
                
                /* arrDiscountCoupon=[[NSMutableArray alloc]init];
                 for (int i=0; i<arrTempCoupon.count; i++)
                 {
                 [arrDiscountCoupon addObject:[arrTempCoupon objectAtIndex:i]];
                 }
                 [self heightForDiscountCoupon];*/
                
            }
        }
        
        
    }
}


-(void)heightForDiscountCoupon
{
    _const_TableDiscount_H.constant=108;
    _const_ViewDiscountCoupon_H.constant=165;
    UILabel *lbl;
    lbl=[[UILabel alloc]init];
    lbl.frame=CGRectMake(0, 0, _tblCouponDiscount.frame.size.width-50, 21);
    arrTempCoupon=[[NSMutableArray alloc]init];
    [arrTempCoupon addObjectsFromArray:arrDiscountCoupon];
    
    float totalHeightStan=0;
    for (int i=0; i<arrTempCoupon.count; i++)
    {
        
        NSManagedObject *dict=[arrTempCoupon objectAtIndex:i];
        
        lbl.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
        
        CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lbl.lineBreakMode];
        
        totalHeightStan=totalHeightStan+expectedLabelSize.height;
        
    }
    [_tblCouponDiscount reloadData];
    _const_TableDiscount_H.constant=totalHeightStan+[arrDiscountCoupon count]*80;
    _const_ViewDiscountCoupon_H.constant=_const_TableDiscount_H.constant+50;
}
-(void)finalUpdatedTableDataWithBundleDiscountNew:(NSString *)strServiceNameDiscount :(NSString *)strDiscountAmount CheckForDiscountPer:(NSString*)strDisountCheck DiscountPerValue:(NSString *)strDiscountPercent
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND bundleId=%@",strLeadId,@"0"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjDiscount = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObjDiscount.count==0)
    {
        
    }
    else
    {
        bool chkDiscountApplied,chkForServiceBasedCoupon,chkForBundle=NO;
        chkDiscountApplied=NO;chkForServiceBasedCoupon=NO;chkForSaveCoupon=NO;
        for (int i=0; i<arrAllObjDiscount.count; i++)
        {
            matchesDiscoutUpdate=arrAllObjDiscount[i];
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                chkForBundle=NO;
                
            }
            else
            {
                chkForBundle=YES;
            }
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                //strServiceNameDiscount=@"SignaturePestControl";
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    chkForServiceBasedCoupon=YES;
                    if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        if (discountPerCoupon==100|| discountPerCoupon>100)
                        {
                            discountPerCoupon=100.00;
                            
                        }
                        
                        //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        //Calulcate Applied Discount Amount For Initial And Maint
                        
                        
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                        appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                        appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                        
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountInitial=lroundf(appliedDiscountInitial);
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=lroundf(appliedDiscountMaint);*/
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountAmount floatValue])/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountAmount floatValue])/100;*/
                        // appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        //  appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        //14 June Temp
                        float appDiscIntial,appDiscMaint;
                        appDiscIntial=0;appDiscMaint=0;
                        appDiscIntial=discount;
                        appDiscMaint=discount;
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            appDiscIntial=discount;
                            
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                            appDiscMaint=discount;
                            
                        }
                        
                        appliedDiscountInitial=appDiscIntial;
                        appliedDiscountMaint=appDiscMaint;
                        
                        //Nilind 29 June
                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        }
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                        {
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                        }
                        //End
                        
                        
                    }
                    
                    chkForSaveCoupon=YES;
                }
                
            }
            else  //For Bundle
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    chkForServiceBasedCoupon=YES;
                    if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        if (discountPerCoupon==100|| discountPerCoupon>100)
                        {
                            discountPerCoupon=100.00;
                            
                        }
                        
                        //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        //Calulcate Applied Discount Amount For Initial And Maint
                        
                        
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                        
                        appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                        appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        /* appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountAmount floatValue])/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountAmount floatValue])/100;*/
                        // appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        // appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        //14 June Temp
                        float appDiscIntial,appDiscMaint;
                        appDiscIntial=0;appDiscMaint=0;
                        appDiscIntial=discount;
                        appDiscMaint=discount;
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            appDiscIntial=discount;
                            
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                            appDiscMaint=discount;
                            
                        }
                        
                        appliedDiscountInitial=appDiscIntial;
                        appliedDiscountMaint=appDiscMaint;
                        
                        //Nilind 29 June
                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        }
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                        {
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                        }
                        //End
                        
                        
                    }
                    
                    chkForSaveCoupon=YES;
                    chkForBundle=YES;
                }
            }
        }
        if(chkDiscountApplied==NO)//&& chkForServiceBasedCoupon==NO
        {
            if(strServiceNameDiscount.length==0)//Non ServiceBased
            {
                if (arrAllObjDiscount.count>0)
                {
                    matchesDiscoutUpdate=arrAllObjDiscount[0];
                    
                    if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                    {
                        chkForBundle=NO;
                    }
                    else
                    {
                        chkForBundle=YES;
                    }
                    
                    
                    if (chkForBundle==NO)
                    {
                        matchesDiscoutUpdate=arrAllObjDiscount[0];
                        
                        if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                        {
                            float discountPerCoupon=0, discount=0;
                            
                            discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                            if (discountPerCoupon==100|| discountPerCoupon>100)
                            {
                                discountPerCoupon=100.00;
                                
                            }
                            
                            //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                            
                            discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                            
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                //13 June Temp
                                // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                            
                            chkDiscountApplied=YES;
                            strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                            
                            /* appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                             appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                            appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                            appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                            
                            
                        }
                        else
                        {
                            float discountPerCoupon=0, discount=0;
                            //strDiscountAmount=@"50";
                            discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                //13 June Temp
                                //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            
                            
                            discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                            
                            discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                            
                            strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                            
                            /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                             appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                            /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountAmount floatValue])/100;
                             appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountAmount floatValue])/100;*/
                            
                            
                            //  appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            // appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            
                            //14 June Temp
                            float appDiscIntial,appDiscMaint;
                            appDiscIntial=0;appDiscMaint=0;
                            appDiscIntial=discount;
                            appDiscMaint=discount;
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                appDiscIntial=discount;
                                
                                
                                //13 June Temp
                                //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                                appDiscMaint=discount;
                                
                            }
                            
                            /*appliedDiscountInitial=appDiscIntial;
                             appliedDiscountMaint=appDiscMaint;*/
                            
                            
                            //Nilind 29 June
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            
                            if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                            {
                                appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            }
                            if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                            {
                                appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                            }
                            //End
                            
                        }
                    }
                    else
                    {
                        for (int i=0; i<arrAllObjDiscount.count; i++)
                        {
                            matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                            
                            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                            {
                            }
                            else
                            {
                                matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                                if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                                {
                                    float discountPerCoupon=0, discount=0;
                                    
                                    discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                    if (discountPerCoupon==100|| discountPerCoupon>100)
                                    {
                                        discountPerCoupon=100.00;
                                        
                                    }
                                    
                                    //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                                    
                                    discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                                    
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                    }
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                                    
                                    chkDiscountApplied=YES;
                                    strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                                    
                                    /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                                     appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                                    appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                                    appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                                    
                                    
                                }
                                else
                                {
                                    float discountPerCoupon=0, discount=0;
                                    //strDiscountAmount=@"50";
                                    discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                    }
                                    
                                    
                                    discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                    
                                    discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                    
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                                    strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                                    
                                    /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                                     appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                                    /* appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountAmount floatValue])/100;
                                     appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountAmount floatValue])/100;*/
                                    // appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    //appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    //14 June Temp
                                    float appDiscIntial,appDiscMaint;
                                    appDiscIntial=0;appDiscMaint=0;
                                    appDiscIntial=discount;
                                    appDiscMaint=discount;
                                    
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                        appDiscIntial=discount;
                                        
                                        //13 June Temp
                                        //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                                    }
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                                        appDiscMaint=discount;
                                        
                                    }
                                    
                                    /*appliedDiscountInitial=appDiscIntial;
                                     appliedDiscountMaint=appDiscMaint;*/
                                    
                                    
                                    
                                    //Nilind 29 June
                                    appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    
                                    if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                    {
                                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                    }
                                    if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                                    {
                                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                                    }
                                    //End
                                    
                                    
                                    
                                }
                                
                                break;
                            }
                            
                        }
                    }
                    
                    chkForSaveCoupon=YES;
                    
                }
            }
        }
        [context save:&error1];
    }
    [self fetchFromCoreDataStandard];
    [_tblRecord reloadData];
    _txtApplyDiscount.text=@"";
}
-(BOOL)checkForAppliedDiscountFromCoreData:(NSString *)strDiscoutCode : (NSString *)strDiscountUsageType
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    
    NSPredicate *predicate;
    
    if ([strDiscountUsageType caseInsensitiveCompare:@"OneTime"]==NSOrderedSame)
    {
        predicate =[NSPredicate predicateWithFormat:@"accountNo=%@ && discountCode=%@",strAccountNoGlobal,strDiscoutCode];
    }
    else if([strDiscountUsageType caseInsensitiveCompare:@"Multiple"]==NSOrderedSame)
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && discountCode=%@",strLeadId,strDiscoutCode];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && discountCode=%@",strLeadId,strDiscoutCode];
    }
    
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    // NSManagedObject *matchesDiscount;
    
    //One Time
    if ([strDiscountUsageType caseInsensitiveCompare:@"OneTime"]==NSOrderedSame)
    {
        if (arrAllObjSales.count==0)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    //Multiple
    if ([strDiscountUsageType caseInsensitiveCompare:@"Multiple"]==NSOrderedSame)
    {
        if (arrAllObjSales.count==0)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
}
-(BOOL)checkForAppliedDiscountFromCoreDataTemp:(NSString *)strDiscoutCode
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    if (arrAllObjSales.count==0)
    {
        return NO;
    }
    else
    {
        BOOL chkDiscount=NO;
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if ([strDiscoutCode isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"discountCode"]]])
            {
                chkDiscount=YES;
                break;
            }
            
            NSLog(@"Lead IDDDD====%@",[matchesDiscount valueForKey:@"leadId"]);
        }
        if (chkDiscount==YES)
        {
            /* if ([[matchesDiscount valueForKey:@"accountNo"] isEqualToString:strAccountNoGlobal])
             {
             if([strLeadId isEqualToString:[matchesDiscount valueForKey:@"leadId"]])
             {
             return YES;
             }
             else
             {
             if([[dictDiscountUsageFromSysName valueForKey:[matchesDiscount valueForKey:@"discountSysName"] ]caseInsensitiveCompare:@"OneTime"] == NSOrderedSame )
             {
             return YES;
             }
             else
             {
             return NO;
             }
             }
             
             }
             else
             {
             return NO;
             
             }*/
            return YES;
        }
        else
        {
            return NO;
        }
    }
}

-(void)fetchForAppliedDiscountFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrDiscountCoupon=[[NSMutableArray alloc]init];
    
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        //NSArray *uniqueArray = [[NSSet setWithArray:arrSoldServiceStandardIdForCoupon] allObjects];
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
            {
                [arrDiscountCoupon addObject:matchesDiscount];
            }
            
        }
    }
    [self heightForDiscountCoupon];
}
-(void)fetchForAppliedDiscountServiceFromCoreData:(NSString *)strServiceIdAppliedDiscount
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrDiscountCoupon=[[NSMutableArray alloc]init];
    
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"] && [[matchesDiscount valueForKey:@"soldServiceId"] isEqualToString:strServiceIdAppliedDiscount])
            {
                [arrDiscountCoupon addObject:matchesDiscount];
                break;
            }
            
        }
    }
    if(arrDiscountCoupon.count==0)
    {
        [self fetchForAppliedDiscountFromCoreData];
    }
    else
    {
        [self deleteAppliedCouponFromCoreDataSalesInfo:matchesDiscount:@""];
        [self heightForDiscountCoupon];
    }
}

-(void)saveAppliedDiscountCoreData:(NSDictionary *)dictForCoupon
{
    
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    
    LeadAppliedDiscounts *objLeadAppliedDiscounts = [[LeadAppliedDiscounts alloc]initWithEntity:entityLeadAppliedDiscounts insertIntoManagedObjectContext:context];
    objLeadAppliedDiscounts.leadAppliedDiscountId=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountSetupId"]];
    objLeadAppliedDiscounts.serviceSysName=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"ServiceSysName"]];
    objLeadAppliedDiscounts.discountSysName=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"SysName"]];
    objLeadAppliedDiscounts.discountType=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Type"]];
    objLeadAppliedDiscounts.discountCode=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountCode"]];
    objLeadAppliedDiscounts.discountAmount=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountAmount"]];
    objLeadAppliedDiscounts.discountDescription=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Description"]];
    
    if ([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"IsActive"]] isEqualToString:@"1"])
    {
        objLeadAppliedDiscounts.isActive=@"true";
        
    }
    else
    {
        objLeadAppliedDiscounts.isActive=@"false";
        
    }
    if ([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"])
    {
        objLeadAppliedDiscounts.isDiscountPercent=@"true";
        objLeadAppliedDiscounts.discountPercent=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountPercent"]];
        objLeadAppliedDiscounts.discountAmount=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountPercent"]];
    }
    else
    {
        objLeadAppliedDiscounts.isDiscountPercent=@"false";
        objLeadAppliedDiscounts.discountPercent=@"0";
        
        
    }
    objLeadAppliedDiscounts.soldServiceId=strAppliedDiscountServiceId;
    objLeadAppliedDiscounts.serviceType=@"Standard";
    objLeadAppliedDiscounts.name=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Name"]];
    
    objLeadAppliedDiscounts.leadId=strLeadId;
    objLeadAppliedDiscounts.userName=strUserName;
    objLeadAppliedDiscounts.companyKey=strCompanyKey;
    
    objLeadAppliedDiscounts.appliedMaintDiscount=[NSString stringWithFormat:@"%.5f",appliedDiscountMaint];
    objLeadAppliedDiscounts.appliedInitialDiscount=[NSString stringWithFormat:@"%.5f",appliedDiscountInitial];
    
    objLeadAppliedDiscounts.accountNo=strAccountNoGlobal;
    NSError *error1;
    [context save:&error1];
}
-(void)deleteAppliedCouponFromCoreDataSalesInfo:(NSManagedObject *)dictForCoupon :(NSString *)strDirect
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    NSString *strService=[dictForCoupon valueForKey:@"serviceSysName"];
    NSString *strServiceId=[dictForCoupon valueForKey:@"soldServiceId"];
    
    NSString *strDiscountCode=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountCode"]];
    NSString *strDiscountAmount=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountAmount"]];
    NSString *strDiscountPer=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountPercent"]];
    NSString *strDiscountSysName=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountSysName"]];
    NSString *strChkDiscountPer=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"isDiscountPercent"]];
    //13 June Temp
    strDiscountAmount=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"appliedInitialDiscount"]];
    
    NSString *strServiceIdFinal;
    if([strDirect isEqualToString:@"strDirect"])
    {
        for (NSManagedObject * data in Data)
        {
            NSString *strSysNameServiceDataBase,*strCouponCodeDataBase;
            strSysNameServiceDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"serviceSysName"]];
            strCouponCodeDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"discountCode"]];
            /*if([strSysNameServiceDataBase isEqualToString:strService]&&[strCouponCodeDataBase isEqualToString:strDiscountCode])
             {
             [context deleteObject:data];
             break;
             }*/
            if([[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceId"]] isEqualToString:strServiceId] && [strDiscountSysName isEqualToString:[NSString stringWithFormat:@"%@",[data valueForKey:@"discountSysName"]]])
            {
                strServiceIdFinal=[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceId"]];
                [context deleteObject:data];
                break;
                
            }
        }
        
    }
    else
    {
        for (NSManagedObject * data in Data)
        {
            NSString *strSysNameServiceDataBase,*strCouponCodeDataBase;
            strSysNameServiceDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"serviceSysName"]];
            strCouponCodeDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"discountCode"]];
            
            if([[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceId"]] isEqualToString:strServiceId])
            {
                strServiceIdFinal=[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceId"]];
                
                [context deleteObject:data];
                
            }
        }
        
        
    }
    NSError *saveError = nil;
    [context save:&saveError];
    [self fetchForAppliedDiscountFromCoreData];
    [_tblCouponDiscount reloadData];
    
    if (chkDirectServiceDelete==YES)
    {
        chkDirectServiceDelete=NO;
    }
    else
    {
        
        [self updateDiscountAfterDelete:strService DiscountAmount:strDiscountAmount DiscountPercent:strDiscountPer CheckForDiscountPer:strChkDiscountPer:strServiceIdFinal];
    }
    //[self updateDiscountAfterDelete:strService DiscountAmount:strDiscountAmount DiscountPercent:strDiscountPer];
}
-(void)updateDiscountAfterDelete:(NSString *)strServiceNameDiscount DiscountAmount: (NSString *)strDiscountAmount DiscountPercent:(NSString *)StrDiscPer CheckForDiscountPer:(NSString*)strChkForDiscountPer : (NSString *)strServiceIdFinal
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND bundleId=%@",strLeadId,@"0"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND soldServiceStandardId =%@",strLeadId,strServiceIdFinal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjDiscount = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObjDiscount.count==0)
    {
        
    }else
    {
        bool chkDiscountApplied;
        chkDiscountApplied=NO;
        BOOL chkForBundle=NO;
        for (int i=0; i<arrAllObjDiscount.count; i++)
        {
            matchesDiscoutUpdate=arrAllObjDiscount[i];
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                chkForBundle=NO;
            }
            else
            {
                chkForBundle=YES;
            }
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        // discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    
                }
                
            }
            else  //For Bundle
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        // discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    
                }
                
                chkForBundle=YES;
                break;
                
            }
        }
        if(chkDiscountApplied==NO)
        {
            if (arrAllObjDiscount.count>0)
            {
                matchesDiscoutUpdate=arrAllObjDiscount[0];
                
                if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                {
                    chkForBundle=NO;
                }
                else
                {
                    chkForBundle=YES;
                }
                
                if (chkForBundle==NO)
                {
                    matchesDiscoutUpdate=arrAllObjDiscount[0];
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        // strDiscountAmount=@"50";
                        //discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        //  discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        /* if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                         {
                         discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                         }
                         
                         discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                         */
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        // strDiscountAmount=@"50";
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        //  discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        /* if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                         {
                         discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                         }
                         
                         discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                         */
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                    }
                    
                }
                // BUNDLE
                else
                {
                    for (int i=0; i<arrAllObjDiscount.count; i++)
                    {
                        matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                        
                        if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                        {
                        }
                        else
                        {
                            matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                            if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                            {
                                float discountPerCoupon=0, discount=0;
                                
                                discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                                
                                discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                                // strDiscountAmount=@"50";
                                //discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                                //  discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                                
                                /* if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                 {
                                 discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                 }
                                 
                                 discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                 */
                                discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                            }
                            else
                            {
                                float discountPerCoupon=0, discount=0;
                                // strDiscountAmount=@"50";
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                                if(discount<0)
                                {
                                    discount=0;
                                }
                                //  discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                                
                                /* if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                 {
                                 discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                 }
                                 
                                 discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                 */
                                discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                if(discountPerCoupon<0)
                                {
                                    discountPerCoupon=0;
                                }
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                                
                            }
                        }
                    }
                }
                
                
                
                
            }
        }
        
        [context save:&error1];
    }
    [self fetchFromCoreDataStandard];
    [_tblRecord reloadData];
    _txtApplyDiscount.text=@"";
}
-(void)fetchFromCoreDataStandardForBundleForDiscountCupon:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrSoldServiceStandardIdBundle=[[NSMutableArray alloc]init];
    
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                
                [arrSoldServiceStandardIdBundle addObject:[matches valueForKey:@"soldServiceStandardId"]];
                
            }
        }
    }
    // [self heightManage];
}
-(BOOL)discountValidityCheck:(NSDictionary*)dict
{
    //Discount Validity Check
    BOOL chkValidity;
    chkValidity=NO;
    NSString *strCurrentDate,*strValidFrom,*strValidTo;
    NSDate *dateCurrent,*dateValidFrom,*dateValidTo;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    
    strCurrentDate = [formatter stringFromDate:[NSDate date]];
    
    strValidFrom=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ValidFrom"]];
    dateCurrent=[formatter dateFromString:strCurrentDate];
    
    
    
    strValidFrom=[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:strValidFrom];
    dateValidFrom=[formatter dateFromString:strValidFrom];
    
    strValidTo=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ValidTo"]];
    
    strValidTo=[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:strValidTo];
    dateValidTo=[formatter dateFromString:strValidTo];
    
    
    
    NSComparisonResult result = [dateValidFrom compare:dateCurrent];
    
    if(result==NSOrderedAscending || result== NSOrderedSame )
    {
        NSComparisonResult result2 = [dateValidTo compare:dateCurrent];
        if (result2==NSOrderedDescending|| result2==NSOrderedSame)
        {
            NSLog(@"valid B");
            chkValidity=YES;
        }
    }
    //End
    return chkValidity;
    
}
-(void)updateLeadAppliedDiscountForAppliedCoupon
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            for (int j=0; j<arrSoldServiceIdOfAllService.count; j++)
            {
                if ([[matchesDiscount valueForKey:@"soldServiceId"] isEqualToString:[arrSoldServiceIdOfAllService objectAtIndex:j]])
                {
                    [matchesDiscount setValue:@"true" forKey:@"isApplied"];
                }
                
            }
        }
    }
    [context save:&error1];
}
-(void)updateLeadAppliedDiscountForCredit
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"]isEqualToString:@"Credit"]||[[matchesDiscount valueForKey:@"discountType"]isEqualToString:@"credit"])
            {
                
                if(arrSoldServiceIdOfAllService.count>0)
                {
                    [matchesDiscount setValue:[arrSoldServiceIdOfAllService objectAtIndex:0] forKey:@"soldServiceId"];
                }
                
                /*for (int j=0; j<arrAllUnsoldServiceId.count; j++)
                 {
                 if(arrSoldServiceIdOfAllService.count>0)
                 {
                 if ([[matchesDiscount valueForKey:@"soldServiceId"] isEqualToString:[arrAllUnsoldServiceId objectAtIndex:j]])
                 {
                 [matchesDiscount setValue:[arrSoldServiceIdOfAllService objectAtIndex:0] forKey:@"soldServiceId"];
                 }
                 }
                 
                 }*/
            }
        }
    }
    [context save:&error1];
}

-(void)deleteLeadAppliedDiscountFromCoreData
{
    //Delete LeadAppliedDiscounts Data
    
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    
    NSFetchRequest *allDataLeadAppliedDiscounts = [[NSFetchRequest alloc] init];
    [allDataLeadAppliedDiscounts setEntity:[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context]];
    
    NSPredicate *predicateLeadAppliedDiscounts =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    
    [allDataLeadAppliedDiscounts setPredicate:predicateLeadAppliedDiscounts];
    [allDataLeadAppliedDiscounts setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorLeadAppliedDiscounts= nil;
    NSArray * DataLeadAppliedDiscounts = [context executeFetchRequest:allDataLeadAppliedDiscounts error:&errorLeadAppliedDiscounts];
    //error handling goes here
    for (NSManagedObject * data in DataLeadAppliedDiscounts) {
        [context deleteObject:data];
    }
    NSError *saveErrorLeadAppliedDiscounts = nil;
    [context save:&saveErrorLeadAppliedDiscounts];
}
//......................................................

// Saavan Changes 9 july 2018

-(NSArray*)fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot :(NSString*)strType
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    NSMutableArray *arrDiscountCouponNew=[[NSMutableArray alloc]init];
    NSMutableArray *arrDiscountCreditNew=[[NSMutableArray alloc]init];
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            for (int j=0; j<arrSoldServiceStandardId.count; j++)
            {
                NSString *strId=[arrSoldServiceStandardId objectAtIndex:j];
                if ([strId isEqualToString:[matchesDiscount valueForKey:@"soldServiceId"]])
                {
                    if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
                    {
                        [arrDiscountCouponNew addObject:matchesDiscount];
                    }
                    else
                    {
                        [arrDiscountCreditNew addObject:matchesDiscount];
                        
                    }
                    
                    
                }
                // [arrDiscountCoupon addObject:matchesDiscount];
            }
        }
    }
    
    NSMutableArray *arrTempToReturn=[[NSMutableArray alloc]init];
    
    //if ([strType isEqualToString:@"Credit"]) {
    
    if (arrDiscountCreditNew.count>0) {
        
        [arrTempToReturn addObjectsFromArray:arrDiscountCreditNew];
        
    }
    
    
    //} else {
    
    if (arrDiscountCouponNew.count>0) {
        
        [arrTempToReturn addObjectsFromArray:arrDiscountCouponNew];
        
    }
    
    // }
    
    NSArray *arrTemopNew=[[NSArray alloc]init];
    
    arrTemopNew=arrTempToReturn;
    
    return arrTemopNew;
    
}

- (IBAction)actionOnChemicalSensitivityList:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                 bundle: nil];
        ChemicalSensitivityList
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChemicalSensitivityList"];
        objByProductVC.strFrom=@"sales";
        objByProductVC.strId=strLeadId;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
    //End
    
}
-(void)fetchForAppliedDiscountFromCoreDataFromBack
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrDiscountCoupon=[[NSMutableArray alloc]init];
    
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        
        
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
            {
                for (int l=0; l<arrSoldServiceStandardId.count; l++)
                {
                    if ([[arrSoldServiceStandardId objectAtIndex:l]isEqualToString:[matchesDiscount valueForKey:@"soldServiceId"]])
                    {
                        [arrDiscountCoupon addObject:matchesDiscount];
                    }
                }
                //[arrDiscountCoupon addObject:matchesDiscount];
            }
            
        }
        
    }
    [self heightForDiscountCoupon];
    
    
}
-(void)newAlertMethodAddImage
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              NSLog(@"The CApture Image.");
                              
                              
                              if (arrNoImage.count<10)
                              {
                                  NSLog(@"The CApture Image.");
                                  
                                  BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                  
                                  if (isCameraPermissionAvailable) {
                                      
                                      if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                                      {
                                          
                                          [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                                          
                                      }else{
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }
                                  }else{
                                      
                                      UIAlertController *alert= [UIAlertController
                                                                 alertControllerWithTitle:@"Alert"
                                                                 message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                      
                                      UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                            {
                                                                
                                                                
                                                                
                                                            }];
                                      [alert addAction:yes];
                                      UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                   [[UIApplication sharedApplication] openURL:url];
                                                               } else {
                                                                   
                                                               }
                                                           }];
                                      [alert addAction:no];
                                      [self presentViewController:alert animated:YES completion:nil];
                                  }
                                  
                              }
                              else
                              {
                                  chkForDuplicateImageSave=YES;
                                  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                  [alert show];
                              }
                              //............
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             NSLog(@"The Gallery.");
                             if (arrNoImage.count<10)
                             {
                                 BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                 
                                 if (isCameraPermissionAvailable) {
                                     
                                     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                                     {
                                         
                                         [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                                         
                                     }else{
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }
                                 }else{
                                     
                                     UIAlertController *alert= [UIAlertController
                                                                alertControllerWithTitle:@"Alert"
                                                                message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                preferredStyle:UIAlertControllerStyleAlert];
                                     
                                     UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               
                                                               
                                                           }];
                                     [alert addAction:yes];
                                     UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                handler:^(UIAlertAction * action)
                                                          {
                                                              
                                                              if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                  NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                  [[UIApplication sharedApplication] openURL:url];
                                                              }
                                                              else
                                                              {
                                                                  
                                                                  
                                                                  
                                                              }
                                                              
                                                          }];
                                     [alert addAction:no];
                                     [self presentViewController:alert animated:YES completion:nil];
                                 }
                             }
                             else
                             {
                                 chkForDuplicateImageSave=YES;
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                 [alert show];
                             }
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)setHeight
{
    _const_TblService_H.constant=125;
    _const_TblService_H.constant=_const_TblService_H.constant*arrSavedServiceDetail.count;
    _const_TblScope_H.constant=125;
    
    _const_TblScope_H.constant=_const_TblScope_H.constant*arrSavedScopeDetail.count;
    _const_TblTarget_H.constant=100;
    _const_TblTarget_H.constant=_const_TblTarget_H.constant*arrSavedTargetDetail.count;
    
    
    
}
-(void)buttonClickedCheckBoxAgreementService:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    ClarkPestSelectServiceTableViewCell *tappedCell = (ClarkPestSelectServiceTableViewCell *)[_tblService cellForRowAtIndexPath:indexpath];
    
    
    if ([tappedCell.btnAddToAgreement.currentImage isEqual:[UIImage imageNamed:@"check_box_2New.png"]])
    {
        // [tappedCell.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
        chkBtnCheckBoxStan=NO;
        
        
    }
    else
    {
        //  [tappedCell.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        chkBtnCheckBoxStan=YES;
        
    }
    NSManagedObject *record = [arrSavedServiceDetail objectAtIndex:sender.tag];//[self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath1];
    [self updateServiceClarkPest:record];
    if(isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in buttonClickedStandardService");
        [global updateSalesModifydate:strLeadId];
    }
    [_tblService reloadData];
}
-(void)buttonClickedCheckBoxAgreementNonStanadrdService:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    ClarkPestTableViewCellNonStandard *tappedCell = (ClarkPestTableViewCellNonStandard *)[_tblNonStandardClarkPest cellForRowAtIndexPath:indexpath];
    
    
    if ([tappedCell.btnAddToAgreement.currentImage isEqual:[UIImage imageNamed:@"check_box_2New.png"]])
    {
        chkBtnCheckBoxNonStan=NO;
        
    }
    else
    {
        chkBtnCheckBoxNonStan=YES;
    }
    NSManagedObject *record = [arrSavedNonStanServiceDetail objectAtIndex:sender.tag];
    
    [self updateServiceNonStandardClarkPest:record];
    
    if(isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in buttonClickedStandardService");
        [global updateSalesModifydate:strLeadId];
    }
    [_tblNonStandardClarkPest reloadData];
}
- (IBAction)actionOnProposedServiceFreq:(id)sender
{
    [self endEditing];
    tblData.tag=74;
    [self tableLoad:tblData.tag];
}
- (IBAction)actionOnProposedServiceMonth:(id)sender
{
    [self endEditing];
    tblData.tag=75;
    [self tableLoad:tblData.tag];
}

- (IBAction)actionOnBtnSelection:(id)sender
{
    [self endEditing];
    if(selectionType == 1)
    {
        if(arrayTarget.count==0)
        {
            [global AlertMethod:@"Alert!" :@"No Target available"];
        }
        else
        {
            tblData.tag=73;
            [self tableLoad:tblData.tag];
        }
    }
    else if(selectionType == 2)
    {
        [self getScope];
        
        if (arrayScope.count==0)
        {
            [global AlertMethod:@"Alert!" :@"No Scope available"];
        }
        else
        {
            tblData.tag=72;
            [self tableLoad:tblData.tag];
        }
    }
    else if(selectionType == 3)// service
    {
        /*tblData.tag=73;
         [self tableLoad:tblData.tag];*/
        
        if ([_btnSelectionCategory.titleLabel.text isEqualToString:@"Select Category"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select category" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            [self getServiceForCategory];
            if (arrayService.count==0)
            {
                [global AlertMethod:@"Alert!" :@"No Service available"];
            }
            else
            {
                
                tblData.tag=71;
                [self tableLoad:tblData.tag];
            }
        }
    }
}

- (IBAction)actionOnSavePopUp:(id)sender
{
    [self endEditing];
    if (selectionType==1) //Target
    {
        NSLog(@"Save Target");
        strTargetDescription=_textViewDescription_PopUp.text;
        
        BOOL chkExist;
        chkExist=[self checkTargetAvailabilityInDataBase:strTargetSysName];
        if (chkExist==YES)
        {
            [global AlertMethod:@"Alert!" :@"Target already taken"];
        }
        else
        {
            if (strTargetSysName.length==0 || [strTargetSysName isEqualToString:@""]
                || [strTargetSysName isEqual:nil] || !([strTargetSysName isKindOfClass:[NSString class]]))
            {
                [global AlertMethod:@"Alert!" :@"Please select Target"];
            }
            else
            {
                [self saveTargetCoreDataClarkPest];
                strIsChangedServiceDesc = @"false";
                strIsChangedScopeDesc = @"false";
                strIsChangedTargetDesc = @"false";
                strTargetSysName=@"";
                [_viewPopUp removeFromSuperview];
                [viewBackground removeFromSuperview];
                [_btnSelectionCategory setTitle:@"Select Category" forState:UIControlStateNormal];
            }
        }
    }
    else if (selectionType==2)
    {
        NSLog(@"Save Scope");
        strScopeDescription=_textViewDescription_PopUp.text;
        BOOL chkExist;
        chkExist=[self checkScopeAvailabilityInDataBase:strScopeSysName];
        if (chkExist==YES)
        {
            [global AlertMethod:@"Alert!" :@"Scope already taken"];
        }
        else
        {
            if (strScopeSysName.length==0 || [strScopeSysName isEqualToString:@""]
                || [strScopeSysName isEqual:nil] || !([strScopeSysName isKindOfClass:[NSString class]]))
            {
                [global AlertMethod:@"Alert!" :@"Please select Scope"];
            }
            else
            {
                [self saveScopeCoreDataClarkPest];
                strIsChangedServiceDesc = @"false";
                strIsChangedScopeDesc = @"false";
                strIsChangedTargetDesc = @"false";
                strScopeSysName=@"";
                [_viewPopUp removeFromSuperview];
                [viewBackground removeFromSuperview];
                [_btnSelectionCategory setTitle:@"Select Category" forState:UIControlStateNormal];
            }
        }
    }
    else if (selectionType==3) //For Service
    {
        
        NSLog(@"Save Category Serivce");
        strServiceDescription=_textViewDescription_PopUp.text;
        
        BOOL chkExist;
        chkExist=[self checkServiceAvialabiltyInDataBaseClarkPest:strServiceSysName];
        if (chkExist==YES)
        {
            [global AlertMethod:@"Alert!" :@"Service already taken"];
        }
        else
        {
            if (strServiceSysName.length==0 || [strServiceSysName isEqualToString:@""]
                || [strServiceSysName isEqual:nil] || !([strServiceSysName isKindOfClass:[NSString class]]))
            {
                [global AlertMethod:@"Alert!" :@"Please select Category and Service both"];
            }
            else
            {
                [self SaveSoldServiceStandardDetailClarkPest];
                strIsChangedServiceDesc = @"false";
                strIsChangedScopeDesc = @"false";
                strIsChangedTargetDesc = @"false";
                strServiceSysName=@"";
                strCategorySysName=@"";
                [_viewPopUp removeFromSuperview];
                [viewBackground removeFromSuperview];
                [_btnSelectionCategory setTitle:@"Select Category" forState:UIControlStateNormal];
            }
        }
        //[self goToAddStanNonStanService];
        
    }
    
    
    
    
}

- (IBAction)actionOnCancelPopUp:(id)sender
{
    [self endEditing];
    strCategorySysName=@"";
    strServiceSysName=@"";
    strTargetSysName=@"";
    strScopeSysName=@"";
    
    [_btnSelectionCategory setTitle:@"Select Category" forState:UIControlStateNormal];
    [_viewPopUp removeFromSuperview];
    [viewBackground removeFromSuperview];
    
}
-(void)addPopUpView
{
    viewBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    viewBackground.backgroundColor = [UIColor blackColor];
    viewBackground.alpha = 0.5;
    _viewPopUp.frame = CGRectMake(20, (self.view.frame.size.height*20)/100, self.view.frame.size.width-40,self.view.frame.size.height-(220+(self.view.frame.size.height*20)/100));// 220 for keyboard height
    [self.view addSubview:viewBackground];
    //    _viewPopUp_Demo.center = self.view.center;
    _viewPopUp.layer.borderColor = [[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor];
    _viewPopUp.layer.cornerRadius = 5.0;
    _viewPopUp.layer.borderWidth = 1.0;
    [self.view addSubview:_viewPopUp];
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundViewPopUp)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    if(selectionType==3)
    {
        
        /*[_buttonSelectOptions_PopUp setTitle:@"Select Target" forState:UIControlStateNormal];
         [_btnSelectionCategory setTitle:@"Select Category" forState:UIControlStateNormal];
         _imgViewDropDownPopUp.hidden = YES;*/
        
        [_buttonSelectOptions_PopUp setTitle:@"Select Service" forState:UIControlStateNormal];
        _imgViewDropDownPopUp.hidden = NO;
        
    }
    else if (selectionType==2)
    {
        [_buttonSelectOptions_PopUp setTitle:@"Select Scope" forState:UIControlStateNormal];
        [_btnSelectionCategory setTitle:@"Select Category" forState:UIControlStateNormal];
        _imgViewDropDownPopUp.hidden = YES;
    }
    else if (selectionType==1)
    {
        /*[_buttonSelectOptions_PopUp setTitle:@"Select Service" forState:UIControlStateNormal];
         _imgViewDropDownPopUp.hidden = NO;*/
        
        [_buttonSelectOptions_PopUp setTitle:@"Select Target" forState:UIControlStateNormal];
        [_btnSelectionCategory setTitle:@"Select Category" forState:UIControlStateNormal];
        _imgViewDropDownPopUp.hidden = YES;
        
    }
    else
    {
        [_btnSelectionCategory setTitle:@"Select Category" forState:UIControlStateNormal];
    }
    _textViewDescription_PopUp.text = @"";
}
- (IBAction)actionOnSelectionCategory:(id)sender
{
    [self endEditing];
    if (arrCategoryPopUp.count==0)
    {
        [global AlertMethod:@"Alert!" :@"No Category Available"];
    }
    else
    {
        tblData.tag=70;
        [self tableLoad:tblData.tag];
    }
}

- (IBAction)actionOnPrposedServiceFreq:(id)sender {
}

#pragma mark- Fetch Category
-(NSArray *)getCategoryDeptWise
{
    
    //Nilind 05 Sept
    NSDictionary  *dictSalesLeadMaster;
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strBranchSysName;
    strBranchSysName=[defs valueForKey:@"branchSysName"];
    NSMutableArray *arrDepartment;
    arrDepartment=[[NSMutableArray alloc]init];
    
    for (int i=0;i<arrDeptName.count; i++)
    {
        NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
        NSArray *arry=[dictBranch valueForKey:@"Departments"];
        for (int j=0; j<arry.count; j++)
        {
            NSDictionary *dict=[arry objectAtIndex:j];
            if ([strBranchSysName isEqualToString:[dictBranch valueForKey:@"SysName"]])
            {
                NSString *strIsActive=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]];
                if ([strIsActive isEqualToString:@"false"]||[strIsActive isEqualToString:@"0"])
                {
                    
                }
                else
                {
                    [arrDepartment addObject:dict];
                }
            }
            
        }
        
    }
    
    
    NSMutableArray *arrCategoryBranchWise;
    arrCategoryBranchWise=[[NSMutableArray alloc]init];
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCtgryNew=[dictMasters valueForKey:@"Categories"];
    
    for (int i=0; i<arrDepartment.count; i++)
    {
        NSDictionary *dict=[arrDepartment objectAtIndex:i];
        NSString *strDeptSysName;
        strDeptSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        for (int j=0; j<arrCtgryNew.count; j++)
        {
            NSDictionary *dictNew=[arrCtgryNew objectAtIndex:j];
            
            if ([[dictNew valueForKey:@"DepartmentSysName"] isEqualToString:strDeptSysName])
            {
                if ([[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"IsActive"] ]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"IsActive"] ]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"IsActive"] ]isEqualToString:@"True"])
                {
                    
                    [arrCategoryBranchWise addObject:dictNew];
                }
                
            }
        }
        
    }
    return arrCategoryBranchWise;
}
-(void)getServiceForCategory
{
    for (int i=0; i<arrCategoryPopUp.count; i++)
    {
        NSDictionary *dict=[arrCategoryPopUp objectAtIndex:i];
        if ([strCategorySysName isEqualToString:[dict valueForKey:@"SysName"]])
        {
            arrayService=[dict valueForKey:@"Services"];
            break;
        }
    }
}
-(void)getScope
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrTargetSales=[dictMasters valueForKey:@"ScopeMaster"];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    for (int i=0; i<arrTargetSales.count; i++)
    {
        NSDictionary *dict=[arrTargetSales objectAtIndex:i];
        /* if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]] isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]] isEqualToString:@"true"])
         {
         [arr addObject:dict];
         }*/
        [arr addObject:dict];
    }
    arrayScope=arr;
}

-(void)getTarget
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrTargetSales=[dictMasters valueForKey:@"Targets"];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    for (int i=0; i<arrTargetSales.count; i++)
    {
        NSDictionary *dict=[arrTargetSales objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]] isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]] isEqualToString:@"true"])
        {
            [arr addObject:dict];
        }
    }
    arrayTarget=arr;
}
-(void)getFrequency
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    arrProposedFreq=[dictMasters valueForKey:@"Frequencies"];
    
}
-(void)setTabelBordeColor
{
    _tblService.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblService.layer.borderWidth=1.0;
    
    _tblScope.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblScope.layer.borderWidth=1.0;
    
    _tblTarget.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblTarget.layer.borderWidth=1.0;
    
    _tblNonStandardClarkPest.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblNonStandardClarkPest.layer.borderWidth=1.0;
    //_viewServiceDetail.layer.cornerRadius=5.0;
}

#pragma mark- --------------- CLARK PEST CODE ----------------


#pragma mark-  ------ Service Save & Fetch From Coredata ------

-(void)SaveSoldServiceStandardDetailClarkPest
{
    isEditedInSalesAuto=YES;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    
    SoldServiceStandardDetail *objentitySoldServiceStandardDetail = [[SoldServiceStandardDetail alloc]initWithEntity:entitySoldServiceStandardDetail insertIntoManagedObjectContext:context];
    objentitySoldServiceStandardDetail.leadId=strLeadId;
    objentitySoldServiceStandardDetail.createdBy=@"";        objentitySoldServiceStandardDetail.createdDate=@"";
    
    objentitySoldServiceStandardDetail.discount=@"";
    
    objentitySoldServiceStandardDetail.initialPrice=@"";
    objentitySoldServiceStandardDetail.isSold=@"false";
    
    
    objentitySoldServiceStandardDetail.maintenancePrice=@"";
    
    objentitySoldServiceStandardDetail.modifiedBy=@"";
    objentitySoldServiceStandardDetail.modifiedDate=[global modifyDate];
    objentitySoldServiceStandardDetail.modifiedInitialPrice=@"";
    objentitySoldServiceStandardDetail.modifiedMaintenancePrice=@"";
    objentitySoldServiceStandardDetail.packageId=@"";
    
    objentitySoldServiceStandardDetail.serviceFrequency=@"";
    objentitySoldServiceStandardDetail.frequencySysName=@"";
    //End
    objentitySoldServiceStandardDetail.serviceId=strServiceId;
    objentitySoldServiceStandardDetail.serviceSysName=strServiceSysName;
    objentitySoldServiceStandardDetail.serviceDescription = strServiceDescription;
    
    //Temp
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strRandomId = [NSString stringWithFormat:@"%@%@",strDate,strTime];
    objentitySoldServiceStandardDetail.soldServiceStandardId=strRandomId;
    objentitySoldServiceStandardDetail.isTBD=@"";
    objentitySoldServiceStandardDetail.serviceTermsConditions=@"";//[dictTermsForService valueForKey:strServiceSysName];
    
    objentitySoldServiceStandardDetail.userName=strUserName;
    objentitySoldServiceStandardDetail.companyKey=strCompanyKey;
    
    //Nilind 24 may
    objentitySoldServiceStandardDetail.unit=@"1";
    
    
    
    objentitySoldServiceStandardDetail.finalUnitBasedInitialPrice=@"";
    objentitySoldServiceStandardDetail.finalUnitBasedMaintPrice=@"";
    objentitySoldServiceStandardDetail.billingFrequencySysName=@"";
    
    
    objentitySoldServiceStandardDetail.discountPercentage=@"";
    
    objentitySoldServiceStandardDetail.bundleDetailId=@"0";
    objentitySoldServiceStandardDetail.bundleId=@"0";
    
    objentitySoldServiceStandardDetail.additionalParameterPriceDcs=@"";
    objentitySoldServiceStandardDetail.billingFrequencyPrice=@"";
    
    
    objentitySoldServiceStandardDetail.servicePackageName=@"";
    
    objentitySoldServiceStandardDetail.serviceDescription=strServiceDescription;
    objentitySoldServiceStandardDetail.isChangeServiceDesc=strIsChangedServiceDesc;
    
    NSError *error1;
    [context save:&error1];
    
    [self fetchFromCoreDataStandardClarkPest];
}
-(void)fetchFromCoreDataStandardClarkPest
{
    float tableViewHeight = 0.0;
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    // arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    arrSavedServiceDetail=nil;
    _const_TblService_H.constant = 0.0;
    
    // NSMutableArray *arrServiceName = [NSMutableArray new];
    
    if (arry.count>0)
    {
        //        for(NSManagedObject *initialInfo in arry)
        //        {
        //            NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
        //            NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
        //            [arrayTemp addObject:dict];
        //        }
        arrSavedServiceDetail = arry;//[arry mutableCopy];
        
        
        for(NSDictionary *dict in arry)
        {
            CGFloat textHght = [self findHeightForText:[dict valueForKey:@"serviceDescription"] havingWidth:self.view.frame.size.width-133 andFont:[UIFont systemFontOfSize:14.0]].height;//625.0
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+155;// 100 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+(155-21));// (100-21) i.e row height after removing description height
            }
        }
        
        [self showAllSavedServiceName];
        
        _const_TblService_H.constant = tableViewHeight;
        [_tblService reloadData];
    }
    else
    {
        _const_TblService_H.constant = 0.0;
        _textViewInitialDescription.text = @"";
        [_textViewInitialDescription resignFirstResponder];
        _textViewMaintDescription.text = @"";
        [_textViewMaintDescription resignFirstResponder];
        
    }
    
}
-(BOOL)checkServiceAvialabiltyInDataBaseClarkPest:(NSString *)strSysName
{
    BOOL chk;
    chk=NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && serviceSysName=%@",strLeadId,strSysName];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    if (arry.count==0)
    {
        chk=NO;
    }
    else
    {
        chk=YES;
    }
    return chk;
}
-(void)deleteFromCoreDataSalesInfoClarkPest:(NSString *)sysName
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        NSString *strSysNameService;
        
        strSysNameService=[NSString stringWithFormat:@"%@",[data valueForKey:@"serviceSysName"]];
        if ([strSysNameService isEqualToString:sysName])
        {
            [context deleteObject:data];
            break;
            
        }
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //[self heightManage];
}
-(void)updateServiceClarkPest:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    
    NSError *error1 = nil;
    NSManagedObject *matchesSerivce =indexValue;
    if (chkBtnCheckBoxStan==YES)
    {
        [matchesSerivce setValue:@"true" forKey:@"isSold"];
    }
    else
    {
        [matchesSerivce setValue:@"false" forKey:@"isSold"];
    }
    [matchesSerivce setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataStandardClarkPest];
}

#pragma mark ------- NonStandard Clark Pest ---------
-(void)fetchFromCoreDataNonStandardClarkPest
{
    
    float tableViewHeight = 0.0;
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    arrSavedNonStanServiceDetail=nil;
    //_const_TblNonStan_H.constant = 0.0;
    
    
    if (arry.count>0)
    {
        
        arrSavedNonStanServiceDetail = arry;
        
        
        for(NSDictionary *dict in arry)
        {
            
            /*CGFloat textHght = [self findHeightForText:[dict valueForKey:@"serviceDescription"] havingWidth:_tblNonStandardClarkPest.frame.size.width-113 andFont:[UIFont systemFontOfSize:16.0]].height;//625.0
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+155;// 125 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+(155-21));// (125-21) i.e row height after removing description height
            }*/
            
            CGFloat textHght = [self getAttributedLabelHeight:[dict valueForKey:@"serviceDescription"]];

            
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+155 ;// 125 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+(155  -21));// (125-21) i.e row height after removing description height
            }
            
            
        }
        
        //[self showAllSavedServiceName];
        
        _const_TblNonStan_H.constant = tableViewHeight;
        [_tblNonStandardClarkPest reloadData];
    }
    else
    {
        _const_TblNonStan_H.constant = 0.0;
        
        _textViewInitialDescription.text = @"";
        [_textViewInitialDescription resignFirstResponder];
        _textViewMaintDescription.text = @"";
        [_textViewMaintDescription resignFirstResponder];
        
    }
    
    float ftbl = _tblNonStandardClarkPest.contentSize.height;

    if (ftbl > _const_TblNonStan_H.constant) {
        
        _const_TblNonStan_H.constant = ftbl;
        
    }
    
    if (arry.count == 0)
    {
        _const_TblNonStan_H.constant = 0;
    }
    [_tblNonStandardClarkPest reloadData];
}

-(void)deleteFromCoreDataSalesInfoNonStandardClarkPest:(NSString *)serviceId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        NSString *strsoldServiceNonStandardId;
        
        strsoldServiceNonStandardId=[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceNonStandardId"]];
        if ([strsoldServiceNonStandardId isEqualToString:serviceId])
        {
            [context deleteObject:data];
            break;
            
        }
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //[self heightManage];
}
-(void)updateServiceNonStandardClarkPest:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceNonStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceNonStandardDetail];
    
    NSError *error1 = nil;
    
    NSManagedObject *matchesSerivce =indexValue;
    
    if (chkBtnCheckBoxNonStan==YES)
    {
        [matchesSerivce setValue:@"true" forKey:@"isSold"];
    }
    else
    {
        [matchesSerivce setValue:@"false" forKey:@"isSold"];
        
    }
    [matchesSerivce setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataNonStandardClarkPest];
}



#pragma mark- ------ Scope Save & Fetch From Coredata -------

-(void)saveScopeCoreDataClarkPest
{
    isEditedInSalesAuto=YES;
    
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    
    LeadCommercialScopeExtDc *objCommercialScopeExtDc = [[LeadCommercialScopeExtDc alloc]initWithEntity:entityLeadCommercialScopeExtDc insertIntoManagedObjectContext:context];
    
    objCommercialScopeExtDc.companyKey=strCompanyKey;
    objCommercialScopeExtDc.userName=strUserName;
    objCommercialScopeExtDc.leadId = strLeadId;
    objCommercialScopeExtDc.leadCommercialScopeId = @"";
    objCommercialScopeExtDc.scopeSysName = strScopeSysName;
    objCommercialScopeExtDc.scopeDescription = strScopeDescription;
    objCommercialScopeExtDc.isChangedScopeDesc = strIsChangedScopeDesc;
    objCommercialScopeExtDc.title=strScopeName;
    
    NSError *error22222;
    [context save:&error22222];
    
    [self fetchScopeFromCoreData];
    
    
}
-(void)fetchScopeFromCoreData
{
    NSMutableArray *arrayTemp = [NSMutableArray new];
    float tableViewHeight = 0.0;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialScopeExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    for(NSManagedObject *initialInfo in arry)
    {
        NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
        NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
        [arrayTemp addObject:dict];
    }
    
    arrSavedScopeDetail=nil;
    
    if (arry.count>0)
    {
        arrSavedScopeDetail = arrayTemp;
        for(NSDictionary *dict in arrSavedScopeDetail)
        {
            CGFloat textHght = [self findHeightForText:[dict valueForKey:@"scopeDescription"] havingWidth:self.view.frame.size.width-133+70 andFont:[UIFont systemFontOfSize:16.0]].height;//
            
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+65;// 65 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+(65-21));// 21 i.e description height
            }
        }
        _const_TblScope_H.constant = tableViewHeight;
        [_tblScope reloadData];
    }
    else
    {
        _const_TblScope_H.constant = 0.0;
    }
}
-(BOOL)checkScopeAvailabilityInDataBase:(NSString *)strSysName
{
    BOOL chk=NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialScopeExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && scopeSysName=%@",strLeadId,strSysName];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    /*for(NSManagedObject *initialInfo in arry)
     {
     NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
     NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
     [arrayTemp addObject:dict];
     }*/
    
    
    if (arry.count==0)
    {
        chk=NO;
    }
    else
    {
        chk=YES;
    }
    
    return chk;
}
-(void)deleteScopeFromCoreDataSalesInfoClarkPest:(NSString *)sysName
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        NSString *strSysNameService;
        
        strSysNameService=[NSString stringWithFormat:@"%@",[data valueForKey:@"scopeSysName"]];
        if ([strSysNameService isEqualToString:sysName])
        {
            [context deleteObject:data];
            break;
            
        }
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //[self heightManage];
}
#pragma mark- ------ Target Save & Fetch From Coredata -------

-(void)saveTargetCoreDataClarkPest
{
    isEditedInSalesAuto=YES;
    
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    
    LeadCommercialTargetExtDc *objCommercialTargetExtDc = [[LeadCommercialTargetExtDc alloc]initWithEntity:entityLeadCommercialTargetExtDc insertIntoManagedObjectContext:context];
    
    objCommercialTargetExtDc.companyKey=strCompanyKey;
    objCommercialTargetExtDc.userName=strUserName;
    objCommercialTargetExtDc.leadId = strLeadId;
    objCommercialTargetExtDc.leadCommercialTargetId = @"";//[global getReferenceNumber];
    objCommercialTargetExtDc.targetSysName = strTargetSysName;
    objCommercialTargetExtDc.targetDescription = strTargetDescription;
    objCommercialTargetExtDc.isChangedTargetDesc = strIsChangedTargetDesc;
    objCommercialTargetExtDc.name=strTargetName;
    objCommercialTargetExtDc.mobileTargetId = [global getReferenceNumber];
    objCommercialTargetExtDc.wdoProblemIdentificationId = @"";
    //objCommercialTargetExtDc.targetImageDetail = @"";
    NSError *error22222;
    [context save:&error22222];
    
    [self fetchTargetFromCoreData];
    
}
-(void)fetchTargetFromCoreData
{
    _const_TblTarget_H.constant = 100;
    NSMutableArray *arrayTemp = [NSMutableArray new];
    float tableViewHeight = 0.0;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTargetExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    /*NSArray *arry = [[self.fetchedResultsControllerSalesInfo fetchedObjects] mutableCopy];
     
     for(NSManagedObject *initialInfo in arry)
     {
     NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
     NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
     [arrayTemp addObject:dict];
     }*/
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    arrSavedTargetDetail=nil;
    if (arry.count>0)
    {
        for(NSManagedObject *initialInfo in arry)
        {
            NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
            NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
            [arrayTemp addObject:dict];
        }
        arrSavedTargetDetail = [arrayTemp mutableCopy];
        
        for(NSDictionary *dict in arry)
        {
            CGFloat textHght = [self findHeightForText:[dict valueForKey:@"targetDescription"] havingWidth:self.view.frame.size.width-133+70 andFont:[UIFont systemFontOfSize:16.0]].height; // 133
            
            
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+100;// 65 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+(100-21));// 21 i.e description height
            }
        }
        
        _const_TblTarget_H.constant = tableViewHeight;
        [_tblTarget reloadData];
    }
    else
    {
        _const_TblTarget_H.constant = 0.0;
    }
    
}

-(BOOL)checkTargetAvailabilityInDataBase:(NSString *)strSysName
{
    BOOL chk=NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTargetExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && targetSysName=%@",strLeadId,strSysName];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    arrSavedTargetDetail=nil;
    if (arry.count==0)
    {
        chk=NO;
    }
    else
    {
        chk=YES;
    }
    return chk;
    
}
-(void)deleteTargetFromCoreDataSalesInfoClarkPest:(NSString *)sysName TargetId:(NSString*)strLeadCommercialTargetId MobileId:(NSString *)strMobileTargetId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        NSString *strSysNameService;
        
        //Nilind 22 Nov
        
        NSString *strTargetId,*strMobileId;
        
        strTargetId=[NSString stringWithFormat:@"%@",[data valueForKey:@"leadCommercialTargetId"]];
        strMobileId=[NSString stringWithFormat:@"%@",[data valueForKey:@"mobileTargetId"]];
        
        if (strLeadCommercialTargetId.length > 0 && ![strLeadCommercialTargetId isEqualToString:@""])
        {
            if ([strLeadCommercialTargetId isEqualToString:strTargetId])
            {
                [context deleteObject:data];
                break;
                
            }
        }
        else
        {
            if ([strMobileTargetId isEqualToString:strMobileId])
            {
                [context deleteObject:data];
                break;
                
            }
        }
        //End
        
        /* strSysNameService=[NSString stringWithFormat:@"%@",[data valueForKey:@"targetSysName"]];
         
         if ([strSysNameService isEqualToString:sysName])
         {
         [context deleteObject:data];
         break;
         
         }*/
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //[self heightManage];
}
#pragma mark- ------- Initial Price Save & Fetch From Coredata -------

-(void)saveLeadCommercialInitialInfoCoreData:(NSDictionary*)dict
{
    isEditedInSalesAuto=YES;
    
    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
    
    LeadCommercialInitialInfoExtDc *objCommercialInitialInfoExtDc = [[LeadCommercialInitialInfoExtDc alloc]initWithEntity:entityLeadCommercialInitialInfoExtDc insertIntoManagedObjectContext:context];
    
    objCommercialInitialInfoExtDc.companyKey=strCompanyKey;
    objCommercialInitialInfoExtDc.userName=strUserName;
    objCommercialInitialInfoExtDc.leadId = strLeadId;
    objCommercialInitialInfoExtDc.initialDescription = [dict valueForKey:@"initialDescription"];
    objCommercialInitialInfoExtDc.leadCommInitialInfoId = [NSString stringWithFormat:@"%@",[self getRandaomID]];
    objCommercialInitialInfoExtDc.initialPrice = [dict valueForKey:@"initialPrice"];
    NSError *error22222;
    [context save:&error22222];
    
    _txtFieldProposedInitialPrice.text=@"";
}
-(void)fetchLeadCommercialInitialInfoFromCoreData
{
    float tableHeight = 0.0;
    NSMutableArray *arrayTemp = [NSMutableArray new];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialInitialInfoExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    // arrAllObj=[[NSArray alloc]init];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    for(NSManagedObject *initialInfo in arry)
    {
        NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
        NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
        [arrayTemp addObject:dict];
    }
    arrSavedInitialPriceDetail=nil;
    subTotalInitialPrice = 0.0;
    
    if (arrayTemp.count>0)
    {
        arrSavedInitialPriceDetail=(NSMutableArray*)arrayTemp;
        float totalInitialPrice = 0.0;
        for(NSDictionary *dict in arrSavedInitialPriceDetail)
        {
            totalInitialPrice = totalInitialPrice + [[dict valueForKey:@"initialPrice"] floatValue];
            float hght = [self findHeightForText:[dict valueForKey:@"initialDescription"] havingWidth:self.view.frame.size.width-125 andFont:[UIFont systemFontOfSize:14.0]].height;
            if(hght>30)
            {
                tableHeight = tableHeight+(73-21)+hght;// (91-30) means.. pehle cell ki total height me se txtview description ki default height minus kari fir upar jo height nikali h(hght) usko add kari h
            }
            else
            {
                tableHeight = tableHeight+73; // 91 is cell default height
            }
        }
        _heightTblViewInitialPrice.constant = tableHeight;
        _labelSubtotalInitialPrice.text = [NSString stringWithFormat:@"%.2f",totalInitialPrice];
        _labelSubtotalInitialPrice.text = [NSString stringWithFormat:@"%.2f",totalInitialPrice];
        subTotalInitialPrice = totalInitialPrice;
        _heightSubtotalInitialPrice.constant = 40.0;
        _heightSubtotalInitialPrice.constant = 0.0;
        [_tableViewInitialPrice reloadData];
    }
    else
    {
        _heightTblViewInitialPrice.constant = 0.0;
        _heightSubtotalInitialPrice.constant = 0.0;
    }
    
    /* arrayTemp = [[self.fetchedResultsControllerSalesInfo fetchedObjects] mutableCopy];
     
     for(NSManagedObject *initialInfo in arrayTemp)
     {
     NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
     NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
     [arrayTemp addObject:dict];
     }
     if(arrayTemp.count>0)
     {
     arrayCommercialInitialPrice = arrayTemp;
     }*/
}

-(void)deleteLeadCommercialInitialInfoFromCoreData:(NSString*)leadCommInitialInfoId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        NSString *strLeadCommInitialInfoId;
        
        strLeadCommInitialInfoId=[NSString stringWithFormat:@"%@",[data valueForKey:@"leadCommInitialInfoId"]];
        if ([strLeadCommInitialInfoId isEqualToString:leadCommInitialInfoId])
        {
            [context deleteObject:data];
            break;
            
        }
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //[self heightManage];
    
}

#pragma mark- ------ Maintenance Price Save & Fetch From Coredata -------

-(void)saveLeadCommercialMaintInfoCoreData:(NSDictionary*)dict
{
    isEditedInSalesAuto=YES;
    
    entityLeadCommercialMaintInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
    
    LeadCommercialMaintInfoExtDc *objCommercialMaintInfoExtDc = [[LeadCommercialMaintInfoExtDc alloc]initWithEntity:entityLeadCommercialMaintInfoExtDc insertIntoManagedObjectContext:context];
    
    objCommercialMaintInfoExtDc.companyKey=strCompanyKey;
    objCommercialMaintInfoExtDc.userName=strUserName;
    objCommercialMaintInfoExtDc.leadId = strLeadId;
    objCommercialMaintInfoExtDc.maintenanceDescription = [dict valueForKey:@"maintenanceDescription"];
    objCommercialMaintInfoExtDc.maintenancePrice = [dict valueForKey:@"maintenancePrice"];
    objCommercialMaintInfoExtDc.frequencySysName = [dict valueForKey:@"frequencySysName"];
    objCommercialMaintInfoExtDc.leadCommMaintInfoId = [NSString stringWithFormat:@"%@",[self getRandaomID]];
    
    NSError *error22222;
    [context save:&error22222];
    
    _txtFieldProposedMaintPrice.text=@"";
    
}
-(void)fetchLeadCommercialMaintInfoFromCoreData
{
    NSMutableArray *arrayTemp = [NSMutableArray new];
    float tableHeight = 0.0;
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    for(NSManagedObject *initialInfo in arry)
    {
        NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
        NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
        [arrayTemp addObject:dict];
    }
    arrSavedMaintPriceDetail=nil;
    subTotalMaintPrice = 0.0;
    
    if (arrayTemp.count>0)
    {
        arrSavedMaintPriceDetail=arrayTemp;
        float totalMaintPrice = 0.0;
        for(NSDictionary *dict in arrSavedMaintPriceDetail)
        {
            totalMaintPrice = totalMaintPrice + [[dict valueForKey:@"maintenancePrice"] floatValue];
            
            float hght = [self findHeightForText:[dict valueForKey:@"mentainanceDescription"] havingWidth:self.view.frame.size.width-125 andFont:[UIFont systemFontOfSize:14.0]].height;
            if(hght>30)
            {
                tableHeight = tableHeight+(104-21)+hght;// (131-30) means.. pehle cell ki total height me se txtview description ki default height minus kari fir upar jo height nikali h(hght) usko add kari h
            }
            else
            {
                tableHeight = tableHeight+104; // 131 is cell default height
            }
        }
        _heightTblViewMaintPrice.constant = tableHeight;
        _labelSubtotalMaintPrice.text = [NSString stringWithFormat:@"%.2f",totalMaintPrice];
        _labelSubtotalMaintPrice.text = [NSString stringWithFormat:@"%.2f",totalMaintPrice];
        subTotalMaintPrice = totalMaintPrice;
        _heightSubtotalMaintPrice.constant = 40.0;
        _heightSubtotalMaintPrice.constant = 0.0;
        [_tableViewMaintPrice reloadData];
    }
    else
    {
        _heightTblViewMaintPrice.constant = 0.0;
        _heightSubtotalMaintPrice.constant = 0.0;
    }
    
    
    /*arrayTemp = [[self.fetchedResultsControllerSalesInfo fetchedObjects] mutableCopy];
     
     
     for(NSManagedObject *initialInfo in arrayTemp)
     {
     NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
     NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
     [arrayTemp addObject:dict];
     }
     if(arrayTemp.count>0)
     {
     arrayCommercialMaintPrice = arrayTemp;
     }*/
}
-(void)deleteLeadCommercialMaintInfoFromCoreData:(NSString*)leadCommMaintInfoId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        NSString *strLeadCommInitialInfoId;
        
        strLeadCommInitialInfoId=[NSString stringWithFormat:@"%@",[data valueForKey:@"leadCommMaintInfoId"]];
        if ([strLeadCommInitialInfoId isEqualToString:leadCommMaintInfoId])
        {
            [context deleteObject:data];
            break;
        }
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //[self heightManage];
}


#pragma mark - action on add initial price
- (IBAction)actionOnAddInitialPrice:(id)sender {
    
    [self endEditing];
    if(!(_txtFieldProposedInitialPrice.text.length>0))
    {
        [global AlertMethod:@"Alert!" :@"Please enter proposed initial price"];
    }
    else
    {
        NSDictionary *dict = @{@"initialPrice":[NSString stringWithFormat:@"%@",_txtFieldProposedInitialPrice.text],
                               @"initialDescription":[NSString stringWithFormat:@"%@",_textViewInitialDescription.text.length>0 ? _textViewInitialDescription.text : @""]
                               };
        
        [self saveLeadCommercialInitialInfoCoreData:dict];
        [self fetchLeadCommercialInitialInfoFromCoreData];
        _txtFieldProposedInitialPrice.text = @"";
        [self showAllSavedServiceName];
        // _textViewInitialDescription.text = @"";
        // [_textViewInitialDescription resignFirstResponder];
        [_txtFieldProposedInitialPrice resignFirstResponder];
    }
}

#pragma mark - action on add maintenance price

- (IBAction)actionOnMaintPrice:(id)sender {
    
    [self endEditing];
    if(!(_txtFieldProposedMaintPrice.text.length>0))
    {
        [global AlertMethod:@"Alert!" :@"Please enter proposed maintenance price"];
    }
    else
    {
        NSDictionary *dict = @{@"maintenancePrice":[NSString stringWithFormat:@"%@",_txtFieldProposedMaintPrice.text],
                               @"frequencySysName":strFreqSysName,
                               @"maintenanceDescription":[NSString stringWithFormat:@"%@",_textViewMaintDescription.text.length>0 ? _textViewMaintDescription.text : @""]
                               };
        
        [self saveLeadCommercialMaintInfoCoreData:dict];
        [self fetchLeadCommercialMaintInfoFromCoreData];
        _txtFieldProposedMaintPrice.text = @"";
        [self showAllSavedServiceName];
        //        _textViewMaintDescription.text = @"";
        //        [_textViewMaintDescription resignFirstResponder];
        [_txtFieldProposedMaintPrice resignFirstResponder];
        
    }
}

#pragma mark - action on ProposedInitialServiceDate

- (IBAction)actionOnProposedInitialServiceDate:(id)sender
{
    [self endEditing];
    [self addPickerViewDate];
}

#pragma mark - methods related to date picker
-(void)addPickerViewDate
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    pickerDate.minimumDate = [NSDate date];
    
    [viewForDate setHidden:NO];
    
    viewBackGround1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround1.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround1];
    
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround1 setUserInteractionEnabled:YES];
    [viewBackGround1 addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround1 addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:14];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 1)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 1)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(30, lblLine.frame.origin.y+10, (viewForDate.frame.size.width-90)/2, 30)];// 90 i.e (30(leading space of btnClose) +30(horizontal space btwn btnClose and btnDone) + 30(trailing space of btnDone))
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnClose.titleLabel.font = [UIFont systemFontOfSize:14.0];
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, btnClose.frame.size.width,30)];
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.titleLabel.font = [UIFont systemFontOfSize:14.0];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
-(void)tapDetectedOnBackGroundView
{
    [viewForDate removeFromSuperview];
    [viewBackGround1 removeFromSuperview];
    [tblData removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSString *strServiceDate = [dateFormat stringFromDate:pickerDate.date];
    [_buttonProposedInitialServiceDate setTitle:strServiceDate forState:UIControlStateNormal];
    
    [viewForDate removeFromSuperview];
    [viewBackGround1 removeFromSuperview];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround1 removeFromSuperview];
}


#pragma mark - method for converting html string to normal string
-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}

#pragma mark - method to get random id
-(NSString*)getRandaomID
{
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strRandomId = [NSString stringWithFormat:@"%@%@",strDate,strTime];
    return strRandomId;
}

#pragma mark - method to get height of string

- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

-(void)goToHTMLEditor:(NSString*)strText
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setHtmlContentOnEditing) name:@"Notificationtypefromwhichview" object:nil];
    
    NSUserDefaults *defsHtml = [NSUserDefaults standardUserDefaults];
    [defsHtml setValue:@"" forKey:@"HtmlContent"];
    [defsHtml synchronize];
    
}
-(void)setHtmlContentOnEditing{
    
    NSUserDefaults *defsHtml = [NSUserDefaults standardUserDefaults];
    
    strHTML = [defsHtml valueForKey:@"HtmlContent"];
    
    //    NSAttributedString *attributedStringTermsnConditions = [[NSAttributedString alloc]
    //                                                            initWithData: [strHTML dataUsingEncoding:NSUnicodeStringEncoding]
    //                                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSFontAttributeName:[UIFont systemFontOfSize:80],NSForegroundColorAttributeName:[UIColor redColor] }
    //                                                            documentAttributes: nil
    //                                                            error: nil
    //                                                            ];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[strHTML dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    /* if(textViewHTML==_textViewInitialDescription)
     {
     _textViewInitialDescription.attributedText = attributedString;
     textViewHTML = nil;
     }
     else if (textViewHTML==_textViewMaintDescription)
     {
     _textViewMaintDescription.attributedText = attributedString;
     textViewHTML = nil;
     }
     else
     {
     _textViewDescription_PopUp.attributedText = attributedString;
     }*/
    _textViewDescription_PopUp.attributedText = attributedString;
    //_textViewDescription_PopUp.attributedText=attributedStringTermsnConditions;
    _textViewDescription_PopUp.font=[UIFont systemFontOfSize:20];
    NSLog(@"Html Code = = = =%@",strHTML);
    
    //[global displayAlertController:@"Html-Code" :strHTML :self];
}

-(NSAttributedString*)abc:(NSString *)str
{
    //  HTML String
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData: [str dataUsingEncoding:NSUnicodeStringEncoding] options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes: nil error: nil ];
    
    return attributedString;
}

-(NSString*)getTitleNameFromSysName:(NSString*)strTempSysName serviceType:(NSString*)strServiceType
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arr=[dictMasters valueForKey:strServiceType];
    NSString *strTitle = @"";
    for(NSDictionary *dict in arr)
    {
        if([[dict valueForKey:@"SysName"] isEqualToString:strTempSysName])
        {
            strTitle = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
            break;
        }
    }
    return strTitle;
}

-(NSAttributedString*)getAttributedString:(NSString *)strString
{
    NSAttributedString *attributedStringTermsnConditions = [[NSAttributedString alloc]
                                                            initWithData: [strString dataUsingEncoding:NSUnicodeStringEncoding]
                                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSFontAttributeName:[UIFont systemFontOfSize:16] }
                                                            documentAttributes: nil
                                                            error: nil
                                                            ];
    
    
    /*  UIFont *font = [UIFont systemFontOfSize:16];
     NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
     forKey:NSFontAttributeName];
     attributedStringTermsnConditions = [[NSAttributedString alloc] initWithString:strString attributes:attrsDictionary];*/
    
    return attributedStringTermsnConditions;
}

-(NSString*)getHTMLstringFromAttributeString:(NSAttributedString*)strAttribute
{
    NSDictionary *documentAttributes = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
    NSData *htmlData = [strAttribute dataFromRange:NSMakeRange(0, strAttribute.length) documentAttributes:documentAttributes error:NULL];
    //Convert the NSData into HTML String with UTF-8 Encoding
    NSString *htmlString = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    return htmlString;
}


-(void)tapDetectedOnBackGroundViewPopUp
{
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}
-(void)fetchFromCoreDataStandardClarkPestSoldCount
{
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;// =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    // arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrSoldCount = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
}
-(void)fetchFromCoreDataNonStandardClarkPestSoldCount
{
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate; //=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrSoldCountNonStan = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
}
-(void)disableUIElementsInCaseOfLeadStatusComplete
{
    _viewServiceDetail.userInteractionEnabled = false;
    
    [btnAddRecord setEnabled:NO];
    _btnGlobalSync.enabled=NO;
    _btnMarkAsLost.hidden=YES;
    _btnApplyDiscount.enabled=NO;
    _btnAddService.enabled=NO;
    
    if ([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
    {
        _btnMarkAsLost.hidden=YES;
    }
    
    _btnCoverLetter.enabled = NO;
    _btnIntroductionLetter.enabled = NO;
    _btnTermsOfServices.enabled = NO;
    _txtViewTermsServices.editable = NO;
}

-(void)showAllSavedServiceName
{
    NSMutableArray *arrayServiceName = [NSMutableArray new];
    
    NSMutableArray *arrShowSoldServie=[[NSMutableArray alloc]init];
    for(NSDictionary *dict in arrSavedServiceDetail)
    {
        NSString *strSoldCheck=[NSString stringWithFormat:@"%@",[dict valueForKey:@"isSold"]];
        if ([strSoldCheck isEqualToString:@"true"]||[strSoldCheck isEqualToString:@"True"]||[strSoldCheck isEqualToString:@"1"])
        {
            [arrShowSoldServie addObject:dict];
        }
    }
    
    
    // for(NSDictionary *dict in arrSavedServiceDetail)
    for(NSDictionary *dict in arrShowSoldServie)
    {
        
        [arrayServiceName addObject:[NSString stringWithFormat:@"%@",[dictServiceName valueForKey:[dict valueForKey:@"serviceSysName"]]]];
    }
    _textViewInitialDescription.text = [arrayServiceName componentsJoinedByString:@","];
    [_textViewInitialDescription resignFirstResponder];
    _textViewMaintDescription.text = [arrayServiceName componentsJoinedByString:@","];
    [_textViewMaintDescription resignFirstResponder];
}
-(void)fetchForCommercialAppliedDiscountFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    NSMutableArray* arrTempDiscountCoupon=[[NSMutableArray alloc]init];
    NSMutableArray* arrTempDiscountCredit=[[NSMutableArray alloc]init];
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
            {
                [arrTempDiscountCoupon addObject:matchesDiscount];
            }
            else
            {
                [arrTempDiscountCredit addObject:matchesDiscount];
            }
        }
    }
    
    NSError *error = nil;
    
    for(matchesDiscount in arrTempDiscountCredit)
    {
        if(([[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"applicableForInitial"]]isEqualToString:@"1"])|| [[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
        {
            if([[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"isDiscountPercent"]]isEqualToString:@"true"])
            {
                float discountPercent = [[matchesDiscount valueForKey:@"discountPercent"] floatValue];
                float discountAmount = (subTotalInitialPrice*discountPercent)/100.0;
                subTotalInitialPrice = subTotalInitialPrice-discountAmount;
                [matchesDiscount setValue:[NSString stringWithFormat:@"%f",discountAmount] forKey:@"appliedInitialDiscount"];
                
            }
        }
        
        if(([[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"applicableForMaintenance"]]isEqualToString:@"1"])|| [[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"applicableForMaintenance"]]isEqualToString:@"true"])
        {
            if([[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"isDiscountPercent"]]isEqualToString:@"true"])
            {
                float discountPercent = [[matchesDiscount valueForKey:@"discountPercent"] floatValue];
                float discountAmount = (subTotalMaintPrice*discountPercent)/100.0;
                subTotalMaintPrice = subTotalMaintPrice-discountAmount;
                [matchesDiscount setValue:[NSString stringWithFormat:@"%f",discountAmount] forKey:@"appliedMaintDiscount"];
            }
            
        }
        [context save:&error];
    }
    
    for(matchesDiscount in arrTempDiscountCoupon)
    {
        if(([[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"applicableForInitial"]]isEqualToString:@"1"])|| [[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
        {
            if([[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"isDiscountPercent"]]isEqualToString:@"true"])
            {
                float discountPercent = [[matchesDiscount valueForKey:@"discountPercent"] floatValue];
                float discountAmount = (subTotalInitialPrice*discountPercent)/100.0;
                subTotalInitialPrice = subTotalInitialPrice-discountAmount;
                [matchesDiscount setValue:[NSString stringWithFormat:@"%f",discountAmount] forKey:@"appliedInitialDiscount"];
                
            }
        }
        [context save:&error];
    }
}

#pragma mark - observer methods related to keyboard

- (void)keyboardDidShow:(NSNotification *)notification

{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.view setFrame:CGRectMake(self.view.frame.origin.x,-110,self.view.frame.size.width,self.view.frame.size.height)];
        
    }];
    
}



-(void)keyboardDidHide:(NSNotification *)notification

{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
        
    }];
    
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)goToAddStanNonStanService
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
    
    AddStandardNonStandardServiceVC
    
    *objAddStandardNonStandardVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddStandardNonStandardServiceVC"];
    objAddStandardNonStandardVC.strWoId = strLeadId;
    objAddStandardNonStandardVC.strBranchSysName = strBranchSysName;
    
    //strWoId
    [self.navigationController pushViewController:objAddStandardNonStandardVC animated:NO];
}
#pragma mark- ------------------- Clark Pest Template Changes -------------------
-(void)setValueForParticipants
{
    if (tblData.tag==14)
    {
        NSMutableArray *arrTitle=[[NSMutableArray alloc]init];
        
        for(int i=0;i<arrSelectedMarketingContent.count;i++)
        {
            [arrTitle addObject:[dictSalesContentTitleFromSysName valueForKey:[arrSelectedMarketingContent objectAtIndex:i]]];
        }
        
        if (arrTitle.count==0)
        {
            [_btnMarketingContent setTitle:@"Select Sales Marketing Content" forState:UIControlStateNormal];
        }
        else
        {
            NSString *joinedComponents = [arrTitle componentsJoinedByString:@","];
            [_btnMarketingContent setTitle:joinedComponents forState:UIControlStateNormal];
        }
        
    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    [self setValueForParticipants];
}
-(void)setBorderTemplate
{
    _viewCoverLetter.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewCoverLetter.layer.borderWidth=1.0;
    
    _viewIntroductionLetter.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewIntroductionLetter.layer.borderWidth=1.0;
    
    
    _viewForSalesMarketingContent.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewForSalesMarketingContent.layer.borderWidth=1.0;
    
    
    _viewTermsOfService.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewTermsOfService.layer.borderWidth=1.0;
    
    
    [self getLetterTemplateAndTermsOfService];
    [self fetchLetterTemplateFromCoreData];
    [self fetchSalesMarketingContentClarkPest];
}


- (IBAction)actionOnBtnCoverLetter:(id)sender
{
    [self endEditing];
    if(arrCoverLetter.count==0)
    {
        [global AlertMethod:Alert :NoDataAvailableee];
        return;
    }
    tblData.tag=10;
    [self tableLoad:tblData.tag];
}
- (IBAction)actionOnBtnIntroductionLetter:(id)sender
{
    [self endEditing];
    if(arrIntroductionLetter.count==0)
    {
        [global AlertMethod:Alert :NoDataAvailableee];
        return;
    }
    tblData.tag=11;
    [self tableLoad:tblData.tag];
    
}

- (IBAction)actionOnMarketingContent:(id)sender
{
    [self endEditing];
    if([arrMarketingContent count]==0 || ![arrMarketingContent isKindOfClass:[NSArray class]])
    {
        [global AlertMethod:@"Alert!" :@"No data available"];
    }
    else
    {
        tblData.tag=14;
        [self tableLoad:tblData.tag];
    }
}
- (IBAction)actionOnBtnTermsOfServices:(id)sender
{
    [self endEditing];
    if(arrTermsOfService.count==0)
    {
        [global AlertMethod:Alert :NoDataAvailableee];
        return;
    }
    tblData.tag=13;
    [self tableLoad:tblData.tag];
}

#pragma mark- ------ Cover Letter Introduction Letter -------------

-(void)getLetterTemplateAndTermsOfService
{
    arrayLetterTemplateMaster = [NSMutableArray new];
    arrCoverLetter = [NSMutableArray new];
    arrIntroductionLetter = [NSMutableArray new];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictMasters=[userDefaults valueForKey:@"MasterSalesAutomation"];
    arrayLetterTemplateMaster = [[dictMasters valueForKey:@"LetterTemplateMaster"] mutableCopy];
    arrTermsOfService = [[dictMasters valueForKey:@"TermsOfServiceMaster"] mutableCopy];
    
    
    for(NSDictionary *dict in arrayLetterTemplateMaster)
    {
        if([[dict valueForKey:@"LetterTemplateType"] isEqualToString:@"CoverLetter"])
        {
            [arrCoverLetter addObject:dict];
        }
        else if ([[dict valueForKey:@"LetterTemplateType"] isEqualToString:@"Introduction"])
        {
            [arrIntroductionLetter addObject:dict];
        }
    }
    
}

-(void)fetchLetterTemplateFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrLetterTemplate = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    if(arrLetterTemplate.count>0)
    {
        matchesDiscount = [arrLetterTemplate firstObject];
        
        if([[matchesDiscount valueForKey:@"coverLetterSysName"] length]>0)
        {
            // [_btnCoverLetter setTitle:[matchesDiscount valueForKey:@"coverLetterSysName"] forState:UIControlStateNormal];
            for(NSDictionary *dict in arrCoverLetter)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"coverLetterSysName"]])
                {
                    dictCoverLetter = dict;
                    [_btnCoverLetter setTitle:[NSString stringWithFormat:@"%@",[dictCoverLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
                    break;
                }
            }
        }
        else
        {
            [_btnCoverLetter setTitle:@"Select Cover Letter" forState:UIControlStateNormal];
        }
        
        if([[matchesDiscount valueForKey:@"introSysName"] length]>0)
        {
            //            [_btnIntroductionLetter setTitle:[matchesDiscount valueForKey:@"introSysName"] forState:UIControlStateNormal];
            
            if([[matchesDiscount valueForKey:@"introContent"] length]>0)
            {
                // _txtViewIntroductionLetter.text = [self convertHTML:[matchesDiscount valueForKey:@"introContent"]];
                _txtViewIntroductionLetter.attributedText = [self getAttributedString:[matchesDiscount valueForKey:@"introContent"]];
            }
            
            for(NSDictionary *dict in arrIntroductionLetter)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"introSysName"]])
                {
                    dictIntroLetter = dict;
                    [_btnIntroductionLetter setTitle:[NSString stringWithFormat:@"%@",[dictIntroLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
                    
                    break;
                }
            }
        }
        else
        {
            [_btnIntroductionLetter setTitle:@"Select Introduction Letter" forState:UIControlStateNormal];
        }
        
        if([[matchesDiscount valueForKey:@"termsOfServiceSysName"] length]>0)
        {
            // [_btnTermsOfServices setTitle:[matchesDiscount valueForKey:@"termsOfServiceSysName"] forState:UIControlStateNormal];
            if([[matchesDiscount valueForKey:@"termsOfServiceContent"] length]>0)
            {
                _txtViewTermsServices.attributedText = [self getAttributedString:[matchesDiscount valueForKey:@"termsOfServiceContent"]];
            }
            
            for(NSDictionary *dict in arrTermsOfService)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"termsOfServiceSysName"]])//
                {
                    dictTermsOfService = dict;
                    [_btnTermsOfServices setTitle:[NSString stringWithFormat:@"%@",[dictTermsOfService valueForKey:@"Title"]] forState:UIControlStateNormal];
                }
            }
        }
        else
        {
            [_btnTermsOfServices setTitle:@"Select Terms Of Services" forState:UIControlStateNormal];
        }
        
        /*  if([[matchesDiscount valueForKey:@"isAgreementValidFor"] isEqualToString:@"true"]||[[matchesDiscount valueForKey:@"isAgreementValidFor"] intValue] == 1)//IsAgreementValidFor
         {
         _txtDays.hidden=NO;
         _lblDays.hidden=NO;
         [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
         strIsAgreementValidFor = @"true";
         _txtDays.text = [matchesDiscount valueForKey:@"validFor"];*?
         }
         else
         {
         [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
         _txtDays.hidden=YES;
         _lblDays.hidden=YES;
         strIsAgreementValidFor = @"false";
         }
         
         if([[matchesDiscount valueForKey:@"isInitialTaxApplicable"] isEqualToString:@"true"]||[[matchesDiscount valueForKey:@"isInitialTaxApplicable"]intValue] == 1)
         {
         [_btnTaxApplicableInitialPrice setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
         }
         else
         {
         [_btnTaxApplicableInitialPrice setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
         }
         
         if([[matchesDiscount valueForKey:@"isMaintTaxApplicable"] isEqualToString:@"true"] || [[matchesDiscount valueForKey:@"isMaintTaxApplicable"] intValue]==1)
         {
         [_btnTaxApplicableMaintPrice setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
         }
         else
         {
         [_btnTaxApplicableMaintPrice setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
         }*/
    }
}

-(void)saveCommercialLetterTemplate:(NSDictionary*)dictTempCoverLetter introLetter:(NSDictionary*)dictTempIntroLetter andTermsConditions:(NSDictionary*)dictTempTerms
{
    entityLeadCommercialDetailExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    
    LeadCommercialDetailExtDc *objLeadDetails = [[LeadCommercialDetailExtDc alloc]initWithEntity:entityLeadCommercialDetailExtDc insertIntoManagedObjectContext:context];
    
    objLeadDetails.leadId = strLeadId;
    objLeadDetails.leadCommercialDetailId = @"";
    objLeadDetails.companyKey = strCompanyKey;
    objLeadDetails.userName = strUserName;
    
    if(dictTempCoverLetter.count>0)
    {
        objLeadDetails.coverLetterSysName = [NSString stringWithFormat:@"%@",[dictTempCoverLetter valueForKey:@"SysName"]];
    }
    else
    {
        objLeadDetails.coverLetterSysName = @"";
    }
    
    if(dictTempIntroLetter.count>0)
    {
        objLeadDetails.introSysName = [NSString stringWithFormat:@"%@",[dictTempIntroLetter valueForKey:@"SysName"]];
        objLeadDetails.introContent = [NSString stringWithFormat:@"%@",[dictTempIntroLetter valueForKey:@"TemplateContent"]];
    }
    else
    {
        objLeadDetails.introSysName = @"";
        objLeadDetails.introContent = @"";
    }
    
    if(dictTempTerms.count>0)
    {
        objLeadDetails.termsOfServiceSysName = [NSString stringWithFormat:@"%@",[dictTempTerms valueForKey:@"SysName"]];
        objLeadDetails.termsOfServiceContent = [NSString stringWithFormat:@"%@",[self convertHTML:[dictTempTerms valueForKey:@"Description"]]];
    }
    else
    {
        objLeadDetails.termsOfServiceSysName = @"";
        objLeadDetails.termsOfServiceContent = @"";
    }
    
    /* if([strIsAgreementValidFor isEqualToString:@"true"])
     {
     objLeadDetails.isAgreementValidFor = @"true";
     objLeadDetails.validFor = [NSString stringWithFormat:@"%@",_txtDays.text];
     }
     else
     {
     objLeadDetails.isAgreementValidFor = @"false";
     objLeadDetails.validFor = @"";
     }*/
    
    objLeadDetails.isInitialTaxApplicable = [NSString stringWithFormat:@"%@",isTaxApplicableInitialPrice];
    
    objLeadDetails.isMaintTaxApplicable = [NSString stringWithFormat:@"%@",isTaxApplicableMaintPrice];
    
    NSError *error1;
    [context save:&error1];
}

-(void)getMultiTerms
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrMultiTerms=[dictMasters valueForKey:@"MultipleGeneralTermsConditions"];
    arrMultipleTermsCondtions = [NSMutableArray new];
    arrSelectedMultipleCondition=[[NSMutableArray alloc]init];
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
        
    }
    if ([arrMultiTerms isKindOfClass:[NSArray class]])
    {
        for(NSDictionary *dict in arrMultiTerms)
        {
            if([[dict valueForKey:@"IsActive"] boolValue]==true || [[dict valueForKey:@"IsActive"] intValue]==1)
            {
                [arrMultipleTermsCondtions addObject:dict];
            }
            //TEMP 22 oCT
            if(([[dict valueForKey:@"IsActive"] boolValue]==true || [[dict valueForKey:@"IsActive"] intValue]==1)&&([[dict valueForKey:@"IsDefault"] boolValue]==true || [[dict valueForKey:@"IsDefault"] intValue]==1))
            {
                if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
                    
                {
                    
                }
                else
                {
                    [arrSelectedMultipleCondition addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]]];
                    [arrSelectedTermsConditionsDescriptions addObject:dict];
                }
            }
        }
        // arrMultipleTermsCondtions=(NSMutableArray*)arrMultiTerms;
    }
    
    
}
-(void)getMultiTermsContionDetails
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrMultiTerms=[dictMasters valueForKey:@"MultipleGeneralTermsConditions"];
    NSMutableArray *arrTitle,*arrId;
    arrTitle=[[NSMutableArray alloc]init];
    arrId=[[NSMutableArray alloc]init];
    for(int i=0;i<arrMultiTerms.count;i++)
    {
        NSDictionary *dict=[arrMultiTerms objectAtIndex:i];
        [arrTitle addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsTitle"]]];
        [arrId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]]];
    }
    dictTitleFromTermId=[NSDictionary dictionaryWithObjects:arrTitle forKeys:arrId];
}
-(void)saveMultiTermsCondition
{
    
    NSMutableArray *arrMultiTermsSave;
    arrMultiTermsSave=[[NSMutableArray alloc]init];
    for (int i=0; i<arrMultipleTermsCondtions.count; i++)
    {
        NSDictionary *dict=[arrMultipleTermsCondtions objectAtIndex:i];
        NSString *strTemrsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
        for (int j=0; j<arrSelectedMultipleCondition.count; j++)
        {
            if ([strTemrsId isEqualToString:[arrSelectedMultipleCondition objectAtIndex:j]])
            {
                [arrMultiTermsSave addObject:dict];
                break;
            }
        }
    }
    
    
    if (arrMultiTermsSave.count>0)
    {
        [self deleteMultipleTermsFromCoreDataClarkPestSalesInfo];
    }
    
    for (int i=0; i<arrMultiTermsSave.count; i++)
    {
        NSDictionary *dict=[arrMultiTermsSave objectAtIndex:i];
        
        entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
        
        LeadCommercialTermsExtDc *objLeadCommercialTermsExtDcc = [[LeadCommercialTermsExtDc alloc]initWithEntity:entityLeadCommercialTermsExtDc insertIntoManagedObjectContext:context];
        objLeadCommercialTermsExtDcc.leadCommercialTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
        objLeadCommercialTermsExtDcc.leadId=strLeadId;
        objLeadCommercialTermsExtDcc.branchSysName=[global strEmpBranchSysName];
        objLeadCommercialTermsExtDcc.termsnConditions=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsConditions"]];
        objLeadCommercialTermsExtDcc.userName=strUserName;
        objLeadCommercialTermsExtDcc.companyKey=strCompanyKey;
        NSError *error1;
        [context save:&error1];
    }
    
}
-(void)fetchMultiTermsFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTermsExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrMultiTermsDetail;
    arrMultiTermsDetail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    [self getMultiTerms];
    if(arrMultiTermsDetail.count==0)
    {
        //  [self getMultiTerms];
        
    }
    else
    {
        NSMutableArray *arrayTemp = [NSMutableArray new];
        for(NSManagedObject *initialInfo in arrMultiTermsDetail)
        {
            NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
            NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
            [arrayTemp addObject:dict];
        }
        //arrMultipleTermsCondtions=arrayTemp;
        
        arrSelectedMultipleCondition=[[NSMutableArray alloc]init];
        for (int i=0; i<arrMultipleTermsCondtions.count; i++)
        {
            NSDictionary *dict=[arrMultipleTermsCondtions objectAtIndex:i];
            
            for(NSDictionary *dictTemp in arrayTemp)
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"leadCommercialTermsId"]]] || [[self convertHTML:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsConditions"]]] isEqualToString:[self convertHTML:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"termsnConditions"]]]])
                {
                    [arrSelectedMultipleCondition addObject:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"leadCommercialTermsId"]]];
                }
            }
            
            
        }
        
        // get saved terms conditoins
        for (int i=0; i<arrMultipleTermsCondtions.count; i++)
        {
            NSDictionary *dict=[arrMultipleTermsCondtions objectAtIndex:i];
            
            for(NSDictionary*dictLocal in arrayTemp)
            {
                //if([[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"leadCommercialTermsId"]] isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]]])
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"leadCommercialTermsId"]]] || [[self convertHTML:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsConditions"]]] isEqualToString:[self convertHTML:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"termsnConditions"]]]])//leadCommercialTermsId
                {
                    [arrSelectedTermsConditionsDescriptions addObject:dict];
                }
            }
        }
        
        //        NSArray *arrUniqueMultipleCondition = [[NSSet setWithArray:arrSelectedMultipleCondition] allObjects];
        //        arrSelectedMultipleCondition=[[NSMutableArray alloc]init];
        //        [arrSelectedMultipleCondition addObjectsFromArray:arrUniqueMultipleCondition];
        
        
        
    }
    NSMutableArray *arrayTitleName = [NSMutableArray new];
    for(NSDictionary *dict in arrSelectedTermsConditionsDescriptions)
    {
        [arrayTitleName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsTitle"]]];
    }
    
    if(arrayTitleName.count>0)
    {
        NSString *joinedComponents = [arrayTitleName componentsJoinedByString:@","];
        // [_btnTermsConditions setTitle:joinedComponents forState:UIControlStateNormal];
    }
}
-(void)deleteMultipleTermsFromCoreDataClarkPestSalesInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    for (NSManagedObject * data in Data)
    {
        
        [context deleteObject:data];
        
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)saveNewAgreementDetails
{
    [self deleteCommercialData];
    
    if([strIsAgreementValidFor isEqualToString:@"true"])
    {
        /*  if(_txtDays.text.length==0)
         {
         [global AlertMethod:@"Alert!" :@"Please enter days"];
         [_txtDays becomeFirstResponder];
         return;
         }
         else
         {
         [self saveCommercialLetterTemplate:dictCoverLetter introLetter:dictIntroLetter andTermsConditions:dictTermsOfService];
         }*/
        [self saveCommercialLetterTemplate:dictCoverLetter introLetter:dictIntroLetter andTermsConditions:dictTermsOfService];
    }
    else
    {
        [self saveCommercialLetterTemplate:dictCoverLetter introLetter:dictIntroLetter andTermsConditions:dictTermsOfService];
    }
    // [self saveMultiTermsCondition];
    [self saveSalesMarketingContent];
}






-(void)setHtmlContentOnEditingTemp{
    
    NSUserDefaults *defsHtml = [NSUserDefaults standardUserDefaults];
    
    NSString* strHTML = [defsHtml valueForKey:@"HtmlContent"];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [strHTML dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSFontAttributeName:[UIFont systemFontOfSize:80],NSForegroundColorAttributeName:[UIColor redColor] }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    // NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[strHTML dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    
    _txtViewIntroductionLetter.attributedText = attributedString;
    _txtViewIntroductionLetter.font=[UIFont systemFontOfSize:20];
    NSLog(@"Html Code = = = =%@",strHTML);
}

-(NSString*)getHTMLstringFromAttributeStringTemp:(NSAttributedString*)strAttribute
{
    NSDictionary *documentAttributes = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
    NSData *htmlData = [strAttribute dataFromRange:NSMakeRange(0, strAttribute.length) documentAttributes:documentAttributes error:NULL];
    //Convert the NSData into HTML String with UTF-8 Encoding
    NSString *htmlString = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    return htmlString;
}

#pragma mark- --------- Sales Marketing Content --------------
-(void)getMarketingContent
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrTempMarketingContent=[dictMasters valueForKey:@"SalesMarketingContentMaster"];
    
    
    arrMarketingContent = [[NSMutableArray alloc]init];
    arrSelectedMarketingContent=[[NSMutableArray alloc]init];
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
        
    }
    if ([arrTempMarketingContent isKindOfClass:[NSArray class]])
    {
        for(NSDictionary *dict in arrTempMarketingContent)
        {
            if([[dict valueForKey:@"IsActive"] boolValue]==true || [[dict valueForKey:@"IsActive"] intValue]==1)
            {
                [arrMarketingContent addObject:dict];
            }
            else
            {
                [arrMarketingContent addObject:dict];
            }
            
        }
    }
}

-(void)saveSalesMarketingContent
{
    NSMutableArray *arrMultiTermsSave;
    arrMultiTermsSave=[[NSMutableArray alloc]init];
    for (int i=0; i<arrMarketingContent.count; i++)
    {
        NSDictionary *dict=[arrMarketingContent objectAtIndex:i];
        NSString *strTemrsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        for (int j=0; j<arrSelectedMarketingContent.count; j++)
        {
            if ([strTemrsId isEqualToString:[arrSelectedMarketingContent objectAtIndex:j]])
            {
                [arrMultiTermsSave addObject:dict];
                break;
            }
        }
    }
    
    
    if (arrMultiTermsSave.count>0)
    {
        [self deleteSalesMarketingContentClarkPestSalesInfo];
    }
    
    for (int i=0; i<arrMultiTermsSave.count; i++)
    {
        NSDictionary *dict=[arrMultiTermsSave objectAtIndex:i];
        
        entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
        
        LeadMarketingContentExtDc *objLeadMarketingContentExtDc = [[LeadMarketingContentExtDc alloc]initWithEntity:entityLeadMarketingContentExtDc insertIntoManagedObjectContext:context];
        objLeadMarketingContentExtDc.contentSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        
        objLeadMarketingContentExtDc.leadId=strLeadId;
        objLeadMarketingContentExtDc.userName=strUserName;
        objLeadMarketingContentExtDc.companyKey=strCompanyKey;
        NSError *error1;
        [context save:&error1];
    }
    
}
-(void)deleteSalesMarketingContentClarkPestSalesInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    for (NSManagedObject * data in Data)
    {
        
        [context deleteObject:data];
        
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)fetchSalesMarketingContentClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadMarketingContentExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrMultiMarketingContentDetail;
    arrMultiMarketingContentDetail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    [self getMarketingContent];
    
    if(arrMultiMarketingContentDetail.count==0)
    {
    }
    else
    {
        NSMutableArray *arrayTemp = [NSMutableArray new];
        
        for(NSManagedObject *initialInfo in arrMultiMarketingContentDetail)
        {
            NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
            NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
            [arrayTemp addObject:dict];
        }
        
        for (int i=0; i<arrMarketingContent.count; i++)
        {
            NSDictionary *dict=[arrMarketingContent objectAtIndex:i];
            
            for(NSDictionary *dictTemp in arrayTemp)
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"contentSysName"]]])
                {
                    [arrSelectedMarketingContent addObject:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"contentSysName"]]];
                }
            }
        }
    }
    NSMutableArray *arrayTitleName = [[NSMutableArray alloc]init];
    for(NSString *strContetnTitle in arrSelectedMarketingContent)
    {
        [arrayTitleName addObject:[NSString stringWithFormat:@"%@",[dictSalesContentTitleFromSysName valueForKey:strContetnTitle]]];
    }
    
    if(arrayTitleName.count>0)
    {
        NSString *joinedComponents = [arrayTitleName componentsJoinedByString:@","];
        [_btnMarketingContent setTitle:joinedComponents forState:UIControlStateNormal];
    }
}

-(void)heightHide
{
    //    _const_ViewAgreement_H.constant=0;
    //    _const_ViewPreferredMonth_H.constant=0;
    //    _const_AddNotes_H.constant=0;
    //    _constAdditionNots_H.constant=0;
    //    _const_ViewTermsOfService_H.constant=0;
    //    _const_ViewAppliedCredit_H.constant=0;
    //    _const_ViewCouponCredit_H.constant=0;
}


-(void)goToAgreement
{
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPad" bundle:nil];
    ClarkPestSalesAgreementProposaliPad *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"ClarkPestSalesAgreementProposaliPad"];
    objSalesAutomationAgreementProposal.strFromSummary=@"FromSummary";
    [self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:YES];
}
-(void)deleteCommercialData
{
    //Delete LeadCommercialDetailExtDc Data
    
    entityLeadCommercialDetailExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    
    NSFetchRequest *allDataLeadCommercialDetailExtDc = [[NSFetchRequest alloc] init];
    [allDataLeadCommercialDetailExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context]];
    
    NSPredicate *predicateLeadCommercialDetailExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    
    [allDataLeadCommercialDetailExtDc setPredicate:predicateLeadCommercialDetailExtDc];
    [allDataLeadCommercialDetailExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorLeadCommercialDetailExtDc= nil;
    NSArray * DataLeadCommercialDetailExtDc = [context executeFetchRequest:allDataLeadCommercialDetailExtDc error:&errorLeadCommercialDetailExtDc];
    //error handling goes here
    for (NSManagedObject * data in DataLeadCommercialDetailExtDc) {
        [context deleteObject:data];
    }
    NSError *saveErrorLeadCommercialDetailExtDc = nil;
    [context save:&saveErrorLeadCommercialDetailExtDc];
    
    //......................................................
    
    //Delete LeadMarketingContentExtDc Data
    
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    
    NSFetchRequest *allDataLeadMarketingContentExtDc = [[NSFetchRequest alloc] init];
    [allDataLeadMarketingContentExtDc setEntity:[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context]];
    
    NSPredicate *predicateLeadMarketingContentExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    
    [allDataLeadMarketingContentExtDc setPredicate:predicateLeadMarketingContentExtDc];
    [allDataLeadMarketingContentExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorLeadMarketingContentExtDc= nil;
    NSArray * DataLeadMarketingContentExtDc = [context executeFetchRequest:allDataLeadMarketingContentExtDc error:&errorLeadMarketingContentExtDc];
    //error handling goes here
    for (NSManagedObject * data in DataLeadMarketingContentExtDc) {
        [context deleteObject:data];
    }
    NSError *saveErrorLeadMarketingContentExtDc = nil;
    [context save:&saveErrorLeadMarketingContentExtDc];
    
    //......................................................
}
//Nilind 24 Oct
-(void)buttonClickedAddTargetImages:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    
    NSDictionary *dict = [arrSavedTargetDetail objectAtIndex:sender.tag];
    NSString *strTargetId= [NSString stringWithFormat:@"%@",[dict valueForKey:@"targetSysName"]];//targetSysName leadCommercialTargetId
    //UIButton *button=(UIButton *) sender;
    
    // NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    //  ClarkPestSelectServiceTableViewCell *tappedCell = (ClarkPestSelectServiceTableViewCell *)[_tblTarget cellForRowAtIndexPath:indexpath];
    
    NSLog(@"Add Target Images");
    
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"PestiPhone" bundle:nil];
    GlobalImageVC *objGlobalImageVC=[storyBoard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objGlobalImageVC.strHeaderTitle = @"Target";
    objGlobalImageVC.strWoId = strLeadId;
    objGlobalImageVC.strWoLeadId = strLeadId;
    objGlobalImageVC.strModuleType = @"Target";
    objGlobalImageVC.strTargetId = strTargetId;
    //MobileTargetId MobileTargetImageId leadCommercialTargetId
    
    NSMutableArray *arrKey, *arrValue;
    arrKey = [[NSMutableArray alloc]init];
    arrValue = [[NSMutableArray alloc]init];
    NSString *strMobileTargetId, *strMobileTargetImageId, *strLeadCommercialTargetId;
    
    strMobileTargetId = [NSString stringWithFormat:@"%@",[dict valueForKey:@"mobileTargetId"]];
    strMobileTargetImageId = [global getReferenceNumber];//[NSString stringWithFormat:@"%@",[dict valueForKey:@"mobileTargetImageId"]];
    strLeadCommercialTargetId = [NSString stringWithFormat:@"%@",[dict valueForKey:@"leadCommercialTargetId"]];
    [arrKey addObject:@"mobileTargetId"];
    [arrKey addObject:@"mobileTargetImageId"];
    [arrKey addObject:@"leadCommercialTargetId"];
    
    [arrValue addObject:strMobileTargetId];
    [arrValue addObject:strMobileTargetImageId];
    [arrValue addObject:strLeadCommercialTargetId];
    
    NSDictionary *dictTarget = [NSDictionary dictionaryWithObjects:arrValue forKeys:arrKey];
    objGlobalImageVC.dictTarget = dictTarget;
    
    
    if ([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strLeadStatusGlobal caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
    {
        
        objGlobalImageVC.strWoStatus = @"Complete";
        
    }
    else
    {
        objGlobalImageVC.strWoStatus = @"InComplete";
        
    }
    [self.navigationController presentViewController:objGlobalImageVC animated:NO completion:nil];
    
    /*
     
     
     let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
     let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageVC") as? GlobalImageVC
     testController?.strHeaderTitle = strType as NSString
     testController?.strWoId = strWoId
     testController?.strModuleType = "ServicePestFlow"
     if objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Complete" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Completed" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "Reset" ||  objWorkorderDetail.value(forKey: "workorderStatus") as? String == "reset"{
     testController?.strWoStatus = "Completed"
     }else{
     testController?.strWoStatus = "InComplete"
     }
             testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
     */
    
}
-(void)endEditing
{
    [self.view endEditing:YES];
}
-(void)goToGlobalmage:(NSString *)strType
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"PestiPhone" bundle:nil];
    GlobalImageVC *objGlobalImageVC=[storyBoard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objGlobalImageVC.strHeaderTitle = strType;
    objGlobalImageVC.strWoId = strLeadId;
    objGlobalImageVC.strWoLeadId = strLeadId;
    objGlobalImageVC.strModuleType = @"SalesFlow";
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        
        objGlobalImageVC.strWoStatus = @"Complete";
        
    }
    else
    {
        objGlobalImageVC.strWoStatus = @"InComplete";
        
    }
    [self.navigationController presentViewController:objGlobalImageVC animated:NO completion:nil];
    
}
-(void)attachImage
{
    [self endEditing];
   // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* addImage = [UIAlertAction actionWithTitle:@"Add Images" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToGlobalmage:@"Before"];
        
    }];
    
    UIAlertAction* addDocument = [UIAlertAction actionWithTitle:@"Add Documents" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToUploadDocument];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addImage];
    [alert addAction:addDocument];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
  
}
-(void)goToUploadDocument
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    UploadDocumentSales *objUploadDocumentSales=[storyBoard instantiateViewControllerWithIdentifier:@"UploadDocumentSales"];
    objUploadDocumentSales.strWoId = strLeadId;

    [self.navigationController presentViewController:objUploadDocumentSales animated:NO completion:nil];
    
}
-(void)goToAppointment
{
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Appointment" bundle:nil];
        AppointmentVC *objAppointmentView=[storyBoard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objAppointmentView animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];AppointmentView*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
#pragma mark: ------------- Email & Service History -------------

-(void)goToEmailHistory
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSString *strLeadNo = [defs valueForKey:@"leadNumber"];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"DashBoard" bundle:nil];
    EmailListNew_iPhoneVC *objEmailListNew_iPhoneVC=[storyBoard instantiateViewControllerWithIdentifier:@"EmailListNew_iPhoneVC"];
    objEmailListNew_iPhoneVC.refNo = strLeadNo;
    [self.navigationController pushViewController:objEmailListNew_iPhoneVC animated:NO];
    //[self.navigationController presentViewController:objEmailListNew_iPhoneVC animated:YES completion:nil];
}
-(void)goToServiceHistory
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strTypeOfService=@"sales";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
}
-(void)goToHistory
{
    [self endEditing];
   // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* addServiceHistory = [UIAlertAction actionWithTitle:@"Service History" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToServiceHistory];
        
    }];
    
    UIAlertAction* addEmailHistory = [UIAlertAction actionWithTitle:@"Email History" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToEmailHistory];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addServiceHistory];
    [alert addAction:addEmailHistory];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
  
}
- (IBAction)actionOnAddService:(id)sender
{
    [self addAction];
}
-(CGFloat)getAttributedLabelHeight:(NSString *)str
{
    CGFloat textWidth = [UIScreen mainScreen].bounds.size.width - 125;//420;
    
    //CGFloat textHght = [self findHeightForText:str havingWidth: textWidth andFont:[UIFont systemFontOfSize:16.0]].height;
    
    UILabel *lbl = [[UILabel alloc]init];
    lbl.frame = CGRectMake(0, 0, textWidth, 21);
    
    
    NSAttributedString *attributedStringHTML = [[NSAttributedString alloc]
                                            initWithData: [str dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    lbl.attributedText = attributedStringHTML;
    
    
    return [self getLabelHeight:lbl];
}
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;

    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    return size.height;
}
@end

