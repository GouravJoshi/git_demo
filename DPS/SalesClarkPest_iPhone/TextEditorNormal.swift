//
//  TextEditorNormal.swift
//  DPS
//
//  Created by Akshay Hastekar on 17/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
protocol TextEditorDelegate: class
{
    func didReceiveEditorText(strText : String)
    func didReceiveEditorTextWithObject(strText : String, object: NSManagedObject)
}
class TextEditorNormal: UIViewController
{

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var txtView: UITextView!
    
    @objc var strHtml = ""
    @objc var strfrom = String()
    @objc var strLeadId = String()
    @objc var strServiceId = String()
    var matchesObject = NSManagedObject()
    var delegate: TextEditorDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        txtView.text = strHtml

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)

    }
    
    @IBAction func actionOnSave(_ sender: Any)
    {
         
        if strfrom == "SalesCommercialTarget"
        {
            updateTargetDescription(strDescription: txtView.text)
        }
        else if strfrom == "SalesCommercialScope"
        {
            updateScopeDescription(strDescription: txtView.text)
        }
        else if strfrom == "SalesCommercialService"
        {
            updateStandardServiceDescription(strDescription: txtView.text)
        }
        else if strfrom == "SalesInternalNotesStandard"
        {
            delegate?.didReceiveEditorText(strText: txtView.text)
        }
        else if strfrom == "SalesInternalNotesNonStandard"
        {
            delegate?.didReceiveEditorText(strText: txtView.text)
        }
        else if strfrom == "SalesFollowUp"
        {
            delegate?.didReceiveEditorTextWithObject(strText: txtView.text, object: matchesObject)
        }
        else if strfrom == "EditCommericalScope"{
            delegate?.didReceiveEditorText(strText: txtView.text)
        }
        else if strfrom == "EditCommericalRenewalDescription"{
            UpdateRenewalServiceDescriptionFromCommerical(strText: txtView.text)
        }
        else
        {
  
        }
        strfrom = ""
        dismiss(animated: false, completion: nil)

    }
    
    func UpdateRenewalServiceDescriptionFromCommerical(strText: String){
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = [
            "renewalDescription"
        ]

        arrOfValues = [
            strText
            ]
        
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@","\(matchesObject.value(forKey: "leadId") ?? "")","\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
    }
    
    func updateStandardServiceDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("serviceDescription")
        arrOfValues.add(strDescription)
        
        arrOfKeys.add("isChangeServiceDesc")
        arrOfValues.add("true")
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@", strLeadId, strServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }
    func updateTargetDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("targetDescription")
        arrOfValues.add(strDescription)
        
        arrOfKeys.add("isChangedTargetDesc")
        arrOfValues.add("true")
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId == %@ && targetSysName == %@", strLeadId, strServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }
    func updateScopeDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("scopeDescription")
        arrOfValues.add(strDescription)
        
        arrOfKeys.add("isChangedScopeDesc")
        arrOfValues.add("true")
        
        
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@ && scopeSysName == %@", strLeadId, strServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }

}
