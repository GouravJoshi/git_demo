//
//  ClarkPestSalesAgreementProposaliPhone.m
//  DPS changes changes changes changes 
//
//  Created by Akshay Hastekar on 24/10/18.
//  Copyright © 2018 Saavan. All rights reserved. Changes
//

#import "ClarkPestConfigureProposaliPhone.h"

#import "SalesAutomationAgreementProposaliPad.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "AppDelegate.h"
#import "MaintenanceServiceTableCell.h"
#import "TableNonStandardCell.h"
#import "SendMailViewController.h"
#import "SignViewController.h"
#import "PaymentInfo.h"
#import "InitialSetUp.h"
#import "RecordAudioView.h"
#import "AppointmentView.h"
#import "SalesAutomationServiceSummary.h"
#import "GlobalSyncViewController.h"
#import "GenerateWorkOrder.h"
#import "AllImportsViewController.h"
#import "CreditCardIntegration.h"
#import <CoreLocation/CoreLocation.h>
#import "AgreementCheckTableViewCell.h"
#import "SelectServiceCouponDiscountTableViewCell.h"
#import "ElectronicAuthorization_iPhone.h"
#import "ClarkPestTableViewCellNonStandard.h"
#import "DPS-Swift.h"

@class WebService;

@interface ClarkPestConfigureProposaliPhone ()

@end

@implementation ClarkPestConfigureProposaliPhone
{
    Global *global;
    UIImage *imageTemp;
    NSDictionary *ResponseDict,*dictForAppoint;
    NSMutableArray *arrayOfButtonsMain;
    UIButton *btnTemp;
    UILabel *lblTemp,*lblBorder,*lblBorder2;
    NSArray *arrOfLeads;
    NSDictionary *dictSalesLeadDetail;
    NSMutableArray * arrStanInitialPrice,*arrStanMaintenancePrice,*arrStanServiceName,*arrStanFrequencyName,*arrStanFinalInitialPrice,*arrStanFinalMaintPrice;
    NSMutableArray * arrNonStanInitialPrice,*arrNonStanServiceName;
    double stantotalInitial,stanTotalMaintenance;
    double nonStanInitial;
    NSString *strLeadId;
    BOOL chkServiceProposal,chkChequeClick,isPreSetSignGlobal;
    // Service Proposal
    NSMutableArray * arrStanInitialPriceServiceProposal,*arrStanMaintenancePriceServiceProposal,*arrStanServiceNameServiceProposal,*arrStanFrequencyNameServiceProposal;
    NSMutableArray * arrNonStanInitialPriceServiceProposal,*arrNonStanServiceNameServiceProposal;
    
    //For Payment Info
    
    NSString *strPaymentMode,*strAmount,*strChequeNo,*strLicenseNo,*strExpirationDate,*strCustomerSignature,*strSalesSignature,*strGlobalAudio,*strIsPresetWO;
    
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    NSString *strDate;
    
    //........................
    
    //NILIND 28 Sept
    
    NSString *signCustomer,*signInspector,*strPaymentType,*strTax;
    //.........................
    
    //Nilinf 1 Oct
    
    NSMutableArray *arrTerms;
    //..................
    
    //Nilind 3 Oct
    
    NSMutableArray *arrSoldTerms;
    NSMutableArray *arrSoldServiceSysname;
    
    UIView *MainViewForm,*ViewFormSections,*viewForm,*viewSection;
    
    //Nilind 2 Nov
    NSDictionary *dictServiceName;
    BOOL chkStatus;
    UIView *viewComplete;
    NSString *strCompanyKey,*strEmployeeNo,*strUserName;
    
    //Nilind 27 Dec
    NSMutableArray *arrSoldServiceCount;
    NSString *strStatus;
    
    //Nilind 05 Jan
    BOOL chkIAgree,chkCustomerNotPresent;
    NSString *status;
    NSMutableArray *arrNonStanSoldCount;
    NSMutableArray *arrStanUnSoldCount,*arrNonStanUnSoldCount;
    UIView *viewBackGroundTerms;
    BOOL isEditedInSalesAuto;
    
    //Nilind 04 May
    
    NSMutableArray *arrOneTime,*arrNotOneTime,*arrPackageNameId;
    NSDictionary *dictPackageName,*dictQuantityStatus;
    //End
    NSString *strFinalTaxabelAmount,*strFinalTaxAmount,*strFinalTotalPrice;
    
    //CheckFrontImage
    
    BOOL isCheckFrontImage,isFromBeforeImage;
    int btnTagCheckImage;
    NSMutableArray *arrOFImagesName,*arrOfCheckBackImage;
    
    
    //Nilind 07 Jun ImageCaption
    
    BOOL chkForDuplicateImageSave;
    
    NSMutableArray *arrNoImage;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    NSMutableArray *arrOfImageCaption,*arrOfImageDescription,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph;
    NSMutableArray *arrImagePath,*arrGraphImage;
    NSString *strLeadStatusGlobal,*strServiceUrlMain;
    NSDictionary *dictFreqNameFromSysname;
    NSMutableArray *arrBillingFreq;
    
    NSDictionary *dictBundleNameFromId;
    NSMutableArray *arrBundleId;
    
    NSMutableArray *arrStanDiscountPrice,*arrNonStanDiscountPrice;
    
    //Bundle Code
    NSArray *arrMatchesBundle;
    NSArray *arrBundleRow;
    NSMutableArray *arrInitialPriceBundle,*arrDiscountPerBundle,*arrDiscountBundle,*arrMaintenancePriceBundle,*arrFrequencyNameBundle,*arrServiceNameBundle,*arrStanIsSoldBundle,*arrSysNameBundle,*arrUnitBundle,*arrFinalInitialPriceBundle,*arrFinalMaintPriceBundle,*arrPackageNameIdBundle,*arrBillingFreqSysNameBundle;
    NSMutableArray *arrTempBundelDetailId,*arrTempBundleServiceSysName;
    double totalInitialBundle,totalMaintBundle;
    NSString *strSoldStatusBundle;
    NSString *strBundleIdForTextField;
    NSMutableArray *arrStanInitialPriceAllServices,*arrStanMaintPriceAllServices,*arrStanFinalInitialPriceAllServices,*arrStanFinalMaintPriceAllServices,*arrStanDiscountAllServices,*arrStanServiceNameAllServices;
    
    
    // Nilind 23 Nov
    
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    
    NSDictionary *dictAgreementNameFromSysName,*dictTermsForService;
    
    NSArray *arrayAgreementCheckList;
    
    BOOL chkForLost;
    NSString *strStageSysName;
    NSString* strIsServiceActive;
    NSString *strStageStatus;
    NSMutableArray *arrPreferredMonths;
    
    BOOL isPreferredMonths;
    
    NSMutableArray *arrAdditionalParamterDcs,*arrUnit,*arrAddParamInital,*arrAddParaMaint,*arrFreqSysName,*arrFreqSysNameProposal,*arrFreqSysNameBundle,*arrBillingFreqSysNameNonStan;
    NSDictionary *dictYearlyOccurFromFreqSysName;
    NSString *strLeadNumber;
    
    NSMutableArray *arrDiscountCoupon,*arrDiscountCredit;
    NSDictionary *dictDiscountNameFromSysName,*dictDiscountCreditNameFromSysName;
    
    
    UITableView *tblData;
    NSDictionary *dictForCreditDiscount;
    NSString *strAppliedDiscountServiceId;
    
    float appliedDiscountInitial;
    float appliedDiscountMaint;
    BOOL chkForSaveCoupon;
    
    NSString *strIsFormFilled,*strIsCompleteLead;
    NSMutableArray *arrDiscountCreditInitial;
    NSMutableArray *arrDiscountCreditMaint;
    
    //New Change 31 May
    NSMutableArray *arrForTaxCoupon,*arrForTaxCredit;
    NSDictionary *dictTaxStatus,*dictCommercialStatus,*dictResidentialStatus;
    NSString *strIsServiceAddrTaxExempt,*strServiceAddressSubType;
    NSMutableArray *arrStanAllSoldServiceStandardId;
    
    //07 June
    
    NSString *strTaxableAmountMaint,*strTaxAmountMaint;
    NSString *strAccountNoGlobal;
    
    NSDictionary *dictDiscountUsageFromSysName,*dictDiscountUsageFromCode;
    NSArray *arrAppliedCreditDetail;
    BOOL chkProposal;
    
    
    //Saavan Chnages on 9th july 2018
    float globalAmountInitialPrice,globalAmountMaintenancePrice;
    NSMutableArray *arrNonStandardTermsCondition;
    
    
    //Clark Pest Changes
    NSMutableArray *arrSelectedMultipleCondition;
    
    NSMutableArray *arrTermsOfService;
    NSMutableArray *arrValueForTermsOfServices;
    
    NSArray *arrSavedServiceDetail,*arrSavedScopeDetail,*arrSavedTargetDetail,*arrSavedInitialPriceDetail,*arrSavedMaintPriceDetail;
    float subTotalInitialPrice,subTotalInitialPriceCopy;
    float subTotalMaintPrice,subTotalMaintPriceCopy;
    NSMutableArray *arrayLetterTemplateMaster,*arrCoverLetter,*arrIntroductionLetter,*arrMultipleTermsCondtions;
    NSDictionary *dictCoverLetter,*dictIntroLetter,*dictTermsOfService;
    NSString *strIsAgreementValidFor;
    
    NSDictionary *dictTitleFromTermId;
    NSString *isTaxApplicableInitialPrice,*isTaxApplicableMaintPrice;
    NSMutableArray *arrSelectedTermsConditionsDescriptions;
    BOOL isTermsOfService;
    BOOL isCreditCouponValueGreaterThanInitialPrice;
    BOOL isCreditValueGreaterThanMaintPrice;
    
    NSMutableArray *arrMarketingContent,*arrSelectedMarketingContent;
    NSDictionary *dictSalesContentTitleFromSysName,*dictSalesMarketingDescFromSysname,*dictCoverLetterContentFromSysName;
    NSString *strFlowType;
    
    UIWebView *webView1, *webView2 ,*webView3, *webView4;
    NSMutableString *strmutableTextCoverLetter,*strMutableTextIntroLetter,*strMutableTextMarketingContent,*strMutableTextTermsOfService;
    
    
    NSString *strCustomerCompanyProfileImage, *strCompanyLogoPath,*strBranchLogoImagePath,*strCompanyAddress,*strAccountManagerName,*strAccountManagerEmail,*strAccountManagerPrimaryPhone,*strBillingPocName,*strEmployeeLicenseNo;
    //28 Sept
    
    NSArray *arrSavedNonStanServiceDetail;
    
    BOOL chkBtnCheckBoxNonStan,chkBtnCheckBoxStan;
    float subtotalInitialPriceClarkPest, subTotalMaintPriceClarkPest;
    
    NSArray *arrSoldCount, *arrSoldCountNonStan;
    
    //14 July 2020
    
    NSMutableArray *arrInitialPriceCommercial, *arrMaintPriceCommercial;
    float totalInitialCommercialTax,totalMaintCommercialTax;
    NSString *strTechnicianName, *strCustomerName;
}


@synthesize strSummarySendPropsal;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    strCustomerName = @"";

    arrSelectedMultipleCondition=[[NSMutableArray alloc]init];
    arrSelectedTermsConditionsDescriptions = [NSMutableArray new];
    
    arrSelectedMarketingContent=[[NSMutableArray alloc]init];
    arrMarketingContent=[[NSMutableArray alloc]init];
    
    [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    
    globalAmountInitialPrice=0.0;
    globalAmountMaintenancePrice=0.0;
    
    [_btnIAgree setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.dataSource=self;
    tblData.delegate=self;
    appliedDiscountInitial=0;
    appliedDiscountMaint=0;
    chkForSaveCoupon=NO;
    NSUserDefaults *defsAudio=[NSUserDefaults standardUserDefaults];
    [defsAudio setBool:NO forKey:@"yesAudio"];
    [defsAudio synchronize];
    
    [self setPreferredImageInitial];
    isEditedInSalesAuto=NO;
    chkForLost=NO;
    arrPreferredMonths=[[NSMutableArray alloc]init];
    //  [_scrollViewAgreement setScrollsToTop:NO];
    //Nilind 05 Jan
    chkCustomerNotPresent=NO;
    // _txtPaidAmountPriceInforamtion.layer.cornerRadius=5.0;
    //............
    _txtPaidAmountPriceInforamtion.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _txtPaidAmountPriceInforamtion.layer.borderWidth=1.0;
    _txtPaidAmountPriceInforamtion.layer.cornerRadius=5.0;
    
    strStatus=@"abcdefg";
    //Nilind 16 Nov
    
    [self serviceName];
    [self serviceTaxableStatus];
    // [self getMultiTerms];
    [self getMultiTermsContionDetails];
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"leadStatusSales"]];
    if ([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strLeadStatusGlobal caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame)
    {
        
        //strLeadStatusGlobal=@"Complete";
        
    }
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    isPreferredMonths=[[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsPreferredMonth"]]boolValue];
    isTermsOfService=[[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsTermsOfService"]]boolValue];
    
    //Nilind 03 Oct
    
    strCompanyLogoPath=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.LogoImagePath"]];
    strBranchLogoImagePath=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeBranchLogoImagePath"]];
    strEmployeeLicenseNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"LicenceNo"]];
    strTechnicianName  = [NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    //For Company Address
    NSString *strCompanyAdd1,*strCompanyAdd2,*strCompanyCountry,*strCompnayCity,*strCompanyState,*strCompnayZipCode;
    strCompanyAdd1 = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine1"]];
    strCompanyAdd2 = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine2"]];
    strCompanyCountry = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CountryName"]];
    strCompnayCity = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CityName"]];
    strCompanyState = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.StateName"]];
    strCompnayZipCode = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ZipCode"]];
    
    
    if (strCompanyAdd1.length>0)
    {
        strCompanyAddress = strCompanyAdd1;
    }
    if (strCompanyAdd2.length>0)
    {
        strCompanyAddress = [NSString stringWithFormat:@"%@, %@",strCompanyAddress,strCompanyAdd2];;
    }
    if (strCompnayCity.length>0)
    {
        strCompanyAddress = [NSString stringWithFormat:@"%@, %@",strCompanyAddress,strCompnayCity];;
    }
    if (strCompanyState.length>0)
    {
        strCompanyAddress = [NSString stringWithFormat:@"%@, %@",strCompanyAddress,strCompanyState];;
    }
    if (strCompnayZipCode.length>0)
    {
        strCompanyAddress = [NSString stringWithFormat:@"%@, %@",strCompanyAddress,strCompnayZipCode];;
    }
    //End
    
    if(isTermsOfService)
    {
        _const_ViewTermsOfService_H.constant=305;
    }
    else
    {
        _const_ViewTermsOfService_H.constant=0;
        
    }
    
    strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strIsServiceActive=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.IsActive"]];
    
    strServiceUrlMain =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    
    
    //.....................
    
    //Nilind 27 Dec
    _btnInitialSetup.hidden=YES;
    _btnCancel.hidden=YES;
    
    // Nilind 04 Sept Check Image
    arrOFImagesName=[[NSMutableArray alloc] init];
    arrOfCheckBackImage=[[NSMutableArray alloc]init];
    btnTagCheckImage=1000;
    
    
    //End
    
    //.............
    
    viewComplete=[[UIView alloc]init];
    _lblServiceProposal.hidden=YES;
    _txtChequeValue.hidden=YES;
    _txtLicenseValue.hidden=YES;
    _txtExpirationDate.hidden=YES;
    _btnServiceProposal.enabled=YES;
    _btnServiceAgreement.enabled=YES;
    
    //NIlind
    
    arrTerms=[[NSMutableArray alloc]init];
    arrSoldTerms=[[NSMutableArray alloc]init];
    arrSoldServiceSysname=[[NSMutableArray alloc]init];
    arrayOfButtonsMain=[[NSMutableArray alloc]init];
    arrSoldServiceCount=[[NSMutableArray alloc]init];
    //...............
    
    _viewInspection.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewInspection.layer.borderWidth=1.0;
    
    
    _viewMaintainanceService.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewMaintainanceService.layer.borderWidth=1.0;
    
    _tblMaintenance.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblMaintenance.layer.borderWidth=1.0;
    
    _tblNonStandardService.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblNonStandardService.layer.borderWidth=1.0;
    
    _viewNonStandardService.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewNonStandardService.layer.borderWidth=1.0;
    
    _viewPersonalInfo.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewPersonalInfo.layer.borderWidth=1.0;
    
    _viewAdditionalNotes.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewAdditionalNotes.layer.borderWidth=1.0;
    
    _viewCreditCard.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewCreditCard.layer.borderWidth=1.0;
    
    _viewTermsConditions.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewTermsConditions.layer.borderWidth=1.0;
    
    _viewAdditionalNotes.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewAdditionalNotes.layer.borderWidth=1.0;
    
    _viewPriceInformation.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewPriceInformation.layer.borderWidth=1.0;
    
    _viewAgreementCheck.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewAgreementCheck.layer.borderWidth=1.0;
    
    _viewPreferredMonth.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewPreferredMonth.layer.borderWidth=1.0;
    
    
    _viewForCouponCredit.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewForCouponCredit.layer.borderWidth=1.0;
    
    _viewNewPriceInfo.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewNewPriceInfo.layer.borderWidth=1.0;
    
    _viewForCouponCredit.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewForCouponCredit.layer.borderWidth=1.0;
    
    _viewNewPriceInfo.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewNewPriceInfo.layer.borderWidth=1.0;
    
    _viewForAppliedCredit.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewForAppliedCredit.layer.borderWidth=1.0;
    
    
    _viewTermsOfService.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewTermsOfService.layer.borderWidth=1.0;
    
    _viewForSalesMarketingContent.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewForSalesMarketingContent.layer.borderWidth=1.0;
    
    //arrStanServiceNameServiceProposal=[[NSMutableArray alloc]init];
    
    _txtViewAdditionalNotes.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _txtViewAdditionalNotes.layer.borderWidth=1.0;
    
    
    _viewInternalNotes.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewInternalNotes.layer.borderWidth=1.0;
    
    _txtViewInternalNotes.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _txtViewInternalNotes.layer.borderWidth=1.0;
    
    // Payment Info
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    strPaymentMode=@"";
    strCustomerSignature=@"";
    strSalesSignature=@"";
    strAmount=@"";
    strChequeNo=@"";
    strDate=@"";
    strLicenseNo=@"";
    //................
    
    chkServiceProposal=NO;
    chkChequeClick=NO;
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    //strTax =[defsLead valueForKey:@"taxValue"];
    
    _lblNameAcount.text=[defsLead valueForKey:@"lblName"];
    _lblNameTitle.text=[defsLead valueForKey:@"nameTitle"];
    _lblNameAcount.font=[UIFont boldSystemFontOfSize:14];
    _lblNameTitle.font=[UIFont boldSystemFontOfSize:14];
    
    _lblNameAcount.textAlignment=NSTextAlignmentCenter;
    
    
    // _viewCreditCard.hidden=YES;
    // Do any additional setup after loading the view.
    global = [[Global alloc] init];
    [self getClockStatus];
    
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 150+50, 50);
    lblTemp.frame=CGRectMake(0, 0, 150+50, 50);
    
    
    [self salesFetch];
    arrOfLeads=[[NSMutableArray alloc]init];
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Service Summary",@"Proposal", nil];
    }
    else
    {
        arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Service Summary",@"Agreement", nil];
    }
    
    arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Agreement", nil];
    
    if ([strFlowType isEqualToString:@"Commercial"])
    {
        arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Configure Proposal",@"Agreement", nil];
    }
    
    CGFloat scrollWidth=arrOfLeads.count*110;
    [_scrollViewLeads setFrame:CGRectMake(0,_viewTop.frame.origin.y+_viewTop.frame.size.height,[UIScreen mainScreen].bounds.size.width,_scrollViewLeads.frame.size.height)];
    [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,60)];
    
    _scrollViewLeads.showsHorizontalScrollIndicator = NO;
    lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-2, 100*5+20, 2)];
    
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-3, 100*5+20, 2);
    }
    lblBorder.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height, _scrollViewLeads.frame.size.width, 2)];
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
    
    [_scrollViewLeads addSubview:lblBorder2];
    [_scrollViewLeads addSubview:lblBorder];
    _scrollViewLeads.backgroundColor=[UIColor whiteColor];
    [_scrollViewLeads addSubview:btnTemp];
    [_scrollViewLeads addSubview:lblTemp];
    [self addButtons];
    
#pragma mark- SALES LEAD DETAIL FETCHING
    [self fetchForPaymentInfo];
    
    [self salesFetch];
    [self fetchMultiTermsFromCoreDataClarkPest];
    [self fetchSalesMarketingContentClarkPest];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
    }
    else
    {
        // [self creditDetailBackground];
    }
    
    NSLog(@"%@",dictSalesLeadDetail);
    _lblAccountName.text=[matches valueForKey:@"accountNo"];
    _lblCustomerName.text=[matches valueForKey:@"customerName"];
    _lblDateValue.text=[NSString stringWithFormat:@"%@",[global ChangeDateToLocalDateOther:[NSString stringWithFormat:@"%@",[matches valueForKey:@"scheduleStartDate"]]]];
    
    if([NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceAddress2"]].length>0)
    {
        _lblServiceAddressValue.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matches valueForKey:@"servicesAddress1"],[matches valueForKey:@"serviceAddress2"],[matches valueForKey:@"serviceCity"],[matches valueForKey:@"serviceState"],[matches valueForKey:@"serviceZipcode"]];
    }
    else
    {
        _lblServiceAddressValue.text=[NSString stringWithFormat:@"%@, %@,%@, %@",[matches valueForKey:@"servicesAddress1"],[matches valueForKey:@"serviceCity"],[matches valueForKey:@"serviceState"],[matches valueForKey:@"serviceZipcode"]];
    }
    //End
    
    if([NSString stringWithFormat:@"%@",[matches valueForKey:@"billingAddress2"]].length>0)
    {
        _lblBillingAddressValue.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matches valueForKey:@"billingAddress1"],[matches valueForKey:@"billingAddress2"],[matches valueForKey:@"billingCity"],[matches valueForKey:@"billingState"],[matches valueForKey:@"billingZipcode"]];
        
    }
    else
    {
        _lblBillingAddressValue.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[matches valueForKey:@"billingAddress1"],[matches valueForKey:@"billingCity"],[matches valueForKey:@"billingState"],[matches valueForKey:@"billingZipcode"]];
        
    }
    
    _lblPrimaryPhoneValue.text=[matches valueForKey:@"primaryPhone"];
    _lblSecondarPhoneValue.text=[matches valueForKey:@"secondaryPhone"];
    _lblCellNo.text=[matches valueForKey:@"cellNo"];
    
    _lblEmailIdValue.text=[matches valueForKey:@"primaryEmail"];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    _lblInspectorValue.text=[defs valueForKey:@"inspectorName"];
    [defs setObject:_lblCustomerName.text forKey:@"customerName"];
    [defs synchronize];
    
    //Nilind 28 Sept
    
    if ([strPaymentMode isEqualToString: @"Cash"])
    {
        [_imgViewCash setImage:[UIImage imageNamed:@"redio_button_2.png"]];
        _cnstrntViewPriceInfo_H.constant=480-176+45;//650-176;
        _cnstrntViewCreditCard_H.constant=0;
        
    }
    else if ([strPaymentMode isEqualToString:@"Check"])
    {
        [_imgViewCheck setImage:[UIImage imageNamed:@"redio_button_2.png"]];
        _cnstrntViewPriceInfo_H.constant=480+45;
        _cnstrntViewCreditCard_H.constant=176;
        _txtChequeValue.text=strChequeNo;
        _txtLicenseValue.text=strLicenseNo;
        _txtExpirationDate.text=[self ChangeDateToLocalDateTime:strExpirationDate];
        _txtChequeValue.hidden=NO;
        _txtLicenseValue.hidden=NO;
        _txtExpirationDate.hidden=NO;
    }
    
    else if ([strPaymentMode isEqualToString:@"CreditCard"])
    {
        [_imgViewCreditCard setImage:[UIImage imageNamed:@"redio_button_2.png"]];
        _cnstrntViewPriceInfo_H.constant=480-176+45;
        _cnstrntViewCreditCard_H.constant=0;
    }
    else if ([strPaymentMode isEqualToString:@"AutoChargeCustomer"])
    {
        [_imgViewAutoChangeCustomer setImage:[UIImage imageNamed:@"redio_button_2.png"]];
        _cnstrntViewPriceInfo_H.constant=480-176+45;
        _cnstrntViewCreditCard_H.constant=0;
        
        _txtPaidAmountPriceInforamtion.hidden=YES;
    }
    else if ([strPaymentMode isEqualToString:@"CollectattimeofScheduling"])
    {
        [_imgViewCollectAtTimeOfScheduling setImage:[UIImage imageNamed:@"redio_button_2.png"]];
        _cnstrntViewPriceInfo_H.constant=480-176+45;
        _cnstrntViewCreditCard_H.constant=0;
        
        _txtPaidAmountPriceInforamtion.hidden=NO;
    }
    else if ([strPaymentMode isEqualToString:@"Invoice"])
    {
        [_imgViewInvoice setImage:[UIImage imageNamed:@"redio_button_2.png"]];
        _cnstrntViewPriceInfo_H.constant=480-176+45;
        _cnstrntViewCreditCard_H.constant=0;
        
        _txtPaidAmountPriceInforamtion.hidden=YES;
    }
    else
    {
        _cnstrntViewPriceInfo_H.constant=480-176+45;
        _cnstrntViewCreditCard_H.constant=0;
        
    }
    //...........................................................
    
#pragma mark- Change 23 Aug
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
    }
    else
    {
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            
        {
            [self fetchAgreementCheckListSalesOnComplete];
        }
        else
        {
            [self getAgreementListFromMaster];
            
        }
    }
    //For Standard  Service Agreement Fetch
    [self fetchFromCoreDataStandard];
    
    // For NonStandard  Service Agreement Fetch
    [self fetchFromCoreDataNonStandard];
    
    [self generateWorkOrder];
    
    if (arrNonStanInitialPrice.count==0)
    {
        // _cnstrntNonStandardTbl_H.constant=0;
        //_cnstrntViewNonStandard_H.constant=200;
    }
    
    //Chque
    
    // _cnstrntViewCreditCard_H.constant=0;
    // _cnstrntViewPriceInfo_H.constant=620-100;
    
    //...................................................
    
    [_tblMaintenance reloadData];
    [_tblNonStandardService reloadData];
    
    //For Standard  Service Proposal Fetch
    
    //  [self subtotalStandard];
    _scrollViewLeads.contentOffset = CGPointMake(80,0);
    if([UIScreen mainScreen].bounds.size.height==480 || [UIScreen mainScreen].bounds.size.height==568 )
    {
        _scrollViewLeads.contentOffset = CGPointMake(50,0);
    }
    
    //Nilind 3 Oct
    
    
    [self fetchSoldServiceStandardDetail];
    
    
    [self setDynamicData];
    [_btnServiceProposal setBackgroundColor:[UIColor lightGrayColor]];
    [_btnServiceAgreement setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        
        _btnServiceProposal.enabled=NO;
        _btnServiceAgreement.enabled=NO;
        
        [_lblServiceProposal setHidden:NO];
        [_lblServiceProposal setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
        [_scrollViewAgreement setContentOffset:CGPointMake(0,0)];
        chkServiceProposal=YES;
        _viewPersonalInfo.hidden=YES;
        _constPersonalInfoView_H.constant=0;
        _viewAdditionalNotes.hidden=YES;
        _viewPriceInformation.hidden=YES;
        
        _const_AddNotes_H.constant=0;
        _const_PriceTerms_H.constant=0;
        
        // Fetch for service proposal
        // [self fetchFromCoreDataStandardServiceProposal];
        // [self fetchFromCoreDataNonStandardServiceProposal];
        //Subtotal Calculation
        [self subtotalNonStandard];
        // End
        _constViewInspection_H.constant=0;
        _viewInspection.hidden=YES;
        _const_PriceTerms_H.constant=0;
        _cnstrntViewPriceInfo_H.constant=0;
        _cnstrntViewTermsCondition_H.constant=0;
        _lblInspection.hidden=YES;
        _lblInspectionLine.hidden=YES;
        _viewTermsConditions.hidden=YES;
        [_btnSaveContinue setTitle:@"Send Proposal" forState:UIControlStateNormal];
        _lblServiceProposal.textColor=[UIColor whiteColor];
        
        
        _btnIAgree.hidden=YES;
        _lblTerms.hidden=YES;
        _btnTerms.hidden=YES;
    }
    //Nilind 2 Nov
    
    NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
    chkStatus=[dfsStatus boolForKey:@"status"];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
        //Nilind 05 Jan
        _btnGlobalSync.enabled=NO;
        [_btnIAgree setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        //.................
        
        [self disableForCompleteLead];
        if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
        {
            _btnInitialSetup.hidden=YES;
            _btnCancel.hidden=YES;
        }
        else
        {
            
        }
    }
    else
    {
        _btnMarkAsLost.hidden=NO;
    }
    //Nilind 05 Jan
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        _btnInitialSetup.hidden=YES;
        _btnCancel.hidden=YES;
        
    }
    //......
    
    //.......................................................
    
    //Nilind 27 Dec
    [self fetchSoldServiceCount];
    [self fetchFromCoreDataNonStandardNew];
    if(arrSoldServiceCount.count==0)
    {
        _btnInitialSetup.hidden=YES;
        _btnCancel.hidden=YES;
        
        _btnServiceProposal.enabled=NO;
        _btnServiceAgreement.enabled=NO;
        
        [_lblServiceProposal setHidden:NO];
        [_lblServiceProposal setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
        if (arrNonStanSoldCount.count>0)
        {
            [_lblServiceProposal setHidden:YES];
            _btnServiceProposal.enabled=YES;
            _btnServiceAgreement.enabled=YES;
        }
    }
    else
    {
        [_lblServiceProposal setHidden:YES];
        _btnServiceProposal.enabled=YES;
        _btnServiceAgreement.enabled=YES;
    }
    
    //................
    //Nilind 5 Jan
    if([UIScreen mainScreen].bounds.size.height==568 || [UIScreen mainScreen].bounds.size.height==480)
    {
        _btnIAgree.titleLabel.font = [UIFont systemFontOfSize:14];
        _btnTerms.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    
    //.............
    //    else
    //    {
    //        _btnIAgree.titleLabel.font = [UIFont systemFontOfSize:12];
    //        _btnTerms.titleLabel.font = [UIFont systemFontOfSize:12];
    //    }
    [self fetchForAppliedDiscountFromCoreData];
    [self heightMaintenanceTable];
    [self heightNonStandardTable];
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        _btnServiceProposal.enabled=NO;
        _btnServiceAgreement.enabled=NO;
        
        [_lblServiceProposal setHidden:NO];
        [_lblServiceProposal setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
        [self heightProposaNonStandardTable];
        [self heightProposalMaintenanceTable];
    }
    
    //Nilind 14 sEPT
    chkForDuplicateImageSave=NO;
    arrNoImage=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImagePath=[[NSMutableArray alloc]init];
    
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    
    [self fetchImageDetailFromDataBase];
    if([strIsServiceActive isEqualToString:@"false"]||[strIsServiceActive isEqualToString:@"False"]||[strIsServiceActive isEqualToString:@"0"])
    {
        _btnInitialSetup.hidden=YES;
        _btnCancel.hidden=YES;
    }
    //_btnMarkAsLost.layer.borderWidth=1.0;
    
    // _btnMarkAsLost.layer.cornerRadius=5.0;
    
    
    //Setunder line
    
    _lblServiceAddressValue.attributedText=[self getUnderLineAttributedString:_lblServiceAddressValue.text];
    
    _lblBillingAddressValue.attributedText=[self getUnderLineAttributedString:_lblBillingAddressValue.text];
    
    _lblPrimaryPhoneValue.attributedText=[self getUnderLineAttributedString:_lblPrimaryPhoneValue.text];
    _lblSecondarPhoneValue.attributedText=[self getUnderLineAttributedString:_lblSecondarPhoneValue.text];
    _lblEmailIdValue.attributedText=[self getUnderLineAttributedString:_lblEmailIdValue.text];
    
    _lblCellNo.attributedText=[self getUnderLineAttributedString:_lblCellNo.text];
    
    _tblCoupon.rowHeight=UITableViewAutomaticDimension;
    _tblCoupon.estimatedRowHeight=80;
    _tblCredit.rowHeight=UITableViewAutomaticDimension;
    _tblCredit.estimatedRowHeight=80;
    
    _tblAppliedCredit.rowHeight=UITableViewAutomaticDimension;
    _tblAppliedCredit.estimatedRowHeight=80;
    
    BOOL shouldShowElectronicFormLink = [[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsElectronicAuthorizationForm"]]boolValue];
    
    if(shouldShowElectronicFormLink)
    {//show link
        if([strIsFormFilled isEqualToString:@"true"])
        {
            if([self methodToCheckIfElectronicFormExistsForLeadId:strLeadId]==YES)
            {
                _buttonElectronicAuthorizedForm.hidden = NO;
            }
            else
            {
                _buttonElectronicAuthorizedForm.hidden = YES;
            }
        }
        else
        {
            _buttonElectronicAuthorizedForm.hidden = NO;
        }
    }
    else
    { //hide link
        _buttonElectronicAuthorizedForm.hidden = YES;
    }
    //   _buttonElectronicAuthorizedForm.hidden = NO;
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        _buttonElectronicAuthorizedForm.hidden = YES;
    }
    
    _viewServiceDetail.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewServiceDetail.layer.borderWidth=1.0;
    
    _viewCoverLetter.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewCoverLetter.layer.borderWidth=1.0;
    
    _viewIntroductionLetter.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewIntroductionLetter.layer.borderWidth=1.0;
    
    
    _txtViewIntroductionLetter.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    _txtViewIntroductionLetter.layer.cornerRadius=5.0;
    _txtViewIntroductionLetter.placeholder = @"Enter Introdcution letter description";
    _txtViewIntroductionLetter.layer.borderWidth=1.0;
    
    
    _txtViewTermsServices.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _txtViewTermsServices.layer.borderWidth=1.0;
    _txtViewTermsServices.layer.cornerRadius=5.0;
    _txtViewTermsServices.placeholder = @"Enter description for services";
    
    
    _tblService.rowHeight=UITableViewAutomaticDimension;
    _tblService.estimatedRowHeight=80;
    
    _tblScope.rowHeight=UITableViewAutomaticDimension;
    _tblScope.estimatedRowHeight=80;
    
    _tblTarget.rowHeight=UITableViewAutomaticDimension;
    _tblTarget.estimatedRowHeight=80;
    
    if ([_btnAgreementValidFor.currentImage isEqual:[UIImage imageNamed:@"check_box_2New.png"]])
    {
        _txtDays.hidden=NO;
        _lblDays.hidden=NO;
        strIsAgreementValidFor = @"true";
    }
    else
    {
        _txtDays.hidden=YES;
        _lblDays.hidden=YES;
        strIsAgreementValidFor = @"false";
    }
    
    // Fetching Service Scope and Target From DB
    
    [self fetchFromCoreDataStandardClarkPest];
    [self fetchFromCoreDataNonStandardClarkPest];
    
    [self fetchScopeFromCoreData];
    [self fetchTargetFromCoreData];
    
    // fetch initial and maint price
    [self fetchLeadCommercialInitialInfoFromCoreData];
    [self fetchLeadCommercialMaintInfoFromCoreData];
    [self fetchForAppliedDiscountFromCoreData];
    
    [self getLetterTemplateAndTermsOfService];
    [self fetchLetterTemplateFromCoreData];
    
    _btnServiceAgreement.enabled=NO;
    _btnServiceProposal.enabled=NO;
    [_lblServiceProposal setText:@"Service Agreement"];
    [_lblServiceProposal setHidden:NO];
    [_lblServiceProposal setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    
    isTaxApplicableInitialPrice = @"false";
    isTaxApplicableMaintPrice = @"false";
    
    
    
    
    //27 June
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
        _btnIAgree.hidden=NO;
        _lblTerms.hidden=NO;
        _btnTerms.hidden=NO;
        _btnTermsConditions.hidden=YES;
    }
    _txtViewIntroductionLetter.editable=NO;
    _btnInitialSetup.hidden=YES;
    _btnInitialSetup.hidden=YES;
    _btnCancel.hidden=YES;
    
    [self hideToCofigure];
    
    // Nilind 03 Oct
    
    _viewCoverLetterWebView.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewCoverLetterWebView.layer.borderWidth=1.0;
    
    _viewIntroLetterWebView.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewIntroLetterWebView.layer.borderWidth=1.0;
    
    _viewSalesMarketingContentWebView.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewSalesMarketingContentWebView.layer.borderWidth=1.0;
    
    _viewTermsOfServiceWebView.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewTermsOfServiceWebView.layer.borderWidth=1.0;
    
    
    _viewFinalPrice.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewFinalPrice.layer.borderWidth=1.0;
    
    [self fetchLetterTemplateFromCoreDataWebView];
    [self fetchSalesMarketingContentClarkPestWebView];
    
    
    _const_ViewCoverLetter_H.constant = 0;
    _const_ViewIntroductionLetter_H.constant = 0;
    // _const_ViewTermsOfService_H.constant = 0;
    _const_SalesMarketingContent_H.constant = 0;
    _viewCoverLetter.hidden = YES;
    _viewIntroductionLetter.hidden = YES;
    _viewTermsOfService.hidden = YES;
    _viewForSalesMarketingContent.hidden = YES;
    
    _txtViewIntroductionLetter.editable = NO;
    _txtViewTermsServices.editable = NO;
    
    //_scrollViewAgreement.scrollEnabled = NO;
    //webView1.scrollView.scrollEnabled = NO;
}

- (void)viewWillLayoutSubviews{
    
    float ftbl = _tblNonStandardClarkPest.contentSize.height;
    
    if (ftbl > _const_TblNonStan_H.constant) {
        
        _const_TblNonStan_H.constant = ftbl;
        _const_Lbl_NonStandard_H.constant = 35.0;
        
    }
    
}
-(void)setPreferredImageInitial
{
    [_btnJan setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnFeb setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnMar setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnApr setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnMay setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnJun setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnJul setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnAug setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnSept setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnOct setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnNov setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
    [_btnDec setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    [self fetchImageDetailFromDataBaseForGraph];
    
    [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    
    //Nilind 04 Jan
    NSString *strInitialSetupStatus;
    [self fetchSoldServiceCount];
    strInitialSetupStatus=[self fetchLead];
    
    if ([strInitialSetupStatus isEqualToString:@"true"] )
    {
        _btnInitialSetup.hidden=YES;
        _btnCancel.hidden=YES;
    }
    else
    {
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            _btnInitialSetup.hidden=YES;
            _btnCancel.hidden=YES;
        }
        else
        {
            if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
                
            {
                if(arrSoldServiceCount.count>0)
                {
                    _btnInitialSetup.hidden=NO;
                    _btnCancel.hidden=NO;
                    
                }
            }
            else
            {
                _btnInitialSetup.hidden=YES;
                _btnCancel.hidden=YES;
            }
            if ([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
            {
                _btnInitialSetup.hidden=YES;
                _btnCancel.hidden=YES;
            }
            
        }
    }
    if ([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
    {
        _btnMarkAsLost.hidden=YES;
    }
    //................
    
    NSString *strIns,*strCust;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strIns=[defs valueForKey:@"fromInspectorSign"];
    strCust=[defs valueForKey:@"fromCustomerSign"];
    
    //    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    
    
    if ([strIns isEqualToString:@"fromInspectorSign"])
    {
        isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        NSString *strImageName;
        strImageName=[defs valueForKey:@"imagePath"];
        strSalesSignature=strImageName;
        UIImage *imageSign;
        imageSign=[self loadImage:strImageName];
        _imgViewInspectorSign.image=imageSign;
        [defs setValue:@"abc" forKey:@"fromInspectorSign"];
        [defs setValue:@"abc" forKey:@"imagePath"];
        [defs synchronize];
    }
    
    if ([strCust isEqualToString:@"fromCustomerSign"])
    {
        isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        NSString *strImageName;
        strImageName=[defs valueForKey:@"imagePath"];
        strCustomerSignature=strImageName;
        UIImage *imageSign;
        imageSign=[self loadImage:strImageName];
        _imgViewCustomerSignature.image=imageSign;
        [defs setValue:@"xyz" forKey:@"fromCustomerSign"];
        [defs setValue:@"xyz" forKey:@"imagePath"];
        [defs synchronize];
        
    }
    
    if ([_strFromSummary isEqualToString:@"FromSummary123"])
    {
        //For Customer
        [self downloadingImagess:signCustomer];
        // UIImage *imageSign;
        // imageSign=[self loadImage:signCustomer];
        if(CGSizeEqualToSize(imageTemp.size, CGSizeZero))
        {
            imageTemp=[UIImage imageNamed:@"NoImage.jpg"];
        }
        _imgViewCustomerSignature.image=imageTemp;
        
        //For Inspector
        
        
        //isPreSetSignSales
        
        
        NSUserDefaults *defsSign=[NSUserDefaults standardUserDefaults];
        
        //BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignService"];
        BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignSales"];
        
        NSString *strSignUrl=[defsSign valueForKey:@"ServiceTechSignPath"];
        //    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
        if ((isPreSetSign) && (strSignUrl.length>0) && !([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  ))
        {
            
            strSignUrl=[strSignUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            NSString *result;
            NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
            }else{
                result=strSignUrl;
            }
            
            signInspector=result;
            
            [self downloadingImagessTechnicianPreSet:strSignUrl];
            
            [_btnInspector setEnabled:NO];
            
            isPreSetSignGlobal=YES;
            
        }
        else
        {
            
            isPreSetSignGlobal=NO;
            
            [_btnInspector setEnabled:YES];
            
            if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            {
                [_btnInspector setEnabled:NO];
            }
            if (signInspector.length>0 || [signInspector isEqualToString:@""])
            {
                
                
                if ([strIsPresetWO caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsPresetWO isEqualToString:@"1"]) {
                    
                    //downloadingImagessTechnicianIfPreset
                    //[self downloadingImagessTechnicianIfPreset:signInspector];
                    [self downloadingImagessTechnicianIfPreset:strSignUrl];
                    
                    
                }
                else
                {
                    
                    [self downloadingImagess:signInspector];
                    
                    
                }
            }
        }
        
        
        // UIImage *imageSignInspector;
        // imageSignInspector=[self loadImage:signInspector];
        if(CGSizeEqualToSize(imageTemp.size, CGSizeZero))//if([imageSignInspector isEqual:nil])
        {
            imageTemp=[UIImage imageNamed:@"NoImage.jpg"];
        }
        _imgViewInspectorSign.image=imageTemp;
        
        _strFromSummary=@"sdfdsf";
        
    }
    //Nilind 2 Jan
    /*  NSString *strFromSendMail;
     strFromSendMail =[defs valueForKey:@"fromSendmail"];
     if ([strFromSendMail isEqualToString:@"fromSendmail"])
     {
     [self disableForCompleteLead];
     [defs setValue:@"qqqq" forKey:@"fromSendmail"];
     [defs synchronize];
     }*/
    //Nilind 05 Jan
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        [self disableForCompleteLead];
    }
    //............
    
    BOOL yesAudioAvailable=[defs boolForKey:@"yesAudio"];
    if (yesAudioAvailable)
    {
        isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        strGlobalAudio=[defs valueForKey:@"AudioNameService"];
        
        
    }
    if(arrNonStanUnSoldCount.count==0 && arrStanUnSoldCount.count==0)
    {
        _btnServiceAgreement.enabled=NO;
        _btnServiceProposal.enabled=NO;
        [_lblServiceProposal setText:@"Service Agreement"];
        [_lblServiceProposal setHidden:NO];
        [_lblServiceProposal setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    }
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    [defs2 setBool:YES forKey:@"fromAgreement"];
    [defs2 synchronize];
    
    
    [self getImageCollectionView];
    if([strIsServiceActive isEqualToString:@"false"]||[strIsServiceActive isEqualToString:@"False"]||[strIsServiceActive isEqualToString:@"0"])
    {
        _btnInitialSetup.hidden=YES;
        _btnCancel.hidden=YES;
    }
    _btnInitialSetup.hidden=YES;
    _btnCancel.hidden=YES;
    
    
    //27 June
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        // _btnIAgree.hidden=NO;
        //   _lblTerms.hidden=NO;
        //  _btnTerms.hidden=NO;
        _btnTermsConditions.hidden=YES;
    }
    [self hideToCofigure];
}
//============================================================================
#pragma mark- DYNAMIC LABEL AND BUTTON METHOD
//============================================================================
-(void)addButtons
{
    for(int i =0;i<arrOfLeads.count;i++)
    {
        
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+40, 0, 30, 30)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 15;
        lbl.layer.borderWidth=1.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 100, 20);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2-15, 5, 30, 30);
            lbl.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor =  [UIColor darkGrayColor];//[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 100, 20);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 30, 30);
            lbl.textColor = [UIColor darkGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
        }
        for(int j=0;j<=3;j++)
        {
            if (i==j)
            {
                lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
                lbl.textColor = [UIColor whiteColor];
                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];
        btn.titleLabel.numberOfLines=2;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollViewLeads addSubview:btn];
        
        [_scrollViewLeads addSubview:lbl];
    }
}
//-(void)addButtons
//{
//    for(int i =0;i<arrOfLeads.count;i++)
//    {
//
//        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+20, 0, 50, 50)];
//        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
//        lbl.textColor = [UIColor whiteColor];
//        lbl.layer.masksToBounds = YES;
//        lbl.layer.cornerRadius = 25;
//        lbl.layer.borderWidth=1.0;
//        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:14]];
//        lbl.textAlignment = NSTextAlignmentCenter;
//
//        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
//        if (i==0)
//        {
//            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
//            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 50, 50);
//            lbl.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
//            lbl.textColor =  [UIColor lightGrayColor];//[UIColor whiteColor];
//            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        }
//        else
//        {
//
//            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
//            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 50, 50);
//            lbl.textColor = [UIColor darkGrayColor];
//            lbl.backgroundColor=[UIColor whiteColor];
//            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
//
//
//        }
//        for(int j=0;j<=3;j++)
//        {
//            if (i==j)
//            {
//
//
//                lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
//                lbl.textColor = [UIColor whiteColor];
//                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
//            }
//        }
//
//        btnTemp.frame=btn.frame;
//        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
//
//        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
//
//
//        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
//        btn.titleLabel.numberOfLines=2;
//        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
//        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        btn.tag = i;
//        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
//        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
//        // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
//        [_scrollViewLeads addSubview:btn];
//
//        [_scrollViewLeads addSubview:lbl];
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)actionOnBack:(id)sender
{
    
    [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isFromBackServiceSummary"];//@"isFromBackAgreement"];
    [defs synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionOnCrash:(id)sender
{
    chkChequeClick=NO;
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    // _viewCreditCard.hidden=YES;
    _cnstrntViewPriceInfo_H.constant=480-176+45;
    _cnstrntViewCreditCard_H.constant=0;
    //_cnstrntViewPriceInfo_H.constant=620-100;
    
    [_imgViewCash setImage:[UIImage imageNamed:@"redio_button_2.png"]];
    [_imgViewCheck setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCreditCard setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewAutoChangeCustomer setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCollectAtTimeOfScheduling setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewInvoice setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    
    //UIButton *btn = (UIButton*)sender;
    strPaymentMode=@"Cash";
    
    _txtChequeValue.hidden=YES;
    _txtLicenseValue.hidden=YES;
    _txtExpirationDate.hidden=YES;
    _txtPaidAmountPriceInforamtion.hidden=NO;
    _lblTxtPaidAmount.hidden=NO;
    
    
    
}
- (IBAction)actionOnCheck:(id)sender
{
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    _txtPaidAmountPriceInforamtion.hidden=NO;
    _lblTxtPaidAmount.hidden=NO;
    
    chkChequeClick=YES;
    //Checque
    _viewCreditCard.hidden=NO;
    _cnstrntViewCreditCard_H.constant=176;//238
    _cnstrntViewPriceInfo_H.constant=480+45;
    // _txtExpirationDate.hidden=NO;
    [_imgViewCash setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCheck setImage:[UIImage imageNamed:@"redio_button_2.png"]];
    [_imgViewCreditCard setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewAutoChangeCustomer setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCollectAtTimeOfScheduling setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewInvoice setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    
    strPaymentMode=@"Check";
    
    _txtChequeValue.hidden=NO;
    _txtLicenseValue.hidden=NO;
    _txtExpirationDate.hidden=NO;
}
- (IBAction)actionOnAutoChangeCustomer:(id)sender
{
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    _txtPaidAmountPriceInforamtion.hidden=YES;
    
    _lblTxtPaidAmount.hidden=YES;
    //Checque
    chkChequeClick=NO;
    // _viewCreditCard.hidden=YES;
    // _cnstrntViewCreditCard_H.constant=0;
    _cnstrntViewPriceInfo_H.constant=480-176+45;
    _cnstrntViewCreditCard_H.constant=0;
    [_imgViewCash setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCheck setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCreditCard setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewAutoChangeCustomer setImage:[UIImage imageNamed:@"redio_button_2.png"]];
    [_imgViewCollectAtTimeOfScheduling setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewInvoice setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    
    strPaymentMode=@"AutoChargeCustomer";
    _txtChequeValue.hidden=YES;
    _txtLicenseValue.hidden=YES;
    _txtExpirationDate.hidden=YES;
}
- (IBAction)actionOnCollectAtTimeOfScheduling:(id)sender
{
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    //_txtPaidAmountPriceInforamtion.hidden=YES;
    
   // _lblTxtPaidAmount.hidden=YES;
    
    //Checque
    chkChequeClick=NO;
    //  _viewCreditCard.hidden=YES;
    _cnstrntViewPriceInfo_H.constant=480-176+45;
    _cnstrntViewCreditCard_H.constant=0;
    // _cnstrntViewCreditCard_H.constant=0;
    // _cnstrntViewPriceInfo_H.constant=620-100;
    [_imgViewCash setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCheck setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCreditCard setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewAutoChangeCustomer setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCollectAtTimeOfScheduling setImage:[UIImage imageNamed:@"redio_button_2.png"]];
    [_imgViewInvoice setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    
    
    strPaymentMode=@"CollectattimeofScheduling";
    
    _txtChequeValue.hidden=YES;
    _txtLicenseValue.hidden=YES;
    _txtExpirationDate.hidden=YES;
    //_txtPaidAmountPriceInforamtion.text=@"0";
    
    _txtPaidAmountPriceInforamtion.hidden=NO;
    
    _lblTxtPaidAmount.hidden=NO;
    
}
- (IBAction)actionOnCreditCard:(id)sender
{
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    _txtPaidAmountPriceInforamtion.hidden=NO;
    _lblTxtPaidAmount.hidden=NO;
    chkChequeClick=NO;
    _cnstrntViewPriceInfo_H.constant=480-176+45;
    _cnstrntViewCreditCard_H.constant=0;
    //Checque
    // _viewCreditCard.hidden=YES;
    //_cnstrntViewCreditCard_H.constant=0;  //130
    //_cnstrntViewPriceInfo_H.constant=620-100;  //650-130=520
    
    [_imgViewCash setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCheck setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCreditCard setImage:[UIImage imageNamed:@"redio_button_2.png"]];
    [_imgViewAutoChangeCustomer setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCollectAtTimeOfScheduling setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewInvoice setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    
    
    strPaymentMode=@"CreditCard";
    _txtChequeValue.hidden=YES;
    _txtLicenseValue.hidden=YES;
    _txtExpirationDate.hidden=YES;
    
}
#pragma mark- Tableview Delegate Method
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==101 || tableView.tag==102 )
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==106)
    {
        return UITableViewAutomaticDimension;
    }
    else if(tableView.tag==300)
    {
        return UITableViewAutomaticDimension;
    }
    else if(tableView.tag==301)
    {
        return UITableViewAutomaticDimension;
    }
    else if(tableView.tag==302)
    {
        
        return UITableViewAutomaticDimension;
    }
    else if(tableView.tag==305)
    {
        return UITableViewAutomaticDimension;
    }
    else
    {
        return tableView.rowHeight;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==0)
    {
        if(chkServiceProposal==YES)
        {
            return arrStanServiceNameServiceProposal.count;
        }
        else
        {
            return  arrStanInitialPrice.count;
        }
    }
    else if (tableView.tag==2)
    {
        int k=0;
        NSString *strBundleId=[arrBundleRow objectAtIndex:section];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        NSArray *arrTempBundle=[dictMasters valueForKey:@"ServiceBundles"];
        NSMutableArray *arrAllBundles;
        arrAllBundles=[[NSMutableArray alloc]init];
        for (int i=0; i<arrTempBundle.count;i++)
        {
            NSDictionary *dict=[arrTempBundle objectAtIndex:i];
            [arrAllBundles addObject:dict];
            
        }
        for (int i=0; i<arrAllBundles.count; i++)
        {
            NSDictionary *dict=[arrAllBundles objectAtIndex:i];
            if ([strBundleId isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceBundleId"]]])
            {
                NSArray *arrServiceBundle=[dict valueForKey:@"ServiceBundleDetails"];
                k=(int)arrServiceBundle.count;
                break;
            }
        }
        return k;
    }
    else if (tableView.tag==3)
    {
        return arrayAgreementCheckList.count;
    }
    else if (tableView.tag==4)
    {
        return arrDiscountCredit.count;// credit
    }
    else if (tableView.tag==101)
    {
        return arrDiscountCoupon.count;
    }
    else if (tableView.tag==102)
    {
        return arrDiscountCredit.count;
    }
    else if (tableView.tag==103)
    {
        return arrDiscountCoupon.count;
    }
    else if (tableView.tag==104)
    {
        //return arrDiscountCredit.count;
        
        arrDiscountCreditInitial=[[NSMutableArray alloc]init];
        for(int i=0; i<arrDiscountCredit.count;i++)
        {
            NSDictionary *dict=[arrDiscountCredit objectAtIndex:i];
            if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
            {
                [arrDiscountCreditInitial addObject:dict];
            }
            
        }
        
        return arrDiscountCreditInitial.count;
    }
    else if (tableView.tag==105)
    {
        
        //return arrDiscountCredit.count;
        
        
        arrDiscountCreditMaint=[[NSMutableArray alloc]init];
        for(int i=0; i<arrDiscountCredit.count;i++)
        {
            NSDictionary *dict=[arrDiscountCredit objectAtIndex:i];
            if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"true"])
            {
                [arrDiscountCreditMaint addObject:dict];
            }
            
        }
        return arrDiscountCreditMaint.count;
        
    }
    else if (tableView.tag==106)
    {
        return arrAppliedCreditDetail.count;
    }
    else if (tableView.tag==300)
    {
        return arrSavedServiceDetail.count;
    }
    else if (tableView.tag==301)
    {
        return arrSavedScopeDetail.count;
    }
    else if (tableView.tag==302)
    {
        return arrSavedTargetDetail.count;
    }
    else if (tableView.tag==305)
    {
        return arrSavedNonStanServiceDetail.count;
    }
    else if (tableView.tag==10)
    {
        return arrCoverLetter.count;
    }
    else if (tableView.tag==11)
    {
        return arrIntroductionLetter.count;
    }
    else if (tableView.tag==12)
    {
        return arrMultipleTermsCondtions.count;
    }
    else if (tableView.tag==13)
    {
        return arrTermsOfService.count;
    }
    else if (tableView.tag==14)
    {
        return arrMarketingContent.count;
    }
    else
    {
        if(chkServiceProposal==YES)
        {
            return arrNonStanInitialPriceServiceProposal.count;
            
        }
        else
        {
            return arrNonStanInitialPrice.count;
        }
        
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView.tag==4)
    {
        NSDictionary *dict=[arrDiscountCredit objectAtIndex:indexPath.row];
        [_btnSelectCredit setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]] forState:UIControlStateNormal];
        dictForCreditDiscount=dict;
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
    }
    else if (tableView.tag==10)
    {
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            dictCoverLetter = [arrCoverLetter objectAtIndex:indexPath.row];
            [_btnCoverLetter setTitle:[NSString stringWithFormat:@"%@",[dictCoverLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
            [viewBackGround removeFromSuperview];
            [tblData removeFromSuperview];
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            dictCoverLetter = nil;
            [_btnCoverLetter setTitle:@"Select Cover Letter" forState:UIControlStateNormal];
            [viewBackGround removeFromSuperview];
            [tblData removeFromSuperview];
        }
    }
    else if (tableView.tag==11)
    {
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            dictIntroLetter = [arrIntroductionLetter objectAtIndex:indexPath.row];
            
            [_btnIntroductionLetter setTitle:[NSString stringWithFormat:@"%@",[dictIntroLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
            
            _txtViewIntroductionLetter.attributedText = [self getAttributedString:[dictIntroLetter valueForKey:@"TemplateContent"]];
            
            [_txtViewIntroductionLetter resignFirstResponder];
            [viewBackGround removeFromSuperview];
            [tblData removeFromSuperview];
            _txtViewIntroductionLetter.font=[UIFont systemFontOfSize:14];
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            
            dictIntroLetter = nil;
            
            [_btnIntroductionLetter setTitle:@"Select Introduction Letter" forState:UIControlStateNormal];
            
            _txtViewIntroductionLetter.attributedText = [self getAttributedString:@""];
            
            [_txtViewIntroductionLetter resignFirstResponder];
            [viewBackGround removeFromSuperview];
            [tblData removeFromSuperview];
        }
    }
    else if(tableView.tag==12)
    {
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        
        NSDictionary *dict=[arrMultipleTermsCondtions objectAtIndex:indexPath.row];
        NSString *strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
        
        if([strTermsId isEqualToString:@"(null)"]|| [strTermsId isEqual:nil] || [strTermsId isKindOfClass:[NSNull class]])
        {
            strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadCommercialTermsId"]];
        }
        
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [arrSelectedMultipleCondition addObject:strTermsId];
            [arrSelectedTermsConditionsDescriptions addObject:dict];
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            if (arrSelectedMultipleCondition.count==0)
            {
                
            }
            else
            {
                [arrSelectedMultipleCondition removeObject:strTermsId];
                [arrSelectedTermsConditionsDescriptions removeObject:dict];
            }
        }
    }
    else if (tableView.tag==13)
    {
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            dictTermsOfService = [arrTermsOfService objectAtIndex:indexPath.row];
            
            _txtViewTermsServices.attributedText =[self getAttributedString:[NSString stringWithFormat:@"%@",[dictTermsOfService valueForKey:@"Description"]]];
            _txtViewTermsServices.font=[UIFont systemFontOfSize:14];
            [_btnTermsOfServices setTitle:[NSString stringWithFormat:@"%@",[dictTermsOfService valueForKey:@"Title"]] forState:UIControlStateNormal];
            
            [viewBackGround removeFromSuperview];
            [tblData removeFromSuperview];
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            
            dictTermsOfService = nil;
            [_btnTermsOfServices setTitle:@"Select Terms of Services" forState:UIControlStateNormal];
            [viewBackGround removeFromSuperview];
            [tblData removeFromSuperview];
        }
        
        
    }
    else if (tableView.tag==14)
    {
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            
        }
        else
        {
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            
            NSDictionary *dict=[arrMarketingContent objectAtIndex:indexPath.row];
            NSString *strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
            
            if([strTermsId isEqualToString:@"(null)"]|| [strTermsId isEqual:nil] || [strTermsId isKindOfClass:[NSNull class]])
            {
                strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"contentSysName"]];
            }
            
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                
                [arrSelectedMarketingContent addObject:strTermsId];
            }
            else
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                if (arrSelectedMarketingContent.count==0)
                {
                    
                }
                else
                {
                    [arrSelectedMarketingContent removeObject:strTermsId];
                }
            }
        }
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==0)
    {
        static NSString *identifier=@"CellMaintenance";
        MaintenanceServiceTableCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[MaintenanceServiceTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (chkServiceProposal==YES)
        {
            if(arrStanServiceNameServiceProposal.count==0)//arrNonStanInitialPriceServiceProposal
            {
                cell.lblServiceNameMaintenance.text=@"";
                cell.lblFrequencyMaintenance.text=@"";
                
                cell.lblInitialPriceMaintenance.text=@"";
                cell.lblMaintenancePriceValue.text=@"";
            }
            else
            {
                
                //Nilind 05 June
                
                // cell.lblServiceNameMaintenance.text=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceNameServiceProposal objectAtIndex:indexPath.row]]];
                
                NSString *strServiceWithPackage,*strPackage;
                strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameId objectAtIndex:indexPath.row]]];
                if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
                {
                    strPackage=@"NO Package";
                }
                NSString *strName;
                strName=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceNameServiceProposal objectAtIndex:indexPath.row]]];
                strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strName,strPackage];
                
                //For Bundle
                
                /* NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
                 if ([strBundle isEqualToString:@"0"])
                 {
                 }
                 else
                 {
                 strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
                 if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
                 {
                 strPackage=@"NO Package";
                 }
                 strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,strName];
                 
                 }*/
                
                //End
                
                cell.lblServiceNameMaintenance.text=strServiceWithPackage;
                
                [cell.lblServiceNameMaintenance sizeToFit];
                
                //End
                
                
                
                cell.lblFrequencyMaintenance.text=[arrStanFrequencyNameServiceProposal objectAtIndex:indexPath.row];
                
                NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrStanServiceNameServiceProposal objectAtIndex:indexPath.row]]];
                if ([str isEqualToString:@"0"])
                {
                    cell.lblInitialPriceMaintenance.text=[NSString stringWithFormat:@"$%@",[arrStanInitialPriceServiceProposal objectAtIndex:indexPath.row]];
                    
                    cell.lblMaintenancePriceValue.text=[NSString stringWithFormat:@"$%@",[arrStanMaintenancePriceServiceProposal objectAtIndex:indexPath.row]];
                }
                else
                {
                    cell.lblInitialPriceMaintenance.text=[NSString stringWithFormat:@"$%@",[arrStanFinalInitialPrice objectAtIndex:indexPath.row]];
                    
                    cell.lblMaintenancePriceValue.text=[NSString stringWithFormat:@"$%@",[arrStanFinalMaintPrice objectAtIndex:indexPath.row]];
                }
                
                
            }
            
            NSString *strBillingFreq;
            strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysname valueForKey:[arrBillingFreq objectAtIndex:indexPath.row]]];
            if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil] || strBillingFreq.length==0)
            {
                strBillingFreq=@"";
            }
            //Billing Price Calculation
            float totalParaInitial=0.0,totalParaMaint=0.0;
            totalParaInitial=totalParaInitial+([[arrStanInitialPriceServiceProposal objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
            totalParaInitial=totalParaInitial-[[arrStanDiscountPrice objectAtIndex:indexPath.row]floatValue];
            
            totalParaMaint=totalParaMaint+([[arrStanMaintenancePriceServiceProposal objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
            
            
            //            if (indexPath.row==0) {
            //
            //                globalAmountInitialPrice=totalParaInitial;
            //                globalAmountMaintenancePrice=totalParaMaint;
            //
            //            }
            
            NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
            strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFreqSysNameProposal objectAtIndex:indexPath.row]]];
            strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreq objectAtIndex:indexPath.row]]];
            float totalBillingFreqCharge=0.0;
            
            if ([[arrFreqSysNameProposal objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFreqSysNameProposal objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
            {
                
                totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
                // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
                strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
                
                
            }
            else
            {
                totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
                // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
                strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
                
            }
            //End Billing Freq Calculation
            
            cell.lblBillingFreq.text=strBillingFreq;
            cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%@",[arrStanDiscountPrice objectAtIndex:indexPath.row]];
            cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%@",@"00.00"];
            
        }
        else
        {
            if (arrStanServiceName.count==0)
            {
                cell.lblServiceNameMaintenance.text=@"";
                cell.lblFrequencyMaintenance.text=@"";
                
                cell.lblInitialPriceMaintenance.text=@"";
                cell.lblMaintenancePriceValue.text=@"";
            }
            else
            {
                //cell.lblServiceNameMaintenance.text=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:indexPath.row]]];//[arrStanServiceName objectAtIndex:indexPath.row];
                
                //Nilind 05 June
                
                NSString *strServiceWithPackage,*strPackage;
                strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameId objectAtIndex:indexPath.row]]];
                if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
                {
                    strPackage=@"NO Package";
                }
                NSString *strName;
                strName=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:indexPath.row]]];
                strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strName,strPackage];
                
                //For Bundle
                /*
                 NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
                 if ([strBundle isEqualToString:@"0"])
                 {
                 }
                 else
                 {
                 strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
                 if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
                 {
                 strPackage=@"NO Package";
                 }
                 strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,strName];
                 
                 }
                 */
                //End
                
                
                
                cell.lblServiceNameMaintenance.text=strServiceWithPackage;
                [cell.lblServiceNameMaintenance sizeToFit];
                //End
                
                
                cell.lblFrequencyMaintenance.text=[arrStanFrequencyName objectAtIndex:indexPath.row];
                
                NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrStanServiceName objectAtIndex:indexPath.row]]];
                if ([str isEqualToString:@"0"])
                {
                    cell.lblInitialPriceMaintenance.text=[NSString stringWithFormat:@"$%@",[arrStanInitialPrice objectAtIndex:indexPath.row]];
                    cell.lblMaintenancePriceValue.text=[NSString stringWithFormat:@"$%@",[arrStanMaintenancePrice objectAtIndex:indexPath.row]];
                }
                else
                {
                    cell.lblInitialPriceMaintenance.text=[NSString stringWithFormat:@"$%@",[arrStanFinalInitialPrice objectAtIndex:indexPath.row]];
                    cell.lblMaintenancePriceValue.text=[NSString stringWithFormat:@"$%@",[arrStanFinalMaintPrice objectAtIndex:indexPath.row]];
                }
                
            }
            NSString *strBillingFreq;
            strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysname valueForKey:[arrBillingFreq objectAtIndex:indexPath.row]]];
            if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil] || strBillingFreq.length==0)
            {
                strBillingFreq=@"";
            }
            //Billing Frequency Formula Calculation
            float totalParaInitial=0.0,totalParaMaint=0.0;
            totalParaInitial=totalParaInitial+([[arrStanInitialPrice objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
            
            if (indexPath.row==0) {
                
                globalAmountInitialPrice=totalParaInitial;
                
            }
            
            totalParaInitial=totalParaInitial-[[arrStanDiscountPrice objectAtIndex:indexPath.row]floatValue];
            
            totalParaMaint=totalParaMaint+([[arrStanMaintenancePrice objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
            
            if (indexPath.row==0) {
                
                globalAmountMaintenancePrice=totalParaMaint;
                
            }
            
            NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
            strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFreqSysName objectAtIndex:indexPath.row]]];
            strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreq objectAtIndex:indexPath.row]]];
            float totalBillingFreqCharge=0.0;
            
            if ([[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
            {
                
                totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
                // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
                strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
                
                
            }
            else
            {
                totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
                //  strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
                strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
                
            }
            //End Billing Freq Calculation
            cell.lblBillingFreq.text=strBillingFreq;
            
            
            cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%.2f",[[NSString stringWithFormat:@"%@",[arrStanDiscountPrice objectAtIndex:indexPath.row]]floatValue]];
        }
        
        return  cell;
    }
    else if (tableView.tag==2) //bundle
    {
        static NSString *identifier=@"CellMaintenance";
        MaintenanceServiceTableCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[MaintenanceServiceTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (chkServiceProposal==YES)
        {
            NSString *strBundellll=[arrBundleRow objectAtIndex:indexPath.section];
            if (strBundellll.length==0 || [strBundellll isEqualToString:@""] || [strBundellll isEqual:nil])
            {
                
            }
            else
            {
                [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:indexPath.section]];
            }
            
            
            if(arrSysNameBundle.count==0)//arrNonStanInitialPriceServiceProposal
            {
                cell.lblServiceNameMaintenance.text=@"";
                cell.lblFrequencyMaintenance.text=@"";
                
                cell.lblInitialPriceMaintenance.text=@"";
                cell.lblMaintenancePriceValue.text=@"";
            }
            else
            {
                
                NSString *strServiceWithPackage,*strPackage;
                strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameIdBundle objectAtIndex:indexPath.row]]];
                if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
                {
                    strPackage=@"NO Package";
                }
                NSString *strName;
                strName=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrSysNameBundle objectAtIndex:indexPath.row]]];
                strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strName,strPackage];
                
                //For Bundle
                
                NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
                if ([strBundle isEqualToString:@"0"])
                {
                }
                else
                {
                    strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
                    if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
                    {
                        strPackage=@"NO Package";
                    }
                    strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,strName];
                    
                }
                
                //End
                
                cell.lblServiceNameMaintenance.text=strServiceWithPackage;
                
                [cell.lblServiceNameMaintenance sizeToFit];
                
                //End
                
                
                
                cell.lblFrequencyMaintenance.text=[arrFrequencyNameBundle objectAtIndex:indexPath.row];
                
                NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]];
                if ([str isEqualToString:@"0"])
                {
                    cell.lblInitialPriceMaintenance.text=[NSString stringWithFormat:@"$%@",[arrInitialPriceBundle objectAtIndex:indexPath.row]];
                    
                    cell.lblMaintenancePriceValue.text=[NSString stringWithFormat:@"$%@",[arrMaintenancePriceBundle objectAtIndex:indexPath.row]];
                }
                else
                {
                    cell.lblInitialPriceMaintenance.text=[NSString stringWithFormat:@"$%@",[arrFinalInitialPriceBundle objectAtIndex:indexPath.row]];
                    
                    cell.lblMaintenancePriceValue.text=[NSString stringWithFormat:@"$%@",[arrFinalMaintPriceBundle objectAtIndex:indexPath.row]];
                }
                
                
            }
            
            NSString *strBillingFreq;
            strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysname valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
            if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil] || strBillingFreq.length==0)
            {
                strBillingFreq=@"";
            }
            
            //Billing Freq Calculation
            float totalParaInitial=0.0,totalParaMaint=0.0;
            totalParaInitial=totalParaInitial+([[arrInitialPriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
            
            if (indexPath.row==0) {
                
                globalAmountInitialPrice=totalParaInitial;
                
            }
            
            totalParaInitial=totalParaInitial-[[arrDiscountBundle objectAtIndex:indexPath.row]floatValue];
            
            totalParaMaint=totalParaMaint+([[arrMaintenancePriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
            
            if (indexPath.row==0) {
                
                globalAmountMaintenancePrice=totalParaMaint;
                
            }
            
            NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
            strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFreqSysNameBundle objectAtIndex:indexPath.row]]];
            strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
            float totalBillingFreqCharge=0.0;
            
            if ([[arrFreqSysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFreqSysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
            {
                
                totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
                // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
                strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
                
                
            }
            else
            {
                totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
                // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
                strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
                
            }
            //End
            cell.lblBillingFreq.text=strBillingFreq;
            //cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%@",[arrDiscountBundle objectAtIndex:indexPath.row]];
            cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%@",@"00.00"];
            
        }
        else
        {
            if (arrSysNameBundle.count==0)
            {
                cell.lblServiceNameMaintenance.text=@"";
                cell.lblFrequencyMaintenance.text=@"";
                
                cell.lblInitialPriceMaintenance.text=@"";
                cell.lblMaintenancePriceValue.text=@"";
            }
            else
            {
                //cell.lblServiceNameMaintenance.text=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:indexPath.row]]];//[arrStanServiceName objectAtIndex:indexPath.row];
                
                //Nilind 05 June
                NSString *strBundellll=[arrBundleRow objectAtIndex:indexPath.section];
                if (strBundellll.length==0 || [strBundellll isEqualToString:@""] || [strBundellll isEqual:nil])
                {
                    
                }
                else
                {
                    [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:indexPath.section]];
                }
                
                NSString *strServiceWithPackage,*strPackage;
                strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameIdBundle objectAtIndex:indexPath.row]]];
                if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
                {
                    strPackage=@"NO Package";
                }
                NSString *strName;
                strName=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrSysNameBundle objectAtIndex:indexPath.row]]];
                strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strName,strPackage];
                
                //For Bundle
                
                NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
                if ([strBundle isEqualToString:@"0"])
                {
                }
                else
                {
                    strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
                    if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
                    {
                        strPackage=@"NO Package";
                    }
                    strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,strName];
                    
                }
                
                //End
                
                
                
                cell.lblServiceNameMaintenance.text=strServiceWithPackage;
                [cell.lblServiceNameMaintenance sizeToFit];
                //End
                
                
                cell.lblFrequencyMaintenance.text=[arrFrequencyNameBundle objectAtIndex:indexPath.row];
                
                NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]];
                if ([str isEqualToString:@"0"])
                {
                    cell.lblInitialPriceMaintenance.text=[NSString stringWithFormat:@"$%@",[arrInitialPriceBundle objectAtIndex:indexPath.row]];
                    cell.lblMaintenancePriceValue.text=[NSString stringWithFormat:@"$%@",[arrMaintenancePriceBundle objectAtIndex:indexPath.row]];
                }
                else
                {
                    cell.lblInitialPriceMaintenance.text=[NSString stringWithFormat:@"$%@",[arrFinalInitialPriceBundle objectAtIndex:indexPath.row]];
                    cell.lblMaintenancePriceValue.text=[NSString stringWithFormat:@"$%@",[arrFinalMaintPriceBundle objectAtIndex:indexPath.row]];
                }
                
            }
            NSString *strBillingFreq;
            strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysname valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
            if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil] || strBillingFreq.length==0)
            {
                strBillingFreq=@"";
            }
            //Billing Freq Calculation
            float totalParaInitial=0.0,totalParaMaint=0.0;
            totalParaInitial=totalParaInitial+([[arrInitialPriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
            totalParaInitial=totalParaInitial-[[arrDiscountBundle objectAtIndex:indexPath.row]floatValue];
            
            totalParaMaint=totalParaMaint+([[arrMaintenancePriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
            
            NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
            strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFreqSysNameBundle objectAtIndex:indexPath.row]]];
            strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
            float totalBillingFreqCharge=0.0;
            
            if ([[arrFreqSysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFreqSysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
            {
                
                totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
                //  strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
                strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
                
                
            }
            else
            {
                totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
                // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
                strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
                
            }
            //End
            cell.lblBillingFreq.text=strBillingFreq;
            
            cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%.2f",[[NSString stringWithFormat:@"%@",[arrDiscountBundle objectAtIndex:indexPath.row]]floatValue]];
            
            if (indexPath.row==0) {
                
                globalAmountInitialPrice=totalParaInitial;
                
            }
            if (indexPath.row==0) {
                
                globalAmountMaintenancePrice=totalParaMaint;
                
            }
            
            
        }
        
        return  cell;
    }
    else if (tableView.tag==3) //Agreement Check
    {
        static NSString *identifier=@"AgreementCheckTableViewCell";
        AgreementCheckTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[AgreementCheckTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSManagedObject *matchAgreement=[arrayAgreementCheckList objectAtIndex:indexPath.row];
        if([[NSString stringWithFormat:@"%@",[matchAgreement valueForKey:@"isActive"]] isEqualToString:@"true"])
        {
            [cell.btnCheckBoxAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.btnCheckBoxAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            [cell.btnCheckBoxAgreement setEnabled:NO];
        }
        else
        {
            [cell.btnCheckBoxAgreement setEnabled:YES];
            
        }
        //[cell.btnCheckBoxAgreement setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        cell.btnCheckBoxAgreement.tag=indexPath.row;
        [cell.btnCheckBoxAgreement addTarget:self
                                      action:@selector(buttonClickedCheckBoxAgreementService:) forControlEvents:UIControlEventTouchDown];
        cell.lblAgreementService.text=[dictAgreementNameFromSysName valueForKey:[NSString stringWithFormat:@"%@",[matchAgreement valueForKey:@"agreementChecklistId"]]];//agreementChecklistSysName
        cell.lblAgreementService.numberOfLines=2;
        return cell;
    }
    else if (tableView.tag==4) //Credit List Check
    {
        static NSString *identifier=@"creditcoupon";
        UITableViewCell *cellCoupon=[_tblCredit dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSManagedObject *dict=[arrDiscountCredit objectAtIndex:indexPath.row];
        
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]];
        return cellCoupon;
        
    }
    else if (tableView.tag==101) //Coupon Check
    {
        static NSString *identifier=@"SelectServiceCouponDiscountTableViewCell";
        SelectServiceCouponDiscountTableViewCell *cellCoupon=[_tblCoupon dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[SelectServiceCouponDiscountTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSManagedObject *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
        
        cellCoupon.lblCouponName.text=[NSString stringWithFormat:@"%@",[dictDiscountNameFromSysName valueForKey:[dict valueForKey:@"discountSysName"]]];
        
        cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"$%.2f",[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue]];
        
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]] length]>0)
        {
            cellCoupon.lbCouponDescription.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
        }
        else
        {
            cellCoupon.lbCouponDescription.text = @" ";
        }
        
        return cellCoupon;
        
    }else if (tableView.tag==102) //Credit Check
    {
        static NSString *identifier=@"SelectServiceCreditDiscountTableViewCell";
        SelectServiceCouponDiscountTableViewCell *cellCoupon=[_tblCredit dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[SelectServiceCouponDiscountTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (cellCoupon==nil)
        {
            cellCoupon=[[SelectServiceCouponDiscountTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSManagedObject *dict=[arrDiscountCredit objectAtIndex:indexPath.row];
        
        NSString *strOblic=@"/";
        cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"%.2f %@ %.2f",[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue],strOblic,[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedMaintDiscount"]]floatValue]];
        
        // cellCoupon.lblCouponName.text=[NSString stringWithFormat:@"%@",[dictDiscountCreditNameFromSysName valueForKey:[dict valueForKey:@"discountSysName"]]];
        
        
        cellCoupon.lblCouponName.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountSysName"]];
        
        // cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]];
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]] length]>0)
        {
            cellCoupon.lbCouponDescription.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
        }
        else
        {
            cellCoupon.lbCouponDescription.text = @" ";
        }
        
        
        return cellCoupon;
        
    }
    else if (tableView.tag==103) //Coupon AT Initial Price
    {
        static NSString *identifier=@"cell103";
        UITableViewCell *cellCoupon=[_tblNewInitialPriceCouponDiscount dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSManagedObject *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
        
        NSString *strName,*strAmountCoupon;
        
        
        //        strName =[NSString stringWithFormat:@"%@",[dictDiscountNameFromSysName valueForKey:[dict valueForKey:@"discountSysName"]]];
        
        strName = [NSString stringWithFormat:@"%@",[dict valueForKey:@"discountSysName"]];
        
        strAmountCoupon=[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]];
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@ : %.2f",strName,[strAmountCoupon floatValue]];
        [cellCoupon.textLabel setFont:[UIFont systemFontOfSize:13.0]];
        
        return cellCoupon;
        
    }
    else if (tableView.tag==104) //Credit AT Initial Price
    {
        static NSString *identifier=@"cell104";
        UITableViewCell *cellCoupon=[_tblNewInitialPriceCouponDiscount dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        //NSManagedObject *dict=[arrDiscountCredit objectAtIndex:indexPath.row];
        NSManagedObject *dict=[arrDiscountCreditInitial objectAtIndex:indexPath.row];
        
        NSString *strName,*strAmountCoupon;
        
        
        strName =[NSString stringWithFormat:@"%@",[dictDiscountCreditNameFromSysName valueForKey:[dict valueForKey:@"discountSysName"]]];
        strAmountCoupon=[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]];
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@ : %.2f",strName,[strAmountCoupon floatValue]];
        [cellCoupon.textLabel setFont:[UIFont systemFontOfSize:13.0]];
        
        return cellCoupon;
        
    }
    else if (tableView.tag==105) //Credit AT Maint Price
    {
        static NSString *identifier=@"cell105";
        UITableViewCell *cellCoupon=[_tblNewInitialPriceCouponDiscount dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        // NSDictionary *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
        
        //NSManagedObject *dict=[arrDiscountCredit objectAtIndex:indexPath.row];
        NSManagedObject *dict=[arrDiscountCreditMaint objectAtIndex:indexPath.row];
        
        NSString *strName,*strAmountCoupon;
        
        
        strName =[NSString stringWithFormat:@"%@",[dictDiscountCreditNameFromSysName valueForKey:[dict valueForKey:@"discountSysName"]]];
        strAmountCoupon=[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedMaintDiscount"]];
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@ : %.2f",strName,[strAmountCoupon floatValue]];
        
        [cellCoupon.textLabel setFont:[UIFont systemFontOfSize:13.0]];
        
        
        return cellCoupon;
        
    }
    else if (tableView.tag==106) //Account Credit Detail
    {
        static NSString *identifier=@"SelectServiceAppliedCreditTableViewCell";
        SelectServiceCouponDiscountTableViewCell *cellCoupon=[_tblAppliedCredit dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[SelectServiceCouponDiscountTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        // NSDictionary *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
        
        //NSManagedObject *dict=[arrDiscountCredit objectAtIndex:indexPath.row];
        NSDictionary *dict=[arrAppliedCreditDetail objectAtIndex:indexPath.row];
        
        NSString *strName,*strAmountCoupon;
        
        strName =[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]];
        
        strAmountCoupon=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]];
        
        cellCoupon.lblCouponName.text=strName;
        
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]] isEqualToString:@"true"])
        {
            NSString *strPer=@"%";
            cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"%.2f%@",[[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]] floatValue],strPer];
        }
        else
        {
            cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"%.2f",[strAmountCoupon floatValue]];
        }
        
        if([NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountDescription"]].length>0)
        {
            cellCoupon.lbCouponDescription.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountDescription"]];
        }
        else
        {
            cellCoupon.lbCouponDescription.text = @"  ";
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==10) //Cover Letter
    {
        static NSString *identifier=@"demo";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@",[[arrCoverLetter objectAtIndex:indexPath.row] valueForKey:@"TemplateName"]];
        if(dictCoverLetter.count>0)
        {//
            if([cellCoupon.textLabel.text isEqualToString:[dictCoverLetter valueForKey:@"TemplateName"]])
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else
        {
            cellCoupon.accessoryType = UITableViewCellAccessoryNone;
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==11) //Introduction Letter
    {
        static NSString *identifier=@"demo";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@",[[arrIntroductionLetter objectAtIndex:indexPath.row] valueForKey:@"TemplateName"]];
        
        if(dictIntroLetter.count>0)
        {
            if([cellCoupon.textLabel.text isEqualToString:[dictIntroLetter valueForKey:@"TemplateName"]])
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else
        {
            cellCoupon.accessoryType = UITableViewCellAccessoryNone;
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==12) //Multipile Terms Condition
    {
        static NSString *identifier=@"demoTemrs";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSDictionary *dict=[arrMultipleTermsCondtions objectAtIndex:indexPath.row];
        NSString *strMultiTerms=[dict valueForKey:@"TermsTitle"];
        NSString *strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
        // strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadCommercialTermsId"]];
        
        if([strTermsId isEqualToString:@"(null)"]|| [strTermsId isEqual:nil] || [strTermsId isKindOfClass:[NSNull class]])
        {
            strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadCommercialTermsId"]];
            strMultiTerms=[dictTitleFromTermId valueForKey:[dict valueForKey:@"leadCommercialTermsId"]];
            
        }
        
        cellCoupon.textLabel.text=strMultiTerms;
        
        BOOL chkExist;
        chkExist=NO;
        for (int i=0; i<arrSelectedMultipleCondition.count; i++)
        {
            if ([[arrSelectedMultipleCondition objectAtIndex:i]isEqualToString:strTermsId])
            {
                chkExist=YES;
                break;
                //  [cellCoupon setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else
            {
                chkExist=NO;
                //[cellCoupon setAccessoryType:UITableViewCellAccessoryNone];
            }
        }
        if (chkExist==YES)
        {
            [cellCoupon setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cellCoupon setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==13) //Credit List Check
    {
        static NSString *identifier=@"demoTemrs service";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cellCoupon.textLabel.text=[NSString stringWithFormat:@"%@",[[arrTermsOfService objectAtIndex:indexPath.row] valueForKey:@"Title"]];
        
        if(dictTermsOfService.count>0)
        {
            if([cellCoupon.textLabel.text isEqualToString:[dictTermsOfService valueForKey:@"Title"]])
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cellCoupon.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else
        {
            cellCoupon.accessoryType = UITableViewCellAccessoryNone;
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==14) //Marketing Content
    {
        static NSString *identifier=@"MarketingContent";
        UITableViewCell *cellCoupon=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSDictionary *dict=[arrMarketingContent objectAtIndex:indexPath.row];
        NSString *strMultiTerms=[dict valueForKey:@"Title"];
        NSString *strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        // strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadCommercialTermsId"]];
        
        if([strTermsId isEqualToString:@"(null)"]|| [strTermsId isEqual:nil] || [strTermsId isKindOfClass:[NSNull class]])
        {
            strTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"contentSysName"]];
            strMultiTerms=[dictTitleFromTermId valueForKey:[dict valueForKey:@"contentSysName"]];
            
        }
        
        cellCoupon.textLabel.text=strMultiTerms;
        
        BOOL chkExist;
        chkExist=NO;
        for (int i=0; i<arrSelectedMarketingContent.count; i++)
        {
            if ([[arrSelectedMarketingContent objectAtIndex:i]isEqualToString:strTermsId])
            {
                chkExist=YES;
                break;
            }
            else
            {
                chkExist=NO;
            }
        }
        if (chkExist==YES)
        {
            [cellCoupon setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cellCoupon setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            cellCoupon.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        return cellCoupon;
        
    }
    else if (tableView.tag==300)
    {
        static NSString *identifier=@"ClarkPestSelectService";
        ClarkPestSelectServiceTableViewCell *cellService=[_tblService dequeueReusableCellWithIdentifier:identifier];
        if (cellService==nil)
        {
            cellService=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellService.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSManagedObject *matchedServiceDetail=[arrSavedServiceDetail objectAtIndex:indexPath.row];
        cellService.lblServiceName.text=[dictServiceName valueForKey:[matchedServiceDetail valueForKey:@"serviceSysName"]];
        NSString *strDesc=[matchedServiceDetail valueForKey:@"serviceDescription"];
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellService.lblDescription.text=@" ";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            //cellService.lblDescription.text=[self convertHTML:[matchedServiceDetail valueForKey:@"serviceDescription"]];
            cellService.lblDescription.attributedText = [global getAttributedString:[matchedServiceDetail valueForKey:@"serviceDescription"] WithFontStyle:[UIFont systemFontOfSize:14]];
            
        }
        cellService.lblInitialPrice.text = [NSString stringWithFormat:@"$ %@",[matchedServiceDetail valueForKey:@"initialPrice"]];
        
        cellService.lblMaintenancePrice.text = [NSString stringWithFormat:@"$ %@",[matchedServiceDetail valueForKey:@"maintenancePrice"]];
        
        cellService.lblFrequency.text = [NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"serviceFrequency"]];
        
        cellService.btnAddToAgreement.tag=indexPath.row;
        [cellService.btnAddToAgreement addTarget:self
                                          action:@selector(buttonClickedCheckBoxAgreementServiceClarkPest:) forControlEvents:UIControlEventTouchDown];
        if ([[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"true"]||[[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"1"])
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            cellService.btnAddToAgreement.enabled = NO;
        }
        else
        {
            cellService.btnAddToAgreement.enabled = YES;
            
        }
        
        
        return cellService;
        
    }
    else if (tableView.tag==301)
    {
        static NSString *identifier=@"ClarkPestSelectServiceScope";
        ClarkPestSelectServiceTableViewCell *cellScope=[_tblScope dequeueReusableCellWithIdentifier:identifier];
        if (cellScope==nil)
        {
            cellScope=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellScope.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSManagedObject *matchedScopeDetail=[arrSavedScopeDetail objectAtIndex:indexPath.row];
        cellScope.lblServiceName.text=[matchedScopeDetail valueForKey:@"title"];
        //cellScope.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        NSString *strDesc=[matchedScopeDetail valueForKey:@"scopeDescription"];
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellScope.lblDescription.text=@" ";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            // cellScope.lblDescription.text=[self convertHTML:[matchedScopeDetail valueForKey:@"scopeDescription"]];
            cellScope.lblDescription.attributedText = [global getAttributedString:[matchedScopeDetail valueForKey:@"scopeDescription"] WithFontStyle:[UIFont systemFontOfSize:14]];
            
        }
        return cellScope;
        
    }
    else if (tableView.tag==302)
    {
        static NSString *identifier=@"ClarkPestSelectServiceTarget";
        ClarkPestSelectServiceTableViewCell *cellTarget=[_tblTarget dequeueReusableCellWithIdentifier:identifier];
        if (cellTarget==nil)
        {
            cellTarget=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellTarget.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSManagedObject *matchedTargetDetail=[arrSavedTargetDetail objectAtIndex:indexPath.row];
        cellTarget.lblServiceName.text=[matchedTargetDetail valueForKey:@"name"];
        //dgdf dfg dfgd fgcellTarget.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        NSString *strDesc=[matchedTargetDetail valueForKey:@"targetDescription"];
        if(strDesc.length==0 || [strDesc isEqualToString:@" "] || [strDesc isEqual:nil])
        {
            cellTarget.lblDescription.text=@" ";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            //cellTarget.lblDescription.text=[self convertHTML:[matchedTargetDetail valueForKey:@"targetDescription"]];
            cellTarget.lblDescription.attributedText = [global getAttributedString:[matchedTargetDetail valueForKey:@"targetDescription"] WithFontStyle:[UIFont systemFontOfSize:14]];
            
        }
        NSMutableArray *arrTargetImage= [[NSMutableArray alloc]init];
        arrTargetImage = [self fetchTargetImageDetail:[matchedTargetDetail valueForKey:@"leadCommercialTargetId"] MobileTargetId:[matchedTargetDetail valueForKey:@"mobileTargetId"]];
        
        //cellTarget.const_ViewTargetImages_H.constant = arrTargetImage.count * 100;
        cellTarget.viewTargetImages.backgroundColor = [UIColor whiteColor];
        CGFloat imgY = 0.0;
        CGFloat height = 0.0;
        UIImageView *imgView = [[UIImageView alloc]init];
        UILabel *lblCaption,*lblDesc;
        lblCaption = [[UILabel alloc]init];
        
        lblDesc = [[UILabel alloc]init];
        
        for (int i= 0; i<arrTargetImage.count;i++)
        {
            NSManagedObject *matchTarget = [arrTargetImage objectAtIndex:i];
            NSString *strTargetImageName = [NSString stringWithFormat:@"%@",[matchTarget valueForKey:@"leadImagePath"]];
            
            imgView = [[UIImageView alloc]init];
            lblCaption = [[UILabel alloc]init];
            lblDesc = [[UILabel alloc]init];
            
            if (i==0)
            {
                imgView.frame = CGRectMake(5, 0, 100, 100);
                imgY = CGRectGetMaxY(imgView.frame);
                
            }
            else
            {
                imgView.frame = CGRectMake(5, imgY + 5, 100, 100);
                imgY = CGRectGetMaxY(imgView.frame);
            }
            
            lblCaption.frame = CGRectMake(CGRectGetMaxX(imgView.frame)+20, imgView.frame.origin.y, cellTarget.viewTargetImages.frame.size.width - 10, 30);
            
            lblDesc.frame = CGRectMake(CGRectGetMaxX(imgView.frame)+20, CGRectGetMaxY(lblCaption.frame) + 10 , cellTarget.viewTargetImages.frame.size.width - 10, 30);
            
            lblCaption.text = [NSString stringWithFormat:@"%@",[matchTarget valueForKey:@"leadImageCaption"]];
            lblDesc.text = [NSString stringWithFormat:@"%@",[matchTarget valueForKey:@"descriptionImageDetail"]];
            lblCaption.textColor = [UIColor blackColor];
            lblDesc.textColor = [UIColor blackColor];
            lblCaption.font = [UIFont boldSystemFontOfSize:12];
            lblDesc.font = [UIFont boldSystemFontOfSize:12];
            
            /*
             //Image Fetch from directory
             
             NSString *result;
             NSRange equalRange = [strTargetImageName rangeOfString:@"/" options:NSBackwardsSearch];
             if (equalRange.location != NSNotFound)
             {
             result = [strTargetImageName substringFromIndex:equalRange.location + equalRange.length];
             }else{
             result=strTargetImageName;
             }
             if (result.length==0) {
             NSRange equalRange = [strTargetImageName rangeOfString:@"\\" options:NSBackwardsSearch];
             if (equalRange.location != NSNotFound) {
             result = [strTargetImageName substringFromIndex:equalRange.location + equalRange.length];
             }
             }
             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
             NSString *documentsDirectory = [paths objectAtIndex:0];
             NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
             
             UIImage* image = [UIImage imageWithContentsOfFile:path];*/
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                           ^{
                UIImage* image = [self downloadTargetImage:strTargetImageName];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (image==nil) {
                        
                        imgView.image = [UIImage imageNamed:@"NoImage.jpg"];
                        
                    } else {
                        
                        imgView.image = image;
                    }
                });
            });
            /* UIImage* image = [self downloadTargetImage:strTargetImageName];
             
             if (image==nil) {
             
             imgView.image = [UIImage imageNamed:@"NoImage.jpg"];
             
             } else {
             
             imgView.image = image;
             }*/
            
            //End
            
            
            
            
            height = height + imgView.frame.size.height;
            
            [cellTarget.viewTargetImages addSubview:imgView];
            [cellTarget.viewTargetImages addSubview:lblCaption];
            [cellTarget.viewTargetImages addSubview:lblDesc];
            
        }
        cellTarget.const_ViewTargetImages_H.constant = height  + 20;
        
        // cellTarget.viewTargetImages.frame= CGRectMake(cellTarget.viewTargetImages.frame.origin.x, cellTarget.viewTargetImages.frame.origin.y, cellTarget.viewTargetImages.frame.size.width, height);
        
        
        return cellTarget;
        
    }
    else if (tableView.tag==305)
    {
        static NSString *identifier=@"ClarkPestTableViewCellNonStandard";
        ClarkPestTableViewCellNonStandard *cellService=[_tblNonStandardClarkPest dequeueReusableCellWithIdentifier:identifier];
        if (cellService==nil)
        {
            cellService=[[ClarkPestTableViewCellNonStandard alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cellService.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        NSManagedObject *matchedServiceDetail=[arrSavedNonStanServiceDetail objectAtIndex:indexPath.row];
        
        cellService.lblServiceName.text = [matchedServiceDetail valueForKey:@"serviceName"] ;
        
        //NSString *strDesc=[self convertHTML:[matchedServiceDetail valueForKey:@"serviceDescription"]];
        
        NSString *strDesc=[matchedServiceDetail valueForKey:@"serviceDescription"];
        
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil]|| [strDesc isKindOfClass:[NSNull class]] || [strDesc isEqualToString:@"<null>"]||[strDesc isEqualToString:@"(null)"]||[strDesc isEqual:NULL])
        {
            cellService.lblDescription.text=@" ";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            //cellService.lblDescription.text=strDesc;
            NSAttributedString *attributedStringHTML = [[NSAttributedString alloc]
                                                        initWithData: [[matchedServiceDetail valueForKey:@"serviceDescription"] dataUsingEncoding:NSUnicodeStringEncoding]
                                                        options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                        documentAttributes: nil
                                                        error: nil
                                                        ];
            
            cellService.lblDescription.attributedText=attributedStringHTML;
        }
        
        
        [cellService.lblDescription sizeToFit];
        
        if ([[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"true"]||[[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"1"])
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        
        cellService.lblInitialPrice.text = [NSString stringWithFormat:@"$ %@",[matchedServiceDetail valueForKey:@"initialPrice"]];
        cellService.lblMaintenancePrice.text = [NSString stringWithFormat:@"$ %@",[matchedServiceDetail valueForKey:@"maintenancePrice"]];
        
        cellService.lblFrequency.text = [NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"serviceFrequency"]];
        
        
        cellService.btnAddToAgreement.tag=indexPath.row;
        [cellService.btnAddToAgreement addTarget:self
                                          action:@selector(buttonClickedCheckBoxAgreementNonStanadrdServiceClarkPest:) forControlEvents:UIControlEventTouchDown];
        if ([[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"true"]||[[matchedServiceDetail valueForKey:@"isSold"]isEqualToString:@"1"])
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cellService.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            
        }
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            cellService.btnAddToAgreement.enabled = NO;
        }
        else
        {
            cellService.btnAddToAgreement.enabled = YES;
            
        }
        if ([[NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"isBidOnRequest"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"isBidOnRequest"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[matchedServiceDetail valueForKey:@"isBidOnRequest"]] isEqualToString:@"True"])
            
        {
            cellService.lblInitialPrice.text = @"Bid On Request";
            cellService.lblMaintenancePrice.text = @"Bid On Request";
        }
        
        
        return cellService;
        
    }
    else
    {
        static NSString *identifier=@"NonStandardCell";
        TableNonStandardCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[TableNonStandardCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (chkServiceProposal==YES)
        {
            if (arrNonStanServiceNameServiceProposal.count==0 )//arrStanServiceNameServiceProposal
            {
                cell.lblServiceName.text=@"";
                cell.lblInitialPriceValue.text=@"";
            }
            else
            {
                cell.lblServiceName.text=[arrNonStanServiceNameServiceProposal objectAtIndex:indexPath.row];
                cell.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%@",[arrNonStanInitialPriceServiceProposal objectAtIndex:indexPath.row]];
            }
            //cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%@",[arrNonStanDiscountPrice objectAtIndex:indexPath.row]];
            
            
            
            
            
            //Billing Freq Calculation
            NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysname valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
            if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
            {
                strBillingFreq=@"";
            }
            NSString *strBillingFreqYearOccurence;
            strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
            if([strBillingFreqYearOccurence isEqualToString:@"(null)"])
            {
                strBillingFreqYearOccurence=@"0";
            }
            float totalBillingFreqCharge=0.0;
            @try {
                totalBillingFreqCharge=([[arrNonStanInitialPriceServiceProposal objectAtIndex:indexPath.row]floatValue]-[[arrNonStanDiscountPrice objectAtIndex:indexPath.row]floatValue]) /[strBillingFreqYearOccurence floatValue];
                // totalBillingFreqCharge=totalBillingFreqCharge-[[arrNonStanDiscount objectAtIndex:indexPath.row]floatValue];
                
            } @catch (NSException *exception) {
                totalBillingFreqCharge=0.0;
            } @finally {
                
            }
            // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
            cell.lblValueForBillingFrequency.text=strBillingFreq;
            //End
            
            
            //cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%.2f",[[NSString stringWithFormat:@"%@",[arrNonStanDiscountPrice objectAtIndex:indexPath.row]]floatValue]];
            
            cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%@",@"00.00"];
            
            
        }
        else
        {
            if (arrNonStanInitialPrice.count==0)//arrNonStanInitialPrice
            {
                cell.lblServiceName.text=@"";
                cell.lblInitialPriceValue.text=@"";
            }
            else
            {
                cell.lblServiceName.text=[arrNonStanServiceName objectAtIndex:indexPath.row];
                cell.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%@",[arrNonStanInitialPrice objectAtIndex:indexPath.row]];
            }
            //  cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%@",[arrNonStanDiscountPrice objectAtIndex:indexPath.row]];
            
            cell.lblDiscountPrice.text=[NSString stringWithFormat:@"$%.2f",[[NSString stringWithFormat:@"%@",[arrNonStanDiscountPrice objectAtIndex:indexPath.row]]floatValue]];
            
            //Billing Freq Calculation
            NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysname valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
            if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
            {
                strBillingFreq=@"";
            }
            NSString *strBillingFreqYearOccurence;
            strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
            if([strBillingFreqYearOccurence isEqualToString:@"(null)"])
            {
                strBillingFreqYearOccurence=@"0";
            }
            float totalBillingFreqCharge=0.0;
            @try {
                totalBillingFreqCharge=([[arrNonStanInitialPrice objectAtIndex:indexPath.row]floatValue]-[[arrNonStanDiscountPrice objectAtIndex:indexPath.row]floatValue]) /[strBillingFreqYearOccurence floatValue];
                // totalBillingFreqCharge=totalBillingFreqCharge-[[arrNonStanDiscount objectAtIndex:indexPath.row]floatValue];
                
            } @catch (NSException *exception) {
                totalBillingFreqCharge=0.0;
            } @finally {
                
            }
            // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
            cell.lblValueForBillingFrequency.text=strBillingFreq;
            
            
        }
        return  cell;
    }
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==101 || tableView.tag==102)
    {
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        {
            return NO;
        }
        else
        {
            return YES;
            
        }
    }
    else
    {
        return NO;
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==101)
    {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
            [self fetchForAppliedDiscountFromCoreData];
            
            NSManagedObject *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
            //[arrDiscountCoupon removeObjectAtIndex:indexPath.row];
            
            [self deleteAppliedCouponFromCoreDataSalesInfo:dict:@"Coupon"];
            [self heightCreditCoupon];
            [self finalHeightManage];
            
        }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    if (tableView.tag==102)
    {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
            [self fetchForAppliedDiscountFromCoreData];
            
            NSManagedObject *dict=[arrDiscountCredit objectAtIndex:indexPath.row];
            //[arrDiscountCoupon removeObjectAtIndex:indexPath.row];
            
            [self deleteAppliedCouponFromCoreDataSalesInfo:dict:@"Credit"];
            [self heightCreditCoupon];
            [self finalHeightManage];
            
        }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}
-(void)serviceName
{
    NSMutableArray *name,*sysName,*arrPackageId,*arrPackageName,*qtyVal, *arrTermsService;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    arrPackageId=[[NSMutableArray alloc]init];
    arrPackageName=[[NSMutableArray alloc]init];
    qtyVal=[[NSMutableArray alloc]init];
    
    arrTermsService=[[NSMutableArray alloc]init];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [qtyVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsUnitBasedService"]]];
                [arrTermsService addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsConditions"]]];
                
                NSArray *arrPackage=[dict valueForKey:@"ServicePackageDcs"];
                for (int k=0; k<arrPackage.count; k++)
                {
                    NSDictionary *dictPackage=[arrPackage objectAtIndex:k];
                    [arrPackageId addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"ServicePackageId"]]];
                    [arrPackageName addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"PackageName"]]];
                }
                
            }
        }
    }
    dictQuantityStatus = [NSDictionary dictionaryWithObjects:qtyVal forKeys:sysName];
    dictTermsForService=[NSDictionary dictionaryWithObjects:arrTermsService forKeys:sysName];
    
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
    dictPackageName =[NSDictionary dictionaryWithObjects:arrPackageName forKeys:arrPackageId];
    NSLog(@"Package Detail %@",dictPackageName);
    NSLog(@"dictServiceName%@",dictServiceName);
    
    NSMutableArray *arrName,*arrSysName,*arrYearlyOccurence;
    arrName=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    arrYearlyOccurence=[[NSMutableArray alloc]init];
    
    NSArray *arrFreq=[dictMasters valueForKey:@"Frequencies"];
    for (int i=0; i<arrFreq.count; i++)
    {
        NSDictionary *dict=[arrFreq objectAtIndex:i];
        [arrName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]];
        [arrSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        [arrYearlyOccurence addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"YearlyOccurrence"]]];
        
    }
    
    dictFreqNameFromSysname=[NSDictionary dictionaryWithObjects:arrName forKeys:arrSysName];
    dictYearlyOccurFromFreqSysName=[NSDictionary dictionaryWithObjects:arrYearlyOccurence forKeys:arrSysName];
    
    //For Bundle
    NSArray *arrBundleData=[dictMasters valueForKey:@"ServiceBundles"];
    NSMutableArray *nameBundle,*keyBundleId;
    nameBundle=[[NSMutableArray alloc]init];
    keyBundleId=[[NSMutableArray alloc]init];
    for (int i=0; i<arrBundleData.count; i++)
    {
        NSDictionary *dict=[arrBundleData objectAtIndex:i];
        {
            [nameBundle addObject:[dict valueForKey:@"BundleName"]];
            [keyBundleId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceBundleId"]]];
        }
    }
    dictBundleNameFromId =[NSDictionary dictionaryWithObjects:nameBundle forKeys:keyBundleId];
    
    //For Agreement
    NSArray *arrAgreement=[dictMasters valueForKey:@"AgreementChecklist"];
    
    NSMutableArray *valueAgreement,*keyAgreement;
    valueAgreement=[[NSMutableArray alloc]init];
    keyAgreement=[[NSMutableArray alloc]init];
    for (int i=0; i<arrAgreement.count; i++)
    {
        NSDictionary *dict=[arrAgreement objectAtIndex:i];
        {
            [keyAgreement addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"AgreementChecklistId"]]];//AgreementChecklistId //SysName
            [valueAgreement addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]]];
        }
    }
    dictAgreementNameFromSysName =[NSDictionary dictionaryWithObjects:valueAgreement forKeys:keyAgreement];
    
    
    //For Coupon Discount
    NSArray *arrForDiscount;
    NSMutableArray *valNameDiscount,*keySysNameDiscount,*arrDiscountUsage,*arrDiscountCode;
    
    valNameDiscount=[[NSMutableArray alloc]init];
    keySysNameDiscount=[[NSMutableArray alloc]init];
    arrDiscountUsage=[[NSMutableArray alloc]init];
    arrDiscountCode=[[NSMutableArray  alloc]init];
    arrForDiscount=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
    
    if ([arrForDiscount isKindOfClass:[NSArray class]])
    {
        for (int i=0; i<arrForDiscount.count; i++)
        {
            NSDictionary *dict=[arrForDiscount objectAtIndex:i];
            {
                [valNameDiscount addObject:[dict valueForKey:@"Name"]];
                [keySysNameDiscount addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [arrDiscountUsage addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Usage"]]];
                [arrDiscountCode addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountCode"]]];
            }
        }
        dictDiscountNameFromSysName =[NSDictionary dictionaryWithObjects:valNameDiscount forKeys:keySysNameDiscount];
        dictDiscountUsageFromSysName =[NSDictionary dictionaryWithObjects:arrDiscountUsage forKeys:keySysNameDiscount];
        dictDiscountUsageFromCode =[NSDictionary dictionaryWithObjects:arrDiscountUsage forKeys:arrDiscountCode];
        
    }
    
    
    //For Credit Discount
    NSArray *arrForCreditDiscount;
    NSMutableArray *valNameCreditDiscount,*keySysNameCreditDiscount;
    valNameCreditDiscount=[[NSMutableArray alloc]init];
    keySysNameCreditDiscount=[[NSMutableArray alloc]init];
    arrForCreditDiscount=[dictMasters valueForKey:@"DiscountSetupMasterCredit"];
    
    if ([arrForCreditDiscount isKindOfClass:[NSArray class]])
    {
        for (int i=0; i<arrForCreditDiscount.count; i++)
        {
            NSDictionary *dict=[arrForCreditDiscount objectAtIndex:i];
            {
                [valNameCreditDiscount addObject:[dict valueForKey:@"Name"]];
                [keySysNameCreditDiscount addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
            }
        }
        dictDiscountCreditNameFromSysName =[NSDictionary dictionaryWithObjects:valNameCreditDiscount forKeys:keySysNameCreditDiscount];
        
    }
    
    
    //For Sales Content Title
    NSArray *arrSalesContent=[dictMasters valueForKey:@"SalesMarketingContentMaster"];
    NSMutableArray *arrKeySysNameMarketingContent,*arrValueTitleMarketingContent,*arrMarketingContentDesc;
    arrKeySysNameMarketingContent=[[NSMutableArray alloc]init];
    arrValueTitleMarketingContent=[[NSMutableArray alloc]init];
    arrMarketingContentDesc = [[NSMutableArray alloc]init];
    
    
    
    if ([arrSalesContent isKindOfClass:[NSArray class]])
    {
        for (int i=0; i<arrSalesContent.count; i++)
        {
            NSDictionary *dict=[arrSalesContent objectAtIndex:i];
            {
                [arrValueTitleMarketingContent addObject:[dict valueForKey:@"Title"]];
                [arrKeySysNameMarketingContent addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [arrMarketingContentDesc addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Description"]]];
                
            }
        }
        dictSalesContentTitleFromSysName =[NSDictionary dictionaryWithObjects:arrValueTitleMarketingContent forKeys:arrKeySysNameMarketingContent];
        dictSalesMarketingDescFromSysname =[NSDictionary dictionaryWithObjects:arrMarketingContentDesc forKeys:arrKeySysNameMarketingContent];
        
        
    }
    [self getCoverLetterContent];
    
    
}

// 31 August
#pragma mark -SalesAuto Fetch Core Data
-(void)salesFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        matches=arrAllObj[0];
        
        //Nilind 03 Oct
        strAccountManagerName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountManagerName"]];
        strAccountManagerEmail=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountManagerEmail"]];
        strAccountManagerPrimaryPhone=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountManagerPrimaryPhone"]];
        strCustomerName = [NSString stringWithFormat:@"%@",[matches valueForKey:@"customerName"]];

        //End
        
        strFlowType=[NSString stringWithFormat:@"%@",[matches valueForKey:@"flowType"]];
        
        strIsServiceAddrTaxExempt=[matches valueForKey:@"isServiceAddrTaxExempt"];
        strServiceAddressSubType=[matches valueForKey:@"serviceAddressSubType"];
        strTax=[NSString stringWithFormat:@"%@",[matches valueForKey:@"tax"]];
        strAccountNoGlobal=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountNo"]];
        
        //Nilind 03 Oct
        
        NSString *strBillingPocFirstname, *strBillingPocMiddleName,*strBillingPocLastName;
        strBillingPocFirstname =[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingFirstName"]];
        strBillingPocMiddleName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingMiddleName"]];
        strBillingPocLastName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingLastName"]];
        
        if(strBillingPocFirstname.length>0)
        {
            strBillingPocName = [NSString stringWithFormat:@"%@",strBillingPocFirstname];
        }
        if(strBillingPocMiddleName.length>0)
        {
            strBillingPocName = [NSString stringWithFormat:@"%@ %@",strBillingPocName,strBillingPocMiddleName];
        }
        if(strBillingPocLastName.length>0)
        {
            strBillingPocName = [NSString stringWithFormat:@"%@ %@",strBillingPocName,strBillingPocLastName];
        }
        //End
        
        // [arrTerms addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadGeneralTermsConditions"]]];
        [arrTerms addObject:[NSString stringWithFormat:@"%@",([[matches valueForKey:@"leadGeneralTermsConditions"] isKindOfClass:[NSNull class]]) ? @"" : [matches valueForKey:@"leadGeneralTermsConditions"]]];
        
        
        if ([[arrTerms objectAtIndex:0]isEqualToString:@""]||[arrTerms isEqual:nil]||[[arrTerms objectAtIndex:0]isEqualToString:@"(null)"])
        {
            [arrTerms removeObjectAtIndex:0];
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
            
            if([[dictMasters valueForKey:@"GeneralTermsConditions"]isKindOfClass:[NSDictionary class]])
            {
                [arrTerms addObject:[dictMasters valueForKeyPath:@"GeneralTermsConditions.TermsConditions"]];
            }
            else
            {
                [arrTerms addObject:@""];
            }
        }
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        NSLog(@"arrTerms====%@",arrTerms);
        
        
        NSString *audioPath=[NSString stringWithFormat:@"%@",[matches valueForKey:@"audioFilePath"]];
        
        if (audioPath.length>0) {
            
            if ([audioPath isEqualToString:@"(null)"]) {
                strGlobalAudio=@"";
            } else {
                
                NSLog(@"audioPath====%@",audioPath);
                strGlobalAudio=audioPath;
                [self downloadingAudio:audioPath];
                
            }
        }
        
        //Nilind 5 Jan
        
        status=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
        strLeadStatusGlobal = [NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
        
        
        strStageStatus=[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]];
        strStageSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]];//BillingFirstName
        
        //.......................
        
        strIsPresetWO=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isEmployeePresetSignature"]];
        
        
        strLeadNumber=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadNumber"]];//BillingFirstName
        
        NSString *strChk;
        strChk=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCustomerNotPresent"]];
        _viewCustomerSignature.clipsToBounds=YES;
        
        if ([strChk isEqualToString:@"true"])
        {
            chkCustomerNotPresent=YES;
            [_imgCheckBoxCustomerNotPresent setImage:[UIImage imageNamed:@"check_box_2.png"]];
            
            //_cnstrntCustomerSignatureView_H.constant=241;// Actual 131
            //_cnstrntViewTermsCondition_H.constant=633;// 437-131
            _cnstrntCustomerSignatureView_H.constant=0;// Actual 131
            _cnstrntViewTermsCondition_H.constant=306;// 437-131
            
            _btnIAgree.hidden=YES;
            _lblTerms.hidden=YES;
            _btnTerms.hidden=YES;
            
        }
        else
        {
            chkCustomerNotPresent=NO;
            [_imgCheckBoxCustomerNotPresent setImage:[UIImage imageNamed:@"check_box_1.png"]];
            
            _cnstrntCustomerSignatureView_H.constant=131;// Actual 131
            _cnstrntViewTermsCondition_H.constant=320;// 437-131
            
            _btnIAgree.hidden=NO;
            _lblTerms.hidden=NO;
            _btnTerms.hidden=NO;
            
            
        }
        
        //Temp
        
        NSString *strPreferedMonth=[NSString stringWithFormat:@"%@",[matches valueForKey:@"strPreferredMonth"]];
        NSArray *arrPreferMonth = [strPreferedMonth componentsSeparatedByString:@","];
        for(int i=0;i<arrPreferMonth.count;i++)
        {
            NSString *strValuePreferMonth=[arrPreferMonth objectAtIndex:i];
            
            strValuePreferMonth= [strValuePreferMonth stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if ([strValuePreferMonth isEqualToString:@"January"])
            {
                [_btnJan setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"January"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"February"])
            {
                [_btnFeb setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"February"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"March"])
            {
                [_btnMar setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"March"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"April"])
            {
                [_btnApr setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"April"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"May"])
            {
                [_btnMay setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"May"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"June"])
            {
                [_btnJun setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"June"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"July"])
            {
                [_btnJul setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"July"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"August"])
            {
                [_btnAug setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"August"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"September"])
            {
                [_btnSept setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"September"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"October"])
            {
                [_btnOct setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"October"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"November"])
            {
                [_btnNov setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"November"];
                
            }
            else if ([strValuePreferMonth isEqualToString:@"December"])
            {
                [_btnDec setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
                [arrPreferredMonths addObject:@"December"];
                
            }
            
        }
        
        //End
        NSString *strPrintBuntton;
        strPrintBuntton=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isResendAgreementProposalMail"]];
        if([strPrintBuntton isEqualToString:@"true"]||[strPrintBuntton isEqualToString:@"True"])
        {
            _btnPrintAgreement.hidden=NO;
        }
        else
        {
            _btnPrintAgreement.hidden=YES;
            
        }
        strStageSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]];
        if([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
        {
            _btnPrintAgreement.hidden=YES;
            
        }
        if(([strStageSysName isEqualToString:@"Proposed"]||[strStageSysName isEqualToString:@"Proposed"])&&([[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]] isEqualToString:@"Open"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]] isEqualToString:@"open"]))
        {
            _btnPrintAgreement.hidden=NO;
            
        }
        
        if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountElectronicAuthorizationFormSigned"]] isEqualToString:@"true"])
        {
            strIsFormFilled = @"true";
        }
        else
        {
            strIsFormFilled = @"false";
        }
        
        _txtOtherDiscountClarkPest.text =[NSString stringWithFormat:@"%@", [matches valueForKey:@"otherDiscount"]];
        _txtTipDiscountClarkPest.text  =[NSString stringWithFormat:@"%@", [matches valueForKey:@"tipDiscount"]];
        _txtLeadInspectionFee.text  =[NSString stringWithFormat:@"%@", [matches valueForKey:@"leadInspectionFee"]];
        _txtViewInternalNotes.text  =[NSString stringWithFormat:@"%@", [matches valueForKey:@"notes"]];
        
        
        /* float totalInitial =0.0;
         
         totalInitial = totalAmount + [_txtLeadInspectionFee.text floatValue] - [_txtOtherDiscountClarkPest.text floatValue] - [_txtTipDiscountClarkPest.text floatValue];
         _lblTotalPriceInitialClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
         
         _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];*/
        
    }
}
#pragma mark- 31 Aug Nilind
#pragma mark- FETCH CORE DATA
-(void)fetchFromCoreDataStandard
{
    /*
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
     requestNew = [[NSFetchRequest alloc] init];
     [requestNew setEntity:entitySoldServiceStandardDetail];
     NSPredicate *predicate;
     predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
     [requestNew setPredicate:predicate];
     
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
     sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     
     [requestNew setSortDescriptors:sortDescriptors];
     
     self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
     [self.fetchedResultsControllerSalesInfo setDelegate:self];
     
     // Perform Fetch
     NSError *error1 = nil;
     [self.fetchedResultsControllerSalesInfo performFetch:&error1];
     //arrAllObj=[[NSArray alloc]init];
     arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
     
     arrStanInitialPrice=[[NSMutableArray alloc]init];
     arrStanMaintenancePrice=[[NSMutableArray alloc]init];
     arrStanFrequencyName=[[NSMutableArray alloc]init];
     arrStanServiceName=[[NSMutableArray alloc]init];
     arrStanFinalInitialPrice=[[NSMutableArray alloc]init];
     arrStanFinalMaintPrice=[[NSMutableArray alloc]init];
     
     //Nilind 04 May
     arrOneTime=[[NSMutableArray alloc]init];
     arrNotOneTime=[[NSMutableArray alloc]init];
     //End
     arrPackageNameId=[[NSMutableArray alloc]init];
     
     arrBillingFreq=[[NSMutableArray alloc]init];
     //arrBundleId=[[NSMutableArray alloc]init];
     arrStanDiscountPrice=[[NSMutableArray alloc]init];
     
     arrStanInitialPriceAllServices=[[NSMutableArray alloc]init];
     arrStanMaintPriceAllServices=[[NSMutableArray alloc]init];
     arrStanFinalInitialPriceAllServices=[[NSMutableArray alloc]init];
     arrStanFinalMaintPriceAllServices=[[NSMutableArray alloc]init];
     arrStanDiscountAllServices=[[NSMutableArray alloc]init];
     arrStanServiceNameAllServices=[[NSMutableArray alloc]init];
     //arrBundleId=[[NSMutableArray alloc]init];
     //arrBundleId=[[NSMutableArray alloc]init];
     NSMutableArray *arrBundleNew;
     arrBundleNew=[[NSMutableArray alloc]init];
     
     arrTempBundelDetailId=[[NSMutableArray alloc]init];
     arrTempBundleServiceSysName=[[NSMutableArray alloc]init];
     arrBundleRow=[[NSMutableArray alloc]init];
     
     
     arrFreqSysName=[[NSMutableArray alloc]init];
     arrAdditionalParamterDcs=[[NSMutableArray alloc]init];
     arrUnit=[[NSMutableArray alloc]init];
     arrAddParamInital=[[NSMutableArray alloc]init];
     arrAddParaMaint=[[NSMutableArray alloc]init];
     
     arrStanAllSoldServiceStandardId=[[NSMutableArray alloc]init];
     
     
     if (arrAllObj.count==0)
     {
     
     //nILIND 15 Oct
     _cnstrntTblMaintenance_H.constant=0;//293
     _cnsrtViewMaintainService_H.constant=655-293; //506
     
     
     //....................
     
     }else
     {
     
     _cnstrntTblMaintenance_H.constant=293;
     _cnsrtViewMaintainService_H.constant=655;//506
     
     for (int k=0; k<arrAllObj.count; k++)
     {
     matches=arrAllObj[k];
     NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
     if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@""])
     {
     //  [arrStanInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
     // [arrStanMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
     [arrAdditionalParamterDcs addObject:[matches valueForKey:@"additionalParameterPriceDcs"]];
     [arrUnit addObject:[matches valueForKey:@"unit"]];
     
     
     NSArray *arrAdditionalPara=[matches valueForKey:@"additionalParameterPriceDcs"];
     float totalParaInitial=0.0,totalParaMaint=0.0;
     if(arrAdditionalPara.count>0)
     {
     for (int i=0; i<arrAdditionalPara.count; i++)
     {
     NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
     totalParaInitial=totalParaInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]floatValue];
     totalParaMaint=totalParaMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]floatValue];
     
     }
     totalParaInitial=totalParaInitial+[[matches valueForKey:@"initialPrice"]floatValue];
     totalParaMaint=totalParaMaint+[[matches valueForKey:@"maintenancePrice"]floatValue];
     [arrStanInitialPrice addObject:[NSString stringWithFormat:@"%.2f",totalParaInitial]];
     
     [arrStanMaintenancePrice addObject:[NSString stringWithFormat:@"%.2f",totalParaMaint]];
     }
     else
     {
     [arrStanInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
     
     [arrStanMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
     }
     
     [arrStanFinalInitialPrice addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
     [arrStanFinalMaintPrice addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
     [arrStanFrequencyName addObject:[matches valueForKey:@"serviceFrequency"]];
     [arrStanServiceName addObject:[matches valueForKey:@"serviceSysName"]];
     [arrFreqSysName addObject:[matches valueForKey:@"frequencySysName"]];
     
     //Nilind 04 May
     
     if([[matches valueForKey:@"serviceFrequency"] isEqualToString:@"OneTime"]||[[matches valueForKey:@"serviceFrequency"] isEqualToString:@"OneTime"])
     {
     [arrOneTime addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceFrequency"]]];
     }
     else
     {
     [arrNotOneTime addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceFrequency"]]];
     }
     [arrPackageNameId addObject:[matches valueForKey:@"packageId"]];
     [arrBillingFreq addObject:[matches valueForKey:@"billingFrequencySysName"]];
     //[arrBundleId addObject:[matches valueForKey:@"bundleId"]];
     
     [arrStanDiscountPrice addObject:[matches valueForKey:@"discount"]];
     
     }
     else
     {
     [arrBundleNew addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]];
     
     if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"BundleDetailId"]] isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"BundleDetailId"]] isEqual:nil])
     {
     
     }
     else
     {
     //[arrTempBundleId addObject:[matches valueForKey:@"bundleId"]];
     [arrTempBundelDetailId addObject:[matches valueForKey:@"bundleDetailId"]];
     [arrTempBundleServiceSysName addObject:[matches valueForKey:@"serviceSysName"]];
     
     
     }
     
     }
     
     
     // Paramter Serivce Initial
     NSArray *arrAdditionalPara=[matches valueForKey:@"additionalParameterPriceDcs"];
     for(int i=0;i<arrAdditionalPara.count;i++)
     {
     NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
     [arrAddParamInital addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]];
     [arrAddParaMaint addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]];
     }
     //End
     
     //get all services
     
     [arrStanInitialPriceAllServices addObject:[matches valueForKey:@"initialPrice"]];
     [arrStanFinalInitialPriceAllServices addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
     [arrStanDiscountAllServices addObject:[matches valueForKey:@"discount"]];
     [arrStanServiceNameAllServices addObject:[matches valueForKey:@"serviceSysName"]];
     [arrStanMaintPriceAllServices addObject:[matches valueForKey:@"maintenancePrice"]];
     [arrStanFinalMaintPriceAllServices addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
     //End
     
     [arrStanAllSoldServiceStandardId addObject:[matches valueForKey:@"soldServiceStandardId"]];
     
     }
     arrBundleRow = [[NSSet setWithArray:arrBundleNew] allObjects];
     
     
     }
     [_tblBundle reloadData];
     [_tblMaintenance reloadData];
     
     
     */
    
}

-(void)fetchFromCoreDataNonStandard
{
    
    /*
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
     requestNew = [[NSFetchRequest alloc] init];
     [requestNew setEntity:entitySoldServiceNonStandardDetail];
     NSPredicate *predicate;
     predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
     [requestNew setPredicate:predicate];
     
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
     sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     
     [requestNew setSortDescriptors:sortDescriptors];
     
     self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
     [self.fetchedResultsControllerSalesInfo setDelegate:self];
     
     // Perform Fetch
     NSError *error1 = nil;
     [self.fetchedResultsControllerSalesInfo performFetch:&error1];
     arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
     arrNonStanInitialPrice=[[NSMutableArray alloc]init];
     arrNonStanServiceName=[[NSMutableArray alloc]init];
     arrNonStanDiscountPrice=[[NSMutableArray alloc]init];
     arrBillingFreqSysNameNonStan=[[NSMutableArray alloc]init];
     arrNonStandardTermsCondition=[[NSMutableArray alloc]init];
     if (arrAllObj.count==0)
     {
     
     _cnstrntNonStandardTbl_H.constant=0;//293
     _cnstrntViewNonStandard_H.constant=447-293;//447
     
     }else
     {
     _cnstrntNonStandardTbl_H.constant=293;
     _cnstrntViewNonStandard_H.constant=447;
     
     for (int k=0; k<arrAllObj.count; k++)
     {
     matches=arrAllObj[k];
     NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
     [arrNonStanInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
     [arrNonStanServiceName addObject:[matches valueForKey:@"serviceName"]];
     [arrNonStanDiscountPrice addObject:[matches valueForKey:@"discount"]];
     [arrBillingFreqSysNameNonStan addObject:[matches valueForKey:@"billingFrequencySysName"]];
     [arrNonStandardTermsCondition addObject:[matches valueForKey:@"nonStdServiceTermsConditions"]];
     
     
     }
     }
     [_tblNonStandardService reloadData];
     
     */
    
}

#pragma mark-     --------------------------SAVE ACTION----------------------
- (IBAction)actionOnSaveContinue:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        [self goToAgreement];
    }
    
    else
    {
        if([strIsAgreementValidFor isEqualToString:@"true"])
        {
            if(_txtDays.text.length==0)
            {
                [global AlertMethod:@"Alert!" :@"Please enter days for agreement validity"];
                [_txtDays becomeFirstResponder];
                return;
            }
            else
            {
                if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
                {
                    [self goToAgreement];
                }
                else
                {
                    [self fetchFromCoreDataStandardClarkPestSoldCount];
                    [self fetchFromCoreDataNonStandardClarkPestSoldCount];
                    
                    if(arrSoldCount.count==0 && arrSoldCountNonStan.count==0)
                    {
                        [global displayAlertController:@"Alert" :@"To proceed, please add atleast one service to the agreement" :self];
                    }
                    else
                    {
                        
                        [self updateLeadIdDetail];
                        [self saveImageToCoreData];
                        [self saveNewAgreementDetails];
                        [self updatePaymentInfoCoreDataNew];
                        [self goToAgreement];
                    }
                }
                
                
            }
        }
        else
        {
            if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            {
                [self goToAgreement];
            }
            else
            {
                [self fetchFromCoreDataStandardClarkPestSoldCount];
                [self fetchFromCoreDataNonStandardClarkPestSoldCount];
                
                if(arrSoldCount.count==0 && arrSoldCountNonStan.count==0)
                {
                    [global displayAlertController:@"Alert" :@"To proceed, please add atleast one service to the agreement" :self];
                }
                else
                {
                    [self updateLeadIdDetail];
                    [self saveImageToCoreData];
                    [self saveNewAgreementDetails];
                    [self updatePaymentInfoCoreDataNew];
                    [self goToAgreement];
                }
            };
            
            
        }
    }
}

-(void)paymentConditionMethod :(BOOL)NoImagePresent{
    
    //Nilind
    
    if ([strPaymentMode isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Payment Type" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if ([strPaymentMode isEqualToString:@"Cash"])
    {
        //Nilind 025 Jan
        if(NoImagePresent)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Kindly take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            if(_txtPaidAmountPriceInforamtion.text.length==0 || [_txtPaidAmountPriceInforamtion.text isEqualToString:@"0"]||[_txtPaidAmountPriceInforamtion.text isEqualToString:@"0.0"]||[_txtPaidAmountPriceInforamtion.text isEqualToString:@"0.00"])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Amount." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            else
            {
                chkChequeClick=NO;
                strLicenseNo=@"";
                strChequeNo=@"";
                strDate=@"";
                strAmount=_txtPaidAmountPriceInforamtion.text;
                [self updatePaymentInfoCoreData];
                
                if ([self checkForCustomerSignature]==NO)
                {
                    [global displayAlertController:@"Alert!" :@"Kindly take the Customer Signature" :self];
                }
                else
                {
                    if (isEditedInSalesAuto==YES)
                    {
                        NSLog(@"Global mopdify date called in Agreement");
                        [global updateSalesModifydate:strLeadId];
                    }
                    //Nilind 27 Dec
                    
                    [self updateLeadIdDetail];
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Appointment completed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alert show];
                    
                    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
                    SendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailViewController"];
                    NSString *strValue;
                    
                    if (chkCustomerNotPresent) {
                        strValue=@"no";
                    } else {
                        strValue=@"yes";
                    }
                    
                    objSendMail.isCustomerPresent=strValue;
                    
                    [self.navigationController pushViewController:objSendMail animated:NO];
                }
                
                
            }
        }
        
    }
    else if ([strPaymentMode isEqualToString:@"Check"])
    {
        if(NoImagePresent)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Kindly take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            if(_txtPaidAmountPriceInforamtion.text.length==0 || [_txtPaidAmountPriceInforamtion.text isEqualToString:@"0"]||[_txtPaidAmountPriceInforamtion.text isEqualToString:@"0.0"]||[_txtPaidAmountPriceInforamtion.text isEqualToString:@"0.00"])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Amount." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            else if(_txtChequeValue.text.length==0 )
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Check #." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            /* else if(_txtLicenseValue.text.length==0 )
             {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter License #." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [alert show];
             }
             else if (_txtExpirationDate.text.length==0)
             {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Expiration Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [alert show];
             }
             else if (arrOFImagesName.count==0) {
             
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Front Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
             
             }else if (arrOfCheckBackImage.count==0) {
             
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Back Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
             
             }*/
            
            else
            {
                
                chkChequeClick=NO;
                strLicenseNo=_txtLicenseValue.text;
                strChequeNo=_txtChequeValue.text;
                strAmount=_txtPaidAmountPriceInforamtion.text;
                [self updatePaymentInfoCoreData];
                
                if ([self checkForCustomerSignature]==NO)
                {
                    [global displayAlertController:@"Alert!" :@"Kindly take the Customer Signature" :self];
                }
                else
                {
                    
                    
                    if (isEditedInSalesAuto==YES)
                    {
                        NSLog(@"Global mopdify date called in Agreement");
                        [global updateSalesModifydate:strLeadId];
                    }
                    
                    
                    [self updateLeadIdDetail];
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Appointment completed " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alert show];
                    
                    
                    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
                    SendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailViewController"];
                    
                    NSString *strValue;
                    
                    if (chkCustomerNotPresent) {
                        strValue=@"no";
                    } else {
                        strValue=@"yes";
                    }
                    
                    objSendMail.isCustomerPresent=strValue;
                    
                    [self.navigationController pushViewController:objSendMail animated:NO];
                }
                
            }
        }
        
    }
    else if ([strPaymentMode isEqualToString:@"CreditCard"])
    {
        if(NoImagePresent)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Kindly take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            
            if(_txtPaidAmountPriceInforamtion.text.length==0 || [_txtPaidAmountPriceInforamtion.text isEqualToString:@"0"]||[_txtPaidAmountPriceInforamtion.text isEqualToString:@"0.0"]||[_txtPaidAmountPriceInforamtion.text isEqualToString:@"0.00"])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Amount." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            else
            {
                
                
                Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
                NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
                strLicenseNo=@"";
                strChequeNo=@"";
                strAmount=_txtPaidAmountPriceInforamtion.text;
                [self updatePaymentInfoCoreData];
                
                if ([self checkForCustomerSignature]==NO)
                {
                    [global displayAlertController:@"Alert!" :@"Kindly take the Customer Signature" :self];
                }
                else
                {
                    if (isEditedInSalesAuto==YES)
                    {
                        NSLog(@"Global mopdify date called in Agreement");
                        [global updateSalesModifydate:strLeadId];
                    }
                    
                    //Nilind 27 Dec
                    
                    [self updateLeadIdDetail];
                    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                    BOOL isElementIntegration;
                    isElementIntegration=NO;
                    isElementIntegration=[[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsElementIntegration"]]boolValue];
                    if (isElementIntegration==YES)
                    {
                        if (netStatusWify1== NotReachable)
                        {
                            [global AlertMethod:Alert :ErrorInternetMsgPayment];
                        }
                        else
                        {
                            UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
                            CreditCardIntegration *objCreditCard=[storyBoard instantiateViewControllerWithIdentifier:@"CreditCardIntegration"];
                            objCreditCard.strGlobalLeadId=strLeadId;
                            objCreditCard.strAmount=_txtPaidAmountPriceInforamtion.text;
                            objCreditCard.strTypeOfService=@"Lead";
                            objCreditCard.strDeviceType=@"iPhone";
                            [self.navigationController pushViewController:objCreditCard animated:NO];
                        }
                    }
                    else
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Appointment completed " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [alert show];
                        
                        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
                        SendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailViewController"];
                        
                        NSString *strValue;
                        
                        if (chkCustomerNotPresent) {
                            strValue=@"no";
                        } else {
                            strValue=@"yes";
                        }
                        
                        objSendMail.isCustomerPresent=strValue;
                        
                        [self.navigationController pushViewController:objSendMail animated:NO];
                        
                    }
                }
                
            }
        }
        
    }
    else
    {
        /* Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
         NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
         if (netStatusWify1== NotReachable)
         {
         
         }
         else
         {
         _btnInitialSetup.hidden=NO;
         _btnCancel.hidden=NO;
         }*/
        if(NoImagePresent)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Kindly take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            strLicenseNo=@"";
            strChequeNo=@"";
            strAmount=@"0";
            [self updatePaymentInfoCoreData];
            if ([self checkForCustomerSignature]==NO)
            {
                [global displayAlertController:@"Alert!" :@"Kindly take the Customer Signature" :self];
            }
            else
            {
                if (isEditedInSalesAuto==YES)
                {
                    NSLog(@"Global mopdify date called in Agreement");
                    [global updateSalesModifydate:strLeadId];
                }
                
                //Nilind 27 Dec
                
                [self updateLeadIdDetail];
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Appointment completed " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
                //...................
                
                
                /*  if (arrSoldServiceCount.count==0)
                 {
                 _btnInitialSetup.hidden=YES;
                 _btnCancel.hidden=YES;
                 
                 }*/
                UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
                SendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailViewController"];
                
                NSString *strValue;
                
                if (chkCustomerNotPresent) {
                    strValue=@"no";
                } else {
                    strValue=@"yes";
                }
                
                objSendMail.isCustomerPresent=strValue;
                
                [self.navigationController pushViewController:objSendMail animated:NO];
            }
        }
    }
    
}

- (IBAction)actionOnCustomerNotPresent:(UIButton*)sender
{
    [self endEditing];
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    if (sender.selected==NO)
    {
        [_imgCheckBoxCustomerNotPresent setImage:[UIImage imageNamed:@"check_box_2.png"]];
        _cnstrntCustomerSignatureView_H.constant=0;// Actual 131
        _cnstrntViewTermsCondition_H.constant=320-131;// 437-131
        sender.selected=YES;
        _viewCustomerSignature.hidden=YES;
        //Nilind 05 Jan
        chkCustomerNotPresent=YES;
        //........
        
        
        _btnIAgree.hidden=YES;
        _lblTerms.hidden=YES;
        _btnTerms.hidden=YES;
    }
    else
    {
        [_imgCheckBoxCustomerNotPresent setImage:[UIImage imageNamed:@"check_box_1.png"]];
        [_imgViewCustomerSignature setImage:[UIImage imageNamed:@"NoImage.jpg"]];
        _cnstrntCustomerSignatureView_H.constant=131;// Actual 131
        _cnstrntViewTermsCondition_H.constant=320;// 437-131
        sender.selected=NO;
        _viewCustomerSignature.hidden=NO;
        
        //Nilind 05 Jan
        chkCustomerNotPresent=NO;
        //........
        
        _btnIAgree.hidden=NO;
        _lblTerms.hidden=NO;
        _btnTerms.hidden=NO;
    }
    
    _btnTermsConditions.hidden=NO;
    [_btnIAgree setImage:[UIImage imageNamed:@"check_box_1.png"]forState:UIControlStateNormal];
    chkIAgree=NO;
}
- (IBAction)actionOnServiceAgrrment:(id)sender
{
    [self endEditing];
    //27 June
    _btnIAgree.hidden=NO;
    _lblTerms.hidden=NO;
    _btnTerms.hidden=NO;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    BOOL shouldShowElectronicFormLink = [[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsElectronicAuthorizationForm"]]boolValue];
    
    if(shouldShowElectronicFormLink)
    {//show link
        if([strIsFormFilled isEqualToString:@"true"])
        {
            if([self methodToCheckIfElectronicFormExistsForLeadId:strLeadId]==YES)
            {
                _buttonElectronicAuthorizedForm.hidden = NO;
            }
            else
            {
                _buttonElectronicAuthorizedForm.hidden = YES;
            }
        }
        else
        {
            _buttonElectronicAuthorizedForm.hidden = NO;
        }
    }
    else
    { //hide link
        _buttonElectronicAuthorizedForm.hidden = YES;
    }
    //   _buttonElectronicAuthorizedForm.hidden = NO;
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        _buttonElectronicAuthorizedForm.hidden = YES;
    }
    //ENd
    
    
    
    
    _lblTerms.hidden=NO;
    _btnTerms.hidden=NO;
    
    [_scrollViewAgreement setContentOffset:CGPointMake(0,0)];
    
    //_scrollViewAgreement.scrollEnabled=YES;
    chkServiceProposal=NO;
    _viewPersonalInfo.hidden=NO;
    _constPersonalInfoView_H.constant=460;//572;//444;
    _viewAdditionalNotes.hidden=NO;
    _constAdditionNots_H.constant=240;
    _viewPriceInformation.hidden=NO;
    //_cnstrntViewPriceInfo_H.constant=650;
    
    _viewTermsConditions.hidden=NO;
    _cnstrntViewTermsCondition_H.constant=320;
    [self fetchFromCoreDataStandard];
    [self fetchFromCoreDataNonStandard];
    [self subtotalStandard];
    
    [_btnServiceProposal setBackgroundColor:[UIColor lightGrayColor]];
    [_btnServiceAgreement setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    
    
    _const_AddNotes_H.constant=240;
    _const_PriceTerms_H.constant=891-200-50;//891
    if ([strPaymentMode isEqualToString:@""])
    {
        _cnstrntViewPriceInfo_H.constant=480-176+45;//650-130;
        _cnstrntViewCreditCard_H.constant=0;
    }
    if (chkChequeClick==YES)
    {
        // _cnstrntViewPriceInfo_H.constant=650-130;
        _cnstrntViewCreditCard_H.constant=238;
    }
    else
    {
        //_cnstrntViewCreditCard_H.constant=130;
        _cnstrntViewPriceInfo_H.constant=480-176+45;
        _viewCreditCard.hidden=YES;
    }
    //Nilind 2 Nov
    
    /*if (chkStatus==YES)
     {
     
     viewComplete.frame=CGRectMake(_scrollViewAgreement.frame.origin.x, _scrollViewAgreement.frame.origin.y, _scrollViewAgreement.frame.size.width, _scrollViewAgreement.frame.origin.y+_scrollViewAgreement.frame.size.height+1480);
     if ([UIScreen mainScreen].bounds.size.height==568)
     {
     viewComplete.frame=CGRectMake(_scrollViewAgreement.frame.origin.x, 0, _scrollViewAgreement.frame.size.width, _scrollViewAgreement.frame.origin.y+_scrollViewAgreement.frame.size.height+1480+500+_constViewInspection_H.constant);
     }
     
     }*/
    
    [self fetchForAppliedDiscountFromCoreData];
    //.........................................
    [self heightMaintenanceTable];
    [self heightNonStandardTable];
    
    
}

- (IBAction)actionOnServiceProposal:(id)sender
{
    [self endEditing];
    //27 June
    _btnIAgree.hidden=YES;
    _lblTerms.hidden=YES;
    _btnTerms.hidden=YES;
    _buttonElectronicAuthorizedForm.hidden = YES;
    
    //End
    
    [_btnServiceAgreement setBackgroundColor:[UIColor lightGrayColor]];
    [_btnServiceProposal setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    
    
    [_scrollViewAgreement setContentOffset:CGPointMake(0,0)];
    
    
    chkServiceProposal=YES;
    _viewPersonalInfo.hidden=YES;
    _constPersonalInfoView_H.constant=0;
    _viewAdditionalNotes.hidden=YES;
    _viewPriceInformation.hidden=YES;
    
    _const_AddNotes_H.constant=0;
    _const_PriceTerms_H.constant=0;
    
    // Fetch for service proposal
    // [self fetchFromCoreDataStandardServiceProposal];
    // [self fetchFromCoreDataNonStandardServiceProposal];
    //Subtotal Calculation
    [self subtotalNonStandard];
    // End
    
    //Nilind 2 Nov
    
    /* if (chkStatus==YES)
     {
     
     viewComplete.frame=CGRectMake(_scrollViewAgreement.frame.origin.x, _scrollViewAgreement.frame.origin.y, _scrollViewAgreement.frame.size.width, _scrollViewAgreement.frame.origin.y+_scrollViewAgreement.frame.size.height+100);
     if ([UIScreen mainScreen].bounds.size.height==568)
     {
     viewComplete.frame=CGRectMake(_scrollViewAgreement.frame.origin.x, _scrollViewAgreement.frame.origin.y, _scrollViewAgreement.frame.size.width, _scrollViewAgreement.frame.origin.y+_scrollViewAgreement.frame.size.height+100+300+_constViewInspection_H.constant);
     }
     
     }*/
    _viewTermsConditions.hidden=YES;
    _cnstrntViewTermsCondition_H.constant=0;
    
    //.........................................
    [self heightProposaNonStandardTable];
    [self heightProposalMaintenanceTable];
    
    
    _const_ViewNewInitialMaintPrice_H.constant=0;
    _const_ViewCouponCredit_H.constant=0;
    _const_ViewAppliedCredit_H.constant=0;
    
}
#pragma mark- 1 Sept Nilind
#pragma mark- FETCH CORE DATA SERVICE PROPOSAL
-(void)fetchFromCoreDataStandardServiceProposal
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"false"];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrStanInitialPriceServiceProposal=[[NSMutableArray alloc]init];
    arrStanMaintenancePriceServiceProposal=[[NSMutableArray alloc]init];
    arrStanFrequencyNameServiceProposal=[[NSMutableArray alloc]init];
    arrStanServiceNameServiceProposal=[[NSMutableArray alloc]init];
    arrPackageNameId=[[NSMutableArray alloc]init];
    arrStanFinalInitialPrice=[[NSMutableArray alloc]init];
    arrStanFinalMaintPrice=[[NSMutableArray alloc]init];
    
    arrBillingFreq=[[NSMutableArray alloc]init];
    //arrBundleId=[[NSMutableArray alloc]init];
    arrStanDiscountPrice=[[NSMutableArray alloc]init];
    
    
    //For Bundle
    
    arrStanInitialPriceAllServices=[[NSMutableArray alloc]init];
    arrStanFinalInitialPriceAllServices=[[NSMutableArray alloc]init];
    arrStanMaintPriceAllServices=[[NSMutableArray alloc]init];
    arrStanFinalMaintPriceAllServices=[[NSMutableArray alloc]init];
    arrStanDiscountAllServices=[[NSMutableArray alloc]init];
    arrStanServiceNameAllServices=[[NSMutableArray alloc]init];
    //arrBundleId=[[NSMutableArray alloc]init];
    //arrBundleId=[[NSMutableArray alloc]init];
    NSMutableArray *arrBundleNew;
    arrBundleNew=[[NSMutableArray alloc]init];
    
    arrTempBundelDetailId=[[NSMutableArray alloc]init];
    arrTempBundleServiceSysName=[[NSMutableArray alloc]init];
    arrBundleRow=[[NSMutableArray alloc]init];
    
    arrFreqSysNameProposal=[[NSMutableArray alloc]init];
    arrAdditionalParamterDcs=[[NSMutableArray alloc]init];
    arrUnit=[[NSMutableArray alloc]init];
    arrAddParamInital=[[NSMutableArray alloc]init];
    arrAddParaMaint=[[NSMutableArray alloc]init];
    
    
    if (arrAllObj.count==0)
    {
        _cnstrntTblMaintenance_H.constant=0;//293
        _cnsrtViewMaintainService_H.constant=655-293; //506
        
        
        [_tblBundle reloadData];
        [_tblMaintenance reloadData];
    }
    else
    {
        _cnstrntTblMaintenance_H.constant=293;
        _cnsrtViewMaintainService_H.constant=655;//506
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                // [arrStanInitialPriceServiceProposal addObject:[matches valueForKey:@"initialPrice"]];
                
                // [arrStanMaintenancePriceServiceProposal addObject:[matches valueForKey:@"maintenancePrice"]];
                [arrUnit addObject:[matches valueForKey:@"unit"]];
                
                NSArray *arrAdditionalPara=[matches valueForKey:@"additionalParameterPriceDcs"];
                float totalParaInitial=0.0,totalParaMaint=0.0;
                if(arrAdditionalPara.count>0)
                {
                    for (int i=0; i<arrAdditionalPara.count; i++)
                    {
                        NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
                        totalParaInitial=totalParaInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]floatValue];
                        totalParaMaint=totalParaMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]floatValue];
                        
                    }
                    totalParaInitial=totalParaInitial+[[matches valueForKey:@"initialPrice"]floatValue];
                    totalParaMaint=totalParaMaint+[[matches valueForKey:@"maintenancePrice"]floatValue];
                    [arrStanInitialPriceServiceProposal addObject:[NSString stringWithFormat:@"%.2f",totalParaInitial]];
                    
                    [arrStanMaintenancePriceServiceProposal addObject:[NSString stringWithFormat:@"%.2f",totalParaMaint]];
                }
                else
                {
                    [arrStanInitialPriceServiceProposal addObject:[matches valueForKey:@"initialPrice"]];
                    
                    [arrStanMaintenancePriceServiceProposal addObject:[matches valueForKey:@"maintenancePrice"]];
                }
                
                
                
                
                [arrStanFinalInitialPrice addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                [arrStanFinalMaintPrice addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                [arrStanFrequencyNameServiceProposal addObject:[matches valueForKey:@"serviceFrequency"]];
                [arrFreqSysNameProposal addObject:[matches valueForKey:@"frequencySysName"]];
                
                [arrStanServiceNameServiceProposal addObject:[matches valueForKey:@"serviceSysName"]];
                [arrPackageNameId addObject:[matches valueForKey:@"packageId"]];
                
                [arrBillingFreq addObject:[matches valueForKey:@"billingFrequencySysName"]];
                //[arrBundleId addObject:[matches valueForKey:@"bundleId"]];
                
                [arrStanDiscountPrice addObject:[matches valueForKey:@"discount"]];
            }
            else
            {
                [arrBundleNew addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]];
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleDetailId"]] isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleDetailId"]] isEqual:nil])
                {
                    
                }
                else
                {
                    //[arrTempBundleId addObject:[matches valueForKey:@"bundleId"]];
                    [arrTempBundelDetailId addObject:[matches valueForKey:@"bundleDetailId"]];
                    [arrTempBundleServiceSysName addObject:[matches valueForKey:@"serviceSysName"]];
                    
                    
                }
                
            }
            //get all services
            
            // Paramter Serivce Initial
            NSArray *arrAdditionalPara=[matches valueForKey:@"additionalParameterPriceDcs"];
            for(int i=0;i<arrAdditionalPara.count;i++)
            {
                NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
                [arrAddParamInital addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]];
                [arrAddParaMaint addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]];
            }
            //End
            //get all services
            [arrStanInitialPriceAllServices addObject:[matches valueForKey:@"initialPrice"]];
            [arrStanFinalInitialPriceAllServices addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
            
            [arrStanDiscountAllServices addObject:[matches valueForKey:@"discount"]];
            [arrStanServiceNameAllServices addObject:[matches valueForKey:@"serviceSysName"]];
            
            [arrStanMaintPriceAllServices addObject:[matches valueForKey:@"maintenancePrice"]];
            [arrStanFinalMaintPriceAllServices addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
            //End
            
            
        }
        arrBundleRow = [[NSSet setWithArray:arrBundleNew] allObjects];
        
        if (arrStanInitialPriceServiceProposal.count==0)
        {
            //  _cnstrntTblMaintenance_H.constant=0;
            //   _cnsrtViewMaintainService_H.constant=200;
        }
        _tblBundle.delegate=self;
        _tblBundle.dataSource=self;
        [_tblBundle reloadData];
        _tblMaintenance.delegate=self;
        [_tblMaintenance reloadData];
        
    }
}

-(void)fetchFromCoreDataNonStandardServiceProposal
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"false"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrNonStanInitialPriceServiceProposal=[[NSMutableArray alloc]init];
    
    arrNonStanServiceNameServiceProposal=[[NSMutableArray alloc]init];
    arrNonStanDiscountPrice=[[NSMutableArray alloc]init];
    arrBillingFreqSysNameNonStan=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        
        _cnstrntNonStandardTbl_H.constant=0;//293
        _cnstrntViewNonStandard_H.constant=447-293;//447
        
        
        [_tblNonStandardService reloadData];
    }else
    {
        
        _cnstrntNonStandardTbl_H.constant=293;
        _cnstrntViewNonStandard_H.constant=447;
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            [arrNonStanInitialPriceServiceProposal addObject:[matches valueForKey:@"initialPrice"]];
            [arrNonStanServiceNameServiceProposal addObject:[matches valueForKey:@"serviceName"]];
            [arrNonStanDiscountPrice addObject:[matches valueForKey:@"discount"]];
            [arrBillingFreqSysNameNonStan addObject:[matches valueForKey:@"billingFrequencySysName"]];
            
            
        }
        _tblNonStandardService.delegate=self;
        
        if (arrNonStanInitialPriceServiceProposal.count==0)
        {
            //_cnstrntNonStandardTbl_H.constant=0;
            // _cnstrntViewNonStandard_H.constant=200;
        }
        
        [_tblNonStandardService reloadData];
        
    }
}
-(void)subtotalStandard
{
    
    
    // For Sandard Agreement
    
    stantotalInitial=0;stanTotalMaintenance=0;nonStanInitial=0;
    for (int i=0; i<arrStanInitialPriceAllServices.count; i++)//arrStanInitialPrice
    {
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]];
        if ([str isEqualToString:@"0"])
        {
            stantotalInitial=stantotalInitial+[[arrStanInitialPriceAllServices objectAtIndex:i] doubleValue];
            stanTotalMaintenance=stanTotalMaintenance+[[arrStanMaintPriceAllServices objectAtIndex:i]doubleValue];
            
        }
        else
        {
            stantotalInitial=stantotalInitial+[[arrStanFinalInitialPriceAllServices objectAtIndex:i] doubleValue];
            stanTotalMaintenance=stanTotalMaintenance+[[arrStanFinalMaintPriceAllServices objectAtIndex:i]doubleValue];
        }
        
    }
    
    // Parameter Price Service Calculation
    for (int i=0; i<arrAddParamInital.count; i++)
    {
        stantotalInitial=stantotalInitial+[[arrAddParamInital objectAtIndex:i]floatValue];
        stanTotalMaintenance=stanTotalMaintenance+[[arrAddParaMaint objectAtIndex:i]doubleValue];
    }
    //End
    _lblInitialPricevValueMaintenanceService.text=[NSString stringWithFormat:@"%.2f",stantotalInitial];
    _lblMaintenancePriceMaintenanceService.text=[NSString stringWithFormat:@"%.2f",stanTotalMaintenance];
    
    //.......................................................
    
    // For Sandard Agreement
    
    for (int i=0; i<arrNonStanInitialPrice.count; i++)
    {
        nonStanInitial=nonStanInitial+[[arrNonStanInitialPrice objectAtIndex:i] doubleValue];
        
    }
    _lblSubtotalAmountValueNonStandardService.text=[NSString stringWithFormat:@"%.2f",nonStanInitial];
    
    //.......................................................
    
    NSUserDefaults *defsStan=[NSUserDefaults standardUserDefaults];
    NSString *totalStan=[defsStan valueForKey:@"initialStan"];
    NSString *totalDiscount=[defsStan valueForKey:@"discountStan"];
    
    NSString *totalNonStan=[defsStan valueForKey:@"initialNonStan"];
    NSString *totalNonDiscount=[defsStan valueForKey:@"discountNonStan"];
    
    double subStan=[totalStan doubleValue]+[totalNonStan doubleValue];
    double subDiscount=[totalDiscount doubleValue]+[totalNonDiscount doubleValue];
    
    _lblSubtotalAmountValuePriceInformation.text=[NSString stringWithFormat:@"%.2f",subStan];
    _lblCouponDiscountValuePriceInformation.text=[NSString stringWithFormat:@"%.2f",subDiscount];
    
    _lblTotalPriceValuePriceInformation.text=[NSString stringWithFormat:@"%.2f",(subStan-subDiscount)];
    
#pragma mark- TAX CALCULATION NEED TO DONE
    /*double taxCAL;
     if(strTax.length==0|| [strTax isEqualToString:@""])
     {
     strTax=@"0.0";
     }
     taxCAL=((subStan-subDiscount)*[strTax doubleValue])/100;
     */
    
    double calculateTax,taxableAmount;
    calculateTax=0;
    NSString* val1=[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"caluclateTax"]];
    NSString* val2=[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"taxNonStandard"]];
    calculateTax=[val1 doubleValue]+[val2 doubleValue];
    
    taxableAmount=[[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"taxabaleAmountNonStan"]] doubleValue]+[[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"taxableAmountStan"]] doubleValue];
    
    // _lblTaxAmountValuePriceInformation.text=[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"caluclateTax"]];
    _lblTaxAmountValuePriceInformation.text=[NSString stringWithFormat:@"%.2f",calculateTax];
    
    _lblBillingAmountPriceInformation.text=[NSString stringWithFormat:@"%.2f",[_lblTotalPriceValuePriceInformation.text doubleValue]+[_lblTaxAmountValuePriceInformation.text doubleValue]];
    
    _txtPaidAmountPriceInforamtion.text= _lblBillingAmountPriceInformation.text;
    strFinalTaxabelAmount=[NSString stringWithFormat:@"%.2f",taxableAmount];
    strFinalTaxAmount=[NSString stringWithFormat:@"%.2f",calculateTax];
    strFinalTotalPrice=[NSString stringWithFormat:@"%@",_lblTotalPriceValuePriceInformation.text ];
    
    
    //Nilind 06 Oct
    //For Discount Total
    double totalDiscountStanService;
    totalDiscountStanService=0;
    for (int i=0; i<arrStanDiscountAllServices.count; i++)//arrStanDiscountPrice
    {
        totalDiscountStanService=totalDiscountStanService+[[arrStanDiscountAllServices objectAtIndex:i] doubleValue];
    }
    _lblDiscountPriceStandard.text=[NSString stringWithFormat:@"%.2f",totalDiscountStanService];
    
    
    double totalDiscountnonStanServiceProposal;
    totalDiscountnonStanServiceProposal=0;
    for (int i=0; i<arrNonStanDiscountPrice.count; i++)
    {
        totalDiscountnonStanServiceProposal=totalDiscountnonStanServiceProposal+[[arrNonStanDiscountPrice objectAtIndex:i] doubleValue];
    }
    _lblDiscountPriceNonStandard.text=[NSString stringWithFormat:@"%.2f",totalDiscountnonStanServiceProposal];
    
    
    [self calculationForCouponAndCredit];
    
}
-(void)subtotalNonStandard
{
    // For Sandard Service Proposal
    
    double totalInitailServiceProposal,totalMaintnenanceServiceProposal;
    totalInitailServiceProposal=0;totalMaintnenanceServiceProposal=0;
    
    for (int i=0; i<arrStanInitialPriceAllServices.count; i++)//arrStanInitialPriceServiceProposal
    {
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]];
        if ([str isEqualToString:@"0"])
        {
            
            totalInitailServiceProposal=totalInitailServiceProposal+[[arrStanInitialPriceAllServices objectAtIndex:i]doubleValue];
            totalMaintnenanceServiceProposal=totalMaintnenanceServiceProposal+[[arrStanMaintPriceAllServices objectAtIndex:i]doubleValue];
            
        }
        else
        {
            totalInitailServiceProposal=totalInitailServiceProposal+[[arrStanFinalInitialPriceAllServices objectAtIndex:i]doubleValue];
            totalMaintnenanceServiceProposal=totalMaintnenanceServiceProposal+[[arrStanFinalMaintPriceAllServices objectAtIndex:i]doubleValue];
        }
        
    }
    
    // Parameter Price Service Calculation
    for (int i=0; i<arrAddParamInital.count; i++)
    {
        totalInitailServiceProposal=totalInitailServiceProposal+[[arrAddParamInital objectAtIndex:i]floatValue];
        totalMaintnenanceServiceProposal=totalMaintnenanceServiceProposal+[[arrAddParaMaint objectAtIndex:i]doubleValue];
    }
    //End
    
    _lblInitialPricevValueMaintenanceService.text=[NSString stringWithFormat:@"%.2f",totalInitailServiceProposal];
    _lblMaintenancePriceMaintenanceService.text=[NSString stringWithFormat:@"%.2f",totalMaintnenanceServiceProposal];
    
    
    //........................................................
    
    // For NonSandard Service Proposal
    double totalInitailnonStanServiceProposal;
    totalInitailnonStanServiceProposal=0;
    
    for (int i=0; i<arrNonStanInitialPriceServiceProposal.count; i++)
    {
        totalInitailnonStanServiceProposal=totalInitailnonStanServiceProposal+[[arrNonStanInitialPriceServiceProposal objectAtIndex:i] doubleValue];
        
    }
    _lblSubtotalAmountValueNonStandardService.text=[NSString stringWithFormat:@"%.2f",totalInitailnonStanServiceProposal];
    
    
    
    
    //Nilind Discount Service Proposal Standar
    
    //Nilind 06 Oct
    
    //For Discount Total
    
    //Nilind 06 Oct
    //For Discount Total
    double totalDiscountStanService;
    totalDiscountStanService=0;
    for (int i=0; i<arrStanDiscountPrice.count; i++)
    {
        totalDiscountStanService=totalDiscountStanService+[[arrStanDiscountPrice objectAtIndex:i] doubleValue];
    }
    _lblDiscountPriceStandard.text=[NSString stringWithFormat:@"%.2f",totalDiscountStanService];
    
    double totalDiscountnonStanServiceProposal;
    totalDiscountnonStanServiceProposal=0;
    for (int i=0; i<arrNonStanDiscountPrice.count; i++)
    {
        totalDiscountnonStanServiceProposal=totalDiscountnonStanServiceProposal+[[arrNonStanDiscountPrice objectAtIndex:i] doubleValue];
    }
    _lblDiscountPriceNonStandard.text=[NSString stringWithFormat:@"%.2f",totalDiscountnonStanServiceProposal];
    
    //15 June
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        _lblDiscountPriceStandard.text=@"00.00";
        _lblDiscountPriceNonStandard.text=@"00.00";
    }
}
#pragma mark- SIGN VIEW ACTION
- (IBAction)actionOnInspectorSign:(id)sender
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromInspector" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    SignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"SignViewController"];
    objSignViewController.strType=@"inspector";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}

- (IBAction)actionOnCustomerSignature:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustomer" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    SignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"SignViewController"];
    objSignViewController.strType=@"customer";
    
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}

- (IBAction)actionOnExpirayDate:(id)sender
{
    [self addPickerViewDateTo];
}

- (IBAction)actionOnTermsConditions:(id)sender
{
    //Nilind 20 Jan
    viewBackGroundTerms=[[UIView alloc]init];
    viewBackGroundTerms=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundTerms.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    
    _viewTerms.frame=CGRectMake(10  ,100, [UIScreen mainScreen].bounds.size.width-20,[UIScreen mainScreen].bounds.size.height-200);
    
    _scrollTerms.frame=CGRectMake(0  , 0, _viewTerms.frame.size.width,_viewTerms.frame.size.height-5);
    
    
    UILabel *lblTermsConditions=[[UILabel alloc]initWithFrame:CGRectMake(0+10,0,_viewTerms.frame.size.width-20, 50)];
    lblTermsConditions.text=@"General Terms And Conditions:";
    if([UIScreen mainScreen].bounds.size.height==568 || [UIScreen mainScreen].bounds.size.height==480)
    {
        lblTermsConditions.font=[UIFont boldSystemFontOfSize:14];
    }
    else
    {
        lblTermsConditions.font=[UIFont boldSystemFontOfSize:14];
    }
    UILabel *lblTerms=[[UILabel alloc]initWithFrame:CGRectMake(0+10,CGRectGetMaxY(lblTermsConditions.frame)+5,_viewTerms.frame.size.width-20, 80)];
    
    NSString *str;
    str=[arrTerms objectAtIndex:0];
    str=@"";
    str=[self convertHTML:str];
    str = [str stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    lblTerms.backgroundColor=[UIColor whiteColor];
    
    CGSize expectedLabelSize = [str sizeWithFont:lblTerms.font constrainedToSize: CGSizeMake(lblTerms.frame.size.width, FLT_MAX) lineBreakMode:lblTerms.lineBreakMode];
    
    //Nilind 17 Jan
    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX);
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
    NSStringDrawingUsesLineFragmentOrigin;
    NSDictionary *attributDict = @{NSFontAttributeName: [UIFont systemFontOfSize:11]};
    CGRect rectsize = [str boundingRectWithSize:maximumLabelSize options:options attributes:attributDict context:nil];
    
    
    CGRect newFrame = lblTerms.frame;
    
    
    newFrame.size.height =expectedLabelSize.height;
    lblTerms.frame = newFrame;
    lblTerms.text=str;
    lblTerms.textAlignment=NSTextAlignmentJustified;
    lblTerms.numberOfLines=500;
    /* [_scrollTerms addSubview:lblTermsConditions];
     [_scrollTerms addSubview:lblTerms];
     [_viewTerms addSubview:_scrollTerms];
     [viewBackGroundTerms addSubview:_viewTerms];
     [self.view addSubview:viewBackGroundTerms];
     [_scrollTerms setBackgroundColor:[UIColor whiteColor]];*/
    
    int height;
    height=0;
    
    //For Non Standard Service
    
    
    UILabel *lblNonStanDardService;
    if(height==0)
    {
        lblNonStanDardService=[[UILabel alloc]initWithFrame:CGRectMake(0+10,0,_viewTerms.frame.size.width-20, 50)];
    }
    else
    {
        lblNonStanDardService=[[UILabel alloc]initWithFrame:CGRectMake(0+10,0,_viewTerms.frame.size.width-20, 50)];
    }
    lblNonStanDardService.text=@"General Terms And Conditions:";
    if([UIScreen mainScreen].bounds.size.height==568 || [UIScreen mainScreen].bounds.size.height==480)
    {
        lblNonStanDardService.font=[UIFont boldSystemFontOfSize:16];
    }
    else
    {
        lblNonStanDardService.font=[UIFont boldSystemFontOfSize:14];
    }
    //  lblNonStanDardService.font=[UIFont boldSystemFontOfSize:20];
    [_scrollTerms addSubview:lblNonStanDardService];
    [_scrollTerms addSubview:lblNonStanDardService];
    [_viewTerms addSubview:_scrollTerms];
    [viewBackGroundTerms addSubview:_viewTerms];
    [self.view addSubview:viewBackGroundTerms];
    [_scrollTerms setBackgroundColor:[UIColor whiteColor]];
    
    for (int i=0; i<arrSelectedTermsConditionsDescriptions.count; i++)//arrSoldTerms
    {
        UILabel *lblServiceName=[[UILabel alloc]init];
        UILabel *lblTermsCondition=[[UILabel alloc]init];
        ;
        // lblServiceName.font=[UIFont systemFontOfSize:14];
        //lblTermsCondition.font=[UIFont systemFontOfSize:14];
        if (i==0)
        {
            lblServiceName.frame=CGRectMake(lblServiceName.frame.origin.x+10,CGRectGetMaxY(lblNonStanDardService.frame)+10,_viewTerms.frame.size.width-20, 25);
            
        }
        else
        {
            lblServiceName.frame=CGRectMake(lblServiceName.frame.origin.x+10,height+10,_viewTerms.frame.size.width-20, 25);
        }
        
        lblServiceName.backgroundColor=[UIColor whiteColor];
        if([UIScreen mainScreen].bounds.size.height==568 || [UIScreen mainScreen].bounds.size.height==480)
        {
            lblServiceName.font=[UIFont boldSystemFontOfSize:14];
        }
        else
        {
            lblServiceName.font=[UIFont boldSystemFontOfSize:14];
        }
        //TempComment //
        lblServiceName.text=[[arrSelectedTermsConditionsDescriptions objectAtIndex:i] valueForKey:@"TermsTitle"];
        
        lblTermsCondition.frame=CGRectMake(0+10, lblServiceName.frame.origin.y+lblServiceName.frame.size.height+10, _viewTerms.frame.size.width-20, 100);
        
        lblTermsCondition.text=[[arrSelectedTermsConditionsDescriptions objectAtIndex:i] valueForKey:@"TermsConditions"];
        //Nilind Temp
        
        NSString *strSold;
        strSold=[[arrSelectedTermsConditionsDescriptions objectAtIndex:i] valueForKey:@"TermsConditions"];
        strSold=[self convertHTML:strSold];
        strSold = [strSold stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        strSold = [strSold stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        CGSize expectedLabelSizeSold = [strSold sizeWithFont:lblTermsCondition.font constrainedToSize: CGSizeMake(lblTermsCondition.frame.size.width, FLT_MAX)lineBreakMode:lblTermsCondition.lineBreakMode];
        CGRect newFrameSold = lblTermsCondition.frame;
        newFrameSold.size.height =expectedLabelSizeSold.height;
        lblTermsCondition.frame = newFrameSold;
        lblTermsCondition.text=strSold;
        lblTermsCondition.textAlignment=NSTextAlignmentJustified;
        lblTermsCondition.numberOfLines=500;
        lblTermsCondition.font=[UIFont systemFontOfSize:14];
        
        //.......................................................
        
        height=lblTermsCondition.frame.origin.y+lblTermsCondition.frame.size.height;
        lblTermsCondition.backgroundColor=[UIColor whiteColor];
        
        [_scrollTerms addSubview:lblServiceName];
        [_scrollTerms addSubview:lblTermsCondition];
        //[self.view addSubview:_scrollTerms];
        
        
        [_viewTerms addSubview:_scrollTerms];
        [self.view addSubview:_viewTerms];//_scrollTerms
        
    }
    
    
    
    UILabel *lblStanDardService;//=[[UILabel alloc]initWithFrame:CGRectMake(0+10,CGRectGetMaxY(lblTerms.frame)+10,_viewTerms.frame.size.width-20, 50)];
    if(height==0)
    {
        lblStanDardService=[[UILabel alloc]initWithFrame:CGRectMake(0+10, CGRectGetMaxY(lblNonStanDardService.frame)+10,_viewTerms.frame.size.width-20, 50)];
        
    }
    else
    {
        lblStanDardService=[[UILabel alloc]initWithFrame:CGRectMake(0+10,height+10,_viewTerms.frame.size.width-20, 50)];
    }
    lblStanDardService.text=@"Standard Service Terms And Conditions:";
    lblStanDardService.font=[UIFont boldSystemFontOfSize:14];
    [_scrollTerms addSubview:lblStanDardService];
    [_viewTerms addSubview:_scrollTerms];
    
    for (int i=0; i<arrSoldServiceSysname.count; i++)//arrSoldTerms
    {
        UILabel *lblServiceName=[[UILabel alloc]init];
        UILabel *lblTermsCondition=[[UILabel alloc]init];
        ;
        // lblServiceName.font=[UIFont systemFontOfSize:14];
        //lblTermsCondition.font=[UIFont systemFontOfSize:14];
        if (i==0)
        {
            lblServiceName.frame=CGRectMake(lblServiceName.frame.origin.x+10,CGRectGetMaxY(lblStanDardService.frame)+10,_viewTerms.frame.size.width-20, 25);
            
        }
        else
        {
            lblServiceName.frame=CGRectMake(lblServiceName.frame.origin.x+10,height+10,_viewTerms.frame.size.width-20, 25);
        }
        
        lblServiceName.backgroundColor=[UIColor whiteColor];
        if([UIScreen mainScreen].bounds.size.height==568 || [UIScreen mainScreen].bounds.size.height==480)
        {
            lblServiceName.font=[UIFont boldSystemFontOfSize:14];
        }
        else
        {
            lblServiceName.font=[UIFont boldSystemFontOfSize:14];
        }
        //TempComment //
        lblServiceName.text=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrSoldServiceSysname objectAtIndex:i]]];////[arrSoldServiceSysname objectAtIndex:i];
        
        
        lblTermsCondition.frame=CGRectMake(0+10, lblServiceName.frame.origin.y+lblServiceName.frame.size.height+10, _viewTerms.frame.size.width-20, 100);
        
        lblTermsCondition.text=[dictTermsForService valueForKey:[arrSoldServiceSysname objectAtIndex:i]];//[arrSoldTerms objectAtIndex:i];
        //Nilind Temp
        
        NSString *strSold;
        strSold=[dictTermsForService valueForKey:[arrSoldServiceSysname objectAtIndex:i]];//[arrSoldTerms objectAtIndex:i];
        strSold=[self convertHTML:strSold];
        strSold = [strSold stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        strSold = [strSold stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        CGSize expectedLabelSizeSold = [strSold sizeWithFont:lblTermsCondition.font constrainedToSize: CGSizeMake(lblTermsCondition.frame.size.width, FLT_MAX)lineBreakMode:lblTermsCondition.lineBreakMode];
        CGRect newFrameSold = lblTermsCondition.frame;
        newFrameSold.size.height =expectedLabelSizeSold.height;
        lblTermsCondition.frame = newFrameSold;
        lblTermsCondition.text=strSold;
        lblTermsCondition.textAlignment=NSTextAlignmentJustified;
        lblTermsCondition.numberOfLines=500;
        lblTermsCondition.font=[UIFont systemFontOfSize:14];
        
        //.......................................................
        
        height=lblTermsCondition.frame.origin.y+lblTermsCondition.frame.size.height;
        lblTermsCondition.backgroundColor=[UIColor whiteColor];
        
        [_scrollTerms addSubview:lblServiceName];
        [_scrollTerms addSubview:lblTermsCondition];
        //[self.view addSubview:_scrollTerms];
        
        
        [_viewTerms addSubview:_scrollTerms];
        [self.view addSubview:_viewTerms];//_scrollTerms
        
    }
    
    
    
    
    
    if (arrSoldServiceSysname.count==0)//arrSoldTerms
    {
        _scrollTerms.contentSize=CGSizeMake(0, lblTerms.frame.size.height+height+50
                                            );
        
    }
    else
    {
        
        _scrollTerms.contentSize=CGSizeMake(0, height+50
                                            );
    }
    [_scrollTerms setContentOffset:CGPointZero];
    
}
-(void)oldTermsConditions
{
    //Nilind 20 Jan
    viewBackGroundTerms=[[UIView alloc]init];
    viewBackGroundTerms=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundTerms.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    
    _viewTerms.frame=CGRectMake(10  ,100, [UIScreen mainScreen].bounds.size.width-20,[UIScreen mainScreen].bounds.size.height-200);
    
    _scrollTerms.frame=CGRectMake(0  , 0, _viewTerms.frame.size.width,_viewTerms.frame.size.height-5);
    
    
    UILabel *lblTermsConditions=[[UILabel alloc]initWithFrame:CGRectMake(0+10,0,_viewTerms.frame.size.width-20, 50)];
    lblTermsConditions.text=@"";//@"General Terms And Conditions:";
    if([UIScreen mainScreen].bounds.size.height==568 || [UIScreen mainScreen].bounds.size.height==480)
    {
        lblTermsConditions.font=[UIFont boldSystemFontOfSize:14];
    }
    else
    {
        lblTermsConditions.font=[UIFont boldSystemFontOfSize:14];
    }
    UILabel *lblTerms=[[UILabel alloc]initWithFrame:CGRectMake(0+10,CGRectGetMaxY(lblTermsConditions.frame)+5,_viewTerms.frame.size.width-20, 80)];
    
    NSString *str;
    str=[arrTerms objectAtIndex:0];
    str=@"";
    str=[self convertHTML:str];
    str = [str stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    lblTerms.backgroundColor=[UIColor whiteColor];
    
    CGSize expectedLabelSize = [str sizeWithFont:lblTerms.font constrainedToSize: CGSizeMake(lblTerms.frame.size.width, FLT_MAX) lineBreakMode:lblTerms.lineBreakMode];
    
    //Nilind 17 Jan
    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX);
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
    NSStringDrawingUsesLineFragmentOrigin;
    NSDictionary *attributDict = @{NSFontAttributeName: [UIFont systemFontOfSize:11]};
    CGRect rectsize = [str boundingRectWithSize:maximumLabelSize options:options attributes:attributDict context:nil];
    
    
    CGRect newFrame = lblTerms.frame;
    
    
    newFrame.size.height =expectedLabelSize.height;
    lblTerms.frame = newFrame;
    lblTerms.text=str;
    lblTerms.textAlignment=NSTextAlignmentJustified;
    lblTerms.numberOfLines=500;
    [_scrollTerms addSubview:lblTermsConditions];
    [_scrollTerms addSubview:lblTerms];
    [_viewTerms addSubview:_scrollTerms];
    [viewBackGroundTerms addSubview:_viewTerms];
    [self.view addSubview:viewBackGroundTerms];
    [_scrollTerms setBackgroundColor:[UIColor whiteColor]];
    
    int height;
    height=0;
    
    UILabel *lblStanDardService=[[UILabel alloc]initWithFrame:CGRectMake(0+10,CGRectGetMaxY(lblTerms.frame)+10,_viewTerms.frame.size.width-20, 50)];
    lblStanDardService.text=@"Standard Service Terms And Conditions:";
    lblStanDardService.font=[UIFont boldSystemFontOfSize:14];
    [_scrollTerms addSubview:lblStanDardService];
    [_viewTerms addSubview:_scrollTerms];
    
    for (int i=0; i<arrSoldServiceSysname.count; i++)//arrSoldTerms
    {
        UILabel *lblServiceName=[[UILabel alloc]init];
        UILabel *lblTermsCondition=[[UILabel alloc]init];
        ;
        // lblServiceName.font=[UIFont systemFontOfSize:14];
        //lblTermsCondition.font=[UIFont systemFontOfSize:14];
        if (i==0)
        {
            lblServiceName.frame=CGRectMake(lblServiceName.frame.origin.x+10,CGRectGetMaxY(lblStanDardService.frame)+10,_viewTerms.frame.size.width-20, 25);
            
        }
        else
        {
            lblServiceName.frame=CGRectMake(lblServiceName.frame.origin.x+10,height+10,_viewTerms.frame.size.width-20, 25);
        }
        
        lblServiceName.backgroundColor=[UIColor whiteColor];
        if([UIScreen mainScreen].bounds.size.height==568 || [UIScreen mainScreen].bounds.size.height==480)
        {
            lblServiceName.font=[UIFont boldSystemFontOfSize:14];
        }
        else
        {
            lblServiceName.font=[UIFont boldSystemFontOfSize:18];
        }
        //TempComment //
        lblServiceName.text=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrSoldServiceSysname objectAtIndex:i]]];////[arrSoldServiceSysname objectAtIndex:i];
        
        
        lblTermsCondition.frame=CGRectMake(0+10, lblServiceName.frame.origin.y+lblServiceName.frame.size.height+10, _viewTerms.frame.size.width-20, 100);
        
        lblTermsCondition.text=[dictTermsForService valueForKey:[arrSoldServiceSysname objectAtIndex:i]];//[arrSoldTerms objectAtIndex:i];
        //Nilind Temp
        
        NSString *strSold;
        strSold=[dictTermsForService valueForKey:[arrSoldServiceSysname objectAtIndex:i]];//[arrSoldTerms objectAtIndex:i];
        strSold=[self convertHTML:strSold];
        strSold = [strSold stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        strSold = [strSold stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        CGSize expectedLabelSizeSold = [strSold sizeWithFont:lblTermsCondition.font constrainedToSize: CGSizeMake(lblTermsCondition.frame.size.width, FLT_MAX)lineBreakMode:lblTermsCondition.lineBreakMode];
        CGRect newFrameSold = lblTermsCondition.frame;
        newFrameSold.size.height =expectedLabelSizeSold.height;
        lblTermsCondition.frame = newFrameSold;
        lblTermsCondition.text=strSold;
        lblTermsCondition.textAlignment=NSTextAlignmentJustified;
        lblTermsCondition.numberOfLines=500;
        
        //.......................................................
        
        height=lblTermsCondition.frame.origin.y+lblTermsCondition.frame.size.height;
        lblTermsCondition.backgroundColor=[UIColor whiteColor];
        
        [_scrollTerms addSubview:lblServiceName];
        [_scrollTerms addSubview:lblTermsCondition];
        //[self.view addSubview:_scrollTerms];
        
        
        [_viewTerms addSubview:_scrollTerms];
        [self.view addSubview:_viewTerms];//_scrollTerms
        
    }
    
    
    //For Non Standard Service
    
    
    UILabel *lblNonStanDardService;
    if(height==0)
    {
        lblNonStanDardService=[[UILabel alloc]initWithFrame:CGRectMake(0+10,CGRectGetMaxY(lblStanDardService.frame)+10,_viewTerms.frame.size.width-20, 50)];
    }
    else
    {
        lblNonStanDardService=[[UILabel alloc]initWithFrame:CGRectMake(0+10,height+10,_viewTerms.frame.size.width-20, 50)];
    }
    lblNonStanDardService.text=@"Terms And Conditions:";
    lblNonStanDardService.font=[UIFont boldSystemFontOfSize:14];
    [_scrollTerms addSubview:lblNonStanDardService];
    [_viewTerms addSubview:_scrollTerms];
    
    for (int i=0; i<arrSelectedTermsConditionsDescriptions.count; i++)//arrSoldTerms
    {
        UILabel *lblServiceName=[[UILabel alloc]init];
        UILabel *lblTermsCondition=[[UILabel alloc]init];
        ;
        // lblServiceName.font=[UIFont systemFontOfSize:14];
        //lblTermsCondition.font=[UIFont systemFontOfSize:14];
        if (i==0)
        {
            lblServiceName.frame=CGRectMake(lblServiceName.frame.origin.x+10,CGRectGetMaxY(lblNonStanDardService.frame)+10,_viewTerms.frame.size.width-20, 25);
            
        }
        else
        {
            lblServiceName.frame=CGRectMake(lblServiceName.frame.origin.x+10,height+10,_viewTerms.frame.size.width-20, 25);
        }
        
        lblServiceName.backgroundColor=[UIColor whiteColor];
        if([UIScreen mainScreen].bounds.size.height==568 || [UIScreen mainScreen].bounds.size.height==480)
        {
            lblServiceName.font=[UIFont boldSystemFontOfSize:14];
        }
        else
        {
            lblServiceName.font=[UIFont boldSystemFontOfSize:14];
        }
        //TempComment //
        lblServiceName.text=[[arrSelectedTermsConditionsDescriptions objectAtIndex:i] valueForKey:@"TermsTitle"];
        
        lblTermsCondition.frame=CGRectMake(0+10, lblServiceName.frame.origin.y+lblServiceName.frame.size.height+10, _viewTerms.frame.size.width-20, 100);
        
        lblTermsCondition.text=[[arrSelectedTermsConditionsDescriptions objectAtIndex:i] valueForKey:@"TermsConditions"];
        //Nilind Temp
        
        NSString *strSold;
        strSold=[[arrSelectedTermsConditionsDescriptions objectAtIndex:i] valueForKey:@"TermsConditions"];
        strSold=[self convertHTML:strSold];
        strSold = [strSold stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        strSold = [strSold stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        CGSize expectedLabelSizeSold = [strSold sizeWithFont:lblTermsCondition.font constrainedToSize: CGSizeMake(lblTermsCondition.frame.size.width, FLT_MAX)lineBreakMode:lblTermsCondition.lineBreakMode];
        CGRect newFrameSold = lblTermsCondition.frame;
        newFrameSold.size.height =expectedLabelSizeSold.height;
        lblTermsCondition.frame = newFrameSold;
        lblTermsCondition.text=strSold;
        lblTermsCondition.textAlignment=NSTextAlignmentJustified;
        lblTermsCondition.numberOfLines=500;
        
        //.......................................................
        
        height=lblTermsCondition.frame.origin.y+lblTermsCondition.frame.size.height;
        lblTermsCondition.backgroundColor=[UIColor whiteColor];
        
        [_scrollTerms addSubview:lblServiceName];
        [_scrollTerms addSubview:lblTermsCondition];
        //[self.view addSubview:_scrollTerms];
        
        
        [_viewTerms addSubview:_scrollTerms];
        [self.view addSubview:_viewTerms];//_scrollTerms
        
    }
    
    
    if (arrSoldServiceSysname.count==0)//arrSoldTerms
    {
        _scrollTerms.contentSize=CGSizeMake(0, lblTerms.frame.size.height+height+50
                                            );
        
    }
    else
    {
        
        _scrollTerms.contentSize=CGSizeMake(0, height+50
                                            );
    }
    [_scrollTerms setContentOffset:CGPointZero];
    
}

#pragma mark- PAYMENT INFO UPDATE COREDATA
//Nilind 13 Sept

-(void)updatePaymentInfoCoreData
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        [self savePaymentInfo];
    }
    else
    {
        
        matches=arrAllObj[0];
        [matches setValue:strPaymentMode forKey:@"PaymentMode"];
        [matches setValue:strAmount forKey:@"amount"];
        [matches setValue:strChequeNo forKey:@"checkNo"];
        [matches setValue:strLicenseNo forKey:@"licenseNo"];
        
        [matches setValue:strDate forKey:@"expirationDate"];
        
        if (chkCustomerNotPresent) {
            
            [matches setValue:@"" forKey:@"customerSignature"];
            
        } else {
            
            [matches setValue:strCustomerSignature forKey:@"customerSignature"];
            
        }
        
        //[matches setValue:strCustomerSignature forKey:@"customerSignature"];
        [matches setValue:strSalesSignature forKey:@"salesSignature"];
        
        //For Preset
        
        NSUserDefaults *defsSign=[NSUserDefaults standardUserDefaults];
        NSString *strSignUrl=[defsSign valueForKey:@"ServiceTechSignPath"];
        //BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignService"];
        BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignSales"];
        if ((isPreSetSign ==YES) && (strSignUrl.length>0))
        {
            //NSString *thePresetFileName =[strSignUrl lastPathComponent];
            strSignUrl=[strSignUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            NSString *result;
            NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
            }else{
                result=strSignUrl;
            }
            
            
            [matches setValue:result forKey:@"salesSignature"];
            
        }
        
        
        [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
        [matches setValue:_txtViewAdditionalNotes.text forKey:@"SpecialInstructions"];
        //Nilind 27 Sept
        
        //Nilind 16 Nov
        [matches setValue:strCompanyKey forKey:@"companyKey"];
        [matches setValue:strUserName forKey:@"userName"];
        
        //.............
        
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSMutableArray *arrImageToSend,*arrTemp;
        
        arrTemp=[[NSMutableArray alloc]init];
        arrImageToSend=[[NSMutableArray alloc]init];
        arrTemp=[defs valueForKey:@"arrImageToSend"];
        
        for (int i=0; i<arrTemp.count; i++)
        {
            [arrImageToSend addObject:[arrTemp objectAtIndex:i]];
        }
        
        
        if (chkCustomerNotPresent) {
            
            //[arrImageToSend addObject:strCustomerSignature];
            
        } else {
            
            [arrImageToSend addObject:strCustomerSignature];
            
        }
        
        //[arrImageToSend addObject:strCustomerSignature];
        [arrImageToSend addObject:strSalesSignature];
        
        if ([strPaymentMode isEqualToString:@"Check"])
        {
            
            if (!(arrOFImagesName.count==0))
            {
                
                [matches setValue:[arrOFImagesName objectAtIndex:0] forKey:@"CheckFrontImagePath"];
            }
            else
            {
                [matches setValue:@"" forKey:@"CheckFrontImagePath"];
            }
            if (!(arrOfCheckBackImage.count==0))
            {
                [matches setValue:[arrOfCheckBackImage objectAtIndex:0] forKey:@"checkBackImagePath"];
            }
            else
            {
                [matches setValue:@"" forKey:@"checkBackImagePath"];            }
        }
        
        
        [defs setObject:arrImageToSend forKey:@"arrImageToSend"];
        [defs synchronize];
        
        //........................................................
        [context save:&error1];
    }
    [self fetchForPaymentInfo];
}
-(void)fetchForPaymentInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        //[self savePaymentInfo];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            signCustomer=[NSString stringWithFormat:@"%@",[matches valueForKey:@"customerSignature"]];
            signInspector=[NSString stringWithFormat:@"%@",[matches valueForKey:@"salesSignature"]];
            strPaymentMode=[NSString stringWithFormat:@"%@",[matches valueForKey:@"paymentMode"]];
            
            strChequeNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"checkNo"]];
            strLicenseNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"licenseNo"]];
            strExpirationDate=[NSString stringWithFormat:@"%@",[matches valueForKey:@"expirationDate"]];
            _txtViewAdditionalNotes.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"specialInstructions"]];
            
            
            NSString* checkFrontImagePath=[matches valueForKey:@"checkFrontImagePath"];
            if (checkFrontImagePath.length>0)
            {
                [arrOFImagesName addObject:checkFrontImagePath];
                //[self downloadingImagess:[arrOFImagesName objectAtIndex:0]];
            }
            NSString* checkBackImagePath=[matches valueForKey:@"checkBackImagePath"];
            if (checkBackImagePath.length>0)
            {
                [arrOfCheckBackImage addObject:checkBackImagePath];
                //[self downloadingImagess:[arrOfCheckBackImage objectAtIndex:0]];
                
            }
            //  [self downloadingImagess:signInspector];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            strCustomerSignature = signCustomer;
            strSalesSignature = signInspector;
        }
    }
}


//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    
    pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:14];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewBackGround removeFromSuperview];
    [viewForDate removeFromSuperview];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [_txtChequeValue resignFirstResponder];
    [_txtLicenseValue resignFirstResponder];
    [_txtExpirationDate resignFirstResponder];
    [_txtPaidAmountPriceInforamtion resignFirstResponder];
    [_txtEnterCouponNo resignFirstResponder];
    
    [self setValueForParticipants];
    
    //[_scrollTerms removeFromSuperview];
    // [_viewTerms removeFromSuperview];
    // [viewBackGroundTerms removeFromSuperview];
    //[_txtViewAdditionalNotes resignFirstResponder];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    _txtExpirationDate.text=strDate;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)tapDetectedOnBackGroundView{
    [viewBackGround removeFromSuperview];
}

//...........................................................
//NILIND 28 SEPT=============================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================

-(void)downloadingImagess:(NSString *)str
{
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation;
    strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }
    if (result.length==0)
    {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSString *strUrl;// = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
    // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];//str
    // strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    if ([str rangeOfString:@"Documents"].location == NSNotFound) {
        NSLog(@"string does not Documents");
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];//s
    } else {
        NSLog(@"string contains Documents!");
        strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists)
    {
        [self stopdejal];
        ////[self ShowFirstImage];
    }
    else
    {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        // NSURL *url = [NSURL URLWithString:strNewString];
        
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
        UIImage *image = [UIImage imageWithData:photoData];
        if (result.length==0)
        {
            result=str;
        }
        [self saveImageAfterDownload1:image :result];
    }
    
    [self stopdejal];
    imageTemp= [self loadImage:result];
}

-(void)downloadingImagessTechnicianIfPreset:(NSString *)str
{
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation;
    strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.HrmsServiceModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }
    if (result.length==0)
    {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSString *strUrl;// = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
    //strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];//str
    strUrl = [NSString stringWithFormat:@"%@",str];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists)
    {
        [self stopdejal];
        ////[self ShowFirstImage];
    }
    else
    {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        strNewString=[strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
        UIImage *image = [UIImage imageWithData:photoData];
        if (result.length==0)
        {
            result=str;
        }
        //result = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        // result = [result stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        [self saveImageAfterDownload1:image :result];
    }
    
    [self stopdejal];
    imageTemp= [self loadImage:result];
}

-(void)downloadingImagessTechnicianPreSet:(NSString *)str
{
    NSString *result;
    NSRange equalRange = [signInspector rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [signInspector substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=signInspector;
    }
    if (result.length==0) {
        NSRange equalRange = [signInspector rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [signInspector substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@",str];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists)
    {
        [self stopdejal];
        ////[self ShowFirstImage];
    }
    else
    {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        strNewString=[strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
        UIImage *image = [UIImage imageWithData:photoData];
        if (result.length==0)
        {
            result=str;
        }
        //result = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        //result = [result stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        [self saveImageAfterDownload1:image :result];
    }
    
    [self stopdejal];
    imageTemp= [self loadImage:result];
}

// Image  Load 12 Sept
- (UIImage*)loadImage:(NSString *)imageName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}
-(void)stopdejal{
    [DejalActivityView removeView];
}
-(void)startDejal
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading Image..."];
}

//...........................................................
#pragma mark- TEXTFIELD DELEGATE METHOD
//...........................................................

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtChequeValue resignFirstResponder];
    [_txtLicenseValue resignFirstResponder];
    [_txtExpirationDate resignFirstResponder];
    [_txtPaidAmountPriceInforamtion resignFirstResponder];
    [_txtEnterCouponNo resignFirstResponder];
    [_txtDays resignFirstResponder];
    [_txtTipDiscountClarkPest resignFirstResponder];
    [_txtOtherDiscountClarkPest resignFirstResponder];
    //[_txtViewAdditionalNotes resignFirstResponder];
    return YES;
}

//...........................................................
#pragma mark- TEXTVIEW DELEGATE METHOD
//...........................................................
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    // [self.view setFrame:CGRectMake(0,-150,self.view.frame.size.width,self.view.frame.size.height)];
    if(textView==_txtViewIntroductionLetter)
    {
        [_txtViewIntroductionLetter resignFirstResponder];
        // [self goToHTMLEditor:[self getHTMLstringFromAttributeString:_txtViewIntroductionLetter.attributedText]];
        
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    if([text isEqualToString:@"\n"])
    {
      /*  [_txtViewAdditionalNotes resignFirstResponder];
        [txtFieldCaption resignFirstResponder];
        [txtViewImageDescription resignFirstResponder];
        [_txtViewIntroductionLetter resignFirstResponder];
        [_txtViewTermsServices resignFirstResponder];
        [_txtViewInternalNotes resignFirstResponder]; */
        
        if (range.length == 0) {
            if ([text isEqualToString:@"\n"]) {
                textView.text = [NSString stringWithFormat:@"%@\n",textView.text];
                return NO;
            }
        }
        
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}
//============================================================================
#pragma mark-  DATE TIME
//============================================================================


-(NSString *)ChangeDateToLocalDateTime :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    /// scrollView.contentOffset = CGPointMake(0, 500);
    
    
}

-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}
//.........Nilind 3'rd Oct...................................
#pragma mark- FETCH SOLD SERVICE
//...........................................................
-(void)fetchSoldServiceStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    for (int i=0; i<arrAllObj.count; i++)
    {
        matches=[arrAllObj objectAtIndex:i];
        if([[matches valueForKey:@"isSold"] isEqualToString:@"true"])
        {
            
            [arrSoldTerms addObject:[NSString stringWithFormat :@"%@",[matches valueForKey:@"serviceTermsConditions"]]];
            [arrSoldServiceSysname addObject:[NSString stringWithFormat :@"%@",[matches valueForKey:@"serviceSysName"]]];
            
        }
    }
    
    
    /*NSArray *arrSoldTermsUnique = [[NSSet setWithArray:arrSoldTerms] allObjects];
     NSArray *arrSoldServiceSysnameUnique = [[NSSet setWithArray:arrSoldServiceSysname] allObjects];
     
     arrSoldTerms=[[NSMutableArray alloc]init];
     arrSoldServiceSysname=[[NSMutableArray alloc]init];
     
     arrSoldTerms=(NSMutableArray*)arrSoldTermsUnique;
     arrSoldServiceSysname=(NSMutableArray*)arrSoldServiceSysnameUnique;*/
    
    NSArray *arrSoldServiceSysnameUnique = [[NSSet setWithArray:arrSoldServiceSysname] allObjects];
    arrSoldServiceSysname=[[NSMutableArray alloc]init];
    arrSoldServiceSysname=(NSMutableArray*)arrSoldServiceSysnameUnique;
    
    NSLog(@"arrSoldServiceSysname>>>%@",arrSoldServiceSysname);
    NSLog(@"arrSoldTerms>>%@",arrSoldTerms);
}
//  NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
//dictMasters=[defs valueForKey:@"MasterSalesAutomation"];


- (IBAction)scrollBack:(id)sender
{
    
    //[_scrollTerms removeFromSuperview];
    for(UILabel *lbl in _scrollTerms.subviews)
    {
        [lbl removeFromSuperview];
    }
    [_viewTerms removeFromSuperview];
    [viewBackGroundTerms removeFromSuperview];
    
    
}

//Nilind 3 Oct
#pragma mark- LABEL FRAME

-(void)setLabelFrame:(NSString *)str:(UILabel *)lbl
{
    CGSize expectedLabelSize = [str sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lbl.lineBreakMode];
    
    CGRect newFrame = lbl.frame;
    newFrame.size.height =expectedLabelSize.height;
    lbl.frame = newFrame;
}

//..........................................................................
- (IBAction)btnTerms:(id)sender {
}

//============================================================================
#pragma mark- setDynamicData Method
//============================================================================
-(void)setDynamicData{
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    NSArray *arrDynamicData=[defss valueForKey:@"salesDynamicForm"];
    int  val;
    val=0;
    
    int constantHeighttt=80.0;
    int BtnMainViewKiHeight=0.0;
    
    for (int k=0; k<arrDynamicData.count; k++) {
        
        CGFloat scrollViewHeight=0.0;
        
        NSDictionary *dictMain=arrDynamicData[k];
        
        UIButton *BtnMainView;
        
        if (!MainViewForm.frame.size.height) {
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, 60, [UIScreen mainScreen].bounds.size.width-20, 42)];
        }
        else{
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, BtnMainViewKiHeight-25, [UIScreen mainScreen].bounds.size.width-20, 42)];
            
            if (BtnMainViewKiHeight==0)
            {
                BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, 60, [UIScreen mainScreen].bounds.size.width-20, 42)];
                
            }
        }
        //  [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        
        [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentName"] forState:UIControlStateNormal];
        
        //    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 40, 40)];
        //    imgView.image=[UIImage imageNamed:@"minus_icon.png"];
        
        //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
        //  [BtnMainView addSubview:imgView];
        
        [_viewInspection addSubview:BtnMainView];
        
        [arrayOfButtonsMain addObject:BtnMainView];
        
        // Form Creation
        MainViewForm=[[UIView alloc]init];
        
        MainViewForm.backgroundColor=[UIColor clearColor];
        
        MainViewForm.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, 1000);
        
        [_viewInspection addSubview:MainViewForm];
        
        NSArray *arrSections=[dictMain valueForKey:@"Sections"];
        
        
        int yAxisViewFormSections=0.0;
        int heightVIewFormSections=0.0;
        int heightMainViewFormSections=0.0;
        
        BOOL isValuePrsent,isValuePrsentForSection;
        isValuePrsent=NO;
        isValuePrsentForSection=NO;
        NSMutableArray *arrValuePresentSection;
        arrValuePresentSection=[[NSMutableArray alloc]init];
        for (int j=0; j<arrSections.count; j++)
        {
            NSDictionary *dictSection=[arrSections objectAtIndex:j];
            NSArray *arrControls=[dictSection valueForKey:@"Controls"];
            
            // Section Creation Start
            heightVIewFormSections=0.0;
            ViewFormSections=[[UIView alloc]init];
            ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width-20, 400);
            
            ViewFormSections.backgroundColor=[UIColor whiteColor];
            UILabel *lblTitleSection=[[UILabel alloc]init];
            lblTitleSection.backgroundColor=[UIColor lightGrayColor];
            lblTitleSection.layer.borderWidth = 1.5f;
            lblTitleSection.layer.cornerRadius = 4.0f;
            lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
            
            lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-20, 30);
            lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]] ;
            lblTitleSection.textAlignment=NSTextAlignmentCenter;
            [ViewFormSections addSubview:lblTitleSection];
            
            ViewFormSections.layer.borderWidth = 1.5f;
            ViewFormSections.layer.cornerRadius = 4.0f;
            ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
            
            [MainViewForm addSubview:ViewFormSections];
            
            CGFloat xPositionOfControls= 0.0;
            CGFloat yPositionOfControls = 0.0;
            CGFloat heightOfControls= 0.0;
            CGFloat widthOfControls= 0.0;
            
            NSMutableArray *arrOfControlsValurPrsent=[[NSMutableArray alloc]init];
            
            
            for (int k=0; k<arrControls.count; k++)
            {
                NSDictionary *dictControls=[arrControls objectAtIndex:k];
                UILabel *lblTitleControl=[[UILabel alloc]init];
                lblTitleControl.backgroundColor=[UIColor blackColor];
                lblTitleControl.frame=CGRectMake(0, 30*k+35, [UIScreen mainScreen].bounds.size.width-20, 30);
                lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
                lblTitleControl.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
                lblTitleControl.textAlignment=NSTextAlignmentCenter;
                // [ViewFormSections addSubview:lblTitleControl];
                
                NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                if (strIndexOfValue.length==0)
                {
                    // isValuePrsent=NO;
                    lblTitleControl.frame=CGRectMake(0, 0,0, 0);
                    
                }
                else
                {
                    
                    [arrOfControlsValurPrsent arrayByAddingObject:@"1"];
                    [arrValuePresentSection addObject:@"1"];
                    
                    isValuePrsent=YES;
                    isValuePrsentForSection=YES;
                    NSArray *arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    
                    if (!yPositionOfControls)
                    {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width-30, s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.font = [UIFont systemFontOfSize:15];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    
                    if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"]||[[dictControls valueForKey:@"Element"] isEqualToString:@"el-textbox"]) {
                        
                        CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                        UITextView *lblItem = [[UITextView alloc] init];
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, 65);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        lblItem.text =[dictControls valueForKey:@"Value"];
                        lblItem.editable=NO;
                        [view addSubview:lblItem];
                        //  view.backgroundColor=[UIColor redColor];
                        
                        // [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, lblItem.frame.size.height+12+20+50)];
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                        
                        
                    }
                    else if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]){
                        
                        NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                        NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                        NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                        
                        for (int k=0; k<arrOfValues.count; k++) {
                            
                            for (int l=0; l<arrOfOptions.count; l++) {
                                
                                NSDictionary *dictData=arrOfOptions[l];
                                NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                                
                                if ([strValue isEqualToString:arrOfValues[k]]) {
                                    
                                    if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]) {
                                        [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                        
                                    } else {
                                        [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Label"]]];
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@"\n \n"];
                        
                        CGSize s11 = [strTextView sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                        UILabel *lblItem = [[UILabel alloc] init];
                        if (s11.height<30) {
                            
                            s11.height=30;
                            
                        }
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =strTextView;//[dictControls valueForKey:@"Value"];
                        [view addSubview:lblItem];
                        
                        // [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, s11.height+12+20)];
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                        
                        
                    }
                    else
                    {
                        
                        CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                        UILabel *lblItem = [[UILabel alloc] init];
                        if (s11.height<30) {
                            
                            s11.height=30;
                            
                        }
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =[dictControls valueForKey:@"Value"];
                        [view addSubview:lblItem];
                        
                        // [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, s11.height+12+20)];
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                        
                        
                    }
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, lbl.frame.size.height+12+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
            }
            
            if (!isValuePrsent)
            {
                
                [ViewFormSections removeFromSuperview];
                
                [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrControls.count*0+0)];
                
                heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrControls.count*0+50-50;//10
                
                MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, heightMainViewFormSections+60);
                
                constantHeighttt=MainViewForm.frame.size.height+MainViewForm.frame.origin.y;
                
                _constViewInspection_H.constant=constantHeighttt;
                _const_ScrollAgreement_H.constant=_constViewInspection_H.constant+_const_ScrollAgreement_H.constant;
                //temp
                
                /*if(k==arrDynamicData.count-1)
                 {
                 if(isValuePrsentForSection==NO)
                 {
                 BtnMainView.backgroundColor=[UIColor clearColor];//[UIColor blackColor];
                 
                 [BtnMainView setTitle:@"" forState:UIControlStateNormal];
                 }
                 else
                 {
                 NSDictionary *dict=[arrDynamicData objectAtIndex:k];
                 BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
                 
                 [BtnMainView setTitle:[dict valueForKey:@"DepartmentName"] forState:UIControlStateNormal];
                 }
                 }*/
                /* if(arrValuePresentSection.count==0)
                 {
                 BtnMainView.backgroundColor=[UIColor clearColor];//[UIColor blackColor];
                 
                 [BtnMainView setTitle:@"" forState:UIControlStateNormal];
                 }*/
                /*if(isValuePrsentForSection==NO)
                 {
                 BtnMainView.backgroundColor=[UIColor clearColor];//[UIColor blackColor];
                 
                 [BtnMainView setTitle:@"" forState:UIControlStateNormal];
                 }
                 else
                 {
                 BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
                 
                 [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentName"] forState:UIControlStateNormal];
                 }*/
                
                // BtnMainView.backgroundColor=[UIColor clearColor];//[UIColor blackColor];
                
                //  [BtnMainView setTitle:@"" forState:UIControlStateNormal];
                //MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, MainViewForm.frame.size.height-60);
                
                
                
#pragma mark- TEMP COMMENT 3 NOV
            }
            else
            {
                isValuePrsent=NO;
                
                
                
                [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrOfControlsValurPrsent.count*10+50+100)];
                //heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrOfControlsValurPrsent.count*10+50;//10
                heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrOfControlsValurPrsent.count*10+50+100;//10
                
                
                MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, heightMainViewFormSections+60);
                
                
                BtnMainViewKiHeight=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
                
                yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+10;
                
                //constantHeighttt=MainViewForm.frame.size.height+constantHeighttt;
                
                constantHeighttt=MainViewForm.frame.size.height+MainViewForm.frame.origin.y;
                
                _constViewInspection_H.constant=constantHeighttt;
                
                _const_ScrollAgreement_H.constant=_constViewInspection_H.constant+_const_ScrollAgreement_H.constant;
                
                
            }
            if(arrValuePresentSection.count==0)
            {
                BtnMainView.backgroundColor=[UIColor clearColor];//[UIColor blackColor];
                
                [BtnMainView setTitle:@"" forState:UIControlStateNormal];
            }
            else
            {
                NSDictionary *dict=[arrDynamicData objectAtIndex:k];
                BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
                
                [BtnMainView setTitle:[dict valueForKey:@"DepartmentName"] forState:UIControlStateNormal];
            }
        }
        
        //        if (!isValuePrsentForSection)
        //        {
        //            [BtnMainView removeFromSuperview];
        //            [arrayOfButtonsMain removeLastObject];
        //            [ViewFormSections removeFromSuperview];
        //        }
        
        //yaha par logic laga na hai
        
    }
    
    
    if (arrDynamicData.count==0) {
        [MainViewForm removeFromSuperview];
        _constViewInspection_H.constant=80;
        _const_ScrollAgreement_H.constant=_constViewInspection_H.constant+_const_ScrollAgreement_H.constant;
        
    }
}
-(void)setDynamicDataOld{
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    NSArray *arrDynamicData=[defss valueForKey:@"salesDynamicForm"];
    int  val;
    val=0;
    
    int constantHeighttt=80.0;
    int BtnMainViewKiHeight=0.0;
    
    for (int k=0; k<arrDynamicData.count; k++) {
        
        CGFloat scrollViewHeight=0.0;
        
        NSDictionary *dictMain=arrDynamicData[k];
        
        UIButton *BtnMainView;
        
        if (!MainViewForm.frame.size.height) {
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, 60, [UIScreen mainScreen].bounds.size.width-20, 42)];
        }
        else{
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, BtnMainViewKiHeight-25, [UIScreen mainScreen].bounds.size.width-20, 42)];
        }
        //  [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        
        [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentName"] forState:UIControlStateNormal];
        
        //    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 40, 40)];
        //    imgView.image=[UIImage imageNamed:@"minus_icon.png"];
        
        //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
        //  [BtnMainView addSubview:imgView];
        
        [_viewInspection addSubview:BtnMainView];
        
        [arrayOfButtonsMain addObject:BtnMainView];
        
        // Form Creation
        MainViewForm=[[UIView alloc]init];
        
        MainViewForm.backgroundColor=[UIColor clearColor];
        
        MainViewForm.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, 1000);
        
        [_viewInspection addSubview:MainViewForm];
        
        NSArray *arrSections=[dictMain valueForKey:@"Sections"];
        
        
        int yAxisViewFormSections=0.0;
        int heightVIewFormSections=0.0;
        int heightMainViewFormSections=0.0;
        
        BOOL isValuePrsent,isValuePrsentForSection;
        isValuePrsent=NO;
        isValuePrsentForSection=NO;
        
        for (int j=0; j<arrSections.count; j++)
        {
            NSDictionary *dictSection=[arrSections objectAtIndex:j];
            NSArray *arrControls=[dictSection valueForKey:@"Controls"];
            
            // Section Creation Start
            heightVIewFormSections=0.0;
            ViewFormSections=[[UIView alloc]init];
            ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width-20, 400);
            
            ViewFormSections.backgroundColor=[UIColor whiteColor];
            UILabel *lblTitleSection=[[UILabel alloc]init];
            lblTitleSection.backgroundColor=[UIColor lightGrayColor];
            lblTitleSection.layer.borderWidth = 1.5f;
            lblTitleSection.layer.cornerRadius = 4.0f;
            lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
            
            lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-20, 30);
            lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]] ;
            lblTitleSection.textAlignment=NSTextAlignmentCenter;
            [ViewFormSections addSubview:lblTitleSection];
            
            ViewFormSections.layer.borderWidth = 1.5f;
            ViewFormSections.layer.cornerRadius = 4.0f;
            ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
            
            [MainViewForm addSubview:ViewFormSections];
            
            CGFloat xPositionOfControls= 0.0;
            CGFloat yPositionOfControls = 0.0;
            CGFloat heightOfControls= 0.0;
            CGFloat widthOfControls= 0.0;
            
            NSMutableArray *arrOfControlsValurPrsent=[[NSMutableArray alloc]init];
            
            
            for (int k=0; k<arrControls.count; k++)
            {
                NSDictionary *dictControls=[arrControls objectAtIndex:k];
                UILabel *lblTitleControl=[[UILabel alloc]init];
                lblTitleControl.backgroundColor=[UIColor blackColor];
                lblTitleControl.frame=CGRectMake(0, 30*k+35, [UIScreen mainScreen].bounds.size.width-20, 30);
                lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
                lblTitleControl.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
                lblTitleControl.textAlignment=NSTextAlignmentCenter;
                // [ViewFormSections addSubview:lblTitleControl];
                
                NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                if (strIndexOfValue.length==0)
                {
                    // isValuePrsent=NO;
                }
                else
                {
                    
                    [arrOfControlsValurPrsent arrayByAddingObject:@"1"];
                    
                    isValuePrsent=YES;
                    isValuePrsentForSection=YES;
                    NSArray *arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    
                    if (!yPositionOfControls)
                    {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width-30, s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.font = [UIFont systemFontOfSize:15];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    
                    if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"]||[[dictControls valueForKey:@"Element"] isEqualToString:@"el-textbox"]) {
                        
                        CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                        UITextView *lblItem = [[UITextView alloc] init];
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, 65);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        lblItem.text =[dictControls valueForKey:@"Value"];
                        lblItem.editable=NO;
                        [view addSubview:lblItem];
                        //  view.backgroundColor=[UIColor redColor];
                        
                        // [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, lblItem.frame.size.height+12+20+50)];
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                        
                        
                    }
                    else if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]){
                        
                        NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                        NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                        NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                        
                        for (int k=0; k<arrOfValues.count; k++) {
                            
                            for (int l=0; l<arrOfOptions.count; l++) {
                                
                                NSDictionary *dictData=arrOfOptions[l];
                                NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                                
                                if ([strValue isEqualToString:arrOfValues[k]]) {
                                    
                                    if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]) {
                                        [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                        
                                    } else {
                                        [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Label"]]];
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@"\n \n"];
                        
                        CGSize s11 = [strTextView sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                        UILabel *lblItem = [[UILabel alloc] init];
                        if (s11.height<30) {
                            
                            s11.height=30;
                            
                        }
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =strTextView;//[dictControls valueForKey:@"Value"];
                        [view addSubview:lblItem];
                        
                        // [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, s11.height+12+20)];
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                        
                        
                    }
                    else
                    {
                        
                        CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                        UILabel *lblItem = [[UILabel alloc] init];
                        if (s11.height<30) {
                            
                            s11.height=30;
                            
                        }
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =[dictControls valueForKey:@"Value"];
                        [view addSubview:lblItem];
                        
                        // [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, s11.height+12+20)];
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                        
                        
                    }
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, lbl.frame.size.height+12+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
            }
            
            if (!isValuePrsent)
            {
                
                /* BtnMainViewKiHeight=85;
                 
                 MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, 0);
                 constantHeighttt=80;
                 
                 // [BtnMainView removeFromSuperview];
                 if (arrayOfButtonsMain.count==0) {
                 
                 } else {
                 int countt=(int)arrayOfButtonsMain.count;
                 if (countt==0) {
                 UIButton *btnTempNew=arrayOfButtonsMain[countt];
                 [btnTempNew setHidden:YES];
                 } else {
                 UIButton *btnTempNew=arrayOfButtonsMain[countt-1];
                 [btnTempNew setHidden:YES];
                 }
                 [arrayOfButtonsMain removeLastObject];
                 }
                 [ViewFormSections removeFromSuperview];*/
                
                
                [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrControls.count*10+50)];
                
                heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrControls.count*10+50;//10
                
                MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, heightMainViewFormSections+60);
                
                BtnMainViewKiHeight=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
                
                yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+10;
                
                // constantHeighttt=MainViewForm.frame.size.height+constantHeighttt;
                
                constantHeighttt=MainViewForm.frame.size.height+MainViewForm.frame.origin.y;
                
                _constViewInspection_H.constant=constantHeighttt;
                
                _const_ScrollAgreement_H.constant=_constViewInspection_H.constant+_const_ScrollAgreement_H.constant;
                
#pragma mark- TEMP COMMENT 3 NOV
            }
            else
            {
                isValuePrsent=NO;
                
                [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrControls.count*10+50)];
                
                heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrControls.count*10+50;//10
                
                MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, heightMainViewFormSections+60);
                
                BtnMainViewKiHeight=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
                
                yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+10;
                
                // constantHeighttt=MainViewForm.frame.size.height+constantHeighttt;
                
                constantHeighttt=MainViewForm.frame.size.height+MainViewForm.frame.origin.y;
                
                _constViewInspection_H.constant=constantHeighttt;
                
                _const_ScrollAgreement_H.constant=_constViewInspection_H.constant+_const_ScrollAgreement_H.constant;
                
                
            }
        }
        
        //        if (!isValuePrsentForSection)
        //        {
        //            [BtnMainView removeFromSuperview];
        //            [arrayOfButtonsMain removeLastObject];
        //            [ViewFormSections removeFromSuperview];
        //        }
        
        //yaha par logic laga na hai
        
    }
    
    
    if (arrDynamicData.count==0) {
        [MainViewForm removeFromSuperview];
        _constViewInspection_H.constant=80;
        _const_ScrollAgreement_H.constant=_constViewInspection_H.constant+_const_ScrollAgreement_H.constant;
        
    }
}


//Nilind 24 Oct

-(void)savePaymentInfo
{
    // For Payment Info
    //PaymentInfo Entity
    
    
    
    entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    PaymentInfo *objentityPaymentInfo = [[PaymentInfo alloc]initWithEntity:entityPaymentInfo insertIntoManagedObjectContext:context];
    
    objentityPaymentInfo.leadId=strLeadId;
    
    objentityPaymentInfo.leadPaymentDetailId=@"";
    objentityPaymentInfo.paymentMode=strPaymentMode;
    objentityPaymentInfo.amount=strAmount;
    objentityPaymentInfo.checkNo= strChequeNo;
    objentityPaymentInfo.licenseNo=strLicenseNo;
    
    objentityPaymentInfo.expirationDate=strDate;
    objentityPaymentInfo.specialInstructions=_txtViewAdditionalNotes.text;
    objentityPaymentInfo.agreement=@"";
    objentityPaymentInfo.proposal=@"";
    
    if (chkCustomerNotPresent) {
        
        objentityPaymentInfo.customerSignature=@"";
        
    } else {
        
        objentityPaymentInfo.customerSignature=strCustomerSignature;
        
    }
    
    //objentityPaymentInfo.customerSignature=strCustomerSignature;
    objentityPaymentInfo.salesSignature=strSalesSignature;
    
    //For Preest
    NSUserDefaults *defsSign=[NSUserDefaults standardUserDefaults];
    NSString *strSignUrl=[defsSign valueForKey:@"ServiceTechSignPath"];
    //BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignService"];
    BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignSales"];
    if ((isPreSetSign ==YES) && (strSignUrl.length>0))
    {
        //NSString *thePresetFileName =[strSignUrl lastPathComponent];
        strSignUrl=[strSignUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        NSString *result;
        NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=strSignUrl;
        }
        objentityPaymentInfo.salesSignature=result;
        
    }
    objentityPaymentInfo.createdBy=@"";
    objentityPaymentInfo.createdDate=@"";
    objentityPaymentInfo.modifiedBy=@"";
    objentityPaymentInfo.modifiedDate=[global modifyDate];
    
    //Nilind 16 Nov
    
    objentityPaymentInfo.companyKey=strCompanyKey;
    objentityPaymentInfo.userName=strUserName;
    
    if ([strPaymentMode isEqualToString:@"Check"]) {
        
        //objentityPaymentInfo.paidAmount=_txtAmount.text;
        //objentityPaymentInfo.checkNo=_txtCheckNo.text;
        //objentityPaymentInfo.drivingLicenseNo=@"";
        //objentityPaymentInfo.expirationDate=_btnCheckExpDate.titleLabel.text;
        if (!(arrOFImagesName.count==0)) {
            objentityPaymentInfo.checkFrontImagePath=arrOFImagesName[0];
        }else{
            objentityPaymentInfo.checkFrontImagePath=@"";
        }
        if (!(arrOfCheckBackImage.count==0)) {
            objentityPaymentInfo.checkBackImagePath=arrOfCheckBackImage[0];
        }else{
            objentityPaymentInfo.checkBackImagePath=@"";
        }
    }
    else
    {
        
        // objentityPaymentInfo.paidAmount=_txtAmountSingle.text;
        //  objentityPaymentInfo.checkNo=@"";
        //objentityPaymentInfo.drivingLicenseNo=@"";
        // objentityPaymentInfo.expirationDate=@"";
        objentityPaymentInfo.checkFrontImagePath=@"";
        objentityPaymentInfo.checkBackImagePath=@"";
        
    }
    
    
    //.............
    
    NSError *error1;
    [context save:&error1];
    
    //........................................................................
}
//...................
- (IBAction)actionOnDynamicFormButton:(id)sender
{
    _constViewInspection_H.constant=_constViewInspection_H.constant+20;
    _const_ScrollAgreement_H.constant=_constViewInspection_H.constant+_const_ScrollAgreement_H.constant;
}

//Nilind 10 Nov

-(void)disableForCompleteLead
{
    //_txtViewTerms.editable=NO;
    _txtPaidAmountPriceInforamtion.enabled=NO;
    _txtChequeValue.enabled=NO;
    _txtExpirationDate.enabled=NO;
    _txtLicenseValue.enabled=NO;
    _txtViewAdditionalNotes.editable=NO;
    
    _btnCash.enabled=NO;
    _btnCheck.enabled=NO;
    _btnCredit.enabled=NO;
    _btnAutoChangeCustomer.enabled=NO;
    _btnCollectTimeOfScheduling.enabled=NO;
    _btnCheckBox.enabled=NO;
    _btnInspector.enabled=NO;
    _btnCustomer.enabled=NO;
    
    //Nilind 5 Jan
    _btnIAgree.enabled=NO;
    //..........
    _btnExpirationDate.enabled=NO;
    
    _btnMarkAsLost.hidden=YES;
    
    _btnAddCredit.enabled=NO;
    _btnApplyCoupon.enabled=NO;
    _btnSelectCredit.enabled=NO;
    
    // commercial elements
    _txtViewIntroductionLetter.editable = NO;
    _btnIntroductionLetter.userInteractionEnabled = NO;
    
    _btnCoverLetter.userInteractionEnabled = NO;
    
    _btnTermsConditions.userInteractionEnabled = NO;
    
    _btnTermsOfServices.userInteractionEnabled = NO;
    _txtViewTermsServices.editable = NO;
    
    _btnAgreementValidFor.userInteractionEnabled = NO;
    
    _btnTaxApplicableInitialPrice.userInteractionEnabled = NO;
    _btnTaxApplicableMaintPrice.userInteractionEnabled = NO;
    
    _btnTaxApplicableInitialPriceClarkPest.userInteractionEnabled = NO;
    _btnTaxApplicableMaintPriceClarkPest.userInteractionEnabled = NO;
    
    _txtDays.enabled = NO;
    _txtTipDiscountClarkPest.enabled = NO;
    _txtOtherDiscountClarkPest.enabled = NO;
    _txtViewInternalNotes.editable=YES;
    _txtLeadInspectionFee.enabled = NO;
    
}

//Nilind

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    // Prevent crashing undo bug – see note below.
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField==_txtChequeValue)
    {
        return  newLength <= 60;
        
    }
    else if (textField==_txtPaidAmountPriceInforamtion)
    {
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtPaidAmountPriceInforamtion.text];
        
        if (isNuberOnly) {
            
            return YES;
        } else {
            return NO;
        }
        
        // return  newLength <= 15;
        
    }
    else if(textField == _txtDays)
    {
        if(string.length > 0)
        {
            NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if(newLength <= 2)
            {
                BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
                return stringIsValid;
            }
            else
            {
                return false;
            }
        }
    }
    else if (textField == _txtOtherDiscountClarkPest)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtOtherDiscountClarkPest.text];
        if (isNuberOnly)
        {
            
            if ([string isEqualToString:@""])
            {
                // double initial,discountPer;
                NSString *strDiscount;
                strDiscount=[_txtOtherDiscountClarkPest.text stringByReplacingCharactersInRange:range withString:string];
                
                if (([strDiscount floatValue] + [_txtTipDiscountClarkPest.text floatValue])  > subtotalInitialPriceClarkPest)
                {
                    //[global AlertMethod:@"Alert!":@"Discount can't be greater than subtotal amount"];
                    return NO;
                }
                
                
                float totalInitial =0.0;
                
                totalInitial = subtotalInitialPriceClarkPest - [strDiscount floatValue] - [_txtTipDiscountClarkPest.text floatValue]+ [_txtLeadInspectionFee.text floatValue];
                
                if (totalInitial < 0)
                {
                    totalInitial = 0.0 ;
                }
                
                _lblTotalPriceInitialClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                
                //Tax Code
                
                float taxAmountInitial = [self calculateTaxableAmountInitialPrice:strDiscount TipDiscount:_txtTipDiscountClarkPest.text LeadInspectionFee:_txtLeadInspectionFee.text];
                
                
                _lblTaxAmountInitialPrice.text = [NSString stringWithFormat:@"%.2f",taxAmountInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",(totalInitial + taxAmountInitial)];
                
                //End
                
            }
            else
            {
                NSString *strDiscount;
                strDiscount=[NSString stringWithFormat:@"%@%@",_txtOtherDiscountClarkPest.text,string];
                
                
                if (([strDiscount floatValue] + [_txtTipDiscountClarkPest.text floatValue])  > subtotalInitialPriceClarkPest)
                {
                    //[global AlertMethod:@"Alert!":@"Discount can't be greater than subtotal amount"];
                    return NO;
                }
                
                float totalInitial =0.0;
                
                totalInitial = subtotalInitialPriceClarkPest - [strDiscount floatValue] - [_txtTipDiscountClarkPest.text floatValue]+ [_txtLeadInspectionFee.text floatValue];
                
                if (totalInitial < 0)
                {
                    totalInitial = 0.0 ;
                }
                
                _lblTotalPriceInitialClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                
                //Tax Code
                
                float taxAmountInitial = [self calculateTaxableAmountInitialPrice:strDiscount TipDiscount:_txtTipDiscountClarkPest.text LeadInspectionFee:_txtLeadInspectionFee.text];
                
                _lblTaxAmountInitialPrice.text = [NSString stringWithFormat:@"%.2f",taxAmountInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",(totalInitial + taxAmountInitial)];
                
                //End
            }
            
            return YES;
        }
        else
        {
            
            return NO;
        }
    }
    else if (textField == _txtTipDiscountClarkPest)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtTipDiscountClarkPest.text];
        if (isNuberOnly)
        {
            
            if ([string isEqualToString:@""])
            {
                // double initial,discountPer;
                NSString *strDiscount;
                strDiscount=[_txtTipDiscountClarkPest.text stringByReplacingCharactersInRange:range withString:string];
                
                
                if (([strDiscount floatValue] + [_txtOtherDiscountClarkPest.text floatValue])  > subtotalInitialPriceClarkPest)
                {
                    return NO;
                }
                
                
                float totalInitial =0.0;
                
                totalInitial = subtotalInitialPriceClarkPest - [strDiscount floatValue] - [_txtOtherDiscountClarkPest.text floatValue]+ [_txtLeadInspectionFee.text floatValue];
                
                if (totalInitial < 0)
                {
                    totalInitial = 0.0 ;
                }
                
                
                _lblTotalPriceInitialClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                //Tax Code
                
                float taxAmountInitial = [self calculateTaxableAmountInitialPrice:_txtOtherDiscountClarkPest.text TipDiscount:strDiscount LeadInspectionFee:_txtLeadInspectionFee.text];
                
                _lblTaxAmountInitialPrice.text = [NSString stringWithFormat:@"%.2f",taxAmountInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",(totalInitial + taxAmountInitial)];
                
                //End
                
            }
            else
            {
                NSString *strDiscount;
                strDiscount=[NSString stringWithFormat:@"%@%@",_txtTipDiscountClarkPest.text,string];
                
                if (([strDiscount floatValue] + [_txtOtherDiscountClarkPest.text floatValue])  > subtotalInitialPriceClarkPest)
                {
                    return NO;
                }
                
                float totalInitial =0.0;
                
                totalInitial = subtotalInitialPriceClarkPest - [strDiscount floatValue] - [_txtOtherDiscountClarkPest.text floatValue] + [_txtLeadInspectionFee.text floatValue];
                
                if (totalInitial < 0)
                {
                    totalInitial = 0.0 ;
                }
                
                _lblTotalPriceInitialClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                //Tax Code
                
                float taxAmountInitial = [self calculateTaxableAmountInitialPrice:_txtOtherDiscountClarkPest.text TipDiscount:strDiscount LeadInspectionFee:_txtLeadInspectionFee.text];
                
                _lblTaxAmountInitialPrice.text = [NSString stringWithFormat:@"%.2f",taxAmountInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",(totalInitial + taxAmountInitial)];
                
                //End
            }
            
            return YES;
        }
        else
        {
            
            return NO;
        }
    }
    else if (textField == _txtLeadInspectionFee)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtLeadInspectionFee.text];
        
        if (isNuberOnly)
        {
            
            if ([string isEqualToString:@""])
            {
                // double initial,discountPer;
                
                NSString *strLeadInspectionFee;
                strLeadInspectionFee=[_txtLeadInspectionFee.text stringByReplacingCharactersInRange:range withString:string];
                
                double totalAmount = 0.0;
                
                totalAmount = subtotalInitialPriceClarkPest ;//- ([_txtTipDiscountClarkPest.text floatValue] + [_txtOtherDiscountClarkPest.text floatValue]);
                
                
                /*if ([strLeadInspectionFee floatValue] > totalAmount)
                 {
                 return NO;
                 }*/
                
                
                float totalInitial =0.0;
                
                totalInitial = totalAmount + [strLeadInspectionFee floatValue] - [_txtOtherDiscountClarkPest.text floatValue] - [_txtTipDiscountClarkPest.text floatValue];
                
                if (totalInitial < 0)
                {
                    totalInitial = 0.0 ;
                }
                
                _lblTotalPriceInitialClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                //Tax Code
                
                float taxAmountInitial = [self calculateTaxableAmountInitialPrice:_txtOtherDiscountClarkPest.text TipDiscount:_txtTipDiscountClarkPest.text LeadInspectionFee:strLeadInspectionFee];
                
                _lblTaxAmountInitialPrice.text = [NSString stringWithFormat:@"%.2f",taxAmountInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",(totalInitial + taxAmountInitial)];
                
                //End
                
                
            }
            else
            {
                // double initial,discountPer;
                
                NSString *strLeadInspectionFee;
                strLeadInspectionFee=[NSString stringWithFormat:@"%@%@",_txtLeadInspectionFee.text,string];
                
                
                
                double totalAmount = 0.0;
                totalAmount = subtotalInitialPriceClarkPest;
                // - ([_txtTipDiscountClarkPest.text floatValue] + [_txtOtherDiscountClarkPest.text floatValue]);
                
                
                /*if ([strLeadInspectionFee floatValue] > totalAmount)
                 {
                 return NO;
                 }*/
                
                
                float totalInitial =0.0;
                
                totalInitial = totalAmount + [strLeadInspectionFee floatValue] - [_txtOtherDiscountClarkPest.text floatValue] - [_txtTipDiscountClarkPest.text floatValue];
                
                if (totalInitial < 0)
                {
                    totalInitial = 0.0 ;
                }
                
                _lblTotalPriceInitialClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitial];
                
                //Tax Code
                
                float taxAmountInitial = [self calculateTaxableAmountInitialPrice:_txtOtherDiscountClarkPest.text TipDiscount:_txtTipDiscountClarkPest.text LeadInspectionFee:strLeadInspectionFee];
                
                _lblTaxAmountInitialPrice.text = [NSString stringWithFormat:@"%.2f",taxAmountInitial];
                
                _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",(totalInitial + taxAmountInitial)];
                
                //End
                
            }
            
            return YES;
        }
        else
        {
            
            return NO;
        }
    }
    else
    {
        return  newLength <= 60;
    }
    return YES;
}

//...........
//Nilind 27 Dec
-(void)updateLeadIdDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        matches=arrAllObj[0];
        
        NSArray *uniqueArray = [[NSOrderedSet orderedSetWithArray:arrPreferredMonths] array];
        [matches setValue:[uniqueArray componentsJoinedByString:@","] forKey:@"strPreferredMonth"];
        
        float tipDiscount = [_txtTipDiscountClarkPest.text floatValue];
        if (tipDiscount < 0)
        {
            tipDiscount = 0.0;
        }
        float otherDiscount = [_txtOtherDiscountClarkPest.text floatValue];
        if (otherDiscount < 0)
        {
            otherDiscount = 0.0;
        }
        
        float leadInspectionFee = [_txtLeadInspectionFee.text floatValue];
        if (leadInspectionFee < 0)
        {
            leadInspectionFee = 0.0;
        }
        
        
        [matches setValue:[NSString stringWithFormat:@"%.2f",tipDiscount] forKey:@"tipDiscount"];
        [matches setValue:[NSString stringWithFormat:@"%.2f",otherDiscount] forKey:@"otherDiscount"];
        [matches setValue:[NSString stringWithFormat:@"%.2f",leadInspectionFee] forKey:@"leadInspectionFee"];
        [matches setValue:_txtViewInternalNotes.text forKey:@"notes"];
        
        //  [matches setValue:[arrPreferredMonths componentsJoinedByString:@","] forKey:@"strPreferredMonth"];
        [context save:&error1];
    }
    
}
-(void)updateLeadIdDetailOld
{
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
    }
    else
    {
        [self saveImageToCoreData];
        [self saveNewAgreementDetails];
    }
    
    
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        
        matches=arrAllObj[0];
        if ([strPaymentMode isEqualToString:@"CreditCard"]||[strPaymentMode isEqualToString:@"Credit Card"])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            BOOL isElementIntegration;
            isElementIntegration=NO;
            isElementIntegration=[[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsElementIntegration"]]boolValue];
            if (isElementIntegration==NO)
            {
                [matches setValue:@"Complete" forKey:@"statusSysName"];
            }
        }
        else
        {
            [matches setValue:@"Complete" forKey:@"statusSysName"];
        }
        
        //[matches setValue:@"InComplete" forKey:@"statusSysName"];
        if (strGlobalAudio.length>0)
        {
            [matches setValue:strGlobalAudio forKey:@"audioFilePath"];
        }else{
            
            [matches setValue:@"" forKey:@"audioFilePath"];
            
        }
        //Nilind 05 Jan
        [matches setValue:@"true" forKey:@"iAgreeTerms"];
        
        NSString *str;
        str=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCustomerNotPresent"]];
        
        if (chkCustomerNotPresent==YES)
        {
            [matches setValue:@"true" forKey:@"isCustomerNotPresent"];
            
        }
        else
        {
            [matches setValue:@"false" forKey:@"isCustomerNotPresent"];
        }
        
        if ([_imgViewCustomerSignature.image isEqual:[UIImage imageNamed:@"NoImage.jpg"]]) {
            
            [matches setValue:@"false" forKey:@"isAgreementSigned"];
            [matches setValue:@"false" forKey:@"isAgreementGenerated"];
            
            
        }else{
            
            [matches setValue:@"true" forKey:@"isAgreementSigned"];
            [matches setValue:@"true" forKey:@"isAgreementGenerated"];
            
            
        }
        
        if (isPreSetSignGlobal) {
            
            [matches setValue:@"true" forKey:@"isEmployeePresetSignature"];
            
        } else {
            
            [matches setValue:@"false" forKey:@"isEmployeePresetSignature"];
            
        }
        
        ////Yaha Par zSyn ko yes karna hai
        [matches setValue:@"yes" forKey:@"zSync"];
        
        NSString *strFinalBilledAmount,*strFinalCollectedAmount,*strFinalDiscount,*strFinalSubTotal;
        
        //strFinalBilledAmount=[NSString stringWithFormat:@"%@",_lblBillingAmountPriceInformation.text ];
        strFinalBilledAmount=[NSString stringWithFormat:@"%@",_lblNewInitialPriceBillingAmount.text ];
        
        
        //strFinalDiscount=[NSString stringWithFormat:@"%@",_lblCouponDiscountValuePriceInformation.text ];
        float discountTotalTemp=0.0;
        discountTotalTemp=[[NSString stringWithFormat:@"%@",_lblNewInitialPriceCouponDiscount.text ]floatValue]+[[NSString stringWithFormat:@"%@",_lblNewInitialPriceOtherDiscount.text ]floatValue];
        strFinalDiscount=[NSString stringWithFormat:@"%.2f",discountTotalTemp ];
        
        
        
        // strFinalSubTotal=[NSString stringWithFormat:@"%@",_lblSubtotalAmountValuePriceInformation.text ];
        
        strFinalSubTotal=[NSString stringWithFormat:@"%@",_lblNewInitialPriceSubtotalAmount.text ];
        
        
        if ([strPaymentMode caseInsensitiveCompare:@"Cash"]== NSOrderedSame||[strPaymentMode caseInsensitiveCompare:@"Check"]== NSOrderedSame||[strPaymentMode caseInsensitiveCompare:@"CreditCard"]== NSOrderedSame)
        {
            
            strFinalCollectedAmount=[NSString stringWithFormat:@"%@",_txtPaidAmountPriceInforamtion.text ];
        }
        else
        {
            strFinalCollectedAmount=@"0";
        }
        /*[matches setValue:strFinalBilledAmount forKey:@"billedAmount"];
         [matches setValue:strFinalDiscount forKey:@"couponDiscount"];
         [matches setValue:strFinalCollectedAmount forKey:@"collectedAmount"];
         [matches setValue:strFinalSubTotal forKey:@"subTotalAmount"];
         
         [matches setValue:strFinalTaxabelAmount forKey:@"taxableAmount"];
         [matches setValue:strFinalTaxAmount forKey:@"taxAmount"];
         [matches setValue:strFinalTotalPrice forKey:@"totalPrice"];*/
        
        
        [matches setValue:strFinalBilledAmount forKey:@"billedAmount"];
        [matches setValue:strFinalDiscount forKey:@"couponDiscount"];
        [matches setValue:strFinalCollectedAmount forKey:@"collectedAmount"];
        [matches setValue:strFinalSubTotal forKey:@"subTotalAmount"];
        
        [matches setValue:strFinalTaxabelAmount forKey:@"taxableAmount"];
        [matches setValue:strFinalTaxAmount forKey:@"taxAmount"];
        [matches setValue:strFinalTotalPrice forKey:@"totalPrice"];
        [matches setValue:strTaxableAmountMaint forKey:@"taxableMaintAmount"];
        [matches setValue:strTaxAmountMaint forKey:@"taxMaintAmount"];
        
        
        //[matches setValue:@"1234" forKey:@"linearSqFt"];
        
        //New Change 07 Feb
        
        [matches setValue:@"Won" forKey:@"stageSysName"];
        
        if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"]||_btnIAgree.hidden==YES)
        {
            [matches setValue:@"Open" forKey:@"statusSysName"];
            [matches setValue:@"Proposed" forKey:@"stageSysName"];
        }
        if (chkCustomerNotPresent==YES)
        {
            [matches setValue:@"CompletePending" forKey:@"stageSysName"];
            if ([strPaymentMode isEqualToString:@"CreditCard"]||[strPaymentMode isEqualToString:@"Credit Card"])
            {
            }
            else
            {
                [matches setValue:@"Complete" forKey:@"statusSysName"];
            }
            
        }
        if(chkForLost==YES)
        {
            [matches setValue:@"Lost" forKey:@"stageSysName"];
            [matches setValue:@"Complete" forKey:@"statusSysName"];
            
            
        }
        
        /*int sumPreferredMonths=0;
         for (int i=0; i<arrPreferredMonths.count; i++)
         {
         sumPreferredMonths=sumPreferredMonths+[[arrPreferredMonths objectAtIndex:i]intValue];
         }
         */
        
        NSArray *uniqueArray = [[NSOrderedSet orderedSetWithArray:arrPreferredMonths] array];
        [matches setValue:[uniqueArray componentsJoinedByString:@","] forKey:@"strPreferredMonth"];
        // [matches setValue:[arrPreferredMonths componentsJoinedByString:@","] forKey:@"strPreferredMonth"];
        
        
        
        
    }
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
    }
    else
    {
        // [self updateLeadAppliedDiscountForAppliedCoupon];
    }
    //[matches setValue:_lblNewMaintPriceTotalPrice.text       forKey:@"subTotalMaintAmount"];
    NSString *strSubTotalMaintAmount,*strTotalMaintAmount;
    
    if(_lblNewMaintPriceSubtotalAmount.text.length>0)
    {
        strSubTotalMaintAmount = _lblNewMaintPriceSubtotalAmount.text;
    }
    else
    {
        strSubTotalMaintAmount = @"";
    }
    
    if(_lblNewMaintPriceTotalPrice.text.length>0)
    {
        strTotalMaintAmount = _lblNewMaintPriceTotalPrice.text;
    }
    else
    {
        strTotalMaintAmount = @"";
    }
    
    [matches setValue:strSubTotalMaintAmount forKey:@"subTotalMaintAmount"];
    [matches setValue:strTotalMaintAmount forKey:@"totalMaintPrice"];
    
    [context save:&error1];
}


- (IBAction)actionOnInitialSetup:(id)sender
{
    if([_btnInitialSetup.titleLabel.text isEqualToString:@"Initial Setup"])
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"fromAgreementInitialSetup"];
        [defs setValue:@"forFollowUp" forKey:@"FollowUp"];
        [defs synchronize];
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
        InitialSetUp *objInitialSetUp=[storyBoard instantiateViewControllerWithIdentifier:@"InitialSetUp"];
        [self.navigationController pushViewController:objInitialSetUp animated:NO];
        
    }
    else //For Generate Workorder
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"fromGenerateWorkorder" forKey:@"fromGenerateWorkorder"];
        [defs synchronize];
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
        GenerateWorkOrder *objGenerateWorkOrder=[storyBoard instantiateViewControllerWithIdentifier:@"GenerateWorkOrder"];
        [self.navigationController pushViewController:objGenerateWorkOrder animated:NO];
    }
    
}

- (IBAction)actionOnCancel:(id)sender
{
    [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isFromBackServiceSummary"];//@"isFromBackAgreement"];
    [defs synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    /* if (_btnInitialSetup.hidden==NO)
     {
     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];
     AppointmentView *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
     [self.navigationController pushViewController:objByProductVC animated:NO];
     }
     else
     {
     for (UIViewController *controller in self.navigationController.viewControllers)
     {
     if ([controller isKindOfClass:[ClarkPestSalesSelectServiceiPhone class]])
     {
     [self.navigationController popToViewController:controller animated:NO];
     break;
     }
     }
     
     
     }*/
}
//.......................................................

//Nilind 04 Jan

-(NSString *)fetchLead
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSString *str;
    if (arrAllObj12.count==0)
    {
        
    }
    else
    {
        
        NSManagedObject *matches12=arrAllObj12[0];
        
        str=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"isInitialSetupCreated"]];//isInitialSetupCreated
        strLeadStatusGlobal = [NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]];
        
        strStatus=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]];
        
        
        strStageSysName=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"stageSysName"]];
        strStageStatus = [NSString stringWithFormat:@"%@",[matches12 valueForKey:@"stageSysName"]];
        
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            
        {
            
            [_btnRecordAudio setEnabled:NO];
            
        }
        else
        {
            
            [_btnRecordAudio setEnabled:YES];
            
        }
        
        //Nilind 05 Jan
        
        NSString *strChk;
        strChk=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"iAgreeTerms"]];
        if ([strChk isEqualToString:@"true"])
        {
            chkIAgree=YES;
        }
        else
        {
            chkIAgree=NO;
            
        }
        
        //..........
        
        //End
        NSString *strPrintBuntton;
        strPrintBuntton=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"isResendAgreementProposalMail"]];
        if([strPrintBuntton isEqualToString:@"true"]||[strPrintBuntton isEqualToString:@"True"])
        {
            _btnPrintAgreement.hidden=NO;
        }
        else
        {
            _btnPrintAgreement.hidden=YES;
            
        }
        
        if([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
        {
            _btnPrintAgreement.hidden=YES;
            
        }
        if(([strStageSysName isEqualToString:@"Proposed"]||[strStageSysName isEqualToString:@"Proposed"])&&([[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]] isEqualToString:@"Open"]||[[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]] isEqualToString:@"open"]))
        {
            _btnPrintAgreement.hidden=NO;
            
        }
        
    }
    return str;
    
}
-(void)fetchSoldServiceCount
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj1 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrSoldServiceCount=[[NSMutableArray alloc]init];
    arrStanUnSoldCount=[[NSMutableArray alloc]init];
    for (int i=0; i<arrAllObj1.count; i++)
    {
        NSManagedObject *matches12=[arrAllObj1 objectAtIndex:i];
        if([[matches12 valueForKey:@"isSold"] isEqualToString:@"true"])
        {
            
            [arrSoldServiceCount addObject:[NSString stringWithFormat :@"%d",i]];
        }
        else
        {
            [arrStanUnSoldCount addObject:[NSString stringWithFormat :@"%d",i]];
        }
    }
}

- (IBAction)action_RecordAudio:(id)sender {
    
    LandingSong=nil;
    if (LandingSong.isPlaying)
    {
        
    }
    else
    {
        [LandingSong stop];
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isfirstTimeAudio=[defs boolForKey:@"firstAudio"];
    
    if (isfirstTimeAudio) {
        
        [defs setBool:NO forKey:@"firstAudio"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        RecordAudioView
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioView"];
        objByProductVC.strFromWhere=@"SalesInvoice";
        [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
        
        
    } else {
        
        BOOL isAudioPermissionAvailable=[global isAudioPermissionAvailable];
        
        if (isAudioPermissionAvailable) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            RecordAudioView
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioView"];
            objByProductVC.strFromWhere=@"SalesInvoice";
            [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
            
            
        }else{
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:AlertAudioVideoPermission //@"Audio Permission not allowed.Please go to settings and allow"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                
                
                
            }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                
                if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    [[UIApplication sharedApplication] openURL:url];
                } else {
                    
                    //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //                                     [alert show];
                    
                }
                
            }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}
- (IBAction)action_PlayAudio:(id)sender {
    
    if ([_btnPlayAudio.titleLabel.text isEqualToString:@"Play"]) {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        BOOL yesAudio=[defs boolForKey:@"yesAudio"];
        if (yesAudio) {
            [_btnPlayAudio setTitle:@"Stop" forState:UIControlStateNormal];
            //  _lblAudioStatus.text=@"Audio Available";
            //[defs setBool:NO forKey:@"yesAudio"];
            [defs synchronize];
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            NSString *strAudioName=[defs valueForKey:@"AudioNameService"];
            [self playLandingAudio:strAudioName];
        }else{
            // _lblAudioStatus.text=@"Audio Not Available";
            [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
        }
    } else {
        [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
        // [LandingSong stop];
        LandingSong=nil;
        if (LandingSong.isPlaying)
        {
            
        }
        else
        {
            [LandingSong stop];
        }
    }
    
}

//============================================================================
#pragma mark- Play Audio
//============================================================================

-(void)playLandingAudio :(NSString *)strAudioName
{
    // NSString *result;
    
    strAudioName=[global strNameBackSlashIssue:strAudioName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    
    [global playAudio:path :self.view];
    
    /*
     // NSString *sounfiles1=[[NSBundle mainBundle]pathForResource:@"Flag_Intro Audio" ofType:@"mp3"];
     NSURL *url=[NSURL fileURLWithPath:path];
     LandingSong=[[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
     LandingSong.delegate = self;
     [LandingSong play];
     if (LandingSong.isPlaying)
     {
     
     }
     else
     {
     [LandingSong stop];
     }
     */
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
    
}


//============================================================================
//============================================================================
#pragma mark -----------------------Download Audio-------------------------------
//============================================================================
//============================================================================

-(void)downloadingAudio :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strSalesssUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSString *strUrl = [NSString stringWithFormat:@"%@/documents/%@",strSalesssUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        // [self ShowFirstImage :str];
        // _lblAudioStatus.text=@"Audio Available";
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:result forKey:@"AudioNameService"];
        [defs synchronize];
        
    } else {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        // NSURL *url = [NSURL URLWithString:strNewString];
        
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *audioData = [NSData dataWithContentsOfURL:photoURL];
        [self saveAudioAfterDownload1:audioData :result];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:result forKey:@"AudioNameService"];
        [defs setBool:YES forKey:@"yesAudio"];
        [defs synchronize];
        
        // _lblAudioStatus.text=@"Audio Available";
    }
}
- (void)saveAudioAfterDownload1: (NSData*)audioData :(NSString*)name {
    
    name=[global strNameBackSlashIssue:name];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    [audioData writeToFile:path atomically:YES];
    
}

//Nilind 5 Jan
- (IBAction)actionOnIAgree:(id)sender
{
    if(arrSelectedMultipleCondition.count==0)
    {
        [global displayAlertController:@"Alert!" :@"Please select Terms and Conditions" :self];
        //Please select Terms and Conditions
    }
    else
    {
        isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        UIButton *btn=(UIButton *)sender;
        if([btn.currentImage isEqual:[UIImage imageNamed:@"check_box_1.png"]])
        {
            
            [_btnIAgree setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            btn.selected=YES;
            chkIAgree=YES;
            
        }
        else
        {
            [_btnIAgree setImage:[UIImage imageNamed:@"check_box_1.png"]forState:UIControlStateNormal];
            btn.selected=NO;
            chkIAgree=NO;
            
        }
    }
    
}
/*{
 isEditedInSalesAuto=YES;
 NSLog(@"Global mopdify date set to YES");
 UIButton *btn=(UIButton *)sender;
 if (btn.selected==NO)
 {
 
 [_btnIAgree setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
 btn.selected=YES;
 chkIAgree=YES;
 
 }
 else
 {
 [_btnIAgree setImage:[UIImage imageNamed:@"check_box_1.png"]forState:UIControlStateNormal];
 btn.selected=NO;
 chkIAgree=NO;
 
 }
 // [self updateLeadDetailForIAgree];
 
 }*/

- (IBAction)actionOnGlobalSync:(id)sender
{
    [self endEditing];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"Alert!":@"No Internet connection available"];
    }
    else
    {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
        [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];
    }
    
}
-(void)methodSync
{
    GlobalSyncViewController *objGlobalSyncViewController=[[GlobalSyncViewController alloc]init];
    NSString *str=@"general";
    
    [objGlobalSyncViewController syncCall:str];
}
-(void)stopLoader
{
    [DejalBezelActivityView removeView];
}


-(void)updateLeadDetailForIAgree
{
    
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arr;
    arr = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesNew;
    if (arr.count==0)
    {
        
    }else
    {
        
        matchesNew=arr[0];
        [matchesNew setValue:@"true" forKey:@"iAgreeTerms"];
        
        [context save:&error1];
    }
}

-(void)heightMaintenanceTable
{
    [self setHeight];
    _const_ViewPreferredMonth_H.constant=220;
    
    _cnstrntTblMaintenance_H.constant=100+24;
    _cnstrntTblMaintenance_H.constant=(_cnstrntTblMaintenance_H.constant* [arrStanInitialPrice count]);
    
    //Bundle
    int  heightTempBundle=0;
    for (int i=0; i<arrBundleRow.count; i++)
    {
        [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:i]];
        for (int j=0; j<arrSysNameBundle.count; j++)
        {
            heightTempBundle=heightTempBundle+_tblBundle.rowHeight+35;
            
        }
    }
    _constTblBundle_H.constant=heightTempBundle;
    //End
    
    _cnsrtViewMaintainService_H.constant= _constTblBundle_H.constant+ _cnstrntTblMaintenance_H.constant+200+100;
    
    if(arrStanInitialPrice.count==0)
    {
        //_cnsrtViewMaintainService_H.constant=0;
    }
    //_viewMaintainanceService.backgroundColor=[UIColor redColor];
    //_tblMaintenance.backgroundColor=[UIColor yellowColor];
    //_tblBundle.backgroundColor=[UIColor purpleColor];
    
    //For Agreement View
    
    
    //For Agreement View
    if (arrayAgreementCheckList.count>0)
    {
        _const_TableAgreement_H.constant=55;
        _const_TableAgreement_H.constant=(_const_TableAgreement_H.constant* arrayAgreementCheckList.count);
        _const_ViewAgreement_H.constant=_const_TableAgreement_H.constant+50;
    }
    else
    {
        _const_TableAgreement_H.constant=0;
        _const_TableAgreement_H.constant=0;
        _const_ViewAgreement_H.constant=0;
        
    }
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        _const_TableAgreement_H.constant=0;
        _const_TableAgreement_H.constant=0;
        _const_ViewAgreement_H.constant=0;
        _const_ViewPreferredMonth_H.constant=0;
        
        
    }
    
    [self heightCreditCoupon];
    
    _cnsrtViewMaintainService_H.constant=0;
    _cnstrntViewNonStandard_H.constant=0;
    
    if(isPreferredMonths==NO)
    {
        _const_ViewPreferredMonth_H.constant=0;
        
    }
    
}
-(void)heightCreditCoupon
{
    NSMutableArray* arrDiscountCreditInitialTemp=[[NSMutableArray alloc]init];
    for(int i=0; i<arrDiscountCredit.count;i++)
    {
        NSDictionary *dict=[arrDiscountCredit objectAtIndex:i];
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
        {
            [arrDiscountCreditInitialTemp addObject:dict];
        }
        
    }
    NSMutableArray* arrDiscountCreditMaintTemp=[[NSMutableArray alloc]init];
    for(int i=0; i<arrDiscountCredit.count;i++)
    {
        NSDictionary *dict=[arrDiscountCredit objectAtIndex:i];
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"true"])
        {
            [arrDiscountCreditMaintTemp addObject:dict];
        }
        
    }
    
    //    _const_ViewCouponCredit_H.constant=470;
    //    _const_TblCoupon_H.constant=80;
    //    _const_TblCredit_H.constant=80;
    //
    //    //For Applied Credit View
    //    _const_ViewAppliedCredit_H.constant=170;
    //    _const_TblAppliedCredit_H.constant=80;
    //    NSMutableArray *arrTempCouponAppliedCredit;
    //    arrTempCouponAppliedCredit=[[NSMutableArray alloc]init];
    //
    //    [arrTempCouponAppliedCredit addObjectsFromArray:arrAppliedCreditDetail];
    //
    //    float totalHeightStanAppliedCredit=0;
    //    UILabel *lblAppliedCredit=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, _tblAppliedCredit.frame.size.width-20, 21)];
    //    for (int i=0; i<arrTempCouponAppliedCredit.count; i++)
    //    {
    //
    //        NSDictionary *dict=[arrTempCouponAppliedCredit objectAtIndex:i];
    //
    //        lblAppliedCredit.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountDescription"]];
    //
    //        CGSize expectedLabelSizeAppliedCredit = [lblAppliedCredit.text sizeWithFont:lblAppliedCredit.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lblAppliedCredit.lineBreakMode];
    //
    //        totalHeightStanAppliedCredit=totalHeightStanAppliedCredit+expectedLabelSizeAppliedCredit.height;
    //    }
    //
    //    _const_TblAppliedCredit_H.constant=totalHeightStanAppliedCredit+(_const_TblAppliedCredit_H.constant*arrAppliedCreditDetail.count);
    //
    //    _const_ViewAppliedCredit_H.constant=_const_TblAppliedCredit_H.constant+40;
    //
    //    if(arrAppliedCreditDetail.count==0)
    //    {
    //        _const_ViewAppliedCredit_H.constant=0;
    //    }
    //
    //    //_const_ViewAppliedCredit_H.constant=0;
    //
    //    //End
    //
    //    //For Coupon
    //    NSMutableArray *arrTempCoupon;
    //    arrTempCoupon=[[NSMutableArray alloc]init];
    //
    //    [arrTempCoupon addObjectsFromArray:arrDiscountCoupon];
    //
    //    float totalHeightStan=0;
    //    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, _tblCoupon.frame.size.width-20, 21)];
    //    for (int i=0; i<arrTempCoupon.count; i++)
    //    {
    //
    //        NSManagedObject *dict=[arrTempCoupon objectAtIndex:i];
    //
    //        lbl.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
    //
    //        CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lbl.lineBreakMode];
    //
    //        totalHeightStan=totalHeightStan+expectedLabelSize.height;
    //
    //    }
    //
    //    //End
    //
    //    _const_TblCoupon_H.constant=totalHeightStan+(_const_TblCoupon_H.constant*arrDiscountCoupon.count);
    //
    //    //For Credit
    //    NSMutableArray *arrTempCouponCredit;
    //    arrTempCouponCredit=[[NSMutableArray alloc]init];
    //
    //    [arrTempCouponCredit addObjectsFromArray:arrDiscountCredit];
    //
    //    float totalHeightStanCredit=0;
    //    UILabel *lblCredit=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, _tblCredit.frame.size.width-20, 21)];
    //    for (int i=0; i<arrTempCouponCredit.count; i++)
    //    {
    //
    //        NSManagedObject *dictCredit=[arrTempCouponCredit objectAtIndex:i];
    //
    //        lblCredit.text=[NSString stringWithFormat:@"%@",[dictCredit valueForKey:@"discountDescription"]];
    //
    //        CGSize expectedLabelSizeCredit = [lblCredit.text sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lblCredit.lineBreakMode];
    //
    //        totalHeightStanCredit=totalHeightStanCredit+expectedLabelSizeCredit.height;
    //
    //    }
    //
    //    //For Coupon
    //    if(arrTempCoupon.count==0)
    //    {
    //        _const_lblCouponDisc_InitialPrice_H.constant=0;
    //        _const_TblNewInitialPriceCouponDiscount_H.constant=0;
    //    }
    //    else
    //    {
    //        _const_lblCouponDisc_InitialPrice_H.constant=21;
    //    }
    //
    //    //For Credit Initial
    //    if(arrTempCouponCredit.count==0)
    //    {
    //        _const_lblCredit_InitialPrice_H.constant=0;
    //        // _const_lblCredit_MaintPrice_H.constant=0;
    //        _const_TblNewInitialPriceCredit_H.constant=0;
    //        //_const_TblNewMaintPriceCredit_H.constant=0;
    //    }
    //    else
    //    {
    //        _const_lblCredit_InitialPrice_H.constant=21;
    //        //_const_lblCredit_MaintPrice_H.constant=21;
    //    }
    //    //For Maint
    //    if(arrDiscountCreditMaintTemp.count==0)
    //    {
    //        _const_lblCredit_MaintPrice_H.constant=0;
    //        _const_TblNewMaintPriceCredit_H.constant=0;
    //    }
    //    else
    //    {
    //        _const_lblCredit_MaintPrice_H.constant=21;
    //    }
    //
    //
    //    _const_TblCredit_H.constant=totalHeightStanCredit+(_const_TblCredit_H.constant*arrDiscountCredit.count);
    //
    //    _const_ViewCouponCredit_H.constant=_const_ViewCouponCredit_H.constant+_const_TblCredit_H.constant+_const_TblCoupon_H.constant-210;
    //
    //
    //New Price Info  View
    _const_ViewNewInitialMaintPrice_H.constant=600;
    _const_TblNewInitialPriceCouponDiscount_H.constant=30;
    _const_TblNewInitialPriceCredit_H.constant=30;
    _const_TblNewMaintPriceCredit_H.constant=30;
    
    //
    //    //New Initial Price
    //
    _const_TblNewInitialPriceCouponDiscount_H.constant=_const_TblNewInitialPriceCouponDiscount_H.constant*arrDiscountCoupon.count;
    //
    _const_TblNewInitialPriceCredit_H.constant= _const_TblNewInitialPriceCredit_H.constant*arrDiscountCreditInitialTemp.count;//arrDiscountCredit
    //
    //
    //_const_TblNewMaintPriceCredit_H.constant=_const_TblNewMaintPriceCredit_H.constant*arrDiscountCredit.count;
    _const_TblNewMaintPriceCredit_H.constant=_const_TblNewMaintPriceCredit_H.constant*arrDiscountCreditMaintTemp.count;
    
    
    _const_ViewNewInitialMaintPrice_H.constant=_const_ViewNewInitialMaintPrice_H.constant+_const_TblNewInitialPriceCouponDiscount_H.constant+_const_TblNewInitialPriceCredit_H.constant+_const_TblNewMaintPriceCredit_H.constant-100;
    //
    //    //New MaintPrice
    //
    //    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    //    {
    //        _const_ViewNewInitialMaintPrice_H.constant=0;
    //        _const_ViewCouponCredit_H.constant=0;
    //        _const_ViewAppliedCredit_H.constant=0;
    //    }
    //
    //    _cnsrtViewMaintainService_H.constant=0;
    //    _cnstrntViewNonStandard_H.constant=0;
    
    
    
    if(arrAppliedCreditDetail.count==0)
    {
        _const_ViewAppliedCredit_H.constant=0;
    }
    else
    {
        float tableHeightCreditDetail = 0.0;
        for(NSDictionary *dict in arrAppliedCreditDetail)
        {
            NSString *strDescription = [dict valueForKey:@"DiscountDescription"];
            if(strDescription.length>0)
            {
                
            }
            else
            {
                strDescription = @"";
            }
            CGFloat strHeight = [self findHeightForText:strDescription havingWidth:self.view.frame.size.width-148.0 andFont:[UIFont systemFontOfSize:13.0]].height;//
            
            if(strHeight<=21)//|| strHeight<=21.2871094
            {
                tableHeightCreditDetail = tableHeightCreditDetail+80;
            }
            else
            {
                tableHeightCreditDetail = tableHeightCreditDetail+(80-21)+strHeight;
            }
        }
        _const_TblAppliedCredit_H.constant = tableHeightCreditDetail+40;
        _const_ViewAppliedCredit_H.constant=_const_TblAppliedCredit_H.constant+40;
    }
    
    // calculate height for coupon table
    float tableHeightCoupon = 0.0;
    for(NSDictionary *dict in arrDiscountCoupon)
    {
        NSString *strDescription = [dict valueForKey:@"discountDescription"];
        CGFloat strHeight = [self findHeightForText:strDescription havingWidth:self.view.frame.size.width-148.0 andFont:[UIFont systemFontOfSize:13.0]].height;
        
        if(strHeight<=21)
        {
            tableHeightCoupon = tableHeightCoupon+80;
        }
        else
        {
            tableHeightCoupon = tableHeightCoupon+(80-21)+strHeight;
        }
    }
    _const_TblCoupon_H.constant = tableHeightCoupon;
    //_const_TblCoupon_H.constant = 1800;
    ///////////////
    
    // calculate height for credit table
    float tableHeightCedit = 0.0;
    for(NSDictionary *dict in arrDiscountCredit)
    {
        NSString *strDescription = [dict valueForKey:@"discountDescription"];
        CGFloat strHeight = [self findHeightForText:strDescription havingWidth:self.view.frame.size.width-148.0 andFont:[UIFont systemFontOfSize:13.0]].height;
        
        if(strHeight<=21)
        {
            tableHeightCedit = tableHeightCedit+80;
        }
        else
        {
            tableHeightCedit = tableHeightCedit+(80-21)+strHeight;
        }
    }
    _const_TblCredit_H.constant = tableHeightCedit;
    
    if(_const_TblCredit_H.constant==0 && _const_TblCoupon_H.constant==0)
    {
        _const_ViewCouponCredit_H.constant=(470+_const_TblCredit_H.constant+_const_TblCoupon_H.constant)-220;
    }
    else
    {
        _const_ViewCouponCredit_H.constant=(470+_const_TblCredit_H.constant+_const_TblCoupon_H.constant)-220;
    }
}

-(void)heightNonStandardTable
{
    _cnstrntNonStandardTbl_H.constant=100+26;
    _cnstrntNonStandardTbl_H.constant=(_cnstrntNonStandardTbl_H.constant* [arrNonStanInitialPrice count]);
    _cnstrntViewNonStandard_H.constant= _cnstrntNonStandardTbl_H.constant+200-35+26;
    
    if(arrNonStanInitialPrice.count==0)
    {
        //_cnstrntViewNonStandard_H.constant=0;
    }
    _cnsrtViewMaintainService_H.constant=0;
    _cnstrntViewNonStandard_H.constant=0;
}
-(void)heightProposalMaintenanceTable
{
    
    //temp
    _const_TableAgreement_H.constant=0;
    _const_TableAgreement_H.constant=0;
    _const_ViewAgreement_H.constant=0;
    _const_ViewPreferredMonth_H.constant=0;
    
    //end
    
    _cnstrntTblMaintenance_H.constant=150+24;
    _cnstrntTblMaintenance_H.constant=(_cnstrntTblMaintenance_H.constant* [arrStanServiceNameServiceProposal count]);
    //Bundle
    int  heightTempBundle=0;
    for (int i=0; i<arrBundleRow.count; i++)
    {
        [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:i]];
        for (int j=0; j<arrSysNameBundle.count; j++)
        {
            heightTempBundle=heightTempBundle+_tblBundle.rowHeight+35;
            
        }
    }
    _constTblBundle_H.constant=heightTempBundle;
    //End
    _cnsrtViewMaintainService_H.constant=_constTblBundle_H.constant+ _cnstrntTblMaintenance_H.constant+200+100;
    
    if (arrStanServiceNameServiceProposal.count==0)
    {
        //_cnsrtViewMaintainService_H.constant=0+200;
    }
    
    if ([strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
    {
        _const_ViewNewInitialMaintPrice_H.constant=0;
        _const_ViewCouponCredit_H.constant=0;
    }
    //Coupon Credit Height
    
    _const_ViewNewInitialMaintPrice_H.constant=0;
    _const_ViewCouponCredit_H.constant=0;
    
    _cnsrtViewMaintainService_H.constant=0;
    _cnstrntViewNonStandard_H.constant=0;
}
-(void)heightProposaNonStandardTable
{
    
    
    
    _cnstrntNonStandardTbl_H.constant=100+26;
    _cnstrntNonStandardTbl_H.constant=(_cnstrntNonStandardTbl_H.constant* [arrNonStanInitialPriceServiceProposal count]);
    _cnstrntViewNonStandard_H.constant= _cnstrntNonStandardTbl_H.constant+200-35;
    
    if(arrNonStanServiceNameServiceProposal.count==0)
    {
        //_cnstrntViewNonStandard_H.constant=0+200;
    }
    
    _cnsrtViewMaintainService_H.constant=0;
    _cnstrntViewNonStandard_H.constant=0;
    
}
-(BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    return false;
    // [_scrollViewAgreement setScrollsToTop:NO];
}
//Nilnd 20 Jan

-(void)fetchFromCoreDataNonStandardNew
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    
    NSArray* arrAllObjNew1 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSManagedObject *matchesNew1;
    // arrNonStanCount=[[NSMutableArray alloc]init];
    arrNonStanSoldCount=[[NSMutableArray alloc]init];
    //arrUnSoldNonStanCount=[[NSMutableArray alloc]init];
    arrNonStanUnSoldCount=[[NSMutableArray alloc]init];
    if (arrAllObjNew1.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjNew1.count; k++)
        {
            matchesNew1=arrAllObjNew1[k];
            NSLog(@"Lead IDDDD====%@",[matchesNew1 valueForKey:@"leadId"]);
            if([[NSString stringWithFormat:@"%@",[matchesNew1 valueForKey:@"isSold"]]isEqualToString:@"true"])
            {
                [arrNonStanSoldCount addObject:[NSString stringWithFormat:@"%d",k]];
                
            }
            else
            {
                [arrNonStanUnSoldCount addObject:[NSString stringWithFormat:@"%d",k]];
            }
            
        }
    }
}

//End
//Nilind 03 May
-(void)generateWorkOrder
{
    NSLog(@"AARONE TIME >>%@\n  ARRNON ONE TIME%@",arrOneTime,arrNotOneTime);
    
    if(arrNotOneTime.count>0)
    {
        [_btnInitialSetup setTitle:@"Initial Setup" forState:UIControlStateNormal];
    }
    else
    {
        if(arrOneTime.count>0)
        {
            [_btnInitialSetup setTitle:@"Generate WorkOrder" forState:UIControlStateNormal];
        }
        else
        {
            [_btnInitialSetup setTitle:@"Initial Setup" forState:UIControlStateNormal];
        }
    }
    
}
//End
- (IBAction)actionOnCheckFrontImage:(id)sender
{
    isCheckFrontImage=YES;
    btnTagCheckImage=0;
    /*UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:nil
     otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
     
     [actionSheet showInView:self.view];*/
    [self newAlertMethodCheckImage];
    
    
}

- (IBAction)actionOnCheckBackImage:(id)sender
{
    isCheckFrontImage=NO;
    btnTagCheckImage=0;
    /* UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:nil
     otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
     
     [actionSheet showInView:self.view];*/
    [self newAlertMethodCheckImage];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (btnTagCheckImage==0)
    {
        // isFromImagePicker=YES;
        //yesEditedSomething=YES;
        NSLog(@"Yes Something Edited");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDateCheck = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        // NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\CheckImg%@%@.jpg",strDateCheck,strTime];
        NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\CheckImages\\CheckImg%@%@.jpg",strDateCheck,strTime];
        //strImageNamess = [NSString stringWithFormat:@"CheckImg%@%@.jpg",strDateCheck,strTime];
        //strImageNamess = [NSString stringWithFormat:@"CheckImages\\CheckImg%@%@.jpg",strDateCheck,strTime];
        strImageNamess = [NSString stringWithFormat:@"CheckImg%@%@.jpg",strDateCheck,strTime];
        UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
        
        [self resizeImage:image :strImageNamess];
        
        //[self saveImage:image :strImageNamess];
        
        if (isCheckFrontImage)
        {
            
            [arrOFImagesName addObject:strImageNamess];
            
        }
        else
        {
            
            [arrOfCheckBackImage addObject:strImageNamess];
            
        }
        
        [self.navigationController dismissViewControllerAnimated: YES completion: nil];
        
        
    }
    else //For Collection view
    {
        isEditedInSalesAuto=YES;
        NSLog(@"Database edited");
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"backfromDynamicView"];
        [defs synchronize];
        
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
        [arrNoImage addObject:strImageNamess];
        //UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
        
        // [self saveImage:chosenImage :strImageNamess];
        
        [self resizeImage:chosenImage :strImageNamess];
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        //Lat long code
        
        CLLocationCoordinate2D coordinate = [global getLocation] ;
        NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        [arrImageLattitude addObject:latitude];
        [arrImageLongitude addObject:longitude];
        
        //imageCaption
        
        NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
        
        BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
        
        if (yesImageCaption) {
            
            [self alertViewCustom];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            [self saveImageToCoreData];
            
        }
        
        
    }
}
-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    if (btnTagCheckImage==0)
    {
        float actualHeight = image.size.height;
        float actualWidth = image.size.width;
        float maxHeight = 1000;//actualHeight/1.5;
        float maxWidth = 1000;//actualWidth/1.5;
        float imgRatio = actualWidth/actualHeight;
        float maxRatio = maxWidth/maxHeight;
        float compressionQuality = 0.5;//50 percent compression
        
        if (actualHeight > maxHeight || actualWidth > maxWidth)
        {
            if(imgRatio < maxRatio)
            {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio)
            {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else
            {
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
        
        NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
        
        UIGraphicsEndImageContext();
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
        [imageData writeToFile:path atomically:YES];
        
        return [UIImage imageWithData:imageData];
    }
    else //For collection
    {
        [arrImagePath addObject:imageName];
        
        float actualHeight = image.size.height;
        float actualWidth = image.size.width;
        float maxHeight = 1000;//actualHeight/1.5;
        float maxWidth = 1000;//actualWidth/1.5;
        float imgRatio = actualWidth/actualHeight;
        float maxRatio = maxWidth/maxHeight;
        float compressionQuality = 0.5;//50 percent compression
        
        if (actualHeight > maxHeight || actualWidth > maxWidth)
        {
            if(imgRatio < maxRatio)
            {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio)
            {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else
            {
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
        
        NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
        
        UIGraphicsEndImageContext();
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
        [imageData writeToFile:path atomically:YES];
        
        return [UIImage imageWithData:imageData];
    }
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    if (btnTagCheckImage==0)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
    else //For Collection view
    {
        [arrImagePath addObject:imageName];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
        
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
        
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(btnTagCheckImage==0)
    {
        
        if (buttonIndex ==0)
        {
            if (isCheckFrontImage)
            {
                
                if (arrOFImagesName.count==0)
                {
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }
                else
                {
                    
                    [self performSelector:@selector(goToImagePreviewCheckFrontImage)  withObject:nil afterDelay:0.1];
                    
                    /* NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                     [defs setBool:YES forKey:@"forCheckFrontImageDelete"];
                     [defs setBool:NO forKey:@"forCheckBackImageDelete"];
                     [defs synchronize];
                     
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                     bundle: nil];
                     ImagePreviewSalesAutoiPad
                     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAutoiPad"];
                     objByProductVC.arrOfImages=arrOFImagesName;
                     objByProductVC.indexOfImage=@"0";
                     objByProductVC.statusOfWorkOrder=strStatus;                    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];*/
                }
                
            }
            else
            {
                
                if (arrOfCheckBackImage.count==0)
                {
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }
                else
                {
                    
                    [self performSelector:@selector(goToImagePreviewCheckBackImage)  withObject:nil afterDelay:0.1];
                    
                    /*NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                     [defs setBool:YES forKey:@"forCheckBackImageDelete"];
                     [defs setBool:NO forKey:@"forCheckFrontImageDelete"];
                     [defs synchronize];
                     
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                     bundle: nil];
                     ImagePreviewSalesAutoiPad
                     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAutoiPad"];
                     objByProductVC.arrOfImages=arrOfCheckBackImage;
                     objByProductVC.indexOfImage=@"0";
                     objByProductVC.statusOfWorkOrder=strStatus;                  [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];*/
                }
            }
        }
        else if (buttonIndex==1)
        {
            if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
                
            {
                [global AlertMethod:@"Alert!" :@"No Allowed"];
            }
            else
            {
                NSArray *arrTemp;
                if (isCheckFrontImage)
                {
                    
                    arrTemp=arrOFImagesName;
                    
                }
                else
                {
                    
                    arrTemp=arrOfCheckBackImage;
                    
                }
                
                if (arrTemp.count>=1) {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check front image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                } else {
                    NSLog(@"The CApture Image.");
                    
                    BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                    
                    if (isCameraPermissionAvailable) {
                        
                        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                        {
                            
                            [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                            
                        }else{
                            
                            UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                            imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                            imagePickController.delegate=(id)self;
                            imagePickController.allowsEditing=TRUE;
                            [self presentViewController:imagePickController animated:YES completion:nil];
                            
                        }
                        
                    }else{
                        
                        UIAlertController *alert= [UIAlertController
                                                   alertControllerWithTitle:@"Alert"
                                                   message:@"Camera Permission not allowed.Please go to settings and allow"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action)
                                              {
                            
                            
                            
                        }];
                        [alert addAction:yes];
                        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                            
                            if ( ( (&UIApplicationOpenSettingsURLString)) != NULL) {
                                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                [[UIApplication sharedApplication] openURL:url];
                            } else {
                                
                                //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                //                                     [alert show];
                                
                            }
                            
                        }];
                        [alert addAction:no];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    
                }
            }
            
        }
        else if (buttonIndex == 2)
        {
            if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
                
            {
                [global AlertMethod:@"Alert!" :@"No Allowed"];
            }
            else
            {
                NSArray *arrTemp;
                if (isCheckFrontImage)
                {
                    
                    arrTemp=arrOFImagesName;
                    
                }else{
                    
                    arrTemp=arrOfCheckBackImage;
                    
                }
                
                if (arrTemp.count>=1)
                {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check back image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    
                    BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                    
                    if (isCameraPermissionAvailable) {
                        
                        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                        {
                            
                            [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                            
                        }
                        else
                        {
                            
                            UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                            imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                            imagePickController.delegate=(id)self;
                            imagePickController.allowsEditing=TRUE;
                            [self presentViewController:imagePickController animated:YES completion:nil];
                            
                        }
                        
                    }else{
                        
                        UIAlertController *alert= [UIAlertController
                                                   alertControllerWithTitle:@"Alert"
                                                   message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action)
                                              {
                            
                            
                            
                        }];
                        [alert addAction:yes];
                        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                            
                            if ( ((&UIApplicationOpenSettingsURLString)) != NULL) {
                                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                [[UIApplication sharedApplication] openURL:url];
                            } else {
                                
                                //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                //                                     [alert show];
                                
                            }
                            
                        }];
                        [alert addAction:no];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    
                }
            }
            
        }
    }
    else //For Collection view
    {
        if (buttonIndex ==7)
        {
            
            
            NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
            
            
            NSArray *arrOfImagesDetail=arrNoImage;
            
            arrOfBeforeImages=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfImagesDetail.count; k++) {
                
                if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                    
                    NSDictionary *dictData=arrOfImagesDetail[k];
                    
                    [arrOfBeforeImages addObject:dictData];
                    
                }else{
                    
                    [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
                    
                }
                
            }
            
            arrOfImagesDetail=arrOfBeforeImages;
            
            if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
                [global AlertMethod:Info :NoBeforeImg];
            }else if (arrOfImagesDetail.count==0){
                [global AlertMethod:Info :NoBeforeImg];
            }
            else {
                NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
                for (int k=0; k<arrOfImagesDetail.count; k++) {
                    if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dict=arrOfImagesDetail[k];
                        [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
                    }else{
                        
                        [arrOfImagess addObject:arrOfImagesDetail[k]];
                        
                    }
                }
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                         bundle: nil];
                ImagePreviewSalesAuto
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
                objByProductVC.arrOfImages=arrOfImagess;
                
                [self.navigationController pushViewController:objByProductVC animated:YES];
                
            }
        }
        else if (buttonIndex == 0)
        {
            NSLog(@"The CApture Image.");
            
            
            if (arrNoImage.count<10)
            {
                NSLog(@"The CApture Image.");
                
                BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Camera Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                        
                        
                        
                    }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                        
                        if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                            [[UIApplication sharedApplication] openURL:url];
                        } else {
                            
                        }
                    }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
            else
            {
                chkForDuplicateImageSave=YES;
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            //............
            
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"The Gallery.");
            if (arrNoImage.count<10)
            {
                BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Gallery Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                        
                        
                        
                    }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                        
                        if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                            [[UIApplication sharedApplication] openURL:url];
                        }
                        else
                        {
                            
                            
                            
                        }
                        
                    }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else
            {
                chkForDuplicateImageSave=YES;
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
        
    }
}
-(void)openGalleryy{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)goToImagePreviewCheckFrontImage
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"forCheckFrontImageDelete"];
    [defs setBool:NO forKey:@"forCheckBackImageDelete"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                             bundle: nil];
    ImagePreviewSalesAuto
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
    objByProductVC.arrOfImages=arrOFImagesName;
    objByProductVC.indexOfImage=@"0";
    objByProductVC.statusOfWorkOrder=strStatus;
    objByProductVC.strForCheckImage=@"CheckImages";
    
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
}
-(void)goToImagePreviewCheckBackImage
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"forCheckBackImageDelete"];
    [defs setBool:NO forKey:@"forCheckFrontImageDelete"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                             bundle: nil];
    ImagePreviewSalesAuto
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
    objByProductVC.arrOfImages=arrOfCheckBackImage;
    objByProductVC.indexOfImage=@"0";
    objByProductVC.statusOfWorkOrder=strStatus;
    objByProductVC.strForCheckImage=@"CheckImages";
    
    
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
}


//Nilind 14 Sept
#pragma mark - --------Nilind 14 Sept change FOOTER CHANGE--------------


- (IBAction)actionAddAfterImages:(id)sender
{
    [self endEditing];
    btnTagCheckImage=100;
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        /* UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
         delegate:self
         cancelButtonTitle:@"Cancel"
         destructiveButtonTitle:nil
         otherButtonTitles:@"Capture New", @"Gallery", nil];
         
         [actionSheet showInView:self.view];*/
        [self newAlertMethodAddImage];
        
        
    }
    
    
    
}

- (IBAction)actionOnCancelAfterImage:(id)sender
{
    [self endEditing];
    btnTagCheckImage=100;
    
    [_viewForAfterImage removeFromSuperview];
    
}
- (IBAction)actionOnAfterImgView:(id)sender
{
    //  [self endEditing];
    //  [self goToGlobalmage:@"Before"];
    [self attachImage];
    
    
}

#pragma mark- ---------- FETCH IMAGE ----------
-(void)fetchImageDetailFromDataBase
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" : leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                /*
                 [arrOfImagenameCollewctionView addObject:leadImagePath];
                 
                 //Nilind 07 June
                 
                 NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                 
                 if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                 
                 [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                 
                 }
                 else
                 {
                 
                 
                 [arrOfImageCaptionGraph addObject:strImageCaption];
                 
                 }
                 
                 
                 NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                 
                 if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                 
                 [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                 
                 } else {
                 
                 [arrOfImageDescriptionGraph addObject:strImageDescription];
                 
                 }
                 
                 
                 */
                //End
                
            }
            else
            {
                
                [arrNoImage addObject:dict_ToSendLeadInfo];
                
                //Nilind 07 June
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageLongitude addObject:strLong];
                    
                }
                //End
                
            }
            
            
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    }
    else
    {
    }
}


/*-(void)openGalleryy{
 
 UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
 imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
 imagePickController.delegate=(id)self;
 imagePickController.allowsEditing=TRUE;
 [self presentViewController:imagePickController animated:YES completion:nil];
 
 }
 -(void)openCamera
 {
 
 UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
 imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
 imagePickController.delegate=(id)self;
 imagePickController.allowsEditing=TRUE;
 [self presentViewController:imagePickController animated:YES completion:nil];
 
 }*/

//============================================================================

#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==_collectionViewAfterImage)
    {
        return arrNoImage.count;
    }
    else
    {
        return arrGraphImage.count;
    }
    
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_collectionViewAfterImage)
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    else
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *str = [arrNoImage objectAtIndex:indexPath.row];
    if (collectionView==_collectionViewAfterImage)
    {
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreview : strIndex];
            
        }
    }
    else
    {
        // NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *str;// = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrGraphImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    }
    
    
}
//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        
        
        
    }else if (arrOfImagesDetail.count==0){
        
        
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1)
        {
        }
        else if (arrOfImagess.count==2)
        {
            
        }
        else
        {
        }
        //_const_ImgView_H.constant=100;  temp comment
        //_const_SaveContinue_B.constant=62;
        
        //21 April 2020
        //[self downloadImages:arrOfImagess];
        
    }
}


-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            [self ShowFirstImage:str:k];
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    //NIlind 14 Sept
    
    [_collectionViewAfterImage reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImage : name : indexx];
    
}

- (UIImage*)loadImage :(NSString*)name :(int)indexxx
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (indexxx==0)
    {
        
        
    } else if(indexxx==1)
    {
        
        
        
    }else{
        
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}
-(void)goingToPreview :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                 bundle: nil];
        ImagePreviewSalesAuto
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAuto"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaption;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescription;
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender
{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollViewAgreement.contentSize.height - _scrollViewAgreement.bounds.size.height);
        [_scrollViewAgreement setContentOffset:bottomOffset animated:YES];
        [self saveImageToCoreData];
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollViewAgreement.contentSize.height - _scrollViewAgreement.bounds.size.height);
    [_scrollViewAgreement setContentOffset:bottomOffset animated:YES];
    [self saveImageToCoreData];
    
}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=8;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollViewAgreement.contentSize.height - _scrollViewAgreement.bounds.size.height);
            [_scrollViewAgreement setContentOffset:bottomOffset animated:YES];
            
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollViewAgreement.contentSize.height - _scrollViewAgreement.bounds.size.height);
        [_scrollViewAgreement setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
        
        // [self alertToEnterImageCaption];
        
    }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
//.................................................................................
#pragma mark- Save & Remove Image  Methods
//============================================================================
//============================================================================
- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
#pragma mark- Note
    
    /*  NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     
     BOOL yesfromback=[defs boolForKey:@"backfromDynamicView"];
     if (yesfromback) {
     
     [defs setBool:NO forKey:@"backfromDynamicView"];
     [defs synchronize];
     
     }
     else
     {
     
     [self deleteImageFromCoreDataSalesInfo];
     
     for (int k=0; k<arrNoImage.count; k++)
     {
     // Image Detail Entity
     entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
     
     ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
     
     if ([arrNoImage[k] isKindOfClass:[NSString class]])
     {
     
     objImageDetail.createdBy=@"";
     objImageDetail.createdDate=@"";
     objImageDetail.descriptionImageDetail=@"";
     objImageDetail.leadImageId=@"";
     objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[arrNoImage objectAtIndex:k]];
     objImageDetail.leadImageType=@"Before";
     objImageDetail.modifiedBy=@"";
     objImageDetail.modifiedDate=[global modifyDate];
     objImageDetail.leadId=strLeadId;
     objImageDetail.userName=strUserName;
     objImageDetail.companyKey=strCompanyKey;
     
     
     NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
     if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
     
     objImageDetail.leadImageCaption=@"";
     
     } else {
     
     objImageDetail.leadImageCaption=strImageCaptionToSet;
     
     }
     
     NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
     if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
     
     objImageDetail.descriptionImageDetail=@"";
     
     } else {
     
     objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
     
     }
     //Latlong code
     
     NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
     objImageDetail.latitude=strlat;
     NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
     objImageDetail.longitude=strlong;
     
     NSError *error1;
     [context save:&error1];
     }
     else
     {
     NSDictionary *dictData=[arrNoImage objectAtIndex:k];
     objImageDetail.createdBy=@"";
     objImageDetail.createdDate=@"";
     objImageDetail.descriptionImageDetail=@"";
     objImageDetail.leadImageId=@"";
     objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"leadImagePath"]];
     objImageDetail.leadImageType=@"Before";
     objImageDetail.modifiedBy=@"";
     objImageDetail.modifiedDate=[global modifyDate];
     objImageDetail.leadId=strLeadId;
     objImageDetail.userName=strUserName;
     objImageDetail.companyKey=strCompanyKey;
     
     
     NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
     if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
     
     objImageDetail.leadImageCaption=@"";
     
     } else {
     
     objImageDetail.leadImageCaption=strImageCaptionToSet;
     
     }
     
     NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
     if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
     
     objImageDetail.descriptionImageDetail=@"";
     
     } else {
     
     objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
     
     }
     //Latlong code
     
     NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
     objImageDetail.latitude=strlat;
     NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
     objImageDetail.longitude=strlong;
     
     NSError *error1;
     [context save:&error1];
     
     }
     
     }
     for (int k=0; k<arrGraphImage.count; k++)
     {
     // Image Detail Entity
     entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
     
     ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
     NSDictionary *dict=[arrGraphImage objectAtIndex:k];
     if ([arrGraphImage[k] isKindOfClass:[NSDictionary class]])
     {
     
     objImageDetail.createdBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdBy"]];
     objImageDetail.createdDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdDate"]];
     objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
     objImageDetail.leadImageId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageId"]];
     objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImagePath"]];
     objImageDetail.leadImageType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageType"]];
     objImageDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"modifiedBy"]];
     objImageDetail.modifiedDate=[global modifyDate];
     objImageDetail.leadId=strLeadId;
     objImageDetail.userName=strUserName;
     objImageDetail.companyKey=strCompanyKey;
     objImageDetail.leadImageCaption=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageCaption"]];
     objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
     //Latlong code
     
     //NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
     objImageDetail.latitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"latitude"]];
     // NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
     objImageDetail.longitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"longitude"]];
     
     NSError *error1;
     [context save:&error1];
     }
     }
     
     
     
     }*/
    //........................................................................
    // [self fetchForUpdate];
    
    //[self fetchImageFromCoreDataStandard];
    
}
-(void)deleteImageFromCoreDataSalesInfo
{
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
    [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    [allDataImage setPredicate:predicateImage];
    
    [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage) {
        [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
}
-(void)getImageCollectionView
{
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    }
    else
    {
        
        //Nilind 07 June Image Caption
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackAgreement"];//@"isFromBackSendMail"];
        
        if (isFromBack)
        {
            
            //arrOfBeforeImageAll=nil;
            //arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            arrNoImage=[[NSMutableArray alloc]init];
            
            
            arrImageLattitude=nil;
            arrImageLattitude=[[NSMutableArray alloc]init];
            arrImageLongitude=nil;
            arrImageLongitude=[[NSMutableArray alloc]init];
            
            arrImageGraphLattitude=nil;
            arrImageGraphLattitude=[[NSMutableArray alloc]init];
            arrImageGraphLongitude=nil;
            arrImageGraphLongitude=[[NSMutableArray alloc]init];
            
            
            [defsBack setBool:NO forKey:@"isFromBackAgreement"];//@"isFromBackSendMail"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseForGraph];
            [self fetchImageDetailFromDataBase];
            
        }
        
        //End
        
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreviewSales"];
        if (yesFromDeleteImage)
        {
            isEditedInSalesAuto=YES;
            NSLog(@"Database edited");
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreviewSales"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrNoImage.count; k++)
            {
                
                NSDictionary *dictdat=arrNoImage[k];
                NSString *strImageName;
                if([dictdat isKindOfClass:[NSDictionary class]])
                {
                    strImageName=[dictdat valueForKey:@"leadImagePath"];
                }
                else
                {
                    strImageName=arrNoImage[k];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImagesSales"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrNoImage[k]];
                        
                    }
                }
                
            }
            if (!(arrTempBeforeImage.count==0))
            {
                
                [arrNoImage removeObjectsInArray:arrTempBeforeImage];
            }
            else if((arrTempBeforeImage.count==0))
            {
                arrNoImage=nil;
                arrNoImage=[[NSMutableArray alloc]init];
            }
            
            //nIlind 09 April
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImagesSales"];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            
            
            [defsnew synchronize];
            
            [self saveImageToCoreData];
            
            //End
            
        }
        else
        {
            
        }
        //Nilind 07 June Image Caption
        
        
        //Change in image Captions
        BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //End
        
        
        [self downloadingImagesThumbNailCheck];
        //Nilind 10 Jan
        
    }
    //...........
    [_collectionViewAfterImage reloadData];
    [_collectionViewGraphImage reloadData];
    
    
}

- (IBAction)actionOnAudioBottom:(id)sender
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Choose Options"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Record-Audio" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
        LandingSong=nil;
        if (LandingSong.isPlaying)
        {
            
        }
        else
        {
            [LandingSong stop];
        }
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        BOOL isfirstTimeAudio=[defs boolForKey:@"firstAudio"];
        
        if (isfirstTimeAudio) {
            
            [defs setBool:NO forKey:@"firstAudio"];
            [defs synchronize];
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            RecordAudioView
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioView"];
            objByProductVC.strFromWhere=@"SalesInvoice";
            [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
            
            
        } else {
            
            BOOL isAudioPermissionAvailable=[global isAudioPermissionAvailable];
            
            if (isAudioPermissionAvailable) {
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                RecordAudioView
                
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioView"];
                objByProductVC.strFromWhere=@"SalesInvoice";
                [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
                
                
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:AlertAudioVideoPermission//@"Audio Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                    
                    
                    
                }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                    
                    if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        [[UIApplication sharedApplication] openURL:url];
                    } else {
                        
                        //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        //                                     [alert show];
                        
                    }
                    
                }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        
        
    }];
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        [alert addAction:yes];
        
    }
    
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Play-Audio" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
        if ([_btnPlayAudio.titleLabel.text isEqualToString:@"Play"]) {
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            BOOL yesAudio=[defs boolForKey:@"yesAudio"];
            if (yesAudio) {
                [_btnPlayAudio setTitle:@"Stop" forState:UIControlStateNormal];
                //  _lblAudioStatus.text=@"Audio Available";
                //[defs setBool:NO forKey:@"yesAudio"];
                [defs synchronize];
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                NSString *strAudioName=[defs valueForKey:@"AudioNameService"];
                [self playLandingAudio:strAudioName];
            }else{
                // _lblAudioStatus.text=@"Audio Not Available";
                [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
            }
        } else {
            [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
            // [LandingSong stop];
            LandingSong=nil;
            if (LandingSong.isPlaying)
            {
                
            }
            else
            {
                [LandingSong stop];
            }
        }
        
        
    }];
    [alert addAction:no];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
        
        
    }];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
#pragma mark -------------- ADDING GRAPH IMAGE ----------------

- (IBAction)actionOnAddGraphImage:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"firstGraphImage"];
        [defs setBool:NO forKey:@"servicegraph"];
        
        [defs synchronize];
        
        /*UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         EditGraphViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewController"];
         objSignViewController.strLeadId=strLeadId;
         objSignViewController.strCompanyKey=strCompanyKey;
         objSignViewController.strUserName=strUserName;
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
        objSignViewController.strLeadId=strLeadId;
        objSignViewController.strCompanyKey=strCompanyKey;
        objSignViewController.strUserName=strUserName;
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    }
}

- (IBAction)actionOnCancelBeforeImage:(id)sender
{
    [self endEditing];
    [_viewForGraphImage removeFromSuperview];
}

- (IBAction)actionOnGraphImageFooter:(id)sender
{
    [self endEditing];
    [self goToGlobalmage:@"Graph"];
    /* CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewForFinalSave.frame.origin.y-_viewForGraphImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewForGraphImage.frame.size.height);
     [_viewForGraphImage setFrame:frameFor_view_BeforeImageInfo];
     [_collectionViewGraphImage reloadData];
     [self.view addSubview:_viewForGraphImage];*/
}
-(void)goingToPreviewGraph :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrGraphImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++)
    {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
        {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }
        else
        {
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++)
        {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        GraphImagePreviewViewController
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewController"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strLeadId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
        
        
        
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
-(void)fetchImageDetailFromDataBaseForGraph
{
    
    arrGraphImage=nil;
    
    arrGraphImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" :leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                
                //[arrOfImagenameCollewctionView addObject:leadImagePath];
                [arrGraphImage addObject:dict_ToSendLeadInfo];
                
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
            }
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    } else {
    }
    
    [self downloadImagesGraphs:arrGraphImage];
    
}
-(void)downloadImagesGraphs :(NSArray*)arrOfImagesDownload
{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        // NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
        NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrOfImagesDownload objectAtIndex:k];
        }
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    [_collectionViewGraphImage reloadData];
    
}
#pragma mark- BUNDLE CHANGE

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        NSString *sectionName;
        sectionName=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        return sectionName;
    }
    else
    {
        return @"";
    }
    
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        NSString *sectionName;
        sectionName=[NSString stringWithFormat:@"%@ Footer",[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        return sectionName;
    }
    else
    {
        return @"";
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==2)
    {
        return arrBundleRow.count;
    }
    else
    {
        return 1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        return 35.0f;
    }
    else
    {
        return 0.0f;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        return 30.0f;
    }
    else
    {
        return 0.0f;
    }
}
-(void)fetchFromCoreDataStandardForBundle:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    
    
    
    arrInitialPriceBundle=[[NSMutableArray alloc]init];
    arrDiscountPerBundle=[[NSMutableArray alloc]init];
    
    arrDiscountBundle=[[NSMutableArray alloc]init];
    arrMaintenancePriceBundle=[[NSMutableArray alloc]init];
    arrFrequencyNameBundle=[[NSMutableArray alloc]init];
    arrServiceNameBundle=[[NSMutableArray alloc]init];
    arrStanIsSoldBundle=[[NSMutableArray alloc]init];
    arrSysNameBundle=[[NSMutableArray alloc]init];
    
    arrUnitBundle=[[NSMutableArray alloc]init];
    arrFinalInitialPriceBundle=[[NSMutableArray alloc]init];
    arrFinalMaintPriceBundle=[[NSMutableArray alloc]init];
    
    arrPackageNameIdBundle=[[NSMutableArray alloc]init];
    arrBillingFreqSysNameBundle=[[NSMutableArray alloc]init];
    arrBundleId=[[NSMutableArray alloc]init];
    arrFreqSysNameBundle=[[NSMutableArray alloc]init];
    
    // arrBundleRow=[[NSMutableArray alloc]init];
    NSMutableArray *arrBundleNew;
    arrBundleNew=[[NSMutableArray alloc]init];
    
    
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                [arrFrequencyNameBundle addObject:[matches valueForKey:@"serviceFrequency"]];
                [arrFreqSysNameBundle addObject:[matches valueForKey:@"frequencySysName"]];
                
                if ([[matches valueForKey:@"serviceSysName"]isEqualToString:@""])
                {
                    [arrServiceNameBundle addObject:@""];
                }
                else
                {
                    [arrServiceNameBundle addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                }
                
                [arrStanIsSoldBundle addObject: [matches valueForKey:@"isSold"]];
                
                [arrSysNameBundle addObject:[matches valueForKey:@"serviceSysName"]];
                
                //Nilind 6 oct
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isTBD"]]isEqualToString:@"true"])
                {
                    [arrInitialPriceBundle addObject:@"TBD"];
                    [arrDiscountPerBundle addObject:@"TBD"];
                    [arrDiscountBundle addObject:@"TBD"];
                    [arrMaintenancePriceBundle addObject:@"TBD"];
                    [arrFinalInitialPriceBundle addObject:@"TBD"];
                    [arrFinalMaintPriceBundle addObject:@"TBD"];
                }
                else
                {
                    [arrInitialPriceBundle addObject:[matches valueForKey:@"initialPrice"]];
                    [arrDiscountBundle addObject:[matches valueForKey:@"discount"]];
                    [arrMaintenancePriceBundle addObject:[matches valueForKey:@"maintenancePrice"]];
                    [arrFinalInitialPriceBundle addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                    [arrFinalMaintPriceBundle addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                    [arrDiscountPerBundle addObject:[matches valueForKey:@"discountPercentage"]];
                    
                }
                [arrUnitBundle addObject:[matches valueForKey:@"unit"]];
                
                [arrPackageNameIdBundle addObject:[matches valueForKey:@"packageId"]];
                [arrBillingFreqSysNameBundle addObject:[matches valueForKey:@"billingFrequencySysName"]];
                [arrBundleId addObject:[matches valueForKey:@"bundleId"]];
            }
        }
    }
    // [self heightManage];
}
/*-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 
 }*/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 35)];
    headerView.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    headerView.backgroundColor = [UIColor colorWithRed:58.0/255 green:63.0/255 blue:76.0/255 alpha:1];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = section + 1000;
    button.frame = CGRectMake(_tblBundle.frame.origin.x+4, 2, 30, 30);
    
    [self fetchFromCoreDataStandardForSectionCheckBoxBundle:[arrBundleRow objectAtIndex:section]];
    if ([strSoldStatusBundle isEqualToString:@"true"])
    {
        [button setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [button setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    [button addTarget:self action:@selector(checkBundleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    // [headerView addSubview:button];
    [button setTitle:[arrBundleRow objectAtIndex:section] forState:UIControlStateNormal];
    // button.titleLabel.textColor=[UIColor clearColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 250, 35)];
    
    
    NSString *sectionName;
    sectionName=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]];
    if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
    {
        sectionName=@"";
    }
    headerLabel.text =sectionName;
    headerLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabel.textColor = [UIColor whiteColor];
    //headerLabel.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabel];
    
    UIButton *buttonDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonDelete.tag = section + 1000;
    buttonDelete.frame = CGRectMake(CGRectGetMaxX(headerView.frame)-70, 0, 35, 35);
    //[buttonDelete setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    //buttonDelete.backgroundColor=[UIColor redColor];
    [buttonDelete addTarget:self action:@selector(deleteBundleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    // [headerView addSubview:buttonDelete];
    [buttonDelete setTitle:[arrBundleRow objectAtIndex:section]  forState:UIControlStateNormal];
    buttonDelete.titleLabel.textColor=[UIColor clearColor];
    [buttonDelete setImage:[UIImage imageNamed:@"ic_action_delete.png"] forState:UIControlStateNormal];
    [buttonDelete setBackgroundImage:[UIImage imageNamed:@"ic_action_delete.png"] forState:UIControlStateNormal];
    return headerView;
}
- (IBAction)deleteBundleButtonPressed:(UIButton *)sender
{
    // NSInteger section = sender.tag - 1000;
    //UIButton *selectedButton = (UIButton *) sender;
    
    //[self deleteBundleButtonPressed:sender.titleLabel.text];
    NSLog(@"Section Bundle Id %@",sender.titleLabel.text);
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert!"
                               message:@"Are you sure want to delete Bundle"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
        [self fetchFromCoreDataStandard];
        
        [self deleteBundleFromCoreDataSalesInfo:[NSString stringWithFormat:@"%@",sender.titleLabel.text]];
        [self fetchFromCoreDataStandard];
        [_tblBundle reloadData];
        
    }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
        
        
        
    }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)checkBundleButtonPressed:(UIButton *)sender
{
    NSInteger section = sender.tag - 1000;
    
    UIButton *selectedButton = (UIButton *) sender;
    NSLog(@"Section Bundle Id %@",selectedButton.titleLabel.text);
    if([selectedButton.currentImage isEqual:[UIImage imageNamed:@"check_box_2.png"]])
        //if(selectedButton.selected==YES)
    {
        [selectedButton setSelected:NO];
        [selectedButton setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [self updateBundle:selectedButton.titleLabel.text :@"false"];
        
        
    }
    else
    {
        [selectedButton setSelected:YES];
        [selectedButton setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [self updateBundle:selectedButton.titleLabel.text :@"true"];
    }
    
}
-(void)updateBundle: (NSString*)strButtonBundleId : (NSString*)status
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,strButtonBundleId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            //if([[matches valueForKey:@"bundleId"]isEqualToString:strButtonBundleId])
            //{
            [matches setValue:status forKey:@"isSold"];
            [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
            // [context save:&error1];
            //}
            
        }
        [context save:&error1];
    }
    
    [self fetchFromCoreDataStandard];
    
}
-(void)deleteBundleFromCoreDataSalesInfo:(NSString*)strBundleIdd
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    for (int i=0; i<Data.count; i++)
    {
        NSManagedObject *dataMatches=[Data objectAtIndex:i];
        if ([[dataMatches valueForKey:@"bundleId"]isEqualToString:strBundleIdd])
        {
            [context deleteObject:dataMatches];
            NSError *saveError = nil;
            [context save:&saveError];
            
        }
    }
    
    /*for (NSManagedObject * data in Data)
     {
     [context deleteObject:data];
     }*/
    
    //[self fetchFromCoreDataStandard];
    //[self heightManage];
    //[self fetchFromCoreDataStandard];
    
    // [_tblBundle reloadData];
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 30)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(_tblBundle.frame.origin.x+5, 0, _tblBundle.frame.size.width/2, 30)];
    [self fetchFromCoreDataStandardForSectionCheckBoxBundle:[arrBundleRow objectAtIndex:section]];
    headerLabel.text =[NSString stringWithFormat:@"Total Initial Cost = $%.2f",totalInitialBundle];
    headerLabel.font=[UIFont boldSystemFontOfSize:12];
    headerLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabel];
    
    UILabel *headerLabelMaint = [[UILabel alloc] initWithFrame:CGRectMake(headerLabel.frame.origin.x+headerLabel.frame.size.width+120, 0,_tblBundle.frame.size.width/2, 30)];
    headerLabelMaint.text =[NSString stringWithFormat:@"Total Maint Cost = $%.2f",totalMaintBundle];
    CGSize expectedLabelSize = [ headerLabelMaint.text sizeWithFont:headerLabelMaint.font constrainedToSize:CGSizeMake(headerLabelMaint.frame.size.width, FLT_MAX) lineBreakMode:headerLabelMaint.lineBreakMode];
    headerLabelMaint.frame=CGRectMake(_tblBundle.frame.size.width -expectedLabelSize.width-10, 0, expectedLabelSize.width, 30);
    
    headerLabelMaint.font=[UIFont boldSystemFontOfSize:14];
    
    headerLabelMaint.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabelMaint.textColor = [UIColor blackColor];
    headerLabelMaint.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabelMaint];
    
    return headerView;
}
-(void)fetchFromCoreDataStandardForSectionCheckBoxBundle:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    strSoldStatusBundle=@"false";
    NSMutableArray *arrTotalInitialCostBundle,*arrTotalMaintCostBundle;
    arrTotalInitialCostBundle=[[NSMutableArray alloc]init];
    arrTotalMaintCostBundle=[[NSMutableArray alloc]init];
    totalInitialBundle=0;totalMaintBundle=0;
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                strSoldStatusBundle=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isSold"]];
                
                
                
                NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                if ([str isEqualToString:@"0"])
                {
                    totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                    totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                    
                }
                else
                {
                    if (str.length==0 || [str isEqualToString:@"(null)"])
                    {
                        totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                        totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                        
                    }
                    else
                    {
                        totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedInitialPrice"]]doubleValue];
                        
                        totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedMaintPrice"]]doubleValue];
                    }
                }
                
                //totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                //totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                //break;
            }
        }
    }
}
- (IBAction)actionOnNotesHistory:(id)sender
{
    [self endEditing];
    [self goToHistory];
    
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
            NSString *strClockStatus= [global getCurrentTimerOfClock];
            NSLog(@"%@",strClockStatus);
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                {
                    [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                }
                else if ([strClockStatus isEqualToString:@"WorkingTime"])
                {
                    [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                    
                }
                else if ([strClockStatus isEqualToString:@"BreakTime"])
                {
                    [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                    
                }
                else
                {
                    [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                    
                }
                
                
            });
        });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    [self endEditing];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ClockInOutViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
-(void)buttonClickedCheckBoxAgreementService:(UIButton*)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        [self fetchAgreementCheckListSales];
        NSLog(@"%ld",(long)sender.tag);
        UIButton *button=(UIButton *) sender;
        
        
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
        
        AgreementCheckTableViewCell *tappedCell = (AgreementCheckTableViewCell *)[_tblAgreement cellForRowAtIndexPath:indexpath];
        
        NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfoAgreementList objectAtIndexPath:indexpath];
        if ([tappedCell.btnCheckBoxAgreement.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            //[tappedCell.btnImgStandardService setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            [managedObject setValue:@"true" forKey:@"isActive"];
            
            [tappedCell.btnCheckBoxAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [managedObject setValue:@"false" forKey:@"isActive"];
            
            [tappedCell.btnCheckBoxAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
        }
        NSError *saveError = nil;
        [contextAgreementList save:&saveError];
        [_tblAgreement reloadData];
    }
}
-(void)getAgreementListFromMaster
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    
    arrayAgreementCheckList=[dictMasters valueForKey:@"AgreementChecklist"];
    
    NSMutableArray *arrTempAgreementList;
    arrTempAgreementList=[[NSMutableArray alloc]init];
    for (int i=0; i<arrayAgreementCheckList.count; i++)
    {
        NSDictionary *dict=[arrayAgreementCheckList objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]] isEqualToString:@"1"])
        {
            [arrTempAgreementList addObject:dict];
        }
    }
    arrayAgreementCheckList=arrTempAgreementList;
    
    [self fetchAgreementCheckListSales];
    
    
    /*NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrayAgreementCheckList];
     NSArray *arrayWithoutDuplicates = [orderedSet array];
     arrayAgreementCheckList=arrayWithoutDuplicates;*/
    
    [_tblAgreement reloadData];
    
}
-(void)fetchAgreementCheckListSales
{
    appDelegateAgreementList = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextAgreementList = [appDelegateAgreementList managedObjectContext];
    
    NSFetchRequest *requestAgreementList = [[NSFetchRequest alloc] init];
    entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:contextAgreementList];
    [requestAgreementList setEntity:entityLeadAgreementChecklistSetups];
    NSPredicate *predicateAgreementList =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestAgreementList setPredicate:predicateAgreementList];
    
    sortDescriptorAgreementList = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptorsAgreementList = [NSArray arrayWithObject:sortDescriptorAgreementList];
    [requestAgreementList setSortDescriptors:sortDescriptorsAgreementList];
    
    self.fetchedResultsControllerSalesInfoAgreementList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestAgreementList managedObjectContext:contextAgreementList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfoAgreementList setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfoAgreementList performFetch:&error1];
    arrAllObjAgreementList = [self.fetchedResultsControllerSalesInfoAgreementList fetchedObjects];
    if (arrAllObjAgreementList.count==0)
    {
        [self saveAgreementCheckList];
    }
    else
    {
        arrayAgreementCheckList=arrAllObjAgreementList;
        
        
        /* NSArray *arrTempAgreementListMaster;
         arrTempAgreementListMaster=arrayAgreementCheckList;
         NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
         [arrTemp addObjectsFromArray:arrayAgreementCheckList];
         
         for (int i=0; i<arrTemp.count; i++)
         {
         NSManagedObject *matchesAgreement=[arrTemp objectAtIndex:i];
         NSString *strAgreementIdDataBase;
         strAgreementIdDataBase=[NSString stringWithFormat:@"%@",[matchesAgreement valueForKey:@"agreementChecklistId"]];
         for (int j=0; j<arrTempAgreementListMaster.count; j++)
         {
         NSDictionary *dict=[arrTempAgreementListMaster objectAtIndex:j];
         NSString *strAgreementId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AgreementChecklistId"]];
         if ([strAgreementIdDataBase isEqualToString:strAgreementId])
         {
         [arrTemp removeObject:matchesAgreement];
         break;
         }
         
         }
         }
         arrayAgreementCheckList=arrTemp;*/
        
    }
}
-(void)fetchAgreementCheckListSalesOnComplete
{
    appDelegateAgreementList = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextAgreementList = [appDelegateAgreementList managedObjectContext];
    
    NSFetchRequest *requestAgreementList = [[NSFetchRequest alloc] init];
    entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:contextAgreementList];
    [requestAgreementList setEntity:entityLeadAgreementChecklistSetups];
    NSPredicate *predicateAgreementList =[NSPredicate predicateWithFormat:@"leadId=%@ AND isActive=%@",strLeadId,@"true"];
    
    [requestAgreementList setPredicate:predicateAgreementList];
    
    sortDescriptorAgreementList = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptorsAgreementList = [NSArray arrayWithObject:sortDescriptorAgreementList];
    [requestAgreementList setSortDescriptors:sortDescriptorsAgreementList];
    
    self.fetchedResultsControllerSalesInfoAgreementList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestAgreementList managedObjectContext:contextAgreementList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfoAgreementList setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfoAgreementList performFetch:&error1];
    arrAllObjAgreementList = [self.fetchedResultsControllerSalesInfoAgreementList fetchedObjects];
    
    arrayAgreementCheckList=arrAllObjAgreementList;
    
}

-(void)saveAgreementCheckList
{
    //NSArray *arrAgreementCheckList;//=[dictLeadDetail valueForKeyPath:@"LeadAgreementChecklistSetups"];
    
    for (int m=0; m<arrayAgreementCheckList.count; m++)
    {
        NSMutableDictionary *dictAgreementCheckList=[[NSMutableDictionary alloc]init];
        dictAgreementCheckList=[arrayAgreementCheckList objectAtIndex:m];
        
        entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:context];
        LeadAgreementChecklistSetups *objentityLeadAgreementChecklistSetups = [[LeadAgreementChecklistSetups alloc]initWithEntity:entityLeadAgreementChecklistSetups insertIntoManagedObjectContext:context];
        
        
        objentityLeadAgreementChecklistSetups.leadId=strLeadId;
        
        objentityLeadAgreementChecklistSetups.leadAgreementChecklistSetupId=@"0";
        
        objentityLeadAgreementChecklistSetups.agreementChecklistId=[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"AgreementChecklistId"]];
        
        objentityLeadAgreementChecklistSetups.agreementChecklistSysName=[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"SysName"]];
        
        if([[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"IsActive"]] isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"IsActive"]] isEqualToString:@"true"])
        {
            objentityLeadAgreementChecklistSetups.isActive=@"true";
            objentityLeadAgreementChecklistSetups.isActive=@"false";
            
        }
        else
        {
            objentityLeadAgreementChecklistSetups.isActive=@"false";
        }
        //Nilind 16 Nov
        
        objentityLeadAgreementChecklistSetups.userName=strUserName;
        objentityLeadAgreementChecklistSetups.companyKey=strCompanyKey;
        
        //........................
        NSError *error1;
        [context save:&error1];
        
    }
    if(arrayAgreementCheckList.count>0)
    {
        [self fetchAgreementCheckListSales];
    }
}
-(void)UpdateAgreementCheckListSales
{
    appDelegateAgreementList = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextAgreementList = [appDelegate managedObjectContext];
    
    NSFetchRequest *requestAgreementList = [[NSFetchRequest alloc] init];
    entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:contextAgreementList];
    [requestAgreementList setEntity:entityLeadAgreementChecklistSetups];
    NSPredicate *predicateAgreementList =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestAgreementList setPredicate:predicateAgreementList];
    
    sortDescriptorAgreementList = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptorsAgreementList = [NSArray arrayWithObject:sortDescriptorAgreementList];
    [requestAgreementList setSortDescriptors:sortDescriptorsAgreementList];
    
    self.fetchedResultsControllerSalesInfoAgreementList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestAgreementList managedObjectContext:contextAgreementList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfoAgreementList setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfoAgreementList performFetch:&error1];
    arrAllObjAgreementList = [self.fetchedResultsControllerSalesInfoAgreementList fetchedObjects];
    if (arrAllObjAgreementList.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjAgreementList.count; k++)
        {
            matchesAgreementList=arrAllObjAgreementList[k];
        }
    }
}
- (IBAction)actionOnBtnMarkAsLost:(id)sender
{
    [self endEditing];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert!"
                                  message:@"Are you sure to mark Opportunity as lost?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
        chkForLost=YES;
        [self updateLeadIdDetail];
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            //[global AlertMethod:@"Alert!":@"No Internet connection available"];
        }
        else
        {
            /*[DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
             
             [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
             [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];*/
            
        }
        
        [self goToAppointment];
        // [alert dismissViewControllerAnimated:YES completion:nil];
        
        
    }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"NO"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)actionOnJanuary:(UIButton *)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnJan.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnJan setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"January"];
            
            
        }
        else
        {
            
            [_btnJan setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"January"];
        }
    }
    
}

- (IBAction)actionOnFebruary:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnFeb.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnFeb setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"February"];
            
        }
        else
        {
            
            [_btnFeb setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"February"];
            
        }
    }
    
}

- (IBAction)actionOnMarch:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnMar.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnMar setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"March"];
            
        }
        else
        {
            
            [_btnMar setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"March"];
            
        }
    }
    
}

- (IBAction)actionOnApril:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnApr.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnApr setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"April"];
            
        }
        else
        {
            
            [_btnApr setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"April"];
            
        }
    }
    
}

- (IBAction)actionOnMay:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnMay.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnMay setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"May"];
            
        }
        else
        {
            
            [_btnMay setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"May"];
            
        }
    }
    
}

- (IBAction)actionOnJune:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnJun.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnJun setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"June"];
            
        }
        else
        {
            
            [_btnJun setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"June"];
            
        }
    }
    
}

- (IBAction)actionOnJuly:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnJul.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnJul setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"July"];
            
        }
        else
        {
            
            [_btnJul setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"July"];
            
        }
    }
    
}

- (IBAction)actionOnAugust:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnAug.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnAug setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"August"];
            
        }
        else
        {
            
            [_btnAug setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"August"];
            
        }
    }
    
}

- (IBAction)actionOnSeptember:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnSept.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnSept setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"September"];
            
        }
        else
        {
            
            [_btnSept setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"September"];
            
        }
    }
    
}

- (IBAction)actionOnOctober:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnOct.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnOct setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"October"];
            
        }
        else
        {
            
            [_btnOct setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"October"];
            
        }
    }
    
}

- (IBAction)actionOnNovember:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnNov.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnNov setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"November"];
            
        }
        else
        {
            
            [_btnNov setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"November"];
            
        }
    }
    
}

- (IBAction)actionOnDecember:(id)sender
{
    [self endEditing];
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
    }
    else
    {
        isEditedInSalesAuto=YES;
        
        if ([_btnDec.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
        {
            
            [_btnDec setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths addObject:@"December"];
            
        }
        else
        {
            
            [_btnDec setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            [arrPreferredMonths removeObject:@"December"];
            
        }
    }
    
}

- (IBAction)actionBackToSchedule:(id)sender
{
    [self endEditing];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert!"
                                  message:@"Are you sure to move on Appointment List?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
        
        [self goToAppointment];
        
        
    }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"NO"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)actionOnPrimaryPhone:(id)sender
{
    [self endEditing];
    if (_lblPrimaryPhoneValue.text.length>0)
    {
        [global calling:_lblPrimaryPhoneValue.text];
        
    }
}
- (IBAction)actionOnSecondaryPhone:(id)sender
{
    [self endEditing];
    if (_lblSecondarPhoneValue.text.length>0)
    {
        [global calling:_lblSecondarPhoneValue.text];
        
    }
}
- (IBAction)actionOnEmailId:(id)sender
{
    [self endEditing];
    if (_lblEmailIdValue.text.length>0)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            [global emailComposer:_lblEmailIdValue.text :@"" :@"" :self];
        }
        else
        {
            [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
            
            
        }
    }
    else
    {
        
    }
}
- (IBAction)actionOnPrintAgreement:(id)sender
{
    [self endEditing];
    [self oPentPrinterView:[NSString stringWithFormat:@"%@",strLeadNumber]];
    
}
-(void)oPentPrinterView :(NSString*)strLeadToSend{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PrintiPhoneViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"PrintiPhoneViewController"];
    objSignViewController.strAgreementName=@"";
    objSignViewController.strProposalName=@"";
    objSignViewController.strInspectionName=@"";
    objSignViewController.strLeadId=strLeadToSend;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

- (IBAction)actionOnServiceAddress:(id)sender
{
    [self endEditing];
    if (_lblServiceAddressValue.text.length>0)
    {
        [global redirectOnAppleMap:self :_lblServiceAddressValue.text];
        
    }
}

- (IBAction)actionOnBillingAddress:(id)sender
{
    [self endEditing];
    if (_lblBillingAddressValue.text.length>0)
    {
        [global redirectOnAppleMap:self :_lblBillingAddressValue.text];
        
    }
}
-(NSAttributedString*)getUnderLineAttributedString:(NSString*)strText
{
    if(![strText isKindOfClass:[NSString class]])
    {
        strText=@"";
    }
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:strText];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    
    return attributeString;
    
}
- (IBAction)actionOnApplyCoupon:(id)sender
{
    [self endEditing];
    //[self fetchFromCoreDataStandard]; // commented by Akshay
    if(_txtEnterCouponNo.text.length==0)
    {
        [global AlertMethod:@"Alert!" :@"Please enter coupon code"];
        _txtEnterCouponNo.text=@"";
    }
    else /// working for clarkpest
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        // arrDiscountCoupon=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
        NSArray *arrayMasterCoupons=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
        
        NSString *strCouponDiscount=_txtEnterCouponNo.text;
        BOOL isEnteredCouponIsValid = NO;
        for(NSDictionary *dict in arrayMasterCoupons)
        {
            if([strCouponDiscount isEqualToString:[dict valueForKey:@"DiscountCode"]])
            {
                isEnteredCouponIsValid = YES;
                
                NSString *strDiscountUsage=[dictDiscountUsageFromCode valueForKey:strCouponDiscount];
                
                BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount:@"Coupon":strDiscountUsage];
                
                if (chkDiscountApplied==YES)
                {
                    [global AlertMethod:@"Alert!" :@"Coupon already applied"];
                    return;
                }
                else
                {
                    BOOL chkValidDiscount=[self discountValidityCheck:dict];
                    
                    if(chkValidDiscount)
                    {
                        // coupon is applicable only on Initial price
                        //                        if([[dict valueForKey:@"ApplicableForInitial"]boolValue]==YES||[[dict valueForKey:@"ApplicableForInitial"]intValue]==1||[[dict valueForKey:@"ApplicableForInitial"]intValue]==0)
                        //                        {
                        NSString *strIsDiscountPercent=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
                        if([_lblNewInitialPriceTotalPrice.text floatValue]==0)
                        {
                            [global AlertMethod:@"Alert!" :@"Coupon can not be applied"];
                            return;
                        }
                        else
                        {
                            if ([strIsDiscountPercent isEqualToString:@"1"] || [strIsDiscountPercent isEqualToString:@"true"] || [strIsDiscountPercent isEqualToString:@"True"])
                            {
                                float dicountPercent = [[dict valueForKey:@"DiscountPercent"] floatValue];
                                
                                appliedDiscountInitial = (subTotalInitialPrice*dicountPercent)/100.0;
                                if(appliedDiscountInitial>=[_lblNewInitialPriceTotalPrice.text floatValue])// agar percent value subtotal se jyada h to jitna subtotal h utna hi discount lagega
                                {
                                    appliedDiscountInitial = subTotalInitialPrice;
                                    subTotalInitialPrice = 0.0;
                                }
                                else
                                {
                                    subTotalInitialPrice = subTotalInitialPrice-appliedDiscountInitial;
                                }
                                [self saveAppliedDiscountCouponAndCreditCoreData:dict :@"Coupon"];
                                return;
                                
                            }
                            else
                            {
                                float dicountAmt = [[dict valueForKey:@"DiscountAmount"] floatValue];
                                if(dicountAmt>[_lblNewInitialPriceTotalPrice.text floatValue])
                                {
                                    appliedDiscountInitial = subTotalInitialPrice;
                                    subTotalInitialPrice = 0.0;
                                }
                                else
                                {
                                    appliedDiscountInitial = dicountAmt;
                                    subTotalInitialPrice = subTotalInitialPrice-appliedDiscountInitial;
                                }
                                
                                [self saveAppliedDiscountCouponAndCreditCoreData:dict :@"Coupon"];
                                return;
                            }
                        }
                        //}
                    }
                    else
                    {
                        [global AlertMethod:@"Alert!" :@"Invalid Coupon"];
                        return;
                    }
                }
            }
        }
        if(isEnteredCouponIsValid == NO)
        {
            [global AlertMethod:@"Alert!" :@"Invalid Coupon"];
            return;
        }
    }
    
    /////////////////////////////////////////akshay/////////////////////////////////////////////
    
    //        //14 June Temp
    //        NSString *strDiscountUsage=[dictDiscountUsageFromCode valueForKey:strCouponDiscount];
    //
    //        BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount:@"Coupon":strDiscountUsage];
    //
    //        if (chkDiscountApplied==YES)
    //        {
    //            [global AlertMethod:@"Alert!" :@"Coupon already applied"];
    //            return;
    //        }
    //        else
    //        {
    //            BOOL chkValidDiscount = NO;
    //            for(int i=0;i<arrayMasterCoupons.count;i++)
    //            {
    //                NSDictionary *dict=[arrayMasterCoupons objectAtIndex:i];
    //
    //                if ([strCouponDiscount isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountCode"]]])
    //                {
    //                    NSLog(@"Matched Discount");
    //
    //                    chkValidDiscount=[self discountValidityCheck:dict];
    //                    if (chkValidDiscount==YES)
    //                    {
    //                        appliedDiscountMaint=0.0;
    //                        appliedDiscountInitial=0.0;
    //
    //                        NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Coupon"];
    //
    //                        float totalDiscountAmoutAlreadyPresent=0.0;
    //
    //                        for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
    //
    //                            NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
    //
    //                            NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedInitialDiscount"]];
    //
    //                            float discountAmountPresentt=[strDiscountAmountPresent floatValue];
    //
    //                            if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"true"])
    //                            {
    //                              // coupon kewal initial price per lagega
    //
    //                            }
    //                            else
    //                            {
    //                                totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //                            }
    //                        }
    //
    //                        BOOL ifToSaveOrNot = NO;
    //
    //                        NSString *strDiscountAmountTemp=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]];
    //
    //                        float discount=[strDiscountAmountTemp floatValue];
    //
    //                        NSString *strIsDiscountTypeNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
    //
    //                        if ([strIsDiscountTypeNew isEqualToString:@"1"] || [strIsDiscountTypeNew isEqualToString:@"true"] || [strIsDiscountTypeNew isEqualToString:@"True"]) {
    //
    //                            NSString *strDiscountPercenteNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]];
    //
    //                            float discountPercentValue=[strDiscountPercenteNew floatValue];
    //
    //                            if (discountPercentValue==100 || discountPercentValue>100)
    //                            {
    //                                discountPercentValue=100.00;
    //                            }
    //
    //                            discount=(globalAmountInitialPrice*discountPercentValue)/100;
    //                        }
    //
    //                        if (!(globalAmountInitialPrice>=(totalDiscountAmoutAlreadyPresent+discount)))
    //                        {
    //                            if (globalAmountInitialPrice<(totalDiscountAmoutAlreadyPresent+discount))
    //                            {
    //                                float amountDiscountToApplyCredit=globalAmountInitialPrice-totalDiscountAmoutAlreadyPresent;
    //
    //                                NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
    //
    //                                [dictTempToSave addEntriesFromDictionary:dict];
    //
    //                                [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"DiscountAmount"];
    //
    //                                dict=dictTempToSave;
    //
    //                                ifToSaveOrNot=YES;
    //
    //                                if (amountDiscountToApplyCredit>0) {
    //
    //                                    ifToSaveOrNot=YES;
    //                                    appliedDiscountInitial=amountDiscountToApplyCredit;
    //                                }
    //                                else
    //                                {
    //                                    ifToSaveOrNot=NO;
    //                                    appliedDiscountInitial=0.0;
    //                                }
    //                            }
    //                        }
    //                        else
    //                        {
    //                            ifToSaveOrNot=YES;
    //                        }
    //
    //                        if (ifToSaveOrNot) {
    //
    //                            NSString *strDiscountPerCheck=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
    //                            if ([strDiscountPerCheck isEqualToString:@"1"]||[strDiscountPerCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
    //                            {
    //                                [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :@"0" CheckForDiscountPer:@"1" DiscountPerValue:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]];
    //                            }
    //                            else
    //                            {
    //                                [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]]CheckForDiscountPer:@"0" DiscountPerValue:@"0"];
    //                            }
    //
    //                            if(chkForSaveCoupon==YES)
    //                            {
    //                                [self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Coupon"];
    //                                [self fetchForAppliedDiscountFromCoreData];
    //
    //                                [self calculationForCouponAndCredit];
    //                            }
    //
    //                        }else{
    //
    //                            [global AlertMethod:Alert :@"Coupon can not be more then billing amount."];
    //
    //                        }
    //                        NSLog(@"VALID DISCOUNT");
    //                    }
    //                    else
    //                    {
    //                        NSLog(@"INVALID DISCOUNT");
    //                    }
    //                    break;
    //                }
    //                else
    //                {
    //                    chkValidDiscount=NO;
    //                }
    //            }
    //            if(chkValidDiscount==NO)// && chkForSaveCoupon==NO)
    //            {
    //                [global AlertMethod:@"Alert" :@"Invalid coupon"];
    //
    //            }
    //            else
    //            {
    //                if(chkForSaveCoupon==NO)
    //                {
    //                    [global AlertMethod:@"Alert" :@"Invalid coupon"];
    //
    //                }
    //            }
    //        }
    
    ////
    
    
    
    
    
    
    
    /*else if(arrStanInitialPrice.count==0 && arrTempBundleServiceSysName.count>0)
     {
     
     [global AlertMethod:@"Alert!" :@"No service available to apply coupon"];
     _txtEnterCouponNo.text=@"";
     
     }*/
    //    else
    //    {
    //        arrDiscountCoupon=[[NSMutableArray alloc]init];
    //        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //        arrDiscountCoupon=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
    //        //arrTempCoupon=[[NSMutableArray alloc]init];
    //        if ([arrDiscountCoupon isKindOfClass:[NSArray class]])
    //        {
    //            NSString *strCouponDiscount=_txtEnterCouponNo.text;
    //            //14 June Temp
    //            NSString *strDiscountUsage=[dictDiscountUsageFromCode valueForKey:strCouponDiscount];
    //
    //            BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount:@"Coupon":strDiscountUsage];
    //            //End
    //            // BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount:@"Coupon"];
    //            if (chkDiscountApplied==YES)
    //            {
    //                [global AlertMethod:@"Alert!" :@"Coupon already applied"];
    //            }
    //            else
    //            {
    //                BOOL chkValidDiscount;
    //                chkValidDiscount=NO;
    //                for(int i=0;i<arrDiscountCoupon.count;i++)
    //                {
    //                    NSDictionary *dict=[arrDiscountCoupon objectAtIndex:i];
    //
    //                    if ([strCouponDiscount isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountCode"]]])
    //                    {
    //                        NSLog(@"Matched Discount");
    //
    //
    //                        chkValidDiscount=[self discountValidityCheck:dict];
    //                        if (chkValidDiscount==YES)
    //                        {
    //                            appliedDiscountMaint=0.0;
    //                            appliedDiscountInitial=0.0;
    //
    //
    //                            NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Coupon"];
    //
    //                            float totalDiscountAmoutAlreadyPresent=0.0;
    //
    //                            for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
    //
    //                                NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
    //
    //                                NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedInitialDiscount"]];
    //
    //                                float discountAmountPresentt=[strDiscountAmountPresent floatValue];
    //
    //                                if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"true"])
    //                                {
    //
    //
    //                                }else{
    //                                    totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //                                }
    //                            }
    //
    //                            BOOL ifToSaveOrNot;
    //
    //                            ifToSaveOrNot=NO;
    //
    //                            NSString *strDiscountAmountTemp=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]];
    //
    //                            float discount=[strDiscountAmountTemp floatValue];
    //
    //                            NSString *strIsDiscountTypeNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
    //
    //                            if ([strIsDiscountTypeNew isEqualToString:@"1"] || [strIsDiscountTypeNew isEqualToString:@"true"] || [strIsDiscountTypeNew isEqualToString:@"True"]) {
    //
    //                                NSString *strDiscountPercenteNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]];
    //
    //                                float discountPercentValue=[strDiscountPercenteNew floatValue];
    //
    //                                if (discountPercentValue==100 || discountPercentValue>100)
    //                                {
    //                                    discountPercentValue=100.00;
    //                                }
    //
    //                                discount=(globalAmountInitialPrice*discountPercentValue)/100;
    //
    //                            }
    //
    //
    //                            if (!(globalAmountInitialPrice>=(totalDiscountAmoutAlreadyPresent+discount))) {
    //
    //
    //                                if (globalAmountInitialPrice<(totalDiscountAmoutAlreadyPresent+discount)) {
    //
    //                                    float amountDiscountToApplyCredit=globalAmountInitialPrice-totalDiscountAmoutAlreadyPresent;
    //
    //                                    NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
    //
    //                                    [dictTempToSave addEntriesFromDictionary:dict];
    //
    //                                    [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"DiscountAmount"];
    //
    //                                    dict=dictTempToSave;
    //
    //                                    ifToSaveOrNot=YES;
    //
    //                                    if (amountDiscountToApplyCredit>0) {
    //
    //                                        ifToSaveOrNot=YES;
    //                                        appliedDiscountInitial=amountDiscountToApplyCredit;
    //
    //
    //                                    }else{
    //
    //                                        ifToSaveOrNot=NO;
    //                                        appliedDiscountInitial=0.0;
    //
    //
    //                                    }
    //
    //                                }
    //                            }else{
    //
    //                                ifToSaveOrNot=YES;
    //
    //                            }
    //
    //
    //                            if (ifToSaveOrNot) {
    //
    //                                NSString *strDiscountPerCheck=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
    //                                if ([strDiscountPerCheck isEqualToString:@"1"]||[strDiscountPerCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
    //                                {
    //                                    [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :@"0" CheckForDiscountPer:@"1" DiscountPerValue:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]];
    //                                }
    //                                else
    //                                {
    //                                    [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]]CheckForDiscountPer:@"0" DiscountPerValue:@"0"];
    //                                }
    //
    //                                if(chkForSaveCoupon==YES)
    //                                {
    //                                    [self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Coupon"];
    //                                    [self fetchForAppliedDiscountFromCoreData];
    //
    //                                    [self calculationForCouponAndCredit];
    //                                }
    //
    //                            }else{
    //
    //                                [global AlertMethod:Alert :@"Coupon can not be more then billing amount."];
    //
    //                            }
    //                            NSLog(@"VALID DISCOUNT");
    //
    //
    //                        }
    //                        else
    //                        {
    //                            NSLog(@"INVALID DISCOUNT");
    //
    //                        }
    //
    //                        break;
    //
    //                    }
    //                    else
    //                    {
    //                        chkValidDiscount=NO;
    //                    }
    //
    //                }
    //                if(chkValidDiscount==NO)// && chkForSaveCoupon==NO)
    //                {
    //                    [global AlertMethod:@"Alert" :@"Invalid coupon"];
    //
    //                }
    //                else
    //                {
    //                    if(chkForSaveCoupon==NO)
    //                    {
    //                        [global AlertMethod:@"Alert" :@"Invalid coupon"];
    //
    //                    }
    //                }
    //            }
    //        }
    //        // [self fetchForAppliedDiscountFromCoreData];
    //
    //
    //    }
    
    [self finalHeightManage];
    _txtEnterCouponNo.text=@"";
}


- (IBAction)actionOnSelectCredit:(id)sender
{
    [self endEditing];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    
    if (netStatusWify1== NotReachable)
    {
        arrDiscountCredit=[[NSMutableArray alloc]init];
        arrDiscountCredit=[dictMasters valueForKey:@"DiscountSetupMasterCredit"];
    }
    else
    {
        arrDiscountCredit=[[NSMutableArray alloc]init];
        arrDiscountCredit=[dictMasters valueForKey:@"DiscountSetupMasterCredit"];
        
        NSMutableArray *arrTemp,*arrTempNew;
        arrTemp=[[NSMutableArray alloc]init];
        arrTempNew=[[NSMutableArray alloc]init];
        
        //[arrTemp addObjectsFromArray:arrDiscountCredit];
        [arrTempNew addObjectsFromArray:arrDiscountCredit];
        for(int i=0;i<arrDiscountCredit.count;i++)
        {
            NSDictionary *dictCredit=[arrDiscountCredit objectAtIndex:i];
            
            for (int j=0; j<arrAppliedCreditDetail.count; j++)
            {
                NSDictionary *dict=[arrAppliedCreditDetail objectAtIndex:j];
                if ([[NSString stringWithFormat:@"%@",[dictCredit valueForKey:@"SysName"]]isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]])
                {
                    //[arrTemp removeObjectAtIndex:i];
                    [arrTemp addObject:dictCredit];
                    break;
                }
            }
        }
        [arrTempNew removeObjectsInArray:arrTemp];
        arrDiscountCredit=[[NSMutableArray alloc]init];
        [arrDiscountCredit addObjectsFromArray:arrTempNew];
        //[arrDiscountCredit addObjectsFromArray:arrTemp];
        
    }
    if([arrDiscountCredit isKindOfClass:[NSArray class]])
    {
        if(arrDiscountCredit.count==0)
        {
            [global AlertMethod:@"Alert" :@"No credit available"];
        }
        else
        {
            tblData.tag=4;
            [self tableLoad:tblData.tag];
            
        }
    }
    else
    {
        [global AlertMethod:@"Alert" :@"No credit available"];
    }
    
}

- (IBAction)actionOnAddCredit:(id)sender
{
    [self endEditing];
    if(_btnSelectCredit.titleLabel.text.length==0 || [_btnSelectCredit.titleLabel.text isEqualToString:@"Select Credit"])
    {
        [global AlertMethod:@"Alert!" :@"Please select code"];
    }
    // clarkpest
    else
    {
        appliedDiscountInitial = 0.0;
        appliedDiscountMaint = 0.0;
        
        if([[dictForCreditDiscount valueForKey:@"ApplicableForInitial"] boolValue]==YES && [[dictForCreditDiscount valueForKey:@"ApplicableForMaintenance"] boolValue]==YES)
        {
            NSString *strDiscountCredit=[NSString stringWithFormat:@"%@",[dictForCreditDiscount valueForKey:@"SysName"]];
            BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strDiscountCredit:@"Credit":@"OneTime"];
            
            if (chkDiscountApplied==YES)
            {
                [global AlertMethod:@"Alert!" :@"Credit Coupon already applied"];
                [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                return;
            }
            else
            {
                if([_lblNewInitialPriceTotalPrice.text floatValue]==0 && [_lblNewMaintPriceTotalPrice.text floatValue]==0)
                {
                    [global AlertMethod:@"Alert!" :@"Credit can not be applied"];
                    [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                    return;
                }
                else
                {
                    if([_lblNewInitialPriceTotalPrice.text floatValue]==0)
                    {// discount will not be applied on initial price
                        [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                    }
                    else
                    {
                        if([[dictForCreditDiscount valueForKey:@"IsDiscountPercent"] boolValue]==YES || [[dictForCreditDiscount valueForKey:@"IsDiscountPercent"] intValue]==1)
                        {
                            float dicountPer = [[dictForCreditDiscount valueForKey:@"DiscountPercent"] floatValue];
                            
                            appliedDiscountInitial = (subTotalInitialPrice*dicountPer)/100.0;
                            if(appliedDiscountInitial>=[_lblNewInitialPriceTotalPrice.text floatValue])// agar percent value subtotal se jyada h to jitna subtotal h utna hi discount lagega
                            {
                                appliedDiscountInitial = subTotalInitialPrice;
                                subTotalInitialPrice = 0.0;
                            }
                            else
                            {
                                subTotalInitialPrice = subTotalInitialPrice-appliedDiscountInitial;
                            }
                            // [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
                        }
                        else
                        {
                            float dicountAmt = [[dictForCreditDiscount valueForKey:@"DiscountAmount"] floatValue];
                            if(dicountAmt>[_lblNewInitialPriceTotalPrice.text floatValue])
                            {
                                appliedDiscountInitial = subTotalInitialPrice;
                                subTotalInitialPrice = 0.0;
                            }
                            else
                            {
                                appliedDiscountInitial = dicountAmt;
                                subTotalInitialPrice = subTotalInitialPrice-appliedDiscountInitial;
                            }
                            
                            // [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
                        }
                    }
                    
                    if([_lblNewMaintPriceTotalPrice.text floatValue]==0)
                    { // discount will not be applied on maintenance price
                        [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                    }
                    else
                    {
                        if([[dictForCreditDiscount valueForKey:@"IsDiscountPercent"] boolValue]==YES || [[dictForCreditDiscount valueForKey:@"IsDiscountPercent"] intValue]==1)
                        {
                            float dicountPer = [[dictForCreditDiscount valueForKey:@"DiscountPercent"] floatValue];
                            
                            appliedDiscountMaint = (subTotalMaintPrice*dicountPer)/100.0;
                            if(appliedDiscountMaint>=[_lblNewMaintPriceTotalPrice.text floatValue])
                            {
                                appliedDiscountMaint = subTotalMaintPrice;
                                subTotalMaintPrice = 0.0;
                            }
                            else
                            {
                                subTotalMaintPrice = subTotalMaintPrice - appliedDiscountMaint;
                            }
                            //[self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
                        }
                        else
                        {
                            float dicountAmt = [[dictForCreditDiscount valueForKey:@"DiscountAmount"] floatValue];
                            if(dicountAmt>=[_lblNewMaintPriceTotalPrice.text floatValue])
                            {
                                appliedDiscountMaint = subTotalMaintPrice;
                                subTotalMaintPrice = 0.0;
                            }
                            else
                            {
                                appliedDiscountMaint = dicountAmt;
                                subTotalMaintPrice = subTotalMaintPrice - appliedDiscountMaint;
                            }
                            
                            // [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
                        }
                    }
                }
            }
            [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
            [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
        }
        else if([[dictForCreditDiscount valueForKey:@"ApplicableForInitial"] boolValue]==YES)
        {
            NSString *strDiscountCredit=[NSString stringWithFormat:@"%@",[dictForCreditDiscount valueForKey:@"SysName"]];
            BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strDiscountCredit:@"Credit":@"OneTime"];
            if (chkDiscountApplied==YES)
            {
                [global AlertMethod:@"Alert!" :@"Credit Coupon already applied"];
                [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                return;
            }
            else
            {
                if([_lblNewInitialPriceTotalPrice.text floatValue]==0)
                {
                    [global AlertMethod:@"Alert!" :@"Credit can not be applied"];
                    [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                    return;
                }
                else
                {
                    if([[dictForCreditDiscount valueForKey:@"IsDiscountPercent"] boolValue]==YES || [[dictForCreditDiscount valueForKey:@"IsDiscountPercent"] intValue]==1)
                    {
                        float dicountPer = [[dictForCreditDiscount valueForKey:@"DiscountPercent"] floatValue];
                        appliedDiscountInitial = (subTotalInitialPrice*dicountPer)/100.0;
                        if(appliedDiscountInitial>=[_lblNewInitialPriceTotalPrice.text floatValue])// agar percent value subtotal se jyada h to jitna subtotal h utna hi discount lagega
                        {
                            appliedDiscountInitial = subTotalInitialPrice;
                            subTotalInitialPrice = 0.0;
                        }
                        else
                        {
                            subTotalInitialPrice = subTotalInitialPrice-appliedDiscountInitial;
                        }
                        [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                        [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
                    }
                    else
                    {
                        float dicountAmt = [[dictForCreditDiscount valueForKey:@"DiscountAmount"] floatValue];
                        if(dicountAmt>[_lblNewInitialPriceTotalPrice.text floatValue])
                        {
                            appliedDiscountInitial = subTotalInitialPrice;
                            subTotalInitialPrice = 0.0;
                        }
                        else
                        {
                            appliedDiscountInitial = dicountAmt;
                            subTotalInitialPrice = subTotalInitialPrice-appliedDiscountInitial;
                        }
                        [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                        [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
                    }
                }
            }
        }
        else if ([[dictForCreditDiscount valueForKey:@"ApplicableForMaintenance"] boolValue]==YES || [[dictForCreditDiscount valueForKey:@"ApplicableForMaintenance"] isEqualToString:@"1"])
        {
            NSString *strDiscountCredit=[NSString stringWithFormat:@"%@",[dictForCreditDiscount valueForKey:@"SysName"]];
            BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strDiscountCredit:@"Credit":@"OneTime"];
            if (chkDiscountApplied==YES)
            {
                [global AlertMethod:@"Alert!" :@"Credit Coupon already applied"];
                [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                return;
            }
            else
            {
                if([_lblNewMaintPriceTotalPrice.text floatValue]==0)
                {
                    [global AlertMethod:@"Alert!" :@"Credit can not be applied"];
                    [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                    return;
                }
                else
                {
                    if([[dictForCreditDiscount valueForKey:@"IsDiscountPercent"] boolValue]==YES || [[dictForCreditDiscount valueForKey:@"IsDiscountPercent"] intValue]==1)
                    {
                        float dicountPer = [[dictForCreditDiscount valueForKey:@"DiscountPercent"] floatValue];
                        appliedDiscountMaint = (subTotalMaintPrice*dicountPer)/100.0;
                        if(appliedDiscountMaint>=[_lblNewMaintPriceTotalPrice.text floatValue])
                        {
                            appliedDiscountMaint = subTotalMaintPrice;
                            subTotalMaintPrice = 0.0;
                        }
                        else
                        {
                            subTotalMaintPrice = subTotalMaintPrice - appliedDiscountMaint;
                        }
                        [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                        [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
                    }
                    else
                    {
                        float dicountAmt = [[dictForCreditDiscount valueForKey:@"DiscountAmount"] floatValue];
                        
                        if(dicountAmt>=[_lblNewMaintPriceTotalPrice.text floatValue])
                        {
                            appliedDiscountMaint = subTotalMaintPrice;
                            subTotalMaintPrice = 0.0;
                        }
                        else
                        {
                            appliedDiscountMaint = dicountAmt;
                            subTotalMaintPrice = subTotalMaintPrice - appliedDiscountMaint;
                        }
                        [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
                        [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
                    }
                }
            }
        }
        
        
        //        if(arrDiscountCredit.count==0)
        //        {
        //          [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
        //        }
        //        else
        //        {
        //            for(NSDictionary *dict in arrDiscountCredit)
        //            {
        //                if([[dict valueForKey:@"discountSysName"] isEqualToString:[dictForCreditDiscount valueForKey:@"SysName"]])
        //                {
        //                    [global AlertMethod:@"Alert!" :@"This credit already applied"];
        //                    return;
        //                }
        //            }
        //
        //            [self saveAppliedDiscountCouponAndCreditCoreData:dictForCreditDiscount :@"Credit"];
        //        }
    }
    
    ///////
    //    else
    //    {
    //        if(arrStanInitialPriceAllServices.count==0)
    //        {
    //            [global AlertMethod:@"Alert!" :@"Credit applicable service not available"];
    //        }
    //        else
    //        {
    //            arrDiscountCredit=[[NSMutableArray alloc]init];
    //            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //            NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //            arrDiscountCredit=[dictMasters valueForKey:@"DiscountSetupMasterCredit"];
    //            if ([arrDiscountCredit isKindOfClass:[NSArray class]])
    //            {
    //                NSString *strDiscountCredit=[NSString stringWithFormat:@"%@",[dictForCreditDiscount valueForKey:@"SysName"]];
    //
    //                BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strDiscountCredit:@"Credit":@"OneTime"];
    //                if (chkDiscountApplied==YES)
    //                {
    //                    [global AlertMethod:@"Alert!" :@"Credit Coupon already applied"];
    //                    [_btnSelectCredit setTitle:@"Select Credit" forState:UIControlStateNormal];
    //                }
    //                else
    //                {
    //                    for(int i=0;i<arrDiscountCredit.count;i++)
    //                    {
    //                        NSDictionary *dict=[arrDiscountCredit objectAtIndex:i];
    //
    //                        if ([strDiscountCredit isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]])
    //                        {
    //                            NSLog(@"Matched Credit Discount");
    //
    //                            appliedDiscountMaint=0.0;
    //                            appliedDiscountInitial=0.0;
    //
    //                            NSUserDefaults *defsStan=[NSUserDefaults standardUserDefaults];
    //                            NSString *totalStan=[defsStan valueForKey:@"initialStan"];
    //                            NSString *totalNonStan=[defsStan valueForKey:@"initialNonStan"];
    //                            double subStan=[totalStan doubleValue]+[totalNonStan doubleValue];
    //                            if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]] caseInsensitiveCompare:@"true"] == NSOrderedSame)
    //                            {
    //
    //                                float valueInitial,valueMaint;
    //                                valueInitial=0;valueMaint=0;
    //                                valueInitial=[[arrStanInitialPriceAllServices objectAtIndex:0]floatValue];
    //                                valueMaint=0;
    //                                valueMaint=[[arrStanMaintPriceAllServices objectAtIndex:0]floatValue];
    //
    //                                /*appliedDiscountInitial =(subStan*[[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]floatValue])/100;
    //                                 appliedDiscountMaint =(stanTotalMaintenance*[[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]floatValue])/100;*/
    //                                appliedDiscountInitial =(valueInitial*[[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]floatValue])/100;
    //
    //                                appliedDiscountMaint =(valueMaint*[[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]floatValue])/100;
    //
    //                                BOOL ifToSaveOrNot;
    //
    //                                ifToSaveOrNot=NO;
    //
    //
    //                                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicableForMaintenance"] ]isEqualToString:@"1"])
    //                                {
    //
    //                                    NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Credit"];
    //
    //                                    float totalDiscountAmoutAlreadyPresent=0.0;
    //
    //                                    for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
    //
    //                                        NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
    //
    //                                        NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedMaintDiscount"]];
    //
    //                                        float discountAmountPresentt=[strDiscountAmountPresent floatValue];
    //
    //                                        if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"true"])
    //                                        {
    //
    //                                            totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //
    //                                        }
    //                                        if(![[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"discountType"] ]isEqualToString:@"Credit"])
    //                                        {
    //
    //                                            //totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //
    //                                        }
    //
    //                                    }
    //
    //                                    if (!(globalAmountMaintenancePrice>=(totalDiscountAmoutAlreadyPresent+appliedDiscountMaint))) {
    //
    //
    //                                        if (globalAmountMaintenancePrice<(totalDiscountAmoutAlreadyPresent+appliedDiscountMaint)) {
    //
    //                                            float amountDiscountToApplyCredit=globalAmountMaintenancePrice-totalDiscountAmoutAlreadyPresent;
    //
    //                                            NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
    //
    //                                            [dictTempToSave addEntriesFromDictionary:dict];
    //
    //                                            [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"discountAmount"];
    //
    //                                            dict=dictTempToSave;
    //
    //                                            ifToSaveOrNot=YES;
    //
    //                                            if (amountDiscountToApplyCredit>0) {
    //
    //                                                ifToSaveOrNot=YES;
    //                                                appliedDiscountMaint=amountDiscountToApplyCredit;
    //
    //                                            }else{
    //
    //                                                ifToSaveOrNot=NO;
    //                                                appliedDiscountMaint=0.0;
    //
    //                                            }
    //                                        }
    //                                    }else{
    //
    //                                        if(appliedDiscountMaint>[[NSString stringWithFormat:@"%@",[arrStanMaintPriceAllServices objectAtIndex:0]]floatValue])//arrStanInitialPriceAllServices
    //                                        {
    //                                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[arrStanMaintPriceAllServices objectAtIndex:0]]floatValue];
    //                                        }
    //                                        //appliedDiscountInitial=0.0;
    //
    //                                        ifToSaveOrNot=YES;
    //
    //                                        //[self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Credit"];
    //
    //                                    }
    //
    //                                }
    //                                else
    //                                {
    //                                    appliedDiscountMaint=0.0;
    //                                }
    //
    //                                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicableForInitial"] ]isEqualToString:@"1"])
    //                                {
    //
    //                                    NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Credit"];
    //
    //                                    float totalDiscountAmoutAlreadyPresent=0.0;
    //
    //                                    for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
    //
    //                                        NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
    //
    //                                        NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedInitialDiscount"]];
    //
    //                                        float discountAmountPresentt=[strDiscountAmountPresent floatValue];
    //
    //                                        if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForInitial"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForInitial"] ]isEqualToString:@"true"])
    //                                        {
    //
    //                                            totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //
    //                                        }
    //                                        if(![[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"discountType"] ]isEqualToString:@"Credit"])
    //                                        {
    //
    //                                            totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //
    //                                        }
    //
    //
    //                                    }
    //
    //                                    if (!(globalAmountInitialPrice>=(totalDiscountAmoutAlreadyPresent+appliedDiscountInitial))) {
    //
    //
    //                                        if (globalAmountInitialPrice<(totalDiscountAmoutAlreadyPresent+appliedDiscountInitial)) {
    //
    //                                            float amountDiscountToApplyCredit=globalAmountInitialPrice-totalDiscountAmoutAlreadyPresent;
    //
    //                                            NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
    //
    //                                            [dictTempToSave addEntriesFromDictionary:dict];
    //
    //                                            [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"discountAmount"];
    //
    //                                            dict=dictTempToSave;
    //
    //                                            ifToSaveOrNot=YES;
    //
    //                                            if (amountDiscountToApplyCredit>0) {
    //
    //                                                ifToSaveOrNot=YES;
    //                                                appliedDiscountInitial=amountDiscountToApplyCredit;
    //
    //                                            }else{
    //
    //                                                //ifToSaveOrNot=NO;
    //                                                appliedDiscountInitial=0.0;
    //
    //                                            }
    //
    //                                        }
    //
    //                                    }else{
    //
    //                                        if(appliedDiscountInitial>[[NSString stringWithFormat:@"%@",[arrStanInitialPriceAllServices objectAtIndex:0]]floatValue])
    //                                        {
    //                                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[arrStanInitialPriceAllServices objectAtIndex:0]]floatValue];
    //                                        }
    //                                        //appliedDiscountMaint=0.0;
    //
    //                                        ifToSaveOrNot=YES;
    //
    //                                        //[self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Credit"];
    //
    //                                    }
    //
    //                                }
    //                                else
    //                                {
    //                                    appliedDiscountInitial=0.0;
    //                                }
    //
    //                                if (ifToSaveOrNot) {
    //
    //                                    [self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Credit"];
    //
    //                                }else{
    //
    //                                    [global AlertMethod:Alert :@"Credit can not be more then billing amount."];
    //
    //                                }
    //
    //
    //
    //                            }
    //                            else
    //                            {
    //                                // appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]]floatValue];
    //                                // appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]]floatValue];
    //
    //                                appliedDiscountMaint=0.0;
    //                                appliedDiscountInitial=0.0;
    //
    //                                float appDiscIntial,appDiscMaint,discount;
    //                                appDiscIntial=0;appDiscMaint=0;discount=0;
    //                                discount=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]]floatValue];
    //                                appDiscIntial=discount;
    //                                appDiscMaint=discount;
    //
    //                                BOOL ifToSaveOrNot;
    //
    //                                ifToSaveOrNot=NO;
    //
    //                                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicableForMaintenance"] ]isEqualToString:@"1"])
    //                                {
    //
    //                                    NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Credit"];
    //
    //                                    float totalDiscountAmoutAlreadyPresent=0.0;
    //
    //                                    for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
    //
    //                                        NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
    //
    //                                        NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedMaintDiscount"]];
    //
    //                                        float discountAmountPresentt=[strDiscountAmountPresent floatValue];
    //
    //                                        if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"true"])
    //                                        {
    //
    //                                            totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //
    //                                        }
    //                                        if(![[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"discountType"] ]isEqualToString:@"Credit"])
    //                                        {
    //
    //                                            //totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //
    //                                        }
    //
    //                                    }
    //
    //                                    if (!(globalAmountMaintenancePrice>=(totalDiscountAmoutAlreadyPresent+discount))) {
    //
    //                                        if (globalAmountMaintenancePrice<(totalDiscountAmoutAlreadyPresent+discount)) {
    //
    //                                            float amountDiscountToApplyCredit=globalAmountMaintenancePrice-totalDiscountAmoutAlreadyPresent;
    //
    //                                            NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
    //
    //                                            [dictTempToSave addEntriesFromDictionary:dict];
    //
    //                                            [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"discountAmount"];
    //
    //                                            dict=dictTempToSave;
    //
    //                                            ifToSaveOrNot=YES;
    //
    //                                            if (amountDiscountToApplyCredit>0) {
    //
    //                                                ifToSaveOrNot=YES;
    //                                                appliedDiscountMaint=amountDiscountToApplyCredit;
    //
    //
    //                                            }else{
    //
    //                                                //ifToSaveOrNot=NO;
    //                                                appliedDiscountMaint=0.0;
    //
    //                                            }
    //                                        }
    //
    //                                    }else{
    //
    //                                        if(discount>[[NSString stringWithFormat:@"%@",[arrStanMaintPriceAllServices objectAtIndex:0]]floatValue])//arrStanInitialPriceAllServices
    //                                        {
    //                                            appDiscMaint=[[NSString stringWithFormat:@"%@",[arrStanMaintPriceAllServices objectAtIndex:0]]floatValue];
    //                                        }
    //                                        appliedDiscountMaint=appDiscMaint;
    //                                        //appliedDiscountInitial=0.0;
    //
    //                                        ifToSaveOrNot=YES;
    //
    //                                        //[self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Credit"];
    //
    //                                    }
    //
    //                                }
    //                                else
    //                                {
    //                                    appliedDiscountMaint=0.0;
    //                                }
    //
    //                                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicableForInitial"] ]isEqualToString:@"1"])
    //                                {
    //
    //                                    NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Credit"];
    //
    //                                    float totalDiscountAmoutAlreadyPresent=0.0;
    //
    //                                    for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
    //
    //                                        NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
    //
    //                                        NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedInitialDiscount"]];
    //
    //                                        float discountAmountPresentt=[strDiscountAmountPresent floatValue];
    //
    //                                        if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForInitial"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForInitial"] ]isEqualToString:@"true"])
    //                                        {
    //
    //                                            totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //
    //                                        }
    //                                        if(![[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"discountType"] ]isEqualToString:@"Credit"])
    //                                        {
    //
    //                                            totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
    //
    //                                        }
    //
    //                                    }
    //
    //                                    if (!(globalAmountInitialPrice>=(totalDiscountAmoutAlreadyPresent+discount))) {
    //
    //
    //                                        if (globalAmountInitialPrice<(totalDiscountAmoutAlreadyPresent+discount)) {
    //
    //                                            float amountDiscountToApplyCredit=globalAmountInitialPrice-totalDiscountAmoutAlreadyPresent;
    //
    //                                            NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
    //
    //                                            [dictTempToSave addEntriesFromDictionary:dict];
    //
    //                                            [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"discountAmount"];
    //
    //                                            dict=dictTempToSave;
    //
    //                                            ifToSaveOrNot=YES;
    //
    //                                            if (amountDiscountToApplyCredit>0) {
    //
    //                                                ifToSaveOrNot=YES;
    //                                                appliedDiscountInitial=amountDiscountToApplyCredit;
    //
    //                                            }else{
    //
    //                                                //ifToSaveOrNot=NO;
    //                                                appliedDiscountInitial=0.0;
    //
    //                                            }
    //
    //                                        }
    //
    //                                    }else{
    //
    //                                        if(discount>[[NSString stringWithFormat:@"%@",[arrStanInitialPriceAllServices objectAtIndex:0]]floatValue])
    //                                        {
    //                                            appDiscIntial=[[NSString stringWithFormat:@"%@",[arrStanInitialPriceAllServices objectAtIndex:0]]floatValue];
    //                                        }
    //                                        appliedDiscountInitial=appDiscIntial;
    //                                        //appliedDiscountMaint=0.0;
    //
    //                                        ifToSaveOrNot=YES;
    //
    //                                        //[self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Credit"];
    //
    //                                    }
    //
    //                                }
    //                                else
    //                                {
    //                                    appliedDiscountInitial=0.0;
    //                                }
    //
    //                                if (ifToSaveOrNot) {
    //
    //                                    [self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Credit"];
    //
    //                                }else{
    //
    //                                    [global AlertMethod:Alert :@"Credit can not be more then billing amount."];
    //
    //                                }
    //
    //                            }
    //
    //                            //[self saveAppliedDiscountCouponAndCreditCoreData:dict:@"Credit"];
    //                            [self fetchForAppliedDiscountFromCoreData];
    //                            break;
    //
    //                        }
    //                    }
    //                    [self calculationForCouponAndCredit];
    //
    //                }
    //                [_tblNewMaintPriceCredit reloadData];
    //                [_tblNewInitialPriceCredit reloadData];
    //            }
    //        }
    //
    //    }
    
    [self finalHeightManage];
    
}


#pragma mark- ------------ Coupon Credit Methods ------------------

-(void)fetchForAppliedDiscountFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrDiscountCoupon=[[NSMutableArray alloc]init];
    arrDiscountCredit=[[NSMutableArray alloc]init];
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
            {
                [arrDiscountCoupon addObject:matchesDiscount];
                //                NSArray *uniqueArray = [[NSSet setWithArray:arrStanAllSoldServiceStandardId] allObjects];
                //                for (int j=0; j<uniqueArray.count; j++)
                //                {
                //                    NSString *strId=[uniqueArray objectAtIndex:j];
                //                    if ([strId isEqualToString:[matchesDiscount valueForKey:@"soldServiceId"]])
                //                    {
                //                        [arrDiscountCoupon addObject:matchesDiscount];
                //                    }
                //                }
            }
            else
            {
                [arrDiscountCredit addObject:matchesDiscount];
            }
        }
    }
    [self heightCreditCoupon];
    [_tblCoupon reloadData];
    [_tblCredit reloadData];
    [_tblNewInitialPriceCouponDiscount reloadData];
    if(subTotalInitialPriceCopy>0)
    {
        [_tblNewInitialPriceCredit reloadData];
    }
    if(subTotalMaintPriceCopy)
    {
        [_tblNewMaintPriceCredit reloadData];
    }
    // [self heightCreditCoupon];
    
    float totalCreditAndCouponAppliedOnIntialPrice = 0.0,totalCreditAppliedOnMaintPrice = 0.0;
    float creditDiscountOnInitialPrice = 0.0,couponDiscountOnInitialPrice=0.0,creditDiscountOnMaintPrice=0.0;
    
    for(NSDictionary *dict in arrDiscountCredit)
    {
        if(([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"1"])|| [[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
        {
            totalCreditAndCouponAppliedOnIntialPrice = totalCreditAndCouponAppliedOnIntialPrice+[[dict valueForKey:@"appliedInitialDiscount"] floatValue];
            creditDiscountOnInitialPrice = creditDiscountOnInitialPrice+[[dict valueForKey:@"appliedInitialDiscount"] floatValue];
        }
        
        if(([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"1"])|| [[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"true"])
        {
            totalCreditAppliedOnMaintPrice = totalCreditAppliedOnMaintPrice + [[dict valueForKey:@"appliedMaintDiscount"] floatValue];
            creditDiscountOnMaintPrice = creditDiscountOnMaintPrice+[[dict valueForKey:@"appliedMaintDiscount"] floatValue];
        }
    }
    
    for(NSDictionary *dict in arrDiscountCoupon)
    {
        if(([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"1"])|| [[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
        {
            totalCreditAndCouponAppliedOnIntialPrice = totalCreditAndCouponAppliedOnIntialPrice+[[dict valueForKey:@"appliedInitialDiscount"] floatValue];
            couponDiscountOnInitialPrice = couponDiscountOnInitialPrice+[[dict valueForKey:@"appliedInitialDiscount"] floatValue];
        }
    }
    
    if(subTotalInitialPriceCopy>=0)
    {
        // show total credit and coupon discount on Initial and Maint price
        _lblNewInitialPriceCouponDiscount.text = [NSString stringWithFormat:@"%.2f",couponDiscountOnInitialPrice];
        _lblNewInitialPriceCredit.text = [NSString stringWithFormat:@"%.2f",creditDiscountOnInitialPrice];
        _lblNewMaintPriceCredit.text = [NSString stringWithFormat:@"%.2f",creditDiscountOnMaintPrice];
        
        // show initial price calculations price
        
        if(totalCreditAndCouponAppliedOnIntialPrice>=subTotalInitialPriceCopy)
        {
            if(totalCreditAndCouponAppliedOnIntialPrice>subTotalInitialPriceCopy)
            {
                isCreditCouponValueGreaterThanInitialPrice = YES;
                
                _lblNewInitialPriceTotalPrice.text = @"0.00";
                subTotalInitialPrice = 0.0;
                _lblNewInitialPriceBillingAmount.text =@"0.00";
            }
            else
            {
                isCreditCouponValueGreaterThanInitialPrice = NO;
                
                _lblNewInitialPriceTotalPrice.text = @"0.00";
                subTotalInitialPrice = 0.0;
                _lblNewInitialPriceBillingAmount.text =@"0.00";
            }
            
        }
        else
        {
            _lblNewInitialPriceTotalPrice.text = [NSString stringWithFormat:@"%.2f",subTotalInitialPriceCopy-totalCreditAndCouponAppliedOnIntialPrice];
            subTotalInitialPrice = [_lblNewInitialPriceTotalPrice.text floatValue];
            _lblNewInitialPriceBillingAmount.text = [NSString stringWithFormat:@"%.2f",[_lblNewInitialPriceTotalPrice.text floatValue]];//+[_lblNewInitialPriceTaxAmount.text floatValue]]; udita mam ne bola ki commercial flow me tax calculate nhi hoga
            
            _txtPaidAmountPriceInforamtion.text = _lblNewInitialPriceBillingAmount.text;
            [_txtPaidAmountPriceInforamtion resignFirstResponder];
            
            isCreditCouponValueGreaterThanInitialPrice = NO;
        }
        
        
        
        /*_lblNewInitialPriceTaxAmount.text = [NSString stringWithFormat:@"%.2f",([_lblNewInitialPriceTotalPrice.text floatValue]*[strTax floatValue])/100.0];
         */ // udita mam ne bola ki commercial flow me tax calculate nhi hoga
        
        
    }
    
    
    if(subTotalMaintPriceCopy>=0)
    {
        // show maint price calculations price
        
        if(totalCreditAppliedOnMaintPrice>=subTotalMaintPriceCopy)
        {
            if(totalCreditAppliedOnMaintPrice>subTotalMaintPriceCopy)
            {
                isCreditValueGreaterThanMaintPrice = YES;
                
                _lblNewMaintPriceTotalPrice.text = @"0.00";
                subTotalMaintPrice = 0.0;
                _lblNewMaintPriceTotalDueAmount.text = @"0.00";
            }
            else
            {
                _lblNewMaintPriceTotalPrice.text = @"0.00";
                subTotalMaintPrice = 0.0;
                _lblNewMaintPriceTotalDueAmount.text = @"0.00";
                
                isCreditValueGreaterThanMaintPrice = NO;
            }
            
        }
        else
        {
            _lblNewMaintPriceTotalPrice.text = [NSString stringWithFormat:@"%.2f",subTotalMaintPriceCopy-totalCreditAppliedOnMaintPrice];
            
            subTotalMaintPrice = [_lblNewMaintPriceTotalPrice.text floatValue];
            
            /*_lblNewMaintPriceTaxAmount.text = [NSString stringWithFormat:@"%.2f",([_lblNewMaintPriceTotalPrice.text floatValue]*[strTax floatValue])/100.0];*/
            
            _lblNewMaintPriceTotalDueAmount.text = [NSString stringWithFormat:@"%.2f",[_lblNewMaintPriceTotalPrice.text floatValue]];//+[_lblNewMaintPriceTaxAmount.text floatValue]];
            
            isCreditValueGreaterThanMaintPrice = NO;
        }
    }
    
    _txtPaidAmountPriceInforamtion.text=_lblNewInitialPriceTotalPrice.text;
}


-(BOOL)checkForAppliedDiscountFromCoreData:(NSString*)strDiscountCode :(NSString *)strType : (NSString *)strDiscountUsageType
{
    if ([strType isEqualToString:@"Credit"]) //For Credit Discount
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
        requestNew = [[NSFetchRequest alloc] init];
        [requestNew setEntity:entityLeadCommercialDiscountExtDc];
        
        NSPredicate *predicate;
        
        if ([strDiscountUsageType caseInsensitiveCompare:@"OneTime"]==NSOrderedSame)
        {
            predicate =[NSPredicate predicateWithFormat:@"accountNo=%@ && discountSysName=%@",strAccountNoGlobal,strDiscountCode];
        }
        else if([strDiscountUsageType caseInsensitiveCompare:@"Multiple"]==NSOrderedSame)
        {
            predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && discountSysName=%@",strLeadId,strDiscountCode];//
        }
        else
        {
            predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && discountSysName=%@",strLeadId,strDiscountCode];
        }
        
        
        [requestNew setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [requestNew setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        // NSManagedObject *matchesDiscount;
        
        //One Time
        if ([strDiscountUsageType caseInsensitiveCompare:@"OneTime"]==NSOrderedSame)
        {
            if (arrAllObjSales.count==0)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        //Multiple
        if ([strDiscountUsageType caseInsensitiveCompare:@"Multiple"]==NSOrderedSame)
        {
            if (arrAllObjSales.count==0)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        else
        {
            return YES;
        }
    }
    else  //For Coupon Discount
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
        requestNew = [[NSFetchRequest alloc] init];
        [requestNew setEntity:entityLeadCommercialDiscountExtDc];
        
        NSPredicate *predicate;
        
        if ([strDiscountUsageType caseInsensitiveCompare:@"OneTime"]==NSOrderedSame)
        {
            predicate =[NSPredicate predicateWithFormat:@"accountNo=%@ && discountCode=%@",strAccountNoGlobal,strDiscountCode];
        }
        else if([strDiscountUsageType caseInsensitiveCompare:@"Multiple"]==NSOrderedSame)
        {
            predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && discountCode=%@",strLeadId,strDiscountCode];//
        }
        else
        {
            predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && discountCode=%@",strLeadId,strDiscountCode];
        }
        
        [requestNew setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [requestNew setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        // NSManagedObject *matchesDiscount;
        
        //One Time
        if ([strDiscountUsageType caseInsensitiveCompare:@"OneTime"]==NSOrderedSame)
        {
            if (arrAllObjSales.count==0)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        //Multiple
        if ([strDiscountUsageType caseInsensitiveCompare:@"Multiple"]==NSOrderedSame)
        {
            if (arrAllObjSales.count==0)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        else
        {
            return YES;
        }
    }
}
//For Credit
-(BOOL)checkForAppliedDiscountFromCoreDataTemp:(NSString*)strDiscountCode :(NSString *)strType
{
    if ([strType isEqualToString:@"Credit"]) //For Credit Discount
    {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
        requestNew = [[NSFetchRequest alloc] init];
        [requestNew setEntity:entityLeadAppliedDiscounts];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
        
        [requestNew setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [requestNew setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        NSManagedObject *matchesDiscount;
        if (arrAllObjSales.count==0)
        {
            return NO;
        }
        else
        {
            BOOL chkDiscount=NO;
            for (int k=0; k<arrAllObjSales.count; k++)
            {
                matchesDiscount=arrAllObjSales[k];
                
                if ([strDiscountCode isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"discountSysName"]]])
                {
                    chkDiscount=YES;
                    break;
                }
                
                
                NSLog(@"Lead IDDDD====%@",[matchesDiscount valueForKey:@"leadId"]);
            }
            if (chkDiscount==YES)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
    }
    else  //For Coupon Discount
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
        requestNew = [[NSFetchRequest alloc] init];
        [requestNew setEntity:entityLeadAppliedDiscounts];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
        
        [requestNew setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [requestNew setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        NSManagedObject *matchesDiscount;
        if (arrAllObjSales.count==0)
        {
            return NO;
        }
        else
        {
            BOOL chkDiscount=NO;
            for (int k=0; k<arrAllObjSales.count; k++)
            {
                matchesDiscount=arrAllObjSales[k];
                
                if ([strDiscountCode isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"discountCode"]]])
                {
                    chkDiscount=YES;
                    break;
                }
                
                NSLog(@"Lead IDDDD====%@",[matchesDiscount valueForKey:@"leadId"]);
            }
            if (chkDiscount==YES)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
    }
}

-(void)saveAppliedDiscountCouponAndCreditCoreData:(NSDictionary *)dictForCoupon : (NSString *)strDiscountType
{
    if ((appliedDiscountMaint>0) || (appliedDiscountInitial>0)) {
        
        if ([strDiscountType isEqualToString:@"Credit"])
        {
            entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
            
            LeadCommercialDiscountExtDc *objLeadAppliedDiscounts = [[LeadCommercialDiscountExtDc alloc]initWithEntity:entityLeadCommercialDiscountExtDc insertIntoManagedObjectContext:context];
            
            objLeadAppliedDiscounts.discountSysName=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"SysName"]];
            objLeadAppliedDiscounts.discountType=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Type"]];
            
            objLeadAppliedDiscounts.discountDescription=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Description"]];
            
            if ([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"])
            {
                objLeadAppliedDiscounts.isDiscountPercent=@"true";
                objLeadAppliedDiscounts.discountPercent=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountPercent"]];
            }
            else
            {
                objLeadAppliedDiscounts.isDiscountPercent=@"false";
                objLeadAppliedDiscounts.discountPercent=@"0";
            }
            
            objLeadAppliedDiscounts.discountCode = [NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountCode"]];
            objLeadAppliedDiscounts.leadId=strLeadId;
            objLeadAppliedDiscounts.userName=strUserName;
            objLeadAppliedDiscounts.companyKey=strCompanyKey;
            objLeadAppliedDiscounts.appliedMaintDiscount=[NSString stringWithFormat:@"%.2f",appliedDiscountMaint];
            objLeadAppliedDiscounts.appliedInitialDiscount=[NSString stringWithFormat:@"%.2f",appliedDiscountInitial];
            objLeadAppliedDiscounts.accountNo=strAccountNoGlobal;
            
            NSString *strApplicabelForInitial,*strApplicabelFroMaint;
            
            if([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"ApplicableForInitial"]]isEqualToString:@"1"])
            {
                strApplicabelForInitial=@"true";
            }
            else
            {
                strApplicabelForInitial=@"false";
            }
            if([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"ApplicableForMaintenance"] ]isEqualToString:@"1"])
            {
                strApplicabelFroMaint=@"true";
            }
            else
            {
                strApplicabelFroMaint=@"false";
            }
            
            objLeadAppliedDiscounts.applicableForInitial=strApplicabelForInitial;
            objLeadAppliedDiscounts.applicableForMaintenance=strApplicabelFroMaint;
            objLeadAppliedDiscounts.name = [NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Name"]];
            
            NSError *error1;
            [context save:&error1];
        }
        else
        {
            entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
            
            LeadCommercialDiscountExtDc *objLeadAppliedDiscounts = [[LeadCommercialDiscountExtDc alloc]initWithEntity:entityLeadCommercialDiscountExtDc insertIntoManagedObjectContext:context];
            
            objLeadAppliedDiscounts.discountSysName=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"SysName"]];
            objLeadAppliedDiscounts.discountType=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Type"]];
            objLeadAppliedDiscounts.discountDescription=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Description"]];
            
            if ([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"])
            {
                objLeadAppliedDiscounts.isDiscountPercent=@"true";
                objLeadAppliedDiscounts.discountPercent=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountPercent"]];
            }
            else
            {
                objLeadAppliedDiscounts.isDiscountPercent=@"false";
                objLeadAppliedDiscounts.discountPercent=@"0";
            }
            
            objLeadAppliedDiscounts.discountCode = [NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountCode"]];
            objLeadAppliedDiscounts.leadId=strLeadId;
            objLeadAppliedDiscounts.userName=strUserName;
            objLeadAppliedDiscounts.companyKey=strCompanyKey;
            objLeadAppliedDiscounts.appliedMaintDiscount=[NSString stringWithFormat:@"%.2f",appliedDiscountMaint];
            objLeadAppliedDiscounts.appliedInitialDiscount=[NSString stringWithFormat:@"%.2f",appliedDiscountInitial];
            objLeadAppliedDiscounts.accountNo=strAccountNoGlobal;
            
            NSString *strApplicabelForInitial,*strApplicabelFroMaint;
            
            if([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"ApplicableForInitial"]]isEqualToString:@"1"])
            {
                strApplicabelForInitial=@"true";
            }
            else
            {
                strApplicabelForInitial=@"false";
            }
            if([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"ApplicableForMaintenance"] ]isEqualToString:@"1"])
            {
                strApplicabelFroMaint=@"true";
            }
            else
            {
                strApplicabelFroMaint=@"false";
            }
            
            strApplicabelForInitial=@"true";
            strApplicabelFroMaint=@"true";
            objLeadAppliedDiscounts.applicableForInitial=strApplicabelForInitial;
            objLeadAppliedDiscounts.applicableForMaintenance=strApplicabelFroMaint;
            
            NSError *error1;
            [context save:&error1];
        }
    }else{
        
        NSString *strAlertMsg=[NSString stringWithFormat:@"%@ can not be more than billing amount.",strDiscountType];
        
        [global AlertMethod:Alert :strAlertMsg];
        
    }
    
    [self fetchForAppliedDiscountFromCoreData];
}


-(void)deleteAppliedCouponFromCoreDataSalesInfo:(NSManagedObject *)dictForCoupon :(NSString *)strDiscountType
{
    
    if ([strDiscountType isEqualToString:@"Credit"])
    {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        //  Delete Lead Detail Data
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
        NSFetchRequest *allData = [[NSFetchRequest alloc] init];
        [allData setPredicate:predicate];
        [allData setEntity:[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context]];
        [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error = nil;
        NSArray * Data = [context executeFetchRequest:allData error:&error];
        //error handling goes here
        
        NSString *strDiscountSysName=[dictForCoupon valueForKey:@"discountSysName"];
        
        for (NSManagedObject * data in Data)
        {
            if([strDiscountSysName isEqualToString:[NSString stringWithFormat:@"%@",[data valueForKey:@"discountSysName"]]])
            {
                [context deleteObject:data];
            }
        }
        NSError *saveError = nil;
        [context save:&saveError];
        [self fetchForAppliedDiscountFromCoreData];
        [_tblCredit reloadData];
        // [self updateDiscountAfterDelete:strService DiscountAmount:strDiscountAmount DiscountPercent:strDiscountPer];
    }
    else  //Coupon
    {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        //  Delete Lead Detail Data
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
        NSFetchRequest *allData = [[NSFetchRequest alloc] init];
        [allData setPredicate:predicate];
        [allData setEntity:[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context]];
        [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error = nil;
        NSArray * Data = [context executeFetchRequest:allData error:&error];
        //error handling goes here
        
        NSString *strDiscountCode=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountCode"]];
        
        for (NSManagedObject * data in Data)
        {
            NSString *strCouponCodeDataBase;
            
            strCouponCodeDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"discountCode"]];
            
            if([strCouponCodeDataBase isEqualToString:strDiscountCode])
            {
                [context deleteObject:data];
                break;
            }
        }
        
        NSError *saveError = nil;
        [context save:&saveError];
        [self fetchForAppliedDiscountFromCoreData];
        [_tblCoupon reloadData];
        // [self updateDiscountAfterDelete:strService DiscountAmount:strDiscountAmount DiscountPercent:strDiscountPer CheckForDiscountPer:strChkDiscountPer];
    }
    // [self calculationForCouponAndCredit];
    
}
-(void)updateDiscountAfterDelete:(NSString *)strServiceNameDiscount DiscountAmount: (NSString *)strDiscountAmount DiscountPercent:(NSString *)StrDiscPer CheckForDiscountPer:(NSString*)strChkForDiscountPer
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND bundleId=%@",strLeadId,@"0"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjDiscount = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObjDiscount.count==0)
    {
        
    }else
    {
        bool chkDiscountApplied;
        chkDiscountApplied=NO;
        BOOL chkForBundle=NO;
        
        float totalInitialPriceFinalToSave=0.0,totalMaintenancePriceFinalToSave=0.0;
        
        for (int i=0; i<arrAllObjDiscount.count; i++)
        {
            matchesDiscoutUpdate=arrAllObjDiscount[i];
            
            NSString *strInitialPriceFromDB=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]];
            
            NSString *strMaintenancePricePriceFromDB=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]];
            
            float initialPriceTemp=[strInitialPriceFromDB floatValue];
            
            float maintenancePriceTemp=[strMaintenancePricePriceFromDB floatValue];
            
            totalInitialPriceFinalToSave=totalInitialPriceFinalToSave+initialPriceTemp;
            totalMaintenancePriceFinalToSave=totalMaintenancePriceFinalToSave+maintenancePriceTemp;
            
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                chkForBundle=NO;
            }
            else
            {
                chkForBundle=YES;
            }
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        // discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    
                }
                
            }
            else  //For Bundle
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        // discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    
                }
                
                chkForBundle=YES;
                break;
                
            }
        }
        if(chkDiscountApplied==NO)
        {
            if (arrAllObjDiscount.count>0)
            {
                matchesDiscoutUpdate=arrAllObjDiscount[0];
                
                if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                {
                    chkForBundle=NO;
                }
                else
                {
                    chkForBundle=YES;
                }
                
                if (chkForBundle==NO)
                {
                    matchesDiscoutUpdate=arrAllObjDiscount[0];
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        // strDiscountAmount=@"50";
                        //discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        //  discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        /* if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                         {
                         discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                         }
                         
                         discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                         */
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        // strDiscountAmount=@"50";
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        //  discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        /* if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                         {
                         discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                         }
                         
                         discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                         */
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                    }
                    
                }
                // BUNDLE
                else
                {
                    for (int i=0; i<arrAllObjDiscount.count; i++)
                    {
                        matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                        
                        if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                        {
                        }
                        else
                        {
                            matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                            if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                            {
                                float discountPerCoupon=0, discount=0;
                                
                                discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                                
                                discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                                // strDiscountAmount=@"50";
                                //discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                                //  discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                                
                                /* if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                 {
                                 discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                 }
                                 
                                 discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                 */
                                discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                            }
                            else
                            {
                                float discountPerCoupon=0, discount=0;
                                // strDiscountAmount=@"50";
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                                if(discount<0)
                                {
                                    discount=0;
                                }
                                //  discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                                
                                /* if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                 {
                                 discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                 }
                                 
                                 discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                 */
                                discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                if(discountPerCoupon<0)
                                {
                                    discountPerCoupon=0;
                                }
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                                [matchesDiscoutUpdate  setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                                
                            }
                        }
                    }
                }
            }
        }
        
        //[matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%f",totalInitialPriceFinalToSave] forKey:@"totalInitialPrice"];
        // [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%f",totalMaintenancePriceFinalToSave] forKey:@"totalMaintPrice"];
        
        [context save:&error1];
    }
    [self fetchFromCoreDataStandard];
    [_tblMaintenance reloadData];
    _txtEnterCouponNo.text=@"";
}

//-(void)finalUpdatedTableDataWithBundleDiscountNew:(NSString *)strServiceNameDiscount :(NSString *)strDiscountAmount
-(void)finalUpdatedTableDataWithBundleDiscountNew:(NSString *)strServiceNameDiscount :(NSString *)strDiscountAmount CheckForDiscountPer:(NSString*)strDisountCheck DiscountPerValue:(NSString *)strDiscountPercent

{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND bundleId=%@",strLeadId,@"0"];
    NSPredicate *predicate;// =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjDiscount = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObjDiscount.count==0)
    {
        
    }
    else
    {
        bool chkDiscountApplied,chkForServiceBasedCoupon,chkForBundle=NO;
        chkDiscountApplied=NO;chkForServiceBasedCoupon=NO;chkForSaveCoupon=NO;
        for (int i=0; i<arrAllObjDiscount.count; i++)
        {
            matchesDiscoutUpdate=arrAllObjDiscount[i];
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                chkForBundle=NO;
                
            }
            else
            {
                chkForBundle=YES;
            }
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                //strServiceNameDiscount=@"SignaturePestControl";
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    chkForServiceBasedCoupon=YES;
                    if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        if (discountPerCoupon==100|| discountPerCoupon>100)
                        {
                            discountPerCoupon=100.00;
                            
                        }
                        
                        //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        //Calulcate Applied Discount Amount For Initial And Maint
                        
                        
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                        appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                        appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                        
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountInitial=lroundf(appliedDiscountInitial);
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=lroundf(appliedDiscountMaint);*/
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountAmount floatValue])/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountAmount floatValue])/100;*/
                        // appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        //  appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        //14 June Temp
                        float appDiscIntial,appDiscMaint;
                        appDiscIntial=0;appDiscMaint=0;
                        appDiscIntial=discount;
                        appDiscMaint=discount;
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            appDiscIntial=discount;
                            
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                            appDiscMaint=discount;
                            
                        }
                        
                        appliedDiscountInitial=appDiscIntial;
                        appliedDiscountMaint=appDiscMaint;
                        
                        //Nilind 29 June
                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        }
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                        {
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                        }
                        //End
                        
                        
                    }
                    
                    chkForSaveCoupon=YES;
                }
                
            }
            else  //For Bundle
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    chkForServiceBasedCoupon=YES;
                    if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        if (discountPerCoupon==100|| discountPerCoupon>100)
                        {
                            discountPerCoupon=100.00;
                            
                        }
                        
                        //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                        
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        //Calulcate Applied Discount Amount For Initial And Maint
                        
                        
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                        
                        appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                        appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        /* appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountAmount floatValue])/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountAmount floatValue])/100;*/
                        // appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        // appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        //14 June Temp
                        float appDiscIntial,appDiscMaint;
                        appDiscIntial=0;appDiscMaint=0;
                        appDiscIntial=discount;
                        appDiscMaint=discount;
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            appDiscIntial=discount;
                            
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                            appDiscMaint=discount;
                            
                        }
                        
                        appliedDiscountInitial=appDiscIntial;
                        appliedDiscountMaint=appDiscMaint;
                        
                        //Nilind 29 June
                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                        {
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                        }
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                        {
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                        }
                        //End
                        
                        
                    }
                    
                    chkForSaveCoupon=YES;
                    chkForBundle=YES;
                }
            }
        }
        if(chkDiscountApplied==NO)//&& chkForServiceBasedCoupon==NO
        {
            if(strServiceNameDiscount.length==0)//Non ServiceBased
            {
                if (arrAllObjDiscount.count>0)
                {
                    matchesDiscoutUpdate=arrAllObjDiscount[0];
                    
                    if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                    {
                        chkForBundle=NO;
                    }
                    else
                    {
                        chkForBundle=YES;
                    }
                    
                    
                    if (chkForBundle==NO)
                    {
                        matchesDiscoutUpdate=arrAllObjDiscount[0];
                        
                        if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                        {
                            float discountPerCoupon=0, discount=0;
                            
                            discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                            if (discountPerCoupon==100|| discountPerCoupon>100)
                            {
                                discountPerCoupon=100.00;
                                
                            }
                            
                            //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                            
                            discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                            
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                //13 June Temp
                                // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                            
                            chkDiscountApplied=YES;
                            strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                            
                            /* appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                             appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                            appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                            appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                            
                            
                        }
                        else
                        {
                            float discountPerCoupon=0, discount=0;
                            //strDiscountAmount=@"50";
                            discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                //13 June Temp
                                //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            
                            
                            discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                            
                            discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                            
                            strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                            
                            /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                             appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                            /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountAmount floatValue])/100;
                             appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountAmount floatValue])/100;*/
                            
                            
                            //  appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            // appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            
                            //14 June Temp
                            float appDiscIntial,appDiscMaint;
                            appDiscIntial=0;appDiscMaint=0;
                            appDiscIntial=discount;
                            appDiscMaint=discount;
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                appDiscIntial=discount;
                                
                                
                                //13 June Temp
                                //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                                appDiscMaint=discount;
                                
                            }
                            
                            /*appliedDiscountInitial=appDiscIntial;
                             appliedDiscountMaint=appDiscMaint;*/
                            
                            
                            //Nilind 29 June
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            
                            if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                            {
                                appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                            }
                            if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                            {
                                appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                            }
                            //End
                            
                        }
                    }
                    else
                    {
                        for (int i=0; i<arrAllObjDiscount.count; i++)
                        {
                            matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                            
                            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                            {
                            }
                            else
                            {
                                matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                                if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                                {
                                    float discountPerCoupon=0, discount=0;
                                    
                                    discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                    if (discountPerCoupon==100|| discountPerCoupon>100)
                                    {
                                        discountPerCoupon=100.00;
                                        
                                    }
                                    
                                    //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                                    
                                    discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                                    
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                    }
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                                    
                                    chkDiscountApplied=YES;
                                    strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                                    
                                    /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                                     appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                                    appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                                    appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountPercent floatValue])/100;
                                    
                                    
                                }
                                else
                                {
                                    float discountPerCoupon=0, discount=0;
                                    //strDiscountAmount=@"50";
                                    discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                    }
                                    
                                    
                                    discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                    
                                    discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                    
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPerCoupon] forKey:@"discountPercentage"];
                                    strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                                    
                                    /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                                     appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                                    /* appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*[strDiscountAmount floatValue])/100;
                                     appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*[strDiscountAmount floatValue])/100;*/
                                    // appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    //appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    //14 June Temp
                                    float appDiscIntial,appDiscMaint;
                                    appDiscIntial=0;appDiscMaint=0;
                                    appDiscIntial=discount;
                                    appDiscMaint=discount;
                                    
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                        appDiscIntial=discount;
                                        
                                        //13 June Temp
                                        //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                                    }
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                                        appDiscMaint=discount;
                                        
                                    }
                                    
                                    /*appliedDiscountInitial=appDiscIntial;
                                     appliedDiscountMaint=appDiscMaint;*/
                                    
                                    
                                    
                                    //Nilind 29 June
                                    appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    
                                    if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue])
                                    {
                                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue];
                                    }
                                    if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue])
                                    {
                                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue];
                                    }
                                    //End
                                    
                                    
                                    
                                }
                                
                                break;
                            }
                            
                        }
                    }
                    
                    chkForSaveCoupon=YES;
                    
                }
            }
        }
        [context save:&error1];
    }
    [self fetchFromCoreDataStandard];
    [_tblMaintenance reloadData];
    _txtEnterCouponNo.text=@"";
}


-(void)setTableFrame:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 4:
        {
            [self setTableFrame:i];
            break;
        }
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 11:
        {
            [self setTableFrame:i];
            break;
        }
        case 12:
        {
            [self setTableFrame:i];
            break;
        }
        case 13:
        {
            [self setTableFrame:i];
            break;
        }
        case 14:
        {
            [self setTableFrame:i];
            break;
        }
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
    
    [tblData scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
}
-(void)calculationForCouponAndCredit
{
    [self fetchForAppliedDiscountFromCoreData];
    // For Sandard Agreement
    
    stantotalInitial=0;stanTotalMaintenance=0;nonStanInitial=0;
    for (int i=0; i<arrStanInitialPriceAllServices.count; i++)//arrStanInitialPrice
    {
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]];
        if ([str isEqualToString:@"0"])
        {
            stantotalInitial=stantotalInitial+[[arrStanInitialPriceAllServices objectAtIndex:i] doubleValue];
            stanTotalMaintenance=stanTotalMaintenance+[[arrStanMaintPriceAllServices objectAtIndex:i]doubleValue];
            
        }
        else
        {
            stantotalInitial=stantotalInitial+[[arrStanFinalInitialPriceAllServices objectAtIndex:i] doubleValue];
            stanTotalMaintenance=stanTotalMaintenance+[[arrStanFinalMaintPriceAllServices objectAtIndex:i]doubleValue];
        }
        
    }
    
    // Parameter Price Service Calculation
    for (int i=0; i<arrAddParamInital.count; i++)
    {
        stantotalInitial=stantotalInitial+[[arrAddParamInital objectAtIndex:i]floatValue];
        stanTotalMaintenance=stanTotalMaintenance+[[arrAddParaMaint objectAtIndex:i]doubleValue];
    }
    //End
    _lblNewInitialPriceSubtotalAmount.text=[NSString stringWithFormat:@"%.2f",stantotalInitial];
    _lblNewMaintPriceSubtotalAmount.text=[NSString stringWithFormat:@"%.2f",stanTotalMaintenance];
    
    //.......................................................
    
    // For Sandard Agreement
    
    for (int i=0; i<arrNonStanInitialPrice.count; i++)
    {
        nonStanInitial=nonStanInitial+[[arrNonStanInitialPrice objectAtIndex:i] doubleValue];
        
    }
    _lblSubtotalAmountValueNonStandardService.text=[NSString stringWithFormat:@"%.2f",nonStanInitial];
    
    //.......................................................
    
    NSUserDefaults *defsStan=[NSUserDefaults standardUserDefaults];
    NSString *totalStan=[defsStan valueForKey:@"initialStan"];
    NSString *totalDiscount=[defsStan valueForKey:@"discountStan"];
    NSString *totalNonStan=[defsStan valueForKey:@"initialNonStan"];
    NSString *totalNonDiscount=[defsStan valueForKey:@"discountNonStan"];
    double subStan=[totalStan doubleValue]+[totalNonStan doubleValue];
    double subDiscount=[totalDiscount doubleValue]+[totalNonDiscount doubleValue];
    
    //Temp
    subDiscount=0;
    for (int i=0; i<arrStanDiscountAllServices.count;i++)
    {
        subDiscount=subDiscount+[[arrStanDiscountAllServices objectAtIndex:i]floatValue];
    }
    for (int i=0;i< arrNonStanDiscountPrice.count; i++)
    {
        subDiscount=subDiscount+[[arrNonStanDiscountPrice objectAtIndex:i]floatValue];
    }
    //End
    
    
    //Change
    /*subStan=0;
     for (int i=0; i<arrStanDiscountAllServices.count; i++)
     {
     subStan=subStan+[[arrStanDiscountAllServices objectAtIndex:i]floatValue];
     }
     */
    //End
    
    
    _lblSubtotalAmountValuePriceInformation.text=[NSString stringWithFormat:@"%.2f",subStan];
    _lblCouponDiscountValuePriceInformation.text=[NSString stringWithFormat:@"%.2f",subDiscount];
    
    _lblTotalPriceValuePriceInformation.text=[NSString stringWithFormat:@"%.2f",(subStan-subDiscount)];
    
#pragma mark- TAX CALCULATION NEED TO DONE
    /*double taxCAL;
     if(strTax.length==0|| [strTax isEqualToString:@""])
     {
     strTax=@"0.0";
     }
     taxCAL=((subStan-subDiscount)*[strTax doubleValue])/100;
     */
    
    double calculateTax,taxableAmount;
    calculateTax=0;
    NSString* val1=[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"caluclateTax"]];
    NSString* val2=[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"taxNonStandard"]];
    calculateTax=[val1 doubleValue]+[val2 doubleValue];
    
    taxableAmount=[[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"taxabaleAmountNonStan"]] doubleValue]+[[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"taxableAmountStan"]] doubleValue];
    
    // _lblTaxAmountValuePriceInformation.text=[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"caluclateTax"]];
    _lblTaxAmountValuePriceInformation.text=[NSString stringWithFormat:@"%.2f",calculateTax];
    
    _lblBillingAmountPriceInformation.text=[NSString stringWithFormat:@"%.2f",[_lblTotalPriceValuePriceInformation.text doubleValue]+[_lblTaxAmountValuePriceInformation.text doubleValue]];
    
    _txtPaidAmountPriceInforamtion.text= _lblBillingAmountPriceInformation.text;
    
    strFinalTaxabelAmount=[NSString stringWithFormat:@"%.2f",taxableAmount];
    strFinalTaxAmount=[NSString stringWithFormat:@"%.2f",calculateTax];
    strFinalTotalPrice=[NSString stringWithFormat:@"%@",_lblTotalPriceValuePriceInformation.text ];
    
    
    //Nilind 06 Oct
    //For Discount Total
    double totalDiscountStanService;
    totalDiscountStanService=0;
    for (int i=0; i<arrStanDiscountAllServices.count; i++)//arrStanDiscountPrice
    {
        totalDiscountStanService=totalDiscountStanService+[[arrStanDiscountAllServices objectAtIndex:i] doubleValue];
    }
    _lblDiscountPriceStandard.text=[NSString stringWithFormat:@"%.2f",totalDiscountStanService];
    
    
    
    //Coupon Calculation
    //Initial Price
    
    _lblNewInitialPriceSubtotalAmount.text=[NSString stringWithFormat:@"%.2f",subStan];
    
    float totalCouponDiscount=0;
    for (int i=0; i<arrDiscountCoupon.count; i++)
    {
        NSDictionary *dict=[arrDiscountCoupon objectAtIndex:i];
        totalCouponDiscount=totalCouponDiscount+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue];
    }
    _lblSubtotalCoupon.text=[NSString stringWithFormat:@"Subtotal $: %.2f",totalCouponDiscount];
    _lblSubtotalCoupon.font=[UIFont systemFontOfSize:20];
    _lblNewInitialPriceCouponDiscount.text=[NSString stringWithFormat:@"%.2f",totalCouponDiscount];
    
    float totalCreditDiscount=0;
    for (int i=0; i<arrDiscountCredit.count; i++)
    {
        NSDictionary *dict=[arrDiscountCredit objectAtIndex:i];
        totalCreditDiscount=totalCreditDiscount+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue];
    }
    _lblSubtotalCredit.text=[NSString stringWithFormat:@"Subtotal $: %.2f",totalCreditDiscount];
    
    _lblSubtotalCredit.font=[UIFont systemFontOfSize:20];
    
    float totalCreditDiscountInitial=0;
    
    
    
    NSMutableArray* arrDiscountCreditInitialTemp=[[NSMutableArray alloc]init];
    for(int i=0; i<arrDiscountCredit.count;i++)
    {
        NSDictionary *dict=[arrDiscountCredit objectAtIndex:i];
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
        {
            [arrDiscountCreditInitialTemp addObject:dict];
        }
        
    }
    
    
    for (int i=0; i<arrDiscountCreditInitialTemp.count; i++)
    {
        NSDictionary *dict=[arrDiscountCreditInitialTemp objectAtIndex:i];
        totalCreditDiscountInitial=totalCreditDiscountInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue];
    }
    
    
    _lblNewInitialPriceCredit.text=[NSString stringWithFormat:@"%.2f",totalCreditDiscountInitial];
    
    float otherDiscount=0;
    if(subDiscount>totalCouponDiscount)
    {
        otherDiscount=subDiscount-(totalCouponDiscount);
        
    }
    else
    {
        otherDiscount=totalCouponDiscount-(subDiscount);
        
    }
    if(subDiscount==0)
    {
        otherDiscount=subDiscount-(totalCouponDiscount);
        
    }
    if(otherDiscount<0||otherDiscount<0)
    {
        otherDiscount=0.0;
    }
    _lblNewInitialPriceOtherDiscount.text=[NSString stringWithFormat:@"%.2f",otherDiscount];
    
    
    float totalPrice=0;
    totalPrice=subStan-otherDiscount-(totalCouponDiscount+totalCreditDiscountInitial);
    if (totalPrice<0)
    {
        totalPrice=0.0;
    }
    
    _lblNewInitialPriceTotalPrice.text=[NSString stringWithFormat:@"%.2f",totalPrice];
    
    //New Change 07 June
    strFinalTotalPrice=[NSString stringWithFormat:@"%.2f",totalPrice];
    //End
    
    
    float calculateTaxDiscount=0;
    calculateTaxDiscount=[self taxCalculation];
    
    //calculateTaxDiscount=(totalPrice*[strTax floatValue])/100;
    
    //New Change 07 June
    taxableAmount=[[NSString stringWithFormat:@"%@",[defsStan valueForKey:@"taxabaleAmountNonStan"]] floatValue];
    
    taxableAmount=taxableAmount+calculateTaxDiscount;
    
    strFinalTaxabelAmount=[NSString stringWithFormat:@"%.2f",taxableAmount];
    
    float calculateTaxDiscountNew=0;
    
    calculateTaxDiscountNew=calculateTaxDiscountNew+taxableAmount;
    //End
    
    calculateTaxDiscountNew=(calculateTaxDiscountNew*[strTax floatValue])/100;
    if(calculateTaxDiscountNew<0)
    {
        calculateTaxDiscountNew=0.0;
    }
    _lblNewInitialPriceTaxAmount.text=[NSString stringWithFormat:@"%.2f",calculateTaxDiscountNew];
    
    //New 07 June
    strFinalTaxAmount=[NSString stringWithFormat:@"%.2f",calculateTaxDiscountNew];
    //End
    
    
    float totalBillingPrice=0;
    //  totalBillingPrice=totalPrice+calculateTaxDiscount;
    totalBillingPrice=totalPrice+calculateTaxDiscountNew;
    
    if (totalBillingPrice<0)
    {
        totalBillingPrice=0.0;
    }
    
    _lblNewInitialPriceBillingAmount.text=[NSString stringWithFormat:@"%.2f",totalBillingPrice];
    
    //Maint Price
    
    _lblNewMaintPriceSubtotalAmount.text=[NSString stringWithFormat:@"%.2f",stanTotalMaintenance];
    
    
    
    NSMutableArray* arrDiscountCreditMaintTemp=[[NSMutableArray alloc]init];
    for(int i=0; i<arrDiscountCredit.count;i++)
    {
        NSDictionary *dict=[arrDiscountCredit objectAtIndex:i];
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"true"])
        {
            [arrDiscountCreditMaintTemp addObject:dict];
        }
        
    }
    
    
    float totalCreditDiscountMaint=0;
    for (int i=0; i<arrDiscountCreditMaintTemp.count; i++)
    {
        NSDictionary *dict=[arrDiscountCreditMaintTemp objectAtIndex:i];
        totalCreditDiscountMaint=totalCreditDiscountMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedMaintDiscount"]]floatValue];
    }
    
    _lblNewMaintPriceCredit.text=[NSString stringWithFormat:@"%.2f",totalCreditDiscountMaint];
    float totalMaitnPrice=0;
    NSLog(@"%@",arrStanInitialPrice);
    
    totalMaitnPrice=stanTotalMaintenance-totalCreditDiscountMaint;
    if (totalMaitnPrice<0)
    {
        totalMaitnPrice=0.0;
    }
    
    _lblNewMaintPriceTotalPrice.text=[NSString stringWithFormat:@"%.2f",totalMaitnPrice];
    //Maintenance Tax calculation
    
    float taxCalculationForMaint=0;
    float calculateTaxDiscountMaint=0;
    
    taxCalculationForMaint=[self taxCalculationForMaint];
    
    
    
    calculateTaxDiscountMaint=(taxCalculationForMaint*[strTax floatValue])/100;
    
    //Nilind 07 June
    strTaxableAmountMaint=[NSString stringWithFormat:@"%.2f",taxCalculationForMaint];
    strTaxAmountMaint=[NSString stringWithFormat:@"%.2f",calculateTaxDiscountMaint];
    //ENd
    
    
    //calculateTaxDiscountMaint=(totalMaitnPrice*[strTax floatValue])/100;
    
    _lblNewMaintPriceTaxAmount.text=[NSString stringWithFormat:@"%.2f",calculateTaxDiscountMaint];
    float totalDueAmountAmount=0;
    totalDueAmountAmount=totalMaitnPrice+calculateTaxDiscountMaint;
    
    if (totalDueAmountAmount<0)
    {
        totalDueAmountAmount=0.0;
    }
    _lblNewMaintPriceTotalDueAmount.text=[NSString stringWithFormat:@"%.2f",totalDueAmountAmount];
    
    _txtPaidAmountPriceInforamtion.text=[NSString stringWithFormat:@"%.2f",totalBillingPrice];
}
- (IBAction)actionOnElectroinicAuthorizedForm:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSDictionary *dictHTMLData = [dictMasters valueForKey:@"ElectronicAuthorizationFormMaster"];
    if([dictHTMLData isKindOfClass:[NSDictionary class]])
    {
        
        _buttonElectronicAuthorizedForm.enabled = NO;
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            _buttonElectronicAuthorizedForm.enabled = YES;
        });
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ElectronicAuthorization_iPhone" bundle:nil];
        ElectronicAuthorization_iPhone *vc = [storyboard instantiateViewControllerWithIdentifier:@"ElectronicAuthorization_iPhone"];
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            
        {
            Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
            NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
            
            if (netStatusWify1== NotReachable)
            {
                [global AlertMethod:Alert :@"Internet connection is reqiured to fill electronic authorization form in case of complete lead."];
            }
            else
            {
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        else
        {
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else
    {
        [global AlertMethod:Alert :NoDataAvailable];
        
    }
    
    
    
}
-(BOOL)methodToCheckIfElectronicFormExistsForLeadId:(NSString*)strleadid
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityElectronicAuthorizedForm=[NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityElectronicAuthorizedForm];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strleadid];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if(arrAllObj12.count>0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(BOOL)discountValidityCheck:(NSDictionary*)dict
{
    //Discount Validity Check
    BOOL chkValidity;
    chkValidity=NO;
    NSString *strCurrentDate,*strValidFrom,*strValidTo;
    NSDate *dateCurrent,*dateValidFrom,*dateValidTo;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    
    strCurrentDate = [formatter stringFromDate:[NSDate date]];
    
    strValidFrom=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ValidFrom"]];
    dateCurrent=[formatter dateFromString:strCurrentDate];
    
    
    
    strValidFrom=[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:strValidFrom];
    dateValidFrom=[formatter dateFromString:strValidFrom];
    
    strValidTo=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ValidTo"]];
    
    strValidTo=[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:strValidTo];
    dateValidTo=[formatter dateFromString:strValidTo];
    
    
    
    NSComparisonResult result = [dateValidFrom compare:dateCurrent];
    
    if(result==NSOrderedAscending || result== NSOrderedSame )
    {
        NSComparisonResult result2 = [dateValidTo compare:dateCurrent];
        if (result2==NSOrderedDescending|| result2==NSOrderedSame)
        {
            NSLog(@"valid B");
            chkValidity=YES;
        }
    }
    //End
    return chkValidity;
    
}

#pragma mark- 31 may

-(float)taxCalculation
{
    
    //For Coupon
    
    float taxCalculationCoupon,taxCalculationCredit,taxCalculationTotal;
    taxCalculationCoupon=0;taxCalculationCredit=0;taxCalculationTotal=0;
    [self fetchForAppliedDiscountFromCoreDataForTaxCalculation];
    /*if (arrForTaxCoupon.count>0)
     {
     taxCalculationCoupon= [self fetchFromCoreDataStandardForTax:arrForTaxCoupon:@"coupon"];
     
     }*/
    //if (arrForTaxCredit.count>0)
    //{
    taxCalculationCredit= [self fetchFromCoreDataStandardForTax:arrForTaxCredit:@"credit"];
    if(taxCalculationCredit<0)
    {
        taxCalculationCredit=0;
    }
    
    // }
    //For Credit
    
    taxCalculationTotal=taxCalculationCoupon+taxCalculationCredit;
    
    
    return taxCalculationTotal;
}
-(float)taxCalculationForMaint
{
    float taxCalculationCreditMaint;
    taxCalculationCreditMaint=0;
    [self fetchForAppliedDiscountFromCoreDataForTaxCalculation];
    /*if (arrForTaxCredit.count>0)
     {
     taxCalculationCreditMaint= [self fetchFromCoreDataStandardForTaxForMaint];
     }*/
    taxCalculationCreditMaint= [self fetchFromCoreDataStandardForTaxForMaint];
    return taxCalculationCreditMaint;
}
-(void)fetchForAppliedDiscountFromCoreDataForTaxCalculation
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    arrForTaxCoupon=[[NSMutableArray alloc]init];
    arrForTaxCredit=[[NSMutableArray alloc]init];
    
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            /*if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
             {
             [arrForTaxCoupon addObject:matchesDiscount];
             }
             else
             {
             [arrForTaxCredit addObject:matchesDiscount];
             
             }*/
            [arrForTaxCredit addObject:matchesDiscount];
        }
    }
    
}
-(float)fetchFromCoreDataStandardForTax :(NSMutableArray *)arrayCouponTax :(NSString *)discoutType
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController * fetchedResultsControllerSalesInfoTax;
    fetchedResultsControllerSalesInfoTax = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerSalesInfoTax setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [fetchedResultsControllerSalesInfoTax performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrAllObjTax;
    arrAllObjTax = [fetchedResultsControllerSalesInfoTax fetchedObjects];
    
    float totalTaxApplicableAmountCoupon=0;
    
    NSMutableArray *arrDiscountCreditInitialTemp=[[NSMutableArray alloc]init];
    NSMutableArray *arrDiscountCouponInitialTemp=[[NSMutableArray alloc]init];
    
    for(int i=0; i<arrForTaxCredit.count;i++)
    {
        NSDictionary *dict=[arrForTaxCredit objectAtIndex:i];
        
        /* if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
         {
         [arrDiscountCreditInitialTemp addObject:dict];
         }
         if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountType"]]isEqualToString:@"coupon"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountType"]]isEqualToString:@"Coupon"])
         {
         [arrDiscountCreditInitialTemp addObject:dict];
         }*/
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForInitial"]]isEqualToString:@"true"])
        {
            [arrDiscountCreditInitialTemp addObject:dict];
        }
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountType"]]isEqualToString:@"coupon"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountType"]]isEqualToString:@"Coupon"])
        {
            [arrDiscountCouponInitialTemp addObject:dict];
        }
        
    }
    
    for (int i=0; i<arrAllObjTax.count; i++)
    {
        NSManagedObject *matchesAllObjTax=[arrAllObjTax objectAtIndex:i];
        
        if ([strIsServiceAddrTaxExempt isEqualToString:@"false"])
        {
            if([strServiceAddressSubType isEqualToString:@"Commercial"])
            {
                if ([[dictCommercialStatus valueForKey:[matchesAllObjTax valueForKey:@"serviceSysName"]] isEqualToString:@"1"])
                {
                    
                    float totalCouponDiscount,totalCreditDiscount;
                    totalCouponDiscount=0;totalCreditDiscount=0;
                    for (int k=0; k<arrDiscountCouponInitialTemp.count; k++)
                    {
                        totalCouponDiscount=0;
                        NSManagedObject* matchesTaxCoupon=[arrDiscountCouponInitialTemp objectAtIndex:k];
                        if([[NSString stringWithFormat:@"%@",[matchesTaxCoupon valueForKey:@"soldServiceId"]] isEqualToString:[NSString stringWithFormat:@"%@",[matchesAllObjTax valueForKey:@"soldServiceStandardId"]]])
                        {
                            totalCouponDiscount=totalCouponDiscount+[[matchesTaxCoupon valueForKey:@"appliedInitialDiscount"]floatValue];
                        }
                        
                    }
                    //New
                    for (int k=0; k<arrDiscountCreditInitialTemp.count; k++)
                    {
                        totalCreditDiscount=0;
                        NSManagedObject* matchesTaxCoupon=[arrDiscountCreditInitialTemp objectAtIndex:k];
                        if([[NSString stringWithFormat:@"%@",[matchesTaxCoupon valueForKey:@"soldServiceId"]] isEqualToString:[NSString stringWithFormat:@"%@",[matchesAllObjTax valueForKey:@"soldServiceStandardId"]]])
                        {
                            totalCreditDiscount=totalCreditDiscount+[[matchesTaxCoupon valueForKey:@"appliedInitialDiscount"]floatValue];
                        }
                        
                    }
                    
                    //ENd
                    
                    
                    float otherDiscount=0;
                    if([[matchesAllObjTax valueForKey:@"discount"]floatValue]>totalCouponDiscount)
                    {
                        otherDiscount=[[matchesAllObjTax valueForKey:@"discount"]floatValue]-totalCouponDiscount;
                    }
                    else
                    {
                        otherDiscount=totalCouponDiscount-[[matchesAllObjTax valueForKey:@"discount"]floatValue];
                    }
                    float unitValue;
                    
                    unitValue=[[matchesAllObjTax valueForKey:@"unit"]floatValue];
                    if (unitValue==0)
                    {
                        unitValue=1;
                    }
                    totalTaxApplicableAmountCoupon=totalTaxApplicableAmountCoupon+([[matchesAllObjTax valueForKey:@"initialPrice"]floatValue]*unitValue)-totalCouponDiscount-otherDiscount-totalCreditDiscount;
                    
                }
            }
            else if ([strServiceAddressSubType isEqualToString:@"Residential"])
            {
                if ([[dictResidentialStatus valueForKey:[matchesAllObjTax valueForKey:@"serviceSysName"]] isEqualToString:@"1"])
                {
                    
                    float totalCouponDiscount,totalCreditDiscount;
                    totalCouponDiscount=0;totalCreditDiscount=0;
                    for (int k=0; k<arrDiscountCouponInitialTemp.count; k++)
                    {
                        totalCouponDiscount=0;
                        NSManagedObject* matchesTaxCoupon=[arrDiscountCouponInitialTemp objectAtIndex:k];
                        if([[NSString stringWithFormat:@"%@",[matchesTaxCoupon valueForKey:@"soldServiceId"]] isEqualToString:[NSString stringWithFormat:@"%@",[matchesAllObjTax valueForKey:@"soldServiceStandardId"]]])
                        {
                            totalCouponDiscount=totalCouponDiscount+[[matchesTaxCoupon valueForKey:@"appliedInitialDiscount"]floatValue];
                        }
                        
                    }
                    //New
                    for (int k=0; k<arrDiscountCreditInitialTemp.count; k++)
                    {
                        totalCreditDiscount=0;
                        NSManagedObject* matchesTaxCoupon=[arrDiscountCreditInitialTemp objectAtIndex:k];
                        if([[NSString stringWithFormat:@"%@",[matchesTaxCoupon valueForKey:@"soldServiceId"]] isEqualToString:[NSString stringWithFormat:@"%@",[matchesAllObjTax valueForKey:@"soldServiceStandardId"]]])
                        {
                            totalCreditDiscount=totalCreditDiscount+[[matchesTaxCoupon valueForKey:@"appliedInitialDiscount"]floatValue];
                        }
                        
                    }
                    float otherDiscount=0;
                    
                    if([[matchesAllObjTax valueForKey:@"discount"]floatValue]>totalCouponDiscount)
                    {
                        otherDiscount=[[matchesAllObjTax valueForKey:@"discount"]floatValue]-totalCouponDiscount;
                    }
                    else
                    {
                        otherDiscount=totalCouponDiscount-[[matchesAllObjTax valueForKey:@"discount"]floatValue];
                    }
                    
                    float unitValue;
                    
                    unitValue=[[matchesAllObjTax valueForKey:@"unit"]floatValue];
                    if (unitValue==0)
                    {
                        unitValue=1;
                    }
                    totalTaxApplicableAmountCoupon=totalTaxApplicableAmountCoupon+([[matchesAllObjTax valueForKey:@"initialPrice"]floatValue]*unitValue)-totalCouponDiscount-otherDiscount-totalCreditDiscount;
                    
                    
                }
            }
            
        }
        
    }
    
    return totalTaxApplicableAmountCoupon;
}
-(float)fetchFromCoreDataStandardForTaxForMaint
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController * fetchedResultsControllerSalesInfoTax;
    fetchedResultsControllerSalesInfoTax = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerSalesInfoTax setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [fetchedResultsControllerSalesInfoTax performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrAllObjTax;
    arrAllObjTax = [fetchedResultsControllerSalesInfoTax fetchedObjects];
    
    float totalTaxApplicableAmountCoupon=0;
    
    /*for (int i=0; i<arrForTaxCredit.count; i++)
     {
     NSManagedObject *matchesTaxCoupon=[arrForTaxCredit objectAtIndex:i];
     NSString *strIdTaxCoupon=[NSString stringWithFormat:@"%@",[matchesTaxCoupon valueForKey:@"soldServiceId"]];
     
     for (int j=0; j<arrAllObjTax.count; j++)
     {
     NSManagedObject *matchesAllObjTax=[arrAllObjTax objectAtIndex:j];
     
     if ([strIdTaxCoupon isEqualToString:[NSString stringWithFormat:@"%@",[matchesAllObjTax valueForKey:@"soldServiceStandardId"]]])
     {
     if ([strIsServiceAddrTaxExempt isEqualToString:@"false"])
     {
     if([strServiceAddressSubType isEqualToString:@"Commercial"])
     {
     if ([[dictCommercialStatus valueForKey:[matchesAllObjTax valueForKey:@"serviceSysName"]] isEqualToString:@"1"])
     {
     totalTaxApplicableAmountCoupon=totalTaxApplicableAmountCoupon+([[matchesAllObjTax valueForKey:@"maintenancePrice"]floatValue]*[[matchesAllObjTax valueForKey:@"unit"]floatValue])-[[matchesTaxCoupon valueForKey:@"appliedMaintDiscount"]floatValue];
     }
     }
     else if ([strServiceAddressSubType isEqualToString:@"Residential"])
     {
     if ([[dictResidentialStatus valueForKey:[matchesAllObjTax valueForKey:@"serviceSysName"]] isEqualToString:@"1"])
     {
     totalTaxApplicableAmountCoupon=totalTaxApplicableAmountCoupon+([[matchesAllObjTax valueForKey:@"maintenancePrice"]floatValue]*[[matchesAllObjTax valueForKey:@"unit"]floatValue])-[[matchesTaxCoupon valueForKey:@"appliedMaintDiscount"]floatValue];
     
     }
     }
     }
     }
     }
     }*/
    
    NSMutableArray* arrDiscountCreditMaintTemp=[[NSMutableArray alloc]init];
    for(int i=0; i<arrForTaxCredit.count;i++)
    {
        NSDictionary *dict=[arrForTaxCredit objectAtIndex:i];
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"applicableForMaintenance"]]isEqualToString:@"true"])
        {
            [arrDiscountCreditMaintTemp addObject:dict];
        }
        
    }
    
    for (int i=0; i<arrAllObjTax.count; i++)
    {
        NSManagedObject *matchesAllObjTax=[arrAllObjTax objectAtIndex:i];
        
        if ([strIsServiceAddrTaxExempt isEqualToString:@"false"])
        {
            if([strServiceAddressSubType isEqualToString:@"Commercial"])
            {
                if ([[dictCommercialStatus valueForKey:[matchesAllObjTax valueForKey:@"serviceSysName"]] isEqualToString:@"1"])
                {
                    
                    float totalCouponDiscount;
                    totalCouponDiscount=0;
                    for (int k=0; k<arrDiscountCreditMaintTemp.count; k++)
                    {
                        totalCouponDiscount=0;
                        NSManagedObject* matchesTaxCoupon=[arrDiscountCreditMaintTemp objectAtIndex:k];
                        if([[NSString stringWithFormat:@"%@",[matchesTaxCoupon valueForKey:@"soldServiceId"]] isEqualToString:[NSString stringWithFormat:@"%@",[matchesAllObjTax valueForKey:@"soldServiceStandardId"]]])
                        {
                            totalCouponDiscount=totalCouponDiscount+[[matchesTaxCoupon valueForKey:@"appliedMaintDiscount"]floatValue];
                        }
                        
                    }
                    float otherDiscount=0;
                    if([[matchesAllObjTax valueForKey:@"discount"]floatValue]==0)
                    {
                        //otherDiscount=totalCouponDiscount-[[matchesAllObjTax valueForKey:@"discount"]floatValue];
                    }
                    else
                    {
                        if([[matchesAllObjTax valueForKey:@"discount"]floatValue]>totalCouponDiscount)
                        {
                            otherDiscount=[[matchesAllObjTax valueForKey:@"discount"]floatValue]-totalCouponDiscount;
                        }
                        else
                        {
                            otherDiscount=totalCouponDiscount-[[matchesAllObjTax valueForKey:@"discount"]floatValue];
                        }
                    }
                    
                    totalTaxApplicableAmountCoupon=totalTaxApplicableAmountCoupon+([[matchesAllObjTax valueForKey:@"maintenancePrice"]floatValue]*[[matchesAllObjTax valueForKey:@"unit"]floatValue])-totalCouponDiscount;
                    //                                totalTaxApplicableAmountCoupon=totalTaxApplicableAmountCoupon+([[matchesAllObjTax valueForKey:@"initialPrice"]floatValue]*[[matchesAllObjTax valueForKey:@"unit"]floatValue])-[[matchesTaxCoupon valueForKey:@"appliedInitialDiscount"]floatValue];
                    
                }
            }
            else if ([strServiceAddressSubType isEqualToString:@"Residential"])
            {
                if ([[dictResidentialStatus valueForKey:[matchesAllObjTax valueForKey:@"serviceSysName"]] isEqualToString:@"1"])
                {
                    
                    float totalCouponDiscount;
                    totalCouponDiscount=0;
                    for (int k=0; k<arrDiscountCreditMaintTemp.count; k++)
                    {
                        //totalCouponDiscount=0;
                        NSManagedObject* matchesTaxCoupon=[arrDiscountCreditMaintTemp objectAtIndex:k];
                        if([[NSString stringWithFormat:@"%@",[matchesTaxCoupon valueForKey:@"soldServiceId"]] isEqualToString:[NSString stringWithFormat:@"%@",[matchesAllObjTax valueForKey:@"soldServiceStandardId"]]])
                        {
                            totalCouponDiscount=totalCouponDiscount+[[matchesTaxCoupon valueForKey:@"appliedMaintDiscount"]floatValue];
                        }
                        
                    }
                    
                    float otherDiscount=0;
                    if([[matchesAllObjTax valueForKey:@"discount"]floatValue]==0)
                    {
                        // otherDiscount=totalCouponDiscount-[[matchesAllObjTax valueForKey:@"discount"]floatValue];
                    }
                    else
                    {
                        if([[matchesAllObjTax valueForKey:@"discount"]floatValue]>totalCouponDiscount)
                        {
                            otherDiscount=[[matchesAllObjTax valueForKey:@"discount"]floatValue]-totalCouponDiscount;
                        }
                        else
                        {
                            otherDiscount=totalCouponDiscount-[[matchesAllObjTax valueForKey:@"discount"]floatValue];
                        }
                    }
                    
                    totalTaxApplicableAmountCoupon=totalTaxApplicableAmountCoupon+([[matchesAllObjTax valueForKey:@"maintenancePrice"]floatValue]*[[matchesAllObjTax valueForKey:@"unit"]floatValue])-totalCouponDiscount;
                    
                    //                                totalTaxApplicableAmountCoupon=totalTaxApplicableAmountCoupon+([[matchesAllObjTax valueForKey:@"initialPrice"]floatValue]*[[matchesAllObjTax valueForKey:@"unit"]floatValue])-[[matchesTaxCoupon valueForKey:@"appliedInitialDiscount"]floatValue];
                    
                }
            }
        }
        
    }
    return totalTaxApplicableAmountCoupon;
}
-(void)serviceTaxableStatus
{
    
    NSMutableArray *name,*sysName,*commercialVal,*residentialVal;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    commercialVal=[[NSMutableArray alloc]init];
    residentialVal=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *str;
                str=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsTaxable"]];
                NSLog(@"str>>>>%@",str);
                [name addObject:str];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [commercialVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsCommercialTaxable"]]];
                [residentialVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsResidentialTaxable"]]];
                
            }
        }
    }
    
    dictTaxStatus = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
    dictCommercialStatus = [NSDictionary dictionaryWithObjects:commercialVal forKeys:sysName];
    dictResidentialStatus = [NSDictionary dictionaryWithObjects:residentialVal forKeys:sysName];
    NSLog(@"Service Tax Status Dictionary %@/n Commercial %@/n Residential %@",dictTaxStatus,dictCommercialStatus,dictResidentialStatus);
    
}
-(void)updateLeadAppliedDiscountForAppliedCoupon
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            for (int j=0; j<arrStanAllSoldServiceStandardId.count; j++)
            {
                if ([[matchesDiscount valueForKey:@"soldServiceId"] isEqualToString:[arrStanAllSoldServiceStandardId objectAtIndex:j]])
                {
                    [matchesDiscount setValue:@"true" forKey:@"isApplied"];
                }
                
            }
        }
    }
    [context save:&error1];
}
-(void)creditDetailBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getAppliedCreditDetail];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}
-(void)getAppliedCreditDetail{
    
    global = [[Global alloc] init];
    
    NSString *strDetailUrl=@"/api/CoreToSaleAuto/GetDiscountsForAccountExcludingLeadNo?";//@"/api/MobileTosaleAuto/GetAllDiscountsByDiscountTypeAsync?";
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSString *strType=@"Credit";
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strEmpBranchSysName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeBranchSysName"]];
    
    //NSString *strUrl = [NSString stringWithFormat:@"%@%@companyKey=%@&branchSysName=%@&type=%@&AccountNo=%@&&leadNoToExclude=%@",strServiceUrlMain,strDetailUrl,strCompanyKey,strEmpBranchSysName,strType,strAccountNoGlobal,strLeadNumber];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@companyKey=%@&branchSysName=%@&AccountNo=%@&&leadNoToExclude=%@",strServiceUrlMain,strDetailUrl,strCompanyKey,strEmpBranchSysName,strAccountNoGlobal,strLeadNumber];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
            NSData* jsonData = [NSData dataWithData:data];
            NSDictionary* ResponseDictCredit = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
            [dictTempResponse setObject:ResponseDictCredit forKey:@"response"];
            NSDictionary *dict=[[NSDictionary alloc]init];
            dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
            
            NSLog(@"Response on Applied Credit Detail Without Null = = = =  = %@",dict);
            arrAppliedCreditDetail=[dict valueForKey:@"response"];
            _tblAppliedCredit.tag=106;
            [_tblAppliedCredit reloadData];
            [self heightCreditCoupon];
        }];
    }
    @catch (NSException *exception) {
        [global AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}

// Saavan Changes 9 july 2018

-(NSArray*)fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot :(NSString*)strType
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    NSMutableArray *arrDiscountCouponNew=[[NSMutableArray alloc]init];
    NSMutableArray *arrDiscountCreditNew=[[NSMutableArray alloc]init];
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            for (int j=0; j<arrStanAllSoldServiceStandardId.count; j++)
            {
                NSString *strId=[arrStanAllSoldServiceStandardId objectAtIndex:j];
                if ([strId isEqualToString:[matchesDiscount valueForKey:@"soldServiceId"]])
                {
                    if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
                    {
                        [arrDiscountCouponNew addObject:matchesDiscount];
                    }
                    else
                    {
                        [arrDiscountCreditNew addObject:matchesDiscount];
                        
                    }
                    
                    
                }
                // [arrDiscountCoupon addObject:matchesDiscount];
            }
        }
    }
    
    NSMutableArray *arrTempToReturn=[[NSMutableArray alloc]init];
    
    //if ([strType isEqualToString:@"Credit"]) {
    
    if (arrDiscountCreditNew.count>0) {
        
        [arrTempToReturn addObjectsFromArray:arrDiscountCreditNew];
        
    }
    
    
    //} else {
    
    if (arrDiscountCouponNew.count>0) {
        
        [arrTempToReturn addObjectsFromArray:arrDiscountCouponNew];
        
    }
    
    // }
    
    NSArray *arrTemopNew=[[NSArray alloc]init];
    
    arrTemopNew=arrTempToReturn;
    
    return arrTemopNew;
    
}


-(void)fetchSoldServiceStandardDetailToUpdate
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrObjTemp = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    for (int i=0; i<arrObjTemp.count; i++)
    {
        NSManagedObject *obj =[arrObjTemp objectAtIndex:i];
        
    }
}
- (IBAction)actionOnChemicalSensitivityList:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                 bundle: nil];
        ChemicalSensitivityList
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChemicalSensitivityList"];
        objByProductVC.strFrom=@"sales";
        objByProductVC.strId=strLeadId;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
    //End
    
}
-(void)newAlertMethodAddImage
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
        NSLog(@"The CApture Image.");
        
        
        if (arrNoImage.count<10)
        {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                    
                    
                    
                }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                    
                    if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        [[UIApplication sharedApplication] openURL:url];
                    } else {
                        
                    }
                }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //............
        
    }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
        NSLog(@"The Gallery.");
        if (arrNoImage.count<10)
        {
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                    
                    
                    
                }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                    
                    if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        [[UIApplication sharedApplication] openURL:url];
                    }
                    else
                    {
                        
                        
                        
                    }
                    
                }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
        
        
        
    }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)newAlertMethodCheckImage
{//otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* yesPreview = [UIAlertAction actionWithTitle:@"Preview Image" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
        if (isCheckFrontImage)
        {
            
            if (arrOFImagesName.count==0)
            {
                
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }
            else
            {
                
                [self performSelector:@selector(goToImagePreviewCheckFrontImage)  withObject:nil afterDelay:0.1];
                
                
            }
            
        }
        else
        {
            
            if (arrOfCheckBackImage.count==0)
            {
                
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }
            else
            {
                
                [self performSelector:@selector(goToImagePreviewCheckBackImage)  withObject:nil afterDelay:0.1];
                
                
            }
        }
    }];
    [alert addAction:yesPreview];
    
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            
        {
            [global AlertMethod:@"Alert!" :@"No Allowed"];
        }
        else
        {
            NSArray *arrTemp;
            if (isCheckFrontImage)
            {
                
                arrTemp=arrOFImagesName;
                
            }
            else
            {
                
                arrTemp=arrOfCheckBackImage;
                
            }
            
            if (arrTemp.count>=1) {
                
                if(isCheckFrontImage)
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check front image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check back image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            } else {
                NSLog(@"The CApture Image.");
                
                BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                    
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Camera Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                        
                        
                        
                    }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                        
                        if ( ( (&UIApplicationOpenSettingsURLString)) != NULL) {
                            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                            [[UIApplication sharedApplication] openURL:url];
                        } else {
                            
                            //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                            //                                     [alert show];
                            
                        }
                        
                    }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
        }
        
    }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
        if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
            
        {
            [global AlertMethod:@"Alert!" :@"No Allowed"];
        }
        else
        {
            NSArray *arrTemp;
            if (isCheckFrontImage)
            {
                
                arrTemp=arrOFImagesName;
                
            }else{
                
                arrTemp=arrOfCheckBackImage;
                
            }
            
            if (arrTemp.count>=1)
            {
                if(isCheckFrontImage)
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check front image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check back image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
                
            }
            else
            {
                
                BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                        
                    }
                    else
                    {
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                    
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Gallery Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                        
                        
                        
                    }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                        
                        if ( ((&UIApplicationOpenSettingsURLString)) != NULL) {
                            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                            [[UIApplication sharedApplication] openURL:url];
                        } else {
                            
                            //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                            //                                     [alert show];
                            
                        }
                        
                    }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
        }
        
    }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
        
        
        
    }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)finalHeightManage
{
    [self fetchFromCoreDataStandard];
    [self fetchForAppliedDiscountFromCoreData];
    [self heightMaintenanceTable];
    [self heightNonStandardTable];
}
- (IBAction)actionOnBtnTermsAndConditions:(id)sender
{
    [self endEditing];
    if(arrMultipleTermsCondtions.count>0)
    {
        tblData.tag=12;
        [self tableLoad:tblData.tag];
    }
    else
    {
        [global AlertMethod:Alert :NoDataAvailable];
    }
}
- (IBAction)actionOnBtnCoverLetter:(id)sender
{
    [self endEditing];
    if(arrCoverLetter.count==0)
    {
        [global AlertMethod:Alert :NoDataAvailableee];
        return;
    }
    tblData.tag=10;
    [self tableLoad:tblData.tag];
}

- (IBAction)actionOnBtnIntroductionLetter:(id)sender
{
    [self endEditing];
    if(arrIntroductionLetter.count==0)
    {
        [global AlertMethod:Alert :NoDataAvailableee];
        return;
    }
    tblData.tag=11;
    [self tableLoad:tblData.tag];
}
-(void)setValueForParticipants
{
    if (tblData.tag==12)
    {
        NSMutableArray *arrTitle=[[NSMutableArray alloc]init];
        
        for(int i=0;i<arrSelectedMultipleCondition.count;i++)
        {
            [arrTitle addObject:[dictTitleFromTermId valueForKey:[arrSelectedMultipleCondition objectAtIndex:i]]];
        }
        
        if (arrTitle.count==0)
        {
            [_btnTermsConditions setTitle:@"--Select Terms & Conditions --" forState:UIControlStateNormal];
        }
        else
        {
            NSString *joinedComponents = [arrTitle componentsJoinedByString:@","];
            [_btnTermsConditions setTitle:joinedComponents forState:UIControlStateNormal];
        }
        
        if(arrSelectedMultipleCondition.count==0)
        {
            [_btnIAgree setImage:[UIImage imageNamed:@"check_box_1.png"]forState:UIControlStateNormal];
            chkIAgree=NO;
        }
    }
    else if (tblData.tag==14)
    {
        NSMutableArray *arrTitle=[[NSMutableArray alloc]init];
        
        for(int i=0;i<arrSelectedMarketingContent.count;i++)
        {
            [arrTitle addObject:[dictSalesContentTitleFromSysName valueForKey:[arrSelectedMarketingContent objectAtIndex:i]]];
        }
        
        if (arrTitle.count==0)
        {
            [_btnMarketingContent setTitle:@"Select Sales Marketing Content" forState:UIControlStateNormal];
        }
        else
        {
            NSString *joinedComponents = [arrTitle componentsJoinedByString:@","];
            [_btnMarketingContent setTitle:joinedComponents forState:UIControlStateNormal];
        }
        
    }
}
- (IBAction)actionOnBtnTermsOfServices:(id)sender
{
    if(arrTermsOfService.count==0)
    {
        [global AlertMethod:Alert :NoDataAvailableee];
        return;
    }
    tblData.tag=13;
    [self tableLoad:tblData.tag];
}
- (IBAction)actionOnBtnAgreementValidFor:(id)sender
{
    if ([_btnAgreementValidFor.currentImage isEqual:[UIImage imageNamed:@"check_box_1New.png"]])
    {
        _txtDays.hidden=NO;
        _lblDays.hidden=NO;
        [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        strIsAgreementValidFor = @"true";    }
    else
    {
        [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
        _txtDays.hidden=YES;
        _lblDays.hidden=YES;
        strIsAgreementValidFor = @"false";
    }
}

-(void)setHeight
{
    _const_TblService_H.constant=80*arrSavedServiceDetail.count;
    _const_TblScope_H.constant=80*arrSavedScopeDetail.count;
    _const_TblTarget_H.constant=80*arrSavedTargetDetail.count;
}

//============================================================================
#pragma mark- ------------------ Fetch From DB Methods (Service/Scope/Target) ----------------
//============================================================================


-(void)buttonClickedCheckBoxAgreementServiceClarkPest:(UIButton*)sender
{
    [self endEditing];
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    ClarkPestSelectServiceTableViewCell *tappedCell = (ClarkPestSelectServiceTableViewCell *)[_tblService cellForRowAtIndexPath:indexpath];
    
    
    if ([tappedCell.btnAddToAgreement.currentImage isEqual:[UIImage imageNamed:@"check_box_2New.png"]])
    {
        // [tappedCell.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
        chkBtnCheckBoxStan=NO;
        
        
    }
    else
    {
        //  [tappedCell.btnAddToAgreement setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
        chkBtnCheckBoxStan=YES;
        
    }
    NSManagedObject *record = [arrSavedServiceDetail objectAtIndex:sender.tag];//[self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath1];
    [self updateServiceClarkPest:record];
    if(isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in buttonClickedStandardService");
        [global updateSalesModifydate:strLeadId];
    }
    [_tblService reloadData];
    [self CalculateFinalPrice];
}
-(void)buttonClickedCheckBoxAgreementNonStanadrdServiceClarkPest:(UIButton*)sender
{
    [self endEditing];
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    ClarkPestTableViewCellNonStandard *tappedCell = (ClarkPestTableViewCellNonStandard *)[_tblNonStandardClarkPest cellForRowAtIndexPath:indexpath];
    
    
    if ([tappedCell.btnAddToAgreement.currentImage isEqual:[UIImage imageNamed:@"check_box_2New.png"]])
    {
        chkBtnCheckBoxNonStan=NO;
        
    }
    else
    {
        chkBtnCheckBoxNonStan=YES;
    }
    NSManagedObject *record = [arrSavedNonStanServiceDetail objectAtIndex:sender.tag];
    
    [self updateServiceNonStandardClarkPest:record];
    
    if(isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in buttonClickedStandardService");
        [global updateSalesModifydate:strLeadId];
    }
    [_tblNonStandardClarkPest reloadData];
    [self CalculateFinalPrice];
}
-(void)updateServiceClarkPest:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    
    NSError *error1 = nil;
    NSManagedObject *matchesSerivce =indexValue;
    if (chkBtnCheckBoxStan==YES)
    {
        [matchesSerivce setValue:@"true" forKey:@"isSold"];
    }
    else
    {
        [matchesSerivce setValue:@"false" forKey:@"isSold"];
    }
    [matchesSerivce setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataStandardClarkPest];
}
-(void)updateServiceNonStandardClarkPest:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceNonStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceNonStandardDetail];
    
    NSError *error1 = nil;
    
    NSManagedObject *matchesSerivce =indexValue;
    
    if (chkBtnCheckBoxNonStan==YES)
    {
        [matchesSerivce setValue:@"true" forKey:@"isSold"];
    }
    else
    {
        [matchesSerivce setValue:@"false" forKey:@"isSold"];
        
    }
    [matchesSerivce setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataNonStandardClarkPest];
}


-(void)fetchFromCoreDataStandardClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;// =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    //predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrSavedServiceDetail=nil;
    
    
    float tableViewHeight = 0.0;
    if (arry.count>0)
    {
        arrSavedServiceDetail = arry;//[arry mutableCopy];
        
        for(NSDictionary *dict in arry)
        {
            CGFloat textHght = [self findHeightForText:[dict valueForKey:@"serviceDescription"] havingWidth:self.view.frame.size.width-135.0 andFont:[UIFont systemFontOfSize:14.0]].height;//625.0
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+190;// 78 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+(190-21));// (78-21) i.e row height after removing description height
            }
        }
        
        _const_TblService_H.constant = tableViewHeight;
        
        _const_ServiceLabel_H.constant=30;
        _const_Service_Top.constant=10;
        _const_Service_Bottom.constant=10;
        [_tblService reloadData];
    }
    else
    {
        _const_TblService_H.constant = 0.0;
        
        _const_ServiceLabel_H.constant=0;
        _const_Service_Top.constant=0;
        _const_Service_Bottom.constant=0;
    }
    
    
    //    [_tblService reloadData];
    //    [self setHeight];
}
-(void)fetchFromCoreDataNonStandardClarkPest
{
    
    float tableViewHeight = 0.0;
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate ;
    
    // predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    arrSavedNonStanServiceDetail=nil;
    //_const_TblNonStan_H.constant = 0.0;
    
    if (arry.count>0)
    {
        
        arrSavedNonStanServiceDetail = arry;
        
        
        for(NSDictionary *dict in arry)
        {
            
            /*  CGFloat textHght = [self findHeightForText:[dict valueForKey:@"serviceDescription"] havingWidth:self.view.frame.size.width-135.0 andFont:[UIFont systemFontOfSize:14.0]].height;//625.0
             
             if(textHght<=21.0)
             {
             tableViewHeight = tableViewHeight+190;// 125 i.e default row height
             }
             else
             {
             tableViewHeight = tableViewHeight+(textHght+(190-21));// (125-21) i.e row height after removing description height
             }*/
            
            CGFloat textHght = [self getAttributedLabelHeight:[dict valueForKey:@"serviceDescription"]];
            
            WebService *objWebService = [[WebService alloc] init];
            
            textHght = [objWebService estimatedHeightOfLabelForObjectiveCWithText:[dict valueForKey:@"serviceDescription"] view:self.view];
            
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+190 ;// 125 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+(190-21));// (125-21) i.e row height after removing description height
            }
        }
        
        //[self showAllSavedServiceName];
        
        //_const_TblNonStan_H.constant = tableViewHeight + (100*arry.count);
        //_const_Lbl_NonStandard_H.constant = 35.0;
        [_tblNonStandardClarkPest reloadData];
        
        //float ftbl = _tblNonStandardClarkPest.frame.origin.y + _tblNonStandardClarkPest.contentSize.height + 15;
        
    }
    else
    {
        _const_TblNonStan_H.constant = 0.0;
        _const_Lbl_NonStandard_H.constant = 0.0;
        
        
        
        /*  _textViewInitialDescription.text = @"";
         [_textViewInitialDescription resignFirstResponder];
         _textViewMaintDescription.text = @"";
         [_textViewMaintDescription resignFirstResponder];*/
        
    }
    [self CalculateFinalPrice];
}

-(void)fetchScopeFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialScopeExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    arrSavedScopeDetail=nil;
    float tableViewHeight = 0.0;
    if (arry.count>0)
    {
        arrSavedScopeDetail=arry;
        for(NSDictionary *dict in arrSavedScopeDetail)
        {
            CGFloat textHght = [self findHeightForText:[dict valueForKey:@"scopeDescription"] havingWidth:self.view.frame.size.width-135.0 andFont:[UIFont systemFontOfSize:14.0]].height;
            
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+78;// 78 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+57);// 57 i.e row height after removing description height
            }
        }
        _const_TblScope_H.constant = tableViewHeight;
        
        _const_ScopeLabel_H.constant=30;
        _const_Scope_Top.constant=10;
        _const_Scope_Bottom.constant=10;
        
        [_tblScope reloadData];
    }
    else
    {
        _const_TblScope_H.constant = 0.0;
        
        _const_ScopeLabel_H.constant=0;
        _const_Scope_Top.constant=0;
        _const_Scope_Bottom.constant=0;
    }
    
    
    //    [_tblScope reloadData];
    //
    //    [self setHeight];
}

-(void)fetchTargetFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTargetExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    arrSavedTargetDetail=nil;
    
    float tableViewHeight = 0.0;
    if (arry.count>0)
    {
        arrSavedTargetDetail=arry;
        
        for(NSDictionary *dict in arry)
        {
            CGFloat textHght = [self findHeightForText:[dict valueForKey:@"targetDescription"] havingWidth:self.view.frame.size.width-135.0 andFont:[UIFont systemFontOfSize:14.0]].height;
            
            if(textHght<=21.0)
            {
                tableViewHeight = tableViewHeight+78;// 78 i.e default row height
            }
            else
            {
                tableViewHeight = tableViewHeight+(textHght+57);// 57 i.e row height after removing description height
            }
        }
        
        float targetImageHeight = 0.0;
        for (int i=0;i<arry.count;i++)
        {
            NSManagedObject *matchedTargetDetail = [arry objectAtIndex:i];
            NSMutableArray *arrTargetImage= [[NSMutableArray alloc]init];
            
            arrTargetImage = [self fetchTargetImageDetail:[matchedTargetDetail valueForKey:@"leadCommercialTargetId"] MobileTargetId:[matchedTargetDetail valueForKey:@"mobileTargetId"]];
            
            targetImageHeight = targetImageHeight + (arrTargetImage.count * 100 +5);
            
            
            /*for (int j=0;i<arrTargetImage.count;j++)
             {
             targetImageHeight = targetImageHeight + 105;
             }*/
        }
        
        
        
        
        
        _const_TblTarget_H.constant = tableViewHeight + targetImageHeight  ;
        
        _const_TargetLabel_H.constant=30;
        _const_Target_Top.constant=10;
        _const_Target_Bottom.constant=10;
        
        [_tblTarget reloadData];
    }
    else
    {
        _const_TblTarget_H.constant = 0.0;
        
        _const_TargetLabel_H.constant=0;
        _const_Target_Top.constant=0;
        _const_Target_Bottom.constant=0;
    }
    
    //    [_tblTarget reloadData];
    //    [self setHeight];
}
-(void)fetchFromCoreDataStandardClarkPestSoldCount
{
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    // arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrSoldCount = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
}
-(void)fetchFromCoreDataNonStandardClarkPestSoldCount
{
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrSoldCountNonStan = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
}


#pragma mark - fetchInitialPrice
-(void)fetchLeadCommercialInitialInfoFromCoreData
{
    NSMutableArray *arrayTemp = [NSMutableArray new];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialInitialInfoExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    for(NSManagedObject *initialInfo in arry)
    {
        NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
        NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
        [arrayTemp addObject:dict];
    }
    
    float totalInitialPrice = 0.0;
    if (arrayTemp.count>0)
    {
        arrSavedInitialPriceDetail=(NSMutableArray*)arrayTemp;
        
        for(NSDictionary *dict in arrSavedInitialPriceDetail)
        {
            totalInitialPrice = totalInitialPrice + [[dict valueForKey:@"initialPrice"] floatValue];
        }
    }
    subTotalInitialPrice = totalInitialPrice;
    subTotalInitialPriceCopy = totalInitialPrice;
    _lblNewInitialPriceSubtotalAmount.text = [NSString stringWithFormat:@"%.2f",subTotalInitialPrice];
    
}

-(void)fetchLeadCommercialMaintInfoFromCoreData
{
    NSMutableArray *arrayTemp = [NSMutableArray new];
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    for(NSManagedObject *initialInfo in arry)
    {
        NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
        NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
        [arrayTemp addObject:dict];
    }
    
    float totalMaintPrice = 0.0;
    
    if (arrayTemp.count>0)
    {
        arrSavedMaintPriceDetail=arrayTemp;
        
        for(NSDictionary *dict in arrSavedMaintPriceDetail)
        {
            totalMaintPrice = totalMaintPrice + [[dict valueForKey:@"maintenancePrice"] floatValue];
        }
    }
    subTotalMaintPrice = totalMaintPrice;
    subTotalMaintPriceCopy = totalMaintPrice;
    _lblNewMaintPriceSubtotalAmount.text = [NSString stringWithFormat:@"%.2f",subTotalMaintPrice];
    
}
-(void)saveCommercialLetterTemplate:(NSDictionary*)dictTempCoverLetter introLetter:(NSDictionary*)dictTempIntroLetter andTermsConditions:(NSDictionary*)dictTempTerms
{
    entityLeadCommercialDetailExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    
    LeadCommercialDetailExtDc *objLeadDetails = [[LeadCommercialDetailExtDc alloc]initWithEntity:entityLeadCommercialDetailExtDc insertIntoManagedObjectContext:context];
    
    objLeadDetails.leadId = strLeadId;
    objLeadDetails.leadCommercialDetailId = @"";
    objLeadDetails.companyKey = strCompanyKey;
    objLeadDetails.userName = strUserName;
    
    if(dictTempCoverLetter.count>0)
    {
        objLeadDetails.coverLetterSysName = [NSString stringWithFormat:@"%@",[dictTempCoverLetter valueForKey:@"SysName"]];
    }
    else
    {
        objLeadDetails.coverLetterSysName = @"";
    }
    
    if(dictTempIntroLetter.count>0)
    {
        objLeadDetails.introSysName = [NSString stringWithFormat:@"%@",[dictTempIntroLetter valueForKey:@"SysName"]];
        objLeadDetails.introContent = [NSString stringWithFormat:@"%@",[dictTempIntroLetter valueForKey:@"TemplateContent"]];
    }
    else
    {
        objLeadDetails.introSysName = @"";
        objLeadDetails.introContent = @"";
    }
    
    if(dictTempTerms.count>0)
    {
        objLeadDetails.termsOfServiceSysName = [NSString stringWithFormat:@"%@",[dictTempTerms valueForKey:@"SysName"]];
        objLeadDetails.termsOfServiceContent = [NSString stringWithFormat:@"%@",[self convertHTML:[dictTempTerms valueForKey:@"Description"]]];
    }
    else
    {
        objLeadDetails.termsOfServiceSysName = @"";
        objLeadDetails.termsOfServiceContent = @"";
    }
    
    if([strIsAgreementValidFor isEqualToString:@"true"])
    {
        objLeadDetails.isAgreementValidFor = @"true";
        objLeadDetails.validFor = [NSString stringWithFormat:@"%@",_txtDays.text];
    }
    else
    {
        objLeadDetails.isAgreementValidFor = @"false";
        objLeadDetails.validFor = @"";
    }
    
    objLeadDetails.isInitialTaxApplicable = [NSString stringWithFormat:@"%@",isTaxApplicableInitialPrice];
    
    objLeadDetails.isMaintTaxApplicable = [NSString stringWithFormat:@"%@",isTaxApplicableMaintPrice];
    
    NSError *error1;
    [context save:&error1];
}

//-(void)fetchLead
//{
//    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    context = [appDelegate managedObjectContext];
//    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
//    requestNew = [[NSFetchRequest alloc] init];
//    [requestNew setEntity:entityLeadDetail];
//    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
//
//    [requestNew setPredicate:predicate];
//
//    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
//    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//
//    [requestNew setSortDescriptors:sortDescriptors];
//
//    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
//    [self.fetchedResultsControllerSalesInfo setDelegate:self];
//
//    // Perform Fetch
//    NSError *error1 = nil;
//    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
//    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
//    NSString *str;
//    if (arrAllObj12.count==0)
//    {
//    }
//    else
//    {
//        NSManagedObject *matches12=arrAllObj12[0];
//        str=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]];
//
////        _txtFieldProposedServiceDate.text = [global ChangeDateMechanicalEquipment:[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"initialServiceDate"]]];//
////        [_btnRecurringMonth setTitle:[matches12 valueForKey:@"recurringServiceMonth"] forState:UIControlStateNormal];
//    }
//}
#pragma mark - method to get height of string

- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGSize size = CGSizeZero;
    if (text) {
        CGRect frame = [[self convertHTML:text] boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

-(void)getLetterTemplateAndTermsOfService
{
    arrayLetterTemplateMaster = [NSMutableArray new];
    arrCoverLetter = [NSMutableArray new];
    arrIntroductionLetter = [NSMutableArray new];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictMasters=[userDefaults valueForKey:@"MasterSalesAutomation"];
    arrayLetterTemplateMaster = [[dictMasters valueForKey:@"LetterTemplateMaster"] mutableCopy];
    arrTermsOfService = [[dictMasters valueForKey:@"TermsOfServiceMaster"] mutableCopy];
    
    
    for(NSDictionary *dict in arrayLetterTemplateMaster)
    {
        if([[dict valueForKey:@"LetterTemplateType"] isEqualToString:@"CoverLetter"])
        {
            [arrCoverLetter addObject:dict];
        }
        else if ([[dict valueForKey:@"LetterTemplateType"] isEqualToString:@"Introduction"])
        {
            [arrIntroductionLetter addObject:dict];
        }
    }
}
-(void)fetchLetterTemplateFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrLetterTemplate = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    if(arrLetterTemplate.count>0)
    {
        matchesDiscount = [arrLetterTemplate firstObject];
        
        if([[matchesDiscount valueForKey:@"coverLetterSysName"] length]>0)
        {
            // [_btnCoverLetter setTitle:[matchesDiscount valueForKey:@"coverLetterSysName"] forState:UIControlStateNormal];
            for(NSDictionary *dict in arrCoverLetter)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"coverLetterSysName"]])
                {
                    dictCoverLetter = dict;
                    [_btnCoverLetter setTitle:[NSString stringWithFormat:@"%@",[dictCoverLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
                    break;
                }
            }
        }
        else
        {
            [_btnCoverLetter setTitle:@"Select Cover Letter" forState:UIControlStateNormal];
        }
        
        if([[matchesDiscount valueForKey:@"introSysName"] length]>0)
        {
            //            [_btnIntroductionLetter setTitle:[matchesDiscount valueForKey:@"introSysName"] forState:UIControlStateNormal];
            
            if([[matchesDiscount valueForKey:@"introContent"] length]>0)
            {
                // _txtViewIntroductionLetter.text = [self convertHTML:[matchesDiscount valueForKey:@"introContent"]];
                _txtViewIntroductionLetter.attributedText = [self getAttributedString:[matchesDiscount valueForKey:@"introContent"]];
            }
            
            for(NSDictionary *dict in arrIntroductionLetter)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"introSysName"]])
                {
                    dictIntroLetter = dict;
                    [_btnIntroductionLetter setTitle:[NSString stringWithFormat:@"%@",[dictIntroLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
                    
                    break;
                }
            }
        }
        else
        {
            [_btnIntroductionLetter setTitle:@"Select Introduction Letter" forState:UIControlStateNormal];
        }
        
        if([[matchesDiscount valueForKey:@"termsOfServiceSysName"] length]>0)
        {
            // [_btnTermsOfServices setTitle:[matchesDiscount valueForKey:@"termsOfServiceSysName"] forState:UIControlStateNormal];
            if([[matchesDiscount valueForKey:@"termsOfServiceContent"] length]>0)
            {
                _txtViewTermsServices.attributedText = [self getAttributedString:[matchesDiscount valueForKey:@"termsOfServiceContent"]];
            }
            
            for(NSDictionary *dict in arrTermsOfService)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"termsOfServiceSysName"]])//
                {
                    dictTermsOfService = dict;
                    [_btnTermsOfServices setTitle:[NSString stringWithFormat:@"%@",[dictTermsOfService valueForKey:@"Title"]] forState:UIControlStateNormal];
                }
            }
        }
        else
        {
            [_btnTermsOfServices setTitle:@"Select Terms Of Services" forState:UIControlStateNormal];
        }
        
        if([[matchesDiscount valueForKey:@"isAgreementValidFor"] isEqualToString:@"true"]||[[matchesDiscount valueForKey:@"isAgreementValidFor"] intValue] == 1)//IsAgreementValidFor
        {
            _txtDays.hidden=NO;
            _lblDays.hidden=NO;
            [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            strIsAgreementValidFor = @"true";
            _txtDays.text = [matchesDiscount valueForKey:@"validFor"];//
        }
        else
        {
            [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            _txtDays.hidden=YES;
            _lblDays.hidden=YES;
            strIsAgreementValidFor = @"false";
        }
        
        if([[matchesDiscount valueForKey:@"isInitialTaxApplicable"] isEqualToString:@"true"]||[[matchesDiscount valueForKey:@"isInitialTaxApplicable"]intValue] == 1)
        {
            // [_btnTaxApplicableInitialPrice setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            [_btnTaxApplicableInitialPriceClarkPest setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        }
        else
        {
            // [_btnTaxApplicableInitialPrice setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            [_btnTaxApplicableInitialPriceClarkPest setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            
        }
        
        if([[matchesDiscount valueForKey:@"isMaintTaxApplicable"] isEqualToString:@"true"] || [[matchesDiscount valueForKey:@"isMaintTaxApplicable"] intValue]==1)
        {
            [_btnTaxApplicableMaintPriceClarkPest setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_btnTaxApplicableMaintPriceClarkPest setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        }
    }
}
-(void)getCoverLetterContent
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCoverLetter=[dictMasters valueForKey:@"LetterTemplateMaster"];
    NSMutableArray *arrSysName,*arrContent;
    arrSysName=[[NSMutableArray alloc]init];
    arrContent=[[NSMutableArray alloc]init];
    
    for (int i=0;i<arrCoverLetter.count; i++)
    {
        NSDictionary *dict=[arrCoverLetter objectAtIndex:i];
        
        [arrSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        [arrContent addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TemplateContent"]]];
        
    }
    dictCoverLetterContentFromSysName = [NSDictionary dictionaryWithObjects:arrContent forKeys:arrSysName];
    
}
-(void)getMultiTerms
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrMultiTerms=[dictMasters valueForKey:@"MultipleGeneralTermsConditions"];
    arrMultipleTermsCondtions = [NSMutableArray new];
    arrSelectedMultipleCondition=[[NSMutableArray alloc]init];
    
    
    if ([arrMultiTerms isKindOfClass:[NSArray class]])
    {
        for(NSDictionary *dict in arrMultiTerms)
        {
            if([[dict valueForKey:@"IsActive"] boolValue]==true || [[dict valueForKey:@"IsActive"] intValue]==1)
            {
                [arrMultipleTermsCondtions addObject:dict];
            }
            //TEMP 22 oCT
            if(([[dict valueForKey:@"IsActive"] boolValue]==true || [[dict valueForKey:@"IsActive"] intValue]==1)&&([[dict valueForKey:@"IsDefault"] boolValue]==true || [[dict valueForKey:@"IsDefault"] intValue]==1))
            {
                [arrSelectedMultipleCondition addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]]];
                [arrSelectedTermsConditionsDescriptions addObject:dict];
            }
        }
        // arrMultipleTermsCondtions=(NSMutableArray*)arrMultiTerms;
    }
    
    
}
-(void)getMultiTermsContionDetails
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrMultiTerms=[dictMasters valueForKey:@"MultipleGeneralTermsConditions"];
    NSMutableArray *arrTitle,*arrId;
    arrTitle=[[NSMutableArray alloc]init];
    arrId=[[NSMutableArray alloc]init];
    for(int i=0;i<arrMultiTerms.count;i++)
    {
        NSDictionary *dict=[arrMultiTerms objectAtIndex:i];
        [arrTitle addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsTitle"]]];
        [arrId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]]];
    }
    dictTitleFromTermId=[NSDictionary dictionaryWithObjects:arrTitle forKeys:arrId];
}
-(void)saveMultiTermsCondition
{
    
    NSMutableArray *arrMultiTermsSave;
    arrMultiTermsSave=[[NSMutableArray alloc]init];
    for (int i=0; i<arrMultipleTermsCondtions.count; i++)
    {
        NSDictionary *dict=[arrMultipleTermsCondtions objectAtIndex:i];
        NSString *strTemrsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
        for (int j=0; j<arrSelectedMultipleCondition.count; j++)
        {
            if ([strTemrsId isEqualToString:[arrSelectedMultipleCondition objectAtIndex:j]])
            {
                [arrMultiTermsSave addObject:dict];
                break;
            }
        }
    }
    
    
    if (arrMultiTermsSave.count>0)
    {
        [self deleteMultipleTermsFromCoreDataClarkPestSalesInfo];
    }
    
    for (int i=0; i<arrMultiTermsSave.count; i++)
    {
        NSDictionary *dict=[arrMultiTermsSave objectAtIndex:i];
        
        entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
        
        LeadCommercialTermsExtDc *objLeadCommercialTermsExtDcc = [[LeadCommercialTermsExtDc alloc]initWithEntity:entityLeadCommercialTermsExtDc insertIntoManagedObjectContext:context];
        objLeadCommercialTermsExtDcc.leadCommercialTermsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
        objLeadCommercialTermsExtDcc.leadId=strLeadId;
        objLeadCommercialTermsExtDcc.branchSysName=[global strEmpBranchSysName];
        objLeadCommercialTermsExtDcc.termsnConditions=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsConditions"]];
        objLeadCommercialTermsExtDcc.userName=strUserName;
        objLeadCommercialTermsExtDcc.companyKey=strCompanyKey;
        NSError *error1;
        [context save:&error1];
    }
    
}
-(void)fetchMultiTermsFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTermsExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrMultiTermsDetail;
    arrMultiTermsDetail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    [self getMultiTerms];
    if(arrMultiTermsDetail.count==0)
    {
        //  [self getMultiTerms];
        
    }
    else
    {
        NSMutableArray *arrayTemp = [NSMutableArray new];
        for(NSManagedObject *initialInfo in arrMultiTermsDetail)
        {
            NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
            NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
            [arrayTemp addObject:dict];
        }
        arrMultipleTermsCondtions=arrayTemp;
        arrSelectedMultipleCondition=[[NSMutableArray alloc]init];
        for (int i=0; i<arrMultipleTermsCondtions.count; i++)
        {
            NSDictionary *dict=[arrMultipleTermsCondtions objectAtIndex:i];
            
            for(NSDictionary *dictTemp in arrayTemp)
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"leadCommercialTermsId"]]] || [[self convertHTML:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsConditions"]]] isEqualToString:[self convertHTML:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"termsnConditions"]]]])
                {
                    [arrSelectedMultipleCondition addObject:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"leadCommercialTermsId"]]];
                }
            }
            
            
        }
        
        // get saved terms conditoins
        for (int i=0; i<arrMultipleTermsCondtions.count; i++)
        {
            NSDictionary *dict=[arrMultipleTermsCondtions objectAtIndex:i];
            
            for(NSDictionary*dictLocal in arrayTemp)
            {
                //if([[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"leadCommercialTermsId"]] isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]]])
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"leadCommercialTermsId"]]] || [[self convertHTML:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsConditions"]]] isEqualToString:[self convertHTML:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"termsnConditions"]]]])//leadCommercialTermsId
                {
                    [arrSelectedTermsConditionsDescriptions addObject:dict];
                }
            }
        }
        
        //        NSArray *arrUniqueMultipleCondition = [[NSSet setWithArray:arrSelectedMultipleCondition] allObjects];
        //        arrSelectedMultipleCondition=[[NSMutableArray alloc]init];
        //        [arrSelectedMultipleCondition addObjectsFromArray:arrUniqueMultipleCondition];
        
        
        
    }
    NSMutableArray *arrayTitleName = [NSMutableArray new];
    for(NSDictionary *dict in arrSelectedTermsConditionsDescriptions)
    {
        [arrayTitleName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsTitle"]]];
    }
    
    if(arrayTitleName.count>0)
    {
        NSString *joinedComponents = [arrayTitleName componentsJoinedByString:@","];
        [_btnTermsConditions setTitle:joinedComponents forState:UIControlStateNormal];
    }
    [self CalculateFinalPrice];
    
}


-(void)deleteMultipleTermsFromCoreDataClarkPestSalesInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    for (NSManagedObject * data in Data)
    {
        
        [context deleteObject:data];
        
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)saveNewAgreementDetails
{
    [self deleteCommercialData];
    if([strIsAgreementValidFor isEqualToString:@"true"])
    {
        if(_txtDays.text.length==0)
        {
            [global AlertMethod:@"Alert!" :@"Please enter days"];
            [_txtDays becomeFirstResponder];
            return;
        }
        else
        {
            [self saveCommercialLetterTemplate:dictCoverLetter introLetter:dictIntroLetter andTermsConditions:dictTermsOfService];
        }
    }
    else
    {
        [self saveCommercialLetterTemplate:dictCoverLetter introLetter:dictIntroLetter andTermsConditions:dictTermsOfService];
    }
    // [self saveMultiTermsCondition];
    [self saveSalesMarketingContent];
}
-(NSAttributedString*)getAttributedString:(NSString *)htmlString
{
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSFontAttributeName:[UIFont systemFontOfSize:25] }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    /*  UIFont *font = [UIFont systemFontOfSize:16];
     NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
     forKey:NSFontAttributeName];
     attributedStringTermsnConditions = [[NSAttributedString alloc] initWithString:strString attributes:attrsDictionary];*/
    
    return attributedString;
}

- (IBAction)actionOnTaxApplicableInitialPrice:(id)sender
{
    if([isTaxApplicableInitialPrice isEqualToString:@"false"])
    {
        [_btnTaxApplicableInitialPriceClarkPest setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        isTaxApplicableInitialPrice = @"true";
    }
    else
    {
        [_btnTaxApplicableInitialPriceClarkPest setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        isTaxApplicableInitialPrice = @"false";
    }
}

- (IBAction)actionOnTaxApplicableMaintPrice:(id)sender
{
    if([isTaxApplicableMaintPrice isEqualToString:@"false"])
    {
        [_btnTaxApplicableMaintPriceClarkPest setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        isTaxApplicableMaintPrice = @"true";
    }
    else
    {
        [_btnTaxApplicableMaintPriceClarkPest setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        isTaxApplicableMaintPrice = @"false";
    }
}
-(void)goToHTMLEditor:(NSString*)strText
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setHtmlContentOnEditing) name:@"Notificationtypefromwhichview" object:nil];
    
    NSUserDefaults *defsHtml = [NSUserDefaults standardUserDefaults];
    [defsHtml setValue:@"" forKey:@"HtmlContent"];
    [defsHtml synchronize];
    
}
-(void)setHtmlContentOnEditing{
    
    NSUserDefaults *defsHtml = [NSUserDefaults standardUserDefaults];
    
    NSString* strHTML = [defsHtml valueForKey:@"HtmlContent"];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [strHTML dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSFontAttributeName:[UIFont systemFontOfSize:80],NSForegroundColorAttributeName:[UIColor redColor] }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    // NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[strHTML dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    
    _txtViewIntroductionLetter.attributedText = attributedString;
    _txtViewIntroductionLetter.font=[UIFont systemFontOfSize:20];
    NSLog(@"Html Code = = = =%@",strHTML);
}

-(NSString*)getHTMLstringFromAttributeString:(NSAttributedString*)strAttribute
{
    NSDictionary *documentAttributes = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
    NSData *htmlData = [strAttribute dataFromRange:NSMakeRange(0, strAttribute.length) documentAttributes:documentAttributes error:NULL];
    //Convert the NSData into HTML String with UTF-8 Encoding
    NSString *htmlString = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    return htmlString;
}

- (IBAction)actionOnInvoice:(id)sender
{
    isEditedInSalesAuto=YES;
    NSLog(@"Global mopdify date set to YES");
    _txtPaidAmountPriceInforamtion.hidden=YES;
    
    _lblTxtPaidAmount.hidden=YES;
    //Checque
    chkChequeClick=NO;
    // _viewCreditCard.hidden=YES;
    // _cnstrntViewCreditCard_H.constant=0;
    _cnstrntViewPriceInfo_H.constant=480-176+45;
    _cnstrntViewCreditCard_H.constant=0;
    [_imgViewCash setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCheck setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCreditCard setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewAutoChangeCustomer setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewCollectAtTimeOfScheduling setImage:[UIImage imageNamed:@"redio_button_1.png"]];
    [_imgViewInvoice setImage:[UIImage imageNamed:@"redio_button_2.png"]];
    
    
    strPaymentMode=@"Invoice";
    _txtChequeValue.hidden=YES;
    _txtLicenseValue.hidden=YES;
    _txtExpirationDate.hidden=YES;
}
#pragma mark- --------- Sales Marketing Content --------------
-(void)getMarketingContent
{
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrTempMarketingContent=[dictMasters valueForKey:@"SalesMarketingContentMaster"];
    
    
    arrMarketingContent = [[NSMutableArray alloc]init];
    arrSelectedMarketingContent=[[NSMutableArray alloc]init];
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
        
    {
        
    }
    if ([arrTempMarketingContent isKindOfClass:[NSArray class]])
    {
        for(NSDictionary *dict in arrTempMarketingContent)
        {
            if([[dict valueForKey:@"IsActive"] boolValue]==true || [[dict valueForKey:@"IsActive"] intValue]==1)
            {
                [arrMarketingContent addObject:dict];
            }
            else
            {
                [arrMarketingContent addObject:dict];
            }
            
        }
    }
}


-(void)saveSalesMarketingContent
{
    NSMutableArray *arrMultiTermsSave;
    arrMultiTermsSave=[[NSMutableArray alloc]init];
    for (int i=0; i<arrMarketingContent.count; i++)
    {
        NSDictionary *dict=[arrMarketingContent objectAtIndex:i];
        NSString *strTemrsId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        for (int j=0; j<arrSelectedMarketingContent.count; j++)
        {
            if ([strTemrsId isEqualToString:[arrSelectedMarketingContent objectAtIndex:j]])
            {
                [arrMultiTermsSave addObject:dict];
                break;
            }
        }
    }
    
    
    if (arrMultiTermsSave.count>0)
    {
        [self deleteSalesMarketingContentClarkPestSalesInfo];
    }
    
    for (int i=0; i<arrMultiTermsSave.count; i++)
    {
        NSDictionary *dict=[arrMultiTermsSave objectAtIndex:i];
        
        entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
        
        LeadMarketingContentExtDc *objLeadMarketingContentExtDc = [[LeadMarketingContentExtDc alloc]initWithEntity:entityLeadMarketingContentExtDc insertIntoManagedObjectContext:context];
        objLeadMarketingContentExtDc.contentSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        
        objLeadMarketingContentExtDc.leadId=strLeadId;
        objLeadMarketingContentExtDc.userName=strUserName;
        objLeadMarketingContentExtDc.companyKey=strCompanyKey;
        NSError *error1;
        [context save:&error1];
    }
    
}
-(void)deleteSalesMarketingContentClarkPestSalesInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    for (NSManagedObject * data in Data)
    {
        
        [context deleteObject:data];
        
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)fetchSalesMarketingContentClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadMarketingContentExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrMultiMarketingContentDetail;
    arrMultiMarketingContentDetail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    [self getMarketingContent];
    
    if(arrMultiMarketingContentDetail.count==0)
    {
    }
    else
    {
        NSMutableArray *arrayTemp = [NSMutableArray new];
        
        for(NSManagedObject *initialInfo in arrMultiMarketingContentDetail)
        {
            NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
            NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
            [arrayTemp addObject:dict];
        }
        
        for (int i=0; i<arrMarketingContent.count; i++)
        {
            NSDictionary *dict=[arrMarketingContent objectAtIndex:i];
            
            for(NSDictionary *dictTemp in arrayTemp)
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"contentSysName"]]])
                {
                    [arrSelectedMarketingContent addObject:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"contentSysName"]]];
                }
            }
        }
    }
    NSMutableArray *arrayTitleName = [[NSMutableArray alloc]init];
    for(NSString *strContetnTitle in arrSelectedMarketingContent)
    {
        [arrayTitleName addObject:[NSString stringWithFormat:@"%@",[dictSalesContentTitleFromSysName valueForKey:strContetnTitle]]];
    }
    
    if(arrayTitleName.count>0)
    {
        NSString *joinedComponents = [arrayTitleName componentsJoinedByString:@","];
        [_btnMarketingContent setTitle:joinedComponents forState:UIControlStateNormal];
    }
}
- (IBAction)actionOnMarketingContent:(id)sender
{
    [self endEditing];
    if([arrMarketingContent count]==0 || ![arrMarketingContent isKindOfClass:[NSArray class]])
    {
        [global AlertMethod:@"Alert!" :@"No data available"];
    }
    else
    {
        tblData.tag=14;
        [self tableLoad:tblData.tag];
    }
}
-(BOOL)checkForCustomerSignature
{
    BOOL chkCustomerSignPresent;
    chkCustomerSignPresent=NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObCheckCustomerSign = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesCheckCustomerSign;
    if (arrAllObCheckCustomerSign.count==0)
    {
        //[self savePaymentInfo];
    }
    else
    {
        for (int k=0; k<arrAllObCheckCustomerSign.count; k++)
        {
            matchesCheckCustomerSign=arrAllObCheckCustomerSign[k];
            
            NSLog(@"Lead IDDDD====%@",[matchesCheckCustomerSign valueForKey:@"leadId"]);
            
            signCustomer=[NSString stringWithFormat:@"%@",[matchesCheckCustomerSign valueForKey:@"customerSignature"]];
            
            if (signCustomer.length==0 || [signCustomer isEqualToString:@""])
            {
                chkCustomerSignPresent=NO;
                
                if (chkCustomerNotPresent==YES || [strSummarySendPropsal isEqualToString:@"strSummarySendPropsal"])
                {
                    chkCustomerSignPresent=YES;
                }
            }
            else
            {
                chkCustomerSignPresent=YES;
            }
            
            
        }
    }
    
    return chkCustomerSignPresent;
}
-(void)hideToCofigure
{
    _const_ViewAppliedCredit_H.constant = 0;
    _const_ViewCouponCredit_H.constant = 0;
    _const_ViewNewInitialMaintPrice_H.constant = 0;
    _cnstrntViewPriceInfo_H.constant  = 0;
    _const_PriceTerms_H.constant = 0;
    _cnstrntViewTermsCondition_H.constant = 0;
    _const_LastView_H.constant = 0;
}
-(void)goToAgreement
{
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPhone" bundle:nil];
    ClarkPestSalesAgreementProposaliPhone *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"ClarkPestSalesAgreementProposaliPhone"];
    objSalesAutomationAgreementProposal.strFromSummary=@"FromSummary";
    [self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:YES];
}
-(void)deleteCommercialData
{
    //Delete LeadCommercialDetailExtDc Data
    
    entityLeadCommercialDetailExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    
    NSFetchRequest *allDataLeadCommercialDetailExtDc = [[NSFetchRequest alloc] init];
    [allDataLeadCommercialDetailExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context]];
    
    NSPredicate *predicateLeadCommercialDetailExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    
    [allDataLeadCommercialDetailExtDc setPredicate:predicateLeadCommercialDetailExtDc];
    [allDataLeadCommercialDetailExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorLeadCommercialDetailExtDc= nil;
    NSArray * DataLeadCommercialDetailExtDc = [context executeFetchRequest:allDataLeadCommercialDetailExtDc error:&errorLeadCommercialDetailExtDc];
    //error handling goes here
    for (NSManagedObject * data in DataLeadCommercialDetailExtDc) {
        [context deleteObject:data];
    }
    NSError *saveErrorLeadCommercialDetailExtDc = nil;
    [context save:&saveErrorLeadCommercialDetailExtDc];
    
    //......................................................
    
    //Delete LeadMarketingContentExtDc Data
    
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    
    NSFetchRequest *allDataLeadMarketingContentExtDc = [[NSFetchRequest alloc] init];
    [allDataLeadMarketingContentExtDc setEntity:[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context]];
    
    NSPredicate *predicateLeadMarketingContentExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    
    [allDataLeadMarketingContentExtDc setPredicate:predicateLeadMarketingContentExtDc];
    [allDataLeadMarketingContentExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorLeadMarketingContentExtDc= nil;
    NSArray * DataLeadMarketingContentExtDc = [context executeFetchRequest:allDataLeadMarketingContentExtDc error:&errorLeadMarketingContentExtDc];
    //error handling goes here
    for (NSManagedObject * data in DataLeadMarketingContentExtDc) {
        [context deleteObject:data];
    }
    NSError *saveErrorLeadMarketingContentExtDc = nil;
    [context save:&saveErrorLeadMarketingContentExtDc];
    
    //......................................................
}
- (IBAction)actionOnCellNo:(id)sender
{
    if (_lblCellNo.text.length>0)
    {
        [global calling:_lblCellNo.text];
        
    }
}
//Nilind 08 OCT 2019
-(NSMutableAttributedString*)getUpdateAttributedString : (NSMutableAttributedString*) strAttributedText stringToReplace: (NSString *)stringToReplace stringWithReplace:(NSString*)strWithReplace
{
    NSRange range = [strAttributedText.string rangeOfString:stringToReplace];
    
    [strAttributedText replaceCharactersInRange:range withString:strWithReplace];
    
    return strAttributedText;
}
-(NSMutableString*)getUpdateHtmlString : (NSMutableString*) strMutableText stringToReplace: (NSString *)stringToReplace stringWithReplace:(NSString*)strWithReplace
{
    NSRange range = [strMutableText rangeOfString:stringToReplace];
    
    if(range.location == NSNotFound)
    {
        
    }
    else
    {
        @try
        {
            [strMutableText replaceCharactersInRange:range withString:strWithReplace];
        }
        @catch (NSException *exception)
        {
            
        }
        @finally
        {
            
        }
    }
    
    return strMutableText;
}

#pragma mark: ----------- For WebView ---------------

-(void)fetchLetterTemplateFromCoreDataWebView
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrLetterTemplate = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    if(arrLetterTemplate.count>0)
    {
        matchesDiscount = [arrLetterTemplate firstObject];
        
        if([[matchesDiscount valueForKey:@"coverLetterSysName"] length]>0)
        {
            for(NSDictionary *dict in arrCoverLetter)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"coverLetterSysName"]])
                {
                    dictCoverLetter = dict;
                    [_btnCoverLetter setTitle:[NSString stringWithFormat:@"%@",[dictCoverLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
                    break;
                }
            }
            
            
            
            
            NSMutableString *strText;
            
            strText =[[NSMutableString alloc]initWithString:[dictCoverLetterContentFromSysName valueForKey:[matchesDiscount valueForKey:@"coverLetterSysName"]]];
            
            
            NSMutableString *strMutableText = [self getUpdateHtmlString:strText stringToReplace:@"[Logo]" stringWithReplace:strCompanyLogoPath]; //strCompanyLogoPath
            
            NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
            
            for(int i=0; i<4  ;i++)
            {
                
                NSDictionary *dictInspector = [defs valueForKey:@"inspectorDetail"];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Logo]" stringWithReplace:strCompanyLogoPath];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BranchLogo]" stringWithReplace:strBranchLogoImagePath];
                
                //Custom Tag
                
                NSString *strCustomTagBranchLogoImage;
                strCustomTagBranchLogoImage = [NSString stringWithFormat:@"<img src=%@ style = max-width:170px  />",strBranchLogoImagePath];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BranchLogoImage]" stringWithReplace:strCustomTagBranchLogoImage];
                
                //End
                
                
                // custom tag
                
                NSString *strCustomTag;
                strCustomTag = [NSString stringWithFormat:@"<img src=%@ style = max-width:170px  />",strCustomerCompanyProfileImage];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyLogoImage]" stringWithReplace:strCustomTag];
                
                //end
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyLogo]" stringWithReplace:strCustomerCompanyProfileImage];//strCompanyLogoPath
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceDateTime]" stringWithReplace:[global getCurrentDate]];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CurrentDate]" stringWithReplace:[global getCurrentDate]];
                
                
                //strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyName]" stringWithReplace:strUserName];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyName]" stringWithReplace:_lblNameTitle.text];
                //_lblNameTitle.text
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceAddress]" stringWithReplace:_lblServiceAddressValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceAddressName]" stringWithReplace:_lblServiceAddressValue.text];
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[InspectorEmail]" stringWithReplace:[NSString stringWithFormat:@"%@",[dictInspector valueForKey:@"PrimaryEmail"]]];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BillingAddress]" stringWithReplace:_lblBillingAddressValue.text];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BillingAddressName]" stringWithReplace:_lblBillingAddressValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountNo]" stringWithReplace:strAccountNoGlobal];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[LeadNo]" stringWithReplace:strLeadNumber];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[InspectorPrimaryPhone]" stringWithReplace:[NSString stringWithFormat:@"%@",[dictInspector valueForKey:@"PrimaryPhone"]]];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Technician]" stringWithReplace:strTechnicianName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerName]" stringWithReplace:strAccountManagerName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerEmail]" stringWithReplace:strAccountManagerEmail];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CellPhone]" stringWithReplace:_lblCellNo.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CellNumber]" stringWithReplace:_lblCellNo.text];
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[OpportunityContactName]" stringWithReplace:strCustomerName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Licenseno]" stringWithReplace:strEmployeeLicenseNo];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[PrimaryPhone]" stringWithReplace:_lblPrimaryPhoneValue.text];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerPrimaryPhone]" stringWithReplace:strAccountManagerPrimaryPhone];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CompanyAddress]" stringWithReplace:strCompanyAddress]; //[CompanyAddress]
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BillingPOC]" stringWithReplace:strBillingPocName]; //[CompanyAddress]
            }
            
            // _txtViewCoverLetterForAgreement.text = strMutableText;
            
            webView1    = [[ UIWebView alloc]initWithFrame:CGRectMake(0, 5, _viewCoverLetterWebView.frame.size.width, 10)];
            webView1.scalesPageToFit=YES;
            webView1.delegate=self;
            webView1.autoresizesSubviews = YES;
            
            
            
            //[_webViewCoverLetter loadHTMLString:strMutableText baseURL: [[NSBundle mainBundle] bundleURL]];
            [_viewCoverLetterWebView addSubview:webView1];
            strmutableTextCoverLetter = strMutableText;
            
            //[webView1 loadHTMLString:strMutableText baseURL: [[NSBundle mainBundle] bundleURL]];
            [self performSelector:@selector(loadCoverLetter) withObject:nil afterDelay:1.0 ];
            
            
            
            
            //[self textViewContentHeightUpdateForCoverLetter];
            
            
        }
        else
        {
            [_btnCoverLetter setTitle:@"Select Cover Letter" forState:UIControlStateNormal];
            _const_ViewCoverLetter_H.constant = 0;
        }
        
        if([[matchesDiscount valueForKey:@"introSysName"] length]>0)
        {
            //            [_btnIntroductionLetter setTitle:[matchesDiscount valueForKey:@"introSysName"] forState:UIControlStateNormal];
            
            if([[matchesDiscount valueForKey:@"introContent"] length]>0)
            {
                // _txtViewIntroductionLetter.text = [self convertHTML:[matchesDiscount valueForKey:@"introContent"]];
                _txtViewIntroductionLetter.attributedText = [self getAttributedString:[matchesDiscount valueForKey:@"introContent"]];
            }
            
            for(NSDictionary *dict in arrIntroductionLetter)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"introSysName"]])
                {
                    dictIntroLetter = dict;
                    [_btnIntroductionLetter setTitle:[NSString stringWithFormat:@"%@",[dictIntroLetter valueForKey:@"TemplateName"]] forState:UIControlStateNormal];
                    
                    break;
                }
            }
            
            
            NSMutableString *strText;
            
            //[matchesDiscount valueForKey:@"introContent"]
            
            strText = [[NSMutableString alloc]initWithString:[matchesDiscount valueForKey:@"introContent"]];
            
            
            NSMutableString *strMutableText = [self getUpdateHtmlString:strText stringToReplace:@"[Logo]" stringWithReplace:strCompanyLogoPath]; //strCompanyLogoPath
            
            NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
            
            for(int i=0; i<4  ;i++)
            {
                
                NSDictionary *dictInspector = [defs valueForKey:@"inspectorDetail"];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BranchLogo]" stringWithReplace:strBranchLogoImagePath];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Logo]" stringWithReplace:strCompanyLogoPath];
                
                //Custom Tag
                
                NSString *strCustomTagBranchLogoImage;
                strCustomTagBranchLogoImage = [NSString stringWithFormat:@"<img src=%@ style = max-width:170px  />",strBranchLogoImagePath];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BranchLogoImage]" stringWithReplace:strCustomTagBranchLogoImage];
                
                //End
                
                
                // custom tag
                
                NSString *strCustomTag;
                strCustomTag = [NSString stringWithFormat:@"<img src=%@ style = max-width:170px  />",strCustomerCompanyProfileImage];
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyLogoImage]" stringWithReplace:strCustomTag];
                //end
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyLogo]" stringWithReplace:strCustomerCompanyProfileImage];//strCompanyLogoPath
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceDateTime]" stringWithReplace:[global getCurrentDate]];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CurrentDate]" stringWithReplace:[global getCurrentDate]];
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyName]" stringWithReplace:_lblNameTitle.text];//strusername
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceAddress]" stringWithReplace:_lblServiceAddressValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceAddressName]" stringWithReplace:_lblServiceAddressValue.text];
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[InspectorEmail]" stringWithReplace:[NSString stringWithFormat:@"%@",[dictInspector valueForKey:@"PrimaryEmail"]]];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BillingAddress]" stringWithReplace:_lblBillingAddressValue.text];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BillingAddressName]" stringWithReplace:_lblBillingAddressValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountNo]" stringWithReplace:strAccountNoGlobal];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[LeadNo]" stringWithReplace:strLeadNumber];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[InspectorPrimaryPhone]" stringWithReplace:[NSString stringWithFormat:@"%@",[dictInspector valueForKey:@"PrimaryPhone"]]];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Technician]" stringWithReplace:strTechnicianName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerName]" stringWithReplace:strAccountManagerName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerEmail]" stringWithReplace:strAccountManagerEmail];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CellPhone]" stringWithReplace:_lblCellNo.text];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CellNumber]" stringWithReplace:_lblCellNo.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[OpportunityContactName]" stringWithReplace:strCustomerName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Licenseno]" stringWithReplace:strEmployeeLicenseNo];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[PrimaryPhone]" stringWithReplace:_lblPrimaryPhoneValue.text];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerPrimaryPhone]" stringWithReplace:strAccountManagerPrimaryPhone];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CompanyAddress]" stringWithReplace:strCompanyAddress]; //[CompanyAddress]
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BillingPOC]" stringWithReplace:strBillingPocName]; //[CompanyAddress]
                
                
                
            }
            
            
            
            //_webViewCoverLetter.hidden=YES;
            //_webViewIntroductionLetter.hidden =YES;
            
            webView2    = [[ UIWebView alloc]initWithFrame:CGRectMake(0, 5, _viewIntroLetterWebView.frame.size.width, 10)];
            // webView2.scalesPageToFit=YES;
            
            webView2.delegate=self;
            webView2.scalesPageToFit = YES;
            webView2.autoresizesSubviews = YES;
            [_viewIntroLetterWebView addSubview:webView2];
            
            strMutableTextIntroLetter =strMutableText;
            
            // [webView2 loadHTMLString:strMutableText baseURL: [[NSBundle mainBundle] bundleURL]];
            
            [self performSelector:@selector(loadIntroLetter) withObject:nil afterDelay:1.0 ];
            
            
            //  [self textViewContentHeightUpdateForIntroductionLetter];
            
            
            
        }
        else
        {
            [_btnIntroductionLetter setTitle:@"Select Introduction Letter" forState:UIControlStateNormal];
            _const_ViewIntroductionLetter_H.constant = 0;
            
        }
        //[self textViewContentHeightUpdateForIntroductionLetter];
        
        if([[matchesDiscount valueForKey:@"termsOfServiceSysName"] length]>0)
        {
            // [_btnTermsOfServices setTitle:[matchesDiscount valueForKey:@"termsOfServiceSysName"] forState:UIControlStateNormal];
            if([[matchesDiscount valueForKey:@"termsOfServiceContent"] length]>0)
            {
                _txtViewTermsServices.attributedText = [self getAttributedString:[matchesDiscount valueForKey:@"termsOfServiceContent"]];
            }
            
            for(NSDictionary *dict in arrTermsOfService)
            {
                if([[dict valueForKey:@"SysName"] isEqualToString:[matchesDiscount valueForKey:@"termsOfServiceSysName"]])//
                {
                    dictTermsOfService = dict;
                    [_btnTermsOfServices setTitle:[NSString stringWithFormat:@"%@",[dictTermsOfService valueForKey:@"Title"]] forState:UIControlStateNormal];
                }
            }
            
            
            
            NSMutableString *strText;
            
            //[matchesDiscount valueForKey:@"introContent"]
            
            strText = [[NSMutableString alloc]initWithString:[matchesDiscount valueForKey:@"termsOfServiceContent"]];
            
            
            NSMutableString *strMutableText = [self getUpdateHtmlString:strText stringToReplace:@"[Logo]" stringWithReplace:strCompanyLogoPath]; //strCompanyLogoPath
            
            for(int i=0; i<4  ;i++)
            {
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BranchLogo]" stringWithReplace:strBranchLogoImagePath];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Logo]" stringWithReplace:strCompanyLogoPath];
                
                //Custom Tag
                
                NSString *strCustomTagBranchLogoImage;
                strCustomTagBranchLogoImage = [NSString stringWithFormat:@"<img src=%@ style = max-width:170px  />",strBranchLogoImagePath];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BranchLogoImage]" stringWithReplace:strCustomTagBranchLogoImage];
                
                //End
                
                
                // custom tag
                
                NSString *strCustomTag;
                strCustomTag = [NSString stringWithFormat:@"<img src=%@ style = max-width:170px  />",strCustomerCompanyProfileImage];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyLogoImage]" stringWithReplace:strCustomTag];
                //end
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyLogo]" stringWithReplace:strCustomerCompanyProfileImage];//strCompanyLogoPath
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceDateTime]" stringWithReplace:[global getCurrentDate]];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CurrentDate]" stringWithReplace:[global getCurrentDate]];
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CustomerCompanyName]" stringWithReplace:_lblNameTitle.text];//strusername
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceAddress]" stringWithReplace:_lblServiceAddressValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[ServiceAddressName]" stringWithReplace:_lblServiceAddressValue.text];
                
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[InspectorEmail]" stringWithReplace:_lblEmailIdValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BillingAddress]" stringWithReplace:_lblBillingAddressValue.text];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[BillingAddressName]" stringWithReplace:_lblBillingAddressValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountNo]" stringWithReplace:strAccountNoGlobal];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[LeadNo]" stringWithReplace:strLeadNumber];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[InspectorPrimaryPhone]" stringWithReplace:_lblPrimaryPhoneValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Technician]" stringWithReplace:strTechnicianName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerName]" stringWithReplace:strUserName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerEmail]" stringWithReplace:_lblEmailIdValue.text];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[CellPhone]" stringWithReplace:_lblPrimaryPhoneValue.text];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[OpportunityContactName]" stringWithReplace:strCustomerName];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[Licenseno]" stringWithReplace:strEmployeeLicenseNo];
                
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[PrimaryPhone]" stringWithReplace:strUserName];
                strMutableText = [self getUpdateHtmlString:strMutableText stringToReplace:@"[AccountManagerPrimaryPhone]" stringWithReplace:strUserName];
                
                
                
                
            }
            
            
            
            
            webView4    = [[ UIWebView alloc]initWithFrame:CGRectMake(0, 5, _viewTermsOfServiceWebView.frame.size.width, 10)];
            webView4.delegate=self;
            webView4.scalesPageToFit = YES;
            webView4.autoresizesSubviews = YES;
            [_viewTermsOfServiceWebView addSubview:webView4];
            
            strMutableTextTermsOfService =strMutableText;
            
            // [webView2 loadHTMLString:strMutableText baseURL: [[NSBundle mainBundle] bundleURL]];
            
            [self performSelector:@selector(loadTermsOfService) withObject:nil afterDelay:1.0 ];
            
            
            
        }
        else
        {
            [_btnTermsOfServices setTitle:@"Select Terms Of Services" forState:UIControlStateNormal];
            _const_ViewTermsOfService_H.constant = 0;
        }
        
        if([[matchesDiscount valueForKey:@"isAgreementValidFor"] isEqualToString:@"true"]||[[matchesDiscount valueForKey:@"isAgreementValidFor"] intValue] == 1)//IsAgreementValidFor
        {
            _txtDays.hidden=NO;
            _lblDays.hidden=NO;
            [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_2New.png"] forState:UIControlStateNormal];
            strIsAgreementValidFor = @"true";
            _txtDays.text = [matchesDiscount valueForKey:@"validFor"];//
        }
        else
        {
            [_btnAgreementValidFor setImage:[UIImage imageNamed:@"check_box_1New.png"] forState:UIControlStateNormal];
            _txtDays.hidden=YES;
            _lblDays.hidden=YES;
            strIsAgreementValidFor = @"false";
        }
        
        if([[matchesDiscount valueForKey:@"isInitialTaxApplicable"] isEqualToString:@"true"]||[[matchesDiscount valueForKey:@"isInitialTaxApplicable"]intValue] == 1)
        {
            [_btnTaxApplicableInitialPriceClarkPest  setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_btnTaxApplicableInitialPriceClarkPest  setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        }
        
        if([[matchesDiscount valueForKey:@"isMaintTaxApplicable"] isEqualToString:@"true"] || [[matchesDiscount valueForKey:@"isMaintTaxApplicable"] intValue]==1)
        {
            [_btnTaxApplicableMaintPriceClarkPest  setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_btnTaxApplicableMaintPriceClarkPest  setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        }
        
        
    }
}
-(void)fetchSalesMarketingContentClarkPestWebView
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadMarketingContentExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrMultiMarketingContentDetail;
    arrMultiMarketingContentDetail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    [self getMarketingContent];
    
    if(arrMultiMarketingContentDetail.count==0)
    {
    }
    else
    {
        NSMutableArray *arrayTemp = [NSMutableArray new];
        
        for(NSManagedObject *initialInfo in arrMultiMarketingContentDetail)
        {
            NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
            NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
            [arrayTemp addObject:dict];
        }
        
        for (int i=0; i<arrMarketingContent.count; i++)
        {
            NSDictionary *dict=[arrMarketingContent objectAtIndex:i];
            
            for(NSDictionary *dictTemp in arrayTemp)
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"contentSysName"]]])
                {
                    [arrSelectedMarketingContent addObject:[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"contentSysName"]]];
                }
            }
        }
    }
    NSMutableArray *arrayTitleName = [[NSMutableArray alloc]init];
    for(NSString *strContetnTitle in arrSelectedMarketingContent)
    {
        [arrayTitleName addObject:[NSString stringWithFormat:@"%@",[dictSalesContentTitleFromSysName valueForKey:strContetnTitle]]];
    }
    
    if(arrayTitleName.count>0)
    {
        NSString *joinedComponents = [arrayTitleName componentsJoinedByString:@","];
        [_btnMarketingContent setTitle:joinedComponents forState:UIControlStateNormal];
        
        NSMutableString *strMarkeingContent =[[NSMutableString alloc]initWithString:@""];
        for (int i=0; i<arrSelectedMarketingContent.count; i++)
        {
            [strMarkeingContent appendString:[dictSalesMarketingDescFromSysname valueForKey:[arrSelectedMarketingContent objectAtIndex:i]]];
            // [strMarkeingContent appendString:@"\n-------------------------------------------------------------------------------------------------------------------------------\n "];
            NSString *strPer =@"100%";
            
            NSString *strWidth = [NSString stringWithFormat:@"<HR WIDTH=%@ SIZE=3 NOSHADE>",strPer];
            
            if(i==arrSelectedMarketingContent.count-1)
            {
                
            }
            else
            {
                [strMarkeingContent appendString:strWidth];
            }
            
        }
        
        webView3    = [[ UIWebView alloc]initWithFrame:CGRectMake(0, 5, _viewSalesMarketingContentWebView.frame.size.width, 10)];
        webView3.delegate=self;
        webView3.scalesPageToFit = YES;
        webView3.autoresizesSubviews = YES;
        [_viewSalesMarketingContentWebView addSubview:webView3];
        [webView3 loadHTMLString:strMarkeingContent baseURL: [[NSBundle mainBundle] bundleURL]];
        
    }
    else
    {
        _const_SalesMarketingContent_H.constant = 0;
    }
}

-(void)loadCoverLetter
{
    [webView1 loadHTMLString:strmutableTextCoverLetter baseURL: [[NSBundle mainBundle] bundleURL]];
    
}
-(void)loadIntroLetter
{
    [webView2 loadHTMLString:strMutableTextIntroLetter baseURL: [[NSBundle mainBundle] bundleURL]];
}

-(void)loadMarketingContent
{
    [webView3 loadHTMLString:strMutableTextMarketingContent baseURL: [[NSBundle mainBundle] bundleURL]];
}
-(void)loadTermsOfService
{
    [webView4 loadHTMLString:strMutableTextTermsOfService baseURL: [[NSBundle mainBundle] bundleURL]];
}

#pragma mark - ------Web View Delegate Methods ------------

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"StartLoadingh");
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"ErrorLoadingh");
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"FinishLoadingh");
    
    if(webView == webView1)//_webViewCoverLetter)
    {
        
        _const_ViewCoverLetter_H.constant = 100;//180;
        
        CGRect frame = webView1.frame;
        frame.size.height = 1;
        CGSize fittingSize = [webView1 sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView1.frame = frame;
        
        NSLog(@"In delegate size: %f, %f", fittingSize.width, fittingSize.height);
        
        //webView.frame = CGRectMake(0, 0,webView.frame.size.width, fittingSize.height+100);
        webView1.frame = CGRectMake(0, 0,_viewCoverLetter.frame.size.width, fittingSize.height+100);
        
        
        _const_ViewCoverLetter_H.constant = 50-50+15+ webView1.frame.size.height+50;
        
    }
    else if(webView == webView2)// _webViewIntroductionLetter)
    {
        
        _const_ViewIntroductionLetter_H.constant = 100;//180;
        CGRect frame = webView2.frame;
        frame.size.height = 1;
        webView2.frame = frame;
        CGSize fittingSize = [webView2 sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView2.frame = frame;
        
        NSLog(@"In delegate size: %f, %f", fittingSize.width, fittingSize.height);
        
        webView2.frame = CGRectMake(0, 0,_viewIntroductionLetter.frame.size.width, fittingSize.height+100);
        _const_ViewIntroductionLetter_H.constant =50-50+15+webView2.frame.size.height+50;
    }
    else if(webView == webView3)// _webViewIntroductionLetter)
    {
        
        _const_SalesMarketingContent_H.constant = 100;//180;
        
        CGRect frame = webView3.frame;
        frame.size.height = 1;
        webView3.frame = frame;
        CGSize fittingSize = [webView3 sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView3.frame = frame;
        
        NSLog(@"In delegate size: %f, %f", fittingSize.width, fittingSize.height);
        
        webView3.frame = CGRectMake(0, 0,_viewForSalesMarketingContent.frame.size.width, fittingSize.height+100);
        
        _const_SalesMarketingContent_H.constant = 50-50+15+webView3.frame.size.height+50;
    }
    else if(webView == webView4) // TERMS OF SERVICE
    {
        
        _const_ViewTermsOfService_H.constant = 100;//180;
        
        
        CGRect frame = webView.frame;
        frame.size.height = 1;
        webView.frame = frame;
        CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView.frame = frame;
        
        NSLog(@"In delegate size: %f, %f", fittingSize.width, fittingSize.height);
        
        webView.frame = CGRectMake(0, 0,webView.frame.size.width, fittingSize.height+100);
        
        _const_ViewTermsOfService_H.constant = 50-50+15+webView.frame.size.height+50;
    }
    
    
    
}

- (void)loadWebView {
    
    // ** GET BUNDLE-PATH TO BUILD THE 'baseURL' **
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:bundlePath];
    
    // ** GET PATH-STRING OF 'index.html' FILE **
    NSString *filePath = [NSBundle pathForResource:@"htmltest" ofType:@"html" inDirectory:bundlePath];
    
    // ** GET HTML-STRING OF FILE **
    NSString* htmlString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    // ** START LOADING WEBVIEW WITH 'htmlString' AND 'baseURL' **
    //[_webViewCoverLetter loadHTMLString:htmlString baseURL:baseURL];
}

#pragma mark: --------- Final Price Calculation -------------

-(void)CalculateFinalPrice
{
    [self calculateInitialPriceMaintPriceStandardClarkPest];
    [self calculateInitialPriceMaintPriceNonStandardClarkPest];
    
    [self calculateTaxCommercial];
    
    _lblSubTotalAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",subtotalInitialPriceClarkPest];
    
    _lblSubtotalAmountMaintenancePriceClarkPest.text = [NSString stringWithFormat:@"%.2f",subTotalMaintPriceClarkPest];
    
    float totalInitialPrice = 0.0;
    float otherDiscountInitialPrice = 0.0;
    float tipDiscountInitialPrice = 0.0;
    float billingAmountInitialPrice = 0.0;
    float leadInspectionFee = 0.0;
    
    otherDiscountInitialPrice = [_txtOtherDiscountClarkPest.text floatValue];
    if (otherDiscountInitialPrice < 0)
    {
        otherDiscountInitialPrice = 0.0;
    }
    
    tipDiscountInitialPrice = [_txtTipDiscountClarkPest.text floatValue];
    if (tipDiscountInitialPrice < 0)
    {
        tipDiscountInitialPrice = 0.0;
    }
    
    leadInspectionFee = [_txtLeadInspectionFee.text floatValue];
    if (leadInspectionFee < 0)
    {
        leadInspectionFee = 0.0;
    }
    
    //Tax Calculation
    
    float discountPerInitial = 0.0;
    float totalDiscount = 0.0;
    totalDiscount = otherDiscountInitialPrice  + tipDiscountInitialPrice ;
    
    discountPerInitial = (totalDiscount / (subtotalInitialPriceClarkPest + leadInspectionFee)) * 100;
    
    if (isnan(discountPerInitial))
    {
        discountPerInitial = 0;
    }
    
    if (discountPerInitial < 0)
    {
        discountPerInitial = 0.0;
    }
    
    float totalTaxableAmountInitial = 0.0;
    float totalTaxableAmountMaint = 0.0;
    float amountAfterDiscountInitial = 0.0;
    
    totalInitialCommercialTax = totalInitialCommercialTax +  leadInspectionFee;
    
    amountAfterDiscountInitial = totalInitialCommercialTax - (totalInitialCommercialTax * discountPerInitial/100);
    
    totalTaxableAmountInitial = totalTaxableAmountInitial + amountAfterDiscountInitial;
    
    if(strTax.length==0|| [strTax isEqualToString:@""] || [strTax floatValue] == 0)
    {
        strTax=@"0.0";
    }
    
    float taxAmountInitial = (totalTaxableAmountInitial * [strTax floatValue]) / 100;
    
    
    _lblTaxAmountInitialPrice.text = [NSString stringWithFormat:@"%.2f",taxAmountInitial];
    
    //End
    
    
    totalInitialPrice = subtotalInitialPriceClarkPest - otherDiscountInitialPrice -tipDiscountInitialPrice + leadInspectionFee;
    
    if (totalInitialPrice < 0)
    {
        totalInitialPrice = 0.0;
    }
    
    _lblTotalPriceInitialClarkPest.text = [NSString stringWithFormat:@"%.2f",totalInitialPrice];
    
    _lblTotalBillingAmountInitialPriceClarkPest.text = [NSString stringWithFormat:@"%.2f",(totalInitialPrice + taxAmountInitial)];
    
    
    //For Maint Price
    
    
    //Tax Maint
    
    totalTaxableAmountMaint = totalMaintCommercialTax;
    
    float taxAmountMaint = (totalTaxableAmountMaint * [strTax floatValue]) / 100;
    _lblTaxAmountMaintPrice.text = [NSString stringWithFormat:@"%.2f",taxAmountMaint];
    
    //End
    
    float totalDueAmountMaintPrice = 0.0;
    
    totalDueAmountMaintPrice = subTotalMaintPriceClarkPest + taxAmountMaint;
    
    if (totalDueAmountMaintPrice < 0)
    {
        totalDueAmountMaintPrice = 0.0;
    }
    
    _lblTotalDueMaintenanceAmountClarkPest.text = [NSString stringWithFormat:@"%.2f",totalDueAmountMaintPrice];
    
}

- (IBAction)actionOnTaxApplicableInitialPriceClarkPest:(id)sender
{
    [self endEditing];
    if([isTaxApplicableInitialPrice isEqualToString:@"false"])
    {
        //[_btnTaxApplicableInitialPrice setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [_btnTaxApplicableInitialPriceClarkPest setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
        isTaxApplicableInitialPrice = @"true";
    }
    else
    {
        [_btnTaxApplicableInitialPriceClarkPest setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        isTaxApplicableInitialPrice = @"false";
    }
}
- (IBAction)actionOnTaxAplpicableMaintPriceClarkPest:(id)sender
{
    [self endEditing];
    if([isTaxApplicableMaintPrice isEqualToString:@"false"])
    {
        [_btnTaxApplicableMaintPriceClarkPest
         setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        isTaxApplicableMaintPrice = @"true";
    }
    else
    {
        [_btnTaxApplicableMaintPriceClarkPest setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        isTaxApplicableMaintPrice = @"false";
    }
}
-(void)calculateInitialPriceMaintPriceStandardClarkPest
{
    arrInitialPriceCommercial = [[NSMutableArray alloc]init];
    arrMaintPriceCommercial = [[NSMutableArray alloc]init];
    
    subtotalInitialPriceClarkPest = 0.0;
    subTotalMaintPriceClarkPest = 0.0;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arry.count>0)
    {
        
        for(NSDictionary *dict in arry)
        {
            subtotalInitialPriceClarkPest = subtotalInitialPriceClarkPest +[[NSString stringWithFormat:@"%@",[dict valueForKey:@"initialPrice"]]floatValue];
            subTotalMaintPriceClarkPest = subTotalMaintPriceClarkPest +[[NSString stringWithFormat:@"%@",[dict valueForKey:@"maintenancePrice"]]floatValue];
            
            [arrInitialPriceCommercial addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"totalInitialPrice"]]];
            [arrMaintPriceCommercial addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"totalMaintPrice"]]];
            
        }
        
    }
    else
    {
    }
    
    
}
-(void)calculateInitialPriceMaintPriceNonStandardClarkPest
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate ;
    
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    if (arry.count>0)
    {
        for(NSDictionary *dict in arry)
        {
            subtotalInitialPriceClarkPest = subtotalInitialPriceClarkPest +[[NSString stringWithFormat:@"%@",[dict valueForKey:@"initialPrice"]]floatValue];
            subTotalMaintPriceClarkPest = subTotalMaintPriceClarkPest +[[NSString stringWithFormat:@"%@",[dict valueForKey:@"maintenancePrice"]]floatValue];
            
            
            [arrInitialPriceCommercial addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"initialPrice"]]];
            [arrMaintPriceCommercial addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"maintenancePrice"]]];
            
        }
    }
    else
    {
        
    }
}

-(NSMutableArray *)fetchTargetImageDetail:(NSString *)strLeadCommercialTargetId MobileTargetId:(NSString *)strMobileTargetId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    
    if (strLeadCommercialTargetId.length > 0)
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && leadCommercialTargetId=%@" ,strLeadId, strLeadCommercialTargetId];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && mobileTargetId=%@",strLeadId,strMobileTargetId];
    }
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
                // [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesTargetImage entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictPaymentInfo];
            NSLog(@"TargetImageDetailImageDetail%@",arrImageDetail);
        }
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    return arrImageDetail;
}

#pragma mark: ------------------- Target Image Download ----------------

-(UIImage *)downloadTargetImage :(NSString *)strTargetImage
{
    
    NSString *str = strTargetImage; //@"Img275643Target201912411575.jpg";
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    NSString *strUrl;
    strUrl = [NSString stringWithFormat:@"%@/Documents/Targetimages/%@",strServiceUrlMainServiceAutomation,str];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists)
    {
        return [self ShowFirstTargetImage:str];
    }
    else
    {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
        UIImage *image = [UIImage imageWithData:photoData];
        return [self saveTargetImageAfterDownload:image :result];
        
    }
    
}
-(UIImage *)ShowFirstTargetImage :(NSString*)str
{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    return [self loadTargetImage:result];
}
- (UIImage *)saveTargetImageAfterDownload: (UIImage*)image :(NSString*)name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    return [self ShowFirstTargetImage:name];
    
}
- (UIImage*)loadTargetImage :(NSString*)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}
-(void)endEditing
{
    [self.view endEditing:YES];
}
-(void)goToGlobalmage:(NSString *)strType
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"PestiPhone" bundle:nil];
    GlobalImageVC *objGlobalImageVC=[storyBoard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objGlobalImageVC.strHeaderTitle = strType;
    objGlobalImageVC.strWoId = strLeadId;
    objGlobalImageVC.strWoLeadId = strLeadId;
    objGlobalImageVC.strModuleType = @"SalesFlow";
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        
        objGlobalImageVC.strWoStatus = @"Complete";
        
    }
    else
    {
        objGlobalImageVC.strWoStatus = @"InComplete";
        
    }
    [self.navigationController presentViewController:objGlobalImageVC animated:NO completion:nil];
    
}
-(void)attachImage
{
    [self endEditing];
    // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* addImage = [UIAlertAction actionWithTitle:@"Add Images" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToGlobalmage:@"Before"];
        
    }];
    
    UIAlertAction* addDocument = [UIAlertAction actionWithTitle:@"Add Documents" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToUploadDocument];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addImage];
    [alert addAction:addDocument];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)goToUploadDocument
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    UploadDocumentSales *objUploadDocumentSales=[storyBoard instantiateViewControllerWithIdentifier:@"UploadDocumentSales"];
    objUploadDocumentSales.strWoId = strLeadId;
    
    [self.navigationController presentViewController:objUploadDocumentSales animated:NO completion:nil];
    
}

#pragma mark -------------- Commercial Tax Calculation -----------------

-(void)calculateTaxCommercial
{
    NSLog(@"Subtotal Initial Price : %f",subtotalInitialPriceClarkPest);
    NSLog(@"Subtotal Maintenance Price: %f",subTotalMaintPriceClarkPest);
    
    NSLog(@"Array of Initial Price Standard : %@",arrInitialPriceCommercial);
    NSLog(@"Array of Maint Price Standard : %@",arrMaintPriceCommercial);
    
    [self fetchSoldStandardServiceTaxabelAmount];
    [self fetchSoldNonStandardServiceTaxabelAmount];
}
-(void)fetchSoldStandardServiceTaxabelAmount
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray  *arrAllObjCommTax = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    totalInitialCommercialTax = 0;
    totalMaintCommercialTax = 0;
    for (int i=0; i<arrAllObjCommTax.count; i++)
    {
        
        NSManagedObject *matchesCommTax=[arrAllObjCommTax objectAtIndex:i];
        
        
        
        if ([strIsServiceAddrTaxExempt isEqualToString:@"false"])
        {
            if([strServiceAddressSubType isEqualToString:@"Commercial"])
            {
                if ([[dictCommercialStatus valueForKey:[matchesCommTax valueForKey:@"serviceSysName"]] isEqualToString:@"1"])
                {
                    totalInitialCommercialTax = totalInitialCommercialTax + [[NSString stringWithFormat:@"%@",[matchesCommTax valueForKey:@"totalInitialPrice"]]floatValue];
                    
                    totalMaintCommercialTax = totalMaintCommercialTax + [[NSString stringWithFormat:@"%@",[matchesCommTax valueForKey:@"totalMaintPrice"]]floatValue];
                }
            }
            else if([strServiceAddressSubType isEqualToString:@"Residential"])
            {
                if ([[dictResidentialStatus valueForKey:[matchesCommTax valueForKey:@"serviceSysName"]] isEqualToString:@"1"])
                {
                    totalInitialCommercialTax = totalInitialCommercialTax + [[NSString stringWithFormat:@"%@",[matchesCommTax valueForKey:@"totalInitialPrice"]]floatValue];
                    
                    totalMaintCommercialTax = totalMaintCommercialTax + [[NSString stringWithFormat:@"%@",[matchesCommTax valueForKey:@"totalMaintPrice"]]floatValue];
                    
                }
            }
            
        }
        
    }
}
-(void)fetchSoldNonStandardServiceTaxabelAmount
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray  *arrAllObjCommTax = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    for (int i=0; i<arrAllObjCommTax.count; i++)
    {
        
        NSManagedObject *matchesCommTax=[arrAllObjCommTax objectAtIndex:i];
        
        
        totalInitialCommercialTax = totalInitialCommercialTax + [[NSString stringWithFormat:@"%@",[matchesCommTax valueForKey:@"initialPrice"]]floatValue];
        
        totalMaintCommercialTax = totalMaintCommercialTax + [[NSString stringWithFormat:@"%@",[matchesCommTax valueForKey:@"maintenancePrice"]]floatValue];
        
    }
    
}
-(void)calculateDiscountPercent : OtherDiscount :(NSString *)strOtherDiscount TipDiscount :(NSString *)strTipDiscount
{
    float discountPer = 0.0;
    float totalDiscount = 0.0;
    totalDiscount = [strOtherDiscount floatValue] + [strTipDiscount floatValue];
    discountPer = (totalDiscount / subtotalInitialPriceClarkPest) * 100;
    if (discountPer < 0)
    {
        discountPer = 0.0;
    }
    
    float totalTaxableAmountInitial = 0.0;
    float totalTaxableAmountMaint = 0.0;
    
    /* for (int i = 0 ; i<arrInitialPriceCommercial.count; i++)
     {
     float amountAfterDiscount = 0.0;
     float initialPrice = [[NSString stringWithFormat:@"%@",[arrInitialPriceCommercial objectAtIndex:i]]floatValue];
     amountAfterDiscount = initialPrice - (initialPrice * discountPer/100);
     totalTaxableAmountInitial = totalTaxableAmountInitial + amountAfterDiscount;
     }*/
    float amountAfterDiscountInitial = 0.0;
    
    amountAfterDiscountInitial = totalInitialCommercialTax - (totalInitialCommercialTax * discountPer/100);
    totalTaxableAmountInitial = totalTaxableAmountInitial + amountAfterDiscountInitial;
    
    
    
    /*for (int i = 0 ; i<arrMaintPriceCommercial.count; i++)
     {
     float amountAfterDiscount = 0.0;
     float maintPrice = [[NSString stringWithFormat:@"%@",[arrMaintPriceCommercial objectAtIndex:i]]floatValue];
     amountAfterDiscount = maintPrice - (maintPrice * discountPer/100);
     totalTaxableAmountMaint = totalTaxableAmountInitial + amountAfterDiscount;
     }*/
    
    float amountAfterDiscountMaint = 0.0;
    
    amountAfterDiscountMaint = totalMaintCommercialTax - (totalMaintCommercialTax * discountPer/100);
    totalTaxableAmountMaint = totalTaxableAmountInitial + amountAfterDiscountMaint;
}

-(float)calculateTaxableAmountInitialPrice :(NSString *)strOtherDiscount TipDiscount :(NSString *)strTipDiscount LeadInspectionFee:(NSString *)strLeadInspectionFee
{
    
    [self calculateTaxCommercial];
    //Tax Code
    
    float discountPerInitial = 0.0;
    float totalDiscount = 0.0;
    totalDiscount = [strOtherDiscount floatValue] + [strTipDiscount floatValue] ;
    
    discountPerInitial = (totalDiscount / (subtotalInitialPriceClarkPest + [strLeadInspectionFee floatValue])) * 100;
    
    if (isnan(discountPerInitial))
    {
        discountPerInitial = 0;
    }
    if (discountPerInitial < 0)
    {
        discountPerInitial = 0.0;
    }
    
    float totalTaxableAmountInitial = 0.0;
    float amountAfterDiscountInitial = 0.0;
    
    totalInitialCommercialTax = totalInitialCommercialTax +  [strLeadInspectionFee floatValue];
    
    amountAfterDiscountInitial = totalInitialCommercialTax - (totalInitialCommercialTax * discountPerInitial/100);
    
    totalTaxableAmountInitial = totalTaxableAmountInitial + amountAfterDiscountInitial;
    
    if(strTax.length==0|| [strTax isEqualToString:@""] || [strTax floatValue] == 0)
    {
        strTax=@"0.0";
    }
    
    float taxAmountInitial = (totalTaxableAmountInitial * [strTax floatValue]) / 100;
    
    if (taxAmountInitial < 0)
    {
        taxAmountInitial = 0;
    }
    
    //End
    
    return taxAmountInitial;
}
-(void)updatePaymentInfoCoreDataNew
{
#pragma mark- Note
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjNew.count==0)
    {
        [self savePaymentInfoNew];
    }
    else
    {
        
        NSManagedObject *matchesNew=arrAllObjNew[0];
        [matchesNew setValue:_txtViewAdditionalNotes.text forKey:@"SpecialInstructions"];
        
        //........................................................
        [context save:&error1];
    }
}
-(void)savePaymentInfoNew
{
    // For Payment Info
    //PaymentInfo Entity
    
    entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    PaymentInfo *objentityPaymentInfo = [[PaymentInfo alloc]initWithEntity:entityPaymentInfo insertIntoManagedObjectContext:context];
    
    objentityPaymentInfo.leadId=strLeadId;
    
    objentityPaymentInfo.leadPaymentDetailId=@"";
    objentityPaymentInfo.paymentMode=@"";
    objentityPaymentInfo.amount=@"";
    objentityPaymentInfo.checkNo= @"";
    objentityPaymentInfo.licenseNo=@"";
    
    objentityPaymentInfo.expirationDate=@"";
    objentityPaymentInfo.specialInstructions=_txtViewAdditionalNotes.text;
    objentityPaymentInfo.agreement=@"";
    objentityPaymentInfo.proposal=@"";
    
    
    
    objentityPaymentInfo.customerSignature=@"";
    objentityPaymentInfo.salesSignature=@"";
    objentityPaymentInfo.inspection=@"";
    
    
    objentityPaymentInfo.createdBy=@"";
    objentityPaymentInfo.createdDate=@"";
    objentityPaymentInfo.modifiedBy=@"";
    objentityPaymentInfo.modifiedDate=[global modifyDate];
    
    //Nilind 16 Nov
    
    objentityPaymentInfo.companyKey=strCompanyKey;
    objentityPaymentInfo.userName=strUserName;
    
    
    
    objentityPaymentInfo.checkFrontImagePath=@"";
    objentityPaymentInfo.checkBackImagePath=@"";
    //.............
    
    NSError *error1;
    [context save:&error1];
    
    //........................................................................
}

-(void)goToAppointment
{
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Appointment_iPAD" bundle:nil];
        AppointmentVC *objAppointmentView=[storyBoard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objAppointmentView animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];AppointmentView*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

#pragma mark: ------------- Email & Service History -------------

-(void)goToEmailHistory
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSString *strLeadNo = [defs valueForKey:@"leadNumber"];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"DashBoard" bundle:nil];
    EmailListNew_iPhoneVC *objEmailListNew_iPhoneVC=[storyBoard instantiateViewControllerWithIdentifier:@"EmailListNew_iPhoneVC"];
    objEmailListNew_iPhoneVC.refNo = strLeadNo;
    [self.navigationController pushViewController:objEmailListNew_iPhoneVC animated:NO];
    //[self.navigationController presentViewController:objEmailListNew_iPhoneVC animated:YES completion:nil];
}
-(void)goToServiceHistory
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strTypeOfService=@"sales";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
}
-(void)goToHistory
{
    [self endEditing];
    // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* addServiceHistory = [UIAlertAction actionWithTitle:@"Service History" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
                                        {
        
        [self goToServiceHistory];
        
    }];
    
    UIAlertAction* addEmailHistory = [UIAlertAction actionWithTitle:@"Email History" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
        
        [self goToEmailHistory];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addServiceHistory];
    [alert addAction:addEmailHistory];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(CGFloat)getAttributedLabelHeight:(NSString *)str
{
    CGFloat textWidth = [UIScreen mainScreen].bounds.size.width - 125;//420;
    
    //CGFloat textHght = [self findHeightForText:str havingWidth: textWidth andFont:[UIFont systemFontOfSize:16.0]].height;
    
    UILabel *lbl = [[UILabel alloc]init];
    lbl.frame = CGRectMake(0, 0, textWidth, 21);
    
    
    NSAttributedString *attributedStringHTML = [[NSAttributedString alloc]
                                                initWithData: [str dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
    lbl.attributedText = attributedStringHTML;
    
    
    return [self getLabelHeight:lbl];
}
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font,NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}
@end

