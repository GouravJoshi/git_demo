//
//  DetailDownloadedFilesView.h
//  DPS
//
//  Created by Saavan Patidar on 22/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavan Patidar 2021
//  Saavan Ji Patidar Ji
//  Saavan Patidar


#import <UIKit/UIKit.h>

@interface DetailDownloadedFilesView : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webViewDetail;
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_TitleName;
@property (strong, nonatomic) NSString *strFileName;
@property (strong, nonatomic) NSString *strFrom;
@end
