//
//  AddLead+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 06/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AddLead+CoreDataProperties.h"

@implementation AddLead (CoreDataProperties)

@dynamic companyId;
@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDateTime;
@dynamic customerId;
@dynamic isAgreementGenerated;
@dynamic leadId;
@dynamic pocId;
@dynamic primaryServiceId;
@dynamic userName;
@dynamic customerExtDcDict;
@dynamic leadServiceExtDcsArray;
@dynamic customerAddress;

@end
