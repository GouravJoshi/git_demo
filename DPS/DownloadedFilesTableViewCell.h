//
//  DownloadedFilesTableViewCell.h
//  DPS
//
//  Created by Saavan Patidar on 22/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Saavan Patidar 2021
//  Jai Shree Ram.

#import <UIKit/UIKit.h>

@interface DownloadedFilesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_FileName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Date;

@end
