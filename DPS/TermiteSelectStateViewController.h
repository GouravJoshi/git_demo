//
//  TermiteSelectStateViewController.h
//  DPS
//
//  Created by Saavan Patidar on 08/11/17.
//  Copyright © 2017 Saavan. All rights reserved.
////test

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface TermiteSelectStateViewController : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityWorkOrder;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;

@property (strong, nonatomic)  NSString *strWorkOrder,*strTermiteStateType;
@property (strong, nonatomic)  NSString *strWorkOrderStatus;

- (IBAction)action_Texas:(id)sender;
- (IBAction)action_Florida:(id)sender;
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAccoNo;
@property (weak, nonatomic) IBOutlet UIButton *btnTexas;
@property (weak, nonatomic) IBOutlet UIButton *btnFlorida;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;
@end
