//
//  UpcomingAppointmentTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 22/06/16.
//  Copyright © 2016 Saavan. All rights reserved.

//  Saavan 007 @ 2021

//  Saavan ji Patidar ji 2021

//  Saavan Patidar 2021

//  Saavan Ji Patidar 2021

//  Saavan Patidar 2021

#import <UIKit/UIKit.h>

@interface UpcomingAppointmentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *sendSMS;
@property (strong, nonatomic) IBOutlet UIButton *sendEmail;
@property (strong, nonatomic) IBOutlet UIButton *sendMapView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_WorkOrderNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ServiceName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Type;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Invoice;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AccNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_DAte;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_MapBtn_Leading;
@property (weak, nonatomic) IBOutlet UIButton *btnCreateInitialSetup;
@property (strong, nonatomic) IBOutlet UILabel *lblLeadIddd;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailId;
@property (weak, nonatomic) IBOutlet UILabel *lblTechName;
@property (weak, nonatomic) IBOutlet UILabel *lblSpecialInstruction;
@property (weak, nonatomic) IBOutlet UILabel *lblSpecialAttribute;
@property (strong, nonatomic) IBOutlet UILabel *lblCompanyName;
@property (strong, nonatomic) IBOutlet UIButton *btnResendMail;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceInstruction;
@property (strong, nonatomic) IBOutlet UIButton *btnCellNo;
@property (strong, nonatomic) IBOutlet UIButton *btnPrintOption;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_lblLine_Bottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_SeriviceAttributes_H;
@property (weak, nonatomic) IBOutlet UILabel *lblOpportunityType;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Lbl_OpportunityType_H;
@property (strong, nonatomic) IBOutlet UIButton *btnMechanicalSubWoStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDriveTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEarliestStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblLatestStartTime;
@property (strong, nonatomic) IBOutlet UIButton *btnCellNoNew;

@end
