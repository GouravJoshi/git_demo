//
//  SelectionClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//  test


// Tags For Which View
// 101 - - - - - - -  - -  Corrective actions of Add conditions view
// 102 - - - - - - -  - -  conducive Condition Masters of Add conditions view
// 103 - - - - - - -  - -  materials group of Add Materials view
// 104 - - - - - - -  - -  Targets of Add Materials view
// 105 - - - - - - -  - -  Targets of Add Pests view
// 106 - - - - - - -  - -  Helper Selection of Add Employee Working Time on Time Sheet
// 107 Subsection code on Add problem identificationVC
// 108 Recommendation code on Add problem identificationVC
// 109 General Notes on Add General Notes View.
// 110 Discalimer selection

import UIKit

class SelectionClass: UIViewController {
    
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @objc weak var delegate:PopUpDelegate?
    
    // MARK: - ----Variable
    
    @objc var aryList = NSMutableArray()
    @objc var aryForListData = NSMutableArray()
    @objc var strTitle = String()
    @objc var strTypeOfSelection = String()
    @objc var strTag = Int()
    @objc var arySelectedTargets = NSMutableArray()
    @objc var strSelectedValue = String()
    var arrOfSectionTiles = NSMutableArray()
    var arrMarkAsFaourite = NSMutableArray()

    // MARK: - ----Life Cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //arrMarkAsFaourite = NSMutableArray()
        lblTitle.text = strTitle
        aryForListData = aryList
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        viewHeader.backgroundColor = UIColor.theme()
        
        if strTag == 104 {
            
            let button = UIButton(type: .system)
            button.frame = CGRect(x: UIScreen.main.bounds.size.width-60, y: 2, width: 60, height: 60)
            button.setTitle("Save", for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 22)
            button.addTarget(self,action:#selector(actionOnSave),
                             for:.touchUpInside)
            viewHeader.addSubview(button)
            
        }
        
        if strTag == 101 {
            
            let button = UIButton(type: .system)
            button.frame = CGRect(x: UIScreen.main.bounds.size.width-60, y: 2, width: 60, height: 60)
            button.setTitle("Save", for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 22)
            button.addTarget(self,action:#selector(actionOnSave),
                             for:.touchUpInside)
            viewHeader.addSubview(button)
            
        }

        sectionTilesLogic()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
    }
    // MARK: - ----IBAction
    
    @IBAction func actionOnBack(_ sender: Any) {
        
        self.view.endEditing(true)
        self.dismiss(animated: false) {
            
        }
        setCode()
    }
    func setCode()
    {
        if strTag == 107 || strTag == 108
        {
            
            nsud.set(arrMarkAsFaourite, forKey: "favouriteCode")
            nsud.set(true, forKey: "isSyncFavourite")
            nsud.synchronize()
            
            if(isInternetAvailable() == false)
            {
                                
            }
            else
            {
                
                //callAPIMarkAsFavourite()
                callAPIMarkAsFavouriteNew(strType: "Finding")

            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddToFavourite_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
            
        }
        if strTag == 109
        {
            
            nsud.set(arrMarkAsFaourite, forKey: "favouriteNote")
            nsud.set(true, forKey: "isSyncFavouriteNote")
            nsud.synchronize()
            
            if(isInternetAvailable() == false)
            {
                                
            }
            else
            {
                
                //callAPIMarkAsFavouriteNote()
                callAPIMarkAsFavouriteNew(strType: "Note")

            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddToFavouriteNotes_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
            
        }
    }
    
    @IBAction func actionOnSave(_ sender: UIButton) {
        
        if (strTag == 104){ // for Target Multi Selection
            
            let dictTargets = NSMutableDictionary()
            dictTargets.setValue(self.arySelectedTargets, forKey: "Targets")
            self.delegate?.getDataFromPopupDelegate(dictData: dictTargets, tag: self.strTag)
            
        }else if (strTag == 101){
            
            let dictTargets = NSMutableDictionary()
            dictTargets.setValue(self.arySelectedTargets, forKey: "CorrectiveActions")
            self.delegate?.getDataFromPopupDelegate(dictData: dictTargets, tag: self.strTag)
            
        }
        
        
        self.view.endEditing(true)
        self.dismiss(animated: false) {
            
        }
        
    }
    @objc private func actionOnMarkAsFavourite(_ sender: UIButton)
    {
        if (strTag == 107)
        {
            
            let row = sender.tag/1000
            let section = sender.tag%1000
            
            print("mark as favourite called 107")
            
            let textToSearch = "\(arrOfSectionTiles[section])"
            
            let aryTemp = (aryList.filter({ (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "CodeName")!)".uppercased().hasPrefix(textToSearch.uppercased())
                
            }) as NSArray).mutableCopy() as! NSMutableArray
            
            let dict = aryTemp.object(at: row)as! NSDictionary
            
            //dict = aryList.object(at: sender.tag) as! NSDictionary
            
            if arrMarkAsFaourite .contains("\(dict.value(forKey: "CodeMasterId") ?? "")")
            {
                arrMarkAsFaourite.remove("\(dict.value(forKey: "CodeMasterId") ?? "")")

            }
            else
            {
                arrMarkAsFaourite.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
            }
            
        }
        else if (strTag == 108)
        {
            
            let row = sender.tag/1000
            let section = sender.tag%1000
            
            print("mark as favourite called 107")
            
            let textToSearch = "\(arrOfSectionTiles[section])"
            
            let aryTemp = (aryList.filter({ (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "CodeName")!)".uppercased().hasPrefix(textToSearch.uppercased())
                
            }) as NSArray).mutableCopy() as! NSMutableArray
            
            let dict = aryTemp.object(at: row)as! NSDictionary
            
            //dict = aryList.object(at: sender.tag) as! NSDictionary
            
            if arrMarkAsFaourite .contains("\(dict.value(forKey: "CodeMasterId") ?? "")")
            {
                arrMarkAsFaourite.remove("\(dict.value(forKey: "CodeMasterId") ?? "")")

            }
            else
            {
                arrMarkAsFaourite.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
            }
            
        }
        else if (strTag == 109)
        {
            let dict = aryList.object(at: sender.tag)as! NSDictionary
            
            if arrMarkAsFaourite .contains("\(dict.value(forKey: "ServiceJobDescriptionId") ?? "")")
            {
                arrMarkAsFaourite.remove("\(dict.value(forKey: "ServiceJobDescriptionId") ?? "")")

            }
            else
            {
                arrMarkAsFaourite.add("\(dict.value(forKey: "ServiceJobDescriptionId") ?? "")")
            }

        }
        
        
        tvlist.reloadData()
    }
    func sectionTilesLogic() {
        
        // Logic for first Character Title
        if strTag == 107 || strTag == 108 {
            
            if aryList.count > 0 {
                
                for k1 in 0 ..< aryList.count {
                    
                    let dictData = aryList[k1] as! NSDictionary
                    
                    let strFirstLetter = "\(dictData.value(forKey: "CodeName") ?? "")"
                    
                    if arrOfSectionTiles.contains(strFirstLetter.prefix(1).uppercased()){
                        
                        
                        
                    }else{
                        
                        arrOfSectionTiles.add(strFirstLetter.prefix(1).uppercased())
                        
                    }
                    
                }
                
            }
            
        } else if strTag == 110 {
            
            if aryList.count > 0 {
                
                for k1 in 0 ..< aryList.count {
                    
                    let dictData = aryList[k1] as! NSDictionary
                    
                    let strFirstLetter = "\(dictData.value(forKey: "Title") ?? "")"
                    
                    if arrOfSectionTiles.contains(strFirstLetter.prefix(1).uppercased()){
                        
                        
                        
                    }else{
                        
                        arrOfSectionTiles.add(strFirstLetter.prefix(1).uppercased())
                        
                    }
                    
                }
                
            }
            
        }
        
        let sortedArray = arrOfSectionTiles.sortedArray(comparator: { obj1, obj2 in
            
            return (((obj1 as! String).uppercased()).compare((obj2 as! String).uppercased()))
            
        })
        print("\(sortedArray)")
        
        arrOfSectionTiles = NSMutableArray()
        
        arrOfSectionTiles.addObjects(from: sortedArray)
        
        /*
        for k1 in 0 ..< 30 {

            arrOfSectionTiles.add("S")
            
        }
        */
    }
    
    func getPostDataNew(strType : String) -> NSDictionary
    {
        var dict = [String:Any]()
        
        let arr = NSMutableArray()
    
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        if strType == "Note"
        {
            var arrOfFavouriteNotes = NSArray()
            
            if nsud.value(forKey: "favouriteNote") is NSArray
            {
                let arrFvouriteCodesTemp = nsud.value(forKey: "favouriteNote") as! NSArray
                arrOfFavouriteNotes = arrFvouriteCodesTemp
            }
            else
            {
                arrOfFavouriteNotes = NSArray()
            }
            
            for item in arrOfFavouriteNotes
            {
                let strId = item as! String
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("WoWdoNoteFavoriteDetailId")
                arrOfKeys.add("NoteId")
                arrOfKeys.add("EmployeeId")
                arrOfKeys.add("CompanyId")
                arrOfKeys.add("CreatedDate")
                arrOfKeys.add("CreatedBy")
                arrOfKeys.add("ModifiedDate")
                arrOfKeys.add("ModifiedBy")
                
                arrOfValues.add("")
                arrOfValues.add("\(strId)")
                arrOfValues.add("\(strEmpID)")
                arrOfValues.add("\(strCompanyId)")
                arrOfValues.add(Global().getCurrentDate())
                arrOfValues.add("\(strUserName)")
                arrOfValues.add(Global().getCurrentDate())
                arrOfValues.add("\(strUserName)")
                
                let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
                arr.add(dict_ToSend)
                
            }
            
            dict = [
                    "WoWdoNoteFavoriteDetailExtSerDcs":arr,
            ]

        }
        else
        {
            var arrFvouriteCodesTemp = NSArray()
            
            if nsud.value(forKey: "favouriteCode") is NSArray
            {
                //arrFvouriteCodesTemp = nsud.value(forKey: "favouriteCode") as! NSArray
                
                arrFvouriteCodesTemp = nsud.value(forKey: "favouriteCode") as! NSArray
                
                let arrUniqueDocuments = arrFvouriteCodesTemp.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
            
                arrFvouriteCodesTemp = arrUniqueDocuments//arrFvouriteCodesTemp
            }
            
            
            for item in arrFvouriteCodesTemp
            {
                let strId = item as! String
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("WoWdoFindingFavoriteDetailId")
                arrOfKeys.add("CodeMasterId")
                arrOfKeys.add("EmployeeId")
                arrOfKeys.add("CompanyId")
                arrOfKeys.add("CreatedDate")
                arrOfKeys.add("CreatedBy")
                arrOfKeys.add("ModifiedDate")
                arrOfKeys.add("ModifiedBy")
                
                arrOfValues.add("")
                arrOfValues.add("\(strId)")
                arrOfValues.add("\(strEmpID)")
                arrOfValues.add("\(strCompanyId)")
                arrOfValues.add(Global().getCurrentDate())
                arrOfValues.add("\(strUserName)")
                arrOfValues.add(Global().getCurrentDate())
                arrOfValues.add("\(strUserName)")
                
                let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
                arr.add(dict_ToSend)
                
            }
            
            dict = [
                "WoWdoFindingFavoriteDetailExtSerDcs":arr
            ]
            
        }
        
        return dict as NSDictionary
    }
    
    func callAPIMarkAsFavouriteNew(strType : String)
    {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/AddUpdateNoteAndFindingFavoriteDetail?EmoloyeeId=\(strEmpID)&CompanyId=\(strCompanyId)&EmployeeNo=\(strEmpNo)&Type=\("\(strType)")"
        
        WebService.postRequestWithHeaders(dictJson: getPostDataNew(strType: strType) , url: strURL, responseStringComing: "TimeLine") { (response, status) in
            
            if(status == true)
            {
                
                let dictResponse = response.value(forKey: "data") as! NSDictionary
                
                if strType == "Note" //ForNotes
                {
                    let arrData = dictResponse.value(forKey: "WoWdoNoteFavoriteDetailExtSerDcs") as! NSArray
                    let arrTemp = NSMutableArray()
                    if arrData.count > 0
                    {
                        for item in arrData
                        {
                            let dict = item as! NSDictionary
                            arrTemp.add("\(dict.value(forKey: "NoteId") ?? "")")
                        }
                    }
                    var arrOfFavouriteCodes = NSArray()
                    arrOfFavouriteCodes = arrTemp
                    nsud.set(arrOfFavouriteCodes, forKey: "favouriteNote")
                    nsud.set(false, forKey: "isSyncFavouriteNote")
                    nsud.synchronize()
                }
                else  //For Finding Code
                {
                    
                    if(dictResponse.value(forKey: "WoWdoFindingFavoriteDetailExtSerDcs") is NSArray){
                        let arrData = dictResponse.value(forKey: "WoWdoFindingFavoriteDetailExtSerDcs") as! NSArray
                        let arrTemp = NSMutableArray()
                        if arrData.count > 0
                        {
                            for item in arrData
                            {
                                let dict = item as! NSDictionary
                                arrTemp.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
                            }
                        }
                        var arrOfFavouriteCodes = NSArray()
                        arrOfFavouriteCodes = arrTemp
                        nsud.set(arrOfFavouriteCodes, forKey: "favouriteCode")
                        nsud.set(false, forKey: "isSyncFavourite")
                        nsud.synchronize()
                    }
              
                }
                
            }
            
        }
    }
    
    
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SelectionClass : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if strTag == 107 || strTag == 108 || strTag == 110{
            
            return arrOfSectionTiles.count
            
        } else {
            
            return 1
            
        }
        
    }
    
    /*
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if strTag == 107 || strTag == 108 {
            
            return arrOfSectionTiles[section] as? String
            
        } else {
            
            return ""
            
        }
        
    }*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if strTag == 107 || strTag == 108 || strTag == 110{

            return 50
            
        } else {
            
            return 0
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if strTag == 107 || strTag == 108 || strTag == 110{

            let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
            //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let lblHeader = UILabel()
            lblHeader.frame = CGRect(x: 20, y: 0, width: tableView.frame.width-20, height: 40)
            lblHeader.text = "\(arrOfSectionTiles[section])".uppercased()//arrOfSectionTiles[section] as? String
            lblHeader.font = UIFont.boldSystemFont(ofSize: 22)
            vw.addSubview(lblHeader)
            
            return vw
            
        }else{
            
            let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
            //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            
            let lblHeader = UILabel()
            lblHeader.frame = vw.frame
            lblHeader.text = ""
            lblHeader.font = UIFont.systemFont(ofSize: 22)
            vw.addSubview(lblHeader)
            
            return vw
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if strTag == 107 || strTag == 108 {
            
            let textToSearch = "\(arrOfSectionTiles[section])"
            
            let aryTemp = (aryList.filter({ (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "CodeName")!)".uppercased().hasPrefix(textToSearch.uppercased())
                
            }) as NSArray).mutableCopy() as! NSMutableArray
            
            return aryTemp.count
            
        }else if strTag == 110 {
            
            let textToSearch = "\(arrOfSectionTiles[section])"
            
            let aryTemp = (aryList.filter({ (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "Title")!)".uppercased().hasPrefix(textToSearch.uppercased())
                
            }) as NSArray).mutableCopy() as! NSMutableArray
            
            return aryTemp.count
            
        } else {
            
            return aryList.count
            
        }
        
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        if strTag == 107 || strTag == 108 || strTag == 110{
            
            return arrOfSectionTiles as? [String]
            
        } else {
            
            return arrOfSectionTiles as? [String]
            
        }
        
    }
    
    /*
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]?
    {
        if strTag == 107 || strTag == 108 {
            
            var titles = [String]()
            
            var title = "\(arrOfSectionTiles)"
            
            title = title.uppercased()
            
            titles.append(title)
            
            return titles
            
        }else{
            
            return arrOfSectionTiles as? [String]
            
        }
        
    }*/
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        
        if strTag == 107 || strTag == 108 || strTag == 110{
            
            return arrOfSectionTiles.index(of:title)
            
        } else {
            
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "SelectionCell", for: indexPath as IndexPath) as! SelectionCell
        
      
        
        if strTag == 107 || strTag == 108
        {
            
            let textToSearch = "\(arrOfSectionTiles[indexPath.section])"
            
            let aryTemp = (aryList.filter({ (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "CodeName")!)".uppercased().hasPrefix(textToSearch.uppercased())
                
            }) as NSArray).mutableCopy() as! NSMutableArray
            
            if aryTemp.count > indexPath.row {

                let dict = aryTemp.object(at: indexPath.row)as! NSDictionary
                
                let strTitleToShow = "Code Name: \(dict.value(forKey: "CodeName")!)" + "<br><br>Finding: \(dict.value(forKey: "Finding")!)" + "<br><br>Recommendation: \(dict.value(forKey: "Recommendation")!)"
                
                //cell.lblTitle.text = strTitleToShow
                
                cell.lblTitle.attributedText = htmlAttributedString(strHtmlString: strTitleToShow)
                    
                //Nilind
                
                cell.btnMarkAsFavourite.isHidden = false
                
                cell.btnMarkAsFavourite.tag = indexPath.row*1000+indexPath.section
                
                cell.btnMarkAsFavourite.addTarget(self, action: #selector(actionOnMarkAsFavourite), for: .touchUpInside)
                

                
                if arrMarkAsFaourite.contains("\(dict.value(forKey: "CodeMasterId") ?? "")")
                {
                    cell.btnMarkAsFavourite.setImage(UIImage(named: "Favourite"), for: .normal)
                }
                else
                {
                    cell.btnMarkAsFavourite.setImage(UIImage(named: "NotFavourite"), for: .normal)

                }
                
            }else{
                
                cell.lblTitle.attributedText = htmlAttributedString(strHtmlString: "")

            }

            
        }
        else if strTag == 110
        {
            
            let textToSearch = "\(arrOfSectionTiles[indexPath.section])"
            
            let aryTemp = (aryList.filter({ (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "Title")!)".uppercased().hasPrefix(textToSearch.uppercased())
                
            }) as NSArray).mutableCopy() as! NSMutableArray
            
            if aryTemp.count > indexPath.row {

                let dict = aryTemp.object(at: indexPath.row)as! NSDictionary
                
                let strTitleToShow = "Title: \(dict.value(forKey: "Title")!)" + "<br><br>Disclaimer: \(dict.value(forKey: "Disclaimer")!)"
                
                //htmlAttributedString(strHtmlString: strReplacedString)
                
                cell.lblTitle.attributedText = htmlAttributedString(strHtmlString: strTitleToShow)
                
                //cell.lblTitle.attributedText = htmlAttributedString(strHtmlString: strTitleToShow)
                
            }else{
                
                cell.lblTitle.attributedText = htmlAttributedString(strHtmlString: "")
                
            }

            
        }else{
            
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            
            if (strTag == 101){ // for Corrective action multiple selction
                
                if(arySelectedTargets.contains(aryList.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                
                cell.lblTitle.text = "\(dict.value(forKey: "CorrectiveActionName")as! String)"
                
            }else if(self.strTag == 102){
                
                cell.lblTitle.text = "\(dict.value(forKey: "ConditionName")!)"
                
            }else if(self.strTag == 103){
                
                cell.lblTitle.text = "\(dict.value(forKey: "ProductName")!)"
                
            }else if (strTag == 104){ // for Target Multi Selection
                
                if(arySelectedTargets.contains(aryList.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                
                cell.lblTitle.text = "\(dict.value(forKey: "TargetName")as! String)"
                
            }else if(self.strTag == 105){
                
                cell.lblTitle.text = "\(dict.value(forKey: "TargetName")!)"
                
            }else if(self.strTag == 106){
                
                //cell.lblTitle.text = "\(dict.value(forKey: "FullName")!)"  // FullName
                cell.lblTitle.text = "\(dict.value(forKey: "FullName")!)"  // FullName
                
            }else if(self.strTag == 107 || self.strTag == 108){
                
                let strTitleToShow = "Code Name: \(dict.value(forKey: "CodeName")!)" + "\n\nFinding: \(dict.value(forKey: "Finding")!)" + "\n\nRecommendation: \(dict.value(forKey: "Recommendation")!)"
                
                cell.lblTitle.text = strTitleToShow
                
            }else if(self.strTag == 109){
                
                let strTitleToShow = "Title: \(dict.value(forKey: "Title")!)" + "\n\nDescription: \(dict.value(forKey: "ServiceJobDescription")!)"
                
                cell.lblTitle.text = strTitleToShow
                
                cell.btnMarkAsFavourite.isHidden = false
                
                cell.btnMarkAsFavourite.tag = indexPath.row
                
                cell.btnMarkAsFavourite.addTarget(self, action: #selector(actionOnMarkAsFavourite), for: .touchUpInside)
                
                
                
                if arrMarkAsFaourite.contains("\(dict.value(forKey: "ServiceJobDescriptionId") ?? "")")
                {
                    cell.btnMarkAsFavourite.setImage(UIImage(named: "Favourite"), for: .normal)
                }
                else
                {
                    cell.btnMarkAsFavourite.setImage(UIImage(named: "NotFavourite"), for: .normal)

                }
               

                
                
            }else  if(self.strTag == 1){
                
                cell.lblTitle.text = "\(dict.value(forKey: "TargetGroupName")!)"
                
            }
            
        }
        
        if strTag == 107 || strTag == 108 || strTag == 109
        {
            cell.btnMarkAsFavourite.isHidden = false
        }
        else
        {
            cell.btnMarkAsFavourite.isHidden = true
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        
        if (self.strTag == 104){ // for Source Multi Selection
            
            if(self.arySelectedTargets.contains(self.aryList.object(at: indexPath.row))){ // True
                self.arySelectedTargets.remove(self.aryList.object(at: indexPath.row))
                
            }else{
                self.arySelectedTargets.add(self.aryList.object(at: indexPath.row))
            }
            self.tvlist.reloadData()
        }else if (self.strTag == 101){ // for Source Multi Selection
            
            if(self.arySelectedTargets.contains(self.aryList.object(at: indexPath.row))){ // True
                self.arySelectedTargets.remove(self.aryList.object(at: indexPath.row))
                
            }else{
                self.arySelectedTargets.add(self.aryList.object(at: indexPath.row))
            }
            self.tvlist.reloadData()
        }
        else
        {
            
            if strTag == 107 || strTag == 108 {
                
                let textToSearch = "\(arrOfSectionTiles[indexPath.section])"
                
                let aryTemp = (aryList.filter({ (task) -> Bool in
                    
                    return "\((task as! NSDictionary).value(forKey: "CodeName")!)".uppercased().hasPrefix(textToSearch.uppercased())
                    
                }) as NSArray).mutableCopy() as! NSMutableArray
                
                if aryTemp.count > indexPath.row {

                    let dict = aryTemp.object(at: indexPath.row)as! NSDictionary
                    self.delegate?.getDataFromPopupDelegate(dictData: dict, tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }else{
                    

                    
                }
                

                
                
                setCode()
                
                
                
            }else if strTag == 110 {
                
                let textToSearch = "\(arrOfSectionTiles[indexPath.section])"
                
                let aryTemp = (aryList.filter({ (task) -> Bool in
                    
                    return "\((task as! NSDictionary).value(forKey: "Title")!)".uppercased().hasPrefix(textToSearch.uppercased())
                    
                }) as NSArray).mutableCopy() as! NSMutableArray
                
                if aryTemp.count > indexPath.row {

                    let dict = aryTemp.object(at: indexPath.row)as! NSDictionary
                    self.delegate?.getDataFromPopupDelegate(dictData: dict, tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }else{
                    
                    
                    
                }
                

                
            }
            else if strTag == 109
            {
                
                let dict = aryList.object(at: indexPath.row)as? NSDictionary
                self.delegate?.getDataFromPopupDelegate(dictData: dict!, tag: self.strTag)
                setCode()
                self.dismiss(animated: false) {}
            }
            else{
                
                let dict = aryList.object(at: indexPath.row)as? NSDictionary
                self.delegate?.getDataFromPopupDelegate(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
                
            }
            
        }
        
    }
    
    
}
// MARK: - ----------------SlectionCell
// MARK: -
class SelectionCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UITextView!
    
    
    @IBOutlet weak var btnMarkAsFavourite: UIButton!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  SelectionClass : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "Name contains[c] %@ OR ConditionName contains[c] %@ OR TargetGroupName contains[c] %@ OR CorrectiveActionName contains[c] %@ OR FullName contains[c] %@ OR FirstName contains[c] %@ OR MiddleName contains[c] %@ OR LastName contains[c] %@ OR TargetName contains[c] %@ OR ProductName contains[c] %@ OR CodeName contains[c] %@ OR Finding contains[c] %@ OR Recommendation contains[c] %@ OR Title contains[c] %@ OR ServiceJobDescription contains[c] %@ OR Title contains[c] %@ OR Disclaimer contains[c] %@", argumentArray: [Searching, Searching , Searching , Searching, Searching , Searching , Searching , Searching , Searching , Searching , Searching , Searching , Searching , Searching , Searching , Searching , Searching])
        if !(Searching.length == 0) {
            
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        
        sectionTilesLogic()
        
        if(aryList.count == 0){
        }
    }
}

// MARK: -------Protocol

@objc protocol PopUpDelegate : class{
    func getDataFromPopupDelegate(dictData : NSDictionary ,tag : Int)
}
