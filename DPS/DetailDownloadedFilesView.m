//
//  DetailDownloadedFilesView.m
//  DPS
//
//  Created by Saavan Patidar on 22/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavan Ji Patidar Ji


#import "DetailDownloadedFilesView.h"
#import "DejalActivityView.h"

@interface DetailDownloadedFilesView ()

@end

@implementation DetailDownloadedFilesView

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading File..."];
    _lbl_TitleName.text=_strFileName;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",_strFileName]];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request_pdf = [NSURLRequest requestWithURL:targetURL];
    [_webViewDetail loadRequest:request_pdf];

   // [self loadDoc:@"Auto Data 14-11-2015.xls" inView:_webViewDetail];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadDoc :(NSString*)strName inView:(UIWebView*)webVieww{
    NSString *path=[[NSBundle mainBundle]pathForResource:strName ofType:nil];
    NSURL *urll=[NSURL fileURLWithPath:path];
    NSURLRequest *req=[NSURLRequest requestWithURL:urll];
    [_webViewDetail loadRequest:req];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_Back:(id)sender {
    
    if ([_strFrom isEqualToString:@"Pest"]) {
        
        [self dismissViewControllerAnimated:false completion:nil];
        
    } else {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //[_loaderWebView startAnimating];
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Searching..."];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    // [_loaderWebView stopAnimating];
    [DejalActivityView removeView];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"The File you are looking for does not exist.Please try again later."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                             /* for (UIViewController *controller in self.navigationController.viewControllers)
                              {
                                  if ([controller isKindOfClass:[DownloadedFileView class]])
                                  {
                                      [self.navigationController popToViewController:controller animated:NO];
                                      break;
                                  }
                              } */
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    [DejalActivityView removeView];
}

@end
