//
//  ServiceHistory_iPhoneVC.swift
//  DPS
//
//  Created by NavinPatidar on 5/21/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import SafariServices

class ServiceHistory_iPhoneVC: UIViewController {
    // MARK:
    // MARK: ----------IBOutlet----------------------
    
    @IBOutlet private weak var tvList: UITableView! {
        didSet {
            tvList.tableFooterView = UIView()
            tvList.estimatedRowHeight = 50
        }
    }
    @IBOutlet weak var btn_LoadData: UIButton!
    
      var strAcctNo = ""
      var aryTblList = NSMutableArray() ,aryServiceHistoryData =  NSMutableArray()
      var dictLoginData = NSDictionary()
      var indexTag = "" , str_Inspection = Bool() , str_Graph = Bool()
      var refresher = UIRefreshControl()


    // MARK:
    // MARK: ----------Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_LoadData.layer.cornerRadius = 8.0
        self.dictLoginData =  nsud.value(forKey: "LoginDetails")as! NSDictionary
        btn_LoadData.isHidden = true
        
        strAcctNo = nsud.value(forKey: "AccNoService") as! String

        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

                self.getService_HistoryList()

            })

        }else{
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
            
        }
        
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvList!.addSubview(refresher)
        
    }
    
    // MARK:
    // MARK: ----------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        //self.navigationController?.popViewController(animated: false)
        dismiss(animated: false)

    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let controller = UIStoryboard.init(name: "Task-Activity", bundle: nil).instantiateViewController(withIdentifier: "ServiceHistory_Filter_iPhoneVC") as! ServiceHistory_Filter_iPhoneVC
        controller.tag = 1
        controller.delegate = self
        
        self.present(controller, animated: false, completion: nil)
        //self.navigationController?.pushViewController(controller, animated: false)
    }
    @IBAction func actionOnLoadData(_ sender: UIButton) {
        self.view.endEditing(true)
//        if(str_Inspection == false && str_Graph == false){
//                     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Saavan Sir denge Message", viewcontrol: self)
//                  }else{
//                      self.Call_LoadDataAPI()
//                  }
    }
 
}

// MARK: -
// MARK: -----UITableViewDelegate

extension ServiceHistory_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTblList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "ServiceHistory_iPhoneCell", for: indexPath as IndexPath) as! ServiceHistory_iPhoneCell
        
        let dictData =   removeNullFromDict(dict: (aryTblList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        
        cell.lbl_Name.text = Global().strFullName(dictData as? [AnyHashable : Any])
        cell.lbl_Order.text = "\(dictData.value(forKey: "WorkOrderNo")!)"
       
        cell.lbl_Address.text = Global().strCombinedAddressService(dictData as? [AnyHashable : Any])
        
        cell.lbl_Service.text = "\(dictData.value(forKey: "Service")!)"

        cell.lbl_Route.text = "\(dictData.value(forKey: "RouteNo")!)" == "" ? "N/A" : "\(dictData.value(forKey: "RouteNo")!)"
        
        cell.lbl_Empolyee.text = "\(dictData.value(forKey: "EmployeeName")!) (\(dictData.value(forKey: "EmployeeNo")!))"
                
        cell.lbl_ScheduleStartDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "ScheduleOnStartDateTime")!)", strRequiredFormat: "MM/dd/yyyy hh:mm a")
        
        cell.lbl_ProductsName.text = "\(dictData.value(forKey: "ProductName")!)" == "" ? "-" : "\(dictData.value(forKey: "ProductName")!)"
        
        cell.lbl_Devices.text = "\(dictData.value(forKey: "DeviceName")!)" == "" ? "-" : "\(dictData.value(forKey: "DeviceName")!)"
        
        cell.txtTechComment.isEditable = false
        cell.txtTechComment.text = "\(dictData.value(forKey: "TechComments")!)" == "" ? "-" : "\(dictData.value(forKey: "TechComments")!)"
        
        
        cell.btn_PDF.setTitle("Service Report", for: .normal)
                   let strDepartmentType = "\(dictData.value(forKey: "DepartmentType")!)"
        
        let strServiceStateLocal = "\(dictData.value(forKey: "ServiceState")!)"


           /*if (strDepartmentType == "Termite" && strServiceStateLocal == "California")
           {
               cell.btn_height.constant = 30.0
           }else{
               cell.btn_height.constant = 0.0
           }*/
           cell.btn_height.constant = 0.0

     if(indexTag == "\(indexPath.row)"){
                     self.str_Graph == true ? cell.btn_Graph.setImage( UIImage(named: "check_ipad"), for: .normal) : cell.btn_Graph.setImage( UIImage(named: "uncheck_ipad"), for: .normal)
                     self.str_Inspection == true ? cell.btn_IbspectionDetails.setImage( UIImage(named: "check_ipad"), for: .normal) : cell.btn_IbspectionDetails.setImage( UIImage(named: "uncheck_ipad"), for: .normal)
                     
                     
                 }else{
                     cell.btn_Graph.setImage( UIImage(named: "uncheck_ipad"), for: .normal)
                     cell.btn_IbspectionDetails.setImage( UIImage(named: "uncheck_ipad"), for: .normal)
                 }
        
        
        cell.btn_Graph.tag = indexPath.row
        cell.btn_Graph.addTarget(self, action: #selector(self.actionWithGraph(sender:)), for: .touchUpInside)
        
        
        cell.btn_PDF.tag = indexPath.row
        cell.btn_PDF.addTarget(self, action: #selector(self.actionWithPDF(sender:)), for: .touchUpInside)
        
        cell.btn_IbspectionDetails.tag = indexPath.row
        cell.btn_IbspectionDetails.addTarget(self, action: #selector(self.actionWithIbspectionDetails(sender:)), for: .touchUpInside)
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 365
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
   
    @objc func actionWithPDF(sender : UIButton){
        let dictData =   removeNullFromDict(dict: (aryTblList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        
        
        let strPath = "\(dictData.value(forKey: "InvoicePath")!)"
        if(strPath == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
        }else{
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + strPath
            let strUrlwithString = strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let url = URL(string: strUrlwithString)
            let safariVC = SFSafariViewController(url: url!)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
        
        
    }
       @objc func actionWithIbspectionDetails(sender : UIButton){
          
          
          if(self.indexTag != "\(sender.tag)"){
              self.str_Inspection = true
              self.str_Graph = false

          }else{
             self.str_Inspection == true ? self.str_Inspection = false : (self.str_Inspection = true)
          }
          self.indexTag = "\(sender.tag)"
          self.tvList.reloadData()
   
          
      }
      @objc func actionWithGraph(sender : UIButton){
          
          if(self.indexTag != "\(sender.tag)"){
              self.str_Graph = true
              self.str_Inspection = false
              
          }else{
              self.str_Graph == true ? self.str_Graph = false : (self.str_Graph = true)
          }
          self.indexTag = "\(sender.tag)"
          self.tvList.reloadData()
          
      }
    
}

// MARK: -
// MARK: -ServiceHistoryCell
class ServiceHistory_iPhoneCell: UITableViewCell {
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Order: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_Service: UILabel!

    @IBOutlet weak var lbl_Route: UILabel!
    @IBOutlet weak var lbl_Empolyee: UILabel!
    @IBOutlet weak var lbl_ScheduleStartDate: UILabel!
    @IBOutlet weak var btn_PDF: UIButton!
    @IBOutlet weak var btn_Graph: UIButton!
    @IBOutlet weak var btn_IbspectionDetails: UIButton!
    @IBOutlet weak var btn_height: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_ProductsName: UILabel!
    @IBOutlet weak var lbl_Devices: UILabel!
    @IBOutlet weak var txtTechComment: UITextView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
   
}

// MARK: -
// MARK: - ServiceHistory_Filter_iPadProtocol
extension ServiceHistory_iPhoneVC: ServiceHistory_Filter_iPhone_Protocol
{
    func getDataServiceHistory_Filter_iPhone_Protocol(dict: NSDictionary, tag: Int) {
        if(tag == 1){
            let id =  "\(dict.value(forKey: "id")!)"
            if(id == "1"){ // For All
                aryTblList = NSMutableArray()
                aryTblList = aryServiceHistoryData
                self.tvList.reloadData()
            }
            else if(id == "2"){ // Only Termite
               aryTblList = NSMutableArray()
                for item in aryServiceHistoryData {
                    if "\((item as AnyObject).value(forKey: "DepartmentType")!)" == "Termite"{
                        aryTblList.add(item)
                    }
                }
                self.tvList.reloadData()
            }
        }
    }
}

// MARK: -
// MARK: - API Calling
extension ServiceHistory_iPhoneVC{
    
    @objc func RefreshloadData() {
        
        if (isInternetAvailable()){
            
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.getService_HistoryList()

            })

        }
        
    }
    
    func getService_HistoryList () {
        if(isInternetAvailable()){
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + UrlMechanicalServiceHistory + "\(self.dictLoginData.value(forKeyPath: "Company.CompanyKey")!)&AccountNo=" + strAcctNo
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                if(Status)
                {
                   
                    if(Response["data"] is NSArray){
                        self.aryServiceHistoryData = NSMutableArray()
                        self.aryTblList = NSMutableArray()

                        self.aryServiceHistoryData = ((Response["data"] as! NSArray).mutableCopy()as! NSMutableArray)
                        self.aryTblList = ((Response["data"] as! NSArray).mutableCopy()as! NSMutableArray)
                        
                        /*var arrData = Response.value(forKey: "data") as! NSArray
                        arrData = arrData.reversed() as NSArray
                        self.aryServiceHistoryData = arrData.mutableCopy() as! NSMutableArray
                        self.aryTblList =  self.aryServiceHistoryData*/
                        
                        self.tvList.reloadData()
                        
                    }
                    DispatchQueue.main.async {
                        FTIndicator.dismissProgress()
                        self.refresher.endRefreshing()
                    }
                }
                else
                {
                    
                    DispatchQueue.main.async {
                        FTIndicator.dismissProgress()
                        self.refresher.endRefreshing()
                    }
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }else{
            
            DispatchQueue.main.async {
                FTIndicator.dismissProgress()
                self.refresher.endRefreshing()
            }
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    func Call_LoadDataAPI () {
          if(isInternetAvailable()){
              FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            //http://psrsstaging.pestream.com//api/MobileToServiceAuto/GetWorkOrderLoadDataExtSer?Type=both&WorkOrderId=28281
            var type = ""
            if(str_Inspection == true && str_Graph == true){
                type = "both"
            }else if(str_Inspection == true){
               type = "inspection"
            }else if(str_Graph == true){
               type = "graph"
            }
            let dictData =   removeNullFromDict(dict: (aryTblList.object(at:Int(indexTag)!)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            let strWorkOrderID = "\(dictData.value(forKey: "WorkorderId")!)"
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + UrlServiceHistory_LoadData + "\(type)&WorkOrderId=" + strWorkOrderID
              
              WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                  FTIndicator.dismissProgress()
                print(Response)

                  if(Status)
                  {
                      
                      
                  }
                  else
                  {
                      showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                  }
              }
          }else{
              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
          }
      }
}

// MARK: -
// MARK: -----UITableViewDelegate

extension ServiceHistory_iPhoneVC: SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
           //dismiss(animated: true)
       }
}
