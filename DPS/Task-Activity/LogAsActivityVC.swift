//
//  LogAsActivityVC.swift
//  DPS
//
//  Created by Saavan Patidar on 14/04/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  CopyRight 2020 Saavan 

import UIKit

class LogAsActivityVC: UIViewController {

    // MARK: - -----------------------------------Variables -----------------------------------

    var leadTaskId = ""
    var dictLeadTaskData = NSDictionary()
    var dictLoginData = NSDictionary()
    var logTypeId = ""
    var dictOfAssociations = NSMutableDictionary()
    var strType = ""
    var strRefType = ""
    var strRefTypeId = ""
    var dictAssociationById = NSMutableDictionary()
    var loader = UIAlertController()

    // MARK: - -----------------------------------Outlets -----------------------------------

    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnLogAsActivity: UIButton!
    @IBOutlet weak var btnCreateFollowUp: UIButton!
    @IBOutlet weak var txtfld_Agenda: ACFloatingTextField!
    @IBOutlet weak var txtfld_ActvityType: ACFloatingTextField!
    @IBOutlet weak var constHghtLogAsActivityView: NSLayoutConstraint!
    @IBOutlet weak var constHghtBtnCreatFollowUp: NSLayoutConstraint!
    @IBOutlet weak var constHghtBtnLogAsActvity: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!

    // MARK: - -----------------------------------View Life Cycle -----------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")

        UserDefaults.standard.set(true, forKey: "RefreshTasks_CompanyDetails")
       
        // Do any additional setup after loading the view.
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        btnLogAsActivity.setImage(UIImage(named: "uncheck.png"), for: .normal)
        btnCreateFollowUp.setImage(UIImage(named: "uncheck.png"), for: .normal)
        constHghtLogAsActivityView.constant = 0.0
        btnContinue.layer.cornerRadius = btnContinue.frame.size.height/2
        btnContinue.layer.borderWidth = 0
        
        if strType == "LogAsActivity" {
            
            lblTitle.text = "Log As Activity"
            
            constHghtLogAsActivityView.constant = DeviceType.IS_IPAD ? 150 : 98.0
            constHghtBtnCreatFollowUp.constant = 0.0
            constHghtBtnLogAsActvity.constant = 0.0
            btnLogAsActivity.isUserInteractionEnabled = false
            btnLogAsActivity.setImage(UIImage(named: "checked.png"), for: .normal)

            // call Api to get Associations
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                
                self.getAssocaitionAPI(EntityId: self.strRefTypeId, EntityType: self.strRefType)

            }
            
            
        } else {
            
            lblTitle.text = "Log As Activity/Create Follow Up"

            btnLogAsActivity.isUserInteractionEnabled = true
            constHghtBtnCreatFollowUp.constant = 50.0
            constHghtBtnLogAsActvity.constant = 50.0

            if dictLeadTaskData.count == 0 {
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                    
                    self.callAPIToGetTaskDetailById(strId: self.leadTaskId ,isCreateFollowUpLocal: false)

                }
                
            }
            
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if (nsud.value(forKey: "TaskFollowUpCreated") != nil)
        {
            
            let isAudio = nsud.bool(forKey: "TaskFollowUpCreated")
            
            if isAudio
            {
                
                nsud.set(false, forKey: "TaskFollowUpCreated")
                nsud.synchronize()
                catchNotificationCreatedFollowUp()
                
            }
            
        }
        
    }
    
    // MARK: - -----------------------------------Button Actions -----------------------------------

    @IBAction func action_CreateFollowUp(_ sender: UIButton) {
        
        self.view.endEditing(true)

        if(sender.currentImage == UIImage(named: "checked.png"))
        {
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        else
        {
            sender.setImage(UIImage(named: "checked.png"), for: .normal)

        }
        
    }
    
    @IBAction func action_LogAsActivity(_ sender: UIButton) {
        
        self.view.endEditing(true)

        if(sender.currentImage == UIImage(named: "checked.png"))
        {
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
            constHghtLogAsActivityView.constant = 0.0

        }
        else
        {
            sender.setImage(UIImage(named: "checked.png"), for: .normal)
            constHghtLogAsActivityView.constant =  DeviceType.IS_IPAD ? 150 : 98.0

        }
        
    }
    
    @IBAction func action_Continue(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if strType == "LogAsActivity" {

            if txtfld_ActvityType.text?.count == 0 {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please select actvity log type", viewcontrol: self)
            } else if txtfld_Agenda.text?.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please enter activity", viewcontrol: self)
                
            } else {
                
                // call Api to Only Add Activity
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.logAsActivityAPI()

                }
                
            }
            
        }else{
            
            if(btnLogAsActivity.currentImage == UIImage(named: "checked.png"))
            {
                
                // first Log As Activity
                
                if txtfld_ActvityType.text?.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please select actvity log type", viewcontrol: self)
                    
                } else if txtfld_Agenda.text?.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please enter activity", viewcontrol: self)
                    
                } else {
                    
                    if(btnCreateFollowUp.currentImage == UIImage(named: "checked.png")){
                        
                        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                        self.present(loader, animated: false, completion: nil)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                            self.AddActivityAPI(createFollowUp: true)

                        }
                        
                    }else{
                        
                        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                        self.present(loader, animated: false, completion: nil)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                            self.AddActivityAPI(createFollowUp: false)

                        }
                        
                    }

                }
                
            } else if(btnCreateFollowUp.currentImage == UIImage(named: "checked.png")){
                
                if dictLeadTaskData.count > 0 {
                    
                    self.gotoCreateFollowUp(dictData: dictLeadTaskData)
                    
                } else {
                    
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        
                        self.callAPIToGetTaskDetailById(strId: self.leadTaskId ,isCreateFollowUpLocal: true)

                    }
                    
                }
                
            }else{
                
                self.navigationController?.popViewController(animated: false)

            }
            
        }
        
    }
    
    @IBAction func action_ActivityType(_ sender: UIButton) {
        self.view.endEditing(true)

        let dictTotalLeadCountResponse = (nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary).mutableCopy()as! NSMutableDictionary

        let aryTemp = dictTotalLeadCountResponse.value(forKey: "ActivityLogTypeMasters") as! NSArray
        let resultPredicate = NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 403
            openTableViewPopUp(tag: 403, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        } else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    
    @IBAction func action_Back(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    // MARK: - -----------------------------------API Calling Functions -----------------------------------

    
    fileprivate func callAPIToGetTaskDetailById(strId : String, isCreateFollowUpLocal : Bool){
        
        var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTaskById + strId
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "CRMConactNew_ContactDetails_About") { (response, status) in
            
            self.loader.dismiss(animated: false) {

                if(status)
                {
                    if((response.value(forKey: "data") as! NSDictionary).count > 0)
                    {

                        var dictTaskDetailData = (response.value(forKey: "data") as! NSDictionary)
                       
                        dictTaskDetailData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictTaskDetailData as! [AnyHashable : Any]))! as NSDictionary
                        
                        // goto create follow up
                        
                        if isCreateFollowUpLocal {
                            
                            self.gotoCreateFollowUp(dictData: dictTaskDetailData)

                        } else {
                            
                            self.dictLeadTaskData = dictTaskDetailData
                            
                        }
                        
                    }
                    else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }

            }

        }
    }
    
    func AddActivityAPI(createFollowUp : Bool){
        
        let datee = Global().strCurrentDateFormatted("MM/dd/yyyy", "")
        
        let dictToSend = NSMutableDictionary()
        dictToSend.setValue("0", forKey: "Id")
        dictToSend.setValue(datee, forKey: "FromDate")
        dictToSend.setValue(datee, forKey: "ToDate")
        //dictToSend.setValue("\(dictLoginData.value(forKey: "EmployeeId")!)", forKey: "Participants")
        
        let arrOfParticipantsList = "\(dictLoginData.value(forKey: "EmployeeId")!)".components(separatedBy: ",")
        
        dictToSend.setValue(arrOfParticipantsList, forKey: "ParticipantsList")
        
        dictToSend.setValue(logTypeId, forKey: "LogTypeId")
        
        dictToSend.setValue("\(txtfld_Agenda.text!)", forKey: "Agenda")
        dictToSend.setValue("", forKey: "Address1")
        dictToSend.setValue("", forKey: "Address2")
        dictToSend.setValue("", forKey: "CityName")
        dictToSend.setValue("", forKey: "StateId")
        dictToSend.setValue("", forKey: "CountryId")
        dictToSend.setValue("", forKey: "ZipCode")
        
        dictToSend.setValue(Global().getModifiedDate(), forKey: "CreatedDate")
        dictToSend.setValue(NSArray(), forKey: "ActivityLogTypeMasterExtDc")
        dictToSend.setValue("", forKey: "RefType")
        dictToSend.setValue("", forKey: "RefId")
        dictToSend.setValue("", forKey: "ActivityId")
        dictToSend.setValue(NSArray(), forKey: "ActivityCommentExtDcs")
        dictToSend.setValue(NSArray(), forKey: "LeadExtDcs")
        dictToSend.setValue(NSArray(), forKey: "EmployeeExtDcs")
        dictToSend.setValue("\(dictLoginData.value(forKey: "CreatedBy")!)", forKey: "CreatedBy")
        dictToSend.setValue(Global().getModifiedDate(), forKey: "ModifiedDate")
        dictToSend.setValue("", forKey: "ModifiedBy")
        dictToSend.setValue("", forKey: "ChildTasks")
        dictToSend.setValue(Global().strCurrentDateFormatted("HH:mm", ""), forKey: "ActivityTime")
        
        
         let strACID = "\(dictLeadTaskData.value(forKey: "AccountId")!)"
         let strLeadId = "\(dictLeadTaskData.value(forKey: "LeadId")!)"
         let strWebLeadId = "\(dictLeadTaskData.value(forKey: "WebLeadId")!)"
         let strCrmContactId = "\(dictLeadTaskData.value(forKey: "CrmContactId")!)"
         let strCrmCompanyId = "\(dictLeadTaskData.value(forKey: "CrmCompanyId")!)"
         
         dictToSend.setValue(strACID, forKey: "AccountId")
         dictToSend.setValue(strLeadId, forKey: "LeadId")
         dictToSend.setValue(strWebLeadId, forKey: "WebLeadId")
         dictToSend.setValue(strCrmContactId, forKey: "CrmContactId")
         dictToSend.setValue(strCrmCompanyId, forKey: "CrmCompanyId")
   
         dictToSend.setValue("true", forKey: "IsSystem")
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddUpdateActivityGlobal
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            print(jsonString)
        }
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        let strTypee = "addActivity"
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            self.loader.dismiss(animated: false) {

                if(success)
                {
                    print((response as NSDictionary?)!)

                    if createFollowUp {
                        
                        let alertCOntroller = UIAlertController(title: "Message", message: "Activity Logged Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                                        
                            UserDefaults.standard.set(true, forKey: "isActivityAddedUpdated")
                            UserDefaults.standard.set(true, forKey: "isTaskAddedUpdated")
                            UserDefaults.standard.set(true, forKey: "RefreshTimeline_ContactDetails")
                            UserDefaults.standard.set(true, forKey: "RefreshTimeline_CompanyDetails")
                            
                            if self.dictLeadTaskData.count > 0 {
                                
                                self.gotoCreateFollowUp(dictData: self.dictLeadTaskData)
                                
                            } else {
                                
                                self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                self.present(self.loader, animated: false, completion: nil)
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    
                                    self.callAPIToGetTaskDetailById(strId: self.leadTaskId ,isCreateFollowUpLocal: true)

                                }
                                
                            }
                            
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertCOntroller = UIAlertController(title: "Message", message: "Activity Logged Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            UserDefaults.standard.set(true, forKey: "isActivityAddedUpdated")
                            UserDefaults.standard.set(true, forKey: "isTaskAddedUpdated")
                            
                            self.navigationController?.popViewController(animated: false)
                            
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }
                
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
            
        }
    }
    
     func logAsActivityAPI() {
         
         let datee = Global().strCurrentDateFormatted("MM/dd/yyyy", "")
         
         let dictToSend = NSMutableDictionary()
         dictToSend.setValue("0", forKey: "Id")
         dictToSend.setValue(datee, forKey: "FromDate")
         dictToSend.setValue(datee, forKey: "ToDate")
         //dictToSend.setValue("\(dictLoginData.value(forKey: "EmployeeId")!)", forKey: "Participants")
        
        let arrOfParticipantsList = "\(dictLoginData.value(forKey: "EmployeeId")!)".components(separatedBy: ",")
        
        dictToSend.setValue(arrOfParticipantsList, forKey: "ParticipantsList")
    
         dictToSend.setValue(logTypeId, forKey: "LogTypeId")
         
         dictToSend.setValue("\(txtfld_Agenda.text!)", forKey: "Agenda")
         dictToSend.setValue("", forKey: "Address1")
         dictToSend.setValue("", forKey: "Address2")
         dictToSend.setValue("", forKey: "CityName")
         dictToSend.setValue("", forKey: "StateId")
         dictToSend.setValue("", forKey: "CountryId")
         dictToSend.setValue("", forKey: "ZipCode")
         
         dictToSend.setValue(Global().getModifiedDate(), forKey: "CreatedDate")
         dictToSend.setValue(NSArray(), forKey: "ActivityLogTypeMasterExtDc")
         dictToSend.setValue("", forKey: "RefType")
         dictToSend.setValue("", forKey: "RefId")
         dictToSend.setValue("", forKey: "ActivityId")
         dictToSend.setValue(NSArray(), forKey: "ActivityCommentExtDcs")
         dictToSend.setValue(NSArray(), forKey: "LeadExtDcs")
         dictToSend.setValue(NSArray(), forKey: "EmployeeExtDcs")
         dictToSend.setValue("\(dictLoginData.value(forKey: "CreatedBy")!)", forKey: "CreatedBy")
         dictToSend.setValue(Global().getModifiedDate(), forKey: "ModifiedDate")
         dictToSend.setValue("", forKey: "ModifiedBy")
         dictToSend.setValue("", forKey: "ChildTasks")
         dictToSend.setValue(Global().strCurrentDateFormatted("HH:mm", ""), forKey: "ActivityTime")
         
        var strACID = ""
        var strLeadId = ""
        var strWebLeadId = ""
        var strCrmContactId = ""
        var strCrmCompanyId = ""
        
        if dictAssociationById.count > 0 {
            
            strACID = "\(dictAssociationById.value(forKey: "AccountId")!)"
            strLeadId = "\(dictAssociationById.value(forKey: "LeadId")!)"
            strWebLeadId = "\(dictAssociationById.value(forKey: "WebLeadId")!)"
            strCrmContactId = "\(dictAssociationById.value(forKey: "CrmContactId")!)"
            strCrmCompanyId = "\(dictAssociationById.value(forKey: "CrmCompanyId")!)"
            
        }
          
        dictToSend.setValue(strACID, forKey: "AccountId")
        dictToSend.setValue(strLeadId, forKey: "LeadId")
        dictToSend.setValue(strWebLeadId, forKey: "WebLeadId")
        dictToSend.setValue(strCrmContactId, forKey: "CrmContactId")
        dictToSend.setValue(strCrmCompanyId, forKey: "CrmCompanyId")
    
        dictToSend.setValue("true", forKey: "IsSystem")

         let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddUpdateActivityGlobal
         var jsonString = String()
         
         if(JSONSerialization.isValidJSONObject(dictToSend) == true)
         {
             let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
             jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
             print(jsonString)
         }
         let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
         let strTypee = "addActivity"
         Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
             
             self.loader.dismiss(animated: false) {

                 if(success)
                 {
                     print((response as NSDictionary?)!)

                     let alertCOntroller = UIAlertController(title: "Message", message: "Activity Logged Successfully", preferredStyle: .alert)
                     
                     let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                         
                        UserDefaults.standard.set(true, forKey: "isActivityAddedUpdated")
                        UserDefaults.standard.set(true, forKey: "isTaskAddedUpdated")
                        UserDefaults.standard.set(true, forKey: "RefreshTimeline_ContactDetails")
                        UserDefaults.standard.set(true, forKey: "RefreshTimeline_CompanyDetails")

                         self.navigationController?.popViewController(animated: false)
                         
                     })
                     
                     alertCOntroller.addAction(alertAction)
                     self.present(alertCOntroller, animated: true, completion: nil)
                 
                 }else{
                     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                 }
             }
         }
     }
    
    func getAssocaitionAPI(EntityId: String ,EntityType : String) {
        
        if(isInternetAvailable()){

            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAssociatedEntitiesForTaskActivityAssociations + "EntityType=\(EntityType)&EntityId=\(EntityId)"
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {

                    if(Status)
                    { // For add
                        
                        if Response["data"] is NSDictionary {
                            
                            let dictResponse = removeNullFromDict(dict: (Response["data"] as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                            
                            self.dictAssociationById = dictResponse
                            
                            if EntityType == "CrmCompany" {
                                
                                self.dictAssociationById.setValue(EntityId, forKey: "CrmCompanyId")
                                
                            }
                            
                            if EntityType == "CrmContact" {
                                
                                self.dictAssociationById.setValue(EntityId, forKey: "CrmContactId")

                            }
                            
                        }

                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
            }
        }else{
            
            self.loader.dismiss(animated: false) {

                showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)

            }
        }
        
    }
    
    // MARK: - -----------------------------------Functions -----------------------------------

    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem

        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
             vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func gotoCreateFollowUp(dictData : NSDictionary) {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(2, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountId")!)", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadId")!)", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadId")!)", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactId")!)", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyId")!)", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountDisplayName")!)", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadName")!)", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadName")!)", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactName")!)", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyName")!)", forKeyPath: "Pre_CrmCompanyName")
        
        dictTaskDetailsData.addEntries(from: dictData as! [AnyHashable : Any])

        
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)

    }
    
    func catchNotificationCreatedFollowUp() {
                          
        nsud.set(false, forKey: "TaskFollowUpCreated")
        nsud.synchronize()
        self.navigationController?.popViewController(animated: false)

    }
    
}


// MARK: -
// MARK: - Selection CustomTableView
extension LogAsActivityVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 403)
        {
            if dictData.count == 0 {
                
                txtfld_ActvityType.text = ""
                logTypeId = ""
                
            }else{
                
                txtfld_ActvityType.text = "\(dictData.value(forKey: "Name")!)"
                logTypeId = "\(dictData.value(forKey: "LogTypeId")!)"
                
            }
        }
        
    }
    
}
