//
//  CommentList_iPhoneVC.swift
//  DPS
//
//  Created by Navin Patidar on 3/20/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class CommentList_iPhoneVC: UIViewController {
    
    var  aryCommentList = NSArray()
    var lbl = UILabel()
    var loader = UIAlertController()

    @IBOutlet private weak var tvList: UITableView! {
           didSet {
               tvList.tableFooterView = UIView()
               tvList.estimatedRowHeight = 50
           }
       }
    
    
    var iD_PreviousView = ""
    
    // MARK:
    // MARK: ----------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(iD_PreviousView.count != 0){
            
            if (isInternetAvailable()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    self.getCommentListData(id: self.iD_PreviousView)

                })

            }else{
                
                self.noDataLbl()
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }
    // MARK:
    // MARK: ----------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
        //self.dismiss(animated: false, completion: nil)
    }
    @IBAction func actionOnAddComment(_ sender: UIButton) {
        self.view.endEditing(true)
        goToAddComment()
    }
}
// MARK: -
// MARK: - API Calling
extension CommentList_iPhoneVC {
    
    func goToAddComment() {
        
           let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
        
        vc.strLeadType = "Activity"
        vc.strRefId = self.iD_PreviousView
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func noDataLbl() {
        
        self.tvList.isHidden = true
        
        lbl.removeFromSuperview()
        
        lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        lbl.center = self.view.center
        lbl.text = "No Data Found"
        lbl.textAlignment = .center
        lbl.textColor = UIColor.lightGray
        lbl.font = UIFont.systemFont(ofSize: 20)
      //  self.view.addSubview(lbl)
        
    }
    
    
    func getCommentListData(id: String)  {
        
        self.tvList.isHidden = false

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetActivityComments + id
                
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    if (Response["data"] is NSArray)
                    {
                        self.aryCommentList = (Response.value(forKey: "data") as! NSArray)
                        self.tvList.reloadData()
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
  
        }
        
    }
}

// MARK: -
// MARK: -----------UITableViewDelegate

extension CommentList_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryCommentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath as IndexPath) as! CommentCell
//        cell.lblCommentMessage.text = "The coronavirus COVID-19 is affecting 192 countries and territories around the world and 1 international conveyance (the Diamond Princess cruise ship harbored in Yokohama, Japan). The day is reset after midnight GMT+0."
//        cell.lblCommentBy.text = "By: WHO"
//        cell.lblCommentDate.text = "22:2:2020 11:00 PM"
        
          cell.ConfigureCell(index: indexPath, dict: aryCommentList.object(at: indexPath.row)as! NSDictionary)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

// MARK: -
// MARK: -OtherCell
class CommentCell: UITableViewCell {
    
    @IBOutlet weak var lblCommentMessage: UILabel!
    @IBOutlet weak var lblCommentBy: UILabel!
    @IBOutlet weak var lblCommentDate: UILabel!
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    func ConfigureCell(index: IndexPath , dict : NSDictionary) {
        self.lblCommentMessage.text = "\(dict.value(forKey: "Comment")!)"
        self.lblCommentBy.text = Global().getEmployeeName(viaEmployeeID: "\(dict.value(forKey: "EmployeeId")!)")
        self.lblCommentDate.text = Global().convertDate("\(dict.value(forKey: "ClientCreatedDate")!)")
    }
    
}
