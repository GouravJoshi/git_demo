//
//  AddTaskVC_iPhoneVC.swift
//  DPS
//
//  Created by Navin Patidar on 3/18/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar
//  peSTream 2020
//  Saavan Patidar2020

import UIKit

protocol AddTaskAssociateProtocol : class
{
    
    func getDataAddTaskAssociateProtocol(notification : NSDictionary ,tag : Int)
    
}

class AddTaskVC_iPhoneVC: UIViewController {
    
    // MARK:
    // MARK: ----------IBOutlet
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtfld_SelectTemplate: ACFloatingTextField!
    @IBOutlet weak var txtfld_TaskName: ACFloatingTextField!
    @IBOutlet weak var txtfld_Comment: ACFloatingTextField!
    @IBOutlet weak var txtfld_SelectTaskType: ACFloatingTextField!
    @IBOutlet weak var txtfld_DueDate: ACFloatingTextField!
    @IBOutlet weak var txtfld_DueTime: ACFloatingTextField!
    @IBOutlet weak var txtfld_ReminderDate: ACFloatingTextField!
    @IBOutlet weak var txtfld_ReminderTime: ACFloatingTextField!
    @IBOutlet weak var txtfld_AssignedTo: ACFloatingTextField!
    @IBOutlet weak var txtfld_Urgency: ACFloatingTextField!
    @IBOutlet weak var txtfld_EndDate: ACFloatingTextField!
    
    @IBOutlet weak var btnSave: ButtonWithShadow!
    
    @IBOutlet weak var heightReminderDate: NSLayoutConstraint!
    @IBOutlet weak var topAssignTo: NSLayoutConstraint!
    @IBOutlet weak var heightTemplete: NSLayoutConstraint!
    
    
    
    @IBOutlet private weak var tvList: UITableView! {
        didSet {
            tvList.tableFooterView = UIView()
            tvList.estimatedRowHeight = 50
        }
    }
    var aryTblList = NSMutableArray()
    var dictLoginData = NSDictionary()
    var dictTotalLeadCountResponse = NSMutableDictionary()
    @objc var dictTaskDetailsData = NSMutableDictionary()
    
    
    
    var strComeFrom = ""
    var strLeadTaskId = ""
    var strStausRemove = 0
    
    var loader = UIAlertController()
    
    
    // MARK:
    // MARK: ----------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfld_TaskName.delegate = self
        self.SetupUI_SetInitialContain()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNewLeadNotification(notification:)), name: Notification.Name("AddTaskCRM_AssociationsFromNewLeadVC"), object: nil)
        
    }
    // MARK: - notifcation method
    
    @objc func methodOfReceivedNewLeadNotification(notification: Notification) {
        
        NotificationCenter.default.removeObserver(self, name:Notification.Name("AddTaskCRM_AssociationsFromNewLeadVC") , object: nil)
        if notification.userInfo != nil{
            let dict = notification.userInfo
            if dict != nil{
                getDataAddTaskAssociateProtocol(notification: dict! as NSDictionary, tag: 2)
            }
        }
    }
    // MARK: ----------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if(self.dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2){
            
            nsud.set(true, forKey: "TaskFollowUpCreated")
            nsud.synchronize()
            
        }
        self.navigationController?.popViewController(animated: false)
        
    }
    @IBAction func actionOnSelectTemplate(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTemp = (dictTotalLeadCountResponse.value(forKey: "TaskTemplateMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        let resultPredicate = NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 301
            openTableViewPopUp(tag: 301, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    @IBAction func actionOnSelectTaskType(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTemp = (dictTotalLeadCountResponse.value(forKey: "TaskTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        
        
        //IsSytem not show in case of Add only
        let resultPredicate = dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 0 ? NSPredicate(format: "IsActive == true AND IsSystem != true") : NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 309
            openTableViewPopUp(tag: 309, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    @IBAction func actionOnDueDate(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 302
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        let fromDate = dateFormatter.date(from:((txtfld_DueDate.text!.count) > 0 ? txtfld_DueDate.text! : result))!
        
        gotoDatePickerView(sender: sender, strType: "DateTime", minTime: true, dateToSet: fromDate)
        
    }
    
    @IBAction func actionOnEndDate(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 310
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        let fromDate = dateFormatter.date(from:((txtfld_EndDate.text!.count) > 0 ? txtfld_EndDate.text! : result))!
        
        gotoDatePickerView(sender: sender, strType: "DateTime", minTime: true, dateToSet: fromDate)
        
    }
    @IBAction func actionOnDueTime(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 303
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfld_DueTime.text?.count)! > 0 ? txtfld_DueTime.text! : result))!
        
        gotoDatePickerView(sender: sender, strType: "Time", minTime: self.dateCompare(date1: txtfld_DueDate.text!) , dateToSet: fromDate)
        
    }
    @IBAction func actionOnReminderDate(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 304
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfld_ReminderDate.text?.count)! > 0 ? txtfld_ReminderDate.text! : result))!
        
        gotoDatePickerView(sender: sender, strType: "Date", minTime: true , dateToSet: fromDate)
    }
    @IBAction func actionOnReminderTime(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 305
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfld_ReminderTime.text?.count)! > 0 ? txtfld_ReminderTime.text! : result))!
        
        gotoDatePickerView(sender: sender, strType: "Time", minTime:self.dateCompare(date1: txtfld_ReminderDate.text!) , dateToSet: fromDate)
        
    }
    @IBAction func actionOnAssignedTo(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
        let resultPredicate = NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 306
            openTableViewPopUp(tag: 306, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    @IBAction func actionOnUrgency(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let aryTemp = dictTotalLeadCountResponse.value(forKey: "PriorityMasters") as! NSArray
        let resultPredicate = NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 307
            openTableViewPopUp(tag: 307, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    @IBAction func actionOnSave(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if(validationOnView()){
            if(isInternetAvailable()){
                
                
                
                if( dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2){
                    
                    //DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.Add_UpdateTaskAPI(strFollowUpStatus: true)
                    
                    //}
                    
                }else{
                    
                    //DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.Add_UpdateTaskAPI(strFollowUpStatus: false)
                    
                    //}
                    
                }
                
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
        }
    }
}
// MARK: -
// MARK: -----------UITableViewDelegate

extension AddTaskVC_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTblList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath as IndexPath) as! OtherCell
        cell.ConfigureCell(index: indexPath, dict: aryTblList.object(at: indexPath.row)as! NSDictionary)
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(self.actionWithRemove(sender:)), for: .touchUpInside)
        if(cell.txtfld_AssocitedWith.text == ""){
            cell.btnRemove.isHidden = true
            cell.btnRemoveWidth.constant = 0.0
        }else
        {
            cell.btnRemove.isHidden = false
            cell.btnRemoveWidth.constant = 35.0
            if(indexPath.row == 0){
                if(self.strStausRemove == 1){
                    cell.btnRemove.isHidden = true
                    cell.btnRemoveWidth.constant = 0.0
                }
            }
        }
        cell.btnRemove.isHidden = false
        cell.btnRemoveWidth.constant = 35.0
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DeviceType.IS_IPAD ? 80.0 : 55.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.tvList.tag = indexPath.row
        switch indexPath.row {
        case 0:
            // For Account
            if self.strStausRemove == 0 {
                
                self.gotoAssociateAccount()
                
                //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "1194389", EntityType: enumRefTypeAccount, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            }
            break
        case 1:
            // For Opportunity
            
            self.gotoAssociateOpportunity()
            
            //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "249047", EntityType: enumRefTypeOpportunity, editStatus: true, followupStatus: false, userInfo: NSDictionary())
            
            break
        case 2:
            //For Lead
            
            self.gotoAssociateLead()
            
            //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "167918", EntityType: enumRefTypeWebLead, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            
            break
        case 3:
            //For Contact
            gotoAssociateContact()
            
            //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "1388512", EntityType: enumRefTypeCrmContact, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            break
        case 4:
            //For Company
            
            gotoAssociateCompany()
            
            //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "182021", EntityType: enumRefTypeCrmCompany, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            
            break
        default:
            break
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 45
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Associate with"
    }
    @objc func actionWithRemove(sender : UIButton){
        
        let dict = (self.aryTblList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        
        if "\(dict.value(forKey: "value") ?? "")".count > 0 {
            
            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                let dict = (self.aryTblList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                dict.setValue("", forKey: "id")
                dict.setValue("", forKey: "value")
                self.aryTblList.replaceObject(at: sender.tag, with: dict)
                self.tvList.reloadData()
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
}

// MARK: -
// MARK: -OtherCell
class OtherCell: UITableViewCell {
    @IBOutlet weak var txtfld_AssocitedWith: ACFloatingTextField!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnRemoveWidth: NSLayoutConstraint!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    func ConfigureCell(index: IndexPath , dict : NSDictionary) {
        
        self.txtfld_AssocitedWith.placeholder = "\(dict.value(forKey: "placeholder")!)"
        self.txtfld_AssocitedWith.text = "\(dict.value(forKey: "value")!)"
        // self.txtfld_AssocitedWith.tag = Int("\(dict.value(forKey: "id")!)")!
        
        
    }
    
}
// MARK: -
// MARK: - Selection CustomTableView
extension AddTaskVC_iPhoneVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 301)
        {
            // Task Template Section - (Comment, TaskType, DueDateTime, Priority(Urgency))
            
            
            if(dictData.count == 0){
                
                txtfld_SelectTemplate.text = ""
                txtfld_TaskName.text = ""
                txtfld_Comment.text = ""
                txtfld_Urgency.text = ""
                txtfld_Urgency.tag = 0
                txtfld_SelectTaskType.text = ""
                txtfld_SelectTaskType.tag = 0
            }else{
                
                txtfld_SelectTemplate.text = "\(dictData.value(forKey: "TaskName")!)"
                txtfld_TaskName.text = "\(dictData.value(forKey: "TaskName")!)"
                txtfld_Comment.text = "\(dictData.value(forKey: "Description")!)"
                
                var strDueDate = "\(dictData.value(forKey: "DueDays")!)"
                strDueDate = strDueDate == "" ? "0" : strDueDate
                if(Int(strDueDate) == nil){
                    strDueDate = "0"
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = DateFormatter.Style.short
                dateFormatter.timeStyle = DateFormatter.Style.none
                dateFormatter.dateFormat = "dd/MM/yy"
                let modifiedDate = Calendar.current.date(byAdding: .day, value: Int(strDueDate)!, to: Date())!
                txtfld_DueDate.text = dateTimeConvertor(str: dateFormatter.string(from: modifiedDate), formet: "dd/MM/yy" , strFormatToBeConverted: "MM/dd/yyyy")
                if strDueDate == "0" {
                    txtfld_DueDate.text = ""
                }
                
                //for time Custom Current
                
                if(txtfld_DueDate.text?.count != 0){
                    if("\(dictData.value(forKey: "DueTimeType")!)" == "Custom"){
                        
                        txtfld_DueDate.text = txtfld_DueDate.text! + " " + Global().convertTime("\(dictData.value(forKey: "DueTimeCustom")!)")
                    }else{
                        let date = Date()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "hh:mm a"
                        formatter.locale = Locale(identifier: "EST")
                        let result = formatter.string(from: date)
                        txtfld_DueDate.text = txtfld_DueDate.text! + " " + result
                        
                    }
                }
                
                //   TaskType & Priority
                
                let aryPriority = dictTotalLeadCountResponse.value(forKey: "PriorityMasters") as! NSArray
                let aryPriorityFilter = aryPriority.filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "PriorityId")!)".contains("\(dictData.value(forKey: "PriorityId")!)") && ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1") ))
                } as NSArray
                
                print(aryPriorityFilter)
                
                if(aryPriorityFilter.count != 0){
                    let dictData = aryPriorityFilter.object(at: 0)as! NSDictionary
                    txtfld_Urgency.text = "\(dictData.value(forKey: "Name")!)"
                    txtfld_Urgency.tag = Int("\(dictData.value(forKey: "PriorityId")!)")!
                }else{
                    txtfld_Urgency.text = ""
                    txtfld_Urgency.tag = 0
                }
                
                let aryTaskType = (dictTotalLeadCountResponse.value(forKey: "TaskTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
                let aryTaskTypeFilter = aryTaskType.filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "TaskTypeId")!)".contains("\(dictData.value(forKey: "TaskTypeId")!)") && ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1") ))
                } as NSArray
                print(aryTaskTypeFilter)
                if(aryTaskTypeFilter.count != 0){
                    let dictData = aryTaskTypeFilter.object(at: 0)as! NSDictionary
                    txtfld_SelectTaskType.text = "\(dictData.value(forKey: "Name")!)"
                    txtfld_SelectTaskType.tag = Int("\(dictData.value(forKey: "TaskTypeId")!)")!
                }else{
                    txtfld_SelectTaskType.text = ""
                    txtfld_SelectTaskType.tag = 0
                }
                
            }
            
        }
        else if(tag == 306)
        {
            if(dictData.count == 0){
                txtfld_AssignedTo.text = ""
                txtfld_AssignedTo.tag = 0
            }else{
                txtfld_AssignedTo.text = "\(dictData.value(forKey: "FullName")!)"
                txtfld_AssignedTo.tag = Int("\(dictData.value(forKey: "EmployeeId")!)")!
            }
            
        }
        else if(tag == 307)
        {
            if(dictData.count == 0){
                txtfld_Urgency.text = ""
                txtfld_Urgency.tag = 0
            }else{
                txtfld_Urgency.text = "\(dictData.value(forKey: "Name")!)"
                txtfld_Urgency.tag = Int("\(dictData.value(forKey: "PriorityId")!)")!
            }
        }
        else if(tag == 309)
        {
            if(dictData.count == 0){
                txtfld_SelectTaskType.text = ""
                txtfld_SelectTaskType.tag = 0
            }else{
                txtfld_SelectTaskType.text = "\(dictData.value(forKey: "Name")!)"
                txtfld_SelectTaskType.tag = Int("\(dictData.value(forKey: "TaskTypeId")!)")!
            }
            
            
        }
    }
    
}
extension AddTaskVC_iPhoneVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if(tag == 302)
        {
            txtfld_DueDate.text = strDate
        }
        if(tag == 303)
        {
            txtfld_DueTime.text = strDate
        }
        if(tag == 304)
        {
            txtfld_ReminderDate.text = strDate
        }
        if(tag == 305)
        {
            txtfld_ReminderTime.text = strDate
        }
        if(tag == 310)
        {
            txtfld_EndDate.text = strDate
        }
    }
}


// MARK:
// MARK: ----------Extra Function's

extension AddTaskVC_iPhoneVC{
    
    // MARK:
    // MARK: --------Set DAta On View--------
    func SetupUI_SetInitialContain() {
        
        strComeFrom = dictTaskDetailsData.value(forKey: "Pre_ViewComeFrom")as! String
        
        let id_AccountId = "\(dictTaskDetailsData.value(forKey: "Pre_AccountId") ?? "")" == "0" ? "" : "\(dictTaskDetailsData.value(forKey: "Pre_AccountId") ?? "")"
        let name_Account = "\(dictTaskDetailsData.value(forKey: "Pre_AccountName") ?? "")"
        
        let id_LeadId = "\(dictTaskDetailsData.value(forKey: "Pre_LeadId") ?? "")" == "0" ? "" : "\(dictTaskDetailsData.value(forKey: "Pre_LeadId") ?? "")"
        let name_LeadName = "\(dictTaskDetailsData.value(forKey: "Pre_LeadName") ?? "")"
        
        let id_WebLeadId = "\(dictTaskDetailsData.value(forKey: "Pre_WebLeadId") ?? "")" == "0" ? "" : "\(dictTaskDetailsData.value(forKey: "Pre_WebLeadId") ?? "")"
        let name_WebLeadName = "\(dictTaskDetailsData.value(forKey: "Pre_WebLeadName") ?? "")"
        
        let id_CrmContactId = "\(dictTaskDetailsData.value(forKey: "Pre_CrmContactId") ?? "")" == "0" ? "" : "\(dictTaskDetailsData.value(forKey: "Pre_CrmContactId") ?? "")"
        let name_CrmContactName = "\(dictTaskDetailsData.value(forKey: "Pre_CrmContactName") ?? "")"
        
        let id_CrmCompanyId = "\(dictTaskDetailsData.value(forKey: "Pre_CrmCompanyId") ?? "")" == "0" ? "" : "\(dictTaskDetailsData.value(forKey: "Pre_CrmCompanyId") ?? "")"
        let name_CrmCompanyName = "\(dictTaskDetailsData.value(forKey: "Pre_CrmCompanyName") ?? "")"
        
        
        aryTblList = [["value":name_Account,"placeholder":"Account","id":id_AccountId],["value":name_LeadName,"placeholder":"Opportunity","id":id_LeadId],["value":name_WebLeadName,"placeholder":"Lead","id":id_WebLeadId],["value":name_CrmContactName,"placeholder":"Contact","id":id_CrmContactId],["value":name_CrmCompanyName,"placeholder":"Company","id":id_CrmCompanyId]]
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        dictTotalLeadCountResponse = (nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary).mutableCopy()as! NSMutableDictionary
        // for update
        if(dictTaskDetailsData.value(forKey: "Pre_IsUpdate") == nil){
            dictTaskDetailsData.setValue(0, forKey: "Pre_IsUpdate")
        }
        if(dictTaskDetailsData.value(forKey: "Pre_IsUpdate") is String){
            dictTaskDetailsData.setValue(0, forKey: "Pre_IsUpdate")
        }
        
        if(dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 1 || dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2)
        {
            btnSave.setTitle("Save", for: .normal)
            lblTitle.text = "Update Task"
            
            if(dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2){
                
                btnSave.setTitle("Save", for: .normal)
                lblTitle.text = "Create FollowUp"
                
            }
            
            strLeadTaskId = "\(dictTaskDetailsData.value(forKey: "LeadTaskId") ?? "")"
            
            txtfld_Comment.text = "\(dictTaskDetailsData.value(forKey: "TaskDescription") ?? "")"//TaskDescription
            txtfld_TaskName.text = "\(dictTaskDetailsData.value(forKey: "TaskName") ?? "")"
            
            let PriorityID = "\(dictTaskDetailsData.value(forKey: "Priority") ?? "")"
            PriorityID == "" ? txtfld_Urgency.tag = 0 : (txtfld_Urgency.tag = Int(PriorityID)!)
            
            for item in getPriorityMaster()
            {
                if("\(((item as AnyObject)as! NSDictionary).value(forKey: "PriorityId") ?? "")" == PriorityID)
                {
                    
                    txtfld_Urgency.text = "\(((item as AnyObject)as! NSDictionary).value(forKey: "Name") ?? "")"
                    break
                }
            }
            let assignedToID = "\(dictTaskDetailsData.value(forKey: "AssignedTo") ?? "")"
            assignedToID == "" ? txtfld_AssignedTo.tag = 0 : (txtfld_AssignedTo.tag = Int(assignedToID)!)
            
            if(nsud.value(forKey: "EmployeeList") != nil){
                for item in nsud.value(forKey: "EmployeeList") as! NSArray
                {
                    if(assignedToID == "\(((item as AnyObject)as! NSDictionary).value(forKey: "EmployeeId")!)")
                    {
                        txtfld_AssignedTo.text  = "\(((item as AnyObject)as! NSDictionary).value(forKey: "FullName")!)"
                        
                        break
                    }
                }
            }
            
            
            //txtfld_LogType
            let TaskTypeId = "\(dictTaskDetailsData.value(forKey: "TaskTypeId")!)"
            TaskTypeId == "" ? txtfld_SelectTaskType.tag = 0 : (txtfld_SelectTaskType.tag = Int(TaskTypeId)!)
            let aryTemp = (dictTotalLeadCountResponse.value(forKey: "TaskTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            
            for item in aryTemp
            {
                if(TaskTypeId == "\(((item as AnyObject)as! NSDictionary).value(forKey: "TaskTypeId")!)")
                {
                    txtfld_SelectTaskType.text  = "\(((item as AnyObject)as! NSDictionary).value(forKey: "Name")!)"
                    
                    break
                }
            }
            
            var strDueDate = ""
            if(dictTaskDetailsData.value(forKey: "DueDate") is Date)
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                strDueDate = dateFormatter.string(from: dictTaskDetailsData.value(forKey: "DueDate")! as! Date)
            }
            else
            {
                strDueDate = "\(dictTaskDetailsData.value(forKey: "DueDate")!)"
            }
            let strTaskDate = Global().convertDate("\(strDueDate)")
            let strTaskTime = Global().convertTime("\(strDueDate)")
            
            
            
            
            if strTaskTime != nil && strTaskTime != "" {
                txtfld_DueDate.text = strTaskDate! + " " + strTaskTime!
            }else{
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm a"
                formatter.locale = Locale(identifier: "EST")
                txtfld_DueDate.text = strTaskDate! + " " + formatter.string(from: date)
            }
            
            
            
            
            
            var strEndDate = ""
            if(dictTaskDetailsData.value(forKey: "EndDate") is Date)
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                strEndDate = dateFormatter.string(from: dictTaskDetailsData.value(forKey: "EndDate")! as! Date)
            }
            else
            {
                strEndDate = "\(dictTaskDetailsData.value(forKey: "EndDate")!)"
            }
            if(strEndDate != ""){
                let strEndDateTask = Global().convertDate("\(strEndDate)")
                let strEndTime = Global().convertTime("\(strEndDate)")
                
                if strEndTime != nil && strEndTime != "" {
                    txtfld_EndDate.text = strEndDateTask! + " " + strEndTime!
                }else{
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "hh:mm a"
                    formatter.locale = Locale(identifier: "EST")
                    txtfld_EndDate.text = strEndDateTask! + " " + formatter.string(from: date)
                }
            }
            
            
            
            
            //            if(dictTaskDetailsData.value(forKey: "DueDate") is Date)
            //            {
            //                let dateFormatter = DateFormatter()
            //                dateFormatter.dateFormat = "MM/dd/yyyy"
            //
            //                let strdate = dateFormatter.string(from: dictTaskDetailsData.value(forKey: "DueDate")! as! Date)
            //
            //                txtfld_DueDate.text = strdate
            //            }
            //            else
            //            {
            //                txtfld_DueDate.text = Global().convertDate("\(dictTaskDetailsData.value(forKey: "DueDate")!)")
            //            }
            //
            //
            //            let strDueTime = Global().convertTime("\(dictTaskDetailsData.value(forKey: "DueDate")!)")
            //
            //            if(strDueTime == "00:00")
            //            {
            //                txtfld_DueTime.text = ""
            //            }
            //            else
            //            {
            //                txtfld_DueTime.text = strDueTime
            //            }
            
            
            let strReminderDate = Global().convertDate("\(dictTaskDetailsData.value(forKey: "ReminderDate")!)")
            
            txtfld_ReminderDate.text = strReminderDate
            
            
            let strReminderTime = Global().convertTime("\(dictTaskDetailsData.value(forKey: "ReminderDate")!)")
            
            if(strReminderTime == "00:00")
            {
                txtfld_ReminderTime.text = ""
            }
            else
            {
                txtfld_ReminderTime.text = strReminderTime
            }
            
            if(dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2){
                
                txtfld_ReminderTime.text = ""
                txtfld_ReminderDate.text = ""
                txtfld_DueTime.text = ""
                txtfld_DueDate.text = ""
                dictTaskDetailsData.setValue("Open", forKey: "Status")
                
            }
            //-reminderDate time hide
            /*topAssignTo.constant = 20.0
             heightReminderDate.constant = 45.0
             heightTemplete.constant = 0.0*/
            
            //-reminderDate time hide
            topAssignTo.constant = 0.0
            heightReminderDate.constant = 0.0
            heightTemplete.constant = 0.0
            
        }
        // for add
        else{
            btnSave.setTitle("Save", for: .normal)
            lblTitle.text = "Add Task"
            if(strComeFrom == "DashBoardView"){
                strLeadTaskId = ""
            }
            else if(strComeFrom == "WebLeadVC")
            {
                strLeadTaskId = ""
            }
            else if(strComeFrom == "OpportunityVC")
            {
                strLeadTaskId = ""
            }
            
            txtfld_AssignedTo.text = "\(dictLoginData.value(forKey: "EmployeeName")!)"
            txtfld_AssignedTo.tag = Int("\(dictLoginData.value(forKey: "EmployeeId")!)")!
            
            // call API to fetch Data for Associations
            
            if id_AccountId.count > 0 && id_AccountId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_AccountId, EntityType: enumRefTypeAccount, strName: name_Account)
                
            } else if id_WebLeadId.count > 0 && id_WebLeadId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_WebLeadId, EntityType: enumRefTypeWebLead, strName: name_WebLeadName)
                
            } else if id_LeadId.count > 0 && id_LeadId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_LeadId, EntityType: enumRefTypeOpportunity, strName: name_LeadName)
                
            } else if id_CrmContactId.count > 0 && id_CrmContactId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_CrmContactId, EntityType: enumRefTypeCrmContact, strName: name_CrmContactName)
                
            }else if id_CrmCompanyId.count > 0 && id_CrmCompanyId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_CrmCompanyId, EntityType: enumRefTypeCrmCompany, strName: name_CrmCompanyName)
                
            }
            
            //-----At a time of add Urgency set none
            let aryTemp = dictTotalLeadCountResponse.value(forKey: "PriorityMasters") as! NSArray
            let resultPredicate = NSPredicate(format: "IsActive == true")
            let arrayfilter = aryTemp.filtered(using: resultPredicate)
            for item in arrayfilter {
                if("\((item as AnyObject).value(forKey: "SysName")!)" == "None"){
                    txtfld_Urgency.text = "\((item as AnyObject).value(forKey: "Name")!)"
                    txtfld_Urgency.tag = Int("\((item as AnyObject).value(forKey: "PriorityId")!)")!
                }
            }
            
            
            //-reminderDate time hide
            topAssignTo.constant = 0.0
            heightReminderDate.constant = 0.0
            heightTemplete.constant = 45.0
        }
    }
    
    func setDataWhenComeFromAdd(dictResponse : NSMutableDictionary , strID : String ,strName : String )  {
        var strAccountID = "" , strCrmCompanyId = "" , strCrmContactId = "" ,strAccountName = "" , strCrmCompanyName = "" , strCrmContactName = ""
        
        if self.tvList.tag == 0 { //account 0 -> replace Conatact 3 & comapny 4
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            self.aryTblList.replaceObject(at: 0, with: ["value":strName,"placeholder":"Account","id":strID]) //Account
            //--Check If data is available then Add--
            if(strCrmContactId.count != 0){
                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
            }
            if(strCrmCompanyId.count != 0){
                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
            }
            
            
            
        }
        else if self.tvList.tag == 1 { // Opportunity 1 -> A/c 0 & Contact 3 & Comapny 4
            
            
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            
            self.aryTblList.replaceObject(at: 1, with: ["value":strName,"placeholder":"Opportunity","id":strID]) //Opportunity
            
            
            
            //--Check If data is available then Add--
            if(strAccountID.count != 0){
                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                
            }
            if(strCrmContactId.count != 0){
                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
            }
            if(strCrmCompanyId.count != 0){
                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
            }
        }
        
        else if self.tvList.tag == 2 { //web lead 2 - > Contact 3 & Compnay 4
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            
            self.aryTblList.replaceObject(at: 2, with: ["value":strName,"placeholder":"Lead","id":strID]) //Lead
            
            //--Check If data is available then Add--
            if(strCrmContactId.count != 0){
                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
            }
            if(strCrmCompanyId.count != 0){
                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
            }
        }
        
        else if self.tvList.tag == 3 { // Contact 3-> Company 4 & Account 0
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            
            self.aryTblList.replaceObject(at: 3, with: ["value":strName,"placeholder":"Contact","id":strID]) //Contact
            //--Check If data is available then Add--
            
            if(strAccountID.count != 0){
                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
            }
            if(strCrmCompanyId.count != 0){
                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
            }
            
        }
        
        else if self.tvList.tag == 4 { // Company 4  -> Contact 3 & A/C 0
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            self.aryTblList.replaceObject(at: 4, with: ["value":strName,"placeholder":"Company","id":strID]) //Comapny
            
            //--Check If data is available then Add--
            
            if(strAccountID.count != 0){
                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
            }
            if(strCrmContactId.count != 0){
                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
            }
        }
        self.strStausRemove = 0
        
        self.tvList.reloadData()
    }
    
    
    func setDataWhenComeFromEditFollowUP(dictResponse : NSMutableDictionary , strID : String ,strName : String )  {
        
        var strAccountID = "" , strCrmCompanyId = "" , strCrmContactId = "" ,strAccountName = "" , strCrmCompanyName = "" , strCrmContactName = ""
        
        let str_AccountId = "\((self.aryTblList.object(at: 0)as! NSDictionary).value(forKey: "id")!)"
        // let str_OpportunityId = "\((self.aryTblList.object(at: 1)as! NSDictionary).value(forKey: "id")!)"
        //   let str_LeadId = "\((self.aryTblList.object(at: 2)as! NSDictionary).value(forKey: "id")!)"
        let str_ContacttId = "\((self.aryTblList.object(at: 3)as! NSDictionary).value(forKey: "id")!)"
        let str_CompanyId = "\((self.aryTblList.object(at: 4)as! NSDictionary).value(forKey: "id")!)"
        
        //-----For Account 0 -> replace Conatact 3 & comapny 4
        if(self.tvList.tag == 0){
            
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            //Account
            self.aryTblList.replaceObject(at: 0, with: ["value":strName,"placeholder":"Account","id":strID])
            //check if Contact Company is blank then replace
            if(str_ContacttId == ""){
                if(strCrmContactId.count != 0){
                    self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                }
            }
            if(str_CompanyId == ""){
                if(strCrmCompanyId.count != 0){
                    self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                }
                
            }
        }
        //-----For Opportunity 1 -> A/c 0 & Contact 3 & Comapny 4
        
        
        else if (self.tvList.tag == 1){
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            //Opportunity
            self.aryTblList.replaceObject(at: 1, with: ["value":strName,"placeholder":"Opportunity","id":strID])
            
            //check if A/c 0 & Contact 3 & Comapny 4 is blank then replace
            if(str_AccountId == ""){
                if(strAccountID.count != 0){
                    self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID])
                }
                //Account
                //                                if(strAccountName.count == 0){
                //                                    self.strStausRemove = 0
                //                                }else{
                //                                    self.strStausRemove = 1
                //                                }
                
            }else{
                if(strAccountID.count != 0){
                    self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                    
                }
                
                
            }
            if(str_ContacttId == ""){
                if(strCrmContactId.count != 0){
                    self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                }
                
                
            }
            if(str_CompanyId == ""){
                if(strCrmCompanyId.count != 0){
                    self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                }
                
                
            }
        }
        
        
        
        //-----For web lead 2 - > Contact 3 & Compnay 4
        
        else if (self.tvList.tag == 2){
            
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            //Lead
            self.aryTblList.replaceObject(at: 2, with: ["value":strName,"placeholder":"Lead","id":strID])
            
            //check if Contact Company is blank then replace
            if(str_ContacttId == ""){
                if(strCrmContactId.count != 0){
                    self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                }
                
                
            }
            if(str_CompanyId == ""){
                if(strCrmCompanyId.count != 0){
                    self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                }
                
            }
        }
        //-----For Contact 3-> Company 4 & Account 0
        
        else if (self.tvList.tag == 3){
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            self.aryTblList.replaceObject(at: 3, with: ["value":strName,"placeholder":"Contact","id":strID]) //Contact
            
            //check if Account Company is blank then replace
            
            if(str_AccountId == ""){
                if(strAccountID.count != 0){
                    self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                }
                
            }
            if(str_CompanyId == ""){
                if(strCrmCompanyId.count != 0){
                    self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId])
                }
                
                
            }
        }
        //-----For Company 4  -> Contact 3 & A/C 0
        
        else if (self.tvList.tag == 4){
            //
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            //Comapny
            self.aryTblList.replaceObject(at: 4, with: ["value":strName,"placeholder":"Company","id":strID])
            //check if Account Contact is blank then replace
            
            if(str_AccountId == ""){
                if(strAccountID.count != 0){
                    self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                }
                
            }
            if(str_ContacttId == ""){
                if(strCrmContactId.count != 0){
                    self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                }
                
                
            }
            
            
        }
        self.tvList.reloadData()
        
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
            
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func gotoDatePickerView(sender: UIButton, strType:String , minTime : Bool , dateToSet:Date)  {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = minTime
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
    }
    
    
    func validationOnView() -> Bool {
        /*if(txtfld_SelectTemplate.text == "")
         {
         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Select Template", viewcontrol: self)
         return false
         }*/
        
        /*else if(txtfld_SelectTaskType.text == "")
         {
         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Task type", viewcontrol: self)
         return false
         
         }*/
        if(txtfld_TaskName.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter task name.", viewcontrol: self)
            return false
            
        }
        if(txtfld_DueDate.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter due date time.", viewcontrol: self)
            return false
            
        }
        //        else if(txtfld_DueTime.text == "")
        //        {
        //            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter due time.", viewcontrol: self)
        //            return false
        //
        //        }
        else if(txtfld_ReminderDate.text!.count > 0)
        {
            if(txtfld_ReminderTime.text == "")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter reminder time.", viewcontrol: self)
                return false
                
            }
            
        }
        /*else if(txtfld_ReminderDate.text == "")
         {
         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter reminder date.", viewcontrol: self)
         return false
         
         }
         else if(txtfld_ReminderTime.text == "")
         {
         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter reminder time.", viewcontrol: self)
         return false
         
         }*/
        else  if(txtfld_ReminderDate.text != "" && txtfld_ReminderTime.text != "")
        {
            let dateFormat = DateFormatter.init()
            dateFormat.dateFormat = "MM/dd/yyyyhh:mm a"
            
            let reminderFulDateTime = (txtfld_ReminderDate.text ?? "")! + (txtfld_ReminderTime.text ?? "")!
            
            let dueFullDateTime = (txtfld_DueDate.text ?? "")! + (txtfld_DueTime.text ?? "")!
            
            let reminderDate = dateFormat.date(from: reminderFulDateTime)
            
            let dueDate = dateFormat.date(from: dueFullDateTime)
            
            switch reminderDate!.compare(dueDate!) {
            
            case .orderedDescending:
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Reminder date & time should be less than due date & time", viewcontrol: self)
                return false
                
            case .orderedSame:
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Reminder date & time should be less than due date & time", viewcontrol: self)
                return false
                
            default:break
            }
        }
        else if(txtfld_AssignedTo.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select assign to", viewcontrol: self)
            return false
            
        }
        else if(txtfld_Urgency.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select urgency", viewcontrol: self)
            return false
            
        }
        
        return true
        
    }
    func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    fileprivate func gotoAssociateAccount(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateAccountVC") as! AssociateAccountVC
        controller.strFromWhere = "NotAsscoiate"
        controller.delegateTaskAssociate = self
        controller.selectionTag = 0
        controller.strFromWhereForAssociation = "AddTaskCRM_Associations"
        nsud.set(true, forKey: "isInitalAccount")
        nsud.synchronize()
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    fileprivate func gotoAssociateOpportunity(){
        
        nsud.set(true, forKey: "fromOpportunityAssociation")
        nsud.synchronize()
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
        controller.delegateTaskAssociate = self
        controller.selectionTag = 1
        controller.strTypeOfAssociations = "AddTaskCRM_Associations"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    fileprivate func gotoAssociateLead(){
        
        nsud.set(true, forKey: "fromWebLeadAssociation")
        nsud.synchronize()
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        controller.delegateTaskAssociate = self
        controller.selectionTag = 2
        controller.strTypeOfAssociations = "AddTaskCRM_Associations"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    fileprivate func gotoAssociateContact(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
        
        controller.strFromWhere = "NotAssociate"
        controller.delegateTaskAssociate = self
        controller.selectionTag = 3
        controller.strFromWhereForAssociation = "AddTaskCRM_Associations"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func gotoAssociateCompany(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        controller.strFromWhere = "NotAssociate"
        controller.delegateTaskAssociate = self
        controller.selectionTag = 4
        controller.strFromWhereForAssociation = "AddTaskCRM_Associations"
        //controller.strRefType = enumRefTypeCrmContact
        //controller.strRefId = "\(dictAbout.value(forKey: "CrmContactId") ?? "")"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    func dateCompare(date1: String) -> Bool {
        let dateFormat = DateFormatter.init()
        dateFormat.dateFormat = "MM/dd/yyyy"
        
        if(dateFormat.date(from: date1) == dateFormat.date(from: dateFormat.string(from: Date()))){
            return true
        }
        return false
    }
    
}

// MARK:
// MARK: --API Calling

extension AddTaskVC_iPhoneVC{
    func Add_UpdateTaskAPI(strFollowUpStatus : Bool){
        
        var dateTask = "" , timeTask = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let date = dateFormatter.date(from: txtfld_DueDate.text!)!
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateTask = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        timeTask = dateFormatter.string(from: date)
        
        let strFullDateTime = (dateTask) + " " + (getTimeWithouAMPM(strTime: (timeTask)))
        
        var strFullDateTimeReminder = ""
        
        if txtfld_ReminderDate.text!.count > 0 {
            
            strFullDateTimeReminder =  (txtfld_ReminderDate.text)! + " " + (getTimeWithouAMPM(strTime: (txtfld_ReminderTime.text)!))
            
        }
        
        var strStatus = "Open"
        
        //        if(dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Bool){
        //            strStatus =  "\(dictTaskDetailsData.value(forKey: "Status")!)"
        //        }
        
        if(dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 1){
            
            strStatus =  "\(dictTaskDetailsData.value(forKey: "Status")!)"
            
        }
        
        let dictToSend = NSMutableDictionary()
        
        dictToSend.setValue("0", forKey: "Id")
        dictToSend.setValue(strLeadTaskId, forKey: "LeadTaskId")
        dictToSend.setValue("", forKey: "RefId")
        dictToSend.setValue("", forKey: "RefType")
        dictToSend.setValue("\(txtfld_TaskName.text!)", forKey: "TaskName")
        dictToSend.setValue(strFullDateTime, forKey: "DueDate")
        dictToSend.setValue(strFullDateTimeReminder, forKey: "ReminderDate")
        dictToSend.setValue("\(txtfld_Comment.text!)", forKey: "Description")
        dictToSend.setValue("\(txtfld_AssignedTo.tag == 0 ? "" : "\(txtfld_AssignedTo.tag)")", forKey: "AssignedTo")
        dictToSend.setValue("\(txtfld_Urgency.tag == 0 ? "" : "\(txtfld_Urgency.tag)")", forKey: "Priority")
        
        
        if(txtfld_EndDate.text?.count != 0){
            var EndDateTask = "" , EndtimeTask = ""
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
            let date1 = dateFormatter.date(from: txtfld_EndDate.text!)!
            dateFormatter.dateFormat = "MM/dd/yyyy"
            EndDateTask = dateFormatter.string(from: date1)
            dateFormatter.dateFormat = "hh:mm a"
            EndtimeTask = dateFormatter.string(from: date1)
            let strFullEndDateTime = (EndDateTask) + " " + (getTimeWithouAMPM(strTime: (EndtimeTask)))
            dictToSend.setValue(strFullEndDateTime, forKey: "EndDate")
        }else{
            dictToSend.setValue("", forKey: "EndDate")
        }
        
        
        dictToSend.setValue(strStatus, forKey: "Status")
        dictToSend.setValue("\(dictLoginData.value(forKey: "CreatedBy")!)", forKey: "CreatedBy")
        dictToSend.setValue("", forKey: "Tags")
        dictToSend.setValue(NSMutableArray(), forKey: "ChildActivities")
        
        
        let strACID = "\((aryTblList.object(at: 0)as! NSDictionary).value(forKey: "id")!)"
        let strLeadId = "\((aryTblList.object(at: 1)as! NSDictionary).value(forKey: "id")!)"
        let strWebLeadId = "\((aryTblList.object(at: 2)as! NSDictionary).value(forKey: "id")!)"
        let strCrmContactId = "\((aryTblList.object(at: 3)as! NSDictionary).value(forKey: "id")!)"
        let strCrmCompanyId = "\((aryTblList.object(at: 4)as! NSDictionary).value(forKey: "id")!)"
        
        dictToSend.setValue(strACID == "0" ? "" : strACID, forKey: "AccountId")
        dictToSend.setValue(strLeadId == "0" ? "" : strLeadId, forKey: "LeadId")
        dictToSend.setValue(strWebLeadId == "0" ? "" : strWebLeadId, forKey: "WebLeadId")
        dictToSend.setValue(strCrmContactId == "0" ? "" : strCrmContactId, forKey: "CrmContactId")
        dictToSend.setValue(strCrmCompanyId == "0" ? "" : strCrmCompanyId, forKey: "CrmCompanyId")
        dictToSend.setValue("\(txtfld_SelectTaskType.tag == 0 ? "" : "\(txtfld_SelectTaskType.tag)")", forKey: "TaskTypeId")
        
        if(strFollowUpStatus){
            dictToSend.setValue("", forKey: "LeadTaskId")
            dictToSend.setValue(strLeadTaskId, forKey: "FollowUpFromTaskId")
            //FollowUpFromTaskId
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddUpdateTaskGlobal
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "AddTask"
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            self.loader.dismiss(animated: false) {
                if(success)
                {
                    
                    UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")
                    UserDefaults.standard.set(true, forKey: "RefreshTasks_CompanyDetails")
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedTask_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                    
                    UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")
                    
                    nsud.setValue(true, forKey: "isTaskAddedUpdated")
                    nsud.synchronize()
                    
                    if(self.dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 1){
                        
                        let alertCOntroller = UIAlertController(title: "Message", message: "Task Updated Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TaskUpdated_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            self.navigationController?.popViewController(animated: false)
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }else if(self.dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2){
                        
                        let alertCOntroller = UIAlertController(title: "Message", message: "Task followup created Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            nsud.set(true, forKey: "TaskFollowUpCreated")
                            nsud.synchronize()
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TaskFollowUpCreated_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            self.navigationController?.popViewController(animated: false)
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }
                    else
                    {
                        
                        let alertCOntroller = UIAlertController(title: "Message", message: "Task Added Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TaskAdded_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            self.navigationController?.popViewController(animated: false)
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            
        }
    }
    
    
    func callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: String ,EntityType : String,editStatus : Bool , followupStatus : Bool , userInfo : NSDictionary) {
        
        if(EntityId.count != 0 && EntityId != "<null>"){
            
            if(isInternetAvailable()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAssociatedEntitiesForTaskActivityAssociations + "EntityType=\(EntityType)&EntityId=\(EntityId)"
                
                WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                    
                    self.loader.dismiss(animated: false) {
                        if(Status)
                        {
                            let dictResponse = removeNullFromDict(dict: (Response["data"] as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                            
                            //let strID = "0" , strName = ""
                            //let strID = "\(userInfo.value(forKey: "")!)" , strName = "\(userInfo.value(forKey: "")!)"
                            
                            var strID = ""
                            var strName = ""
                            
                            let arrOfKeys = userInfo.allKeys as NSArray
                            
                            
                            if self.tvList.tag == 0{
                                
                                if arrOfKeys.contains("AccountId") {
                                    
                                    // Account
                                    strID = "\(userInfo.value(forKey: "AccountId")!)"
                                    strName = "\(userInfo.value(forKey: "AccountName")!)"
                                    
                                    let strName1 = "\(userInfo.value(forKey: "AccountName") ?? "")"
                                    
                                    if strName1.count == 0 {
                                        
                                        let strCrmContactName = "\(userInfo.value(forKey: "CrmContactName") ?? "")"
                                        
                                        if strCrmContactName.count == 0 {
                                            
                                            let strCrmCompanyName = "\(userInfo.value(forKey: "CrmCompanyName") ?? "")"
                                            
                                            if strCrmCompanyName.count == 0 {
                                                
                                                strName = " "
                                                
                                            }else{
                                                
                                                strName = "\(strCrmCompanyName)"
                                                
                                            }
                                            
                                        }else{
                                            
                                            strName = "\(strCrmContactName)"
                                            
                                        }
                                        
                                    }else{
                                        
                                        strName = "\(strName1)"
                                        
                                    }
                                    
                                }
                                
                            }else if self.tvList.tag == 1{
                                // Opportunity
                                
                                strID = "\(userInfo.value(forKey: "opportunityId")!)"
                                strName = "\(userInfo.value(forKey: "opportunityName")!)"
                                
                            }else if self.tvList.tag == 2{
                                // Lead
                                
                                strID = "\(userInfo.value(forKey: "leadId")!)"
                                strName = "\(userInfo.value(forKey: "leadName")!)"
                                
                            }else if self.tvList.tag == 3{
                                // Contact
                                
                                strID = "\(userInfo.value(forKey: "CrmContactId")!)"
                                strName = Global().strFullName(userInfo as? [AnyHashable : Any])
                                
                            }else if self.tvList.tag == 4{
                                // Company
                                
                                strID = "\(userInfo.value(forKey: "CrmCompanyId")!)"
                                strName = "\(userInfo.value(forKey: "Name")!)"
                                
                            }
                            
                            var strAccountID = "" , strCrmCompanyId = "" , strCrmContactId = "" ,strAccountName = "" , strCrmCompanyName = "" , strCrmContactName = ""
                            
                            if !(editStatus){
                                self.setDataWhenComeFromAdd(dictResponse: dictResponse, strID: strID, strName: strName)
                                
                            }else{ // for EDit
                                
                                
                                self.setDataWhenComeFromEditFollowUP(dictResponse: dictResponse, strID: strID, strName: strName)
                                
                            }
                            
                            
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }
                    
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            
        }
        
    }
    
    func callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: String ,EntityType : String, strName: String) {
        
        if(EntityId.count != 0 && EntityId != "<null>"){
            
            if(isInternetAvailable()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAssociatedEntitiesForTaskActivityAssociations + "EntityType=\(EntityType)&EntityId=\(EntityId)"
                
                WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                    self.loader.dismiss(animated: false) {
                        if(Status)
                        { // For add
                            
                            let dictResponse = removeNullFromDict(dict: (Response["data"] as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                            
                            var strAccountID = "" , strCrmCompanyId = "" , strCrmContactId = "" ,strAccountName = "" , strCrmCompanyName = "" , strCrmContactName = ""
                            
                            if EntityType == enumRefTypeAccount { //account 0 -> replace Conatact 3 & comapny 4
                                
                                strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
                                strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
                                strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                
                                self.aryTblList.replaceObject(at: 0, with: ["value":strName,"placeholder":"Account","id":EntityId]) //Account
                                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                                
                                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                                
                            }
                            else if EntityType == enumRefTypeOpportunity { // Opportunity 1 -> A/c 0 & Contact 3 & Comapny 4
                                
                                
                                strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
                                strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                
                                strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                
                                strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
                                strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
                                
                                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                                
                                self.aryTblList.replaceObject(at: 1, with: ["value":strName,"placeholder":"Opportunity","id":EntityId]) //Opportunity
                                
                                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                                
                                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                                
                                
                            }
                            else if EntityType == enumRefTypeWebLead { //web lead 2 - > Contact 3 & Compnay 4
                                
                                strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                
                                strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
                                strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
                                
                                
                                self.aryTblList.replaceObject(at: 2, with: ["value":strName,"placeholder":"Lead","id":EntityId]) //Lead
                                
                                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                                
                                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                                
                            }
                            
                            else if EntityType == enumRefTypeCrmContact { // Contact 3-> Company 4 & Account 0
                                strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
                                strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                
                                strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                
                                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                                self.aryTblList.replaceObject(at: 3, with: ["value":strName,"placeholder":"Contact","id":EntityId]) //Contact
                                
                                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                            }
                            
                            else if EntityType == enumRefTypeCrmCompany { // Company 4  -> Contact 3 & A/C 0
                                strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
                                strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
                                strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
                                
                                
                                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                                
                                self.aryTblList.replaceObject(at: 4, with: ["value":strName,"placeholder":"Company","id":EntityId]) //Comapny
                            }
                            self.strStausRemove = 0
                            
                            self.tvList.reloadData()
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }
                    
                    
                    
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            
        }
        
    }
    
}

// MARK: -
// MARK: - AddTaskAssociateProtocol
extension AddTaskVC_iPhoneVC: AddTaskAssociateProtocol
{
    func getDataAddTaskAssociateProtocol(notification: NSDictionary, tag: Int) {
        //   print(notification.userInfo ?? "")
        
        self.tvList.tag = tag
        print(self.tvList.tag)
        
        if let dict = notification as NSDictionary? {
            
            var strEntityId = ""
            var strEntityType = ""
            var strEntityName = ""
            
            let arrOfKeys = dict.allKeys as NSArray
            
            if self.tvList.tag == 0{
                
                if arrOfKeys.contains("AccountId") {
                    
                    // Account
                    strEntityId = "\(dict.value(forKey: "AccountId")!)"
                    strEntityType = ""
                    strEntityName = "\(dict.value(forKey: "AccountName")!)"
                    
                    let strName = "\(dict.value(forKey: "AccountName") ?? "")"
                    
                    if strName.count == 0 {
                        
                        let strCrmContactName = "\(dict.value(forKey: "CrmContactName") ?? "")"
                        
                        if strCrmContactName.count == 0 {
                            
                            let strCrmCompanyName = "\(dict.value(forKey: "CrmCompanyName") ?? "")"
                            
                            if strCrmCompanyName.count == 0 {
                                
                                strEntityName = " "
                                
                            }else{
                                
                                strEntityName = "\(strCrmCompanyName)"
                                
                            }
                            
                        }else{
                            
                            strEntityName = "\(strCrmContactName)"
                            
                        }
                        
                    }else{
                        
                        strEntityName = "\(strName)"
                        
                    }
                    
                }
                
            }else if self.tvList.tag == 1{
                // Opportunity
                
                strEntityId = "\(dict.value(forKey: "opportunityId")!)"
                strEntityType = ""
                strEntityName = "\(dict.value(forKey: "opportunityName")!)"
                
            }else if self.tvList.tag == 2{
                // Lead
                
                strEntityId = "\(dict.value(forKey: "leadId")!)"
                strEntityType = ""
                strEntityName = "\(dict.value(forKey: "leadName")!)"
                
            }else if self.tvList.tag == 3{
                // Contact
                
                strEntityId = "\(dict.value(forKey: "CrmContactId")!)"
                strEntityType = ""
                strEntityName = Global().strFullName(dict as? [AnyHashable : Any])
                
            }else if self.tvList.tag == 4{
                // Company
                
                strEntityId = "\(dict.value(forKey: "CrmCompanyId")!)"
                strEntityType = ""
                strEntityName = "\(dict.value(forKey: "Name")!)"
                
            }
            
            //For EDIT
            if(dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 1 || dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2)
            {
                let strfollowupStatus = dictTaskDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2 ? true : false
                
                if self.tvList.tag == 0 { // Account
                    strEntityType = enumRefTypeAccount
                }
                else if self.tvList.tag == 1 { //Opportunity
                    strEntityType = enumRefTypeOpportunity
                }
                else if self.tvList.tag == 2 { //web lead
                    strEntityType = enumRefTypeWebLead
                }else if self.tvList.tag == 3 { // Contact
                    strEntityType = enumRefTypeCrmContact
                }else if self.tvList.tag == 4 { // Company
                    strEntityType = enumRefTypeCrmCompany
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: strEntityId, EntityType: strEntityType, editStatus: true, followupStatus: strfollowupStatus, userInfo: dict)
                }
            }
            //For ADD
            
            else{
                if self.tvList.tag == 0 { //account
                    strEntityType = enumRefTypeAccount
                }else if self.tvList.tag == 1 { // Opportunity
                    strEntityType = enumRefTypeOpportunity
                }else if self.tvList.tag == 2 { //web lead
                    strEntityType = enumRefTypeWebLead
                }else if self.tvList.tag == 3 { // Contact
                    strEntityType = enumRefTypeCrmContact
                }else if self.tvList.tag == 4 { // Company
                    strEntityType = enumRefTypeCrmCompany
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: strEntityId, EntityType: strEntityType, editStatus: false, followupStatus: false, userInfo: dict)
                    
                }
            }
        }
    }
    
    
}
extension AddTaskVC_iPhoneVC : UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtfld_TaskName){
            if range.location == 0 && string == " " {
                return false
            }
            return true
        }
        return true
    }
}
