//
//  AddActivity_iPhoneVC.swift
//  peSTReam 2020
//  peSTReam 2020
//  Created by Navin Patidar on 3/19/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar 2020
//  Saavan Patidar 2021

import UIKit

protocol AddActivityAssociateProtocol : class
{
    
    func getDataAddActivityAssociateProtocol(notification : NSDictionary ,tag : Int)
    
}


class AddActivity_iPhoneVC: UIViewController {
  
    
    //MARK:
    //MARK: ---------------IBOutlet--------------
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtfld_Agenda: ACFloatingTextField!
    @IBOutlet weak var txtfld_LogType: ACFloatingTextField!
    @IBOutlet weak var txtfld_Participants: ACFloatingTextField!
    
    @IBOutlet weak var txtfld_Date: ACFloatingTextField!
    @IBOutlet weak var txtfld_Time: ACFloatingTextField!
    @IBOutlet weak var heightTemplete: NSLayoutConstraint!
    @IBOutlet weak var txtfld_Templete: ACFloatingTextField!

    @IBOutlet weak var btnSave: ButtonWithShadow!
    @IBOutlet private weak var tvList: UITableView! {
        didSet {
            tvList.tableFooterView = UIView()
            tvList.estimatedRowHeight = 50
        }
    }
    
    var aryTblList = NSMutableArray()
    var dictTotalLeadCountResponse = NSMutableDictionary()
    var dictLoginData = NSDictionary()
    @objc var dictActivityDetailsData = NSMutableDictionary()
    
    var strComeFrom = ""
    var strLeadActivityId = ""
    var strStausRemove = 0
    
    
    
    var str_Name_MultipleParticipants = ""
    var str_ID_MultipleParticipants = ""
    var aryMultipleParticipants = NSMutableArray()
    
    
    var loader = UIAlertController()
    // MARK:
    // MARK: ----------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfld_Agenda.delegate = self
        self.SetupUI_SetInitialContain()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNewLeadNotification(notification:)), name: Notification.Name("AddActivityCRM_AssociationsFromNewLeadVC"), object: nil)
        
    }
    // MARK: - notifcation method
    
    @objc func methodOfReceivedNewLeadNotification(notification: Notification) {
        NotificationCenter.default.removeObserver(self, name:Notification.Name("AddActivityCRM_AssociationsFromNewLeadVC") , object: nil)
        if notification.userInfo != nil{
            let dict = notification.userInfo
            if dict != nil{
                getDataAddActivityAssociateProtocol(notification: dict! as NSDictionary, tag: 2)
            }
        }
    }
    
    
    // MARK:
    // MARK: ----------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnSelectLogType(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTemp = dictTotalLeadCountResponse.value(forKey: "ActivityLogTypeMasters") as! NSArray
        let resultPredicate = NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 403
            openTableViewPopUp(tag: 403, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        } else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnParticipants(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
        let resultPredicate = NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 404
            openTableViewPopUp(tag: 404, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: aryMultipleParticipants)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
            
        }
        
    }
    @IBAction func actionOnDate(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 401
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfld_Date.text!.count) > 0 ? txtfld_Date.text! : result))!
        gotoDatePickerView(sender: sender, strType: "DateTime" , dateToSet: fromDate)
        
    }
    @IBAction func actionOnSelectTemplate(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTemp = (dictTotalLeadCountResponse.value(forKey: "ActivityTemplateMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        let resultPredicate = NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 405
            openTableViewPopUp(tag: 405, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
            
        }
        
    }
    
    @IBAction func actionOnTime(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 402
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfld_Time.text?.count)! > 0 ? txtfld_Time.text! : result))!
        
        gotoDatePickerView(sender: sender, strType: "Time" , dateToSet: fromDate)
        
    }
    @IBAction func actionOnSave(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if(validationOnView()){
            if(isInternetAvailable()){
              
                if( dictActivityDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2){
                    //DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.Add_UpdateActivityAPI(strFollowUpStatus: true)
                    //}
                }else{
                    //DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.Add_UpdateActivityAPI(strFollowUpStatus: false)
                    //}
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
        }
    }
}

// MARK: -
// MARK: -----UITableViewDelegate

extension AddActivity_iPhoneVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTblList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "AddActivityCell", for: indexPath as IndexPath) as! AddActivityCell
        cell.ConfigureCell(index: indexPath, dict: aryTblList.object(at: indexPath.row)as! NSDictionary)
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(self.actionWithRemove(sender:)), for: .touchUpInside)
        if(cell.txtfld_AssocitedWith.text == ""){
            cell.btnRemove.isHidden = true
            cell.btnRemoveWidth.constant = 0.0
        }else
        {
            cell.btnRemove.isHidden = false
            cell.btnRemoveWidth.constant = 35.0
            if(indexPath.row == 0){
                if(self.strStausRemove == 1){
                    cell.btnRemove.isHidden = true
                    cell.btnRemoveWidth.constant = 0.0
                }
            }
        }
        cell.btnRemove.isHidden = false
        cell.btnRemoveWidth.constant = 35.0

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DeviceType.IS_IPAD ? 80.0 : 55.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.tvList.tag = indexPath.row
        switch indexPath.row {
        case 0:
            // For Account
            if self.strStausRemove == 0 {
                
                self.gotoAssociateAccount()
                
                //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "1194389", EntityType: enumRefTypeAccount, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            }
            break
        case 1:
            // For Opportunity
            
            self.gotoAssociateOpportunity()
            
            //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "249047", EntityType: enumRefTypeOpportunity, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            
            break
        case 2:
            //For Lead
            
            self.gotoAssociateLead()
            
            //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "167918", EntityType: enumRefTypeWebLead, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            
            break
        case 3:
            //For Contact
            
            self.gotoAssociateContact()
            
            //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "1388512", EntityType: enumRefTypeCrmContact, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            break
        case 4:
            //For Company
            
            self.gotoAssociateCompany()
            
            //self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: "182021", EntityType: enumRefTypeCrmCompany, editStatus: false, followupStatus: false, userInfo: NSDictionary())
            
            break
        default:
            break
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 45
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Associate with"
    }
    @objc func actionWithRemove(sender : UIButton){
        
        let dict = (self.aryTblList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        
        if "\(dict.value(forKey: "value") ?? "")".count > 0 {
            
            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                let dict = (self.aryTblList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                dict.setValue("", forKey: "id")
                dict.setValue("", forKey: "value")
                self.aryTblList.replaceObject(at: sender.tag, with: dict)
                self.tvList.reloadData()
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }

    }
}

// MARK: -
// MARK: -OtherCell
class AddActivityCell: UITableViewCell {
    @IBOutlet weak var txtfld_AssocitedWith: ACFloatingTextField!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnRemoveWidth: NSLayoutConstraint!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    func ConfigureCell(index: IndexPath , dict : NSDictionary) {
        
        self.txtfld_AssocitedWith.placeholder = "\(dict.value(forKey: "placeholder")!)"
        self.txtfld_AssocitedWith.text = "\(dict.value(forKey: "value")!)"
        // self.txtfld_AssocitedWith.tag = Int("\(dict.value(forKey: "id")!)")!
        
        
    }
    
}
// MARK:
// MARK: ----------Extra Function's

extension AddActivity_iPhoneVC{
    // MARK:
    // MARK: --------Set DAta On View--------
    func SetupUI_SetInitialContain() {
        dictActivityDetailsData = removeNullFromDict(dict: dictActivityDetailsData)
        strComeFrom = dictActivityDetailsData.value(forKey: "Pre_ViewComeFrom")as! String
        
        
        let id_AccountId = "\(dictActivityDetailsData.value(forKey: "Pre_AccountId")!)" == "0" ? "" : "\(dictActivityDetailsData.value(forKey: "Pre_AccountId")!)"
        let name_Account = "\(dictActivityDetailsData.value(forKey: "Pre_AccountName")!)"
        
        
        
        let id_LeadId = "\(dictActivityDetailsData.value(forKey: "Pre_LeadId")!)" == "0" ? "" : "\(dictActivityDetailsData.value(forKey: "Pre_LeadId")!)"
        let name_LeadName = "\(dictActivityDetailsData.value(forKey: "Pre_LeadName")!)"
        
        
        let id_WebLeadId = "\(dictActivityDetailsData.value(forKey: "Pre_WebLeadId")!)" == "0" ? "" : "\(dictActivityDetailsData.value(forKey: "Pre_WebLeadId")!)"
        let name_WebLeadName = "\(dictActivityDetailsData.value(forKey: "Pre_WebLeadName")!)"
        
        let id_CrmContactId = "\(dictActivityDetailsData.value(forKey: "Pre_CrmContactId")!)" == "0" ? "" : "\(dictActivityDetailsData.value(forKey: "Pre_CrmContactId")!)"
        let name_CrmContactName = "\(dictActivityDetailsData.value(forKey: "Pre_CrmContactName")!)"
        
        let id_CrmCompanyId = "\(dictActivityDetailsData.value(forKey: "Pre_CrmCompanyId")!)" == "0" ? "" : "\(dictActivityDetailsData.value(forKey: "Pre_CrmCompanyId")!)"
        let name_CrmCompanyName = "\(dictActivityDetailsData.value(forKey: "Pre_CrmCompanyName")!)"
        
        
        aryTblList = [["value":name_Account,"placeholder":"Account","id":id_AccountId],["value":name_LeadName,"placeholder":"Opportunity","id":id_LeadId],["value":name_WebLeadName,"placeholder":"Lead","id":id_WebLeadId],["value":name_CrmContactName,"placeholder":"Contact","id":id_CrmContactId],["value":name_CrmCompanyName,"placeholder":"Company","id":id_CrmCompanyId]]
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        dictTotalLeadCountResponse = (nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary).mutableCopy()as! NSMutableDictionary
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        dictTotalLeadCountResponse = (nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary).mutableCopy()as! NSMutableDictionary
        
        // for update
        if(dictActivityDetailsData.value(forKey: "Pre_IsUpdate")as! Bool){
            
            btnSave.setTitle("Save", for: .normal)
            lblTitle.text = "Update Activity"
            strLeadActivityId = "\(dictActivityDetailsData.value(forKey: "ActivityId")!)"
            
            let strActivityDate = Global().convertDate("\(dictActivityDetailsData.value(forKey: "FromDate")!)")
            txtfld_Date.text = strActivityDate
            let strActivityTime = Global().convertTime("\(dictActivityDetailsData.value(forKey: "ActivityTime")!)")
            if strActivityTime != nil {
                txtfld_Date.text = strActivityDate! + " " + strActivityTime! == " " ? "" : strActivityDate! + " " + strActivityTime!
            }else{
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm a"
                formatter.locale = Locale(identifier: "EST")
                txtfld_Date.text = strActivityDate! + " " + formatter.string(from: date)
            }
            
            
            // txtfld_Time.text = strActivityTime
            
            
            txtfld_Agenda.text = "\(dictActivityDetailsData.value(forKey: "Agenda")!)"
            
            
            if(dictTotalLeadCountResponse.count != 0){
                let aryTemp = dictTotalLeadCountResponse.value(forKey: "ActivityLogTypeMasters") as! NSArray
                let logTypeID = "\(dictActivityDetailsData.value(forKey: "LogTypeId")!)"
                logTypeID == "" ? txtfld_LogType.tag = 0 : (txtfld_LogType.tag = Int(logTypeID)!)
                
                for item in aryTemp
                {
                    if("\(((item as AnyObject)as! NSDictionary).value(forKey: "LogTypeId")!)" == "\(logTypeID)")
                    {
                        txtfld_LogType.text = "\(((item as AnyObject)as! NSDictionary).value(forKey: "Name")!)"
                        break
                    }
                }
            }
            
            
            
            if("\(dictActivityDetailsData.value(forKey: "ActivityParticipants")!)".count > 0)
            {
                let aryParticipantsIds = dictActivityDetailsData.value(forKey: "ActivityParticipants") as! NSArray
                
                let aryEmp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
                
                for k in 0 ..< aryParticipantsIds.count {
                    
                    let obj = aryParticipantsIds[k] as! NSDictionary
                    
                    for emp in aryEmp
                    {
                        if("\(obj.value(forKey: "EmployeeId")!)" == "\((emp as! NSDictionary).value(forKey: "EmployeeId")!)")//EmployeeId
                        {
                            aryMultipleParticipants.add(emp as! NSDictionary)
                        }
                    }
                    
                }
                
                var termsTitle = ""
                var ids = ""
                for item in aryMultipleParticipants
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "FullName")!)" + ","
                    ids = ids + "\((item as! NSDictionary).value(forKey: "EmployeeId")!)" + ","
                }
                if(aryMultipleParticipants.count > 0)
                {
                    termsTitle.removeLast()
                    ids.removeLast()
                    str_Name_MultipleParticipants = termsTitle
                    str_ID_MultipleParticipants = ids
                    txtfld_Participants.text = termsTitle
                    
                }
                else
                {
                    str_Name_MultipleParticipants = ""
                    str_ID_MultipleParticipants = ""
                    txtfld_Participants.text = ""
                }
            }
            
            heightTemplete.constant = 0.0
            
        }else{
            
            btnSave.setTitle("Save", for: .normal)
            lblTitle.text = "Add Activity"
            
            // Set Default Partcipiants
            str_Name_MultipleParticipants = "\(dictLoginData.value(forKey: "EmployeeName")!)"
            str_ID_MultipleParticipants = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            txtfld_Participants.text = "\(dictLoginData.value(forKey: "EmployeeName")!)"
            
            // call API to fetch Data for Associations
            
            if id_AccountId.count > 0 && id_AccountId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_AccountId, EntityType: enumRefTypeAccount, strName: name_Account)
                
            } else if id_WebLeadId.count > 0 && id_WebLeadId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_WebLeadId, EntityType: enumRefTypeWebLead, strName: name_WebLeadName)
                
            } else if id_LeadId.count > 0 && id_LeadId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_LeadId, EntityType: enumRefTypeOpportunity, strName: name_LeadName)
                
            } else if id_CrmContactId.count > 0 && id_CrmContactId != "0"{
                
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_CrmContactId, EntityType: enumRefTypeCrmContact, strName: name_CrmContactName)
                
            }else if id_CrmCompanyId.count > 0 && id_CrmCompanyId != "0"{
                self.callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: id_CrmCompanyId, EntityType: enumRefTypeCrmCompany, strName: name_CrmCompanyName)
                
            }
            heightTemplete.constant = 45.0
            
            var strDate = "" , strTime = ""
            let date = Date()
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "EST")
            if (strDate.count == 0){
                formatter.dateFormat = "MM/dd/yyyy"
                strDate = formatter.string(from: date)
            }
            if (strTime.count == 0){
                formatter.dateFormat = "hh:mm a"
                strTime = formatter.string(from: date)
            }
            if(strDate.count != 0 && strTime.count != 0){
                txtfld_Date.text = strDate + " " + strTime
            }
            
        }
    }
    func setDataWhenComeFromAdd(dictResponse : NSMutableDictionary , strID : String ,strName : String )  {
        var strAccountID = "" , strCrmCompanyId = "" , strCrmContactId = "" ,strAccountName = "" , strCrmCompanyName = "" , strCrmContactName = ""
        
        if self.tvList.tag == 0 { //account 0 -> replace Conatact 3 & comapny 4
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            self.aryTblList.replaceObject(at: 0, with: ["value":strName,"placeholder":"Account","id":strID]) //Account
          //--Check If data is available then Add--
            if(strCrmContactId.count != 0){
                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
            }
            if(strCrmCompanyId.count != 0){
                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
            }
            
            
            
        }
        else if self.tvList.tag == 1 { // Opportunity 1 -> A/c 0 & Contact 3 & Comapny 4
            
            
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            
            self.aryTblList.replaceObject(at: 1, with: ["value":strName,"placeholder":"Opportunity","id":strID]) //Opportunity
            
            
            
            //--Check If data is available then Add--
            if(strAccountID.count != 0){
                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                
            }
            if(strCrmContactId.count != 0){
                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
            }
            if(strCrmCompanyId.count != 0){
                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
            }
        }
            
        else if self.tvList.tag == 2 { //web lead 2 - > Contact 3 & Compnay 4
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            
            self.aryTblList.replaceObject(at: 2, with: ["value":strName,"placeholder":"Lead","id":strID]) //Lead
            
            //--Check If data is available then Add--
                     if(strCrmContactId.count != 0){
                           self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                     }
            if(strCrmCompanyId.count != 0){
                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
            }
        }
            
        else if self.tvList.tag == 3 { // Contact 3-> Company 4 & Account 0
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            
            self.aryTblList.replaceObject(at: 3, with: ["value":strName,"placeholder":"Contact","id":strID]) //Contact
            //--Check If data is available then Add--
            
            if(strAccountID.count != 0){
                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
            }
            if(strCrmCompanyId.count != 0){
                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
            }
            
        }
            
        else if self.tvList.tag == 4 { // Company 4  -> Contact 3 & A/C 0
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
          
            self.aryTblList.replaceObject(at: 4, with: ["value":strName,"placeholder":"Company","id":strID]) //Comapny

            //--Check If data is available then Add--
            
            if(strAccountID.count != 0){
                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
            }
            if(strCrmContactId.count != 0){
                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
            }
        }
        self.strStausRemove = 0
        
        self.tvList.reloadData()
    }
    
    
    func setDataWhenComeFromEditFollowUP(dictResponse : NSMutableDictionary , strID : String ,strName : String )  {
        
        var strAccountID = "" , strCrmCompanyId = "" , strCrmContactId = "" ,strAccountName = "" , strCrmCompanyName = "" , strCrmContactName = ""
        
        let str_AccountId = "\((self.aryTblList.object(at: 0)as! NSDictionary).value(forKey: "id")!)"
        // let str_OpportunityId = "\((self.aryTblList.object(at: 1)as! NSDictionary).value(forKey: "id")!)"
        //   let str_LeadId = "\((self.aryTblList.object(at: 2)as! NSDictionary).value(forKey: "id")!)"
        let str_ContacttId = "\((self.aryTblList.object(at: 3)as! NSDictionary).value(forKey: "id")!)"
        let str_CompanyId = "\((self.aryTblList.object(at: 4)as! NSDictionary).value(forKey: "id")!)"
        
        //-----For Account 0 -> replace Conatact 3 & comapny 4
        if(self.tvList.tag == 0){
            
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            //Account
            self.aryTblList.replaceObject(at: 0, with: ["value":strName,"placeholder":"Account","id":strID])
            //check if Contact Company is blank then replace
            if(str_ContacttId == ""){
                if(strCrmContactId.count != 0){
                     self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                 }
               
                
            }
            if(str_CompanyId == ""){
                if(strCrmCompanyId.count != 0){
                      self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                 }
              
            }
        }
            //-----For Opportunity 1 -> A/c 0 & Contact 3 & Comapny 4
            
            
        else if (self.tvList.tag == 1){
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            //Opportunity
            self.aryTblList.replaceObject(at: 1, with: ["value":strName,"placeholder":"Opportunity","id":strID])
            
            //check if A/c 0 & Contact 3 & Comapny 4 is blank then replace
            if(str_AccountId == ""){
                if(strAccountID.count != 0){
                      self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                   }
              
                
                //                                if(strAccountName.count == 0){
                //                                    self.strStausRemove = 0
                //                                }else{
                //                                    self.strStausRemove = 1
                //                                }
                
            }else{
                if(strAccountID.count != 0){
                    self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                }
           
            }
            if(str_ContacttId == ""){
                if(strCrmContactId.count != 0){
                     self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                  }
           
            }
            if(str_CompanyId == ""){
                if(strCrmCompanyId.count != 0){
                       self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                  }
            }
        }
            
            
            
            //-----For web lead 2 - > Contact 3 & Compnay 4
            
        else if (self.tvList.tag == 2){
            
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            //Lead
            self.aryTblList.replaceObject(at: 2, with: ["value":strName,"placeholder":"Lead","id":strID])
            
            //check if Contact Company is blank then replace
            if(str_ContacttId == ""){
                if(strCrmContactId.count != 0){
                     self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                  }
               
                
            }
            if(str_CompanyId == ""){
                if(strCrmCompanyId.count != 0){
                     self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                  }
               
            }
        }
            //-----For Contact 3-> Company 4 & Account 0
            
        else if (self.tvList.tag == 3){
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            
            strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
            strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
            
            self.aryTblList.replaceObject(at: 3, with: ["value":strName,"placeholder":"Contact","id":strID]) //Contact
            
            //check if Account Company is blank then replace
            
            if(str_AccountId == ""){
                  if(strAccountID.count != 0){
                      self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                }
              
            }
            if(str_CompanyId == ""){
                  if(strCrmCompanyId.count != 0){
                     self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Company
                }
               
                
            }
        }
            //-----For Company 4  -> Contact 3 & A/C 0
            
        else if (self.tvList.tag == 4){
            //
            strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
            strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
            strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
            strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
            
            //Comapny
            self.aryTblList.replaceObject(at: 4, with: ["value":strName,"placeholder":"Company","id":strID])
            //check if Account Contact is blank then replace
            
            if(str_AccountId == ""){
                 if(strAccountID.count != 0){
                       self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                }
            }
            if(str_ContacttId == ""){
                 if(strCrmContactId.count != 0){
                      self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                }
            }
            
            
        }
        self.tvList.reloadData()
        
    }
    
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        
        
        
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func gotoDatePickerView(sender: UIButton, strType:String , dateToSet:Date)  {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = false
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
    }
    
    func validationOnView() -> Bool {
        if(txtfld_Agenda.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter activity detail.", viewcontrol: self)
            return false
        }
        if(txtfld_LogType.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select activity type.", viewcontrol: self)
            return false
            
        }
        else if(txtfld_Participants.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select participant.", viewcontrol: self)
            return false
            
        }
        else if(txtfld_Date.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter date time.", viewcontrol: self)
            return false
            
        }
//        else if(txtfld_Time.text == "")
//        {
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter time.", viewcontrol: self)
//            return false
//
//        }
        return true
        
    }
    func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    fileprivate func gotoAssociateAccount(){
        
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateAccountVC") as! AssociateAccountVC
        controller.delegateActivityAssociate = self
        controller.selectionTag = 0
        
        controller.strFromWhere = "NotAsscoiate"
        controller.strFromWhereForAssociation = "AddActivityCRM_Associations"
        nsud.set(true, forKey: "isInitalAccount")
        nsud.synchronize()
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    fileprivate func gotoAssociateOpportunity(){
        
        
        nsud.set(true, forKey: "fromOpportunityAssociation")
        nsud.synchronize()
        
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
        
        controller.delegateActivityAssociate = self
        controller.selectionTag = 1
        controller.strTypeOfAssociations = "AddActivityCRM_Associations"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    fileprivate func gotoAssociateLead(){
        
        nsud.set(true, forKey: "fromWebLeadAssociation")
        nsud.synchronize()
        
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        controller.delegateActivityAssociate = self
        controller.selectionTag = 2
        controller.strTypeOfAssociations = "AddActivityCRM_Associations"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func gotoAssociateContact(){
        
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
          
        controller.delegateActivityAssociate = self
        controller.selectionTag = 3
        controller.strFromWhereForAssociation = "AddActivityCRM_Associations"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func gotoAssociateCompany(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "AddActivityCRM_Associations"
        controller.delegateActivityAssociate = self
        controller.selectionTag = 4
        //controller.strRefType = enumRefTypeCrmContact
        //controller.strRefId = "\(dictAbout.value(forKey: "CrmContactId") ?? "")"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
}
// MARK: -
// MARK: - Selection CustomTableView
extension AddActivity_iPhoneVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 403)
        {
            if dictData.count == 0 {
                txtfld_LogType.text = ""
                txtfld_LogType.tag = 0
            }else{
                txtfld_LogType.text = "\(dictData.value(forKey: "Name")!)"
                txtfld_LogType.tag = Int("\(dictData.value(forKey: "LogTypeId")!)")!
            }
        }
        if(tag == 404)
        {
            if dictData.count == 0 {
                str_Name_MultipleParticipants = ""
                str_ID_MultipleParticipants = ""
                txtfld_Participants.text = ""
            }else{
                let aryMultipleParticipants = (dictData.value(forKey: "multi") as! NSArray).mutableCopy() as! NSMutableArray
                
                var termsTitle = ""
                var ids = ""
                for item in aryMultipleParticipants
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "FullName")!)" + ","
                    ids = ids + "\((item as! NSDictionary).value(forKey: "EmployeeId")!)" + ","
                }
                if(aryMultipleParticipants.count > 0)
                {
                    termsTitle.removeLast()
                    ids.removeLast()
                    str_Name_MultipleParticipants = termsTitle
                    str_ID_MultipleParticipants = ids
                    txtfld_Participants.text = termsTitle
                    
                }
                else
                {
                    txtfld_Participants.text = ""
                }
            }
            
        }
        if(tag == 405)
             {
                if(dictData.count == 0){
                    txtfld_Templete.text = ""
                    txtfld_Agenda.text = ""
                    txtfld_LogType.text = ""
                    txtfld_LogType.tag = 0
                }else{
                     
                    txtfld_Templete.text = "\(dictData.value(forKey: "Agenda")!)"
                    txtfld_Agenda.text = "\(dictData.value(forKey: "Agenda")!)"
                    let strLogTypeId = "\(dictData.value(forKey: "LogTypeId")!)"
                    
                    if(strLogTypeId != ""){
                        let aryTemp = dictTotalLeadCountResponse.value(forKey: "ActivityLogTypeMasters") as! NSArray
                        let resultPredicate = NSPredicate(format: "IsActive == true AND LogTypeId == \(strLogTypeId)")
                        let arrayfilter = aryTemp.filtered(using: resultPredicate)
                        if(arrayfilter.count != 0){
                            let dict = arrayfilter[0]as! NSDictionary
                            txtfld_LogType.text = "\(dict.value(forKey: "Name")!)"
                            txtfld_LogType.tag = Int("\(dict.value(forKey: "LogTypeId")!)")!
                        }
                    }else{
                        txtfld_LogType.text = ""
                        txtfld_LogType.tag = 0
                    }
                    
                 }
                 
             }
        
    }
    
}
extension AddActivity_iPhoneVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if(tag == 401)
        {
            txtfld_Date.text = strDate
        }
        if(tag == 402)
        {
            txtfld_Time.text = strDate
        }
        
        
    }
}

// MARK:
// MARK: --API Calling

extension AddActivity_iPhoneVC{
    func Add_UpdateActivityAPI(strFollowUpStatus : Bool){
       
        var dateActivity = "" , timeActivity = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let date = dateFormatter.date(from: txtfld_Date.text!)!
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateActivity = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        timeActivity = dateFormatter.string(from: date)
        
        let dictToSend = NSMutableDictionary()
        dictToSend.setValue("0", forKey: "Id")
        dictToSend.setValue("\(dateActivity)", forKey: "FromDate")
        dictToSend.setValue("\(dateActivity)", forKey: "ToDate")
        //dictToSend.setValue("\(str_ID_MultipleParticipants)", forKey: "Participants")
        
        let arrOfParticipantsList = str_ID_MultipleParticipants.components(separatedBy: ",")
        
        dictToSend.setValue(arrOfParticipantsList, forKey: "ParticipantsList")
        dictToSend.setValue("\(txtfld_LogType.tag == 0 ? "" : "\(txtfld_LogType.tag)")", forKey: "LogTypeId")
        
        dictToSend.setValue("\(txtfld_Agenda.text!)", forKey: "Agenda")
        dictToSend.setValue("", forKey: "Address1")
        dictToSend.setValue("", forKey: "Address2")
        dictToSend.setValue("", forKey: "CityName")
        dictToSend.setValue("", forKey: "StateId")
        dictToSend.setValue("", forKey: "CountryId")
        dictToSend.setValue("", forKey: "ZipCode")
        
        dictToSend.setValue(Global().getModifiedDate(), forKey: "CreatedDate")
        dictToSend.setValue(NSArray(), forKey: "ActivityLogTypeMasterExtDc")
        dictToSend.setValue("", forKey: "RefType")
        dictToSend.setValue("", forKey: "RefId")
        dictToSend.setValue("\(strLeadActivityId)", forKey: "ActivityId")
        dictToSend.setValue(NSArray(), forKey: "ActivityCommentExtDcs")
        dictToSend.setValue(NSArray(), forKey: "LeadExtDcs")
        dictToSend.setValue(NSArray(), forKey: "EmployeeExtDcs")
        dictToSend.setValue("\(dictLoginData.value(forKey: "CreatedBy")!)", forKey: "CreatedBy")
        dictToSend.setValue(Global().getModifiedDate(), forKey: "ModifiedDate")
        dictToSend.setValue("\(dictLoginData.value(forKey: "CreatedBy")!)", forKey: "ModifiedBy")
        dictToSend.setValue("", forKey: "ChildTasks")
        dictToSend.setValue(getTimeWithouAMPM(strTime: (timeActivity)), forKey: "ActivityTime")
        
        
        let strACID = "\((aryTblList.object(at: 0)as! NSDictionary).value(forKey: "id")!)"
        let strLeadId = "\((aryTblList.object(at: 1)as! NSDictionary).value(forKey: "id")!)"
        let strWebLeadId = "\((aryTblList.object(at: 2)as! NSDictionary).value(forKey: "id")!)"
        let strCrmContactId = "\((aryTblList.object(at: 3)as! NSDictionary).value(forKey: "id")!)"
        let strCrmCompanyId = "\((aryTblList.object(at: 4)as! NSDictionary).value(forKey: "id")!)"
        
        dictToSend.setValue(strACID == "0" ? "" : strACID, forKey: "AccountId")
        dictToSend.setValue(strLeadId == "0" ? "" : strLeadId, forKey: "LeadId")
        dictToSend.setValue(strWebLeadId == "0" ? "" : strWebLeadId, forKey: "WebLeadId")
        dictToSend.setValue(strCrmContactId == "0" ? "" : strCrmContactId, forKey: "CrmContactId")
        dictToSend.setValue(strCrmCompanyId == "0" ? "" : strCrmCompanyId, forKey: "CrmCompanyId")
        
        if(strFollowUpStatus){
            dictToSend.setValue("", forKey: "LeadTaskId")
        }
        
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddUpdateActivityGlobal
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            print(jsonString)
        }
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        let strTypee = "addActivity"
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            self.loader.dismiss(animated: false) {
                if(success)
                        {
                            
                            UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")
                            UserDefaults.standard.set(true, forKey: "RefreshTasks_CompanyDetails")
                            
                            nsud.setValue(true, forKey: "isActivityAddedUpdated")
                            nsud.synchronize()
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedActivity_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            print((response as NSDictionary?)!)
                            
                            if(self.dictActivityDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 1){
                                
                                let alertCOntroller = UIAlertController(title: "Message", message: "Activity Updated Successfully", preferredStyle: .alert)
                                
                                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ActivityUpdated_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                    
                                    self.navigationController?.popViewController(animated: false)
                                })
                                
                                alertCOntroller.addAction(alertAction)
                                self.present(alertCOntroller, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                
                                let alertCOntroller = UIAlertController(title: "Message", message: "Activity Added Successfully", preferredStyle: .alert)
                                
                                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ActivityAdded_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                    
                                    self.navigationController?.popViewController(animated: false)
                                })
                                
                                alertCOntroller.addAction(alertAction)
                                self.present(alertCOntroller, animated: true, completion: nil)
                            }
                        }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
        
        }
    }
    func callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: String ,EntityType : String,editStatus : Bool , followupStatus : Bool , userInfo : NSDictionary) {
        
        if(EntityId.count != 0 && EntityId != "<null>"){
            
            if(isInternetAvailable()){
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                       self.present(loader, animated: false, completion: nil)
                
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAssociatedEntitiesForTaskActivityAssociations + "EntityType=\(EntityType)&EntityId=\(EntityId)"
                
                WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                    
                    self.loader.dismiss(animated: false) {
                        if(Status)
                                         {
                                             let dictResponse = removeNullFromDict(dict: (Response["data"] as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                                             
                                             //let strID = "0" , strName = ""
                                             var strID = ""
                                             var strName = ""
                                             
                                             let arrOfKeys = userInfo.allKeys as NSArray
                                             
                                             if self.tvList.tag == 0{
                                                 
                                                 if arrOfKeys.contains("AccountId") {
                                                     
                                                     // Account
                                                     strID = "\(userInfo.value(forKey: "AccountId")!)"
                                                     strName = "\(userInfo.value(forKey: "AccountName")!)"
                                                     
                                                     let strName1 = "\(userInfo.value(forKey: "AccountName") ?? "")"
                                                     
                                                     if strName1.count == 0 {
                                                         
                                                         let strCrmContactName = "\(userInfo.value(forKey: "CrmContactName") ?? "")"
                                                         
                                                         if strCrmContactName.count == 0 {
                                                             
                                                             let strCrmCompanyName = "\(userInfo.value(forKey: "CrmCompanyName") ?? "")"
                                                             
                                                             if strCrmCompanyName.count == 0 {
                                                                 
                                                                 strName = " "
                                                                 
                                                             }else{
                                                                 
                                                                 strName = "\(strCrmCompanyName)"
                                                                 
                                                             }
                                                             
                                                         }else{
                                                             
                                                             strName = "\(strCrmContactName)"
                                                             
                                                         }
                                                         
                                                     }else{
                                                         
                                                         strName = "\(strName1)"
                                                         
                                                     }
                                                     
                                                 }
                                                 
                                             }else if self.tvList.tag == 1{
                                                 // Opportunity
                                                 
                                                 strID = "\(userInfo.value(forKey: "opportunityId")!)"
                                                 strName = "\(userInfo.value(forKey: "opportunityName")!)"
                                                 
                                             }else if self.tvList.tag == 2{
                                                 // Lead
                                                 
                                                 strID = "\(userInfo.value(forKey: "leadId")!)"
                                                 strName = "\(userInfo.value(forKey: "leadName")!)"
                                                 
                                             }else if self.tvList.tag == 3{
                                                 // Contact
                                                 
                                                 strID = "\(userInfo.value(forKey: "CrmContactId")!)"
                                                 strName = Global().strFullName(userInfo as? [AnyHashable : Any])
                                                 
                                             }else if self.tvList.tag == 4{
                                                 // Company
                                                 
                                                 strID = "\(userInfo.value(forKey: "CrmCompanyId")!)"
                                                 strName = "\(userInfo.value(forKey: "Name")!)"
                                                 
                                             }
                                             
                                             
                                             if !(editStatus){ // For add
                                                 self.setDataWhenComeFromAdd(dictResponse: dictResponse, strID: strID, strName: strName)
                                             }else{ // for EDit
                                                 
                                                 self.setDataWhenComeFromEditFollowUP(dictResponse: dictResponse, strID: strID, strName: strName)
                                                 
                                             }
                                             
                                             
                                         }
                                         else
                                         {
                                             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                         }
                    }
                    
                 
                    
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            
        }
        
    }
    
    func callGetAssociatedEntitiesForTaskActivityAssociationsOnAdding(EntityId: String ,EntityType : String, strName: String) {
        
        if(EntityId.count != 0 && EntityId != "<null>"){
            
            if(isInternetAvailable()){
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                     self.present(loader, animated: false, completion: nil)
                
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAssociatedEntitiesForTaskActivityAssociations + "EntityType=\(EntityType)&EntityId=\(EntityId)"
                
                WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                    self.loader.dismiss(animated: false) {
                        if(Status)
                                        { // For add
                                            
                                            let dictResponse = removeNullFromDict(dict: (Response["data"] as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                                            
                                            var strAccountID = "" , strCrmCompanyId = "" , strCrmContactId = "" ,strAccountName = "" , strCrmCompanyName = "" , strCrmContactName = ""
                                            
                                            if EntityType == enumRefTypeAccount { //account 0 -> replace Conatact 3 & comapny 4
                                                
                                                strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
                                                strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
                                                strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                                strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                                
                                                self.aryTblList.replaceObject(at: 0, with: ["value":strName,"placeholder":"Account","id":EntityId]) //Account
                                                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                                                
                                                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                                                
                                            }
                                            else if EntityType == enumRefTypeOpportunity { // Opportunity 1 -> A/c 0 & Contact 3 & Comapny 4
                                                
                                                
                                                strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
                                                strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                                
                                                strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                                strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                                
                                                strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
                                                strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
                                                
                                                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                                                
                                                self.aryTblList.replaceObject(at: 1, with: ["value":strName,"placeholder":"Opportunity","id":EntityId]) //Opportunity
                                                
                                                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                                                
                                                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                                                
                                                
                                            }
                                            else if EntityType == enumRefTypeWebLead { //web lead 2 - > Contact 3 & Compnay 4
                                                
                                                strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                                strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                                
                                                strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
                                                strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
                                                
                                                
                                                self.aryTblList.replaceObject(at: 2, with: ["value":strName,"placeholder":"Lead","id":EntityId]) //Lead
                                                
                                                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                                                
                                                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                                                
                                            }
                                                
                                            else if EntityType == enumRefTypeCrmContact { // Contact 3-> Company 4 & Account 0
                                                strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
                                                strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                                
                                                strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                                strCrmCompanyName = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                                
                                                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                                                self.aryTblList.replaceObject(at: 3, with: ["value":strName,"placeholder":"Contact","id":EntityId]) //Contact
                                                
                                                self.aryTblList.replaceObject(at: 4, with: ["value":strCrmCompanyName,"placeholder":"Company","id":strCrmCompanyId]) //Comapny
                                            }
                                                
                                            else if EntityType == enumRefTypeCrmCompany { // Company 4  -> Contact 3 & A/C 0
                                                strAccountID = "\(dictResponse.value(forKey: "AccountId")!)"
                                                strAccountName = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                                strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)"
                                                strCrmContactName = "\(dictResponse.value(forKey: "ContactName")!)"
                                                
                                                
                                                self.aryTblList.replaceObject(at: 0, with: ["value":strAccountName,"placeholder":"Account","id":strAccountID]) //Account
                                                self.aryTblList.replaceObject(at: 3, with: ["value":strCrmContactName,"placeholder":"Contact","id":strCrmContactId]) //Contact
                                                
                                                self.aryTblList.replaceObject(at: 4, with: ["value":strName,"placeholder":"Company","id":EntityId]) //Comapny
                                            }
                                            self.strStausRemove = 0
                                            
                                            self.tvList.reloadData()
                                        }
                                        else
                                        {
                                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                        }
                    }
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            
        }
        
    }
    
}

// MARK: -
// MARK: - AddActivityAssociateProtocol
extension AddActivity_iPhoneVC: AddActivityAssociateProtocol
{
    func getDataAddActivityAssociateProtocol(notification: NSDictionary, tag: Int) {
        
        self.tvList.tag = tag
        print("self.tvList.tag --\(self.tvList.tag)")
        
        if let dict = notification as NSDictionary? {
            
            var strEntityId = ""
            var strEntityType = ""
            var strEntityName = ""
            let arrOfKeys = dict.allKeys as NSArray
            if self.tvList.tag == 0{
                if arrOfKeys.contains("AccountId") {
                    // Account
                    strEntityId = "\(dict.value(forKey: "AccountId")!)"
                    strEntityType = ""
                    strEntityName = "\(dict.value(forKey: "AccountName")!)"
                    let strName = "\(dict.value(forKey: "AccountName") ?? "")"
                    if strName.count == 0 {
                        let strCrmContactName = "\(dict.value(forKey: "CrmContactName") ?? "")"
                        if strCrmContactName.count == 0 {
                            let strCrmCompanyName = "\(dict.value(forKey: "CrmCompanyName") ?? "")"
                            
                            if strCrmCompanyName.count == 0 {
                                strEntityName = " "
                            }else{
                                strEntityName = "\(strCrmCompanyName)"
                            }
                            
                        }else{
                            strEntityName = "\(strCrmContactName)"
                        }
                        
                    }else{
                        strEntityName = "\(strName)"
                    }
                }
            }else if self.tvList.tag == 1{
                // Opportunity
                strEntityId = "\(dict.value(forKey: "opportunityId")!)"
                strEntityType = ""
                strEntityName = "\(dict.value(forKey: "opportunityName")!)"
                
            }else if self.tvList.tag == 2{
                // Lead
                strEntityId = "\(dict.value(forKey: "leadId")!)"
                strEntityType = ""
                strEntityName = "\(dict.value(forKey: "leadName")!)"
                
            }else if self.tvList.tag == 3{
                // Contact
                strEntityId = "\(dict.value(forKey: "CrmContactId")!)"
                strEntityType = ""
                strEntityName = Global().strFullName(dict as? [AnyHashable : Any])
            }else if self.tvList.tag == 4{
                // Company
                strEntityId = "\(dict.value(forKey: "CrmCompanyId")!)"
                strEntityType = ""
                strEntityName = "\(dict.value(forKey: "Name")!)"
            }
            //For EDIT
            if(dictActivityDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 1 || dictActivityDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2)
            {
                let strfollowupStatus = dictActivityDetailsData.value(forKey: "Pre_IsUpdate")as! Int == 2 ? true : false
                if self.tvList.tag == 0 { // Account
                    strEntityType = enumRefTypeAccount
                }
                else if self.tvList.tag == 1 { //Opportunity
                    strEntityType = enumRefTypeOpportunity
                }
                else if self.tvList.tag == 2 { //web lead
                    strEntityType = enumRefTypeWebLead
                }else if self.tvList.tag == 3 { // Contact
                    strEntityType = enumRefTypeCrmContact
                }else if self.tvList.tag == 4 { // Company
                    strEntityType = enumRefTypeCrmCompany
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                 self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: strEntityId, EntityType: strEntityType, editStatus: true, followupStatus: strfollowupStatus, userInfo: dict)
                }
            }
                //For ADD
            else{
                if self.tvList.tag == 0 { //account
                    strEntityType = enumRefTypeAccount
                }else if self.tvList.tag == 1 { // Opportunity
                    strEntityType = enumRefTypeOpportunity
                }else if self.tvList.tag == 2 { //web lead
                    strEntityType = enumRefTypeWebLead
                }else if self.tvList.tag == 3 { // Contact
                    strEntityType = enumRefTypeCrmContact
                }else if self.tvList.tag == 4 { // Company
                    strEntityType = enumRefTypeCrmCompany
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.callGetAssociatedEntitiesForTaskActivityAssociations(EntityId: strEntityId, EntityType: strEntityType, editStatus: false, followupStatus: false, userInfo: dict)
                }
            }
        }
    }
    
    
}


extension AddActivity_iPhoneVC : UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtfld_Agenda){
            if range.location == 0 && string == " " {
                return false
            }
            return true
        }
        return true
    }
}

