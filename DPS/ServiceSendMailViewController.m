//
//  SendMailViewController.m
//  DPS
//
//  Created by Rakesh Jain on 02/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ServiceSendMailViewController.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "WorkOrderDetailsService.h"
#import "WorkOrderDetailsService+CoreDataProperties.h"
#import "PaymentInfoServiceAuto.h"
#import "PaymentInfoServiceAuto+CoreDataProperties.h"
#import "ServiceAutoCompanyDetails.h"
#import "ServiceAutoCompanyDetails+CoreDataProperties.h"
#import "WorkorderDetailChemicalListService.h"
#import "WorkorderDetailChemicalListService+CoreDataProperties.h"
#import "EmailDetailServiceAuto.h"
#import "EmailDetailServiceAuto+CoreDataProperties.h"
//#import "ImageDetailsServiceAuto.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "ServiceAutoModifyDate.h"
#import "ServiceAutoModifyDate+CoreDataProperties.h"
#import "SalesAutoModifyDate.h"
#import "SalesAutoModifyDate+CoreDataProperties.h"
#import "BeginAuditView.h"
#import "InvoiceAppointmentView.h"
#import "AppointmentView.h"
#import "AllImportsViewController.h"
#import "DPS-Swift.h"

@class WebService;

@interface ServiceSendMailViewController ()
{
    NSString *strLeadId,*strLeadName,*strServiceUrlMainServiceAutomation;
    NSMutableArray *arrEmail,*arrFinalJson,*arrUpdateLeadId;
    UITableViewCell *cell;
    NSMutableArray *arr,*arr2;
    BOOL chk,temp,flag;
    int val;
    NSMutableArray *arrFinalSend,*arrDuplicateMail;
    
    NSMutableArray  *arrFinalLeadDetail,
    *arrFinalPaymenyInfo,
    *arrFinalImageDetail,
    *arrFinalEmailDetail,
    *arrFinalDocumentsDetail,
    *arrFinalSoldStandard,
    *arrFinalLeadPreference,
    *arrFinalSoldNonStandard;
    
    
    NSMutableDictionary *dictFinal;
    NSMutableDictionary *dictFinalResendMail;
    //......
    Global *global;
    
    NSMutableArray *arrImageName,*arrImageSend;
    NSMutableArray *arrOfAllImagesToSendToServer,*arrOfAllSignatureImagesToSendToServer,*arrOfAllCheckImageToSend,*arrOfAllDocumentsToSend;
    NSString *strModifyDateToSendToServerAll;
    int indexToSendImage,indexToSendImageSign,indexToSendCheckImage,indexToSendDocuments;
    BOOL chkClickStatus;
    NSString *strAudioNameGlobal,*strFromEmail,*strDefaultEmployeeEmail,*strCompanyIdResendMail;
    NSManagedObject *objServiceWorkOrderGlobal;
    NSDictionary *dictDetailsFortblView;
    //Nilind 24 Feb
    NSMutableArray *arrSendEmailCount;
    //End
}
@end

@implementation ServiceSendMailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    global = [[Global alloc] init];

    _tblEmailData.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _tblEmailData.layer.borderWidth=1.0;
    _tblEmailData.layer.cornerRadius=5.0;
    
    
    _table_Document.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _table_Document.layer.borderWidth=1.0;
    _table_Document.layer.cornerRadius=5.0;
    
    indexToSendImage=0;
    indexToSendImageSign=0;
    indexToSendDocuments=0;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    strDefaultEmployeeEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    strCompanyIdResendMail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ServiceAutoCompanyId"]];
    
    dictDetailsFortblView=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    strLeadName=[defsLead valueForKey:@"defsLeadName"];
    
    // Email Fetch
    
    NSManagedObject *objServiceDetailss1=[global fetchServiceWorkOrderObjToFindStatus:strLeadId];
    
    NSString *strGlobalWorkOrderStatus1=[NSString stringWithFormat:@"%@",[objServiceDetailss1 valueForKey:@"workorderStatus"]];
    
    if ([strGlobalWorkOrderStatus1 caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus1 caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus1 caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
        
        [self deleteEmailIdFromCoreData];
        [self fetchDefaultEmailFromDb];
        
    }else{
        
        [self deleteEmailIdFromCoreData];
        [self fetchDefaultEmailFromDb];
        
    }

    //[self deleteEmailIdFromCoreData];
    
    [self serviceEmailFetch];
    
    //[self fetchDefaultEmailFromDb];
    
    //Yaha Call KArna hai mail k liye
    
    NSString *strServiceReportType=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportType"]];
    
    if ([strServiceReportType isEqualToString:@"CompanyEmail"]) {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportEmail"]];
        
    } else {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
        
    }
    
    if(strFromEmail.length==0 || [strFromEmail isEqualToString:@""])
    {
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Email"]];
        
    }
    _lblFromValue.text=strFromEmail;
    
    arrImageName=[[NSMutableArray alloc]init];
    //...........
    //...........
    
    chk=NO;temp=YES;flag=YES;
    val=0;
    
    // Mutable Array  Declaration
    
    arrEmail=[[NSMutableArray alloc]init];
    arr = [NSMutableArray array];
    arrFinalSend=[[NSMutableArray alloc]init];
    arrFinalJson=[[NSMutableArray alloc]init];
    
    dictFinal=[[NSMutableDictionary alloc]init];
    arrUpdateLeadId=[[NSMutableArray alloc]init];
    arrImageSend=[[NSMutableArray alloc]init];
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllCheckImageToSend=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    //..........................................................
    
    
    
    [self serviceEmailFetch];
    
    //........................................................
    
    [_tblEmailData reloadData];
    
    NSUserDefaults *defsIsService=[NSUserDefaults standardUserDefaults];
    [defsIsService setBool:YES forKey:@"isServiceSurvey"];
    [defsIsService setBool:NO forKey:@"isMechanicalSurvey"];
    [defsIsService setBool:NO forKey:@"isNewSalesSurvey"];
    [defsIsService synchronize];
    
    [self finalMails];
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isFromSurvey=[defsss boolForKey:@"isFromSurveyToSendEmail"];
    if (isFromSurvey) {
        
        //[_btnSurvey setHidden:YES];
        
        
         if (_strBackRoundImage == nil) {
             
             UIView *viewTemp=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
             [viewTemp setBackgroundColor:[UIColor whiteColor]];
             [self.view addSubview:viewTemp];
             
         } else {
             
             UIImageView *imgViewBackround=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
             imgViewBackround.image = _strBackRoundImage;
             [self.view addSubview:imgViewBackround];
             
         }
         
         [defsss setBool:NO forKey:@"isFromSurveyToSendEmail"];
         [defsss synchronize];
         
         Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
         NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
         if (netStatusWify1== NotReachable)
         {
         [DejalBezelActivityView removeView];
         [self performSelector:@selector(goToAppointmentView) withObject:nil afterDelay:1.0];
         // [self goToAppointmentView];
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Data saved offline" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
         [alert show];
         }
         else
         {
        
         [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];

         [self performSelector:@selector(FetchFromCoreDataToSendServiceDynamic) withObject:nil afterDelay:0.2];
         
         }
        
        
    }
    
    if ([_isCustomerPresent isEqualToString:@"no"]) {
        
        [_btnSurvey setHidden:YES];
        
    } else {
        
        [_btnSurvey setHidden:NO];
        
    }
    
    NSUserDefaults *defsSurvey=[NSUserDefaults standardUserDefaults];
    
    BOOL yesSurvey=[defsSurvey boolForKey:@"YesSurveyService"];
    if (yesSurvey) {
        [_btnSurvey setHidden:NO];
    } else {
        [_btnSurvey setHidden:YES];
    }
    
    [self serviceWoDocumentsFetch];
    
    [self adjusttableViewHeight];
    
    
    BOOL isSurveyCompleted=[global isSurveyCompletedService:strLeadId];
    
    if (isSurveyCompleted) {
        
        [_btnSurvey setHidden:YES];
        
    }
    
    if ([_isCustomerPresent isEqualToString:@"no"]) {
        
        [_btnSurvey setHidden:YES];
        
    } else {
        
        // [_btnSurvey setHidden:NO];
        
    }
    
    
    NSManagedObject *objServiceDetailss=[global fetchServiceWorkOrderObjToFindStatus:strLeadId];
    
    NSString *strGlobalWorkOrderStatus=[NSString stringWithFormat:@"%@",[objServiceDetailss valueForKey:@"isResendInvoiceMail"]];
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"True"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"true"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"1"] == NSOrderedSame) {
        
        [_btnSendMail setTitle:@"Re-Send Mail" forState:UIControlStateNormal];
        
    }else{
        
        [_btnSendMail setTitle:@"Send Mail" forState:UIControlStateNormal];
        
        NSString *strSendServiceReport=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SendServiceReport"]];
        
        if ([strSendServiceReport isEqualToString:@"AfterWorkorderCompletion"]) {
            
            [_btnSendMail setTitle:@"Send Mail" forState:UIControlStateNormal];
            
        } else {
            // AfterBatchRelease
            [_btnSendMail setTitle:@"Save Preference" forState:UIControlStateNormal];
            
        }

    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    NSManagedObject *objServiceDetailss=[global fetchServiceWorkOrderObjToFindStatus:strLeadId];
    
    NSString *strGlobalWorkOrderStatus=[NSString stringWithFormat:@"%@",[objServiceDetailss valueForKey:@"isResendInvoiceMail"]];
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"True"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"true"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"1"] == NSOrderedSame) {
        
        [_btnSendMail setTitle:@"Re-Send Mail" forState:UIControlStateNormal];
        
    }else{
        
        [_btnSendMail setTitle:@"Send Mail" forState:UIControlStateNormal];
        
        NSString *strSendServiceReport=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SendServiceReport"]];
        
        if ([strSendServiceReport isEqualToString:@"AfterWorkorderCompletion"]) {
            
            [_btnSendMail setTitle:@"Send Mail" forState:UIControlStateNormal];
            
        } else {
            // AfterBatchRelease
            [_btnSendMail setTitle:@"Save Preference" forState:UIControlStateNormal];
            
        }
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionOnAdd:(id)sender
{
    
    _txtEmailId.text =  [_txtEmailId.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (_txtEmailId.text.length==0)
        
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        BOOL chkEmail;
        chkEmail=NO;
        for (int i=0; i<arrEmail.count; i++)
        {
            if ([_txtEmailId.text isEqualToString:[arrEmail objectAtIndex:i]])
            {
                chkEmail=YES;
            }
        }
        if (chkEmail==YES)
        {
            chkEmail=NO;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Email Id already exist" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            BOOL emailChk;
            emailChk=[self NSStringIsValidEmail:_txtEmailId.text];
            if (emailChk==NO)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            else
            {
                
                /*chk=YES;
                 [arrEmail addObject:_txtEmailId.text];
                 [arr addObject:[NSString stringWithFormat:@"%lu",(unsigned long)arrEmail.count-1]];
                 [self salesEmailFetchForSave];
                 [_tblEmailData reloadData];
                 [_tblEmailData scrollRectToVisible:CGRectMake(_tblEmailData.contentSize.width - 1,_tblEmailData.contentSize.height - 1, 1, 1) animated:YES];
                 _txtEmailId.text=@"";*/
                chk=YES;
                [arrEmail addObject:_txtEmailId.text];
                [arr addObject:[NSString stringWithFormat:@"%lu",(unsigned long)arrEmail.count-1]];
                [_txtEmailId resignFirstResponder];
                [self salesEmailFetchForSave];
                //[_tblEmailData scrollRectToVisible:CGRectMake(_tblEmailData.contentSize.width - 1,_tblEmailData.contentSize.height - 1, 1, 1) animated:YES];
                _txtEmailId.text=@"";
                [self adjusttableViewHeight];
                
            }
        }
    }
    [self finalMails];
}

- (IBAction)actionOnCancel:(id)sender
{
    [self serviceEmailFetchToUpdateIsMailSentToFalse];
    [self goToAppointmentView];
}
- (IBAction)actionOnSendEmail:(id)sender
{
    
    NSManagedObject *objServiceDetailss=[global fetchServiceWorkOrderObjToFindStatus:strLeadId];
    
    NSString *strGlobalWorkOrderStatus=[NSString stringWithFormat:@"%@",[objServiceDetailss valueForKey:@"isResendInvoiceMail"]];
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"True"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"true"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"1"] == NSOrderedSame) {
        
        [self resendMailService];
        
    }else{
        
        // [self salesEmailFetchForSave];
        //Nilind 24 Feb
        [self emailCountFetch];
        if (arrSendEmailCount.count==0)
        {
            [global AlertMethod:@"Alert!" :@"No email to send"];
        }
        //End
        else
        {
            
            
            
            Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
            NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
            if (netStatusWify1== NotReachable)
            {
                [DejalBezelActivityView removeView];
                [self goToAppointmentView];
                
                [global updateWoIsMailSent:strLeadId :@"true"];

                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Data saved offline" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
            }
            else
            {
                //[self.view makeToast:@"Sending To Server Please Wait..."];
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];

                [self performSelector:@selector(callMethodAfterDelay) withObject:nil afterDelay:0.2];
                
                // [self FetchFromCoreDataToSendServiceDynamic];
                
            }
        }
        
    }
    
}

-(void)callMethodAfterDelay{
    
    [self FetchFromCoreDataToSendServiceDynamic];
    
}

- (IBAction)actionOnContinueToSetup:(id)sender
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        
        [self goToBeginAuditView];
        
    }
    
    
}
-(void)goToBeginAuditView{
    
    //    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
    //        if([UIScreen mainScreen].bounds.size.height == 480.0)
    //        {
    //            //move to your iphone5 xib
    //            BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView" bundle:nil];
    //            [self.navigationController pushViewController:bav animated:YES];
    //        }
    //        else{
    //            //move to your iphone4s xib
    //            BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView_iphone5" bundle:nil];
    //            [self.navigationController pushViewController:bav animated:YES];
    //        }
    //    }
    //    else{
    //        //BeginAuditView_iPad
    //        BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView_iPad" bundle:nil];
    //        [self.navigationController pushViewController:bav animated:NO];
    //    }
    
    
    if([UIScreen mainScreen].bounds.size.height == 480.0)
    {
        //move to your iphone5 xib
        BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView" bundle:nil];
        [self.navigationController pushViewController:bav animated:YES];
    }
    else{
        //move to your iphone4s xib
        BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView_iphone5" bundle:nil];
        [self.navigationController pushViewController:bav animated:YES];
    }
    
}

-(void)sendingWorkOrderToServer{
    
    [self fetchEquipmentDetailsWorkOrderServiceAuto];
    [self fetchingModifyDate];
    [self fetchWorkOrderDetailServiceAuto];
    [self fetchWOProductDetail];
    [self fetchPaymentInfoServiceAuto];
    [self fetchTermiteFromDb];
    //Florida
    [self fetchTermiteFloridaFromDb];
    
    [self fetchImageDetailServiceAuto];
    [self fetchTermiteImageDetailServiceAuto];
    [self fetchEmailDetailServiceAuto];
    [self fetchWorkOrderDocuments];
    
    [self fetchServicePestDataAllFromDBObjC];
    
    NSLog(@"Final Dictionary >>>>>>%@",dictFinal);
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        
        if (arrOfAllDocumentsToSend.count==0) {
            
            if (arrOfAllImagesToSendToServer.count==0) {
                
                [self uploadingAllImages:0];
                
            } else {
                
                [self uploadingAllImages:0];
                
            }
            
        } else {
            
            [self uploadingAllDocumentsPest:0];
            
        }
        
        /*
        if (arrOfAllImagesToSendToServer.count==0) {
            
            [self uploadingAllImages:0];
            
        } else {
            
            [self uploadingAllImages:0];
            
        }
         */
        
    }
    
}
-(void)FetchFromCoreDataToSendServiceDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];//ServiceDynamicForm
    entityServiceDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    requestNewServiceDynamic = [[NSFetchRequest alloc] init];
    [requestNewServiceDynamic setEntity:entityServiceDynamic];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strLeadId];
    
    [requestNewServiceDynamic setPredicate:predicate];
    
    sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
    
    [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
    
    self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDynamic performFetch:&error];
    arrAllObjServiceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
    if ([arrAllObjServiceDynamic count] == 0)
    {
        
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
        
        [self sendingWorkOrderToServer];
        
        //        NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
        //
        //        BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
        //
        //        if (isEquipEnabled) {
        //
        //             [self FetchFromCoreDataToSendEquipmentDynamicForms];
        //
        //        }else
        //        {
        //             [self sendingWorkOrderToServer];
        //        }
    }
    else
    {
        matchesServiceDynamic=arrAllObjServiceDynamic[0];
        NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
        
        NSDictionary *dictDataService;
        
        if ([arrTemp isKindOfClass:[NSDictionary class]]) {
            
            dictDataService=(NSDictionary*)arrTemp;
            
        } else if ([arrTemp isKindOfClass:[NSArray class]]) {
            
            if (arrTemp.count==0) {
                
                //dictDataService=arrTemp[0];

            } else {
                
                dictDataService=arrTemp[0];

            }
            
        } else {
            
            
        }
        
        [self sendingFinalDynamicJsonToServerService:dictDataService];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)sendingFinalDynamicJsonToServerService :(NSDictionary*)dictFinalDynamicJson{
    // NSDictionary *dictFinalDynamicJson=(NSDictionary*)arrTemp;
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL Service FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlServiceDynamicFormSubmission];
    
        
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 // [DejalBezelActivityView removeView];
                 if (success)
                 {
                     //                     NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
                     //
                     //                     BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
                     //
                     //                     if (isEquipEnabled) {
                     //
                     //                         [self FetchFromCoreDataToSendEquipmentDynamicForms];
                     //
                     //                     }else{
                     
                     [self sendingWorkOrderToServer];
                     
                     //                     }
                     
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     if ([str containsString:@"Success"]) {
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                     }else{
                         
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}
//New Change For EquipMent List

-(void)FetchFromCoreDataToSendEquipmentDynamicForms :(BOOL)isUpdated{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityServiceDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    requestNewServiceDynamic = [[NSFetchRequest alloc] init];
    [requestNewServiceDynamic setEntity:entityServiceDynamic];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadId];
    
    [requestNewServiceDynamic setPredicate:predicate];
    
    sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
    
    [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
    
    self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDynamic performFetch:&error];
    arrAllObjServiceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
    if ([arrAllObjServiceDynamic count] == 0)
    {
        
        //[self methodOnResponse];  // Commenting to update jdevice insepction form new
        
        [self FetchFromCoreDataToSendDeviceDynamicForms :true];
        
    }
    else
    {
        
        NSMutableArray *arrOfEquipToSend=[[NSMutableArray alloc] init];
        
        for (int k=0; k<arrAllObjServiceDynamic.count; k++) {
            
            matchesServiceDynamic=arrAllObjServiceDynamic[k];
            NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
            
            NSDictionary *dictDataService;
            
            if ([arrTemp isKindOfClass:[NSDictionary class]]) {
                
                dictDataService=(NSDictionary*)arrTemp;
                
            } else if ([arrTemp isKindOfClass:[NSArray class]]) {
                
                if (arrTemp.count==0) {
                    
                    //dictDataService=arrTemp[0];
                    
                } else {
                    
                    dictDataService=arrTemp[0];
                    
                }
                
            }
            
            [arrOfEquipToSend addObject:dictDataService];
            
            
        }
        
        //        matchesServiceDynamic=arrAllObjServiceDynamic[0];
        //        NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
        //
        //        NSDictionary *dictDataService;
        //
        //        if ([arrTemp isKindOfClass:[NSDictionary class]]) {
        //
        //            dictDataService=(NSDictionary*)arrTemp;
        //
        //        } else {
        //
        //            dictDataService=arrTemp[0];
        //
        //        }
        
        if (arrOfEquipToSend.count==0) {
            
            //[self methodOnResponse];  // Commenting to update jdevice insepction form new
            
            [self FetchFromCoreDataToSendDeviceDynamicForms :true];
            
            
        } else {
            
            if (isUpdated) {
                
                [self sendingFinalDynamicJsonToServerEquipmentDynamicForm:arrOfEquipToSend];
                
            } else {
                
                //[self methodOnResponse];  // Commenting to update jdevice insepction form new
                
                [self FetchFromCoreDataToSendDeviceDynamicForms :true];
                
            }
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)sendingFinalDynamicJsonToServerEquipmentDynamicForm :(NSArray*)arrFinalDynamicJson{
    // NSDictionary *dictFinalDynamicJson=(NSDictionary*)arrTemp;
    
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrFinalDynamicJson,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"EquipmentInspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL Service FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlEquipmentDynamicFormSubmission];
    
    NSLog(@"Equpment Sending URL=====%@",strUrl);
    
    
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 // [DejalBezelActivityView removeView];
                 if (success)
                 {
                     
                     [self methodOnResponse];
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

-(void)methodOnResponse{
    
    [objServiceWorkOrderGlobal setValue:@"no" forKey:@"zSync"];
    [objServiceWorkOrderGlobal setValue:@"True" forKey:@"isResendInvoiceMail"];
    NSError *error;
    [context save:&error];
    
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"Data Synced Successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    [self goToAppointmentView];
    
}

// 2nd Sept
#pragma mark -SalesAuto Fetch Core Data

-(void)serviceEmailFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    arrEmail=[[NSMutableArray alloc]init];
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrEmail addObject:[matches valueForKey:@"emailId"]];
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"emailId"]);
        
    }
    
    chk=YES;
    
    [_tblEmailData reloadData];
    // [_table_Document reloadData];
    
}

-(void)serviceWoDocumentsFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrderDocuments=[NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context];
    requestNewWorkOrderDocuments = [[NSFetchRequest alloc] init];
    [requestNewWorkOrderDocuments setEntity:entityWorkOrderDocuments];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [requestNewWorkOrderDocuments setPredicate:predicate];
    
    sortDescriptorWorkOrderDocuments = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptorsWorkOrderDocuments = [NSArray arrayWithObject:sortDescriptorWorkOrderDocuments];
    
    [requestNewWorkOrderDocuments setSortDescriptors:sortDescriptorsWorkOrderDocuments];
    
    self.fetchedResultsWorkOrderDocuments = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrderDocuments managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsWorkOrderDocuments setDelegate:self];
    
    // Perform Fetch
    //  arrEmail=[[NSMutableArray alloc]init];
    NSError *error1 = nil;
    [self.fetchedResultsWorkOrderDocuments performFetch:&error1];
    arrAllObjWorkOrderDocuments = [self.fetchedResultsWorkOrderDocuments fetchedObjects];
    
    if (arrAllObjWorkOrderDocuments.count==0)
    {
        
    }else
    {
        
        
        
    }
    
    [_table_Document reloadData];
    
}


#pragma mark- Table Delegate Method
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==_tblEmailData)
    {
        NSString *strEmailExist=[NSString stringWithFormat:@"%@",arrEmail[indexPath.row]];
        
        NSString *strEmailExist1;
        
        if (arrAllObj.count > indexPath.row) {
            
            matches=[arrAllObj objectAtIndex:indexPath.row];
            
            //strEmailExist1=[NSString stringWithFormat:@"%@",[matches valueForKey:@"EmailId"]];
            
            BOOL hasEmailId = [[matches.entity propertiesByName] objectForKey:@"emailId"] != nil;
            
            if (hasEmailId) {
                
                strEmailExist1=[NSString stringWithFormat:@"%@",[matches valueForKey:@"emailId"]];
                
            }else{
                
                strEmailExist1 = @"";

            }
            
            
        }else{
            
            strEmailExist1 = @"";
            
        }
        
        if (strEmailExist.length==0) {
            
            return 0;
            
        } else if (strEmailExist1.length==0) {
            
            return 0;
            
        } else {
            
            return 45;
            
        }
        
    }
    else
    {
        return 45;
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_tblEmailData)
    {
        return arrEmail.count;
        
    }
    else
    {
        return arrAllObjWorkOrderDocuments.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblEmailData)
    {
        static NSString *identifier=@"cell";
        
        cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        matches=[arrAllObj objectAtIndex:indexPath.row];
        
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"emailId"]];
        
        if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isMailSent"]] isEqualToString:@"true"])
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            NSString *strEmailExist1=[NSString stringWithFormat:@"%@",[matches valueForKey:@"emailId"]];
            
            if (strEmailExist1.length==0) {
                
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                
            } else {
                
                
            }
            
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        cell.textLabel.font=[UIFont systemFontOfSize:14];
        return cell;
    }
    else
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell11;
        cell11=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell11==nil)
        {
            cell11=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSManagedObject *objWoDocuments=arrAllObjWorkOrderDocuments[indexPath.row];
        
        cell11.textLabel.text=[NSString stringWithFormat:@"%@",[objWoDocuments valueForKey:@"title"]];
        
        cell11.textLabel.font=[UIFont systemFontOfSize:14];
        
        if ([[NSString stringWithFormat:@"%@",[objWoDocuments valueForKey:@"isChecked"]] isEqualToString:@"true"])
        {
            [cell11 setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            
            //[cell11 setAccessoryType:UITableViewCellAccessoryNone];
            NSString *strIsDefaualt=[NSString stringWithFormat:@"%@",[objWoDocuments valueForKey:@"isDefault"]];
            if ([strIsDefaualt isEqualToString:@"1"] || [strIsDefaualt isEqualToString:@"true"] || [strIsDefaualt isEqualToString:@"True"]) {
                
                [cell11 setAccessoryType:UITableViewCellAccessoryCheckmark];
                
            } else {
                
                [cell11 setAccessoryType:UITableViewCellAccessoryNone];
                
            }

        }
        
        return cell11;
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblEmailData)
    {
        
        [self serviceEmailFetch];
        
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            chkClickStatus=YES;
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            chkClickStatus=NO;
        }
        NSManagedObject *record = [arrAllObj objectAtIndex:indexPath.row];
        [self updateMailStatus:record];
        [global updateSalesModifydate:strLeadId];
    }
    else
    {
        
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            
            NSError *error1 = nil;
            NSManagedObject *record = [arrAllObjWorkOrderDocuments objectAtIndex:indexPath.row];
            [record setValue:@"true" forKey:@"isChecked"];
            [context save:&error1];
            
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            NSError *error1 = nil;
            NSManagedObject *record = [arrAllObjWorkOrderDocuments objectAtIndex:indexPath.row];
            [record setValue:@"false" forKey:@"isChecked"];
            [context save:&error1];
            
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        
        NSLog(@"table document called");
    }
}
-(void)finalMails
{
    //    arrFinalSend=[[NSMutableArray alloc]init];
    //    for (int i=0; i<arr.count; i++)
    //    {
    //        int value;
    //        value=[[arr objectAtIndex:i]intValue];
    //        [arrFinalSend addObject:[arrEmail objectAtIndex:value]];
    //
    //    }
    //    NSLog(@"ArrFinalSend >>%@",arrFinalSend);
}



#pragma mark- fetching for service Automation

-(void)fetchingModifyDate{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityServiceModifyDate=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityServiceModifyDate];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadId];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        strModifyDateToSendToServerAll=[matchesServiceModifyDate valueForKey:@"modifyDate"];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

-(void)fetchWorkOrderDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"WorkorderDetail"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            objServiceWorkOrderGlobal=nil;
            objServiceWorkOrderGlobal=matches;
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            NSArray *arrLeadDetailKey;
            //arrLeadDetailKey=[[NSMutableArray alloc]init];
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
            
            NSString *strCustomerSign=[matches valueForKey:@"customerSignaturePath"];
            NSString *strTechSign=[matches valueForKey:@"technicianSignaturePath"];
            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
            
            if (!(strCustomerSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
            }
            
            if (!(strTechSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
            }
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            NSDictionary *dictLeadDetail;
            dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            NSLog(@"LeadDetail%@",dictLeadDetail);
            //[dictFinal setObject:dictLeadDetail forKey:@"LeadDetail"];
            
            int indexToRemove=-1;
            int indexToRemoveDate=-1;
            int indexToReplaceModifyDate=-1;
            int indexToReplaceScheduleDated=-1;
            
            for (int k=0; k<arrLeadDetailKey.count; k++) {
                
                NSString *strKeyLeadId=arrLeadDetailKey[k];
                //dateModified
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"dateModified"]) {
                    
                    indexToRemoveDate=k;
                    
                }
                
                if ([strKeyLeadId isEqualToString:@"modifiedFormatedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"zdateScheduledStart"]) {
                    
                    indexToReplaceScheduleDated=k;
                    
                }
                
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrLeadDetailKey];
            [arrKeyTemp removeObjectAtIndex:indexToRemoveDate];
            // [arrKeyTemp removeObjectAtIndex:indexToRemove-1];
            arrLeadDetailKey=arrKeyTemp;
            [arrLeadDetailValue removeObjectAtIndex:indexToRemoveDate];
            //[arrLeadDetailValue removeObjectAtIndex:indexToRemove-1];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceScheduleDated-1 withObject:@""];
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            //  [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
            // Commit
            //IsNPMATermite
            
            NSString *strBoolKey = [NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"isNPMATermite"]];
            
            BOOL isTrue = [strBoolKey boolValue];
            
            if (isTrue) {
                
                [dictTemp setValue:@"true" forKey:@"isNPMATermite"];
                
            } else {
                
                [dictTemp setValue:@"false" forKey:@"isNPMATermite"];
                
            }
            
            NSLog(@"%@",dictTemp);
            
            // Set Is Mail Sent True For Sending Mail.
            
            [dictTemp setValue:@"true" forKey:@"isMailSent"];

            [dictFinal setObject:dictTemp forKey:@"WorkorderDetail"];
            
        }
    }
}

-(void)fetchPaymentInfoServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfoServiceAuto= [NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfoServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:arrPaymentInfoValue forKey:@"PaymentInfo"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
                
                NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
                NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
                
                if (!(strCheckFrontImage.length==0)) {
                    
                    [arrOfAllCheckImageToSend addObject:strCheckFrontImage];//salesSignature
                    
                }
                if (!(strCheckBackImage.length==0)) {
                    
                    [arrOfAllCheckImageToSend addObject:strCheckBackImage];//salesSignature
                    
                }
                
                NSArray *arrPaymentInfoKey;
                NSMutableArray *arrPaymentInfoValue;
                
                arrPaymentInfoValue=[[NSMutableArray alloc]init];
                arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrPaymentInfoKey);
                
                NSLog(@"all keys %@",arrPaymentInfoValue);
                for (int i=0; i<arrPaymentInfoKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                    [arrPaymentInfoValue addObject:str];
                }
                
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                int indexToRemove=-1;
                int indexToReplaceModifyDate=-1;
                
                for (int k=0; k<arrPaymentInfoKey.count; k++) {
                    
                    NSString *strKeyLeadId=arrPaymentInfoKey[k];
                    
                    if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                        
                        indexToRemove=k;
                        
                    }
                    if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                        
                        indexToReplaceModifyDate=k;
                        
                    }
                }
                
                NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
                
                [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
                
                [arrKeyTemp removeObjectAtIndex:indexToRemove];
                arrPaymentInfoKey=arrKeyTemp;
                [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
                
                [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                
                NSDictionary *dictPaymentInfo;
                dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
                NSLog(@"PaymentInfo%@",dictPaymentInfo);
                [dictFinal setObject:dictPaymentInfo forKey:@"PaymentInfo"];
            }
            
        }
    }
}


-(void)fetchWOProductDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWOProductServiceAuto= [NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context];
    [request setEntity:entityWOProductServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:arrPaymentInfoValue forKey:@"WoProductDetail"];
        
    }else
    {
        //        for (int k=0; k<arrAllObj.count; k++)
        //        {
        //            matches=arrAllObj[k];
        //            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        //            for (int k=0; k<arrAllObj.count; k++)
        //            {
        matches=arrAllObj[0];
        [dictFinal setObject:[matches valueForKey:@"chemicalList"] forKey:@"WoProductDetail"];
        //WoOtherProductDetail
        [dictFinal setObject:[matches valueForKey:@"otherChemicalList"] forKey:@"WoOtherProductDetail"];
        //  }
        
        //  }
    }
}

-(void)fetchTermiteFromDb{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    entityWorkOrder=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
        //        NSMutableArray *arrTexasTermiteServiceDetail;
        //
        //        arrTexasTermiteServiceDetail=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:@"" forKey:@"TexasTermiteServiceDetail"];
        
    }
    else
    {
        
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        NSMutableDictionary *dictGlobalTermiteTexas;
        
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteTexas=(NSMutableDictionary*)[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"];
        
        
        NSString *strBuyersInitials_SignaturePath_10B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_10B"]];
        if (!(strBuyersInitials_SignaturePath_10B.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strBuyersInitials_SignaturePath_10B];
            
        }
        
        
        NSString *strGraphPath__TexasOffiicial_WoodDestroyingInsect=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"GraphPath__TexasOffiicial_WoodDestroyingInsect"]];
        if (!(strGraphPath__TexasOffiicial_WoodDestroyingInsect.length==0)){
            
            [arrOfAllImagesToSendToServer addObject:strGraphPath__TexasOffiicial_WoodDestroyingInsect];
            
            
        }
        
        
        NSString *strInspector_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Inspector_SignaturePath"]];
        if (!(strInspector_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strInspector_SignaturePath];
            
        }
        
        
        NSString *strCertifiedApplicator_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_SignaturePath"]];
        if (!(strCertifiedApplicator_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strCertifiedApplicator_SignaturePath];
            
        }
        
        
        NSString *strSignatureOfPurchaserOfProperty_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfPurchaserOfProperty_SignaturePath"]];
        if (!(strSignatureOfPurchaserOfProperty_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strSignatureOfPurchaserOfProperty_SignaturePath];
            
        }
        
        
        NSString *strBuyersInitials_SignaturePath_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_7B"]];
        if (!(strBuyersInitials_SignaturePath_7B.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strBuyersInitials_SignaturePath_7B];
            
        }
        
        [dictFinal setObject:[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"] forKey:@"TexasTermiteServiceDetail"];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)fetchTermiteFloridaFromDb
{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetailFlorida;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
    requestTermiteFlorida = [[NSFetchRequest alloc] init];
    [requestTermiteFlorida setEntity:entityTermiteFlorida];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadId];
    
    [requestTermiteFlorida setPredicate:predicate];
    
    sortDescriptorTermiteFlorida = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsTermiteFlorida = [NSArray arrayWithObject:sortDescriptorTermiteFlorida];
    
    [requestTermiteFlorida setSortDescriptors:sortDescriptorsTermiteFlorida];
    
    fetchedResultsControllerWorkOrderDetailFlorida = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTermiteFlorida managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetailFlorida setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetailFlorida performFetch:&error];
    arrAllObjTermiteFlorida = [fetchedResultsControllerWorkOrderDetailFlorida fetchedObjects];
    if ([arrAllObjTermiteFlorida count] == 0)
    {
        
        [dictFinal setObject:@"" forKey:@"FloridaTermiteServiceDetail"];
        
    }
    else
    {
        
        matchesWorkOrderFlorida=arrAllObjTermiteFlorida[0];
        
        NSMutableDictionary *dictGlobalTermiteTexas;
        
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteTexas=(NSMutableDictionary*)[matchesWorkOrderFlorida valueForKey:@"floridaTermiteServiceDetail"];
        
        NSString *strSignatureLicenseAgrent=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfLicenseOrAgent"]];
        if (!(strSignatureLicenseAgrent.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strSignatureLicenseAgrent];
            
        }
        
        [dictFinal setObject:[matchesWorkOrderFlorida valueForKey:@"floridaTermiteServiceDetail"] forKey:@"FloridaTermiteServiceDetail"];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchImageDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetailServiceAuto= [NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityImageDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            // [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}

//Nilind 11 Dec
-(void)fetchTermiteImageDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetailsTermite];
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    NSMutableArray *arrOfTermiteImages;
    arrOfTermiteImages=[[NSMutableArray alloc]init];
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerImageDetail fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImageDetailsTermite"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            // [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"TexasTermiteImagesDetail"];
        
    }
}

-(void)fetchWorkOrderDocuments{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOrderDocuments= [NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context];
    [request setEntity:entityWorkOrderDocuments];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptorWorkOrderDocuments = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptorsWorkOrderDocuments = [NSArray arrayWithObject:sortDescriptorWorkOrderDocuments];
    
    [request setSortDescriptors:sortDescriptorsWorkOrderDocuments];
    
    self.fetchedResultsWorkOrderDocuments = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsWorkOrderDocuments setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsWorkOrderDocuments performFetch:&error1];
    arrAllObjWorkOrderDocuments = [self.fetchedResultsWorkOrderDocuments fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObjWorkOrderDocuments.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"WoOtherDocuments"];
        
    }else
    {
        
        for (int k=0; k<arrAllObjWorkOrderDocuments.count; k++)
        {
            matchesWorkOrderDocuments=arrAllObjWorkOrderDocuments[k];
            NSLog(@"Lead IDDDD====%@",[matchesWorkOrderDocuments valueForKey:@"workorderId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesWorkOrderDocuments entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesWorkOrderDocuments valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesWorkOrderDocuments valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"WoOtherDocuments%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"WoOtherDocuments"];
        
    }
    
}

-(void)fetchEmailDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}


-(void)ServiceEmailFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrEmail addObject:[matches valueForKey:@"emailId"]];
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"emailId"]);
        
    }
    chk=YES;
    [_tblEmailData reloadData];
    
}

-(void)fetchEquipmentDetailsWorkOrderServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:arrEmailDetail forKey:@"WoEquipmentDetail"];
        
    }else
    {
        //        for (int k=0; k<arrAllObj.count; k++)
        //        {
        matches=arrAllObj[0];
        objServiceWorkOrderGlobal=nil;
        objServiceWorkOrderGlobal=matches;
        
        NSArray *arrToSend=[matches valueForKey:@"arrOfEquipmentList"];
        
        NSArray *objValue=[NSArray arrayWithObjects:
                           arrToSend,
                           arrToSend,nil];
        NSArray *objKey=[NSArray arrayWithObjects:
                         @"WOEquipmentDcs",
                         @"WOEquipmentDc",nil];
        
        NSMutableDictionary *dictTemp;
        dictTemp=[[NSMutableDictionary alloc]init];
        dictTemp=[NSMutableDictionary dictionaryWithObjects:objValue forKeys:objKey];
        
        //  [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
        //  [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
        NSLog(@"%@",dictTemp);
        [dictFinal setObject:arrToSend forKey:@"WoEquipmentDetail"];
        //        }
    }
}

#pragma mark- FINAL JSON SENDING

-(void)fialJson
{
    ///api/MobileToSaleAuto/UpdateLeadDetail
    
    NSDictionary *dictServiceAddress=[dictFinal valueForKey:@"WorkorderDetail"];
    
    if ([dictServiceAddress isKindOfClass:[NSDictionary class]]) {

        NSString *strImageNameserviceAddress=[dictServiceAddress valueForKey:@"serviceAddressImagePath"];
        
        if (!(strImageNameserviceAddress.length==0)) {
            
            [self uploadImageServiceAddImage:strImageNameserviceAddress];
            
        }
        
    }
    
    //Nilind upload Termite Image
    NSArray *arrTermiteImage=[dictFinal valueForKey:@"TexasTermiteImagesDetail"];
    for (int i=0; i<arrTermiteImage.count; i++)
    {
        NSDictionary *dict=[arrTermiteImage objectAtIndex:i];
        NSString *strImageNameTermite=[dict valueForKey:@"woImagePath"];
        if (!(strImageNameTermite.length==0)) {
            
            [self uploadImageTermite:strImageNameTermite];
            
        }
    }
    //[self uploadImageTermite:[NSString stringWithFormat:@"%@",[dictFinal valueForKeyPath:@"FloridaTermiteServiceDetail.SignatureOfLicenseOrAgent"]]];
    
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictFinal])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictFinal options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Final Service Automation JSON: %@", jsonString);
            }
            
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlUpdateWorkorderDetail];
        
        
        //============================================================================
        //============================================================================
        NSDictionary *dictTemp;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"serviceOrder" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     //Sending Dynamic Json
                     
                     //[DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         [global updateWoIsMailSent:strLeadId :@"false"];
                         
                         NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
                         
                         BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
                         
                         if (isEquipEnabled) {
                             
                             NSString *strResult=[NSString stringWithFormat:@"%@",[response valueForKey:@"Result"]];
                             
                             NSString *strResultWorkOrderId=[NSString stringWithFormat:@"%@",[response valueForKey:@"WorkOrderId"]];
                             
                             BOOL isUpdatedd=YES;
                             
                             if ([strResult isEqualToString:strResultWorkOrderId]) {
                                 
                                 isUpdatedd=YES;
                                 
                             } else {
                                 
                                 isUpdatedd=NO;
                                 
                             }
                             
                             [self FetchFromCoreDataToSendEquipmentDynamicForms :isUpdatedd];
                             
                         }else
                         {
                             
                             //[self methodOnResponse];  // Commenting to update jdevice insepction form new
                             
                             [self FetchFromCoreDataToSendDeviceDynamicForms :true];

                         }
                         
                         if ([[response valueForKey:@"RegularflowResults"] isKindOfClass:[NSDictionary class]]) {
                             
                             NSDictionary *dictRegularPestFlow=[response valueForKey:@"RegularflowResults"];
                             
                             WebService *objWebService = [[WebService alloc] init];
                             
                            // [objWebService savingServicePestAllDataDBWithDictWorkOrderDetail:dictRegularPestFlow strWoId:[NSString stringWithFormat:@"%@",[response valueForKey:@"WorkOrderId"]] strType:@"WithoutWoDetail"];
                             
                             [objWebService savingServicePestAllDataDBWithDictWorkOrderDetail:dictRegularPestFlow strWoId:[NSString stringWithFormat:@"%@",[response valueForKey:@"WorkOrderId"]] strType:@"WithoutWoDetail" strToDeleteAreasDevices:@"Yes" ];

                         }
                         
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
    }
}


#pragma mark- FINAL IMAGE SEND

-(void)uploadingAllImages :(int)indexToSendimages{
    
    if (arrOfAllImagesToSendToServer.count==indexToSendimages) {
        
        [self uploadingAllImagesSignature:0];
        
    } else {
        
        [self uploadImage:arrOfAllImagesToSendToServer[indexToSendimages]];
        
    }
}
-(void)uploadingAllImagesSignature :(int)indexToSendimages{
    
    if (arrOfAllSignatureImagesToSendToServer.count==indexToSendimages) {
        
        [self uploadingAllImagesCheckImages:0];
        
    } else {
        
        [self uploadImageSignature:arrOfAllSignatureImagesToSendToServer[indexToSendimages]];
        
    }
}
-(void)uploadingAllImagesCheckImages :(int)indexToSendimages{
    
    if (arrOfAllCheckImageToSend.count==indexToSendimages) {
        
        if (strAudioNameGlobal.length==0 || [strAudioNameGlobal isEqualToString:@"(null)"]) {
            
            [self fialJson];
            
        } else {
            
            [self uploadAudio:strAudioNameGlobal];
            
        }
        
        
    } else {
        
        [self uploadImageCheckImage:arrOfAllCheckImageToSend[indexToSendimages]];
        
    }
}

//============================================================================
//============================================================================
#pragma mark- Upload Image METHOD
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
        }
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        
    }
    
    indexToSendImage++;
    
    [self uploadingAllImages:indexToSendImage];
    
}

-(void)uploadImageSignature :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
        }
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
        
    }
    
    indexToSendImageSign++;
    [self uploadingAllImagesSignature:indexToSendImageSign];
    
}

-(void)uploadImageCheckImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
        }
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
    }
    
    indexToSendCheckImage++;
    [self uploadingAllImagesCheckImages:indexToSendCheckImage];
}


//============================================================================
//============================================================================
#pragma mark- Upload Audio METHOD
//============================================================================
//============================================================================


-(void)uploadAudio :(NSString*)strAudioName
{
    if ((strAudioName.length==0) && [strAudioName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSRange equalRange = [strAudioName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strAudioName = [strAudioName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlAudioUploadAsync];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Audio Sent");
        }
        
    }
    [self fialJson];
    
}

//============================================================================
//============================================================================
#pragma mark- TEXT FIELD DELEGATE
//============================================================================
//============================================================================
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtEmailId resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==_txtEmailId) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        else{
            return YES;
        }
    }
    else{
        return YES;
    }

}

#pragma mark- SAVE EMAIL TO CORE DATA


-(void)salesEmailFetchForSave
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [requestNew setSortDescriptors:sortDescriptors];
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrDuplicateMail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrDuplicateMail addObject:[matches valueForKey:@"emailId"]];
        }
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"emailId"]);
    }
    NSMutableArray *arrFinalMail;
    arrFinalMail=[[NSMutableArray alloc]init];
    arrFinalMail=arrEmail;//arrFinalSend;
    for (int i=0; i<arrFinalMail.count; i++)
    {
        for (int j=0; j<arrDuplicateMail.count; j++)
        {
            if ([[arrFinalMail objectAtIndex:i] isEqualToString:[arrDuplicateMail objectAtIndex:j]])
            {
                [arrFinalMail removeObjectAtIndex:i];
            }
        }
    }
    [self saveEmailToCoreData:arrFinalMail];
    
}

-(void)saveEmailToCoreData:(NSMutableArray *)arrEmailDetail
{
    for (int j=0; j<arrEmailDetail.count; j++)
    {
        //Email Detail Entity
        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
        objEmailDetail.createdBy=@"0";;
        
        objEmailDetail.createdDate=@"0";
        
        objEmailDetail.emailId=[arrEmailDetail objectAtIndex:j];
        
        objEmailDetail.isCustomerEmail=@"true";
        
        objEmailDetail.isMailSent=@"true";
        
        objEmailDetail.woInvoiceMailId=@"";
        
        objEmailDetail.modifiedBy=@"";
        
        objEmailDetail.modifiedDate=[global modifyDate];
        
        objEmailDetail.subject=@"";
        
        objEmailDetail.workorderId=strLeadId;
        
        NSError *error1;
        
        [context save:&error1];
        
    }
    [self serviceEmailFetch];
}
//Nilind 17 Oct
-(void)updateMailStatus:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    
    NSError *error1 = nil;
    matches=indexValue;
    if (chkClickStatus==YES)
    {
        [matches setValue:@"true" forKey:@"isMailSent"];
    }
    else
    {
        [matches setValue:@"false" forKey:@"isMailSent"];
    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    [context save:&error1];
    [self serviceEmailFetch];
}

//....................................

-(void)fetchTempEmail
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    context = [appDelegate managedObjectContext];
    
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    
    requestNew = [[NSFetchRequest alloc] init];
    
    [requestNew setEntity:entityEmailDetailServiceAuto];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
        }
    }
}

//....................................
//....................................
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
//------------------------------------------
-(void)goToAppointmentView{
    
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment"
                                                                 bundle: nil];
        AppointmentVC
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        AppointmentView
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
}
-(void)goToAppointmentViewNew{
    
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"]) {

        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentVC class]]) {
                index=k1;
                //break;
            }
        }
        AppointmentVC *myController = (AppointmentVC *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        
    } else {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentView class]]) {
                index=k1;
                //break;
            }
        }
        AppointmentView *myController = (AppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
        
    }

    
}

//Nilind 24 Feb
-(void)emailCountFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew;
    arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesNew;
    arrSendEmailCount=[[NSMutableArray alloc]init];
    if (arrAllObjNew.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjNew.count; i++)
        {
            matchesNew=arrAllObjNew[i];
            if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isMailSent"]]isEqualToString:@"true"] )
            {
                // [arrSendEmailCount addObject:@"%@",[NSString stringWithFormat:@"%d",i]];
                [arrSendEmailCount addObject:[NSString  stringWithFormat:@"%d",i] ];
            }
            
        }
        
        
    }
    
}
//End

- (IBAction)action_Back:(id)sender {
    
    if ([_fromWhere isEqualToString:@"Appointment"]) {
        
        NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
        
        NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
        
        if ([strAppointmentFlow isEqualToString:@"New"]) {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentVC class]]) {
                    index=k1;
                    //break;
                }
            }
            AppointmentVC *myController = (AppointmentVC *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
        } else {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentView class]]) {
                    index=k1;
                    //break;
                }
            }
            AppointmentView *myController = (AppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
        }

        
    } else if ([_fromWhere isEqualToString:@"PestInvoice"]) {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[InvoiceDetailVC class]]) {
                index=k1;
                //break;
            }
        }
        InvoiceDetailVC *myController = (InvoiceDetailVC *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
        
    }else {
        
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[InvoiceAppointmentView class]]) {
                index=k1;
                //break;
            }
        }
        InvoiceAppointmentView *myController = (InvoiceAppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
        
        
    }
    
}


///New Change For Email Customs

-(void)deleteEmailIdFromCoreData{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND isDefaultEmail=%@",strLeadId,@"true"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self deleteEmailNew];
}

-(void)deleteEmailNew{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND isDefaultEmail=%@",strLeadId,@"1"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)saveEmailToCoreDataDefaults:(NSMutableArray *)arrEmailDetail :(NSMutableArray *)arrOfDefaultsEmailsInvoiceId
{
    for (int j=0; j<arrEmailDetail.count; j++)
    {
        
        if ([arrEmail containsObject:[arrEmailDetail objectAtIndex:j]]) {
            
            
            
        } else {

        //Email Detail Entity
        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
        objEmailDetail.createdBy=@"0";;
        
        objEmailDetail.createdDate=@"0";
        
        objEmailDetail.emailId=[arrEmailDetail objectAtIndex:j];//arrOfDefaultsEmailsInvoiceId
        
        objEmailDetail.isCustomerEmail=@"true";
        
        objEmailDetail.isMailSent=@"true";
        
        objEmailDetail.woInvoiceMailId=[arrOfDefaultsEmailsInvoiceId objectAtIndex:j];
        
        objEmailDetail.modifiedBy=@"";
        
        objEmailDetail.modifiedDate=[global modifyDate];
        
        objEmailDetail.subject=@"";
        
        objEmailDetail.workorderId=strLeadId;
        
        objEmailDetail.isDefaultEmail=@"true";
        
        NSError *error1;
        
        [context save:&error1];
            
        }
        
    }
}
-(void)saveEmployeeEmailDefaultsTrue{
    
    //Email Detail Entity
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
    objEmailDetail.createdBy=@"0";;
    
    objEmailDetail.createdDate=@"0";
    
    objEmailDetail.emailId=strDefaultEmployeeEmail;
    
    objEmailDetail.isCustomerEmail=@"true";
    
    objEmailDetail.isMailSent=@"true";
    
    objEmailDetail.woInvoiceMailId=@"";
    
    objEmailDetail.modifiedBy=@"";
    
    objEmailDetail.modifiedDate=[global modifyDate];
    
    objEmailDetail.subject=@"";
    
    objEmailDetail.workorderId=strLeadId;
    
    objEmailDetail.isDefaultEmail=@"true";
    
    NSError *error1;
    
    [context save:&error1];
    
}

-(void)fetchDefaultEmailFromDb{
    
    NSArray *arrOfDefaultEmailsDB=[dictDetailsFortblView valueForKey:@"DefaultEmails"];
    
    NSManagedObject *objServiceDetailss1=[global fetchServiceWorkOrderObjToFindStatus:strLeadId];

    NSString *strBranchSysName=[NSString stringWithFormat:@"%@",[objServiceDetailss1 valueForKey:@"branchId"]];

    if ([arrOfDefaultEmailsDB isKindOfClass:[NSArray class]]) {
        
        NSMutableArray *arrOfDefaultsEmails=[[NSMutableArray alloc]init];
        NSMutableArray *arrOfDefaultsEmailsInvoiceId=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfDefaultEmailsDB.count; k++) {
            
            NSDictionary *dictData=arrOfDefaultEmailsDB[k];
            
            NSString *strBranchSysNameLocal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BranchId"]];

            if ([strBranchSysName isEqualToString:strBranchSysNameLocal]) {
             
                NSString *strEmailIds=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EmailId"]];
                if ([strEmailIds isEqualToString:@"##EmployeeEmail##"]) {
                    
                    if ([arrEmail containsObject:strDefaultEmployeeEmail]) {
                        
                        
                        
                    } else {
                        
                        [self saveEmployeeEmailDefaultsTrue];
                        
                    }
                    
                } else {
                    
                    [arrOfDefaultsEmails addObject:strEmailIds];
                    [arrOfDefaultsEmailsInvoiceId addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WoInvoiceMailId"]]];
                    
                }
                
            }

        }

        [self saveEmailToCoreDataDefaults:arrOfDefaultsEmails :arrOfDefaultsEmailsInvoiceId];
        
    } else {
        
    }
}
#pragma  mark- -------------------- DOCUMENTS CODE ---------------------

-(void)adjusttableViewHeight
{
    [self serviceEmailFetch];
    
    _const_SendmailTable_H.constant=45*arrEmail.count;
    _const_DocumentTable_H.constant=45*arrAllObjWorkOrderDocuments.count;
    
    NSInteger height;
    height=_const_SendmailTable_H.constant+_const_DocumentTable_H.constant;
    _constView_H.constant=900;
    _constView_H.constant=_constView_H.constant+height;
    
    [_scrollViewSendMail setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_constView_H.constant+height)];
    
}

-(void)resendMailService{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Re-Sending Mail..."];
    
    ///api/MobileToSaleAuto/UpdateLeadDetail
    
    dictFinalResendMail=[[NSMutableDictionary alloc]init];
    
    [dictFinalResendMail setObject:strCompanyIdResendMail forKey:@"CompanyId"];
    //WorkorderId
    
    [dictFinalResendMail setObject:strLeadId forKey:@"WorkorderId"];
    
    [dictFinalResendMail setObject:strLeadId forKey:@"WorkOrderNo"];
    
    //Adding company id and workorder no
    
    [self fetchingModifyDate];
    
    [self fetchWorkOrderDocumentsResendMail];
    
    [self fetchEmailDetailServiceAutoResendMail];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictFinalResendMail])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictFinalResendMail options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Final Service Automation JSON Resend Mail: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlResendMailService];
        
        
        //============================================================================
        //============================================================================
        NSDictionary *dictTemp;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"resendMailService" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     //Sending Dynamic Json
                     
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         if ([[response valueForKey:@"ReturnMsg"] isEqualToString:@"true"]) {
                             
                             [global AlertMethod:Info :@"Mail Sent Successfully"];
                             
                             [self goToAppointmentViewNew];
                             
                         } else {
                             
                             [global AlertMethod:Info :SorryEmail];
                             
                         }
                         
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
    }
}


-(void)fetchWorkOrderDocumentsResendMail{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOrderDocuments= [NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context];
    [request setEntity:entityWorkOrderDocuments];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptorWorkOrderDocuments = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptorsWorkOrderDocuments = [NSArray arrayWithObject:sortDescriptorWorkOrderDocuments];
    
    [request setSortDescriptors:sortDescriptorsWorkOrderDocuments];
    
    self.fetchedResultsWorkOrderDocuments = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsWorkOrderDocuments setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsWorkOrderDocuments performFetch:&error1];
    arrAllObjWorkOrderDocuments = [self.fetchedResultsWorkOrderDocuments fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObjWorkOrderDocuments.count==0)
    {
        [dictFinalResendMail setObject:arrEmailDetail forKey:@"WoOtherDocuments"];
        
    }else
    {
        
        for (int k=0; k<arrAllObjWorkOrderDocuments.count; k++)
        {
            matchesWorkOrderDocuments=arrAllObjWorkOrderDocuments[k];
            NSLog(@"Lead IDDDD====%@",[matchesWorkOrderDocuments valueForKey:@"workorderId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesWorkOrderDocuments entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesWorkOrderDocuments valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesWorkOrderDocuments valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"WoOtherDocuments%@",arrEmailDetail);
        }
        [dictFinalResendMail setObject:arrEmailDetail forKey:@"WoOtherDocuments"];
        
    }
}

-(void)fetchEmailDetailServiceAutoResendMail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinalResendMail setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
                
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinalResendMail setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}
-(void)uploadImageServiceAddImage :(NSString*)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
        
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
        }
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        //http://tcrms.stagingsoftware.com/api/File/UploadCustomerImagesAsync
        //NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        
        NSUserDefaults *defsAddImage=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginDataAddImage=[defsAddImage valueForKey:@"LoginDetails"];
        NSString *strServiceAddImageUrl=[dictLoginDataAddImage valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
        
        
        NSString *urlString;// =@"http://tcrms.stagingsoftware.com/api/File/UploadCustomerAddressImageAsync";// @"http://tcrms.stagingsoftware.com/api/File/UploadCustomerImagesAsync";
        urlString=[NSString stringWithFormat:@"%@api/File/UploadCustomerAddressImageAsync",strServiceAddImageUrl];
        
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        
    }
}
//Nilind 11 Dec
//============================================================================
#pragma mark- Upload Termite Image METHOD
//============================================================================

-(void)uploadImageTermite :(NSString*)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
        }
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        
    }
    
    // indexToSendImage++;
    
    // [self uploadingAllImages:indexToSendImage];
    
}

-(void)serviceEmailFetchToUpdateIsMailSentToFalse
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    arrEmail=[[NSMutableArray alloc]init];
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjEmail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjEmail.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjEmail.count; i++)
        {
            
            NSManagedObject *matchesEmail=arrAllObjEmail[i];
            [matchesEmail setValue:@"false" forKey:@"isMailSent"];
            NSError *error1;
            [context save:&error1];
            
        }
    }
}


#pragma mark- Service Pest New Flow Fetch Methods

-(void)fetchServicePestDataAllFromDBObjC{
    
    // Areas
    
    NSString *strServiceAddressIdTemp;
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strLeadId];
    
    WebService *objWebService = [[WebService alloc] init];
    
    NSArray *arrTempData = [objWebService getDataFromCoreDataBaseArrayObjectiveCWithStrEntity:@"WorkOrderDetailsService" predicate:predicate];
    
    if (arrTempData.count>0) {
        
        NSManagedObject *objTemp = arrTempData[0];
        
        strServiceAddressIdTemp = [NSString stringWithFormat:@"%@",[objTemp valueForKey:@"serviceAddressId"]];
        
    }
    
    
    NSMutableArray *arrTemp = [self fetchServicePestNewFlowData:@"ServiceAreas" :@"No" : strServiceAddressIdTemp];
    [dictFinal setObject:arrTemp forKey:@"ServiceAreas"];
    
    // ServiceDevices
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceDevices" :@"No" : strServiceAddressIdTemp];
    [dictFinal setObject:arrTemp forKey:@"ServiceDevices"];
    
    // ServiceProducts
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceProducts" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceProducts"];
    
    // ServicePests
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServicePests" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServicePests"];
    
    // ServiceConditions
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditions" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditions"];
    
    // ServiceComments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceComments" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceComments"];
    
    
    // ServiceConditionComments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditionComments" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditionComments"];
    
    
    // ServicePestDocuments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"SAPestDocuments" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"SAPestDocuments"];
    
    
    // ServiceConditionDocuments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditionDocuments" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditionDocuments"];
    
    // ServiceDeviceInspection
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceDeviceInspections" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceDeviceInspections"];
    
    // WOServiceDevices
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"WOServiceDevices" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"WOServiceDevices"];
    
    // WOServiceAreas
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"WOServiceAreas" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"WOServiceAreas"];
}


-(NSMutableArray*)fetchServicePestNewFlowData :(NSString*)strEntityName :(NSString*)strIsDocument :(NSString*)strServiceAddressIdForAreaDevice
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription * entityLocal= [NSEntityDescription entityForName:strEntityName inManagedObjectContext:context];
    [request setEntity:entityLocal];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId=%@",strLeadId];
    
    if (strServiceAddressIdForAreaDevice.length>0){
        
        predicate =[NSPredicate predicateWithFormat:@"serviceAddressId=%@",strServiceAddressIdForAreaDevice];
        
    }
    
    [request setPredicate:predicate];
    
    NSSortDescriptor * sortDescriptorLocal = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:YES];
    NSArray * sortDescriptorsLocal = [NSArray arrayWithObject:sortDescriptorLocal];
    
    [request setSortDescriptors:sortDescriptorsLocal];
    
    self.fetchedResultsControllerServicePestNewFlowArea = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServicePestNewFlowArea setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServicePestNewFlowArea performFetch:&error1];
    NSArray *ObjAllData = [self.fetchedResultsControllerServicePestNewFlowArea fetchedObjects];
    
    NSMutableArray *arrData;
    arrData=[[NSMutableArray alloc]init];
    
    if (ObjAllData.count==0)
    {
        
        //[dictFinal setObject:arrData forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<ObjAllData.count; k++)
        {
            
            NSManagedObject *matchesLocal=ObjAllData[k];
            
            NSArray *keys = [[[matchesLocal entity] attributesByName] allKeys];
            NSDictionary *dictOfData = [matchesLocal dictionaryWithValuesForKeys:keys];
            
            if ([strIsDocument isEqualToString:@"Yes"]) {
                
                NSString *isToUpload = [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"isToUpload"]];
                
                if ([isToUpload isEqualToString:@"Yes"]) {
                    
                    [arrOfAllDocumentsToSend addObject:dictOfData];
                    
                }
                
            }
            
            NSArray *arrOfKey = [dictOfData allKeys];
            
            NSMutableArray *arrTempKey = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfKey.count; k++) {
                
                NSString *strCaps = [NSString stringWithFormat:@"%@",arrOfKey[k]];
                
                NSString *strSTR =  [NSString stringWithFormat:@"%@%@",[[strCaps substringToIndex:1] capitalizedString],[strCaps substringFromIndex:1]];
                
                if ([strCaps isEqualToString:@"isInspectingFirstTime"]) {
                    
                    [arrTempKey addObject:strCaps];
                    
                } else {
                    
                    [arrTempKey addObject:strSTR];
                    
                }
                
                
            }
            
            /// Array Of Values
            
            NSArray *arrOfValue = [dictOfData allValues];
            
            NSMutableArray *arrTempValue = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfValue.count; k++) {
                
                NSString *strBoolKey = [NSString stringWithFormat:@"%@",arrTempKey[k]];
                
                if ([strBoolKey isEqualToString:@"isInspectingFirstTime"] || [strBoolKey isEqualToString:@"IsActive"] || [strBoolKey isEqualToString:@"IsDeletedArea"] || [strBoolKey isEqualToString:@"IsInspected"] || [strBoolKey isEqualToString:@"IsDeletedDevice"] || [strBoolKey isEqualToString:@"Activity"] || [strBoolKey isEqualToString:@"isDeletedDevice"]) {
                    
                    if ([strBoolKey isEqualToString:@"IsDeletedDevice"]) {
                        
                        [arrTempValue addObject:@"false"];
                        
                    }else{
                        
                        if ([arrOfValue[k] isKindOfClass:[NSNull class]]) {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }else{
                            
                        BOOL isTrue = [arrOfValue[k] boolValue];
                        
                        if (isTrue) {
                            
                            [arrTempValue addObject:@"true"];
                            
                        } else {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }
                            
                        }
                        
                    }
                    
                    
                } else {
                    
                    [arrTempValue addObject:[NSString stringWithFormat:@"%@",arrOfValue[k]] ];
                    
                }
                
                
            }
            
            
            //NSArray *arrOfValue = [dictOfData allValues];
            
            NSDictionary *dictData;
            dictData = [NSDictionary dictionaryWithObjects:arrTempValue forKeys:arrTempKey];
            
            [arrData addObject:dictData];
            
        }
        
    }
    
    return arrData;
    
}

//============================================================================
//============================================================================
#pragma mark- Upload  METHOD Service Pest New Flow
//============================================================================
//============================================================================


-(void)uploadDocuments :(NSString*)strDocName :(NSString*)ServerUrl
{
    
    if ((strDocName.length==0) && [strDocName isEqualToString:@"(null)"]) {
        
    } else {
        
        strDocName = [global strDocNameFromPath:strDocName];
        
        NSRange equalRange = [strDocName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strDocName = [strDocName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strDocName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@",ServerUrl];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strDocName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response On Document Upload = = = =  = = %@",returnString);
        
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Document Sent");
        }
        
    }
    
    // Updating Status yes to no for isToUpload
    
    NSDictionary *dictDataDocument = arrOfAllDocumentsToSend[indexToSendDocuments];
    
    NSArray *allKeys = [dictDataDocument allKeys];
    
    if ([allKeys containsObject:@"serviceAddressDocName"]) {
        
        WebService *objWebService = [[WebService alloc] init];
        
        NSMutableArray *arrTKey = [[NSMutableArray alloc] initWithObjects:@"isToUpload", nil];
        NSMutableArray *arrTValue = [[NSMutableArray alloc] initWithObjects:@"No", nil];
        
        NSString *strDocId = [NSString stringWithFormat:@"%@",[dictDataDocument valueForKey:@"mobileServiceAddressDocId"]];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && mobileServiceAddressDocId = %@ && companyKey = %@",[dictDataDocument valueForKey:@"workOrderId"],[dictDataDocument valueForKey:@"mobileServiceAddressDocId"],[dictDataDocument valueForKey:@"companyKey"]];
        
        if (strDocId.length<1) {
            
            strDocId = [NSString stringWithFormat:@"%@",[dictDataDocument valueForKey:@"mobileServiceAddressDocId"]];
            predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && serviceAddressDocId = %@ && companyKey = %@",[dictDataDocument valueForKey:@"workOrderId"],[dictDataDocument valueForKey:@"serviceAddressDocId"],[dictDataDocument valueForKey:@"companyKey"]];
        }
        
        BOOL isSucess =  [objWebService getDataFromDbToUpdateInObjectiveCWithStrEntity:@"SAPestDocuments" predicate:predicate arrayOfKey:arrTKey arrayOfValue:arrTValue];
        
        if (isSucess) {
            
            
        }
        
        //let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@", strWoId, strAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        
    } else {
        
        WebService *objWebService = [[WebService alloc] init];
        
        NSMutableArray *arrTKey = [[NSMutableArray alloc] initWithObjects:@"isToUpload", nil];
        NSMutableArray *arrTValue = [[NSMutableArray alloc] initWithObjects:@"No", nil];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && sAConditionDocumentId = %@ && companyKey = %@",[dictDataDocument valueForKey:@"workOrderId"],[dictDataDocument valueForKey:@"sAConditionDocumentId"],[dictDataDocument valueForKey:@"companyKey"]];
        
        BOOL isSucess =  [objWebService getDataFromDbToUpdateInObjectiveCWithStrEntity:@"ServiceConditionDocuments" predicate:predicate arrayOfKey:arrTKey arrayOfValue:arrTValue];
        
        if (isSucess) {
            
            
        }
        
    }
    
    indexToSendDocuments++;
    
    [self uploadingAllDocumentsPest:indexToSendDocuments];
    
}


-(void)uploadingAllDocumentsPest :(int)indexToSendimages{
    
    if (arrOfAllDocumentsToSend.count==indexToSendimages) {
        
        if (arrOfAllImagesToSendToServer.count==0) {
            
            [self uploadingAllImages:0];
            
        } else {
            
            [self uploadingAllImages:0];
            
        }
        
    } else {
        
        // Yaha Par Documents Sync Krna hai web pr
        
        // serviceAddressDocName  WO pr jo docs hai   serviceAddressDocPath
        
        // documnetTitle   Jo Condiotion pr hai documentPath
        
        NSDictionary *dictDataDocument = arrOfAllDocumentsToSend[indexToSendimages];
        
        NSArray *allKeys = [dictDataDocument allKeys];
        
        if ([allKeys containsObject:@"serviceAddressDocName"]) {
            
            [self uploadDocuments:[NSString stringWithFormat:@"%@",[dictDataDocument valueForKey:@"serviceAddressDocPath"]] :[NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlServicePestDocUploadAsync]];
            
        } else {
            
            [self uploadDocuments:[NSString stringWithFormat:@"%@",[dictDataDocument valueForKey:@"documentPath"]] :[NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlServicePestConditionDocUploadAsync]];
            
        }
        
        
    }
    
}

-(void)sendingDeviceInspectionToServer :(NSArray*)arrFinalDynamicJson{
    
    if (arrFinalDynamicJson.count==0) {
        
        [self methodOnResponse];
        
    } else {
        
        NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                           arrFinalDynamicJson,nil];
        
        NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                         @"DeviceTypeFormInformationJsonExtSerDc",nil];
        
        //NSDictionary *dictFinalDynamicJson=arrFinalDynamicJson[0];
        
        NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];

        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrFinalDynamicJson])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Device JSon: %@", jsonString);
            }
        }
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlDeviceDynamicFormSubmission];
        
        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     // [DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         [self methodOnResponse];
                         
                     }
                     else
                     {
                         
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                         
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
        
    }

}


-(void)FetchFromCoreDataToSendDeviceDynamicForms :(BOOL)isUpdated{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityServiceDynamic=[NSEntityDescription entityForName:@"DeviceInspectionDynamicForm" inManagedObjectContext:context];
    requestNewServiceDynamic = [[NSFetchRequest alloc] init];
    [requestNewServiceDynamic setEntity:entityServiceDynamic];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strLeadId];
    
    [requestNewServiceDynamic setPredicate:predicate];
    
    sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
    
    [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
    
    self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDynamic performFetch:&error];
    arrAllObjDeviceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
    if ([arrAllObjDeviceDynamic count] == 0)
    {
        
        [self methodOnResponse];
        
    }
    else
    {
        
        NSMutableArray *arrOfEquipToSend=[[NSMutableArray alloc] init];
        
        for (int k=0; k<arrAllObjDeviceDynamic.count; k++) {
            
            matchesServiceDynamic=arrAllObjDeviceDynamic[k];
            NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
            
            NSDictionary *dictDataService;
            
            if ([arrTemp isKindOfClass:[NSDictionary class]]) {
                
                dictDataService=(NSDictionary*)arrTemp;
                
            } else if ([arrTemp isKindOfClass:[NSArray class]]){
                
                if (arrTemp.count==0) {
                    
                    //dictDataService=arrTemp[0];
                    
                } else {
                    
                    dictDataService=arrTemp[0];
                    
                }
                
            }
            
            [arrOfEquipToSend addObject:dictDataService];
            
            
        }

        if (arrOfEquipToSend.count==0) {
            
            [self methodOnResponse];
            
        } else {
            
            if (isUpdated) {
                
                [self sendingDeviceInspectionToServer:arrOfEquipToSend];
                
            } else {
                
                [self methodOnResponse];
                
            }
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}


@end

