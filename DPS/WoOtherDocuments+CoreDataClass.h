//
//  WoOtherDocuments+CoreDataClass.h
//  
//
//  Created by Rakesh Jain on 18/09/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WoOtherDocuments : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WoOtherDocuments+CoreDataProperties.h"
