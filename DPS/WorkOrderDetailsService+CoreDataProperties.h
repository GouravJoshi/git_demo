//
//  WorkOrderDetailsService+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WorkOrderDetailsService.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorkOrderDetailsService (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *accountNo;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *audioFilePath;
@property (nullable, nonatomic, retain) NSString *billingAddress1;
@property (nullable, nonatomic, retain) NSString *billingAddress2;
@property (nullable, nonatomic, retain) NSString *billingCity;
@property (nullable, nonatomic, retain) NSString *billingCountry;
@property (nullable, nonatomic, retain) NSString *billingState;
@property (nullable, nonatomic, retain) NSString *billingZipcode;
@property (nullable, nonatomic, retain) NSString *branchId;
@property (nullable, nonatomic, retain) NSString *categoryName;
@property (nullable, nonatomic, retain) NSString *companyId;
@property (nullable, nonatomic, retain) NSString *companyName;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *currentServices;
@property (nullable, nonatomic, retain) NSString *customerSignaturePath;
@property (nullable, nonatomic, retain) NSString *departmentId;
@property (nullable, nonatomic, retain) NSString *direction;
@property (nullable, nonatomic, retain) NSString *electronicSignaturePath;
@property (nullable, nonatomic, retain) NSString *employeeNo;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *invoiceAmount;
@property (nullable, nonatomic, retain) NSString *invoicePath;
@property (nullable, nonatomic, retain) NSString *isActive;
@property (nullable, nonatomic, retain) NSString *isElectronicSignatureAvailable;
@property (nullable, nonatomic, retain) NSString *isFail;
@property (nullable, nonatomic, retain) NSString *isMailSent;
@property (nullable, nonatomic, retain) NSString *isPDFGenerated;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *lastServices;
@property (nullable, nonatomic, retain) NSString *middleName;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *modifiedFormatedDate;
@property (nullable, nonatomic, retain) NSString *officeNotes;
@property (nullable, nonatomic, retain) NSString *operateMedium;
@property (nullable, nonatomic, retain) NSString *otherInstruction;
@property (nullable, nonatomic, retain) NSString *previousBalance;
@property (nullable, nonatomic, retain) NSString *primaryEmail;
@property (nullable, nonatomic, retain) NSString *primaryPhone;
@property (nullable, nonatomic, retain) NSString *productionAmount;
@property (nullable, nonatomic, retain) NSString *resetDescription;
@property (nullable, nonatomic, retain) NSString *resetId;
@property (nullable, nonatomic, retain) NSString *scheduleEndDateTime;
@property (nullable, nonatomic, retain) NSString *scheduleOnEndDateTime;
@property (nullable, nonatomic, retain) NSString *scheduleOnStartDateTime;
@property (nullable, nonatomic, retain) NSString *scheduleStartDateTime;
@property (nullable, nonatomic, retain) NSString *secondaryEmail;
@property (nullable, nonatomic, retain) NSString *secondaryPhone;
@property (nullable, nonatomic, retain) NSString *serviceAddress2;
@property (nullable, nonatomic, retain) NSString *serviceAddressLatitude;
@property (nullable, nonatomic, retain) NSString *serviceAddressLongitude;
@property (nullable, nonatomic, retain) NSString *serviceCity;
@property (nullable, nonatomic, retain) NSString *serviceCountry;
@property (nullable, nonatomic, retain) NSString *serviceDateTime;
@property (nullable, nonatomic, retain) NSString *serviceInstruction;
@property (nullable, nonatomic, retain) NSString *services;
@property (nullable, nonatomic, retain) NSString *servicesAddress1;
@property (nullable, nonatomic, retain) NSString *serviceState;
@property (nullable, nonatomic, retain) NSString *serviceZipcode;
@property (nullable, nonatomic, retain) NSString *specialInstruction;
@property (nullable, nonatomic, retain) NSString *subCategory;
@property (nullable, nonatomic, retain) NSString *surveyID;
@property (nullable, nonatomic, retain) NSString *surveyStatus;
@property (nullable, nonatomic, retain) NSString *tax;
@property (nullable, nonatomic, retain) NSString *technicianComment;
@property (nullable, nonatomic, retain) NSString *technicianSignaturePath;
@property (nullable, nonatomic, retain) NSString *timeIn;
@property (nullable, nonatomic, retain) NSString *timeOut;
@property (nullable, nonatomic, retain) NSString *totalEstimationTime;
@property (nullable, nonatomic, retain) NSString *workorderId;
@property (nullable, nonatomic, retain) NSString *workOrderNo;
@property (nullable, nonatomic, retain) NSString *workorderStatus;
@property (nullable, nonatomic, retain) NSString *atributes;
@property (nullable, nonatomic, retain) NSDate *dateModified;
@property (nullable, nonatomic, retain) NSDate *zdateScheduledStart;
@property (nullable, nonatomic, retain) NSString *onMyWaySentSMSTime;
@property (nullable, nonatomic, retain) NSString *targets;
@property (nullable, nonatomic, retain) NSString *routeNo;
@property (nullable, nonatomic, retain) NSString *routeName;
@property (nullable, nonatomic, retain) NSString *deviceName;
@property (nullable, nonatomic, retain) NSString *deviceVersion;
@property (nullable, nonatomic, retain) NSString *versionDate;
@property (nullable, nonatomic, retain) NSString *versionNumber;
@property (nullable, nonatomic, retain) NSString *isCustomerNotPresent;
@property (nullable, nonatomic, retain) NSString *zSync;
@property (nullable, nonatomic, retain) id routeCrewList;
@property (nullable, nonatomic, retain) NSString *assignCrewIds;
@property (nullable, nonatomic, retain) NSString *totalallowCrew;
@property (nullable, nonatomic, retain) NSString *dbUserName;
@property (nullable, nonatomic, retain) NSString *dbCompanyKey;//CategorySysName
@property (nullable, nonatomic, retain) NSString *categorySysName;
@property (nullable, nonatomic, retain) NSString *primaryServiceSysName;
@property (nullable, nonatomic, retain) NSString *noChemical;
@property (nullable, nonatomic, retain) NSString *serviceJobDescription;
@property (nullable, nonatomic, retain) NSString *serviceJobDescriptionId;//isEmployeePresetSignature
@property (nullable, nonatomic, retain) NSString *isEmployeePresetSignature;
@property(nullable,nonatomic,retain)NSString *serviceNotes;
@property(nullable,nonatomic,retain)NSString *arrivalDuration;
@property(nullable,nonatomic,retain)NSString *keyMap;
@property(nullable,nonatomic,retain)NSString *serviceDescription;
@property(nullable,nonatomic,retain)NSString *problemDescription;
@property(nullable,nonatomic,retain)NSString *departmentType;
@property(nullable,nonatomic,retain)NSString *tags;//accountDescription   isClientApprovalReq  ServiceGateCode ServiceMapCode
@property(nullable,nonatomic,retain)NSString *accountDescription;
@property(nullable,nonatomic,retain)NSString *serviceGateCode;
@property(nullable,nonatomic,retain)NSString *serviceMapCode;
@property(nullable,nonatomic,retain)NSString *isClientApprovalReq;
@property(nullable,nonatomic,retain)NSString *departmentSysName;
@property (nullable, nonatomic, copy) NSString *companyKey;
//dateModified onMyWaySentSMSTime assignCrewIds totalallowCrew dbUserName dbCompanyKey  departmentType tags  DepartmentSysName  isResendInvoiceMail
@property(nullable,nonatomic,retain)NSString *isResendInvoiceMail;

@property(nullable,nonatomic,retain)NSString *cellNo;
@property(nullable,nonatomic,retain)NSString *billingFirstName;
@property(nullable,nonatomic,retain)NSString *billingMiddleName;
@property(nullable,nonatomic,retain)NSString *billingLastName;
@property(nullable,nonatomic,retain)NSString *billingCellNo;
@property(nullable,nonatomic,retain)NSString *billingPrimaryEmail;
@property(nullable,nonatomic,retain)NSString *billingSecondaryEmail;
@property(nullable,nonatomic,retain)NSString *billingPrimaryPhone;
@property(nullable,nonatomic,retain)NSString *billingSecondaryPhone;
@property(nullable,nonatomic,retain)NSString *billingMapCode;


@property(nullable,nonatomic,retain)NSString *serviceFirstName;
@property(nullable,nonatomic,retain)NSString *serviceMiddleName;
@property(nullable,nonatomic,retain)NSString *serviceLastName;
@property(nullable,nonatomic,retain)NSString *servicePrimaryEmail;
@property(nullable,nonatomic,retain)NSString *serviceSecondaryEmail;
@property(nullable,nonatomic,retain)NSString *serviceCellNo;
@property(nullable,nonatomic,retain)NSString *servicePrimaryPhone;
@property(nullable,nonatomic,retain)NSString *serviceSecondaryPhone;
@property(nullable,nonatomic,retain)NSString *serviceAddressImagePath;

//Nilind 07 Dec
@property(nullable,nonatomic,retain)NSString *timeInLatitude;
@property(nullable,nonatomic,retain)NSString *timeInLongitude;
@property(nullable,nonatomic,retain)NSString *timeOutLatitude;
@property(nullable,nonatomic,retain)NSString *timeOutLongitude;
@property(nullable,nonatomic,retain)NSString *onMyWaySentSMSTimeLatitude;
@property(nullable,nonatomic,retain)NSString *onMyWaySentSMSTimeLongitude;

@property(nullable,nonatomic,retain)NSString *serviceAddressId;
@property(nullable,nonatomic,retain)NSString *addressSubType;
@property(nullable,nonatomic,retain)NSString *termiteStateType;
//customerPONumber
@property(nullable,nonatomic,retain)NSString *customerPONumber;
@property(nullable,nonatomic,retain)NSString *servicePOCId;
@property(nullable,nonatomic,retain)NSString *billingPOCId;


@property(nullable,nonatomic,retain)NSString *billingCounty;
@property(nullable,nonatomic,retain)NSString *serviceCounty;
@property(nullable,nonatomic,retain)NSString *billingSchoolDistrict;
@property(nullable,nonatomic,retain)NSString *serviceSchoolDistrict;//thirdPartyAccountNo
@property(nullable,nonatomic,retain)NSString *thirdPartyAccountNo;
@property(nullable,nonatomic,retain)NSString *latestStartTimeStr;
@property(nullable,nonatomic,retain)NSString *earliestStartTimeStr;
@property(nullable,nonatomic,retain)NSString *driveTimeStr;

@property(nullable,nonatomic,retain)NSString *fromDate;
@property(nullable,nonatomic,retain)NSString *toDate;
@property(nullable,nonatomic,retain)NSString *modifyDate;

@property (nullable, nonatomic, retain) NSString *isRegularPestFlow;
@property (nullable, nonatomic, retain) NSString *isRegularPestFlowOnBranch;
//isCollectPayment
@property (nullable, nonatomic, retain) NSString *isCollectPayment;

// isWhetherShow
@property (nullable, nonatomic, retain) NSString *isWhetherShow; //relatedOpportunityNo
@property (nullable, nonatomic, retain) NSString *relatedOpportunityNo; //taxPercent
@property (nullable, nonatomic, retain) NSString *taxPercent; //serviceReportFormat
@property (nullable, nonatomic, retain) NSString *serviceReportFormat; //isBatchReleased
@property (nullable, nonatomic, retain) NSString *isBatchReleased; //accountName
@property (nullable, nonatomic, retain) NSString *accountName; //driveTime
@property (nullable, nonatomic, retain) NSString *driveTime;//isNPMATermite
@property (nullable, nonatomic, retain) NSString *isNPMATermite;//isNPMATermite
@property (nullable, nonatomic, retain) NSString *branchSysName;//branchSysName
@property (nullable, nonatomic, retain) NSString *isLocked;//isLocked
@property (nullable, nonatomic, retain) NSString *isTaxExampt;//isTaxExampt
@property (nullable, nonatomic, retain) NSString *earliestStartTime;//earliestStartTime
@property (nullable, nonatomic, retain) NSString *latestStartTime;//latestStartTime
// WDO Prcing Approval New Changes

@property (nullable, nonatomic, retain) NSString *wdoWorkflowStageId;
@property (nullable, nonatomic, retain) NSString *wdoWorkflowStatus;
@property (nullable, nonatomic, retain) NSString *wdoWorkflowStage;
@property (nullable, nonatomic, retain) NSString *isInspectionSignature;
@property (nullable, nonatomic, retain) NSString *inspectionReportSignaturePath;
@property (nullable, nonatomic, retain) NSString *wdoWorkOrderApprovalLatestDeclinednote;
@property (nullable, nonatomic, retain) NSString *wdoPricingApprovalLatestDeclinednote;
@property (nullable, nonatomic, retain) NSString *verbalApprovalBy;
@property (nullable, nonatomic, retain) NSString *isVerbalApproval;
@property (nullable, nonatomic, retain) NSString *priceChangeNote;
@property (nullable, nonatomic, retain) NSString *isPricingApprovalPending;
@property (nullable, nonatomic, retain) NSString *isPricingApprovalMailSend;
@property (nullable, nonatomic, retain) NSString *priceChangeReasonId;
@property (nullable, nonatomic, retain) NSString *isInspectorEditInfo;
@property (nullable, nonatomic, retain) NSString *wdoWoStageDateTime;//wdoWoStageDateTime //priceChangeReasonOtherDescription
@property (nullable, nonatomic, retain) NSString *priceChangeReasonOtherDescription;
@property (nullable, nonatomic, retain) NSString *isBillingAddressAsSameServiceAddress;


@property (nullable, nonatomic, retain) NSString *isServiceManagerDeclined;
@property (nullable, nonatomic, retain) NSString *rateMasterId;

@end

NS_ASSUME_NONNULL_END
