//
//  ChemicalListPreview.h
//  DPS
//
//  Created by Rakesh Jain on 21/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
@interface ChemicalListPreview : UIViewController<NSFetchedResultsControllerDelegate,UITextViewDelegate,UITextFieldDelegate>
{
   // AVAudioPlayer *LandingSong;
    NSManagedObject *matchesImageDetail;
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSEntityDescription *entityWorkOderDetailServiceAuto,*entityModifyDateServiceAuto;
    NSFetchRequest *requestNewService;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
    NSEntityDescription *entityImageDetail;

}
@property (strong, nonatomic) NSArray *arrChemicalList;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVieww;

@property (strong, nonatomic) IBOutlet UIView *view_CustomerInfo;
@property (strong, nonatomic) IBOutlet UITextField *txt_AccountNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_WorkorderNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_Customer;
@property (strong, nonatomic) IBOutlet UITextField *txt_PrimaryPhone;
@property (strong, nonatomic) IBOutlet UITextField *txt_SecondaryPhone;
@property (strong, nonatomic) IBOutlet UITextField *txt_PrimaryEmail;
@property (strong, nonatomic) IBOutlet UITextField *txt_SecondaryEmail;
@property (strong, nonatomic) IBOutlet UITextField *txt_Technician;
@property (strong, nonatomic) IBOutlet UITextView *txtView_BillingAddress;
@property (strong, nonatomic) IBOutlet UITextView *txt_ServiceAddress;

@property (strong, nonatomic) IBOutlet UIImageView *imgView_CustomerSign;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_TechSign;
@property (strong, nonatomic) IBOutlet UIButton *btnCustNotPrsent;

@property (strong, nonatomic) IBOutlet UITextView *txtView_OfficeNotes;
@property (strong, nonatomic) IBOutlet UITextView *txtView_TechComment;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeOut;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeinn;
@property (strong, nonatomic) IBOutlet UILabel *lblChargesCurrentService;
@property (strong, nonatomic) IBOutlet UILabel *lblTax;
@property (strong, nonatomic) IBOutlet UILabel *lblTotaldue;
@property (strong, nonatomic) IBOutlet UILabel *lblPaymentType;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPaid;
@property (strong, nonatomic) IBOutlet UIView *viewPaymentInfo;
@property (strong, nonatomic) NSDictionary *dictJsonDynamicForm;
@property (strong, nonatomic) NSDictionary *dictOfChemicalList;
@property (strong, nonatomic) NSArray *arrOfChemicalList;
@property (strong, nonatomic) NSArray *arrOfChemicalListOther;

@property (strong, nonatomic) IBOutlet UIButton *btnPlayAudio;

@property (strong, nonatomic) IBOutlet UILabel *lblAudioStatus;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ViewCustSign_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ViewCust_Top;

@property (strong, nonatomic) NSManagedObject *workOrderDetail;
@property (strong, nonatomic) NSManagedObject *paymentInfoDetail;
@property (strong, nonatomic) NSArray *arrAllObjImageDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceAutomation;

@property (strong, nonatomic) NSString *strPaymentModes;
@property (strong, nonatomic) NSString *strDepartmentIdd;
@property (strong, nonatomic) NSString *strPaidAmount;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AccountNos;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) IBOutlet UIButton *btn_RecordAudio;
@property (strong, nonatomic) IBOutlet UIButton *btn_AfterImage;
@property (strong, nonatomic) IBOutlet UIButton *btn_TechSignn;
@property (strong, nonatomic) IBOutlet UIButton *btn_CustSignn;
@property (strong, nonatomic) IBOutlet UIButton *btn_SavenContinue;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_SavenContinue_B;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CustomerSign;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg1;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg2;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg3;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_AfterImage_B;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_AfterImage_BtoImgView;
- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Scroll_H;

@end
