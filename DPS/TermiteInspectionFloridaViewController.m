//
//  TermiteInspectionFloridaViewController.m
//  DPS
//
//  Created by Rakesh Jain on 06/01/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "TermiteInspectionFloridaViewController.h"
#import "UITextView+Placeholder.h"

@interface TermiteInspectionFloridaViewController ()
{
    Global *global;
    BOOL yesEditedSomething;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    UITableView *tblData;
    
    NSString *strGlobalBuyersSign,*strGlobalInspectorSign,*strGlobalApplicatorSign,*strGlobalBuyersSignPurchaser,*strGlobalGraphImage,*strGlobalBuyersSign7B,*strEmpID,*strCompanyKey,*strCompanyId,*strUserName,*strCreatedBy,*strDateInspectiondate,*strDate,*strDateTreatmentByInspectionCompany,*strDatePosted,*strDateOfPurchaserOfProperty,*strGraphImageCaption,*strGraphImageDescription,*strGlobalWorkOrderStatus;
    
    NSString *strGlobalSignOfAgent;
    NSMutableDictionary *dictGlobalTermiteFlorida;
    
    NSString * strDateOfInspectiondate,*strDateOfLicenceeAgent,*strInspectionDate;
    
}
@end

@implementation TermiteInspectionFloridaViewController

- (void)viewDidLoad
{
    
    [_txtFieldForInspectionDateForView5 setEnabled:NO];
    [_txtDateOfInspection setEnabled:NO];
    [_txtDateOfInspection setEnabled:NO];
    [_txtFieldDateForView5 setEnabled:NO];
    
    yesEditedSomething=NO;
    
    strGlobalWorkOrderStatus=_strWorkOrderStatus;
    global=[[Global alloc]init];
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    [self getClockStatus];

    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    [super viewDidLoad];
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    _lblAccountNo.text=[defsLead valueForKey:@"lblName"];
    
    [self methodSetAllViews];
    
    // Do any additional setup after loading the view.
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        
    }
    
    [self.navigationController.navigationBar setHidden:YES];
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strCreatedBy          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"CreatedBy"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strCompanyId      =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
    
    [self getDataFrmDB];
    
    
    [self fetchWorkOrderFromDataBase];
    
    //Placeholder Textview
    
    _txtViewEvidenceOfWDO.placeholderColor = [UIColor lightGrayColor];
    _txtViewEvidenceOfWDO.placeholder = @"(Common Name, Description and Location –Describe evidence -- use additional page, if needed)";
    _txtViewDAMAGEcausedbyWDO.placeholderColor = [UIColor lightGrayColor];
    _txtViewDAMAGEcausedbyWDO.placeholder = @"(Common Name, Description and Location of all visible damage –Describe damage -- use additional page, if needed)";

    
}
-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    NSString *strSignOfAgent;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strSignOfAgent=[defs valueForKey:@"strBuyersInitials7B"];
    
    
    if ([strSignOfAgent isEqualToString:@"strBuyersInitials7B"])
    {
        NSString *strImageName;
        strImageName=[defs valueForKey:@"imagePathTermite"];
        UIImage *imageSign;
        imageSign=[self loadImage:strImageName];
        _imgViewSignatureofLicenseAgent.image=imageSign;
        strGlobalSignOfAgent=strImageName;
        [defs setValue:@"" forKey:@"strBuyersInitials7B"];
        [defs setValue:@"" forKey:@"imagePathTermite"];
        [defs synchronize];
        [dictGlobalTermiteFlorida setValue:strGlobalSignOfAgent forKey:@"SignatureOfLicenseOrAgent"];
    }
    [self setEditableOrNot];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionOnBtnDateOfInspection:(id)sender
{
    yesEditedSomething=YES;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=100;
    [self addPickerViewDateTo :strDateOfInspectiondate];
    
}
#pragma mark- SECTION 3 Methods

- (IBAction)actionOnBtnCheckBoxA:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxA];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxA setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"NoVisibleSignOfWDOObserved"];
        
    }
    else
    {
        
        [_btnCheckBoxA setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"NoVisibleSignOfWDOObserved"];
        
    }
    
    
}
- (IBAction)actionOnBtnCheckBoxB:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxB];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxB setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"VisibleEvidenceOfWDOObserved"];
        
    }
    else
    {
        
        [_btnCheckBoxB setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"VisibleEvidenceOfWDOObserved"];
        
    }
    
}

- (IBAction)actionOnBtnChkBoxLiveWDO:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxLiveWDO];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxLiveWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"LiveWDO"];
        
    }
    else
    {
        
        [_btnCheckBoxLiveWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"LiveWDO"];
        
    }
}
- (IBAction)actionOnbtnCheckBoxEvidenceOfWDO:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxEvidenceOfWDO];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxEvidenceOfWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"EvidenceOfWDO"];
        
    }
    else
    {
        
        [_btnCheckBoxEvidenceOfWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"EvidenceOfWDO"];
        
    }
    
}
- (IBAction)actionOnbtnChkBoxDAMAGEcausedbyWDO:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnChkBoxDAMAGEcausedbyWDO];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxDAMAGEcausedbyWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"DamageCausedbyWDO"];
        
    }
    else
    {
        
        [_btnChkBoxDAMAGEcausedbyWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"DamageCausedbyWDO"];
        
    }
    
}


- (IBAction)actionOnBtnCheckBoxAttic:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxAttic];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxAttic setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"Attic"];
        
    }
    else
    {
        
        [_btnCheckBoxAttic setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"Attic"];
        
    }
}
- (IBAction)actionOnBtnChkBoxInterior:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxInterior];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxInterior setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"Interior"];
        
    }
    else
    {
        
        [_btnCheckBoxInterior setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"Interior"];
        
    }
}
- (IBAction)actionOnCheckBoxExterior:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxExterior];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxExterior setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"Exterior"];
        
    }
    else
    {
        
        [_btnCheckBoxExterior setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"Exterior"];
        
    }
}
- (IBAction)actionOnBtnChkBoxCrawlPlace:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxCrawlPlace];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxCrawlPlace setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"CrawlSpace"];
        
    }
    else
    {
        
        [_btnCheckBoxCrawlPlace setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"CrawlSpace"];
        
    }
}
- (IBAction)actionOnBtnCheckBoxOther:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnCheckBoxOther];
    
    if(isCompare==true)
    {
        
        [_btnCheckBoxOther setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"Other"];
        
    }
    else
    {
        
        [_btnCheckBoxOther setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"Other"];
        
    }
    
}
#pragma mark- SECTION 4 Methods

- (IBAction)actionOnBtnRadioEvidenceYes:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImageRadio:_btnRadioEvidencForYes];
    
    if(isCompare==true)
    {
        [_btnRadioEvidencForYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioEvidenceForNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"EvidencOfPreviousTreatmentObserved"];
    }
    else
    {
        
        [_btnRadioEvidencForYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"" forKey:@"EvidencOfPreviousTreatmentObserved"];
    }
    
}
- (IBAction)actionOnBtnRadioEvidenceNO:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImageRadio:_btnRadioEvidenceForNo];
    
    if(isCompare==true)
    {
        [_btnRadioEvidenceForNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioEvidencForYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"EvidencOfPreviousTreatmentObserved"];
    }
    else
    {
        
        [_btnRadioEvidenceForNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"" forKey:@"EvidencOfPreviousTreatmentObserved"];
    }
    
}
- (IBAction)actionOnBtnRadioCompanyStructureYes:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImageRadio:_btnRadioCompanyStructureForYes];
    
    if(isCompare==true)
    {
        [_btnRadioCompanyStructureForYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioCompnayStructureForNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"TimeOfInspection"];
    }
    else
    {
        
        [_btnRadioCompanyStructureForYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"" forKey:@"TimeOfInspection"];
    }
    
}
- (IBAction)actionOnBtnRadioCompanyStructureNo:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImageRadio:_btnRadioCompnayStructureForNo];
    
    if(isCompare==true)
    {
        [_btnRadioCompnayStructureForNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioCompanyStructureForYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"TimeOfInspection"];
    }
    else
    {
        
        [_btnRadioCompnayStructureForNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"" forKey:@"TimeOfInspection"];
    }
    
}
- (IBAction)actionOnBtnChckBoxWholeStructure:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnChkBoxWholeStructure];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxWholeStructure setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"WholeStructure"];
        
    }
    else
    {
        
        [_btnChkBoxWholeStructure setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"WholeStructure"];
        
    }
    
    
}
- (IBAction)actionOnBtnChkBoxSpotTreatment:(id)sender
{
    yesEditedSomething=YES;
    
    BOOL isCompare =  [global checkImage:_btnChkBoxSpotTreatment];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxSpotTreatment setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"true" forKey:@"SpotTreatment"];
        
    }
    else
    {
        
        [_btnChkBoxSpotTreatment setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:@"false" forKey:@"SpotTreatment"];
        
    }
}

- (IBAction)actionOnbtnSignatureLicenseAgent:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    TermiteSignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"TermiteSignViewController"];
    objSignViewController.strType=@"strBuyersInitials7B";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}
- (IBAction)action_Back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- BASIC METHODS

-(void)methodSetAllViews
{
    
    [self setTextViewColor:_txtViewDAMAGEcausedbyWDO];
    [self setTextViewColor:_txtViewForComment];
    [self setTextViewColor:_txtViewAtticReason];
    [self setTextViewColor:_txtViewOtherReason];
    [self setTextViewColor:_txtViewExteriorReason];
    [self setTextViewColor:_txtViewInteriorReason];
    [self setTextViewColor:_txtViewEvidenceOfWDO];
    [self setTextViewColor:_txViewCrawlPlaceReason];
    
    
    
    [self setButtonwColor:_btnDateOfInspection];
    [self setButtonwColor:_btnInspectionDate];
    [self setButtonwColor:_btnDateOfLicenceeAgent];
    /*[_btnDateOfInspection.layer setCornerRadius:5.0f];
     [_btnDateOfInspection.layer setBorderColor:[UIColor blackColor].CGColor];
     [_btnDateOfInspection.layer setBorderWidth:0.8f];
     [_btnDateOfInspection.layer setShadowColor:[UIColor blackColor].CGColor];
     [_btnDateOfInspection.layer setShadowOpacity:0.3];
     [_btnDateOfInspection.layer setShadowRadius:3.0];
     [_btnDateOfInspection.layer setShadowOffset:CGSizeMake(2.0, 2.0)];*/
    
    CGRect frameFor_View1=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,_view1.frame.size.height);
    [_view1 setFrame:frameFor_View1];
    
    [_scrollVieww addSubview:_view1];
    
    CGRect frameFor_View2;
    if ([UIScreen mainScreen].bounds.size.height<570)
    {
        frameFor_View2=CGRectMake(0, _view1.frame.origin.y+_view1.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view2.frame.size.height);
    }
    else
    {
        frameFor_View2=CGRectMake(0, _view1.frame.origin.y+_view1.frame.size.height, [UIScreen mainScreen].bounds.size.width,1840+20);
    }
    
    [_view2 setFrame:frameFor_View2];
    
    [_scrollVieww addSubview:_view2];
    
    CGRect frameFor_View3=CGRectMake(0, _view2.frame.origin.y+_view2.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view3.frame.size.height);
    [_view3 setFrame:frameFor_View3];
    
    
    
    [_scrollVieww addSubview:_view3];
    
    
    CGRect frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view4.frame.size.height);
    [_view4 setFrame:frameFor_View4];
    
    [_scrollVieww addSubview:_view4];
    
    CGRect frameFor_View5=CGRectMake(0, _view4.frame.origin.y+_view4.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view5.frame.size.height);
    [_view5 setFrame:frameFor_View5];
    
    [_scrollVieww addSubview:_view5];
    
    
    
    [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_view5.frame.size.height+_view5.frame.origin.y)];
    
}
-(void)setTextViewColor:(UITextView*)textView
{
    textView.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    textView.layer.borderWidth=1.0;
    textView.layer.cornerRadius=5.0;
    
}
-(void)setButtonwColor:(UIButton*)btn
{
    [btn.layer setCornerRadius:5.0f];
    [btn.layer setBorderColor:[UIColor blackColor].CGColor];
    [btn.layer setBorderWidth:0.8f];
    [btn.layer setShadowColor:[UIColor blackColor].CGColor];
    [btn.layer setShadowOpacity:0.3];
    [btn.layer setShadowRadius:3.0];
    [btn.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}


//============================================================================
#pragma mark- Load Image
//============================================================================

- (UIImage*)loadImage:(NSString *)imageName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
-(void)addPickerViewDateTo :(NSString*)strDateString
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    // [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strDateString.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strDateString];
        
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    //    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    //    singleTap1.numberOfTapsRequired = 1;
    //    [viewBackGround setUserInteractionEnabled:YES];
    //    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    //btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-100, lblLine.frame.origin.y+5, 100, 40)];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,40)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSInteger i;
    i=tblData.tag;
    if (i==100)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        //[dateFormat setDateFormat:@"EEEE, MMMM dd, yyyy"];
        
        pickerDate.minimumDate=[NSDate date];
        strDate = [dateFormat stringFromDate:pickerDate.date];
        [_btnDateOfInspection setTitle:strDate forState:UIControlStateNormal];
        strDateOfInspectiondate=strDate;
        [dictGlobalTermiteFlorida setValue:strDateOfInspectiondate forKey:@"DateOfInspection"];
        
        
        //[_btnDateOfInspection setTitle:[self getDateInDayFormat:strDate] forState:UIControlStateNormal];
    }
    else if (i==200)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        //[dateFormat setDateFormat:@"EEEE, MMMM dd, yyyy"];
        
        pickerDate.minimumDate=[NSDate date];
        strDate = [dateFormat stringFromDate:pickerDate.date];
        [_btnDateOfLicenceeAgent setTitle:strDate forState:UIControlStateNormal];
        strDateOfLicenceeAgent=strDate;
        [dictGlobalTermiteFlorida setValue:strDateOfLicenceeAgent forKey:@"DateOfLicenseOrAgent"];
        
        
        //[_btnDateOfLicenceeAgent setTitle:[self getDateInDayFormat:strDate] forState:UIControlStateNormal];
    }
    else if (i==300)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        //[dateFormat setDateFormat:@"EEEE, MMMM dd, yyyy"];
        
        pickerDate.minimumDate=[NSDate date];
        strDate = [dateFormat stringFromDate:pickerDate.date];
        [_btnInspectionDate setTitle:strDate forState:UIControlStateNormal];
        strInspectionDate=strDate;
        [dictGlobalTermiteFlorida setValue:strInspectionDate forKey:@"InspectionDate"];
        
        
        //[_btnInspectionDate setTitle:[self getDateInDayFormat:strDate] forState:UIControlStateNormal];
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}
-(void)settingDate :(NSInteger)tag{
    
    if ([_btnDateOfInspection.titleLabel.text isEqualToString:@"Select Date"]) {
        [_btnDateOfInspection setTitle:strDate forState:UIControlStateNormal];
        strDateOfInspectiondate=strDate;
    } else {
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        [_btnDateOfInspection setTitle:strDate forState:UIControlStateNormal];
        strDateOfInspectiondate=strDate;
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}
-(NSString *)getDateInDayFormat:(NSString*)dateString
{
    //Getting date from string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:dateString];
    // converting into our required date format
    //[dateFormatter setDateFormat:@"EEEE, MMMM dd, yyyy"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    NSLog(@"date is %@", reqDateString);
    return reqDateString;
}
//Nilind 15 Dec
-(NSString *)currentDate
{
    //Getting date from string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [NSDate date];
    // converting into our required date format
    //[dateFormatter setDateFormat:@"EEEE, MMMM dd, yyyy"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    NSLog(@"date is %@", reqDateString);
    return reqDateString;
}
//============================================================================

#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}
//.............................
#pragma mark- Textview  Delegate

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        [_txtViewDAMAGEcausedbyWDO resignFirstResponder];
        [_txtViewForComment resignFirstResponder];
        [_txtViewAtticReason resignFirstResponder];
        [_txtViewOtherReason resignFirstResponder];
        [_txtViewExteriorReason resignFirstResponder];
        [_txtViewInteriorReason resignFirstResponder];
        [_txtViewEvidenceOfWDO resignFirstResponder];
        [_txViewCrawlPlaceReason resignFirstResponder];
        
        
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}
#pragma mark- Fetch Florida Data From Coredata
-(void)fetchWorkOrderFromDataBase
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrderNew=arrAllObjWorkOrder[0];
        
        NSString *strServiceAddress2=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"billingAddress2"]];
        NSString *strAdd;
        if (strServiceAddress2.length==0) {
            
            strAdd=[NSString stringWithFormat:@"%@, %@, %@, %@",[matchesWorkOrderNew valueForKey:@"servicesAddress1"],[matchesWorkOrderNew valueForKey:@"serviceCity"],[matchesWorkOrderNew valueForKey:@"serviceState"],[matchesWorkOrderNew valueForKey:@"serviceZipcode"]];
            
            
        }else{
            
            strAdd=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrderNew valueForKey:@"servicesAddress1"],[matchesWorkOrderNew valueForKey:@"serviceAddress2"],[matchesWorkOrderNew valueForKey:@"serviceCity"],[matchesWorkOrderNew valueForKey:@"serviceState"],[matchesWorkOrderNew valueForKey:@"serviceZipcode"]];
            
            
        }
        _txtAddressOfPropoertyInspected.text=strAdd;
        _txtFieldAddressOfPropertyInspectedView5.text=strAdd;
        
    }
    
}
//============================================================================
#pragma mark- Fetch From DB Values
//============================================================================

-(void)getDataFrmDB{
    
    NSLog(@"Work Order Id===%@",_strWorkOrder);
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetailFlorida;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
    requestTermiteFlorida = [[NSFetchRequest alloc] init];
    [requestTermiteFlorida setEntity:entityTermiteFlorida];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestTermiteFlorida setPredicate:predicate];
    
    sortDescriptorTermiteFlorida = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsTermiteFlorida = [NSArray arrayWithObject:sortDescriptorTermiteFlorida];
    
    [requestTermiteFlorida setSortDescriptors:sortDescriptorsTermiteFlorida];
    
    fetchedResultsControllerWorkOrderDetailFlorida = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTermiteFlorida managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetailFlorida setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetailFlorida performFetch:&error];
    arrAllObjTermiteFlorida = [fetchedResultsControllerWorkOrderDetailFlorida fetchedObjects];
    if ([arrAllObjTermiteFlorida count] == 0)
    {
        //matchesWorkOrder=nil;
        [self setFloridaTermiteFormifBlank];
        [self showValuesSavedInDb];
        
    }
    else
    {
        matchesWorkOrder=arrAllObjTermiteFlorida[0];
        dictGlobalTermiteFlorida=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteFlorida=(NSMutableDictionary*)[matchesWorkOrder valueForKey:@"floridaTermiteServiceDetail"];
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[matchesWorkOrder valueForKey:@"floridaTermiteServiceDetail"]
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        
        NSDictionary *dictDataTermite = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        dictDataTermite=[self nestedDictionaryByReplacingNullsWithNil:dictDataTermite];
        dictGlobalTermiteFlorida=[[NSMutableDictionary alloc]init];
        [dictGlobalTermiteFlorida addEntriesFromDictionary:dictDataTermite];
        [self showValuesSavedInDb];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

-(void)setFloridaTermiteFormifBlank{
    
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"TermiteFloridaStaticForm.json"];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSDictionary *dictOfTermiteDate = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    dictGlobalTermiteFlorida=[[NSMutableDictionary alloc]init];
    [dictGlobalTermiteFlorida addEntriesFromDictionary:dictOfTermiteDate];
    
    [dictGlobalTermiteFlorida setValue:_strWorkOrder forKey:@"WorkorderId"];
    [dictGlobalTermiteFlorida setValue:@"" forKey:@"TexasTermiteServiceDetailID"];
    [dictGlobalTermiteFlorida setValue:[global strCurrentDate] forKey:@"CreatedDate"];
    [dictGlobalTermiteFlorida setValue:strCreatedBy forKey:@"CreatedBy"];
    [dictGlobalTermiteFlorida setValue:[global strCurrentDate] forKey:@"ModifiedDate"];
    [dictGlobalTermiteFlorida setValue:strCreatedBy forKey:@"ModifiedBy"];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictGlobalTermiteFlorida])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictGlobalTermiteFlorida options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Final Service Automation JSON: %@", jsonString);
        }
    }
    
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictDataTermite = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    //Equipment Detail Entity
    entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
    
    FloridaTermiteServiceDetail *objImagesDetail = [[FloridaTermiteServiceDetail alloc]initWithEntity:entityTermiteFlorida insertIntoManagedObjectContext:context];
    
    objImagesDetail.companyKey=@"";
    
    objImagesDetail.userName=@"";
    
    objImagesDetail.workorderId=_strWorkOrder;
    
    objImagesDetail.floridaTermiteServiceDetail=dictDataTermite;
    
    NSError *error1;
    [context save:&error1];
    
    
    [self getDataFrmDB];
}

-(void)showValuesSavedInDb
{
    [self fetchWorkOrderFromDataBase];
    
    //For View1
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    BOOL isBranchAddress = false;
        
    if ([[dictLoginData valueForKey:@"Branch"] isKindOfClass:[NSDictionary class]]) {
                
        NSString *strBranch = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine1"]];
        
        if (strBranch.length > 0) {
            
            isBranchAddress = true;
            
        }else{
            
            isBranchAddress = false;
            
        }
        
    } else {
        
        isBranchAddress = false;
        
    }
    
    NSString *strInspectionCompanyName=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionCompanyName"]];
    if ([strInspectionCompanyName isEqualToString:@"" ]||strInspectionCompanyName.length==0||[strInspectionCompanyName isEqualToString:@"(null)"])
    {
        _txtInspectionCompany.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Name"]];
        [dictGlobalTermiteFlorida setValue:_txtInspectionCompany.text forKey:@"InspectionCompanyName"];
        
        
    }
    else
    {
        _txtInspectionCompany.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionCompanyName"]];
        
    }
    
    //_txtBusinessLicenseNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"BusinessLicenseNumber"]];
    
    NSString *strBusinessLicenseNumber=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"BusinessLicenseNumber"]];
    if ([strBusinessLicenseNumber isEqualToString:@"" ]||strBusinessLicenseNumber.length==0||[strBusinessLicenseNumber isEqualToString:@"(null)"])
    {
        _txtBusinessLicenseNo.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.BusinessLicenceNo"]];
        [dictGlobalTermiteFlorida setValue:_txtBusinessLicenseNo.text forKey:@"BusinessLicenseNumber"];
        
    }
    else
    {
        _txtBusinessLicenseNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"BusinessLicenseNumber"]];
        
    }
    
    NSString *strInspectionCompanyAddress=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionCompanyAddress"]];
    
    if ([strInspectionCompanyAddress isEqualToString:@"" ]||strInspectionCompanyAddress.length==0||[strInspectionCompanyAddress isEqualToString:@"(null)"])
    {
        
        if (isBranchAddress) {
            
            NSString *strBranchAddressLine2 = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine2"]];
            
            if (strBranchAddressLine2.length > 0) {
                
                _txtCompanyAddress.text=[NSString stringWithFormat:@"%@, %@",[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine1"],[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine2"]];
                
            } else {
                
                _txtCompanyAddress.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine1"]];

            }

        } else {
            
            NSString *strCompanyAddressLine2 = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine2"]];
            
            if (strCompanyAddressLine2.length > 0) {
                
                _txtCompanyAddress.text=[NSString stringWithFormat:@"%@, %@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine1"],[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine2"]];
                
            } else {
                
                _txtCompanyAddress.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine1"]];

            }
                        
        }
        
        //_txtCompanyAddress.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine1"]];
        
        //_txtCompanyAddress.text = [global strFloridaAddress:[dictLoginData valueForKey:@"Company"]];

        [dictGlobalTermiteFlorida setValue:_txtCompanyAddress.text forKey:@"CompanyAddress"];
    }
    else
    {
        _txtCompanyAddress.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyAddress"]];
    }
    
    
    NSString *strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyCity"]];
    if ([strInspectionCompanyCity isEqualToString:@"" ]||strInspectionCompanyCity.length==0||[strInspectionCompanyCity isEqualToString:@"(null)"])
    {
        if (isBranchAddress) {
            
            strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchCity"]];

        } else {
            
            strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CityName"]];
            
        }
        
        //strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CityName"]];
        [dictGlobalTermiteFlorida setValue:strInspectionCompanyCity forKey:@"CompanyCity"];
    }
    else
    {
        strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyCity"]];
        
    }
    
    NSString *strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyState"]];
    if ([strInspectionCompanyState isEqualToString:@"" ]||strInspectionCompanyState.length==0||[strInspectionCompanyState isEqualToString:@"(null)"])
    {
        if (isBranchAddress) {
            
            strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchState"]];

        } else {
            
            strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.StateName"]];
            
        }
        
        //strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.StateName"]];
        [dictGlobalTermiteFlorida setValue:strInspectionCompanyState forKey:@"CompanyState"];
    }
    else
    {
        strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyState"]];
        
    }
    
    NSString *strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyZipCode"]];
    if ([strInspectionCompanyZipCode isEqualToString:@"" ]||strInspectionCompanyZipCode.length==0||[strInspectionCompanyZipCode isEqualToString:@"(null)"])
    {
        
        if (isBranchAddress) {
            
            strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchZipCode"]];

        } else {
            
            strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ZipCode"]];

        }
        
        //strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ZipCode"]];
        [dictGlobalTermiteFlorida setValue:strInspectionCompanyZipCode forKey:@"CompanyZipCode"];
    }
    else
    {
        strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyZipCode"]];
    }
    
    //_txtComapnyCityStateZipcode.text=[NSString stringWithFormat:@"%@, %@, %@",strInspectionCompanyCity,strInspectionCompanyState,strInspectionCompanyZipCode];
    _txtComapnyCity.text=[NSString stringWithFormat:@"%@",strInspectionCompanyCity];
    _txtCompanyState.text=[NSString stringWithFormat:@"%@",strInspectionCompanyState];
    _txtCompanyZipcode.text=[NSString stringWithFormat:@"%@",strInspectionCompanyZipCode];
    
    //_txtDateOfInspection.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionDate"]];
    
    
    NSString *strInspectionTelephoneNo=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"PhoneNumber"]];
    if ([strInspectionTelephoneNo isEqualToString:@"" ]||strInspectionTelephoneNo.length==0||[strInspectionTelephoneNo isEqualToString:@"(null)"])
    {
        _txtPhoneNo.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.PrimaryPhone"]];
        [dictGlobalTermiteFlorida setValue:_txtPhoneNo.text forKey:@"PhoneNumber"];
        
        
    }
    else
    {
        _txtPhoneNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"PhoneNumber"]];
        
    }
    
    NSString *strInspectionInspectorName=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectorName"]];
    if ([strInspectionInspectorName isEqualToString:@"" ]||strInspectionInspectorName.length==0||[strInspectionInspectorName isEqualToString:@"(null)"])
    {
        _txt1InspectorNameAndIdentificationNo.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
        [dictGlobalTermiteFlorida setValue:_txt1InspectorNameAndIdentificationNo.text forKey:@"InspectorName"];
    }
    else
    {
        _txt1InspectorNameAndIdentificationNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectorName"]];
    }
   
     NSString *strIdentificationNo=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"IdentificationCardNumber"]];
     if ([strIdentificationNo isEqualToString:@"" ]||strIdentificationNo==0||[strIdentificationNo isEqualToString:@"(null)"])
     {
     _txt2InspectorNameAndIdentificationNo.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"IDCardNo"]];
     [dictGlobalTermiteFlorida setValue:_txt2InspectorNameAndIdentificationNo.text forKey:@"IdentificationCardNumber"];
     }
     else
     {
     _txt2InspectorNameAndIdentificationNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"IdentificationCardNumber"]];
     }
   // _txt2InspectorNameAndIdentificationNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"IdentificationCardNumber"]];
    
    
    strDateOfInspectiondate=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"DateOfInspection"]];
    
    if (strDateOfInspectiondate.length==0) {
        //currentDate
        [_btnDateOfInspection setTitle:[self currentDate] forState:UIControlStateNormal];
        
        [dictGlobalTermiteFlorida setValue:[self currentDate] forKey:@"DateOfInspection"];
        
    } else {
        
        strDateOfInspectiondate=[self changeDateToFormattedDate:strDateOfInspectiondate];
        
        [_btnDateOfInspection setTitle:strDateOfInspectiondate forState:UIControlStateNormal];
        
        [dictGlobalTermiteFlorida setValue:strDateOfInspectiondate forKey:@"DateOfInspection"];
        
    }
    
    _txtAddressOfPropoertyInspected.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"AddressOfPropertyInspected"]];
    
    
    _txtStructurePropertyInspected.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"StructureOnPropertyInspected"]];
    
    _txtInspectionAndReportRequestedBy.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionandReportRequestedBy"]];
    _txtInspectionAndReportRequestedTo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"ReportSentToRequestor"]];
    
    //View 1 End
    
    
    
    //For View 2
    
    
    NSString *strbtnChkBoxA=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"NoVisibleSignOfWDOObserved"]];
    if([strbtnChkBoxA isEqualToString:@"true"] || [strbtnChkBoxA isEqualToString:@"1"]){
        
        [_btnCheckBoxA setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxA setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxB=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"VisibleEvidenceOfWDOObserved"]];
    if([strbtnChkBoxB isEqualToString:@"true"] || [strbtnChkBoxB isEqualToString:@"1"]){
        
        [_btnCheckBoxB setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxB setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxLiveWDO=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"LiveWDO"]];
    if([strbtnChkBoxLiveWDO isEqualToString:@"true"] || [strbtnChkBoxLiveWDO isEqualToString:@"1"]){
        
        [_btnCheckBoxLiveWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxLiveWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtLiveWDO.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"LiveWDO_Text"]];
    
    
    
    
    NSString *strbtnChkBoxEvidenceOfWDO=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"EvidenceOfWDO"]];
    if([strbtnChkBoxEvidenceOfWDO isEqualToString:@"true"] || [strbtnChkBoxEvidenceOfWDO isEqualToString:@"1"]){
        
        [_btnCheckBoxEvidenceOfWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxEvidenceOfWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtViewEvidenceOfWDO.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"EvidenceOfWDO_Text"]];
    
    
    
    NSString *strbtnChkBoxDamageCausedbyWDO=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"DamageCausedbyWDO"]];
    if([strbtnChkBoxDamageCausedbyWDO isEqualToString:@"true"] || [strbtnChkBoxDamageCausedbyWDO isEqualToString:@"1"]){
        
        [_btnChkBoxDAMAGEcausedbyWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxDAMAGEcausedbyWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtViewDAMAGEcausedbyWDO.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"DamageCausedbyWDO_Text"]];
    
    //View 2 End
    
    
    //For View 3
    
    
    NSString *strbtnChkBoxAttic=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Attic"]];
    if([strbtnChkBoxAttic isEqualToString:@"true"] || [strbtnChkBoxAttic isEqualToString:@"1"]){
        
        [_btnCheckBoxAttic setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxAttic setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldAtticSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Attic_SpecificAreas"]];
    _txtViewAtticReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Attic_Reason"]];
    
    
    NSString *strbtnChkBoxInterior=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Interior"]];
    if([strbtnChkBoxInterior isEqualToString:@"true"] || [strbtnChkBoxInterior isEqualToString:@"1"]){
        
        [_btnCheckBoxInterior setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxInterior setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldInteriorSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Interior_SpecificAreas"]];
    _txtViewInteriorReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Interior_Reason"]];
    
    
    
    NSString *strbtnChkBoxExterior=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Exterior"]];
    if([strbtnChkBoxExterior isEqualToString:@"true"] || [strbtnChkBoxExterior isEqualToString:@"1"]){
        
        [_btnCheckBoxExterior setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxExterior setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldExteriorSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Exterior_SpecificAreas"]];
    _txtViewExteriorReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Exterior_Reason"]];
    
    
    NSString *strbtnChkBoxCrawlPlace=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CrawlSpace"]];
    if([strbtnChkBoxCrawlPlace isEqualToString:@"true"] || [strbtnChkBoxCrawlPlace isEqualToString:@"1"]){
        
        [_btnCheckBoxCrawlPlace setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxCrawlPlace setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldCrawlPlaceSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CrawlSpace_SpecificAreas"]];
    _txViewCrawlPlaceReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CrawlSpace_Reason"]];
    
    
    NSString *strbtnChkBoxJ=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Other"]];
    if([strbtnChkBoxJ isEqualToString:@"true"] || [strbtnChkBoxJ isEqualToString:@"1"]){
        
        [_btnCheckBoxOther setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxOther setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldOtherSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Other_SpecificAreas"]];
    _txtViewOtherReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Other_Reason"]];
    
    //View 3 End
    
    
    
    //View 4
    
    NSString *strbtnRadioEvidencOfPreviousTreatmentObserved=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"EvidencOfPreviousTreatmentObserved"]];
    if([strbtnRadioEvidencOfPreviousTreatmentObserved isEqualToString:@"true"] || [strbtnRadioEvidencOfPreviousTreatmentObserved isEqualToString:@"1"])
    {
        
        [_btnRadioEvidencForYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioEvidenceForNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnRadioEvidenceForNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioEvidencForYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    _txtFieldForEvidence.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"EvidencOfPreviousTreatmentObserved_Text"]];
    
    
    _txtFieldForNoticeOfInspection.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"NoticeOfInspection"]];
    
    
    
    
    NSString *strbtnRadioTimeOfInspection=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"TimeOfInspection"]];
    if([strbtnRadioTimeOfInspection isEqualToString:@"true"] || [strbtnRadioTimeOfInspection isEqualToString:@"1"])
    {
        
        [_btnRadioCompanyStructureForYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioCompnayStructureForNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnRadioCompnayStructureForNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioCompanyStructureForYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    _txtFieldForCommonNameOfOrganism.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CommonNameOfOrganisationTreated"]];
    _txtFieldForNameOfPesticideUsed.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"NameOfPesticideUsed"]];
    _txtFieldForTermsAndCondition.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"TermsandConditionsOfTreatment"]];
    
    
    NSString *strbtnChkBoxWholeStructure=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"WholeStructure"]];
    if([strbtnChkBoxWholeStructure isEqualToString:@"true"] || [strbtnChkBoxWholeStructure isEqualToString:@"1"]){
        
        [_btnChkBoxWholeStructure setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxWholeStructure setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxSpotTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"WholeStructure"]];
    if([strbtnChkBoxSpotTreatment isEqualToString:@"true"] || [strbtnChkBoxSpotTreatment isEqualToString:@"1"]){
        
        [_btnChkBoxSpotTreatment setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxSpotTreatment setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldForMethodOfTreatment.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"SpotTreatment_Text"]];
    _txtFieldForSpecifyTreatmentNotice.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"SpecifyTreatmentNoticeLocation"]];
    
    //View 4 End
    
    
    //View 5
    
    _txtViewForComment.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Comments"]];
    
    
    
    
    //DateOfPurchaserOfProperty
    
    
    NSString *strSignOfAgentPath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"SignatureOfLicenseOrAgent"]];
    
    if (!(strSignOfAgentPath.length==0)) {
        
        [self downloadBuyersInitials:strSignOfAgentPath :_imgViewSignatureofLicenseAgent];
        strGlobalSignOfAgent=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"SignatureOfLicenseOrAgent"]];
        
    }else{
        
        _imgViewSignatureofLicenseAgent.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalSignOfAgent=@"";
        
    }
    
    
    strInspectionDate=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionDate"]];
    
    if (strInspectionDate.length==0) {
        //currentDate
        [_btnInspectionDate setTitle:[self currentDate] forState:UIControlStateNormal];
        
        [dictGlobalTermiteFlorida setValue:[self currentDate] forKey:@"InspectionDate"];
        
    } else {
        
        strInspectionDate=[self changeDateToFormattedDate:strInspectionDate];
        
        [_btnInspectionDate setTitle:strInspectionDate forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:strInspectionDate forKey:@"InspectionDate"];
    }
    
    //SignatureOfLicenseOrAgent
    strDateOfLicenceeAgent=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"DateOfLicenseOrAgent"]];
    
    if (strDateOfLicenceeAgent.length==0) {
        //currentDate
        [_btnDateOfLicenceeAgent setTitle:[self currentDate] forState:UIControlStateNormal];
        
        [dictGlobalTermiteFlorida setValue:[self currentDate] forKey:@"DateOfLicenseOrAgent"];
        
    }
    else
    {
        
        strDateOfLicenceeAgent=[self changeDateToFormattedDate:strDateOfLicenceeAgent];
        
        [_btnDateOfLicenceeAgent setTitle:strDateOfLicenceeAgent forState:UIControlStateNormal];
        [dictGlobalTermiteFlorida setValue:strDateOfLicenceeAgent forKey:@"DateOfLicenseOrAgent"];
        
    }
    
    _txtFieldAddressOfPropertyInspectedView5.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"AddressOfPropertInspected"]];
    
    
}
-(NSString*)changeDateToFormattedDate :(NSString*)strDateBeingConverted{
    //2017-11-29T00:00:00
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateBeingConverted];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateBeingConverted];
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    
    //New change for Formatted Date
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    return finalTime;
    
}
-(void)downloadBuyersInitials :(NSString*)str :(UIImageView*)imgView{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str :imgView];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str :imgView];
                
            });
        });
        
    }
}


-(void)ShowFirstImageTech :(NSString*)str :(UIImageView*)imgView{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    imgView.image=[self loadImage:result];
    
}

- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}

- (IBAction)actionOnBtnDateOfLicenceeAgent:(id)sender
{
    yesEditedSomething=YES;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=200;
    [self addPickerViewDateTo :strDateOfLicenceeAgent];
}
- (IBAction)actionOnBtnInspectionDate:(id)sender
{
    yesEditedSomething=YES;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=300;
    [self addPickerViewDateTo :strInspectionDate];
}

- (IBAction)actionOnCancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (IBAction)actionOnSave:(id)sender
{
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
        
        [self goToInvoiceView];
        
        
    }
    else
    {
        
        if (strGlobalSignOfAgent.length==0) {
            
            [global AlertMethod:Alert :@"Please complete signatures to continue"];
            
        }
        else
        {
            if (yesEditedSomething) {
                
                [self updateModifyDate];
                
            }
            
            [dictGlobalTermiteFlorida setValue:[global strCurrentDate] forKey:@"ModifiedDate"];
            
            //Nilind
            [dictGlobalTermiteFlorida setValue:_txtViewEvidenceOfWDO.text forKey:@"EvidenceOfWDO_Text"];
            [dictGlobalTermiteFlorida setValue:_txtViewDAMAGEcausedbyWDO.text forKey:@"DamageCausedbyWDO_Text"];
            [dictGlobalTermiteFlorida setValue:_txtViewAtticReason.text forKey:@"Attic_Reason"];
            [dictGlobalTermiteFlorida setValue:_txtViewInteriorReason.text forKey:@"Interior_Reason"];
            [dictGlobalTermiteFlorida setValue:_txtViewExteriorReason.text forKey:@"Exterior_Reason"];
            [dictGlobalTermiteFlorida setValue:_txViewCrawlPlaceReason.text forKey:@"CrawlSpace_Reason"];
            [dictGlobalTermiteFlorida setValue:_txtViewOtherReason.text forKey:@"Other_Reason"];
            [dictGlobalTermiteFlorida setValue:_txtViewForComment.text forKey:@"Comments"];
            [dictGlobalTermiteFlorida setValue:_txtAddressOfPropoertyInspected.text forKey:@"AddressOfPropertyInspected"];
            [dictGlobalTermiteFlorida setValue:_txtFieldAddressOfPropertyInspectedView5.text forKey:@"AddressOfPropertInspected"];
            //End
            
            
            NSError *errorNew = nil;
            NSData *json;
            NSString *jsonString;
            // Dictionary convertable to JSON ?
            if ([NSJSONSerialization isValidJSONObject:dictGlobalTermiteFlorida])
            {
                // Serialize the dictionary
                json = [NSJSONSerialization dataWithJSONObject:dictGlobalTermiteFlorida options:NSJSONWritingPrettyPrinted error:&errorNew];
                
                // If no errors, let's view the JSON
                if (json != nil && errorNew == nil)
                {
                    jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                    NSLog(@"Final Florida Service Automation JSON: %@", jsonString);
                }
            }
            
            NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dictDataTermite = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            [matchesWorkOrder setValue:dictDataTermite forKey:@"floridaTermiteServiceDetail"];
            NSError *error1;
            [context save:&error1];
            
            [self goToInvoiceView];
            
        }
        
    }
    
}

//============================================================================
#pragma mark- Text Field Delegate Methods
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    yesEditedSomething=YES;
    
    if (textField==_txtInspectionCompany) {
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"InspectionCompanyName"];
        
    }else if (textField==_txtBusinessLicenseNo){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"BusinessLicenseNumber"];
        
    }else if (textField==_txtCompanyAddress){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"CompanyAddress"];
        
    }
    else if (textField==_txtPhoneNo){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"PhoneNumber"];
        
    }
    else if (textField==_txtComapnyCity){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"CompanyCity"];
        
    }
    else if (textField==_txtCompanyState){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"CompanyState"];
        
    }else if (textField==_txtCompanyZipcode){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"CompanyZipcode"];
        
    }
    else if (textField==_txt1InspectorNameAndIdentificationNo){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"InspectorName"];
    }
    else if (textField==_txt2InspectorNameAndIdentificationNo){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"IdentificationCardNumber"];
        
    }
    else if (textField==_txtAddressOfPropoertyInspected){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"AddressOfPropertyInspected"];//AddressOfPropertyInspected//AddressOfPropertInspected
        
    }
    else if (textField==_txtStructurePropertyInspected){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"StructureOnPropertyInspected"];
        
    }
    else if (textField==_txtInspectionAndReportRequestedBy){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"InspectionandReportRequestedBy"];
        
    }
    else if (textField==_txtInspectionAndReportRequestedTo){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"ReportSentToRequestor"];
        
    }
    else if (textField==_txtLiveWDO){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"LiveWDO_Text"];
        
    }
    else if (textField==_txtFieldAtticSpecificArea){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"Attic_SpecificAreas"];
        
    }
    else if (textField==_txtFieldInteriorSpecificArea){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"Interior_SpecificAreas"];
        
    }
    else if (textField==_txtFieldExteriorSpecificArea){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"Exterior_SpecificAreas"];
        
    }
    else if (textField==_txtFieldCrawlPlaceSpecificArea){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"CrawlSpace_SpecificAreas"];
        
    }
    else if (textField==_txtFieldOtherSpecificArea){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"Other_SpecificAreas"];
        
    }
    else if (textField==_txtFieldForEvidence){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"EvidencOfPreviousTreatmentObserved_Text"];
        
    }
    else if (textField==_txtFieldForNoticeOfInspection){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"NoticeOfInspection"];
        
    }
    else if (textField==_txtFieldForCommonNameOfOrganism){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"CommonNameOfOrganisationTreated"];
        
    }
    else if (textField==_txtFieldForNameOfPesticideUsed){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"NameOfPesticideUsed"];
        
    }
    else if (textField==_txtFieldForTermsAndCondition)
    {
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"TermsandConditionsOfTreatment"];
        
    }
    else if (textField==_txtFieldForMethodOfTreatment){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"SpotTreatment_Text"];
        
    }
    else if (textField==_txtFieldForSpecifyTreatmentNotice){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"SpecifyTreatmentNoticeLocation"];
        
    }
    else if (textField==_txtFieldAddressOfPropertyInspectedView5){
        
        [dictGlobalTermiteFlorida setValue:textField.text forKey:@"AddressOfPropertInspected"];
        
    }
    
    [textField resignFirstResponder];
}

-(void)goToInvoiceView{
    
    [self fetchWorkOrderFromDataBase];
    [self fetchImageDetailFromDataBase];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                             bundle: nil];
    InvoiceAppointmentView
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"InvoiceAppointmentView"];
    
    objByProductVC.workOrderDetail=matchesWorkOrderNew;
    
    if ([strGlobalWorkOrderStatus isEqualToString:@"Complete"] || [strGlobalWorkOrderStatus isEqualToString:@"Completed"])
    {
        
    }
    else
    {
    }
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    objByProductVC.strDepartmentIdd=[NSString stringWithFormat:@"%@",[defsLead valueForKey:@"strDepartmentId"]];
    objByProductVC.strTermiteType=@"florida";
    objByProductVC.arrAllObjImageDetail=arrAllObjImageDetail;

    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
//============================================================================
#pragma mark- ---------******** Modify Date Save *******----------------
//============================================================================
-(void)updateModifyDate{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityModifyDateServiceAuto];
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        
        NSString *strCurrentDateFormatted = [global modifyDateService];
        
        [matchesServiceModifyDate setValue:strCurrentDateFormatted forKey:@"modifyDate"];
        
        [global fetchWorkOrderFromDataBaseForServiceToUpdateModifydate:_strWorkOrder :strCurrentDateFormatted];
        
        //Final Save
        NSError *error;
        [context save:&error];
    }
}
-(void)setEditableOrNot{
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
    {
        [_txtInspectionCompany setEnabled:NO];
        
        //[self setTextDisable:_txtInspectionCompany];
        
        [_txtBusinessLicenseNo setEnabled:NO];
        
        //[self setTextDisable:_txtBusinessLicenseNo];
        
        [_txtCompanyAddress setEnabled:NO];
        [_txtComapnyCity setEnabled:NO];
        [_txtCompanyState setEnabled:NO];
        [_txtCompanyZipcode setEnabled:NO];
        [_txtPhoneNo setEnabled:NO];
        [_txt1InspectorNameAndIdentificationNo setEnabled:NO];
        [_txt2InspectorNameAndIdentificationNo setEnabled:NO];
        [_btnDateOfInspection setEnabled:NO];
        [_txtAddressOfPropoertyInspected setEnabled:NO];
        [_txtStructurePropertyInspected setEnabled:NO];
        [_txtInspectionAndReportRequestedBy setEnabled:NO];
        [_txtInspectionAndReportRequestedTo setEnabled:NO];
        [_btnCheckBoxA setEnabled:NO];
        [_btnCheckBoxB setEnabled:NO];
        [_btnCheckBoxLiveWDO setEnabled:NO];
        [_txtLiveWDO setEnabled:NO];
        [_btnCheckBoxEvidenceOfWDO setEnabled:NO];
        [_txtViewEvidenceOfWDO setEditable:NO];
        [_btnChkBoxDAMAGEcausedbyWDO setEnabled:NO];
        [_txtViewDAMAGEcausedbyWDO setEditable:NO];
        [_btnCheckBoxAttic setEnabled:NO];
        [_txtFieldAtticSpecificArea setEnabled:NO];
        [_txtViewAtticReason setEditable:NO];
        [_btnCheckBoxInterior setEnabled:NO];
        [_txtFieldInteriorSpecificArea setEnabled:NO];
        [_btnCheckBoxExterior setEnabled:NO];
        [_txtFieldExteriorSpecificArea setEnabled:NO];
        [_txtViewExteriorReason setEditable:NO];
        [_btnCheckBoxCrawlPlace setEnabled:NO];
        [_txtFieldCrawlPlaceSpecificArea setEnabled:NO];
        
        [_txViewCrawlPlaceReason setEditable:NO];
        [_btnCheckBoxOther setEnabled:NO];
        [_txtFieldOtherSpecificArea setEnabled:NO];
        [_txtViewOtherReason setEditable:NO];
        [_btnRadioEvidencForYes setEnabled:NO];
        [_btnRadioEvidenceForNo setEnabled:NO];
        [_txtFieldForEvidence setEnabled:NO];
        [_txtFieldForNoticeOfInspection setEnabled:NO];
        [_btnRadioCompanyStructureForYes setEnabled:NO];
        [_btnRadioCompnayStructureForNo setEnabled:NO];
        [_txtFieldForCommonNameOfOrganism setEnabled:NO];
        [_txtFieldForNameOfPesticideUsed setEnabled:NO];
        [_txtFieldForTermsAndCondition setEnabled:NO];
        [_btnChkBoxWholeStructure setEnabled:NO];
        [_btnChkBoxSpotTreatment setEnabled:NO];
        
        [_txtViewForComment setEditable:NO];
        [_btnSignatureLicenseAgent setEnabled:NO];
        [_btnInspectionDate setEnabled:NO];
        [_btnDateOfLicenceeAgent setEnabled:NO];
        [_txtFieldAddressOfPropertyInspectedView5 setEnabled:NO];
        
    }
    else
    {
    }
}
-(void)setTextDisable:(UITextField*)textField
{
    textField.inputView = [[UIView alloc] init];
    textField.delegate = self;
    
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ClockInOutViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
#pragma mark: -------- FETCH IMAGE ----------

-(void)fetchImageDetailFromDataBase
{
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSString* strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    
    
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
@end
