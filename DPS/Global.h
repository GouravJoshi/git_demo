//
//  Global.h
//  CaptureTheirFlag
//  Testing
//  Created by Rakesh Jain on 30/09/15.
//  Copyright © 2015 Sankalp. All rights reserved.
//  Y wali main hai

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVKit/AVKit.h>

//
@interface Global : NSObject<NSFetchedResultsControllerDelegate,CLLocationManagerDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,AVAudioPlayerDelegate,MFMessageComposeViewControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityAddLead;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityOutBox,*entitySalesModifyDate,*entityEmailDetailServiceAuto;
    
    // Modify Date Change
    NSEntityDescription *entityModifyDate,*entityLeadDetailss;
    NSFetchRequest *requestModifyDate,*requestLeadDetailss;
    NSSortDescriptor *sortDescriptorModifyDate,*sortDescriptorLeadDetailss;
    NSArray *sortDescriptorsModifyDate,*sortDescriptorsLeadDetailss;
    NSManagedObject *matchesModifyDate,*matchesLeadDetailss;
    NSArray *arrAllObjModifyDate,*arrAllObjLeadDetailss;
    
    //For Clock Detail
    
    AppDelegate *appDelegateClock;
    NSEntityDescription *entityClockInOutDetail,*entityMechanicalSubWorkOrderHelper;
    NSManagedObjectContext *contextClock;
    NSFetchRequest *requestNewClock,*requestSubWorkOrderHelper;
    NSFetchRequest *requestNewServiceClock;
    NSSortDescriptor *sortDescriptorClock,*sortDescriptorSubWorkOrderHelper;
    NSArray *sortDescriptorsClock,*sortDescriptorsSubWorkOrderHelper;
    NSManagedObject *matchesClock;
    NSArray *arrAllObjClock;

    //MPMoviePlayerController *movieplayer;
    AVPlayerViewController *moviePlayer1;
    
    NSEntityDescription *entitySubWOCompleteTimeExtSerDcs,*entitySubWoEmployeeWorkingTimeExtSerDcs,*entitySubWoEmployeeWorkingTimeExtSerDcsTemp;
    
    
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerClockInOutDetail;
-(NSString*)fetchClockInOutDetailCoreData
;
-(NSString*)getCurrentTimerOfClock;

-(void)AlertMethod :(NSString *)strMsgTitle :(NSString *)strMsg;

//============================================================================
//============================================================================

typedef void (^WebServiceCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForUrl:(NSString *)url :(NSString *)Type withCallback:(WebServiceCompletionBlock)callback;

//============================================================================
//============================================================================

typedef void (^WebServicePostCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForUrlPostMethod:(NSString *)url :(NSData *)requestDataa withCallback:(WebServicePostCompletionBlock)callback;


//============================================================================
//============================================================================

typedef void (^WebServiceCompletionBlockGetAsynchronus)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForUrlGetAynchronusMethod:(NSString *)url :(NSString *)type withCallback:(WebServiceCompletionBlockGetAsynchronus)callback;


//============================================================================
//============================================================================

typedef void (^CoreDataAddleadsSaveBlock)(BOOL success, NSError *error);
- (void)saveToCoreDataLeadInfo:(NSString *)url :(NSString *)type :(NSDictionary *)dictData withCallback:(CoreDataAddleadsSaveBlock)callback;

//============================================================================
//============================================================================

typedef void (^CoreDataOutBoxSaveBlock)(BOOL success, NSError *error);
- (void)saveToCoreDataOutBox:(NSString *)url :(NSString *)type :(NSDictionary *)dictData withCallback:(CoreDataOutBoxSaveBlock)callback;

//============================================================================
//============================================================================

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerAddLead;
-(void)deleteDatabaseBeforeAdding;

//============================================================================
//============================================================================

typedef void (^SendLeadToServerPostCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForSendLeadToServer:(NSString *)url :(NSData *)requestDataa :(NSDictionary *)dictData :(NSString*)indexToDletee :(NSString*)Type withCallback:(SendLeadToServerPostCompletionBlock)callback;

//============================================================================
//============================================================================

typedef void (^SalesDynamicJsonPostCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForSalesDynamicJson:(NSString *)url :(NSData *)requestDataa withCallback:(SalesDynamicJsonPostCompletionBlock)callback;


//============================================================================
//============================================================================
#pragma mark ---------------- Date Change Method--------------------------
//============================================================================
//============================================================================

-(NSString *)ChangeDateToLocalDate :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOther :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOtherNew :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateFull :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOtherSep :(NSString*)strDateToConvert;
-(NSString *)modifyDate;
-(NSString *)ChangeDateToLocalDateOtherTimeAlso :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOtherServiceAppointments :(NSString*)strDateToConvert;
-(BOOL)isCameraPermissionAvailable;
-(BOOL)isAudioPermissionAvailable;
-(BOOL)isGalleryPermission;

-(NSString *)modifyDateService;

typedef void (^SendLeadToServerPostCompletionBlockOffline)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForSendLeadToServerOffline:(NSString *)url :(NSData *)requestDataa :(NSDictionary *)dictData :(NSString*)indexToDletee :(NSDictionary *)dictDataAllValues withCallback:(SendLeadToServerPostCompletionBlockOffline)callback;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerLeadDetailss;

-(void)updateSalesModifydate:(NSString *)strLeadId;
-(void)updateSalesZSYNC:(NSString *)strLeadId :(NSString *)strYesNo;
-(NSString *)ChangeDateToLocalDateOtherLeadView :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOtherServiceAppointmentsUpdated :(NSString*)strDateToConvert;
- (NSString *)getIPAddress;
-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image;
-(NSString *)RemovingWhiteSpaces :(NSString*)strStringToRemove;
-(BOOL)isGreaterDate :(NSDate*)fromDate :(NSDate*)toDate;
-(void)autoDismissAlerView :(UIAlertView*)alertView;
-(BOOL)isStringNullNilBlank:(NSString *)strString;
-(NSDictionary *) nestedDictionaryByReplacingNSDATEwithStringDate:(NSDictionary*)sourceDictionary;
-(NSString *)ChangeDateToLocalDateOtherSaavan :(NSString*)strDateToConvert;
-(NSString *)getReferenceNumber;

-(NSString *)ChangeDateToLocalDateAkshay :(NSString*)strDateToConvert;


typedef void (^WebServicePostCompletionBlockSalesPayment)(BOOL success, NSDictionary *response, NSError *error);
- (void)responseOnSalesPaymentServices:(NSString *)url :(NSString *)strTypeSales :(NSData *)requestDataa withCallback:(WebServicePostCompletionBlockSalesPayment)callback;
+(void)addPaddingView:(UITextField*)textField;

-(NSString*)changeDateToFormattedDateNew :(NSString*)strDateBeingConverted;
-(NSString *)getEmployeeNameViaId :(NSString *)strEmpId;
-(NSString *)strCurrentDate;
-(NSDate *)ChangeDateToLocalDateMechanical :(NSString*)strDateToConvert;
-(NSDate *)ChangeDateToLocalDateMechanicalTimeOut :(NSString*)strDateToConvert;
-(NSString *)strCurrentDateFormattedForMechanical;
-(NSString *)getEmployeeNameViaEMPId :(NSString *)strEmpId;
-(int)ChangeTimeMechanical :(NSString*)strDateToConvert;
-(int)ChangeTimeMechanicalLabor :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateMechanicalParts :(NSString*)strDateToConvert;
-(NSString *)ChangeTimeToHrnMin :(NSString*)strTimeToConvert;
-(BOOL)isNetReachable;
-(NSString *)ChangeDateToLocalDateMechanicalAllDates :(NSString*)strDateToConvert;
-(void)fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate :(NSString*)strWorkOrderId;
-(NSManagedObject*)fetchMechanicalWorkOrderObj :(NSString*)strWorkOrderId;
-(NSManagedObject*)fetchMechanicalSubWorkOrderObj :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId;
-(NSString *)strBoolValueFromServer :(NSString*)strValueComing;
-(NSString *)ChangeDateFormatOnGeneralInf :(NSString*)strDateToConvert;
-(NSString *)ChangeDateFormatOnGeneralInfoTime :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateMechanicalAllDatesEST :(NSString*)strDateToConvert;
-(void)fetchWorkOrderFromDataBaseForMechanicalToUpdateClockStatus :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId :(NSString*)status;
-(void)fetchWorkOrderFromDataBaseForMechanicalToUpdateAdditionalInfo :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId :(NSString*)strAdditionalInfo;
-(BOOL)isNumberOnly :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength :(NSString*)stringTocheck;
-(NSString *)getTaxForMechanicalService:(NSString *)strServiceUrl :(NSString *)strCompanyKeyMechanical :(NSString *)strWorkorderIdMechanical;
-(BOOL)isNumberOnlyWithDecimal :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength :(NSString*)stringTocheck :(NSString*)stringFullText;
-(NSString*)changeDateToFormattedDateNewEXPDATE :(NSString*)strDateBeingConverted;
-(void)getUnsignedDocumentsWoNos :(NSString*)strServiceUrlMainServiceAutomation :(NSString*)strCompanyKeyy;
-(BOOL)runningInBackground;
-(BOOL)runningInForeground;
-(BOOL)runningInInActiveState;
-(void)fetchWorkOrderFromDbToUpdateSurveyStatus :(NSString*)strWorkOrderId :(NSString*)strSurveyStatus;
-(void)fetchLeadFromDbToUpdateSurveyStatus :(NSString*)strWorkOrderId :(NSString*)strSurveyStatus:(NSString*)strStageType;
-(BOOL)isSurveyCompletedService :(NSString*)strWorkOrderId;
-(BOOL)isSurveyCompletedSales :(NSString*)strWorkOrderId;
-(NSManagedObject*)fetchServiceWorkOrderObjToFindStatus :(NSString*)strWorkOrderId;
-(BOOL)checkImage :(UIButton*)btnImgToCheck;
-(BOOL)checkImageRadio :(UIButton*)btnImgToCheck;
-(CLLocationCoordinate2D) getLocation;//:(CLLocationManager*)locationManager;
-(NSDictionary*)getInventoryDetailsByItemNo :(NSString*)strServiceUrlMainServiceAutomation :(NSString*)strCompanyIddd :(NSString*)strItemNumber;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerAddCustomerAddress;

typedef void (^WebServicePostCompletionBlockCustomerAddress)(BOOL success, NSDictionary *response, NSError *error);
- (void)responseOnCustomerAddress:(NSString *)url :(NSString *)strTypeSales :(NSString *)strMode withCallback:(WebServicePostCompletionBlockCustomerAddress)callback;
-(NSString *)ChangeTimeFromTwToTFHrsFormat :(NSString*)strDateToConvert;
-(NSMutableArray*)getHoursConfiFromMaster :(NSString*)strCompanyKey :(NSString*)strDepartMentSysName :(NSString*)strWorkOrderAccNo :(NSString*)strWorkOrderAddressId :(NSString*)strWorkOrderAddressSubType;
-(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp :(NSString*)strCategorySysName :(NSString*)strType :(NSMutableArray*)arrOfPriceLookup;
-(NSMutableArray*)getPricLookupFromMaster :(NSString*)strCompanyKey :(NSString*)strDepartMentSysName :(NSString*)strWorkOrderAccNo :(NSString*)strWorkOrderAddressId :(NSString*)strWorkOrderAddressSubType :(NSString*)strCategorySysName;
-(NSMutableArray*)getPartsMaster :(NSString*)strDepartMentSysName :(NSString*)strCategoryMasterId;
-(float)fetchPartDetailViaPartCode :(NSString*)strPartCode :(NSMutableArray*)arrOfPartsGlobalMasters;
-(NSString*)fetchPartCategorySysNameDetailViaPartCode :(NSString*)strPartCode :(NSMutableArray*)arrOfPartsGlobalMasters;
-(NSDictionary*)fetchPartDetailDictionaryViaPartCode :(NSString*)strPartCode :(NSMutableArray*)arrOfPartsGlobalMasters;
-(BOOL)isNumbernDecimalOnly :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength :(NSString*)stringTocheck;
-(NSDate *)changeStringTimeToDate :(NSString*)strDateToConvert;
-(NSDate *)convertDateToTime :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTimeFromDate :(NSDate*)strDateToConvert;
-(float)totalPriceCalcualtionForLabor :(NSString*)strFromDate :(NSString*)strToDate :(NSArray*)arrOfHoursConfig :(BOOL)isStandardSubWorkOrder;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
//fetchedResultsControllerMechanicalSubWorkOrder
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalSubWorkOrder;
-(void)fetchSubWorkOrderFromDBToUpdateStatus :(NSString*)strWorkOrderId :(NSString*)strType;
-(NSMutableArray*)getSalesTaxFromMaster :(NSString*)strCompanyKey :(NSString*)strDepartMentSysName :(NSString*)strWorkOrderPropertyType;
-(NSDate *)getOnlyDate :(NSDate*)strDateToConvert;
-(NSString *)strCurrentDateFormattedForMechanicalNewLocalTime;
-(NSDate *)getOnlyTime :(NSDate*)strDateToConvert;
-(NSString*)strEmpBranchID;
-(NSString*)strEmpBranchSysName;
-(NSString*)strIsCorporateUser;
-(NSString*)strEmpLoginId;
-(NSDate *)ChangeDateToLocalDateMechanicalUTC :(NSString*)strDateToConvert;
-(NSString *)getStartTimeofDay :(NSDate*)strDateToConvert;
-(NSString *)ChangeDateMechanicalEquipment :(NSString*)strDateToConvert;
-(void)calling :(NSString*)strCellNo;
-(void)emailComposer :(NSString*)stEmailIDs :(NSString*)strSubject  :(NSString*)strBody :(UIViewController*)controller;
-(CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr;
-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary;
-(void)playAudio :(NSString*)path :(UIView*)viewToPlayAudio;
-(NSString*)strNameBackSlashIssue :(NSString*)strAudioName;
-(NSAttributedString*)getUnderLineAttributedString:(NSString*)strText;
-(NSMutableAttributedString*)removeUnderLineAttributedString:(UIButton*)strText;
-(int)ChangeTimeMechanicalInvoice :(NSString*)strDateToConvert;
-(NSString *)ChangeDateFormatOnGeneralInfNew :(NSString*)strDateToConvert;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;
-(NSString*)getEmployeeDeatils;
-(BOOL)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted :(NSString*)strEmployeeNo :(NSString*)strSubWorkOrderId;
-(BOOL)isCompletedSatusMechanical :(NSString*)strStatus;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderHelper;
-(NSMutableArray*)employeeTimeSheetSlotWiseTimeEntry :(NSString*)strFromDate :(NSString*)strToDate :(NSArray*)arrOfHoursConfig :(BOOL)isStandardSubWorkOrder :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId :(NSString*)subWorkOrderActualHourId;
-(NSArray*)fetchSubWorkOrderHelperFromDataBaseForSlotWiseTimeEntry :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal;
-(NSString*)methodToCalculateLaborPriceGlobal :(NSString *)strTimeEntered :(BOOL)isLaborType :(NSArray*)arrOfHoursConfig :(BOOL)isStandardSubWorkOrder :(BOOL)isHoliday :(NSString*)strAfterHrsDuration;
-(void)saveEmployeeTimeSheetSlotWise :(NSArray*)arrEmpSlots;

-(NSString *)getMemberShipCHargeForMechanical:(NSString *)strServiceUrl :(NSString *)strCompanyKeyMechanical :(NSString *)strDepartment :(NSString *)strAccountNo;
-(NSString *)changeDtaeEmpTimeSheet :(NSString*)strDateToConvert;
-(NSMutableArray*)createEmpSheetDataInBackground :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal :(NSArray*)arrOfHoursConfig :(NSString*)strEmpID :(NSString*)strEmployeeNoLoggedIn :(NSString*)strEmpName :(NSString*)strFromWhere :(NSString*)strCurrentTime;
-(NSMutableArray*)arrOfHeaderTitleGlobal :(NSArray*)arrOfGlobalDynamicEmpSheetFinal;
-(UIView*)createDynamicEmpSheet :(NSArray*)arrOfGlobalDynamicEmpSheetFinal :(NSArray*)arrOfHeaderTitleForSlots :(UIScrollView*)scrollViewEmpTimeSheet;
-(NSString*)getHrAndMinsGlobal:(NSString*)strMins;
-(NSString*)convertIntoJson :(NSDictionary*)dictData :(NSArray*)arrData :(NSString*)strType :(NSString*)strLogMsg;
-(CGFloat)globalWidthDevice;
-(NSString*)slotHeaderTitleFromTimeSlot :(NSArray*)arrOfHoursConfig :(NSString*)strSlots;
-(void)copySubWorkOrder :(NSString*)strServiceUrlMainServiceAutomation :(NSString*)strCompanyKey :(NSString*)strIdWorkOrder :(NSString*)strIdSubWorkOrder :(NSString*)strLoggedInUserName;
-(void)displayAlertController :(NSString *)strMsgTitle :(NSString *)strMsg :(UIViewController*)view;
-(void)fetchWorkOrderFromDataBaseForServiceToUpdateModifydate :(NSString*)strWorkOrderId :(NSString*)strCurrentDateToSet;
-(BOOL)isSlotMatched :(NSString*)strSlot1 :(NSString*)strSlotTitle1 :(NSString*)strSlotsToMatch;
-(NSString*)strStatNameFromID :(NSString*)strId;
-(NSString*)strCombinedAddress :(NSDictionary*)dictData;
-(NSString*)strFullName :(NSDictionary*)dictData;
-(NSString*)strCombinedAddressService :(NSDictionary*)dictData;
-(NSString*)strCombinedAddressBilling :(NSDictionary*)dictData;
-(BOOL)isHowMuchLength :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength;
-(NSDictionary *) nestedDictionaryByReplacingNullsWithNilReplicaForArray:(NSDictionary*)sourceDictionary;
-(NSString*)strClientTimeZone;
-(NSArray*)arrFilter :(NSArray*)arrToFilter :(NSString*)strDateParam :(BOOL)ascendingYes;
-(void)redirectOnAppleMap :(UIViewController*)viewToShowOn :(NSString*) addressStrTo;
-(NSString *)ChangeDateToLocalDueTime :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDueTime24Hr :(NSString*)strDateToConvert;
-(BOOL)isMechanicType :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal :(NSString*)strEmpNo;
-(BOOL)isMechanicTypeForLoggedInUser :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal;
-(NSMutableArray*)fetchRunningSubWorkOrder :(NSString*)strEmployeeNo :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId;
-(void)deleteDataFromCoreData :(NSEntityDescription*)entityDescription :(NSString*)strMechanicalWoId  :(NSString*)strEntityForName;
-(NSString *)ChangeDateToLocalActivityTime :(NSString*)strDateToConvert;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssues;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderMechanicalEquipment;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderNotes;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWOCompleteTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepair;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairParts;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairLabour;
//Nilind
-(NSString *)ChangeDateToLocalDateOtherWithTimeNilind :(NSString*)strDateToConvert;
-(NSString*)checkIfStringNull :(NSString*)strValueToCheck;


//============================================================================
//============================================================================

typedef void (^WebServiceCompletionBlockSynchronous)(BOOL success, NSDictionary *response, NSError *error);
- (void)getSynchronousServerResponseForUrl:(NSString *)url :(NSString *)Type withCallback:(WebServiceCompletionBlockSynchronous)callback;

//============================================================================
//============================================================================
-(NSString *)getBranchNameFromSysNam:(NSString*)strBranchSysName;
-(NSString *)ChangeDateToLocalTimeAkshay :(NSString*)strDateToConvert type:(NSString*) strType;

-(NSString *)getCurrentDate;
-(NSString *)getReferenceNumberNew;
-(NSString *)getCurrentDateEST;
-(NSString*)getStartDate: (NSString*)strCurrentDate :(NSInteger)noOfDays;
-(NSString*)getEndDate: (NSString*)strCurrentDate :(NSInteger)noOfDays;
-(BOOL)alertForTaxCode :(NSString*)strTaxCodeSelected;
-(void)defaultSelectedStatus;
-(float)roundedOffValuess :(float)hrsTime;

typedef void (^WebServicePostCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForUrlPostMethodChangePass:(NSString *)url :(NSData *)requestDataa withCallback:(WebServicePostCompletionBlock)callback;

-(BOOL)isNumberOnlyWithDecimalNew :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength :(NSString*)stringTocheck :(NSString*)stringFullText;

-(void)fetchIfEmailIdExistInDB :(NSString*)strEmailId :(NSString*)strType :(NSString*)strWOID;

-(BOOL)emailAlreadyExists :(NSString*)strEmailId :(NSString*)strWOID;

-(NSArray *)getEmployeeBlockTime:(NSString *)strUrl :(NSString *)strCompanyKey :(NSString *)strEmpNo;

-(NSString *)strBlockDateTime :(NSString*)strDate;

-(void)getPlumbingMasterForAccountAddress :(NSString*)strServiceUrlMain :(NSString*)companyKey :(NSDictionary*)dict_ToSend;

-(void)updateStatusOnMyWayLifeStyle :(NSString*)strServiceUrlMain :(NSDictionary*)dict_ToSend;

-(NSString *)strCurrentDateFormatted :(NSString*)strFormat :(NSString*)strTimeZone;

-(NSString *)getSignedAgreementBadgeCount;

-(NSString *)ChangeDateToLocalDateOtherTaskDue :(NSString*)strDateToConvert;

-(NSString*)strReplacedEmail :(NSString*)strStringToBeReplaced;

-(BOOL)validEmail :(NSString*)strEmail;

-(BOOL)checkIfCommaSepartedEmailsAreValid :(NSString*)strEmail;

-(NSString*)fetchSubWorkOrderActualHrsFromDataBaseForActualHourId :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal;

-(NSString*)fetchWorkOrderFromDataBaseForMechanicalToFetchWorkOrderId :(NSString*)strWorkOrderNo;

-(NSManagedObject*)fetchMechanicalSubWorkOrderToStopJob :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId;

-(NSString *)convertDate :(NSString*)strDateToConvert;

-(void)setDefaultNonBillableFromTodate;

-(UIImage *)resizeImageGlobal:(UIImage *)image :(NSString*)imageName;

-(void)playAudioGlobal :(NSString*)strAudioName :(UIView*)viewToPlay;

-(NSString*)returnAudioPath :(NSString*)strAudioName;

-(NSDate *)changeStringDateToDate :(NSString*)strDateToConvert;

-(NSArray*)fetchSubWorkOrderActualHrsFromDataBaseForEmployeeTimeSheet :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal;

-(BOOL)isAlreadyHelperInDb : (NSString*)strHelperIdGlobal :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal;

-(NSArray*)fetchSubWorkOrderHelperFromDataBaseForMechanical :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal;

-(NSString *)getUserName;

-(NSString *)getCompanyKey;

-(NSString*)strFullNameForNSManagedObject :(NSManagedObject*)dictData;

-(NSString*)strCombinedAddressServiceForNSManagedObject :(NSManagedObject*)managedObjectData;

-(NSString*)strCombinedAddressBillingForNSManagedObject :(NSManagedObject*)managedObjectData;

-(NSString *)getCompanyId;

-(NSString *)getEmployeeId;

-(void)fetchServicePestWoToUpdateModifyDateJugad :(NSString*)strWorkOrderId;

-(void)fetchServicePestWoToUpdateModifyDate :(NSString*)strWorkOrderId :(NSString*)strCurrentDateToSet;

-(BOOL)isMoreThenTwoMB :(NSURL*)myURL :(int )sizee;

-(NSString*)strDocNameFromPath :(NSString*)strPath;

-(UIImage *)resizeImageGloballl:(UIImage *)image;

-(NSData *)resizeImageToData:(UIImage *)image;

-(NSArray*)arrOfFilteredData :(NSArray*)arrData :(NSString*)strId :(NSString*)strIdType;

-(NSString *)getEmployeeBranchSysName;

-(NSDate *)convertStringDateToDateForPestFlow :(NSString*)strDateToConvert :(NSString*)strTimeZone;

-(NSArray *)getCategoryDeptWiseGlobal;

-(NSString*)getTotalPrice :(NSMutableArray*)arrPricing;

-(NSDictionary*)getInspectorDetails:(NSString*)strInspectorId;

-(NSString*)fetchLeadStatus :(NSString*)strWdoLeadId;

-(NSString*)strTechSign :(NSString*)strSignUrl;

-(void)updateWoVersionNo :(NSString*)strWorkOrderId;

-(void)updateLeadVersion :(NSString*)strLeadId;

-(void)removeImageFromDocumentDirectory:(NSString *)filename;

-(CGSize)getHeightForString :(NSString*)strString :(int)fontSize :(CGFloat)widthh;
-(NSString *)convertTime :(NSString*)strDateToConvert;
-(NSDate*)getDueDate:(NSString*)strDueDate;
-(NSDate*)getCurrentDateAkshay;
-(NSString *)getModifiedDate;
-(NSDate*)getCompareDate;
-(NSString*)splitString :(NSString*)str :(NSString*)strSplitBy;
-(NSManagedObject*)fetchLeadDetailFromDb :(NSString*)strLeadIdLocal;
-(NSDate *)convertStringDateToDateForCrmNewFlow :(NSString*)strDateToConvert :(NSString*)strDateFormatToConvert :(NSString*)strTimeZone;
-(NSString*)getYesterdayDate: (NSString*)strCurrentDate;
-(NSString*)getYesterdayDateEST: (NSString*)strCurrentDate;
-(NSMutableDictionary*)getCureentMonthDate;
-(NSMutableDictionary*)getCureentMonthDateEST;
-(NSString*)getSourceNameMultiple :(NSArray*)arrSourceIdSelected :(NSArray*)arrayOfSource;
-(NSString*)getBranchNameMultiple :(NSArray*)arrBranchSysName;
-(void)playAudiVedio :(NSString*)path :(UIView*)viewToPlayAudio;
-(NSString *)getEmployeeNameViaEmployeeID :(NSString *)strEmpId;
-(NSString *)strGetCurrentDateInFormat : (NSString*)strFormat;
-(NSString *)latestStringDateToStringFormatted :(NSString*)strDateToConvert :(NSString*)strDateFormatToConvert :(NSString*)strTimeZone;

-(NSMutableAttributedString *)getAttributedString : (NSString *)strHtmlDesc WithFontStyle : (UIFont*)fontStyle;
-(void)checkAppVersionLogic : (UIViewController *)viewToShowAlert;
-(void)diskSpaceCheck : (UIViewController *)viewToShowAlert;
-(BOOL)logicForDownloadingMasters;
-(void)setCrmMastersInUserDefaults :(NSArray*)arrOfLeadStatusMasters :(NSDictionary*)responseDictLocal;
-(NSDictionary*)convertNullArray :(NSDictionary*)dictLocalResponse;
-(NSMutableDictionary*)dictMenuOptions;
-(void)checkIfAppUpdateAvilable;

-(void)getLeadCountNew :(NSString*)stringUrl;
-(void)getEmployeeListNew :(NSString*)stringUrl;
-(void)getMasterServiceAutoNew :(NSString*)stringUrl;
-(void)getServiceDynamicFormMasterNew :(NSString*)stringUrl;
-(void)getProductChemicalNew :(NSString*)stringUrl;
-(void)getMasterEquipmentsNew :(NSString*)stringUrl;
-(void)getSalesMasterNew :(NSString*)stringUrl;
-(void)getDynamicFormSalesAutoMasterNew :(NSString*)stringUrl;


typedef void (^swiftWebServicePostCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getDataFromSwift:(NSString *)url :(NSString *)requestDataa withCallback:(swiftWebServicePostCompletionBlock)callback;
-(NSString*)strFloridaAddress :(NSDictionary*)dictData;
-(NSArray*)filterDataAfterMergingAppointments :(NSArray*)arrOfEmpBlockTime :(NSArray*)arrAllObj;
-(NSString*)strFullNameFromCoreDB :(NSManagedObject*)dictData;
-(void)DeleteFromCoreDataMechancialWOs;
-(BOOL)isGenerateWoMechanical;
-(NSArray*)arrPO :(NSDictionary*)dictData;
-(NSDate*)getDueDateNeww:(NSString*)strDueDate;
-(NSMutableDictionary*)dateStartEndSales;

-(void)updateWdoNpmaIsMailSent :(NSString*)strWoIdd;
-(NSString *)getOnlyDateNeww :(NSDate*)strDateToConvert;
-(void)updateWoIsMailSent :(NSString*)strWoIdd :(NSString*)strTrueFalse;
-(void)updateLeadStatus:(NSString *)strLeadId;

-(BOOL)isEaMasterAvailable;
-(NSMutableDictionary*) dictOfDeviceInfo;
-(void)fetchWorkOrderAndSaveOrUpdateData :(NSString*)strWorkOrderId :(NSString*)strKeyName :(NSString*)strValue;
-(float)heightOfAttrbuitedText:(NSAttributedString *)attrStr width:(CGFloat )width;
-(NSDictionary*)convertNullArrayNew :(NSDictionary*)dictLocalResponse;
@end
