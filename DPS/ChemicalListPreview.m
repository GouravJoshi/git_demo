//
//  ChemicalListPreview.m
//  DPS
//
//  Created by Rakesh Jain on 21/02/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "ChemicalListPreview.h"
#import "InvoiceAppointmentView.h"
#import "ServiceDetailAppointmentView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "ServiceSignViewController.h"
#import "RecordAudioView.h"
#import "ImagePreviewGeneralInfoAppointmentView.h"
#import "BeginAuditView.h"
#import "ServiceSendMailViewController.h"
#import "ServiceAutoModifyDate.h"
#import "ServiceAutoModifyDate+CoreDataProperties.h"
#import "ServiceDocumentsViewController.h"
//#import "ImageDetailsServiceAuto.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
@interface ChemicalListPreview ()
{
    Global *global;
    UIView *MainViewForm,*ViewFormSections,*viewForm,*viewSection;
    NSMutableArray *arrayOfButtonsMain;
    NSMutableArray *arrOFImagesName;
    NSString *strEmpID,*strCompanyKey,*strCompanyId,*strServiceUrlMain,*strUserName;
    
    //Change for Chemical List
    
    UIView *MainViewFormChemical,*ViewFormSectionsChemical,*viewFormChemical,*viewSectionChemical;
    UIButton *BtnMainViewChemical;
    
    //New change on 24 august 2016 for dynamic form
    NSMutableArray *arrayViewsChemical,*arrayProductName,*arrayOfUnit,*arrayOfTarget;
    NSMutableArray *arrayOfButtonsMainChemical;
    NSMutableArray *arrOfHeightsViewsChemical;
    NSMutableArray *arrOfYAxisViewsChemical;
    NSMutableArray *arrOfButtonImagesChemical;
    NSMutableArray *arrOfIndexesChemical;
    NSMutableArray *arrOfControlssMainChemical,*arrOfAfterImagesAll;
    NSString *strWorkOrderId,*strGlobalTechSignImage,*strGlobalCustSignImage,*strGlobalAudio,*strGlobalWorkOrderStatus;
    BOOL isCustomerImage,isCustomerNotPrsent,yesEditedSomething;
}
@end

@implementation ChemicalListPreview

- (void)viewDidLoad
{
    
    yesEditedSomething=NO;
    
    [super viewDidLoad];
    
    isCustomerNotPrsent=NO;
    
    global = [[Global alloc] init];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    
    arrayOfButtonsMain=[[NSMutableArray alloc]init];
    arrOFImagesName=[[NSMutableArray alloc] init];
    arrOfAfterImagesAll=[[NSMutableArray alloc] init];
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    
    _lbl_AccountNos.text=[defsLead valueForKey:@"lblName"];
    
    NSString *strThirdPartyAccountNo=[defsLead valueForKey:@"lblThirdPartyAccountNo"];
    if (strThirdPartyAccountNo.length>0) {
        _lbl_AccountNos.text=strThirdPartyAccountNo;
    }

     [self dynamicChemicalList];

}

-(void)viewWillAppear:(BOOL)animated{
    
    //yah par condition check lagani hai vapis se reload hoga data to yad se

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//============================================================================
#pragma mark- setDynamicData Method
//============================================================================


-(void)dynamicChemicalList
{
    
    NSMutableArray *arrOfUnitMaster=[[NSMutableArray alloc]init];  //=[_dictOfChemicalList valueForKey:@"UnitMasterInfo"];
    NSMutableArray *arrOfTargetMaster=[[NSMutableArray alloc]init];//[_dictOfChemicalList valueForKey:@"TargetMasterInfo"];
    NSMutableArray *arrOfAppMethodInfoMaster=[[NSMutableArray alloc]init];//[_dictOfChemicalList valueForKey:@"AppMethodInfo"];
    
    
    NSMutableArray *arrOfCombineChemicalList=[[NSMutableArray alloc]init];
    
    [arrOfCombineChemicalList addObjectsFromArray:_arrChemicalList];
    [arrOfCombineChemicalList addObjectsFromArray:_arrOfChemicalListOther];
    
    NSArray *arrCombined=(NSArray*)arrOfCombineChemicalList;
    
    NSArray *arrOfProductMaster=arrCombined;
    
    for (int k=0; k<arrOfProductMaster.count; k++) {
        
        NSDictionary *dictData=arrOfProductMaster[k];
        
        [arrOfUnitMaster addObject:[dictData valueForKey:@"UnitId"]];
        [arrOfTargetMaster addObject:[dictData valueForKey:@"TargetId"]];
        [arrOfAppMethodInfoMaster addObject:[dictData valueForKey:@"ApplicationMethodId"]];
        
    }
    
    CGFloat scrollViewHeight=0.0;
    
    arrOfIndexesChemical =[[NSMutableArray alloc]init];
    arrOfControlssMainChemical =[[NSMutableArray alloc]init];
    
    CGFloat yXisForBtnCheckUncheck=0.0;
    if (ViewFormSections.frame.size.height==0) {
        
        //yXisForBtnCheckUncheck=_view_CustomerInfo.frame.origin.y+_view_CustomerInfo.frame.size.height+20;
        
        
    } else {
        
      //  yXisForBtnCheckUncheck=_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+MainViewForm.frame.size.height+20+50;
        
    }
    scrollViewHeight=MainViewForm.frame.size.height;
    
    for (int i=0; i<arrOfProductMaster.count; i++)
    {
        NSDictionary *dictProductMasterDetail=[arrOfProductMaster objectAtIndex:i];
        if (!MainViewFormChemical.frame.size.height) {
            BtnMainViewChemical=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck, 30, 30)];
        }
        else{
            BtnMainViewChemical=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck, 30, 30)];
        }
        
        BtnMainViewChemical.tag=i+5555;
        //  [BtnMainViewChemical addTarget:self action:@selector(actionOpenCloseChemicalList:) forControlEvents:UIControlEventTouchDown];
        // BtnMainViewChemical.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        
        NSString *strProductId=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductId"]];
        BOOL isMatchedProductId;
        isMatchedProductId=NO;
        
        [BtnMainViewChemical setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
        [_scrollVieww addSubview:BtnMainViewChemical];
        
        [arrayOfButtonsMainChemical addObject:BtnMainViewChemical];
        
        // Form Creation
        MainViewFormChemical=[[UIView alloc]init];
        
        MainViewFormChemical.backgroundColor=[UIColor clearColor];
        
        MainViewFormChemical.frame=CGRectMake(0, yXisForBtnCheckUncheck+BtnMainViewChemical.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 150);
        
        MainViewFormChemical.tag=i+9999;
        MainViewFormChemical.backgroundColor=[UIColor clearColor];
        
        [_scrollVieww addSubview:MainViewFormChemical];
        
        UILabel *lblTitleSection=[[UILabel alloc]init];
        lblTitleSection.backgroundColor=[UIColor lightGrayColor];
        lblTitleSection.layer.borderWidth = 1.5f;
        lblTitleSection.layer.cornerRadius = 4.0f;
        lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
        
        lblTitleSection.frame=CGRectMake(BtnMainViewChemical.frame.origin.x+BtnMainViewChemical.frame.size.width+5, BtnMainViewChemical.frame.origin.y, [UIScreen mainScreen].bounds.size.width-BtnMainViewChemical.frame.origin.x-BtnMainViewChemical.frame.size.width-15, 30);
        
        lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductName"]] ;
        lblTitleSection.textAlignment=NSTextAlignmentLeft;
        
        [arrayProductName addObject:lblTitleSection];
        
        [_scrollVieww addSubview:lblTitleSection];
        
        NSString *strProductAmountt,*strProductPercentt,*strUnitt,*strTargett,*strUnittNameShow,*strTargetNameShow,*strEPAReg,*strMethodInfo,*strMethodInfoToShow;
        
        for (int k=0; k<arrOfProductMaster.count; k++) {
            
            NSDictionary *dictData=arrOfProductMaster[k];
            NSString *strId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductId"]];
            
            if ([strId isEqualToString:strProductId]) {
                
                strProductAmountt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ProductAmount"]];
                strProductPercentt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DefaultPercentage"]];
                strUnitt=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"UnitId"]];
                strTargett=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
                strEPAReg=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EPARegNumber"]];
                strMethodInfo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
                
                if ([strEPAReg isEqualToString:@"(null)"] || [strEPAReg isKindOfClass:[NSNull class]]) {
                    
                    strEPAReg=@"";
                    
                }
                
                for (int j=0; j<arrOfUnitMaster.count; j++) {
                    
                    //NSDictionary *dict=arrOfUnitMaster[j];
                    NSString *strUId=[NSString stringWithFormat:@"%@",arrOfUnitMaster[j]];
                    // NSString *strUnitNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitName"]];
                    if ([strUId isEqualToString:strUnitt]) {
                        
                        strUnittNameShow=strUId;
                        
                    }
                    
                }
                
                for (int j=0; j<arrOfTargetMaster.count; j++) {
                    
                   // NSDictionary *dict=arrOfTargetMaster[j];
                    NSString *strTId=[NSString stringWithFormat:@"%@",arrOfTargetMaster[j]];
                    //  NSString *strTargetNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetName"]];
                    if ([strTId isEqualToString:strTargett]) {
                        
                        strTargetNameShow=strTId;
                        
                    }
                    
                }
                
                
                for (int j=0; j<arrOfAppMethodInfoMaster.count; j++) {
                    
                    //NSDictionary *dict=arrOfAppMethodInfoMaster[j];
                    NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",arrOfAppMethodInfoMaster[j]];
                    //  NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodName"]];
                    if ([strApplicationMethodId isEqualToString:strMethodInfo]) {
                        
                        strMethodInfoToShow=strApplicationMethodId;
                        
                    }
                    
                }
                
                isMatchedProductId=YES;
                break;
                
            }
        }
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt EPA Reg Number -----------------
        //============================================================================
        //============================================================================
        UIView *viewwEPA=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        UITextField *txtFieldEPA=[[UITextField alloc]init];
        txtFieldEPA.placeholder=@"EPAReg #";
        txtFieldEPA.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"EPARegNumber"]];
        txtFieldEPA.keyboardType=UIKeyboardTypeNumberPad;
        txtFieldEPA.layer.borderWidth = 1.5f;
        txtFieldEPA.layer.cornerRadius = 4.0f;
        txtFieldEPA.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldEPA.tag=i+25000;
        txtFieldEPA.inputView=viewwEPA;
        
        txtFieldEPA.delegate=self;
        txtFieldEPA.frame=CGRectMake(BtnMainViewChemical.frame.origin.x-10, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldEPA];
        
        yXisForBtnCheckUncheck=yXisForBtnCheckUncheck+200;
        
        //============================================================================
        //============================================================================
#pragma mark----------------------Txt Percentage-----------------
        //============================================================================
        //============================================================================
        
        UITextField *txtFieldPercentage=[[UITextField alloc]init];
        txtFieldPercentage.placeholder=@"Percentage";
        txtFieldPercentage.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"DefaultPercentage"]];
        txtFieldPercentage.keyboardType=UIKeyboardTypeNumberPad;
        txtFieldPercentage.layer.borderWidth = 1.5f;
        txtFieldPercentage.layer.cornerRadius = 4.0f;
        txtFieldPercentage.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldPercentage.tag=i+30000;
        txtFieldPercentage.inputView=viewwEPA;
        txtFieldPercentage.delegate=self;
        txtFieldPercentage.frame=CGRectMake(txtFieldEPA.frame.origin.x+txtFieldEPA.frame.size.width+5, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldPercentage];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt Amount-----------------
        //============================================================================
        //============================================================================
        UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        UITextField *txtFieldAmount=[[UITextField alloc]init];
        txtFieldAmount.placeholder=@"Amount";
        txtFieldAmount.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        txtFieldAmount.keyboardType=UIKeyboardTypeNumberPad;
        txtFieldAmount.layer.borderWidth = 1.5f;
        txtFieldAmount.layer.cornerRadius = 4.0f;
        txtFieldAmount.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldAmount.tag=i+25000;
        txtFieldAmount.inputView=vieww;
        txtFieldAmount.delegate=self;
        txtFieldAmount.frame=CGRectMake(txtFieldEPA.frame.origin.x, txtFieldEPA.frame.origin.y+txtFieldEPA.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldAmount];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Unit-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnUnit=[[UIButton alloc]init];
        
        btnUnit.frame=CGRectMake(txtFieldPercentage.frame.origin.x, txtFieldPercentage.frame.origin.y+txtFieldPercentage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        NSArray *arrOfUnitIdToFetchUnitName=[_dictOfChemicalList valueForKey:@"UnitMasterInfo"];
        
        for (int s=0; s<arrOfUnitIdToFetchUnitName.count; s++) {
            
            NSDictionary *dictData=arrOfUnitIdToFetchUnitName[s];
            
            NSString *strUnitIdToCheck=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"UnitId"]];
            if ([strUnitIdToCheck isEqualToString:strUnittNameShow]) {
                
                strUnittNameShow=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"UnitName"]];
                break;
                
            }
        }
        
        [btnUnit setTitle:strUnittNameShow forState:UIControlStateNormal];
        [btnUnit setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnUnit.tag=i+15000;
        // [btnUnit addTarget:self action:@selector(actionUnitList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfUnit addObject:btnUnit];
        [MainViewFormChemical addSubview:btnUnit];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn App Method-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnAppMethod=[[UIButton alloc]init];
        
        btnAppMethod.frame=CGRectMake(txtFieldAmount.frame.origin.x, txtFieldAmount.frame.origin.y+txtFieldAmount.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        NSArray *arrOfAppMethodIdToFetchAppMethodName=[_dictOfChemicalList valueForKey:@"AppMethodInfo"];
        
        NSMutableArray *arrTempValueAppMethod=[[NSMutableArray alloc]init];
        
        NSArray *objValueControls=[NSArray arrayWithObjects:
                                   @"0",
                                   @"Select Method",
                                   @"00001",nil];
        
        NSArray *objKeyControls=[NSArray arrayWithObjects:
                                 @"ApplicationMethodId",
                                 @"ApplicationMethodName",
                                 @"DepartmentId",nil];
        
        NSDictionary *dictAddExtra=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
        
        [arrTempValueAppMethod addObject:dictAddExtra];
        
        [arrTempValueAppMethod addObjectsFromArray:arrOfAppMethodIdToFetchAppMethodName];
        
        arrOfAppMethodIdToFetchAppMethodName=arrTempValueAppMethod;
        
        for (int s=0; s<arrOfAppMethodIdToFetchAppMethodName.count; s++) {
            
            NSDictionary *dictData=arrOfAppMethodIdToFetchAppMethodName[s];
            
            NSString *strUnitIdToCheck=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
            if ([strUnitIdToCheck isEqualToString:strMethodInfoToShow]) {
                
                strMethodInfoToShow=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodName"]];
                break;
                
            }
        }
        if (strMethodInfoToShow.length==0) {
            strMethodInfoToShow=@"Select Method";
        }
        [btnAppMethod setTitle:strMethodInfoToShow forState:UIControlStateNormal];
        [btnAppMethod setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnAppMethod.tag=i+20000;
        //  [btnTarget addTarget:self action:@selector(actionTargetList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfTarget addObject:btnAppMethod];
        [MainViewFormChemical addSubview:btnAppMethod];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Target-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnTarget=[[UIButton alloc]init];
        
        btnTarget.frame=CGRectMake(btnUnit.frame.origin.x, btnUnit.frame.origin.y+btnUnit.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        NSArray *arrOfTargetIdToFetchTargetName=[_dictOfChemicalList valueForKey:@"TargetMasterInfo"];
        
        for (int s=0; s<arrOfTargetIdToFetchTargetName.count; s++) {
            
            NSDictionary *dictData=arrOfTargetIdToFetchTargetName[s];
            
            NSString *strUnitIdToCheck=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
            if ([strUnitIdToCheck isEqualToString:strTargetNameShow]) {
                
                strTargetNameShow=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetName"]];
                break;
                
            }
        }
        
        [btnTarget setTitle:strTargetNameShow forState:UIControlStateNormal];
        [btnTarget setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnTarget.tag=i+20000;
        //  [btnTarget addTarget:self action:@selector(actionTargetList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfTarget addObject:btnTarget];
        [MainViewFormChemical addSubview:btnTarget];
        
        
        // scrollViewHeight=scrollViewHeight+MainViewFormChemical.frame.size.height;
        
        [arrayViewsChemical addObject:MainViewFormChemical];
        
        CGFloat heightTemp=MainViewFormChemical.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewFormChemical.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        
        
        [arrOfHeightsViewsChemical addObject:strTempHeight];
        
        [arrOfYAxisViewsChemical addObject:strTempYAxis];
        
    }
    _scrollVieww.backgroundColor=[UIColor clearColor];
    
    CGRect frameFor_viewPaymentInfo=CGRectMake(10, 200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20, self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
    frameFor_viewPaymentInfo.origin.x=10;
    frameFor_viewPaymentInfo.origin.y=200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20;
    
    [_viewPaymentInfo setFrame:frameFor_viewPaymentInfo];
    
    [_scrollVieww addSubview:_viewPaymentInfo];
    
    // _scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, [UIScreen mainScreen].bounds.size.width-2,_scrollVieww.frame.size.height+_viewPaymentInfo.frame.size.height+scrollViewHeight+_view_CustomerInfo.frame.size.height+100*arrOfProductMaster.count);
    
    //  [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_scrollVieww.contentSize.height+_viewPaymentInfo.frame.size.height+scrollViewHeight+_view_CustomerInfo.frame.size.height+100*arrOfProductMaster.count)];
    
    
    //    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewPaymentInfo.frame.size.height+scrollViewHeight+_view_CustomerInfo.frame.size.height+200*arrOfProductMaster.count+100+_scrollVieww.contentSize.height+1000)];
    
   
    _const_Scroll_H.constant=_const_Scroll_H.constant+((MainViewFormChemical.frame.size.height+30)*_arrChemicalList.count)-200;
     [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_const_Scroll_H.constant)];
    
}
-(void)hideTextFields{
    UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
    _txt_Customer.inputView=vieww;
    _txt_AccountNo.inputView=vieww;
    _txt_Technician.inputView=vieww;
    _txt_WorkorderNo.inputView=vieww;
    _txt_PrimaryEmail.inputView=vieww;
    _txt_PrimaryPhone.inputView=vieww;
    _txt_SecondaryEmail.inputView=vieww;
    _txt_SecondaryPhone.inputView=vieww;
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
    [_scrollVieww setContentOffset:CGPointMake(0, _viewPaymentInfo.frame.origin.y+textField.frame.origin.y-50) animated:YES];
    
}
-(void)textViewDidEndEditing:(UITextView *)textField
{
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }

    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}


-(void)goingToPreview :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfAfterImagesAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentView
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

- (IBAction)actionOnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}
@end

