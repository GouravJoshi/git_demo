//
//  TermiteSelectStateViewController.m
//  DPS
//
//  Created by Saavan Patidar on 08/11/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "TermiteSelectStateViewController.h"
#import "AllImportsViewController.h"
#import "TermiteInspectionFloridaViewController.h"
#import "DPS-Swift.h"

@interface TermiteSelectStateViewController ()
{
    Global *global;
}
@end

@implementation TermiteSelectStateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    global=[[Global alloc]init];
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    _lblAccoNo.text=[defsLead valueForKey:@"lblName"];
    
    NSString *strThirdPartyAccountNo=[defsLead valueForKey:@"lblThirdPartyAccountNo"];
    if (strThirdPartyAccountNo.length>0) {
        _lblAccoNo.text=strThirdPartyAccountNo;
    }

    [self getClockStatus];

    if ([_strWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [_strWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [_strWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
    {
        if ([_strTermiteStateType isEqualToString:@"Texas"]||[_strTermiteStateType isEqualToString:@"texas"])
        {
            _btnFlorida.enabled=NO;
            
        }
        else if ([_strTermiteStateType isEqualToString:@"Florida"]||[_strTermiteStateType isEqualToString:@"florida"])
        {
            _btnTexas.enabled=NO;

        }
        else
        {
            _btnFlorida.enabled=YES;
            _btnTexas.enabled=YES;
        }
    }

    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 *////////////123

- (IBAction)action_Texas:(id)sender
{
    
    if ([_strWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [_strWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [_strWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
    {
        
    }
    else
    {
        [self UpdateTermiteTypeServiceWorkorder:@"Texas"];
    }
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    TermiteInspectionTexasViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TermiteInspectionTexasViewController"];
    objByProductVC.strWorkOrder=_strWorkOrder;
    objByProductVC.strWorkOrderStatus=_strWorkOrderStatus;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

- (IBAction)action_Florida:(id)sender
{
    
    if ([_strWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [_strWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [_strWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
    {
        
    }
    else
    {
        [self UpdateTermiteTypeServiceWorkorder:@"Florida"];
    }
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    TermiteInspectionFloridaViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TermiteInspectionFloridaViewController"];
    objByProductVC.strWorkOrder=_strWorkOrder;
    objByProductVC.strWorkOrderStatus=_strWorkOrderStatus;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

- (IBAction)action_Back:(id)sender {
    
   /* int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[GeneralInfoAppointmentView class]]) {
            index=k1;
            //break;
        }
    }
    GeneralInfoAppointmentView *myController = (GeneralInfoAppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
    [self.navigationController popToViewController:myController animated:NO];*/
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[GeneralInfoVC class]]) {
            index=k1;
            //break;
        }
    }
    GeneralInfoVC *myController = (GeneralInfoVC *)[self.navigationController.viewControllers objectAtIndex:index];
    [self.navigationController popToViewController:myController animated:NO];
    
}
-(void)UpdateTermiteTypeServiceWorkorder :(NSString *)strTermiteType
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        [matchesWorkOrder setValue:strTermiteType forKey:@"termiteStateType"];
        NSError *error;
        [context save:&error];
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ClockInOutViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
@end
