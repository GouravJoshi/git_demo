//
//  SelectServiceVC.swift
//  DPS
//  peSTream 2020
//  Created by Saavan Patidar on 07/04/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar 2021

import UIKit

class CellSelectService:UITableViewCell{
    
    @IBOutlet weak var lblPrimary: UILabel!
    @IBOutlet weak var lblServicename: UILabel!
    @IBOutlet weak var btnRadioPrimary: UIButton!
    @IBOutlet weak var btnCheckBoxServices: UIButton!
    
}

class SelectServiceVC: UIViewController {

    // MARK: -----------------------------------Variables----------------------------------------
    var arrayServices = NSMutableArray()
    var arrayServicesData = NSMutableArray()
    var strServiceIdPrimary = ""
    var arrayServicesSelected = NSMutableArray()
    var strBranchSysName = ""
    var strFromWhere = ""

    // MARK: -----------------------------------Outlets----------------------------------------

    @IBOutlet weak var tblViewServices: UITableView!
    @IBOutlet weak var searchBarServices: UISearchBar!
    @IBOutlet weak var btnDone: UIButton!

    // MARK: -----------------------------------View's Life Cycle----------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            searchBarServices.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
        } else {
            // Fallback on earlier versions
        }

        tblViewServices.estimatedRowHeight = DeviceType.IS_IPAD ? 200 :150
        tblViewServices.rowHeight = UITableView.automaticDimension;
        tblViewServices.tableFooterView = UIView()

        if let txfSearchField = searchBarServices.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
        }
        searchBarServices.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBarServices.layer.borderWidth = 1
        searchBarServices.layer.opacity = 1.0
        
        btnDone.layer.cornerRadius = btnDone.frame.size.height/2
        btnDone.layer.borderWidth = 0
        
//        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
//        strBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        //arrayServicesData = returnFilteredArray(array: arrOfData)
        arrayServicesData = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchSysName )
        
        //Nilind
        let arrTemp = NSMutableArray()
        
        for item in arrayServicesData
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1"
            {
                arrTemp.add(dict)
            }
        }
        arrayServicesData = arrTemp
        //End
        
        arrayServices = arrayServicesData
        tblViewServices.reloadData()
                
        if arrayServicesSelected.contains(strServiceIdPrimary) {
                        
        }else{
            
            if strServiceIdPrimary.count > 0 {
                
                //arrayServicesSelected.add(strServiceIdPrimary)
                
            }
            
        }
        // Do any additional setup after loading the view.
    }
    
    // MARK: -----------------------------------Functions----------------------------------------

    @objc func actionOnRadioBtn(sender: UIButton!)
    {
        
        let dictData = arrayServices[sender.tag] as! NSDictionary
        
        if "\(dictData.value(forKey: "ServiceId") ?? "")" == strServiceIdPrimary {
            
            strServiceIdPrimary = ""

        } else {
            
            strServiceIdPrimary = "\(dictData.value(forKey: "ServiceId") ?? "")"

        }
        
        if arrayServicesSelected.contains(strServiceIdPrimary) {
            
            
        } else {
            
            if strServiceIdPrimary.count > 0 {
                
                //arrayServicesSelected.add(strServiceIdPrimary)
                
            }
            
        }
        
        
        tblViewServices.reloadData()

    }
    
    @objc func actionOnCheckBoxBtn(sender: UIButton!)
    {
        
        let dictData = arrayServices[sender.tag] as! NSDictionary
        let strLocalServiceId = "\(dictData.value(forKey: "ServiceId") ?? "")"
        
        
        if arrayServicesSelected.count == 0 {
            // Set first service as primary service
            //strServiceIdPrimary = strLocalServiceId
            
        }
        
        if arrayServicesSelected.contains(strLocalServiceId) {
            
            arrayServicesSelected.remove(strLocalServiceId)
            
        } else {
            
            arrayServicesSelected.add(strLocalServiceId)
            
        }
        tblViewServices.reloadData()

    }
    
    // MARK: -----------------------------------Actions----------------------------------------

    @IBAction func action_Back(_ sender: Any) {
        
        self.view.endEditing(true)
        
        self.dismiss(animated: false, completion: nil)

    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if strServiceIdPrimary.count == 0 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please choose primary service", viewcontrol: self)
            
        } else {
            
            // Nsnotification for added services
            
            // remove primary id froma dditional services if exist
            
            if arrayServicesSelected.contains(strServiceIdPrimary) {
                
                arrayServicesSelected.remove(strServiceIdPrimary)
                
            }
            
            var keys = NSArray()
            var values = NSArray()
            keys = ["PrimaryService",
                    "AdditionalService"]
            
            values = [strServiceIdPrimary,
                      arrayServicesSelected]
            
            let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
            
            if strFromWhere == "AddLeadCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedServicesAddLeadCRM_Notification"), object: nil, userInfo: dictToSend as? [AnyHashable : Any])
                
            } else if strFromWhere == "EditOpportunityCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedServicesEditOpportunityCRM_Notification"), object: nil, userInfo: dictToSend as? [AnyHashable : Any])
                
            } else if strFromWhere == "EditLeadCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedServicesEditLeadCRM_Notification"), object: nil, userInfo: dictToSend as? [AnyHashable : Any])
                
            } else if strFromWhere == "ConvertOpportunityCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedServicesConvertOpportunityCRM_Notification"), object: nil, userInfo: dictToSend as? [AnyHashable : Any])
                
            }else {
                
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedServices_Notification"), object: nil, userInfo: dictToSend as? [AnyHashable : Any])

                
            }
            
            self.dismiss(animated: false, completion: nil)
            
        }

    }
    
}

// MARK: --------------------------- Extensions ---------------------------


extension SelectServiceVC:UITableViewDelegate, UITableViewDataSource {
    
    // MARK: UItableView's delegate and datasource
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arrayServices.count
        
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSelectService") as! CellSelectService
       //ServiceId
        let dictData = arrayServices[indexPath.row] as! NSDictionary
        
        cell.lblServicename.text = "\(dictData.value(forKey: "Name") ?? "")"
        
        if strServiceIdPrimary ==  "\(dictData.value(forKey: "ServiceId") ?? "")"{
            
            cell.btnRadioPrimary.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            //cell.lblPrimary.text = "Primary Service"

        } else {
            
            cell.btnRadioPrimary.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            cell.lblPrimary.text = ""

        }
        
        if arrayServicesSelected.contains("\(dictData.value(forKey: "ServiceId") ?? "")") {
            
            cell.btnCheckBoxServices.setImage(UIImage(named: "checked.png"), for: .normal)

        } else {
            
            cell.btnCheckBoxServices.setImage(UIImage(named: "uncheck.png"), for: .normal)

        }
        
        
        cell.btnRadioPrimary.tag = indexPath.row
        cell.btnCheckBoxServices.tag = indexPath.row
        cell.btnRadioPrimary.addTarget(self, action: #selector(actionOnRadioBtn), for: .touchUpInside)
        cell.btnCheckBoxServices.addTarget(self, action: #selector(actionOnCheckBoxBtn), for: .touchUpInside)
        
        return cell
           
       }
       
//       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.deselectRow(at: indexPath, animated: false)
       }
}
extension  SelectServiceVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
        //const_ViewTop_H.constant = 60
        //const_SearchBar_H.constant = 0
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: searchBar.text! as NSString)
    }
    func searchAutocomplete(Searching: NSString) -> Void
    {
        
        let resultPredicate = NSPredicate(format: "Name contains[c] %@", argumentArray: [Searching])
        if !(Searching.length == 0)
        {
            let arrayfilter = (self.arrayServicesData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arrayServices = NSMutableArray()
            self.arrayServices = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tblViewServices.reloadData()
        }
        else
        {
            self.arrayServices = NSMutableArray()
            self.arrayServices = self.arrayServicesData.mutableCopy() as! NSMutableArray
            self.tblViewServices.reloadData()
            self.view.endEditing(true)
            searchBarServices.text = ""
        }
        if(arrayServices.count == 0){
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty//if searchText.count == 0
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                       
                       self.view.endEditing(true)
                       
                   })
            self.searchAutocomplete(Searching: "")
            searchBar.text = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                searchBar.resignFirstResponder()
            }
            
        }
    }
}
extension SelectServiceVC : UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Text field did end calls")
    }
}
