//
//  ScheduleOpportunityVC.swift
//  DPS
//
//  Created by Saavan Patidar on 25/06/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ScheduleOpportunityVC: UIViewController {

    // MARK: - ----------------------------------- Global Variables -----------------------------------

    var loader = UIAlertController()
    var dictOppData = NSMutableDictionary()
    var strFieldSalesPersonId = ""
    var dictLoginData = NSDictionary()
    var strRefType = enumRefTypeLead
    var globalAudioData  = Data ()
    let dispatchGroup = DispatchGroup()
    var imageCount = 0
    var strGlobalNameAudio = ""
    var arrOfImages = NSMutableArray()
    var strRefId = ""
    var isScheduleNow = false
    var isCreateWebLeadOrNot = false
    var isDefaultValueForWebLead = false
    var isRadioButtonClicked = false
    
    var aryTimeRange = NSArray() ,arySelectedRange = NSMutableArray()
    //MARK:
    //MARK:---------IBOutlet-----------
    @IBOutlet weak var tf_Date: ACFloatingTextField!
    @IBOutlet weak var tf_Time: ACFloatingTextField!
    @IBOutlet weak var tf_Range: ACFloatingTextField!
    @IBOutlet weak var height_tf_Range: NSLayoutConstraint!
    @IBOutlet weak var btn_Specific: UIButton!
    @IBOutlet weak var btn_TimeRange: UIButton!
    

    @IBOutlet weak var btnScheduleLater: UIButton!
    @IBOutlet weak var btnScheduleNow: UIButton!
    
    @IBOutlet weak var btnThirdParty: UIButton!
    
    @IBOutlet weak var btnYes: UIButton!
    
    @IBOutlet weak var btnNo: UIButton!
    
    // MARK: - ----------------------------------- View's LifeCycle -----------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        if(nsud.value(forKey: "MasterSalesTimeRange") != nil){
            if(nsud.value(forKey: "MasterSalesTimeRange") is NSArray){
                self.aryTimeRange = nsud.value(forKey: "MasterSalesTimeRange") as! NSArray
            }
        }
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
        
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        btnScheduleLater.layer.cornerRadius = btnScheduleLater.frame.size.height/2
        btnScheduleLater.layer.borderWidth = 0
        
        btnScheduleNow.layer.cornerRadius = btnScheduleNow.frame.size.height/2
        btnScheduleNow.layer.borderWidth = 0
        
        if tf_Date.text?.count == 0 {
            tf_Date.text = Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
        }
        if tf_Time.text?.count == 0 {
            tf_Time.text = Global().strGetCurrentDate(inFormat: "hh:mm a")
        }
        
        
        //Schedule Third party Button
        if isScheduleThirdParty() {
            btnThirdParty.isHidden = false
        }else{
            btnThirdParty.isHidden = true
        }
        btnThirdParty.setImage(UIImage(named: "check_box_1New.png"), for: .normal)

        createWebLeadByConfiguration()
        // Do any additional setup after loading the view.
    }
    // MARK: - ----------------------------------- Date and Time Picker -----------------------------------

    func createWebLeadByConfiguration()  {
        
        if "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.CreateWebLeadOrNot") ?? "")" == "1"
        {
            isCreateWebLeadOrNot = true
        }
        
        if "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.DefaultValueForWebLead") ?? "")" == "1"
        {
            isDefaultValueForWebLead = true
        }
        
        if "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.DefaultValueForWebLead") ?? "")" == "1"
        {
            btnYes.setImage(UIImage(named: "redio_2"), for: .normal)
            btnNo.setImage(UIImage(named: "redio_1"), for: .normal)
            isRadioButtonClicked = true
        }
        else if "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.DefaultValueForWebLead") ?? "")" == "0"
        {
            btnYes.setImage(UIImage(named: "redio_1"), for: .normal)
            btnNo.setImage(UIImage(named: "redio_2"), for: .normal)
            isRadioButtonClicked = true
        }
        else
        {
            btnYes.setImage(UIImage(named: "redio_1"), for: .normal)
            btnNo.setImage(UIImage(named: "redio_1"), for: .normal)
        }
    }

    
    func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    func getDateFormatted(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    // MARK: - ----------------------------------- Functions -----------------------------------
    
    func AddLeadOpportunityAPI(dictToSend : NSDictionary) {
        
        if(isInternetAvailable()){

            var jsonString = String()
            
            if(JSONSerialization.isValidJSONObject(dictToSend) == true)
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                
                print(jsonString)
                
            }
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddLeadOpportunity
            
            let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            
            let strTypee = "SearchCrmContactsNew"
            
            Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in

                if(success)
                {
                    
                    let dictResponse = (response as NSDictionary?)!
                    
                    if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                        
                        let arrOfKey = dictResponse.allKeys as NSArray
                        
                        if arrOfKey.contains("WebLeadId") || arrOfKey.contains("OpportunityId") {

                            // CrmCompanyId es mai strRefId aa ri hai jo bhejna hai wapis
                            
                            if (self.strGlobalNameAudio.count == 0) && (self.arrOfImages.count == 0) {
                                
                                self.responseOnUpload()
                                
                            } else {
                                
                                // Upload all images and audio here
                                
                                if self.arrOfImages.count > 0 {
                                    
                                    self.imageCount = 0
                                    
                                    if self.strRefType == "WebLead" {

                                        self.strRefId = "\((dictResponse as NSDictionary?)!.value(forKey: "WebLeadId")!)"

                                    }else{
                                        
                                        self.strRefId = "\((dictResponse as NSDictionary?)!.value(forKey: "OpportunityId")!)"
                                        
                                    }
                                    
                                    self.dispatchGroup.enter()

                                    self.callToUploadImage()
                                    
                                }
                                
                                if self.strGlobalNameAudio.count > 0 {
                                    
                                    if self.strRefType == "WebLead" {

                                        self.strRefId = "\((dictResponse as NSDictionary?)!.value(forKey: "WebLeadId")!)"

                                    }else{
                                        
                                        self.strRefId = "\((dictResponse as NSDictionary?)!.value(forKey: "OpportunityId")!)"
                                        
                                    }
                                    
                                    self.dispatchGroup.enter()
                                    
                                    var  strAudioName = ""
                                    if(nsud.value(forKey: "AudioNameDocument") != nil){
                                        strAudioName = "\(nsud.value(forKey: "AudioNameDocument")!)"
                                    }
                                    let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                                    let localUrl = documentDirectory!.appendingPathComponent(strAudioName)

                                    if FileManager.default.fileExists(atPath: localUrl.path){
                                        if let cert = NSData(contentsOfFile: localUrl.path) {
                                            self.globalAudioData = cert as Data
                                            self.strGlobalNameAudio = strAudioName
                                        }
                                    }
                                    
                                    self.uploadAudioFile(strMediaType: "Audio", strRefIdd: self.strRefId, strMediaName: self.strGlobalNameAudio, mediaData: self.globalAudioData)
                                    
                                }
                                
                                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                                    
                                    self.responseOnUpload()

                                })
                                
                            }
                            
                        }else {
                            
                            self.loader.dismiss(animated: false) {

                                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                            }
                            
                        }
                        
                    }else {
                        
                        self.loader.dismiss(animated: false) {

                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                        }
                        
                    }
                    
                }else {
                    
                    self.loader.dismiss(animated: false) {

                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                    }
                    
                }
                
            }
            
        }else{
            
            self.loader.dismiss(animated: false) {

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

            }
            
        }
        
    }
    
    func responseOnUpload() {
        
        self.view.endEditing(true)

        UserDefaults.standard.set(true, forKey: "isLeadOppAddedUpdated")

        self.loader.dismiss(animated: false) {
            
            if self.isScheduleNow {

                let alertCOntroller = UIAlertController(title: Info, message: "Opportunity added Successfully.", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "Go to Appointments", style: .default, handler: { (action) in
                    if(DeviceType.IS_IPAD){
                        let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                        
                        if strAppointmentFlow == "New" {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "Appointment_iPAD",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                            objByProductVC?.isCallDelay = "yes"
                            objByProductVC?.strTodayAll = "Today"
                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            
                        } else {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "MainiPad",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            
                        }
                    }else{
                        
                        let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                        
                        if strAppointmentFlow == "New" {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "Appointment",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                            objByProductVC?.isCallDelay = "yes"
                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            
                        } else {
                            
                        let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
                        self.navigationController?.pushViewController(controller, animated: false)
                            
                        }
                        
                    }
                })
                
                let alertActionNo = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                    nsud.set(true, forKey: "fromScheduleOppVC")
                    nsud.synchronize()
                    self.navigationController?.popViewController(animated: false)
                })
                alertCOntroller.addAction(alertActionNo)
                alertCOntroller.addAction(alertAction)
                self.present(alertCOntroller, animated: true, completion: nil)
                
            }else{
                
                let alertCOntroller = UIAlertController(title: Info, message: "Opportunity added Successfully.", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    nsud.set(true, forKey: "fromScheduleOppVC")
                    nsud.synchronize()
                    self.navigationController?.popViewController(animated: false)
                })
                alertCOntroller.addAction(alertAction)
                self.present(alertCOntroller, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    func callToUploadImage() {
        
        if (self.arrOfImages.count > 0) && (self.arrOfImages.count > self.imageCount){
                        
            let imagee = self.arrOfImages[self.imageCount] as? UIImage

            let dataToSend = imagee?.jpegData(compressionQuality: 1.0)
            
            let imageNamee = "img" + "\(self.imageCount)" + "\(getUniqueValueForId())" + ".png"
            
            self.uploadImageFile(strMediaType: "Image", strRefIdd: strRefId, strMediaName: imageNamee, mediaData: dataToSend!)
            
        }
        
    }
    
        func uploadImageFile(strMediaType : String , strRefIdd : String , strMediaName : String , mediaData : Data) {
        
             let dictSendDocument =
                 ["RefId": strRefIdd,
                 "RefType": strRefType,
                 "MediaType": strMediaType,
                 "Title": strMediaType,
                 "CompanyKey": Global().getCompanyKey(),
                 "WebLeadDocumentId": "0",
                 "Createdby": Global().getEmployeeId()]
             
                      let encoder = JSONEncoder()
                      if let jsonData = try? encoder.encode(dictSendDocument) {
                          if let jsonString = String(data: jsonData, encoding: .utf8) {
                              print(jsonString)
                             let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                             let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddDocumentsByRefIdAndRefType
                                     WebService.getRequestWithHeadersWithMultipleImage(JsonData: jsonString,JsonDataKey: "WebLeadDocDc", data: mediaData, strDataName : strMediaName, url: strURL) { (responce, status) in
                                              if(status)
                                              {

                                                //self.dispatchGroup.leave()

                                                 if(responce.value(forKey: "data")is NSDictionary)
                                                 {
                                                     
                                                    // Handle Success Response Here
                                                    
                                                    /*let alert = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertController.Style.alert)
                                                     
                                                       // add the actions (buttons)
                                                       alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                                           self.dismiss(animated: false)
                                                       }))
                                                       self.present(alert, animated: true, completion: nil)
                                                    */
                                                    
                                                 }
                                                 else
                                                 {
                                                     //showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "String", viewcontrol: self)
                                                 }
                                              }else{
    //                                            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Error", viewcontrol: self)
                      }
                                        
                                        self.imageCount = self.imageCount + 1

                                        if (self.arrOfImages.count > 0) && (self.arrOfImages.count > self.imageCount){
                                            
                                            self.callToUploadImage()
                                            
                                        }else{
                                            
                                            self.dispatchGroup.leave()
                                            
                                        }
                                        
                   }
                }
             }
         }
        
        func uploadAudioFile(strMediaType : String , strRefIdd : String , strMediaName : String , mediaData : Data) {
        
             let dictSendDocument =
                 ["RefId": strRefIdd,
                 "RefType": strRefType,
                 "MediaType": strMediaType,
                 "Title": strMediaType,
                 "CompanyKey": Global().getCompanyKey(),
                 "WebLeadDocumentId": "0",
                 "Createdby": Global().getEmployeeId()]
             
                      let encoder = JSONEncoder()
                      if let jsonData = try? encoder.encode(dictSendDocument) {
                          if let jsonString = String(data: jsonData, encoding: .utf8) {
                              print(jsonString)
                             let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                             let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddDocumentsByRefIdAndRefType
                                     WebService.getRequestWithHeadersWithMultipleImage(JsonData: jsonString,JsonDataKey: "WebLeadDocDc", data: mediaData, strDataName : strMediaName, url: strURL) { (responce, status) in
                                              if(status)
                                              {

                                                self.dispatchGroup.leave()

                                                 if(responce.value(forKey: "data")is NSDictionary)
                                                 {
                                                     
                                                    // Handle Success Response Here
                                                    
                                                    /*let alert = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertController.Style.alert)
                                                     
                                                       // add the actions (buttons)
                                                       alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                                           self.dismiss(animated: false)
                                                       }))
                                                       self.present(alert, animated: true, completion: nil)
                                                    */
                                                    
                                                 }
                                                 else
                                                 {
                                                     //showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "String", viewcontrol: self)
                                                 }
                                              }else{
                                                //showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Error", viewcontrol: self)
                      }
                   }
                }
             }
         }
    
    
    
    // MARK: - ----------------------------------- Actions -----------------------------------
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func action_OnDate(_ sender: UIButton) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Date.text?.count)! > 0 ? tf_Date.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.date = fromDate
        datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Service Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Date.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
      
    }
    @IBAction func action_OnTime(_ sender: UIButton) {
        self.view.endEditing(true)
   
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Time.text?.count)! > 0 ? tf_Time.text! : result))!
        
        let datePicker = UIDatePicker()
       // datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.time
        datePicker.date = fromDate
      //  datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)

        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate, Picker: datePicker, view: self.view ,strTitle : "Select Service Time")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Time.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func action_OnSelectRange(_ sender: Any) {
        self.view.endEditing(true)
        if(aryTimeRange.count != 0){
            goToNewSalesSelectionVC(arrItem: aryTimeRange.mutableCopy()as! NSMutableArray, arrSelectedItem: self.arySelectedRange, tag: 1, titleHeader: "Time Range", isMultiSelection: false, ShowNameKey: "StartInterval,EndInterval")
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
    }

    @IBAction func action_OnSpecific(_ sender: Any) {
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
        arySelectedRange = NSMutableArray()
        tf_Range.text = ""
        tf_Range.tag = 0
    }
    @IBAction func action_OnTimeRange(_ sender: Any) {
        height_tf_Range.constant = DeviceType.IS_IPAD ? 50.0 : 44.0
        btn_Specific.setImage(UIImage(named: "redio_1"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_2"), for: .normal)

    }
    
    @IBAction func action_OnThirdParty(_ sender: UIButton) {
        self.view.endEditing(true)
        //check_box_1New uncheck
        if sender.currentImage == UIImage(named: "check_box_1New.png") {
            btnThirdParty.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        }else{
            btnThirdParty.setImage(UIImage(named: "check_box_1New.png"), for: .normal)

        }
    }
    
    @IBAction func actionOnScheduleLater(_ sender: Any)
    {
        if isInternetAvailable() {
            
            if isCreateWebLeadOrNot
            {
                if isRadioButtonClicked
                {
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)
                    isScheduleNow = false
                    // Schedule ThirdParty
                    let isThirdParty = btnThirdParty.currentImage == UIImage(named: "check_box_2New.png") ? "true" : "false"
                    dictOppData.setValue(isThirdParty, forKey: "CreateBlankScheduleInPP")
                    
                    if btnYes.currentImage == UIImage(named: "redio_2")
                    {
                        dictOppData.setValue("true", forKey: "CreateWebLeadWithOpportunity")
                    }
                    else
                    {
                        dictOppData.setValue(false, forKey: "CreateWebLeadWithOpportunity")
                    }
                    self.AddLeadOpportunityAPI(dictToSend: dictOppData)
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select an option to create lead", viewcontrol: self)
                }
            }
            else
            {
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                isScheduleNow = false
                // Schedule ThirdParty
                let isThirdParty = btnThirdParty.currentImage == UIImage(named: "check_box_2New.png") ? "true" : "false"
                dictOppData.setValue(isThirdParty, forKey: "CreateBlankScheduleInPP")
                if btnYes.currentImage == UIImage(named: "redio_2")
                {
                    dictOppData.setValue("true", forKey: "CreateWebLeadWithOpportunity")
                }
                else
                {
                    dictOppData.setValue(false, forKey: "CreateWebLeadWithOpportunity")
                }
                self.AddLeadOpportunityAPI(dictToSend: dictOppData)
            }

            
        } else {
            
            self.loader.dismiss(animated: false) {

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

            }
            
        }
    }
    
    @IBAction func actionOnScheduleNow(_ sender: Any)
    {
        if isInternetAvailable() {
            
            if tf_Date.text?.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select date to schedule now.", viewcontrol: self)
                
            }else if tf_Time.text?.count == 0{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select time to schedule now.", viewcontrol: self)

            } else if (btn_TimeRange.currentImage == UIImage(named: "redio_2") && tf_Range.text == "") {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select time range to schedule now.", viewcontrol: self)
                }else{
                    
                    if isCreateWebLeadOrNot
                    {
                        if isRadioButtonClicked
                        {
                            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                            self.present(loader, animated: false, completion: nil)
                            
                            isScheduleNow = true
                            
                            dictOppData.setValue(getDateFormatted(strTime: (tf_Date.text!)), forKey: "ScheduleDate")
                            dictOppData.setValue(getTimeWithouAMPM(strTime: (tf_Time.text!)), forKey: "ScheduleTime")
                            dictOppData.setValue(strFieldSalesPersonId, forKey: "FieldSalesPerson")
                            var strType = "Specific" , strRangeID = ""
                            if(tf_Range.tag != 0 && tf_Range.text != ""){
                                strType = "Range"
                                strRangeID = "\(tf_Range.tag)"
                            }
                            dictOppData.setValue(strType, forKey: "ScheduleTimeType")
                            dictOppData.setValue(strRangeID, forKey: "RangeOfTimeId")
                            
                            
                            //Schedule Third party
                            let isThirdParty = btnThirdParty.currentImage == UIImage(named: "check_box_2New.png") ? "true" : "false"
                            dictOppData.setValue(isThirdParty, forKey: "CreateBlankScheduleInPP")
                            
                            if btnYes.currentImage == UIImage(named: "redio_2")
                            {
                                dictOppData.setValue("true", forKey: "CreateWebLeadWithOpportunity")
                            }
                            else
                            {
                                dictOppData.setValue(false, forKey: "CreateWebLeadWithOpportunity")
                            }

                            DispatchQueue.main.asyncAfter(deadline: .now() + 1 )  {
                                self.AddLeadOpportunityAPI(dictToSend: self.dictOppData)
                            }
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select an option to create lead", viewcontrol: self)

                        }
                    }
                    else
                    {
                        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                        self.present(loader, animated: false, completion: nil)
                        
                        isScheduleNow = true
                        
                        dictOppData.setValue(getDateFormatted(strTime: (tf_Date.text!)), forKey: "ScheduleDate")
                        dictOppData.setValue(getTimeWithouAMPM(strTime: (tf_Time.text!)), forKey: "ScheduleTime")
                        dictOppData.setValue(strFieldSalesPersonId, forKey: "FieldSalesPerson")
                        var strType = "Specific" , strRangeID = ""
                        if(tf_Range.tag != 0 && tf_Range.text != ""){
                            strType = "Range"
                            strRangeID = "\(tf_Range.tag)"
                        }
                        dictOppData.setValue(strType, forKey: "ScheduleTimeType")
                        dictOppData.setValue(strRangeID, forKey: "RangeOfTimeId")
                        
                        if btnYes.currentImage == UIImage(named: "redio_2")
                        {
                            dictOppData.setValue("true", forKey: "CreateWebLeadWithOpportunity")
                        }
                        else
                        {
                            dictOppData.setValue(false, forKey: "CreateWebLeadWithOpportunity")
                        }
                        
                        //Schedule Third party
                        let isThirdParty = btnThirdParty.currentImage == UIImage(named: "check_box_2New.png") ? "true" : "false"
                        dictOppData.setValue(isThirdParty, forKey: "CreateBlankScheduleInPP")

                        DispatchQueue.main.asyncAfter(deadline: .now() + 1 )  {
                            self.AddLeadOpportunityAPI(dictToSend: self.dictOppData)
                        }
                    }

                }

  
            
        } else {
            
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
    }
 
    @IBAction func actionOnBtnYes(_ sender: Any) {
        
        isRadioButtonClicked = true
        btnYes.setImage(UIImage(named: "redio_2"), for: .normal)
        btnNo.setImage(UIImage(named: "redio_1"), for: .normal)
    }
    
    @IBAction func actionOnBtnNo(_ sender: Any) {
        
        isRadioButtonClicked = true
        btnYes.setImage(UIImage(named: "redio_1"), for: .normal)
        btnNo.setImage(UIImage(named: "redio_2"), for: .normal)
    }
    
    
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.delegate = self
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
}
// MARK: -
// MARK: - SalesNew_SelectionProtocol


extension ScheduleOpportunityVC: SalesNew_SelectionProtocol
{
    func didSelect(aryData: NSMutableArray, tag: Int) {
        
     }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        print(dictData)
        if(dictData.count != 0){
            self.arySelectedRange = NSMutableArray()
            self.arySelectedRange.add(dictData)
            let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
            let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
            let strrenge = StartInterval + " - " + EndInterval
            tf_Range.text = strrenge
            tf_Time.text = StartInterval
            
            tf_Range.tag = Int("\(dictData.value(forKey: "RangeofTimeId")!)")!
            
        }else{
            tf_Range.text = ""
            tf_Range.tag = 0
        }
    }
}
