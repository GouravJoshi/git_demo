//  peSTream 2020
//  AddLeadProspectVC.swift
//  peSTream 2020
//  Created by Saavan Patidar on 06/04/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan patdiar
//  Saavan 2021


import UIKit

protocol AddLeadAssociateProtocol : class
{
    
    func getDataAddLeadAssociateProtocol(notification : NSDictionary ,tag : String)
    
}

class LeadProspectImgCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
}

class AddLeadProspectVC: UIViewController {

    //MARK:---------------Variables--
    var strAssignedToId = ""
    
    var strUrgencyId = ""
    var strAccountId = ""
    var strCrmContactId = ""
    var strCrmCompanyId = ""
    var strBtnTag = ""
    var dictLoginData = NSDictionary()
    var arrSelectedSource = NSMutableArray()
    var arrSourceId = NSMutableArray()
    var arrAddress = NSMutableArray()
    var strGlobalNameAudio = ""
    var imagePicker = UIImagePickerController()
    var arrOfImages = NSMutableArray()
    var arrOfAdditionalServiceSelected = NSMutableArray()
    var strPrimaryService = ""
    var arrOfAdditionalService = NSArray()
    @objc var dictLeadDetailsData = NSMutableDictionary()
    var dictScheduleNow = NSDictionary()
    var dictAddress = NSMutableDictionary()
    var strRefType = ""
    var globalAudioData  = Data ()
    let dispatchGroup = DispatchGroup()
    var imageCount = 0
    var strRefId = ""
    var loader = UIAlertController()
    var dictOfContactAddress = NSDictionary()
    var dictOfCompanyAddress = NSDictionary()
    var arrOfAccountAddress = NSArray()
    var aryPropertySelected = NSMutableArray()
    var isSourceCodeRequired = Bool()
    var isSelfAssign = Bool()

    // MARK: - ----------- Google Address Code ----------------
    
    @IBOutlet weak var tableView: UITableView!
    
    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var radius: Double = 0.0
    private var strictBounds: Bool = false
    var txtAddressMaxY = CGFloat()
    var imgPoweredBy = UIImageView()
    var strStateId = ""
    var strCityName = ""
    var strAddressLine1 = ""
    var strZipCode = ""
    var strCustomerAddressId = ""
    
    private var places = [Place]() {
        didSet { tableView.reloadData() }
    }
    
    //MARK: --------Outlets---------------
       //MARK:
    @IBOutlet weak var btnExistingCustomer: UIButton!
    @IBOutlet weak var btnNewCustomer: UIButton!
    
    @IBOutlet weak var imgViewCollection: UICollectionView!
    
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnScheduleNow: UIButton!
    @IBOutlet weak var btnSelf: UIButton!
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var txtFldType: ACFloatingTextField!
    @IBOutlet weak var txtFldAccount: ACFloatingTextField!
    @IBOutlet weak var txtFldContact: ACFloatingTextField!
    @IBOutlet weak var txtFldCompany: ACFloatingTextField!
    @IBOutlet weak var txtFldTitle: ACFloatingTextField!
    @IBOutlet weak var txtFldAddress: ACFloatingTextField!
    @IBOutlet weak var txtFldSource: ACFloatingTextField!
    @IBOutlet weak var txtFldAssignTo: ACFloatingTextField!
    @IBOutlet weak var txtFldService: ACFloatingTextField!
    @IBOutlet weak var txtFldUrgency: ACFloatingTextField!
    @IBOutlet weak var txtFldComment: ACFloatingTextField!
    
    @IBOutlet weak var txtfldPropertyType: ACFloatingTextField!
    
    @IBOutlet weak var heightConstraint_ViewCompany : NSLayoutConstraint!
    @IBOutlet weak var heightConstraint_ViewAccount : NSLayoutConstraint!
    @IBOutlet weak var topConstraint_ViewCompany : NSLayoutConstraint!
    @IBOutlet weak var topConstraint_ViewAccount : NSLayoutConstraint!

    
    @IBOutlet weak var txtBranch: ACFloatingTextField!

    //Deepak.s
    @IBOutlet weak var btnLead: UIButton!
    @IBOutlet weak var btnOpportunity: UIButton!
    var isLead: Bool = true
    @IBOutlet weak var viewForUserDefineField: UIView!
    @IBOutlet weak var heightUserDefine: NSLayoutConstraint!
    @IBOutlet weak var height_superView: NSLayoutConstraint!
    //***********
    //Deepak.s
    @IBOutlet weak var viewExistingCustomer: UIView!
    @IBOutlet weak var viewNewCustomer: UIView!
    @IBOutlet weak var btnExisting: UIButton!
    @IBOutlet weak var btnNew: UIButton!
    
    
    // MARK: -----------------------------------View's Life Cycle----------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.txtFldAddress.isUserInteractionEnabled = true
        
//        btnSubmit.layer.cornerRadius = btnSubmit.frame.size.height/2
//        btnSubmit.layer.borderWidth = 0
        
        btnScheduleNow.layer.cornerRadius = btnScheduleNow.frame.size.height/2
        btnScheduleNow.layer.borderWidth = 0
        
        btnSelf.setImage(UIImage(named: "uncheck.png"), for: .normal)
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        
        if "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsWebLeadSourceRequired") ?? "")" == "1" || "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsWebLeadSourceRequired") ?? "")".caseInsensitiveCompare("true") == .orderedSame
        {
            isSourceCodeRequired = true
        }
        else
        {
            isSourceCodeRequired = false
        }
        
        if "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsSelfAssign") ?? "")" == "1" || "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsWebLeadSourceRequired") ?? "")".caseInsensitiveCompare("true") == .orderedSame
        {
            isSelfAssign = true
        }
        else
        {
            isSelfAssign = false
        }
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "SelectedServicesAddLeadCRM_Notification"),
                                               object: nil,
                                               queue: nil,
                                               using:catchNotification)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "ScheduleNow_Notification"),
                                               object: nil,
                                               queue: nil,
                                               using:catchNotificationScheduleNow)
        
        loadGoogleAddress()
        
        self.btnCompany.isUserInteractionEnabled = true
        
        if strBtnTag == "1" {
            
            callGetAssociatedEntitiesForLeadAssociations(EntityId: "\(dictLeadDetailsData.value(forKey: "CrmContactId")!)", EntityType: enumRefTypeCrmContact, editStatus: false, userInfo: dictLeadDetailsData)
            
        }
        
        //Deepak.s
        self.viewExistingCustomer.backgroundColor = .init(hex: "E5E5E5")
        self.viewNewCustomer.backgroundColor = .clear
        self.btnExistingCustomer.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        self.btnNewCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        //btnSelf.setImage(UIImage(named: "checked.png"), for: .normal)
        //strAssignedToId = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
        //txtFldAssignTo.text = "\(dictLoginData.value(forKeyPath: "EmployeeName")!)"
        
        if isSelfAssign
        {
            btnSelf.setImage(UIImage(named: "checked.png"), for: .normal)
            strAssignedToId = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            txtFldAssignTo.text = "\(dictLoginData.value(forKeyPath: "EmployeeName")!)"
        }
        else
        {
            btnSelf.setImage(UIImage(named: "uncheck.png"), for: .normal)
            strAssignedToId = ""
            txtFldAssignTo.text = ""
        }
        
        UserDefaults.standard.set(false, forKey: "fromScheduleOppVC")
        
        heightConstraint_ViewCompany.constant = 0.0
        topConstraint_ViewCompany.constant = 0.0
        setDefaultUrgency()
        
      getDefaultSourceMaster()
        
        //-------SetBranch----
        let EmployeeBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName") ?? "")"
        
        for item in SwitchBranchVC().getBranches()
        {
            let dict = item as! NSDictionary
            
            if EmployeeBranchSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
               txtBranch.text = "\(dict.value(forKey: "Name") ?? "")"
                break
            }
        }
        self.creatUDFView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if nsud.bool(forKey: "fromScheduleOppVC") {
            
            UserDefaults.standard.set(false, forKey: "fromScheduleOppVC")
            
            self.navigationController?.popViewController(animated: false)
            
        }else{
            
            //For Audio
            if (nsud.value(forKey: "yesAudioDocument") != nil)
            {
                
                let isAudio = nsud.bool(forKey: "yesAudioDocument")
                
                if isAudio
                {
                    
                    //deleteData()
                    nsud.set(false, forKey: "yesAudioDocument")
                    nsud.synchronize()
                    strGlobalNameAudio = nsud.value(forKey: "AudioNameDocument") as! String
                    //strGlobalNameAudio = "Audio_iOS" + "\(getUniqueValueForId())" + ".mp4"
                    globalAudioData = GetDataFromDocumentDirectory(strFileName: strGlobalNameAudio)
                    
                }
                
            }
            
            if strCustomerAddressId.count > 0 {
                
                self.txtFldAddress.isUserInteractionEnabled = false
                
            }
            
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        
        print("Text Adddress\(txtFldAddress.frame.origin.y)")
        
    }
    
    func creatUDFView()  {
        self.heightUserDefine.constant = 0.0
        let strID = ""//"\(dataGeneralInfo.value(forKey: "leadId") ?? "")"
        var strType = ""
        if isLead {
            strType = Userdefinefiled_DynemicForm_Type.Lead
        }else {
            strType = Userdefinefiled_DynemicForm_Type.Opportunity
        }
        var aryUDFSaveData_Service = UserDefineFiledVC().getUserdefineSaveData(type: strType, id: strID)
        let aryMasterData = UserDefineFiledVC().getUserdefineMasterData(type: strType)
        //----------When data is blank then use master data -------
        let arytempSave = NSMutableArray()
        //if aryUDFSaveData.count == 0 {
        for item in aryMasterData {
            let strType = "\((item as AnyObject).value(forKey: "type") ?? "")"
            let name = "\((item as AnyObject).value(forKey: "name") ?? "")"
            
            
            let temp = aryUDFSaveData_Service.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(name)" } as NSArray
            
            
            
            if (temp.count == 0 && (strType == Userdefinefiled_DynemicForm.radio_group  || strType == Userdefinefiled_DynemicForm.select || strType == Userdefinefiled_DynemicForm.checkbox_group) ) {
                if(((item as AnyObject).value(forKey: "values") is NSArray)){
                    let ary = ((item as AnyObject).value(forKey: "values") as! NSArray)
                    
                    let temp = ary.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "selected")!)".lowercased() == "true" ||  "\((task as! NSDictionary).value(forKey: "selected")!)".lowercased() == "1" } as NSArray
                    
                    if(temp.count != 0){
                        let aryValue = NSMutableArray()
                        for item in temp {
                            aryValue.add("\((item as AnyObject).value(forKey: "value") ?? "")")
                        }
                        let dict = NSMutableDictionary()
                        dict.setValue(aryValue, forKey: "userData")
                        dict.setValue(name, forKey: "name")
                        arytempSave.add(dict)
                    }
                }
            }
            if (temp.count == 0 && (strType == Userdefinefiled_DynemicForm.number ||  strType == Userdefinefiled_DynemicForm.text || strType == Userdefinefiled_DynemicForm.textarea || strType == Userdefinefiled_DynemicForm.date)){
                let dict = NSMutableDictionary()
                let aryValue = NSMutableArray()
                if((item as AnyObject).value(forKey: "value") != nil){
                    aryValue.add("\((item as AnyObject).value(forKey: "value") ?? "")")
                    dict.setValue(aryValue, forKey: "userData")
                    dict.setValue(name, forKey: "name")
                    arytempSave.add(dict)
                }
                
            }
        }
        aryUDFSaveData_Service += arytempSave.mutableCopy() as! NSMutableArray
        
  
  
    // remove blank value
    let aryTem = NSMutableArray()
    for item in aryUDFSaveData_Service {
        if(item is NSDictionary){
            let dictSaved = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
            if(dictSaved.value(forKey: "userData") is NSArray){
                let ary = (dictSaved.value(forKey: "userData") as! NSArray)
                let array = ary.filter({  "\($0)" != ""})
                dictSaved.setValue(array, forKey: "userData")
                aryTem.add(dictSaved)
            }
         
        }
        
    }
        aryUDFSaveData_Service = NSMutableArray()
        aryUDFSaveData_Service  = aryTem
        aryOtherUDF = NSMutableArray()
        /// }
    //----------End-------
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            
            UserDefineFiledVC().CreatFormUserdefine(strid: strID, aryJson_Master: aryMasterData, aryResponceObj:  aryUDFSaveData_Service, viewUserdefinefiled:  self.viewForUserDefineField, controllor: self) { aryUserdefine, aryUserdefineResponce, height in
                self.heightUserDefine.constant = CGFloat(height)
                if UIDevice.current.userInterfaceIdiom == .pad {
                     // iPad
                 } else {
                     //iPhone
                     self.height_superView.constant = 960
                     self.height_superView.constant = self.height_superView.constant + self.heightUserDefine.constant
                 }
                
            }
        }
        
    }
    
    // MARK: -----------------------------------Association Function----------------------------------------
    
    fileprivate func gotoAssociateAccount(){
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateAccountVC") as! AssociateAccountVC
        
        
        controller.delegateLeadAssociate = self
        controller.selectionTag = 0
        controller.strSelectionTag = "0"
        controller.strFromWhere = "NotAsscoiate"
        controller.strFromWhereForAssociation = "AddLeadCRM_Associations"
        nsud.set(true, forKey: "isInitalAccount")
        nsud.synchronize()
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func gotoAssociateContact(){
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
        
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "AddLeadCRM_Associations"
        controller.delegateLeadAssociate = self
        controller.selectionTag = 0
        controller.strSelectionTag = "1"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func gotoAssociateCompany(){
        
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "AddLeadCRM_Associations"
        controller.delegateLeadAssociate = self
        controller.selectionTag = 0
        controller.strSelectionTag = "2"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    
    // MARK: -----------------------------------Functions----------------------------------------
    
    
    func setDefaultUrgency()  {
        
        strUrgencyId = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.DefaultUrgency") ?? "")"
        
        if strUrgencyId.count > 0
        {
            let defsLogindDetail = UserDefaults.standard
            
            if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
                
                let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
                
                if dictLeadDetailMaster.value(forKey: "UrgencyMasters") is NSArray {
                                     
                    
                    let arrData = dictLeadDetailMaster.value(forKey: "UrgencyMasters") as! NSArray
                    
                    let arrUrgency = arrData.filter { (dict) -> Bool in
                        
                        return "\((dict as! NSDictionary).value(forKey: "UrgencyId") ?? "")" == strUrgencyId
                    } as NSArray
                    
                    if arrUrgency.count > 0
                    {
                        let dictObject = arrUrgency.object(at: 0) as! NSDictionary
                        strUrgencyId = "\(dictObject.value(forKeyPath: "UrgencyId") ?? "")"
                        txtFldUrgency.text = "\(dictObject.value(forKeyPath: "Name") ?? "")"
                    }
                    
                }
                
            }
        }
 
    }
    
    func getDefaultSourceMaster()  {
        arrSelectedSource = NSMutableArray()
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary {
            let dictDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            print(dictDetailMaster)
            if dictDetailMaster.value(forKey: "OpportunityDefaultSources") is NSArray {
                let aryTemp = dictDetailMaster.value(forKey: "OpportunityDefaultSources") as! NSArray
                if(aryTemp.count != 0){
                    for item in aryTemp{
                    
                        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary {
                            let dictLeadDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
                            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                                let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                                let aryTemp = arrOfData.filter { (task) -> Bool in
                                    
                                    return "\((task as! NSDictionary).value(forKey: "SourceId")!)" == "\((item as AnyObject).value(forKey: "SourceId")!)" }
                                if aryTemp.count != 0 {
                                    arrSelectedSource.add(aryTemp[0])
                                }
                            }
                        }
                    }
                    
                    var strName = ""
                    arrSourceId = NSMutableArray()
                    for item in arrSelectedSource{
                        strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                        arrSourceId.add("\((item as AnyObject).value(forKey: "SourceId")!)")
                    }
                    
                    if strName.count != 0{
                        strName = String(strName.dropFirst())
                    }
                    txtFldSource.text = strName
                }
            }
        }
    }
    
    
    func goToNewSelectionVC(arrOfData : NSMutableArray, tag : Int, titleHeader : String) {
        if(arrOfData.count != 0){
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectGlobalVC") as! SelectGlobalVC
            controller.titleHeader = titleHeader
            controller.arrayData = arrOfData
            controller.arraySelectionData = arrOfData
            controller.arraySelected = arrSelectedSource
            controller.delegate = self
            controller.tag = tag
            self.navigationController?.present(controller, animated: false, completion: nil)
        }else {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func mergeAllAddress() {
        
        self.arrAddress = NSMutableArray()
        
        if arrOfAccountAddress.count > 0 {
            
            self.arrAddress.addObjects(from: arrOfAccountAddress as! [Any])
            
        }
        
        /*if dictOfContactAddress.count > 0 {
         
         let zipCodeLocal = "\(self.dictOfContactAddress.value(forKey: "ZipCode")!)"
         
         if (zipCodeLocal.count > 0) {
         
         /*if !self.arrAddress.contains{ $0 == self.dictOfContactAddress } {
         self.arrAddress.add(dictOfContactAddress)
         }*/
         
         if !checkIfAddressAlreadyPresent(strAddress: Global().strCombinedAddress(dictOfContactAddress as? [AnyHashable : Any])) {
         self.arrAddress.add(dictOfContactAddress)
         }
         
         }
         
         }
         
         if dictOfCompanyAddress.count > 0 {
         
         let zipCodeLocal = "\(self.dictOfCompanyAddress.value(forKey: "ZipCode")!)"
         
         if (zipCodeLocal.count > 0) {
         
         /*if !self.arrAddress.contains{ $0 == self.dictOfCompanyAddress } {
         self.arrAddress.add(dictOfCompanyAddress)
         }*/
         
         if !checkIfAddressAlreadyPresent(strAddress: Global().strCombinedAddress(dictOfCompanyAddress as? [AnyHashable : Any])) {
         self.arrAddress.add(dictOfCompanyAddress)
         }
         
         }
         
         }*/
        
    }
    
    func checkIfAddressAlreadyPresent(strAddress : String) -> Bool {
        
        var isPresent = false
        
        for k in 0 ..< arrAddress.count {
            
            if arrAddress[k] is NSDictionary {
                
                let dictOfData = arrAddress[k] as! NSDictionary
                
                let addressLocal = Global().strCombinedAddress(dictOfData as? [AnyHashable : Any])
                
                if addressLocal == strAddress {
                    
                    isPresent = true
                    
                    break
                    
                }
                
            }
            
        }
        
        return isPresent
        
    }
    
    func loadGoogleAddress()
    {
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        txtAddressMaxY = (txtFldAddress.frame.origin.y) - 35
        imgPoweredBy.image = UIImage(named: "powered-by-google-on-white")
        imgPoweredBy.contentMode = .center
    }
    
    func catchNotificationScheduleNow(notification:Notification) {
        
        if let dict = notification.userInfo as NSDictionary? {
            
            print(dict)
            dictScheduleNow = dict
            
            scheduleToAddress()
            
            if dictScheduleNow.count > 0 {
                
                btnScheduleNow.setTitle("Re-Schedule", for: .normal)
                
            }else{
                
                btnScheduleNow.setTitle("Schedule Now", for: .normal)
                
            }
            
        }
        
    }
    func catchNotification(notification:Notification) {
        
        if let dict = notification.userInfo as NSDictionary? {
            
            strPrimaryService = "\(dict.value(forKey: "PrimaryService")!)"
            
            let arrOfServiceName = NSMutableArray()
            
            let strServiceName = fetchServiceNameViaId(strId: strPrimaryService)
            
            if strServiceName.count > 0 {
                
                arrOfServiceName.add(strServiceName)
                
            }
            
            let arrOfAdditionalServicesSelected = (dict.value(forKey: "AdditionalService")!) as! NSMutableArray
            
            if arrOfAdditionalServicesSelected.count > 0 {
                
                arrOfAdditionalServicesSelected.addingObjects(from: arrOfAdditionalServicesSelected as! [Any])
                
            }
            
            for k in 0 ..< arrOfAdditionalServicesSelected.count {
                
                let strServiceNameLocal = fetchServiceNameViaId(strId: arrOfAdditionalServicesSelected[k] as! String)
                if strServiceNameLocal.count > 0 {
                    
                    arrOfServiceName.add(strServiceNameLocal)
                    
                }
                
            }
            
            if arrOfAdditionalServicesSelected.count > 0 {
                
                arrOfAdditionalService = arrOfAdditionalServicesSelected
                
            }
            
            txtFldService.text = arrOfServiceName.componentsJoined(by: ", ")
            
        }
    }
    
    func fetchServiceNameViaId(strId : String) -> String {
        
        var serviceName = ""
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        for k in 0 ..< arrOfData.count {
            
            if arrOfData[k] is NSDictionary {
                
                let dictOfData = arrOfData[k] as! NSDictionary
                
                if "\(dictOfData.value(forKey: "ServiceId")!)" == strId {
                    
                    serviceName = "\(dictOfData.value(forKey: "Name")!)"
                    break
                }
                
            }
            
        }
        
        return serviceName
        
    }
    
    func clickOnAudioBtn(sender : UIButton) {
        
        self.view.endEditing(true)
        
        if strGlobalNameAudio.count > 0 {
            
            let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            alert.addAction(UIAlertAction(title: "Play Audio", style: .default , handler:{ (UIAlertAction)in
                
                self.playAudioo(strAudioName: self.strGlobalNameAudio)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Re-take Audio", style: .default , handler:{ (UIAlertAction)in
                
                self.goToRecordView()
                
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
                
            }))
            //alert.popoverPresentationController?.sourceRect = sender.frame
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as UIView
                popoverController.sourceRect = sender.bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
            }
            
            self.present(alert, animated: true, completion: {
                
                
            })
            
        } else {
            
            goToRecordView()
            
        }
        
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func goToRecordView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
        testController?.strFromWhere = "PestDocument"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func playAudioo(strAudioName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func getAddress(from dataString: String) ->  NSMutableArray {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        let resultsArray = NSMutableArray()
        // put matches into array of Strings
        for match in matches {
            if match.resultType == .address,
               let components = match.addressComponents {
                resultsArray.add(components)
            } else {
                print("no components found")
            }
        }
        return resultsArray
    }
    
    func addressDict() {
        
        var countryId = "1"
        
        /*if strCustomerAddressId.count > 0 {
         
         strAddressLine1 = ""
         strCityName = ""
         strZipCode = ""
         strStateId = ""
         strAddressLine1 = ""
         countryId = ""
         
         }*/
        
        var keys = NSArray()
        var values = NSArray()
        keys = ["Address1",
                "CityName",
                "Zipcode",
                "StateId",
                "CountryId",
                "CustomerAddressId"]
        
        values = [strAddressLine1,
                  strCityName,
                  strZipCode,
                  strStateId,
                  countryId,
                  strCustomerAddressId]
        
        let dict = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        dictAddress.addEntries(from: dict as! [AnyHashable : Any])
        
    }
    
    func scheduleToAddress() {
        
        if dictScheduleNow.count > 0 {
            
            dictAddress.setValue("\(dictScheduleNow.value(forKey: "CustomerAddressId")!)", forKey: "CustomerAddressId")
            dictAddress.setValue("\(dictScheduleNow.value(forKey: "Address1")!)", forKey: "Address1")
            dictAddress.setValue("\(dictScheduleNow.value(forKey: "CityName")!)", forKey: "CityName")
            dictAddress.setValue("\(dictScheduleNow.value(forKey: "Zipcode")!)", forKey: "Zipcode")
            dictAddress.setValue("\(dictScheduleNow.value(forKey: "StateId")!)", forKey: "StateId")
            dictAddress.setValue("\(dictScheduleNow.value(forKey: "CountryId")!)", forKey: "CountryId")
            
            strCustomerAddressId = "\(dictScheduleNow.value(forKey: "CustomerAddressId")!)"
            
            /*if strAccountId.count > 0 {
             
             loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
             self.present(loader, animated: false, completion: nil)
             
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
             
             self.callApiToGetAddressViaAccountId(strAccountIdLocal: self.strAccountId, strType: "Selected")
             
             })
             
             }*/
            
            txtFldAddress.text = Global().strCombinedAddress(dictAddress as? [AnyHashable : Any])
            
        }
        
    }
    func addressToSchedule() {
        
        if dictAddress.count > 0 {
            
            let dictScheduleLocal = NSMutableDictionary()
            
            if dictScheduleNow.count > 0 {
                
                dictScheduleLocal.addEntries(from: dictScheduleNow as! [AnyHashable : Any])
                
            }else{
                
                dictScheduleLocal.setValue("", forKey: "ScheduleDate")
                dictScheduleLocal.setValue("", forKey: "ScheduleTime")
                dictScheduleLocal.setValue("\(dictLoginData.value(forKeyPath: "EmployeeId")!)", forKey: "FieldSalesPerson")
                
            }
            
            dictScheduleLocal.setValue("\(dictAddress.value(forKey: "CustomerAddressId")!)", forKey: "CustomerAddressId")
            dictScheduleLocal.setValue("\(dictAddress.value(forKey: "Address1")!)", forKey: "Address1")
            dictScheduleLocal.setValue("\(dictAddress.value(forKey: "CityName")!)", forKey: "CityName")
            dictScheduleLocal.setValue("\(dictAddress.value(forKey: "Zipcode")!)", forKey: "Zipcode")
            dictScheduleLocal.setValue("\(dictAddress.value(forKey: "StateId")!)", forKey: "StateId")
            dictScheduleLocal.setValue("\(dictAddress.value(forKey: "CountryId")!)", forKey: "CountryId")
            
            dictScheduleNow = dictScheduleLocal
            
        }
        
    }
    
    func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    func responseOnUpload() {
        
        self.loader.dismiss(animated: false) {
            
            if self.strRefType == "WebLead" {
                
                let alertCOntroller = UIAlertController(title: Info, message: "Lead added Successfully", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedLeadOpportunity_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                    
                    //self.navigationController?.popViewController(animated: false)
                    let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
                    self.navigationController?.pushViewController(controller, animated: false)
                    
                })
                
                alertCOntroller.addAction(alertAction)
                self.present(alertCOntroller, animated: true, completion: nil)
                
            }else{
                
                let alertCOntroller = UIAlertController(title: Info, message: "Opportunity added Successfully.", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Go to Appointments", style: .default, handler: { (action) in
                    
                    //                nsud.set(true, forKey: "fromCompanyVC")
                    //                nsud.synchronize()
                    if(DeviceType.IS_IPAD){
                        
                        let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                        
                        if strAppointmentFlow == "New" {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "Appointment_iPAD",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            
                        } else {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "MainiPad",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            
                        }
                             
                    }else{
                        
                        let alertCOntroller = UIAlertController(title: Info, message: "Opportunity added Successfully.", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Go to Appointments", style: .default, handler: { (action) in
                            
            //                nsud.set(true, forKey: "fromCompanyVC")
            //                nsud.synchronize()
                                if(DeviceType.IS_IPAD){
                                             let mainStoryboard = UIStoryboard(
                                                 name: "MainiPad",
                                                 bundle: nil)
                                             let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                                             self.navigationController?.pushViewController(objByProductVC!, animated: false)
                                         }else{
                                    
                                    let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                                    
                                    if strAppointmentFlow == "New" {
                                        
                                        let mainStoryboard = UIStoryboard(
                                            name: "Appointment",
                                            bundle: nil)
                                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                                        self.navigationController?.pushViewController(objByProductVC!, animated: false)
                                        
                                    } else {
                                        
                                             let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
                                             self.navigationController?.pushViewController(controller, animated: false)
                                        
                                    }
                                         }
                            
                        })
                        
                        let alertActionNo = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedLeadOpportunity_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            self.navigationController?.popViewController(animated: false)
                        })

                        alertCOntroller.addAction(alertActionNo)
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }
                    
                })
                
                let alertActionNo = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedLeadOpportunity_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                    
                    self.navigationController?.popViewController(animated: false)
                })
                
                alertCOntroller.addAction(alertActionNo)
                alertCOntroller.addAction(alertAction)
                self.present(alertCOntroller, animated: true, completion: nil)
                
            }
            
            
        }
        
    }
    
    func callToUploadImage() {
        
        if (self.arrOfImages.count > 0) && (self.arrOfImages.count > self.imageCount){
            
            let imagee = self.arrOfImages[self.imageCount] as? UIImage
            
            let dataToSend = imagee?.jpegData(compressionQuality: 1.0)
            
            let imageNamee = "img" + "\(self.imageCount)" + "\(getUniqueValueForId())" + ".png"
            
            self.uploadImageFile(strMediaType: "Image", strRefIdd: strRefId, strMediaName: imageNamee, mediaData: dataToSend!)
            
        }
        
    }
    
    func getAddressTypeFromMasterSalesAutomation() -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    
    // MARK: -----------------------------------Validation----------------------------------------
    
    // make crm contact mandatory
    
    func validationOnAddLeadOppotunity(strType : String) -> Bool {
        
        if(txtFldType.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select type.", viewcontrol: self)
            return false
            
        }
        
        if strType == enumRefTypeLead {
            
            strRefType = enumRefTypeLead
            
            if(txtFldContact.text == "")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select contact.", viewcontrol: self)
                return false
                
            }
            
            if btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") {
                
                if(txtFldAccount.text == "")
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select account.", viewcontrol: self)
                    return false
                    
                }
                
            }
            
            if (txtFldAddress.text?.count == 0) {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address.", viewcontrol: self)
                return false
                
            }
            
            if(txtFldSource.text == "")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select source.", viewcontrol: self)
                return false
                
            }
            
            if(txtFldService.text == "")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select service.", viewcontrol: self)
                return false
                
            }
            
        }else{
            
            strRefType = enumRefTypeWebLead
            
            if((txtFldContact.text == "") && (txtFldCompany.text == ""))
            {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select contact or company.", viewcontrol: self)
                return false
                
            }
            
        }
        
        if (txtFldAddress.text!.count > 0) {
            
            let dictOfEnteredAddress = formatEnteredAddress(value: txtFldAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                
                return false
                
            } else {
                
                //test
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"
                
                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"
                
                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""
                    
                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strCityName.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strStateId.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count != 5 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                
            }
            
            self.addressDict()
            
        }
        
        if(isSourceCodeRequired && txtFldSource.text == "" && strType == enumRefTypeWebLead)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select source.", viewcontrol: self)
            return false
        }
        
        if strType == enumRefTypeLead {
            
            if(strAssignedToId.count == 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select assigned to.", viewcontrol: self)
                return false
                
            }
            
        }
        
        /*if(strAssignedToId.count == 0)
         {
         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select assigned to.", viewcontrol: self)
         return false
         
         }*/
        if arrOfAdditionalService.count > 0 {
            
            if(strPrimaryService.count == 0)
            {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select primary service.", viewcontrol: self)
                return false
                
            }
            
        }
        //Deepak.s
        let aryMasterData = UserDefineFiledVC().GetMasterData()
        let aryresponceobj = UserDefineFiledVC().GetSaveData()
        
        if(UserDefineFiledVC().validation_UserdefineFiled(aryMaster: aryMasterData, aryresponceobj: aryresponceobj) != ""){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: UserDefineFiledVC().validation_UserdefineFiled(aryMaster: aryMasterData, aryresponceobj: aryresponceobj), viewcontrol: self)
            return false
        }
        //******
        return true
        
    }
    
    func validationOnAddLeadOppotunityOldddddd() -> Bool {
        
        if(txtFldType.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select type.", viewcontrol: self)
            return false
            
        }
        
        /*if btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") {
         
         if(txtFldAccount.text == "")
         {
         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select account.", viewcontrol: self)
         return false
         
         }
         
         }*/
        
        if dictScheduleNow.count > 0 {
            
            let strSc = "\(self.dictScheduleNow.value(forKey: "ScheduleDate")!)"
            
            if strSc.count > 0 {
                
                strRefType = enumRefTypeLead
                
                if(txtFldContact.text == "")
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select contact.", viewcontrol: self)
                    return false
                    
                }
                
                if btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") {
                    
                    if(txtFldAccount.text == "")
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select account.", viewcontrol: self)
                        return false
                        
                    }
                    
                }
                
                if(txtFldSource.text == "")
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select source.", viewcontrol: self)
                    return false
                    
                }
                
                if(txtFldService.text == "")
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select service.", viewcontrol: self)
                    return false
                    
                }
                
            } else {
                
                strRefType = enumRefTypeWebLead
                
                if((txtFldContact.text == "") && (txtFldCompany.text == ""))
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select contact or company.", viewcontrol: self)
                    return false
                    
                }
                
            }
            
        }else{
            
            strRefType = enumRefTypeWebLead
            
            if((txtFldContact.text == "") && (txtFldCompany.text == ""))
            {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select contact or company.", viewcontrol: self)
                return false
                
            }
            
        }
        
        if (txtFldAddress.text!.count > 0) {
            
            let dictOfEnteredAddress = formatEnteredAddress(value: txtFldAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                
                return false
                
            } else {
                
                //test
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"
                
                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"
                
                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""
                    
                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strCityName.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strStateId.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count != 5 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                
            }
            
            self.addressDict()
            
        }
        
        /*if ((txtFldAddress.text!.count > 0) && (strCustomerAddressId.count <= 0)) {
         
         // Fetch Address
         
         let arrOfAddress = self.getAddress(from: txtFldAddress.text!)
         
         let tempArrayOfKeys = NSMutableArray()
         let tempDictData = NSMutableDictionary()
         
         for k in 0 ..< arrOfAddress.count {
         
         if arrOfAddress[k] is NSDictionary {
         
         let tempDict = arrOfAddress[k] as! NSDictionary
         
         tempArrayOfKeys.addObjects(from: tempDict.allKeys)
         tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])
         
         }
         
         }
         
         if tempArrayOfKeys.contains("City") {
         
         strCityName = "\(tempDictData.value(forKey: "City")!)"
         
         }
         if tempArrayOfKeys.contains("ZIP") {
         
         strZipCode = "\(tempDictData.value(forKey: "ZIP")!)"
         
         }
         if tempArrayOfKeys.contains("State") {
         
         let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
         
         if dictStateDataTemp.count > 0 {
         
         strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
         //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
         
         }else{
         
         strStateId = ""
         //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)
         
         }
         
         }
         if tempArrayOfKeys.contains("Street") {
         
         strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"
         
         }
         
         if strAddressLine1.count == 0 || strCityName.count == 0 || strZipCode.count == 0 || strStateId.count == 0 {
         
         
         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter proper address (address, city, zipcode, state).", viewcontrol: self)
         
         return false
         
         }
         
         addressDict()
         
         }else{
         
         addressDict()
         
         }*/
        if(strAssignedToId.count == 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select assigned to.", viewcontrol: self)
            return false
            
        }
        if arrOfAdditionalService.count > 0 {
            
            if(strPrimaryService.count == 0)
            {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select primary service.", viewcontrol: self)
                return false
                
            }
            
        }
        
        
        return true
        
    }
    
    // MARK: - ----------- Service Address Api's Background----------------
    
    // API's Calling In backGround For Getting Address
    
    func callApiToGetAddressViaAccountIdBackGround(strAccountIdLocal : String , strType : String) {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetServiceAddressesByAccountId + strAccountIdLocal
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            //self.loader.dismiss(animated: false) {
            
            if(Status)
            {
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    self.arrOfAccountAddress = Response.value(forKey: "data") as! NSArray
                    
                    if self.arrOfAccountAddress.count > 0 {
                        
                        self.mergeAllAddress()
                        
                    }
                    
                }
                
            }
            
            //}
            
        }
    }
    
    func callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal : String , strType : String) {
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmContactId + strCrmContactIdLocal
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            self.loader.dismiss(animated: false) {
                if(Status)
                {
                    let arrKeys = Response.allKeys as NSArray
                    if arrKeys.contains("data") {
                        self.dictOfContactAddress = Response.value(forKey: "data") as! NSDictionary
                        self.mergeAllAddress()
                    }
                }
            }
        }
    }
    
    func callApiToGetAddressViaCrmCompanyIdBackGround(strCrmCoCompanyIdLocal : String , strType : String) {
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmCompanyId + strCrmCoCompanyIdLocal
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            //self.loader.dismiss(animated: false) {
            if(Status)
            {
                let arrKeys = Response.allKeys as NSArray
                if arrKeys.contains("data") {
                    self.dictOfCompanyAddress = Response.value(forKey: "data") as! NSDictionary
                    self.mergeAllAddress()
                }
            }
            //}
        }
    }
    
    // MARK: -----------------------------------API's Calling----------------------------------------
    
    func uploadImageFile(strMediaType : String , strRefIdd : String , strMediaName : String , mediaData : Data) {
        
        let dictSendDocument =
            ["RefId": strRefIdd,
             "RefType": strRefType,
             "MediaType": strMediaType,
             "Title": strMediaType,
             "CompanyKey": Global().getCompanyKey(),
             "WebLeadDocumentId": "0",
             "Createdby": Global().getEmployeeId()]
        
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(dictSendDocument) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print(jsonString)
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddDocumentsByRefIdAndRefType
                WebService.getRequestWithHeadersWithMultipleImage(JsonData: jsonString,JsonDataKey: "WebLeadDocDc", data: mediaData, strDataName : strMediaName, url: strURL) { (responce, status) in
                    if(status)
                    {
                        
                        //self.dispatchGroup.leave()
                        
                        if(responce.value(forKey: "data")is NSDictionary)
                        {
                            
                            // Handle Success Response Here
                            
                            /*let alert = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertController.Style.alert)
                             
                             // add the actions (buttons)
                             alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                             self.dismiss(animated: false)
                             }))
                             self.present(alert, animated: true, completion: nil)
                             */
                            
                        }
                        else
                        {
                            //showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "String", viewcontrol: self)
                        }
                    }else{
                        //                                            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Error", viewcontrol: self)
                    }
                    
                    self.imageCount = self.imageCount + 1
                    
                    if (self.arrOfImages.count > 0) && (self.arrOfImages.count > self.imageCount){
                        
                        self.callToUploadImage()
                        
                    }else{
                        
                        self.dispatchGroup.leave()
                        
                    }
                    
                }
            }
        }
    }
    
    func uploadAudioFile(strMediaType : String , strRefIdd : String , strMediaName : String , mediaData : Data) {
        
        let dictSendDocument =
            ["RefId": strRefIdd,
             "RefType": strRefType,
             "MediaType": strMediaType,
             "Title": strMediaType,
             "CompanyKey": Global().getCompanyKey(),
             "WebLeadDocumentId": "0",
             "Createdby": Global().getEmployeeId()]
        
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(dictSendDocument) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print(jsonString)
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddDocumentsByRefIdAndRefType
                WebService.getRequestWithHeadersWithMultipleImage(JsonData: jsonString,JsonDataKey: "WebLeadDocDc", data: mediaData, strDataName : strMediaName, url: strURL) { (responce, status) in
                    if(status)
                    {
                        
                        self.dispatchGroup.leave()
                        
                        if(responce.value(forKey: "data")is NSDictionary)
                        {
                            
                            // Handle Success Response Here
                            
                            /*let alert = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertController.Style.alert)
                             
                             // add the actions (buttons)
                             alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                             self.dismiss(animated: false)
                             }))
                             self.present(alert, animated: true, completion: nil)
                             */
                            
                        }
                        else
                        {
                            //showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "String", viewcontrol: self)
                        }
                    }else{
                        //showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Error", viewcontrol: self)
                    }
                }
            }
        }
    }
    
    func callApiToGetAddressViaAccountId(strAccountIdLocal : String , strType : String) {
        
        self.txtFldAddress.isUserInteractionEnabled = true
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetServiceAddressesByAccountId + strAccountIdLocal
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {
                
                if(Status)
                {
                    let arrKeys = Response.allKeys as NSArray
                    if arrKeys.contains("data") {
                        let dictData = Global().convertNullArray(Response as? [AnyHashable : Any])! as NSDictionary
                        
                        if(dictData.value(forKey: "data") is NSArray){
                            self.arrOfAccountAddress = dictData.value(forKey: "data") as! NSArray
                            
                            //self.arrOfAccountAddress = Response.value(forKey: "data") as! NSArray
                            
                            if self.arrOfAccountAddress.count > 0 {
                            
                              
                                
                                self.mergeAllAddress()
                                
                                for k in 0 ..< self.arrOfAccountAddress.count {
                                    
                                    let dictData = self.arrOfAccountAddress[k] as! NSDictionary
                                    let AddressPropertyName = "\(dictData.value(forKey: "AddressPropertyName")!)"
                                    let AddressPropertyTypeId = "\(dictData.value(forKey: "AddressPropertyTypeId")!)"
                                    self.txtfldPropertyType.text = AddressPropertyName
                                    self.txtfldPropertyType.tag = (AddressPropertyTypeId == "" ? 0 : Int(AddressPropertyTypeId))!
                                    if (dictData.value(forKey: "IsPrimary") as! Bool) == true {
                                        
                                        if self.txtFldAddress.text?.count != 0 {
                                            
                                            let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                            
                                            let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                                
                                                //AddressSubType
                                                
                                                if self.txtFldType.text?.count == 0 {
                                                    
                                                    self.txtFldType.text = "\(dictData.value(forKey: "AddressSubType") ?? "")"
                                                    
                                                }
                                                
                                                self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                                                self.strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                                                self.txtFldAddress.isUserInteractionEnabled = false
                                                
                                            })
                                            
                                            let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                                
                                                
                                                
                                            })
                                            
                                            alertCOntroller.addAction(alertAction)
                                            alertCOntroller.addAction(alertActionCancel)
                                            
                                            self.present(alertCOntroller, animated: true, completion: nil)
                                            
                                        }else{
                                            
                                            self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                                            self.strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                                            self.txtFldAddress.isUserInteractionEnabled = false
                                            
                                            //AddressSubType
                                            
                                            if self.txtFldType.text?.count == 0 {
                                                
                                                self.txtFldType.text = "\(dictData.value(forKey: "AddressSubType") ?? "")"
                                                
                                            }
                                            
                                        }
                                        
                                        break
                                        
                                    }
                                    
                                }
                                
                            }
                        }
               
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetAddressViaCrmContactId(strCrmContactIdLocal : String , strType : String) {
        
        self.txtFldAddress.isUserInteractionEnabled = true
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmContactId + strCrmContactIdLocal
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {
                
                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfContactAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.dictOfContactAddress = removeNullFromDict(dict: self.dictOfContactAddress.mutableCopy()as! NSMutableDictionary)
                        
                        if self.dictOfContactAddress.count > 0 {
                            
                            let zipCodeLocal = "\(self.dictOfContactAddress.value(forKey: "ZipCode")!)"
                            
                            if (zipCodeLocal.count > 0) {
                                
                                if self.txtFldAddress.text?.count != 0 {
                                    
                                    let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                    
                                    let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                        
                                        self.txtFldAddress.text = Global().strCombinedAddress(self.dictOfContactAddress as? [AnyHashable : Any])
                                        //self.strCustomerAddressId = "\(self.dictOfContactAddress.value(forKey: "CustomerAddressId") ?? "")"
                                        self.strCustomerAddressId = ""
                                        
                                        //self.txtFldAddress.isUserInteractionEnabled = false
                                        
                                    })
                                    
                                    let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                        
                                        
                                        
                                    })
                                    
                                    alertCOntroller.addAction(alertAction)
                                    alertCOntroller.addAction(alertActionCancel)
                                    
                                    self.present(alertCOntroller, animated: true, completion: nil)
                                    
                                }else{
                                    
                                    self.txtFldAddress.text = Global().strCombinedAddress(self.dictOfContactAddress as? [AnyHashable : Any])
                                    //self.strCustomerAddressId = "\(self.dictOfContactAddress.value(forKey: "CustomerAddressId") ?? "")"
                                    self.strCustomerAddressId = ""
                                    
                                    //self.txtFldAddress.isUserInteractionEnabled = false
                                    
                                }
                                
                                self.mergeAllAddress()
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetAddressViaCrmCompanyId(strCrmCoCompanyIdLocal : String , strType : String) {
        
        self.txtFldAddress.isUserInteractionEnabled = true
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmCompanyId + strCrmCoCompanyIdLocal
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {
                
                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfCompanyAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.dictOfCompanyAddress = removeNullFromDict(dict: self.dictOfCompanyAddress.mutableCopy()as! NSMutableDictionary)
                        
                        if self.dictOfCompanyAddress.count > 0 {
                            
                            let zipCodeLocal = "\(self.dictOfCompanyAddress.value(forKey: "ZipCode")!)"
                            let AddressPropertyName = "\(self.dictOfCompanyAddress.value(forKey: "AddressPropertyName")!)"
                            let AddressPropertyTypeId = "\(self.dictOfCompanyAddress.value(forKey: "AddressPropertyTypeId")!)"
                            self.txtfldPropertyType.text = AddressPropertyName
                            self.txtfldPropertyType.tag = (AddressPropertyTypeId == "" ? 0 : Int(AddressPropertyTypeId))!

                            if (zipCodeLocal.count > 0) {
                                if self.txtFldAddress.text?.count != 0 {
                                    let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                    let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                        
                                        self.txtFldAddress.text = Global().strCombinedAddress(self.dictOfCompanyAddress as? [AnyHashable : Any])
                                        //self.strCustomerAddressId = "\(self.dictOfCompanyAddress.value(forKey: "CustomerAddressId") ?? "")"
                                        self.strCustomerAddressId = ""
                                        
                                        //self.txtFldAddress.isUserInteractionEnabled = false
                                        
                                    })
                                    
                                    let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                        
                                        
                                        
                                    })
                                    
                                    alertCOntroller.addAction(alertAction)
                                    alertCOntroller.addAction(alertActionCancel)
                                    
                                    self.present(alertCOntroller, animated: true, completion: nil)
                                    
                                }else{
                                    
                                    self.txtFldAddress.text = Global().strCombinedAddress(self.dictOfCompanyAddress as? [AnyHashable : Any])
                                    //self.strCustomerAddressId = "\(self.dictOfCompanyAddress.value(forKey: "CustomerAddressId") ?? "")"
                                    self.strCustomerAddressId = ""
                                    
                                    //self.txtFldAddress.isUserInteractionEnabled = false
                                    
                                }
                                
                                self.mergeAllAddress()
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callGetAssociatedEntitiesForLeadAssociations(EntityId: String ,EntityType : String,editStatus : Bool , userInfo : NSDictionary) {
        
        if(EntityId.count != 0 && EntityId != "<null>"){
            
            if(isInternetAvailable()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAssociatedEntitiesForTaskActivityAssociations + "EntityType=\(EntityType)&EntityId=\(EntityId)"
                
                WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                    
                    self.loader.dismiss(animated: false) {
                        
                        if(Status)
                        {
                            let dictResponse = removeNullFromDict(dict: (Response["data"] as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                            
                            var strID = ""
                            var strName = ""
                            
                            let arrOfKeys = userInfo.allKeys as NSArray
                            
                            if self.strBtnTag == "0"{
                                
                                // set company uneditable
                                
                                //self.btnCompany.isUserInteractionEnabled = false
                                
                                if arrOfKeys.contains("AccountId") {
                                    
                                    // Account
                                    strID = "\(userInfo.value(forKey: "AccountId")!)"
                                    strName = "\(userInfo.value(forKey: "AccountName")!)"
                                    
                                    let strName1 = "\(userInfo.value(forKey: "AccountName") ?? "")"
                                    
                                    if strName1.count == 0 {
                                        
                                        let strCrmContactName = "\(userInfo.value(forKey: "CrmContactName") ?? "")"
                                        
                                        if strCrmContactName.count == 0 {
                                            
                                            let strCrmCompanyName = "\(userInfo.value(forKey: "CrmCompanyName") ?? "")"
                                            
                                            if strCrmCompanyName.count == 0 {
                                                
                                                strName = " "
                                                
                                            }else{
                                                
                                                strName = "\(strCrmCompanyName)"
                                                
                                            }
                                            
                                        }else{
                                            
                                            strName = "\(strCrmContactName)"
                                            
                                        }
                                        
                                    }else{
                                        
                                        strName = "\(strName1)"
                                        
                                    }
                                    
                                }
                                
                                if strID.count > 0 {
                                    
                                    self.strAccountId = strID
                                    self.txtFldAccount.text = strName
                                    
                                    if (isInternetAvailable()){
                                        
                                        self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                        self.present(self.loader, animated: false, completion: nil)
                                        
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                            
                                            self.callApiToGetAddressViaAccountId(strAccountIdLocal: self.strAccountId ,strType : "sadf")
                                            
                                        })
                                        
                                    }
                                    
                                }
                                
                            }else if self.strBtnTag == "1"{
                                
                                // Contact
                                
                                strID = "\(userInfo.value(forKey: "CrmContactId") ?? "")"
                                strName = Global().strFullName(userInfo as? [AnyHashable : Any])
                                
                                if strName.count == 0 || strName == "N/A" {
                                    
                                    strName = "\(userInfo.value(forKey: "CrmContactName") ?? "")"
                                    
                                }
                                
                                if strID.count > 0 {
                                    
                                    self.strCrmContactId = strID
                                    self.txtFldContact.text = strName
                                    
                                    // Calling API for Contact Address
                                    
                                    if (isInternetAvailable()){
                                        
                                        self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                        self.present(self.loader, animated: false, completion: nil)
                                        
                                        self.callApiToGetAddressViaCrmContactId(strCrmContactIdLocal: self.strCrmContactId, strType: "")
                                        
                                    }
                                    
                                }
                                
                            }else if self.strBtnTag == "2"{
                                // Company
                                
                                strID = "\(userInfo.value(forKey: "CrmCompanyId")!)"
                                strName = "\(userInfo.value(forKey: "Name")!)"
                                
                                if strID.count > 0 {
                                    
                                    self.strCrmCompanyId = strID
                                    self.txtFldCompany.text = strName
                                    
                                    // Calling API for Company Address
                                    
                                    if (isInternetAvailable()){
                                        
                                        self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                        self.present(self.loader, animated: false, completion: nil)
                                        
                                        self.callApiToGetAddressViaCrmCompanyId(strCrmCoCompanyIdLocal: self.strCrmCompanyId, strType: "")
                                        
                                    }
                                    
                                }
                                
                            }
                            
                            if (editStatus){ // For edit
                                if self.strBtnTag == "0" { //account 0 -> replace Conatact 3 & comapny 4
                                    
                                    if "\(dictResponse.value(forKey: "CrmContactId")!)".count > 0 {
                                        
                                        self.strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)" == "" ? "" : "\(dictResponse.value(forKey: "CrmContactId")!)"
                                        self.txtFldContact.text = "\(dictResponse.value(forKey: "ContactName")!)"
                                        
                                    }
                                    
                                    if "\(dictResponse.value(forKey: "CrmCompanyId")!)".count > 0 {
                                        
                                        self.strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)" == "" ? "" : "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                        self.txtFldCompany.text = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                        
                                    }
                                    
                                    if self.strCrmCompanyId.count > 0  {
                                        
                                        self.btnCompany.isUserInteractionEnabled = false
                                        
                                    } else {
                                        
                                        self.btnCompany.isUserInteractionEnabled = true
                                        
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strCrmContactId.count > 0 {
                                            self.callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal: self.strCrmContactId, strType: "")
                                        }
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strCrmCompanyId.count > 0 {
                                            self.callApiToGetAddressViaCrmCompanyIdBackGround(strCrmCoCompanyIdLocal: self.strCrmCompanyId, strType: "")
                                        }
                                    }
                                    
                                }else if self.strBtnTag == "1" { // Contact 3-> Company 4 & Account 0
                                    
                                    if "\(dictResponse.value(forKey: "AccountId")!)".count > 0 {
                                        
                                        self.strAccountId = "\(dictResponse.value(forKey: "AccountId")!)" == "" ? "" : "\(dictResponse.value(forKey: "AccountId")!)"
                                        self.txtFldAccount.text = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                        
                                    }
                                    
                                    if "\(dictResponse.value(forKey: "CrmCompanyId")!)".count > 0 {
                                        
                                        self.strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)" == "" ? "" : "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                        self.txtFldCompany.text = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                        
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strAccountId.count > 0 {
                                            self.callApiToGetAddressViaAccountIdBackGround(strAccountIdLocal: self.strAccountId ,strType : "sadf")
                                        }
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strCrmCompanyId.count > 0 {
                                            self.callApiToGetAddressViaCrmCompanyIdBackGround(strCrmCoCompanyIdLocal: self.strCrmCompanyId, strType: "")
                                        }
                                    }
                                    
                                }else if self.strBtnTag == "2" { // Company 4  -> Contact 3 & A/C 0
                                    
                                    if "\(dictResponse.value(forKey: "AccountId")!)".count > 0 {
                                        
                                        self.strAccountId = "\(dictResponse.value(forKey: "AccountId")!)" == "" ? "" : "\(dictResponse.value(forKey: "AccountId")!)"
                                        self.txtFldAccount.text = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                        
                                    }
                                    
                                    if "\(dictResponse.value(forKey: "CrmContactId")!)".count > 0 {
                                        
                                        self.strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)" == "" ? "" : "\(dictResponse.value(forKey: "CrmContactId")!)"
                                        self.txtFldContact.text = "\(dictResponse.value(forKey: "ContactName")!)"
                                        
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strAccountId.count > 0 {
                                            self.callApiToGetAddressViaAccountIdBackGround(strAccountIdLocal: self.strAccountId ,strType : "sadf")
                                        }
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strCrmContactId.count > 0 {
                                            self.callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal: self.strCrmContactId, strType: "")
                                        }
                                    }
                                    
                                }
                                
                            }else{ // For add
                                if self.strBtnTag == "0" { //account 0 -> replace Conatact 3 & comapny 4
                                    
                                    if "\(dictResponse.value(forKey: "CrmContactId")!)".count > 0 {
                                        
                                        self.strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)" == "" ? "" : "\(dictResponse.value(forKey: "CrmContactId")!)"
                                        self.txtFldContact.text = "\(dictResponse.value(forKey: "ContactName")!)"
                                        
                                    }
                                    
                                    if "\(dictResponse.value(forKey: "CrmCompanyId")!)".count > 0 {
                                        
                                        self.strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)" == "" ? "" : "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                        self.txtFldCompany.text = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                        
                                    }
                                    
                                    if self.strCrmCompanyId.count > 0  {
                                        
                                        self.btnCompany.isUserInteractionEnabled = false
                                        
                                    } else {
                                        
                                        self.btnCompany.isUserInteractionEnabled = true
                                        
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strCrmContactId.count > 0 {
                                            self.callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal: self.strCrmContactId, strType: "")
                                        }
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strCrmCompanyId.count > 0 {
                                            self.callApiToGetAddressViaCrmCompanyIdBackGround(strCrmCoCompanyIdLocal: self.strCrmCompanyId, strType: "")
                                        }
                                    }
                                    
                                }else if self.strBtnTag == "1" { // Contact 3-> Company 4 & Account 0
                                    
                                    if "\(dictResponse.value(forKey: "AccountId")!)".count > 0 {
                                        
                                        self.strAccountId = "\(dictResponse.value(forKey: "AccountId")!)" == "" ? "" : "\(dictResponse.value(forKey: "AccountId")!)"
                                        self.txtFldAccount.text = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                        
                                    }
                                    
                                    if "\(dictResponse.value(forKey: "CrmCompanyId")!)".count > 0 {
                                        
                                        self.strCrmCompanyId = "\(dictResponse.value(forKey: "CrmCompanyId")!)" == "" ? "" : "\(dictResponse.value(forKey: "CrmCompanyId")!)"
                                        self.txtFldCompany.text = "\(dictResponse.value(forKey: "CrmCompanyName")!)"
                                        
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strAccountId.count > 0 {
                                            self.callApiToGetAddressViaAccountIdBackGround(strAccountIdLocal: self.strAccountId ,strType : "sadf")
                                        }
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strCrmCompanyId.count > 0 {
                                            self.callApiToGetAddressViaCrmCompanyIdBackGround(strCrmCoCompanyIdLocal: self.strCrmCompanyId, strType: "")
                                        }
                                    }
                                    
                                }else if self.strBtnTag == "2" { // Company 4  -> Contact 3 & A/C 0
                                    
                                    if "\(dictResponse.value(forKey: "AccountId")!)".count > 0 {
                                        
                                        self.strAccountId = "\(dictResponse.value(forKey: "AccountId")!)" == "" ? "" : "\(dictResponse.value(forKey: "AccountId")!)"
                                        self.txtFldAccount.text = "\(dictResponse.value(forKey: "AccountDisplayName")!)"
                                        
                                    }
                                    
                                    if "\(dictResponse.value(forKey: "CrmContactId")!)".count > 0 {
                                        
                                        self.strCrmContactId = "\(dictResponse.value(forKey: "CrmContactId")!)" == "" ? "" : "\(dictResponse.value(forKey: "CrmContactId")!)"
                                        self.txtFldContact.text = "\(dictResponse.value(forKey: "ContactName")!)"
                                        
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strAccountId.count > 0 {
                                            self.callApiToGetAddressViaAccountIdBackGround(strAccountIdLocal: self.strAccountId ,strType : "sadf")
                                        }
                                    }
                                    
                                    if (isInternetAvailable()){
                                        if self.strCrmContactId.count > 0 {
                                            self.callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal: self.strCrmContactId, strType: "")
                                        }
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }
                    
                }
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }
    
    func AddLeadOpportunityAPI(dictToSend : NSDictionary , strType : String) {
        
        if(isInternetAvailable()){
            
            var jsonString = String()
            
            if(JSONSerialization.isValidJSONObject(dictToSend) == true)
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                
                print(jsonString)
                
            }
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddLeadOpportunity
            
            let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            
            let strTypee = "SearchCrmContactsNew"
            
            Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
                
                if(success)
                {
                    
                    let dictResponse = (response as NSDictionary?)!
                    
                    if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                        
                        let arrOfKey = dictResponse.allKeys as NSArray
                        
                        if arrOfKey.contains("WebLeadId") || arrOfKey.contains("OpportunityId") {
                            
                            
                            
                            
                            // CrmCompanyId es mai strRefId aa ri hai jo bhejna hai wapis
                            
                            if (self.strGlobalNameAudio.count == 0) && (self.arrOfImages.count == 0) {
                                
                                self.responseOnUpload()
                                
                            } else {
                                
                                // Upload all images and audio here
                                
                                if self.arrOfImages.count > 0 {
                                    
                                    self.imageCount = 0
                                    
                                    if self.strRefType == "WebLead" {
                                        
                                        self.strRefId = "\((dictResponse as NSDictionary?)!.value(forKey: "WebLeadId")!)"
                                        
                                    }else{
                                        
                                        self.strRefId = "\((dictResponse as NSDictionary?)!.value(forKey: "OpportunityId")!)"
                                        
                                    }
                                    
                                    self.dispatchGroup.enter()
                                    
                                    self.callToUploadImage()
                                    
                                }
                                
                                if self.strGlobalNameAudio.count > 0 {
                                    
                                    if self.strRefType == "WebLead" {
                                        
                                        self.strRefId = "\((dictResponse as NSDictionary?)!.value(forKey: "WebLeadId")!)"
                                        
                                    }else{
                                        
                                        self.strRefId = "\((dictResponse as NSDictionary?)!.value(forKey: "OpportunityId")!)"
                                        
                                    }
                                    
                                    self.dispatchGroup.enter()
                                    
                                    var  strAudioName = ""
                                    if(nsud.value(forKey: "AudioNameDocument") != nil){
                                        strAudioName = "\(nsud.value(forKey: "AudioNameDocument")!)"
                                    }
                                    let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                                    let localUrl = documentDirectory!.appendingPathComponent(strAudioName)
                                    
                                    if FileManager.default.fileExists(atPath: localUrl.path){
                                        if let cert = NSData(contentsOfFile: localUrl.path) {
                                            self.globalAudioData = cert as Data
                                            self.strGlobalNameAudio = strAudioName
                                        }
                                    }
                                    
                                    self.uploadAudioFile(strMediaType: "Audio", strRefIdd: self.strRefId, strMediaName: self.strGlobalNameAudio, mediaData: self.globalAudioData)
                                    
                                }
                                
                                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                                    
                                    self.responseOnUpload()
                                    
                                })
                                
                            }
                            
                        }else {
                            
                            self.loader.dismiss(animated: false) {
                                
                                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                                
                            }
                            
                        }
                        
                    }else {
                        
                        self.loader.dismiss(animated: false) {
                            
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                            
                        }
                        
                    }
                    
                }else {
                    
                    self.loader.dismiss(animated: false) {
                        
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }
                    
                }
                
            }
            
        }else{
            
            self.loader.dismiss(animated: false) {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }
    
    
    // MARK: -----------------------------------Button Actions----------------------------------------
    @IBAction func action_on_Lead_or_Opportunity
    (_ sender: UIButton) {
        //Deepak.s
        if sender.tag == 0 {
            isLead = true
            btnLead.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            btnOpportunity.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        }else {
            isLead = false
            btnLead.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            btnOpportunity.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        }
        self.creatUDFView()
    }
    
    @IBAction func action_OnExistingNewCustomer(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if(sender == btnExisting){
            self.viewExistingCustomer.backgroundColor = .init(hex: "E5E5E5")
            self.viewNewCustomer.backgroundColor = .clear
            self.btnExistingCustomer.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            self.btnNewCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            
            self.strAccountId = ""
            self.txtFldAccount.text = ""
            self.strCustomerAddressId = ""
            heightConstraint_ViewAccount.constant = DeviceType.IS_IPAD ? 60.0 : 40.0
            topConstraint_ViewAccount.constant = DeviceType.IS_IPAD ? 15.0 : 08.0
            
        }else{
            self.viewExistingCustomer.backgroundColor = .clear
            self.viewNewCustomer.backgroundColor = .init(hex: "E5E5E5")
            self.btnExistingCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            self.btnNewCustomer.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            
            self.strAccountId = ""
            self.txtFldAccount.text = ""
            self.strCustomerAddressId = ""
            
            heightConstraint_ViewAccount.constant = 0.0
            topConstraint_ViewAccount.constant = 0.0
            
        }
        
    }
    
    @IBAction func action_Type(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        var keys = NSArray()
        var values = NSArray()
        keys = ["Name"
        ]
        values = ["Residential"
        ]
        let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        var keys1 = NSArray()
        var values1 = NSArray()
        keys1 = ["Name"
        ]
        values1 = ["Commercial"
        ]
        let dictToSend1 = NSDictionary.init(objects: values1 as! [Any], forKeys: keys1 as! [NSCopying])
        
        let arrayfilter = NSMutableArray()
        arrayfilter.add(dictToSend)
        arrayfilter.add(dictToSend1)
        
        if(arrayfilter.count != 0){
            
            sender.tag = 59
            openTableViewPopUp(tag: 59, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
            
        }
        
    }
    
    @IBAction func action_Account(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        strBtnTag = "0"
        self.gotoAssociateAccount()
        
    }
    
    @IBAction func action_Contact(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        strBtnTag = "1"
        self.gotoAssociateContact()
        
    }
    
    @IBAction func action_Company(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        strBtnTag = "2"
        self.gotoAssociateCompany()
        
    }
    
    @IBAction func action_Address(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if arrAddress.count > 0  {
            self.openTableViewPopUp(tag: 63, ary: (self.arrAddress as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableCustomerAddress, viewcontrol: self)
        }
    }
    
    @IBAction func action_Source(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let defsLogindDetail = UserDefaults.standard
        
        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
            
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                
                var arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                arrOfData = returnFilteredArray(array: arrOfData)
                if(arrOfData.count != 0){
                    
                    sender.tag = 100
                    
                    goToNewSelectionVC(arrOfData: arrOfData, tag: 100, titleHeader: "Source")
                    
                    //openTableViewPopUp(tag: 60, ary: (arrOfData as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: arrSelectedSource)
                    
                }
                
            }
            
        }
        
    }
    
    @IBAction func action_AssignTo(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
        let resultPredicate = NSPredicate(format: "IsActive == true")
        let arrayfilter = aryTemp.filtered(using: resultPredicate)
        if(arrayfilter.count != 0){
            sender.tag = 61
            openTableViewPopUp(tag: 61, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }
        
    }
    
    @IBAction func action_Self(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if(sender.currentImage == UIImage(named: "checked.png"))
        {
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
            
            if strAssignedToId == "\(dictLoginData.value(forKeyPath: "EmployeeId")!)" {
                
                strAssignedToId = ""
                txtFldAssignTo.text = ""
                
            }
            
        }
        else
        {
            sender.setImage(UIImage(named: "checked.png"), for: .normal)
            strAssignedToId = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            txtFldAssignTo.text = "\(dictLoginData.value(forKeyPath: "EmployeeName")!)"
        }
        
    }
    
    @IBAction func action_SelectService(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let defsLogindDetail = UserDefaults.standard
        
        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
            
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictLeadDetailMaster.value(forKey: "ServiceMasters") is NSArray {
                
                //let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
                var arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                //arrOfData = returnFilteredArray(array: arrOfData)
                arrOfData = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :"\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)" )
                if(arrOfData.count != 0){
                    let mainStoryboard = UIStoryboard(
                        name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect",
                        bundle: nil)
                    let controller = mainStoryboard.instantiateViewController(withIdentifier: "SelectServiceVC") as! SelectServiceVC
                    controller.strServiceIdPrimary = strPrimaryService
                    controller.strFromWhere = "AddLeadCRM"
                    controller.strBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
                    controller.arrayServicesSelected = arrOfAdditionalServiceSelected
                    self.navigationController?.present(controller, animated: false, completion: nil)
                    
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                }
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
            
        }
        
    }
    
    @IBAction func action_Urgency(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let defsLogindDetail = UserDefaults.standard
        
        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
            
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictLeadDetailMaster.value(forKey: "UrgencyMasters") is NSArray {
                
                //let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
                var arrOfData = (dictLeadDetailMaster.value(forKey: "UrgencyMasters") as! NSArray).mutableCopy() as! NSMutableArray
                arrOfData = returnFilteredArray(array: arrOfData)
                if(arrOfData.count != 0){
                    sender.tag = 62
                    openTableViewPopUp(tag: 62, ary: (arrOfData as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
                }
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
            
        }
        
    }
    
    @IBAction func action_Camera(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if arrOfImages.count >= 10 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: imageThree, viewcontrol: self)
            
        } else {
            
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
        }
    }
    
    @IBAction func action_Gallery(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if arrOfImages.count >= 10 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: imageThree, viewcontrol: self)
            
        } else {
            
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
        }
        
    }
    
    @IBAction func action_Audio(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        clickOnAudioBtn(sender: sender)
        
    }
    
    @IBAction func action_Submit(_ sender: UIButton) {
        //Deepak.s
        /*self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if isInternetAvailable() {
            
            if(validationOnAddLeadOppotunity(strType: enumRefTypeWebLead)){
                
                var localStrCustomerAddressId = ""
                var strBranchSysNameToSend = ""
                
                if strRefType == "WebLead" {
                    
                    localStrCustomerAddressId = ""
                    
                    if txtFldService.text!.count > 0 {
                        
                        strBranchSysNameToSend = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
                        
                    }
                    
                } else if btnNewCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") {
                    
                    localStrCustomerAddressId = ""
                    
                }else {
                    
                    localStrCustomerAddressId = (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CustomerAddressId")!)" : ""
                    
                }
                
                imageCount = 0
                
                var keys = NSArray()
                var values = NSArray()
                keys = ["CompanyKey",
                        "Title",
                        "FlowType",
                        "UrgencyId",
                        "Comment",
                        "PrimaryServiceId",
                        "AdditionalServiceIds",
                        "SourceIds",
                        "AccountId",
                        "CrmContactId",
                        "CrmCompanyId",
                        "ScheduleDate",
                        "ScheduleTime",
                        "FieldSalesPerson",
                        "AssignedToId",
                        "CustomerAddressId",
                        "Address1",
                        "CityName",
                        "StateId",
                        "ZipCode",
                        "CountryId",
                        "CreateNewAccount",
                        "RefType",
                        "BranchSysName","AddressPropertyTypeId"]
                values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                          (txtFldTitle.text?.count)! > 0 ? txtFldTitle.text! : "",
                          (txtFldType.text?.count)! > 0 ? txtFldType.text! : "",
                          strUrgencyId,
                          (txtFldComment.text?.count)! > 0 ? txtFldComment.text! : "",
                          strPrimaryService,
                          arrOfAdditionalService,
                          arrSourceId,
                          btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") ? strAccountId : "",
                          strCrmContactId,
                          strCrmCompanyId,
                          "",
                          "",
                          "",
                          strAssignedToId,
                          localStrCustomerAddressId,
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "Address1")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CityName")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "StateId")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "Zipcode")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CountryId")!)" : "",
                          btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") ? false : true,
                          strRefType,
                          strBranchSysNameToSend,"\(txtfldPropertyType.tag)" == "0" ? "" : "\(txtfldPropertyType.tag)"]
                
                let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    self.AddLeadOpportunityAPI(dictToSend: dictToSend, strType: self.strRefType)
                    
                })
                
            }
            
        } else {
            
            self.loader.dismiss(animated: false) {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }*/
        
        if isLead {
            self.createLead()
        }else {
            self.createOpportunity()
        }
        
    }
    //Deepak.s
   /* @IBAction func action_ScheduleNow(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if isInternetAvailable() {
            
            if(validationOnAddLeadOppotunity(strType: enumRefTypeLead)){
                
                var localStrCustomerAddressId = ""
                let strBranchSysNameToSend = ""
                
                if btnNewCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") {
                    
                    localStrCustomerAddressId = ""
                    
                }else {
                    
                    localStrCustomerAddressId = (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CustomerAddressId")!)" : ""
                    
                }
                
                imageCount = 0
                
                var keys = NSArray()
                var values = NSArray()
                keys = ["CompanyKey",
                        "Title",
                        "FlowType",
                        "UrgencyId",
                        "Comment",
                        "PrimaryServiceId",
                        "AdditionalServiceIds",
                        "SourceIds",
                        "AccountId",
                        "CrmContactId",
                        "CrmCompanyId",
                        "ScheduleDate",
                        "ScheduleTime",
                        "FieldSalesPerson",
                        "AssignedToId",
                        "CustomerAddressId",
                        "Address1",
                        "CityName",
                        "StateId",
                        "ZipCode",
                        "CountryId",
                        "CreateNewAccount",
                        "RefType",
                        "BranchSysName","AddressPropertyTypeId"]
                
                values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                          (txtFldTitle.text?.count)! > 0 ? txtFldTitle.text! : "",
                          (txtFldType.text?.count)! > 0 ? txtFldType.text! : "",
                          strUrgencyId,
                          (txtFldComment.text?.count)! > 0 ? txtFldComment.text! : "",
                          strPrimaryService,
                          arrOfAdditionalService,
                          arrSourceId,
                          btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") ? strAccountId : "",
                          strCrmContactId,
                          strCrmCompanyId,
                          "",
                          "",
                          "",
                          strAssignedToId,
                          localStrCustomerAddressId,
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "Address1")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CityName")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "StateId")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "Zipcode")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CountryId")!)" : "",
                          btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") ? false : true,
                          strRefType,
                          strBranchSysNameToSend,"\(txtfldPropertyType.tag)" == "0" ? "0" : "\(txtfldPropertyType.tag)"]
                
                let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Lead-Prospect" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleOpportunityVC") as! ScheduleOpportunityVC
                
                
                let dictMutableOppData = NSMutableDictionary()
                dictMutableOppData.addEntries(from: dictToSend as! [AnyHashable : Any])
                controller.dictOppData = dictMutableOppData
                controller.strFieldSalesPersonId = strAssignedToId
                controller.strGlobalNameAudio = strGlobalNameAudio
                controller.arrOfImages = arrOfImages
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
        } else {
            
            self.loader.dismiss(animated: false) {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }*/
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func createLead() {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if isInternetAvailable() {
            
            if(validationOnAddLeadOppotunity(strType: enumRefTypeWebLead)){
                
                var localStrCustomerAddressId = ""
                var strBranchSysNameToSend = ""
                //Deepak.s
                let jsonStringUDF = "\(json(from:aryUDFSaveData as Any) ?? "")"
                //********
                
                if strRefType == "WebLead" {
                    
                    localStrCustomerAddressId = ""
                    strBranchSysNameToSend = "\(dictLoginData.value(forKey: "EmployeeBranchSysName") ?? "")"
                    
//                    if txtFldService.text!.count > 0 {
//
//                        strBranchSysNameToSend = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
//
//                    }
                    
                } else if btnNewCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") {
                    
                    localStrCustomerAddressId = ""
                    
                }else {
                    
                    localStrCustomerAddressId = (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CustomerAddressId")!)" : ""
                    
                }
                
                imageCount = 0
                
                var keys = NSArray()
                var values = NSArray()
                keys = ["CompanyKey",
                        "Title",
                        "FlowType",
                        "UrgencyId",
                        "Comment",
                        "PrimaryServiceId",
                        "AdditionalServiceIds",
                        "SourceIds",
                        "AccountId",
                        "CrmContactId",
                        "CrmCompanyId",
                        "ScheduleDate",
                        "ScheduleTime",
                        "FieldSalesPerson",
                        "AssignedToId",
                        "CustomerAddressId",
                        "Address1",
                        "CityName",
                        "StateId",
                        "ZipCode",
                        "CountryId",
                        "CreateNewAccount",
                        "RefType",
                        "BranchSysName",
                        "AddressPropertyTypeId",
                        "UserDefinedFields"]
                values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                          (txtFldTitle.text?.count)! > 0 ? txtFldTitle.text! : "",
                          (txtFldType.text?.count)! > 0 ? txtFldType.text! : "",
                          strUrgencyId,
                          (txtFldComment.text?.count)! > 0 ? txtFldComment.text! : "",
                          strPrimaryService,
                          arrOfAdditionalService,
                          arrSourceId,
                          btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") ? strAccountId : "",
                          strCrmContactId,
                          strCrmCompanyId,
                          "",
                          "",
                          "",
                          strAssignedToId,
                          localStrCustomerAddressId,
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "Address1")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CityName")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "StateId")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "Zipcode")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CountryId")!)" : "",
                          btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") ? false : true,
                          strRefType,
                          strBranchSysNameToSend,
                          "\(txtfldPropertyType.tag)" == "0" ? "" : "\(txtfldPropertyType.tag)",
                          jsonStringUDF]
                
                let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    self.AddLeadOpportunityAPI(dictToSend: dictToSend, strType: self.strRefType)
                    
                })
                
            }
            
        } else {
            
            self.loader.dismiss(animated: false) {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
        
    }
    
    func createOpportunity() {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if isInternetAvailable() {
            
            if(validationOnAddLeadOppotunity(strType: enumRefTypeLead)){
                
                var localStrCustomerAddressId = ""
                let strBranchSysNameToSend = ""
                //Deepak.s
                let jsonStringUDF = "\(json(from:aryUDFSaveData as Any) ?? "")"
                //********
                
                if btnNewCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") {
                    
                    localStrCustomerAddressId = ""
                    
                }else {
                    
                    localStrCustomerAddressId = (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CustomerAddressId")!)" : ""
                    
                }
                
                imageCount = 0
                
                var keys = NSArray()
                var values = NSArray()
                keys = ["CompanyKey",
                        "Title",
                        "FlowType",
                        "UrgencyId",
                        "Comment",
                        "PrimaryServiceId",
                        "AdditionalServiceIds",
                        "SourceIds",
                        "AccountId",
                        "CrmContactId",
                        "CrmCompanyId",
                        "ScheduleDate",
                        "ScheduleTime",
                        "FieldSalesPerson",
                        "AssignedToId",
                        "CustomerAddressId",
                        "Address1",
                        "CityName",
                        "StateId",
                        "ZipCode",
                        "CountryId",
                        "CreateNewAccount",
                        "RefType",
                        "BranchSysName",
                        "AddressPropertyTypeId",
                        "UserDefinedFields"]
                
                values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                          (txtFldTitle.text?.count)! > 0 ? txtFldTitle.text! : "",
                          (txtFldType.text?.count)! > 0 ? txtFldType.text! : "",
                          strUrgencyId,
                          (txtFldComment.text?.count)! > 0 ? txtFldComment.text! : "",
                          strPrimaryService,
                          arrOfAdditionalService,
                          arrSourceId,
                          btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") ? strAccountId : "",
                          strCrmContactId,
                          strCrmCompanyId,
                          "",
                          "",
                          "",
                          strAssignedToId,
                          localStrCustomerAddressId,
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "Address1")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CityName")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "StateId")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "Zipcode")!)" : "",
                          (dictAddress.count) > 0 ? "\(dictAddress.value(forKey: "CountryId")!)" : "",
                          btnExistingCustomer.currentImage == UIImage(named: "RadioButton-Selected.png") ? false : true,
                          strRefType,
                          strBranchSysNameToSend,"\(txtfldPropertyType.tag)" == "0" ? "0" : "\(txtfldPropertyType.tag)",
                          jsonStringUDF]
                
                let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Lead-Prospect" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleOpportunityVC") as! ScheduleOpportunityVC
                
                
                let dictMutableOppData = NSMutableDictionary()
                dictMutableOppData.addEntries(from: dictToSend as! [AnyHashable : Any])
                controller.dictOppData = dictMutableOppData
                controller.strFieldSalesPersonId = strAssignedToId
                controller.strGlobalNameAudio = strGlobalNameAudio
                controller.arrOfImages = arrOfImages
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
        } else {
            
            self.loader.dismiss(animated: false) {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }
    
    @IBAction func action_Back(_ sender: Any) {
        // test
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        //test
        self.navigationController?.popViewController(animated: false)
        // testt
        //test
    }
    @IBAction func action_ClearAddress(_ sender: Any) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        txtFldAddress.text = ""
        self.txtFldAddress.isUserInteractionEnabled = true
        strCustomerAddressId = ""
        strAddressLine1 = ""
        strCityName = ""
        strZipCode = ""
        strStateId = ""
        strAddressLine1 = ""
        
    }
    
    @IBAction func action_ClearAccount(_ sender: Any) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtFldAccount.text!.count > 0 {
            
            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtFldAccount.text = ""
                self.strAccountId = ""
                self.btnCompany.isUserInteractionEnabled = true
                
                self.arrOfAccountAddress = NSArray()
                
                self.mergeAllAddress()
                
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func action_ClearContact(_ sender: Any) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtFldContact.text!.count > 0 {
            
            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtFldContact.text = ""
                self.strCrmContactId = ""
                
                self.dictOfContactAddress = NSDictionary()
                
                self.mergeAllAddress()
                
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func action_ClearCompany(_ sender: Any) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtFldCompany.text!.count > 0 {
            
            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtFldCompany.text = ""
                self.strCrmCompanyId = ""
                
                self.dictOfCompanyAddress = NSDictionary()
                
                self.mergeAllAddress()
                
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func action_DashBaord(_ sender: Any) {
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    @IBAction func action_AddAddress(_ sender: Any) {
        
        let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
        let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
        vc.tag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: false, completion: {})
        
    }
    
    @IBAction func action_SelectPropertyType(_ sender: Any) {
        if(self.getAddressTypeFromMasterSalesAutomation().count != 0){
           // openTableViewPopUp(tag: 65, ary: self.getAddressTypeFromMasterSalesAutomation() , aryselectedItem: self.aryPropertySelected)
            openTableViewPopUp(tag: 65, ary: self.getAddressTypeFromMasterSalesAutomation() , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Property type not found!!!", viewcontrol: self)
            
        }
        
    }
}

// MARK: -
// MARK: - -----------------------Source Selction Delgates

extension AddLeadProspectVC : SelectGlobalVCDelegate{
    
    func getDataSelectGlobalVC(dictData: NSMutableDictionary, tag: Int) {
        
        if tag == 100 {
            
            if(dictData.count == 0){
                
                arrSourceId = NSMutableArray()
                arrSelectedSource = NSMutableArray()
                txtFldSource.text = ""
                
            }else{
                
                let arySource = dictData.object(forKey: "source")as! NSArray
                print(arySource)
                var strName = ""
                arrSourceId = NSMutableArray()
                
                arrSelectedSource = NSMutableArray()
                
                arrSelectedSource = arySource.mutableCopy() as! NSMutableArray
                
                for item in arySource{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                    arrSourceId.add("\((item as AnyObject).value(forKey: "SourceId")!)")
                }
                if strName == ""
                {
                    strName = ""
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtFldSource.text = strName
                
            }
            
        }
        
    }
    
}

// MARK: -
// MARK: - -----------------------Add Address Delgates

extension AddLeadProspectVC : AddNewAddressiPhoneDelegate{
    
    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {
        
        print(dictData)
        
        self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
        self.strCustomerAddressId = ""
        
        /*if self.txtFldAddress.text?.count != 0 {
         
         let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
         
         let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
         
         self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
         self.strCustomerAddressId = ""
         
         })
         
         let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
         
         
         
         })
         
         alertCOntroller.addAction(alertAction)
         alertCOntroller.addAction(alertActionCancel)
         
         self.present(alertCOntroller, animated: true, completion: nil)
         
         }else{
         
         self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
         self.strCustomerAddressId = ""
         
         }*/
        
    }
    
}

// MARK: -
// MARK: - Selection CustomTableView
extension AddLeadProspectVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        // Company Hide when type is Residential
        if(tag == 59)
        {
            if(dictData.count == 0){
                txtFldType.text = ""
                heightConstraint_ViewCompany.constant = 0.0
                topConstraint_ViewCompany.constant = 0.0
            }else{
                txtFldType.text = "\(dictData.value(forKey: "Name")!)"
                if("\(dictData.value(forKey: "Name")!)" == "Residential"){
                    heightConstraint_ViewCompany.constant = 0.0
                    topConstraint_ViewCompany.constant = 0.0
                            
                    self.txtFldCompany.text = ""
                    self.strCrmCompanyId = ""
                    self.dictOfCompanyAddress = NSDictionary()
                }else{
                    heightConstraint_ViewCompany.constant = DeviceType.IS_IPAD ? 60.0 : 40.0
                    topConstraint_ViewCompany.constant = DeviceType.IS_IPAD ? 15.0 : 8.0
                }
            }
        }
        else if(tag == 60)
        {
            if(dictData.count == 0){
                
                arrSourceId = NSMutableArray()
                arrSelectedSource = NSMutableArray()
                txtFldSource.text = ""
                
            }else{
                
                let arySource = dictData.object(forKey: "source")as! NSArray
                print(arySource)
                var strName = ""
                arrSourceId = NSMutableArray()
                
                arrSelectedSource = NSMutableArray()
                
                arrSelectedSource = arySource.mutableCopy() as! NSMutableArray
                
                for item in arySource{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                    arrSourceId.add("\((item as AnyObject).value(forKey: "SourceId")!)")
                }
                if strName == ""
                {
                    strName = ""
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtFldSource.text = strName
                
            }
            // Source
        }
        else if(tag == 61)
        {
            if(dictData.count == 0){
                txtFldAssignTo.text = ""
                strAssignedToId = ""
                self.btnSelf.setImage(UIImage(named: "uncheck.png"), for: .normal)
            }else{
                txtFldAssignTo.text = "\(dictData.value(forKey: "FullName")!)"
                strAssignedToId = "\(dictData.value(forKey: "EmployeeId")!)"
                
                if strAssignedToId != "\(dictLoginData.value(forKeyPath: "EmployeeId")!)" {
                    self.btnSelf.setImage(UIImage(named: "uncheck.png"), for: .normal)
                }
                
            }
            // Assigned to
        }
        else if(tag == 62)
        {
            if(dictData.count == 0){
                txtFldUrgency.text = ""
                strUrgencyId = ""
            }else{
                
                txtFldUrgency.text = "\(dictData.value(forKey: "Name")!)"
                strUrgencyId = "\(dictData.value(forKeyPath: "UrgencyId")!)"
                
            }
            // urgency
        }
        else if(tag == 63)
        {
            if(dictData.count == 0){
                txtFldAddress.text = ""
                self.txtFldAddress.isUserInteractionEnabled = true
                strCustomerAddressId = ""
                
            }

           
            else{
                if !("\(dictData.value(forKey: "Name") ?? "")" == ""){
                    aryPropertySelected = NSMutableArray()
                    aryPropertySelected.add(dictData)
                    txtfldPropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
                    txtfldPropertyType.tag = Int("\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")" == "" ? "0" : "\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")")!
                
                }
             
               
                
                txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                self.txtFldAddress.isUserInteractionEnabled = false
                //strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                
                let arrTemp = dictData.allKeys as NSArray
                
                if arrTemp.contains("CustomerAddressId") {
                    
                    strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId")!)"
                    
                }else{
                    
                    strCustomerAddressId = ""
                    
                }
                
            }
            // Address
        }
        // For PropertyType
        else if(tag == 65)
        {
            if(dictData.count == 0){
                txtfldPropertyType.text = ""
                txtfldPropertyType.tag = 0
                aryPropertySelected = NSMutableArray()
            }else{
                aryPropertySelected = NSMutableArray()
                aryPropertySelected.add(dictData)
                txtfldPropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
                txtfldPropertyType.tag = Int("\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")" == "" ? "0" : "\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")")!
            }
        }

    }
    
}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension AddLeadProspectVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        //let imageData = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.jpegData(compressionQuality:1.0)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            let imageResized = Global().resizeImageGloballl(img)
            self.arrOfImages.add(imageResized!)
            self.imgViewCollection.reloadData()
            
        }
        //let data = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.pngData() as NSData?
        //info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
    }
}


// MARK: - -----------------------------------Collection View Delegates -----------------------------------

extension AddLeadProspectVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrOfImages.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        
        return DeviceType.IS_IPAD ? CGSize(width: 130, height: 130) :  CGSize(width: 75, height: 75)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeadProspectImgCell", for: indexPath as IndexPath) as! LeadProspectImgCell
        
        cell.imgView.image = arrOfImages[indexPath.row] as? UIImage
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: "Delete", style: .default , handler:{ (UIAlertAction)in
            self.arrOfImages.removeObject(at: indexPath.row)
            self.imgViewCollection.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "View Image", style: .default , handler:{ (UIAlertAction)in
            
            //let image: UIImage = UIImage(named: "NoImage.jpg")!
            let imageView = UIImageView(image: self.arrOfImages[indexPath.row] as? UIImage)
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            self.present(testController!, animated: false, completion: nil)
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------

extension AddLeadProspectVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtFldAddress){
            
            self.scrollView.isScrollEnabled = false
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtFldAddress){
            
            self.scrollView.isScrollEnabled = true
            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if(textField == txtFldAddress){
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()
                    
                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = txtFldAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()
                
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtFldAddress){
            
            let txtAfterUpdate:NSString = txtFldAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            
            //            self.getPlaces(with: parameters) {
            //                self.places = $0
            //            }
            return true
        }
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
extension AddLeadProspectVC {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }
                else
                {
                    /* for item in self.scrollView.subviews {
                     if(item is UITableView){
                     item.removeFromSuperview()
                     }
                     }
                     self.imgPoweredBy.removeFromSuperview()
                     self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                     self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                     
                     self.scrollView.addSubview(self.tableView)
                     self.scrollView.addSubview(self.imgPoweredBy)*/
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    
                    // self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    
                    if DeviceType.IS_IPHONE_6_OR_LESS {
                        
                        self.tableView.frame = CGRect(x: self.txtFldType.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldType.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)
                        
                        
                    } else {
                        
                        self.tableView.frame = CGRect(x: self.txtFldType.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldType.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)
                        
                    }
                    
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)
                }
            }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension AddLeadProspectVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            //self.txtFldAddress.text = value.formattedAddress
            self.txtFldAddress.text = addressFormattedByGoogle(value: value)
            self.strCustomerAddressId = ""
            
            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()
        
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}


// MARK: -
// MARK: - AddLeadAssociateProtocol
extension AddLeadProspectVC: AddLeadAssociateProtocol
{
    func getDataAddLeadAssociateProtocol(notification: NSDictionary, tag: String) {
        print(notification)
        
        self.strBtnTag = tag
        
        if let dict = notification as NSDictionary? {
            
            var strEntityId = ""
            var strEntityType = ""
            var strEntityName = ""
            
            let arrOfKeys = dict.allKeys as NSArray
            
            if self.strBtnTag == "0"{
                
                if arrOfKeys.contains("AccountId") {
                    
                    // Account
                    strEntityId = "\(dict.value(forKey: "AccountId")!)"
                    strEntityType = enumRefTypeAccount
                    strEntityName = "\(dict.value(forKey: "AccountName")!)"
                    
                    let strName = "\(dict.value(forKey: "AccountName") ?? "")"
                    
                    if strName.count == 0 {
                        
                        let strCrmContactName = "\(dict.value(forKey: "CrmContactName") ?? "")"
                        
                        if strCrmContactName.count == 0 {
                            
                            let strCrmCompanyName = "\(dict.value(forKey: "CrmCompanyName") ?? "")"
                            
                            if strCrmCompanyName.count == 0 {
                                
                                strEntityName = " "
                                
                            }else{
                                
                                strEntityName = "\(strCrmCompanyName)"
                                
                            }
                            
                        }else{
                            
                            strEntityName = "\(strCrmContactName)"
                            
                        }
                        
                    }else{
                        
                        strEntityName = "\(strName)"
                        
                    }
                    
                }
                
                
            } else if self.strBtnTag == "1" {
                // Contact
                
                strEntityId = "\(dict.value(forKey: "CrmContactId")!)"
                strEntityType = enumRefTypeCrmContact
                strEntityName = Global().strFullName(dict as? [AnyHashable : Any])
                
            } else if self.strBtnTag == "2" {
                // Company
                strEntityId = "\(dict.value(forKey: "CrmCompanyId")!)"
                strEntityType = enumRefTypeCrmCompany
                strEntityName = "\(dict.value(forKey: "Name")!)"
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.callGetAssociatedEntitiesForLeadAssociations(EntityId: strEntityId, EntityType: strEntityType, editStatus: true, userInfo: dict)
                
            }
            
        }
        
    }
    
}
