//
//  ConvertToOppoetunity_iPhoneVC.swift
//  DPS
//  peSTream 2020
//  Created by Navin Patidar on 24/04/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ConvertToOppoetunity_iPhoneVC: UIViewController {
    
    @IBOutlet weak var btnCommercial: UIButton!
    @IBOutlet weak var btnResidential: UIButton!
    @IBOutlet weak var btnSchedulenow: UIButton!
    @IBOutlet weak var btnMoreInfo: UIButton!
    @IBOutlet weak var btnIsTax: UIButton!
    
    @IBOutlet weak var txtAccountNumber: ACFloatingTextField!
    @IBOutlet weak var txtCompanyName: ACFloatingTextField!
    @IBOutlet weak var txtContactName: ACFloatingTextField!
    @IBOutlet weak var txtServiceAddress: ACFloatingTextField!
    @IBOutlet weak var txtSelectBranch: ACFloatingTextField!
    @IBOutlet weak var txtselectDepartment: ACFloatingTextField!
    @IBOutlet weak var txtSelectSource: ACFloatingTextField!
    @IBOutlet weak var txtSelectService: ACFloatingTextField!
    @IBOutlet weak var txtFiledSalesPerson: ACFloatingTextField!
   
    @IBOutlet weak var txtCounty: ACFloatingTextField!
    @IBOutlet weak var txtSchoolDistric: ACFloatingTextField!
    @IBOutlet weak var txtAdrressSubType: ACFloatingTextField!
    @IBOutlet weak var txtTaxCOde: ACFloatingTextField!
    
    @IBOutlet weak var height_ScheduleNowView: NSLayoutConstraint!
    @IBOutlet weak var height_ServiceAddress: NSLayoutConstraint!
    @IBOutlet weak var height_IstaxExempt: NSLayoutConstraint!
    @IBOutlet weak var width_ServiceAddressCross: NSLayoutConstraint!
    @IBOutlet weak var txtfldPropertyType: ACFloatingTextField!
    @IBOutlet weak var txtfldUrgencyType: ACFloatingTextField!


    @IBOutlet weak var btnThirdParty: UIButton!


    var tagIsTax = 1 , tagIsSchedule = 1 , tagMoreInfo = 1 ,tagForSelection = 0
    
    var strBranchSysName = "" , strDepartmentSysName = "" , strSelectedTaxCodeSysName =  ""
    var strCrmContactId = "" , strCrmCompanyId = "" ,strFiledSalesPersonID = "" ,strPrimaryService = "" ,strAccountId = ""

    
    var arrTaxCode = NSMutableArray() , arySelectSubAddress = NSMutableArray() , arrSelectedSource = NSMutableArray() , aryDepartment = NSMutableArray(), arySource = NSMutableArray() , aryAddress = NSMutableArray() , aryOfService = NSMutableArray() , aryServiceMaster = NSMutableArray() , arrOfAdditionalServiceSelected = NSMutableArray()
    
    var dictLoginData = NSDictionary(), dictLeadDetail = NSDictionary() , dictAccountDetail = NSDictionary()
    
    var strTypeToConvert = "" , strAddressLine1 = "" , strAddressline2 = "" , strCityName = "" , strStateId = "", strZipcode = "" , strThirdPartyAccountNo = "" ,strAddressId = ""
    
    var dictOfContactAddress = NSDictionary()
    var dictOfCompanyAddress = NSDictionary()
    var arrOfAccountAddress = NSArray()
    var aryPropertySelected = NSMutableArray()
    var aryUrgencySelected = NSMutableArray()

    var loader = UIViewController()
    //MARK:---------Time renge-----------
    @IBOutlet weak var tf_Date: ACFloatingTextField!
    @IBOutlet weak var tf_Time: ACFloatingTextField!
    @IBOutlet weak var tf_Range: ACFloatingTextField!
    @IBOutlet weak var height_tf_Range: NSLayoutConstraint!
    @IBOutlet weak var btn_Specific: UIButton!
    @IBOutlet weak var btn_TimeRange: UIButton!
    var aryTimeRange = NSArray() ,arySelectedRange = NSMutableArray()
    
    
    // MARK: - ----------- Google Address Code ----------------
       @IBOutlet weak var scrollView: UIScrollView!

       @IBOutlet weak var tableView: UITableView!
       private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
       private var placeType: PlaceType = .all
       private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
       private var radius: Double = 0.0
       private var strictBounds: Bool = false
       var txtAddressMaxY = CGFloat()
       var imgPoweredBy = UIImageView()
       private var places = [Place]() {
           didSet { tableView.reloadData() }
       }
    // MARK:
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        setUpInitialData()
        self.btnCommercial.setImage(UIImage(named: "check_CRM"), for: .normal)
        self.btnResidential.setImage(UIImage(named: "uncheck_CRM"), for: .normal)
        self.btnSchedulenow.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        self.btnIsTax.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        self.btnMoreInfo.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        hideShowAccordingToCheckBOX()
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotificationPost(_:)), name: NSNotification.Name(rawValue: "AssociatedConvertOpportunityCRM_Notification"), object: nil)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "SelectedServicesConvertOpportunityCRM_Notification"),
                                               object: nil,
                                               queue: nil,
                                               using:catchNotification)
        
        getServiceFromMaster()
     
          // set current date
       
             
//        if txtScheduleDate.text?.count == 0 {
//            txtScheduleDate.text = Global().strGetCurrentDate(inFormat: "MM/dd/yyyy hh:mm a")
//        }
        if tf_Date.text?.count == 0 {
            tf_Date.text = Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
        }
        if tf_Time.text?.count == 0 {
            tf_Time.text = Global().strGetCurrentDate(inFormat: "hh:mm a")
        }
        if(nsud.value(forKey: "MasterSalesTimeRange") != nil){
            if(nsud.value(forKey: "MasterSalesTimeRange") is NSArray){
                self.aryTimeRange = nsud.value(forKey: "MasterSalesTimeRange") as! NSArray
            }
        }
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
//
//        if txtTime.text?.count == 0 {
//
//            // set current time
//
//            txtTime.text = Global().strGetCurrentDate(inFormat: "hh:mm a")
//
//        }

        //Schedule Third party Button
        if isScheduleThirdParty() {
            btnThirdParty.isHidden = false
        }else{
            btnThirdParty.isHidden = true
        }
        btnThirdParty.setImage(UIImage(named: "check_box_1New.png"), for: .normal)

        
    }
    
    // MARK:
    // MARK: ----------Extra Function------------
    func getAddressTypeFromMasterSalesAutomation() -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    func getPropertyTypeNameFromID_FromMasterSalesAutomation(id : String) -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")) && "\((task as! NSDictionary).value(forKey: "AddressPropertyTypeId")!)".contains("\(id)")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    
    func getUrgencyNameFromID_FromMaster(id : String) -> NSMutableArray {
        if(nsud.value(forKey: "LeadDetailMaster") != nil){
            let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if(dictMaster.value(forKey: "UrgencyMasters") != nil){
                let aryTemp = (dictMaster.value(forKey: "UrgencyMasters") as! NSArray).filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")) && "\((task as! NSDictionary).value(forKey: "UrgencyId")!)".contains("\(id)")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }

    func goToNewSelectionVC(arrOfData : NSMutableArray, tag : Int, titleHeader : String) {
        
        if(arrOfData.count != 0){
            
              let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectGlobalVC") as! SelectGlobalVC
            controller.titleHeader = titleHeader
            controller.arrayData = arrOfData
            controller.arraySelectionData = arrOfData
            controller.arraySelected = arrSelectedSource
            controller.delegate = self
            controller.tag = tag
            self.navigationController?.present(controller, animated: false, completion: nil)
            
        }else {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    func mergeAllAddress() {
        
        self.aryAddress = NSMutableArray()
        
        if arrOfAccountAddress.count > 0 {
            
            self.aryAddress.addObjects(from: arrOfAccountAddress as! [Any])
            
        }
        /*if dictOfContactAddress.count > 0 {
            
            let zipCodeLocal = "\(self.dictOfContactAddress.value(forKey: "ZipCode")!)"
            
            if (zipCodeLocal.count > 0) {
                
                /*if !self.aryAddress.contains{ $0 == self.dictOfContactAddress } {
                    self.aryAddress.add(dictOfContactAddress)
                }*/
                
                if !checkIfAddressAlreadyPresent(strAddress: Global().strCombinedAddress(dictOfContactAddress as? [AnyHashable : Any])) {
                    self.aryAddress.add(dictOfContactAddress)
                }

            }
            
        }
        
        if dictOfCompanyAddress.count > 0 {
            
            let zipCodeLocal = "\(self.dictOfCompanyAddress.value(forKey: "ZipCode")!)"
            
            if (zipCodeLocal.count > 0) {
                
                /*if !self.aryAddress.contains{ $0 == self.dictOfCompanyAddress } {
                    self.aryAddress.add(dictOfCompanyAddress)
                }*/
                
                if !checkIfAddressAlreadyPresent(strAddress: Global().strCombinedAddress(dictOfCompanyAddress as? [AnyHashable : Any])) {
                    self.aryAddress.add(dictOfContactAddress)
                }

            }
            
        }*/
        
    }
    
    func checkIfAddressAlreadyPresent(strAddress : String) -> Bool {
        
        var isPresent = false
        
        for k in 0 ..< aryAddress.count {
            
            if aryAddress[k] is NSDictionary {
                
                let dictOfData = aryAddress[k] as! NSDictionary
                
                let addressLocal = Global().strCombinedAddress(dictOfData as? [AnyHashable : Any])
                
                if addressLocal == strAddress {
                    
                    isPresent = true
                    
                    break
                    
                }
                
            }
            
        }
        
        return isPresent
        
    }
    
    func setUpInitialData() {
        print(dictLeadDetail)
        
        
        let propertyTypeID =  "\(dictLeadDetail.value(forKey: "AddressPropertyTypeId")!)"
        self.aryPropertySelected = NSMutableArray()
        self.aryPropertySelected = getPropertyTypeNameFromID_FromMasterSalesAutomation(id: propertyTypeID)
        if(self.aryPropertySelected .count != 0){
            let dictData = self.aryPropertySelected.object(at: 0)as! NSDictionary
            txtfldPropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
            txtfldPropertyType.tag = Int("\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")")!
        }

        let urgencyID =  "\(dictLeadDetail.value(forKey: "UrgencyId")!)"
        self.aryUrgencySelected = NSMutableArray()
        self.aryUrgencySelected = getUrgencyNameFromID_FromMaster(id: urgencyID)
        if(self.aryUrgencySelected .count != 0){
            let dictData = self.aryUrgencySelected.object(at: 0)as! NSDictionary
            txtfldUrgencyType.text = "\(dictData.value(forKey: "Name") ?? "")"
            txtfldUrgencyType.tag = Int("\(dictData.value(forKey: "UrgencyId") ?? "0")")!
        }
        
        
        
        if(strTypeToConvert == "New"){
            txtAccountNumber.text = ""
            strThirdPartyAccountNo = ""
            
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "BranchMastersAll") as! NSArray).mutableCopy() as! NSMutableArray
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let EmployeeBranchId = "\(dictLoginData.value(forKey: "EmployeeBranchId")!)"
            
            for item  in arrOfData {
                let strBranchMasterId = "\((item as AnyObject).value(forKey: "BranchMasterId")!)"
                if(strBranchMasterId == EmployeeBranchId){
                    self.aryDepartment = NSMutableArray()
                    self.aryDepartment = ((item as AnyObject).value(forKey: "Departments")as! NSArray).mutableCopy()as! NSMutableArray
                    strBranchSysName = "\((item as AnyObject).value(forKey: "SysName")!)"
                    txtSelectBranch.text = "\((item as AnyObject).value(forKey: "Name")!)"
                    break
                }
            }
        }else{
            txtAccountNumber.text =  "\(dictAccountDetail.value(forKey: "FirstName")!)"
            strThirdPartyAccountNo = "\(dictAccountDetail.value(forKey: "ThirdPartyAccountNumber")!)"
            strAccountId = "\(dictAccountDetail.value(forKey: "AccountId")!)"

            
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "BranchMastersAll") as! NSArray).mutableCopy() as! NSMutableArray
            
            if dictLeadDetail.value(forKey: "BranchesSysNames") is NSArray {
                
                let aryBranchesSysNames = dictLeadDetail.value(forKey: "BranchesSysNames")as! NSArray
                if(aryBranchesSysNames.count != 0){
                    let strsysNAme = "\((aryBranchesSysNames.object(at: 0)))"
                    for item  in arrOfData {
                        let branchSysName  = "\((item as AnyObject).value(forKey: "SysName")!)"
                        if(strsysNAme == branchSysName){
                            self.aryDepartment = NSMutableArray()
                            self.aryDepartment = ((item as AnyObject).value(forKey: "Departments")as! NSArray).mutableCopy()as! NSMutableArray
                            strBranchSysName = "\((item as AnyObject).value(forKey: "SysName")!)"
                            txtSelectBranch.text = "\((item as AnyObject).value(forKey: "Name")!)"
                            break
                        }
                    }
                }
            }
            
        }
        
        //-------Company-----------
        if(dictLeadDetail.value(forKey: "CrmCompany") is NSDictionary){
            let dictCompany =  dictLeadDetail.value(forKey: "CrmCompany")as! NSDictionary
            txtCompanyName.text =  "\(dictCompany.value(forKey: "Name")!)"
            strCrmCompanyId = "\(dictCompany.value(forKey: "CrmCompanyId")!)"
        }
        //-------Contact-----------
        txtContactName.text =  Global().strFullName(dictLeadDetail as? [AnyHashable : Any])
       strCrmContactId  = "\(dictLeadDetail.value(forKey: "CrmContactId")!)"
        
        
        //--------Address-------------
        txtServiceAddress.text = Global().strCombinedAddress(dictLeadDetail as? [AnyHashable : Any])
        
        strAddressLine1 = "\(dictLeadDetail.value(forKey: "Address1")!)"
        strAddressline2 = "\(dictLeadDetail.value(forKey: "Address2")!)"
        strCityName = "\(dictLeadDetail.value(forKey: "CityName")!)"
        strStateId = "\(dictLeadDetail.value(forKey: "StateId")!)"
        strZipcode = "\(dictLeadDetail.value(forKey: "Zipcode")!)"
        
        txtSchoolDistric.text = "\(dictLeadDetail.value(forKey: "SchoolDistrict")!)"
        txtCounty.text = "\(dictLeadDetail.value(forKey: "CountyName")!)"
        //--------Adrress Sub Type-----------
        txtAdrressSubType.text = "\(dictLeadDetail.value(forKey: "FlowType")!)"
        if("\(dictLeadDetail.value(forKey: "FlowType")!)" == "Commercial"){
            self.btnCommercial.setImage(UIImage(named: "check_CRM"), for: .normal)
            self.btnResidential.setImage(UIImage(named: "uncheck_CRM"), for: .normal)
        }else{
            self.btnCommercial.setImage(UIImage(named: "uncheck_CRM"), for: .normal)
            self.btnResidential.setImage(UIImage(named: "check_CRM"), for: .normal)
        }
        //--------FiledSalesPerson-----------
        txtFiledSalesPerson.text = "\(dictLeadDetail.value(forKey: "AssignedToName")!)"
        strFiledSalesPersonID = "\(dictLeadDetail.value(forKey: "AssignedToId")!)"
        
        //--------Source-------------
        if dictLeadDetail.value(forKey: "WebLeadSourceId") is NSArray
        {
            let arrMultipleSource = dictLeadDetail.value(forKey: "WebLeadSourceId") as! NSArray
            
            if(arrMultipleSource.count > 0)
            {
                let defsLogindDetail = UserDefaults.standard
                let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
                let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                
                let arrTempSource = NSMutableArray()
                var strSourceName = ""
                for souceIdFromServer in arrMultipleSource
                {
                    for souceIdLocal in arrOfData
                    {
                        let dict = (souceIdLocal as AnyObject) as! NSDictionary
                        if("\(souceIdFromServer)" == "\(dict.value(forKey: "SourceId")!)")
                        {
                            arySource.add("\(souceIdFromServer)")
                            arrTempSource.add(dict)
                        }
                    }
                }
                
                if(arrTempSource.count > 0)
                {
                    for item in arrTempSource{
                        strSourceName = "\(strSourceName),\((item as AnyObject).value(forKey: "Name")!)"
                    }
                    if strSourceName == ""
                    {
                        strSourceName = ""
                    }
                    else{
                        strSourceName = String(strSourceName.dropFirst())
                    }
                    
                    txtSelectSource.text = strSourceName
                    arrSelectedSource = arrTempSource
                }
            }
        }
        
        //---------------WebLeadServices--------

        if let service = dictLeadDetail.value(forKey: "WebLeadServices"){
            
            if(service is NSArray){
                
                if((service as! NSArray).count > 0){
                    
                    aryOfService = (service as! NSArray).mutableCopy() as! NSMutableArray
                    let arrOfServiceName = NSMutableArray()
                    
                    for k in 0 ..< aryOfService.count {

                        let dict = aryOfService[k] as! NSDictionary
                       
                        let strServiceNameLocal = fetchServiceNameViaId(strId: "\(dict.value(forKey: "ServiceId")!)")
                    
                        if strServiceNameLocal.count > 0 {
                            
                            arrOfServiceName.add(strServiceNameLocal)
                            
                        }
                        if "\(dict.value(forKey: "IsPrimary")!)" == "1" || "\(dict.value(forKey: "IsPrimary")!)" == "true" || "\(dict.value(forKey: "IsPrimary")!)" == "True"{
                            
                            strPrimaryService = "\(dict.value(forKey: "ServiceId")!)"
                            
                            self.getDepartmentViaServiceId(strServiceIdd: strPrimaryService)

                            
                        }else{
                            
                            arrOfAdditionalServiceSelected.add("\(dict.value(forKey: "ServiceId")!)")
                            
                        }
                    }
                    
                    txtSelectService.text = arrOfServiceName.componentsJoined(by: ", ")
                }
            }
        }
        
        //---------------taxCode--------
        
        // Call Address Api's In Background
        
        if (isInternetAvailable()){
            
            if strAccountId.count > 0 {
                
                self.callApiToGetAddressViaAccountIdBackGround(strAccountIdLocal: self.strAccountId ,strType : "sadf")
                
            }

        }
        if (isInternetAvailable()){
       
            if strCrmContactId.count > 0 {

                self.callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal: self.strCrmContactId, strType: "")

            }
            
        }
        
        if (isInternetAvailable()){
        
            if strCrmCompanyId.count > 0 {

                self.callApiToGetAddressViaCrmCompanyIdBackGround(strCrmCoCompanyIdLocal: strCrmCompanyId, strType: "")

            }
            
        }
         
    }
    
    
    
    func hideShowAccordingToCheckBOX() {
        
        arySelectSubAddress = [["title":"Commercial","id":"1"],["title":"Residential","id":"2"]]
        //txtServiceAddress.text?.count == 0 ? width_ServiceAddressCross.constant = 35.0 : (width_ServiceAddressCross.constant = 35.0)
        tagIsSchedule == 1 ? height_ScheduleNowView.constant = DeviceType.IS_IPAD ? 350 : 250 : (height_ScheduleNowView.constant = 0.0)
        tagIsSchedule == 1 ? self.btnSchedulenow.setImage(UIImage(named: "check_box_2New.png"), for: .normal) : self.btnSchedulenow.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
        tagIsTax == 1 ? height_IstaxExempt.constant = 60.0 : (height_IstaxExempt.constant = 0.0)
        tagIsTax == 1 ? self.btnIsTax.setImage(UIImage(named: "check_box_2New.png"), for: .normal) : self.btnIsTax.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
        if(tagMoreInfo == 1){
            tagIsTax == 1 ? height_IstaxExempt.constant = 60.0 : (height_IstaxExempt.constant = 0.0)
            tagIsTax == 1 ? self.btnIsTax.setImage(UIImage(named: "check_box_2New.png"), for: .normal) : self.btnIsTax.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
            height_ServiceAddress.constant = DeviceType.IS_IPAD ? 190.0 : 147.0
            btnIsTax.isHidden = false
            self.btnMoreInfo.setImage(UIImage(named: "drop_down_up_ipad"), for: .normal)
        }else{
            height_IstaxExempt.constant = 0.0
            height_ServiceAddress.constant = 0.0
            btnIsTax.isHidden = true
            self.btnMoreInfo.setImage(UIImage(named: "arrow_2"), for: .normal)
        }
        
        if tagIsSchedule == 1 {
            
            if tf_Date.text?.count == 0 {
                tf_Date.text = Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
            }
            if tf_Time.text?.count == 0 {
                tf_Time.text = Global().strGetCurrentDate(inFormat: "hh:mm a")
            }
            
//            if txtScheduleDate.text?.count == 0 {
//
//                // set current date
//
//                txtScheduleDate.text = Global().strGetCurrentDate(inFormat: "MM/dd/yyyy hh:mm a")
//
//            }
            
//            if txtTime.text?.count == 0 {
//
//                // set current time
//
//                txtTime.text = Global().strGetCurrentDate(inFormat: "hh:mm a")
//
//            }
            
        }
        
    }
    
//
//    func gotoDatePickerView(sender: UIButton, strType:String ,dateToSet:Date)  {
//        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
//        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
//        vc.strTag = sender.tag
//        vc.chkForMinDate = true
//        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        vc.modalTransitionStyle = .coverVertical
//        vc.handleDateSelectionForDatePickerProtocol = self
//        vc.strType = strType
//        vc.dateToSet = dateToSet
//        self.present(vc, animated: true, completion: {})
//    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    @objc func handleNotificationPost(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        
        if let dict = notification.userInfo as NSDictionary? {
            //Account
            var strEntityId = ""
            var strEntityType = ""
            var strEntityName = ""
            
            let arrOfKeys = dict.allKeys as NSArray

            if self.tagForSelection == 1{
                
                if arrOfKeys.contains("AccountId") {

                    // Account
                    strEntityId = "\(dict.value(forKey: "AccountId")!)"
                    strEntityType = enumRefTypeAccount
                    strEntityName = "\(dict.value(forKey: "AccountName")!)"
                    
                    let strName = "\(dict.value(forKey: "AccountName") ?? "")"
                    
                    if strName.count == 0 {
                        
                        let strCrmContactName = "\(dict.value(forKey: "CrmContactName") ?? "")"
                        
                        if strCrmContactName.count == 0 {
                            
                            let strCrmCompanyName = "\(dict.value(forKey: "CrmCompanyName") ?? "")"
                            
                            if strCrmCompanyName.count == 0 {
                                
                                strEntityName = " "
                                
                            }else{
                                
                                strEntityName = "\(strCrmCompanyName)"
                                
                            }
                            
                        }else{
                            
                            strEntityName = "\(strCrmContactName)"
                            
                        }
                        
                    }else{
                        
                        strEntityName = "\(strName)"
                        
                    }
                    
                }
                
            }else if self.tagForSelection == 2{
                
                // Company
                strEntityId = "\(dict.value(forKey: "CrmCompanyId")!)"
                strEntityType = enumRefTypeCrmCompany
                strEntityName = "\(dict.value(forKey: "Name")!)"
            }else if self.tagForSelection == 3{
                
                // Contact
                
                strEntityId = "\(dict.value(forKey: "CrmContactId")!)"
                strEntityType = enumRefTypeCrmContact
                strEntityName = Global().strFullName(dict as? [AnyHashable : Any])
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.callGetAssociatedEntitiesForLeadAssociations(EntityId: strEntityId, EntityType: strEntityType, editStatus: true, userInfo: dict)

                }
            
        }
        
    }
    func getAddress(from dataString: String) ->  NSMutableArray {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        let resultsArray = NSMutableArray()
        // put matches into array of Strings
        for match in matches {
            if match.resultType == .address,
                let components = match.addressComponents {
                resultsArray.add(components)
            } else {
                print("no components found")
            }
        }
        return resultsArray
    }
    fileprivate func getServiceFromMaster(){
        
        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            
            if(dict is NSDictionary){
                
                if let aryIndustry = (dict as! NSDictionary).value(forKey: "ServiceMasters"){
                    
                    if(aryIndustry is NSArray){
                        
                        for item  in aryIndustry as! NSArray{
                            
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                
                                aryServiceMaster.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
    }
    func getTimeWithouAMPM(strTime:String)-> String
      {
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "hh:mm a"
          let date = dateFormatter.date(from: strTime)!
          dateFormatter.dateFormat = "HH:mm"
          let dateString1 = dateFormatter.string(from: date)
          return dateString1
      }
    
    func fetchServiceNameViaId(strId : String) -> String {
        
        var serviceName = ""
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        for k in 0 ..< arrOfData.count {
            
            if arrOfData[k] is NSDictionary {
                
                let dictOfData = arrOfData[k] as! NSDictionary
                
                if "\(dictOfData.value(forKey: "ServiceId")!)" == strId {
                    
                    serviceName = "\(dictOfData.value(forKey: "Name")!)"
                    break
                }
                
            }
            
        }
        
        return serviceName
        
    }
    
    func catchNotification(notification:Notification) {

        if let dict = notification.userInfo as NSDictionary? {

            aryOfService.removeAllObjects()
            strPrimaryService = "\(dict.value(forKey: "PrimaryService")!)"
            
            let arrOfServiceName = NSMutableArray()
            
            let strServiceName = fetchServiceNameViaId(strId: strPrimaryService)
            
            if strServiceName.count > 0 {
                
                arrOfServiceName.add(strServiceName)
                let dictTemp = ["ServiceId":"\(strPrimaryService)","IsPrimary":true] as [String : Any]
                aryOfService.add(dictTemp)
            }
            
            let arrOfAdditionalServicesSelected = (dict.value(forKey: "AdditionalService")!) as! NSMutableArray
            
            if arrOfAdditionalServicesSelected.count > 0 {
                                
                arrOfAdditionalServicesSelected.addingObjects(from: arrOfAdditionalServicesSelected as! [Any])
                
            }
            
            for k in 0 ..< arrOfAdditionalServicesSelected.count {

                let strServiceNameLocal = fetchServiceNameViaId(strId: arrOfAdditionalServicesSelected[k] as! String)
                if strServiceNameLocal.count > 0 {
                    
                    arrOfServiceName.add(strServiceNameLocal)
                   
                    let dictTemp = ["ServiceId":"\(arrOfAdditionalServicesSelected[k])","IsPrimary":false] as [String : Any]
                    
                    aryOfService.add(dictTemp)
                    
                }
            }
            
            txtSelectService.text = arrOfServiceName.componentsJoined(by: ", ")
        
            // Fetch Department of Primary Service
            
            self.getDepartmentViaServiceId(strServiceIdd: strPrimaryService)
            
        }
    }
    
    func getDepartmentViaServiceId(strServiceIdd : String) {
        
        txtselectDepartment.text = ""
        strDepartmentSysName = ""
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        var arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        arrOfData = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchSysName )
        
        if arrOfData.count > 0 {
            
            for k in 0 ..< arrOfData.count {
                
                let dictData = arrOfData[k] as! NSDictionary
                
                let strLocalId = "\(dictData.value(forKey: "ServiceId")!)"
            
                if strLocalId == strServiceIdd {
                    
                    //DepartmentId
                    
                    self.getDepartmentDetail(strDepartmentIdd : "\(dictData.value(forKey: "DepartmentSysName")!)")
                    
                    break
                    
                }
                
            }
            
        }
        
    }
    
    
    func getDepartmentDetail(strDepartmentIdd : String) {
        //DepartmentSysName
        for k1 in 0 ..< aryDepartment.count {
            
            let dictDataNew = aryDepartment[k1] as! NSDictionary
            
            let strLocalDepartmentId = "\(dictDataNew.value(forKey: "SysName")!)"
        
            if strLocalDepartmentId == strDepartmentIdd {
                
                txtselectDepartment.text = "\(dictDataNew.value(forKey: "Name")!)"
                strDepartmentSysName = "\(dictDataNew.value(forKey: "SysName")!)"
                
                break

            }
            
        }
        
    }
    
    // MARK:
    // MARK: ----------IBAction------------
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func action_OnCommercialAndRecidential(_ sender: UIButton) {
         self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if(sender == btnCommercial){
            self.btnCommercial.setImage(UIImage(named: "check_CRM"), for: .normal)
            self.btnResidential.setImage(UIImage(named: "uncheck_CRM"), for: .normal)
        }else{
            self.btnCommercial.setImage(UIImage(named: "uncheck_CRM"), for: .normal)
            self.btnResidential.setImage(UIImage(named: "check_CRM"), for: .normal)
        }
        
    }
    @IBAction func action_Account(_ sender: UIButton) {
         self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        tagForSelection = 1
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateAccountVC") as! AssociateAccountVC
        controller.strFromWhere = "NotAsscoiate"
        controller.strFromWhereForAssociation = "ConvertOpportunityCRM"
        nsud.set(true, forKey: "isInitalAccount")
        nsud.synchronize()
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    @IBAction func action_Company(_ sender: UIButton) {
         self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        tagForSelection = 2
        
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "ConvertOpportunityCRM"
        self.navigationController?.pushViewController(controller, animated: false)
        
        /*let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "ConvertOpportunityCRM"
        self.navigationController?.pushViewController(controller, animated: false)*/
        
    }
    @IBAction func action_Contact(_ sender: UIButton) {
         self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        tagForSelection = 3
        
           let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
          
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "ConvertOpportunityCRM"
        self.navigationController?.pushViewController(controller, animated: false)
        
        /*let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "ConvertOpportunityCRM"
        self.navigationController?.pushViewController(controller, animated: false)*/
        
    }
    
    @IBAction func action_ServiceAddress(_ sender: UIButton) {
         self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
                          self.tableView.removeFromSuperview()
        
        if aryAddress.count > 0  {
            
            self.openTableViewPopUp(tag: 63, ary: (self.aryAddress as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableCustomerAddress, viewcontrol: self)
            
        }
    
    }
    @IBAction func action_ServiceAddressClose(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        txtServiceAddress.text = ""
        strAddressId = ""
        hideShowAccordingToCheckBOX()
    }
    
    @IBAction func action_SelectBranch(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        var arrOfData = (dictLeadDetailMaster.value(forKey: "BranchMastersAll") as! NSArray).mutableCopy() as! NSMutableArray
        arrOfData = returnFilteredArray(array: arrOfData)
        if(arrOfData.count != 0){
            openTableViewPopUp(tag: 91, ary: (arrOfData as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    @IBAction func action_SelectDepartment(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        aryDepartment = returnFilteredArray(array: aryDepartment)
        if(aryDepartment.count != 0){
            openTableViewPopUp(tag: 92, ary: (aryDepartment as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            Global().displayAlertController(Alert, "No Data Available.\n Either select Branch Or\n Please check either Data is set on Web, if set on Web check your internet connection and Sync Masters from Menu options.", self)
        }
        
    }
    
    @IBAction func action_SelectService(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if strBranchSysName.count > 0 {
            
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            var arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
            //arrOfData = returnFilteredArray(array: arrOfData)
            arrOfData = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchSysName )
            if(arrOfData.count != 0){
                
                let controller = storyboardLeadProspect.instantiateViewController(withIdentifier: "SelectServiceVC") as! SelectServiceVC
                controller.strFromWhere = "ConvertOpportunityCRM"
                controller.strServiceIdPrimary = strPrimaryService
                controller.strBranchSysName = strBranchSysName
                controller.arrayServicesSelected = arrOfAdditionalServiceSelected
                self.navigationController?.present(controller, animated: false, completion: nil)
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select branch", viewcontrol: self)
            
        }
        
    }
    
    @IBAction func action_SelectSource(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        var arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        arrOfData = returnFilteredArray(array: arrOfData)
        if(arrOfData.count != 0){
            sender.tag = 60
            
            goToNewSelectionVC(arrOfData: (arrOfData as NSArray).mutableCopy() as! NSMutableArray, tag: 100, titleHeader: "Source")

            //openTableViewPopUp(tag: 60, ary: (arrOfData as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: arrSelectedSource)
        }
    }
    
    @IBAction func action_ScheduleNow(_ sender: UIButton) {
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        tagIsSchedule == 1 ?  tagIsSchedule = 0 : (tagIsSchedule = 1)
        hideShowAccordingToCheckBOX()
    }
    
    
    @IBAction func action_SalesPerson(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let isFieldPersonAssign = nsud.bool(forKey: "isFieldPersonAssign")

        if isFieldPersonAssign {
            
            let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
            let resultPredicate = NSPredicate(format: "IsActive == true")
            let arrayfilter = aryTemp.filtered(using: resultPredicate)
            if(arrayfilter.count != 0){
                openTableViewPopUp(tag: 61, ary: (arrayfilter as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Not authorized to change", viewcontrol: self)
            
        }
        
    }
    


    
    @IBAction func action_MoreInfo(_ sender: UIButton) {
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        tagMoreInfo == 1 ?  tagMoreInfo = 0 : (tagMoreInfo = 1)
        hideShowAccordingToCheckBOX()
        
    }
    
    @IBAction func action_IsTAx(_ sender: Any) {
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        tagIsTax == 1 ?  tagIsTax = 0 : (tagIsTax = 1)
        hideShowAccordingToCheckBOX()
    }
    @IBAction func action_AddressSubType(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        openTableViewPopUp(tag: 90, ary: self.arySelectSubAddress , aryselectedItem: NSMutableArray())
        
    }
    @IBAction func action_TaxCOde(_ sender: Any) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if strBranchSysName.count > 0 {
            
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
            //let strBranchName = Global().getBranchName(fromSysNam: strBranchNameGlobal)
            
            if(dictLeadDetailMaster.count != 0)
            {
                
                let arrOfData = (dictLeadDetailMaster.value(forKey: "TaxMaster") as! NSArray).mutableCopy() as! NSMutableArray
                
                arrTaxCode = NSMutableArray()
                arrTaxCode = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchSysName)
                
                openTableViewPopUp(tag: 93, ary: returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchSysName ), aryselectedItem: NSMutableArray())
            }
            else
            {
                Global().displayAlertController(Alert, NoDataAvailableee, self)
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select branch.", viewcontrol: self)

        }
        
    }
    @IBAction func action_Save(_ sender: UIButton) {
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if isInternetAvailable() {

            if(self.validation()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                self.Add_CovertToOpportunityAPI()
                
            }
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        
        }
        
    }
    
    @IBAction func actionOnCancelAccount(_ sender: Any)
    {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtAccountNumber.text!.count > 0 {

            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtAccountNumber.text = ""
                self.strAccountId = ""
                
                self.arrOfAccountAddress = NSArray()
                
                self.mergeAllAddress()
                
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func actionOnCancelContact(_ sender: Any)
    {
        
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtContactName.text!.count > 0 {

            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtContactName.text = ""
                self.strCrmContactId = ""
                
                self.dictOfContactAddress = NSDictionary()
                
                self.mergeAllAddress()
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func actionOnCancelCompany(_ sender: Any)
    {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtCompanyName.text!.count > 0 {

            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtCompanyName.text = ""
                self.strCrmCompanyId = ""
                
                self.dictOfCompanyAddress = NSDictionary()
                
                self.mergeAllAddress()
                
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func action_AddAddress(_ sender: UIButton) {
              let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
              let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
              vc.tag = 1
              vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
              vc.modalTransitionStyle = .coverVertical
              vc.delegate = self
              self.present(vc, animated: false, completion: {})
          }
    
    @IBAction func action_SelectPropertyType(_ sender: Any) {
        if(self.getAddressTypeFromMasterSalesAutomation().count != 0){
         //   openTableViewPopUp(tag: 65, ary: self.getAddressTypeFromMasterSalesAutomation() , aryselectedItem: self.aryPropertySelected)
            openTableViewPopUp(tag: 65, ary: self.getAddressTypeFromMasterSalesAutomation() , aryselectedItem: NSMutableArray())

        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Property type not found!!!", viewcontrol: self)
            
        }
        
    }
    @IBAction func action_SelectUrgencyType(_ sender: Any) {
        if(self.getUrgencyFromMaster().count != 0){
            openTableViewPopUp(tag: 62, ary: self.getUrgencyFromMaster() , aryselectedItem: self.aryUrgencySelected)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Urgency not found!!!", viewcontrol: self)
            
        }
        
    }
    
func getUrgencyFromMaster() -> NSMutableArray {
    if(nsud.value(forKey: "LeadDetailMaster") != nil){
        let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        if(dictMaster.value(forKey: "UrgencyMasters") != nil){
            let aryTemp = (dictMaster.value(forKey: "UrgencyMasters") as! NSArray).filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")}
            return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
        }
        
        return NSMutableArray()
    }
    
    return NSMutableArray()

}
    // MARK:
    // MARK:----------Validation-----------
    func validation() -> Bool {
        
        /*if (txtAccountNumber.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select account.", viewcontrol: self)

            return false
        }else
        if (txtCompanyName.text?.count == 0){
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select company name.", viewcontrol: self)
                   return false
               }
        else if (txtContactName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select contact name.", viewcontrol: self)
            return false
        }
        else */

        if (txtServiceAddress.text!.count > 0) {

            let dictOfEnteredAddress = formatEnteredAddress(value: txtServiceAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                
                return false
                
            } else {
                
                
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                strZipcode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""

                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strCityName.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strStateId.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipcode.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipcode.count != 5 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                
            }
            
        }
        
        /*if ((txtServiceAddress.text!.count > 0)) {
            
            // Fetch Address
            
            let arrOfAddress = self.getAddress(from: txtServiceAddress.text!)
            
            let tempArrayOfKeys = NSMutableArray()
            let tempDictData = NSMutableDictionary()
            
            for k in 0 ..< arrOfAddress.count {
                
                if arrOfAddress[k] is NSDictionary {
                    
                    let tempDict = arrOfAddress[k] as! NSDictionary
                    
                    tempArrayOfKeys.addObjects(from: tempDict.allKeys)
                    tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])
                    
                }
                
            }
            
            if tempArrayOfKeys.contains("City") {
                
                strCityName = "\(tempDictData.value(forKey: "City")!)"
                
            }
            if tempArrayOfKeys.contains("ZIP") {
                
                strZipcode = "\(tempDictData.value(forKey: "ZIP")!)"
                
            }
            if tempArrayOfKeys.contains("State") {
                
                let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
                    
                }else{
                    
                    strStateId = ""
                    //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)
                    
                }
                
            }
            if tempArrayOfKeys.contains("Street") {
                
                strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"
                
            }
            
            if strAddressLine1.count == 0 || strCityName.count == 0 || strZipcode.count == 0 || strStateId.count == 0 {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter proper address (address, city, zipcode, state).", viewcontrol: self)
                return false
            }
        }*/
            
        if (txtSelectBranch.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select branch.", viewcontrol: self)
            return false
        }
        else if (txtselectDepartment.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select department.", viewcontrol: self)
            return false
        }
//        else if (txtSelectService.text?.count == 0){
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select service.", viewcontrol: self)
//            return false
//        }
        else if (txtSelectSource.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select source.", viewcontrol: self)
            return false
        }
        if(tagIsSchedule == 1){
    
            if(txtFiledSalesPerson.text?.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select field sales person.", viewcontrol: self)
                return false
            }
            
            else if(tf_Date.text == "")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select schedule date.", viewcontrol: self)
                return false
                
              
            }
            else if(tf_Time.text == "")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select schedule time.", viewcontrol: self)
                return false
                
              
            }
           
            else if (btn_TimeRange.currentImage == UIImage(named: "redio_2") && tf_Range.text == "") {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select time range.", viewcontrol: self)
                return false
                
             
                }
            
//            else if (txtScheduleDate.text?.count == 0){
//                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select Schedule date and time.", viewcontrol: self)
//
//                return false
//            }
//            else if (txtTime.text?.count == 0){
//                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select Schedule time.", viewcontrol: self)
//
//                return false
//            }
        }
        
        let isTaxRequired = Global().alert(forTaxCode: strSelectedTaxCodeSysName)
        
        if isTaxRequired {
            
            if(tagIsTax == 1){
                if(txtTaxCOde.text?.count == 0){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: TaxCode, viewcontrol: self)
                    
                    return false
                }
                
            }
            
        }
        
        if(txtTaxCOde.text?.count != 0 && strBranchSysName.count != 0){
            
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictLeadDetailMaster.count != 0)
            {
                
                let arrOfData = (dictLeadDetailMaster.value(forKey: "TaxMaster") as! NSArray).mutableCopy() as! NSMutableArray
                
                for item in arrOfData {
                    let branchStr = "\((item as! NSDictionary).value(forKey: "BranchSysName") ?? "")"
                    let strSysName = "\((item as! NSDictionary).value(forKey: "SysName") ?? "")"
                    
                    if strSysName == strSelectedTaxCodeSysName {
                        
                        if(strBranchSysName != branchStr){
                            
                            let alertMsg = "Select taxcode of " + txtSelectBranch.text!
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMsg, viewcontrol: self)
                            
                            return false
                        }
                        
                        break
                    }

                }
                
            }
            
        }
        
        /*if(txtCounty.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter county.", viewcontrol: self)

            return false
        }
        else if(txtSchoolDistric.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please school distric.", viewcontrol: self)

            return false
        }
        else if(txtAdrressSubType.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select address sub type.", viewcontrol: self)

            return false
        }*/
        return true
    }
    @IBAction func action_OnDate(_ sender: UIButton) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Date.text?.count)! > 0 ? tf_Date.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.date = fromDate
        datePicker.datePickerMode = UIDatePicker.Mode.date
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Service Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Date.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
      
    }
    @IBAction func action_OnTime(_ sender: UIButton) {
   
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Time.text?.count)! > 0 ? tf_Time.text! : result))!
        
        let datePicker = UIDatePicker()
       // datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.time
        datePicker.date = fromDate
      //  datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)

        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate, Picker: datePicker, view: self.view ,strTitle : "Select Service Time")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Time.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func action_OnSelectRange(_ sender: Any) {
        self.view.endEditing(true)
        if(aryTimeRange.count != 0){
            goToNewSalesSelectionVC(arrItem: aryTimeRange.mutableCopy()as! NSMutableArray, arrSelectedItem: self.arySelectedRange, tag: 1, titleHeader: "Time Range", isMultiSelection: false, ShowNameKey: "StartInterval,EndInterval")
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
    }

    @IBAction func action_OnSpecific(_ sender: Any) {
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
        arySelectedRange = NSMutableArray()
        tf_Range.text = ""
        tf_Range.tag = 0
    }
    @IBAction func action_OnTimeRange(_ sender: Any) {
        height_tf_Range.constant = DeviceType.IS_IPAD ? 50.0 : 44.0
        btn_Specific.setImage(UIImage(named: "redio_1"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_2"), for: .normal)

    }
    
    
    @IBAction func action_OnThirdParty(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.currentImage == UIImage(named: "check_box_1New.png") {
            btnThirdParty.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        }else{
            btnThirdParty.setImage(UIImage(named: "check_box_1New.png"), for: .normal)

        }
    }
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.delegate = self
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    // MARK: - ----------- Service Address Api's ----------------

    func callApiToGetAddressViaCrmContactId(strCrmContactIdLocal : String , strType : String) {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmContactId + strCrmContactIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfContactAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.dictOfContactAddress = removeNullFromDict(dict: self.dictOfContactAddress.mutableCopy()as! NSMutableDictionary)

                        if self.dictOfContactAddress.count > 0 {
                            
                            let zipCodeLocal = "\(self.dictOfContactAddress.value(forKey: "ZipCode")!)"
                            
                            if (zipCodeLocal.count > 0) {
                                
                                if self.txtServiceAddress.text?.count != 0 {
                                    
                                    let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                                      
                                    let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                        
                                        self.txtServiceAddress.text = Global().strCombinedAddress(self.dictOfContactAddress as? [AnyHashable : Any])
                                        self.strAddressId = ""
                                        
                                    })
                                    
                                    let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                        
                                        
                                        
                                    })
                                    
                                    alertCOntroller.addAction(alertAction)
                                    alertCOntroller.addAction(alertActionCancel)

                                    self.present(alertCOntroller, animated: true, completion: nil)
                                    
                                }else{
                                    
                                    self.txtServiceAddress.text = Global().strCombinedAddress(self.dictOfContactAddress as? [AnyHashable : Any])
                                    self.strAddressId = ""

                                    
                                }
                                
                                self.mergeAllAddress()
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetAddressViaCrmCompanyId(strCrmCoCompanyIdLocal : String , strType : String) {
        

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmCompanyId + strCrmCoCompanyIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfCompanyAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.dictOfCompanyAddress = removeNullFromDict(dict: self.dictOfCompanyAddress.mutableCopy()as! NSMutableDictionary)

                        if self.dictOfCompanyAddress.count > 0 {
                            
                            let zipCodeLocal = "\(self.dictOfCompanyAddress.value(forKey: "ZipCode")!)"
                            let AddressPropertyName = "\(self.dictOfCompanyAddress.value(forKey: "AddressPropertyName")!)"
                            let AddressPropertyTypeId = "\(self.dictOfCompanyAddress.value(forKey: "AddressPropertyTypeId")!)"
                            self.txtfldPropertyType.text = AddressPropertyName
                            self.txtfldPropertyType.tag = (AddressPropertyTypeId == "" ? 0 : Int(AddressPropertyTypeId))!
                            if (zipCodeLocal.count > 0) {
                                
                                if self.txtServiceAddress.text?.count != 0 {
                                    
                                    let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                                      
                                    let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                        
                                        self.txtServiceAddress.text = Global().strCombinedAddress(self.dictOfCompanyAddress as? [AnyHashable : Any])
                                        self.strAddressId = ""
                                        
                                    })
                                    
                                    let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                        
                                        
                                        
                                    })
                                    
                                    alertCOntroller.addAction(alertAction)
                                    alertCOntroller.addAction(alertActionCancel)

                                    self.present(alertCOntroller, animated: true, completion: nil)
                                    
                                }else{
                                    
                                    self.txtServiceAddress.text = Global().strCombinedAddress(self.dictOfCompanyAddress as? [AnyHashable : Any])
                                    self.strAddressId = ""

                                    
                                }
                                
                                self.mergeAllAddress()
                                                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    // MARK: - ----------- Service Address Api's Background----------------

    // API's Calling In backGround For Getting Address
    
    func callApiToGetAddressViaAccountIdBackGround(strAccountIdLocal : String , strType : String) {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetServiceAddressesByAccountId + strAccountIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            //self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.arrOfAccountAddress = Response.value(forKey: "data") as! NSArray
                        
                        if self.arrOfAccountAddress.count > 0 {
                            
                            self.mergeAllAddress()
                            
                        }
                        
                    }
                    
                }
                
            //}
            
        }
        
    }
    
    func callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal : String , strType : String) {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmContactId + strCrmContactIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfContactAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.mergeAllAddress()

                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetAddressViaCrmCompanyIdBackGround(strCrmCoCompanyIdLocal : String , strType : String) {
        

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmCompanyId + strCrmCoCompanyIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            //self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfCompanyAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.mergeAllAddress()

                    }
                    
                }
                
            //}
            
        }
        
    }
    
}

// MARK:
// MARK:-----------API Calling------------
extension ConvertToOppoetunity_iPhoneVC {
    func callApiToGetAddressViaAccountId(strAccountIdLocal : String , strType : String) {
        
        self.txtServiceAddress.isUserInteractionEnabled = true

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetServiceAddressesByAccountId + strAccountIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        //self.arrOfAccountAddress = Response.value(forKey: "data") as! NSArray
                        
                        let dictData = Global().convertNullArray(Response as? [AnyHashable : Any])! as NSDictionary
                        
                        if(dictData.value(forKey: "data")  is NSArray){
                            self.arrOfAccountAddress = dictData.value(forKey: "data") as! NSArray

                            if self.arrOfAccountAddress.count > 0 {
                             
                                self.mergeAllAddress()

                                for k in 0 ..< self.arrOfAccountAddress.count {
                                    
                                    let dictData = self.arrOfAccountAddress[k] as! NSDictionary
                                    let AddressPropertyName = "\(dictData.value(forKey: "AddressPropertyName")!)"
                                    let AddressPropertyTypeId = "\(dictData.value(forKey: "AddressPropertyTypeId")!)"
                                    self.txtfldPropertyType.text = AddressPropertyName
                                    self.txtfldPropertyType.tag = (AddressPropertyTypeId == "" ? 0 : Int(AddressPropertyTypeId))!
                                    if (dictData.value(forKey: "IsPrimary") as! Bool) == true {
                                        
                                        if self.txtServiceAddress.text?.count != 0 {
                                            
                                            let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                                              
                                            let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                                
                                                if self.txtAdrressSubType.text?.count == 0 {
                                                    
                                                    self.txtAdrressSubType.text = "\(dictData.value(forKey: "AddressSubType") ?? "")"
                                                    
                                                }
                                                
                                                self.txtServiceAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                                                self.strAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                                                self.txtServiceAddress.isUserInteractionEnabled = false
                                                
                                            })
                                            
                                            let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                                
                                                
                                                
                                            })
                                            
                                            alertCOntroller.addAction(alertAction)
                                            alertCOntroller.addAction(alertActionCancel)

                                            self.present(alertCOntroller, animated: true, completion: nil)
                                            
                                        }else{
                                            
                                            self.txtServiceAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                                            self.strAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                                            self.txtServiceAddress.isUserInteractionEnabled = false
                                            
                                            if self.txtAdrressSubType.text?.count == 0 {
                                                
                                                self.txtAdrressSubType.text = "\(dictData.value(forKey: "AddressSubType") ?? "")"
                                                
                                            }
                                            
                                        }
                                        
                                        break
                                        
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        
                
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    func callGetAssociatedEntitiesForLeadAssociations(EntityId: String ,EntityType : String,editStatus : Bool , userInfo : NSDictionary) {
        
        if(EntityId.count != 0 && EntityId != "<null>"){
            
            if(isInternetAvailable()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAssociatedEntitiesForTaskActivityAssociations + "EntityType=\(EntityType)&EntityId=\(EntityId)"
                
                WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                    
                    
                    self.loader.dismiss(animated: false) {
                        if(Status)
                        {
                            let dictResponse = removeNullFromDict(dict: (Response["data"] as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                            
                            var strID = ""
                            var strName = ""
                            
                            let arrOfKeys = userInfo.allKeys as NSArray
                            
                            if self.tagForSelection == 1{
                                
                                // set company uneditable
                                
                                //self.btnCompany.isUserInteractionEnabled = false
                                
                                if arrOfKeys.contains("AccountId") {
                                    
                                    // Account
                                    strID = "\(userInfo.value(forKey: "AccountId")!)"
                                    strName = "\(userInfo.value(forKey: "AccountName")!)"
                                    
                                    let strName1 = "\(userInfo.value(forKey: "AccountName") ?? "")"
                                    
                                    if strName1.count == 0 {
                                        
                                        let strCrmContactName = "\(userInfo.value(forKey: "CrmContactName") ?? "")"
                                        
                                        if strCrmContactName.count == 0 {
                                            
                                            let strCrmCompanyName = "\(userInfo.value(forKey: "CrmCompanyName") ?? "")"
                                            
                                            if strCrmCompanyName.count == 0 {
                                                
                                                strName = " "
                                                
                                            }else{
                                                
                                                strName = "\(strCrmCompanyName)"
                                                
                                            }
                                            
                                        }else{
                                            
                                            strName = "\(strCrmContactName)"
                                            
                                        }
                                        
                                    }else{
                                        
                                        strName = "\(strName1)"
                                        
                                    }
                                    
                                }
                                
                                if strID.count > 0 {
                                    
                                    self.strAccountId = strID
                                    self.txtAccountNumber.text = strName
                                    
                                    if (isInternetAvailable()){
                                        
                                        self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                        self.present(self.loader, animated: false, completion: nil)
                                        
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                            
                                            self.callApiToGetAddressViaAccountId(strAccountIdLocal: self.strAccountId ,strType : "sadf")

                                        })

                                    }
                                    
                                }
                                
                            }else if self.tagForSelection == 2{
                                // Company
                                
                                strID = "\(userInfo.value(forKey: "CrmCompanyId") ?? "")"
                                strName = "\(userInfo.value(forKey: "Name") ?? "")"
                                self.strCrmCompanyId = strID
                                
                                if strID.count > 0 {
                                    
                                    self.txtCompanyName.text = strName
                                    
                                    if (isInternetAvailable()){

                                    self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                    self.present(self.loader, animated: false, completion: nil)
                                    
                                    self.callApiToGetAddressViaCrmCompanyId(strCrmCoCompanyIdLocal: strID, strType: "")
                                        
                                    }
                                    
                                }
                                
                            }
                            else if self.tagForSelection == 3{
                                // Contact
                                
                                strID = "\(userInfo.value(forKey: "CrmContactId") ?? "")"
                                strName = Global().strFullName(userInfo as? [AnyHashable : Any])
                                
                                if strName.count == 0 || strName == "N/A" {
                                    
                                    strName = "\(userInfo.value(forKey: "CrmContactName") ?? "")"
                                    
                                }
                                
                                 self.strCrmContactId = strID
                                
                                if strID.count > 0 {
                                    
                                    self.txtContactName.text = strName
                                    
                                    if (isInternetAvailable()){

                                    self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                    self.present(self.loader, animated: false, completion: nil)
                                    
                                    self.callApiToGetAddressViaCrmContactId(strCrmContactIdLocal: strID, strType: "")
                                        
                                    }
                                    
                                }
                                
                            }
                            
                            
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }
                    
                    
                }
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }
    
    func Add_CovertToOpportunityAPI()
    {
        
        let dictLoginData = UserDefaults.standard.value(forKey: "LoginDetails") as! NSDictionary
        
        let strEmpId = dictLoginData.value(forKeyPath: "EmployeeId")
        let strCompanyKey = dictLoginData.value(forKeyPath: "Company.CompanyKey")
        let strWebLeanNo = "\(dictLeadDetail.value(forKey: "LeadId")!)"
        
        let dictToSend = NSMutableDictionary()
        
        if(tagIsTax == 1){
            dictToSend.setValue("\(strSelectedTaxCodeSysName)", forKey: "TaxSysName")
        }else{
            dictToSend.setValue("", forKey: "TaxSysName")
        }
        
        if(tagIsSchedule == 1){
            
//            var dateSchedule = "" , timeSchedule = ""
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
//            let date = dateFormatter.date(from: txtScheduleDate.text!)!
//            dateFormatter.dateFormat = "MM/dd/yyyy"
//            dateSchedule = dateFormatter.string(from: date)
//            dateFormatter.dateFormat = "hh:mm a"
//            timeSchedule = dateFormatter.string(from: date)
//            dictToSend.setValue("\( getTimeWithouAMPM(strTime: (timeSchedule)))", forKey: "ScheduleTime")
//            dictToSend.setValue("\(dateSchedule)", forKey: "ScheduleDate")
            
            dictToSend.setValue("\(strFiledSalesPersonID)", forKey: "FieldSalesPerson")
          
            
            dictToSend.setValue(tf_Date.text ?? "", forKey: "ScheduleDate")
            dictToSend.setValue(getTimeWithouAMPM(strTime: (tf_Time.text!)), forKey: "ScheduleTime")
            var strType = "Specific" , strRangeID = ""
            if(tf_Range.tag != 0 && tf_Range.text != ""){
                strType = "Range"
                strRangeID = "\(tf_Range.tag)"
            }
            dictToSend.setValue(strType, forKey: "ScheduleTimeType")
            dictToSend.setValue(strRangeID, forKey: "RangeOfTimeId")
            
        }else{
            dictToSend.setValue("", forKey: "FieldSalesPerson")
            dictToSend.setValue("", forKey: "ScheduleDate")
            dictToSend.setValue("", forKey: "ScheduleTime")
            dictToSend.setValue("", forKey: "ScheduleTimeType")
            dictToSend.setValue("", forKey: "RangeOfTimeId")
        }
        
        dictToSend.setValue(strCompanyKey, forKey: "CompanyKey")
        dictToSend.setValue(strWebLeanNo, forKey: "WebLeadId")
        dictToSend.setValue(strEmpId, forKey: "EmployeeId")
        dictToSend.setValue(strAccountId, forKey: "AccountId")
        dictToSend.setValue(strCrmContactId, forKey: "CrmContactId")
        dictToSend.setValue(strCrmCompanyId, forKey: "CrmCompanyId")
        
        dictToSend.setValue("\(strBranchSysName)", forKey: "BranchSysName")
        dictToSend.setValue("\(strDepartmentSysName)", forKey: "DepartmentSysName")
        dictToSend.setValue(self.arySource, forKey: "SourceIdsList")
        dictToSend.setValue(strAddressId, forKey: "CustomerAddressId")
        dictToSend.setValue("\(strAddressLine1)", forKey: "Address1")
        dictToSend.setValue("\(strAddressline2)", forKey: "Address2")
        dictToSend.setValue("\(strCityName)", forKey: "CityName")
        dictToSend.setValue("\(strStateId)", forKey: "StateId")
        dictToSend.setValue("1", forKey: "CountryId")
        dictToSend.setValue("\(strZipcode)", forKey: "Zipcode")
        dictToSend.setValue("\(txtCounty.text!)", forKey: "County")
        dictToSend.setValue("\(txtSchoolDistric.text!)", forKey: "SchoolDistrict")
        dictToSend.setValue("\(txtAdrressSubType.text!)", forKey: "AddressSubType")
        
        dictToSend.setValue("\(strThirdPartyAccountNo)", forKey: "ThirdPartyAccountNumber")
        dictToSend.setValue(aryOfService, forKey: "WebLeadServices")
        if txtfldPropertyType.tag == 0{
            dictToSend.setValue("", forKey: "AddressPropertyTypeId")
        }
        else{
            dictToSend.setValue("\(txtfldPropertyType.tag)", forKey: "AddressPropertyTypeId")
        }
        dictToSend.setValue("\(txtfldUrgencyType.tag)", forKey: "UrgencyId")

        dictToSend.setValue(btnThirdParty.currentImage == UIImage(named: "check_box_2New.png") ? "true" : "false", forKey: "CreateBlankScheduleInPP")

        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlConvertWebLeadToOpportunityV2
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", "CrmCompaniesNew") { (success, response, error) in
            
            
            DispatchQueue.main.async {
                
                self.loader.dismiss(animated: false) {
                    
                    if(success)
                    {
                        
                        let strResponse = "\((response as NSDictionary?)!.value(forKey: "ReturnMsg")!)"
                        
                        if strResponse == "true" {
                            
                            nsud.set(true, forKey: "fromWebLeadDetailTableAction")
                            nsud.synchronize()
                            
                            if(self.tagIsSchedule == 1){
                                
                                let alertCOntroller = UIAlertController(title: Info, message: "Lead converted successfully", preferredStyle: .alert)
                                
                                let alertAppointment = UIAlertAction(title: "Go to Appointments", style: .default, handler: { (action) in
                                            
                                    nsud.set(true, forKey: "fromScheduledOpportunity")
                                    //nsud.set(true, forKey: "fromCompanyVC")
                                    nsud.synchronize()
                                    
                                       if(DeviceType.IS_IPAD){
                                        
                                        let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                                        
                                        if strAppointmentFlow == "New" {
                                            
                                            let mainStoryboard = UIStoryboard(
                                                name: "Appointment_iPAD",
                                                bundle: nil)
                                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                                            
                                        } else {
                                            
                                            let mainStoryboard = UIStoryboard(
                                                name: "MainiPad",
                                                bundle: nil)
                                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                                            
                                        }
                                        
                                                 
                                       }else{
                                        let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                                        
                                        if strAppointmentFlow == "New" {
                                            
                                            let mainStoryboard = UIStoryboard(
                                                name: "Appointment",
                                                bundle: nil)
                                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                                            
                                        } else {
                                            
                                                     let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
                                                     self.navigationController?.pushViewController(controller, animated: false)
                                            
                                        }
                                                 }
                                    
                                })
                                
                                let alertCancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                                               
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EditLead_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                    
                                    self.navigationController?.popViewController(animated: false)
                                    
                                })
                                
                                alertCOntroller.addAction(alertCancelAction)
                                alertCOntroller.addAction(alertAppointment)
                                self.present(alertCOntroller, animated: true, completion: nil)
                                
                            }else{
                                
                                let alertCOntroller = UIAlertController(title: Info, message: "Lead converted successfully", preferredStyle: .alert)
                                
                                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EditLead_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                    
                                    self.navigationController?.popViewController(animated: false)
                                })
                                
                                alertCOntroller.addAction(alertAction)
                                self.present(alertCOntroller, animated: true, completion: nil)
                                
                            }
                            
                        } else {
                            
                            
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                            
                        }
                        
                    }else {
                        
                        
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }
                    
                }
                
                
                
            }
            
            
        }
    }
    
}



// MARK:
// MARK:----------CustomTableView Selection delegate-----------
extension ConvertToOppoetunity_iPhoneVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        //----------FiledPerson-----------
        if(tag == 61)
        {
            if(dictData.count == 0){
                txtFiledSalesPerson.text = ""
                strFiledSalesPersonID = ""
            }else{
                txtFiledSalesPerson.text = "\(dictData.value(forKey: "FullName")!)"
                strFiledSalesPersonID = "\(dictData.value(forKey: "EmployeeId")!)"
           
            }
        }
            
            //----------Address-----------

            else if(tag == 63)
                   {
                       if(dictData.count == 0){
                          txtServiceAddress.text = ""
                        self.height_ServiceAddress.constant = 0.0
                        strAddressId = ""
                       }else{
                        txtServiceAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                        self.height_ServiceAddress.constant = 30.0
                        
                        let arrTemp = dictData.allKeys as NSArray
                        
                        if arrTemp.contains("CustomerAddressId") {
                                                  
                            strAddressId = "\(dictData.value(forKey: "CustomerAddressId")!)"

                        }else{
                            
                            strAddressId = ""
                            
                        }
                        
                    }
        }
            //----------AdrressSubType-----------
            
        else if(tag == 90){
            
            if(dictData.count != 0){
                txtAdrressSubType.text = "\(dictData.value(forKey: "title")!)"
            }else{
                txtAdrressSubType.text = ""
            }
            
        }
            //----------Branch-----------
            
        else if(tag == 91){
            
            if(dictData.count != 0){
                txtSelectBranch.text = "\(dictData.value(forKey: "Name")!)"
                strBranchSysName = "\(dictData.value(forKey: "SysName")!)"
                aryDepartment = NSMutableArray()
                aryDepartment = (dictData.value(forKey: "Departments")as! NSArray).mutableCopy()as! NSMutableArray
                txtselectDepartment.text = ""
                strDepartmentSysName = ""
                strPrimaryService = ""
                arrOfAdditionalServiceSelected = NSMutableArray()
                txtSelectService.text = ""
                txtSelectService.tag = 0
                strPrimaryService = ""
            }else{
                txtSelectBranch.text = ""
                strBranchSysName = ""
                aryDepartment = NSMutableArray()
                txtselectDepartment.text = ""
                strDepartmentSysName = ""
                strPrimaryService = ""
                arrOfAdditionalServiceSelected = NSMutableArray()
                txtSelectService.text = ""
                txtSelectService.tag = 0
                strPrimaryService = ""
            }
            
        }
            //----------Department-----------
        else if(tag == 92){
            
            if(dictData.count != 0){
                txtselectDepartment.text = "\(dictData.value(forKey: "Name")!)"
                strDepartmentSysName = "\(dictData.value(forKey: "SysName")!)"
                txtSelectService.text = ""
                txtSelectService.tag = 0
                strPrimaryService = ""
                arrOfAdditionalServiceSelected = NSMutableArray()
            }else{
                txtselectDepartment.text = ""
                strDepartmentSysName = ""
                txtSelectService.text = ""
                txtSelectService.tag = 0
                strPrimaryService = ""
                arrOfAdditionalServiceSelected = NSMutableArray()
            }
            
        }
            //----------Source-----------
            
        else if(tag == 60)
        {
            if(dictData.count == 0){
                
                //  arrSourceId = NSMutableArray()
                arrSelectedSource = NSMutableArray()
                txtSelectSource.text = ""
                self.arySource = NSMutableArray()
            }else{
                
                let arySource = dictData.object(forKey: "source")as! NSArray
                print(arySource)
                var strName = ""
                // arrSourceId = NSMutableArray()
                
                arrSelectedSource = NSMutableArray()
                
                arrSelectedSource = arySource.mutableCopy() as! NSMutableArray
                self.arySource = NSMutableArray()
                for item in arySource{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                    self.arySource.add("\((item as AnyObject).value(forKey: "SourceId")!)")
                }
                if strName == ""
                {
                    strName = ""
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtSelectSource.text = strName
                
            }
            // Source
        }
            
            
            //----------TAxCode-----------
        else if(tag == 93){
            
            if(dictData.count != 0){
                txtTaxCOde.text = "\(dictData.value(forKey: "Name")!)"
                strSelectedTaxCodeSysName = "\(dictData.value(forKey: "SysName")!)"
                
            }else{
                txtTaxCOde.text = ""
                strSelectedTaxCodeSysName = ""
            }
            
        }
        //----------PropertyType-----------
        else if(tag == 65)
       {
           if(dictData.count == 0){
               txtfldPropertyType.text = ""
               txtfldPropertyType.tag = 0
               aryPropertySelected = NSMutableArray()
           }else{
               aryPropertySelected = NSMutableArray()
               aryPropertySelected.add(dictData)
               txtfldPropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
            txtfldPropertyType.tag = Int("\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")" == "" ? "0" : "\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")")!
           }
       }
        //---Urgency
        else if(tag == 62)
        {
            if(dictData.count == 0){
                txtfldUrgencyType.text = ""
                txtfldUrgencyType.tag = 0
                aryUrgencySelected = NSMutableArray()
            }else{
                aryUrgencySelected = NSMutableArray()
                aryUrgencySelected.add(dictData)
                txtfldUrgencyType.text = "\(dictData.value(forKey: "Name") ?? "")"
                txtfldUrgencyType.tag = Int("\(dictData.value(forKey: "UrgencyId") ?? "0")" == "" ? "0" : "\(dictData.value(forKey: "UrgencyId") ?? "0")")!
            }
       }
    }
    
}


// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------

extension ConvertToOppoetunity_iPhoneVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtServiceAddress){

            self.scrollView.isScrollEnabled = false
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtServiceAddress){

            self.scrollView.isScrollEnabled = true
            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if(textField == txtServiceAddress){
                        
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()
                    
                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = txtServiceAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtServiceAddress){

            let txtAfterUpdate:NSString = txtServiceAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            
//            self.getPlaces(with: parameters) {
//                self.places = $0
//            }
            return true
        }
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
extension ConvertToOppoetunity_iPhoneVC {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }
                else
                {
                   /* for item in self.scrollView.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)*/
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    
                   // self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.tableView.frame = CGRect(x: self.txtServiceAddress.frame.origin.x, y: 273, width: self.scrollView.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)

                    
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension ConvertToOppoetunity_iPhoneVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            //self.txtServiceAddress.text = value.formattedAddress
            self.txtServiceAddress.text = addressFormattedByGoogle(value: value)
            self.strAddressId = ""

            self.hideShowAccordingToCheckBOX()
            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}

// MARK: -
// MARK: - -----------------------Source Selction Delgates

extension ConvertToOppoetunity_iPhoneVC : SelectGlobalVCDelegate{
    
    func getDataSelectGlobalVC(dictData: NSMutableDictionary, tag: Int) {
        
        if tag == 100 {
            if(dictData.count == 0){
                
                //  arrSourceId = NSMutableArray()
                arrSelectedSource = NSMutableArray()
                txtSelectSource.text = ""
                self.arySource = NSMutableArray()
            }else{
                
                let arySource = dictData.object(forKey: "source")as! NSArray
                print(arySource)
                var strName = ""
                // arrSourceId = NSMutableArray()
                
                arrSelectedSource = NSMutableArray()
                
                arrSelectedSource = arySource.mutableCopy() as! NSMutableArray
                self.arySource = NSMutableArray()
                for item in arySource{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                    self.arySource.add("\((item as AnyObject).value(forKey: "SourceId")!)")
                }
                if strName == ""
                {
                    strName = ""
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtSelectSource.text = strName
                
            }
            // Source
        }
        
    }

}
// MARK: -
// MARK: - -----------------------Add Address Delgates

extension ConvertToOppoetunity_iPhoneVC : AddNewAddressiPhoneDelegate{

    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {
        
        print(dictData)
        txtServiceAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
        
            strAddressId = ""
    }

}
// MARK: -
// MARK: - SalesNew_SelectionProtocol


extension ConvertToOppoetunity_iPhoneVC: SalesNew_SelectionProtocol
{
    func didSelect(aryData: NSMutableArray, tag: Int) {
        
     }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        print(dictData)
        if(dictData.count != 0){
            self.arySelectedRange = NSMutableArray()
            self.arySelectedRange.add(dictData)
            let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
            let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
            let strrenge = StartInterval + " - " + EndInterval
            tf_Range.text = strrenge
            tf_Time.text = StartInterval
            
            tf_Range.tag = Int("\(dictData.value(forKey: "RangeofTimeId")!)")!
            
        }else{
            tf_Range.text = ""
            tf_Range.tag = 0
        }
    }
}
