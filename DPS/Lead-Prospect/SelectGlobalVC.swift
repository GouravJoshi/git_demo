//
//  SelectGlobalVC.swift
//  DPS
//
//  Created by Saavan Patidar on 26/06/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol SelectGlobalVCDelegate : class
{
    func getDataSelectGlobalVC(dictData : NSMutableDictionary ,tag : Int)
}

class CellSelectGlobalVC:UITableViewCell{
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    
}


class SelectGlobalVC: UIViewController {

    // MARK: -----------------------------------Variables----------------------------------------
    var arrayData = NSMutableArray()
    var arraySelectionData = NSMutableArray()
    var arraySelected = NSMutableArray()
    var tag = 0
    var titleHeader = ""

    // MARK: -----------------------------------Outlets----------------------------------------

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    weak var delegate: SelectGlobalVCDelegate?
    
    // MARK: -----------------------------------View's Life Cycle----------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
                 searchBar.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
             } else {
                 // Fallback on earlier versions
             }
        lblTitle.text = "Select" + " " + titleHeader
        searchBar.placeholder = "Search" + " " + titleHeader
        
        tblView.estimatedRowHeight = 200
        tblView.rowHeight = UITableView.automaticDimension;
        tblView.tableFooterView = UIView()

        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
        }
        searchBar.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBar.layer.borderWidth = 1
        searchBar.layer.opacity = 1.0
        
        btnDone.layer.cornerRadius = btnDone.frame.size.height/2
        btnDone.layer.borderWidth = 0

    }
    
    // MARK: -----------------------------------Functions----------------------------------------
    
    @objc func actionOnCheckBoxBtn(sender: UIButton!)
    {
        
        if (self.tag == 100){
            
            if(self.arraySelected.contains(self.arrayData.object(at: sender.tag))){ // True
                self.arraySelected.remove(self.arrayData.object(at: sender.tag))
                
            }else{
                self.arraySelected.add(self.arrayData.object(at: sender.tag))
            }
            self.tblView.reloadData()
            
        }else if (self.tag == 101){
            
            if(self.arraySelected.contains(self.arrayData.object(at: sender.tag))){ // True
                
                self.arraySelected.remove(self.arrayData.object(at: sender.tag))
                
                self.arraySelected = NSMutableArray()

            }else{
                
                self.arraySelected = NSMutableArray()
                
                self.arraySelected.add(self.arrayData.object(at: sender.tag))
                
            }
            self.tblView.reloadData()
            
        }

    }
    
    // MARK: -----------------------------------Actions----------------------------------------

    @IBAction func action_Back(_ sender: Any) {
        
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: nil)

    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if(tag == 100)
        {
            
            let dictSource = NSMutableDictionary()
            dictSource.setValue(self.arraySelected, forKey: "source")
            self.delegate?.getDataSelectGlobalVC(dictData: dictSource, tag: tag)
            self.dismiss(animated: false, completion: nil)

        }else if(tag == 101){
            
            let dictSource = NSMutableDictionary()
            
            if arraySelected.count > 0 {
                
                dictSource.addEntries(from: self.arraySelected.object(at: 0) as! [AnyHashable : Any])

            }
            self.delegate?.getDataSelectGlobalVC(dictData: dictSource, tag: tag)
            self.dismiss(animated: false, completion: nil)
            
        }

    }
    
}


// MARK: --------------------------- Extensions ---------------------------


extension SelectGlobalVC:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    // MARK: UItableView's delegate and datasource
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arrayData.count
        
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSelectGlobalVC") as! CellSelectGlobalVC

        if (self.tag == 100 || self.tag == 101)
        { // for Source Multi Selection
            
            if(arraySelected.contains(arrayData.object(at: indexPath.row))){ // True
                cell.btnCheckBox.setImage(UIImage(named: "checked.png"), for: .normal)
            }else{
                cell.btnCheckBox.setImage(UIImage(named: "uncheck.png"), for: .normal)
            }
            
            let dict = arrayData.object(at: indexPath.row) as! NSDictionary
            indexPath.row == -1 ? cell.lblName.text = "----Select----" : (cell.lblName.text = "\(dict.value(forKey: "Name")!)")
            
        }
    
        cell.accessoryType = .none
        cell.btnCheckBox.tag = indexPath.row
        cell.btnCheckBox.addTarget(self, action: #selector(actionOnCheckBoxBtn), for: .touchUpInside)
        
        return cell
           
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          
        tableView.deselectRow(at: indexPath, animated: false)
        
       }
}
extension  SelectGlobalVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
        //const_ViewTop_H.constant = 60
        //const_SearchBar_H.constant = 0
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: searchBar.text! as NSString)
    }
    func searchAutocomplete(Searching: NSString) -> Void
    {
        
        let resultPredicate = NSPredicate(format: "Name contains[c] %@", argumentArray: [Searching])
        if !(Searching.length == 0)
        {
            let arrayfilter = (self.arraySelectionData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arrayData = NSMutableArray()
            self.arrayData = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tblView.reloadData()
        }
        else
        {
            self.arrayData = NSMutableArray()
            self.arrayData = self.arraySelectionData.mutableCopy() as! NSMutableArray
            self.tblView.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(arrayData.count == 0){
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty//if searchText.count == 0
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                       
                       self.view.endEditing(true)
                       
                   })
            self.searchAutocomplete(Searching: "")
            searchBar.text = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                searchBar.resignFirstResponder()
            }
            
        }
    }
}
extension SelectGlobalVC : UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Text field did end calls")
    }
}
