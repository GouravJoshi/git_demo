//
//  EditOpportunityCRM_VC.swift
//  DPS
//
//  Created by Saavan Patidar on 20/04/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  2021

import UIKit

class EditOpportunityCRM_VC: UIViewController {

    
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    let global = Global()
    
    // MARK: -----------------------------------Variables----------------------------------------
    
    var isServiceAddress = true
    var strOpportunityType = ""
    var strAssignedToId = ""
    var strUrgencyId = ""
    var strCrmContactId = ""
    var dictLoginData = NSDictionary()
    var arrSelectedSource = NSMutableArray()
    var arrSourceId = NSMutableArray()
    var arrOfAdditionalServiceSelected = NSMutableArray()
    var strPrimaryService = ""
    @objc var dictOpportunity = NSDictionary()
    var dictOfServiceAddress = NSDictionary()
    var dictOfBillingAddress = NSDictionary()
    var strDepartmentNameGlobal = ""
    var strServiceCategoryID = ""
    var strPrimaryReasonForCallDepSysName = ""
    var strServiceId = ""
    var strInsideSalesPersonId = ""
    var strFieldSalesPersonId = ""
    var strSubmittedById = ""
    var strCompanySizeId = ""
    var strIndustryId = ""
    var strBranchNameGlobal = ""
    var strServiceTaxCode = ""
    var hghtScroll : CGFloat = DeviceType.IS_IPAD ? 2500 : 1750.0
    var arrOfAdditionalService = NSArray()
    var arrOfServiceAddress = NSMutableArray()
    var arrOfBillingAddress = NSMutableArray()
    
    var dictOpportunityStageNameFromId = NSDictionary()
    var dictOpportunityStageNameFromSysName = NSDictionary()
    var dictOpportunityStatusNameFromId = NSDictionary()
    var dictOpportunityStatusNameFromSysName = NSDictionary()
    
    var dictOfContactAddress = NSDictionary()
    var arrOfAccountAddress = NSArray()
    
    // MARK: -----------------------------------Outlets----------------------------------------

    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var btnCommercial: UIButton!
    @IBOutlet weak var btnResidential: UIButton!

    @IBOutlet weak var txtFldOpportunityName: ACFloatingTextField!
    @IBOutlet weak var txtFldContact: ACFloatingTextField!
    @IBOutlet weak var txtFldServiceAddress: ACFloatingTextField!
    @IBOutlet weak var txtFldPrimaryReasonForCall: ACFloatingTextField!
    @IBOutlet weak var txtFldService: ACFloatingTextField!
    @IBOutlet weak var txtFldProjectedAmount: ACFloatingTextField!
    @IBOutlet weak var txtFldInsideSalesPerson: ACFloatingTextField!
    @IBOutlet weak var txtFldSource: ACFloatingTextField!
    @IBOutlet weak var txtFldUrgency: ACFloatingTextField!
    @IBOutlet weak var txtFldBillingAddress: ACFloatingTextField!
    @IBOutlet weak var btnSameAsServiceAddress: UIButton!
    @IBOutlet weak var constBillingAddress_H: NSLayoutConstraint!

    @IBOutlet weak var btnScheduleNow: UIButton!
    @IBOutlet weak var constScheduleNow_H: NSLayoutConstraint!
    @IBOutlet weak var txtFldFieldSalesPerson: ACFloatingTextField!
    @IBOutlet weak var txtFldScheduleDate: ACFloatingTextField!
     @IBOutlet weak var txtFldScheduleTime: ACFloatingTextField!
    @IBOutlet weak var txtFldEstimatedDuration: ACFloatingTextField!
    @IBOutlet weak var txtFldDriveTime: ACFloatingTextField!

    @IBOutlet weak var btnMoreInfo: UIButton!
    @IBOutlet weak var constMoreInfoView_H: NSLayoutConstraint!

    @IBOutlet weak var txtSubmittedBy: ACFloatingTextField!
    @IBOutlet weak var txtProposedAmount: ACFloatingTextField!
    @IBOutlet weak var txtFldCompanySize: ACFloatingTextField!
    @IBOutlet weak var txtFldIndustry: ACFloatingTextField!
    @IBOutlet weak var txtFollowUpDate: ACFloatingTextField!
    @IBOutlet weak var txtGrabbedDate: ACFloatingTextField!
    @IBOutlet weak var txtExpectedClosingDate: ACFloatingTextField!
    @IBOutlet weak var txtDescription: ACFloatingTextField!

    @IBOutlet weak var txtServiceAddressCounty: ACFloatingTextField!
    @IBOutlet weak var txtServiceAddressSchoolDistrict: ACFloatingTextField!
    @IBOutlet weak var txtServiceAddressNotes: ACFloatingTextField!
    @IBOutlet weak var txtServiceAddressDirection: ACFloatingTextField!
    @IBOutlet weak var txtServiceAddressMapCode: ACFloatingTextField!
    @IBOutlet weak var txtServiceAddressGateCode: ACFloatingTextField!

    @IBOutlet weak var btnIsTaxExempt: UIButton!
    @IBOutlet weak var txtTaxExemption: ACFloatingTextField!
    @IBOutlet weak var txtTaxCode: ACFloatingTextField!
    @IBOutlet weak var constTaxExemption_H: NSLayoutConstraint!

    @IBOutlet weak var txtBillingAddressCounty: ACFloatingTextField!
    @IBOutlet weak var txtBillingAddressSchoolDistrict: ACFloatingTextField!
    @IBOutlet weak var constBillingAddressViewMore_H: NSLayoutConstraint!
    @IBOutlet weak var txtBillingAddressMapCode: ACFloatingTextField!
    
    @IBOutlet weak var txtFldServiceAddSubType: ACFloatingTextField!
    
    @IBOutlet weak var btnThirdParty: UIButton!
    @IBOutlet weak var viewThirdParty: UIView!

    //MARK:---------Time renge-----------
    @IBOutlet weak var tf_Date: ACFloatingTextField!
    @IBOutlet weak var tf_Time: ACFloatingTextField!
    @IBOutlet weak var tf_Range: ACFloatingTextField!
    @IBOutlet weak var height_tf_Range: NSLayoutConstraint!
    @IBOutlet weak var btn_Specific: UIButton!
    @IBOutlet weak var btn_TimeRange: UIButton!
    var aryTimeRange = NSArray() ,arySelectedRange = NSMutableArray()
    
    // MARK: - ----------- Google Address Code ----------------
    
    @IBOutlet weak var tableView: UITableView!

    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var radius: Double = 0.0
    private var strictBounds: Bool = false
    var txtAddressMaxY = CGFloat()
    var imgPoweredBy = UIImageView()
    var strStateId = ""
    var strCityName = ""
    var strAddressLine1 = ""
    var strZipCode = ""
    var strServiceCustomerAddressId = ""
    var strBillingCustomerAddressId = ""
    var strBillingStateId = ""
    var strBillingCityName = ""
    var strBillingAddressLine1 = ""
    var strBillingZipCode = ""
    //Deepak.s
    var arrUDFDataFromDetail = NSArray()
    @IBOutlet weak var viewForUserDefineField: UIView!
    @IBOutlet weak var heightUserDefine: NSLayoutConstraint!
    @IBOutlet weak var height_superView: NSLayoutConstraint!
    //*****
    
    private var places = [Place]() {
        didSet { tableView.reloadData() }
    }
    
    
    var loader = UIAlertController()

    // MARK: -----------------------------------View Life Cycle----------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //scrollView.setsuper
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
        
        
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        txtAddressMaxY = (txtFldServiceAddress.frame.origin.y) - 35
        imgPoweredBy.image = UIImage(named: "powered-by-google-on-white")
        imgPoweredBy.contentMode = .center
        
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: hghtScroll)
        
        btnCommercial.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnResidential.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        btnSameAsServiceAddress.setImage(UIImage(named: "uncheck.png"), for: .normal)
        constBillingAddress_H.constant =  DeviceType.IS_IPAD ? 60.0 : 40.0
        constBillingAddressViewMore_H.constant = DeviceType.IS_IPAD ? 220.0 : 144.0
        
        btnScheduleNow.setImage(UIImage(named: "uncheck.png"), for: .normal)
        constScheduleNow_H.constant = 0.0
        btnMoreInfo.setImage(UIImage(named: "arrow_2.png"), for: .normal)
        constMoreInfoView_H.constant = DeviceType.IS_IPAD ? 1250 :900
        btnIsTaxExempt.setImage(UIImage(named: "uncheck.png"), for: .normal)
        constTaxExemption_H.constant = DeviceType.IS_IPAD ? 60.0 : 40.0
        
        heightScrollView()
        
        showValues()
        
        // fetch Address By Account ID AccountId
        
         if (isInternetAvailable()){
        
             if strCrmContactId.count > 0 {

                 self.callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal: self.strCrmContactId, strType: "")

             }
             
         }
        
        if isInternetAvailable() {
            
            self.getAddressAll(strAccountNo: "\(self.dictOpportunity.value(forKey: "AccountNo") ?? "")")
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotificationPost(_:)), name: NSNotification.Name(rawValue: "AssociatedEditOpportunityCRM_Notification"), object: nil)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "SelectedServicesEditOpportunityCRM_Notification"),
                                               object: nil,
                                               queue: nil,
                                               using:catchNotification)
        
        //txtFldServiceAddress.isUserInteractionEnabled = true
        // txtFldBillingAddress.isUserInteractionEnabled = true
        
        txtProposedAmount.isEnabled = false
        txtGrabbedDate.isEnabled = false
        
        opportunityStageStatusFromId()
        
        //Schedule Third party Button
        viewThirdParty.clipsToBounds = true
        if isScheduleThirdParty() {
            viewThirdParty.isHidden = false
        }else{
            viewThirdParty.isHidden = true
        }
        btnThirdParty.setImage(UIImage(named: "uncheck.png"), for: .normal)
        if dictOpportunity.value(forKey: "CreateBlankScheduleInPP") != nil {
          let createBlankScheduleInPP = "\(dictOpportunity.value(forKey: "CreateBlankScheduleInPP") ?? "")"
            if(createBlankScheduleInPP.lowercased() == "true" || createBlankScheduleInPP == "1"){
                btnThirdParty.setImage(UIImage(named: "checked.png"), for: .normal)
            }
        }
        //Deepak.s
        print("UserDefine: ", "\(dictOpportunity.value(forKey: "UserDefinedFields") ?? "")")
        let jsonStringUDF = "\(dictOpportunity.value(forKey: "UserDefinedFields") ?? "")"

        let data = jsonStringUDF.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
               print(jsonArray) // use the json here
                self.arrUDFDataFromDetail = jsonArray as NSArray
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
        print(self.arrUDFDataFromDetail)
        self.creatUDFView()
        //***********
    }
    
    
    //Deepak.s
    func creatUDFView()  {
        self.heightUserDefine.constant = 0.0
        let strID = ""//"\(dataGeneralInfo.value(forKey: "leadId") ?? "")"
        let strType = Userdefinefiled_DynemicForm_Type.Opportunity
        
        var aryUDFSaveData_Service = self.arrUDFDataFromDetail.mutableCopy() as! NSMutableArray//UserDefineFiledVC().getUserdefineSaveData(type: strType, id: strID)
        let aryMasterData = UserDefineFiledVC().getUserdefineMasterData(type: strType)
        //----------When data is blank then use master data -------
        let arytempSave = NSMutableArray()
        //if aryUDFSaveData.count == 0 {
        for item in aryMasterData {
            let strType = "\((item as AnyObject).value(forKey: "type") ?? "")"
            let name = "\((item as AnyObject).value(forKey: "name") ?? "")"
            
            
            let temp = aryUDFSaveData_Service.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(name)" } as NSArray
            
            
            
            if (temp.count == 0 && (strType == Userdefinefiled_DynemicForm.radio_group  || strType == Userdefinefiled_DynemicForm.select || strType == Userdefinefiled_DynemicForm.checkbox_group) ) {
                if(((item as AnyObject).value(forKey: "values") is NSArray)){
                    let ary = ((item as AnyObject).value(forKey: "values") as! NSArray)
                    
                    let temp = ary.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "selected")!)".lowercased() == "true" ||  "\((task as! NSDictionary).value(forKey: "selected")!)".lowercased() == "1" } as NSArray
                    
                    if(temp.count != 0){
                        let aryValue = NSMutableArray()
                        for item in temp {
                            aryValue.add("\((item as AnyObject).value(forKey: "value") ?? "")")
                        }
                        let dict = NSMutableDictionary()
                        dict.setValue(aryValue, forKey: "userData")
                        dict.setValue(name, forKey: "name")
                        arytempSave.add(dict)
                    }
                }
            }
            if (temp.count == 0 && (strType == Userdefinefiled_DynemicForm.number ||  strType == Userdefinefiled_DynemicForm.text || strType == Userdefinefiled_DynemicForm.textarea || strType == Userdefinefiled_DynemicForm.date)){
                let dict = NSMutableDictionary()
                let aryValue = NSMutableArray()
                if((item as AnyObject).value(forKey: "value") != nil){
                    aryValue.add("\((item as AnyObject).value(forKey: "value") ?? "")")
                    dict.setValue(aryValue, forKey: "userData")
                    dict.setValue(name, forKey: "name")
                    arytempSave.add(dict)
                }
                
            }
        }
        aryUDFSaveData_Service += arytempSave.mutableCopy() as! NSMutableArray
        
  
  
    // remove blank value
    let aryTem = NSMutableArray()
    for item in aryUDFSaveData_Service {
        if(item is NSDictionary){
            let dictSaved = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
            if(dictSaved.value(forKey: "userData") is NSArray){
                let ary = (dictSaved.value(forKey: "userData") as! NSArray)
                let array = ary.filter({  "\($0)" != ""})
                dictSaved.setValue(array, forKey: "userData")
                aryTem.add(dictSaved)
            }
         
        }
        
    }
        aryUDFSaveData_Service = NSMutableArray()
        aryUDFSaveData_Service  = aryTem
        aryOtherUDF = NSMutableArray()
        /// }
    //----------End-------
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            
            UserDefineFiledVC().CreatFormUserdefine(strid: strID, aryJson_Master: aryMasterData, aryResponceObj:  aryUDFSaveData_Service, viewUserdefinefiled:  self.viewForUserDefineField, controllor: self) { aryUserdefine, aryUserdefineResponce, height in
                self.heightUserDefine.constant = CGFloat(height)
                if UIDevice.current.userInterfaceIdiom == .pad {
                     // iPad
                    self.height_superView.constant = 2700
                 } else {
                     //iPhone
                     self.height_superView.constant = 1950
                 }
                self.height_superView.constant = self.height_superView.constant + self.heightUserDefine.constant

                self.constMoreInfoView_H.constant = self.constMoreInfoView_H.constant + self.heightUserDefine.constant

            }
        }
        
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    //***********
    
    // MARK: -----------------------------------API's Calling----------------------------------------
    
    func getAddressAll(strAccountNo : String) {
        
        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        let strUrlMain = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") as? String
        var strCompanyKey: String? = nil
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            strCompanyKey = "\(value)"
        }
        
        var strUrl = ""
        
        strUrl = "\(strUrlMain ?? "")\(UrlGetServiceAddressesByAccountNoNew)\(strAccountNo)\(UrlGetActivityListcompanyKey)\(strCompanyKey ?? "")"
        
    
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
              self.present(loader, animated: false, completion: nil)
        
        WebService.callAPIBYGET(parameter: NSDictionary(), url: strUrl) { (Response, Status) in
            self.loader.dismiss(animated: false) {
                if(Status){
                              // ServiceAddresses
                              let dictAddress = Response["data"] as! NSDictionary
                              self.arrOfServiceAddress.add("----Select----")
                              self.arrOfBillingAddress.add("----Select----")
                              
                              //self.arrOfServiceAddress.addObjects(from: (dictAddress.value(forKey: "ServiceAddresses") as! [Any]))
                              
                              self.arrOfAccountAddress = dictAddress.value(forKey: "ServiceAddresses") as! NSArray

                              self.arrOfBillingAddress.addObjects(from: (dictAddress.value(forKey: "BillingAddresses") as! [Any]))
                    
                              self.mergeAllAddress()
                              
                          }
                          else
                          {
                              
                              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Something went wrong, try again later.", viewcontrol: self)
                              
                          }
            }
          
          
            
        }
        
    }
    
    func callApiToGetAddressViaCrmContactId(strCrmContactIdLocal : String , strType : String) {
        
        self.txtFldServiceAddress.isUserInteractionEnabled = true

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmContactId + strCrmContactIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfContactAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.dictOfContactAddress = removeNullFromDict(dict: self.dictOfContactAddress.mutableCopy()as! NSMutableDictionary)

                        if self.dictOfContactAddress.count > 0 {
                            
                            let zipCodeLocal = "\(self.dictOfContactAddress.value(forKey: "ZipCode")!)"
                            
                            if (zipCodeLocal.count > 0) {
                                
                                if self.txtFldServiceAddress.text?.count != 0 {
                                    
                                    let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                                      
                                    let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                        
                                        self.txtFldServiceAddress.text = Global().strCombinedAddress(self.dictOfContactAddress as? [AnyHashable : Any])
                                        //self.strCustomerAddressId = "\(self.dictOfContactAddress.value(forKey: "CustomerAddressId") ?? "")"
                                        self.strServiceCustomerAddressId = ""

                                        self.txtFldServiceAddress.isUserInteractionEnabled = false
                                        
                                    })
                                    
                                    let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                        
                                        
                                        
                                    })
                                    
                                    alertCOntroller.addAction(alertAction)
                                    alertCOntroller.addAction(alertActionCancel)

                                    self.present(alertCOntroller, animated: true, completion: nil)
                                    
                                }else{
                                    
                                    self.txtFldServiceAddress.text = Global().strCombinedAddress(self.dictOfContactAddress as? [AnyHashable : Any])
                                    //self.strCustomerAddressId = "\(self.dictOfContactAddress.value(forKey: "CustomerAddressId") ?? "")"
                                    self.strServiceCustomerAddressId = ""

                                    self.txtFldServiceAddress.isUserInteractionEnabled = false
                                    
                                }
                                
                                self.mergeAllAddress()
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetAddressViaCrmContactIdBackGround(strCrmContactIdLocal : String , strType : String) {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmContactId + strCrmContactIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfContactAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.mergeAllAddress()

                    }
                    
                }
                
            }
            
        }
        
    }
    
    // MARK: - -----------------------------------Update Opportunity-----------------------------------
    func updateOpportunity()
    {
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        
        var strUrl =  ""
        strUrl = strServiceUrlMain + "api/LeadNowAppToSalesProcess/EditOpportunityV2"
        
        let Url = String(format: strUrl)
        
        guard
            
            let serviceUrl = URL(string: Url) else { return }
        
        let dictJson = NSMutableDictionary()
        
        
        dictJson.setValue(strCompanyKey, forKey: "CompanyKey")
        dictJson.setValue("\(dictOpportunity.value(forKey: "OpportunityId") ?? "")", forKey: "OpportunityId")
        dictJson.setValue(strEmpID, forKey: "EmployeeId")
        dictJson.setValue(strCrmContactId, forKey: "CrmContactId")
        dictJson.setValue(txtFldProjectedAmount.text ?? "", forKey: "ProjectedAmount")
        dictJson.setValue(strUrgencyId, forKey: "PriorityId")
        dictJson.setValue(arrSourceId, forKey: "SourceIds")
        
        dictJson.setValue(strServiceCategoryID, forKey: "ServiceCategoryId")
        
        dictJson.setValue(strPrimaryService, forKey: "PrimaryServiceId")
        dictJson.setValue(strOpportunityType, forKey: "FlowType")
        dictJson.setValue(strSubmittedById, forKey: "SubmittedBy")
        dictJson.setValue(strCompanySizeId, forKey: "CompanySizeId")
        dictJson.setValue(strIndustryId, forKey: "IndustryId")
        
        dictJson.setValue(txtFollowUpDate.text ?? "", forKey: "FollowUpDate")
        
        dictJson.setValue(txtExpectedClosingDate.text ?? "", forKey: "CloseDate")
        
        dictJson.setValue(strInsideSalesPersonId, forKey: "InsideSalesPerson")
        
        dictJson.setValue(txtDescription.text ?? "", forKey: "Description")
        
        
        if btnScheduleNow.currentImage == UIImage(named: "checked.png")
        {
            dictJson.setValue(strFieldSalesPersonId, forKey: "FieldSalesPerson")
            
            
       
            
            dictJson.setValue(tf_Date.text ?? "", forKey: "ScheduleDate")
            dictJson.setValue(getTimeWithouAMPM(strTime: (tf_Time.text!)), forKey: "ScheduleTime")
            var strType = "Specific" , strRangeID = ""
            if(tf_Range.tag != 0 && tf_Range.text != ""){
                strType = "Range"
                strRangeID = "\(tf_Range.tag)"
            }
            dictJson.setValue(strType, forKey: "ScheduleTimeType")
            dictJson.setValue(strRangeID, forKey: "RangeOfTimeId")
            
        }else{
            
            dictJson.setValue(strFieldSalesPersonId, forKey: "FieldSalesPerson")
            
            dictJson.setValue("", forKey: "ScheduleDate")
            dictJson.setValue("", forKey: "ScheduleTime")
            dictJson.setValue("", forKey: "ScheduleTimeType")
            dictJson.setValue("", forKey: "RangeOfTimeId")
        }
        
        dictJson.setValue(((txtFldEstimatedDuration.text == "") ? "" :  txtFldEstimatedDuration.text!.trimmingCharacters(in: CharacterSet.whitespaces)+":00"), forKey: "TotalEstimationTime")
        
        dictJson.setValue(((txtFldDriveTime.text == "") ? "" :  txtFldDriveTime.text!.trimmingCharacters(in: CharacterSet.whitespaces)+":00"), forKey: "DriveTime")
        
        dictJson.setValue(txtFldServiceAddSubType.text ?? "", forKey: "AddressSubType")
        
        dictJson.setValue(strServiceCustomerAddressId, forKey: "ServiceLocationId")
        dictJson.setValue(strAddressLine1, forKey: "ServiceAddress1")
        dictJson.setValue("", forKey: "ServiceAddress2")
        dictJson.setValue("1", forKey: "ServiceCountryId")
        dictJson.setValue(strStateId, forKey: "ServiceStateId")
        dictJson.setValue(strCityName, forKey: "ServiceCity")
        dictJson.setValue(strZipCode, forKey: "ServiceZipcode")
        
        dictJson.setValue(txtServiceAddressCounty.text ?? "", forKey: "ServiceCounty")
        dictJson.setValue(txtServiceAddressSchoolDistrict.text ?? "", forKey: "ServiceSchoolDistrict")
        dictJson.setValue(txtServiceAddressMapCode.text ?? "", forKey: "ServiceMapCode")
        dictJson.setValue(txtServiceAddressGateCode.text ?? "", forKey: "ServiceGateCode")
        dictJson.setValue(txtServiceAddressNotes.text ?? "", forKey: "ServiceNotes")
        dictJson.setValue(txtServiceAddressDirection.text ?? "", forKey: "ServiceDirection")
        
        dictJson.setValue(strServiceTaxCode, forKey: "ServiceTaxSysName")
        
        if btnIsTaxExempt.currentImage == UIImage(named: "checked.png")
        {
            dictJson.setValue("true", forKey: "ServiceIsTaxExempt") // bool
        }
        else
        {
            dictJson.setValue("false", forKey: "ServiceIsTaxExempt") // bool
        }
        
        dictJson.setValue(txtTaxExemption.text ?? "", forKey: "ServiceTaxExemptionNo")
        
        if btnSameAsServiceAddress.currentImage == UIImage(named: "checked.png")
        {
            dictJson.setValue("true", forKey: "BillingAddressSameAsServiceAddress") // bool
        }
        else
        {
            dictJson.setValue("false", forKey: "BillingAddressSameAsServiceAddress") // bool
        }
        
        dictJson.setValue(strBillingCustomerAddressId, forKey: "BillingLocationId")
        dictJson.setValue(strBillingAddressLine1, forKey: "BillingAddress1")
        dictJson.setValue("", forKey: "BillingAddress2")
        dictJson.setValue("1", forKey: "BillingCountryId")
        dictJson.setValue(strBillingStateId, forKey: "BillingStateId")
        dictJson.setValue(strBillingCityName, forKey: "BillingCity")
        dictJson.setValue(strBillingZipCode, forKey: "BillingZipcode")
        
        dictJson.setValue(txtBillingAddressCounty.text ?? "", forKey: "BillingCounty")
        dictJson.setValue(txtBillingAddressSchoolDistrict.text ?? "", forKey: "BillingSchoolDistrict")
        dictJson.setValue(txtBillingAddressMapCode.text ?? "", forKey: "BillingMapCode")
        
        dictJson.setValue(txtFldOpportunityName.text ?? "", forKey: "OpportunityName")
        dictJson.setValue(arrOfAdditionalService, forKey: "AdditionalServiceIds")
        
        //Schedule Thirdparty
        dictJson.setValue(btnThirdParty.currentImage == UIImage(named: "checked.png") ? "true" : "false", forKey: "CreateBlankScheduleInPP")
        //Deepak.s
        let jsonStringUDF = "\(json(from:aryUDFSaveData as Any) ?? "")"
        dictJson.setValue(jsonStringUDF, forKey: "UserDefinedFields")
        //********

    
        let parameterDictionary = dictJson
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dictJson, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        
        print(jsonString)
        
        var request = URLRequest(url: serviceUrl)
        
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
        
        request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
        
        request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
        
        request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
        
        
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
        
        request.addValue("IOS", forHTTPHeaderField: "Browser")
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return
        }
        
        request.httpBody = httpBody
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async{
                self.loader.dismiss(animated: false) {
                    if let response = response {
                        
                        print(response)
                    }
                    
                    if let data = data
                    {
                        do
                        {
                            let strResponse = String(decoding: data, as: UTF8.self)
                            
                            if((strResponse == "true") || (strResponse == "True") || (strResponse == "TRUE"))
                            {
                                
                                
                                let alertController = UIAlertController(title: "Message", message: "Opportunity Updated Successfully.", preferredStyle:UIAlertController.Style.alert)
                                
                                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                                { action -> Void in
                                    
                                    if nsud.bool(forKey: "fromNearByOpportunity") == true
                                    {
                                        
                                    }
                                    else
                                    {
                                        let userDef = UserDefaults.standard
                                        userDef.set(true, forKey: "isFromEditOpprtunityVC")
                                        userDef.set(true, forKey: "forEditOpportunity")
                                        userDef.synchronize()
                                    }
                                    
                                    
                                    //self.dismiss(animated: false)
                                    self.navigationController?.popViewController(animated: false)
                                })
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Something went wrong, please try again later.", viewcontrol: self)
                                
                            }
                        }
                        catch
                        {
                            print(error)
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Something went wrong, please try again later.", viewcontrol: self)
                        }
                    }
                }
            }
        }.resume()
    }
    
    // MARK: -----------------------------------Functions----------------------------------------

    func goToNewSelectionVC(arrOfData : NSMutableArray, tag : Int, titleHeader : String) {
        
        if(arrOfData.count != 0){
            
             let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectGlobalVC") as! SelectGlobalVC
            controller.titleHeader = titleHeader
            controller.arrayData = arrOfData
            controller.arraySelectionData = arrOfData
            controller.arraySelected = arrSelectedSource
            controller.delegate = self
            controller.tag = tag
            self.navigationController?.present(controller, animated: false, completion: nil)
            
        }else {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    func mergeAllAddress() {
        
        self.arrOfServiceAddress = NSMutableArray()
        
        if arrOfAccountAddress.count > 0 {
            
            self.arrOfServiceAddress.addObjects(from: arrOfAccountAddress as! [Any])
            
        }
        /*if dictOfContactAddress.count > 0 {
            
            let zipCodeLocal = "\(self.dictOfContactAddress.value(forKey: "ZipCode")!)"
            
            if (zipCodeLocal.count > 0) {
                
                /*if !self.arrOfServiceAddress.contains{ $0 == self.dictOfContactAddress } {
                    self.arrOfServiceAddress.add(dictOfContactAddress)
                }*/
                
                if !checkIfAddressAlreadyPresent(strAddress: Global().strCombinedAddress(dictOfContactAddress as? [AnyHashable : Any])) {
                    self.arrOfServiceAddress.add(dictOfContactAddress)
                }
                
            }
            
        }*/
        
    }
    func checkIfAddressAlreadyPresent(strAddress : String) -> Bool {
        
        var isPresent = false
        
        for k in 0 ..< arrOfServiceAddress.count {
            
            if arrOfServiceAddress[k] is NSDictionary {
                
                let dictOfData = arrOfServiceAddress[k] as! NSDictionary
                
                let addressLocal = Global().strCombinedAddress(dictOfData as? [AnyHashable : Any])
                
                if addressLocal == strAddress {
                    
                    isPresent = true
                    
                    break
                    
                }
                
            }
            
        }
        
        return isPresent
        
    }
    
    func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    func heightScrollView() {
       
        let totalHeight =  (constBillingAddress_H.constant + constScheduleNow_H.constant) + (constMoreInfoView_H.constant + constTaxExemption_H.constant + constBillingAddressViewMore_H.constant)
        
        
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: totalHeight + (DeviceType.IS_IPAD ? 1000.0 : 800.0))
     
     
    }
    
    func showValues() {
        
//        var dict = NSDictionary()
//        dict = getLowerCasedDictionary(dictDetail: dictOpportunity)
//        dictOpportunity = dict
        
        // Load Saved Values
        strBranchNameGlobal = "\(dictOpportunity.value(forKey: "BranchSysName") ?? "")"
        strDepartmentNameGlobal = "\(dictOpportunity.value(forKey: "DepartmentSysName") ?? "")"

        //-------- Show Opportunity Type ----------//
        
        if(("\(dictOpportunity.value(forKey: "OpportunityType") ?? "")").count > 0)
        {
            
            if "\(dictOpportunity.value(forKey: "OpportunityType") ?? "")" == enumCommercial {
                
                strOpportunityType = enumCommercial
                btnCommercial.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                btnResidential.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                
            } else {
                
                strOpportunityType = enumResidential
                btnResidential.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                btnCommercial.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                
            }
            
        }
        else
        {
            
            strOpportunityType = enumCommercial
            btnCommercial.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            btnResidential.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            
        }
        
        txtFldOpportunityName.text = "\(dictOpportunity.value(forKey: "OpportunityName") ?? "")"
        strCrmContactId = "\(dictOpportunity.value(forKey: "CrmContactId") ?? "")"
        txtFldContact.text = Global().strFullName(dictOpportunity as? [AnyHashable : Any])
        
        // ServiceAddress
            
        if dictOpportunity.value(forKey: "ServiceAddress") is NSDictionary {
            
            dictOfServiceAddress = dictOpportunity.value(forKey: "ServiceAddress") as! NSDictionary
            
            if dictOfServiceAddress.count > 0 {
                
                txtFldServiceAddress.text = Global().strCombinedAddress(dictOfServiceAddress as? [AnyHashable : Any])
                
                strServiceCustomerAddressId = "\(dictOfServiceAddress.value(forKey: "CustomerAddressId") ?? "")"
                
                if strServiceCustomerAddressId.count > 0 {
                    
                    self.txtFldServiceAddress.isUserInteractionEnabled = true //false

                }
                
            }
            
        }
        
        //Primary Reason for call
        
        //-------- Show Primary reason for call (i.e service category id) ----------//
        if(("\(dictOpportunity.value(forKey: "ServiceCategoryId") ?? "")").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            if(nsud.value(forKey: "MasterSalesAutomation") != nil)
            {
                let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
                if(dictLeadDetailMaster.count != 0)
                {
                    let arrOfData = (dictLeadDetailMaster.value(forKey: "Categories") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    var arrOfFilteredData = returnFilteredArray(array: arrOfData)
                    
                    strDepartmentNameGlobal = "\(dictOpportunity.value(forKey: "DepartmentSysName") ?? "")"
                    
                    arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
                    
                    for item in arrOfFilteredData
                    {
                        let dict = (item as AnyObject) as! NSDictionary
                        
                        if("\(dictOpportunity.value(forKey: "ServiceCategoryId")!)" == "\(dict.value(forKey: "CategoryId")!)")
                        {
                            
                            txtFldPrimaryReasonForCall.text = dict["Name"] as? String
                            strServiceCategoryID = "\(dict.value(forKeyPath: "CategoryId")!)"
                            strPrimaryReasonForCallDepSysName = "\(dict.value(forKeyPath: "DepartmentSysName")!)"
                            break
                            
                        }
                    }
                }
            }
        }
        
        // Service
        
        if(("\(dictOpportunity.value(forKey: "ServiceId")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            var arrOfFilteredData = returnFilteredArray(array: arrOfData)
            
            arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
            
            if(arrOfFilteredData.count != 0)
            {
                
                for item in arrOfFilteredData
                {
                    let dict = (item as AnyObject) as! NSDictionary
                    if("\(dictOpportunity.value(forKey: "ServiceId")!)" == "\(dict.value(forKey: "ServiceId")!)")
                    {
                        txtFldService.text = dict["Name"] as? String
                        strServiceId = "\(dict.value(forKeyPath: "ServiceId")!)"
                        strPrimaryService = strServiceId
                        break
                    }
                }
            }
        }
        
        //
        // Showing Projected Amount
        
        /*if(("\(dictOpportunity.value(forKey: "OpportunityValue")!)").count > 0)
        {
            txtFldProjectedAmount.text = "\(dictOpportunity.value(forKey: "OpportunityValue")!)"
        }*/
        
        if(("\(dictOpportunity.value(forKey: "OpportunityValue")!)").count > 0)
        {
            let amount = ("\(dictOpportunity.value(forKey: "OpportunityValue") ?? "")" as NSString).doubleValue
            txtFldProjectedAmount.text = String(format: "%.02f", Double(amount))
        }
        
        
        // Showing Inside Sales Person
        if(("\(dictOpportunity.value(forKey: "InsideSalesPerson")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in arrOfData
            {
                let dictItem = (item as AnyObject) as! NSDictionary
                
                if("\(dictOpportunity.value(forKey: "InsideSalesPerson")!)" == "\(dictItem.value(forKey: "EmployeeId")!)")
                {
                    txtFldInsideSalesPerson.text = dictItem["FullName"] as? String
                    strInsideSalesPersonId = "\(dictItem.value(forKeyPath: "EmployeeId")!)"
                }
            }
        }
        
        //-------- Show Source ----------//SourceIdsList
        
        
        if dictOpportunity.value(forKey: "SourceIdsList") is NSArray
        {
            let arrMultipleSource = dictOpportunity.value(forKey: "SourceIdsList") as! NSArray
            
            if(arrMultipleSource.count > 0)
            {
                let defsLogindDetail = UserDefaults.standard
                let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
                let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                
                let arrTempSource = NSMutableArray()
                var strSourceName = ""
                for souceIdFromServer in arrMultipleSource
                {
                    for souceIdLocal in arrOfData
                    {
                        let dict = (souceIdLocal as AnyObject) as! NSDictionary
                        if("\(souceIdFromServer)" == "\(dict.value(forKey: "SourceId")!)")
                        {
                            arrSourceId.add("\(souceIdFromServer)")
                            arrTempSource.add(dict)
                        }
                    }
                }
                
                if(arrTempSource.count > 0)
                {
                    for item in arrTempSource{
                        strSourceName = "\(strSourceName),\((item as AnyObject).value(forKey: "Name")!)"
                    }
                    if strSourceName == ""
                    {
                        strSourceName = ""
                    }
                    else{
                        strSourceName = String(strSourceName.dropFirst())
                    }
                    
                    txtFldSource.text = strSourceName
                    arrSelectedSource = arrTempSource
                }
            }
        }
        
        //-------- Show Urgency ----------//
        if(("\(dictOpportunity.value(forKey: "Urgency")!)").count != 0)
        {
            txtFldUrgency.text = "\(dictOpportunity.value(forKey: "Urgency")!)"
            strUrgencyId = "\(dictOpportunity.value(forKeyPath: "UrgencyId")!)"
        }
        
        // Billing Address
        if dictOpportunity.value(forKey: "BillingAddress") is NSDictionary {
            
            dictOfBillingAddress = dictOpportunity.value(forKey: "BillingAddress") as! NSDictionary
            
            if dictOfBillingAddress.count > 0 {
                
                txtFldBillingAddress.text = Global().strCombinedAddress(dictOfBillingAddress as? [AnyHashable : Any])
                
                strBillingCustomerAddressId = "\(dictOfBillingAddress.value(forKey: "CustomerAddressId")!)"
                
                if strBillingCustomerAddressId.count > 0 {
                    
                    self.txtFldBillingAddress.isUserInteractionEnabled = true //false
                    
                }
                
            }
            
        }
        
        // showing if billing address is same as service address
        if ("\(dictOpportunity.value(forKey: "BillingAddressSameAsService")!)" == "true" || "\(dictOpportunity.value(forKey: "BillingAddressSameAsService")!)" == "True" || "\(dictOpportunity.value(forKey: "BillingAddressSameAsService")!)" == "TRUE" || "\(dictOpportunity.value(forKey: "BillingAddressSameAsService")!)" == "1")
        {
            
            btnSameAsServiceAddress.setImage(UIImage(named: "checked.png"), for: .normal)
            constBillingAddress_H.constant = 0.0
            constBillingAddressViewMore_H.constant = 0.0

        
            
            strBillingCustomerAddressId = ""
            txtFldBillingAddress.text = ""
            
        }else{
            
            btnSameAsServiceAddress.setImage(UIImage(named: "uncheck.png"), for: .normal)
            if(DeviceType.IS_IPAD){
                           constBillingAddress_H.constant = 60.0
                           constBillingAddressViewMore_H.constant = 220.0
                       }else{
                           constBillingAddress_H.constant = 40.0
                           constBillingAddressViewMore_H.constant = 144.0
                       }
      
            
        }
            heightScrollView()
        // showing Opportunity Schedule
        
        if(("\(dictOpportunity.value(forKey: "OpportunityStatus")!)" == "Open") && "\(dictOpportunity.value(forKey: "OpportunityStage")!)" == "Scheduled")
        {
            btnScheduleNow.setImage(UIImage(named: "checked.png"), for: .normal)
            
           if(DeviceType.IS_IPAD){
                          constScheduleNow_H.constant = 500.0
                      }else{
                          constScheduleNow_H.constant = 350.0
                          
            }
        }
        else
        {
            btnScheduleNow.setImage(UIImage(named: "uncheck.png"), for: .normal)
            
            constScheduleNow_H.constant = 0.0
            
        }
        heightScrollView()
        if "\(dictOpportunity.value(forKey: "OpportunityStatus")!)" == "Complete"
        {
            btnScheduleNow.isUserInteractionEnabled = false
        }
        
        
        if(("\(dictOpportunity.value(forKey: "FieldSalesPersonId")!)").count > 0)
        {
            // EmployeeList
            let defsLogindDetail = UserDefaults.standard
            let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
            
            if(arrOfData.count != 0)
            {
                for item in arrOfData
                {
                    let dict = (item as AnyObject) as! NSDictionary
                    if("\(dictOpportunity.value(forKey: "FieldSalesPersonId")!)" == "\(dict.value(forKey: "EmployeeId")!)")
                    {
                        txtFldFieldSalesPerson.text = dict["FullName"] as? String
                        strFieldSalesPersonId = "\(dictOpportunity.value(forKey: "FieldSalesPersonId")!)"
                    }
                }
            }
        }
        
        if strFieldSalesPersonId.count == 0 {
                                    
            txtFldFieldSalesPerson.text = strEmpName
            strFieldSalesPersonId = strEmpID
            
        }
        
        // showing schedule date
   /*     var strDate = "" , strTime = ""
        if(("\(dictOpportunity.value(forKey: "ScheduleDate")!)").count > 0)
        {
            strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunity.value(forKey: "ScheduleDate")!)")
        }
        if(("\(dictOpportunity.value(forKey: "ScheduleTime")!)").count > 0)
        {
            strTime = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunity.value(forKey: "ScheduleTime")!)", type: "time")
        }
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "EST")
        if (strDate.count == 0){
            formatter.dateFormat = "MM/dd/yyyy"
            strDate = formatter.string(from: date)
        }
        if (strTime.count == 0){
            formatter.dateFormat = "hh:mm a"
            strTime = formatter.string(from: date)
        }
        if(strDate.count != 0 && strTime.count != 0){
            txtFldScheduleDate.text = strDate + " " + strTime
        }
        */
        
        
        // showing estimated duration
        if(("\(dictOpportunity.value(forKey: "TotalEstimationTime")!)").count > 0)
        {
            
            let strEstTime = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunity.value(forKey: "TotalEstimationTime")!)", type: "")
            
            if(strEstTime?.count != 0)
            {
                txtFldEstimatedDuration.text = strEstTime!
            }
        }
        
        
        // showing drive time
        if(("\(dictOpportunity.value(forKey: "DriveTime")!)").count > 0)
        {
            
            let strDriveTime = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunity.value(forKey: "DriveTime")!)", type: "")
            
            if(strDriveTime?.count != 0)
            {
                txtFldDriveTime.text = strDriveTime!
                
            }
            
        }
        
        // Showing Submitted By
        if(("\(dictOpportunity.value(forKey: "SubmittedById")!)").count > 0)
        {
            
            let defsLogindDetail = UserDefaults.standard
            let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in arrOfData
            {
                if("\(dictOpportunity.value(forKey: "SubmittedById")!)" == "\((item as AnyObject).value(forKey: "EmployeeId")!)")
                {
                    txtSubmittedBy.text = "\((item as AnyObject).value(forKeyPath: "FullName")!)"
                    strSubmittedById = "\((item as AnyObject).value(forKeyPath: "EmployeeId")!)"
                    break
                }
            }
            
        }
        
        // ProposedAmount
        
        // Showing Submitted By
        if(("\(dictOpportunity.value(forKey: "ProposedAmount")!)").count > 0)
        {
            
            txtProposedAmount.text = "\(dictOpportunity.value(forKey: "ProposedAmount")!)"
            
        }
        
        // Showing Company Size
        if(("\(dictOpportunity.value(forKey: "SizeId")!)").count > 0)
        {
            txtFldCompanySize.text = dictOpportunity["CompanySize"] as? String
            strCompanySizeId = "\(dictOpportunity.value(forKeyPath: "SizeId")!)"
        }
        
        
        // Showing Industry
        
        if(("\(dictOpportunity.value(forKey: "IndustryId")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "TotalLeadCountResponse") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "IndustryMasters") as! NSArray)
            
            for item in arrOfData
            {
                if("\(dictOpportunity.value(forKey: "IndustryId")!)" == "\((item as AnyObject).value(forKey: "IndustryId")!)")
                {
                    txtFldIndustry.text = "\((item as AnyObject).value(forKey: "Name")!)"
                    
                    strIndustryId = "\(dictOpportunity.value(forKey: "IndustryId")!)"
                    break
                }
            }
        }
        
        // Showing FollowUpDate
        if(("\(dictOpportunity.value(forKey: "FollowUpDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunity.value(forKey: "FollowUpDate")!)")
            
            if(strDate?.count != 0)
            {
                txtFollowUpDate.text = strDate!
            }
        }
        
        // Showing GrabbedDate
        if(("\(dictOpportunity.value(forKey: "GrabbedDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunity.value(forKey: "GrabbedDate")!)")
            
            if(strDate?.count != 0)
            {
                txtGrabbedDate.text = strDate!

            }
            
        }
        
        // Showing Expected Closing Date
        if(("\(dictOpportunity.value(forKey: "ExpectedClosingDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunity.value(forKey: "ExpectedClosingDate")!)")
            
            if(strDate?.count != 0)
            {
                txtExpectedClosingDate.text = strDate!
            }
            
        }
        
        // Showing Description
        
        if(("\(dictOpportunity.value(forKey: "OpportunityDescription")!)").count > 0)
        {
            txtDescription.text = "\(dictOpportunity.value(forKey: "OpportunityDescription")!)"
        }
        
        // Service Address
        
        // Notes
        
        if(("\(dictOpportunity.value(forKey: "Notes")!)").count > 0)
        {
            txtServiceAddressNotes.text = "\(dictOpportunity.value(forKey: "Notes")!)"
        }
        
        // Showing direction
        
        if(("\(dictOpportunity.value(forKey: "Direction")!)").count > 0)
        {
            txtServiceAddressDirection.text = "\(dictOpportunity.value(forKey: "Direction")!)"
        }
        
        if dictOfServiceAddress.count > 0 {
            
            // Showing County
            
            if(("\(dictOfServiceAddress.value(forKey: "County")!)").count > 0)
            {
                txtServiceAddressCounty.text = "\(dictOfServiceAddress.value(forKey: "County")!)"
            }
            
            // Showing SchoolDistrict
            
            if(("\(dictOfServiceAddress.value(forKey: "SchoolDistrict")!)").count > 0)
            {
                txtServiceAddressSchoolDistrict.text = "\(dictOfServiceAddress.value(forKey: "SchoolDistrict")!)"
            }
            
            // Showing Mapcode
                   
                   
            if(("\(dictOfServiceAddress.value(forKey: "MapCode")!)").count > 0)
            {
                txtServiceAddressMapCode.text = "\(dictOfServiceAddress.value(forKey: "MapCode")!)"
            }
            
            // Showing Gatecpde
                   
            if(("\(dictOfServiceAddress.value(forKey: "GateCode")!)").count > 0)
            {
                txtServiceAddressGateCode.text = "\(dictOfServiceAddress.value(forKey: "GateCode")!)"
            }
            
        }
        
        // Is Tax Exempt
        
        if(("\(dictOpportunity.value(forKey: "IsTaxExempt")!)").count > 0)
        {
            if(("\(dictOpportunity.value(forKey: "IsTaxExempt")!)" == "true") || ("\(dictOpportunity.value(forKey: "IsTaxExempt")!)" == "True") || ("\(dictOpportunity.value(forKey: "IsTaxExempt")!)" == "TRUE") || ("\(dictOpportunity.value(forKey: "IsTaxExempt")!)" == "1"))
            {
                
                btnIsTaxExempt.setImage(UIImage(named: "checked.png"), for: .normal)
                constTaxExemption_H.constant = 40.0
                
                hghtScroll = hghtScroll + 40.0
                heightScrollView()
                txtTaxExemption.text = "\(dictOpportunity.value(forKey: "TaxExemptionNo") ?? "")"
                
            }
            else
            {
                            
                btnIsTaxExempt.setImage(UIImage(named: "uncheck.png"), for: .normal)
                constTaxExemption_H.constant = 0.0
                
                txtTaxExemption.text = ""
                
            }
        }
        
        if(("\(dictOpportunity.value(forKey: "TaxSysName")!)").count != 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictLeadDetailMaster.count != 0)
            {
                let arrOfData = (dictLeadDetailMaster.value(forKey: "TaxMaster") as! NSArray).mutableCopy() as! NSMutableArray
                
                //  strBranchNameGlobal = "Plumbing Branch"
                
                let arrayTaxCode = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchNameGlobal)
                
                if(arrayTaxCode.count > 0)
                {
                    for item in arrayTaxCode
                    {
                        let dictTaxCode = (item as AnyObject) as! NSDictionary
                        if("\(dictOpportunity.value(forKey: "TaxSysName")!)" == "\(dictTaxCode.value(forKey: "SysName")!)")
                        {
                            txtTaxCode.text = dictTaxCode["Name"] as? String
                            
                            strServiceTaxCode = "\(dictTaxCode.value(forKey: "SysName")!)"
                            break
                        }
                    }
                }
            }
        }
        
        if dictOfBillingAddress.count > 0 {

            // Showing County
            
            if(("\(dictOfBillingAddress.value(forKey: "County")!)").count > 0)
            {
                txtBillingAddressCounty.text = "\(dictOfBillingAddress.value(forKey: "County")!)"
            }
            
            // Showing SchoolDistrict
            
            if(("\(dictOfBillingAddress.value(forKey: "SchoolDistrict")!)").count > 0)
            {
                txtBillingAddressSchoolDistrict.text = "\(dictOfBillingAddress.value(forKey: "SchoolDistrict")!)"
            }
            
        }
        
        if dictOpportunity.value(forKeyPath: "ServiceAddress") is NSDictionary {
            
            if(("\(dictOpportunity.value(forKeyPath: "ServiceAddress.AddressSubType")!)").count > 0)
            {
                txtFldServiceAddSubType.text = "\(dictOpportunity.value(forKeyPath: "ServiceAddress.AddressSubType")!)"
            }
            else
            {
                txtFldServiceAddSubType.text = ""
            }
            
        }else{
            
            txtFldServiceAddSubType.text = ""
            
        }

        // --------Renge time
        
        if(nsud.value(forKey: "MasterSalesTimeRange") != nil){
            if(nsud.value(forKey: "MasterSalesTimeRange") is NSArray){
                self.aryTimeRange = nsud.value(forKey: "MasterSalesTimeRange") as! NSArray
            }
        }
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
        var rangeofTimeId = "" , scheduleDate = "" ,  scheduleTime = "" , scheduleTimeType = ""
        if(dictOpportunity.value(forKey: "ScheduleDate")  != nil){
            scheduleDate = "\(dictOpportunity.value(forKey: "ScheduleDate") ?? "")"
        }
        if(dictOpportunity.value(forKey: "ScheduleTime")  != nil){
            scheduleTime = "\(dictOpportunity.value(forKey: "ScheduleTime") ?? "")"

        }
        if(dictOpportunity.value(forKey: "RangeofTimeId")  != nil){
            rangeofTimeId = "\(dictOpportunity.value(forKey: "RangeofTimeId") ?? "")"

        }
        if(dictOpportunity.value(forKey: "ScheduleTimeType")  != nil){
            scheduleTimeType = "\(dictOpportunity.value(forKey: "ScheduleTimeType") ?? "")"
        }
        
        
        //-------------Scheduled TimeRangeId
        if rangeofTimeId != "" && rangeofTimeId != "0"{
            height_tf_Range.constant = DeviceType.IS_IPAD ? 60.0 : 44.0
            btn_Specific.setImage(UIImage(named: "redio_1"), for: .normal)
            btn_TimeRange.setImage(UIImage(named: "redio_2"), for: .normal)
            
            if scheduleDate.count > 0
            {
                tf_Date.text = changeStringDateToGivenFormat(strDate: scheduleDate, strRequiredFormat: "MM/dd/yyyy")
            }
      
            if scheduleTime.count > 0
            {
                tf_Time.text = changeStringDateToGivenFormat(strDate: scheduleTime, strRequiredFormat: "hh:mm a")
            }
            
            let aryTimeRange = self.aryTimeRange.filter { (task) -> Bool in
                return ("\((task as! NSDictionary).value(forKey: "RangeofTimeId")!)".lowercased().contains(rangeofTimeId))
            } as NSArray
            
            if(aryTimeRange.count != 0){
                let dictData = aryTimeRange.object(at: 0) as! NSDictionary
                let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
                let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
                let strrenge = StartInterval + " - " + EndInterval
                tf_Range.text = strrenge
                
                if tf_Time.text == ""
                {
                    tf_Time.text = StartInterval
                }

                self.arySelectedRange = NSMutableArray()
                self.arySelectedRange.add(dictData)
                if(rangeofTimeId == ""){
                    rangeofTimeId = "0"
                }
                tf_Range.tag = Int("\(rangeofTimeId)")!

            }
 
        }else{
            
            height_tf_Range.constant = 0.0
            btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
            btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
            
            if scheduleDate.count > 0
            {
                tf_Date.text = changeStringDateToGivenFormat(strDate: scheduleDate, strRequiredFormat: "MM/dd/yyyy")
            }
           
            
            if scheduleTime.count > 0
            {
                tf_Time.text = changeStringDateToGivenFormat(strDate: scheduleTime, strRequiredFormat: "hh:mm a") //"\(dataGeneralInfo.value(forKey: "scheduleTime") ?? "")"//

            }
        }
        
        
        
    }
    
    //Post Notification callBack
    @objc func handleNotificationPost(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            
            self.strCrmContactId = "\(dict.value(forKey: "CrmContactId") ?? "")"
            self.txtFldContact.text = Global().strFullName((dict as AnyHashable as! [AnyHashable : Any]))

            if self.strCrmContactId.count > 0  {
                
                // Calling API for Contact Address
                
                if (isInternetAvailable()){

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {

                        self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                        self.present(self.loader, animated: false, completion: nil)
                        
                        self.callApiToGetAddressViaCrmContactId(strCrmContactIdLocal: self.strCrmContactId, strType: "")
                        
                    }
                    
                }
                
            }
            
        }
    }
    func catchNotification(notification:Notification) {

        if let dict = notification.userInfo as NSDictionary? {

            strPrimaryService = "\(dict.value(forKey: "PrimaryService")!)"
            
            let arrOfServiceName = NSMutableArray()
            
            let strServiceName = fetchServiceNameViaId(strId: strPrimaryService)
            
            if strServiceName.count > 0 {
                
                arrOfServiceName.add(strServiceName)
                
            }
            
            let arrOfAdditionalServicesSelected = (dict.value(forKey: "AdditionalService")!) as! NSMutableArray
            
            if arrOfAdditionalServicesSelected.count > 0 {
                                
                arrOfAdditionalServicesSelected.addingObjects(from: arrOfAdditionalServicesSelected as! [Any])
                
            }
            
            for k in 0 ..< arrOfAdditionalServicesSelected.count {

                let strServiceNameLocal = fetchServiceNameViaId(strId: arrOfAdditionalServicesSelected[k] as! String)
                if strServiceNameLocal.count > 0 {
                    
                    arrOfServiceName.add(strServiceNameLocal)
                    
                }
                
            }
            
            if arrOfAdditionalServicesSelected.count > 0 {
                
                arrOfAdditionalService = arrOfAdditionalServicesSelected
                
            }
            
            txtFldService.text = arrOfServiceName.componentsJoined(by: ", ")
        
        }
    }
    
    func fetchServiceNameViaId(strId : String) -> String {
        
        var serviceName = ""
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        for k in 0 ..< arrOfData.count {
            
            if arrOfData[k] is NSDictionary {
                
                let dictOfData = arrOfData[k] as! NSDictionary
                
                if "\(dictOfData.value(forKey: "ServiceId")!)" == strId {
                    
                    serviceName = "\(dictOfData.value(forKey: "Name")!)"
                    break
                }
                
            }
            
        }
        
        return serviceName
        
    }
    
    func gotoPopViewWithArray(tagg: Int , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = tagg
        vc.arySelectedItems = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSelectionOnEditOpportunity = self
            
            // Adding Select String in array
            if ((tagg == 2) || (tagg == 3) || (tagg == 4) || (tagg == 5) || (tagg == 8) || (tagg == 9) || (tagg == 20) || (tagg == 17)) {
                
                vc.aryTBL = addSelectInArray(strName: "Name", array: aryData)

            } else if ((tagg == 7) || (tagg == 14) || (tagg == 23)) {
                
                vc.aryTBL = addSelectInArray(strName: "FullName", array: aryData)

            }else {
                
                vc.aryTBL = aryData

            }
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func gotoDatePickerView(tagg: Int, strType:String ,dateToSet: Date)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = tagg
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.dateToSet = dateToSet
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    func getAddress(from dataString: String) ->  NSMutableArray {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        let resultsArray = NSMutableArray()
        // put matches into array of Strings
        for match in matches {
            if match.resultType == .address,
                let components = match.addressComponents {
                resultsArray.add(components)
            } else {
                print("no components found")
            }
        }
        return resultsArray
    }
    
    func opportunityStageStatusFromId()
    {
        let dictTemp = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        let arrStageMaster = dictTemp.value(forKey: "LeadStageMasters") as! NSArray
        let arrStatusMaster = dictTemp.value(forKey: "LeadStatusMasters") as! NSArray
        print(arrStageMaster,arrStatusMaster)
        
        let arrStageName = NSMutableArray()
        let arrStageId = NSMutableArray()
        let arrStageSysName = NSMutableArray()
        let arrStatusName = NSMutableArray()
        let arrStatusId = NSMutableArray()
        let arrStatusSysName = NSMutableArray()
        
        if arrStageMaster.count > 0
        {
            for item in arrStageMaster
            {
                let dict = item as! NSDictionary
                arrStageName.add("\(dict.value(forKey: "Name")!)")
                arrStageId.add("\(dict.value(forKey: "LeadStageId")!)")
                arrStageSysName.add("\(dict.value(forKey: "SysName")!)")
            }
        }
        
        if arrStatusMaster.count > 0
        {
            for item in arrStatusMaster
            {
                let dict = item as! NSDictionary
                arrStatusName.add("\(dict.value(forKey: "StatusName")!)")
                arrStatusId.add("\(dict.value(forKey: "StatusId")!)")
                arrStatusSysName.add("\(dict.value(forKey: "SysName")!)")
            }
        }
        

        dictOpportunityStageNameFromId = NSDictionary(objects:arrStageName as! [Any], forKeys:arrStageId as! [NSCopying]) as Dictionary as NSDictionary
        dictOpportunityStageNameFromSysName = NSDictionary(objects:arrStageName as! [Any], forKeys:arrStageSysName as! [NSCopying]) as Dictionary as NSDictionary
        
        dictOpportunityStatusNameFromId = NSDictionary(objects:arrStatusName as! [Any], forKeys:arrStatusId as! [NSCopying]) as Dictionary as NSDictionary
        dictOpportunityStatusNameFromSysName = NSDictionary(objects:arrStatusName as! [Any], forKeys:arrStatusSysName as! [NSCopying]) as Dictionary as NSDictionary
        
        print(dictOpportunityStageNameFromId)
        print(dictOpportunityStageNameFromSysName)
        
    }
    
    // MARK: - -----------------------------------Validation Methods-----------------------------------
    
    func validateFields() -> String
    {
        var strMessage = ""
        
        let isTaxCodeReq = Global().alert(forTaxCode: strServiceTaxCode)

        if(strOpportunityType == "")
        {
            strMessage = "Please select opportunity type."
            return strMessage
        }
        if(txtFldOpportunityName.text?.count == 0)
        {
            strMessage = "Please enter oppportunity name."
            return strMessage
        }
        if(txtFldContact.text?.count == 0)
        {
            strMessage = "Please select contact."
            return strMessage
        }
        if btnScheduleNow.currentImage == UIImage(named: "checked.png")
        {
            
            // If Schedule Now Service Address and Billing Address Is Mandatory
            
            if(txtFldServiceAddress.text == "")
            {
                strMessage = "Please enter service address."
                return strMessage
            }
            if(txtFldFieldSalesPerson.text == "")
            {
                strMessage = "Please select field sales person."
                return strMessage
            }
            else if(tf_Date.text == "")
            {
                strMessage = "Please select schedule date."
                return strMessage
            }
            else if(tf_Time.text == "")
            {
                strMessage = "Please select schedule time."
                return strMessage
            }
            else if(tf_Time.text == "")
            {
                strMessage = "Please select schedule time."
                return strMessage
            }
            else if (btn_TimeRange.currentImage == UIImage(named: "redio_2") && tf_Range.text == "") {
                strMessage = "Please select time range."
                return strMessage
                }
//            else if(txtFldScheduleTime.text == "")
//            {
//                strMessage = "Please select schedule time."
//                return strMessage
//            }
            if (txtFldServiceAddress.text!.count > 0) {

                let dictOfEnteredAddress = formatEnteredAddress(value: txtFldServiceAddress.text!)
                
                if dictOfEnteredAddress.count == 0 {
                    
                    strMessage = "Please enter valid service address.\n\n Address Format (address 1, city, state, zipcode)."
                    return strMessage
                                        
                } else {
                    
                    
                    strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                    strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                    let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                    
                    if dictStateDataTemp.count > 0 {
                        
                        strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                        
                    }else{
                        
                        strStateId = ""

                    }
                    
                    strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                    
                    if strAddressLine1.count == 0 {
                                                
                        strMessage = "Please enter service address 1.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    if strCityName.count == 0 {
                                                
                        strMessage = "Please enter valid service city.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    if strStateId.count == 0 {
                                                
                        strMessage = "Please enter valid service state name.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    if strZipCode.count == 0 {
                                                
                        strMessage = "Please enter service zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    if strZipCode.count != 5 {
                                                
                        strMessage = "Please enter valid service zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    
                }
                
                
            }
            /*if ((txtFldServiceAddress.text!.count > 0)) {
                
                    // Fetch Address
                    
                let arrOfAddress = self.getAddress(from: txtFldServiceAddress.text!)
                
                let tempArrayOfKeys = NSMutableArray()
                let tempDictData = NSMutableDictionary()

                for k in 0 ..< arrOfAddress.count {
                 
                          if arrOfAddress[k] is NSDictionary {
                    
                             let tempDict = arrOfAddress[k] as! NSDictionary
                        
                             tempArrayOfKeys.addObjects(from: tempDict.allKeys)
                             tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])

                          }
                    
                }
                
                if tempArrayOfKeys.contains("City") {
                    
                    strCityName = "\(tempDictData.value(forKey: "City")!)"

                }
                if tempArrayOfKeys.contains("ZIP") {
                    
                    strZipCode = "\(tempDictData.value(forKey: "ZIP")!)"

                }
                if tempArrayOfKeys.contains("State") {
                    
                    let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
                    
                    if dictStateDataTemp.count > 0 {
                        
                        strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                        //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
                        
                    }else{
                        
                        strStateId = ""
                        //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)

                    }

                }
                if tempArrayOfKeys.contains("Street") {
                    
                    strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"

                }
                
                if strAddressLine1.count == 0 || strCityName.count == 0 || strZipCode.count == 0 || strStateId.count == 0 {
                    
                    
                    strMessage = "Please enter proper service address (address, city, zipcode, state)."
                    return strMessage
                    
                }
                
            }*/
            if btnSameAsServiceAddress.currentImage == UIImage(named: "uncheck.png")
            {
                
                if(txtFldBillingAddress.text == "")
                {
                    strMessage = "Please enter billing address."
                    return strMessage
                }
                
                if (txtFldBillingAddress.text!.count > 0) {

                    let dictOfEnteredAddress = formatEnteredAddress(value: txtFldBillingAddress.text!)
                    
                    if dictOfEnteredAddress.count == 0 {
                        
                        strMessage = "Please enter valid billing address.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                                            
                    } else {
                        
                        
                        strBillingCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                        strBillingZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                        let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                        
                        if dictStateDataTemp.count > 0 {
                            
                            strBillingStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                            
                        }else{
                            
                            strBillingStateId = ""

                        }
                        
                        strBillingAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                        
                        if strBillingAddressLine1.count == 0 {
                                                    
                            strMessage = "Please enter billing address 1.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        if strBillingCityName.count == 0 {
                                                    
                            strMessage = "Please enter valid billing city.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        if strBillingStateId.count == 0 {
                                                    
                            strMessage = "Please enter valid billing state name.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        if strBillingZipCode.count == 0 {
                                                    
                            strMessage = "Please enter billing zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        if strBillingZipCode.count != 5 {
                                                    
                            strMessage = "Please enter valid billing zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        
                    }
                    
                    
                }
                
                /*if ((txtFldBillingAddress.text!.count > 0)) {
                    
                        // Fetch Address
                        
                    let arrOfAddress = self.getAddress(from: txtFldBillingAddress.text!)
                    
                    let tempArrayOfKeys = NSMutableArray()
                    let tempDictData = NSMutableDictionary()

                    for k in 0 ..< arrOfAddress.count {
                     
                              if arrOfAddress[k] is NSDictionary {
                        
                                 let tempDict = arrOfAddress[k] as! NSDictionary
                            
                                 tempArrayOfKeys.addObjects(from: tempDict.allKeys)
                                 tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])

                              }
                        
                    }
                    
                    if tempArrayOfKeys.contains("City") {
                        
                        strBillingCityName = "\(tempDictData.value(forKey: "City")!)"

                    }
                    if tempArrayOfKeys.contains("ZIP") {
                        
                        strBillingZipCode = "\(tempDictData.value(forKey: "ZIP")!)"

                    }
                    if tempArrayOfKeys.contains("State") {
                        
                        let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
                        
                        if dictStateDataTemp.count > 0 {
                            
                            strBillingStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                            //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
                            
                        }else{
                            
                            strBillingStateId = ""
                            //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)

                        }

                    }
                    if tempArrayOfKeys.contains("Street") {
                        
                        strBillingAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"

                    }
                    
                    if strBillingAddressLine1.count == 0 || strBillingCityName.count == 0 || strBillingZipCode.count == 0 || strBillingStateId.count == 0 {
                        
                        
                        strMessage = "Please enter proper billing address (address, city, zipcode, state)."
                        return strMessage
                        
                    }
                    
                }*/
            }
            if (isTaxCodeReq && (txtTaxCode.text?.count == 0)) {
                
                strMessage = "Please select tax code."
                return strMessage
                
            }
            
        }
        else
        {
            
            if ((txtFldServiceAddress.text!.count > 0))
            {

                let dictOfEnteredAddress = formatEnteredAddress(value: txtFldServiceAddress.text!)
                
                if dictOfEnteredAddress.count == 0 {
                    
                    strMessage = "Please enter valid service address.\n\n Address Format (address 1, city, state, zipcode)."
                    return strMessage
                                        
                } else {
                    
                    
                    strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                    strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                    let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                    
                    if dictStateDataTemp.count > 0 {
                        
                        strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                        
                    }else{
                        
                        strStateId = ""

                    }
                    
                    strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                    
                    if strAddressLine1.count == 0 {
                                                
                        strMessage = "Please enter service address 1.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    if strCityName.count == 0 {
                                                
                        strMessage = "Please enter valid service city.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    if strStateId.count == 0 {
                                                
                        strMessage = "Please enter valid service state name.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    if strZipCode.count == 0 {
                                                
                        strMessage = "Please enter service zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    if strZipCode.count != 5 {
                                                
                        strMessage = "Please enter valid service zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                    }
                    
                }
                
                
            }
            if btnSameAsServiceAddress.currentImage == UIImage(named: "uncheck.png") {
                
                if ((txtFldBillingAddress.text!.count > 0)) {

                    let dictOfEnteredAddress = formatEnteredAddress(value: txtFldBillingAddress.text!)
                    
                    if dictOfEnteredAddress.count == 0 {
                        
                        strMessage = "Please enter valid billing address.\n\n Address Format (address 1, city, state, zipcode)."
                        return strMessage
                                            
                    } else {
                        
                        
                        strBillingCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                        strBillingZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                        let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                        
                        if dictStateDataTemp.count > 0 {
                            
                            strBillingStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                            
                        }else{
                            
                            strBillingStateId = ""

                        }
                        
                        strBillingAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                        
                        if strBillingAddressLine1.count == 0 {
                                                    
                            strMessage = "Please enter billing address 1.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        if strBillingCityName.count == 0 {
                                                    
                            strMessage = "Please enter valid billing city.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        if strBillingStateId.count == 0 {
                                                    
                            strMessage = "Please enter valid billing state name.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        if strBillingZipCode.count == 0 {
                                                    
                            strMessage = "Please enter billing zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        if strBillingZipCode.count != 5 {
                                                    
                            strMessage = "Please enter valid billing zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                            return strMessage
                        }
                        
                    }
                    
                    
                }
            }
            
        }
        /*if(txtFldProjectedAmount.text?.count > 10)
        {
            strMessage = "Projected amount can't be greater than 10 characters"
            return strMessage
        }*/
        if(txtSubmittedBy.text?.count == 0)
        {
            strMessage = "Please select submitted by"
            return strMessage
        }
        if(txtFldEstimatedDuration.text?.count != 0)
        {
            if(txtFldEstimatedDuration.text!.contains(":"))
            {
                let arrDuration = txtFldEstimatedDuration.text?.components(separatedBy: ":")
                
                if(Int(arrDuration![0])! > 24)
                {
                    strMessage = "Please enter estimation duration hrs less than 24 hrs"
                    return strMessage
                }
                if(Int(arrDuration![0])! == 24 && Int(arrDuration![1])! > 0)
                {
                    strMessage = "Please enter estimation duration less than 24 hrs"
                    return strMessage
                }
                if(Int(arrDuration![1])! > 60)
                {
                    strMessage = "Please enter estimation duration minutes less than 60 minutes"
                    return strMessage
                }
                
            }
            else
            {
                if(Int(txtFldEstimatedDuration.text!)! > 24)
                {
                    strMessage = "Please enter estimation duration less than 24 hrs"
                    return strMessage
                }
            }
        }
        
        if(txtFldDriveTime.text?.count != 0)
        {
            if(txtFldDriveTime.text!.contains(":"))
            {
                let arrDriveTime = txtFldDriveTime.text?.components(separatedBy: ":")
                
                if(Int(arrDriveTime![0])! > 24)
                {
                    strMessage = "Please enter drive time hrs less than 24 hrs"
                    return strMessage
                }
                if(Int(arrDriveTime![0])! == 24 && Int(arrDriveTime![1])! > 0)
                {
                    strMessage = "Please enter drive time less than 24 hrs"
                    return strMessage
                }
                if(Int(arrDriveTime![1])! > 60)
                {
                    strMessage = "Please enter drive time minutes less than 60 minutes"
                    return strMessage
                }
            }
            else
            {
                if(Int(txtFldDriveTime.text!)! > 24)
                {
                    strMessage = "Please enter drive time less than 24 hrs"
                    return strMessage
                }
            }
        }
        return strMessage
    }
    
    // MARK: -----------------------------------Button Actions----------------------------------------

    @IBAction func action_Back(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        //dismiss(animated: false)

        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func action_Save(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if isInternetAvailable() == true
        {// call api to update opportunity
            
            
            let strMsg = validateFields()
            if(strMsg == "")
            {
                updateOpportunity()
                //showAlertWithoutAnyAction(strtitle: Info, strMessage: "Opportunity saved successfully", viewcontrol: self)
                
                //self.navigationController?.popViewController(animated: false)

            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMsg, viewcontrol: self)
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        
    }
    
    @IBAction func action_Commercial(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        sender.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnResidential.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        strOpportunityType = enumCommercial


    }
    
    @IBAction func action_Residential(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        sender.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCommercial.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        strOpportunityType = enumResidential
        
    }
    
    @IBAction func action_Contact(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
           let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
          
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "EditOpportunityCRM"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
        
    @IBAction func action_ClearServiceAddress(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        txtFldServiceAddress.text = ""

        self.txtFldServiceAddress.isUserInteractionEnabled = true

    }
    
    @IBAction func action_SelectServiceAddress(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if(arrOfServiceAddress.count > 0)
        {
            gotoPopViewWithArray(tagg: 17, aryData: arrOfServiceAddress , strTitle: "")
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    @IBAction func action_PrimaryReasonForCall(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictLeadDetailMaster.count != 0)
        {
            let arrOfData = (dictLeadDetailMaster.value(forKey: "Categories") as! NSArray).mutableCopy() as! NSMutableArray
            
            var arrOfFilteredData = returnFilteredArray(array: arrOfData)
            
            //            strDepartmentNameGlobal = "testdep"
            strDepartmentNameGlobal = "\(dictOpportunity.value(forKey: "DepartmentSysName")!)"
            
            arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
            
            gotoPopViewWithArray(tagg: 3, aryData: returnFilteredArray(array: arrOfFilteredData) , strTitle: "")
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    @IBAction func action_SelectService(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if strBranchNameGlobal.count > 0 {
                    
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            var arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
            arrOfData = returnFilteredArray(array: arrOfData)
            if(arrOfData.count != 0){
                
                let controller = storyboardLeadProspect.instantiateViewController(withIdentifier: "SelectServiceVC") as! SelectServiceVC
                controller.strFromWhere = "EditOpportunityCRM"
                controller.strServiceIdPrimary = strPrimaryService
                controller.strBranchSysName = strBranchNameGlobal
                controller.arrayServicesSelected = arrOfAdditionalServiceSelected
                self.navigationController?.present(controller, animated: false, completion: nil)

            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select branch", viewcontrol: self)
            
        }

    }
    
    @IBAction func action_InsideSalesPerson(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        let defsLogindDetail = UserDefaults.standard
        let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(tagg: 14, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
    }
    
    @IBAction func action_Source(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        goToNewSelectionVC(arrOfData: returnFilteredArray(array: arrOfData), tag: 100, titleHeader: "Source")

        //gotoPopViewWithArray(tagg: 4 , aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_Urgency(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "UrgencyMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(tagg: 2, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_SameAsServiceAddress(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if sender.currentImage == UIImage(named: "checked.png")
        {
            
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
            
            if(DeviceType.IS_IPAD){
                constBillingAddress_H.constant = 60.0
                constBillingAddressViewMore_H.constant = 220.0
            }else{
                constBillingAddress_H.constant = 40.0
                constBillingAddressViewMore_H.constant = 144.0
            }
            
        }
        else
        {
            sender.setImage(UIImage(named: "checked.png"), for: .normal)
            constBillingAddress_H.constant = 0.0
            constBillingAddressViewMore_H.constant = 0.0
            strBillingCustomerAddressId = ""
        }
        heightScrollView()
    }
    
    @IBAction func action_ClearBillingAddress(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        txtFldBillingAddress.text = ""

        self.txtFldBillingAddress.isUserInteractionEnabled = true

    }
    
    @IBAction func action_SelectBillingAddress(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if(arrOfBillingAddress.count > 1)
        {
            gotoPopViewWithArray(tagg: 21, aryData: arrOfBillingAddress , strTitle: "")
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    @IBAction func action_ScheduleNow(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if sender.currentImage == UIImage(named: "checked.png") {
            
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
            constScheduleNow_H.constant = 0.0
            
            
        } else {
            
            sender.setImage(UIImage(named: "checked.png"), for: .normal)
            
            if(DeviceType.IS_IPAD){
                constScheduleNow_H.constant = 500.0
                
            }else{
                constScheduleNow_H.constant = 350.0
                
            }
            
            
            
            
        }
    heightScrollView()
    }
    
    @IBAction func action_FieldSalesPerson(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let isFieldPersonAssign = nsud.bool(forKey: "isFieldPersonAssign")

        if isFieldPersonAssign {
            
            let defsLogindDetail = UserDefaults.standard
            let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
            
            gotoPopViewWithArray(tagg: 23, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Not authorized to change", viewcontrol: self)
            
        }
        
    }
    
    @IBAction func action_ScheduleDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        let fromDate = dateFormatter.date(from:((txtFldScheduleDate.text!.count) > 0 ? txtFldScheduleDate.text! : result))!
        
        self.gotoDatePickerView(tagg: 15, strType: "DateTime",dateToSet: fromDate)

    }
    
    @IBAction func action_ScheduleTime(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
       // self.gotoDatePickerView(tagg: 16, strType: "Time")

    }
    
    @IBAction func action_MoreInfo(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if sender.currentImage == UIImage(named: "arrow_2.png") {
            
            sender.setImage(UIImage(named: "detail_less_CRM"), for: .normal)
            constMoreInfoView_H.constant = 0.0
          

        } else {
            
            sender.setImage(UIImage(named: "arrow_2.png"), for: .normal)
            if(DeviceType.IS_IPAD){
                  constMoreInfoView_H.constant = 1250.0
            }else{
                  constMoreInfoView_H.constant = 900.0
            }
          
           

        }
 heightScrollView()
    }
    
    @IBAction func action_SubmittedBy(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        let defsLogindDetail = UserDefaults.standard
        let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(tagg: 7, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_CompanySIze(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "SizeMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(tagg: 8, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_Industry(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "IndustryMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(tagg: 9, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")

    }
    
    @IBAction func action_FollowUpDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        self.gotoDatePickerView(tagg: 10, strType: "Date", dateToSet: Date())

    }
    
    @IBAction func action_GrabbedDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        self.gotoDatePickerView(tagg: 11, strType: "Date", dateToSet: Date())

    }
    
    @IBAction func action_ExpectedClosingDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        self.gotoDatePickerView(tagg: 12, strType: "Date", dateToSet: Date())

    }
    
    @IBAction func action_IsTaxExempt(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if sender.currentImage == UIImage(named: "checked.png") {
            
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
            constTaxExemption_H.constant = 0.0

         
            heightScrollView()
            
        } else {
            sender.setImage(UIImage(named: "checked.png"), for: .normal)
            if(DeviceType.IS_IPAD){
                constTaxExemption_H.constant = 60.0
            }else{
                constTaxExemption_H.constant = 40.0
            }
        }
         heightScrollView()
    }
    
    @IBAction func action_TaxCode(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if strBranchNameGlobal.count > 0 {
            
            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictLeadDetailMaster.count != 0)
            {
                let arrOfData = (dictLeadDetailMaster.value(forKey: "TaxMaster") as! NSArray).mutableCopy() as! NSMutableArray
                
                gotoPopViewWithArray(tagg: 20, aryData: returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchNameGlobal ) , strTitle: "")
                
            }
            else
            {
                Global().displayAlertController(Alert, NoDataAvailableee, self)
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select branch.", viewcontrol: self)

        }

        
    }
    @IBAction func action_ServiceAddressSubType(_ sender: Any)
    {
        
        let myArrayOfDict: NSMutableArray = [
            ["Name": "Residential"]
            ,   ["Name": "Commercial"]
        ]
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        gotoPopViewWithArray(tagg: 1, aryData: myArrayOfDict , strTitle: "")
    }
    
    @IBAction func actionOnCancelContact(_ sender: Any)
    {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtFldContact.text!.count > 0 {

            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtFldContact.text = ""
                self.strCrmContactId = ""
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func actionOnCancelCompany(_ sender: Any) {
    }
        
    @IBAction func action_AddAddress(_ sender: UIButton) {
          
        let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
        let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
        vc.tag = sender.tag // 1 for service address 2 for billing address
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: false, completion: {})
        
    }
    @IBAction func action_OnDate(_ sender: UIButton) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Date.text?.count)! > 0 ? tf_Date.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.date = fromDate
        datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Service Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Date.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
      
    }
    @IBAction func action_OnTime(_ sender: UIButton) {
   
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Time.text?.count)! > 0 ? tf_Time.text! : result))!
        
        let datePicker = UIDatePicker()
       // datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.time
        datePicker.date = fromDate
      //  datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)

        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate, Picker: datePicker, view: self.view ,strTitle : "Select Service Time")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Time.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func action_OnSelectRange(_ sender: Any) {
        self.view.endEditing(true)
        if(aryTimeRange.count != 0){
            goToNewSalesSelectionVC(arrItem: aryTimeRange.mutableCopy()as! NSMutableArray, arrSelectedItem: self.arySelectedRange, tag: 1, titleHeader: "Time Range", isMultiSelection: false, ShowNameKey: "StartInterval,EndInterval")
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
    }

    @IBAction func action_OnSpecific(_ sender: Any) {
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
        arySelectedRange = NSMutableArray()
        tf_Range.text = ""
        tf_Range.tag = 0
    }
    @IBAction func action_OnTimeRange(_ sender: Any) {
        height_tf_Range.constant = DeviceType.IS_IPAD ? 50.0 : 44.0
        btn_Specific.setImage(UIImage(named: "redio_1"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_2"), for: .normal)

    }
    @IBAction func action_OnThirdParty(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.currentImage == UIImage(named: "checked.png") {
            btnThirdParty.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }else{
            btnThirdParty.setImage(UIImage(named: "checked.png"), for: .normal)

        }
 
        
    }
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.delegate = self
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------

extension EditOpportunityCRM_VC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
                
        if(textField == txtFldServiceAddress)
        {
            //self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 150), animated: false)
            self.scrollView.isScrollEnabled = false
            isServiceAddress = true
            
        }
        
        if(textField == txtFldBillingAddress)
        {
           // self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 500), animated: false)
            self.scrollView.isScrollEnabled = false
            isServiceAddress = false

        }
        if(textField == txtFldProjectedAmount)
        {
           // self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 200 ), animated: false)
            self.scrollView.isScrollEnabled = true
        }
        
        if(textField == txtProposedAmount || textField == txtDescription || textField == txtFldEstimatedDuration || textField == txtFldDriveTime)
        {
          //  self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 700 ), animated: false)
            self.scrollView.isScrollEnabled = true
        }
        if(textField == txtTaxExemption || textField == txtServiceAddressNotes  || textField == txtServiceAddressDirection || textField == txtBillingAddressCounty  || textField == txtBillingAddressSchoolDistrict  || textField == txtBillingAddressMapCode)
        {
            //self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 1000 ), animated: false)
            self.scrollView.isScrollEnabled = true
        }
        if(textField == txtServiceAddressMapCode || textField == txtServiceAddressGateCode || textField == txtServiceAddressCounty || textField == txtServiceAddressSchoolDistrict)
        {
           // self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 1000 ), animated: false)
            self.scrollView.isScrollEnabled = true
        }
        if( textField == txtBillingAddressCounty  || textField == txtBillingAddressSchoolDistrict  || textField == txtBillingAddressMapCode)
        {
           // self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 1250 ), animated: false)
            self.scrollView.isScrollEnabled = true
        }
         
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtFldServiceAddress){

            self.scrollView.isScrollEnabled = true
            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            
        }
        if(textField == txtFldBillingAddress){

            self.scrollView.isScrollEnabled = true
            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            
        }
        if(textField == txtTaxExemption)
        {
            //self.scrollView.setContentOffset(CGPoint(x: 0.0, y: txtTaxExemption.frame.maxY ), animated: false)
            self.scrollView.isScrollEnabled = true
            //isServiceAddress = false
            //
           
            
            // move the root view up by the distance of keyboard height
            //self.view.frame.origin.y = 0
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        
        if textField == txtFldEstimatedDuration {
            
            return validateTimeFormat(textField: textField, range: range, string: string as NSString)
            
        } else if textField == txtFldDriveTime {
            
            return validateTimeFormat(textField: textField, range: range, string: string as NSString)
            
        }  else if ( textField == txtProposedAmount || textField == txtFldProjectedAmount)
        {
            if textField == txtFldProjectedAmount
            {
                if string == "."
                {
                    return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 10)
                    
                }
                else
                {
                    return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 9)
                    
                }
            }
            else
            {
                
                return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 10)
            }
            
        }
        else if (textField == txtServiceAddressCounty || textField == txtServiceAddressSchoolDistrict || textField == txtServiceAddressMapCode || textField == txtServiceAddressGateCode){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 300)
            
        } else if (textField == txtBillingAddressCounty || textField == txtBillingAddressSchoolDistrict || textField == txtBillingAddressMapCode){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 300)
            
        }
        else if(textField == txtTaxExemption)
        {
            return txtFieldValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 10) //NUMBER
        }
        if((textField == txtFldServiceAddress) || (textField == txtFldBillingAddress)){
                        
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()
                    
                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = ""
            
            if (textField == txtFldServiceAddress) {
                
                txtAfterUpdate = txtFldServiceAddress.text! as NSString
                
            } else {
                
                txtAfterUpdate = txtFldBillingAddress.text! as NSString
                
            }
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()
                
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtFldServiceAddress){

            let txtAfterUpdate:NSString = txtFldServiceAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            
//            self.getPlaces(with: parameters) {
//                self.places = $0
//            }
            return true
        }
        
                if(textField == txtFldBillingAddress){

                    let txtAfterUpdate:NSString = txtFldBillingAddress.text! as NSString
                    if(txtAfterUpdate == ""){
                        places = [];
                        return true
                    }
                    let parameters = getParameters(for: txtAfterUpdate as String)
                    
        //            self.getPlaces(with: parameters) {
        //                self.places = $0
        //            }
                    return true
                }
        
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
extension EditOpportunityCRM_VC {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }
                else
                {
                   /* for item in self.scrollView.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)*/
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    
                   // self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    
                    if self.isServiceAddress {
                        
                       
                        

                        self.tableView.frame = CGRect(x: self.txtFldServiceAddress.frame.origin.x, y: self.txtFldServiceAddress.frame.maxY + 10  , width: self.txtFldContact.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)
                        
                        //self.tableView.frame = CGRect(x: self.txtFldServiceAddress.frame.origin.x, y: self.txtFldPrimaryReasonForCall.frame.maxY , width: self.txtFldContact.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)


                        

                    } else {
                        
                       
                        self.tableView.frame = CGRect(x: self.txtFldBillingAddress.frame.origin.x, y: self.txtFldBillingAddress.frame.maxY + 10 , width: self.txtFldContact.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)


                    }


                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension EditOpportunityCRM_VC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            
            if self.isServiceAddress {
                
                self.txtFldServiceAddress.text = value.formattedAddress
                self.txtFldServiceAddress.text = addressFormattedByGoogle(value: value)
                self.strServiceCustomerAddressId = ""


            }else{
                
                self.txtFldBillingAddress.text = value.formattedAddress
                self.txtFldBillingAddress.text = addressFormattedByGoogle(value: value)
                self.strBillingCustomerAddressId = ""

            }
            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()

        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}


// MARK:- -----------------------------------Selection Delegates -----------------------------------

extension EditOpportunityCRM_VC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        
        if tag == 1 {
            
            //            btn_State.setTitle(dictData["Name"] as? String, for: .normal)
            //            strStateId = "\(dictData.value(forKeyPath: "StateId")!)"
            
            if dictData["Name"] as? String  == strSelectString {
                
                txtFldServiceAddSubType.text = ""
                strBillingZipCode = ""
                
            }else{
             
                txtFldServiceAddSubType.text = dictData["Name"] as? String
                
            }
            
        } else if tag == 2{
            
            if dictData["Name"] as? String  == strSelectString {
                
                txtFldUrgency.text = ""
                strUrgencyId = ""
                
            }else{
             
                txtFldUrgency.text = dictData["Name"] as? String
                strUrgencyId = "\(dictData.value(forKeyPath: "UrgencyId")!)"
                
            }

        }else if tag == 3{
            
            if dictData["Name"] as? String  == strSelectString {
                
                txtFldPrimaryReasonForCall.text = ""
                strPrimaryReasonForCallDepSysName = ""
                strServiceCategoryID = ""
                txtFldService.text = ""
                strPrimaryService = ""
                arrOfAdditionalServiceSelected = NSMutableArray()
                
            }else{
             
                txtFldPrimaryReasonForCall.text = dictData["Name"] as? String
                strPrimaryReasonForCallDepSysName = "\(dictData.value(forKeyPath: "DepartmentSysName")!)"
                strServiceCategoryID = "\(dictData.value(forKeyPath: "CategoryId")!)"
                txtFldService.text = ""
                strPrimaryService = ""
                arrOfAdditionalServiceSelected = NSMutableArray()
                
            }
            
        }else if tag == 4{
            
            if dictData["Name"] as? String  == strSelectString {
                
                //SourceId
                arrSourceId = NSMutableArray()
                arrSelectedSource = NSMutableArray()
                txtFldSource.text = ""
                
            }else{
             
                //SourceId
                let arySource = dictData.object(forKey: "source")as! NSArray
                print(arySource)
                var strName = ""
                arrSourceId = NSMutableArray()
                
                arrSelectedSource = NSMutableArray()
                
                arrSelectedSource = arySource.mutableCopy() as! NSMutableArray
                
                for item in arySource{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                    arrSourceId.add("\((item as AnyObject).value(forKey: "SourceId")!)")
                }
                if strName == ""
                {
                    strName = ""
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtFldSource.text = strName
                
            }
            
        }else if tag == 5{
            
            if dictData["Name"] as? String  == strSelectString {
                
                txtFldService.text = ""
                strServiceId = ""
                strPrimaryService = ""

            }else{
             
                txtFldService.text = dictData["Name"] as? String
                strServiceId = "\(dictData.value(forKeyPath: "ServiceId")!)"
                strPrimaryService = strServiceId

            }
            
        }else if tag == 6{
            
            //btn_OpportunityType.setTitle(dictData["Name"] as? String, for: .normal)
            //strOpportunityType = "\(dictData.value(forKeyPath: "Name")!)"
            
        }else if tag == 7{
            
            if dictData["FullName"] as? String  == strSelectString {
                
                txtSubmittedBy.text = ""
                strSubmittedById = ""
                
            }else{
             
                txtSubmittedBy.text = dictData["FullName"] as? String
                strSubmittedById = "\(dictData.value(forKeyPath: "EmployeeId")!)"
                
            }
            
        }else if tag == 8{
            
            if dictData["Name"] as? String  == strSelectString {
                
                txtFldCompanySize.text = ""
                strCompanySizeId = ""
                
            }else{
             
                txtFldCompanySize.text = dictData["Name"] as? String
                strCompanySizeId = "\(dictData.value(forKeyPath: "SizeId")!)"
                
            }
            
        }else if tag == 9{
            //IndustryId
            
            if dictData["Name"] as? String  == strSelectString {
                
                txtFldIndustry.text = ""
                strIndustryId = ""
                
            }else{
             
                txtFldIndustry.text = dictData["Name"] as? String
                strIndustryId = "\(dictData.value(forKeyPath: "IndustryId")!)"
                
            }
            
        }else if tag == 14{
            
            if dictData["FullName"] as? String  == strSelectString {
                
                txtFldInsideSalesPerson.text = ""
                strInsideSalesPersonId = ""
                
            }else{
             
                txtFldInsideSalesPerson.text = dictData["FullName"] as? String
                strInsideSalesPersonId = "\(dictData.value(forKeyPath: "EmployeeId")!)"
                
            }
            
        }else if tag == 17{
            
            if dictData.count == 0 {
                
                txtFldServiceAddress.text = ""
                
                // Setting values from Address
                
                strAddressLine1 = ""
                strCityName = ""
                strZipCode = ""
                txtServiceAddressCounty.text = ""
                txtServiceAddressSchoolDistrict.text = ""
                //txtFld_ServiceMapCode.text = ""
                //txtFld_ServiceGateCode.text = ""
                strStateId = ""
                strServiceCustomerAddressId = ""
                
                self.txtFldServiceAddress.isUserInteractionEnabled = true //false

                
            }else{
                
                txtFldServiceAddress.text = Global().strCombinedAddress((dictData as! [AnyHashable : Any]))
                
                let arrTemp = dictData.allKeys as NSArray
                
                if arrTemp.contains("CustomerAddressId") {
                                          
                    strServiceCustomerAddressId = checkNullValue(str: "\(dictData.value(forKeyPath: "CustomerAddressId")!)")

                }else{
                    
                    strServiceCustomerAddressId = ""
                    
                }
                
                self.txtFldServiceAddress.isUserInteractionEnabled = true //false


            }
            
        }else if tag == 18{
            
            //btn_ServiceState.setTitle(dictData["Name"] as? String, for: .normal)
            //strStateIdService = "\(dictData.value(forKeyPath: "StateId")!)"
            
        }else if tag == 19{
            
            //btn_ServiceAddressSubtype.setTitle(dictData["Name"] as? String, for: .normal)
            //strServiceAddressSubType = "\(dictData.value(forKeyPath: "Name")!)"
            
        }else if tag == 20{
            
            if dictData["Name"] as? String  == strSelectString {
                
                txtTaxCode.text = ""
                strServiceTaxCode = ""
                
            }else{
             
                txtTaxCode.text = dictData["Name"] as? String
                strServiceTaxCode = "\(dictData.value(forKeyPath: "SysName")!)"
                
            }
            
        }else if tag == 21{
            
            if dictData.count == 0{
                
                txtFldBillingAddress.text = ""
                
                // Setting values from Address
                
                strBillingAddressLine1 = ""
                strBillingCityName = ""
                strBillingZipCode = ""
                txtBillingAddressCounty.text = ""
                txtBillingAddressSchoolDistrict.text = ""
                //txtFld_BillingMapCode.text = ""
                strBillingCustomerAddressId = ""
                
                self.txtFldBillingAddress.isUserInteractionEnabled = true

            }else {
                
                txtFldBillingAddress.text = Global().strCombinedAddress((dictData as! [AnyHashable : Any]))
                
                strBillingCustomerAddressId = checkNullValue(str: "\(dictData.value(forKeyPath: "CustomerAddressId")!)")

                self.txtFldBillingAddress.isUserInteractionEnabled = true //false

                
            }
            
        }else if tag == 22{
            
            //btn_BillingState.setTitle(dictData["Name"] as? String, for: .normal)
            //strStateIdBilling = "\(dictData.value(forKeyPath: "StateId")!)"
            
        }else if tag == 23{
            
            if dictData["FullName"] as? String  == strSelectString {
                
                txtFldFieldSalesPerson.text = ""
                strFieldSalesPersonId = ""
                
            }else{
             
                txtFldFieldSalesPerson.text = dictData["FullName"] as? String
                strFieldSalesPersonId = "\(dictData.value(forKeyPath: "EmployeeId")!)"
                
            }
            
        }
    }
}

// MARK: - -----------------------------------Date Picker Protocol-----------------------------------

//protocol datePickerView : class{
//
//    func setDateOnSelction(strDate : String ,tag : Int)
//
//}
extension EditOpportunityCRM_VC: DatePickerProtocol
{
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if tag == 10 {
            
            txtFollowUpDate.text = strDate
            
            
        } else if tag == 11 {
            
            txtGrabbedDate.text = strDate
            
        }else if tag == 12 {
            
            txtExpectedClosingDate.text = strDate
            
        }else if tag == 13 {
            
            //btn_SubmittedDate.setTitle(strDate, for: .normal)
            
        }else if tag == 16 {
            
            //btn_Time.setTitle(strDate, for: .normal)
        //    txtFldScheduleTime.text = strDate

            let dateString = getTimeStringIn24HrsFormatFrom12HrsFormat(timeToConvert: strDate)
            
            if(dateString.count != 0)
            {
                //strTime = dateString
            }
            
        }else if tag == 15 {
            
            txtFldScheduleDate.text = strDate
            
        }
    }
}

// MARK: -
// MARK: - -----------------------Source Selction Delgates

extension EditOpportunityCRM_VC : SelectGlobalVCDelegate{
    
    func getDataSelectGlobalVC(dictData: NSMutableDictionary, tag: Int) {
        
        if tag == 100 {
            
            if(dictData.count == 0){
                
                arrSourceId = NSMutableArray()
                arrSelectedSource = NSMutableArray()
                txtFldSource.text = ""

            }else{
                
                let arySource = dictData.object(forKey: "source")as! NSArray
                print(arySource)
                var strName = ""
                arrSourceId = NSMutableArray()
                
                arrSelectedSource = NSMutableArray()
                
                arrSelectedSource = arySource.mutableCopy() as! NSMutableArray
                
                for item in arySource{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                    arrSourceId.add("\((item as AnyObject).value(forKey: "SourceId")!)")
                }
                if strName == ""
                {
                    strName = ""
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtFldSource.text = strName
                
            }
            
        }
        
    }

}
// MARK: -
// MARK: - -----------------------Add Address Delgates

extension EditOpportunityCRM_VC : AddNewAddressiPhoneDelegate{

    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {
        
        print(dictData)
        //Service address
        if(tag == 1){
            self.txtFldServiceAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
            self.strServiceCustomerAddressId = ""
        }
            // Billing Address
        else if (tag == 2){
            self.txtFldBillingAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
            self.strBillingCustomerAddressId = ""
        }
        
        
        /*if self.txtFldAddress.text?.count != 0 {
            
            let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                              
            let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                
                self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                self.strCustomerAddressId = ""
                
            })
            
            let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                
                
                
            })
            
            alertCOntroller.addAction(alertAction)
            alertCOntroller.addAction(alertActionCancel)

            self.present(alertCOntroller, animated: true, completion: nil)
            
        }else{
            
            self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
            self.strCustomerAddressId = ""
            
        }*/

    }

}
// MARK: -
// MARK: - SalesNew_SelectionProtocol


extension EditOpportunityCRM_VC: SalesNew_SelectionProtocol
{
    func didSelect(aryData: NSMutableArray, tag: Int) {
        
     }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        print(dictData)
        if(dictData.count != 0){
            self.arySelectedRange = NSMutableArray()
            self.arySelectedRange.add(dictData)
            let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
            let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
            let strrenge = StartInterval + " - " + EndInterval
            tf_Range.text = strrenge
            tf_Time.text = StartInterval
            
            tf_Range.tag = Int("\(dictData.value(forKey: "RangeofTimeId")!)")!
            
        }else{
            tf_Range.text = ""
            tf_Range.tag = 0
        }
    }
}
