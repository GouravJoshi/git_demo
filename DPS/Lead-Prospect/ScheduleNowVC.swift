//
//  ScheduleNowVC.swift
//  DPS
//  peSTream
//  Created by Saavan Patidar on 08/04/20.
//  Copyright © 2020 Saavan. All rights reserved.

import UIKit

class ScheduleNowVC: UIViewController {
    
    // MARK: - ----------------------------------- Outlets Variables -----------------------------------
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var lblPrimaryService: UILabel!
    @IBOutlet weak var txtFldAddress: ACFloatingTextField!
    @IBOutlet weak var btnGoForAddress: UIButton!
    @IBOutlet weak var btnAssignedTo: UIButton!
    
    
    @IBOutlet weak var txtFldAssignTo: ACFloatingTextField!
    @IBOutlet weak var txtFldDate: ACFloatingTextField!
    @IBOutlet weak var txtFldTime: ACFloatingTextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - ----------- Google Address Code ----------------
    
    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var radius: Double = 0.0
    private var strictBounds: Bool = false
    var txtAddressMaxY = CGFloat()
    var imgPoweredBy = UIImageView()
    var strAccountId = ""
    var arrAddress = NSArray()
    var strStateId = ""
    var strCityName = ""
    var strAddressLine1 = ""
    var strZipCode = ""
    var strCustomerAddressId = ""

    private var places = [Place]() {
        didSet { tableView.reloadData() }
    }
    
    
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------

    var arrEmployeeList = NSMutableArray()
    var dictLoginData = NSDictionary()
    var strAccounName = ""
    var strPrimaryServiceId = ""
    var strAssignedToId = ""
    var scheduleNowDict = NSDictionary()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.txtFldAddress.isUserInteractionEnabled = true

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"

        if strAccounName.count > 0 {
            
            lblAccNo.text = "Account: \(strAccounName)"

        } else {
            
            lblAccNo.text = ""

        }
        
        let strServiceName = fetchServiceNameViaId(strId: strPrimaryServiceId)
        
        if strServiceName.count > 0 {
            
            lblPrimaryService.text = "Primary service: \(strServiceName)"

        } else {
            
            lblPrimaryService.text = ""

        }
        
        loadGoogleAddress()
        
        getEmployeeList()
        
        strAssignedToId = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
        txtFldAssignTo.text = Global().getEmployeeName(viaEmployeeID: "\(dictLoginData.value(forKeyPath: "EmployeeId")!)")
        
        if scheduleNowDict.count > 0 {
                        
            strCustomerAddressId = "\(scheduleNowDict.value(forKey: "CustomerAddressId") ?? "")"
            
            if strCustomerAddressId.count > 0 {
                
                self.txtFldAddress.isUserInteractionEnabled = false
                
                FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    self.callApiToGetAddressViaAccountId(strType: "Selected")

                })
                
            }
            
            txtFldAddress.text = Global().strCombinedAddress(scheduleNowDict as? [AnyHashable : Any])
            
            if txtFldAddress.text == "N/A" {
                
                txtFldAddress.text = ""
                
            }
            
            strAssignedToId = "\(scheduleNowDict.value(forKey: "FieldSalesPerson") ?? "")"
            txtFldAssignTo.text = Global().getEmployeeName(viaEmployeeID: "\(scheduleNowDict.value(forKey: "FieldSalesPerson") ?? "")")
            
            txtFldDate.text = Global().latestStringDate(toStringFormatted: "\(scheduleNowDict.value(forKey: "ScheduleDate") ?? "")", "MM/dd/yyyy", "UTC")
            
            txtFldTime.text = Global().changeDate(toLocalActivityTime: "\(scheduleNowDict.value(forKey: "ScheduleTime") ?? "")")

        }
        
        if txtFldDate.text?.count == 0 {
            
            // set current date
            
            txtFldDate.text = Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
            
        }
        
        if txtFldTime.text?.count == 0 {
            
            // set current time
            
            txtFldTime.text = Global().strGetCurrentDate(inFormat: "hh:mm a")

        }
        
        if strAssignedToId.count == 0 {
            
            strAssignedToId = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            txtFldAssignTo.text = Global().getEmployeeName(viaEmployeeID: "\(dictLoginData.value(forKeyPath: "EmployeeId")!)")
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - ----------------------------------- Actions -----------------------------------
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionOnSave(_ sender: Any) {
        
        if(validationOnScheduleView()){
         
            var countryId = "1"
            
            /*if strCustomerAddressId.count > 0 {
                
                strAddressLine1 = ""
                strCityName = ""
                strZipCode = ""
                strStateId = ""
                strAddressLine1 = ""
                countryId = ""
                
            }*/
            
            var keys = NSArray()
            var values = NSArray()
            keys = ["Address1",
                    "CityName",
                    "Zipcode",
                    "StateId",
                    "CountryId",
                    "FieldSalesPerson",
                    "ScheduleDate",
                    "ScheduleTime",
                    "CustomerAddressId"]
            
            values = [strAddressLine1,
                      strCityName,
                      strZipCode,
                      strStateId,
                      countryId,
                      strAssignedToId,
                      txtFldDate.text!,
                      getTimeWithouAMPM(strTime: (txtFldTime.text!)),
                      strCustomerAddressId]
            
            let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ScheduleNow_Notification"), object: nil, userInfo: dictToSend as? [AnyHashable : Any])
            
            self.dismiss(animated: false, completion: nil)
            
        }
        
    }
    
    @IBAction func actionOnCancelAddress(_ sender: Any) {
        self.view.endEditing(true)
        txtFldAddress.text = ""
        strCustomerAddressId = ""
        self.txtFldAddress.isUserInteractionEnabled = true
        
    }
    @IBAction func actionOnGoForAddress(_ sender: Any) {
        
        if strAccountId.count > 0 {
            
            if self.arrAddress.count > 0 {
                
                //sender.tag = 63
                self.openTableViewPopUp(tag: 63, ary: (self.arrAddress as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
                
            }else{
             
                if (isInternetAvailable()){
                    
                    FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        self.callApiToGetAddressViaAccountId(strType: "sadsadf")

                    })

                }else{
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                    
                }
                
            }
            
        }else{
         
            showAlertWithoutAnyAction(strtitle: Info, strMessage: "Please select account to get address", viewcontrol: self)
            
        }
        
    }
    
    @IBAction func actionOnAssignTo(_ sender: Any)
    {
    
           let isFieldPersonAssign = nsud.bool(forKey: "isFieldPersonAssign")

           if isFieldPersonAssign {
               
               if arrEmployeeList.count > 0
               {
                   self.view.endEditing(true)

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {

                        self.view.endEditing(true)

                    })
                   
                   btnAssignedTo.tag = 500
                   gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrEmployeeList ) , strTitle: "")
               }
               else
               {
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
               }
               
           } else {
               
               showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Not authorized to change", viewcontrol: self)
               
           }
       }
    
    @IBAction func actionOnDate(_ sender: Any)
    {
        self.view.endEditing(true)

         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {

             self.view.endEditing(true)

         })
        gotoDatePickerView(sender: sender as! UIButton, strType: "Date", tag: 10)
    }
    
    @IBAction func actionOnTime(_ sender: Any)
    {
        self.view.endEditing(true)

         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {

             self.view.endEditing(true)

         })
        gotoDatePickerView(sender: sender as! UIButton, strType: "Time", tag: 11)
    }
    
    // MARK: -----------------------------------Validation----------------------------------------

    func validationOnScheduleView() -> Bool {

        if(txtFldAddress.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address.", viewcontrol: self)
            return false
            
        }
        if(txtFldAssignTo.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select assign to.", viewcontrol: self)
            return false
            
        }
        else if(txtFldDate.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select schedule date.", viewcontrol: self)
            return false
            
        }
        else if(txtFldTime.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select schedule time.", viewcontrol: self)
            return false
            
        }
        
        if (txtFldAddress.text!.count > 0) {

            let dictOfEnteredAddress = formatEnteredAddress(value: txtFldAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                
                return false
                
            } else {
                
                
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""

                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strCityName.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strStateId.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count != 5 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                
            }
            
            
        }
        /*if ((txtFldAddress.text!.count > 0) && (strCustomerAddressId.count <= 0)) {
            
                // Fetch Address
                
            let arrOfAddress = self.getAddress(from: txtFldAddress.text!)
            
            let tempArrayOfKeys = NSMutableArray()
            let tempDictData = NSMutableDictionary()

            for k in 0 ..< arrOfAddress.count {
             
                      if arrOfAddress[k] is NSDictionary {
                
                         let tempDict = arrOfAddress[k] as! NSDictionary
                    
                         tempArrayOfKeys.addObjects(from: tempDict.allKeys)
                         tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])

                      }
                
            }
            
            if tempArrayOfKeys.contains("City") {
                
                strCityName = "\(tempDictData.value(forKey: "City")!)"

            }
            if tempArrayOfKeys.contains("ZIP") {
                
                strZipCode = "\(tempDictData.value(forKey: "ZIP")!)"

            }
            if tempArrayOfKeys.contains("State") {
                
                let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
                    
                }else{
                    
                    strStateId = ""
                    //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)

                }

            }
            if tempArrayOfKeys.contains("Street") {
                
                strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"

            }
            
            if strAddressLine1.count == 0 || strCityName.count == 0 || strZipCode.count == 0 || strStateId.count == 0 {
                
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter proper address (address, city, zipcode, state).", viewcontrol: self)
                
                return false
                
            }
            
        }*/
        
        return true
        
    }
    
    // MARK: -----------------------------------API's Calling----------------------------------------
    
    func callApiToGetAddressViaAccountId(strType : String) {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetServiceAddressesByAccountId + strAccountId
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            FTIndicator.dismissProgress()
            if(Status)
            {
                
                let arrKeys = Response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    self.arrAddress = Response.value(forKey: "data") as! NSArray
                    
                    if self.arrAddress.count > 0 {
                        
                        if strType == "Selected" {
                            
                            for k in 0 ..< self.arrAddress.count {
                                
                                let dictData = self.arrAddress[k] as! NSDictionary
                                
                                if "\(dictData.value(forKey: "CustomerAddressId") ?? "")" == self.strCustomerAddressId {
                                    
                                    self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                                    self.strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                                    self.txtFldAddress.isUserInteractionEnabled = false
                                    
                                    if self.txtFldAddress.text == "N/A" {
                                        
                                        self.txtFldAddress.text = ""
                                        
                                    }
                                    
                                    break
                                    
                                }
                                
                            }
                            
                        }else{
                         
                            //sender.tag = 63
                            self.openTableViewPopUp(tag: 63, ary: (self.arrAddress as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
                            
                        }
                        
                    }else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }

                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
            
        }
        
    }
    // MARK: - ----------------------------------- Local Functions -----------------------------------

    func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    func fetchServiceNameViaId(strId : String) -> String {
        
        var serviceName = ""
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        for k in 0 ..< arrOfData.count {
            
            if arrOfData[k] is NSDictionary {
                
                let dictOfData = arrOfData[k] as! NSDictionary
                
                if "\(dictOfData.value(forKey: "ServiceId")!)" == strId {
                    
                    serviceName = "\(dictOfData.value(forKey: "Name")!)"
                    break
                }
                
            }
            
        }
        
        return serviceName
        
    }
    
    func getAddress(from dataString: String) ->  NSMutableArray {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        let resultsArray = NSMutableArray()
        // put matches into array of Strings
        for match in matches {
            if match.resultType == .address,
                let components = match.addressComponents {
                resultsArray.add(components)
            } else {
                print("no components found")
            }
        }
        return resultsArray
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String, tag: Int)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func getEmployeeList()
    {
        arrEmployeeList = NSMutableArray()
       if nsud.value(forKey: "EmployeeList") is NSArray
       {
        var arrEmployee = NSArray()
        arrEmployee = nsud.value(forKey: "EmployeeList") as! NSArray
        
        for item in arrEmployee
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1"
            {
                arrEmployeeList.add(dict)
            }
            
        }
        
        }

    }
    func loadGoogleAddress()
    {
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        txtAddressMaxY = (txtFldAddress.frame.origin.y) - 35
        imgPoweredBy.image = UIImage(named: "powered-by-google-on-white")
        imgPoweredBy.contentMode = .center
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)

            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
}


// MARK: - ----------------------------------- Extensions -----------------------------------

// MARK: --------------------- DatePicker Delegate --------------

extension ScheduleNowVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if tag == 10
        {
            
            txtFldDate.text = strDate
        }
        else if tag == 11
        {
            
            txtFldTime.text = strDate
        }
    }
    
    
}
// MARK: --------------------- Tableview Delegate --------------

extension ScheduleNowVC : CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int)
    {
        if tag == 500
        {
            //btnSelectEmployee.setTitle("\(dictData.value(forKey: "FullName") ?? "")", for: .normal)
            txtFldAssignTo.text = "\(dictData.value(forKey: "FullName") ?? "")"
            strAssignedToId = "\(dictData.value(forKey: "EmployeeId") ?? "")"
            
        }
        else if(tag == 63)
        {
            if(dictData.count == 0){
               txtFldAddress.text = ""
                strCustomerAddressId = ""
                self.txtFldAddress.isUserInteractionEnabled = true

            }else{
                
                txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                self.txtFldAddress.isUserInteractionEnabled = false
                
                if txtFldAddress.text == "N/A" {
                    
                    txtFldAddress.text = ""
                    
                }
                
            }
            // Address
        }
       
        
    }
    
}
// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------

extension ScheduleNowVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if(textField == txtFldAddress){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()

                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = txtFldAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()

                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtFldAddress){
            let txtAfterUpdate:NSString = txtFldAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            
            self.getPlaces(with: parameters) {
                self.places = $0
            }
            return true
        }
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
extension ScheduleNowVC {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }
                else
                {
                   /* for item in self.scrollView.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)*/
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    
                   // self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.tableView.frame = CGRect(x: 0, y: self.txtFldAddress.frame.maxY, width: self.txtFldAssignTo.frame.width + 20, height: DeviceType.IS_IPHONE_6 ? 200 : 250)

                    
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.view.addSubview(self.tableView)
                    self.view.addSubview(self.imgPoweredBy)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension ScheduleNowVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            //self.txtFldAddress.text = value.formattedAddress
            self.txtFldAddress.text = addressFormattedByGoogle(value: value)
            self.strCustomerAddressId = ""

            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()

        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
// MARK: -
// MARK: -ReminderCell
/*class addressCell: UITableViewCell {
    @IBOutlet weak var lbl_Address: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}*/
