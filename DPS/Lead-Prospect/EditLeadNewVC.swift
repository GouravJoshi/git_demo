//
//  EditLeadNewVC.swift
//  DPS
//
//  Created by Rakesh Jain on 17/04/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Changes 2020

import UIKit

fileprivate enum LeadType : String{
    
    case commercial, residential
}

fileprivate enum AssigneType : String{
    
    case individual, team
}

class EditLeadNewVC: UIViewController {
    
    // Outlets
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var btnCommercial: UIButton!
    @IBOutlet weak var btnResidential: UIButton!
    @IBOutlet weak var txtfldLeadName: ACFloatingTextField!
    @IBOutlet weak var txtfldContact: ACFloatingTextField!
    @IBOutlet weak var txtfldCompany: ACFloatingTextField!
    @IBOutlet weak var txtfldAddress: ACFloatingTextField!
    @IBOutlet weak var txtfldIndustry: ACFloatingTextField!
    @IBOutlet weak var txtfldBranch: ACFloatingTextField!
    @IBOutlet weak var txtfldSource: ACFloatingTextField!
    @IBOutlet weak var txtfldService: ACFloatingTextField!
    @IBOutlet weak var btnIndividual: UIButton!
    @IBOutlet weak var btnTeam: UIButton!
    @IBOutlet weak var txtfldAssigneeName: ACFloatingTextField!
    @IBOutlet weak var txtviewComment: UITextView!
    
    @IBOutlet weak var txtfldPropertyType: ACFloatingTextField!
    @IBOutlet weak var txtfldUrgencyType: ACFloatingTextField!

    // Variable
    fileprivate var leadType = LeadType.commercial
    fileprivate var assignType = AssigneType.individual
    fileprivate var arySourceId = NSMutableArray()
    fileprivate var arySelectedSource = NSMutableArray()
    fileprivate var aryOfService = NSMutableArray()
    fileprivate var aryIndustryMaster = NSMutableArray()
    fileprivate var aryBranchMaster = NSMutableArray()
    fileprivate var arySourceMaster = NSMutableArray()
    fileprivate var aryServiceMaster = NSMutableArray()
    fileprivate var aryTeamMaster = NSMutableArray()
    fileprivate var aryEmployeeList = NSMutableArray()
    fileprivate var dictSelectedService = NSDictionary()
    fileprivate var dictSelectedTeam = NSDictionary()
    fileprivate var dictSelectedEmployee = NSDictionary()
    fileprivate var dictLoginData = NSDictionary()
    fileprivate var strUserName = ""
    fileprivate var strBtnTag = ""
    fileprivate var strCRMContactId = ""
    fileprivate var strCRMCompanyId = ""
    fileprivate var strAssignType = ""
    fileprivate var strAssignToId = ""
    fileprivate var strEmpID = ""
    fileprivate var strIndustryId = ""
    fileprivate var strBranchSysName = ""
    fileprivate var strServiceUrlMain = ""
    var arrOfAdditionalServiceSelected = NSMutableArray()
    var strPrimaryService = ""
    var loader = UIAlertController()
    var dictOfContactAddress = NSDictionary()
    var dictOfCompanyAddress = NSDictionary()
    
    var dictLeadDetails = NSDictionary()
    var aryPropertySelected = NSMutableArray()
    var aryUrgencySelected = NSMutableArray()
    //Deepak.s
    var arrUDFDataFromDetail = NSArray()
    @IBOutlet weak var viewForUserDefineField: UIView!
    @IBOutlet weak var heightUserDefine: NSLayoutConstraint!
//    @IBOutlet weak var height_superView: NSLayoutConstraint!
    //*****
    
    // MARK: - ----------- Google Address Code ----------------
       
       @IBOutlet weak var tableView: UITableView!
       private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
       private var placeType: PlaceType = .all
       private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
       private var radius: Double = 0.0
       private var strictBounds: Bool = false
       var txtAddressMaxY = CGFloat()
       var imgPoweredBy = UIImageView()
       var strStateId = ""
       var strCityName = ""
       var strAddressLine1 = ""
       var strZipCode = ""
       
       private var places = [Place]() {
           didSet { tableView.reloadData() }
       }
    
    // MARK:- view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUp()
    }
    
    // MARK:- UIButton action
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnUpdate(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let strMessage = validation()
        if(strMessage.count > 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMessage, viewcontrol: self)
        }
        else{
            if(isInternetAvailable() == false)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            else
            {
                
                self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(self.loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.callAPIToEditLead()
                    
                }
            }
        }
    }
    
    
    @IBAction func actionOnCommercial(_ sender: UIButton) {
        self.view.endEditing(true)
        leadType = LeadType.commercial
        btnCommercial.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnResidential.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
    }
    
    @IBAction func actionOnResidential(_ sender: UIButton) {
        self.view.endEditing(true)
        leadType = LeadType.residential
        btnCommercial.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnResidential.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
    }
    
    @IBAction func actionOnContact(_ sender: UIButton) {
        self.view.endEditing(true)
        strBtnTag = "1"
        gotoAssociateContact()
    }
    
    @IBAction func actionOnCompany(_ sender: UIButton) {
        self.view.endEditing(true)
        strBtnTag = "2"
        gotoAssociateCompany()
    }
    
    @IBAction func actionOnClearAddress(_ sender: UIButton) {
        self.view.endEditing(true)
        
        txtfldAddress.text = ""
    }
    
    @IBAction func actionOnAddress(_ sender: UIButton) {
      
        
        
    }
    
    @IBAction func action_SelectPropertyType(_ sender: Any) {
        if(self.getAddressTypeFromMasterSalesAutomation().count != 0){
           // openTableViewPopUp(tag: 65, ary: self.getAddressTypeFromMasterSalesAutomation() , aryselectedItem: self.aryPropertySelected)
            openTableViewPopUp(tag: 65, ary: self.getAddressTypeFromMasterSalesAutomation() , aryselectedItem: NSMutableArray())


        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Property type not found!!!", viewcontrol: self)
            
        }
        
    }
    @IBAction func action_SelectUrgencyType(_ sender: Any) {
        if(self.getUrgencyFromMaster().count != 0){
            openTableViewPopUp(tag: 62, ary: self.getUrgencyFromMaster() , aryselectedItem: self.aryUrgencySelected)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Urgency not found!!!", viewcontrol: self)
            
        }
        
    }
    @IBAction func action_AddAddress(_ sender: UIButton) {
           let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
           let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
           vc.tag = 1
           vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
           vc.modalTransitionStyle = .coverVertical
           vc.delegate = self
           self.present(vc, animated: false, completion: {})
       }
    
    @IBAction func actionOnSelectIndustry(_ sender: UIButton) {
        self.view.endEditing(true)
        if(aryIndustryMaster.count > 0){
            openTableViewPopUp(tag: 1100, ary: aryIndustryMaster, aryselectedItem: NSMutableArray())
        }
    }
    
    @IBAction func actionOnSelectBranch(_ sender: UIButton) {
        self.view.endEditing(true)
        if(aryBranchMaster.count > 0){
            openTableViewPopUp(tag: 1101, ary: aryBranchMaster, aryselectedItem: NSMutableArray())
        }
    }
    
    @IBAction func actionOnSelectSource(_ sender: UIButton) {
        self.view.endEditing(true)
        if(arySourceMaster.count > 0){
            openTableViewPopUp(tag: 1102, ary: arySourceMaster, aryselectedItem: arySelectedSource)
        }
    }
    
    @IBAction func actionOnSelectService(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if strBranchSysName.count > 0 {
            
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            var arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
            //arrOfData = returnFilteredArray(array: arrOfData)
            arrOfData = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchSysName )
            if(arrOfData.count != 0){
                
                let controller = storyboardLeadProspect.instantiateViewController(withIdentifier: "SelectServiceVC") as! SelectServiceVC
                controller.strFromWhere = "EditLeadCRM"
                controller.strServiceIdPrimary = strPrimaryService
                controller.strBranchSysName = strBranchSysName
                controller.arrayServicesSelected = arrOfAdditionalServiceSelected
                self.navigationController?.present(controller, animated: false, completion: nil)
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select branch", viewcontrol: self)
            
        }
        
    }
    
    @IBAction func actionOnIndividual(_ sender: UIButton) {
        
        assignType = AssigneType.individual
        btnIndividual.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnTeam.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        txtfldAssigneeName.placeholder = "Assign To"
        txtfldAssigneeName.text = strUserName
        strAssignType = "Individual"
        strAssignToId = strEmpID
        txtfldAssigneeName.resignFirstResponder()
        
        if(dictSelectedEmployee.count > 0){
            
            txtfldAssigneeName.text = "\(dictSelectedEmployee.value(forKey: "FullName")!)"
            strAssignToId = "\(dictSelectedEmployee.value(forKey: "EmployeeId")!)"
            txtfldAssigneeName.resignFirstResponder()
            
        }
        
    }
    
    @IBAction func actionOnTeam(_ sender: UIButton) {
        
        btnIndividual.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnTeam.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        assignType = AssigneType.team
        txtfldAssigneeName.placeholder = "-Select Team-"
        if(dictSelectedTeam.count > 0){
       
            txtfldAssigneeName.text = "\(dictSelectedTeam.value(forKey: "Title")!)"
            txtfldAssigneeName.resignFirstResponder()
            
        }
        else{
            txtfldAssigneeName.text = nil
            txtfldAssigneeName.resignFirstResponder()
        }
    }
    
    @IBAction func actionOnAssigneName(_ sender: UIButton) {
        
        switch assignType {
        case .individual:
            
            if(aryEmployeeList.count > 0){
                
                let isFieldPersonAssign = nsud.bool(forKey: "isFieldPersonAssign")

                if isFieldPersonAssign {
                    
                    openTableViewPopUp(tag: 1105, ary: aryEmployeeList, aryselectedItem: NSMutableArray())
                    
                } else {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Not authorized to change", viewcontrol: self)
                    
                }
                
            }
            
            break
            
        default:
            if(aryTeamMaster.count > 0){
                
                openTableViewPopUp(tag: 1104, ary: aryTeamMaster, aryselectedItem: NSMutableArray())
                
            }
        }
    }
    
    @IBAction func actionOnCancelContact(_ sender: Any)
    {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtfldContact.text!.count > 0 {

            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtfldContact.text = ""
                self.strCRMContactId = ""
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }

    }
    
    @IBAction func actionOnCancelCompany(_ sender: Any)
    {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if self.txtfldCompany.text!.count > 0 {

            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                self.txtfldCompany.text = ""
                self.strCRMCompanyId = ""
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }

    }
    
    // MARK:- Notification
    //Post Notification callBack
    @objc func handleNotificationPost(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        
        
        if "\(nsud.value(forKey: "associateType") ?? "")" == "contact"
        {
            self.strBtnTag = "1"
        }
        if "\(nsud.value(forKey: "associateType") ?? "")" == "company"
        {
            self.strBtnTag = "2"
        }
        //nsud.setValue("", forKey: "associateType")
        //nsud.synchronize()
        
        if let dict = notification.userInfo as NSDictionary? {
            
            if self.strBtnTag == "1"{
                // Contact
                
                strCRMContactId = "\(dict.value(forKey: "CrmContactId")!)"
                txtfldContact.text = Global().strFullName(dict as? [AnyHashable : Any])
                
                if strCRMContactId.count > 0 {
                    
                    if (isInternetAvailable()){

                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                            self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                            self.present(self.loader, animated: false, completion: nil)
                            
                            self.callApiToGetAddressViaCrmContactId(strCrmContactIdLocal: self.strCRMContactId, strType: "")
                            
                        }
                        
                    }
                    
                }
                
              /*  strEntityId = "\(dict.value(forKey: "CrmContactId")!)"
                strEntityType = enumRefTypeCrmContact
                strEntityName = Global().strFullName(dict as? [AnyHashable : Any])*/
                
            }else if self.strBtnTag == "2"{
                // Company
               
                strCRMCompanyId = "\(dict.value(forKey: "CrmCompanyId")!)"
                txtfldCompany.text = "\(dict.value(forKey: "Name")!)"
                
               if strCRMCompanyId.count > 0 {

                if (isInternetAvailable()){

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {

                        self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                        self.present(self.loader, animated: false, completion: nil)
                        
                        self.callApiToGetAddressViaCrmCompanyId(strCrmCoCompanyIdLocal: self.strCRMCompanyId, strType: "")
                        
                    }
                    
                }
                
                }
                /* strEntityId = "\(dict.value(forKey: "CrmCompanyId")!)"
                strEntityType = enumRefTypeCrmCompany
                strEntityName = "\(dict.value(forKey: "Name")!)"*/
                
            }
            
            //self.callGetAssociatedEntitiesForLeadAssociations(EntityId: strEntityId, EntityType: strEntityType, editStatus: true, followupStatus: false, userInfo: dict)
            
                
        }
        
    }
    
    func catchNotification(notification:Notification) {

        if let dict = notification.userInfo as NSDictionary? {

            aryOfService.removeAllObjects()
            strPrimaryService = "\(dict.value(forKey: "PrimaryService")!)"
            
            let arrOfServiceName = NSMutableArray()
            
            let strServiceName = fetchServiceNameViaId(strId: strPrimaryService)
            
            if strServiceName.count > 0 {
                
                arrOfServiceName.add(strServiceName)
                let dictTemp = ["ServiceId":"\(strPrimaryService)","IsPrimary":true] as [String : Any]
                aryOfService.add(dictTemp)
            }
            
            let arrOfAdditionalServicesSelected = (dict.value(forKey: "AdditionalService")!) as! NSMutableArray
            
            if arrOfAdditionalServicesSelected.count > 0 {
                                
                arrOfAdditionalServicesSelected.addingObjects(from: arrOfAdditionalServicesSelected as! [Any])
                
            }
            
            for k in 0 ..< arrOfAdditionalServicesSelected.count {

                let strServiceNameLocal = fetchServiceNameViaId(strId: arrOfAdditionalServicesSelected[k] as! String)
                if strServiceNameLocal.count > 0 {
                    
                    arrOfServiceName.add(strServiceNameLocal)
                   
                    let dictTemp = ["ServiceId":"\(arrOfAdditionalServicesSelected[k])","IsPrimary":false] as [String : Any]
                    
                    aryOfService.add(dictTemp)
                    
                }
            }
            
            txtfldService.text = arrOfServiceName.componentsJoined(by: ", ")
        
        }
    }
    // MARK:- Functions
    fileprivate func initialSetUp(){
        
        // get all required masters
        getIndustryFromMaster()
        getBranchFromMaster()
        getSourceFromMaster()
        getServiceFromMaster()
        getTeamFromMaster()
        getEmployeeList()
        
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
    
        /*  strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username")!)"
        txtfldAssigneeName.text = strUserName
        txtfldAssigneeName.resignFirstResponder()*/
        
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        
        // configure textview comment
        txtviewComment.layer.cornerRadius = 5.0
        txtviewComment.layer.borderColor = UIColor.theme()?.cgColor
        txtviewComment.layer.borderWidth = 0.8
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "SelectedServicesEditLeadCRM_Notification"),
                                               object: nil,
                                               queue: nil,
                                               using:catchNotification)
     
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotificationPost(_:)), name: NSNotification.Name(rawValue: "AssociatedEditLeadCRM_Notification"), object: nil)
        
        loadGoogleAddress()
        
        showData()
        
        //Deepak.s
        print("UserDefine: ", "\(dictLeadDetails.value(forKey: "UserDefinedFields") ?? "")")
        let jsonStringUDF = "\(dictLeadDetails.value(forKey: "UserDefinedFields") ?? "")"

        let data = jsonStringUDF.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
               print(jsonArray) // use the json here
                self.arrUDFDataFromDetail = jsonArray as NSArray
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
        print(self.arrUDFDataFromDetail)
        self.creatUDFView()
        //***********
       
    }
    
    fileprivate func showData(){
    print(dictLeadDetails)
        if(dictLeadDetails.count > 0){
            
            if("\(dictLeadDetails.value(forKey: "FlowType")!)" == "Commercial" || "\(dictLeadDetails.value(forKey: "FlowType")!)" == "commercial"){
                
                leadType = LeadType.commercial
                btnCommercial.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                btnResidential.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                
            }
            else{
                leadType = LeadType.residential
                btnCommercial.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                btnResidential.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            }
            
            if let leadName = (dictLeadDetails.value(forKey: "LeadName")){
                
                txtfldLeadName.text = "\(leadName)"
            }
            
        
            if let crmContactId = (dictLeadDetails.value(forKey: "CrmContactId")){
                
                if ("\(crmContactId)".count > 0){
                    
                   strCRMContactId = "\(crmContactId)"
                   txtfldContact.text = Global().strFullName(dictLeadDetails as? [AnyHashable : Any])
                    
                }
            }
            
            if let crmCompany = (dictLeadDetails.value(forKey: "CrmCompany")){
                
                if(crmCompany is NSDictionary){
                    
                    if((crmCompany as! NSDictionary).count > 0){
                        
                        strCRMCompanyId = "\((crmCompany as! NSDictionary).value(forKey: "CrmCompanyId")!)"
                        
                        txtfldCompany.text = "\((crmCompany as! NSDictionary).value(forKey: "Name")!)"
                    }
                }
            }
            
            if let address1 = (dictLeadDetails.value(forKey: "Address1")){
                if("\(address1)".count > 0){
                    txtfldAddress.text = "\(address1)"
                }
                
            }
            
            txtfldAddress.text = Global().strCombinedAddress(dictLeadDetails as? [AnyHashable : Any])
            
            if let industryId = (dictLeadDetails.value(forKey: "IndustryId")){
                
                if("\(industryId)".count > 0)
                {
                    for item in aryIndustryMaster{
                        
                        if("\((item as! NSDictionary).value(forKey: "IndustryId")!)" ==  "\(industryId)"){
                                                       
                            txtfldIndustry.text = "\((item as! NSDictionary).value(forKey: "Name")!)"
                            strIndustryId = "\(industryId)"
                            
                        }
                    }
                }
            }
            
            if let branchSysName = (dictLeadDetails.value(forKey: "BranchesSysNames")){
                
                if(branchSysName is NSArray){
                    
                    if((branchSysName as! NSArray).count > 0){
                        
                        for item in aryBranchMaster{
                            
                            if((branchSysName as! NSArray).firstObject as! String == "\((item as! NSDictionary).value(forKey: "SysName")!)")
                            {
                               strBranchSysName = "\((item as! NSDictionary).value(forKey: "SysName")!)"
                               txtfldBranch.text = "\((item as! NSDictionary).value(forKey: "Name")!)"
                                
                            }
                        }
                    }
                }
            }
            
          
                
            if let sourceIds = dictLeadDetails.value(forKey: "WebLeadSourceId"){
                
                if(sourceIds is NSArray){
                    
                    if((sourceIds as! NSArray).count > 0){
                        
                        var strName = ""
                        arySourceId = NSMutableArray()
                        arySelectedSource = NSMutableArray()
                        
                        for item in (sourceIds as! NSArray){
                            
                            let dict = fetchSourceDictViaId(strId: "\(item)")
                            
                            if(dict.count > 0){
                                
                                strName = "\(strName),\(dict.value(forKey: "Name")!)"
                                arySourceId.add("\(dict.value(forKey: "SourceId")!)")
                                arySelectedSource.add(dict)
                            }
                        }
                        if strName == ""
                        {
                            strName = ""
                        }
                        if strName.count != 0{
                            strName = String(strName.dropFirst())
                        }
                        txtfldSource.text = strName
                    }
                }
            }
            
            
            if let service = dictLeadDetails.value(forKey: "WebLeadServices"){
                
                if(service is NSArray){
                    
                    if((service as! NSArray).count > 0){
                        
                        aryOfService = (service as! NSArray).mutableCopy() as! NSMutableArray
                        let arrOfServiceName = NSMutableArray()
                        
                        for k in 0 ..< aryOfService.count {

                            let dict = aryOfService[k] as! NSDictionary
                           
                            let strServiceNameLocal = fetchServiceNameViaId(strId: "\(dict.value(forKey: "ServiceId")!)")
                        
                            if strServiceNameLocal.count > 0 {
                                
                                arrOfServiceName.add(strServiceNameLocal)
                                
                            }
                            if "\(dict.value(forKey: "IsPrimary")!)" == "1" || "\(dict.value(forKey: "IsPrimary")!)" == "true" || "\(dict.value(forKey: "IsPrimary")!)" == "True"{
                                
                                strPrimaryService = "\(dict.value(forKey: "ServiceId")!)"
                                
                            }else{
                                
                                arrOfAdditionalServiceSelected.add("\(dict.value(forKey: "ServiceId")!)")
                                
                            }
                            
                        }
                        
                        txtfldService.text = arrOfServiceName.componentsJoined(by: ", ")
                    }
                }
            }
            
            if let assigneeType = dictLeadDetails.value(forKey: "AssignedType"){
                
                if("\(assigneeType)" == "Individual" || "\(assigneeType)" == "individual"){
                    
                    assignType = AssigneType.individual
                    btnIndividual.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                    btnTeam.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                   
                    strAssignType = "\(dictLeadDetails.value(forKey: "AssignedType")!)"
                    strAssignToId = "\(dictLeadDetails.value(forKey: "AssignedToId")!)"
                   
                    strUserName = "\(dictLeadDetails.value(forKey: "AssignedToName")!)"
                
                    txtfldAssigneeName.placeholder = "Assign To"
                    txtfldAssigneeName.text = strUserName
                    txtfldAssigneeName.resignFirstResponder()
                    
                    for item in aryEmployeeList{
                        
                        if("\((item as! NSDictionary).value(forKey: "EmployeeId")!)" == strAssignToId){
                            dictSelectedEmployee = item as! NSDictionary
                            break
                        }
                    }
                    
                }
                else{
                    
                     assignType = AssigneType.team
                     btnIndividual.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                     btnTeam.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                     strAssignType = "\(dictLeadDetails.value(forKey: "AssignedType")!)"
                     strAssignToId = "\(dictLeadDetails.value(forKey: "AssignedToId")!)"
                  
                    txtfldAssigneeName.placeholder = "-Select Team-"
                    txtfldAssigneeName.text = "\(dictLeadDetails.value(forKey: "AssignedToName")!)"
                    
                    for item in aryTeamMaster{
                        
                        if("\((item as! NSDictionary).value(forKey: "TeamId")!)" == strAssignToId){
                            dictSelectedTeam = item as! NSDictionary
                            break
                        }
                    }
                }
            }
            
            let propertyTypeID =  "\(dictLeadDetails.value(forKey: "AddressPropertyTypeId")!)"
            self.aryPropertySelected = NSMutableArray()
            self.aryPropertySelected = getPropertyTypeNameFromID_FromMasterSalesAutomation(id: propertyTypeID)
            if(self.aryPropertySelected .count != 0){
                let dictData = self.aryPropertySelected.object(at: 0)as! NSDictionary
                txtfldPropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
                txtfldPropertyType.tag = Int("\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")" == "" ? "0" : "\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")")!
            }
            
            let urgencyId =  "\(dictLeadDetails.value(forKey: "UrgencyId")!)"
            self.aryUrgencySelected = NSMutableArray()
            self.aryUrgencySelected = getUrgencyNameFromID_FromMaster(id: urgencyId)
            if(self.aryUrgencySelected .count != 0){
                let dictData = self.aryUrgencySelected.object(at: 0)as! NSDictionary
                txtfldUrgencyType.text = "\(dictData.value(forKey: "Name") ?? "")"
                txtfldUrgencyType.tag = Int("\(dictData.value(forKey: "UrgencyId") ?? "0")" == "" ? "0" : "\(dictData.value(forKey: "UrgencyId") ?? "0")")!
            }
            
        }
    }
    
    //Deepak.s
    func creatUDFView()  {
        self.heightUserDefine.constant = 0.0
        let strID = ""//"\(dataGeneralInfo.value(forKey: "leadId") ?? "")"
        let strType = Userdefinefiled_DynemicForm_Type.Lead
        
        var aryUDFSaveData_Service = self.arrUDFDataFromDetail.mutableCopy() as! NSMutableArray//UserDefineFiledVC().getUserdefineSaveData(type: strType, id: strID)
        let aryMasterData = UserDefineFiledVC().getUserdefineMasterData(type: strType)
        //----------When data is blank then use master data -------
        let arytempSave = NSMutableArray()
        //if aryUDFSaveData.count == 0 {
        for item in aryMasterData {
            let strType = "\((item as AnyObject).value(forKey: "type") ?? "")"
            let name = "\((item as AnyObject).value(forKey: "name") ?? "")"
            
            
            let temp = aryUDFSaveData_Service.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(name)" } as NSArray
            
            
            
            if (temp.count == 0 && (strType == Userdefinefiled_DynemicForm.radio_group  || strType == Userdefinefiled_DynemicForm.select || strType == Userdefinefiled_DynemicForm.checkbox_group) ) {
                if(((item as AnyObject).value(forKey: "values") is NSArray)){
                    let ary = ((item as AnyObject).value(forKey: "values") as! NSArray)
                    
                    let temp = ary.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "selected")!)".lowercased() == "true" ||  "\((task as! NSDictionary).value(forKey: "selected")!)".lowercased() == "1" } as NSArray
                    
                    if(temp.count != 0){
                        let aryValue = NSMutableArray()
                        for item in temp {
                            aryValue.add("\((item as AnyObject).value(forKey: "value") ?? "")")
                        }
                        let dict = NSMutableDictionary()
                        dict.setValue(aryValue, forKey: "userData")
                        dict.setValue(name, forKey: "name")
                        arytempSave.add(dict)
                    }
                }
            }
            if (temp.count == 0 && (strType == Userdefinefiled_DynemicForm.number ||  strType == Userdefinefiled_DynemicForm.text || strType == Userdefinefiled_DynemicForm.textarea || strType == Userdefinefiled_DynemicForm.date)){
                let dict = NSMutableDictionary()
                let aryValue = NSMutableArray()
                if((item as AnyObject).value(forKey: "value") != nil){
                    aryValue.add("\((item as AnyObject).value(forKey: "value") ?? "")")
                    dict.setValue(aryValue, forKey: "userData")
                    dict.setValue(name, forKey: "name")
                    arytempSave.add(dict)
                }
                
            }
        }
        aryUDFSaveData_Service += arytempSave.mutableCopy() as! NSMutableArray
        
  
  
    // remove blank value
    let aryTem = NSMutableArray()
    for item in aryUDFSaveData_Service {
        if(item is NSDictionary){
            let dictSaved = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
            if(dictSaved.value(forKey: "userData") is NSArray){
                let ary = (dictSaved.value(forKey: "userData") as! NSArray)
                let array = ary.filter({  "\($0)" != ""})
                dictSaved.setValue(array, forKey: "userData")
                aryTem.add(dictSaved)
            }
         
        }
        
    }
        aryUDFSaveData_Service = NSMutableArray()
        aryUDFSaveData_Service  = aryTem
        aryOtherUDF = NSMutableArray()
        /// }
    //----------End-------
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            
            UserDefineFiledVC().CreatFormUserdefine(strid: strID, aryJson_Master: aryMasterData, aryResponceObj:  aryUDFSaveData_Service, viewUserdefinefiled:  self.viewForUserDefineField, controllor: self) { aryUserdefine, aryUserdefineResponce, height in
                self.heightUserDefine.constant = CGFloat(height)
                if UIDevice.current.userInterfaceIdiom == .pad {
                     // iPad
                 } else {
                     //iPhone
//                     self.height_superView.constant = 1950
//                     self.height_superView.constant = self.height_superView.constant + self.heightUserDefine.constant
//                     self.constMoreInfoView_H.constant = self.constMoreInfoView_H.constant + self.heightUserDefine.constant
                 }
                
            }
        }
        
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    //***********
    
    func getAddressTypeFromMasterSalesAutomation() -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    
        
    func getUrgencyFromMaster() -> NSMutableArray {
        if(nsud.value(forKey: "LeadDetailMaster") != nil){
            let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if(dictMaster.value(forKey: "UrgencyMasters") != nil){
                let aryTemp = (dictMaster.value(forKey: "UrgencyMasters") as! NSArray).filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    func getPropertyTypeNameFromID_FromMasterSalesAutomation(id : String) -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")) && "\((task as! NSDictionary).value(forKey: "AddressPropertyTypeId")!)".contains("\(id)")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    func getUrgencyNameFromID_FromMaster(id : String) -> NSMutableArray {
        if(nsud.value(forKey: "LeadDetailMaster") != nil){
            let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if(dictMaster.value(forKey: "UrgencyMasters") != nil){
                let aryTemp = (dictMaster.value(forKey: "UrgencyMasters") as! NSArray).filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")) && "\((task as! NSDictionary).value(forKey: "UrgencyId")!)".contains("\(id)")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    func loadGoogleAddress()
       {
           apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
           txtAddressMaxY = (txtfldAddress.frame.origin.y) - 40
           imgPoweredBy.image = UIImage(named: "powered-by-google-on-white")
           imgPoweredBy.contentMode = .center
       }
    
    fileprivate func getIndustryFromMaster(){
        
        if let dict = nsud.value(forKey: "TotalLeadCountResponse") {
            
            if(dict is NSDictionary){
                
                if let aryIndustry = (dict as! NSDictionary).value(forKey: "IndustryMasters"){
                    
                    if(aryIndustry is NSArray){
                        
                        for item  in aryIndustry as! NSArray{
                            
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                
                                aryIndustryMaster.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func getBranchFromMaster(){
        
        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            
            if(dict is NSDictionary){
                
                if let aryIndustry = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    
                    if(aryIndustry is NSArray){
                        
                        for item  in aryIndustry as! NSArray{
                            
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                
                                aryBranchMaster.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func getSourceFromMaster(){
        
        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            
            if(dict is NSDictionary){
                
                if let aryIndustry = (dict as! NSDictionary).value(forKey: "SourceMasters"){
                    
                    if(aryIndustry is NSArray){
                        
                        for item  in aryIndustry as! NSArray{
                            
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                
                                arySourceMaster.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func getServiceFromMaster(){
        
        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            
            if(dict is NSDictionary){
                
                if let aryIndustry = (dict as! NSDictionary).value(forKey: "ServiceMasters"){
                    
                    if(aryIndustry is NSArray){
                        
                        for item  in aryIndustry as! NSArray{
                            
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                
                                aryServiceMaster.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func getTeamFromMaster(){
        
        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            
            if(dict is NSDictionary){
                
                if let aryIndustry = (dict as! NSDictionary).value(forKey: "TeamMasters"){
                    
                    if(aryIndustry is NSArray){
                        
                        for item  in aryIndustry as! NSArray{
                            
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                
                                aryTeamMaster.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
    }
    fileprivate func getEmployeeList(){
        
        if let aryTemp = nsud.value(forKeyPath: "EmployeeList"){
            if(aryTemp is NSArray){
                if((aryTemp as! NSArray).count > 0){
                   
                    let resultPredicate = NSPredicate(format: "IsActive == true")
                    let result = (aryTemp as! NSArray).filtered(using: resultPredicate)
                    aryEmployeeList = (result as NSArray).mutableCopy() as! NSMutableArray
                }
            }
        }
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "EditLeadNewVC", array: ary)

            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func fetchServiceNameViaId(strId : String) -> String {
        
        var serviceName = ""
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        for k in 0 ..< arrOfData.count {
            
            if arrOfData[k] is NSDictionary {
                
                let dictOfData = arrOfData[k] as! NSDictionary
                
                if "\(dictOfData.value(forKey: "ServiceId")!)" == strId {
                    
                    serviceName = "\(dictOfData.value(forKey: "Name")!)"
                    break
                }
                
            }
            
        }
        
        return serviceName
        
    }
    
    fileprivate func fetchSourceDictViaId(strId : String) -> NSDictionary{
        
        for item in arySourceMaster{
            
            if("\((item as! NSDictionary).value(forKey: "SourceId")!)" == strId){
             
                return (item as! NSDictionary)
                
            }
        }
        return NSDictionary()
    }
    
    fileprivate func gotoAssociateContact()
    {
        nsud.setValue("contact", forKey: "associateType")
        nsud.synchronize()
        
             let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
            
          controller.strFromWhere = "NotAssociate"
          controller.strFromWhereForAssociation = "EditLeadCRM"
          self.navigationController?.pushViewController(controller, animated: false)
          
      }
    
    fileprivate func gotoAssociateCompany(){
        
        nsud.setValue("company", forKey: "associateType")
        nsud.synchronize()
        
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        
        controller.strFromWhere = "NotAssociate"
        controller.strFromWhereForAssociation = "EditLeadCRM"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    func getAddress(from dataString: String) ->  NSMutableArray {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        let resultsArray = NSMutableArray()
        // put matches into array of Strings
        for match in matches {
            if match.resultType == .address,
                let components = match.addressComponents {
                resultsArray.add(components)
            } else {
                print("no components found")
            }
        }
        return resultsArray
    }
   
    

    fileprivate func validation()-> String {
        
        var strMsg = ""

        if((txtfldContact.text == "") && (txtfldCompany.text == ""))
        {
           
            strMsg = "Please select contact or company."
             return strMsg
            
        }
        
        let isSourceMendatory = nsud.bool(forKey: "IsSourceMandatory")
        if(txtfldLeadName.text?.count == 0){
            
           strMsg = "Please enter lead name"
            return strMsg
            
        }
        
        if (txtfldAddress.text!.count > 0) {

            let dictOfEnteredAddress = formatEnteredAddress(value: txtfldAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                strMsg = "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode)."
                return strMsg
                                    
            } else {
                
                
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""

                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                                            
                    strMsg = "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode)."
                    return strMsg
                }
                if strCityName.count == 0 {
                                            
                    strMsg = "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode)."
                    return strMsg
                }
                if strStateId.count == 0 {
                                            
                    strMsg = "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode)."
                    return strMsg
                }
                if strZipCode.count == 0 {
                                            
                    strMsg = "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                    return strMsg
                }
                if strZipCode.count != 5 {
                                            
                    strMsg = "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode)."
                    return strMsg
                }
                
            }
            
        }
        
        if(strBranchSysName.count == 0){
           strMsg = "Please select branch"
            return strMsg
        }
        if(isSourceMendatory == true){
            
            if(arySourceId.count == 0){
                strMsg = "Please select source"
                return strMsg
            }
        }
        if(assignType == .team){
            
            if(txtfldAssigneeName.text?.count == 0){
                
               strMsg = "Please select team"
                return strMsg
            }
        }
        
        return strMsg
    }
    
    func callApiToGetAddressViaCrmContactId(strCrmContactIdLocal : String , strType : String) {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmContactId + strCrmContactIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfContactAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.dictOfContactAddress = removeNullFromDict(dict: self.dictOfContactAddress.mutableCopy()as! NSMutableDictionary)

                        if self.dictOfContactAddress.count > 0 {
                            
                            let zipCodeLocal = "\(self.dictOfContactAddress.value(forKey: "ZipCode")!)"
                            
                            if (zipCodeLocal.count > 0) {
                                
                                if self.txtfldAddress.text?.count != 0 {
                                    
                                    let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                                      
                                    let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                        
                                        self.txtfldAddress.text = Global().strCombinedAddress(self.dictOfContactAddress as? [AnyHashable : Any])
                                        //self.strCustomerAddressId = ""
                                        
                                    })
                                    
                                    let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                        
                                        
                                        
                                    })
                                    
                                    alertCOntroller.addAction(alertAction)
                                    alertCOntroller.addAction(alertActionCancel)

                                    self.present(alertCOntroller, animated: true, completion: nil)
                                    
                                }else{
                                    
                                    self.txtfldAddress.text = Global().strCombinedAddress(self.dictOfContactAddress as? [AnyHashable : Any])
                                    //self.strCustomerAddressId = ""

                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callApiToGetAddressViaCrmCompanyId(strCrmCoCompanyIdLocal : String , strType : String) {
        

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetAddressesByCrmCompanyId + strCrmCoCompanyIdLocal
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.dictOfCompanyAddress = Response.value(forKey: "data") as! NSDictionary
                        
                        self.dictOfCompanyAddress = removeNullFromDict(dict: self.dictOfCompanyAddress.mutableCopy()as! NSMutableDictionary)

                        if self.dictOfCompanyAddress.count > 0 {
                            
                            let zipCodeLocal = "\(self.dictOfCompanyAddress.value(forKey: "ZipCode")!)"
                            let AddressPropertyName = "\(self.dictOfCompanyAddress.value(forKey: "AddressPropertyName")!)"
                            let AddressPropertyTypeId = "\(self.dictOfCompanyAddress.value(forKey: "AddressPropertyTypeId")!)"
                            self.txtfldPropertyType.text = AddressPropertyName
                            self.txtfldPropertyType.tag = (AddressPropertyTypeId == "" ? 0 : Int(AddressPropertyTypeId))!
                            if (zipCodeLocal.count > 0) {
                                
                                if self.txtfldAddress.text?.count != 0 {
                                    
                                    let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                                      
                                    let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                        
                                        self.txtfldAddress.text = Global().strCombinedAddress(self.dictOfCompanyAddress as? [AnyHashable : Any])
                                        //self.strCustomerAddressId = ""
                                        
                                    })
                                    
                                    let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                        
                                        
                                        
                                    })
                                    
                                    alertCOntroller.addAction(alertAction)
                                    alertCOntroller.addAction(alertActionCancel)

                                    self.present(alertCOntroller, animated: true, completion: nil)
                                    
                                }else{
                                    
                                    self.txtfldAddress.text = Global().strCombinedAddress(self.dictOfCompanyAddress as? [AnyHashable : Any])
                                    //self.strCustomerAddressId = ""

                                    
                                }
                                                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    fileprivate func callAPIToEditLead(){
        
        var strFlowType = ""
      
        switch leadType {
        case .commercial:
           strFlowType = "Commercial"
            break
        default:
           strFlowType = "Residential"
        }
        
        var strComment = ""
       
        if(txtviewComment.text.count > 0){
         
            strComment = txtviewComment.text
            
        }
        //Deepak.s
        let jsonStringUDF = "\(json(from:aryUDFSaveData as Any) ?? "")"
        //********
        let dictJson = ["WebLeadId":"\(dictLeadDetails.value(forKey: "LeadId")!)",
                    "FlowType" : strFlowType,
                    "Name":txtfldLeadName.text!,
                    "CrmContactId":strCRMContactId,
                    "CrmCompanyId":strCRMCompanyId,
                    "IndustryId":strIndustryId,
                    "BranchSysName":strBranchSysName,
                    "SourceIds":arySourceId,
                    "AssignedToType":strAssignType,
                    "AssignedToId":strAssignToId,
                    "Comment":strComment, "Address1":strAddressLine1,
                    "Address2":"",
                    "CityName":strCityName,
                    "Zipcode":strZipCode,
                    "StateId":strStateId,
                    "CountryId":"1",
                    "WebLeadServices":aryOfService,"AddressPropertyTypeId":"\(txtfldPropertyType.tag)" == "0" ? "" : "\(txtfldPropertyType.tag)","UrgencyId":"\(txtfldUrgencyType.tag)" == "0" ? "" : "\(txtfldUrgencyType.tag)",
                        "UserDefinedFields": jsonStringUDF] as [String : Any]
        
       let strUrl =  strServiceUrlMain + "/api/LeadNowAppToSalesProcess/EditWebLead"
     
        WebService.postRequestWithHeaders(dictJson: dictJson as NSDictionary, url: strUrl, responseStringComing: "EditLeadNewVC") { (response, status) in
                        
            self.loader.dismiss(animated: false) {

                if(status == true)
                {
                    if(response.value(forKey: "data") is Bool){
                        
                        if(response.value(forKey: "data") as! Bool == true){
                            let alert = UIAlertController(title: "Message", message: "Lead updated successfully.", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EditLead_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TimeLine_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedContact_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])

                                self.navigationController?.popViewController(animated: false)
                            }))
                            self.present(alert, animated: true, completion: nil)
                            nsud.set(true, forKey: "fromWebLeadDetailTableAction")
                            nsud.synchronize()
                            
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                } else {
                    
                    print("Data is not there ")
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }

        }
    }
        
    
}

extension EditLeadNewVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 1100)// Industry
        {
            if(dictData.count == 0){
                txtfldIndustry.text = ""
                strIndustryId = ""
                txtfldIndustry.resignFirstResponder()
            }
            else{
                txtfldIndustry.text = "\(dictData.value(forKey: "Name")!)"
                strIndustryId = "\(dictData.value(forKey: "IndustryId")!)"
                txtfldIndustry.resignFirstResponder()
            }
            
        }
        if(tag == 1101)// Branch
        {
            if(dictData.count == 0){
                strBranchSysName = ""
                txtfldBranch.text = ""
                txtfldBranch.resignFirstResponder()
                /*  txtfldService.text = ""
                 aryOfService = NSMutableArray()*/
            }
            else{
                strBranchSysName = "\(dictData.value(forKey: "SysName")!)"
                txtfldBranch.text = "\(dictData.value(forKey: "Name")!)"
                txtfldBranch.resignFirstResponder()
                /*  txtfldService.text = ""
                 aryOfService = NSMutableArray()*/
            }
           
        }
        if(tag == 1102)// Source
        {
            if(dictData.count == 0){
                
                arySourceId = NSMutableArray()
                arySelectedSource = NSMutableArray()
                txtfldSource.text = ""

            }else{
                
                let arySource = dictData.object(forKey: "source")as! NSArray
                print(arySource)
                var strName = ""
                arySourceId = NSMutableArray()
                
                arySelectedSource = NSMutableArray()
                
                arySelectedSource = arySource.mutableCopy() as! NSMutableArray
                
                for item in arySource{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                    arySourceId.add("\((item as AnyObject).value(forKey: "SourceId")!)")
                }
                if strName == ""
                {
                    strName = ""
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtfldSource.text = strName
                
            }
           /* dictSelectedSource = dictData
            txtfldSource.text = "\(dictData.value(forKey: "Name")!)"
            txtfldSource.resignFirstResponder()*/
        }
       /* if(tag == 1103)// Service
        {
            dictSelectedService = dictData
            txtfldService.text = "\(dictData.value(forKey: "Name")!)"
            txtfldService.resignFirstResponder()
        }*/
        if(tag == 1104)// Team
        {
            if(dictData.count == 0){
                strAssignType = "Team"
                strAssignToId = ""
                dictSelectedTeam = NSDictionary()
                txtfldAssigneeName.text = ""
                txtfldAssigneeName.resignFirstResponder()
            }
            else{
                strAssignType = "Team"
                strAssignToId = "\(dictData.value(forKey: "TeamId")!)"
                dictSelectedTeam = dictData
                txtfldAssigneeName.text = "\(dictData.value(forKey: "Title")!)"
                txtfldAssigneeName.resignFirstResponder()
            }
            
        }
        if(tag == 1105)// Employee
        {
            if(dictData.count == 0){
                strAssignType = "Individual"
                strAssignToId = ""
                dictSelectedEmployee = NSDictionary()
                txtfldAssigneeName.text = ""
                txtfldAssigneeName.resignFirstResponder()
            }
            else{
                strAssignType = "Individual"
                strAssignToId = "\(dictData.value(forKey: "EmployeeId")!)"
                dictSelectedEmployee = dictData
                txtfldAssigneeName.text = "\(dictData.value(forKey: "FullName")!)"
                txtfldAssigneeName.resignFirstResponder()
            }
        }
        if(tag == 65)
       {
           if(dictData.count == 0){
               txtfldPropertyType.text = ""
               txtfldPropertyType.tag = 0
               aryPropertySelected = NSMutableArray()
           }else{
               aryPropertySelected = NSMutableArray()
               aryPropertySelected.add(dictData)
               txtfldPropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
               txtfldPropertyType.tag = Int("\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")")!
           }
       }
        if(tag == 62)
        {
            if(dictData.count == 0){
                txtfldUrgencyType.text = ""
                txtfldUrgencyType.tag = 0
                aryUrgencySelected = NSMutableArray()
            }else{
                aryUrgencySelected = NSMutableArray()
                aryUrgencySelected.add(dictData)
                txtfldUrgencyType.text = "\(dictData.value(forKey: "Name") ?? "")"
                txtfldUrgencyType.tag = Int("\(dictData.value(forKey: "UrgencyId") ?? "0")" == "" ? "0" : "\(dictData.value(forKey: "UrgencyId") ?? "0")")!
            }
        }
    }
}

extension EditLeadNewVC {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }
                else
                {
                   /* for item in self.scrollView.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)*/
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    
                  
                    self.tableView.frame = CGRect(x: self.txtfldAddress.frame.origin.x, y: self.txtfldAddress.frame.maxY + 10, width: self.txtfldCompany.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)

                    
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}

// MARK: -  ---------------- UITableViewDelegate ----------------

extension EditLeadNewVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            //self.txtfldAddress.text = value.formattedAddress
            self.txtfldAddress.text = addressFormattedByGoogle(value: value)

            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()

        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------

extension EditLeadNewVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtfldAddress){

            self.scrollView.isScrollEnabled = false
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtfldAddress){

            self.scrollView.isScrollEnabled = true
            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if(textField == txtfldAddress){
                        
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()
                    
                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = txtfldAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()
                
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtfldAddress){

            let txtAfterUpdate:NSString = txtfldAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            
//            self.getPlaces(with: parameters) {
//                self.places = $0
//            }
            return true
        }
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------

extension EditLeadNewVC : UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
       
        if (range.location == 0) && (text == " ")
        {
            return false
        }
        
        return true
    }
}

// MARK: -
// MARK: - -----------------------Add Address Delgates

extension EditLeadNewVC : AddNewAddressiPhoneDelegate{

    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {
        
        print(dictData)
        txtfldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
        

    }

}
