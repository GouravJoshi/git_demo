//
//  AddNewAddress_iPhoneVC.swift
//  DPS
//
//  Created by NavinPatidar on 6/25/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
protocol AddNewAddressiPhoneDelegate : class
{
    func getDataAddNewAddressiPhone(dictData : NSMutableDictionary ,tag : Int)
}
class AddNewAddress_iPhoneVC: UIViewController {
    
    // MARK:
       // MARK:-----IBOutlet
    
    @IBOutlet weak var txtFldAddressLine1: ACFloatingTextField!
    @IBOutlet weak var txtFldAddressLine2: ACFloatingTextField!
    @IBOutlet weak var txtFldCity: ACFloatingTextField!
    @IBOutlet weak var txtFldState: ACFloatingTextField!
    @IBOutlet weak var txtFldZipCode: ACFloatingTextField!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    weak var delegate: AddNewAddressiPhoneDelegate?
    var tag = 0
    var dictAddress = NSMutableDictionary()

    // MARK:
    // MARK:-----Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblAddress.textColor = hexStringToUIColor(hex: appThemeColor)
        btnAdd.layer.cornerRadius = 20.0
    //    btnAdd.backgroundColor = hexStringToUIColor(hex: orangeColor)
        if(dictAddress.count != 0){

            
            txtFldAddressLine1.text = "\(dictAddress.value(forKey: "Address1")!)"
            txtFldAddressLine2.text = "\(dictAddress.value(forKey: "Address2")!)"
            txtFldCity.text = "\(dictAddress.value(forKey: "CityName")!)"
            txtFldState.text = "\(dictAddress.value(forKey: "State")!)"
            txtFldState.tag = Int("\(dictAddress.value(forKey: "StateId")!)")!
            txtFldZipCode.text = "\(dictAddress.value(forKey: "ZipCode")!)"

        }
        
    }
    
    // MARK:
    // MARK:-----Actions
    @IBAction func action_Back(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: nil)
     }
    
    @IBAction func action_Add(_ sender: Any) {
         self.view.endEditing(true)
        if( validationAddress()){
                dictAddress.setValue(txtFldAddressLine1.text!, forKey: "Address1")
                dictAddress.setValue(txtFldAddressLine2.text!, forKey: "Address2")
                dictAddress.setValue(txtFldCity.text!, forKey: "CityName")
                dictAddress.setValue(txtFldState.text!, forKey: "State")
                dictAddress.setValue("\(txtFldState.tag)", forKey: "StateId")
                dictAddress.setValue(txtFldZipCode.text!, forKey: "ZipCode")
                delegate?.getDataAddNewAddressiPhone(dictData: dictAddress, tag: tag)
                self.dismiss(animated: false, completion: nil)
        }
    
        }
    
    @IBAction func action_State(_ sender: Any) {
        self.view.endEditing(true)
        if let path = Bundle.main.path(forResource: StateJsonSwift, ofType: "json") {
               do {
                   let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                   let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
                   let aryState = (jsonResult as! NSArray).mutableCopy() as! NSMutableArray
                   if(aryState.count != 0){
                            let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
                            let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
                            vc.strTitle = "Select"
                            vc.strTag = 250
                            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            vc.modalTransitionStyle = .coverVertical
                            vc.handelDataSelectionTable = self
                            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: aryState)
                            self.present(vc, animated: false, completion: {})
                        }
               } catch {

               }
           }
         }
    
    func validationAddress() -> Bool {
        if(txtFldAddressLine1.text?.count == 0){
            addErrorMessage(textView: txtFldAddressLine1, message: Alert_Required)
            return false
        }else if(txtFldCity.text?.count == 0){
            addErrorMessage(textView: txtFldCity, message: Alert_Required)

            return false
        }
        else if(txtFldState.text?.count == 0){
            addErrorMessage(textView: txtFldState, message: Alert_Required)

            return false

        } else if(txtFldZipCode.text?.count == 0){
            addErrorMessage(textView: txtFldZipCode, message: Alert_Required)

            return false

        }
        else if(txtFldZipCode.text!.count < 5){
                   addErrorMessage(textView: txtFldZipCode, message: "ZipCode is invalid!")
                   return false
               }
        
        return true
    }
    func addErrorMessage(textView : ACFloatingTextField, message : String) {
        textView.showError(withText: message)
    }
}
// MARK:-
// MARK:- ---------UITextFieldDelegate

extension AddNewAddress_iPhoneVC : UITextFieldDelegate {
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        if(textField == txtFldZipCode){
          return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 4)
        }
        return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 200)

        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        self.view.endEditing(true)
        return true
    }
}
// MARK:-
// MARK:- ---------POPUpView Delegate

extension AddNewAddress_iPhoneVC : CustomTableView {
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 250){ // for state
            if(dictData.count != 0){
                txtFldState.text = "\(dictData.value(forKey: "Name")!)"
                txtFldState.tag = Int("\(dictData.value(forKey: "StateId")!)")!
            }else{
               txtFldState.text = ""
                txtFldState.tag = 0
            }
            
        }
    }
}

