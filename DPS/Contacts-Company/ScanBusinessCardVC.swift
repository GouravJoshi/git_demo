//
//  ScanBusinessCardVC.swift
//  DPS
//
//  Created by Saavan Patidar on 06/01/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar changes on 2019
//  Saavan Patidar 2019 

import UIKit

class ScanBusinessCardVC: UIViewController {

    // MARK: - ---------------Global Variables---------------

    var imagePicker = UIImagePickerController()

    // MARK: - ---------------Outlets---------------
    
    @IBOutlet weak var scannedImageView: UIImageView!
    
    @IBOutlet weak var resultTxtView: UITextView!
    
    @IBOutlet weak var filteredResultTxtView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - ---------------Button Action---------------
    
    @IBAction func action_Back(_ sender: UIButton) {
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_Scan(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        if let popoverController = alert.popoverPresentationController {
                              popoverController.sourceView = sender as UIView
                              popoverController.sourceRect = sender.bounds
                            //  popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                          }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    

}


// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension ScanBusinessCardVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        let imageData = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.jpegData(compressionQuality:1.0)
        
        scannedImageView.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        //btnActions.setImage(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, for: .normal)
        
    }
}
