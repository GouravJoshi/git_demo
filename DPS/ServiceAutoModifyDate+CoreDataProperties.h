//
//  ServiceAutoModifyDate+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ServiceAutoModifyDate.h"

NS_ASSUME_NONNULL_BEGIN

@interface ServiceAutoModifyDate (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *workorderId;
@property (nullable, nonatomic, retain) NSString *modifyDate;

@end

NS_ASSUME_NONNULL_END
