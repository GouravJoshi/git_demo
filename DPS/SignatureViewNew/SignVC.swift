//
//  SignVC.swift
//  SignDemo
//
//  Created by Navin Patidar on 9/13/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

protocol SignatureViewDelegate : class {
    func imageFromSignatureView(image: UIImage)
}




class SignVC: UIViewController {
    
    @IBOutlet weak var viewSign: VMSignatureView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnSubmite: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgview: UIImageView!
    
    var strMessage = String()
    
    var delegate:SignatureViewDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        /*self.lblMessage.transform = CGAffineTransform(rotationAngle: (90.0 * .pi) / 180.0)
         self.viewSign.transform = CGAffineTransform(rotationAngle: (90.0 * .pi) / 180.0)
         self.btnClear.transform = CGAffineTransform(rotationAngle: (90.0 * .pi) / 180.0)
         self.btnSubmite.transform = CGAffineTransform(rotationAngle: (90.0 * .pi) / 180.0)
         self.btnBack.transform = CGAffineTransform(rotationAngle: (90.0 * .pi) / 180.0)
         self.lblMessage.text = strMessage
         
         var yPoint = 20
         if(yPoint == 20){
         yPoint = 40
         }
         self.btnClear.frame = CGRect(x: 8, y: yPoint, width: 35, height: Int(self.view.frame.height)/2 - yPoint*2)
         self.btnSubmite.frame = CGRect(x: 8, y: Int(self.view.frame.height)/2 + 20, width: 35, height: Int(self.view.frame.height)/2 - yPoint*2)
         self.btnBack.frame = CGRect(x: Int(self.view.frame.width) - 44, y: yPoint, width: 44, height: 44)
         self.lblMessage.frame = CGRect(x: Int(self.view.frame.width) - 44, y: Int(self.btnBack.frame.height) + 30, width: 44, height: Int(self.view.frame.height) - (Int(self.btnBack.frame.height) + 30 + yPoint))
         self.viewSign.frame = CGRect(x: 44, y: yPoint, width: Int(self.view.frame.width) - 100, height: Int(self.view.frame.height) - 40)
         print(self.btnBack.frame)*/
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSign.layer.borderWidth = 1.0
        self.viewSign.layer.borderColor = UIColor.blue.cgColor
        
        btnClear.layer.cornerRadius = btnClear.frame.size.height/2
        btnClear.layer.borderWidth = 0
        btnClear.layer.borderColor = UIColor.black.cgColor
        btnClear.layer.borderWidth = 0.5
        
        btnSubmite.layer.cornerRadius = btnSubmite.frame.size.height/2
        btnSubmite.layer.borderWidth = 0
        btnSubmite.layer.borderColor = UIColor.black.cgColor
        btnSubmite.layer.borderWidth = 0.5
        
    }
    
    
    @IBAction func clearTapped(_ sender: Any) {
        self.viewSign.clear()
    }
    @IBAction func BackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func getImageTapped(_ sender: Any) {
        
        imgview.image = self.viewSign.getSignature()
        
        if(self.viewSign.getSignature() != nil){
            delegate?.imageFromSignatureView(image: self.viewSign.getSignature()!)
            self.dismiss(animated: true, completion: nil)
        }
    }
}


/*
 
 extension ViewController : SignatureViewDelegate{
 func imageFromSignatureView(image: UIImage) {
 self.imageview.image = image
 }
 }
 
 */
/*
 
 let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignVC") as! SignVC
 vc.strMessage = "Navin Patidar"
 vc.delegate = self
 self.present(vc, animated: true, completion: nil)
 
 */
