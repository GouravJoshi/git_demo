//
//  EquipmentListInvoiceViewTableViewCell.h
//  DPS
//hjkjhkjh
//  Created by Saavan Patidar on 27/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquipmentListInvoiceViewTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_Category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EquipName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Area;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AssociatedWith;
@property (strong, nonatomic) IBOutlet UILabel *lbl_WarrantyDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_InstallDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_LastServiceDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ServiceDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_NextServiceDate;
@property (strong, nonatomic) IBOutlet UIButton *btnMorenLess;
@property (strong, nonatomic) IBOutlet UIView *viewBackgroundEquipDynamicForm;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckUncheck;
@property (strong, nonatomic) IBOutlet UILabel *lblSerialNo;
@property (strong, nonatomic) IBOutlet UILabel *lblModelNo;
@property (strong, nonatomic) IBOutlet UILabel *lblManufacturer;

@end
