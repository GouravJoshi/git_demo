

#import "DrawingBoardViewController.h"
#import "DrawingBoardView.h"
#import "CellDataSource.h"

#import "ControlProtocol.h"
#import "DrawBrushProtocol.h"
#import "TopDrawingBoardView.h"

#import "BrushModel.h"
#import "BrushCharacterModel.h"
#import "DrawingBoardViewDelegate.h"
#import "Global.h"
#import "DPS-Swift.h"

@interface CellOptions : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *viewColorIndicator;
@property (weak, nonatomic) IBOutlet UILabel *lblValueIndicator;

@end

@interface DrawingBoardViewController ()<UITableViewDelegate, UITableViewDataSource, DrawingBoardViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>
{
    NSString *strImagePathToEdit;
    CGFloat thicknessGlobal;
    int indexSelected;
    NSArray *aryColor;
    NSArray *aryColorName;
    NSMutableArray *arrThickness;
    Global *global;
}

@property (strong, nonatomic) IBOutlet DrawingBoardView *drawingBoardView;
@property (strong, nonatomic) IBOutlet TopDrawingBoardView *topDrawingBoardView;
@property (strong, nonatomic) IBOutlet UIView *drawerMenuView;
@property (strong, nonatomic) IBOutlet UITableView *controlPanelTableView;

@property (nonatomic, strong) NSArray<CellDataSource *> *controlList;
@property (nonatomic, strong) UIColor *brushColor;


@end
@interface cellColorPick : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewColor;
@property (weak, nonatomic) IBOutlet UILabel *lblColorName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewDot_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewDot_W;

@end
@implementation DrawingBoardViewController
{
    UIColor *_color;
    UIColor *_GraphLineColor;
    int sectionIndex;
    
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    _height_ViewDetail.constant = 0.0;
    global = [[Global alloc] init];
    
    NSUserDefaults *defs1 = [NSUserDefaults standardUserDefaults];
    BOOL isDrawGraphLines=[defs1 boolForKey:@"drawgraphlines"];
    if (isDrawGraphLines) {
        
        [_switchGraphLines setOn:YES];
        
    } else {
        
        [_switchGraphLines setOn:NO];
        
    }
    
    aryColor = [[NSArray alloc]initWithObjects:[UIColor redColor],[UIColor blackColor],[UIColor blueColor],[UIColor yellowColor],[UIColor purpleColor],[UIColor greenColor],[UIColor whiteColor],[UIColor grayColor],[UIColor orangeColor], nil];
    aryColorName = [[NSArray alloc]initWithObjects:@"Red",@"Black",@"Blue",@"Yellow",@"Purple",@"Green",@"White",@"Gray",@"Orange",nil];
    
    // [self CreatMenuGraph];// commented by akshay on 3 oct 2019
    [_collectionColorPick setHidden:true];
    
    indexSelected=0;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSString *strThickness=[defs valueForKey:@"thickness"];
    
    if (strThickness.length==0) {
        
        thicknessGlobal=3;
        
    } else {
        
        thicknessGlobal=[strThickness floatValue];
        
    }
    
    NSData *theData=[[NSUserDefaults standardUserDefaults] dataForKey:@"colorGraph"];
    
    if (theData != nil)
    {
        _color =(UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData];
        
    }else{
        _color = UIColor.blackColor;
        NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_color];
        [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraph"];
    }
    
    
    NSData *theData1=[[NSUserDefaults standardUserDefaults] dataForKey:@"colorGraphLines"];
    if (theData != nil)
    {
        _GraphLineColor =(UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData1];
        
    }else{
        _GraphLineColor = UIColor.blackColor;
        NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_GraphLineColor];
        [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraphLines"];
        
    }
    
    
    
    
    self.controlList = [CellDataSource dataSource];;
    [self.controlPanelTableView registerClass:UITableViewCell.class forCellReuseIdentifier:NSStringFromClass(UITableViewCell.class)];
    [self.controlPanelTableView reloadData];
    self.drawingBoardView.hidden = NO;
    [self.drawingBoardView removeFromSuperview];
    
    self.brushColor = [UIColor blackColor];
    
    if (_color==nil) {
        
        self.brushColor = [UIColor blackColor];
        
        
    } else {
        
        self.brushColor = _color;
        
        
    }
    
    id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
    self.topDrawingBoardView.brushModel = brush;
    self.topDrawingBoardView.delegate = self;
    self.topDrawingBoardView.drawEnable = YES;
    self.drawingBoardView.backgroundColor=[UIColor clearColor];
    self.topDrawingBoardView.backgroundColor=[UIColor clearColor];
    
    if (self.backgroundImage != nil) {
        _backgroundImageView.image = self.backgroundImage;
    } else {
        NSUserDefaults *defsImageName=[NSUserDefaults standardUserDefaults];
        strImagePathToEdit=[defsImageName valueForKey:@"editImagePath"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImagePathToEdit]];
        UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];
        //_imageToEdit=imageEdit;
        _backgroundImageView.image=imageEdit;
    }
    // Navin 14 oct 2019 ///
    // For iPhone
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        [self configureIndicatorViews];
    }
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
 
    NSTimeInterval delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        LegendGraphView *obj = [[LegendGraphView alloc] init];
        [obj CreatFormLegendWithViewLegend:_viewGraphLegend controllor:self OnResultBlock:^(UIView * viewContain) {
            int count = (int)[viewContain tag];
            _heightGraphLegend.constant = 150.0;
            _heightScrollGraphLegend.constant = count + 50;
            
        }];
  
        
    });
    
  
}
#pragma mark -- DrawingBoardViewDelegate

- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didBeganPoint:(CGPoint)point {
    _height_ViewDetail.constant = 0.0;
}

- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didMovedPoint:(CGPoint)point {
    _height_ViewDetail.constant = 0.0;

}

- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didEndedPoint:(CGPoint)point brush:(id<DrawBrushProtocol>)brush {
    _height_ViewDetail.constant = 0.0;

}

#pragma mark -- UITableViewDelegate, UITableViewDataSource

#pragma mark:- UITableView's delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _controlList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 36.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"CellOptions";
    
    CellOptions *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[CellOptions alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.lblTitle.text = self.controlList[indexPath.row].title;
    cell.lblValueIndicator.hidden = YES;
    cell.viewColorIndicator.hidden = YES;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        //  cell.lblTitle.font=[UIFont systemFontOfSize:17];
        cell.lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f];
    }
    else
    {
        // cell.lblTitle.font=[UIFont systemFontOfSize:15];
        cell.lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
    }
    
    if(indexPath.row == 10 || indexPath.row == 12)
    {
        cell.viewColorIndicator.hidden = NO;
        if(indexPath.row == 12){
            
            NSData *theData=[[NSUserDefaults standardUserDefaults] dataForKey:@"colorGraphLines"];
            
            if (theData != nil)
            {
                cell.viewColorIndicator.backgroundColor =(UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData];
            }
            
        }else{
            
            NSData *theData=[[NSUserDefaults standardUserDefaults] dataForKey:@"colorGraph"];
            
            if (theData != nil)
            {
                cell.viewColorIndicator.backgroundColor =(UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData];
            }
            
            
            // cell.viewColorIndicator.backgroundColor = _color;
        }
        cell.viewColorIndicator.layer.cornerRadius = cell.viewColorIndicator.frame.size.height/2;
        cell.viewColorIndicator.layer.masksToBounds = YES;
    }else{
        cell.viewColorIndicator.hidden = YES;
    }
    
    if(indexPath.row == 11)
    {
        cell.lblValueIndicator.text = [NSString stringWithFormat:@"%d pts",(int)thicknessGlobal];
        cell.lblValueIndicator.hidden = NO;
    }
    
    if((int)indexPath.row == sectionIndex){
        cell.contentView.backgroundColor = UIColor.groupTableViewBackgroundColor;
        
    }else{
        cell.contentView.backgroundColor = UIColor.whiteColor;
        
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    sectionIndex = (int)indexPath.row;
    [tableView reloadData];
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    _height_ViewDetail.constant = 0.0;
    
    if (indexPath.row==10) {
        _height_ViewDetail.constant = 135.0;

        _collectionColorPick.tag = 1;
        [_collectionColorPick setHidden:false];
        
        //        isColorPickerForGraphLines=NO;
        //        [self openColorPickerView];
        
        [_collectionColorPick reloadData];
        
    } else if (indexPath.row==11){
        
        //[self openThicknessInputAlert];
        _height_ViewDetail.constant = 135.0;

        arrThickness = [[NSMutableArray alloc] init];
        
        for (int k=0; k<9; k++) {
            
            NSDictionary *dictData = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",k+1],@"Name", nil];
            
            [arrThickness addObject:dictData];
            
        }
        
        _collectionColorPick.tag = 2;
        [_collectionColorPick setHidden:false];
        
        [_collectionColorPick reloadData];
        
    } else if (indexPath.row==12){
        _height_ViewDetail.constant = 135.0;

        _collectionColorPick.tag = 0;
        [_collectionColorPick setHidden:false];
        
        //        isColorPickerForGraphLines=YES;
        //        [self openColorPickerView];
        //
        
        [_collectionColorPick reloadData];
        
    }else {
        NSString *title = self.controlList[indexPath.row].title;
        NSLog(@"%@", title);
        MenuType type = self.controlList[indexPath.row].type;
        
        if ([title caseInsensitiveCompare:@"Redo"]==NSOrderedSame||[title caseInsensitiveCompare:@"undo"]==NSOrderedSame||[title caseInsensitiveCompare:@"clear"]==NSOrderedSame)
        {
            
            /*
             id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
             self.topDrawingBoardView.brushModel = brush;
             self.topDrawingBoardView.delegate = self;
             self.topDrawingBoardView.drawEnable = NO;
             */
            
        }
        else
        {
            id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            // indexSelected = (int)indexPath.row;
            
            /*   id<DrawBrushProtocol> brush = [BrushModel brushWithShape:indexSelected color:self.brushColor thickness:thicknessGlobal];*/
            
            self.topDrawingBoardView.brushModel = brush;
            self.topDrawingBoardView.delegate = self;
            self.topDrawingBoardView.drawEnable = YES;
        }
        
        [self operatingWithMenuType:type];
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (void)operatingWithMenuType:(MenuType)type {
    if (MenuType_DrawingFreeform <= type && type <= MenuType_Eraser) {
        
        id<DrawBrushProtocol> brush = [self genBrushWithMenuType:type];
        self.topDrawingBoardView.brushModel = brush;
    }
    
    if (MenuType_Undo <= type && type <= MenuType_Clear ) {
        switch (type) {
            case MenuType_Undo:
                [self.topDrawingBoardView undo:YES];
                break;
                
            case MenuType_Redo:
                [self.topDrawingBoardView redo];
                break;
                
            case MenuType_Clear:
                [self.topDrawingBoardView clear:YES];
                break;
                
            default:
                break;
        }
    }
}

- (id<DrawBrushProtocol>)genBrushWithMenuType:(MenuType)menuType {
    id<DrawBrushProtocol> brush = nil;
    switch (menuType) {
        case MenuType_DrawingFreeform:
            brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingStraightLine:
            brush = [BrushModel brushWithShape:ShapeType_StraightLine color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingSquare:
            brush = [BrushModel brushWithShape:ShapeType_Square color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingElipse:
            brush = [BrushModel brushWithShape:ShapeType_Elipse color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingRound:
            brush = [BrushModel brushWithShape:ShapeType_Round color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingText: {
            brush = [BrushCharacterModel defaultWithShape:ShapeType_Text text:@"" color:self.brushColor font:36.f frame:self.view.frame];
        }
            break;
            
        case MenuType_Eraser:
            brush = [BrushModel brushWithShape:ShapeType_Freeform color:UIColor.whiteColor thickness:thicknessGlobal];
            break;
            
        default:
            break;
    }
    self.topDrawingBoardView.brushModel = brush;
    return brush;
}

- (IBAction)action_Cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)action_Save:(id)sender {
    
    /*
     // create graphics context with screen size
     CGRect screenRect = CGRectMake(_backgroundImageView.frame.origin.x, _backgroundImageView.frame.origin.y, _backgroundImageView.frame.size.width, _backgroundImageView.frame.size.height);
     
     UIGraphicsBeginImageContext(screenRect.size);
     CGContextRef ctx = UIGraphicsGetCurrentContext();
     [[UIColor blackColor] set];
     CGContextFillRect(ctx, screenRect);
     
     // grab reference to our window
     UIWindow *window = [UIApplication sharedApplication].keyWindow;
     
     // transfer content into our context
     [window.layer renderInContext:ctx];
     UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     */
    
    CGRect rect = CGRectMake(0, 0, _backgroundImageView.frame.size.width, _backgroundImageView.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.mainView.layer renderInContext:context];
    UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (self.backgroundImage != nil) {
        [self.delegate refresImage:self.indexClicked withImage:screengrab];
    } else {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImagePathToEdit]];
    NSData *signImageData = UIImagePNGRepresentation(screengrab);
    [signImageData writeToFile:path atomically:YES];
    
    if ([_strModuleType isEqualToString:@"WdoSalesServiceFlow"]) {
        
        NSString *strStatusLead = [global fetchLeadStatus:_strWdoLeadId];
        
        if ([strStatusLead isEqualToString:@"complete"]) {
            
            
            
        } else {
            
            NSMutableString *strTempImagePath = [NSMutableString stringWithString:strImagePathToEdit];
            
            NSString *strImageNameSales = [strTempImagePath stringByReplacingOccurrencesOfString:@"\\Documents\\UploadImages\\" withString:@""];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImageNameSales]];
            NSData *signImageData = UIImagePNGRepresentation(screengrab);
            [signImageData writeToFile:path atomically:YES];
            
        }
        
    }
    
    if ([_strModuleType isEqualToString:@"ProblemId"]) {
        
        NSString *strStatusLead = [global fetchLeadStatus:_strWdoLeadId];
        
        if ([strStatusLead isEqualToString:@"complete"]) {
            
            
            
        } else {
            
            NSMutableString *strTempImagePath = [NSMutableString stringWithString:strImagePathToEdit];
            
            NSString *strImageNameSales = [strTempImagePath stringByReplacingOccurrencesOfString:@"\\Documents\\ProblemIdentificationImages\\" withString:@""];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImageNameSales]];
            NSData *signImageData = UIImagePNGRepresentation(screengrab);
            [signImageData writeToFile:path atomically:YES];
            
        }
        
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    if([[defs valueForKey:@"isFromTermiteTexas"] boolValue]==YES)
    {
        [defs setValue:strImagePathToEdit forKey:@"graphImagePathTermite"];
        [defs setValue:@"strAddGraphImg" forKey:@"strAddGraphImg"];
        [defs synchronize];
    }
     
        if ([_strModuleType isEqualToString:@"SalesFlow"]) {
            
            [defs setBool:YES forKey:@"fromEditImageSales"];
            [defs synchronize];
        }
        
        
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)action_CancelColor:(id)sender {
    
    [_colorPickerViewMain removeFromSuperview];
    
}

- (IBAction)action_SaveColor:(id)sender {
    
    self.brushColor=_color;
    [_colorPickerViewMain removeFromSuperview];
    id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
    self.topDrawingBoardView.brushModel = brush;
    
    NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_color];
    [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraph"];
    
}
-(void)openThicknessInputAlert{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Thickness"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Thickness";
        textField.textColor = [UIColor blueColor];
        textField.delegate=self;
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        NSString *strThickness=[defs valueForKey:@"thickness"];
        
        if (strThickness.length==0) {
            
            textField.text=@"3";
            
        } else {
            
            textField.text=strThickness;
            
        }
        
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeNumberPad;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        int textDays=[txtHistoricalDays.text intValue];
        if ((textDays>=1) && (textDays<=100)) {
            
            NSString *strString=[NSString stringWithFormat:@"%@",txtHistoricalDays.text];
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:strString forKey:@"thickness"];
            [defs synchronize];
            thicknessGlobal=[strString floatValue];
            
            id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
            self.topDrawingBoardView.brushModel = brush;
            
        } else {
            [self performSelector:@selector(alertViewForThickness) withObject:nil afterDelay:0.2];
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)alertViewForThickness{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter thickness between 1 to 100" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}


- (id<DrawBrushProtocol>)genBrushWithMenuTypeNew:(int)menuType {
    
    id<DrawBrushProtocol> brush = nil;
    switch (menuType) {
        case 0:
            brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 1:
            brush = [BrushModel brushWithShape:ShapeType_StraightLine color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 2:
            brush = [BrushModel brushWithShape:ShapeType_Square color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 3:
            brush = [BrushModel brushWithShape:ShapeType_Elipse color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 4:
            brush = [BrushModel brushWithShape:ShapeType_Round color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 5: {
            brush = [BrushCharacterModel defaultWithShape:ShapeType_Text text:@"" color:self.brushColor font:36.f frame:self.view.frame];
        }
            break;
            
        case 6:
            brush = [BrushModel brushWithShape:ShapeType_Freeform color:UIColor.whiteColor thickness:thicknessGlobal];
            break;
            
        default:
            break;
    }
    self.topDrawingBoardView.brushModel = brush;
    return brush;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    BOOL  isReturnTrue;
    
    isReturnTrue=YES;
    
    if (!isNumeric) {
        
        if ([string isEqualToString:@""]) {
            
            isReturnTrue=YES;
            
        }else
            
            isReturnTrue=NO;
        
    }
    
    if (!isReturnTrue) {
        
        return NO;
        
    }
    
    return YES;
}

//Akshay 24 may 2019
/* return @[@"FreeForm",@"Straight Line",@"Rectangle",@"Oval",
 @"Circle",@"Text",@"Eraser",
 @"Undo",@"Redo",@"Clear",@"Select color",@"Thickness",@"Select color for graph lines"];*/
- (IBAction)actoinOnSelectOption:(id)sender {
    
    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select" message:@"" preferredStyle:   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? UIAlertControllerStyleActionSheet :UIAlertControllerStyleAlert];
    
    UIAlertAction *actionFreeForm = [UIAlertAction actionWithTitle:@"FreeForm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        id<DrawBrushProtocol> brush = nil;
        _height_ViewDetail.constant = 0.0;

        brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionStraightLine = [UIAlertAction actionWithTitle:@"Straight Line" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_StraightLine color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionRectangle = [UIAlertAction actionWithTitle:@"Rectangle" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_Square color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionOval = [UIAlertAction actionWithTitle:@"Oval" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_Elipse color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionCircle = [UIAlertAction actionWithTitle:@"Circle" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_Round color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionText = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        id<DrawBrushProtocol> brush = nil;
        brush = [BrushCharacterModel defaultWithShape:ShapeType_Text text:@"" color:self.brushColor font:36.f frame:self.view.frame];
        self.topDrawingBoardView.brushModel = brush;
        _height_ViewDetail.constant = 0.0;

        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionEraser = [UIAlertAction actionWithTitle:@"Eraser" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_Freeform color:UIColor.whiteColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionUndo = [UIAlertAction actionWithTitle:@"Undo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        [self.topDrawingBoardView undo:YES];
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionRedo = [UIAlertAction actionWithTitle:@"Redo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        [self.topDrawingBoardView redo];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionClear = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        [self.topDrawingBoardView clear:YES];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionSelectColor = [UIAlertAction actionWithTitle:@"Select color" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 135.0;

        _collectionColorPick.tag = 1;
        [_collectionColorPick setHidden:false];
        [_collectionColorPick reloadData];
        
        //   [self openColorPickerView];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionThickness = [UIAlertAction actionWithTitle:@"Thickness" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 135.0;
        arrThickness = [[NSMutableArray alloc] init];
        for (int k=0; k<9; k++) {
            NSDictionary *dictData = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",k+1],@"Name", nil];
            [arrThickness addObject:dictData];
        }
        _collectionColorPick.tag = 2;
        [_collectionColorPick setHidden:false];
        
        [_collectionColorPick reloadData];
        //  [self openThicknessInputAlert];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionSelectColorforGraphLines = [UIAlertAction actionWithTitle:@"Graph line color" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 135.0;
        _collectionColorPick.tag = 0;
        [_collectionColorPick setHidden:false];
        [_collectionColorPick reloadData];
        //  [self openColorPickerView];
        //  [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertController addAction:actionFreeForm];
    [alertController addAction:actionStraightLine];
    [alertController addAction:actionRectangle];
    [alertController addAction:actionOval];
    [alertController addAction:actionCircle];
    [alertController addAction:actionText];
    [alertController addAction:actionEraser];
    [alertController addAction:actionUndo];
    [alertController addAction:actionRedo];
    [alertController addAction:actionClear];
    [alertController addAction:actionSelectColor];
    [alertController addAction:actionThickness];
    [alertController addAction:actionSelectColorforGraphLines];
    [alertController addAction:actionCancel];
    
    // Check if the Device is an iPhone/iPad //
//         if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone) {
//             [alertController.popoverPresentationController setPermittedArrowDirections:UIPopoverArrowDirectionUp];
//             CGRect rect =  [sender frame];
//             rect.origin.x = self.view.frame.size.width;
//             rect.origin.y = rect.origin.y + 50;
//             alertController.popoverPresentationController.sourceView = self.view;
//             alertController.popoverPresentationController.sourceRect = rect;
//         }
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
// MARK: CollectionView Delegate
// MARK:
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (_collectionColorPick.tag==2) {
        
        return [arrThickness count];
        
    }else{
        
        return [aryColor count];
        
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cellColorPick *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellColorPick" forIndexPath:indexPath];
    if (_collectionColorPick.tag==2) {
        
        NSDictionary *dictData =arrThickness[indexPath.row];
        NSString *strString=[NSString stringWithFormat:@"%@ Point",[dictData valueForKey:@"Name"]];
        
        cell.lblColorName.text = strString;
        cell.lblColorName.textColor = UIColor.themeColor;
        cell.viewColor.backgroundColor = UIColor.blackColor;
        cell.const_ViewDot_H.constant = [strString intValue];
        cell.const_ViewDot_W.constant = [strString intValue];
        cell.viewColor.layer.cornerRadius = [strString intValue]/2;
        
    }else{
        
        cell.lblColorName.text = [aryColorName objectAtIndex:indexPath.row];
        cell.viewColor.backgroundColor = [aryColor objectAtIndex:indexPath.row];
        
        if ([cell.lblColorName.text isEqualToString:@"White"]) {
            
            cell.lblColorName.textColor = UIColor.blackColor;
            
        } else {
            
            cell.lblColorName.textColor = [aryColor objectAtIndex:indexPath.row];
            
        }
        
        cell.const_ViewDot_H.constant = 20;
        cell.const_ViewDot_W.constant = 20;
        cell.viewColor.layer.cornerRadius = 10;
        cell.viewColor.layer.borderColor = UIColor.groupTableViewBackgroundColor.CGColor;
        cell.viewColor.layer.borderWidth = 1.0;
        
    }
    
    return cell;
}
//
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return CGSizeMake(180, 40);
        
    }else{
        return CGSizeMake(110, 30);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _height_ViewDetail.constant = 0.0;
    if (_collectionColorPick.tag==2) {
        
        NSDictionary *dictData =arrThickness[indexPath.row];
        
        NSString *strString=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:strString forKey:@"thickness"];
        [defs synchronize];
        thicknessGlobal=[strString floatValue];
        
        id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
        self.topDrawingBoardView.brushModel = brush;
        
    }else{
        
        
        if (_collectionColorPick.tag==0) { // For GraphLines
            _GraphLineColor = [aryColor objectAtIndex:indexPath.row];
            NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_GraphLineColor];
            [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraphLines"];
            
        } else { // For Draw Graph
            _color = [aryColor objectAtIndex:indexPath.row];
            self.brushColor=_color;
            id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
            self.topDrawingBoardView.brushModel = brush;
            NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_color];
            [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraph"];
        }
        
    }
    
    [_collectionColorPick setHidden:true];
    
    [self callMethod];
    
    [_controlPanelTableView reloadData];
    
    // Navin 14 oct 2019 ///
    // For iPhone
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        [self configureIndicatorViews];
    }
    
    
}
-(void)configureIndicatorViews
{
    _viewSelectedGraphColor_iPhone.backgroundColor = _color;
    _viewSelectedGraphColor_iPhone.layer.cornerRadius = _viewSelectedGraphColor_iPhone.frame.size.height/2;
    _viewSelectedGraphColor_iPhone.layer.masksToBounds = YES;
    _viewSelectedGraphColor_iPhone.layer.borderWidth = 1.0;
    _viewSelectedGraphColor_iPhone.layer.borderColor = UIColor.groupTableViewBackgroundColor.CGColor;
    
    
    
    
    _viewSelectedGraphLineColor_iPhone.backgroundColor = _GraphLineColor;
    _viewSelectedGraphLineColor_iPhone.layer.cornerRadius = _viewSelectedGraphLineColor_iPhone.frame.size.height/2;
    _viewSelectedGraphLineColor_iPhone.layer.masksToBounds = YES;
    _viewSelectedGraphLineColor_iPhone.layer.borderWidth = 1.0;
    _viewSelectedGraphLineColor_iPhone.layer.borderColor = UIColor.groupTableViewBackgroundColor.CGColor;
    
    _lblValueThickness.text = [NSString stringWithFormat:@"Thickness %d pts", (int)thicknessGlobal];
}
-(void)callMethod{
    
    id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
    self.topDrawingBoardView.brushModel = brush;
    self.topDrawingBoardView.delegate = self;
    self.topDrawingBoardView.drawEnable = YES;
    
}

- (IBAction)action_SwitchGraphLines:(id)sender {
    
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    if ([sender isOn]) {
        
        [defs setBool:YES forKey:@"drawgraphlines"];
        [defs synchronize];
        
    } else {
        
        [defs setBool:NO forKey:@"drawgraphlines"];
        [defs synchronize];
        
    }
    
}


@end
