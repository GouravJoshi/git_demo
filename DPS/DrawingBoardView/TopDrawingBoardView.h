

#import <UIKit/UIKit.h>

#import "DrawingBoardOperatingProtocol.h"
#import "DrawingBoardViewProtocol.h"
#import "DrawingBoardViewDelegate.h"
#import "DrawingBoardProxyProtocol.h"

@interface TopDrawingBoardView : UIView<DrawingBoardViewProtocol>
@property (nonatomic, assign) BOOL drawEnable;
@property (nonatomic, strong) id<DrawBrushProtocol> brushModel;
@property (nonatomic, strong, readonly) NSArray<id<DrawBrushProtocol>> *brushes;
@property (nonatomic, weak) id<DrawingBoardViewDelegate> delegate;
@property (nonatomic, strong) id<DrawingBoardProxyProtocol> proxy;
- (void)drawRect:(CGRect)rect;

    
@end
