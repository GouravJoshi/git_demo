
#import <Foundation/Foundation.h>
#import "DrawBrushProtocol.h"


@interface BrushModel : NSObject<DrawBrushProtocol, NSCopying, NSCoding>
@property (nonatomic, assign) BOOL isFill;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) ShapeType shape;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, copy) NSMutableArray<NSValue*> *frames;

+ (instancetype)brushWithShape:(ShapeType)shape lineColor:(UIColor *)color lineWidth:(CGFloat)width fillColor:(UIColor *)fillColor frames:(NSArray<NSValue*> *)frames fill:(BOOL)isFill;
+ (instancetype)defaultWithShape:(ShapeType)shape lineColor:(UIColor *)color lineWidth:(CGFloat)width frames:(NSArray<NSValue*> *)frames;
@end
