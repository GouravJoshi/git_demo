
#import "CellDataSource.h"

@implementation CellDataSource

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

+ (NSArray<NSString *> *)titles {
    return @[@"FreeForm",@"Straight Line",@"Rectangle",@"Oval",
             @"Circle",@"Text",@"Eraser",
             @"Undo",@"Redo",@"Clear",@"Color",@"Thickness",@"Graph line color"];
}

+ (NSArray<NSNumber *> *)enums {
    return @[@(MenuType_DrawingFreeform),@(MenuType_DrawingStraightLine),@(MenuType_DrawingSquare),@(MenuType_DrawingElipse),
             @(MenuType_DrawingRound),@(MenuType_DrawingText),@(MenuType_Eraser),
             @(MenuType_Undo),@(MenuType_Redo),@(MenuType_Clear),@(MenuType_SelectColor),@(MenuType_Thickness),@(MenuType_SelectColorGraphLines)];
}

+ (NSArray<CellDataSource *> *)dataSource {
    
    NSMutableArray<CellDataSource *> *array = [NSMutableArray arrayWithCapacity:CellDataSource.titles.count];
    [CellDataSource.titles enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CellDataSource *data = [CellDataSource new];
        data.type = CellDataSource.enums[idx].integerValue;
        data.title = obj;
        [array addObject:data];
    }];
    
    return array;
}
@end
