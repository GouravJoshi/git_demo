

#import "BrushCharacterModel.h"
#import <objc/runtime.h>
#import "NSObject+DeepCopy.h"

@implementation BrushCharacterModel

- (id)copyWithZone:(NSZone *)zone {
    BrushCharacterModel *model = [[[self class] allocWithZone:zone] init];
    model.shape = self.shape;
    model.color = self.color;
    model.character = self.character;
    model.fontSize = self.fontSize;
    model.frames = self.frames;
    return model;
}

- (instancetype)deepCopy {
    return [self rs_deepCopy];
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    unsigned int count = 0;
    Ivar *ivarLists = class_copyIvarList([BrushCharacterModel class], &count);
    for (int i = 0; i < count; i++) {
        const char* name = ivar_getName(ivarLists[i]);
        NSString* strName = [NSString stringWithUTF8String:name];
        [aCoder encodeObject:[self valueForKey:strName] forKey:strName];
    }
    free(ivarLists);
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        unsigned int count = 0;
        Ivar *ivarLists = class_copyIvarList([BrushCharacterModel class], &count);
        for (int i = 0; i < count; i++) {
            const char* name = ivar_getName(ivarLists[i]);
            NSString* strName = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
            id value = [aDecoder decodeObjectForKey:strName];
            [self setValue:value forKey:strName];
        }
        free(ivarLists);
    }
    return self;
}

- (instancetype)initWithShape:(ShapeType)shape text:(NSString *)text color:(UIColor *)color font:(CGFloat)fontSize frame:(CGRect)frame {
    self = [super init];
    if (self) {
        _shape = shape;
        _character = text;
        _color = color;
        _fontSize = fontSize;
        _frames = [NSMutableArray arrayWithObject:[NSValue valueWithCGRect:frame]];
    }
    return self;
}

+ (instancetype)brushWithShape:(ShapeType)shape color:(UIColor *)color thickness:(CGFloat)width {
    BrushCharacterModel *model = [[BrushCharacterModel alloc] initWithShape:shape text:@"" color:color font:36.f frame:CGRectZero];
    return model;
}

+ (instancetype)defaultWithShape:(ShapeType)shape text:(NSString *)text color:(UIColor *)color font:(CGFloat)fontSize frame:(CGRect)frame {
    BrushCharacterModel *model = [[BrushCharacterModel alloc] initWithShape:shape text:text color:color font:fontSize frame:frame];
    return model;
}

- (NSString *)debugDescription {
    
    NSString *shape = nil;
    switch (self.shape) {
        case ShapeType_Freeform:
            shape = @"SFreeform";
            break;
        case ShapeType_StraightLine:
            shape = @"StraightLine";
            break;
        case ShapeType_Square:
            shape = @"Square";
            break;
        case ShapeType_Elipse:
            shape = @"Ellipse";
            break;
        case ShapeType_Round:
            shape = @"Round";
            break;
        case ShapeType_Text:
            shape = @"Text";
            break;
        case ShapeType_Eraser:
            shape = @"Eraser";
            break;
            
        default:
            break;
    }
    
    NSString *debug = [NSString stringWithFormat:@"shape : %@, fontsize : %f, text: %@ \n", shape, self.fontSize, self.character];
    NSMutableString *point = [NSMutableString string];
    [self.frames enumerateObjectsUsingBlock:^(NSValue * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGRect frame = obj.CGRectValue;
        NSString *str = [NSString stringWithFormat:@"x : %f,  y : %f,  w : %f,  h : %f \n",frame.origin.x,frame.origin.y,frame.size.width,frame.size.height];
        [point appendString:str];
    }];
    
    return [NSString stringWithFormat:@"Debug",debug,point];
}

@end
