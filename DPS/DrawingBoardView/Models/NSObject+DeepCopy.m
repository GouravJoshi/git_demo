

#import "NSObject+DeepCopy.h"

@implementation NSObject (DeepCopy)

- (instancetype)rs_deepCopy {
    NSData *archiveForCopy = [NSKeyedArchiver archivedDataWithRootObject:self];
    NSObject *deepCoy = [NSKeyedUnarchiver unarchiveObjectWithData:archiveForCopy];
    return deepCoy;
}

@end
