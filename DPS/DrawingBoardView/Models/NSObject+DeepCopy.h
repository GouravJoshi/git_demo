
#import <Foundation/Foundation.h>

@interface NSObject (DeepCopy)
- (instancetype)rs_deepCopy;
@end
