

#import <Foundation/Foundation.h>
#import "DrawBrushProtocol.h"

@interface BrushCharacterModel : NSObject<DrawBrushProtocol, NSCopying, NSCoding>

@property (nonatomic, assign) ShapeType shape;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, copy) NSMutableArray<NSValue*> *frames;

@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, copy) NSString *character;

+ (instancetype)defaultWithShape:(ShapeType)shape text:(NSString *)text color:(UIColor *)color font:(CGFloat)fontSize frame:(CGRect)frame;
@end
