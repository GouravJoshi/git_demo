#import <Foundation/Foundation.h>
#import "BrushModel.h"


typedef NS_ENUM(NSUInteger, MenuType) {
    MenuType_DrawingFreeform    = 0 << 8 | ShapeType_Freeform,
    MenuType_DrawingStraightLine     = 1 << 8 | ShapeType_StraightLine,
    MenuType_DrawingSquare      = 2 << 8 | ShapeType_Square,
    MenuType_DrawingElipse      = 3 << 8 | ShapeType_Elipse ,
    MenuType_DrawingRound       = 4 << 8 | ShapeType_Round,
    MenuType_DrawingText        = 5 << 8 | ShapeType_Text,
    MenuType_Eraser             = 6 << 8 | ShapeType_Eraser,
    
    MenuType_Undo               = 8 << 8,
    MenuType_Redo               = 9 << 8,
    MenuType_Clear              = 10 << 8,
    MenuType_SelectColor              = 11 << 8,
    MenuType_Thickness              = 12 << 8,
    MenuType_SelectColorGraphLines              = 13 << 8,
    
    MenuType_Default            = MenuType_DrawingFreeform,
};


@interface CellDataSource : NSObject
@property (nonatomic, assign) MenuType type;
@property (nonatomic, copy) NSString *title;

+ (NSArray<CellDataSource *> *)dataSource;
@end
