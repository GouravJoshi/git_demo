

#import "GraphDrawingBoardViewController.h"
#import "DrawingBoardView.h"
#import "CellDataSource.h"

#import "ControlProtocol.h"
#import "DrawBrushProtocol.h"
#import "TopDrawingBoardView.h"

#import "BrushModel.h"
#import "BrushCharacterModel.h"
#import "DrawingBoardViewDelegate.h"
#import "AllImportsViewController.h"
#import <Foundation/Foundation.h>
#import "DPS-Swift.h"



@interface CellOptions : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *viewColorIndicator;
@property (weak, nonatomic) IBOutlet UILabel *lblValueIndicator;
@end

@implementation CellOptions

@end

@interface cellColorPick : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewColor;
@property (weak, nonatomic) IBOutlet UILabel *lblColorName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewDot_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewDot_W;

@end

@implementation cellColorPick

@end

@interface GraphDrawingBoardViewController ()<DrawingBoardViewDelegate,UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UITextViewDelegate>
{
    CGFloat thicknessGlobal;
    BOOL isSignImage,isFirstTime,isUploadedGraph;
    UIView *viewForSign;
    NSString *strSignImageName,*strImagePathToEdit,*strGraphImageName,*strGraphCaption,*strGraphDescription;
    NSData *signImageData;
    BOOL image, isColorPickerForGraphLines;
    UITableView *tblData;
    NSMutableArray *arrDataTblView,*arrOfColor;
    UIView *viewForDate;
    UIView *viewBackGround;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    Global *global;
    int indexSelected;
    NSArray *aryColor;
    NSArray *aryColorName;
    NSMutableArray *arrThickness;
    
}


@property (strong, nonatomic) IBOutlet DrawingBoardView *drawingBoardView;
@property (strong, nonatomic) IBOutlet TopDrawingBoardView *topDrawingBoardView;
@property (nonatomic, strong) NSArray<CellDataSource *> *controlList;
@property (nonatomic, strong) UIColor *brushColor;

@end

@implementation GraphDrawingBoardViewController
{
    UIColor *_color;
    UIColor *_GraphLineColor;
    int sectionIndex;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _height_ViewDetail.constant = 0.0;
    indexSelected=0;
    
    NSUserDefaults *defs1 = [NSUserDefaults standardUserDefaults];
    
    BOOL isDrawGraphLines=[defs1 boolForKey:@"drawgraphlines"];
    
    if (isDrawGraphLines) {
        
        [_switchGraphLines setOn:YES];
        
    } else {
        
        [_switchGraphLines setOn:NO];
        
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSString *strThickness=[defs valueForKey:@"thickness"];
    
    
    NSData *theData=[[NSUserDefaults standardUserDefaults] dataForKey:@"colorGraph"];
    
    if (theData != nil)
    {
        _color =(UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData];
        
    }else{
        _color = UIColor.blackColor;
        NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_color];
        [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraph"];
    }
    
    
    NSData *theData1=[[NSUserDefaults standardUserDefaults] dataForKey:@"colorGraphLines"];
    if (theData != nil)
    {
        _GraphLineColor =(UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData1];
        
    }else{
        _GraphLineColor = UIColor.blackColor;
        NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_GraphLineColor];
        [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraphLines"];
        
    }
    
    
    
    
    if (strThickness.length==0) {
        
        thicknessGlobal=3;
        
    } else {
        
        thicknessGlobal=[strThickness floatValue];
        
    }
    self.controlList = [CellDataSource dataSource];;
    self.drawingBoardView.hidden = NO;
    [self.drawingBoardView removeFromSuperview];
    self.brushColor = [UIColor blackColor];
    if (_color==nil) {
        self.brushColor = [UIColor blackColor];
    } else {
        self.brushColor = _color;
    }
    
    id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
    self.topDrawingBoardView.brushModel = brush;
    self.topDrawingBoardView.delegate = self;
    self.topDrawingBoardView.drawEnable = YES;
    self.drawingBoardView.backgroundColor=[UIColor clearColor];
    self.topDrawingBoardView.backgroundColor=[UIColor clearColor];
    
    isFirstTime=NO;
    isUploadedGraph=NO;
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsImageName=[NSUserDefaults standardUserDefaults];
    
    isFirstTime=[defsImageName boolForKey:@"firstGraphImage"];
    
    // Akshay 13 may 2019 ////
    bool isFromTermiteTexas = [defsImageName boolForKey:@"isFromTermiteTexas"];//user came from termite texas view
    ////////////isFromTermiteTexasInvoice
    
    // Akshay 14 may 2019 ////
    bool isFromTermiteTexasInvoice = [defsImageName boolForKey:@"isForNewGraph"];//user came from termite texas Invoice View
    ////////////
    
    
    if (isFirstTime) {
        
        [defsImageName setBool:NO forKey:@"firstGraphImage"];
        [defsImageName synchronize];
        
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        
        // strImagePathToEdit = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
        
        // strImagePathToEdit = [NSString stringWithFormat:@"graphImg%@%@.jpg",strDate,strTime];
        //Nilind
        //strImagePathToEdit = [NSString stringWithFormat:@"graphImg%@%@%@.jpg",strDate,strTime,_strLeadId];
        strImagePathToEdit = [NSString stringWithFormat:@"graphImg%@_%@%@.jpg",_strLeadId,strDate,strTime];

        // Akshay 13 may 2019 /////
        if(isFromTermiteTexas)
        {
            //strImagePathToEdit = [NSString stringWithFormat:@"\\Documents\\UploadImages\\graphImg%@%@.jpg",strDate,strTime];
            strImagePathToEdit = [NSString stringWithFormat:@"\\Documents\\UploadImages\\graphImg%@%@%@.jpg",strDate,strTime,_strLeadId];
            
            [defsImageName setBool:NO forKey:@"isFromTermiteTexas"];
            [defsImageName synchronize];
        }
        else if(isFromTermiteTexasInvoice)
        {
            // strImagePathToEdit = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];;
            strImagePathToEdit = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@%@.jpg",strDate,strTime,_strLeadId];
            
            [defsImageName setBool:NO forKey:@"isForNewGraph"];
            [defsImageName synchronize];
        }
        ////////////////////
        _imageToEdit=[UIImage imageNamed:@"graph-paper.png"];
        
    } else {
        
        strImagePathToEdit=[defsImageName valueForKey:@"editImageGraph"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImagePathToEdit]];
        UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];
        _imageToEdit=imageEdit;
    }
    global=[[Global alloc]init];
    
    aryColor = [[NSArray alloc]initWithObjects:[UIColor redColor],[UIColor blackColor],[UIColor blueColor],[UIColor yellowColor],[UIColor purpleColor],[UIColor greenColor],[UIColor whiteColor],[UIColor grayColor],[UIColor orangeColor], nil];
    aryColorName = [[NSArray alloc]initWithObjects:@"Red",@"Black",@"Blue",@"Yellow",@"Purple",@"Green",@"White",@"Gray",@"Orange",nil];
    
    // [self CreatMenuGraph];// commented by akshay on 3 oct 2019
    [_collectionColorPick setHidden:true];
    
    
    [_tblviewOptions reloadData];
    
    // Navin 14 oct 2019 ///
    // For iPhone
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        [self configureIndicatorViews];
    }
    
    ///////
}
- (void)viewWillAppear:(BOOL)animated
{
 
    NSTimeInterval delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        LegendGraphView *obj = [[LegendGraphView alloc] init];
        [obj CreatFormLegendWithViewLegend:_viewGraphLegend controllor:self OnResultBlock:^(UIView * viewContain) {
            int count = (int)[viewContain tag];
            _heightGraphLegend.constant = 150.0;
            _heightScrollGraphLegend.constant = count + 50;
            
        }];
  
        
    });
    
  
}

// MARK: CreatMenuGraph
// MARK:
-(void)CreatMenuGraph{
    
    CGRect buttonFrame = CGRectMake(5.0f, 10.0f, 130, 35.0f);
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        buttonFrame = CGRectMake(5.0f, 5.0f, 150, 40.0f);
    }
    
    for (int index = 0; index <_controlList.count; index++) {
        UIButton *button = [[UIButton alloc]initWithFrame:buttonFrame];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setTag:index];
        
        NSString *title = self.controlList[index].title;
        [button setTitle:title forState:UIControlStateNormal];
        button.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        button.layer.borderWidth = 1.0;
        UIColor*colrBG = [UIColor groupTableViewBackgroundColor];
        button.backgroundColor = colrBG;
        
        [button setTitleColor:[UIColor colorWithRed:95.0 / 255 green:178.0 / 255 blue:175.0 / 255 alpha:1] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:18];
        button.layer.cornerRadius = 8.0;
        
        [self.scrollContain addSubview:button];
        buttonFrame.origin.x+=buttonFrame.size.width+5.0f;
        
    }
    
    CGSize contentSize = self.scrollContain.frame.size;
    contentSize.width = buttonFrame.origin.x;
    [self.scrollContain setContentSize:contentSize];
    
}

-(void)buttonPressed:(UIButton *)button
{
    indexSelected = (int)button.tag;
    if (button.tag==10) {
        _collectionColorPick.tag = 1;
        [_collectionColorPick setHidden:false];
        
        //        isColorPickerForGraphLines=NO;
        //        [self openColorPickerView];
        
        [_collectionColorPick reloadData];
        
    } else if (button.tag==11){
        
        //[self openThicknessInputAlert];

        arrThickness = [[NSMutableArray alloc] init];
        
        for (int k=0; k<9; k++) {
            
            NSDictionary *dictData = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",k+1],@"Name", nil];
            
            [arrThickness addObject:dictData];
            
        }
        
        _collectionColorPick.tag = 2;
        [_collectionColorPick setHidden:false];
        
        [_collectionColorPick reloadData];
        
    } else if (button.tag==12){

        _collectionColorPick.tag = 0;
        [_collectionColorPick setHidden:false];
        
        //        isColorPickerForGraphLines=YES;
        //        [self openColorPickerView];
        //
        
        [_collectionColorPick reloadData];
        
    }else {

        NSString *title = self.controlList[button.tag].title;
        NSLog(@"%@", title);
        MenuType type = self.controlList[button.tag].type;
        
        if ([title caseInsensitiveCompare:@"Redo"]==NSOrderedSame||[title caseInsensitiveCompare:@"undo"]==NSOrderedSame||[title caseInsensitiveCompare:@"clear"]==NSOrderedSame)
        {
            /*
            id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            self.topDrawingBoardView.brushModel = brush;
            self.topDrawingBoardView.delegate = self;
            self.topDrawingBoardView.drawEnable = NO;
             */
        }
        else
        {
            id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            self.topDrawingBoardView.brushModel = brush;
            self.topDrawingBoardView.delegate = self;
            self.topDrawingBoardView.drawEnable = YES;
        }
        
        [self operatingWithMenuType:type];
    }
    
}

// MARK: CollectionView Delegate
// MARK:
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (_collectionColorPick.tag==2) {
        
        return [arrThickness count];
        
    }else{
        
        return [aryColor count];
        
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cellColorPick *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellColorPick" forIndexPath:indexPath];
    
    if (_collectionColorPick.tag==2)
    {
        
        NSDictionary *dictData =arrThickness[indexPath.row];
        NSString *strString=[NSString stringWithFormat:@"%@ Point",[dictData valueForKey:@"Name"]];
        
        cell.lblColorName.text = strString;
        cell.lblColorName.textColor = UIColor.themeColor;
        cell.viewColor.backgroundColor = UIColor.blackColor;
        cell.const_ViewDot_H.constant = [strString intValue];
        cell.const_ViewDot_W.constant = [strString intValue];
        cell.viewColor.layer.cornerRadius = [strString intValue]/2;
        
    }
    else
    {
        
        cell.lblColorName.text = [aryColorName objectAtIndex:indexPath.row];
        cell.viewColor.backgroundColor = [aryColor objectAtIndex:indexPath.row];
        
        if ([cell.lblColorName.text isEqualToString:@"White"]) {
            
            cell.lblColorName.textColor = UIColor.blackColor;
            
        } else {
            
            cell.lblColorName.textColor = [aryColor objectAtIndex:indexPath.row];
            
        }
        
        cell.const_ViewDot_H.constant = 20;
        cell.const_ViewDot_W.constant = 20;
        cell.viewColor.layer.cornerRadius = 10;
        cell.viewColor.layer.borderColor = UIColor.groupTableViewBackgroundColor.CGColor;
        cell.viewColor.layer.borderWidth = 1.0;
        
    }
    
    return cell;
}
//
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return CGSizeMake(180, 40);
        
    }else{
        return CGSizeMake(110, 30);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _height_ViewDetail.constant = 0.0;

    if (_collectionColorPick.tag==2) {
        
        NSDictionary *dictData =arrThickness[indexPath.row];
        
        NSString *strString=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:strString forKey:@"thickness"];
        [defs synchronize];
        thicknessGlobal=[strString floatValue];
        
        id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
        self.topDrawingBoardView.brushModel = brush;
        
    }else{
        
        
        if (_collectionColorPick.tag==0) { // For GraphLines
            _GraphLineColor = [aryColor objectAtIndex:indexPath.row];
            NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_GraphLineColor];
            [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraphLines"];
            
        } else { // For Draw Graph
            _color = [aryColor objectAtIndex:indexPath.row];
            self.brushColor=_color;
            id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
            self.topDrawingBoardView.brushModel = brush;
            NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_color];
            [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraph"];
        }
        
    }
    
    [_collectionColorPick setHidden:true];
    [self callMethod];
    [_tblviewOptions reloadData];
    
    // Navin 14 oct 2019 ///
    // For iPhone
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        [self configureIndicatorViews];
    }
    
}


#pragma mark -- DrawingBoardViewDelegate

- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didBeganPoint:(CGPoint)point {
    NSLog(@"Begin");
    _height_ViewDetail.constant = 0.0;

}

- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didMovedPoint:(CGPoint)point {
    _height_ViewDetail.constant = 0.0;

}

- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didEndedPoint:(CGPoint)point brush:(id<DrawBrushProtocol>)brush {
    NSLog(@"End");
    _height_ViewDetail.constant = 0.0;

}


- (void)operatingWithMenuType:(MenuType)type{
    if (MenuType_DrawingFreeform <= type && type <= MenuType_Eraser) {
        
        id<DrawBrushProtocol> brush = [self genBrushWithMenuType:type];
        self.topDrawingBoardView.brushModel = brush;
        
    }
    
    if (MenuType_Undo <= type && type <= MenuType_Clear ) {
        
        //self.topDrawingBoardView.brushModel = nil;
        
        switch (type) {
            case MenuType_Undo:
                [self.topDrawingBoardView undo:YES];
                break;
                
            case MenuType_Redo:
                
                [self.topDrawingBoardView redo];
                
                break;
                
            case MenuType_Clear:
                [self.topDrawingBoardView clear:YES];
                break;
                
            default:
                break;
        }
    }
}

-(void)callMethod{
    
    id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
    self.topDrawingBoardView.brushModel = brush;
    self.topDrawingBoardView.delegate = self;
    self.topDrawingBoardView.drawEnable = YES;
    
}

- (id<DrawBrushProtocol>)genBrushWithMenuType:(MenuType)menuType {
    id<DrawBrushProtocol> brush = nil;
    switch (menuType) {
        case MenuType_DrawingFreeform:
            brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingStraightLine:
            brush = [BrushModel brushWithShape:ShapeType_StraightLine color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingSquare:
            brush = [BrushModel brushWithShape:ShapeType_Square color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingElipse:
            brush = [BrushModel brushWithShape:ShapeType_Elipse color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingRound:
            brush = [BrushModel brushWithShape:ShapeType_Round color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case MenuType_DrawingText: {
            brush = [BrushCharacterModel defaultWithShape:ShapeType_Text text:@"" color:self.brushColor font:36.f frame:self.view.frame];
        }
            break;
            
        case MenuType_Eraser:
            brush = [BrushModel brushWithShape:ShapeType_Freeform color:[UIColor whiteColor] thickness:thicknessGlobal];
            break;
            
        default:
            break;
    }
    self.topDrawingBoardView.brushModel = brush;
    return brush;
}


- (IBAction)action_Cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)action_Save:(id)sender {
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSettingSales"];
    
    if (yesImageCaption) {
        
        [self alertViewCustom];
        
    } else {
        
        strGraphCaption=@"No Caption Available..!!";
        strGraphDescription=@"No Description Available..!!";
        
        strGraphCaption=@"";
        strGraphDescription=@"";
        
        [self saveGraphImage];
    }
    
}


/*
 -(void)saveGraphImage{
 
 
 
 CGRect rect = CGRectMake(0, 0, _backgroundImageView.frame.size.width, _backgroundImageView.frame.size.height);
 UIGraphicsBeginImageContext(rect.size);
 CGContextRef context1 = UIGraphicsGetCurrentContext();
 [self.mainView.layer renderInContext:context1];
 UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
 UIGraphicsEndImageContext();
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImagePathToEdit]];
 signImageData = UIImagePNGRepresentation(screengrab);
 [signImageData writeToFile:path atomically:YES];
 
 if (isFirstTime) {
 
 
 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
 
 BOOL isServiceGraph=[defs boolForKey:@"servicegraph"];
 
 if (isServiceGraph) {
 
 [defs setBool:NO forKey:@"servicegraph"];
 [defs synchronize];
 
 [self saveGraphImageToCoreDataServiceAuto];
 
 } else {
 
 [self saveGraphImageToCoreData];
 
 }
 
 }
 [self dismissViewControllerAnimated:YES completion:nil];
 
 }
 */

-(void)saveGraphImage{
    
    CGRect rect = CGRectMake(0, 0, _backgroundImageView.frame.size.width, _backgroundImageView.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context1 = UIGraphicsGetCurrentContext();
    [self.mainView.layer renderInContext:context1];
    UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImagePathToEdit]];
    signImageData = UIImagePNGRepresentation(screengrab);
    [signImageData writeToFile:path atomically:YES];
    
    if (isFirstTime) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        BOOL isServiceGraph=[defs boolForKey:@"servicegraph"];
        
        BOOL isTermiteGraph=[defs boolForKey:@"termitegraph"];// Akshay 13 may 2019
        
        
        if (isServiceGraph) {
            
            [defs setBool:NO forKey:@"servicegraph"];
            [defs synchronize];
            
            [self saveGraphImageToCoreDataServiceAuto];
            
        }
        // Akshay 13 may 2019 ////
        else if(isTermiteGraph)
        {
            [defs setBool:NO forKey:@"termitegraph"];
            [defs synchronize];
            // Yaha Termite Graph Save Hoga
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:strImagePathToEdit forKey:@"graphImagePathTermite"];
            [defs setValue:strGraphCaption forKey:@"graphImageCaption"];
            [defs setValue:strGraphDescription forKey:@"graphImageDescription"];
            [defs setValue:@"strAddGraphImg" forKey:@"strAddGraphImg"];
            [defs synchronize];
        }
        /////////////
        
        
        else {
            
            [self saveGraphImageToCoreData];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)saveGraphImageToCoreData{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    
    ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
    
    objImageDetail.createdBy=@"";
    objImageDetail.createdDate=@"";
    objImageDetail.descriptionImageDetail=strGraphDescription;
    objImageDetail.leadImageCaption=strGraphCaption;
    objImageDetail.leadImageId=[NSString stringWithFormat:@"%@",strImagePathToEdit];
    objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",strImagePathToEdit];
    objImageDetail.leadImageType=@"Graph";
    objImageDetail.modifiedBy=@"";
    objImageDetail.modifiedDate=[global modifyDate];
    objImageDetail.leadId=_strLeadId;
    objImageDetail.userName=_strUserName;
    objImageDetail.companyKey=_strCompanyKey;
    objImageDetail.isAddendum = @"false";
    objImageDetail.isImageSyncforMobile = @"false";
    objImageDetail.isInternalUsage = @"false";
    //Lat long code
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objImageDetail.latitude=latitude;
    objImageDetail.longitude=longitude;
    
    NSError *error1;
    [context save:&error1];
    
    [global updateSalesModifydate:_strLeadId];

}

-(void)saveGraphImageToCoreDataServiceAuto{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    
    ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
    
    objImageDetail.createdBy=@"";
    objImageDetail.createdDate=@"";
    objImageDetail.imageDescription=strGraphDescription;
    objImageDetail.imageCaption=strGraphCaption;
    objImageDetail.woImageId=@"";
    objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",strImagePathToEdit];
    objImageDetail.woImageType=@"Graph";
    objImageDetail.modifiedBy=@"";
    objImageDetail.modifiedDate=[global modifyDate];
    objImageDetail.workorderId=_strLeadId;
    objImageDetail.userName=_strUserName;
    objImageDetail.companyKey=_strCompanyKey;
    
    //Lat long code
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objImageDetail.latitude=latitude;
    objImageDetail.longitude=longitude;
    NSError *error1;
    [context save:&error1];
    
    if ([_strModuleType isEqualToString:@"WdoSalesServiceFlow"]) {
        
        NSString *strStatusLead = [global fetchLeadStatus:_strWdoLeadId];
        
        if ([strStatusLead isEqualToString:@"complete"]) {
            
        }else{
            
            [self saveGraphImageToCoreDataForWdoSales];
            
        }
        
    }
    
    NSString *strCurrentDateFormatted = [global modifyDateService];
    [global fetchWorkOrderFromDataBaseForServiceToUpdateModifydate:_strWdoLeadId :strCurrentDateFormatted];
    [global fetchServicePestWoToUpdateModifyDate:_strWdoLeadId :strCurrentDateFormatted];
    [global fetchServicePestWoToUpdateModifyDateJugad:_strWdoLeadId];
    
}

-(void)saveGraphImageToCoreDataForWdoSales{
    

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    
    ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
    
    objImageDetail.createdBy=@"";
    objImageDetail.createdDate=@"";
    objImageDetail.descriptionImageDetail=strGraphDescription;
    objImageDetail.leadImageCaption=strGraphCaption;
    objImageDetail.leadImageId=@"";
    
    NSMutableString *strTempImagePath = [NSMutableString stringWithString:strImagePathToEdit];
    
    NSString *strImageNameSales = [strTempImagePath stringByReplacingOccurrencesOfString:@"\\Documents\\UploadImages\\" withString:@""];
    
    objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",strImageNameSales];
    objImageDetail.leadImageType=@"Graph";
    objImageDetail.modifiedBy=@"";
    objImageDetail.modifiedDate=[global modifyDate];
    objImageDetail.leadId=_strWdoLeadId;
    objImageDetail.userName=_strUserName;
    objImageDetail.companyKey=_strCompanyKey;
    
    //Lat long code
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objImageDetail.latitude=latitude;
    objImageDetail.longitude=longitude;
    
    NSError *error1;
    [context save:&error1];
    
    CGRect rect = CGRectMake(0, 0, _backgroundImageView.frame.size.width, _backgroundImageView.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context1 = UIGraphicsGetCurrentContext();
    [self.mainView.layer renderInContext:context1];
    UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImageNameSales]];
    signImageData = UIImagePNGRepresentation(screengrab);
    [signImageData writeToFile:path atomically:YES];
    
}

-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter graph caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter graph description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel & Save Graph" forState:UIControlStateNormal];
    btnDone.titleLabel.numberOfLines=2;
    btnDone.titleLabel.textAlignment=NSTextAlignmentCenter;
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            strGraphCaption=txtFieldCaption.text;
            
        } else {
            
            strGraphCaption=@"No Caption Available..!!";
            strGraphCaption=@"";
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            strGraphDescription=txtViewImageDescription.text;
            
        } else {
            
            strGraphDescription=@"No Description Available..!!";
            strGraphDescription=@"";
            
        }
        
        [viewBackAlertt removeFromSuperview];
        
        [self saveGraphImage];
        
    } else {
        
        [self AlertViewForImageCaption];
        
    }
    
}

-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    strGraphCaption=@"No Caption Available..!!";
    strGraphDescription=@"No Description Available..!!";
    
    strGraphCaption=@"";
    strGraphDescription=@"";
    
    [self saveGraphImage];
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TextView Delegates METHOD-----------------
//============================================================================
//============================================================================


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    BOOL  isReturnTrue;
    
    isReturnTrue=YES;
    
    if (!isNumeric) {
        
        if ([string isEqualToString:@""]) {
            
            isReturnTrue=YES;
            
        }else
            
            isReturnTrue=NO;
        
    }
    
    if (!isReturnTrue) {
        
        return NO;
        
    }
    
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex==0) {
        
        [self openCamera];
        
    } else {
        
        [self openGallery];
        
    }
}


- (IBAction)action_CancelColor:(id)sender {
    
    [_colorPickerViewMain removeFromSuperview];
    
}

- (IBAction)action_SaveColor:(id)sender {
    
    self.brushColor=_color;
    [_colorPickerViewMain removeFromSuperview];
    
    id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
    self.topDrawingBoardView.brushModel = brush;
    
    NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_color];
    
    if (isColorPickerForGraphLines) {
        
        [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraphLines"];
        
    } else {
        
        [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraph"];
        
    }
    
    
}
-(void)openThicknessInputAlert{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Thickness"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Thickness";
        textField.textColor = [UIColor blueColor];
        textField.delegate=self;
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        NSString *strThickness=[defs valueForKey:@"thickness"];
        
        if (strThickness.length==0) {
            
            textField.text=@"3";
            
        } else {
            
            textField.text=strThickness;
            
        }
        
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeNumberPad;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        int textDays=[txtHistoricalDays.text intValue];
        if ((textDays>=1) && (textDays<=100)) {
            
            NSString *strString=[NSString stringWithFormat:@"%@",txtHistoricalDays.text];
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:strString forKey:@"thickness"];
            [defs synchronize];
            thicknessGlobal=[strString floatValue];
            
            id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
            self.topDrawingBoardView.brushModel = brush;
            
            //  [self configureIndicatorViews]; // akshay 13 Oct 2019
            
        } else {
            [self performSelector:@selector(alertViewForThickness) withObject:nil afterDelay:0.2];
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)alertViewForThickness{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter thickness between 1 to 100" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)action_UploadGraphImage:(id)sender {
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Choose option" message:@"" delegate:self cancelButtonTitle:@"Camera" otherButtonTitles:@"Gallery", nil];
    [alert show];
    
}

-(void)openGallery{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

-(void)openCamera{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [self resizeImage:chosenImage :@"UploadedGraphImage"];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",@"UploadedGraphImage"]];
    UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];
    
    _imageToEdit=imageEdit;
    
    _backgroundImageView.image = _imageToEdit;
    
    isUploadedGraph=YES;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [self.topDrawingBoardView clear:YES];
}

-(void)SetGraphImageBack{
    
    _backgroundImageView.image = _imageToEdit;
    
}

-(UIImage *)resizeImage:(UIImage *)image1 :(NSString*)imageName
{
    float actualHeight = image1.size.height;
    float actualWidth = image1.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image1 drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    // NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

- (id<DrawBrushProtocol>)genBrushWithMenuTypeNew:(int)menuType {
    
    id<DrawBrushProtocol> brush = nil;
    switch (menuType) {
        case 0:
            brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 1:
            brush = [BrushModel brushWithShape:ShapeType_StraightLine color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 2:
            brush = [BrushModel brushWithShape:ShapeType_Square color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 3:
            brush = [BrushModel brushWithShape:ShapeType_Elipse color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 4:
            brush = [BrushModel brushWithShape:ShapeType_Round color:self.brushColor thickness:thicknessGlobal];
            break;
            
        case 5: {
            brush = [BrushCharacterModel defaultWithShape:ShapeType_Text text:@"" color:self.brushColor font:36.f frame:self.view.frame];
        }
            break;
            
        case 6:
            brush = [BrushModel brushWithShape:ShapeType_Freeform color:UIColor.whiteColor thickness:thicknessGlobal];
            break;
            
        default:
            break;
    }
    self.topDrawingBoardView.brushModel = brush;
    return brush;
}

- (IBAction)action_SwitchGraphLines:(id)sender {
    
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    if ([sender isOn]) {
        
        [defs setBool:YES forKey:@"drawgraphlines"];
        [defs synchronize];
        
    } else {
        
        [defs setBool:NO forKey:@"drawgraphlines"];
        [defs synchronize];
        
    }
    
}

- (IBAction)action_SetDefaultBlackColor:(id)sender {
    
    self.brushColor = [UIColor blackColor];
    _color = [UIColor blackColor];
    [_colorPickerViewMain removeFromSuperview];
    
    id<DrawBrushProtocol> brush = [self genBrushWithMenuType:indexSelected];
    self.topDrawingBoardView.brushModel = brush;
    
    NSData *theData=[NSKeyedArchiver archivedDataWithRootObject:_color];
    
    if (isColorPickerForGraphLines) {
        
        [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraphLines"];
        
    } else {
        
        [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"colorGraph"];
        
    }
    
}

//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self.topDrawingBoardView touchesBegan:touches withEvent:event];
//    [self.drawingBoardView touchesBegan:touches withEvent:event];
//}

// Akshay 24 may 2019
/*
 - (IBAction)actionOnSelectOptions:(id)sender {
 
 //    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
 //                            @"FreeForm",
 //                            @"Straight Line",
 //                            @"Rectangle",
 //                            @"Oval",
 //                            @"Circle",
 //                            @"Text",
 //                            @"Eraser",
 //                            @"Undo",
 //                            @"Redo",
 //                            @"Clear",
 //                            @"Select color",
 //                            @"Thickness",
 //                            @"Select color for graph lines",
 //                            nil];
 
 
 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
 
 
 UIAlertAction *actionFreeForm = [UIAlertAction actionWithTitle:@"FreeForm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 id<DrawBrushProtocol> brush = nil;
 
 brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
 self.topDrawingBoardView.brushModel = brush;
 // [self dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 UIAlertAction *actionStraightLine = [UIAlertAction actionWithTitle:@"Straight Line" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 id<DrawBrushProtocol> brush = nil;
 brush = [BrushModel brushWithShape:ShapeType_StraightLine color:self.brushColor thickness:thicknessGlobal];
 self.topDrawingBoardView.brushModel = brush;
 // [self dismissViewControllerAnimated:YES completion:nil];
 }];
 
 UIAlertAction *actionRectangle = [UIAlertAction actionWithTitle:@"Rectangle" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 id<DrawBrushProtocol> brush = nil;
 brush = [BrushModel brushWithShape:ShapeType_Square color:self.brushColor thickness:thicknessGlobal];
 self.topDrawingBoardView.brushModel = brush;
 // [self dismissViewControllerAnimated:YES completion:nil];
 }];
 
 UIAlertAction *actionOval = [UIAlertAction actionWithTitle:@"Oval" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 id<DrawBrushProtocol> brush = nil;
 brush = [BrushModel brushWithShape:ShapeType_Elipse color:self.brushColor thickness:thicknessGlobal];
 self.topDrawingBoardView.brushModel = brush;
 // [self dismissViewControllerAnimated:YES completion:nil];
 }];
 
 UIAlertAction *actionCircle = [UIAlertAction actionWithTitle:@"Circle" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 id<DrawBrushProtocol> brush = nil;
 brush = [BrushModel brushWithShape:ShapeType_Round color:self.brushColor thickness:thicknessGlobal];
 self.topDrawingBoardView.brushModel = brush;
 // [self dismissViewControllerAnimated:YES completion:nil];
 }];
 
 UIAlertAction *actionText = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 id<DrawBrushProtocol> brush = nil;
 brush = [BrushCharacterModel defaultWithShape:ShapeType_Text text:@"" color:self.brushColor font:36.f frame:self.view.frame];
 self.topDrawingBoardView.brushModel = brush;
 // [self dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 UIAlertAction *actionEraser = [UIAlertAction actionWithTitle:@"Eraser" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 id<DrawBrushProtocol> brush = nil;
 brush = [BrushModel brushWithShape:ShapeType_Freeform color:UIColor.whiteColor thickness:thicknessGlobal];
 self.topDrawingBoardView.brushModel = brush;
 // [self dismissViewControllerAnimated:YES completion:nil];
 }];
 
 UIAlertAction *actionUndo = [UIAlertAction actionWithTitle:@"Undo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 [self.topDrawingBoardView undo:YES];
 // [self dismissViewControllerAnimated:YES completion:nil];
 }];
 
 UIAlertAction *actionRedo = [UIAlertAction actionWithTitle:@"Redo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 [self.topDrawingBoardView redo];
 // [self dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 UIAlertAction *actionClear = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 [self.topDrawingBoardView clear:YES];
 // [self dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 UIAlertAction *actionSelectColor = [UIAlertAction actionWithTitle:@"Select color" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 [self openColorPickerView];
 // [self dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 UIAlertAction *actionThickness = [UIAlertAction actionWithTitle:@"Thickness color" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 [self openThicknessInputAlert];
 // [self dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 UIAlertAction *actionSelectColorforGraphLines = [UIAlertAction actionWithTitle:@"Select color for graph lines" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
 [self openColorPickerView];
 //  [self dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
 
 
 // [self dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 [alertController addAction:actionFreeForm];
 [alertController addAction:actionStraightLine];
 [alertController addAction:actionRectangle];
 [alertController addAction:actionOval];
 [alertController addAction:actionCircle];
 [alertController addAction:actionText];
 [alertController addAction:actionEraser];
 [alertController addAction:actionUndo];
 [alertController addAction:actionRedo];
 [alertController addAction:actionClear];
 [alertController addAction:actionSelectColor];
 [alertController addAction:actionThickness];
 [alertController addAction:actionSelectColorforGraphLines];
 [alertController addAction:actionCancel];
 [self presentViewController:alertController animated:YES completion:nil];
 
 }*/

// akshay 13 oct 2019
#pragma mark:- UITableView's delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _controlList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 38.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"CellOptions";
    
    CellOptions *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[CellOptions alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.lblTitle.text = self.controlList[indexPath.row].title;
    cell.lblValueIndicator.hidden = YES;
    cell.viewColorIndicator.hidden = YES;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        //  cell.lblTitle.font=[UIFont systemFontOfSize:17];
        cell.lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f];
    }
    else
    {
        // cell.lblTitle.font=[UIFont systemFontOfSize:15];
        cell.lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
    }
    
    if(indexPath.row == 10 || indexPath.row == 12)
    {
        cell.viewColorIndicator.hidden = NO;
        if(indexPath.row == 12){
            
            NSData *theData=[[NSUserDefaults standardUserDefaults] dataForKey:@"colorGraphLines"];
            
            if (theData != nil)
            {
                cell.viewColorIndicator.backgroundColor =(UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData];
            }
            
        }else{
            
            NSData *theData=[[NSUserDefaults standardUserDefaults] dataForKey:@"colorGraph"];
            
            if (theData != nil)
            {
                cell.viewColorIndicator.backgroundColor =(UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData];
            }
            
            
            // cell.viewColorIndicator.backgroundColor = _color;
        }
        cell.viewColorIndicator.layer.cornerRadius = cell.viewColorIndicator.frame.size.height/2;
        cell.viewColorIndicator.layer.masksToBounds = YES;
    }else{
        cell.viewColorIndicator.hidden = YES;
    }
    
    if(indexPath.row == 11)
    {
        cell.lblValueIndicator.text = [NSString stringWithFormat:@"%d pts",(int)thicknessGlobal];
        cell.lblValueIndicator.hidden = NO;
    }
    
    if((int)indexPath.row == sectionIndex){
        cell.contentView.backgroundColor = UIColor.groupTableViewBackgroundColor;
        
    }else{
        cell.contentView.backgroundColor = UIColor.whiteColor;
        
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    sectionIndex = (int)indexPath.row;
    [tableView reloadData];
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    _height_ViewDetail.constant = 0.0;

    if (indexPath.row==10) {
        _height_ViewDetail.constant = 135.0;
        _collectionColorPick.tag = 1;
        [_collectionColorPick setHidden:false];
        
        //        isColorPickerForGraphLines=NO;
        //        [self openColorPickerView];
        
        [_collectionColorPick reloadData];
        
    } else if (indexPath.row==11){
        
        //[self openThicknessInputAlert];
        _height_ViewDetail.constant = 135.0;
        arrThickness = [[NSMutableArray alloc] init];
        
        for (int k=0; k<9; k++) {
            
            NSDictionary *dictData = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",k+1],@"Name", nil];
            
            [arrThickness addObject:dictData];
            
        }
        
        _collectionColorPick.tag = 2;
        [_collectionColorPick setHidden:false];
        
        [_collectionColorPick reloadData];
        
    } else if (indexPath.row==12){
        _height_ViewDetail.constant = 135.0;
        _collectionColorPick.tag = 0;
        [_collectionColorPick setHidden:false];
        
        //        isColorPickerForGraphLines=YES;
        //        [self openColorPickerView];
        //
        
        [_collectionColorPick reloadData];
        
    }else {
        NSString *title = self.controlList[indexPath.row].title;
        NSLog(@"%@", title);
        MenuType type = self.controlList[indexPath.row].type;
        
        if ([title caseInsensitiveCompare:@"Redo"]==NSOrderedSame||[title caseInsensitiveCompare:@"undo"]==NSOrderedSame||[title caseInsensitiveCompare:@"clear"]==NSOrderedSame)
        {
            /*
            id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            self.topDrawingBoardView.brushModel = brush;
            self.topDrawingBoardView.delegate = self;
            self.topDrawingBoardView.drawEnable = NO;
             */
        }
        else
        {
            id<DrawBrushProtocol> brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
            // indexSelected = (int)indexPath.row;
            
            /*   id<DrawBrushProtocol> brush = [BrushModel brushWithShape:indexSelected color:self.brushColor thickness:thicknessGlobal];*/
            
            self.topDrawingBoardView.brushModel = brush;
            self.topDrawingBoardView.delegate = self;
            self.topDrawingBoardView.drawEnable = YES;
        }
        
        [self operatingWithMenuType:type];
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

#pragma mark -  Select Option action

- (IBAction)actionOnSelectOption:(UIButton *)sender {
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction *actionFreeForm = [UIAlertAction actionWithTitle:@"FreeForm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        id<DrawBrushProtocol> brush = nil;
        _height_ViewDetail.constant = 0.0;

        brush = [BrushModel brushWithShape:ShapeType_Freeform color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionStraightLine = [UIAlertAction actionWithTitle:@"Straight Line" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_StraightLine color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionRectangle = [UIAlertAction actionWithTitle:@"Rectangle" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_Square color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionOval = [UIAlertAction actionWithTitle:@"Oval" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_Elipse color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionCircle = [UIAlertAction actionWithTitle:@"Circle" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_Round color:self.brushColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionText = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        id<DrawBrushProtocol> brush = nil;
        brush = [BrushCharacterModel defaultWithShape:ShapeType_Text text:@"" color:self.brushColor font:36.f frame:self.view.frame];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
        _height_ViewDetail.constant = 0.0;

    }];
    
    UIAlertAction *actionEraser = [UIAlertAction actionWithTitle:@"Eraser" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        id<DrawBrushProtocol> brush = nil;
        brush = [BrushModel brushWithShape:ShapeType_Freeform color:UIColor.whiteColor thickness:thicknessGlobal];
        self.topDrawingBoardView.brushModel = brush;
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionUndo = [UIAlertAction actionWithTitle:@"Undo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        [self.topDrawingBoardView undo:YES];
        // [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *actionRedo = [UIAlertAction actionWithTitle:@"Redo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        [self.topDrawingBoardView redo];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionClear = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 0.0;

        [self.topDrawingBoardView clear:YES];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionSelectColor = [UIAlertAction actionWithTitle:@"Select color" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 135.0;
        _collectionColorPick.tag = 1;
        [_collectionColorPick setHidden:false];
        [_collectionColorPick reloadData];
        
        //   [self openColorPickerView];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionThickness = [UIAlertAction actionWithTitle:@"Thickness" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 135.0;

        arrThickness = [[NSMutableArray alloc] init];
        for (int k=0; k<9; k++) {
            NSDictionary *dictData = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",k+1],@"Name", nil];
            [arrThickness addObject:dictData];
        }
        _collectionColorPick.tag = 2;
        [_collectionColorPick setHidden:false];
        
        [_collectionColorPick reloadData];
        //  [self openThicknessInputAlert];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionSelectColorforGraphLines = [UIAlertAction actionWithTitle:@"Graph line color" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _height_ViewDetail.constant = 135.0;

        _collectionColorPick.tag = 0;
        [_collectionColorPick setHidden:false];
        [_collectionColorPick reloadData];
        //  [self openColorPickerView];
        //  [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertController addAction:actionFreeForm];
    [alertController addAction:actionStraightLine];
    [alertController addAction:actionRectangle];
    [alertController addAction:actionOval];
    [alertController addAction:actionCircle];
    [alertController addAction:actionText];
    [alertController addAction:actionEraser];
    [alertController addAction:actionUndo];
    [alertController addAction:actionRedo];
    [alertController addAction:actionClear];
    [alertController addAction:actionSelectColor];
    [alertController addAction:actionThickness];
    [alertController addAction:actionSelectColorforGraphLines];
    [alertController addAction:actionCancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}


-(void)configureIndicatorViews
{
    _viewSelectedGraphColor_iPhone.backgroundColor = _color;
    _viewSelectedGraphColor_iPhone.layer.cornerRadius = _viewSelectedGraphColor_iPhone.frame.size.height/2;
    _viewSelectedGraphColor_iPhone.layer.masksToBounds = YES;
    
    _viewSelectedGraphLineColor_iPhone.backgroundColor = _GraphLineColor;
    _viewSelectedGraphLineColor_iPhone.layer.cornerRadius = _viewSelectedGraphLineColor_iPhone.frame.size.height/2;
    _viewSelectedGraphLineColor_iPhone.layer.masksToBounds = YES;
    _lblValueThickness.text = [NSString stringWithFormat:@"Thickness %d pts", (int)thicknessGlobal];
}
@end
