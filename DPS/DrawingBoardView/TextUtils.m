
#import "TextUtils.h"
#import <AVFoundation/AVFoundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CoreMedia/CoreMedia.h>

@implementation TextUtils

#pragma mark -- NSString 

+ (CGSize)getContentSizeWithText:(nonnull NSString *)text font:(nonnull UIFont *)font size:(CGSize)maxSize {
    CGSize size;
    NSDictionary * fontDict  = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    size = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:fontDict context:nil].size;
    size.height += 8;
    size.width = size.width >= maxSize.width ? maxSize.width : size.width;
    return size;
}








@end
