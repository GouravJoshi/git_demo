

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"
@protocol EditImageViewDelegate <NSObject>
-(void)refresImage:(NSInteger)indexClicked withImage: (UIImage *)updatedImage;
@end

@interface DrawingBoardViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
- (IBAction)action_Cancel:(id)sender;
- (IBAction)action_Save:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *colorPickerViewMain;
- (IBAction)action_CancelColor:(id)sender;
- (IBAction)action_SaveColor:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionColorPick;
@property (strong, nonatomic) IBOutlet UISwitch *switchGraphLines;


@property (nonatomic, strong) UIColor* color;


@property (strong, nonatomic) IBOutlet UIView *viewSelectedGraphColor_iPhone;
@property (strong, nonatomic) IBOutlet UIView *viewSelectedGraphLineColor_iPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblValueThickness;
@property (strong, nonatomic) NSString *strModuleType;
@property (strong, nonatomic) NSString *strWdoLeadId;

@property (strong, nonatomic) IBOutlet UIImage *backgroundImage;
@property (nonatomic, weak) id<EditImageViewDelegate> delegate;
@property (nonatomic) NSInteger indexClicked;
@property (strong, nonatomic) IBOutlet UIView *viewGraphLegend;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightGraphLegend;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightScrollGraphLegend;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *height_ViewDetail;


@end

