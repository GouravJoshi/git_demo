

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class OUPoint;

NS_ASSUME_NONNULL_BEGIN

@interface TextUtils : NSObject
+ (CGSize)getContentSizeWithText:(nonnull NSString *)text font:(nonnull UIFont *)font size:(CGSize)maxSize;
@end
NS_ASSUME_NONNULL_END
