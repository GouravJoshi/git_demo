

#import <UIKit/UIKit.h>

@interface UIImage (Category)

+ (instancetype)rs_imageWithSize:(CGSize)size borderColor:(UIColor *)color borderWidth:(CGFloat)borderWidth;
- (UIImage *)rs_scaledWithSize:(CGSize)size;

@end
