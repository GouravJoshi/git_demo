

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"


@interface GraphDrawingBoardViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityTask;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entityCurrentService,
    *entitySoldServiceStandardDetail;
    
}
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
- (IBAction)action_Cancel:(id)sender;
- (IBAction)action_Save:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *colorPickerViewMain;
- (IBAction)action_CancelColor:(id)sender;
- (IBAction)action_SaveColor:(id)sender;
@property (strong, nonatomic) NSString *strLeadId;
@property (strong, nonatomic) NSString *strCompanyKey;
@property (strong, nonatomic) NSString *strUserName;
@property (strong, nonatomic) NSString *strModuleType;
@property (strong, nonatomic) NSString *strWdoLeadId;


@property (nonatomic, strong) UIColor* color;
- (IBAction)action_UploadGraphImage:(id)sender;
@property(weak,nonatomic)UIImage *imageToEdit;
@property (strong, nonatomic) IBOutlet UISwitch *switchGraphLines;


@property (strong, nonatomic) IBOutlet UIScrollView *scrollContain;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionColorPick;



- (IBAction)action_SwitchGraphLines:(id)sender;
- (IBAction)action_SetDefaultBlackColor:(id)sender;

// akshay 13 Oct 2019
@property (weak, nonatomic) IBOutlet UITableView *tblviewOptions;
@property (strong, nonatomic) IBOutlet UIView *viewSelectedGraphColor_iPhone;
@property (strong, nonatomic) IBOutlet UIView *viewSelectedGraphLineColor_iPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblValueThickness;

@property (strong, nonatomic) IBOutlet UIView *viewGraphLegend;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightGraphLegend;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightScrollGraphLegend;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *height_ViewDetail;


@end

