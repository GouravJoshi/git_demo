

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>

@protocol DrawingBoardOperatingProtocol,DrawBrushProtocol;

@protocol DrawingBoardProxyProtocol <DrawingBoardOperatingProtocol>

@property (nonatomic, strong, readonly) id<DrawBrushProtocol> currentBrush;
@property (nonatomic, strong, readonly) NSMutableArray<id<DrawBrushProtocol>> *brushes;

- (void)drawWithContext:(CGContextRef)context;

- (void)drawBeganWithPoint:(CGPoint)point brush:(id<DrawBrushProtocol>)brush;
- (void)drawMovedWithPoint:(CGPoint)point;
- (void)drawEndedWithPoint:(CGPoint)point;

- (void)drawPartWithBrush:(id<DrawBrushProtocol>)brush;

- (void)drawTextBeganWithFrame:(CGRect)frame brush:(id<DrawBrushProtocol>)brush;
- (void)drawTextChangedWithFrame:(CGRect)frame text:(NSString *)text;
- (void)drawTextEndedWithFrame:(CGRect)frame text:(NSString *)text;

@end
