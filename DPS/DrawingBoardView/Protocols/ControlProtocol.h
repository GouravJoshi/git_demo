

#import <Foundation/Foundation.h>

typedef void(^ControlCallback)(NSInteger type, id obj);

@protocol ControlProtocol <NSObject>

- (void)setType:(NSInteger)type;
- (void)setCallback:(ControlCallback)callback;

- (void)run;
- (void)stop;
@end
