
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "DrawShapeProtocol.h"

@protocol DrawBrushProtocol <NSObject>

@required
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) ShapeType shape;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, copy) NSMutableArray<NSValue*> *frames;

@optional
@property (nonatomic, assign) BOOL isFill;
@property (nonatomic, strong) UIColor *fillColor;


+ (instancetype)brushWithShape:(ShapeType)shape color:(UIColor *)color thickness:(CGFloat)width;
- (instancetype)deepCopy;
@end
