

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol DrawingBoardViewDelegate <NSObject>
- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didBeganPoint:(CGPoint)point;
- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didMovedPoint:(CGPoint)point;
- (void)drawingBoardView:(id<DrawingBoardViewProtocol>)boardView didEndedPoint:(CGPoint)point brush:(id<DrawBrushProtocol>)brush;
@end
