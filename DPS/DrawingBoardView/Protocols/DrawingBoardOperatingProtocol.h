

#import <Foundation/Foundation.h>

@protocol DrawingBoardOperatingProtocol <NSObject>


- (void)undo:(BOOL)canRedo;


- (void)redo;


- (void)clear:(BOOL)canUndo;
@end

