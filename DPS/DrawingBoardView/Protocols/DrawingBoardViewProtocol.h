

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol DrawingBoardViewDelegate,DrawingBoardOperatingProtocol,DrawBrushProtocol;

@protocol DrawingBoardViewProtocol <DrawingBoardOperatingProtocol>
@property (nonatomic, assign) BOOL drawEnable;
@property (nonatomic, strong) id<DrawBrushProtocol> brushModel;
@property (nonatomic, strong, readonly) NSArray<id<DrawBrushProtocol>> *brushes;
@property (nonatomic, weak) id<DrawingBoardViewDelegate> delegate;

- (void)restore;
- (void)drawPartWithBrush:(id<DrawBrushProtocol>)brush;

@end
