
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#define SHAPE_DISPLACEMENT (4)

typedef NS_ENUM(NSUInteger, ShapeType) {
    ShapeType_Freeform  = 0 << SHAPE_DISPLACEMENT,
    ShapeType_StraightLine   = 1 << SHAPE_DISPLACEMENT,
    ShapeType_Square    = 2 << SHAPE_DISPLACEMENT,
    ShapeType_Elipse    = 3 << SHAPE_DISPLACEMENT,
    ShapeType_Round     = 4 << SHAPE_DISPLACEMENT,
    ShapeType_Text      = 5 << SHAPE_DISPLACEMENT,
    ShapeType_Eraser    = 6 << SHAPE_DISPLACEMENT,
    ShapeType_Default   = ShapeType_Freeform,
};


@protocol DrawBrushProtocol;
@protocol DrawShapeProtocol <NSObject>
@required
- (void)drawContext:(CGContextRef)context brush:(id<DrawBrushProtocol>)brush;
@end
