

#import "DrawingBoardProxy.h"


#import "DrawingBoardOperatingProtocol.h"
#import "DrawingBoardProxyProtocol.h"
#import "BrushCharacterModel.h"


#import "Freeform.h"
#import "StraightLine.h"
#import "Rectangle.h"
#import "Circular.h"
#import "OvalShape.h"
#import "TextChars.h"
#import "Eraser.h"


#import "DrawBrushProtocol.h"

#import "DrawingBoardOperations.h"

@interface DrawingBoardProxy ()<DrawingBoardProxyProtocol>
@property (nonatomic, strong) NSMutableArray<id<DrawShapeProtocol>> *shapes;
@property (nonatomic, strong) id<DrawBrushProtocol> currentBrush;
@property (nonatomic, strong) DrawingBoardOperations *operate;
@end

@implementation DrawingBoardProxy

- (void)dealloc {
    NSLog(@"DrawingBoardProxy Release");
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _brushes = [NSMutableArray array];
        _operate = [DrawingBoardOperations new];
    }
    return self;
}


- (void)undo:(BOOL)canRedo {
    _brushes = _operate.undo.brushes;
}

- (void)redo {
    _brushes = _operate.redo.brushes;
}

- (void)clear:(BOOL)canUndo {
    if (!canUndo) {
        [_brushes removeAllObjects];
        return;
    }
    _brushes = _operate.clear.brushes;
}

#pragma mark --  DrawingBoardProxyProtocol

- (id<DrawBrushProtocol>)currentBrush {
    return _brushes.lastObject;
}

- (void)drawWithContext:(CGContextRef)context {
    [_brushes enumerateObjectsUsingBlock:^(id<DrawBrushProtocol>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        id<DrawShapeProtocol> shape = [self shapeRef:obj.shape];
        [shape drawContext:context brush:obj];
    }];
}

- (void)drawBeganWithPoint:(CGPoint)point brush:(id<DrawBrushProtocol>)brush {
    if (!brush || brush.shape == ShapeType_Text) return;
    [self drawWithBrush:brush];

    CGRect rect = CGRectMake(point.x, point.y, 0, 0);
    NSValue *value = [NSValue valueWithCGRect:rect];
    if (!_brushes.lastObject.frames) {
        _brushes.lastObject.frames = [NSMutableArray array];
    }
    [_brushes.lastObject.frames addObject:value];
}

- (void)drawMovedWithPoint:(CGPoint)point {
    if (_brushes.lastObject.shape == ShapeType_Text) return;
    CGRect rect = CGRectMake(point.x, point.y, 0, 0);
    NSValue *value = [NSValue valueWithCGRect:rect];
    [_brushes.lastObject.frames addObject:value];
}

- (void)drawEndedWithPoint:(CGPoint)point {
    if (_brushes.lastObject.shape == ShapeType_Text) return;
    CGRect rect = CGRectMake(point.x, point.y, 0, 0);
    NSValue *value = [NSValue valueWithCGRect:rect];
    [_brushes.lastObject.frames addObject:value];
}

#pragma mark --

- (void)drawPartWithBrush:(id<DrawBrushProtocol>)brush {
    if (!brush) return;
    [self drawWithBrush:brush];
}

#pragma mark -- 

- (void)drawTextBeganWithFrame:(CGRect)frame brush:(id<DrawBrushProtocol>)brush {
    if (!brush || brush.shape != ShapeType_Text) return;
    [self drawWithBrush:brush];
}

- (void)drawTextChangedWithFrame:(CGRect)frame text:(NSString *)text {
    BrushCharacterModel *model = (BrushCharacterModel *)(_brushes.lastObject);
    if (!model || model.shape != ShapeType_Text) return;
    model.character = text;
    [model.frames removeAllObjects];
    NSValue *value = [NSValue valueWithCGRect:frame];
    [model.frames addObject:value];
}

- (void)drawTextEndedWithFrame:(CGRect)frame text:(NSString *)text {
    BrushCharacterModel *model = (BrushCharacterModel *)(_brushes.lastObject);
    if (!model || model.shape != ShapeType_Text) return;
    model.character = text;
    [model.frames removeAllObjects];
    NSValue *value = [NSValue valueWithCGRect:frame];
    [model.frames addObject:value];
}

#pragma mark -- Private Method

- (void)drawWithBrush:(id<DrawBrushProtocol>)brush {
    
    @try {
        [_brushes addObject:brush];
        [_operate push:brush];
    }
    @catch (NSException * e) {
        NSLog(@"Exception------------: %@", e);
    }
    @finally {
        NSLog(@"finally-------------");
    }
    
}

- (id<DrawShapeProtocol>)shapeRef:(ShapeType)type {
    if (type >> SHAPE_DISPLACEMENT > self.shapes.count - 1) return nil;
    id<DrawShapeProtocol> shape = self.shapes[type >> SHAPE_DISPLACEMENT];
    return shape;
}

- (NSMutableArray<id<DrawShapeProtocol>> *)shapes {
    if (_shapes) return _shapes;
    NSMutableArray<id<DrawShapeProtocol>> *array = [NSMutableArray array];
    [array addObject:(id<DrawShapeProtocol>)Freeform.new];
    [array addObject:(id<DrawShapeProtocol>)StraightLine.new];
    [array addObject:(id<DrawShapeProtocol>)Rectangle.new];
    [array addObject:(id<DrawShapeProtocol>)OvalShape.new];
    [array addObject:(id<DrawShapeProtocol>)Circular.new];
    [array addObject:(id<DrawShapeProtocol>)TextChars.new];
    [array addObject:(id<DrawShapeProtocol>)Eraser.new];
    _shapes = array;
    return array;
}

@end
