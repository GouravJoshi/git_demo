

#import "DrawingBoardView.h"
#import "DrawShapeProtocol.h"
#import "BrushModel.h"
#import "BrushCharacterModel.h"

#import "StraightLine.h"

#import "Rectangle.h"
#import "Circular.h"
#import "OvalShape.h"
#import "TextChars.h"


#define SuperViewTransparent(arg1,arg2) ([arg1 colorWithAlphaComponent:arg2])

@implementation DrawingBoardView {
    NSMutableArray<NSValue *> * mCoordinateArray;
    NSMutableArray<NSMutableArray *> * mLineArray;
    NSMutableArray<UIColor *> * mLineColorArray;
    NSMutableArray<NSNumber *> * mLineWidthArray;

    NSMutableArray<NSMutableArray *> * mLastLineArray;
    NSMutableArray<UIColor *> * mLastLineColorArray;
    NSMutableArray<NSNumber *> * mLastLineWidthArray;
    
    id<DrawShapeProtocol> mShape;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initMember];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initMember];
    }
    return self;
}

- (void)initMember {
    self.lineWidth = 2.f;
    self.lineColor = [UIColor blackColor];
    self.backgroundColor = SuperViewTransparent(UIColor.grayColor, 0.6);
    if (mLastLineArray.count <= 0) {
        mLastLineArray = [[NSMutableArray alloc] initWithCapacity:10];
        mLastLineColorArray = [[NSMutableArray alloc] initWithCapacity:10];
        mLastLineWidthArray = [[NSMutableArray alloc] initWithCapacity:10];
    }
}

- (void)initCoordinateAarray {

    mCoordinateArray = [[NSMutableArray alloc] initWithCapacity:10];
    [self insertsSgementLineWidth:self.lineWidth];
    [self insertsSgementLineColor:self.lineColor];
}

- (void)insertLineToArray {
  
    if (!mLineArray && mLineArray.count <= 0) {
        mLineArray = [[NSMutableArray alloc] initWithCapacity:10];
    }
    [mLineArray addObject:mCoordinateArray];
}

- (void)insertCoordinate:(CGPoint)sender {
    NSValue* pointvalue = [NSValue valueWithCGPoint:sender];
    [mCoordinateArray addObject:pointvalue];
}

- (void)insertsSgementLineColor:(UIColor *)color {
    if (!mLineColorArray && mLineColorArray.count <= 0) {
        mLineColorArray = [[NSMutableArray alloc] initWithCapacity:10];
    }
    [mLineColorArray addObject:color];
}

- (void)insertsSgementLineWidth:(CGFloat)sender {
    if (!mLineWidthArray && mLineWidthArray.count <= 0) {
        mLineWidthArray = [[NSMutableArray alloc] initWithCapacity:10];
    }
    [mLineWidthArray addObject:@(sender)];
}

- (void)clearLine {

   if (mLineArray.count > 0) {
       
       [mLastLineArray addObjectsFromArray:[[mLineArray reverseObjectEnumerator]allObjects]];
       [mLastLineColorArray addObjectsFromArray:[[mLineColorArray reverseObjectEnumerator]allObjects]];
       [mLastLineWidthArray addObjectsFromArray:[[mLineWidthArray reverseObjectEnumerator]allObjects]];
       
       [mLineArray removeAllObjects];
       [mLineColorArray removeAllObjects];
       [mLineWidthArray removeAllObjects];
       [mCoordinateArray removeAllObjects];
       mLineArray = [[NSMutableArray alloc] initWithCapacity:10];
       mLineColorArray = [[NSMutableArray alloc] initWithCapacity:10];
       mLineWidthArray = [[NSMutableArray alloc] initWithCapacity:10];
        [self setNeedsDisplay];
    }
}

- (void)undo {

    if (mLineArray.count > 0) {
        NSMutableArray * array = [NSMutableArray array];
        [array addObjectsFromArray:mLineArray.lastObject];
        [mLastLineArray addObject:array];
        [mLastLineColorArray addObject:mLineColorArray.lastObject];
        [mLastLineWidthArray addObject:mLineWidthArray.lastObject];
        
        [mLineArray  removeLastObject];
        [mLineColorArray removeLastObject];
        [mLineWidthArray removeLastObject];
        [mCoordinateArray removeAllObjects];
        [self setNeedsDisplay];
    }
}

- (void)redo {

    if (mLastLineColorArray.count > 0) {
        [mLineColorArray addObject:mLastLineColorArray.lastObject];
        [mLastLineColorArray removeLastObject];
    }
    
    if (mLastLineWidthArray.count > 0) {
        [mLineWidthArray addObject:mLastLineWidthArray.lastObject];
        [mLastLineWidthArray removeLastObject];
    }
    
    if (mLastLineArray.count > 0) {
        [mLineArray addObject:mLastLineArray.lastObject];
        [mLastLineArray removeLastObject];
        [self setNeedsDisplay];
    }
}

#pragma mark -- Private Method

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch* touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    [self initCoordinateAarray];
    [self insertCoordinate:point];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSArray * movePointArray = [touches allObjects];
    CGPoint point = [movePointArray.firstObject locationInView:self];
    [self insertCoordinate:point];
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self insertLineToArray];
    [self setNeedsDisplay];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
}

- (void)drawRect:(CGRect)rect {
    
    if (!mShape) {
        mShape = (id<DrawShapeProtocol>)[[Circular alloc] init];
    }

    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetLineCap(context, kCGLineCapRound);

    CGContextSetLineJoin(context, kCGLineJoinRound);

    if (mLineArray.count > 0) {
        for (int i = 0; i < mLineArray.count; i++) {
            NSArray<NSValue *> * tempArray = mLineArray[i];
            UIColor * color = nil;
            CGFloat width = 2.f;
            if (mLineColorArray.count > 0) {
                color = mLineColorArray[i];
                width = mLineWidthArray[i].floatValue;
            }
            if (tempArray.count > 1) {
                CGContextBeginPath(context);
                CGPoint myStartPoint = tempArray.firstObject.CGPointValue;
                CGContextMoveToPoint(context, myStartPoint.x, myStartPoint.y);
                
                for (int j = 1; j < tempArray.count; j++) {
                    CGPoint endPoint = tempArray[j].CGPointValue;
                    CGContextAddLineToPoint(context, endPoint.x,endPoint.y);
                }
                CGContextSetStrokeColorWithColor(context, color.CGColor);
                CGContextSetLineWidth(context, width);
                CGContextStrokePath(context);
            }
        }
    }
    
    if (mCoordinateArray.count > 1) {
        CGContextBeginPath(context);
     
        CGPoint startPoint = mCoordinateArray.firstObject.CGPointValue;
        CGContextMoveToPoint(context, startPoint.x, startPoint.y);
      
        for (int i = 1; i < mCoordinateArray.count; i++) {
            CGPoint endPoint = mCoordinateArray[i].CGPointValue;
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
        }
      
        CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
        CGContextSetFillColorWithColor (context,  self.lineColor.CGColor);
  
        CGContextSetLineWidth(context, self.lineWidth);

        CGContextStrokePath(context);
    }
}


@end
