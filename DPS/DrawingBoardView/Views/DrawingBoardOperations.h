

#import <Foundation/Foundation.h>

@protocol DrawBrushProtocol;

typedef NS_ENUM(NSUInteger, OperationalType) {
    Operate_Undo    = 0,
    Operate_Redo    = 1,
    Operate_Clear   = 2,
    OperateDefault  = Operate_Undo,
};

@interface DrawingBoardOperationsEntity : NSObject
@property (nonatomic, assign) BOOL shouldDisplay;
@property (nonatomic, strong) NSMutableArray<id<DrawBrushProtocol>> *brushes;
@end

@interface DrawingBoardOperations : NSObject
- (void)push:(id<DrawBrushProtocol>)brush;
- (DrawingBoardOperationsEntity *)undo;
- (DrawingBoardOperationsEntity *)redo;
- (DrawingBoardOperationsEntity *)clear;
@end
