

#import "Freeform.h"
#import "DrawShapeProtocol.h"
#import "DrawBrushProtocol.h"

@interface Freeform ()<DrawShapeProtocol>

@end

@implementation Freeform

- (void)drawContext:(CGContextRef)context brush:(id<DrawBrushProtocol>)brush {
    if (brush.frames.count <= 0) {
        return;
    }
    
    __block CGPoint currentPoint;
    __block CGPoint previousPoint1;
    __block CGPoint previousPoint2;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineCapStyle = kCGLineCapRound;
    path.miterLimit = 0;
    path.lineWidth = brush.width;
    [brush.color set];
    [brush.frames enumerateObjectsUsingBlock:^(NSValue * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            previousPoint1 = previousPoint2 = currentPoint = obj.CGRectValue.origin;
            [path moveToPoint: CGPointMid(previousPoint1, previousPoint2)];
        }else {
            previousPoint2 = previousPoint1;
            previousPoint1 = currentPoint;
            currentPoint = obj.CGRectValue.origin;
            [path addLineToPoint: CGPointMid(previousPoint1, previousPoint2)];
            [path addQuadCurveToPoint: CGPointMid(currentPoint, previousPoint1) controlPoint: previousPoint1];
        }
    }];
    [path stroke];
}


static CGPoint CGPointMid(CGPoint a, CGPoint b) {
    return (CGPoint) {(a.x+b.x)/2.0, (a.y+b.y)/2.0};
}
@end
