

#import "StraightLine.h"
#import "DrawShapeProtocol.h"
#import "DrawBrushProtocol.h"

@interface StraightLine ()<DrawShapeProtocol>

@end

@implementation StraightLine

- (void)drawContext:(CGContextRef)context brush:(id<DrawBrushProtocol>)brush {
    
    if (brush.frames.count < 2) {
                return;
    }
    

    CGContextSetLineCap(context, kCGLineCapRound);

    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextBeginPath(context);
    CGPoint origin = brush.frames.firstObject.CGRectValue.origin;
    CGContextMoveToPoint(context, origin.x, origin.y);
    NSLog(@"StraightLine");
    CGPoint next = brush.frames.lastObject.CGRectValue.origin;
    CGContextAddLineToPoint(context, next.x, next.y);
    CGContextSetStrokeColorWithColor(context, brush.color.CGColor);
    CGContextSetBlendMode(context,kCGBlendModeNormal);
    CGContextSetLineWidth(context, brush.width);
    CGContextStrokePath(context);
}

- (CGPathRef)pathWithBrush:(id<DrawBrushProtocol>)brush {
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    return path.CGPath;
}


@end
