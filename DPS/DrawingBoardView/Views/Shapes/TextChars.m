

#import "TextChars.h"
#import "DrawShapeProtocol.h"
#import "DrawBrushProtocol.h"
#import "BrushCharacterModel.h"

@interface TextChars ()<DrawShapeProtocol>

@end

@implementation TextChars

- (void)drawContext:(CGContextRef)context brush:(id<DrawBrushProtocol>)brush {
    
    if (!brush.color)  return;

    
    if (![brush isKindOfClass:BrushCharacterModel.class]) {
     
        return;
    }
    
    BrushCharacterModel *model = (BrushCharacterModel *)brush;
    if (model.character.length <= 0) {
       
        return;
    }
    
    if (brush.frames.count <= 0) {
        
        return;
    }
    CGContextSetBlendMode(context,kCGBlendModeNormal);

    CGRect frame = model.frames.firstObject.CGRectValue;
    if (CGSizeEqualToSize(frame.size, CGSizeZero)) {
        CGFloat width = UIScreen.mainScreen.bounds.size.width - frame.origin.x - 10;
        CGFloat height = UIScreen.mainScreen.bounds.size.height - frame.origin.y - 10;
        frame.size.width = width;
        frame.size.height = height;
    }
    
    UIFont *font = [UIFont systemFontOfSize:model.fontSize];
    [model.character drawWithRect:frame
                          options:NSStringDrawingUsesLineFragmentOrigin
                       attributes:@{NSFontAttributeName:font, NSForegroundColorAttributeName:model.color}
                          context:nil];
}

@end
