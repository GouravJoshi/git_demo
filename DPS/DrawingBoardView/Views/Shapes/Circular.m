
#import "Circular.h"
#import "DrawShapeProtocol.h"
#import "DrawBrushProtocol.h"


@interface Circular ()<DrawShapeProtocol>

@end


@implementation Circular

- (void)drawContext:(CGContextRef)context brush:(id<DrawBrushProtocol>)brush {
    if (brush.frames.count < 2) {
        return;
    }
    CGPoint origin = brush.frames.firstObject.CGRectValue.origin;
    CGPoint next = brush.frames.lastObject.CGRectValue.origin;
    CGFloat width = next.x - origin.x;
    CGFloat height = next.y - origin.y;
    CGFloat radius = MAX(fabs(width), fabs(height));
    
    CGContextSetLineWidth(context, brush.width);
    CGContextSetStrokeColorWithColor(context, brush.color.CGColor);
    
    if (brush.isFill) {
        CGContextSetFillColorWithColor(context, brush.fillColor.CGColor);
    }
    CGContextSetBlendMode(context,kCGBlendModeNormal);
    CGContextAddArc(context, origin.x, origin.y, radius, 0, 2 * M_PI, 0);
    CGContextDrawPath(context, brush.isFill ? kCGPathFillStroke : kCGPathStroke);
}

@end
