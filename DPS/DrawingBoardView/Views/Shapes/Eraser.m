

#import "Eraser.h"
#import "DrawShapeProtocol.h"
#import "DrawBrushProtocol.h"

@interface Eraser ()<DrawShapeProtocol>

@end

@implementation Eraser

- (void)drawContext:(CGContextRef)context brush:(id<DrawBrushProtocol>)brush {
    if (brush.frames.count <= 0) {
        NSLog(@"Freeform drawContext : Draw a Freeform");
        return;
    }
    CGPoint origin = brush.frames.firstObject.CGRectValue.origin;
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, origin.x, origin.y);
    
    [brush.frames enumerateObjectsUsingBlock:^(NSValue * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGPoint point = obj.CGRectValue.origin;
        CGContextAddLineToPoint(context, point.x,point.y);
    }];
    CGContextSetStrokeColorWithColor(context, brush.color.CGColor);
    CGContextSetBlendMode(context,kCGBlendModeClear);
    CGContextSetLineWidth(context, brush.width);
    CGContextStrokePath(context);
}

@end
