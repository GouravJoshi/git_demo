

#import "OvalShape.h"
#import "DrawShapeProtocol.h"
#import "DrawBrushProtocol.h"

@interface OvalShape ()<DrawShapeProtocol>

@end

@implementation OvalShape

- (void)drawContext:(CGContextRef)context brush:(id<DrawBrushProtocol>)brush {
    if (brush.frames.count < 2) {
        return;
    }
    CGPoint origin = brush.frames.firstObject.CGRectValue.origin;
    CGPoint next = brush.frames.lastObject.CGRectValue.origin;
    CGFloat width = next.x - origin.x;
    CGFloat height = next.y - origin.y;
    CGRect bounds = CGRectMake(origin.x, origin.y, width, height);
    
    CGContextSetLineWidth(context, brush.width);
    CGContextSetStrokeColorWithColor(context, brush.color.CGColor);
    
    if (brush.isFill) {
        CGContextSetFillColorWithColor(context, brush.fillColor.CGColor);
    }
    CGContextSetBlendMode(context,kCGBlendModeNormal);
    CGContextAddEllipseInRect(context, bounds);
    CGContextDrawPath(context, brush.isFill ? kCGPathFillStroke : kCGPathStroke);
}

@end
