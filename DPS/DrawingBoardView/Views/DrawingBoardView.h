

#import <UIKit/UIKit.h>

@interface DrawingBoardView : UIView
@property (nonatomic, assign) CGFloat lineWidth;;
@property (nonatomic, strong) UIColor *lineColor;
- (void)clearLine;
- (void)undo;
- (void)redo;
@end
