

#import "DrawingBoardOperations.h"
#import "DrawBrushProtocol.h"

@implementation DrawingBoardOperationsEntity

@end


@interface DrawingBoardOperations ()
@property (nonatomic, assign) BOOL canRedo;
@property (nonatomic, assign) OperationalType currentOperate;
@property (nonatomic, strong) NSMutableArray<id<DrawBrushProtocol>> *recordStack;
@property (nonatomic, strong) NSMutableArray<id<DrawBrushProtocol>> *cacheStack;
@property (nonatomic, strong) NSMutableArray<id<DrawBrushProtocol>> *clearTempStack;
@end

@implementation DrawingBoardOperations

- (void)dealloc {
    
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _recordStack = [NSMutableArray array];
        _cacheStack = [NSMutableArray array];
        _clearTempStack = [NSMutableArray array];
    }
    return self;
}

- (void)push:(id<DrawBrushProtocol>)brush {
    _canRedo = NO;
    [_cacheStack removeAllObjects];
    [_recordStack addObject:brush];
}

- (DrawingBoardOperationsEntity *)undo {
    _canRedo = YES;
    _currentOperate = Operate_Undo;
    return self.entity;
}

- (DrawingBoardOperationsEntity *)redo {
    if (!_canRedo)  {
        DrawingBoardOperationsEntity *entity = [DrawingBoardOperationsEntity new];
        entity.brushes = [_recordStack copy];
        entity.shouldDisplay = NO;
        return entity;
    }
    _currentOperate = Operate_Redo;
    return self.entity;
}

- (DrawingBoardOperationsEntity *)clear {
    _canRedo = YES;
    _currentOperate = Operate_Clear;
    return self.entity;
}

- (DrawingBoardOperationsEntity *)entity {
    DrawingBoardOperationsEntity *entity = [DrawingBoardOperationsEntity new];
    BOOL shoudlDidplay = NO;
    switch (_currentOperate) {
            
        case Operate_Undo: {
            if (_recordStack.count > 0) {
                [_cacheStack addObject:_recordStack.lastObject];
                [_recordStack removeLastObject];
                
                shoudlDidplay = YES;
            }
        }
            
            break;
        case Operate_Redo: {
            if (_cacheStack.count > 0) {
                [_recordStack addObject:_cacheStack.lastObject];
                [_cacheStack removeLastObject];
                
                shoudlDidplay = YES;
            }
        }
            
            break;
        case Operate_Clear: {
            if (_recordStack.count > 0) {
                NSArray *array = _recordStack.reverseObjectEnumerator.allObjects;
                [_cacheStack addObjectsFromArray:array];
                [_recordStack removeAllObjects];
                
                shoudlDidplay = YES;
            }
        }
            break;
            
        default:
            break;
    }
    entity.shouldDisplay = shoudlDidplay;
    entity.brushes = [_recordStack mutableCopy];
    return entity;
}

@end



