

#import <Foundation/Foundation.h>

@protocol DrawBrushProtocol;

@interface DrawingBoardProxy : NSObject
@property (nonatomic, strong, readonly) id<DrawBrushProtocol> currentBrush;
@property (nonatomic, strong, readonly) NSMutableArray<id<DrawBrushProtocol>> *brushes;
@end
