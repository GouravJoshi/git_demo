//
//  UIColor+globalColor.h
//  DPS
//
//  Created by Saavan Patidar on 21/04/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  saavan Patidar 2021

#import <UIKit/UIKit.h>

@interface UIColor (globalColor)
+ (UIColor *) themeColor;
+ (UIColor *) themeColorBlack;
+ (UIColor *) lightTextColorTimeSheet;
+ (UIColor *) lightColorForCell;
+(UIColor*) headerFooterColor;

@end
