//
//  HelpTicket_AddDocumentVC.swift
//  DPS
//
//  Created by Ruchika Verma on 09/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class HelpTicket_AddDocumentVC: UIViewController,  UIDocumentPickerDelegate {
        
        // MARK: - ----IB-Outlets
        
        @IBOutlet weak var txtTitle: ACFloatingTextField!
        @IBOutlet weak var lblName: UILabel!
        @IBOutlet weak var btnActions: UIButton!
        
        // MARK: - ----Variable
        
        var imagePicker = UIImagePickerController()
        var strGlobalName = String ()
        var strGlobalDocumentType = String ()
        var globalData  = Data ()
        var timer = Timer()
        var videoURL = NSURL()
        var isAddedFromAudioVC = false
        
        // MARK: - ----Views Life Cycle
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            lblName.text = ""
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            //For Audio
                let isAudio = nsud.value(forKey: "AudioName") as? String ?? ""
                if isAudio != ""
                {
                    deleteData()
                    strGlobalDocumentType = ""
                    strGlobalName = ""
                    globalData = Data()
                    isAddedFromAudioVC = true
                    self.strGlobalDocumentType = "audio"
                    strGlobalName = nsud.value(forKey: "AudioName") as! String
                    globalData = GetDataFromDocumentDirectory(strFileName: strGlobalName)
                    lblName.text = strGlobalName
                    btnActions.isHidden = false
                    btnActions.setImage(UIImage(named: "audio"), for: .normal)
                    nsud.set("", forKey: "AudioName")
                    nsud.synchronize()
                }
            
        }
        
        // MARK: - ----Button Action
        
        @IBAction func action_Back(_ sender: Any) {
            
            deleteData()
            self.back()
            
        }
        
        @IBAction func action_Browse(_ sender: ButtonWithShadow) {
            
            self.selectDocument(sender: sender)
            
        }
        
        
        @IBAction func action_Save(_ sender: UIButton) {
            
            self.saveDocuments()
            
        }
        @IBAction func keyDown(_ sender: Any) {
            
            self.view.endEditing(true)
            
        }
        
    @IBAction func action_Play(_ sender: Any) {
        
        if strGlobalDocumentType == "image" {
            
            let uiImage: UIImage = UIImage(data: globalData)!
            
            let  imageView = UIImageView(image: uiImage)
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        }
        else if strGlobalDocumentType == "pdf" {
            
            savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
            goToPdfView(strPdfName: strGlobalName)
            
        }
        else if strGlobalDocumentType == "video" {
            
            let player = AVPlayer(url: videoURL as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        else if strGlobalDocumentType == "audio" {
            
            playAudioo(strAudioName: strGlobalName)
        }
        else{
            savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
            goToPdfView(strPdfName: strGlobalName)
        }
    }
        
        // MARK: - ----Functions
        
        func deleteData() {
            
            if strGlobalDocumentType == "audio"{
                
                if strGlobalName.count > 0 {
                    
                    removeImageFromDirectory(itemName: strGlobalName)
                    
                }
                
            }
            
            if strGlobalDocumentType == "pdf"{
                
                if strGlobalName.count > 0 {
                    
                    removeImageFromDirectory(itemName: strGlobalName)
                    
                }
                
            }
            
            if strGlobalDocumentType == "image"{
                
                if strGlobalName.count > 0 {
                    
                    removeImageFromDirectory(itemName: strGlobalName)
                    
                }
                
            }
            
        }
        
        func back()  {
            dismiss(animated: false)
        }
        
        func playAudioo(strAudioName : String) {
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
            testController!.strAudioName = strAudioName
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        }
        
        func goToPdfView(strPdfName : String) {
            
            let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
            testController!.strFileName = strPdfName
            testController!.strFrom = "Pest"
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        }
        
        func saveDocuments() {
            
            if (strGlobalName.count) < 1 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectDoc, viewcontrol: self)
                
            } else {
                
                var arrOfDocumentNames = UserDefaults.standard.stringArray(forKey: "HelpTicket_CommentDocumentsName") ?? [String]()
                var arrOfDocumentType = UserDefaults.standard.stringArray(forKey: "HelpTicket_CommentDocumentsType") ?? [String]()
                
                if arrOfDocumentNames.count > 0{
                    arrOfDocumentNames.append(strGlobalName)
                    UserDefaults.standard.set(arrOfDocumentNames, forKey: "HelpTicket_CommentDocumentsName")
                    
                    arrOfDocumentType.append(strGlobalDocumentType)
                    UserDefaults.standard.set(arrOfDocumentType, forKey: "HelpTicket_CommentDocumentsType")
                }
                else{
                    UserDefaults.standard.set([strGlobalName], forKey: "HelpTicket_CommentDocumentsName")
                    UserDefaults.standard.set([strGlobalDocumentType], forKey: "HelpTicket_CommentDocumentsType")
                }
                
                
                if strGlobalDocumentType == "image" {
                    
                    let uiImage: UIImage = UIImage(data: globalData)!
                    
                    let imageResized = Global().resizeImageGloballl(uiImage)
                    
                    saveImageDocumentDirectory(strFileName: strGlobalName, image: imageResized!)
                    
                }else if strGlobalDocumentType == "pdf" {
                    
                    savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
                    
                }
                else if strGlobalDocumentType == "video" {
                    
                    savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
                    
                }
                else if strGlobalDocumentType == "audio"{
                    if isAddedFromAudioVC == true{
                        
                    }
                    else{
                        savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
                    }
                }
                else{
                    savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
                }
                dismiss(animated: false)
            }
            
        }
        
        func selectDocument(sender: ButtonWithShadow)  {
            
            self.view.endEditing(true)
            
            let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
                self.imagePicker = UIImagePickerController()
                
                self.strGlobalDocumentType = "image"
                
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    print("Button capture")
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }))
            
            
            alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
                self.imagePicker = UIImagePickerController()
                
                self.strGlobalDocumentType = "image"
                
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    print("Button capture")
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .savedPhotosAlbum
                    self.imagePicker.allowsEditing = false
                    self.imagePicker.mediaTypes = ["public.image", "public.movie"]
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }))
            
            
            alert.addAction(UIAlertAction(title: "Voice Note", style: .default , handler:{ (UIAlertAction)in
                
                self.strGlobalDocumentType = "audio"
                
                self.goToRecordView()
                
            }))
            
            alert.addAction(UIAlertAction(title: "Video", style: .default , handler:{ (UIAlertAction)in
                
                self.strGlobalDocumentType = "video"
                self.imagePicker = UIImagePickerController()
                self.imagePicker.sourceType = .camera
                self.imagePicker.cameraDevice = .rear
                self.imagePicker.videoMaximumDuration = 30
                self.imagePicker.delegate = self
                //self.imagePicker.videoQuality = .typeLow
                let mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)
                let videoMediaTypesOnly = (mediaTypes as NSArray?)?.filtered(using: NSPredicate(format: "(SELF contains %@)", "movie"))
                let movieOutputPossible: Bool = videoMediaTypesOnly != nil
                if movieOutputPossible {
                    if let videoMediaTypesOnly = videoMediaTypesOnly as? [String] {
                        self.imagePicker.mediaTypes = videoMediaTypesOnly
                    }
                    self.timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.pickerDismiss), userInfo: nil, repeats: false)
                    self.present(self.imagePicker, animated: true)
                }
                
            }))
            alert.addAction(UIAlertAction(title: "More...", style: .default , handler:{ (UIAlertAction)in
             let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypeImage),String(kUTTypePDF), String(kUTTypeText), String(kUTTypeSpreadsheet), String(kUTTypePresentation), String(kUTTypeAudio), String(), String(kUTTypeVideo), String(kUTTypeMovie), String(kUTTypeContent)], in: UIDocumentPickerMode.import)
                                        documentPicker.delegate = self
                                      documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                                      documentPicker.popoverPresentationController?.sourceView = self.view

                                      self.present(documentPicker, animated: true, completion: nil)

                
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
                
                
            }))
            //alert.popoverPresentationController?.sourceRect = sender.frame
            if let popoverController = alert.popoverPresentationController {
                                  popoverController.sourceView = sender as UIView
                                  popoverController.sourceRect = sender.bounds
                                //  popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                              }
            self.present(alert, animated: true, completion: {
                
                
            })
            
        }
        @objc func pickerDismiss()
        {
            showAlertWithoutAnyAction(strtitle: "Max Limit Exceeded", strMessage: "Your video recorded succesfully!", viewcontrol: self)
            imagePicker.stopVideoCapture()
        }
        func goToRecordView()  {
            
            let isAudioPermission = Global().isAudioPermissionAvailable()
            if isAudioPermission {
                let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
                testController?.modalPresentationStyle = .fullScreen
                self.present(testController!, animated: false, completion: nil)
            }
            else{
                let alertController = UIAlertController (title: Alert, message: alertAudioVideoPermission, preferredStyle: .alert)
                let settingsAction = UIAlertAction(title: "Go-To Settings", style: .default) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertController.addAction(cancelAction)
                present(alertController, animated: true, completion: nil)
                
            }
            
        }
        
        // MARK: - -----------------------------------UIDocumentPickerViewController Delegate Method-----------------------------------
        
        
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        
        print("import result : \(myURL)")
        
        do {
            
//            let isMoreThenTwoMB = Global().isMoreThenTwoMB(myURL, 50)
//
//            if isMoreThenTwoMB {
//
//                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_DataLimit, viewcontrol: self)
//
//            } else {
                
                deleteData()
                
                let myUrlString = "\(myURL)"
                lblName.text = Global().strDocName(fromPath: myUrlString)
                
                if myURL.pathExtension.lowercased() == "pdf" {
                    let data = try Data(contentsOf: myURL)
                    globalData = data
                    self.strGlobalDocumentType = "pdf"
                    strGlobalName =  "\(getUniqueValueForId())" + ".pdf"
                    btnActions.isHidden = false
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                }
                
                else  if myURL.pathExtension.lowercased() ==  "xls" {
                    let data = try Data(contentsOf: myURL)
                    globalData = data
                    self.strGlobalDocumentType = "xls"
                    strGlobalName =  "\(getUniqueValueForId())" + ".xls"
                    btnActions.isHidden = false
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                }
                
                else if  myURL.pathExtension.lowercased() ==  "xlsx"{
                    let data = try Data(contentsOf: myURL)
                    globalData = data
                    self.strGlobalDocumentType = "xlsx"
                    strGlobalName =  "\(getUniqueValueForId())" + ".xlsx"
                    btnActions.isHidden = false
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                }
                
                else  if  myURL.pathExtension.lowercased() ==  "doc" {
                    let data = try Data(contentsOf: myURL)
                    globalData = data
                    self.strGlobalDocumentType = "doc"
                    strGlobalName =  "\(getUniqueValueForId())" + ".doc"
                    btnActions.isHidden = false
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                }
                
                else if myURL.pathExtension.lowercased() == "docx"{
                    let data = try Data(contentsOf: myURL)
                    globalData = data
                    self.strGlobalDocumentType = "docx"
                    strGlobalName =  "\(getUniqueValueForId())" + ".docx"
                    btnActions.isHidden = false
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                }
                
                else if myURL.pathExtension.lowercased() == "png" || myURL.pathExtension.lowercased() == "jpeg"  ||  myURL.pathExtension.lowercased() == "jpg"{
                    // image
                    let data = try Data(contentsOf: myURL)
                    let uiImage: UIImage = UIImage(data: data)!
                    globalData = data
                    self.strGlobalDocumentType = "image"
                    strGlobalName = "\(getUniqueValueForId())" + "." + myURL.pathExtension.lowercased()
                    btnActions.isHidden = false
                    btnActions.setImage(uiImage, for: .normal)
                }
                else if myURL.pathExtension.lowercased() == "mov" || myURL.pathExtension.lowercased() == "mp4" {
                    
                    videoURL = myURL as NSURL
                    let data = try Data(contentsOf: myURL)
                    globalData = data
                    self.strGlobalDocumentType = "video"
                    strGlobalName =  "\(getUniqueValueForId())" + "." + myURL.pathExtension.lowercased()
                    btnActions.isHidden = false
                    btnActions.setImage(UIImage(named: "video"), for: .normal)
                }
                
                else if myURL.pathExtension.lowercased() ==  "mp3" {
                    let data = try Data(contentsOf: myURL)
                    globalData = data
                    self.strGlobalDocumentType = "audio"
                    strGlobalName =  "\(getUniqueValueForId())" + ".mp3"
                    btnActions.isHidden = false
                    lblName.text = strGlobalName
                    btnActions.setImage(UIImage(named: "audio"), for: .normal)
                }
                
                else{
                    let data = try Data(contentsOf: myURL)
                    globalData = data
                    self.strGlobalDocumentType = "other"
                    strGlobalName =  "\(getUniqueValueForId())" + "." + myURL.pathExtension.lowercased()
                    btnActions.isHidden = false
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                }
                lblName.text = strGlobalName
           // }
            
            
        } catch _ as NSError  {
            
        }catch {
            
        }
        
    }
        
        func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
             print("view was cancelled")
             //dismiss(animated: true, completion: nil)
         }
        
        func documentMenu(_ documentMenu: UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
            
        }
   
    }

    // MARK: - ----------------UIImagePickerControllerDelegate
    // MARK: -
    extension HelpTicket_AddDocumentVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            self.dismiss(animated: true, completion: { () -> Void in
                
            })
            deleteData()
            if(self.strGlobalDocumentType == "video"){ // For Video
                // video
                btnActions.isHidden = false
                
                btnActions.setImage(UIImage(named: "video"), for: .normal)
                
                self.timer.invalidate()
                let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String
                if (mediaType == "public.movie") {
                    videoURL = (info[UIImagePickerController.InfoKey.mediaURL] as? NSURL)!
                    print("videoURL---------\(videoURL)")
                    self.strGlobalDocumentType = "video"
                    if let data = try? Data(contentsOf: videoURL as URL)
                    {
                        self.globalData = data
                        btnActions.isHidden = false
                        strGlobalName =  "\(getUniqueValueForId())" + ".mp4"
                        lblName.text = strGlobalName
                        btnActions.isHidden = false
                        btnActions.setImage(UIImage(named: "video"), for: .normal)
                    }else{
                        
                    }
                    
                }
            }else{
                
                if  info[UIImagePickerController.InfoKey.mediaType] as! String == "public.movie"{
                    videoURL = (info[UIImagePickerController.InfoKey.mediaURL] as? NSURL)!
                    print("videoURL---------\(videoURL)")
                    self.strGlobalDocumentType = "video"
                    if let data = try? Data(contentsOf: videoURL as URL)
                    {
                        self.globalData = data
                        btnActions.isHidden = false
                        strGlobalName =  "\(getUniqueValueForId())" + "." + videoURL.pathExtension!
                        lblName.text = strGlobalName
                        btnActions.isHidden = false
                        btnActions.setImage(UIImage(named: "video"), for: .normal)
                    }else{
                        
                    }
                    
                }
                else{
                    let imageData = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.jpegData(compressionQuality:1.0)
                    
                    
                    btnActions.isHidden = false
                    btnActions.setImage(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, for: .normal)
                    globalData = imageData!
                    self.strGlobalDocumentType = "image"
                    strGlobalName =  "\(getUniqueValueForId())" + ".png"
                    lblName.text = strGlobalName
                }
            }
        }
    }
