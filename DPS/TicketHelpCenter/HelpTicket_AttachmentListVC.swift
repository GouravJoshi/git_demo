//
//  HelpTicket_AttachmentListVC.swift
//  DPS
//
//  Created by APPLE on 20/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class HelpTicket_AttachmentListVC: UIViewController {

    @IBOutlet weak var tblAttchment: UITableView!
    var arrAttachmentList = [AttachmentList]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension HelpTicket_AttachmentListVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAttachmentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! HelpTicket_AttchmentCell
        cell.selectionStyle = .none
        
        let objAttachment: AttachmentList = self.arrAttachmentList[indexPath.row]
        cell.lblAttchment.text = objAttachment.strFileName.capitalizingFirstLetter()
        
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let objAttachment: AttachmentList = self.arrAttachmentList[indexPath.row]

        print(objAttachment.strFilePath)
        
   
        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_PreviewDocument") as? HelpTicket_PreviewDocument
        objByProductVC?.strFilePath = objAttachment.strFilePath
        objByProductVC?.strFileName = objAttachment.strFileName
        self.navigationController?.pushViewController(objByProductVC!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}


class HelpTicket_AttchmentCell: UITableViewCell {

    //MARK: -IBOutlets
    
    @IBOutlet weak var lblAttchment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
