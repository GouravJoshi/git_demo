//
//  HelpTicket_EditTicketVC.swift
//  DPS
//
//  Created by INFOCRATS on 29/11/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class HelpTicket_EditTicketVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var txtTitle: ACFloatingTextField!
    @IBOutlet weak var txtPriority: ACFloatingTextField!
    @IBOutlet weak var txtInputFile: ACFloatingTextField!
    @IBOutlet weak var lblAddedBy :UILabel!
    @IBOutlet weak var lblAddedDate :UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var collectionViewAttchments: UICollectionView!
    @IBOutlet weak var lblCount: UILabel!

    //MARK: - Variable
    var charLenth = 0
    var dictTicketDetail = HelpTicket_DetailModel()
    var arrOfPriorityModel = [HelpTicket_PriorityModel]()
    var strSelectedPriority = ""
    weak var delegateEdit : HelpTicketAddCommentDelegate?

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewAttchments.delegate = self
        collectionViewAttchments.dataSource = self
        assignData()
    }
    
    //MARK: - IBActions
    @IBAction func btnSaveAction(_ sender: UIButton) {
        
        if(Validation() != ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Validation(), viewcontrol: self)
        }else{
           callWebServiceToUpdateTicketDetail()
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnPriortiyAction(_ sender: UIButton) {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "-----Select-----"
        vc.strTag = 6001
        if arrOfPriorityModel.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelPaymentTable = self
            var arrOfPro = [String]()
            for i in 0..<arrOfPriorityModel.count{
                arrOfPro.append(arrOfPriorityModel[i].strPriorityName)
            }
            vc.aryPayment = arrOfPro
            self.present(vc, animated: false, completion: {})
        }
        else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    
    //MARK: - Custom functions
    func Validation() -> String {
        
        let tempTitle = txtTitle.text?.trimmingCharacters(in: .whitespaces)
//        let tempDecription = txtViewDescription.text?.trimmingCharacters(in: .whitespaces)

        if tempTitle?.count == 0 {
            return "Title required"
        }
        
        if txtPriority.text?.count == 0 {
            return "Priority required"
        }
        
//        if tempDecription?.count == 0 {
//            return "Description required"
//        }
        return ""
    }
    
    func assignData(){
        callWebServiceToGetPriorityList()
        txtTitle.text = self.dictTicketDetail.strTaskName
        self.lblAddedBy.text = "By " + self.dictTicketDetail.dictThirdPartyUserDetail.strName.capitalized

        txtPriority.text = self.dictTicketDetail.strClientPriority
        txtViewDescription.text = self.dictTicketDetail.strTaskDescription
        
        let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: dictTicketDetail.strCreatedDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let Date1 = formatter.date(from: scheduleStartDate!)
        let strDate = formatter.string(from: Date1!)
        lblAddedDate.text = strDate
    }
       
    func getPostData() -> [String: Any]
    {
        let param = [
            "TaskTimeEntryId" : self.dictTicketDetail.strTaskTimeEntryId,
            "Title": txtTitle.text!.trimmingCharacters(in: .whitespaces) ,
            "Description": txtViewDescription.text.trimmingCharacters(in: .whitespaces) ?? "",
            "ClientPriority": strSelectedPriority,
            "CompanyKey": Global().getCompanyKey() ?? "",
            "EmployeeId": Global().getEmployeeId()
        ] as [String: Any]
        
        return param
    }
    
    //MARK: -Call Web service
    func callWebServiceToGetPriorityList(){
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            let strURL = String(format: URL.HelpTicket.getClientPriorityList)
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { [self] (response, status) in
                if(status)
                {
                    loader.dismiss(animated: false) {
                        HelpTicketParser.parseHelpTicketPriortyFilterJSON(response: response, completionHandler: { (obj) in
                            self.arrOfPriorityModel = obj
                            if self.arrOfPriorityModel.count > 0{
                                self.strSelectedPriority = self.dictTicketDetail.strClientPriority
                            }
                        })
                    }
                }
                else{
                    loader.dismiss(animated: false) {
                        print("failed")
                    }
                }
            }
        }
    }
    
    func callWebServiceToUpdateTicketDetail(){
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            let strURL = String(format: URL.HelpTicket.upadteTicketDetail)
            
            WebService.postRequestWithHeaders(dictJson: getPostData() as NSDictionary, url: strURL, responseStringComing: "TimeLine") { [self] (response, status) in
                if(status)
                {
                    loader.dismiss(animated: false) {
                        
                        if response["data"] is [String:Any]{
                            let dict = response["data"] as! [String:Any]
                            let isTitleSave = dict["IsTitleSaved"] as? Bool ?? false
                            let isDescriptionSaved = dict["IsDescriptionSaved"] as? Bool ?? false
                            let isClientProritySaved = dict["IsClientPrioritySaved"] as? Bool ?? false
                            
                            if isTitleSave == true && isDescriptionSaved == true && isClientProritySaved == true{
                                
                                let alert = UIAlertController(title: "Success", message: "Detail updated successfully", preferredStyle: UIAlertController.Style.alert)
                                
                                alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                    
                                    delegateEdit?.refreshAfterAddComment()
                                    self.navigationController?.popViewController(animated: false)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else{
                                self.navigationController?.popViewController(animated: false)
                                showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "something went wrong", viewcontrol: self)
                            }
                        }
                    }
                }
                else{
                    loader.dismiss(animated: false) {
                        print("failed")
                    }
                }
            }
        }
    }
    
}

//MARK: - Extension for picker of prority
extension HelpTicket_EditTicketVC: PaymentInfoDetails
{
    func getPaymentInfo(dictData: String, index: Int) {
        
        if(dictData.count == 0){
        }else{
            print(dictData)
            strSelectedPriority = dictData
            txtPriority.text = strSelectedPriority
        }
        
    }
}

//MARK: - Extension for collection view
extension HelpTicket_EditTicketVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        cell.lblTitle.text = "AattachedFileFire.png"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        if DeviceType.IS_IPAD{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 18.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 38)
        }else{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 14.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 38)
        }
        
    }
}
extension HelpTicket_EditTicketVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if newText.count > 5000{
            print("jyada hai")
        }
        else{
            lblCount.text =  "\(newText.count)/5000"
        }
        return newText.count < 5000
    }
    
}
