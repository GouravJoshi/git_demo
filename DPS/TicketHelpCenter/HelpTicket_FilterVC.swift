//
//  HelpTicket_FilterVC.swift
//  DPS
//
//  Created by INFOCRATS on 01/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
protocol HelpTicketFilter: class {
    
    func filterOnTicket(strPriority: String, strToDate: String, strFromDate: String)
}
class HelpTicket_FilterVC: UIViewController {
    
    //MARK: - IBOutlets
    
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    
    //MARK: - Variable
    var arrOfStatusModel = [HelpTicket_StatusModel]()
    var arrOfPriorityModel = [HelpTicket_PriorityModel]()
    
    var loader = UIAlertController()
    var strSelectedPriority = ""
    var strSelectedToDate = ""
    var strSelectedFromDate = ""
    weak var delegateFiler : HelpTicketFilter?
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.reloadData()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        // callWebServiceToGetStatusList()
        callWebServiceToGetPriorityList()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        if strSelectedPriority == "---Select---" {
            strSelectedPriority = ""
        }
        self.delegateFiler?.filterOnTicket(strPriority: strSelectedPriority, strToDate: strSelectedToDate, strFromDate: strSelectedFromDate)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func clearAction(_ sender: Any) {
        strSelectedPriority = ""
        strSelectedToDate = ""
        strSelectedFromDate = ""
        self.delegateFiler?.filterOnTicket(strPriority: strSelectedPriority, strToDate: strSelectedToDate, strFromDate: strSelectedFromDate)
        self.tblView.reloadData()
    }
    
    //MARK: -Call Web service
    func callWebServiceToGetStatusList(){
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            let strURL = String(format: URL.HelpTicket.getStatus , Global().getCompanyKey())
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { [self] (response, status) in
                callWebServiceToGetPriorityList()
                if(status)
                {
                    HelpTicketParser.parseHelpTicketStatusFilterJSON(response: response, completionHandler: { (obj) in
                        self.arrOfStatusModel.removeAll()
                        self.arrOfStatusModel = obj
                        if self.arrOfStatusModel.count > 0{
                            tblView.reloadData()
                        }
                    })
                    
                }
                else{
                    print("failed")
                }
            }
        }
    }
    
    func callWebServiceToGetPriorityList(){
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            let strURL = String(format: URL.HelpTicket.getClientPriorityList)
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { [self] (response, status) in
                if(status)
                {
                    self.loader.dismiss(animated: false) {
                        HelpTicketParser.parseHelpTicketPriortyFilterJSON(response: response, completionHandler: { (obj) in
                            self.arrOfPriorityModel = obj
                            if self.arrOfPriorityModel.count > 0{
                                //self.strSelectedPriority = self.arrOfPriorityModel[0].strPriorityName
                                tblView.reloadData()
                            }
                        })
                    }
                }
                else{
                    self.loader.dismiss(animated: false) {
                        print("failed")
                    }
                }
            }
        }
    }
    
}

extension HelpTicket_FilterVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        headerView.backgroundColor = .white
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        
        if DeviceType.IS_IPAD{
            label.font = .systemFont(ofSize: 20)
        }
        else{
            label.font = .systemFont(ofSize: 16)
        }
        
        
        if section == 0{
            label.text = "Priority"
            
        }
        else if section == 1{
            label.text = "Select Date"
            
        }
        
        label.textColor = .appThemeColor
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        if indexPath.section == 0{
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTicket_FilterStatusCell") as! HelpTicket_FilterStatusCell
        //            cell.selectionStyle = .none
        //
        //            cell.btnTitle.setTitle(arrOfStatusModel[indexPath.row].strStatusName, for: .normal)
        //            cell.btnChecked.setImage(UIImage(named: "check_box_1.png"), for: .normal)
        //
        //            return cell
        //        }
        if indexPath.section == 0{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTicket_FilterProrityCell") as! HelpTicket_FilterProrityCell
            cell.selectionStyle = .none
            
            if strSelectedPriority == "" {
                strSelectedPriority = "---Select---"
            }
            
            cell.btnProrityTitle.setTitle(strSelectedPriority, for: .normal)
            cell.btnProrityTitle.addTarget(self, action: #selector(actionOnPriortiy), for: .touchUpInside)
            
            cell.btnPrority.setTitle(strSelectedPriority, for: .normal)
            cell.btnPrority.addTarget(self, action: #selector(actionOnPriortiy), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTicket_FilterSelectDateCell") as! HelpTicket_FilterSelectDateCell
            cell.selectionStyle = .none
            
            cell.txtToDate.text = strSelectedToDate
            cell.txtFromDate.text = strSelectedFromDate
            
            cell.txtToDate.isUserInteractionEnabled = false
            cell.txtFromDate.isUserInteractionEnabled = false
            
            cell.btnToDate.addTarget(self, action: #selector(actionOnToDate), for: .touchUpInside)
            
            cell.btnFromDate.addTarget(self, action: #selector(actionOnFromDate), for: .touchUpInside)
            
            return cell
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.section == 0{
            return 50
        }
        else if indexPath.section == 1{
            return 75
        }
        return UITableView.automaticDimension
    }
    
    //MARK: - IBAction
    @objc func actionOnPriortiy(_ sender: UIButton){
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "-----Select-----"
        vc.strTag = 6001
        if arrOfPriorityModel.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelPaymentTable = self
            var arrOfPro = [String]()
            for i in 0..<arrOfPriorityModel.count{
                arrOfPro.append(arrOfPriorityModel[i].strPriorityName)
            }
            vc.aryPayment = arrOfPro
            self.present(vc, animated: false, completion: {})
        }
        else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    
    @objc func actionOnToDate(_ sender: UIButton){
        
        if strSelectedFromDate.isEmpty {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select from date first.", viewcontrol: self)
            
        }else {
            let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
            
            let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.strType = "Date"
            vc.handleDateSelectionForDatePickerProtocol = self
            vc.strTag = 1
            
            vc.chkCustomDate = true
            
            print(strSelectedFromDate)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.dateFormat = "MM/dd/yyyy"
            //            let strDateGlobal = dateFormatter.string(from: dateToComp!)
            
            let dateFrom = dateFormatter.date(from: strSelectedFromDate)
            vc.minDate = dateFrom!
            vc.chkForMaxDate = true
            
            self.present(vc, animated: true, completion: {})
        }
        
        
    }
    
    @objc func actionOnFromDate(_ sender: UIButton){
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.strType = "Date"
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strTag = 2
        vc.chkForMinDate = false
        vc.chkForMaxDate = true
        
        self.present(vc, animated: true, completion: {})
        
    }
    
}

extension HelpTicket_FilterVC: DatePickerProtocol{
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int) {
        if tag == 1{
            strSelectedToDate = strDate
        }
        else if tag == 2{
            strSelectedFromDate = strDate
        }
        tblView.reloadData()
    }
}

class HelpTicket_FilterStatusCell: UITableViewCell {
    
    //MARK: -IBOutlets
    
    
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var btnChecked: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class HelpTicket_FilterProrityCell: UITableViewCell {
    
    //MARK: -IBOutlets
    @IBOutlet weak var btnProrityTitle: UIButton!
    @IBOutlet weak var btnPrority: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnProrityTitle.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class HelpTicket_FilterSelectDateCell: UITableViewCell {
    
    //MARK: -IBOutlets
    @IBOutlet weak var txtFromDate: ACFloatingTextField!
    @IBOutlet weak var txtToDate: ACFloatingTextField!
    
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension HelpTicket_FilterVC: PaymentInfoDetails
{
    func getPaymentInfo(dictData: String, index: Int) {
        
        if(dictData.count == 0){
        }else{
            print(dictData)
            strSelectedPriority = dictData
            tblView.reloadData()
        }
        
    }
}
