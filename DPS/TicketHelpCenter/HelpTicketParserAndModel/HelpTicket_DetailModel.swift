//
//  HelpTicket_DetailModel.swift
//  DPS
//
//  Created by APPLE on 06/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class HelpTicket_DetailModel: NSObject {
    
    var strTaskTimeEntryId      = ""
    var strProjectId            = ""
    var strModuleId             = ""
    var strSubmoduleId          = ""
    var strStatusId             = ""
    var strTaskName             = ""
    var strTaskDescription      = ""
    var strStartDate            = ""
    var strTaskDate             = ""
    var strIsDeleted            = ""
    var strCompletionPercentage = ""
    var strCustomerId           = ""
    var strContactId            = ""
    var strClientPriority       = ""
    var strType                 = ""
    var strIsFeedback           = ""
    var strSource               = ""
    var strIntegrationKey       = ""
    var strProjectName          = ""
    var strStatusName           = ""
    var strCustomerName         = ""
    var arrAttachmentList       = [AttachmentList]()
    var arrCommentList          = [CommentList]()
    var dictThirdPartyUserDetail = ThirdPartyUserDetail()
    var strCreatedDate          = ""
    var strCreatedBy            = ""
    var strModifyDate           = ""
    var strModifyBy             = ""
}

class AttachmentList: NSObject {
    var strAttachmentId         = ""
    var strEntityMasterId       = ""
    var strEntityId             = ""
    var strFileName             = ""
    var strFilePath             = ""
    var strIsDeleted            = ""
    var strCreatedBy            = ""
    var strModifyBy             = ""
    var strFiles                = ""
}

class CommentList: NSObject {
    
    var strTaskCommentId            = ""
    var strEntityMasterId           = ""
    var strEntityId                 = ""
    var strComment                  = ""
    var strIsDeleted                = ""
    var strCreatedBy                = ""
    var strCreatedByName            = ""
    var strModifyBy                 = ""
    var strCreatedByImageUrl        = ""
    var strCreatedDate              = ""
    var strThirdPartyUserName       = ""
    var strIsVisibleComment         = ""
    var arrAttachmentList           = [AttachmentList]()
    var strThirdPartyUserDetail     = ""
    
}

class ThirdPartyUserDetail: NSObject {
    var strTaskThirdPartyUserId = ""
    var strEntityId = ""
    var strEntityMasterId = ""
    var strUserName = ""
    var strName = ""
    var strPhoneNo = ""
    var strEmailId = ""
    var strSource = ""
    var strIntegrationKey = ""
}
