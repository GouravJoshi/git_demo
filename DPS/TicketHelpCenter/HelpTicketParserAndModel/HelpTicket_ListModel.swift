//
//  HelpTicket_ListModel.swift
//  DPS
//
//  Created by APPLE on 02/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class HelpTicket_ListModel: NSObject {
    var strTaskTimeEntryId      = ""
    var strTaskName             = ""
    var strTaskDescription      = ""
    var strStartDate            = ""
    var strTaskDate             = ""
    var strCompletionPercentage = ""
    var strClientPriority       = ""
    var strPriority             = ""
    var strType                 = ""
    var strCreatedDate          = ""
    var strCreatedByName        = ""
    var strModifyDate           = ""
    var strModifyByName         = ""
    var strProjectName          = ""
    var strModuleName           = ""
    var strEmployeeName         = ""
    var strStatusName           = ""
}
