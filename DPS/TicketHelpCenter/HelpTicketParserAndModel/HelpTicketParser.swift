//
//  HelpTicketPaarser.swift
//  DPS
//
//  Created by INFOCRATS on 02/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

class HelpTicketParser: NSObject {
    
    class func parseHelpTicketStatusFilterJSON(response : NSDictionary, completionHandler: @escaping ([HelpTicket_StatusModel]) -> Void) {
        
        var statusModel = [HelpTicket_StatusModel]()
        let tempResponse =  response["data"] as! [String:Any]
        let objStauts = tempResponse["StatusLists"] as! [[String:Any]]
        for i in 0..<objStauts.count{
            let obj = HelpTicket_StatusModel()
            
            obj.strStatusId = String(describing: objStauts[i]["StatusId"] as? NSNumber ?? 0)
            obj.strStatusName = objStauts[i]["StatusName"] as? String ?? ""
            
            statusModel.append(obj)
        }
        completionHandler(statusModel)
    }
    
    class func parseHelpTicketPriortyFilterJSON(response : NSDictionary, completionHandler: @escaping ([HelpTicket_PriorityModel]) -> Void) {
        
        var ProrityModel = [HelpTicket_PriorityModel]()
        let tempResponse =  response["data"] as! [String:Any]
        let objStauts = tempResponse["ClientPriorityList"] as! [String]
        
        for i in 0..<objStauts.count{
            let obj = HelpTicket_PriorityModel()
            obj.strPriorityName = objStauts[i]
            ProrityModel.append(obj)
        }
        completionHandler(ProrityModel)
    }
    
    class func parseHelpTicketListJSON(response : NSDictionary, completionHandler: @escaping ([HelpTicket_ListModel]) -> Void) {
        
        var ListModel = [HelpTicket_ListModel]()
        let tempResponse =  response["data"] as! [String:Any]
        let objTicketList = tempResponse["ticketDc"] as? [[String: Any]] ?? [[:]]
        
        for i in 0..<objTicketList.count{
            let obj = HelpTicket_ListModel()
            obj.strTaskTimeEntryId = String(describing: objTicketList[i]["TaskTimeEntryId"] as? NSNumber ?? 0)
            obj.strTaskName = objTicketList[i]["TaskName"] as? String ?? ""
            obj.strTaskDescription = objTicketList[i]["TaskDescription"] as? String ?? ""
            obj.strStartDate = objTicketList[i]["StartDate"] as? String ?? ""
            obj.strTaskDate = objTicketList[i]["TaskDate"] as? String ?? ""
            obj.strCompletionPercentage = objTicketList[i]["CompletionPercentage"] as? String ?? ""
            obj.strClientPriority = objTicketList[i]["ClientPriority"] as? String ?? ""
            obj.strPriority = objTicketList[i]["Priority"] as? String ?? ""
            obj.strType = objTicketList[i]["Type"] as? String ?? ""
            obj.strCreatedDate = objTicketList[i]["CreatedDate"] as? String ?? ""
            obj.strCreatedByName = objTicketList[i]["CreatedByName"] as? String ?? ""
            obj.strModifyDate = objTicketList[i]["ModifyDate"] as? String ?? ""
            obj.strModifyByName = objTicketList[i]["ModifyByName"] as? String ?? ""
            obj.strProjectName = objTicketList[i]["ProjectName"] as? String ?? ""
            obj.strModuleName = objTicketList[i]["ModuleName"] as? String ?? ""
            obj.strEmployeeName = objTicketList[i]["EmployeeName"] as? String ?? ""
            obj.strStatusName = objTicketList[i]["StatusName"] as? String ?? ""

            ListModel.append(obj)
        }
        completionHandler(ListModel)
    }
    
    class func parseHelpTicketDetailJSON(response : NSDictionary, completionHandler: @escaping ([HelpTicket_DetailModel]) -> Void) {
        
        var ProrityModel = [HelpTicket_DetailModel]()
        let objTicketDetail =  response["data"] as! [String:Any]
        // let objTicketDetail = tempResponse
        
        //        for i in 0..<objTicketDetail.count{
        let obj = HelpTicket_DetailModel()
        
        obj.strTaskTimeEntryId      = String(describing: objTicketDetail["TaskTimeEntryId"] as? NSNumber ?? 0)
        obj.strProjectId            = String(describing: objTicketDetail["ProjectId"] as? NSNumber ?? 0)
        obj.strModuleId             = String(describing: objTicketDetail["ModuleId"] as? NSNumber ?? 0)
        obj.strSubmoduleId          = String(describing: objTicketDetail["SubmoduleId"] as? NSNumber ?? 0)
        obj.strStatusId             = String(describing: objTicketDetail["StatusId"] as? NSNumber ?? 0)
        obj.strTaskName             = objTicketDetail["TaskName"] as? String ?? ""
        obj.strTaskDescription      = objTicketDetail["TaskDescription"] as? String ?? ""
        obj.strStartDate            = objTicketDetail["StartDate"] as? String ?? ""
        obj.strTaskDate             = objTicketDetail["TaskDate"] as? String ?? ""
        obj.strIsDeleted            = objTicketDetail["IsDeleted"] as? String ?? ""
        obj.strCompletionPercentage = objTicketDetail["CompletionPercentage"] as? String ?? ""
        obj.strCustomerId           = String(describing: objTicketDetail["CustomerId"] as? NSNumber ?? 0)
        obj.strContactId            = String(describing: objTicketDetail["ContactId"] as? NSNumber ?? 0)
        obj.strClientPriority       = objTicketDetail["ClientPriority"] as? String ?? ""
        obj.strType                 = objTicketDetail["Type"] as? String ?? ""
        obj.strIsFeedback           = objTicketDetail["IsFeedback"] as? String ?? ""
        obj.strSource               = objTicketDetail["Source"] as? String ?? ""
        obj.strIntegrationKey       = objTicketDetail["IntegrationKey"] as? String ?? ""
        obj.strProjectName          = objTicketDetail["ProjectName"] as? String ?? ""
        obj.strStatusName           = objTicketDetail["StatusName"] as? String ?? ""
        obj.strCustomerName         = objTicketDetail["CustomerName"] as? String ?? ""
        
        let objAttachment = objTicketDetail["AttachmentList"] as? [[String: Any]]
        if (objAttachment?.count ?? 0) > 0{
            for i in 0..<objAttachment!.count{
                let objA = AttachmentList()
                
                objA.strAttachmentId         = String(describing: objAttachment![i]["AttachmentId"] as? NSNumber ?? 0)
                objA.strEntityMasterId       = String(describing: objAttachment![i]["EntityMasterId"] as? NSNumber ?? 0)
                objA.strEntityId             = String(describing: objAttachment![i]["EntityId"] as? NSNumber ?? 0)
                objA.strFileName             = objAttachment![i]["FileName"] as? String ?? ""
                objA.strFilePath             = objAttachment![i]["FilePath"] as? String ?? ""
                objA.strIsDeleted            = objAttachment![i]["IsDeleted"] as? String ?? ""
                objA.strCreatedBy            = objAttachment![i]["CreatedBy"] as? String ?? ""
                objA.strModifyBy             = objAttachment![i]["ModifyBy"] as? String ?? ""
                objA.strFiles                = objAttachment![i]["Files"] as? String ?? ""
                
                obj.arrAttachmentList.append(objA)
            }
        }
        
        let objrComment = objTicketDetail["CommentList"] as? [[String: Any]]
        if (objrComment?.count ?? 0) > 0{
            for i in 0..<objrComment!.count{
                let objC = CommentList()
                
                objC.strTaskCommentId              = String(describing: objrComment![i]["AttachmentId"] as? NSNumber ?? 0)
                objC.strEntityMasterId             = String(describing: objrComment![i]["EntityMasterId"] as? NSNumber ?? 0)
                objC.strEntityId                   = String(describing: objrComment![i]["EntityId"] as? NSNumber ?? 0)
                objC.strComment                    = objrComment![i]["Comment"] as? String ?? ""
                objC.strIsDeleted                  = objrComment![i]["IsDeleted"] as? String ?? ""
                objC.strCreatedBy                  = objrComment![i]["CreatedBy"] as? String ?? ""
                objC.strCreatedByName              = objrComment![i]["CreatedByName"] as? String ?? ""
                objC.strModifyBy                   = objrComment![i]["ModifyBy"] as? String ?? ""
                objC.strCreatedByImageUrl          = objrComment![i]["CreatedByImageUrl"] as? String ?? ""
                objC.strCreatedDate                = objrComment![i]["CreatedDate"] as? String ?? ""
                objC.strThirdPartyUserName         = objrComment![i]["ThirdPartyUserName"] as? String ?? ""
                objC.strIsVisibleComment           = objrComment![i]["IsVisibleComment"] as? String ?? ""
                
                let objAttachment1 = objrComment![i]["AttachmentList"] as? [[String: Any]]
                if (objAttachment1?.count ?? 0) > 0{
                    for i in 0..<objAttachment1!.count{
                        let objA = AttachmentList()
                        
                        objA.strAttachmentId         = String(describing: objAttachment1![i]["AttachmentId"] as? NSNumber ?? 0)
                        objA.strEntityMasterId       = String(describing: objAttachment1![i]["EntityMasterId"] as? NSNumber ?? 0)
                        objA.strEntityId             = String(describing: objAttachment1![i]["EntityId"] as? NSNumber ?? 0)
                        objA.strFileName             = objAttachment1![i]["FileName"] as? String ?? ""
                        objA.strFilePath             = objAttachment1![i]["FilePath"] as? String ?? ""
                        objA.strIsDeleted            = objAttachment1![i]["IsDeleted"] as? String ?? ""
                        objA.strCreatedBy            = objAttachment1![i]["CreatedBy"] as? String ?? ""
                        objA.strModifyBy             = objAttachment1![i]["ModifyBy"] as? String ?? ""
                        objA.strFiles                = objAttachment1![i]["Files"] as? String ?? ""
                        objC.arrAttachmentList.append(objA)
                    }
                }
                objC.strThirdPartyUserDetail       = objrComment![i]["ThirdPartyUserDetail"] as? String ?? ""
                
                obj.arrCommentList.append(objC)
            }
        }
        
        
        let objThirdParty = objTicketDetail["ThirdPartyUserDetail"] as? [String: Any]
        if (objThirdParty?.count ?? 0) > 0{
            let objTh = ThirdPartyUserDetail()
            objTh.strTaskThirdPartyUserId   = String(describing: objThirdParty!["TaskThirdPartyUserId"] as? NSNumber ?? 0)
            objTh.strEntityId               = String(describing: objThirdParty!["EntityId"] as? NSNumber ?? 0)
            objTh.strEntityMasterId         = String(describing: objThirdParty!["EntityMasterId"] as? NSNumber ?? 0)
            objTh.strUserName               = objThirdParty!["UserName"] as? String ?? ""
            objTh.strName                   = objThirdParty!["Name"] as? String ?? ""
            objTh.strPhoneNo                = objThirdParty!["PhoneNo"] as? String ?? ""
            objTh.strEmailId                = objThirdParty!["EmailId"] as? String ?? ""
            objTh.strSource                 = objThirdParty!["Source"] as? String ?? ""
            objTh.strIntegrationKey         = objThirdParty!["IntegrationKey"] as? String ?? ""
            
            obj.dictThirdPartyUserDetail = objTh
        }
        
        
        obj.strCreatedDate          = objTicketDetail["CreatedDate"] as? String ?? ""
        obj.strCreatedBy            = objTicketDetail["CreatedBy"] as? String ?? ""
        obj.strModifyDate           = objTicketDetail["ModifyDate"] as? String ?? ""
        obj.strModifyBy             = objTicketDetail["ModifyBy"] as? String ?? ""
        ProrityModel.append(obj)
        //        }
        completionHandler(ProrityModel)
    }
    
}
