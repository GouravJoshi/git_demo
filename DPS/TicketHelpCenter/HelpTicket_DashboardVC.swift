//
//  HelpTicket_Dashboard.swift
//  DPS
//
//  Created by INFOCRATS on 30/11/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class HelpTicket_DashboardVC: UIViewController, HelpTicketFilter, HelpTicketClose {

    //MARK: - IBOutlets
    @IBOutlet weak var btnOpen: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblOpenBar: UILabel!
    @IBOutlet weak var lblCloseBar: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAddTicket: UIButton!
    @IBOutlet weak var btnFilter: UIButton!

    var loader = UIAlertController()
    var arrOfTicketListToShow = [HelpTicket_ListModel]()
    var arrOfTicketListOpen = [HelpTicket_ListModel]()
    var arrOfTicketListClose = [HelpTicket_ListModel]()

    var isOpen = true
    let refreshControl = UIRefreshControl()
    var strPriorityFilter   = ""
    var strToDateFilter     = ""
    var strFromDateFilter   = ""
    

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblCloseBar.isHidden = true
        lblOpenBar.isHidden = false
        
        // Do any additional setup after loading the view.
        //Call API
        self.callWebServiceToGetTicketList()
        self.setupViews()
        self.deleteData()
    }
    
    func setupViews()
    {

        if #available(iOS 13.0, *) {
            searchBar.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
        }
        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
        }
        searchBar.layer.borderWidth = 0
        searchBar.layer.opacity = 1.0
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tblView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
       // Code to refresh table view
        self.callWebServiceToGetTicketList()
    }
    
    //MARK: - IBActions
    @IBAction func btnBackAction(_ sender: UIButton) {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func btnOpenAction(_ sender: UIButton) {
        lblCloseBar.isHidden = true
        lblOpenBar.isHidden = false
        isOpen = true
        self.searchBar.text = ""
        self.view.endEditing(true)
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
        
        self.arrOfTicketListToShow.removeAll()
        self.replaceArray()
        self.filterAutocomplete(priority: self.strPriorityFilter, toDate: self.strToDateFilter, fromDate: self.strFromDateFilter)

        
//        self.tblView.reloadData()
//        self.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        lblOpenBar.isHidden = true
        lblCloseBar.isHidden = false
        isOpen = false
        self.searchBar.text = ""
        self.view.endEditing(true)

        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
        
        self.arrOfTicketListToShow.removeAll()
        self.replaceArray()
        self.filterAutocomplete(priority: self.strPriorityFilter, toDate: self.strToDateFilter, fromDate: self.strFromDateFilter)

//        self.tblView.reloadData()
//        self.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)

    }
    
    @IBAction func btnFilterAction(_ sender: UIButton) {
        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_FilterVC") as? HelpTicket_FilterVC
        
        objByProductVC!.delegateFiler = self
        objByProductVC!.strSelectedPriority = self.strPriorityFilter
        objByProductVC!.strSelectedToDate = self.strToDateFilter
        objByProductVC!.strSelectedFromDate = self.strFromDateFilter

        self.navigationController?.pushViewController(objByProductVC!, animated: true)
    }
    
    @IBAction func btnAddTicketAction(_ sender: UIButton) {
        self.strPriorityFilter = ""
        self.strToDateFilter   = ""
        self.strFromDateFilter = ""
        
        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_AddTicketVC") as? HelpTicket_AddTicketVC
        objByProductVC?.delegateTicketDetail = self
        self.navigationController?.pushViewController(objByProductVC!, animated: true)
    }
    
    @IBAction func actionOnClickVoiceRecognizationButton(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.arrOfTicketListToShow.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }
    
    func refreshDashboard(isAddTicket: Bool) {
        if isAddTicket {
            lblCloseBar.isHidden = true
            lblOpenBar.isHidden = false
            isOpen = true
        }else {
            lblOpenBar.isHidden = true
            lblCloseBar.isHidden = false
            isOpen = false
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.callWebServiceToGetTicketList()
        }
    }
    
    func refreshDashboardOnBack() {
        self.replaceArray()
        self.filterAutocomplete(priority: self.strPriorityFilter, toDate: self.strToDateFilter, fromDate: self.strFromDateFilter)
    }
    
    func filterOnTicket(strPriority: String, strToDate: String, strFromDate: String) {
        print(strPriority)
        print(strToDate)
        print(strFromDate)
        
        self.strPriorityFilter = strPriority
        self.strToDateFilter   = strToDate
        self.strFromDateFilter = strFromDate
        
        self.arrOfTicketListToShow.removeAll()
        
        self.replaceArray()
        self.filterAutocomplete(priority: self.strPriorityFilter, toDate: self.strToDateFilter, fromDate: self.strFromDateFilter)
    }
    
    func deleteData() {
        UserDefaults.standard.set([], forKey: "HelpTicket_CommentDocumentsName")
        UserDefaults.standard.set([], forKey: "HelpTicket_CommentDocumentsType")
    }
    
    //MARK: -Call Web service
    func callWebServiceToGetTicketList(){
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            
            let strURL = String(format: URL.HelpTicket.getTicketList)
            
            WebService.postRequestWithHeaders(dictJson: getPostData(), url: strURL, responseStringComing: "TimeLine") { [self] (response, status) in
                if(status)
                {
                    self.loader.dismiss(animated: false) {
                        HelpTicketParser.parseHelpTicketListJSON(response: response, completionHandler: { (obj) in
                            // print(obj)
                            let arrOfTicketListModel = obj
                            if arrOfTicketListModel.count > 0{
                                
                                self.arrOfTicketListOpen = arrOfTicketListModel.filter { ($0.strStatusName.contains("Open"))}
                                self.arrOfTicketListClose = arrOfTicketListModel.filter { ($0.strStatusName.contains("Close"))}
                                
                                //Show Open Ticket first
                                self.arrOfTicketListToShow.removeAll()
                                self.replaceArray()
                                self.tblView.reloadData()
//                                    self.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                            }
                            
                        })
                    }
                }
                else{
                    self.loader.dismiss(animated: false) {
                        print("failed")
                    }
                }
                refreshControl.endRefreshing()
            }
        }
    }
    
    func replaceArray() {
        
        if isOpen {
            self.arrOfTicketListToShow = self.arrOfTicketListOpen
        }else{
            self.arrOfTicketListToShow = self.arrOfTicketListClose
        }
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        
        
        //if needed filter with modifid date should be there: strCreatedDate = strModifyDate
        self.arrOfTicketListToShow.sort { (firstItem, secondItem) -> Bool in
            let dateAString  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: firstItem.strCreatedDate)
            let dateBString  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: secondItem.strCreatedDate)
            
            let dateA = formatter.date(from: dateAString!)
            let dateB = formatter.date(from: dateBString!)

            if dateA != nil && dateB != nil {
                return dateA!.compare(dateB!) == .orderedDescending
            }
            return false
        }
        
        self.loader.dismiss(animated: false) {}
    }
    
    func getPostData() -> NSDictionary
    {
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("StatusIds")
        arrOfKeys.add("IntegrationKey")
        arrOfKeys.add("UserName")

        arrOfValues.add("")
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getEmployeeId())
        
        
        let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
                
        return dict_ToSend as NSDictionary
    }
}

extension HelpTicket_DashboardVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.arrOfTicketListToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTicket_DashboardCell") as! HelpTicket_DashboardCell
        
        cell.selectionStyle = .none
        
        cell.setTicketListValues(objTicket: self.arrOfTicketListToShow[indexPath.row])
        
        
       
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.strPriorityFilter = ""
        self.strToDateFilter   = ""
        self.strFromDateFilter = ""
        
        let objTicket : HelpTicket_ListModel = self.arrOfTicketListToShow[indexPath.row]
        print(objTicket.strTaskTimeEntryId)
        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_TicketDetailVC") as? HelpTicket_TicketDetailVC
        objByProductVC?.strTicketId = "\(objTicket.strTaskTimeEntryId)"
        objByProductVC!.delegateTicketDetail = self

        self.navigationController?.pushViewController(objByProductVC!, animated: true)
    }
}

extension  HelpTicket_DashboardVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.text = ""
        //const_ViewTop_H.constant = 60
        //const_SearchBar_H.constant = 0
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
    }
    
    func searchAutocomplete(searchText: String) -> Void{
       
        if searchText.isEmpty {
            
            self.arrOfTicketListToShow.removeAll()
            self.replaceArray()
            self.tblView.reloadData()

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.resignFirstResponder()
            }
            
        }
        
        else
        {
            
            var aryTaskss = [HelpTicket_ListModel]()
            
            for (index, _) in self.arrOfTicketListToShow.enumerated() {
                let dictDataTicket = self.arrOfTicketListToShow[index]
                aryTaskss.append(dictDataTicket)
            }
            
            let aryTaskTemp = aryTaskss.filter { (activity) -> Bool in
                let objTicket: HelpTicket_ListModel = activity
                return objTicket.strTaskName.lowercased().contains(searchText.lowercased()) || objTicket.strTaskDescription.lowercased().contains(searchText.lowercased())
            }
            
            var aryFilter = [HelpTicket_ListModel]()
            for item in aryTaskTemp {
                aryFilter.append(item)
            }
            
            self.arrOfTicketListToShow.removeAll()
            self.arrOfTicketListToShow = aryFilter
            
            self.tblView.reloadData()
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchAutocomplete(searchText: searchText)
        
    }
    
    func filterAutocomplete(priority: String, toDate: String, fromDate: String) {
        
        if priority.isEmpty {
            
            self.arrOfTicketListToShow.removeAll()
            self.replaceArray()
            
            if !toDate.isEmpty && !fromDate.isEmpty {
                var aryFilter = [HelpTicket_ListModel]()
                for item in arrOfTicketListToShow {
                    print(item.strCreatedDate)
                    let strCreatedDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: item.strCreatedDate)
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy hh:mm a"
                    let dateToComp = formatter.date(from: strCreatedDate!)

                    let dateFormatter = DateFormatter()
                    dateFormatter.dateStyle = DateFormatter.Style.short
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    let strDateGlobal = dateFormatter.string(from: dateToComp!)
                    
                    let dateToCompair = dateFormatter.date(from: strDateGlobal)
                    let dateTo = dateFormatter.date(from: toDate)
                    let dateFrom = dateFormatter.date(from: fromDate)
                    
                    //Compair date
                    if dateToCompair != nil && dateTo != nil && dateFrom != nil {
                        if dateToCompair! >= dateFrom! && dateToCompair! <= dateTo! {
                            aryFilter.append(item)
                        }
                    }else {
                        aryFilter.append(item)
                    }

                    
                }
                
                self.arrOfTicketListToShow.removeAll()
                self.arrOfTicketListToShow = aryFilter
                
            }
            self.tblView.reloadData()

            
            
        }else {
            var aryTaskss = [HelpTicket_ListModel]()
            
            for (index, _) in self.arrOfTicketListToShow.enumerated() {
                let dictDataTicket = self.arrOfTicketListToShow[index]
                aryTaskss.append(dictDataTicket)
            }
            
            let aryTaskTemp = aryTaskss.filter { (activity) -> Bool in
                let objTicket: HelpTicket_ListModel = activity
                
                return objTicket.strClientPriority.lowercased().contains(priority.lowercased())
            }
            
            var aryFilter = [HelpTicket_ListModel]()
            for item in aryTaskTemp {
                print(item.strCreatedDate)
                let strCreatedDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: item.strCreatedDate)
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy hh:mm a"
                let dateToComp = formatter.date(from: strCreatedDate!)

                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = DateFormatter.Style.short
                dateFormatter.timeStyle = DateFormatter.Style.none
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let strDateGlobal = dateFormatter.string(from: dateToComp!)
                
                let dateToCompair = dateFormatter.date(from: strDateGlobal)
                let dateTo = dateFormatter.date(from: toDate)
                let dateFrom = dateFormatter.date(from: fromDate)
                //Compair date
                if dateToCompair != nil && dateTo != nil && dateFrom != nil {
                    if dateToCompair! >= dateFrom! && dateToCompair! <= dateTo! {
                        aryFilter.append(item)
                    }
                }else {
                    aryFilter.append(item)
                }

                
            }
            
            self.arrOfTicketListToShow.removeAll()
            self.arrOfTicketListToShow = aryFilter
            
            self.tblView.reloadData()
        }
        
    //    self.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
    }
}

class HelpTicket_DashboardCell: UITableViewCell {

    //MARK: -IBOutlets
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblPriorityTitle: UILabel!
    @IBOutlet weak var viewSpaceConstrint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTicketListValues(objTicket : HelpTicket_ListModel)
    {
        lblTitle.text = objTicket.strTaskName.capitalizingFirstLetter()
        lblDescription.text = objTicket.strTaskDescription.capitalizingFirstLetter()
        lblStatus.text = objTicket.strStatusName.capitalizingFirstLetter()
        lblPriority.text = objTicket.strClientPriority.capitalizingFirstLetter()
        
        let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: objTicket.strCreatedDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let Date1 = formatter.date(from: scheduleStartDate!)
        let strDate = formatter.string(from: Date1!)
        lblDate.text = strDate
        
        if lblStatus.text == "Close" {
            //priorityView.isHidden = true
            lblPriority.text = ""
            lblPriorityTitle.text = ""
            //viewSpaceConstrint.constant = 0
        }else{
            lblPriorityTitle.text = "Priority: "
           // viewSpaceConstrint.constant = 20
        }
        
        if objTicket.strClientPriority.lowercased() == "Critical".lowercased() {
            lblPriority.textColor = UIColor(hex: "cb5a5e")
        }
        else if objTicket.strClientPriority.lowercased() == "High".lowercased(){
            lblPriority.textColor = UIColor(hex: "cb5a5e")
        }
        else if objTicket.strClientPriority.lowercased() == "Urgent".lowercased(){
            lblPriority.textColor = UIColor(hex: "cb5a5e")
        }
        else  if objTicket.strClientPriority.lowercased() == "Low".lowercased(){
            lblPriority.textColor = UIColor(hex: "26a69a")
            
        }
        else  if objTicket.strClientPriority.lowercased() == "Medium".lowercased(){
            lblPriority.textColor = UIColor(hex: "daae2b")
        }
        
    }

}

// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension HelpTicket_DashboardVC : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if #available(iOS 13.0, *) {
            self.searchBar.searchTextField.text = str
        } else {
            self.searchBar.text = str
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.searchAutocomplete(searchText: str)
        }
    }
}
