//
//  HelpTicket_WebServiceHelperVC.swift
//  DPS
//
//  Created by Ruchika Verma on 09/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//


import UIKit
import Alamofire

class HelpTicket_WebServiceHelper: NSObject {
    
    
    func callWebServiceToPostDictInMultipart(_ arrOfImage: [Data],_ arrOfImageNames: [String],_ arrofDocumentType: [String], _ url: String, _ param: [String: Any], _ callBack:@escaping (_ response: String?, _ error:String?) -> Void) {
        
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in param {
                if let subArray = value as? NSDictionary {
                    do {
                        if(key == "RequestDc"){
                            let data = try JSONSerialization.data(withJSONObject: value, options: [])
                            let strReqiuestDC = String(data: data, encoding: String.Encoding.utf8)!
                            print(strReqiuestDC)
                            multiPart.append((strReqiuestDC as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                        }
                        else{
                            print("some other key")
                        }
                    }
                    catch {
                        print("error msg")
                    }
                }
               
            }
            
            if arrOfImage.count > 0{
                for i in 0..<arrOfImage.count{
                        multiPart.append(arrOfImage[i], withName: "\(arrOfImageNames[i])", fileName: "\(arrOfImageNames[i])", mimeType: "image/png") // application/octet-stream
                    }
                }
            
            
        }, to: url)
        .responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print("Response for upload data =================================== \(value)")
                if value is NSNumber{
                    let dict = String(describing:  value as? NSNumber ?? 0)
                    callBack(dict, nil)
                }
                break
            case .failure(let error):
                print(error)
                callBack(nil, error.localizedDescription)
                break
            }
        }
    }
    
    func callWebServiceToPostImageInMultipart(_ arrOfImage: UIImage, _ currentFileName: String, _ url: String, _ param: [String: Any], _ callBack:@escaping (_ response: NSDictionary?, _ error:String?) -> Void) {
        
        AF.upload(multipartFormData: { multiPart in
            
            if let imageData = arrOfImage.jpegData(compressionQuality: 1.0) {
               multiPart.append(imageData, withName: "document", fileName: "\(currentFileName)", mimeType: "image/png")
            }
            for (key, value) in param {
                multiPart.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
            
        }, to: url)
        .responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print(value)
                if value is NSDictionary{
                    let dict = value as! NSDictionary
                    callBack(dict, nil)
                }
                break
            case .failure(let error):
                print(error)
                callBack(nil, error.localizedDescription)
                break
            }
        }
    }
    
}
