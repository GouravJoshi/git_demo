//
//  HelpTicket_AddTicketVC.swift
//  DPS
//
//  Created by INFOCRATS on 01/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class HelpTicket_AddTicketVC: UIViewController  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var txtTitle: ACFloatingTextField!
    @IBOutlet weak var txtPriority: ACFloatingTextField!
    @IBOutlet weak var txtInputFile: ACFloatingTextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var collectionViewAttchments: UICollectionView!
    
    //MARK: - Variable
    var charLenth = 0
    var arrOfDocumentNames = [String]()
    var arrOfDocumentType = [String]()
    var arrOfPriorityModel = [HelpTicket_PriorityModel]()
    var strSelectedPriority = ""
    weak var delegateTicketDetail : HelpTicketClose?

    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewAttchments.delegate = self
        collectionViewAttchments.dataSource = self
        callWebServiceToGetPriorityList()
        deleteData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getDataFromUD()
        collectionViewAttchments.reloadData()
    }
    
    //MARK: - IBActions
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        if(Validation() != ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Validation(), viewcontrol: self)
        }else{
            callWebServiceToAddTicket()
        }
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.delegateTicketDetail?.refreshDashboardOnBack()
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnAttachFile(_ sender: UIButton) {
        
        let storyboardIpad = UIStoryboard.init(name: "TicketHelpCenter", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "HelpTicket_AddDocumentVC") as? HelpTicket_AddDocumentVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    @IBAction func btnPriortiyAction(_ sender: UIButton) {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "-----Select-----"
        vc.strTag = 6001
        if arrOfPriorityModel.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelPaymentTable = self
            var arrOfPro = [String]()
            for i in 0..<arrOfPriorityModel.count{
                arrOfPro.append(arrOfPriorityModel[i].strPriorityName)
            }
            vc.aryPayment = arrOfPro
            self.present(vc, animated: false, completion: {})
        }
        else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    
    //MARK: - Custom functions
    func deleteData() {
        
        if arrOfDocumentNames.count > 0{
            for i in 0..<arrOfDocumentNames.count{
                removeImageFromDirectory(itemName: arrOfDocumentNames[i])
            }
        }
        UserDefaults.standard.set([], forKey: "HelpTicket_CommentDocumentsName")
        UserDefaults.standard.set([], forKey: "HelpTicket_CommentDocumentsType")
        
    }
    
    func Validation() -> String {
        
        let tempTitle = txtTitle.text?.trimmingCharacters(in: .whitespaces)
        let tempDecription = txtViewDescription.text?.trimmingCharacters(in: .whitespaces)

        if tempTitle?.count == 0 {
            return "Title required"
        }
        
        if txtPriority.text?.count == 0 {
            return "Priority required"
        }
        
//        if tempDecription?.count == 0 {
//            return "Description required"
//        }
        return ""
    }
    
    func getAttachemnetList() -> [[String:Any]]{
        
        var arrOfAttachementList = [[String: Any]]()
        
        for i in 0..<arrOfDocumentNames.count{
            let dictAttachment  =
            [
                "AttachmentId": "0",
                "FileName": arrOfDocumentNames[i],
                "FilePath": "",
                "IsDeleted": "false"
            ]
            as [String:Any]
            
            arrOfAttachementList.append(dictAttachment)
        }
        
        return arrOfAttachementList
    }
    
    func getPostData() -> [String: Any]{
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let  strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        let  strEmpEmail = "\(dictLoginData.value(forKey: "EmployeeEmail") ?? "")"
        let  strEmpNumber = "\(dictLoginData.value(forKey: "EmployeePrimaryPhone") ?? "")"
        
        let dictOfThirdPartyUserDetail  = [
            "UserName": Global().getEmployeeId() ,
            "Name": strEmpName,
            "PhoneNo": strEmpNumber,
            "EmailId": strEmpEmail
        ] as [String:Any]
        
        let param = [ "TaskTimeEntryId": "0",
                      "StatusId": "1",
                      "TaskName": txtTitle.text!.trimmingCharacters(in: .whitespaces),
                      "ClientPriority" : txtPriority.text!,
                      "TaskDescription": txtViewDescription.text!.trimmingCharacters(in: .whitespaces),
                      "Source": "Dream Service Software",
                      "IntegrationKey": Global().getCompanyKey(),
                      "ThirdPartyUserDetail": dictOfThirdPartyUserDetail,
                      "AttachmentList": getAttachemnetList()] as [String:Any]
        
        return param
    }
        
    func getDataFromString(arr: [String]) -> [Data]{
        var arrofData = [Data]()
        for i in 0..<arr.count{
            if arrOfDocumentType[i] == "image"{
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: arr[i])
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: arr[i])
                if isImageExists == true{
                    if let imageData = image!.jpegData(compressionQuality: 1.0) {
                        arrofData.append(imageData)
                    }
                }
            }
            else{
                let dataToSend = GetDataFromDocumentDirectory(strFileName: arr[i])
                arrofData.append(dataToSend)
            }
        }
        return arrofData
    }
    
    @objc func actionDeleteAttachement(sender : UIButton) {
        if arrOfDocumentNames.count > 0{
            removeImageFromDirectory(itemName: arrOfDocumentNames[sender.tag])
            arrOfDocumentNames.remove(at: sender.tag)
            arrOfDocumentType.remove(at: sender.tag)
            UserDefaults.standard.set(arrOfDocumentNames, forKey: "HelpTicket_CommentDocumentsName")
            UserDefaults.standard.set(arrOfDocumentType, forKey: "HelpTicket_CommentDocumentsType")
            getDataFromUD()
            self.collectionViewAttchments.reloadData()
        }
    }
    
    func getDataFromUD(){
        arrOfDocumentNames.removeAll()
        arrOfDocumentType.removeAll()
        arrOfDocumentNames = UserDefaults.standard.stringArray(forKey: "HelpTicket_CommentDocumentsName") ?? [String]()
        arrOfDocumentType = UserDefaults.standard.stringArray(forKey: "HelpTicket_CommentDocumentsType") ?? [String]()
    }
    //MARK: - Call web service
    
    func callWebServiceToGetPriorityList(){
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            let strURL = String(format: URL.HelpTicket.getClientPriorityList)
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { [self] (response, status) in
                if(status)
                {
                    loader.dismiss(animated: false) {
                        HelpTicketParser.parseHelpTicketPriortyFilterJSON(response: response, completionHandler: { (obj) in
                            self.arrOfPriorityModel = obj
                            if self.arrOfPriorityModel.count > 0{
                                self.strSelectedPriority = self.arrOfPriorityModel[0].strPriorityName
                            }
                        })
                    }
                }
                else{
                    loader.dismiss(animated: false) {
                        print("failed")
                    }
                }
            }
        }
    }
    func callWebServiceToAddTicket(){
        
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            let strURL = String(format: URL.HelpTicket.addTicket, Global().getCompanyKey())
            
            let finalDict = ["RequestDc": getPostData()]
            
            HelpTicket_WebServiceHelper().callWebServiceToPostDictInMultipart(getDataFromString(arr: arrOfDocumentNames), arrOfDocumentNames, arrOfDocumentType, strURL, finalDict, { (response, error) in
                if response == nil {
                    loader.dismiss(animated: false) {
                        self.navigationController?.popViewController(animated: false)
                        self.deleteData()
                        showAlertWithoutAnyAction(strtitle: "Success", strMessage: "Some error occured", viewcontrol: self)
                    }
                }
                else if response!.count > 0{
                    loader.dismiss(animated: false) {
                        if response != ""{
                            
                            let alert = UIAlertController(title: "Success", message: "Ticket added successfully", preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                
                                self.deleteData()
                                self.delegateTicketDetail?.refreshDashboard(isAddTicket: true)
                                self.navigationController?.popViewController(animated: false)
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                else{
                    loader.dismiss(animated: false) {
                        self.navigationController?.popViewController(animated: false)
                        self.deleteData()
                        showAlertWithoutAnyAction(strtitle: "Success", strMessage: "Failed to add ticket", viewcontrol: self)
                        
                    }
                }
            })
        }
    }
}
extension HelpTicket_AddTicketVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfDocumentNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        cell.btnDeleteAttachment.tag = indexPath.row
        cell.btnDeleteAttachment.addTarget(self, action: #selector(actionDeleteAttachement), for: .touchUpInside)
        
        cell.lblTitle.text =  arrOfDocumentNames[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        if DeviceType.IS_IPAD{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 18.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 38)
        }else{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 14.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 38)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let strDocPathLocal = arrOfDocumentNames[indexPath.row]
        let strDocTypeLocal = arrOfDocumentType[indexPath.row]
        
        
        if strDocTypeLocal == "image" {
            
            let image1: UIImage = UIImage(named: "NoImage.jpg")!
            var imageView = UIImageView(image: image1)
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            if image != nil  {
                imageView = UIImageView(image: image)
            }else {
                imageView = UIImageView(image: image1)
            }
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        } else if strDocTypeLocal == "pdf" || strDocTypeLocal == "xls"{
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            goToPdfView(strPdfName: strImagePath!)
            
        } else if strDocTypeLocal == "audio" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                playAudioo(strAudioName: strImagePath!)
            }else {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
            }
            
        }else if strDocTypeLocal == "video" {
            let strVideoPath = Global().strDocName(fromPath: strDocPathLocal)
            let isVideoExists: Bool? = checkIfImageExistAtPath(strFileName: strVideoPath!)
            
            if isVideoExists!  {
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let documentsDirectory = paths[0]
                let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strVideoPath!)")
                print(path)
                let player = AVPlayer(url: path)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }else {
                
                var vedioUrl = "\(strDocPathLocal)"
                
                vedioUrl = vedioUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let player = AVPlayer(url: URL(string: vedioUrl)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
        else{
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            goToPdfView(strPdfName: strImagePath!)
        }
        
    }
}


//MARK: - Extension for Documents
extension HelpTicket_AddTicketVC: UIDocumentPickerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func playAudioo(strAudioName : String) {
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
    
    func goToPdfView(strPdfName : String) {
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
}

//MARK: - Extension for picker of prority
extension HelpTicket_AddTicketVC: PaymentInfoDetails
{
    func getPaymentInfo(dictData: String, index: Int) {
        
        if(dictData.count == 0){
        }else{
            print(dictData)
            strSelectedPriority = dictData
            txtPriority.text = strSelectedPriority
        }
    }
}
extension HelpTicket_AddTicketVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if newText.count > 5000{
            print("jyada hai")
        }
        else{
            lblCount.text =  "\(newText.count)/5000"
        }
        return newText.count < 5000
    }
    
}
