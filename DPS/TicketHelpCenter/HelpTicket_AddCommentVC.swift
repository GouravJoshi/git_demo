//
//  HelpTicket_AddCommentVC.swift
//  DPS
//
//  Created by INFOCRATS on 26/11/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol HelpTicketAddCommentDelegate: class {
    func refreshAfterAddComment()
}

class HelpTicket_AddCommentVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var txtViewComment: UITextView!
    @IBOutlet weak var txtInputFile: ACFloatingTextField!
    @IBOutlet weak var lblCommentplaceholder :UILabel!
    @IBOutlet weak var btnSubmitComment: UIButton!
    @IBOutlet weak var collectionViewAttchments: UICollectionView!
    @IBOutlet weak var lblCount: UILabel!

    //MARK: - Variable
    var charLenth = 0
    var arrOfDocumentNames = [String]()
    var arrOfDocumentType = [String]()
    var strEntityId = ""
    weak var delegateAddComment : HelpTicketAddCommentDelegate?

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        txtViewComment.delegate = self
        lblCommentplaceholder.isHidden = false
        lblCommentplaceholder.text = "Write your comment here.."
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getDataFromUD()
        collectionViewAttchments.reloadData()
    }
    
    //MARK: - IBActions
    @IBAction func btnSubmitCommentAction(_ sender: UIButton) {
        if(Validation() != ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Validation(), viewcontrol: self)
        }else{
           callWebServiceToAddComment()
        }
    }
    
    @IBAction func btnAttachFile(_ sender: UIButton) {

        let storyboardIpad = UIStoryboard.init(name: "TicketHelpCenter", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "HelpTicket_AddDocumentVC") as? HelpTicket_AddDocumentVC
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)

    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        deleteData()
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK: - Custom functions
    func deleteData() {
        
        if arrOfDocumentNames.count > 0{
            for i in 0..<arrOfDocumentNames.count{
                removeImageFromDirectory(itemName: arrOfDocumentNames[i])
            }
        }
        UserDefaults.standard.set([], forKey: "HelpTicket_CommentDocumentsName")
        UserDefaults.standard.set([], forKey: "HelpTicket_CommentDocumentsType")

    }
    
    func Validation() -> String {
        let tempViewComment = txtViewComment.text?.trimmingCharacters(in: .whitespaces)

        if tempViewComment?.count == 0 {
            return "Description required"
        }
        return ""
    }
    
    func getAttachemnetList() -> [[String:Any]]{
        
        var arrOfAttachementList = [[String: Any]]()
        
        for i in 0..<arrOfDocumentNames.count{
            let dictAttachment  =
            [
                "AttachmentId": "0",
                "FileName": arrOfDocumentNames[i],
                "FilePath": "",
                "IsDeleted": "false"
            ]
            as [String:Any]
            
            arrOfAttachementList.append(dictAttachment)
        }
        
        return arrOfAttachementList
    }
    func getPostData() -> [String: Any]{
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let  strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        let  strEmpEmail = "\(dictLoginData.value(forKey: "EmployeeEmail") ?? "")"
        let  strEmpNumber = "\(dictLoginData.value(forKey: "EmployeePrimaryPhone") ?? "")"

        let dictOfThirdPartyUserDetail  = [
            "UserName": Global().getEmployeeId() ,
            "Name": strEmpName,
            "PhoneNo": strEmpNumber,
            "EmailId": strEmpEmail
        ] as [String:Any]
        
        let param = [ "TaskCommentId": "0",
                      "EntityId": strEntityId,
                      "Comment": txtViewComment.text.trimmingCharacters(in: .whitespaces),
                      "IsDeleted": "false",
                      "ThirdPartyUserDetail": dictOfThirdPartyUserDetail,
                      "AttachmentList": getAttachemnetList()] as [String:Any]
        
        return param
    }
    
    func getDataFromString(arr: [String]) -> [Data]{
        var arrofData = [Data]()
        for i in 0..<arr.count{
            if arrOfDocumentType[i] == "image"{
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: arr[i])
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: arr[i])
                if isImageExists == true{
                    if let imageData = image!.jpegData(compressionQuality: 1.0) {
                        arrofData.append(imageData)
                    }
                }
            }
            else{
                let dataToSend = GetDataFromDocumentDirectory(strFileName: arr[i])
                arrofData.append(dataToSend)
            }
        }
        return arrofData
    }
    
    
    //MARK: - Call web service
    func callWebServiceToAddComment(){
        
        let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        let strURL = String(format: URL.HelpTicket.addComment, Global().getCompanyKey())
        
        let finalDict = ["RequestDc": getPostData()]
        
        HelpTicket_WebServiceHelper().callWebServiceToPostDictInMultipart(getDataFromString(arr: arrOfDocumentNames), arrOfDocumentNames, arrOfDocumentType , strURL, finalDict, { (response, error) in
            if response == nil {
                loader.dismiss(animated: false) {
                    self.navigationController?.popViewController(animated: false)
                    self.deleteData()
                    showAlertWithoutAnyAction(strtitle: "Success", strMessage: "Some error occured", viewcontrol: self)
                }
            }
            else if response!.count > 0{
                loader.dismiss(animated: false) {
                    if response != ""{
                        
                        
                        let alert = UIAlertController(title: "Success", message: "Comment added successfully", preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            
                            self.deleteData()
                            self.delegateAddComment?.refreshAfterAddComment()
                            self.navigationController?.popViewController(animated: false)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else{
                loader.dismiss(animated: false) {
                    self.navigationController?.popViewController(animated: false)
                    self.deleteData()
                    showAlertWithoutAnyAction(strtitle: "Success", strMessage: "Failed to add comment", viewcontrol: self)

                }
            }
        })
    }
}

//MARK: - UITextView Delegate
extension HelpTicket_AddCommentVC: UITextViewDelegate{
    
    //MARK: - UItextView Delelgate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        else{
            
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
                if newText.count > 5000{
                    print("jyada hai")
                }
                else{
                    lblCount.text =  "\(newText.count)/5000"
                }
                return newText.count < 5000
            
            
//            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//            let numberOfChars = newText.count
//            charLenth = numberOfChars
//            if newText.count > 0 {
//                self.lblCommentplaceholder.isHidden = true
//            }
//
//            else if newText.count == 0{
//                self.lblCommentplaceholder.isHidden = false
//                self.lblCommentplaceholder.text = "Write your comment here.."
//
//            }
//            return numberOfChars < 1000
        }
        
      
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.lblCommentplaceholder.isHidden = true

    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
        self.lblCommentplaceholder.isHidden = false
        self.lblCommentplaceholder.text = "Write your comment here.."
        }
    }
}

//MARK: - CollectionView functions
extension HelpTicket_AddCommentVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfDocumentNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        cell.lblTitle.text =  arrOfDocumentNames[indexPath.row]
        cell.btnDeleteAttachment.tag = indexPath.row
        cell.btnDeleteAttachment.addTarget(self, action: #selector(actionDeleteAttachement), for: .touchUpInside)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        if DeviceType.IS_IPAD{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 18.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 38)
        }else{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 14.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 38)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        let strDocPathLocal = arrOfDocumentNames[indexPath.row]
        let strDocTypeLocal = arrOfDocumentType[indexPath.row]
        
        
        if strDocTypeLocal == "image" {
            
            let image1: UIImage = UIImage(named: "NoImage.jpg")!
            var imageView = UIImageView(image: image1)
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            if image != nil  {
                imageView = UIImageView(image: image)
            }else {
                imageView = UIImageView(image: image1)
            }
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        } else if strDocTypeLocal == "pdf" || strDocTypeLocal == "xls"{
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            goToPdfView(strPdfName: strImagePath!)
            
        } else if strDocTypeLocal == "audio" {
            
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
            
            if isImageExists!  {
                playAudioo(strAudioName: strImagePath!)
            }else {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
            }
            
        }else if strDocTypeLocal == "video" {
            let strVideoPath = Global().strDocName(fromPath: strDocPathLocal)
            let isVideoExists: Bool? = checkIfImageExistAtPath(strFileName: strVideoPath!)
            
            if isVideoExists!  {
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let documentsDirectory = paths[0]
                let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strVideoPath!)")
                print(path)
                let player = AVPlayer(url: path)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }else {
                
                var vedioUrl = "\(strDocPathLocal)"
                
                vedioUrl = vedioUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let player = AVPlayer(url: URL(string: vedioUrl)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
        else{
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            goToPdfView(strPdfName: strImagePath!)
        }
    }
    
    @objc func actionDeleteAttachement(sender : UIButton) {
        if arrOfDocumentNames.count > 0{
            removeImageFromDirectory(itemName: arrOfDocumentNames[sender.tag])
            arrOfDocumentNames.remove(at: sender.tag)
            arrOfDocumentType.remove(at: sender.tag)
            UserDefaults.standard.set(arrOfDocumentNames, forKey: "HelpTicket_CommentDocumentsName")
            UserDefaults.standard.set(arrOfDocumentType, forKey: "HelpTicket_CommentDocumentsType")
            getDataFromUD()
            self.collectionViewAttchments.reloadData()
        }
    }
    
    func getDataFromUD(){
        arrOfDocumentNames.removeAll()
        arrOfDocumentType.removeAll()
        arrOfDocumentNames = UserDefaults.standard.stringArray(forKey: "HelpTicket_CommentDocumentsName") ?? [String]()
        arrOfDocumentType = UserDefaults.standard.stringArray(forKey: "HelpTicket_CommentDocumentsType") ?? [String]()
    }
}

//MARK: - Extension for Documents
extension HelpTicket_AddCommentVC: UIDocumentPickerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func playAudioo(strAudioName : String) {
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
    
    func goToPdfView(strPdfName : String) {
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
    }
}
