//
//  HelpTicket_PreviewDocument.swift
//  DPS
//
//  Created by INFOCRATS on 29/11/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class HelpTicket_PreviewDocument: UIViewController, WKNavigationDelegate {

    //MARK: - IBOutlets
    var webView: WKWebView!
    var loadCount: Int = 0
    @IBOutlet weak var loadSpinner: UIActivityIndicatorView!
    @IBOutlet weak var viewForWeb: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    var strFileName = ""
    var strFilePath = ""

    //MARK: - Variable

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblTitle.text = strFileName
        
        
        self.loadSpinner.isHidden = true
        self.startLoading()
        webView = WKWebView(frame: CGRect.init(x: 0, y: 0, width: self.viewForWeb.bounds.width, height: self.viewForWeb.bounds.height))
        
        let url = URL(string: strFilePath)
        if url != nil {
            self.webView.load(URLRequest(url: url!))
            self.viewForWeb.addSubview(webView)
            self.webView.navigationDelegate = self
            
            self.webView.topAnchor.constraint(equalTo: self.viewForWeb.topAnchor).isActive = true
            self.webView.leftAnchor.constraint(equalTo: self.viewForWeb.leftAnchor).isActive = true
            self.webView.rightAnchor.constraint(equalTo: self.viewForWeb.rightAnchor).isActive = true
            self.webView.bottomAnchor.constraint(equalTo: self.viewForWeb.bottomAnchor).isActive = true
            self.webView.translatesAutoresizingMaskIntoConstraints = false
        }
        
        self.view.bringSubviewToFront(self.loadSpinner)

    }
    
    //MARK: - WebView Delegate
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loadCount += 1
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

        loadCount -= 1

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            if self!.loadCount == 0 {
                self!.stopLoading()
            }
        }

    }
    
    func startLoading() {
        self.loadSpinner.isHidden = false
        self.loadSpinner.startAnimating()
    }
    
    func stopLoading() {
        self.loadSpinner.stopAnimating()
        self.loadSpinner.isHidden = true
    }
    //MARK: - IBActions
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
    }
    
    @IBAction func btnDriveAction(_ sender: UIButton) {
        
    }
    
}
