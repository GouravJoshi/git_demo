//
//  HelpTicket_EditDetailVC.swift
//  DPS
//
//  Created by INFOCRATS on 26/11/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import AVFoundation

protocol HelpTicketClose: class {
    func refreshDashboard(isAddTicket: Bool)
    func refreshDashboardOnBack()
}

class HelpTicket_TicketDetailVC: UIViewController, HelpTicketAddCommentDelegate, HelpTicketCommentCellDelegate {
    

    //MARK: - IBOutlets
    
    @IBOutlet weak var lblAddedByName       : UILabel!
    @IBOutlet weak var lblAddedDate         : UILabel!
    @IBOutlet weak var lblTicletTitle       : UILabel!
    @IBOutlet weak var txtViewTicketDescription : UITextView!
    @IBOutlet weak var lblTicketStatus      : UILabel!
    @IBOutlet weak var lblTicketProprity    : UILabel!
    @IBOutlet weak var btnClose             : UIButton!
    @IBOutlet weak var btnAddComment        : UIButton!
    @IBOutlet weak var btnEditTicket        : UIButton!
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var tblViewComments: UITableView!
    @IBOutlet weak var heightConstrantForDetailView: NSLayoutConstraint!
    @IBOutlet weak var attachmentHight: NSLayoutConstraint!
    @IBOutlet weak var attachmentTop: NSLayoutConstraint!
    @IBOutlet weak var heightConstrantForTextView: NSLayoutConstraint!
    @IBOutlet weak var heightConstrantForTitleLabel: NSLayoutConstraint!

    var loader = UIAlertController()
    var strTicketId = ""
    var dictTicketDetail = HelpTicket_DetailModel()
    var arrAttachmentList = [AttachmentList]()
    var arrCommentList = [CommentList]()
    var strCloseStatusId = ""
    var isEdited = false
    weak var delegateTicketDetail : HelpTicketClose?

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addDeletegatendDtSources()
        self.deleteData()
        self.txtViewTicketDescription.isEditable = false
        //Call API
        self.callWebServiceToGetStatusList()
        self.callWebServiceToGetTicketDetails()
        
    }
    
    func refreshAfterAddComment() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.isEdited = true
            self.callWebServiceToGetTicketDetails()
        }
    }
    
    //MARK: - Functions
    func addDeletegatendDtSources(){
        
        tblViewComments.delegate = self
        tblViewComments.dataSource = self
    }
    
    func setValuesFromResponse() {
        self.lblAddedByName.text = "By " + self.dictTicketDetail.dictThirdPartyUserDetail.strName.capitalized
        
        let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: dictTicketDetail.strCreatedDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let Date1 = formatter.date(from: scheduleStartDate!)
        let strDate = formatter.string(from: Date1!)
        self.lblAddedDate.text = strDate//self.dictTicketDetail.strCreatedDate
        
        self.lblTicletTitle.text = self.dictTicketDetail.strTaskName.capitalizingFirstLetter()
        self.txtViewTicketDescription.text = self.dictTicketDetail.strTaskDescription.capitalizingFirstLetter()
        self.lblTicketStatus.text = self.dictTicketDetail.strStatusName
        self.lblTicketProprity.text = self.dictTicketDetail.strClientPriority
        
        if dictTicketDetail.strStatusName.capitalizingFirstLetter() == "Close" {
            btnClose.isHidden = true
        }else {
            btnClose.isHidden = false
        }
        
        self.arrAttachmentList = self.dictTicketDetail.arrAttachmentList
        self.arrCommentList = self.dictTicketDetail.arrCommentList
        
        if self.arrAttachmentList.count == 0 {
            self.attachmentHight.constant = 0
            self.attachmentTop.constant = 0
            self.btnAttachment.isHidden = true
        }else {
            self.attachmentHight.constant = 45
            self.attachmentTop.constant = 30
            self.btnAttachment.isHidden = false
        }

        if self.dictTicketDetail.strTaskDescription.capitalizingFirstLetter() == ""{
            txtViewTicketDescription.isHidden = true
            heightConstrantForTextView.constant = 0
            if DeviceType.IS_IPAD {
                heightConstrantForTitleLabel.constant = (lblTicletTitle.text?.height(withConstrainedWidth: lblTicletTitle.frame.size.width, font: UIFont.systemFont(ofSize: 21)))!
                heightConstrantForDetailView.constant = 0 + (lblTicletTitle.text?.height(withConstrainedWidth: lblTicletTitle.frame.size.width, font: UIFont.systemFont(ofSize: 22)))! +  70
            }
            else{
               heightConstrantForTitleLabel.constant = (lblTicletTitle.text?.height(withConstrainedWidth: lblTicletTitle.frame.size.width, font: UIFont.systemFont(ofSize: 21)))!
                heightConstrantForDetailView.constant = 0 + (lblTicletTitle.text?.height(withConstrainedWidth: lblTicletTitle.frame.size.width, font: UIFont.systemFont(ofSize: 21)))! +  60
            }
            
        }
        else{
            txtViewTicketDescription.isHidden = false
            if DeviceType.IS_IPAD {
                
                heightConstrantForTextView.constant = 190
                heightConstrantForTitleLabel.constant = (lblTicletTitle.text?.height(withConstrainedWidth: lblTicletTitle.frame.size.width, font: UIFont.systemFont(ofSize: 21)))!
                heightConstrantForDetailView.constant = 190 + (lblTicletTitle.text?.height(withConstrainedWidth: lblTicletTitle.frame.size.width, font: UIFont.systemFont(ofSize: 22)))! +  70
            }
            else{
                heightConstrantForTextView.constant = 120
                heightConstrantForTitleLabel.constant = (lblTicletTitle.text?.height(withConstrainedWidth: lblTicletTitle.frame.size.width, font: UIFont.systemFont(ofSize: 21)))!
                heightConstrantForDetailView.constant = 120 + (lblTicletTitle.text?.height(withConstrainedWidth: lblTicletTitle.frame.size.width, font: UIFont.systemFont(ofSize: 21)))! +  60
            }
            
        }
       
        if dictTicketDetail.strClientPriority.lowercased() == "Critical".lowercased() {
            lblTicketProprity.textColor = UIColor(hex: "cb5a5e")
        }
        else if dictTicketDetail.strClientPriority.lowercased() == "High".lowercased(){
            lblTicketProprity.textColor = UIColor(hex: "cb5a5e")
        }
        else if dictTicketDetail.strClientPriority.lowercased() == "Urgent".lowercased(){
            lblTicketProprity.textColor = UIColor(hex: "cb5a5e")
        }
        else  if dictTicketDetail.strClientPriority.lowercased() == "Low".lowercased(){
            lblTicketProprity.textColor = UIColor.appThemeColor
        }
        else  if dictTicketDetail.strClientPriority.lowercased() == "Medium".lowercased(){
            lblTicketProprity.textColor = UIColor(hex: "daae2b")
        }
        
        if dictTicketDetail.strStatusName.lowercased() == "Closed".lowercased() || dictTicketDetail.strStatusName.lowercased() == "Close".lowercased(){
            btnAddComment.isHidden = true
            btnEditTicket.isHidden = true
        }
        else{
            btnAddComment.isHidden = false
            btnEditTicket.isHidden = false
        }
        
        
        
//        self.collectionViewAttchments.reloadData()
        self.tblViewComments.reloadData()
    }
    
    func deleteData() {
        UserDefaults.standard.set([], forKey: "HelpTicket_CommentDocumentsName")
        UserDefaults.standard.set([], forKey: "HelpTicket_CommentDocumentsType")
    }
    
    //MARK: - IBActions
    
    @IBAction func btnCloseTicketAction(_ sender: UIButton) {
        let alert = UIAlertController(title: alertMessage, message: "Are you sure, you want to close ticket?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "YES", style: .default, handler: { (nil) in
            //Call api to close ticket
            self.apiToCloseTicket()
        }))
        alert.addAction(UIAlertAction (title: "NO", style: .cancel, handler: { (nil) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnAddCommentAction(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "TicketHelpCenter", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpTicket_AddCommentVC") as? HelpTicket_AddCommentVC
        vc?.strEntityId = dictTicketDetail.strTaskTimeEntryId
        vc?.delegateAddComment = self
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if self.isEdited {
            self.delegateTicketDetail?.refreshDashboard(isAddTicket: true)
            self.navigationController?.popViewController(animated: false)
        }else {
            self.delegateTicketDetail?.refreshDashboardOnBack()
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func btnEditTicketAction(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "TicketHelpCenter", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpTicket_EditTicketVC") as? HelpTicket_EditTicketVC
        vc!.dictTicketDetail = dictTicketDetail
        vc?.delegateEdit = self

        self.navigationController?.pushViewController(vc!, animated: false)
    }

    @IBAction func attchmentAction(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_AttachmentListVC") as? HelpTicket_AttachmentListVC
        objByProductVC?.arrAttachmentList = self.arrAttachmentList
        self.navigationController?.pushViewController(objByProductVC!, animated: true)
    }
    
    
    //MARK: -Call Web service
    func callWebServiceToGetTicketDetails(){
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            
            let strURL = String(format: URL.HelpTicket.getTicketDetails, self.strTicketId, Global().getCompanyKey())
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { [self] (response, status) in
                if(status)
                {
                    self.loader.dismiss(animated: false) {
                        HelpTicketParser.parseHelpTicketDetailJSON(response: response, completionHandler: { (obj) in
                            // print(obj)
                            let arrOfTicketDetailModel: [HelpTicket_DetailModel] = obj
                            if arrOfTicketDetailModel.count > 0{
                                self.dictTicketDetail = arrOfTicketDetailModel[0]
                                print(self.dictTicketDetail)
                                self.setValuesFromResponse()
                            }
                        })
                    }
                }
                else{
                    self.loader.dismiss(animated: false) {
                        print("failed")
                    }
                }
            }
        }
    }
    
    //MARK: -Call Web service
    func callWebServiceToGetStatusList(){
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            let strURL = String(format: URL.HelpTicket.getStatus , Global().getCompanyKey())
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { [self] (response, status) in
                if(status)
                {
                    HelpTicketParser.parseHelpTicketStatusFilterJSON(response: response, completionHandler: { (obj) in
                        var arrOfStatusModel = [HelpTicket_StatusModel]()
                        arrOfStatusModel = obj
                        if arrOfStatusModel.count > 0 {
                            
                            for statue in arrOfStatusModel {
                                var objStatus: HelpTicket_StatusModel = statue
                                if objStatus.strStatusName == "Close" {
                                    print(objStatus.strStatusId)
                                    strCloseStatusId = objStatus.strStatusId
                                    break
                                }
                            }
                        }
                    })
                    
                }
                else{
                    print("failed")
                }
            }
        }
    }
    
    func apiToCloseTicket() {
        
        if !isInternetAvailable(){
            showAlertWithoutAnyAction(strtitle: "Info", strMessage: "No internet connection available", viewcontrol: self)
        }
        else
        {
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            
            let strURL = String(format: URL.HelpTicket.closeTicket, self.dictTicketDetail.strTaskTimeEntryId, self.strCloseStatusId, "Status", Global().getCompanyKey(), Global().getEmployeeId())
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { [self] (response, status) in
                if(status)
                {
                    self.loader.dismiss(animated: false) {
                        if (response["data"] != nil) == true {
                            self.delegateTicketDetail?.refreshDashboard(isAddTicket: true)
                            self.navigationController?.popViewController(animated: false)
                        }
                    }
                }
                else{
                    self.loader.dismiss(animated: false) {
                        print("failed")
                    }
                }
            }
        }
    }

    
    func callBackAttachment(cell: HelpTicket_CommentCell, objAttachment: [AttachmentList]) {
        //        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
        //        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_PreviewDocument") as? HelpTicket_PreviewDocument
        //        objByProductVC?.strFilePath = objAttachment.strFilePath
        //        objByProductVC?.strFileName = objAttachment.strFileName
        //        self.navigationController?.pushViewController(objByProductVC!, animated: true)
        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_AttachmentListVC") as? HelpTicket_AttachmentListVC
        objByProductVC?.arrAttachmentList = objAttachment
        self.navigationController?.pushViewController(objByProductVC!, animated: true)
    }
    
}

extension HelpTicket_TicketDetailVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCommentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTicket_CommentCell") as! HelpTicket_CommentCell
        cell.selectionStyle = .none
       
        let objComment: CommentList = self.arrCommentList[indexPath.row]

        cell.lblComment.text = objComment.strComment.capitalizingFirstLetter()
        cell.lblCommentBy.text = objComment.strCreatedByName.capitalized + ", Commented"

        let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: objComment.strCreatedDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let Date1 = formatter.date(from: scheduleStartDate!)
        let strDate = formatter.string(from: Date1!)
        cell.lblCommentDate.text = strDate

        cell.delegateCommentCell = self
        if objComment.arrAttachmentList.count > 0 {
            cell.btnAttachment.isHidden = false
            cell.hightConstant.constant = 25
            cell.setCollectionAttachment(arrAttachmentList: objComment.arrAttachmentList)
        }else {
            cell.btnAttachment.isHidden = true
            cell.hightConstant.constant = 0
        }
      
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
    
extension HelpTicket_TicketDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrAttachmentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        let objAttachment: AttachmentList = self.arrAttachmentList[indexPath.row]
        cell.lblTitle.text = objAttachment.strFileName.capitalizingFirstLetter()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let objAttachment: AttachmentList = self.arrAttachmentList[indexPath.row]

        print(objAttachment.strFilePath)
        
   
        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_PreviewDocument") as? HelpTicket_PreviewDocument
        objByProductVC?.strFilePath = objAttachment.strFilePath
        objByProductVC?.strFileName = objAttachment.strFileName
        self.navigationController?.pushViewController(objByProductVC!, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        if DeviceType.IS_IPAD{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 18.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 38)
        }else{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 14.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 38)
        }
        
    }

    
}


protocol HelpTicketCommentCellDelegate: class {
    func callBackAttachment(cell: HelpTicket_CommentCell, objAttachment: [AttachmentList])
}

class HelpTicket_CommentCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    //MARK: -IBOutlets
    
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblCommentBy: UILabel!
    @IBOutlet weak var lblCommentDate: UILabel!
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var commentCollection: UICollectionView!
    @IBOutlet weak var hightConstant: NSLayoutConstraint!
    
    var arrAttachmentList = [AttachmentList]()
    weak var delegateCommentCell : HelpTicketCommentCellDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func attachmentAction(_ sender: Any) {
        
//        let objAttachment: AttachmentList = self.arrAttachmentList[indexPath.row]
        self.delegateCommentCell?.callBackAttachment(cell: self, objAttachment: self.arrAttachmentList)

    }
    
    func setCollectionAttachment (arrAttachmentList: [AttachmentList]) {
        self.arrAttachmentList = arrAttachmentList
        self.commentCollection.delegate = self
        self.commentCollection.dataSource = self
        
        self.commentCollection.reloadData()
    }

    //MARK:- Collection Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrAttachmentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        let objAttachment: AttachmentList = self.arrAttachmentList[indexPath.row]
        cell.lblTitle.text = objAttachment.strFileName.capitalizingFirstLetter()

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let objAttachment: AttachmentList = self.arrAttachmentList[indexPath.row]

        //print(objAttachment.strFilePath)
        
//        let mainStoryboard = UIStoryboard(name: "TicketHelpCenter", bundle: nil)
//        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "HelpTicket_PreviewDocument") as? HelpTicket_PreviewDocument
//        objByProductVC?.strFilePath = objAttachment.strFilePath
//        objByProductVC?.strFileName = objAttachment.strFileName
//        self.navigationController?.pushViewController(objByProductVC!, animated: true)
//        self.delegateCommentCell?.callBackAttachment(cell: self, objAttachment: objAttachment)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpTicket_AttachmentColletionCell", for: indexPath) as! HelpTicket_AttachmentColletionCell
        
        if DeviceType.IS_IPAD{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 18.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 20)
        }else{
            let width = cell.lblTitle.text?.width(withConstraintedHeight: 35, font: UIFont.systemFont(ofSize: 14.0))
            let finalWidth = width! + 30
            
            return CGSize(width: finalWidth, height: 20)
        }
        
    }
}

class HelpTicket_AttachmentColletionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDeleteAttachment: UIButton!

}
