//
//  NearbySearch_iPhoneVC.swift
//  DPS
//
//  Created by NavinPatidar on 6/16/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar 2021

import UIKit
import CoreLocation
import Alamofire

class NearbySearch_iPhoneVC: UIViewController {
    
    @IBOutlet weak var txtfld_LeadStatus: ACFloatingTextField!
    @IBOutlet weak var txtfld_OpportunityStatus: ACFloatingTextField!
    @IBOutlet weak var txtfld_Radius: ACFloatingTextField!
    @IBOutlet weak var txtfld_Address: ACFloatingTextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCurrentAddress: UIButton!
    @IBOutlet weak var btnOtherAddress: UIButton!

    
    var aryOpportunityStatus = NSMutableArray() , aryLeadStatus = NSMutableArray()
    var ary_SelectedOpportunityStatus = NSMutableArray() , arySelectedLeadStatus = NSMutableArray()
    var locationManager = CLLocationManager()
    
    var strLatForNearBySearch = "" , strLongForNearBySearch = ""
    var dictNearbySearchData = NSMutableDictionary()
    var dictLoginData = NSDictionary()

    // MARK: - ----------- Google Address Code ----------------
    @IBOutlet weak var scrollView: UIScrollView!

        @IBOutlet weak var tableView: UITableView!
        private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
        private var placeType: PlaceType = .all
        private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
        private var radius: Double = 0.0
        private var strictBounds: Bool = false
        var txtAddressMaxY = CGFloat()
        var imgPoweredBy = UIImageView()
        private var places = [Place]() {
            didSet { tableView.reloadData() }
        }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        
        self.LoadData()
        
    }
    
    // MARK:
      // MARK: ----------GetCurrentLocation----------
    func GetCurrentLocation() {
        locationManager.requestWhenInUseAuthorization()
        var currentLoc: CLLocation!
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways) {
            currentLoc = locationManager.location
            
            self.strLatForNearBySearch = "\(String(describing: currentLoc.coordinate.latitude))"
            self.strLongForNearBySearch = "\(String(describing: currentLoc.coordinate.longitude))"
            getAddressFromLatLong(latitude: currentLoc.coordinate.latitude, longitude: currentLoc.coordinate.longitude)
            print(currentLoc.coordinate.latitude)
            print(currentLoc.coordinate.longitude)
        }
    }
    func getAddressFromLatLong(latitude: Double, longitude : Double) {
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(apiKey)"

        AF.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:

                let responseJson = response.value! as! NSDictionary

                if let results = responseJson.object(forKey: "results")! as? [NSDictionary] {
                    if results.count > 0 {
                        if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                            self.txtfld_Address.text = results[0]["formatted_address"] as? String
//                            for component in addressComponents {
//                                if let temp = component.object(forKey: "types") as? [String] {
//                                    if (temp[0] == "postal_code") {
//                                        self.pincode = component["long_name"] as? String
//                                    }
//                                    if (temp[0] == "locality") {
//                                        self.city = component["long_name"] as? String
//                                    }
//                                    if (temp[0] == "administrative_area_level_1") {
//                                        self.state = component["long_name"] as? String
//                                    }
//                                    if (temp[0] == "country") {
//                                        self.country = component["long_name"] as? String
//                                    }
//                                }
//                            }
                        }
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK:
    // MARK: ----------LoadData----------
    func LoadData() {
        if(nsud.value(forKey: "WebLeadStatusMasters") != nil){
            self.aryLeadStatus = (nsud.value(forKey: "WebLeadStatusMasters")as! NSArray).mutableCopy()as! NSMutableArray
        }
        if(nsud.value(forKey: "LeadStatusMasters") != nil){
            self.aryOpportunityStatus = (nsud.value(forKey: "LeadStatusMasters")as! NSArray).mutableCopy()as! NSMutableArray
        }
        
        if(nsud.value(forKey: "DPS_NearBySearch_Data") == nil){
            let dict = NSMutableDictionary()
            dict.setValue("2", forKey: "Radius")
            dict.setValue(aryLeadStatus, forKey: "leadStatus")
            dict.setValue(aryOpportunityStatus, forKey: "OpportunityStatus")
            dict.setValue("", forKey: "Address")
            dict.setValue("", forKey: "Lat")
            dict.setValue("", forKey: "Long")
            nsud.set(dict, forKey: "DPS_NearBySearch_Data")
            self.arySelectedLeadStatus = aryLeadStatus
            self.ary_SelectedOpportunityStatus = aryOpportunityStatus

        }
        
        dictNearbySearchData = (nsud.value(forKey: "DPS_NearBySearch_Data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
        
        
        //----------For Radius ---------//
        
        txtfld_Radius.text = "\(dictNearbySearchData.value(forKey: "Radius")!)"
        
        
        //----------For Address-----------//
        btnOtherAddress.setImage(UIImage(named: "redio_1"), for: .normal)
        btnCurrentAddress.setImage(UIImage(named: "redio_1"), for: .normal)
        btnOtherAddress.currentImage == UIImage(named: "redio_2") ? txtfld_Address.isHidden = false : (txtfld_Address.isHidden = true)
        
        txtfld_Address.text = "\(dictNearbySearchData.value(forKey: "Address")!)"
        strLatForNearBySearch = "\(dictNearbySearchData.value(forKey: "Lat")!)"
        strLongForNearBySearch = "\(dictNearbySearchData.value(forKey: "Long")!)"

        
        //----------For lead Status-----------//
        arySelectedLeadStatus = NSMutableArray()
        arySelectedLeadStatus = (dictNearbySearchData.value(forKey: "leadStatus")as! NSArray).mutableCopy() as! NSMutableArray
        var strLeadStatus = ""
        if(arySelectedLeadStatus.count == 0){
            arySelectedLeadStatus = NSMutableArray()
            arySelectedLeadStatus = aryLeadStatus
        }
        for item in arySelectedLeadStatus {
            strLeadStatus =  strLeadStatus + "\((item as AnyObject).value(forKey: "Name")!),"
        }
        if(strLeadStatus.count != 0){
            strLeadStatus = String(strLeadStatus.dropLast())
        }
        txtfld_LeadStatus.text = strLeadStatus
        
        //----------For Opportunity Status-----------//
        ary_SelectedOpportunityStatus = NSMutableArray()
        ary_SelectedOpportunityStatus = (dictNearbySearchData.value(forKey: "OpportunityStatus")as! NSArray).mutableCopy() as! NSMutableArray
        var strOpportunityStatus = ""
        if(ary_SelectedOpportunityStatus.count == 0){
                   ary_SelectedOpportunityStatus = NSMutableArray()
                   ary_SelectedOpportunityStatus = aryOpportunityStatus
               }
        for item in ary_SelectedOpportunityStatus {
            strOpportunityStatus =  strOpportunityStatus + "\((item as AnyObject).value(forKey: "StatusName")!),"
        }
        if(strOpportunityStatus.count != 0){
            strOpportunityStatus = String(strOpportunityStatus.dropLast())
        }
        txtfld_OpportunityStatus.text = strOpportunityStatus
    }
    
    
    // MARK:
    // MARK: ----------IBAction---------
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnLeadStatus(_ sender: UIButton) {
           self.view.endEditing(true)
        if(nsud.value(forKey: "WebLeadStatusMasters") != nil){
            self.aryLeadStatus = (nsud.value(forKey: "WebLeadStatusMasters")as! NSArray).mutableCopy()as! NSMutableArray
            openTableViewPopUp(tag: 950, ary: aryLeadStatus , aryselectedItem: arySelectedLeadStatus)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
        }
     }
    
    @IBAction func actionOnSelectOpportunityStatus(_ sender: UIButton) {
        self.view.endEditing(true)
            if(nsud.value(forKey: "LeadStatusMasters") != nil){
                 self.aryOpportunityStatus = (nsud.value(forKey: "LeadStatusMasters")as! NSArray).mutableCopy()as! NSMutableArray
                 openTableViewPopUp(tag: 951, ary: aryOpportunityStatus , aryselectedItem: ary_SelectedOpportunityStatus)
             }else{
                 showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
             }
     }
    
    @IBAction func actionOnSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            let dict = NSMutableDictionary()
            dict.setValue(txtfld_Radius.text!, forKey: "Radius")
            dict.setValue(arySelectedLeadStatus, forKey: "leadStatus")
            dict.setValue(ary_SelectedOpportunityStatus, forKey: "OpportunityStatus")
            
            if(btnOtherAddress.currentImage == UIImage(named: "redio_1") && btnCurrentAddress.currentImage == UIImage(named: "redio_1")){
                dict.setValue("", forKey: "Address")
                dict.setValue("", forKey: "Lat")
                dict.setValue("", forKey: "Long")
            }else{
                dict.setValue(txtfld_Address.text!, forKey: "Address")
                dict.setValue(strLatForNearBySearch, forKey: "Lat")
                dict.setValue(strLongForNearBySearch, forKey: "Long")
            }
            
            nsud.set(dict, forKey: "DPS_NearBySearch_Data")
            
            self.navigationController?.popViewController(animated: true)
        }
        
        
    }
    
    @IBAction func actionOnCurrentAddress(_ sender: UIButton) {
        self.view.endEditing(true)
        
        txtfld_Address.text = ""
        self.strLatForNearBySearch = ""
        self.strLongForNearBySearch = ""
        
        btnOtherAddress.setImage(UIImage(named: "redio_1"), for: .normal)
        sender.currentImage == UIImage(named: "redio_2") ? btnCurrentAddress.setImage(UIImage(named: "redio_1"), for: .normal) : btnCurrentAddress.setImage(UIImage(named: "redio_2"), for: .normal)
        txtfld_Address.isHidden = true
        if(sender.currentImage == UIImage(named: "redio_2")){
            self.GetCurrentLocation()
        }
    }
    @IBAction func actionOnOtherAddress(_ sender: UIButton) {
        self.view.endEditing(true)
        btnCurrentAddress.setImage(UIImage(named: "redio_1"), for: .normal)
        
        if(sender.currentImage == UIImage(named: "redio_2")){
            btnOtherAddress.setImage(UIImage(named: "redio_1"), for: .normal)
            txtfld_Address.isHidden = true
        }else{
            (txtfld_Address.isHidden = false)
            btnOtherAddress.setImage(UIImage(named: "redio_2"), for: .normal)
        }
        txtfld_Address.text = ""
        self.strLatForNearBySearch = ""
        self.strLongForNearBySearch = ""
    }
    
    func validation() -> Bool {
        if(txtfld_Radius.text!.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "", viewcontrol: self)
            return false
        }else if(btnCurrentAddress.currentImage == UIImage(named: "redio_2")){
            if(strLongForNearBySearch.count == 0 || strLatForNearBySearch.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "", viewcontrol: self)
                           return false
            }
        }
        if(btnOtherAddress.currentImage == UIImage(named: "redio_2")){
            if(strLongForNearBySearch.count == 0 || strLatForNearBySearch.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "", viewcontrol: self)
                           return false
            }
        }
        return true
    }
      // MARK:
      // MARK: ----------TableViewPopUp
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
     {
         let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
         let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
         vc.strTitle = "Select"
         vc.strTag = tag
         vc.arySelectedItems = aryselectedItem
         
         if ary.count != 0{
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
             vc.modalTransitionStyle = .coverVertical
             vc.handelDataSelectionTable = self
             vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
             self.present(vc, animated: false, completion: {})
         }else{
             
             Global().displayAlertController(Alert, NoDataAvailableee, self)
         }
     }
    
    
    
    
}
// MARK: -
// MARK: - Selection CustomTableView
extension NearbySearch_iPhoneVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 950){
            if(dictData.count != 0){
                var strLeadStatus = ""
                    arySelectedLeadStatus = NSMutableArray()
                    arySelectedLeadStatus = (dictData.value(forKey: "multi")as! NSArray).mutableCopy()as! NSMutableArray
                          for item in arySelectedLeadStatus {
                              strLeadStatus =  strLeadStatus + "\((item as AnyObject).value(forKey: "Name")!),"
                          }
                          if(strLeadStatus.count != 0){
                              strLeadStatus = String(strLeadStatus.dropLast())
                          }
                          txtfld_LeadStatus.text = strLeadStatus
            }else{
                txtfld_LeadStatus.text = ""
                arySelectedLeadStatus = NSMutableArray()
            }
    
            
        }else if(tag == 951){
            
            if(dictData.count != 0){
                var strOpportunityStatus = ""
                ary_SelectedOpportunityStatus = NSMutableArray()
                ary_SelectedOpportunityStatus =  (dictData.value(forKey: "multi")as! NSArray).mutableCopy()as! NSMutableArray
                for item in ary_SelectedOpportunityStatus {
                    strOpportunityStatus =  strOpportunityStatus + "\((item as AnyObject).value(forKey: "StatusName")!),"
                }
                if(strOpportunityStatus.count != 0){
                    strOpportunityStatus = String(strOpportunityStatus.dropLast())
                }
                txtfld_OpportunityStatus.text = strOpportunityStatus
            }else{
                txtfld_OpportunityStatus.text = ""
                ary_SelectedOpportunityStatus = NSMutableArray()
            }
        }
    }
}



// MARK: - --------UITextFieldDelegate ------------//

extension NearbySearch_iPhoneVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtfld_Address){

            self.scrollView.isScrollEnabled = false
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtfld_Address){

            self.scrollView.isScrollEnabled = true
            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ( textField == txtfld_Radius){
                   
                   return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
                   
               }
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if(textField == txtfld_Address){
                        
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()
                    
                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = txtfld_Address.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtfld_Address){

            let txtAfterUpdate:NSString = txtfld_Address.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            
//            self.getPlaces(with: parameters) {
//                self.places = $0
//            }
            return true
        }
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
extension NearbySearch_iPhoneVC {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }
                else
                {
                   /* for item in self.scrollView.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)*/
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    
                   // self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.tableView.frame = CGRect(x: self.txtfld_Address.frame.origin.x, y: 305, width: self.scrollView.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)

                    
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension NearbySearch_iPhoneVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            //self.txtServiceAddress.text = value.formattedAddress
            self.txtfld_Address.text = addressFormattedByGoogle(value: value)
            self.strLatForNearBySearch = "\(String(describing: value.coordinate?.latitude))"
            self.strLongForNearBySearch = "\(String(describing: value.coordinate?.longitude))"
            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
