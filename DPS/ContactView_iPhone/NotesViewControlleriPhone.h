//
//  NotesViewController.h
//  DPS
//
//  Created by Saavan Patidar on 28/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesViewControlleriPhone : UIViewController

- (IBAction)action_Back:(id)sender;
- (IBAction)action_AddNotes:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtView_Notes;
@property (strong, nonatomic) IBOutlet UITableView *tblViewNotes;
@property (weak, nonatomic)  NSString *strLeadNumber;
@property (weak, nonatomic)  NSString *strOpportunityNumber;

@end
