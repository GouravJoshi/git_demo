//
//  OutBoxView.h
//  DPS
//
//  Created by Rakesh Jain on 21/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface OutBoxView : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityOutBox;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
}
- (IBAction)actionBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewOutBox;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerOutBox;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CountOfLead;

@end
