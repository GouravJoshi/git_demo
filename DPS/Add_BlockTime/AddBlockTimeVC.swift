//
//  AddBlockTimeVC.swift
//  DPS
//
//  Created by APPLE on 08/04/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import UIKit

protocol AddBlockTimeProtocol : class
{
    func getDataOnAddBlockTime(arrAppointmentData : NSArray)
}

class AddBlockTimeVC: UIViewController {
    
    // MARK:
    // MARK:-----IBOutlet
    
    @IBOutlet weak var txtFldAddressLine1: ACFloatingTextField!
    @IBOutlet weak var txtFldAddressLine2: ACFloatingTextField!
    @IBOutlet weak var txtFldCity: ACFloatingTextField!
    @IBOutlet weak var txtFldState: ACFloatingTextField!
    @IBOutlet weak var txtFldZipCode: ACFloatingTextField!
    @IBOutlet weak var txtFldCountry: ACFloatingTextField!
    @IBOutlet weak var txtFldTitle: ACFloatingTextField!
    @IBOutlet weak var txtFldDate: ACFloatingTextField!
    @IBOutlet weak var txtFldStartTime: ACFloatingTextField!
    @IBOutlet weak var txtFldEndTime: ACFloatingTextField!
    @IBOutlet weak var txtView_Description: UITextView!
    @IBOutlet weak var heightAddressView: NSLayoutConstraint!
    @IBOutlet weak var btnAddressExpand: UIButton!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var view_BackGround: UIView!
    @IBOutlet weak var constant_ViewBack_H: NSLayoutConstraint!
    
    // MARK:
    // MARK:-----Global Variable
    
    weak var handelDataOnAddingBlockTime: AddBlockTimeProtocol?
    var loader = UIAlertController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll.contentSize = CGSize(width: self.view.frame.width, height: 450)
        heightAddressView.constant = 0.0
        txtFldState.tag = 0
        btnAddressExpand.setImage(UIImage(named: "drop_down_ipad"), for: .normal)
        txtFldCountry.isEnabled = false
        txtFldCountry.text = "United State"
        constant_ViewBack_H.constant = constant_ViewBack_H.constant - (DeviceType.IS_IPAD ? 0 : 400)
    }
    
    
    // MARK:
    // MARK:-----IBAction
    @IBAction func action_Back(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func action_State(_ sender: Any) {
        self.view.endEditing(true)
        if let path = Bundle.main.path(forResource: StateJsonSwift, ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
                let aryState = (jsonResult as! NSArray).mutableCopy() as! NSMutableArray
                if(aryState.count != 0){
                    let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
                    let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
                    vc.strTitle = "Select"
                    vc.strTag = 250
                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    vc.modalTransitionStyle = .coverVertical
                    vc.handelDataSelectionTable = self
                    vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: aryState)
                    self.present(vc, animated: false, completion: {})
                }
            } catch {
                
            }
        }
        
    }
    @IBAction func action_OnDate(_ sender: Any) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((txtFldDate.text?.count)! > 0 ? txtFldDate.text! : result))!
        let datePicker = UIDatePicker()
        //datePicker.minimumDate = Date()
        datePicker.date = fromDate
        datePicker.datePickerMode = UIDatePicker.Mode.date
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Service Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
            
            txtFldDate.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func action_OnStartTime(_ sender: Any) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((txtFldStartTime.text?.count)! > 0 ? txtFldStartTime.text! : result))!
        
        let datePicker = UIDatePicker()
        // datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.time
        datePicker.date = fromDate
        //  datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate, Picker: datePicker, view: self.view ,strTitle : "Select Service Time")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
            
            txtFldStartTime.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func action_OnEndTime(_ sender: Any) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((txtFldEndTime.text?.count)! > 0 ? txtFldEndTime.text! : result))!
        
        let datePicker = UIDatePicker()
        // datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.time
        datePicker.date = fromDate
        //  datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate, Picker: datePicker, view: self.view ,strTitle : "Select Service Time")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
            
            txtFldEndTime.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func action_OnAddressExpand(_ sender: Any) {
        self.view.endEditing(true)
        if(heightAddressView.constant == 0.0){
            heightAddressView.constant = DeviceType.IS_IPAD ? 450 : 400
            btnAddressExpand.setImage(UIImage(named: "drop_down_up_ipad"), for: .normal)
            scroll.contentSize = CGSize(width: self.view.frame.width, height: (450 + heightAddressView.constant))
            constant_ViewBack_H.constant = ((DeviceType.IS_IPAD ? self.view.frame.size.height : 450) + (DeviceType.IS_IPAD ? self.view.frame.size.height : heightAddressView.constant))
        }else{
            heightAddressView.constant = 0.0
            btnAddressExpand.setImage(UIImage(named: "drop_down_ipad"), for: .normal)
            scroll.contentSize = CGSize(width: self.view.frame.width, height: (450 + heightAddressView.constant))
            constant_ViewBack_H.constant = ((DeviceType.IS_IPAD ? self.view.frame.size.height : 450) + (DeviceType.IS_IPAD ? self.view.frame.size.height : heightAddressView.constant))
        }
    }
    @IBAction func action_OnSave(_ sender: Any) {
        self.view.endEditing(true)
        if(validation()){
            if(heightAddressView.constant != 0.0){
                if(validationAddress()){
                    IsEmployeeBlockTimeExists()
                }
            }else{
                IsEmployeeBlockTimeExists()

            }
        }
    }
    
    // MARK:
    // MARK:-----validationAddress
    
    func validation() -> Bool  {
        if(txtFldTitle.text?.count == 0){
            addErrorMessage(textView: txtFldTitle, message: Alert_Required)
            return false
        }
        else if(txtFldDate.text?.count == 0){
            addErrorMessage(textView: txtFldDate, message: Alert_Required)
            return false
        }
        else if(txtFldStartTime.text?.count == 0){
            addErrorMessage(textView: txtFldStartTime, message: Alert_Required)
            return false
        }
        else if(txtFldEndTime.text?.count == 0){
            addErrorMessage(textView: txtFldEndTime, message: Alert_Required)
            return false
        }
        else if(txtFldEndTime.text?.count != 0) && (txtFldStartTime.text?.count != 0)  && txtFldEndTime.text == txtFldStartTime.text{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select To time greater then from time.", viewcontrol: self)
            return false
        }
        else if (txtFldDate.text?.count != 0) && (txtFldStartTime.text?.count != 0) && (txtFldEndTime.text?.count != 0) {
            
            var strFromDateMerged = txtFldDate.text! + " \(txtFldStartTime.text!)"
            var strToDateMerged = txtFldDate.text! + " \(txtFldEndTime.text!)"
            //"2022-04-07T15:00:00"
            strFromDateMerged = dateStringToFormatedDateStringNew(dateToConvert: strFromDateMerged, dateToConvertFormat: "MM/dd/yyyy hh:mm a", dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
            strToDateMerged = dateStringToFormatedDateStringNew(dateToConvert: strToDateMerged, dateToConvertFormat: "MM/dd/yyyy hh:mm a", dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "EST")
            let fromDate = dateFormatter.date(from: strFromDateMerged)
            let toDate = dateFormatter.date(from: strToDateMerged)
            if fromDate! >= toDate! {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select To time greater then from time.", viewcontrol: self)

                return false
            }else{
                return true
            }
        }
        return true
        
    }
    
    
    func validationAddress() -> Bool {
         
        if (txtFldAddressLine1.text?.count == 0) && (txtFldAddressLine2.text?.count == 0) && (txtFldCity.text?.count == 0) && (txtFldState.text?.count == 0) && (txtFldZipCode.text?.count == 0) {
            return true
        }
        
        if(txtFldAddressLine1.text?.count == 0){
            addErrorMessage(textView: txtFldAddressLine1, message: Alert_Required)
            return false
        }else if(txtFldCity.text?.count == 0){
            addErrorMessage(textView: txtFldCity, message: Alert_Required)
            
            return false
        }
        else if(txtFldState.text?.count == 0){
            addErrorMessage(textView: txtFldState, message: Alert_Required)
            
            return false
            
        } else if(txtFldZipCode.text?.count == 0){
            addErrorMessage(textView: txtFldZipCode, message: Alert_Required)
            
            return false
            
        }
        else if((txtFldZipCode.text?.count ?? 0) < 5){
            addErrorMessage(textView: txtFldZipCode, message: "ZipCode is invalid!")
            return false
        }
        
        return true
    }
    func addErrorMessage(textView : ACFloatingTextField, message : String) {
        textView.showError(withText: message)
    }
    
    
    // MARK:
    // MARK:-----API Calling
    
    func callAddBlockAPI()  {
        
      

        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.CoreServiceModule.ServiceUrl") ?? "")"
        
        var strFromDateMerged = txtFldDate.text! + " \(txtFldStartTime.text!)"
        var strToDateMerged = txtFldDate.text! + " \(txtFldEndTime.text!)"
        //"2022-04-07T15:00:00"
        strFromDateMerged = dateStringToFormatedDateStringNew(dateToConvert: strFromDateMerged, dateToConvertFormat: "MM/dd/yyyy hh:mm a", dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        strToDateMerged = dateStringToFormatedDateStringNew(dateToConvert: strToDateMerged, dateToConvertFormat: "MM/dd/yyyy hh:mm a", dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        
        let coordinate = Global().getLocation()

        let dictJson = ["EmployeeBlockTimeId":"0",
                    "EmployeeIdList" : [strEmpID],
                    "BlockType":"General",
                    "FromDate":strFromDateMerged,
                    "ToDate":strToDateMerged,
                    "Title":txtFldTitle.text ?? "",
                    "Description":txtView_Description.text ?? "",
                    "IsActive":"true",
                    "EmployeeBlockTimeSetupId":"",
                    "EmployeeBlockTimeAssignmentDcs":"",
                    "CountryId":"1",
                    "StateId":txtFldState.tag == 0 ? "" : txtFldState.tag,
                    "AddressLine1":txtFldAddressLine1.text ?? "",
                    "AddressLine2":txtFldAddressLine2.text ?? "",
                    "City":txtFldCity.text ?? "",
                    "ZipCode":txtFldZipCode.text ?? "",
                    "Latitude":"\(coordinate.latitude)",
                    "Longitude":"\(coordinate.longitude)",
                    "CreatedDate":"",
                    "CreatedBy":"\(Global().getEmployeeId() ?? "")",
                    "ModifiedDate":"",
                    "ModifiedBy":""] as [String : Any]
        
        let strUrl =  strServiceUrlMain + "/api/EmployeeBlockTime/AddUpdateEmployeeBlockTime?companyKey=" + "\(Global().getCompanyKey() ?? "")"
     
        WebService.postRequestWithHeaders(dictJson: dictJson as NSDictionary, url: strUrl, responseStringComing: "AddBlockTimeVC") { (response, status) in
                        
            self.loader.dismiss(animated: false) {

                if(status == true)
                {
                    if(response.value(forKey: "data") is NSDictionary){
                        let dictDataR = response.value(forKey: "data") as! NSDictionary
                        if "\(dictDataR.value(forKey: "EmployeeBlockTimeId") ?? "")".count > 0 && "\(dictDataR.value(forKey: "EmployeeBlockTimeId") ?? "")" != "0" {
                            self.saveEmpBlockTimeToDB(dictDataTemp: dictDataR)
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                } else {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
  
    func IsEmployeeBlockTimeExists() {
        
        self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.CoreServiceModule.ServiceUrl") ?? "")"
        
        var strFromDateMerged = txtFldDate.text! + " \(txtFldStartTime.text!)"
        var strToDateMerged = txtFldDate.text! + " \(txtFldEndTime.text!)"
        //"2022-04-07T15:00:00"
        strFromDateMerged = dateStringToFormatedDateStringNew(dateToConvert: strFromDateMerged, dateToConvertFormat: "MM/dd/yyyy hh:mm a", dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        strToDateMerged = dateStringToFormatedDateStringNew(dateToConvert: strToDateMerged, dateToConvertFormat: "MM/dd/yyyy hh:mm a", dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        let strUrl =  strServiceUrlMain + "/api/EmployeeBlockTime/IsEmployeeBlockTimeExists?companyKey=\("\(Global().getCompanyKey() ?? "")")&employeeIds=\(strEmpID)&fromDate=\(strFromDateMerged)&toDate=\(strToDateMerged)&employeeBlockTimeId=0"
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl, responseStringComing: "BlockTime") { [self] responce, status in
            if status {
                let result = "\(responce.value(forKey: "data") ?? "")"
                if result.lowercased() == "true"  || result.lowercased() == "yes" || result.lowercased() == "1"{
                    self.loader.dismiss(animated: false) {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Duplicate Record Exists.", viewcontrol: self)
                    }
                   
                }else{
                    callAddBlockAPI()
                }
            }else{
                self.loader.dismiss(animated: false) {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }

    
    
    func saveEmpBlockTimeToDB(dictDataTemp: NSDictionary)  {
        
        // Saving Data in DB
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("strEmpId")

        arrOfKeys.add("blockType")
        arrOfKeys.add("descriptionBlockTime")
        arrOfKeys.add("employeeBlockTimeAssignmentId")
        arrOfKeys.add("employeeBlockTimeId")
        arrOfKeys.add("employeeName")
        arrOfKeys.add("employeeNo")
        arrOfKeys.add("fromDate")
        arrOfKeys.add("isActive")
        arrOfKeys.add("modifyDate")
        arrOfKeys.add("titleBlockTime")
        arrOfKeys.add("toDate")
        arrOfKeys.add("fromDateFormatted")

        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(Global().getEmployeeId())
        
        arrOfValues.add("\(dictDataTemp.value(forKey: "BlockType") ?? "")")
        arrOfValues.add("\(dictDataTemp.value(forKey: "Description") ?? "")")
        arrOfValues.add("")//"\(dictDataTemp.value(forKey: "EmployeeBlockTimeAssignmentId") ?? "")"
        arrOfValues.add("\(dictDataTemp.value(forKey: "EmployeeBlockTimeId") ?? "")")
        arrOfValues.add("\(Global().getUserName() ?? "")")
        arrOfValues.add("\(Global().getEmployeeId() ?? "")")
        arrOfValues.add("\(dictDataTemp.value(forKey: "FromDate") ?? "")")
        arrOfValues.add("\(dictDataTemp.value(forKey: "IsActive") ?? "")")
        arrOfValues.add("\(dictDataTemp.value(forKey: "ModifiedDate") ?? "")")
        arrOfValues.add("\(dictDataTemp.value(forKey: "Title") ?? "")")
        arrOfValues.add("\(dictDataTemp.value(forKey: "ToDate") ?? "")")
        arrOfValues.add(Global().getDueDateNeww("\(dictDataTemp.value(forKey: "FromDate") ?? "")"))//getDueDate

        saveDataInDB(strEntity: Entity_EmployeeBlockTimeAppointmentVC, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        self.reloadAppointmentData()
        
    }
    
    func reloadAppointmentData() {
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username")!)"
        
        var arrayAppointment = NSArray()
        
        let arrayOfLeadsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "employeeNo_Mobile == %@", strEmpNo))
        
        let arrayOfWoLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceWorkOrderDetailsService, predicate: NSPredicate(format: "dbUserName == %@", strEmpNo))
        
        let arrayOfBlockTime = getDataFromCoreDataBaseArray(strEntity: Entity_EmployeeBlockTimeAppointmentVC, predicate: NSPredicate(format: "userName == %@", strUserName))
        
        let arrOfTasksList = getDataFromCoreDataBaseArray(strEntity: Entity_CrmTaskAppointmentVC, predicate: NSPredicate(format: "userName == %@", strUserName))
        
        arrayAppointment = arrayOfLeadsLocal + arrayOfWoLocal
        
        let arrMergedBlockTimeAndTask = arrayOfBlockTime + arrOfTasksList
        
        arrayAppointment = Global().filterData(afterMergingAppointments: arrMergedBlockTimeAndTask as? [Any] , arrayAppointment as? [Any] )! as NSArray
        
        self.handelDataOnAddingBlockTime?.getDataOnAddBlockTime(arrAppointmentData: arrayAppointment)
        self.navigationController?.popViewController(animated: false)

    }
}
// MARK:-
// MARK:- ---------UITextFieldDelegate

extension AddBlockTimeVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtFldZipCode){
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 4)
        }
        return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 200)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
}
// MARK:-
// MARK:- ---------POPUpView Delegate

extension AddBlockTimeVC : CustomTableView {
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 250){ // for state
            if(dictData.count != 0){
                txtFldState.text = "\(dictData.value(forKey: "Name")!)"
                txtFldState.tag = Int("\(dictData.value(forKey: "StateId")!)")!
            }else{
                txtFldState.text = ""
                txtFldState.tag = 0
            }
        }
    }
}

