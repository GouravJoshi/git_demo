//
//  EquipmentListInvoiceViewTableViewCell.m
//  DPS
//
//  Created by Saavan Patidar on 27/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "EquipmentListInvoiceViewTableViewCell.h"

@implementation EquipmentListInvoiceViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
