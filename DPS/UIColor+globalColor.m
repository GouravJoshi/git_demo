//
//  UIColor+globalColor.m
//  DPS
//
//  Created by Saavan Patidar on 21/04/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  Saavan Patidar 2021

#import "UIColor+globalColor.h"

@implementation UIColor (globalColor)

+ (UIColor *) themeColor {
   return [UIColor colorWithRed:91.0f/255.0f green:178.0f/255.0f blue:175.0f/255.0f alpha:1.0f];    
}
+ (UIColor *) themeColorBlack {
    return [UIColor colorWithRed:56.0f/255.0f green:63.0f/255.0f blue:77.0f/255.0f alpha:1.0f];
}
+ (UIColor *) lightTextColorTimeSheet {
    return [UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:1.0f];
}
+ (UIColor *) lightColorForCell{
    return [UIColor colorWithRed:223.0f/255.0f green:223.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
}
+ (UIColor *) headerFooterColor{
    return [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
}

@end
