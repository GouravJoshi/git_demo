//
//  OutBoxTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 21/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OutBoxTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblAccountNo;
@property (strong, nonatomic) IBOutlet UILabel *lblDatenTime;
@property (strong, nonatomic) IBOutlet UILabel *lblNoService;

@end
