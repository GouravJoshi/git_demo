//
//  ScannerViewController.h
//  DPS
//
//  Created by Saavan Patidar on 07/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScannerViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *scannerPreviewView;
- (IBAction)action_OpenScannerView:(id)sender;
- (IBAction)action_CancelScanning:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTorch;
- (IBAction)action_TorchMode:(id)sender;

@end
