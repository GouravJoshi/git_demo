//
//  ScannerViewController.m
//  DPS
//
//  Created by Saavan Patidar on 07/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "ScannerViewController.h"
#import "AllImportsViewController.h"

@interface ScannerViewController ()
{
    
    Global *global;
}
@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (nonatomic, weak) IBOutlet UIButton *toggleScanningButton;
@property (nonatomic, assign) BOOL captureIsFrozen;
@property (nonatomic, assign) BOOL didShowCaptureWarning;
@property (nonatomic, weak) IBOutlet UIButton *toggleTorchButton;

@end

@implementation ScannerViewController


- (void)viewDidLoad {
    
    global = [[Global alloc] init];
    
    [self startScanning];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Scanner Methods

- (MTBBarcodeScanner *)scanner {
    if (!_scanner) {
        _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_scannerPreviewView];
    }
    return _scanner;
}

#pragma mark - Scanning

- (void)startScanning {
    
    // self.uniqueCodes = [[NSMutableArray alloc] init];
    
    NSError *error = nil;
    [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
        for (AVMetadataMachineReadableCodeObject *code in codes) {
            
            //            UIAlertView *Alet=[[UIAlertView alloc]initWithTitle:@"Scanned Result" message:code.stringValue delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [Alet show];
            
            [self stopScanning];
            
            [self closeScannerView];
            
            [self afterScannedResult:code.stringValue];
            
        }
    } error:&error];
    
    if (error) {
        NSLog(@"An error occurred: %@", error.localizedDescription);
    }
    
    [self.toggleScanningButton setTitle:@"Stop Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = [UIColor redColor];
}

-(void)afterScannedResult :(NSString*)stringValue{
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:stringValue forKey:@"ScannedResult"];
    [defs synchronize];

    [self closeScannerView];
    
}

- (void)stopScanning {
    [self.scanner stopScanning];
    
    [self.toggleScanningButton setTitle:@"Start Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = self.view.tintColor;
    
    self.captureIsFrozen = NO;
}

#pragma mark - Actions

- (IBAction)toggleScanningTapped:(id)sender {
    if ([self.scanner isScanning] || self.captureIsFrozen) {
        [self stopScanning];
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
    } else {
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
    }
}

- (IBAction)switchCameraTapped:(id)sender {
    [self.scanner flipCamera];
}

- (IBAction)toggleTorchTapped:(id)sender {
    if (self.scanner.torchMode == MTBTorchModeOff) {
        self.scanner.torchMode = MTBTorchModeOn;
        self.toggleTorchButton.titleLabel.text = @"Disable Torch";
    } else {
        self.scanner.torchMode = MTBTorchModeOff;
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
    }
}

#pragma mark - Gesture Handlers

- (void)previewTapped {
    if (![self.scanner isScanning] && !self.captureIsFrozen) {
        return;
    }
    
    if (!self.didShowCaptureWarning) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Capture Frozen" message:@"The capture is now frozen. Tap the preview again to unfreeze." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
        
        self.didShowCaptureWarning = YES;
    }
    
    if (self.captureIsFrozen) {
        [self.scanner unfreezeCapture];
    } else {
        [self.scanner freezeCapture];
    }
    
    self.captureIsFrozen = !self.captureIsFrozen;
}

#pragma mark - Setters

- (void)setUniqueCodes:(NSMutableArray *)uniqueCodes {
    // _uniqueCodes = uniqueCodes;
}

#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Scanning Unavaialble" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)action_OpenScannerView:(id)sender {
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
        
    } else {
        
        [global AlertMethod:Alert :@"Please check your internet connection, this functionality requires internet connectivity."];
        
    }
    
}

- (IBAction)action_CancelScanning:(id)sender {
    
    [self stopScanning];
    [self closeScannerView];
    
}

- (IBAction)action_TorchMode:(id)sender {
    
    if (self.scanner.torchMode == MTBTorchModeOff) {
        self.scanner.torchMode = MTBTorchModeOn;
        self.toggleTorchButton.titleLabel.text = @"Disable Torch";
        [_btnTorch setTitle:@"Disable Torch" forState:UIControlStateNormal];
    } else {
        self.scanner.torchMode = MTBTorchModeOff;
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
        [_btnTorch setTitle:@"Enable Torch" forState:UIControlStateNormal];
    }
    
}

-(void)closeScannerView{
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
@end

