//
//  TermiteInspectionTexasViewControlleriPad.h
//  DPS
//
//  Created by Rakesh Jain on 22/12/17.
//  Copyright © 2017 Saavan. All rights reserved.
/////

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface TermiteInspectionTexasViewControlleriPad : UIViewController<UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>
{
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    NSEntityDescription *entityTermiteTexas;
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSEntityDescription *entityImageDetail,*entityPaymentInfoServiceAuto,*entityImageDetailsTermite;
    NSFetchRequest *requestImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSManagedObject *matchesImageDetail;
    NSArray *arrAllObjImageDetail;
    
    NSManagedObject *matchesWorkOrderNew;
    
    
    NSEntityDescription *entityModifyDateServiceAuto;
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
}
@property (strong, nonatomic)  NSString *strWorkOrder;
@property (strong, nonatomic)  NSString *strWorkOrderStatus;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;

- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountNo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVieww;
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *view3;
@property (strong, nonatomic) IBOutlet UIView *view4;
@property (strong, nonatomic) IBOutlet UIView *view5;
@property (strong, nonatomic) IBOutlet UIView *view5Sub;
@property (strong, nonatomic) IBOutlet UIView *view6;
@property (strong, nonatomic) IBOutlet UIView *view7;
@property (strong, nonatomic) IBOutlet UIView *view8;
@property (strong, nonatomic) IBOutlet UIView *view9;
@property (strong, nonatomic) IBOutlet UIView *view10;
@property (strong, nonatomic) IBOutlet UIView *view11;
@property (strong, nonatomic) IBOutlet UIView *view12;
@property (strong, nonatomic) IBOutlet UIView *view13;

//View1
@property (strong, nonatomic) IBOutlet UITextField *txt_InspectedAddress;
@property (strong, nonatomic) IBOutlet UITextField *txt_City;
@property (strong, nonatomic) IBOutlet UITextField *txt_ZipCode;

//View2
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxA;
- (IBAction)action_btnChkBoxA:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxB;
- (IBAction)action_btnChkBoxB:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxC;
- (IBAction)action_btnChkBoxC:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxD;
- (IBAction)action_btnChkBoxD:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxE;
- (IBAction)action_btnChkBoxE:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxF;
- (IBAction)action_btnChkBoxF:(id)sender;

//View3
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxG;
- (IBAction)action_btnChkBoxG:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxH;
- (IBAction)action_btnChkBoxH:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxI;
- (IBAction)action_btnChkBoxI:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxJ;
- (IBAction)action_btnChkBoxJ:(id)sender;


//View4
@property (strong, nonatomic) IBOutlet UITextField *txt_1A;
@property (strong, nonatomic) IBOutlet UITextField *txt_1B;
@property (strong, nonatomic) IBOutlet UITextField *txt_1C;
@property (strong, nonatomic) IBOutlet UITextField *txt_ZipCodeView4;
@property (strong, nonatomic) IBOutlet UITextField *txt_CityView4;
@property (strong, nonatomic) IBOutlet UITextField *txt_State;
@property (strong, nonatomic) IBOutlet UITextField *txt_TelephoneNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_1D;
@property (strong, nonatomic) IBOutlet UITextField *txt_2;
@property (strong, nonatomic) IBOutlet UITextField *txt_3;
@property (strong, nonatomic) IBOutlet UITextField *txt_4B;
@property (strong, nonatomic) IBOutlet UITextField *txt_PersonPurchasingInspection;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_1E;
- (IBAction)action_btnRadio_1_1E:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_1E;
- (IBAction)action_btnRadio_2_1E:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_4A;
- (IBAction)action_btnRadio_1_4A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_4A;
- (IBAction)action_btnRadio_2_4A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_3_4A;
- (IBAction)action_btnRadio_3_4A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_4_4A;
- (IBAction)action_btnRadio_4_4A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_5_4A;
- (IBAction)action_btnRadio_5_4A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_4C;
- (IBAction)action_btnChkBox_1_4C:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_4C;
- (IBAction)action_btnChkBox_2_4C:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_4C;
- (IBAction)action_btnChkBox_3_4C:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_4C;
- (IBAction)action_btnChkBox_4_4C:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_5_4C;
- (IBAction)action_btnChkBox_5_4C:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnInspectionDate;
- (IBAction)action_InspectionDate:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_4A_Other;


//View5

@property (strong, nonatomic) IBOutlet UITextField *txt_5;
@property (strong, nonatomic) IBOutlet UITextField *txt_6BSpecify;
@property (strong, nonatomic) IBOutlet UITextField *txt_7BSpecify;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_6A;
- (IBAction)action_btnRadio_1_6A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_6A;
- (IBAction)action_btnRadio_2_6A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_6B;
- (IBAction)action_btnChkBox_6B:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_6B;
- (IBAction)action_btnChkBox_1_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_6B;
- (IBAction)action_btnChkBox_2_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_6B;
- (IBAction)action_btnChkBox_3_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_6B;
- (IBAction)action_btnChkBox_4_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_5_6B;
- (IBAction)action_btnChkBox_5_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_6_6B;
- (IBAction)action_btnChkBox_6_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_7_6B;
- (IBAction)action_btnChkBox_7_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_8_6B;
- (IBAction)action_btnChkBox_8_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_9_6B;
- (IBAction)action_btnChkBox_9_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_10_6B;
- (IBAction)action_btnChkBox_10_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_11_6B;
- (IBAction)action_btnChkBox_11_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_12_6B;
- (IBAction)action_btnChkBox_12_6B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_13_6B;
- (IBAction)action_btnChkBox_13_6B:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_7A;
- (IBAction)action_btnRadio_1_7A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_7A;
- (IBAction)action_btnRadio_2_7A:(id)sender;


@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_7B;
- (IBAction)action_btnChkBox_1_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_7B;
- (IBAction)action_btnChkBox_2_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_7B;
- (IBAction)action_btnChkBox_3_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_7B;
- (IBAction)action_btnChkBox_4_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_5_7B;
- (IBAction)action_btnChkBox_5_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_6_7B;
- (IBAction)action_btnChkBox_6_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_7_7B;
- (IBAction)action_btnChkBox_7_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_8_7B;
- (IBAction)action_btnChkBox_8_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_9_7B;
- (IBAction)action_btnChkBox_9_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_10_7B;
- (IBAction)action_btnChkBox_10_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_11_7B;
- (IBAction)action_btnChkBox_11_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_12_7B;
- (IBAction)action_btnChkBox_12_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_13_7B;
- (IBAction)action_btnChkBox_13_7B:(id)sender;


//View 6
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8_ActiveInfestation;
- (IBAction)action_btnRadio_1_8_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8_ActiveInfestation;
- (IBAction)action_btnRadio_2_8_ActiveInfestation:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8_PreviousInfestation;
- (IBAction)action_btnRadio_1_8_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8_PreviousInfestation;
- (IBAction)action_btnRadio_2_8_PreviousInfestation:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8_PreviousTreatment;
- (IBAction)action_btnRadio_1_8_PreviousTreatment:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8_PreviousTreatment;
- (IBAction)action_btnRadio_2_8_PreviousTreatment:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8A_ActiveInfestation;
- (IBAction)action_btnRadio_1_8A_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8A_ActiveInfestation;
- (IBAction)action_btnRadio_2_8A_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8A_PreviousInfestation;
- (IBAction)action_btnRadio_1_8A_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8A_PreviousInfestation;
- (IBAction)action_btnRadio_2_8A_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8A_PreviousTreatment;
- (IBAction)action_btnRadio_1_8A_PreviousTreatment:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8A_PreviousTreatment;
- (IBAction)action_btnRadio_2_8A_PreviousTreatment:(id)sender;


@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8B_ActiveInfestation;
- (IBAction)action_btnRadio_1_8B_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8B_ActiveInfestation;
- (IBAction)action_btnRadio_2_8B_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8B_PreviousInfestation;
- (IBAction)action_btnRadio_1_8B_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8B_PreviousInfestation;
- (IBAction)action_btnRadio_2_8B_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8B_PreviousTreatment;
- (IBAction)action_btnRadio_1_8B_PreviousTreatment:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8B_PreviousTreatment;
- (IBAction)action_btnRadio_2_8B_PreviousTreatment:(id)sender;


//View 7
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8C_ActiveInfestation;
- (IBAction)action_btnRadio_1_8C_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8C_ActiveInfestation;
- (IBAction)action_btnRadio_2_8C_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8C_PreviousInfestation;
- (IBAction)action_btnRadio_1_8C_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8C_PreviousInfestation;
- (IBAction)action_btnRadio_2_8C_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8C_PreviousTreatment;
- (IBAction)action_btnRadio_1_8C_PreviousTreatment:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8C_PreviousTreatment;
- (IBAction)action_btnRadio_2_8C_PreviousTreatment:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8D_ActiveInfestation;
- (IBAction)action_btnRadio_1_8D_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8D_ActiveInfestation;
- (IBAction)action_btnRadio_2_8D_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8D_PreviousInfestation;
- (IBAction)action_btnRadio_1_8D_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8D_PreviousInfestation;
- (IBAction)action_btnRadio_2_8D_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8D_PreviousTreatment;
- (IBAction)action_btnRadio_1_8D_PreviousTreatment:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8D_PreviousTreatment;
- (IBAction)action_btnRadio_2_8D_PreviousTreatment:(id)sender;


@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8E_ActiveInfestation;
- (IBAction)action_btnRadio_1_8E_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8E_ActiveInfestation;
- (IBAction)action_btnRadio_2_8E_ActiveInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8E_PreviousInfestation;
- (IBAction)action_btnRadio_1_8E_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8E_PreviousInfestation;
- (IBAction)action_btnRadio_2_8E_PreviousInfestation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8E_PreviousTreatment;
- (IBAction)action_btnRadio_1_8E_PreviousTreatment:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8E_PreviousTreatment;
- (IBAction)action_btnRadio_2_8E_PreviousTreatment:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_8E;



//View8
@property (strong, nonatomic) IBOutlet UITextField *txt_8F;
@property (strong, nonatomic) IBOutlet UITextField *txt_8G;
@property (strong, nonatomic) IBOutlet UITextField *txt_8GD;
@property (strong, nonatomic) IBOutlet UITextField *txt_9;
@property (strong, nonatomic) IBOutlet UITextField *txt_9BSpecifyReason;
@property (strong, nonatomic) IBOutlet UITextField *txt_9B;
@property (strong, nonatomic) IBOutlet UITextField *txt_10A;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_9;
- (IBAction)action_btnRadio_1_9:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_9;
- (IBAction)action_btnRadio_2_9:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_9A;
- (IBAction)action_btnRadio_1_9A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_9A;
- (IBAction)action_btnRadio_2_9A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_9B;
- (IBAction)action_btnRadio_1_9B:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_9B;
- (IBAction)action_btnRadio_2_9B:(id)sender;

//View9

@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_10A_Subterranean;
- (IBAction)action_btnChkBox_1_10A_Subterranean:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_10A_Subterranean;
- (IBAction)action_btnChkBox_2_10A_Subterranean:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_10A_Subterranean;
- (IBAction)action_btnChkBox_3_10A_Subterranean:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_10A_Subterranean;
- (IBAction)action_btnChkBox_4_10A_Subterranean:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_10A_Drywood;
- (IBAction)action_btnChkBox_1_10A_Drywood:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_10A_Drywood;
- (IBAction)action_btnChkBox_2_10A_Drywood:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_DateTreatmentByInspectionCompany;
- (IBAction)action_btn_DateTreatmentByInspectionCompany:(id)sender;


@property (strong, nonatomic) IBOutlet UITextField *txt_DateOfTreatment;
@property (strong, nonatomic) IBOutlet UITextField *txt_CommonNameOfInsect;
@property (strong, nonatomic) IBOutlet UITextField *txt_NameOfPesticide;
@property (strong, nonatomic) IBOutlet UITextField *txt_ListInsects;


@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_10A_Contract;
- (IBAction)action_btnRadio_1_10A_Contract:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_10A_Contract;
- (IBAction)action_btnRadio_2_10A_Contract:(id)sender;


//Vie10
@property (strong, nonatomic) IBOutlet UIButton *btnBuyerIntials;
- (IBAction)action_btnBuyerIntials:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraph;
- (IBAction)action_btnAddGraph:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnInspectorSignature;
- (IBAction)action_btnInspectorSignature:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_AdditionalComments;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBuyerIntials;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAddGraph;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewInspectorSign;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraphImg;
- (IBAction)action_ButtonAddGraphimg:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_Specifys_TexasOffiicial_WoodDestroyingInsect;


//View11
@property (strong, nonatomic) IBOutlet UIButton *btnCertifiedApplicatorSign;
- (IBAction)action_btnCertifiedApplicatorSign:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnBuyerIntialsPurchaser;
- (IBAction)action_btnBuyerIntialsPurchaser:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCertifiedApplicatorSign;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBuyerIntialsPurchaser;
@property (strong, nonatomic) IBOutlet UITextField *txt_12B;
@property (strong, nonatomic) IBOutlet UITextField *txt_ApplicatorLicenseNumber;

@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_12A;
- (IBAction)action_btnChkBox_1_12A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_12A;
- (IBAction)action_btnChkBox_2_12A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_12A;
- (IBAction)action_btnChkBox_3_12A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_12A;
- (IBAction)action_btnChkBox_4_12A:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_DatePosted;
- (IBAction)action_BtnDatePosted:(id)sender;


//View12

@property (strong, nonatomic) IBOutlet UIButton *btnFinalSavenContinue;
- (IBAction)action_btnFinalSavenContinue:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnBuyersInitial_7B;
- (IBAction)action_btnBuyersInitial_7B:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_BuyerInitial_7B;
@property (strong, nonatomic) IBOutlet UIButton *btn_DateOfPurchaserOfProperty;
- (IBAction)action_btn_DateOfPurchaserOfProperty:(id)sender;
//View For Graph Image


//View for Collectionview 7A
@property (strong, nonatomic) IBOutlet UIView *viewForcollectionView7A;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView7A;
- (IBAction)actionOnAddImageForCollectionView7A:(id)sender;

//View For ForcollectionView8
@property (strong, nonatomic) IBOutlet UIView *viewForcollectionView8;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView8;
- (IBAction)actionOnAddImageForCollectionView8:(id)sender;

- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;


@end
