//
//  TermiteSelectStateViewControlleriPad.h
//  DPS
//
//  Created by Rakesh Jain on 22/12/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
@interface TermiteSelectStateViewControlleriPad : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityWorkOrder;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;

@property (strong, nonatomic)  NSString *strWorkOrder,*strTermiteStateType;
@property (strong, nonatomic)  NSString *strWorkOrderStatus;

- (IBAction)action_Texas:(id)sender;
- (IBAction)action_Florida:(id)sender;
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAccoNo;
@property (weak, nonatomic) IBOutlet UIButton *btnTexas;
@property (weak, nonatomic) IBOutlet UIButton *btnFlorida;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;
@end
