//
//  TermiteInspectionFloridaViewControlleriPad.h
//  DPS
//
//  Created by Rakesh Jain on 06/01/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"
#import "FloridaTermiteServiceDetail+CoreDataClass.h"
#import "FloridaTermiteServiceDetail+CoreDataProperties.h"
@interface TermiteInspectionFloridaViewControlleriPad : UIViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    
    //Florida
    NSEntityDescription *entityTermiteFlorida;
    NSFetchRequest *requestTermiteFlorida;
    NSSortDescriptor *sortDescriptorTermiteFlorida;
    NSArray *sortDescriptorsTermiteFlorida;
    NSManagedObject *matchesTermiteFlorida;
    NSManagedObject *matchesWorkOrder;
    
    NSArray *arrAllObjTermiteFlorida;
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrderNew;
    NSArray *arrAllObjWorkOrder;
    
    NSEntityDescription *entityModifyDateServiceAuto;
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
    
    //After Image Nilind
    
    NSFetchRequest *requestImageDetail;
    NSEntityDescription *entityImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSArray *arrAllObjImageDetail;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails,*fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic)  NSString *strWorkOrder;
@property (strong, nonatomic)  NSString *strWorkOrderStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountNo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVieww;
- (IBAction)action_Back:(id)sender;



@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *view3;
@property (strong, nonatomic) IBOutlet UIView *view4;
@property (strong, nonatomic) IBOutlet UIView *view5;


//View1
@property (weak, nonatomic) IBOutlet UITextField *txtInspectionCompany;
@property (weak, nonatomic) IBOutlet UITextField *txtBusinessLicenseNo;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNo;

@property (weak, nonatomic) IBOutlet UITextField *txtCompanyCity;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyState;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyZipcode;

@property (weak, nonatomic) IBOutlet UITextField *txtDateOfInspection;
@property (weak, nonatomic) IBOutlet UIButton *btnDateOfInspection;
- (IBAction)actionOnBtnDateOfInspection:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt1InspectorNameAndIdentificationNo;
@property (weak, nonatomic) IBOutlet UITextField *txt2InspectorNameAndIdentificationNo;
@property (weak, nonatomic) IBOutlet UITextField *txtAddressOfPropoertyInspected;
@property (weak, nonatomic) IBOutlet UITextField *txtStructurePropertyInspected;
@property (weak, nonatomic) IBOutlet UITextField *txtInspectionAndReportRequestedBy;
@property (weak, nonatomic) IBOutlet UITextField *txtInspectionAndReportRequestedTo;

//View2
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxA;
- (IBAction)actionOnBtnCheckBoxA:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxB;
- (IBAction)actionOnBtnCheckBoxB:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxLiveWDO;
- (IBAction)actionOnBtnChkBoxLiveWDO:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtLiveWDO;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxEvidenceOfWDO;
- (IBAction)actionOnbtnCheckBoxEvidenceOfWDO:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewEvidenceOfWDO;
@property (weak, nonatomic) IBOutlet UIButton *btnChkBoxDAMAGEcausedbyWDO;
- (IBAction)actionOnbtnChkBoxDAMAGEcausedbyWDO:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDAMAGEcausedbyWDO;

//View3

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxAttic;
- (IBAction)actionOnBtnCheckBoxAttic:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAtticSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAtticReason;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxInterior;
- (IBAction)actionOnBtnChkBoxInterior:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldInteriorSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txtViewInteriorReason;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxExterior;
- (IBAction)actionOnCheckBoxExterior:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldExteriorSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txtViewExteriorReason;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxCrawlPlace;
- (IBAction)actionOnBtnChkBoxCrawlPlace:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldCrawlPlaceSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txViewCrawlPlaceReason;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxOther;
- (IBAction)actionOnBtnCheckBoxOther:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldOtherSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txtViewOtherReason;

//View4
@property (weak, nonatomic) IBOutlet UIButton *btnRadioEvidencForYes;
- (IBAction)actionOnBtnRadioEvidenceYes:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnRadioEvidenceForNo;
- (IBAction)actionOnBtnRadioEvidenceNO:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtFieldForEvidence;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForNoticeOfInspection;

@property (weak, nonatomic) IBOutlet UIButton *btnRadioCompanyStructureForYes;
- (IBAction)actionOnBtnRadioCompanyStructureYes:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRadioCompnayStructureForNo;
- (IBAction)actionOnBtnRadioCompanyStructureNo:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForCommonNameOfOrganism;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForNameOfPesticideUsed;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForTermsAndCondition;
@property (weak, nonatomic) IBOutlet UIButton *btnChkBoxWholeStructure;
- (IBAction)actionOnBtnChckBoxWholeStructure:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnChkBoxSpotTreatment;
- (IBAction)actionOnBtnChkBoxSpotTreatment:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForMethodOfTreatment;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForSpecifyTreatmentNotice;

//View5
@property (weak, nonatomic) IBOutlet UITextView *txtViewForComment;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSignatureofLicenseAgent;
@property (weak, nonatomic) IBOutlet UIButton *btnSignatureLicenseAgent;
- (IBAction)actionOnbtnSignatureLicenseAgent:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldDateForView5;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddressOfPropertyInspectedView5;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForInspectionDateForView5;
@property (weak, nonatomic) IBOutlet UIButton *btnDateOfLicenceeAgent;
- (IBAction)actionOnBtnDateOfLicenceeAgent:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnInspectionDate;
- (IBAction)actionOnBtnInspectionDate:(id)sender;
- (IBAction)actionOnCancel:(id)sender;

- (IBAction)actionOnSave:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;
@end
