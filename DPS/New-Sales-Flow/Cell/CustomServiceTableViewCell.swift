//
//  CustomServiceTableViewCell.swift
//  DPS
//
//  Created by Saavan Patidar on 17/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol CustomServiceTableCellProtocol: class {
    
    func btnDeleteCustomServiceClicked(matchesObject : NSManagedObject)
    func btnEditCustomServiceClicked(matchesObject : NSManagedObject)
}

class CustomServiceTableViewCell: UITableViewCell {

    
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceFrequencyName: UILabel!
    @IBOutlet var lblBillingFrequencyName: UILabel!
    var delegate : CustomServiceTableCellProtocol?
    
    @IBOutlet weak var lblInitialPriceTitle: UILabel!
    @IBOutlet var lblInitialPrice: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblDiscountPer: UILabel!
    @IBOutlet var lblMaintPrice: UILabel!
    
    @IBOutlet weak var lblFinalMaintPriceTitle: UILabel!
    @IBOutlet weak var lblFinalInitialPriceTitle: UILabel!
    @IBOutlet weak var lblMaintPriceTitle: UILabel!
    @IBOutlet var lblFinalInitialPrice: UILabel!
    @IBOutlet var lblFinalMaintPrice: UILabel!
    var dictLoginData = NSDictionary()
    var chkFreqConfiguration = false
    var matches = NSManagedObject()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(matchesObject : NSManagedObject) {
        
        matches = matchesObject
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "frequencySysName") ?? "")")
        
        lblServiceName.text = "\(matchesObject.value(forKey: "serviceName") ?? "")"
                
        lblServiceFrequencyName.text = "\(matchesObject.value(forKey: "serviceFrequency") ?? "")"
        
        
        
//        if lblServiceFrequencyName.text?.caseInsensitiveCompare("OneTime") == .orderedSame || lblServiceFrequencyName.text?.caseInsensitiveCompare("One Time") == .orderedSame
//        {
//            lblMaintPrice.isHidden = true
//            lblMaintPriceTitle.isHidden = true
//            lblInitialPriceTitle.text = "One Time Initial Price"
//            lblFinalInitialPriceTitle.text = "Final One Time Initial Price"
//
//            lblFinalMaintPrice.isHidden = true
//            lblFinalMaintPriceTitle.isHidden = true
//        }
//        else
//        {
//            lblMaintPrice.isHidden = false
//            lblMaintPriceTitle.isHidden = false
//            lblInitialPriceTitle.text = "Initial Price"
//            lblFinalInitialPriceTitle.text = "Final Initial Price"
//
//            lblFinalMaintPrice.isHidden = false
//            lblFinalMaintPriceTitle.isHidden = false
//        }
        
        lblBillingFrequencyName.text = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
        
        lblInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
        
        lblMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
        
        lblDiscount.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).doubleValue))
        
        lblDiscountPer.text = stringToFloat(strValue: "\(matchesObject.value(forKey: "discountPercentage") ?? "")")
        
        //lblFinalInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
        
        //lblFinalMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
        
        
        lblFinalInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue) - Double(("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).doubleValue))
        
        lblFinalMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
        
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = ("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue
        totalMaintPrice = ("\(matchesObject.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        
        if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1) ) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBillingFrequencyName.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
        
    }
    
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
    
    @IBAction func actionOnDeleteService(_ sender: Any) {
        
        delegate?.btnDeleteCustomServiceClicked(matchesObject: matches)
    }
    
    @IBAction func actionOnEditService(_ sender: Any) {
        
        delegate?.btnEditCustomServiceClicked(matchesObject: matches)
    }
}
