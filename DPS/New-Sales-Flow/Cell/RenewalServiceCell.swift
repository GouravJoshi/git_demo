//
//  RenewalServiceCell.swift
//  DPS
//
//  Created by Saavan Patidar on 29/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol RenewalServiceCellProtocol: class {
    
    func btnEditDescClicked(matchesObject : NSManagedObject)
    func getTextFieldActionRenewal(txtTag : Int, value : String, matchesObject : NSManagedObject)
    func btnDeleteRenewalServiceClicked(matchesObject : NSManagedObject)

}

class RenewalServiceCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet var lblRenewalService: UILabel!
    @IBOutlet var lblRenewalFrequency: UILabel!
    @IBOutlet var txtRenewalPrice: UITextField!
    
    @IBOutlet var lblRenewalDesc: UILabel!
    @IBOutlet var btnEditDesc: UIButton!
    
    var delegate: RenewalServiceCellProtocol?
    var matchesRenewalObject = NSManagedObject()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(matchesObject : NSManagedObject) {
        
        txtRenewalPrice.delegate = self
        txtRenewalPrice.keyboardType = UIKeyboardType.decimalPad
        
        matchesRenewalObject = matchesObject
        
        let dictService = getServiceObjectFromIdAndSysName(strId: "\(matchesObject.value(forKey: "serviceId") ?? "")", strSysName: "")

        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "renewalFrequencySysName") ?? "")")
        
        lblRenewalService.text = "\(dictService.value(forKey: "Name") ?? "")"
        
        lblRenewalFrequency.text =  "\(dictServiceFrequency.value(forKey: "FrequencyName") ?? "")"
        
        txtRenewalPrice.text = stringToFloat(strValue: "\(matchesObject.value(forKey: "renewalAmount") ?? "")")
        
        let strText = "\(matchesObject.value(forKey: "renewalDescription") ?? "")"
        
        if strText.count > 0
        {
//            let data = Data("\(strText)".utf8)
//            if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//            {
//                lblRenewalDesc.attributedText = attributedString
//            }
            lblRenewalDesc.text = strText
        }
        else
        {
            lblRenewalDesc.text = " "
        }

    }
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
    
    
    // MARK: ------------ TEXT FIELD EXTENSION ------------
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
       //let pointInTable = textField.convert(textField.bounds.origin, to: UITableViewCell)
        //       let textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        delegate?.getTextFieldActionRenewal(txtTag: textField.tag, value : textField.text ?? "", matchesObject: matchesRenewalObject)
    }

    @IBAction func actionOnEditDesc(_ sender: Any) {
        
        delegate?.btnEditDescClicked(matchesObject: matchesRenewalObject)
    }
    
    @IBAction func actionOnDeleteService(_ sender: Any) {
        
        delegate?.btnDeleteRenewalServiceClicked(matchesObject: matchesRenewalObject)
    }
    
}

