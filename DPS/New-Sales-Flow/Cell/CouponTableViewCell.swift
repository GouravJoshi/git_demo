//
//  CouponTableViewCell.swift
//  DPS
//
//  Created by Saavan Patidar on 01/07/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol CouponTableCellProtocol: class {

    func btnDeleteCouponServiceClicked(matchesObject : NSManagedObject)
}


class CouponTableViewCell: UITableViewCell {

    @IBOutlet var lblCouponName: UILabel!
    @IBOutlet var btnDelete: UIButton!
    var delegate : CouponTableCellProtocol?
    @IBOutlet var lblCouponDesc: UILabel!
    @IBOutlet var txtFldCouponAmount: UITextField!
    var matches = NSManagedObject()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(matchesObject : NSManagedObject) {
        matches = matchesObject
        
        lblCouponName.text = "\(matches.value(forKey: "name") ?? "")".count == 0 ? "\(matches.value(forKey: "discountCode") ?? "")" : "\(matches.value(forKey: "name") ?? "")"
        txtFldCouponAmount.text = convertDoubleToCurrency(amount: Double("\(matches.value(forKey: "appliedInitialDiscount") ?? "")") ?? 0.0)
        lblCouponDesc.text = "\(matches.value(forKey: "discountDescription") ?? "")"

    }
    
    @IBAction func actionOnDeleteCoupon(_ sender: Any) {
        
        delegate?.btnDeleteCouponServiceClicked(matchesObject: matches)
    }
}
