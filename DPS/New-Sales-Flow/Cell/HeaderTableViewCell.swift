//
//  HeaderTableViewCell.swift
//  DPS
//
//  Created by Saavan Patidar on 14/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol HeaderTitleTableCellProtocol: class
{
    func deleteBundleNew(strId: String)
}

class HeaderTableViewCell: UITableViewCell {

    
    @IBOutlet var tblBundleService: UITableView!
    var dictLoginData = NSDictionary()
    var arrBundle = NSArray()
    var arrBundleDetail = NSArray()
    var arrBundleIdCount = NSArray()
    var delegate: HeaderTitleTableCellProtocol?
    var strEmpName = ""
    var strUserName = ""
    var strCompanyKey = ""
    var strLeadId = "\(nsud.value(forKey: "LeadId") ?? "")"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tblBundleService.tableFooterView = UIView(frame: .zero)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureTable() {
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        arrBundle = fetchBundleServiceFromCoreData()

        tblBundleService.dataSource = self
        tblBundleService.estimatedRowHeight = 320
        tblBundleService.tableFooterView = UIView(frame: .zero)

        tblBundleService.rowHeight = UITableView.automaticDimension
        tblBundleService.delegate = self
        tblBundleService.isScrollEnabled = true
        tblBundleService.reloadData()
    }
    
    func fetchBundleServiceFromCoreData() -> NSArray {
        
        var arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        
        let arr = arryOfData.filter { (matches) -> Bool in
            return "\((matches as! NSManagedObject).value(forKey: "bundleId") ?? "")" != "0" 
        } as NSArray
        
        arryOfData = arr
    
        return arryOfData
    }
    
    func fetchBundleDetailFromCoreData(strBundleId : String) -> NSArray {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && bundleId=%@", strUserName, strLeadId, strBundleId))
    
        return arryOfData
    }
    
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }

}
extension HeaderTableViewCell: UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        let count = NSMutableArray()
        for item in arrBundle
        {
            let matched = item as! NSManagedObject
            count.add("\(matched.value(forKey: "bundleId") ?? "")")
        }
        
        let conciseUniqueValues = count.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(conciseUniqueValues)")
        
        arrBundleIdCount = conciseUniqueValues
        
        return arrBundleIdCount.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tblBundleService.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 30))) // 40
        viewheader.backgroundColor = UIColor.init(red: 246/255, green: 246/255, blue: 246/255, alpha: 1)
        
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: 300, height: (DeviceType.IS_IPAD ? 50 : 30))) // 40
        
        let btnDelete = UIButton(frame: CGRect(x: viewheader.frame.maxX - 50, y: 0, width: (DeviceType.IS_IPAD ? 50 : 30), height: (DeviceType.IS_IPAD ? 50 : 30))) // 40
        btnDelete.tag = section
        btnDelete.setBackgroundImage(UIImage(named: "ic_action_delete"), for: .normal)
        btnDelete.addTarget(self, action: #selector(deleteBundle(sender:)), for: .touchUpInside)
        let strId = "\(arrBundleIdCount.object(at: section))"
        
        let dictBundle = getBundleObjectFromId(strId: strId) as NSDictionary
        
        lbl.text = "Bundle: \(dictBundle.value(forKey: "BundleName") ?? "")"
        lbl.textColor = UIColor.appThemeColor
       
        lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
        viewheader.addSubview(lbl)
        viewheader.addSubview(btnDelete)

        return viewheader
        
    }
    @objc func deleteBundle(sender : UIButton)
    {
        let strId = "\(arrBundleIdCount.object(at: sender.tag))"
        delegate?.deleteBundleNew(strId: strId)
                
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tblBundleService.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 30))) // 40
        viewheader.backgroundColor = UIColor.lightGray//init(red: 246/255, green: 246/255, blue: 246/255, alpha: 1)
        
        let lblIntialCost = UILabel(frame: CGRect(x: 0, y: 0, width: tblBundleService.frame.size.width/2, height: (DeviceType.IS_IPAD ? 50 : 30))) //40
        
        let lblMaintCost = UILabel(frame: CGRect(x: lblIntialCost.frame.maxX + 10, y: 0, width: lblIntialCost.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 30))) //40

                
        let strId = "\(arrBundleIdCount.object(at: section))"
        arrBundleDetail = fetchBundleDetailFromCoreData(strBundleId: strId)
        
        var totalBundleInitialCost = 0.0, totalBundleMaintCost = 0.0
        
        for item in arrBundleDetail
        {
            let match = item as! NSManagedObject
            
            totalBundleInitialCost = totalBundleInitialCost + ("\(match.value(forKey: "totalInitialPrice") ?? "")" as NSString).doubleValue
            totalBundleMaintCost = totalBundleMaintCost + ("\(match.value(forKey: "totalMaintPrice") ?? "")" as NSString).doubleValue
        }

        lblIntialCost.text = "Total Initial Price : \(convertDoubleToCurrency(amount: totalBundleInitialCost))"
        lblMaintCost.text = "Total Maint Price : \(convertDoubleToCurrency(amount: totalBundleMaintCost))"

        lblIntialCost.textColor = UIColor.black
       
        lblIntialCost.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 12))
        lblMaintCost.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 12))

        viewheader.addSubview(lblIntialCost)
        viewheader.addSubview(lblMaintCost)

        return viewheader
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
        
        return (DeviceType.IS_IPAD ? 50 : 30) //60 50
       
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return (DeviceType.IS_IPAD ? 50 : 30)

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let strBundleId = "\(arrBundleIdCount.object(at: section))"//"\(matchesObject.value(forKey: "bundleId") ?? "")"
        arrBundleDetail = fetchBundleDetailFromCoreData(strBundleId: strBundleId)
        return arrBundleDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceTableViewCell", for: indexPath) as! BundleServiceTableViewCell

        let strBundleId = "\(arrBundleIdCount.object(at: indexPath.section))"//"\(matchesObject.value(forKey: "bundleId") ?? "")"
        arrBundleDetail = fetchBundleDetailFromCoreData(strBundleId: strBundleId)
        
        let matchesObject = arrBundleDetail.object(at: indexPath.row) as! NSManagedObject
        cell.setData(matchesObject: matchesObject)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return UITableView.automaticDimension
    }
    
}

