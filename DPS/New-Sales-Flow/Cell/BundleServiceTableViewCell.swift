//
//  BundleServiceTableViewCell.swift
//  DPS
//
//  Created by Saavan Patidar on 14/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class BundleServiceTableViewCell: UITableViewCell {

    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceFrequencyName: UILabel!
    @IBOutlet var lblBillingFrequencyName: UILabel!
    
    @IBOutlet var lblInitialPrice: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblDiscountPer: UILabel!
    @IBOutlet var lblMaintPrice: UILabel!
    
    @IBOutlet var viewUnitPrice: UIView!
    @IBOutlet var lblUnitValue: UILabel!
    @IBOutlet var lblUnitBasedInitialPrice: UILabel!
    @IBOutlet var lblUnitBasedMaintPrice: UILabel!

    @IBOutlet var viewParameterService: UIView!
    
    @IBOutlet var lblFinalInitialPrice: UILabel!
    @IBOutlet var lblFinalMaintPrice: UILabel!
    
    var dictLoginData = NSDictionary()
    var chkFreqConfiguration = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(matchesObject : NSManagedObject) {
        
        
        
        var isUnitBased = false
        var isParameterBased = false
        
        let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "serviceSysName") ?? "")")
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "frequencySysName") ?? "")")

        if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
        {
            isUnitBased = true
        }
        else
        {
            isUnitBased = false
        }
        
        if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
        {
            isParameterBased = true
        }
        else
        {
            isParameterBased = false
        }
        
        lblServiceName.text = "\(dictService.value(forKey: "Name") ?? "")"
                
        lblServiceFrequencyName.text = "\(matchesObject.value(forKey: "serviceFrequency") ?? "")"
        
        lblBillingFrequencyName.text = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
        
        lblInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
        
        lblMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
        
        lblDiscount.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).doubleValue))
        
        lblDiscountPer.text = stringToFloat(strValue: "\(matchesObject.value(forKey: "discountPercentage") ?? "")")
        
        lblFinalInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "totalInitialPrice") ?? "")" as NSString).doubleValue))
        
        lblFinalMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "totalMaintPrice") ?? "")" as NSString).doubleValue))

        if isUnitBased
        {
            viewUnitPrice.isHidden = false
            lblUnitValue.text = "\(matchesObject.value(forKey: "unit") ?? "")"
            lblUnitBasedInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "finalUnitBasedInitialPrice") ?? "")" as NSString).doubleValue))
            lblUnitBasedMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "finalUnitBasedMaintPrice") ?? "")" as NSString).doubleValue))
        }
        else
        {
            viewUnitPrice.isHidden = true
        }
        
        if isParameterBased
        {
            viewParameterService.isHidden = false
        }
        else
        {
           viewParameterService.isHidden = true
        }
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = ("\(matchesObject.value(forKey: "totalInitialPrice") ?? "")" as NSString).doubleValue
        totalMaintPrice = ("\(matchesObject.value(forKey: "totalMaintPrice") ?? "")" as NSString).doubleValue

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        
        if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBillingFrequencyName.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
    }
    
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }

}


