//
//  StandardServiceTableViewCell.swift
//  DPS
//
//  Created by Saavan Patidar on 17/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol StandardTableCellProtocol: class {
    
    func addPlusService(arrPlusService : NSMutableArray, matchesParentObject: NSManagedObject , strServiceSysName: String, strBillingFreqSysName: String, strServiceFreqSysName: String)
    func getTextFieldAction(txtTag : Int, value : String, matchesObject : NSManagedObject)
    func btnDeleteServiceClicked(matchesObject : NSManagedObject)
    func btnEditServiceClicked(matchesObject : NSManagedObject)

}

class StandardServiceTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceFrequencyName: UILabel!
    @IBOutlet var lblBillingFrequencyName: UILabel!
    
    @IBOutlet var lblHeadingInitialPrice: UILabel!
    @IBOutlet var lblInitialPrice: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblDiscountPer: UILabel!
    @IBOutlet var lblMaintPrice: UILabel!
    
    @IBOutlet var lblHeadingMaintPrice: UILabel!
    @IBOutlet var viewUnitPrice: UIView!
    @IBOutlet var lblUnitValue: UILabel!
    @IBOutlet var lblUnitBasedInitialPrice: UILabel!
    @IBOutlet var lblUnitBasedMaintPrice: UILabel!

    @IBOutlet var lblHeadingUnitBasedMaintPrice: UILabel!
    @IBOutlet var viewParameterService: UIView!
    
    @IBOutlet var lblFinalInitialPrice: UILabel!
    @IBOutlet var lblFinalMaintPrice: UILabel!
    
    @IBOutlet var lblHeadingFinalMaintPrice: UILabel!
    @IBOutlet var btnPlusService: UIButton!
    
    @IBOutlet var const_ViewParameterService_H: NSLayoutConstraint!
    
    var dictLoginData = NSDictionary()
    var chkFreqConfiguration = false
    var delegate : StandardTableCellProtocol?
    var arrPlusService = NSMutableArray()
    var strServiceFreqSysName = String()
    var strBillingFreqSysName = String()
    var strServiceSysName = String()
    var matchesParentObject = NSManagedObject()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(matchesObject : NSManagedObject) {
        
        forOneTimeService(matchesObject: matchesObject)
        
        
        var isUnitBased = false
        var isParameterBased = false
        matchesParentObject = matchesObject
        let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "serviceSysName") ?? "")")
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "frequencySysName") ?? "")")
        if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
        {
            isUnitBased = true
        }
        else
        {
            isUnitBased = false
        }
        
        if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
        {
            isParameterBased = true
        }
        else
        {
            isParameterBased = false
        }
        
        lblServiceName.text = "\(dictService.value(forKey: "Name") ?? "")"
                
        lblServiceFrequencyName.text = "\(matchesObject.value(forKey: "serviceFrequency") ?? "")"
        
        lblBillingFrequencyName.text = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
        
        //lblInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
        lblInitialPrice.text = convertDoubleToCurrency(amount: Double((handleNegativeValue(strValue: "\(matchesObject.value(forKey: "initialPrice") ?? "")") as NSString).doubleValue))

        //lblMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
        lblMaintPrice.text = convertDoubleToCurrency(amount: Double((handleNegativeValue(strValue: "\(matchesObject.value(forKey: "maintenancePrice") ?? "")") as NSString).doubleValue))

        
        
        //lblDiscount.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).doubleValue))
        lblDiscount.text = convertDoubleToCurrency(amount: Double((handleNegativeValue(strValue: "\(matchesObject.value(forKey: "discount") ?? "")") as NSString).doubleValue))

        
        
        lblDiscountPer.text = stringToFloat(strValue: "\(matchesObject.value(forKey: "discountPercentage") ?? "")")
        
        //lblFinalInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "totalInitialPrice") ?? "")" as NSString).doubleValue))
        lblFinalInitialPrice.text = convertDoubleToCurrency(amount: Double((handleNegativeValue(strValue: "\(matchesObject.value(forKey: "totalInitialPrice") ?? "")") as NSString).doubleValue))

    
        //lblFinalMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "totalMaintPrice") ?? "")" as NSString).doubleValue))
        lblFinalMaintPrice.text = convertDoubleToCurrency(amount: Double((handleNegativeValue(strValue: "\(matchesObject.value(forKey: "totalMaintPrice") ?? "")") as NSString).doubleValue))

        
        

        if isUnitBased
        {
            viewUnitPrice.isHidden = false
            lblUnitValue.text = "\(matchesObject.value(forKey: "unit") ?? "")"
            //lblUnitBasedInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "finalUnitBasedInitialPrice") ?? "")" as NSString).doubleValue))
            lblUnitBasedInitialPrice.text = convertDoubleToCurrency(amount: Double((handleNegativeValue(strValue: "\(matchesObject.value(forKey: "finalUnitBasedInitialPrice") ?? "")") as NSString).doubleValue))

            //lblUnitBasedMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(matchesObject.value(forKey: "finalUnitBasedMaintPrice") ?? "")" as NSString).doubleValue))
            lblUnitBasedMaintPrice.text = convertDoubleToCurrency(amount: Double((handleNegativeValue(strValue: "\(matchesObject.value(forKey: "finalUnitBasedMaintPrice") ?? "")") as NSString).doubleValue))

        }
        else
        {
            viewUnitPrice.isHidden = true
        }
        
        if isParameterBased
        {
            viewParameterService.isHidden = false
        }
        else
        {
           viewParameterService.isHidden = true
        }
        
        //  Plus service login
        
        if isPlusServiceAvailable(strServiceSysName: "\(matchesObject.value(forKey: "serviceSysName") ?? "")")
        {
            btnPlusService.isHidden = false
        }
        else
        {
            btnPlusService.isHidden = true
        }
        strServiceFreqSysName = "\(matchesObject.value(forKey: "frequencySysName") ?? "")"
        strBillingFreqSysName = "\(matchesObject.value(forKey: "billingFrequencySysName") ?? "")"
        strServiceSysName = "\(matchesObject.value(forKey: "serviceSysName") ?? "")"
        
        
        var sumFinalInitialPrice = 0.0, sumFinalMaintPrice = 0.0

        
        //For Parameter based service
        
        if isParameterBased
        {
            let arrPara = matchesObject.value(forKey: "additionalParameterPriceDcs") as! NSArray
            
            if arrPara.count > 0
            {
                var maxY = 0
                for view in viewParameterService.subviews
                {
                    view.removeFromSuperview()
                }
                
                let heightLabel = DeviceType.IS_IPAD ? 30 : 20
                let basicSpace = DeviceType.IS_IPAD ? 5 : 3
                let lblSpace = DeviceType.IS_IPAD ? 30 : 20
                let editColor = UIColor.theme()
                

                let font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 14) : UIFont.systemFont(ofSize: 12)
                let fontHeading = DeviceType.IS_IPAD ? UIFont.boldSystemFont(ofSize: 16) : UIFont.boldSystemFont(ofSize: 14)

                for i in 0..<arrPara.count
                {
                    let dict = arrPara.object(at: i) as! NSDictionary
                    
                    let arrKey = dict.allKeys as NSArray
                    
                    if arrKey.contains("AdditionalParameterName")
                    {
                        var lbl = UILabel()
                        lbl = UILabel.init(frame: CGRect(x: lblSpace, y: maxY, width: Int(viewParameterService.frame.size.width) - 40, height: heightLabel))
                        
                        var strUnitName = ""

                        if "\(dict.value(forKey: "UnitName") ?? "")".count > 0
                        {
                            strUnitName = "(" + "\(dict.value(forKey: "UnitName") ?? "")" + ")"
                        }
                        else
                        {
                            strUnitName = ""
                        }
                        
                        lbl.text = "\(dict.value(forKey: "AdditionalParameterName") ?? "") \(strUnitName) :"
                        lbl.font = fontHeading
                        maxY = maxY + heightLabel + basicSpace
                        viewParameterService.addSubview(lbl)
                    }
                    if arrKey.contains("UnitName") && "\(dict.value(forKey: "UnitName") ?? "")".count > 0
                    {
                        var lbl = UILabel()
                        var txt = UITextField()
                        lbl = UILabel.init(frame: CGRect(x: lblSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        lbl.font = font
                        lbl.text = "Unit Name"
                        
                        txt = UITextField.init(frame: CGRect(x: Int(CGFloat(lbl.frame.maxX)) + basicSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        txt.font = font
                        txt.text = "\(dict.value(forKey: "UnitName") ?? "")"
                        txt.isEnabled = false
                        txt.tag = 1000+i
                        txt.addTarget(self, action: #selector(getTextAction), for: .touchUpInside)
                        //txt.textAlignment = .right
                        txt.delegate = self
                        txt.keyboardType = UIKeyboardType.decimalPad
                        //maxY = maxY + heightLabel + basicSpace
                        //viewParameterService.addSubview(lbl)
                        //viewParameterService.addSubview(txt)

                    }
                    if arrKey.contains("UnitName") && "\(dict.value(forKey: "UnitName") ?? "")".count > 0
                    {
                        var lbl = UILabel()
                        var txt = UITextField()

                        lbl = UILabel.init(frame: CGRect(x: lblSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        lbl.font = font
                        lbl.text = "Enter Unit"
                        lbl.textColor = editColor
                        
                        txt = UITextField.init(frame: CGRect(x: Int(CGFloat(lbl.frame.maxX)) + basicSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        txt.textColor = editColor
                        txt.font = font
                        txt.text = "\(dict.value(forKey: "Unit") ?? "")"
                        addBottomLineTextField(txt: txt)
                        txt.tag = 2000+i
                        txt.delegate = self
                        txt.keyboardType = UIKeyboardType.decimalPad

                        txt.addTarget(self, action: #selector(getTextAction), for: .touchUpInside)

                        //txt.textAlignment = .right

                        maxY = maxY + heightLabel + basicSpace
                        
                        viewParameterService.addSubview(lbl)
                        viewParameterService.addSubview(txt)

                    }
                    if arrKey.contains("InitialUnitPrice")
                    {
                        var lbl = UILabel()
                        var txt = UITextField()

                        lbl = UILabel.init(frame: CGRect(x: lblSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        lbl.font = font
                        lbl.text = "Initial Unit Price"
                        
                        txt = UITextField.init(frame: CGRect(x: Int(CGFloat(lbl.frame.maxX)) + basicSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        txt.font = font
                        txt.text = stringToFloat(strValue: "\(dict.value(forKey: "InitialUnitPrice") ?? "")")
                        txt.tag = 3000+i
                        txt.addTarget(self, action: #selector(getTextAction), for: .touchUpInside)
                        txt.delegate = self
                        txt.keyboardType = UIKeyboardType.decimalPad
                        txt.isEnabled = false
                        //txt.textAlignment = .right

                        
                        maxY = maxY + heightLabel + basicSpace
                        
                        viewParameterService.addSubview(lbl)
                        viewParameterService.addSubview(txt)

                    }
                    if arrKey.contains("MaintUnitPrice")
                    {
                        var lbl = UILabel()
                        var txt = UITextField()

                        lbl = UILabel.init(frame: CGRect(x: lblSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        lbl.font = font
                        lbl.text = "Maint Unit Price"
                        
                        txt = UITextField.init(frame: CGRect(x: Int(CGFloat(lbl.frame.maxX)) + basicSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        txt.font = font
                        txt.text = stringToFloat(strValue: "\(dict.value(forKey: "MaintUnitPrice") ?? "")")
                        txt.tag = 4000+i
                        txt.addTarget(self, action: #selector(getTextAction), for: .touchUpInside)
                        txt.delegate = self
                        txt.keyboardType = UIKeyboardType.decimalPad
                        txt.isEnabled = false
                        //txt.textAlignment = .right

                        maxY = maxY + heightLabel + basicSpace
                        
                        viewParameterService.addSubview(lbl)
                        viewParameterService.addSubview(txt)

                    }
                    if arrKey.contains("FinalInitialUnitPrice")
                    {
                        var lbl = UILabel()
                        var txt = UITextField()

                        lbl = UILabel.init(frame: CGRect(x: lblSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        lbl.font = font
                        lbl.textColor = editColor
                        lbl.text = "Total Initial Price"
                        
                        txt = UITextField.init(frame: CGRect(x: Int(CGFloat(lbl.frame.maxX)) + basicSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        txt.font = font
                        txt.textColor = editColor
                        txt.text = stringToFloat(strValue: "\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")")
                        txt.tag = 5000+i
                        txt.addTarget(self, action: #selector(getTextAction), for: .touchUpInside)
                        txt.delegate = self
                        txt.keyboardType = UIKeyboardType.decimalPad


                        //txt.textAlignment = .right
                        addBottomLineTextField(txt: txt)


                        maxY = maxY + heightLabel + basicSpace
                        
                        viewParameterService.addSubview(lbl)
                        viewParameterService.addSubview(txt)

                        sumFinalInitialPrice = sumFinalInitialPrice + Double(("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).floatValue)
                    }
                    if arrKey.contains("FinalMaintUnitPrice")
                    {
                        var lbl = UILabel()
                        var txt = UITextField()

                        lbl = UILabel.init(frame: CGRect(x: lblSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        lbl.font = font
                        lbl.textColor = editColor
                        lbl.text = "Total Maint Price"
                        
                        txt = UITextField.init(frame: CGRect(x: Int(CGFloat(lbl.frame.maxX)) + basicSpace, y: maxY, width: Int(lblUnitValue.frame.size.width), height: heightLabel))
                        txt.font = font
                        txt.textColor = editColor
                        txt.text = stringToFloat(strValue: "\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")")
                        txt.tag = 6000+i
                        txt.addTarget(self, action: #selector(getTextAction), for: .touchUpInside)
                        txt.delegate = self
                        txt.keyboardType = UIKeyboardType.decimalPad

                        //txt.textAlignment = .right

                        addBottomLineTextField(txt: txt)
                        
                        
                        maxY = maxY + heightLabel + basicSpace
                        
                        viewParameterService.addSubview(lbl)
                        viewParameterService.addSubview(txt)
                        
                        sumFinalMaintPrice = sumFinalMaintPrice + Double(("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).floatValue)


                    }
                    

                }
                
                
                const_ViewParameterService_H.constant = CGFloat(maxY + 10)
               
            }
            else
            {
                const_ViewParameterService_H.constant = 0
                
                if  const_ViewParameterService_H.constant == 0
                {
                    viewParameterService.isHidden = true
                }
            }
        }
        
        
                
        var unit = 1.0
        if isUnitBased
        {
            unit = Double(("\(matchesObject.value(forKey: "unit") ?? "")" as NSString).floatValue)
        }
        var finalInitialPrice = (("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).floatValue * Float(unit) ) + Float(sumFinalInitialPrice) -  ((("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).floatValue) > 0 ? (("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).floatValue) : 0)
        var finalMaintPrice = (("\(matchesObject.value(forKey: "maintenancePrice") ?? "")" as NSString).floatValue * Float(unit)  ) + Float(sumFinalMaintPrice)
        
        if finalInitialPrice < 0
        {
            finalInitialPrice = 0
        }
        if finalMaintPrice < 0
        {
            finalMaintPrice = 0
        }

        lblFinalInitialPrice.text = convertDoubleToCurrency(amount: Double(finalInitialPrice))
        lblFinalMaintPrice.text = convertDoubleToCurrency(amount: Double(finalMaintPrice))
        
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = Double(finalInitialPrice)
        totalMaintPrice = Double(finalMaintPrice)

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        
        if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice *  (((strServiceFrqYearOccurence as NSString).doubleValue) - 1) ) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBillingFrequencyName.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
        
    }
    func addBottomLineTextField(txt : UITextField)  {
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: txt.frame.height - 1, width: txt.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.theme().cgColor
        txt.borderStyle = UITextField.BorderStyle.none
        txt.layer.addSublayer(bottomLine)
    }
    
    
    func isPlusServiceAvailable(strServiceSysName : String) -> Bool {
        
        var isAvailable = false
        
        let arrCategory = Global().getCategoryDeptWiseGlobal()! as NSArray
        //var arrService = NSArray()
        arrPlusService = NSMutableArray()
        for itemCategory in arrCategory
        {
            let dict = itemCategory as! NSDictionary

            let arr = dict.value(forKey: "Services") as! NSArray

            for itemService in arr
            {
                let dictService = itemService as! NSDictionary

                if "\(dictService.value(forKey: "ParentSysName") ?? "")" == strServiceSysName {

                    arrPlusService.add(dictService)
                }
            }

        }
        
        if arrPlusService.count > 0
        {
            isAvailable = true
        }
        
        return isAvailable
    }
    
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
    
    
    @IBAction func actionOnAddPlusService(_ sender: Any) {
        delegate?.addPlusService(arrPlusService: arrPlusService,matchesParentObject: matchesParentObject, strServiceSysName: strServiceSysName, strBillingFreqSysName: strBillingFreqSysName, strServiceFreqSysName: strServiceFreqSysName)
    }
    @objc func getTextAction(sender : UIButton)
    {
    }
    
    @IBAction func actionOnDeleteService(_ sender: Any) {
        
        delegate?.btnDeleteServiceClicked(matchesObject: matchesParentObject)
    }
    @IBAction func actionOnEditService(_ sender: Any) {
        
        delegate?.btnEditServiceClicked(matchesObject: matchesParentObject)
    }
    
    func forOneTimeService(matchesObject : NSManagedObject) {
        
        if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            lblHeadingInitialPrice.text = "One Time Initial Price"
            lblHeadingMaintPrice.isHidden = true
            lblMaintPrice.isHidden = true
            lblHeadingUnitBasedMaintPrice.isHidden = true
            lblUnitBasedMaintPrice.isHidden = true
            lblFinalMaintPrice.isHidden = true
            lblHeadingFinalMaintPrice.isHidden = true

        }
        else
        {
            lblHeadingInitialPrice.text = "Initial Price"
            lblHeadingMaintPrice.isHidden = false
            lblMaintPrice.isHidden = false
            lblHeadingUnitBasedMaintPrice.isHidden = false
            lblUnitBasedMaintPrice.isHidden = false
            lblFinalMaintPrice.isHidden = false
            lblHeadingFinalMaintPrice.isHidden = false
        }
    }
    
    // MARK: ------------ TEXT FIELD EXTENSION ------------
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
       //let pointInTable = textField.convert(textField.bounds.origin, to: UITableViewCell)
        //       let textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        delegate?.getTextFieldAction(txtTag: textField.tag, value : textField.text ?? "", matchesObject: matchesParentObject)
    }
}

