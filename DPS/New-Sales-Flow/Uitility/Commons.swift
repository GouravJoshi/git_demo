//
//  NotificationName.swift
//  DPS
//
//  Created by APPLE on 20/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

class Commons: NSObject {
    
    static let kNotificationPaymentMethod       =  Notification.Name("paymentMethod")

    static let kNotificationSignature   =  Notification.Name("Signature")
    static let kNotificationNotes   =  Notification.Name("Notes")
    static let kNotificationCredit   =  Notification.Name("Credit")
    static let kNotificationTaxCode   =  Notification.Name("TaxCode")

}
