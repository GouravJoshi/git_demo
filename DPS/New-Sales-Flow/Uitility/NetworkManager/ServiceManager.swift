//
//  ServiceManager.swift
//  DPS
//
//  Created by APPLE on 05/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

/*enum EndPoint: String {
    case auth               = "authentication/"
    case dashboard          = "dashboard/"
}

enum Method: String {
    case signup                 = "registration"
}*/

class ServiceManager {
    let BASE_URL = "https://www.pestream.com/formbuilder"
    let BASE_URL_Staging = "https://psasstaging.pestream.com"
    let BASE_URL_Prod = "https://psas.pestream.com"

    //public var idToken = LoginUserDetail.sharedInstance.token
    private var dataTask : URLSessionDataTask?
    //Shared Instance
    static let sharedInstance = ServiceManager()
    let dateFormatter:DateFormatter = DateFormatter.init()
    
    
    func postRequestWithUrlSession(_ endPoint: String, withParams params: [String: Any], withMethod method: String, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<[String : Any], HTTPError>) -> Void)
    {
        let strURL = BASE_URL + endPoint + method

        let url = URL(string: strURL)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //request.setValue(idToken, forHTTPHeaderField: "Authorization")
        //print("Access-Token:" + idToken)
        request.httpMethod = "POST"
        request.httpBody = params.percentEncoded()

        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) -> Void in

            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {

                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [String : Any] else{
                    completionBlock(.failure(.jsonError))
                    return
                }
                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        }

        task.resume()
    }
    
    func postRequestWithUrlSessionForHtmlString(_ endPoint: String, withParams params: [String: Any], withMethod method: String, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<String, HTTPError>) -> Void)
    {
        var strBaseUrl = ""
        if MainUrl == "https://pcrs.pestream.com/" {
            strBaseUrl = BASE_URL_Prod
        }else {
            strBaseUrl = BASE_URL_Staging
        }
        let strURL = strBaseUrl + "/api/" + endPoint + "/" + method
        let url = URL(string: strURL)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //request.setValue(idToken, forHTTPHeaderField: "Authorization")
        //print("Access-Token:" + idToken)
        request.httpMethod = "POST"
        request.httpBody = params.percentEncoded()

        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) -> Void in

            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {

                // Load HTML code as string
                let contents = String(data: data!, encoding: .utf8)
                completionBlock(.success(contents!))

//                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [String : Any] else{
//                    completionBlock(.failure(.jsonError))
//                    return
//                }
//                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        }

        task.resume()
    }
    
    func uploadImage(_ endPoint: String, withParams params: [String: Any], withMethod method: String, paramName: String, fileName: String, profile_image: UIImage?, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<[String : Any], HTTPError>) -> Void) {


        let strURL = BASE_URL + endPoint + method
        let url = URL(string: strURL)!
        
        let request = NSMutableURLRequest(url: url);
        request.httpMethod = "POST"
       // request.setValue(idToken, forHTTPHeaderField: "Authorization")
//        request.httpBody = params.percentEncoded()

        let boundary = "Boundary-\(NSUUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        let retreivedImage: UIImage? = profile_image


        let imageData = retreivedImage!.jpegData(compressionQuality: 1)
        if (imageData == nil) {
            print("UIImageJPEGRepresentation return nil")
            return
        }

        let body = NSMutableData()
        for (key, value) in params {
            body.append(String(convertFormField(named: key, value: value as! String, using: boundary)).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
        }
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Disposition: form-data; name=\"api_token\"\r\n\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
       // body.append(NSString(format: (UserDefaults.standard.string(forKey: "api_token")? as NSString)).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition: form-data; name=\"image\"; filename=\"testfromios.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(imageData!)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)

        request.httpBody = body as Data

        let task = URLSession.shared.dataTask(with: request as URLRequest) { [weak self] (data, response, error) -> Void in

            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {

                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [String : Any] else{
                    completionBlock(.failure(.jsonError))
                    return
                }
                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        }

        task.resume()
    }
    
    func uploadImageGraph(_ strURL: String, paramName: String, fileName: String, profile_image: UIImage?, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<[String : Any], HTTPError>) -> Void) {


        let url = URL(string: strURL)!
        
        let request = NSMutableURLRequest(url: url);
        request.httpMethod = "POST"
       // request.setValue(idToken, forHTTPHeaderField: "Authorization")
//        request.httpBody = params.percentEncoded()

        let boundary = "Boundary-\(NSUUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        let retreivedImage: UIImage? = profile_image


        let imageData = retreivedImage!.jpegData(compressionQuality: 1)
        if (imageData == nil) {
            print("UIImageJPEGRepresentation return nil")
            return
        }

        let body = NSMutableData()
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Disposition: form-data; name=\"api_token\"\r\n\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
       // body.append(NSString(format: (UserDefaults.standard.string(forKey: "api_token")? as NSString)).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        let fName: NSString = fileName as NSString
        body.append(NSString(format:"Content-Disposition: form-data; name=\"image\"; filename=\"\(fName)\"\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(imageData!)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)

        request.httpBody = body as Data

        let task = URLSession.shared.dataTask(with: request as URLRequest) { [weak self] (data, response, error) -> Void in

            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {

                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [String : Any] else{
                    completionBlock(.failure(.jsonError))
                    return
                }
                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        }

        task.resume()
    }
    
    func convertFormField(named name: String, value: String, using boundary: String) -> String {
      var fieldString = "--\(boundary)\r\n"
      fieldString += "Content-Disposition: form-data; name=\"\(name)\"\r\n"
      fieldString += "\r\n"
      fieldString += "\(value)\r\n"

      return fieldString
    }
    
    func getRequestWithUrlSession(_ endPoint: String, withParams params: [String: Any], withMethod method: String, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<[String : Any], HTTPError>) -> Void){
        
        let strURL = BASE_URL + endPoint + method
        
        let request = NSMutableURLRequest(url: NSURL(string: strURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        //request.setValue(idToken, forHTTPHeaderField: "Authorization")
        //print("Access-Token:" + idToken)


        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        // Invalidate and cancel
        // Required brief detail
        session.invalidateAndCancel()
        
        dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
            
            
            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {
                
                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [String : Any] else{
                    completionBlock(.failure(.jsonError))
                    return
                }
                
                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        })
        
        dataTask!.resume()
    }
    
    func getRequestWithDirectUrlSession(_ strURL: String, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<[Any], HTTPError>) -> Void){
        
        let strURL = strURL
        
        let request = NSMutableURLRequest(url: NSURL(string: strURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        request.setValue("IOS", forHTTPHeaderField: "Browser")

        //print("Access-Token:" + idToken)


        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        // Invalidate and cancel
        // Required brief detail
        session.invalidateAndCancel()
        
        dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
            
            
            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {
                
                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [Any] else{
                    completionBlock(.failure(.jsonError))
                    return
                }
                
                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        })
        
        dataTask!.resume()
    }
    
    func getRequestWithDirectUrlSessionReloadForm(_ strURL: String, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<[String: Any], HTTPError>) -> Void){
        
        let strURL = strURL
        
        let request = NSMutableURLRequest(url: NSURL(string: strURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let strTokan = "Zm9ybS1idWlsZGVyOmJ1aWxkZXIkMjAyMA=="
        request.setValue("Basic \(strTokan)", forHTTPHeaderField: "Authorization")

//        request.setValue("IOS", forHTTPHeaderField: "Browser")
        request.httpMethod = "GET"
        let session = URLSession.shared
        
        // Invalidate and cancel
        session.invalidateAndCancel()
        
        dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
            
            
            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {
                
                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [String: Any] else{
                    completionBlock(.failure(.jsonError))
                    return
                }
                
                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        })
        
        dataTask!.resume()
    }
    
    func getRequestWithDirectUrlForTaxSession(_ strURL: String, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<Any, HTTPError>) -> Void){
        
        let strURL = strURL
        
        let request = NSMutableURLRequest(url: NSURL(string: strURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        request.setValue("IOS", forHTTPHeaderField: "Browser")

        //print("Access-Token:" + idToken)


        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        // Invalidate and cancel
        // Required brief detail
        session.invalidateAndCancel()
        
        dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
            
            
            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {
                
                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? Any else{
                    completionBlock(.failure(.jsonError))
                    return
                }
                
                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        })
        
        dataTask!.resume()
    }
    
    func getRequestWithDirectUrlForTemplateKeySession(_ strURL: String, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<Any, HTTPError>) -> Void){
        
        let strURL = strURL
        
        let request = NSMutableURLRequest(url: NSURL(string: strURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        request.setValue("IOS", forHTTPHeaderField: "Browser")

        //print("Access-Token:" + idToken)


        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        // Invalidate and cancel
        // Required brief detail
        session.invalidateAndCancel()
        
        dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
            
            
            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {
                
                guard let responseDict = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? Any else{
                    completionBlock(.failure(.jsonError))
                    return
                }
                
                completionBlock(.success(responseDict))
            }
            self?.dataTask = nil
        })
        
        dataTask!.resume()
    }
    
    func downloadImageWith(_ strUrl: String, andCompletion completionBlock: @escaping (_ serviceResponse: ServiceResponse<Data, HTTPError>) -> Void){
        
        let request = NSMutableURLRequest(url: NSURL(string: strUrl)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        let session = URLSession.shared
        session.invalidateAndCancel()
        dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
            if (error != nil) {
                completionBlock(.failure(.networkError))
            } else {
                completionBlock(.success(data!))
            }
            self?.dataTask = nil
        })
        dataTask!.resume()
    }
    
    
    
    func cancel(){
        if(dataTask != nil){
            dataTask?.cancel()
            dataTask = nil
        }
    }
}


extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
