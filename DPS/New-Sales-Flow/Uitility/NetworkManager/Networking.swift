//
//  Networking.swift
//  DPS
//
//  Created by APPLE on 05/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

enum ServiceResponse<T,error : Error> {
    case success(T)
    case failure(error)
}

enum HTTPError : Error {
    case requestTimedOut
    case networkError
    case jsonError
}

enum RepositoryError : Error {
    case httpError(HTTPError)
    case repositoriesNotFound
}
