//
//  Helper.swift
//  DPS
//
//  Created by APPLE on 30/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
class Helper{
    //Get Bundle Name
    static func getBundleObjectFromId(strId : String) -> NSDictionary {
        
        var dictObject = NSDictionary()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "ServiceBundles") is NSArray){
                
                let arrBundle = dictMaster.value(forKey: "ServiceBundles") as! NSArray
               
                let arrObject = arrBundle.filter { (dict) -> Bool in
                    
                    return "\((dict as! NSDictionary).value(forKey: "ServiceBundleId") ?? "")".caseInsensitiveCompare(strId) == .orderedSame
                } as NSArray
                   
                if arrObject.count > 0
                {
                    dictObject = arrObject.object(at: 0) as! NSDictionary
                }
            }
        }
        
        return dictObject

    }
}
