//
//  SalesReviewViewController.swift
//  DPS
//
//  Created by APPLE on 25/05/21.
//  Copyright © 2021 Saavan. All rights reserved. change
//

import UIKit

class SalesReviewViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    var arrSectionTitle = ["", "Inspection Details", "Services", "Pricing Information", "Discounts", "Payment Information", "Notes", "Months of Service", "Agreement Checklist", "Signatures"]
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var scrHeaderView: UIScrollView!
    @IBOutlet weak var scrHeader_HeightConstant: NSLayoutConstraint!
    @IBOutlet weak var btnSendProposal: UIButton!
    @IBOutlet weak var btnMarkAsLost: UIButton!
    @IBOutlet weak var scrTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrBootomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnInitialSetup: UIButton!
    
    //var matchesGeneralInfo : NSManagedObject?
    @objc var matchesGeneralInfo = NSManagedObject()
    var objPaymentInfo : NSManagedObject!

    var isInspectionLoaded = false
    var isSendProposal: Bool = false
    var isFromConfig: Bool = false

    //Agreement and Month
    var arrayAgreementCheckList: [Any] = []
    var strAgreementCheckList = ""
    var arrayOfMonth: [String] = []
    var strMonthOfService = ""

    //Services
    var arrStandardServiceReview  : [NSManagedObject] = []
    var arrCustomServiceReview    : [NSManagedObject] = []
    var arrBundleServiceReview    : [NSManagedObject] = []
    
    var arrStandardService  : [NSManagedObject] = []
    var arrCustomService    : [NSManagedObject] = []
    var arrBundleService    : [NSManagedObject] = []
    var arrStandardServiceProposal  : [NSManagedObject] = []
    var arrCustomServiceProposal    : [NSManagedObject] = []
    var arrBundleServiceProposal    : [NSManagedObject] = []
    var dictPricingInfo: [String: Any]?
    var dictPricingInfoProposal: [String: Any] = [:]

    //Discount
    var arrAppliedCoupon : [NSManagedObject] = []
    var arrAppliedCredit : [NSManagedObject] = []

    
    //Notes
    var arrNotes = ["Customer Notes",
                    "Proposal Notes",
                    "Internal Notes"]
    var strCustomerNote  = ""
    var strProposalNote  = ""
    var strInternalNote  = ""
    
    //Signature
    var customerSig: UIImage?
    var techSig: UIImage?
    var isCustomer = false
    var strCustomerSignature = ""
    var strSalesSignature = ""

    //Payment
    var selectecPaymentType = ""
    var strPaymentMode = ""
    var strAmount = ""
    var strChequeNo     = ""
    var strLicenseNo    = ""
    var strDate         = ""
    var strLeadStatusGlobal     = ""
    var strStageSysName         = ""
    var arrTerms: [String] = []
    
    var isFrontImage = false
    var checkFrontImg: UIImage?
    var checkBackImg: UIImage?
    var arrOFImagesName: [String] = []
    var arrOfCheckBackImage: [String] = []
    var strPresetSigFromDB = ""

    //From Login Details
    var isTaxCodeRequired = false
    var strTaxCodeSysName = ""
    var isIAgreeTAndC = false
    var isCustomerNotPresent = false
    var isEditedInSalesAuto = false
    var isAllowToMakeSelection = false

    var strAudioName = ""
    var strCompanyKey = ""
    var strUserName = ""
    var strGlobalAudio = ""
    var strSalesssUrlMainServiceAutomation = ""
    var isElementIntegration = false
    var chkForLost = false
    var strLeadId = ""
    let global = Global()
    var strGlobalStatusSysName = ""
    var strGlobalStageSysName = ""
    var chkFreqConfiguration = false
    var shouldShowElectronicFormLink = false
    var isAuthFormFilled = false

    var strText = "false"
    var strPhone = "false"
    var strEmail = "false"
    var strIsAllowCustomer = "false"

    
    var isBundleService = false
    var isReloadOnce = false
    var isReloadOnceCustomer = false
    var isRefreshInspector = false
    var strTextAmount  = ""
    var dictSelectedCustomPaymentMode = NSDictionary()
    var strStatusPreview = ""
    var strStagePreview = ""
    var isPreview = false

    override func viewDidLoad() {
        super.viewDidLoad()

        if isSendProposal {
            isFromConfig = true
            self.scrTopConstraint.constant = 0.0
            self.scrBootomConstraint.constant = 0.0
        }
        self.strLeadId = "\(nsud.value(forKey: "LeadId") ?? "")"
        
        self.setUpToSendProposal()
        self.loginDetails()
        self.setLeadDetails()
        self.setPaymentDetail()
        self.calculationForCouponAndCredit()
        strAudioName = nsud.value(forKey: "AudioNameService") as! String

        print(dictPricingInfo)
        getCustomerNotes()
        //----Header
        lblTitle.text = "\(nsud.value(forKey: "titleHeader1") ?? "")"
        lblSubTitle.text = "\(nsud.value(forKey: "titleHeader2") ?? "")"
        
        getInspectorAndCustomerName()
    }
    
    func getInspectorAndCustomerName()  {
                
        var strEmpName = ""
        strEmpName = global.getEmployeeName(viaEmployeeID: "\(matchesGeneralInfo.value(forKey: "salesRepId") ?? "")")
        var strCustomerName = ""
        strCustomerName = "\(matchesGeneralInfo.value(forKey: "firstName") ?? "") \(matchesGeneralInfo.value(forKey: "middleName") ?? "") \(matchesGeneralInfo.value(forKey: "lastName") ?? "")"
        nsud.set(strEmpName, forKey: "inspectorName")
        nsud.set(strCustomerName, forKey: "customerName")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        strLeadStatusGlobal = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strGlobalStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        strGlobalStatusSysName = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        
        let defaults = UserDefaults.standard
        let strIns: String = defaults.value(forKey: "fromInspectorSign") as? String ?? ""
        let strCust: String = defaults.value(forKey: "fromCustomerSign") as? String ?? ""
        

        if strIns == "fromInspectorSign" {
            self.isEditedInSalesAuto = true
            self.strSalesSignature = defaults.value(forKey: "imagePath") as? String ?? ""
            let imagePath = self.getImage(strImageName: self.strSalesSignature)
            if imagePath != "" {
                self.techSig = UIImage(contentsOfFile: imagePath)
            }
            
            defaults.setValue("abc", forKey: "fromInspectorSign")
            defaults.setValue("abc", forKey: "imagePath")
            defaults.synchronize()
            
        }
        if strCust == "fromCustomerSign" {
            self.isEditedInSalesAuto = true
            self.strCustomerSignature = defaults.value(forKey: "imagePath") as? String ?? ""
            let imagePath = self.getImage(strImageName: self.strCustomerSignature)
            if imagePath != "" {
                self.customerSig = UIImage(contentsOfFile: imagePath)
            }
            
            defaults.setValue("xyz", forKey: "fromCustomerSign")
            defaults.setValue("xyz", forKey: "imagePath")
            defaults.synchronize()
        }
        
        
        let yesAudioAvailable = defaults.bool(forKey: "yesAudio")
        if yesAudioAvailable {
            self.isEditedInSalesAuto = true
            self.strGlobalAudio = defaults.string(forKey: "AudioNameService") ?? ""
        }
        
        
        self.tblView.reloadData()
        
        
        if getOpportunityStatus()
        {
            self.btnSendProposal.isHidden = true
            self.btnSendProposal.isUserInteractionEnabled = false
        }
        else
        {
            if isSendProposal {
                self.btnSendProposal.isHidden = true
            }else {
                self.btnSendProposal.isHidden = false
            }
            self.btnSendProposal.isUserInteractionEnabled = true
           
        }
        
        if getOpportunityStatus() || getLostStatus()
        {
            self.btnMarkAsLost.isUserInteractionEnabled = false
            self.btnMarkAsLost.isHidden = true
        }
        else
        {
            self.btnMarkAsLost.isUserInteractionEnabled = true
            self.btnMarkAsLost.isHidden = false
        }
        
        
        handleCheckImage()
        
        manageInitialSetup()
    }
    
    func handleCheckImage() {
        
        if nsud.bool(forKey: "frontImageDeleted")
        {
            arrOFImagesName = []
            nsud.setValue(false, forKey: "frontImageDeleted")
            nsud.synchronize()
            self.objPaymentInfo?.setValue("", forKey: "checkFrontImagePath")
        }
        if nsud.bool(forKey: "backImageDeleted")
        {
            arrOfCheckBackImage = []
            nsud.setValue(false, forKey: "backImageDeleted")
            nsud.synchronize()
            self.objPaymentInfo?.setValue("", forKey: "checkBackImagePath")
        }
        
        if nsud.bool(forKey: "yesEditImage")
        {
            nsud.setValue(false, forKey: "yesEditImage")
            nsud.synchronize()
            
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Service_iPhone" : "Service_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditImageViewController") as! EditImageViewController
            
            self.navigationController?.present(vc, animated: true, completion: nil)
        }


        if nsud.bool(forKey: "frontImageDeleted") || nsud.bool(forKey: "backImageDeleted") {
            let context = getContext()
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    
    func getOpportunityStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    func getLostStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Lost") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getImage(strImageName: String) -> String {
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(strImageName)
        if fileManager.fileExists(atPath: imagePAth){
            return imagePAth
        }else{
            return ""
        }
    }
    
    func downloadingAudio(strPath: String) {
        print(self.strSalesssUrlMainServiceAutomation)
        
        var result = ""
        let equalRange = NSString(string: strPath).range(of: "/", options: .backwards)

        if equalRange.location != NSNotFound {
            let fromR = equalRange.location + equalRange.length
            result = strPath.substring(from: fromR)
        }else {
            result = strPath
        }
        print(result)
        if result.count == 0 {
            let range = NSString(string: strPath).range(of: "\\", options: .backwards)

            if range.location != NSNotFound {
                let fromR = range.location + range.length
                result = strPath.substring(from: fromR)
            }
        }
        
        let strUrl = "\(self.strSalesssUrlMainServiceAutomation)/documents/\(strPath)"
        
        print(strUrl)
        let path = (self.getDirectoryPath() as NSString).appendingPathComponent(result)

        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: path){
            let defaults = UserDefaults.standard
            defaults.setValue(result, forKey: "AudioNameService")
            defaults.synchronize()
        }else{
            let strNewString = strUrl.replacingOccurrences(of: "\\", with: "/")
            let photoURL = URL(string: strNewString)
            var audioData: Data? = nil
            if let photoURL = photoURL {
                
                do {
                    audioData = try Data(contentsOf: photoURL)
                   } catch {
                       print(error)
                   }
            }
            self.saveAudio(afterDownload: audioData, result)
            
            let defaults = UserDefaults.standard
            defaults.setValue(result, forKey: "AudioNameService")
            defaults.set(true, forKey: "yesAudio")
            defaults.synchronize()
        }
    }
    
    func saveAudio(afterDownload audioData: Data?, _ name: String?) {
        var name = name

        name = global.strNameBackSlashIssue(name)

        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).map(\.path)
        let documentsDirectory = paths[0]
        let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(name ?? "")").path
        if let audioData = audioData {
            NSData(data: audioData).write(toFile: path, atomically: true)
        }
    }
    
    func loginDetails() {
        let dictLoginData: [String : Any] = nsud.value(forKey: "LoginDetails") as! [String : Any]
        print(dictLoginData)
        
        
        self.isTaxCodeRequired = ((dictLoginData["Company"]
                                    as? [String: Any])?["CompanyConfig"]
                                    as? [String: Any])?["TaxcodeRequired"] as? Bool ?? false
        self.strCompanyKey = (dictLoginData["Company"]
                                as? [String: Any])?["CompanyKey"]
            as? String ?? ""
        self.strUserName = (dictLoginData["Company"]
                                as? [String: Any])?["Username"]
            as? String ?? ""
        
        
        
        self.strSalesssUrlMainServiceAutomation = (((dictLoginData["Company"]
                                                        as? [String: Any])?["CompanyConfig"]
                                                        as? [String: Any])?["SalesAutoModule"]
            as? [String: Any])?["ServiceUrl"]
                                as? String ?? ""
        
        self.isElementIntegration = ((dictLoginData["Company"]
                                        as? [String: Any])?["CompanyConfig"]
                                        as? [String: Any])?["IsElementIntegration"] as? Bool ?? false
        
        chkFreqConfiguration =   ((dictLoginData["Company"]
                                    as? [String: Any])?["CompanyConfig"]
                                    as? [String: Any])?["MaintPriceFreq_1"] as? Bool ?? false

        shouldShowElectronicFormLink =   ((dictLoginData["Company"]
                                    as? [String: Any])?["CompanyConfig"]
                                    as? [String: Any])?["IsElectronicAuthorizationForm"] as? Bool ?? false
    }
    
    func getPaymentModeFromMaster() -> NSMutableArray {
        
        var arrPaymentMaster = NSArray()
        
        var  dictLoginData = NSDictionary()
        
        dictLoginData =  nsud.value(forKey: "LoginDetails") as! NSDictionary

//        let isSalesPaymentTypeDynamic = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsSalesPaymentTypeDynamic") as! Bool
        let strSalesPaymentTypeDynamic = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsSalesPaymentTypeDynamic") ?? "")"
        var isSalesPaymentTypeDynamic = false
        if strSalesPaymentTypeDynamic == "1" || strSalesPaymentTypeDynamic.lowercased() == "true" {
            isSalesPaymentTypeDynamic = true
        }else {
            isSalesPaymentTypeDynamic = false
        }
        if isSalesPaymentTypeDynamic
        {
            if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
            {
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "PaymentTypeObj") is NSArray) {
                    
                    arrPaymentMaster = dictMaster.value(forKey: "PaymentTypeObj") as! NSArray
                }
            }
        }

        let arrPaymentNew = NSMutableArray()
        arrPaymentNew.add(["AddLeadProspect": "Cash"])
        arrPaymentNew.add(["AddLeadProspect": "Check"])
        arrPaymentNew.add(["AddLeadProspect": "Credit Card"])
        arrPaymentNew.add(["AddLeadProspect": "Auto Charge Customer"])
        arrPaymentNew.add(["AddLeadProspect": "Collect at time of Scheduling"])
        arrPaymentNew.add(["AddLeadProspect": "Invoice"])
        
        for item in arrPaymentMaster
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1" || ("\(dict.value(forKey: "IsActive") ?? "")" == "0" && "\(dict.value(forKey: "SysName")!)" == strPaymentMode)
            {
                arrPaymentNew.add(dict)
            }
        }
        print(arrPaymentNew)
        
        return arrPaymentNew
    }
    
    func setPaymentDetail() {
        print(objPaymentInfo)
        self.strPaymentMode = "\(self.objPaymentInfo.value(forKey: "paymentMode") ?? "")"
        
        print(self.selectecPaymentType)
        if self.strPaymentMode == "Cash" {
            self.selectecPaymentType = "Cash"
        }else if self.strPaymentMode == "Check" {
            self.selectecPaymentType = "Check"
        }else if self.strPaymentMode == "CreditCard" {
            self.selectecPaymentType = "Credit Card"
        }else if self.strPaymentMode == "AutoChargeCustomer" {
            self.selectecPaymentType = "Auto Charge Customer"
        }else if self.strPaymentMode == "CollectattimeofScheduling" {
            self.selectecPaymentType = "Collect at time of Scheduling"
        }else if self.strPaymentMode == "Invoice" {
            self.selectecPaymentType = "Invoice"
        }else {
            
            //Fetch Payment Mode From Master
            var paymentModeFound = false
            
            let arrPaymentMode = getPaymentModeFromMaster()
            
            for item in arrPaymentMode
            {
                let dict = item as! NSDictionary
                
                if dict["AddLeadProspect"] == nil
                {
                    if "\(dict.value(forKey: "SysName") ?? "")" == strPaymentMode
                    {
                        self.selectecPaymentType = "\(dict.value(forKey: "Title") ?? "")"
                        paymentModeFound = true
                        dictSelectedCustomPaymentMode = dict
                        break
                    }
                }
                    
            }
            if !paymentModeFound
            {
                self.selectecPaymentType = ""
            }
        }
        
        self.strCustomerSignature = "\(self.objPaymentInfo.value(forKey: "customerSignature") ?? "")"
        self.strSalesSignature = "\(self.objPaymentInfo.value(forKey: "salesSignature") ?? "")"
        
        
        
        let checkFrontImagePath = "\(self.objPaymentInfo.value(forKey: "checkFrontImagePath") ?? "")"
        if (checkFrontImagePath.count > 0)
        {
            arrOFImagesName.append(checkFrontImagePath)
        }
                
        let checkBackImagePath = "\(self.objPaymentInfo.value(forKey: "checkBackImagePath") ?? "")"
        if (checkBackImagePath.count > 0)
        {
            arrOfCheckBackImage.append(checkBackImagePath)
        }
        
        self.strChequeNo = "\(objPaymentInfo.value(forKey: "checkNo") ?? "")"
        self.strLicenseNo = "\(objPaymentInfo.value(forKey: "licenseNo") ?? "")"
       // self.strDate = "\(self.objPaymentInfo.value(forKey: "expirationDate") ?? "")"
        
        if "\(objPaymentInfo.value(forKey: "expirationDate") ?? "")".count > 0
        {
            let str = changeStringDateToGivenFormat(strDate: "\(objPaymentInfo.value(forKey: "expirationDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            self.strDate = str
            //cell.btnExpDate.setTitle("Expiration Date: \(str)", for: .normal)
        }

    }
    
    func setLeadDetails() {
        print(matchesGeneralInfo)

        self.strTaxCodeSysName = "\(matchesGeneralInfo.value(forKey: "taxSysName")!)"
        strLeadStatusGlobal = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strGlobalStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        strGlobalStatusSysName = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        strPresetSigFromDB = "\(matchesGeneralInfo.value(forKey: "isEmployeePresetSignature") ?? "")"

        let strIsAuthFormFilled = "\(matchesGeneralInfo.value(forKey: "accountElectronicAuthorizationFormSigned") ?? "")"
        if strIsAuthFormFilled == "true" || strIsAuthFormFilled == "1" {
            isAuthFormFilled = true
        }else {
            isAuthFormFilled = false
        }
        
        let strIsCusto = "\(matchesGeneralInfo.value(forKey: "isCustomerNotPresent") ?? "")"
        if strIsCusto == "true" || strIsCusto == "1" {
            self.isCustomerNotPresent = true
        }else {
            self.isCustomerNotPresent = false
        }
        
        let strIsAllowCusto = "\(matchesGeneralInfo.value(forKey: "isSelectionAllowedForCustomer") ?? "false")"
        if strIsAllowCusto == "true" || strIsCusto == "1" {
            self.strIsAllowCustomer  = "true"
        }else {
            self.strIsAllowCustomer  = "false"
        }
        print(self.strIsAllowCustomer)

        self.strText = "\(matchesGeneralInfo.value(forKey: "smsReminders") ?? "false")"
        self.strPhone = "\(matchesGeneralInfo.value(forKey: "phoneReminders") ?? "false")"
        self.strEmail = "\(matchesGeneralInfo.value(forKey: "emailReminders") ?? "false")"

        if self.strText == "" || self.strText == "false" || self.strText == "0" {
            self.strText = "false"
        }else {
            self.strText = "true"
        }
        if self.strPhone == "" || self.strPhone == "false" || self.strPhone == "0" {
            self.strPhone = "false"
        }else {
            self.strPhone = "true"
        }
        if self.strEmail == "" || self.strEmail == "false" || self.strEmail == "0" {
            self.strEmail = "false"
        }else {
            self.strEmail = "true"
        }
        strGlobalStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        strGlobalStatusSysName = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        
//        self.strIsServiceAddrTaxExempt = "\(matchesGeneralInfo.value(forKey: "isServiceAddrTaxExempt")!)"
//        self.strServiceAddressSubType = "\(matchesGeneralInfo.value(forKey: "serviceAddressSubType")!)"
        
        arrTerms.append(matchesGeneralInfo.value(forKey: "leadGeneralTermsConditions") as? String ?? "")
        if arrTerms[0] == "" {
            arrTerms.removeAll()
            if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
            {
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                //Getting Credit and Coupon Detail
                if(dictMaster.value(forKey: "MultipleGeneralTermsConditions") is NSArray) {
                    let arrTermsTempNew: NSArray = dictMaster.value(forKey: "MultipleGeneralTermsConditions") as! NSArray
                    var tempMatch = false
                    
                    if arrTermsTempNew.count > 0 {
                        var dictGneralTerms: [String: Any]?
                        for dict in arrTermsTempNew {
                            print(dict)
                            dictGneralTerms = dict as? [String : Any]
                            if "\(dictGneralTerms!["IsDefault"] ?? "0")" == "1"  && "\(dictGneralTerms!["IsActive"] ?? "0")" == "1" {
                                tempMatch = true
                                break
                            }
                        }
                        
                        if tempMatch {
                            arrTerms.append(dictGneralTerms!["TermsConditions"] as! String)
                        }else {
                            arrTerms.append("")
                        }
                        
                    }else {
                        arrTerms.append("")
                    }
                }
            }
        }
        print(arrTerms)
        
        
        let audioPath = "\(matchesGeneralInfo.value(forKey: "audioFilePath") ?? "")"
        if (audioPath.count>0) {
            print(audioPath)
            strGlobalAudio = audioPath;
            self.downloadingAudio(strPath: audioPath)
        }else {
            strGlobalAudio = ""
        }
        
        
        let myFloat = ("\(matchesGeneralInfo.value(forKey: "collectedAmount") ?? "")" as NSString).floatValue
        if myFloat > 0
        {
            if getOpportunityStatus()
            {
                strAmount = String(format: "%.2f", myFloat)
            }
            else
            {
                strAmount = (self.dictPricingInfo?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "")
            }
            //strAmount = String(format: "%.2f", myFloat)
        }
        else
        {
            strAmount = (self.dictPricingInfo?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "")
        }
        
        //Nilind

        manageAllowCustomerToSelection()
        
        //End

    }
    func manageAllowCustomerToSelection()  {
        
        var  dictLoginData = NSDictionary()
        
        dictLoginData =  nsud.value(forKey: "LoginDetails") as! NSDictionary

        isAllowToMakeSelection = dictLoginData.value(forKeyPath: "Company.CompanyConfig.DefaultIsAllowCustomertToMakeSelection") as! Bool

        if isCustomerNotPresent == true
        {
            
            if "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")" == "Complete" && "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")" == "CompletePending"
            {
                let strIsAllowCusto = "\(matchesGeneralInfo.value(forKey: "isSelectionAllowedForCustomer") ?? "false")"
                if strIsAllowCusto == "true" || strIsAllowCusto == "1" {
                    self.strIsAllowCustomer  = "true"
                }else {
                    self.strIsAllowCustomer  = "false"
                }
            }
            else
            {
                if isAllowToMakeSelection
                {
                    self.strIsAllowCustomer  = "true"
                }
                else
                {
                    self.strIsAllowCustomer  = "false"
                }
            }
        }
        else
        {
            self.strIsAllowCustomer  = "false"
        }
        
    }
    
    
    func manageInitialSetup() {
        
        if self.showGenerateWorkorderSetup(dictSalesDB: matchesGeneralInfo) == true {
            let myNormalAttributedTitle = NSAttributedString(string: "Generate Workorder",
                                                             attributes: [
                                                                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),
                                                                NSAttributedString.Key.foregroundColor: UIColor.black,
                                                                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
                                                            ])
            
            self.btnInitialSetup.setAttributedTitle(myNormalAttributedTitle, for: .normal)
        }
        else
        {
            let myNormalAttributedTitle = NSAttributedString(string: "Initial Setup",
                                                             attributes: [
                                                                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),
                                                                NSAttributedString.Key.foregroundColor: UIColor.black,
                                                                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
                                                            ])
            
            self.btnInitialSetup.setAttributedTitle(myNormalAttributedTitle, for: .normal)
        }
        
        if (self.showInitialSetup(dictSalesDB: matchesGeneralInfo) == true)
        {
            self.btnInitialSetup.isHidden = false
        }
        else
        {
            self.btnInitialSetup.isHidden = true
        }
    }
    
    func fetchPaymentInfo() {
                
        let arrPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))

        if(arrPaymentInfo.count > 0)
        {
            objPaymentInfo = (arrPaymentInfo.firstObject as! NSManagedObject)
            print(objPaymentInfo)
            self.strCustomerNote = "\(objPaymentInfo!.value(forKey: "specialInstructions")!)"
            
        }else {
        }
    }
    func getCustomerNotes() {
                
        let arrPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))

        if(arrPaymentInfo.count > 0)
        {
            let objPaymentInfoTemp = (arrPaymentInfo.firstObject as! NSManagedObject)
            self.strCustomerNote = "\(objPaymentInfoTemp.value(forKey: "specialInstructions") ?? "")"
            
        }else {
        }
    }
    
    
    
    func calculationForCouponAndCredit() {
        let defaults = UserDefaults.standard
        print(arrAppliedCoupon)
        print(arrAppliedCredit)

        
    }
 
    
    
    //Observer Method
    @objc private func notesValue(notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: Commons.kNotificationNotes, object: nil)

        
        let dict: [String: String] = (notification.userInfo as? [String: String])!
        if notification.object as! String == NotesForConfigure.customer.rawValue {
            self.strCustomerNote = dict["note"] ?? ""
            self.updatePaymentInfo()
        }else if notification.object as! String == NotesForConfigure.proposal.rawValue {
            self.strProposalNote = dict["note"] ?? ""
            self.updateLeadDetails()
        }else {
            self.strInternalNote = dict["note"] ?? ""
            self.updateLeadDetails()
        }
        
        tblView.reloadData()
    }

    func updateLeadDetails() {

        self.matchesGeneralInfo.setValue(strInternalNote, forKey: "notes")
        self.matchesGeneralInfo.setValue(strProposalNote, forKey: "proposalNotes")

        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func updatePaymentInfo() {
        
        self.objPaymentInfo?.setValue(strCustomerNote, forKey: "specialInstructions")

        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func setUpToSendProposal() {
        if isSendProposal {
            scrHeaderView.isHidden = true
            scrHeader_HeightConstant.constant = 0
            btnSendProposal.isHidden = true
            btnMarkAsLost.isHidden = true
            self.arrStandardService = self.arrStandardServiceProposal
            self.arrCustomService   = self.arrCustomServiceProposal
            self.arrBundleService   = self.arrBundleServiceProposal
            
            /*********Month**********/
            if arrayOfMonth.count > 0 {
                let arrTempMonth = arrayOfMonth.filter { $0 != "" }//arrayOfMonth.compactMap { $0 }
                print(arrTempMonth)
                self.strMonthOfService = arrTempMonth.joined(separator: ", ")
            }
            
            /*********Month**********/
            self.strProposalNote = "\(matchesGeneralInfo.value(forKey: "proposalNotes")!)"
            self.strInternalNote = "\(matchesGeneralInfo.value(forKey: "notes")!)"
            self.strCustomerNote = "\(objPaymentInfo?.value(forKey: "specialInstructions")! ?? "")"
        }else {
            scrHeaderView.isHidden = false
            scrHeader_HeightConstant.constant = DeviceType.IS_IPAD ? 44 : 32
            btnSendProposal.isHidden = false
            btnMarkAsLost.isHidden = false
            self.arrStandardService = self.arrStandardServiceReview
            self.arrCustomService   = self.arrCustomServiceReview
            self.arrBundleService   = self.arrBundleServiceReview
            
            /*********Agreement Check List**********/
            if self.arrayAgreementCheckList.count > 0 {
                var arrTemp: [String] = []
                for value in self.arrayAgreementCheckList {
                    print(value)
                    let dict: NSManagedObject = value as! NSManagedObject
                    let isActive: Bool = Bool("\(dict.value(forKey: "isActive") ?? "0")") ?? false
                    if isActive == true {
                        let dict1 = getAgreementCheckListObject(strCheckListId: "\(dict.value(forKey: "agreementChecklistId") ?? "")")
                        print(dict1)
                        
                        let str: String = "\(dict1.value(forKey: "Title") ?? "")"
                        strAgreementCheckList = strAgreementCheckList.appending(str)
                        arrTemp.append(str)
                    }
                    
                }
                self.strAgreementCheckList = arrTemp.joined(separator: ", ")
            }
            
            
            /*********Month**********/
            if arrayOfMonth.count > 0 {
                let arrTempMonth = arrayOfMonth.filter { $0 != "" }//arrayOfMonth.compactMap { $0 }
                print(arrTempMonth)
                self.strMonthOfService = arrTempMonth.joined(separator: ", ")
            }
            
            /*********Notes**********/
            self.strProposalNote = "\(matchesGeneralInfo.value(forKey: "proposalNotes")!)"
            self.strInternalNote = "\(matchesGeneralInfo.value(forKey: "notes")!)"
            self.strCustomerNote = "\(objPaymentInfo?.value(forKey: "specialInstructions")! ?? "")"
            
        }
        
        btnMarkAsLost.isHidden = false
        self.btnMarkAsLost.isUserInteractionEnabled = true
        
        if arrBundleService.count > 0 {
            self.isBundleService = true
            self.arrSectionTitle.insert("", at: 3)
        }
        
        if getOpportunityStatus() || getLostStatus()
        {
            self.btnMarkAsLost.isUserInteractionEnabled = false
            self.btnMarkAsLost.isHidden = true
        }
    }
    
    @IBAction func sendProposalAction(_ sender: Any) {
        isPreview = false
        
        let count = self.arrStandardServiceProposal.count + self.arrCustomServiceProposal.count + self.arrBundleServiceProposal.count
        
        if count > 0 {
            isSendProposal = true
            self.setUpToSendProposal()
            tblView.reloadData()
        }else {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To proceed, there should be atleast one service to send proposal.", viewcontrol: self)
        }
    }
    
    @IBAction func action_RecordAudio(_ sender: Any) {
        //yesEditedSomething = true
        
        let isAudioPermission = Global().isAudioPermissionAvailable()
        
        if isAudioPermission
        {
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)

            if !getOpportunityStatus()
            {
                alert.addAction(UIAlertAction(title: "Record", style: .default , handler:{ (UIAlertAction)in
                    let storyboardIpad = UIStoryboard.init(name: "MainiPad", bundle: nil)
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioViewiPad") as? RecordAudioViewiPad
                    testController?.strFromWhere = "SalesInvoice"//flowTypeWdoSalesService
                    testController?.modalPresentationStyle = .fullScreen
                    self.present(testController!, animated: false, completion: nil)
                }))
            }
                
            let audioName = "\(nsud.value(forKey: "AudioNameService") ?? "")"
            
            if audioName.count > 0
                
            {
                
                alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
                    
                    let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                    
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
                    
                    testController!.strAudioName = audioName as! String
                    
                    testController?.modalPresentationStyle = .fullScreen
                    
                    self.present(testController!, animated: false, completion: nil)
                    
                }))
                
            }
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in }))
            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: { })
        }
        else
        {
            let alertController = UIAlertController (title: Alert, message: alertAudioVideoPermission, preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: "Go-To Settings", style: .default) { (_) -> Void in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            }
            alertController.addAction(settingsAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
        }

    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func cancleAction(_ sender: Any) {
            
        if isFromConfig{
            self.navigationController?.popViewController(animated: false)
        }else {
            if isSendProposal {
                isSendProposal = false
                self.setUpToSendProposal()
                tblView.reloadData()
            }else {
                self.navigationController?.popViewController(animated: false)
            }
        }
        
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        isPreview = false
        
        Global().updateSalesModifydate(strLeadId as String)

        if isSendProposal {
            //Sand Proposal
            
            //chkChequeClick = NO;
            self.strLicenseNo = "";
            self.strChequeNo = "";
            self.strDate = "";
            self.strAmount = "0"
            self.strPaymentMode = ""
            self.strSalesSignature = ""
            self.strCustomerSignature = ""
            //self.strCustomerNote = ""

            /*Final Update*/
            self.updatePaymentInfoCoreData()
            
            
            if self.checkForCustomerSignature() == false {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
                
            }else {
                if self.isEditedInSalesAuto == true {
                    global.updateSalesModifydate(self.strLeadId)
                }
                self.updateLeadIdDetail()
                
                /*Complition Alert*/
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)
                
                self.goToSendMailViewController()
            }
        }else {
            //Review
            if self.isTaxCodeRequired == true {
                if self.strTaxCodeSysName == "" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Tax code is required. Please go to  General Info screen and select Taxcode under Service Address section.", viewcontrol: self)

                }else {
                    self.finalSaveAfterTaxCodeCheck()
                }
            }else {
                self.finalSaveAfterTaxCodeCheck()
            }
        }
    }
    
    @IBAction func actionOnPreview(_ sender: Any) {
        
        isPreview = true
        
        Global().updateSalesModifydate(strLeadId as String)

        if isSendProposal {
            //Sand Proposal
            
            //chkChequeClick = NO;
            self.strLicenseNo = "";
            self.strChequeNo = "";
            self.strDate = "";
            self.strAmount = "0"
            self.strPaymentMode = ""
            self.strSalesSignature = ""
            self.strCustomerSignature = ""
            //self.strCustomerNote = ""

            /*Final Update*/
            self.updatePaymentInfoCoreData()
            
            
            if self.checkForCustomerSignature() == false
            {
               ////Temp Comment //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
                
                if self.isEditedInSalesAuto == true {
                    global.updateSalesModifydate(self.strLeadId)
                }
                self.updateLeadIdDetail()
                
                /*Complition Alert*/
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)
                
                self.goToSendMailViewController()
                
            }
            else
            {
                if self.isEditedInSalesAuto == true {
                    global.updateSalesModifydate(self.strLeadId)
                }
                self.updateLeadIdDetail()
                
                /*Complition Alert*/
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)
                
                self.goToSendMailViewController()
            }
        }else {
            //Peview
            
            self.updatePaymentInfoCoreData()
            self.updateLeadIdDetail()
            goToSendMailViewController()
            
//            if self.isTaxCodeRequired == true
//            {
//                if self.strTaxCodeSysName == ""
//                {
//                    //Temp Comment //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Tax code is required. Please go to  General Info screen and select Taxcode under Service Address section.", viewcontrol: self)
//                    self.finalSaveAfterTaxCodeCheck()
//
//                }
//                else
//                {
//                    self.finalSaveAfterTaxCodeCheck()
//                }
//            }
//            else
//            {
//                self.finalSaveAfterTaxCodeCheck()
//            }
        }
    }
    
    
    func finalSaveAfterTaxCodeCheck() {
        if !isCustomerNotPresent {
            //present
            if self.isIAgreeTAndC {
                var isNoImage = true
                if self.customerSig != nil {
                    isNoImage = false
                }
                self.paymentConditionMethod(noImage: isNoImage)
            }else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly accept the Terms & Conditions", viewcontrol: self)
            }
            
        }else {
            //not present
            var isNoImage = false
            self.paymentConditionMethod(noImage: isNoImage)
        }
        
    }
    
    func amountMethod(noImage: Bool) {
        
        if noImage {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
        }else {
            
            if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
            }else {
                //chkChequeClick = NO;
                self.strLicenseNo = "";
                self.strChequeNo = "";
                self.strDate = "";
                
                /*Final Update*/
                self.updatePaymentInfoCoreData()
                
                
                if self.checkForCustomerSignature() == false {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

                }else {
                    if self.isEditedInSalesAuto == true {
                        global.updateSalesModifydate(self.strLeadId)
                    }
                    self.updateLeadIdDetail()
                    
                    /*Complition Alert*/
                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)

                    self.goToSendMailViewController()
                }
                
            }
        }
    }
    func noAmountMethod(noImage: Bool) {
        if noImage {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
        }else {
            
            //chkChequeClick = NO;
            self.strLicenseNo = "";
            self.strChequeNo = "";
            self.strDate = "";
            
            self.strAmount = "0";
            
            /*Final Update*/
            self.updatePaymentInfoCoreData()
            
            if self.checkForCustomerSignature() == false {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
                
            }else {
                if self.isEditedInSalesAuto == true {
                    global.updateSalesModifydate(self.strLeadId)
                }
                self.updateLeadIdDetail()
                
                /*Complition Alert*/
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)
                
                self.goToSendMailViewController()
            }
            
        }
    }
    
    
    func paymentConditionMethod(noImage: Bool) {
        
        if self.strPaymentMode == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select Payment Type", viewcontrol: self)
        }else if self.strPaymentMode == "Cash" || self.strPaymentMode == "CollectattimeofScheduling" {
            if noImage {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
            }else {
                
                if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
                }else {
                    //chkChequeClick = NO;
                    self.strLicenseNo = "";
                    self.strChequeNo = "";
                    self.strDate = "";
                    
                    /*Final Update*/
                    self.updatePaymentInfoCoreData()
                    
                    
                    if self.checkForCustomerSignature() == false {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

                    }else {
                        if self.isEditedInSalesAuto == true {
                            global.updateSalesModifydate(self.strLeadId)
                        }
                        self.updateLeadIdDetail()
                        
                        /*Complition Alert*/
                        //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)

                        self.goToSendMailViewController()
                    }
                    
                }
            }
        }else if self.strPaymentMode == "Check" {
            if noImage {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

            }else {
                if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
                }else if self.strChequeNo == "" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Check #.", viewcontrol: self)
                }else {
                    print(self.strChequeNo)
                    print(self.strLicenseNo)
                    print(self.strAmount)
                    print(self.strDate)
                    /*Final Update*/
                    self.updatePaymentInfoCoreData()
                    
                    if self.checkForCustomerSignature() == false {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

                    }else {
                        if self.isEditedInSalesAuto == true {
                            global.updateSalesModifydate(self.strLeadId)
                        }
                        self.updateLeadIdDetail()
                        
                        /*Complition Alert*/
                        //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)

                        self.goToSendMailViewController()
                    }
                }
            }
        }else if self.strPaymentMode == "CreditCard" {
            if noImage {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
            }else {
                
                if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
                }else {
                    //chkChequeClick = NO;
                    self.strLicenseNo = "";
                    self.strChequeNo = "";
                    self.strDate = "";
                    
                    /*Final Update*/
                    self.updatePaymentInfoCoreData()
                    
                    
                    if self.checkForCustomerSignature() == false {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

                    }else {
                        if self.isEditedInSalesAuto == true {
                            global.updateSalesModifydate(self.strLeadId)
                        }
                        self.updateLeadIdDetail()
                        
                        if isElementIntegration {
                            
                            if ReachabilityTest.isConnectedToNetwork() {
                                print("Internet connection available")
                                
                                self.goToCreditCardIntegration()
                            }
                            else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorInternetMsgPayment, viewcontrol: self)
                            }
                        }else {
                            /*Complition Alert*/
                            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)

                            self.goToSendMailViewController()
                        }
                    }
                }
            }
            
        }else {
            
            if dictSelectedCustomPaymentMode.count > 0 && "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1"
            {
                amountMethod(noImage: noImage)
            }
            else
            {
                noAmountMethod(noImage: noImage)
            }
            
            
//            if noImage {
//                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
//            }else {
//
//                //chkChequeClick = NO;
//                self.strLicenseNo = "";
//                self.strChequeNo = "";
//                self.strDate = "";
//                self.strAmount = "0";
//
//                /*Final Update*/
//                self.updatePaymentInfoCoreData()
//
//                if self.checkForCustomerSignature() == false {
//                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
//
//                }else {
//                    if self.isEditedInSalesAuto == true {
//                        global.updateSalesModifydate(self.strLeadId)
//                    }
//                    self.updateLeadIdDetail()
//
//                    /*Complition Alert*/
//                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)
//
//                    self.goToSendMailViewController()
//                }
//
//            }
        }
    }
    
    //Core Data
    func updatePaymentInfoCoreData() {
        
        print(objPaymentInfo)
//        self.objPaymentInfo!.setValue(self.strLeadId, forKey: "leadId")
        self.objPaymentInfo.setValue(self.strPaymentMode, forKey: "paymentMode")
        self.objPaymentInfo.setValue(self.strAmount, forKey: "amount")
        self.objPaymentInfo.setValue(self.strChequeNo, forKey: "checkNo")
        self.objPaymentInfo.setValue(self.strLicenseNo, forKey: "licenseNo")
        self.objPaymentInfo.setValue(self.strDate, forKey: "expirationDate")

        if self.isCustomerNotPresent {
            self.objPaymentInfo.setValue("", forKey: "customerSignature")
        }else {
            self.objPaymentInfo.setValue(self.strCustomerSignature, forKey: "customerSignature")
        }
        self.objPaymentInfo.setValue(self.strSalesSignature, forKey: "salesSignature")

        let defaults = UserDefaults.standard
        var strSignUrl: String = defaults.value(forKey: "ServiceTechSignPath") as? String ?? ""
        let isPreSetSign: Bool = defaults.bool(forKey: "isPreSetSignSales")
        
        matchesGeneralInfo.setValue("false", forKey: "isEmployeePresetSignature")
        if isPreSetSign && strSignUrl != "" && self.strSalesSignature.count == 0 {
            strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
            
            var result = ""
            let equalRange = NSString(string: strSignUrl).range(of: "Documents", options: .backwards)

            if equalRange.location != NSNotFound {
                print(equalRange.location)
                print(equalRange.length)
                let fromR = equalRange.location + equalRange.length
                result = strSignUrl.substring(from: fromR)
            }else {
                result = strSignUrl
            }
            print(result)
            self.objPaymentInfo.setValue(result, forKey: "salesSignature")
            matchesGeneralInfo.setValue("true", forKey: "isEmployeePresetSignature")
        }else {
            
            if self.strSalesSignature.count > 0 && self.strSalesSignature.contains("EmployeeSignatures")
            {
                
                strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")

                var result = ""
                let equalRange = NSString(string: strSignUrl).range(of: "Documents", options: .backwards)

                if equalRange.location != NSNotFound
                {
                    print(equalRange.location)
                    print(equalRange.length)
                    let fromR = equalRange.location + equalRange.length
                    result = strSignUrl.substring(from: fromR)
                }
                else
                {
                    result = strSignUrl
                }
                self.objPaymentInfo.setValue(result, forKey: "salesSignature")
                
                print(strPresetSigFromDB)
                matchesGeneralInfo.setValue(strPresetSigFromDB, forKey: "isEmployeePresetSignature")
            }
            else
            {
                matchesGeneralInfo.setValue("false", forKey: "isEmployeePresetSignature")
            }
            
           /* if isPreSetSign && strSignUrl != "" {
             
                self.strSalesSignature = self.strSalesSignature.replacingOccurrences(of: "\\", with: "/")
                let salesSig = (self.strSalesSignature as NSString).lastPathComponent

                let newString = strSignUrl.replacingOccurrences(of: "\\", with: "/")
                let preSetSig = (newString as NSString).lastPathComponent
                if salesSig == preSetSig {
                    strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")

                    var result = ""
                    let equalRange = NSString(string: strSignUrl).range(of: "Documents", options: .backwards)

                    if equalRange.location != NSNotFound {
                        print(equalRange.location)
                        print(equalRange.length)
                        let fromR = equalRange.location + equalRange.length
                        result = strSignUrl.substring(from: fromR)
                    }else {
                        result = strSignUrl
                    }
                    print(result)

                    self.objPaymentInfo.setValue(result, forKey: "salesSignature")
                    matchesGeneralInfo.setValue("true", forKey: "isEmployeePresetSignature")
                }
            }*/
            
        }
        
        self.objPaymentInfo.setValue(global.modifyDate(), forKey: "modifiedDate")
        self.objPaymentInfo.setValue(self.strCustomerNote, forKey: "specialInstructions")
        self.objPaymentInfo.setValue(self.strCompanyKey, forKey: "companyKey")
        self.objPaymentInfo.setValue(self.strUserName, forKey: "userName")

        let defs = UserDefaults.standard
        let arrImageToSend: NSMutableArray = NSMutableArray()
        var arrTemp: NSMutableArray = NSMutableArray()
        
        if let val = defs.value(forKey: "arrImageToSend") {
            arrTemp = NSMutableArray(object: val)
        }
        for i in 0..<arrTemp.count {
            arrImageToSend.add(arrTemp[i])
        }
        
        
        if self.isCustomerNotPresent {
            
        }else {
            arrImageToSend.add(self.strCustomerSignature)
        }
        arrImageToSend.add(self.strSalesSignature)

        print(arrImageToSend)
        
        if self.strPaymentMode == "Check" {
            
            if arrOFImagesName.count > 0 {
                self.objPaymentInfo.setValue(self.arrOFImagesName[0], forKey: "checkFrontImagePath")
            }else {
                self.objPaymentInfo.setValue("", forKey: "checkFrontImagePath")
            }
            
            if arrOfCheckBackImage.count > 0 {
                self.objPaymentInfo.setValue(self.arrOfCheckBackImage[0], forKey: "checkBackImagePath")
            }else {
                self.objPaymentInfo.setValue("", forKey: "checkBackImagePath")
            }
            
        }
        defs.setValue(arrImageToSend, forKey: "arrImageToSend")
        defs.synchronize()
        
//        self.objPaymentInfo.setValue("", forKey: "leadPaymentDetailId")
//        self.objPaymentInfo.setValue("", forKey: "agreement")
//        self.objPaymentInfo.setValue("", forKey: "proposal")
//        self.objPaymentInfo.setValue("", forKey: "createdBy")
//        self.objPaymentInfo.setValue("", forKey: "createdDate")
//        self.objPaymentInfo.setValue("", forKey: "modifiedBy")
//        self.objPaymentInfo.setValue("", forKey: "inspection")
        
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.fetchPaymentInfo()
    }
    
    func updateLeadIdDetail() {
        
        if isPreview
        {
            updateLeadDetailForPreview()
        }
        else
        {
            let context = getContext()

            if isSendProposal {
                isCustomerNotPresent = false
            }
            
            if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card" {
                if !self.isElementIntegration {
                    matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                }
            }else {
                matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            }
            
            if self.strGlobalAudio.count>0 {
                matchesGeneralInfo.setValue(self.strGlobalAudio, forKey: "audioFilePath")
            }else {
                matchesGeneralInfo.setValue("", forKey: "audioFilePath")
            }
            
            matchesGeneralInfo.setValue("true", forKey: "iAgreeTerms")

            let str = matchesGeneralInfo.value(forKey: "isCustomerNotPresent")
            
            if isCustomerNotPresent {
                matchesGeneralInfo.setValue("true", forKey: "isCustomerNotPresent")
            }else {
                matchesGeneralInfo.setValue("false", forKey: "isCustomerNotPresent")
            }
            
            if self.customerSig == nil {
                matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
                matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
            }else {
                matchesGeneralInfo.setValue("true", forKey: "isAgreementSigned")
                matchesGeneralInfo.setValue("true", forKey: "isAgreementGenerated")
            }
            
            
            
            matchesGeneralInfo.setValue("yes", forKey: "zSync")

            print(self.dictPricingInfo)
            var dictTemp = self.dictPricingInfo
            if isSendProposal {
                self.sendProposalCalculation()
                dictTemp = self.dictPricingInfoProposal
            }
            
           // let strFinalBilledAmount = (dictTemp?["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strFinalBilledAmount = (dictTemp?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strTotalPrice = (dictTemp?["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)


            var strFinalCollectedAmount = strAmount
            
            let strCouponDis: String = (dictTemp?["couponDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strOtherDis: String = (dictTemp?["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let finalDis: Double = (Double(strCouponDis) ?? 0.0) + (Double(strOtherDis) ?? 0.0)
            let strFinalDiscount = String(format: "%.2f", finalDis)
           // let strFinalDiscount = (dictTemp?["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strFinalSubTotal = (dictTemp?["subTotalInitial"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

            if strPaymentMode.caseInsensitiveCompare("Cash") == .orderedSame || strPaymentMode.caseInsensitiveCompare("Check") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CreditCard") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CollectattimeofScheduling") == .orderedSame {
                strFinalCollectedAmount = strAmount
            }else {
                
                if dictSelectedCustomPaymentMode.count > 0 && "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1"
                {
                    strFinalCollectedAmount = strAmount
                }
                else
                {
                    strFinalCollectedAmount = "0"
                }
            }
            
            if !isSendProposal {
                matchesGeneralInfo.setValue(strFinalBilledAmount, forKey: "billedAmount")
                matchesGeneralInfo.setValue(strFinalDiscount, forKey: "couponDiscount")
                matchesGeneralInfo.setValue(strFinalCollectedAmount, forKey: "collectedAmount")
                matchesGeneralInfo.setValue(strFinalSubTotal, forKey: "subTotalAmount")
                
                let strTaxAmt = (dictTemp?["taxAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                let strTaxMaintAmt = (dictTemp?["taxAmountMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                
                let strTaxableInitialAmount = (dictTemp?["taxableAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                let strTaxableMaintAmount = (dictTemp?["taxableMaintAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

                let strSubTotalMaintenance = (dictTemp?["subTotalMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                let strTotalPriceMaintenance = (dictTemp?["totalPriceMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

                //matchesGeneralInfo.setValue(strFinalBilledAmount, forKey: "totalPrice")
                matchesGeneralInfo.setValue(strTotalPrice, forKey: "totalPrice")
                matchesGeneralInfo.setValue(strTaxAmt, forKey: "taxAmount")
                matchesGeneralInfo.setValue(strTaxMaintAmt, forKey: "taxMaintAmount")
                
                matchesGeneralInfo.setValue(strTaxableInitialAmount, forKey: "taxableAmount")
                matchesGeneralInfo.setValue(strTaxableMaintAmount, forKey: "taxableMaintAmount")
                
                matchesGeneralInfo.setValue(strSubTotalMaintenance, forKey: "subTotalMaintAmount")
                matchesGeneralInfo.setValue(strTotalPriceMaintenance, forKey: "totalMaintPrice")
            }
            
            matchesGeneralInfo.setValue("Won", forKey: "stageSysName")

        
            if isCustomerNotPresent {
                matchesGeneralInfo.setValue("CompletePending", forKey: "stageSysName")
                if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card"{
                    if !self.isElementIntegration {
                        matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                    }
                }
            }else {
                matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            }
            
            
            if isSendProposal {
                matchesGeneralInfo.setValue("Open", forKey: "statusSysName")
                matchesGeneralInfo.setValue("Proposed", forKey: "stageSysName")
                matchesGeneralInfo.setValue("true", forKey: "isProposalFromMobile")

            }else {
                matchesGeneralInfo.setValue("false", forKey: "isProposalFromMobile")
            }
            
            
            matchesGeneralInfo.setValue(self.strMonthOfService, forKey: "strPreferredMonth")
            matchesGeneralInfo.setValue(self.strProposalNote, forKey: "proposalNotes")

            if arrTerms.count>0{
                matchesGeneralInfo.setValue(arrTerms[0], forKey: "leadGeneralTermsConditions")
            }
            
            matchesGeneralInfo.setValue(self.strInternalNote, forKey: "notes")
            if isCustomerNotPresent {
                matchesGeneralInfo.setValue(self.strIsAllowCustomer, forKey: "isSelectionAllowedForCustomer")
            }else {
                matchesGeneralInfo.setValue("false", forKey: "isSelectionAllowedForCustomer")
            }
            matchesGeneralInfo.setValue("true", forKey: "isMailSend")
            matchesGeneralInfo.setValue("false", forKey: "isPreview")
            matchesGeneralInfo.setValue(strText, forKey: "smsReminders")
            matchesGeneralInfo.setValue(strPhone, forKey: "phoneReminders")
            matchesGeneralInfo.setValue(strEmail, forKey: "emailReminders")

            
            if isCustomerNotPresent
            {
                matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
                matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
            }
            
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
            let defaults = UserDefaults.standard
            defaults.setValue(matchesGeneralInfo.value(forKey: "statusSysName"), forKey: "leadStatusSales")
            defaults.setValue(matchesGeneralInfo.value(forKey: "stageSysName"), forKey: "stageSysNameSales")
            defaults.synchronize()
            
            if isSendProposal {
            
            }else {
                self.updateLeadAppliedDiscountForAppliedCoupon()
            }
        }
    }
    
    func updateLeadDetailForPreview()  {
    
        let context = getContext()

        if isSendProposal {
            isCustomerNotPresent = false
        }
        
        if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card" {
            if !self.isElementIntegration {
                //matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                strStatusPreview = "Complete"
            }
        }else {
            //matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            strStatusPreview = "Complete"
        }
        
        if self.strGlobalAudio.count>0 {
            matchesGeneralInfo.setValue(self.strGlobalAudio, forKey: "audioFilePath")
        }else {
            matchesGeneralInfo.setValue("", forKey: "audioFilePath")
        }
        
        matchesGeneralInfo.setValue("true", forKey: "iAgreeTerms")

        let str = matchesGeneralInfo.value(forKey: "isCustomerNotPresent")
        
        if isCustomerNotPresent {
            matchesGeneralInfo.setValue("true", forKey: "isCustomerNotPresent")
        }else {
            matchesGeneralInfo.setValue("false", forKey: "isCustomerNotPresent")
        }
        
        if self.customerSig == nil {
            matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
            matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
        }else {
            matchesGeneralInfo.setValue("true", forKey: "isAgreementSigned")
            matchesGeneralInfo.setValue("true", forKey: "isAgreementGenerated")
        }
        
        
        
        matchesGeneralInfo.setValue("yes", forKey: "zSync")

        print(self.dictPricingInfo)
        var dictTemp = self.dictPricingInfo
        if isSendProposal {
            self.sendProposalCalculation()
            dictTemp = self.dictPricingInfoProposal
        }
        
       // let strFinalBilledAmount = (dictTemp?["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strFinalBilledAmount = (dictTemp?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strTotalPrice = (dictTemp?["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)


        var strFinalCollectedAmount = strAmount
        
        let strCouponDis: String = (dictTemp?["couponDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strOtherDis: String = (dictTemp?["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let finalDis: Double = (Double(strCouponDis) ?? 0.0) + (Double(strOtherDis) ?? 0.0)
        let strFinalDiscount = String(format: "%.2f", finalDis)
       // let strFinalDiscount = (dictTemp?["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strFinalSubTotal = (dictTemp?["subTotalInitial"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

        if strPaymentMode.caseInsensitiveCompare("Cash") == .orderedSame || strPaymentMode.caseInsensitiveCompare("Check") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CreditCard") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CollectattimeofScheduling") == .orderedSame {
            strFinalCollectedAmount = strAmount
        }else {
            
            if dictSelectedCustomPaymentMode.count > 0 && "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1"
            {
                strFinalCollectedAmount = strAmount
            }
            else
            {
                strFinalCollectedAmount = "0"
            }
            
            //strFinalCollectedAmount = "0"
        }
        
        if !isSendProposal {
            matchesGeneralInfo.setValue(strFinalBilledAmount, forKey: "billedAmount")
            matchesGeneralInfo.setValue(strFinalDiscount, forKey: "couponDiscount")
            matchesGeneralInfo.setValue(strFinalCollectedAmount, forKey: "collectedAmount")
            matchesGeneralInfo.setValue(strFinalSubTotal, forKey: "subTotalAmount")
            
            let strTaxAmt = (dictTemp?["taxAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strTaxMaintAmt = (dictTemp?["taxAmountMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            
            let strTaxableInitialAmount = (dictTemp?["taxableAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strTaxableMaintAmount = (dictTemp?["taxableMaintAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

            let strSubTotalMaintenance = (dictTemp?["subTotalMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strTotalPriceMaintenance = (dictTemp?["totalPriceMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

            //matchesGeneralInfo.setValue(strFinalBilledAmount, forKey: "totalPrice")
            matchesGeneralInfo.setValue(strTotalPrice, forKey: "totalPrice")
            matchesGeneralInfo.setValue(strTaxAmt, forKey: "taxAmount")
            matchesGeneralInfo.setValue(strTaxMaintAmt, forKey: "taxMaintAmount")
            
            matchesGeneralInfo.setValue(strTaxableInitialAmount, forKey: "taxableAmount")
            matchesGeneralInfo.setValue(strTaxableMaintAmount, forKey: "taxableMaintAmount")
            
            matchesGeneralInfo.setValue(strSubTotalMaintenance, forKey: "subTotalMaintAmount")
            matchesGeneralInfo.setValue(strTotalPriceMaintenance, forKey: "totalMaintPrice")
        }
        
        //matchesGeneralInfo.setValue("Won", forKey: "stageSysName")

        strStagePreview = "Won"
        
        if isCustomerNotPresent
        {
            //matchesGeneralInfo.setValue("CompletePending", forKey: "stageSysName")
            
            strStagePreview = "CompletePending"
            
            if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card"
            {
                if !self.isElementIntegration {
                    //matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                    strStatusPreview = "Complete"
                }
            }
        }
        else
        {
            //matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            strStatusPreview = "Complete"
        }
        
        
        if isSendProposal {
            //matchesGeneralInfo.setValue("Open", forKey: "statusSysName")
            //matchesGeneralInfo.setValue("Proposed", forKey: "stageSysName")
            matchesGeneralInfo.setValue("true", forKey: "isProposalFromMobile")
            strStatusPreview = "Open"
            strStagePreview = "Proposed"

        }else {
            matchesGeneralInfo.setValue("false", forKey: "isProposalFromMobile")
        }
        
        
        matchesGeneralInfo.setValue(self.strMonthOfService, forKey: "strPreferredMonth")
        matchesGeneralInfo.setValue(self.strProposalNote, forKey: "proposalNotes")

        if arrTerms.count>0{
            matchesGeneralInfo.setValue(arrTerms[0], forKey: "leadGeneralTermsConditions")
        }
        
        matchesGeneralInfo.setValue(self.strInternalNote, forKey: "notes")
        if isCustomerNotPresent {
            matchesGeneralInfo.setValue(self.strIsAllowCustomer, forKey: "isSelectionAllowedForCustomer")
        }else {
            matchesGeneralInfo.setValue("false", forKey: "isSelectionAllowedForCustomer")
        }
        
        
        matchesGeneralInfo.setValue(strText, forKey: "smsReminders")
        matchesGeneralInfo.setValue(strPhone, forKey: "phoneReminders")
        matchesGeneralInfo.setValue(strEmail, forKey: "emailReminders")

        
        if isCustomerNotPresent
        {
            matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
            matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
        }
        
        //Points to remember
        
        //matchesGeneralInfo.setValue("true", forKey: "isMailSend")
        matchesGeneralInfo.setValue("true", forKey: "isPreview")
        
        
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        let defaults = UserDefaults.standard
        defaults.setValue(matchesGeneralInfo.value(forKey: "statusSysName"), forKey: "leadStatusSales")
        defaults.setValue(matchesGeneralInfo.value(forKey: "stageSysName"), forKey: "stageSysNameSales")
        defaults.synchronize()
        
        if isSendProposal {
        
        }else {
            self.updateLeadAppliedDiscountForAppliedCoupon()
        }
    }
    
    func updateLeadAppliedDiscountForAppliedCoupon() {
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        let arrAllSold = arrStandardService + arrBundleService
        print(arryOfData)
        var objIn: NSManagedObject?
        if arryOfData.count>0 {
            for data in arryOfData {
                objIn = (data as! NSManagedObject)
                for soldData in arrAllSold {
                    let id1: String = "\(objIn!.value(forKey: "soldServiceId") ?? "")"
                    let id2: String = "\(soldData.value(forKey: "soldServiceStandardId") ?? "")"
                    if id1 == id2 {
                        objIn!.setValue("true", forKey: "isApplied")
                    }
                }
            }
        }
        
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func checkForCustomerSignature() -> Bool {
        var chkCustomerSignPresent = false
        let arrPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))

        if(arrPaymentInfo.count > 0)
        {
            for objPayment in arrPaymentInfo {
                let objPaymentIn: NSManagedObject = objPayment as! NSManagedObject
                print(objPaymentIn)
                
                let signCustomer: String = objPaymentIn.value(forKey: "customerSignature") as! String
                if signCustomer.count == 0 || signCustomer == "" {
                    chkCustomerSignPresent = false
                    
                    if isCustomerNotPresent == true || isSendProposal == true {
                        chkCustomerSignPresent = true
                    }
                }else {
                    chkCustomerSignPresent = true
                }
            }
           
        }
        return chkCustomerSignPresent
    }
    
    
    func sendProposalCalculation() {
        var arrayOfFreqPrice    : [String] = []
        var arrayOfFreqSysName  : [String] = []
        var arrMaintBillingPrice  : [String] = []


        var subTotalInitialPrice    = 0.0
        var totalDiscountPriceP      = 0.0
        var subTotalMaintenanceAmount    = 0.0
        
        
        var totalInitialPrice = 0.0
        var totalDiscountPrice = 0.0
        var totalMaintPrice = 0.0
        
        let arrAllSold = arrStandardService + arrBundleService

        for objServiceDetail in arrAllSold {
            var isUnitBased = false
            var isParameterBased = false
            
            let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "serviceSysName") ?? "")")
            
            if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
            {
                isUnitBased = true
            }
            else
            {
                isUnitBased = false
            }
            
            if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
            {
                isParameterBased = true
            }
            else
            {
                isParameterBased = false
            }
            
            
            var sumFinalInitialPrice = 0.0, sumFinalMaintPrice = 0.0
            //For Parameter based service
            if isParameterBased
            {
                let arrPara = objServiceDetail.value(forKey: "additionalParameterPriceDcs") as! NSArray
                
                if arrPara.count > 0
                {
                    for i in 0..<arrPara.count
                    {
                        let dict = arrPara.object(at: i) as! NSDictionary
                        let arrKey = dict.allKeys as NSArray
                        if arrKey.contains("FinalInitialUnitPrice")
                        {
                            sumFinalInitialPrice = sumFinalInitialPrice + Double(("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).floatValue)
                        }
                        if arrKey.contains("FinalMaintUnitPrice")
                        {
                            sumFinalMaintPrice = sumFinalMaintPrice + Double(("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).floatValue)
                        }
                    }
                }
            }
            
            var unit = 1.0
            if isUnitBased
            {
                unit = Double(("\(objServiceDetail.value(forKey: "unit") ?? "")" as NSString).floatValue)
            }
            
            
            
            //Initial Price
            let initialPrice: String = objServiceDetail.value(forKey: "initialPrice") as! String
            totalInitialPrice = totalInitialPrice + ((Double(initialPrice) ?? 0.0) * unit)
            totalInitialPrice = (totalInitialPrice + sumFinalInitialPrice)
            if totalInitialPrice < 0
            {
                totalInitialPrice = 0
            }
            
            
            //Discount Price
            let discountPrice: String = objServiceDetail.value(forKey: "discount") as! String
            totalDiscountPrice = totalDiscountPrice + (Double(discountPrice) ?? 0.0)
            
            //totalMaintPrice
            let maintPrice: String = objServiceDetail.value(forKey: "totalMaintPrice") as! String
            totalMaintPrice = totalMaintPrice + ((Double(maintPrice) ?? 0.0) * unit) + sumFinalMaintPrice
            if totalMaintPrice < 0
            {
                totalMaintPrice = 0
            }
            
            
            //For Billing Frequancy
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
            let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")")
            
            var totalBillingAmount = 0.0
            let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                totalBillingAmount = (((Double(initialPrice) ?? 0.0) * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
            else
            {
                if chkFreqConfiguration
                {
                    if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                    {
                        totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                    else
                    {
                        totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                else
                {
                    totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            
            if totalBillingAmount < 0
            {
                totalBillingAmount = 0.0
            }
            //                let billingFrequencyPrice: String = objServiceDetail.value(forKey: "billingFrequencyPrice") as! String
            arrayOfFreqPrice.append("\(totalBillingAmount)")
            
            let billingFrequencySysName: String = objServiceDetail.value(forKey: "billingFrequencySysName") as! String
            arrayOfFreqSysName.append(billingFrequencySysName)
                        
            
            //Tax Calculations
          /*  if self.strIsServiceAddrTaxExempt == "false" {
                if strServiceAddressSubType == "Commercial" {
                    if self.dictCommercialStatus.value(forKey: objServiceDetail.value(forKey: "serviceSysName") as! String) as! String == "1" {
                        let strInitial = "\(objServiceDetail.value(forKey: "totalInitialPrice") ?? "0")"
                        let strDiscount = "\(objServiceDetail.value(forKey: "discount") ?? "0")"

                        self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                        
                        self.discountStanTax = self.initialStanTax + (Double(strDiscount) ?? 0.0)

                        self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                        print(self.initialStanTax, self.discountStanTax)
                    }
                }else {
                    if self.dictResidentialStatus.value(forKey: objServiceDetail.value(forKey: "serviceSysName") as! String) as! String == "1" {
                        let strInitial = "\(objServiceDetail.value(forKey: "totalInitialPrice") ?? "0")"
                        let strDiscount = "\(objServiceDetail.value(forKey: "discount") ?? "0")"

                        self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                        
                        self.discountStanTax = self.discountStanTax + (Double(strDiscount) ?? 0.0)

                        self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                        print(self.initialStanTax, self.discountStanTax)
                    }
                }
            }*/
        }

        
        for objServiceDetail in self.arrCustomService {
            
            //Initial Price
            let initialPrice: String = objServiceDetail.value(forKey: "initialPrice") as! String
            totalInitialPrice = totalInitialPrice + (Double(initialPrice) ?? 0.0)
            
            //Discount Price
            let discountPrice: String = objServiceDetail.value(forKey: "discount") as! String
            totalDiscountPrice = totalDiscountPrice + (Double(discountPrice) ?? 0.0)
            
            //totalMaintPrice
            let maintPrice: String = objServiceDetail.value(forKey: "maintenancePrice") as! String
            totalMaintPrice = totalMaintPrice + (Double(maintPrice) ?? 0.0)
            
            
            //For Billing Frequancy
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
            let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")")
            
            var totalBillingAmount = 0.0
            let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                totalBillingAmount = ((Double(initialPrice) ?? 0.0) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
            else
            {
                if chkFreqConfiguration
                {
                    if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                    {
                        totalBillingAmount = ((Double(maintPrice) ?? 0.0) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                    else
                    {
                        totalBillingAmount = ( (Double(maintPrice) ?? 0.0) * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                else
                {
                    totalBillingAmount = ( (Double(maintPrice) ?? 0.0) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            
            if totalBillingAmount < 0
            {
                totalBillingAmount = 0.0
            }
            //                let billingFrequencyPrice: String = objServiceDetail.value(forKey: "billingFrequencyPrice") as! String
            arrayOfFreqPrice.append("\(totalBillingAmount)")
            
            let billingFrequencySysName: String = objServiceDetail.value(forKey: "billingFrequencySysName") as! String
            arrayOfFreqSysName.append(billingFrequencySysName)
            
            print(objServiceDetail.value(forKey: "isTaxable") as! String)
          /*  if "\(objServiceDetail.value(forKey: "isTaxable") ?? "false")" == "false" || "\(objServiceDetail.value(forKey: "isTaxable") ?? "0")" == "0" {
                
                self.initialStanTax = self.initialStanTax + (Double(initialPrice) ?? 0.0)
                
                self.discountStanTax = self.discountStanTax + (Double(discountPrice) ?? 0.0)
                
                
                self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                print(self.initialStanTax, self.discountStanTax, self.initialMaintTax)
                
            }*/
        }
        
        subTotalInitialPrice  = subTotalInitialPrice + totalInitialPrice
        totalDiscountPriceP  = totalDiscountPriceP + totalDiscountPrice
        subTotalMaintenanceAmount  = subTotalMaintenanceAmount + totalMaintPrice
        
        print(subTotalInitialPrice)
        print(totalDiscountPrice)
        print(subTotalMaintenanceAmount)
        
        self.dictPricingInfoProposal["totalDueAmount"] = "\(subTotalInitialPrice)"
        self.dictPricingInfoProposal["totalPriceMaintenance"] = "\(subTotalMaintenanceAmount)"
        self.dictPricingInfoProposal["billingAmount"] = "\(subTotalInitialPrice)"
        self.dictPricingInfoProposal["taxAmountMaintenance"] = ""
        self.dictPricingInfoProposal["couponDiscount"] = ""
        self.dictPricingInfoProposal["otherDiscount"] = "\(totalDiscountPrice)"
        self.dictPricingInfoProposal["totalPrice"] = "\(subTotalInitialPrice)"
        self.dictPricingInfoProposal["credit"] = ""
        self.dictPricingInfoProposal["subTotalInitial"] = "\(subTotalInitialPrice)"
        self.dictPricingInfoProposal["creditMaint"] = ""
        self.dictPricingInfoProposal["subTotalMaintenance"] = "\(subTotalMaintenanceAmount)"
        self.dictPricingInfoProposal["taxAmount"] = ""
        
        
       // var arrMaintBillingPrice  : [String] = []

        let arrUniqFreqSysName = Set(arrayOfFreqSysName)
        for value in arrUniqFreqSysName {
            var price = 0.0
            for i in 0..<arrayOfFreqSysName.count
            {
                let strVal = arrayOfFreqSysName[i]
                let strValPrice = arrayOfFreqPrice[i]
                if value == strVal {
                    price = price + (Double(strValPrice) ?? 0.0)
                }
            }
            let strMaintBillingPrice = "\(String(format: "$%.2f", price))/\(value)"
            arrMaintBillingPrice.append(strMaintBillingPrice)
        }
        self.dictPricingInfoProposal["arrMaintBillingPrice"] = arrMaintBillingPrice
        
        print(self.dictPricingInfoProposal)

    }
    
    //MARK: - ------------------ Footer Actions ---------------------
    
    
    func goToEmailHistory()  {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
              let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
              testController?.modalPresentationStyle = .fullScreen
              testController?.strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
              testController?.strCompanyKey = strCompanyKey
              testController?.strUserName = strUserName

              self.present(testController!, animated: false, completion: nil)
    }
    
    func goToServiceHistory()  {
        
        /*let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = "\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
        
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
        
        
    }
    func goToServiceNotesHistory()  {
        
        //ServiceNotesHistoryViewController
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController?.strTypeOfService = "sales";
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    
    
    @IBAction func actionOnSetting(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
            self.view.endEditing(true)
            
           
            self.goToEmailHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
        
            self.goToSetupHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
        
            self.goToServiceNotesHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
        
            self.goToServiceHistory()
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
           // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.up]
        }
        self.present(alert, animated: true)
       
      }
    
    @IBAction func actionOnImage(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Make Your Selection", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.AddImages, style: .default, handler: { action in
            self.GotoGlobleImageGraphView(strType: "Before")
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.AddDocuments, style: .default, handler: { [self] action in
            let strLeadID =  strLeadId

            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: DeviceType.IS_IPAD ? "UploadDocumentSalesiPad" : "UploadDocumentSales") as! UploadDocumentSales
            vc.strWoId = "\(strLeadID)" as NSString
            self.navigationController?.present(vc, animated: true, completion: nil)

        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
      //      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.down]
        }
        self.present(alert, animated: true)
    }
    
    @IBAction func actionOnGraph(_ sender: Any) {
        
        self.view.endEditing(true)
        nsud.setValue(true, forKey: "firstGraphImage")
        nsud.setValue(false, forKey: "servicegraph")
        GotoGlobleImageGraphView(strType: "Graph")
    }
    
    @IBAction func action_Home(_ sender: Any) {
        self.view.endEditing(true)
        self.goToAppointment()
    }
    
    @IBAction func actionOnChemicalSensitivityList(_ sender: Any) {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoGlobleImageGraphView(strType : String)  {
        
        let strLeadID =  matchesGeneralInfo.value(forKey: "leadId") ?? ""
        var strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
        var strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
        if(nsud.value(forKey: "backFromInspectionNew") != nil){
            
            if(nsud.value(forKey: "backFromInspectionNew") as! Bool == true){
                strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
                strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
            }
        }
    
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPhone" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        vc.strHeaderTitle = strType as NSString
        vc.strWoId = "\(strLeadID)" as NSString
        vc.strWoLeadId = "\(strLeadID)" as NSString
        vc.strModuleType = "SalesFlow"
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won"){
            vc.strWoStatus = "Complete"
        }else{
            vc.strWoStatus = "InComplete"
        }
        vc.strWoStatus = "InComplete"
        self.navigationController?.present(vc, animated: true, completion: nil)

    }
    
    func goToAppointment()
    {
           
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    @IBAction func markAsLostAction(_ sender: Any)
    {
        
        Global().updateSalesModifydate(strLeadId as String)
        
        let alert = UIAlertController(title: alertMessage, message: "Are you sure to mark Opportunity as lost?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "YES", style: .default, handler: { (nil) in
            self.chkForLost = true
            
            self.matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            self.matchesGeneralInfo.setValue("Lost", forKey: "stageSysName")

            let context = getContext()
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
            
            self.goToAppointment()
        }))
        alert.addAction(UIAlertAction (title: "NO", style: .cancel, handler: { (nil) in
            self.chkForLost = false
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backToScheduleAction(_ sender: Any) {
        
        goToAppointment()
    }
    
    @IBAction func initialSetupAction(_ sender: Any) {
        if DeviceType.IS_IPAD {
            if btnInitialSetup.titleLabel?.text == "Initial Setup" {
                let defs = UserDefaults.standard
                defs.set(true, forKey: "fromAgreementInitialSetup")
                defs.setValue("forFollowUp", forKey: "FollowUp")
                defs.synchronize()
                
                let storyBoard = UIStoryboard(name: "SalesMainiPad", bundle: nil)
                let objInitialSetUp = storyBoard.instantiateViewController(withIdentifier: "InitialSetUpiPad") as? InitialSetUpiPad
                if let objInitialSetUp = objInitialSetUp {
                    navigationController?.pushViewController(objInitialSetUp, animated: false)
                }
                
            } else {
                //For Generate Workorder
                let defs = UserDefaults.standard
                defs.setValue("fromGenerateWorkorder", forKey: "fromGenerateWorkorder")
                defs.synchronize()
                
                let storyBoard = UIStoryboard(name: "SalesMainiPad", bundle: nil)
                let objGenerateWorkOrder = storyBoard.instantiateViewController(withIdentifier: "GenerateWorkOrderiPad") as? GenerateWorkOrderiPad
                if let objGenerateWorkOrder = objGenerateWorkOrder {
                    navigationController?.pushViewController(objGenerateWorkOrder, animated: false)
                }
            }
            
        }else {
            
            //iPhone
            if btnInitialSetup.titleLabel?.text == "Initial Setup" {
                let defs = UserDefaults.standard
                defs.set(true, forKey: "fromAgreementInitialSetup")
                defs.setValue("forFollowUp", forKey: "FollowUp")
                defs.synchronize()
                
                let storyBoard = UIStoryboard(name: "SalesMain", bundle: nil)
                let objInitialSetUp = storyBoard.instantiateViewController(withIdentifier: "InitialSetUp") as? InitialSetUp
                if let objInitialSetUp = objInitialSetUp {
                    navigationController?.pushViewController(objInitialSetUp, animated: false)
                }
                
            } else {
                
                //For Generate Workorder
                let defs = UserDefaults.standard
                defs.setValue("fromGenerateWorkorder", forKey: "fromGenerateWorkorder")
                defs.synchronize()
                
                let storyBoard = UIStoryboard(name: "SalesMain", bundle: nil)
                let objGenerateWorkOrder = storyBoard.instantiateViewController(withIdentifier: "GenerateWorkOrder") as? InitialSetUp
                if let objGenerateWorkOrder = objGenerateWorkOrder {
                    navigationController?.pushViewController(objGenerateWorkOrder, animated: false)
                }
            }
            
        }
        
    }
    
    
    @objc func showInitialSetup(dictSalesDB: NSManagedObject) -> Bool
    {
        var show = Bool()
        var dictLoginData = NSDictionary()
        dictLoginData =  nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strInitialSetupStatus = "\(dictSalesDB.value(forKey: "isInitialSetupCreated") ?? "")"
        let strLeadStatus = "\(dictSalesDB.value(forKey: "statusSysName") ?? "")"
        let strLeadIdSales = "\(dictSalesDB.value(forKey: "leadId") ?? "")"
        let strIsSendProposalStatus = "\(dictSalesDB.value(forKey: "isProposalFromMobile") ?? "")"
        let strStageSysName = "\(dictSalesDB.value(forKey: "stageSysName") ?? "")"
        
        let strIsServiceActive = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.IsActive") ?? "")"
        
        let arrSold = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadIdSales, "true"))
        
        
        if !isInternetAvailable()
        {
            show = false
        }
        else if(strIsSendProposalStatus.caseInsensitiveCompare("true") == .orderedSame)
        {
            show = false
        }
        else if(strStageSysName.caseInsensitiveCompare("lost") == .orderedSame)
        {
            show = false
        }
        else if(strIsServiceActive.caseInsensitiveCompare("false") == .orderedSame || strIsServiceActive == "0")
        {
            show = false
        }
        else if(strLeadStatus.caseInsensitiveCompare("open") == .orderedSame)
        {
            show = false
        }
        else if(strIsSendProposalStatus.caseInsensitiveCompare("true") == .orderedSame)
        {
            show = false
        }
        else if(strInitialSetupStatus.caseInsensitiveCompare("false") == .orderedSame && strLeadStatus.caseInsensitiveCompare("complete") == .orderedSame && arrSold.count > 0)
        {
            show = true
        }
        else
        {
            show = false
        }
        
        return show
        
    }
    @objc func showGenerateWorkorderSetup(dictSalesDB: NSManagedObject) -> Bool
    {
        var show = Bool()
        
        let arrOneTime = NSMutableArray()
        let arrNonOneTime = NSMutableArray()
        
        let strLeadIdSales = "\(dictSalesDB.value(forKey: "leadId") ?? "")"
        
        let arrSoldStandard = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadIdSales, "true"))
        
        for obj in arrSoldStandard
        {
            let data = obj as! NSManagedObject
            
            if("\(data.value(forKey: "serviceFrequency") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame || "\(data.value(forKey: "serviceFrequency") ?? "")" == "One Time")
            {
                arrOneTime.add("OneTime")
            }
            else
            {
                arrNonOneTime.add("NonOneTime")
                
            }
        }
        
        let arrSoldNonStandard = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadIdSales, "true"))
        
        for obj in arrSoldNonStandard
        {
            let data = obj as! NSManagedObject
            
            if("\(data.value(forKey: "serviceFrequency") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame || "\(data.value(forKey: "serviceFrequency") ?? "")" == "One Time")
            {
                arrOneTime.add("OneTime")
            }
            else
            {
                arrNonOneTime.add("NonOneTime")
                
            }
        }
        
        if arrNonOneTime.count > 0
        {
            show = false
        }
        else
        {
            if (arrOneTime.count > 0)
            {
                show = true
            }
            else
            {
                show = false
            }
        }
        
        return show
    }
    
    //MARK: - Go To SendMailViewController
    func goToSendMailViewController() {
        
        if isPreview
        {
            if(isInternetAvailable())
            {
                let vc = storyboardNewSalesSendMail.instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
                vc.isSendProposal = self.isSendProposal
                vc.strStatusForPreview = strStatusPreview
                vc.strStageForPreview = strStagePreview
                vc.isPreview = isPreview
                self.navigationController?.pushViewController(vc, animated: false)
                isPreview = false
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            }

        }
        else
        {
            let alert = UIAlertController(title: Alert, message: "Appointment completed", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in

                let vc = storyboardNewSalesSendMail.instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
                vc.isSendProposal = self.isSendProposal
                //Preview
                vc.isPreview = false
                self.navigationController?.pushViewController(vc, animated: false)

            }))

            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: {
            })
        }
    }
    func goToCreditCardIntegration() {
        let mainStoryboard = UIStoryboard(
            name: "SalesMain",
            bundle: nil)
        let objCreditCard = mainStoryboard.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
        
        var strValue = ""
        if self.isCustomerNotPresent {
            strValue = "no"
        }else {
            strValue = "yes"
        }
        objCreditCard!.strGlobalLeadId = self.strLeadId
        objCreditCard!.strAmount = self.strAmount
        objCreditCard!.strTypeOfService = "Lead"

        self.navigationController?.pushViewController(objCreditCard!, animated: false)
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
       // vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    // MARK: ------------- Pestpac Integration -------------------
    
    

    func showCreditCardDetail() -> String {
        // if billToLocationId.count > 0 && billToLocationId != 0 && paymentMode = "CreditCard"
        // show add card button else hide
        
        let billToId = "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")"//"1693164"
        var strTypeNo = "\(matchesGeneralInfo.value(forKey: "creditCardType") ?? "")"//"Discover" //Nedd to add by back end - creditCardType
        let creditCardNumber = "\(matchesGeneralInfo.value(forKey: "cardAccountNumber") ?? "")"//"*************7890" //Nedd to add by back end - creditCardNo
        if creditCardNumber.count >= 4  {
            let last4 = creditCardNumber.suffix(4)
            strTypeNo = strTypeNo + "  \(last4)"
        }
        // For Local Check.
        if(nsud.value(forKey: billToId) != nil){
            
            if nsud.value(forKey: billToId) is NSDictionary {
                
                let dictCreditCardInfo = nsud.value(forKey: billToId) as! NSDictionary
                let accountNumber = String("\(dictCreditCardInfo.value(forKey: "accountNumber") ?? "")".suffix(4))
                strTypeNo = "\(dictCreditCardInfo.value(forKey: "creditCardType") ?? "") " + accountNumber
            }
        }
        
        print(strTypeNo) // Show this value with Add Cart button If strtypeno.count > 0 than only show card value lable
        return strTypeNo
    }
    
    func goToPestPac_PaymentIntegration() {
        
        self.view.endEditing(true)
        
        if isInternetAvailable() {
            
            let firstNameL = "\(matchesGeneralInfo.value(forKey: "billingFirstName") ?? "")"
            let lastNameL = "\(matchesGeneralInfo.value(forKey: "billingLastName") ?? "")"
            let billingAddress1L = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? "")"
            let billingZipcodeL = "\(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
            let billToLocationIdL = "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")"
            let webLeadIdL = strLeadId
            let contactNameToCheck = "\(firstNameL)" + "\(lastNameL)"

            let contactName = "\(firstNameL)" + " " + "\(lastNameL)"
            
            let ammountL = Double(strAmount) ?? 0.0
                        
            if contactNameToCheck.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing contact name.", viewcontrol: self)
                
            }
            else if contactName.count > 30 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Billing contact name must be less than 30 characters", viewcontrol: self)
                
            }
            else if billingAddress1L.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing address.", viewcontrol: self)
                
            }else if billingZipcodeL.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing zipcode.", viewcontrol: self)
                
            }else if billToLocationIdL.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billto location id.", viewcontrol: self)
                
            }else if webLeadIdL.count == 0 || webLeadIdL == "0" {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Lead does not exist.", viewcontrol: self)
                
            }else if ammountL <= 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide amount to charge.", viewcontrol: self)
                
            }else{
                
                let dictPaymentDetails = NSMutableDictionary()

                dictPaymentDetails.setValue(webLeadIdL, forKey: "leadId")
                dictPaymentDetails.setValue(billToLocationIdL, forKey: "billToId")
                dictPaymentDetails.setValue("\(ammountL)", forKey: "amount")
                dictPaymentDetails.setValue(contactName, forKey: "nameOnCard")
                dictPaymentDetails.setValue(billingAddress1L, forKey: "billingAddress")
                dictPaymentDetails.setValue(billingZipcodeL, forKey: "billingPostalCode")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestPacPayment" : "PestPacPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "PestPacPaymentVC") as? PestPacPaymentVC
                vc?.dictPaymentDetails = dictPaymentDetails
                self.navigationController?.present(vc!, animated: false, completion: nil)

            }
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }
        
        /*if isInternetAvailable() {
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterDashboardNew_iPhoneVC") as? FilterDashboardNew_iPhoneVC
            vc!.delegate = self
            vc!.strViewComeFrom = "Dashboard"
            self.navigationController?.present(vc!, animated: true)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }*/
        
    }
    
    
    
    
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SalesReviewViewController: UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSendProposal {
            if self.isBundleService {
                return 3
            }else {
                return 2
            }
        }else {
            return self.arrSectionTitle.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSendProposal {
            if(section == 0){
                let row = (self.arrStandardService.count + self.arrCustomService.count)
                return  row
            }else {
                return  1
            }
            
        }else {
            var count = 0
            if self.isBundleService {
                count = 1
            }
            
            if(section == 0){
                return  1
            }else if(section == 1){
                return  1
            }else if(section == 2){
                let row = (self.arrStandardService.count + self.arrCustomService.count)
                return  row
            }else if(section == 3 + count){
                return  1
            }else if(section == 4 + count){
                return  1
            }else if(section == 5 + count){
                return  1
            }else if(section == 6 + count){
                return  arrNotes.count
            }else if(section == 7 + count){
                return  1
            }else if(section == 8 + count){
                return  1
            }else{
                return  1
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isSendProposal {
            if self.isBundleService {
                if indexPath.section == 1 {
                    //Bundle Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceCell", for: indexPath as IndexPath) as! ConfigBundleServiceTableViewCell
                    cell.isFromReview = true
                    cell.isFromSendProposal = true
                    cell.configureBundleTebleView(arrBundleService: self.arrBundleService, isCompleteWon: getOpportunityStatus())

                    return cell
                }
            }
            
            if indexPath.section == 0 {
                //Service
                let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ServiceTableViewCell
                
                var titleSection = ""
                cell.headerBg.backgroundColor = UIColor.init(hex: "4F5A68")
                cell.contentView.backgroundColor = UIColor.init(hex: "E1E2E2")
                cell.lblTitle.textColor = .white
                cell.lblBilled.textColor = .white
                cell.lblTimePeriod.textColor = .white
                cell.lblRenewalDecription.isHidden = true
                
                print("SSSSS: \(indexPath.row)")
                if indexPath.row < self.arrStandardService.count {
                    if indexPath.row == 0 {
                        titleSection = "Standard Service"
                    }else {
                        cell.headerConstraint.constant = 1.0
                    }
                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrStandardService[indexPath.row]
                    cell.setStandardServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    
                    let strRenewal = self.fetchRenewalServiceFromCoreData(soldServiceId: "\(objCustomServiceDetail.value(forKey: "serviceId") ?? "")")
                    
                    if strRenewal != "" {
                        cell.lblRenewalDecription.isHidden = false
                        let myString = "Renewal: "
                        cell.lblRenewalDecription.text = myString + strRenewal

                    }else {
                        cell.lblRenewalDecription.isHidden = true
                    }
                }else {
                    let index = indexPath.row - self.arrStandardService.count
                    if index < self.arrCustomService.count {
                        if index == 0 {
                            titleSection = "Custom Service"
                        }else {
                            cell.headerConstraint.constant = 1.0
                        }
                        
                        var objCustomServiceDetail = NSManagedObject()
                        objCustomServiceDetail = self.arrCustomService[index]
                        cell.setCustomServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    }
                }
                
                
                cell.lblHeaderTitle.text = titleSection
                cell.collectionSetup(row: indexPath.row)
                
                return cell
                
            } else {
                //Notes
                let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell", for: indexPath as IndexPath) as! NotesTableViewCell
                cell.lblTitle.text = arrNotes[1]
                cell.delegate = self
                cell.lblSubTitle.text = self.strProposalNote
                
                return cell
            }
        }else {
            
            var count = 0
            if self.isBundleService {
                count = 1
            }
            
            //Customer Info
            if(indexPath.section == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "SalesReviewGeneralInfo", for: indexPath as IndexPath) as! SalesReviewGeneralInfo
                cell.cellConfigure(objLeadDetails : self.matchesGeneralInfo)
               
                cell.btn_Email.addTarget(self, action: #selector(actionViewBtnEmail(sender:)), for: .touchUpInside)
                cell.btn_Mobile.addTarget(self, action: #selector(actionViewBtnCall(sender:)), for: .touchUpInside)
                cell.btn_Address.addTarget(self, action: #selector(actionViewBtnAddress(sender:)), for: .touchUpInside)
                
//                if getOpportunityStatus() {
//                    cell.btn_Email.isUserInteractionEnabled = false
//                    cell.btn_Mobile.isUserInteractionEnabled = false
//                    cell.btn_Address.isUserInteractionEnabled = false
//                }
                return cell
                
            }else if(indexPath.section == 1) {
                //Inspection
                let cell = tableView.dequeueReusableCell(withIdentifier: "InspectionCell", for: indexPath as IndexPath) as! InspectionTableViewCell
                
                if !isInspectionLoaded {
                    isInspectionLoaded = true
                    
                    cell.matchesGeneralInfo = self.matchesGeneralInfo
                    
                    // Do any additional setup after loading the view.
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                        cell.loadDynamicForm()
                        
                        tblView.beginUpdates()
                        tblView.endUpdates()
                    }
                }
                
                
                return cell
            }else if indexPath.section == 2 {
                //Service
                let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ServiceTableViewCell
                
                var titleSection = ""
                print("SSSSS: \(indexPath.row)")
                if indexPath.row < self.arrStandardService.count {
                    if indexPath.row == 0 {
                        titleSection = "Standard Service"
                    }else {
                        cell.headerConstraint.constant = 1.0
                    }
                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrStandardService[indexPath.row]
                    cell.setStandardServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    
                    let strRenewal = self.fetchRenewalServiceFromCoreData(soldServiceId: "\(objCustomServiceDetail.value(forKey: "serviceId") ?? "")")
                    
                    if strRenewal != "" {
                        cell.lblRenewalDecription.isHidden = false
                        let myString = "Renewal: "
                        cell.lblRenewalDecription.text = myString + strRenewal

                    }else {
                        cell.lblRenewalDecription.isHidden = true
                    }
                }else {
                    let index = indexPath.row - self.arrStandardService.count
                    if index == 0 {
                        titleSection = "Custom Service"
                    }else {
                        cell.headerConstraint.constant = 1.0
                    }
                    
                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrCustomService[index]
                    cell.setCustomServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                }
                cell.lblHeaderTitle.text = titleSection
                cell.collectionSetup(row: indexPath.row)
                
                return cell
            }
            
            if self.isBundleService {
                if indexPath.section == 3 {
                    //Bundle Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceCell", for: indexPath as IndexPath) as! ConfigBundleServiceTableViewCell
                    cell.isFromReview = true
                    cell.configureBundleTebleView(arrBundleService: self.arrBundleService, isCompleteWon: getOpportunityStatus())

                    return cell
                }
            }
            
            if(indexPath.section == 3 + count) {
                //Pricing Info
                let cell = tableView.dequeueReusableCell(withIdentifier: "PricingCell", for: indexPath as IndexPath) as! PricingInfoTableViewCell
                
                cell.lblSubTotalInitial.text        = (self.dictPricingInfo?["subTotalInitial"] as! String)
                cell.lblCouponDiscount.text         = (self.dictPricingInfo?["couponDiscount"] as! String)
                cell.lblCredit.text                 = (self.dictPricingInfo?["credit"] as! String)
                cell.lblOtherDiscount.text          = (self.dictPricingInfo?["otherDiscount"] as! String)
                cell.lblTotalPrice.text             = (self.dictPricingInfo?["totalPrice"] as! String)
                cell.lblTaxAmount.text              = (self.dictPricingInfo?["taxAmount"] as! String)
                cell.lblBillingAmount.text          = (self.dictPricingInfo?["billingAmount"] as! String)
                
                cell.lblSubTotalMaintenance.text    = (self.dictPricingInfo?["subTotalMaintenance"] as! String)
                cell.lblTotalPriceMaintenance.text  = (self.dictPricingInfo?["totalPriceMaintenance"] as! String)
                cell.lblTaxAmountMaintenance.text   = (self.dictPricingInfo?["taxAmountMaintenance"] as! String)
                cell.lblTotalDueAmount.text         = (self.dictPricingInfo?["totalDueAmount"] as! String)
                cell.lblCreditMaint.text            = (self.dictPricingInfo?["creditMaint"] as! String)

                
                /*Maint billing info*/
                let arrMaintBillingPrice: [String] = self.dictPricingInfo?["arrMaintBillingPrice"] as! [String]
                if arrMaintBillingPrice.count > 0 {
                    cell.tableMaintSetUp(arrMaintBillingPrice : arrMaintBillingPrice)
                }else {
                    cell.arrMaintBillingPrice.removeAll()
                    cell.tblMaintFrqHeight.constant = 20
                    cell.tblMaintFrq.reloadData()
                }
                /**********************/
                
                return cell
            }else if(indexPath.section == 4 + count) {
                //Discount
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountCell", for: indexPath as IndexPath) as! DiscountTableViewCell
                
                /*Coupon Details*/
                if self.arrAppliedCoupon.count > 0 {
                    cell.tableSetUp(arrAppliedCoupon : self.arrAppliedCoupon)
                }else {
                    cell.arrAppliedCoupon.removeAll()
                    cell.tblCouponHeight.constant = 20
                    cell.tblCoupon.reloadData()
                }
                /**********************/
                
                /*Credit Details*/
                if self.arrAppliedCredit.count > 0 {
                    cell.tableCreditSetUp(arrAppliedCredit : self.arrAppliedCredit)
                }else {
                    cell.arrAppliedCredit.removeAll()
                    cell.tblCreditHeight.constant = 20
                    cell.tblCredit.reloadData()
                }
                /**********************/
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
                    tblView.beginUpdates()
                    tblView.endUpdates()
                }
                return cell
            }else if(indexPath.section == 5 + count) {
                //Payment
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath as IndexPath) as! PaymentTableViewCell
                cell.delegate = self
                cell.lblSubTotal.text        = (self.dictPricingInfo?["subTotalInitial"] as! String)
                cell.lblTotal.text          = (self.dictPricingInfo?["totalPrice"] as! String)
                cell.lblTaxAmount.text      = (self.dictPricingInfo?["taxAmount"] as! String)
                cell.lblBillingAmount.text  = (self.dictPricingInfo?["billingAmount"] as! String)
                //cell.lblAmount.text         = (self.dictPricingInfo?["billingAmount"] as! String)
                
                
                
                cell.txtAmount.text = strAmount//strCollectedAmount//(self.dictPricingInfo?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "")
                //self.strAmount = cell.txtAmount.text ?? ""
                
                if self.strChequeNo != "" {
                    cell.txtCheckNo.text = self.strChequeNo
                }
                if self.strLicenseNo != "" {
                    cell.txtLicenceNo.text = self.strLicenseNo
                }
                if self.strDate != "" {
                    cell.btnExpDate.setTitle("Expiration Date: \(self.strDate)", for: .normal)
                }
                
                cell.lblPaymentType.text = selectecPaymentType
                
                cell.amountViewHightConstraint.constant = 0
                cell.amountView.isHidden = true
                cell.checkView.isHidden = true
                cell.licenceView.isHidden = true
                cell.buttonsView.isHidden = true
                
                //amountViewHightConstraint
                if cell.lblPaymentType.text == "Cash" || cell.lblPaymentType.text == "Collect at time of Scheduling" || cell.lblPaymentType.text == "Credit Card" {
                    cell.amountView.isHidden = false

                    cell.amountViewHightConstraint.constant = 65
                }else if cell.lblPaymentType.text == "Check" {
                    cell.amountViewHightConstraint.constant = 350
                    
                    cell.amountView.isHidden = false
                    cell.checkView.isHidden = false
                    cell.licenceView.isHidden = false
                    cell.buttonsView.isHidden = false
                    
                }else {
                    cell.amountViewHightConstraint.constant = 0
                }
                
                //MARK: Hide Unhide View For Card
//                if cell.lblPaymentType.text == "Credit Card" {
//
//                    if "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")".count > 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != nil && isPestPacIntegartion()
//                    {
//                        cell.viewForCardType.isHidden = false
//                        cell.cardTypeHeightConstraint.constant = 50
//                        cell.lblCardType.text = self.showCreditCardDetail()
//                    }
//                    else
//                    {
//                        cell.viewForCardType.isHidden = true
//                        cell.cardTypeHeightConstraint.constant = 0
//                    }
//
//
//                }
//                else
//                {
//                    cell.viewForCardType.isHidden = true
//                    cell.cardTypeHeightConstraint.constant = 0
//                }
                
                if strPaymentMode != ""
                {
                    if "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")".count > 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != nil && isPestPacIntegartion()
                    {
                        cell.viewForCardType.isHidden = false
                        cell.cardTypeHeightConstraint.constant = 50
                        cell.lblCardType.text = self.showCreditCardDetail()
                    }
                    else
                    {
                        cell.viewForCardType.isHidden = true
                        cell.cardTypeHeightConstraint.constant = 0
                    }
                }
                else
                {
                    cell.viewForCardType.isHidden = true
                    cell.cardTypeHeightConstraint.constant = 0
                }

            
                if self.strText == "true" || self.strText == "1" {
                    cell.swText.isOn = true
                }else{
                    cell.swText.isOn = false
                }
                
                if self.strPhone == "true" || self.strPhone == "1" {
                    cell.swPhone.isOn = true
                }else {
                    cell.swPhone.isOn = false
                }
                
                if self.strEmail == "true" || self.strEmail == "1" {
                    cell.swEmail.isOn = true
                }else {
                    cell.swEmail.isOn = false
                }
                
                //Nilind Call For Custom Payment Mode
                
                if cell.lblPaymentType.text != "Cash" && cell.lblPaymentType.text != "Collect at time of Scheduling" && cell.lblPaymentType.text != "Credit Card" && cell.lblPaymentType.text != "Check" && cell.lblPaymentType.text != "Invoice" && cell.lblPaymentType.text != "Auto Charge Customer"  {
                    
                    print(dictSelectedCustomPaymentMode)
                    
                    if "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1" || "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                    {
                        cell.amountView.isHidden = false
                        cell.amountViewHightConstraint.constant = 65
                    }
                    else
                    {
                        cell.amountViewHightConstraint.constant = 0
                        cell.amountView.isHidden = true
                    }
                    print(strPaymentMode)
                }
                
                if shouldShowElectronicFormLink {
                    if isAuthFormFilled {
                        //Available
                        cell.btnEleTAuth.isHidden = false
                        cell.lblEleAuthStatus.textColor = UIColor.init(_colorLiteralRed: 0.0/155.0, green: 51.0/155.0, blue: 25.0/155.0, alpha: 1.0)
                        cell.lblEleAuthStatus.text = "(Signed Form Available On Account)"
                    }else {
                        //Not Available
                        cell.btnEleTAuth.isHidden = false
                        cell.lblEleAuthStatus.textColor = .red
                        cell.lblEleAuthStatus.text = "(Signed Form Not Available On Account)"
                    }
                }else {
                    //Button Hide
                    cell.btnEleTAuth.isHidden = true
                    cell.lblEleAuthStatus.isHidden = true
                }
                
                if getOpportunityStatus() {
                    cell.btnPayment.isUserInteractionEnabled = false
                    cell.btnExpDate.isUserInteractionEnabled = false
                    cell.btnPayment.isUserInteractionEnabled = false
                    //cell.btnFrontI.isUserInteractionEnabled = false
                    //cell.btnBackI.isUserInteractionEnabled = false
                    cell.swText.isUserInteractionEnabled = false
                    cell.swPhone.isUserInteractionEnabled = false
                    cell.swEmail.isUserInteractionEnabled = false
                    cell.txtAmount.isEnabled = false
                    cell.txtCheckNo.isEnabled = false
                    cell.txtLicenceNo.isEnabled = false
                }

                return cell
            }else if(indexPath.section == 6 + count) {
                //Notes
                let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell", for: indexPath as IndexPath) as! NotesTableViewCell
                cell.lblTitle.text = arrNotes[indexPath.row]
                cell.delegate = self
                if indexPath.row == 0 {
                    cell.lblSubTitle.text = self.strCustomerNote
                    
                    if getOpportunityStatus() {
                        cell.btnEdit.isUserInteractionEnabled = false
                    }
                    else
                    {
                        cell.btnEdit.isUserInteractionEnabled = true
                    }
                    
                    
                }else if indexPath.row == 1 {
                    cell.lblSubTitle.text = self.strProposalNote
                    
                    if getOpportunityStatus() {
                        cell.btnEdit.isUserInteractionEnabled = false
                    }
                    else
                    {
                        cell.btnEdit.isUserInteractionEnabled = true
                    }
                    
                }else {
                    cell.lblSubTitle.text = self.strInternalNote
                    
                    cell.btnEdit.isUserInteractionEnabled = true
                }
                
                return cell
            }else if(indexPath.section == 7 + count) {
                //Months
                let cell = tableView.dequeueReusableCell(withIdentifier: "MonthCell", for: indexPath as IndexPath) as! MonthOfServiceTableViewCell
                cell.lblMonths.text = self.strMonthOfService
                return cell
            }else if(indexPath.section == 8 + count) {
                //Agreement
                let cell = tableView.dequeueReusableCell(withIdentifier: "AgreementCell", for: indexPath as IndexPath) as! AgreementTableViewCell
                
                
                cell.lblDescription.text = self.strAgreementCheckList
                return cell
            }
            else{
                //Signature
                let cell = tableView.dequeueReusableCell(withIdentifier: "SignatureCell", for: indexPath as IndexPath) as! SignatureTableViewCell
                cell.delegate = self
                
                if isCustomerNotPresent {
                    cell.btnCustomerNot.isSelected = true
                    cell.custSigHeight.constant =  0
                    cell.tandC_height.constant =  80

                    cell.lblCustSigTitle.isHidden = true
                }
                if cell.btnCustomerNot.isSelected {
                    cell.btnAllowCustomer.isHidden = false
                    cell.lblAllowCustomer.isHidden = false
                    
                    
                    //Nilind
                    
                    if strIsAllowCustomer == "true" || strIsAllowCustomer == "True"
                    {
                        cell.btnAllowCustomer.isSelected = true
                    }
                    else
                    {
                        cell.btnAllowCustomer.isSelected = false
                    }
                    
                    //End
                    
                    
                }else {
                    cell.btnAllowCustomer.isHidden = true
                    cell.lblAllowCustomer.isHidden = true
                }
                
//                self.strCustomerSignature
//                self.strSalesSignature)
                //Set Signature if available
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                let strEmployeeSignature = "\(dictLoginData.value(forKeyPath: "EmployeeSignature")!)"
                if nsud.bool(forKey: "isPreSetSignSales") && strEmployeeSignature.count > 0 && self.strSalesSignature.count == 0
                {
                    if  let urlString = strEmployeeSignature.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)  {
                        
                        self.imageFromServerURL(url: urlString)
                        /*let imgTech: UIImageView = UIImageView()
                        imgTech.kf.setImage(with: url)
                        self.techSig = imgTech.image
                        
                        print("I1: ", self.techSig)
                        if self.techSig == nil {
                            if isReloadOnce == false {
                                isReloadOnce = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                                    self.tblView.reloadData()
                                }
                            }
                        }*/
                       
//                        if isRefreshInspector
//                        {
//                            self.tblView.reloadData()
//                            //isRefreshInspector = false
//                        }
                        cell.btnTechSig.setImage(self.techSig, for: .normal)
                        
                        
                    }
                }else {
                    DispatchQueue.main.async() {
                        if self.strSalesSignature != "" {
                            
                            self.strSalesSignature = self.strSalesSignature.replacingOccurrences(of: "\\", with: "/")
                            let salesSig = (self.strSalesSignature as NSString).lastPathComponent
                            let salesSig1 = "\(self.strSalesssUrlMainServiceAutomation)/Documents//Signature/\(salesSig)"
                            var url: URL?
                            //let preSetSig = (strEmployeeSignature as NSString).lastPathComponent
                            let newString = strEmployeeSignature.replacingOccurrences(of: "\\", with: "/")
                            let preSetSig = (newString as NSString).lastPathComponent
                            var strUrl = ""
                            if salesSig == preSetSig {
                                if  let urlString = strEmployeeSignature.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url1 = URL(string: urlString)  {
                                    strUrl = urlString
                                    //url = url1
                                }
                            }else{
                                strUrl = salesSig1

                               // url = URL(string: salesSig1)
                            }
                            print(url)
                            
                            
//                            let strImagePath = self.strSalesSignature
//
//                            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
//
//                            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
//
//                            if isImageExists!  {
//
//                                self.techSig = image
//                            }
//
//                            else
//                            {
//                                self.imageFromServerURL(url: strUrl)
//                            }
                            
                            self.imageFromServerURL(url: strUrl)

                            
                           /* let imgTech: UIImageView = UIImageView()
                            imgTech.kf.setImage(with: url)

                            if imgTech.image != nil {
                                self.techSig = imgTech.image
                            }else {
                                let imagePath = self.getImage(strImageName: self.strSalesSignature)
                                if imagePath != "" {
                                    self.techSig = UIImage(contentsOfFile: imagePath)
                                }
                            }*/
                            
                        }
//                        print("IMAGE: ", self.techSig)
//                        if self.techSig == nil {
//                            if self.isReloadOnce == false {
//                                self.isReloadOnce = true
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
//                                    self.tblView.reloadData()
//                                }
//                            }
//                        }
                        cell.btnTechSig.setImage(self.techSig, for: .normal)
                    }
                }
                
                DispatchQueue.main.async() {
                    if self.strCustomerSignature != "" {
                        //let custSig = (self.strCustomerSignature as NSString).lastPathComponent
                        let custSig = ((self.strCustomerSignature.replacingOccurrences(of: "\\", with: "/")) as NSString).lastPathComponent
                        let custSig1 = "\(self.strSalesssUrlMainServiceAutomation)/Documents//Signature/\(custSig)"
                        self.imageCustomerFromServerURL(url: custSig1)

                        
                        
//                        let strImagePath = self.strCustomerSignature
//
//                        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
//
//                        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
//
//                        if isImageExists!  {
//
//                            self.customerSig = image
//                        }
//
//                        else
//                        {
//
//                        }
                        
                        
                        /*let url = URL(string: custSig1)
                         let imgCust: UIImageView = UIImageView()
                         imgCust.kf.setImage(with: url)
                         
                         if imgCust.image != nil {
                         self.customerSig = imgCust.image
                         }else {
                         let imagePath = self.getImage(strImageName: self.strCustomerSignature)
                         if imagePath != "" {
                         self.customerSig = UIImage(contentsOfFile: imagePath)
                         }
                         }*/
                        
                    }
                    cell.btnCustomerSig.setImage(self.customerSig, for: .normal)
                }
                
                
                if getOpportunityStatus() {
                    cell.btnIAgree.isSelected = true
                    self.isIAgreeTAndC = true
                    cell.btnIAgree.isUserInteractionEnabled = false
                    cell.btnCustomerNot.isUserInteractionEnabled = false
                    cell.btnTechSig.isUserInteractionEnabled = false
                    cell.btnCustomerSig.isUserInteractionEnabled = false
                }

                cell.btnTechSig.isUserInteractionEnabled = true
                
                
                return cell
            }
        }
        
    }
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        if isSendProposal {
            return 0
        }else {
            if section == 0 {
                return 0
            }else {
                if self.isBundleService {
                    if section == 3 {
                        return 0
                    }
                }
                if self.arrSectionTitle[section] == "Months of Service" && self.strMonthOfService == "" {
                    return 0
                }
                if self.arrSectionTitle[section] == "Agreement Checklist" && self.strAgreementCheckList == "" {
                    return 0
                }
                return 30
            }
        }
    }
    

    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var position = 10.0
        if isSendProposal {
            position = 0.0
        }
        
        let headerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: self.tblView.frame.width,
                                              height: 30))
        headerView.backgroundColor = .white
        
        let bgView = UIView(frame: CGRect(x: position, y: 0, width: self.tblView.frame.width - (position * 2), height: 30))
        bgView.backgroundColor = .appLightGray
        
        let label = UILabel(frame: CGRect(x: (position * 2), y: 0, width: self.tblView.frame.width - (position * 4), height: 30))
        label.textAlignment = .left
        label.text = self.arrSectionTitle[section]
        label.textColor = .appThemeColor
        
        bgView.addSubview(label)
        headerView.addSubview(bgView)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if isSendProposal {
                return UITableView.automaticDimension
            }else{
                return 170
            }
        }else {
            if self.arrSectionTitle[indexPath.section] == "Months of Service" && self.strMonthOfService == "" {
                return 0
            }
            if self.arrSectionTitle[indexPath.section] == "Agreement Checklist" && self.strAgreementCheckList == "" {
                return 0
            }
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return self.tableView(tableView, heightForRowAt: indexPath)
        }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
        
    func imageFromServerURL(url: String){
        let currentUrl = url
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let task = session.dataTask(with: NSURL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
            if error == nil {
                DispatchQueue.global(qos: .background).async {
                    
                    if let downloadedImage = UIImage(data: data!) {
                        if (url == currentUrl) {//Only cache and set the image view when the downloaded image is the one from last request
                            //                                imageCache.setObject(downloadedImage, forKey: url)
                            //                                self.image = downloadedImage
                            self.techSig = downloadedImage
//                            if self.techSig == nil {
                            if self.isReloadOnce == false {
                                self.isReloadOnce = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                                    self.tblView.reloadData()
                                }
                            }
                            if self.isRefreshInspector == true {
                                self.isRefreshInspector = false
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                                    self.tblView.reloadData()
                                }
                            }

                            
                        }
                    }
                }
            }
            else {
                print(error)
            }
        })
        task.resume()
        
    }
    
    func imageCustomerFromServerURL(url: String){
        let currentUrl = url
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let task = session.dataTask(with: NSURL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
            if error == nil {
                DispatchQueue.global(qos: .background).async {
                    
                    if let downloadedImage = UIImage(data: data!) {
                        if (url == currentUrl) {//Only cache and set the image view when the downloaded image is the one from last request
                            
                            self.customerSig = downloadedImage

                            if self.isReloadOnceCustomer == false {
                                self.isReloadOnceCustomer = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                                    self.tblView.reloadData()
                                }
                            }
                        }
                    }
                }
            }
            else {
                print(error)
            }
        })
        task.resume()
        
    }
    
    
    // MARK: -----------CustomerInfo Action---------
    // MARK:
    @objc func actionViewBtnEmail(sender : UIButton) {
        self.view.endEditing(true)
        sendMail()
    }
    
    @objc func actionViewBtnCall(sender : UIButton) {
        self.view.endEditing(true)
        makeCall()
    }
    
    @objc func actionViewBtnAddress(sender : UIButton) {
        self.view.endEditing(true)
        let strAddress = Global().strCombinedAddressService(for: matchesGeneralInfo) ?? ""

        if strAddress.count > 0
        {
            
            let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
            // Create the actions
            
            let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                //[global redirectOnAppleMap:self :_txtViewAddress_ServiceAddress.text]
                Global().redirect(onAppleMap: self, strAddress)
                
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
        }
    }
    
    func makeCall()
    {
        let arrNoToCall = NSMutableArray()
        
        let strCell = "\(self.matchesGeneralInfo.value(forKey: "servicePrimaryPhone") ?? "")"
//        if "\(self.matchesGeneralInfo.value(forKey: "Cell") ?? "")".count > 0
//        {
//            arrNoToCall.add("\(self.matchesGeneralInfo.value(forKey: "Cell") ?? "")")
//        }
//        if "\(self.matchesGeneralInfo.value(forKey: "PrimaryPhone") ?? "")".count > 0
//        {
//            arrNoToCall.add("\(self.matchesGeneralInfo.value(forKey: "PrimaryPhone") ?? "")")
//        }
//        if "\(self.matchesGeneralInfo.value(forKey: "SecondaryPhone") ?? "")".count > 0
//        {
//            arrNoToCall.add("\(self.matchesGeneralInfo.value(forKey: "SecondaryPhone") ?? "")")
//        }
        
        
        if strCell.count > 0
            
        {
            
            let alert = UIAlertController(title: "", message: "Make your call on selection", preferredStyle: .actionSheet)
               alert.view.tintColor = UIColor.black
//            for item in arrNoToCall
//            {
                let strNo = strCell
                
                alert.addAction(UIAlertAction(title: "\(strNo)", style: .default , handler:{ (UIAlertAction)in
                    
                    
                    if (strNo.count > 0)
                    {
                        Global().calling(strNo)
                    }
                    
                }))
                
                
//            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No contacts detail found", viewcontrol: self)
            
        }
    }
    func sendMail()
    {
        let arrEmail = NSMutableArray()
        
        if "\(self.matchesGeneralInfo.value(forKey: "primaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(self.matchesGeneralInfo.value(forKey: "primaryEmail") ?? "")")
        }
        if "\(self.matchesGeneralInfo.value(forKey: "secondaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(self.matchesGeneralInfo.value(forKey: "secondaryEmail") ?? "")")
        }
        
        if arrEmail.count > 0
            
        {
            let alert = UIAlertController(title: "", message: "Make your email on selection", preferredStyle: .actionSheet)
               alert.view.tintColor = UIColor.black
            for item in arrEmail
            {
                let strEmail = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strEmail)", style: .default , handler:{ (UIAlertAction)in
                    
                    if (strEmail.count > 0)
                    {
                        //Global().emailComposer(strEmail, "", "", self)
                        self.sendEmail(strEmail: strEmail)
                    }
                    
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No email found", viewcontrol: self)
            
        }
    }
    
    // MARK: -  ------------------------------ Send Mail Delegate ------------------------------
    
    func sendEmail(strEmail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(strEmail)"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

// MARK: ----------SalesReviewCell Delegate Cell---------
// MARK:
extension SalesReviewViewController: SalesReviewCellDelegate {
    
    func callBackAddCard(cell: PaymentTableViewCell) {
        //MARK: - Add Card Action
        print("Add Card")
        self.goToPestPac_PaymentIntegration()
    }
    
    func callBackRefreshInspecotrSignature(cell: SignatureTableViewCell) {
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Internet connection is reqiured to refresh Inspector signature", viewcontrol: self)
        }
        else
        {
            let strSignUrl: String = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
            let isPreSetSign: Bool = nsud.bool(forKey: "isPreSetSignSales")

            if isPreSetSign && strSignUrl != ""
            {
                self.objPaymentInfo?.setValue("", forKey: "salesSignature")
                self.strSalesSignature = ""
                let context = getContext()
                //save the object
                do { try context.save()
                    print("Record Saved Updated.")
                } catch let error as NSError {
                    debugPrint("Could not save. \(error), \(error.userInfo)")
                }
                
                isRefreshInspector = true
                tblView.reloadData()
            }
            else
            {
                self.isEditedInSalesAuto = true
                let defaults = UserDefaults.standard
                defaults.set("fromInspector", forKey: "signatureType")
                defaults.synchronize()
                
                self.isCustomer = true
                if DeviceType.IS_IPAD {
                    let vc = UIStoryboard.init(name: "SalesMainiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewControlleriPad") as? SignViewControlleriPad
                    vc?.strType = "inspector"
                    self.present(vc!, animated: true)
                }else {
                    let vc = UIStoryboard.init(name: "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewController") as? SignViewController
                    vc?.strType = "inspector"
                    self.present(vc!, animated: true)
                }
            }
           
        }

    }

    
    func textFieldEditingChanged(cell: PaymentTableViewCell, value: String) {
        self.isEditedInSalesAuto = true
        self.strAmount = value
    }
    
    func textFieldEditingChangedCheck(cell: PaymentTableViewCell, value: String) {
        self.isEditedInSalesAuto = true
        self.strChequeNo = value
    }
    
    func textFieldEditingChangedLicence(cell: PaymentTableViewCell, value: String) {
        self.isEditedInSalesAuto = true
        self.strLicenseNo = value
    }
    
    func callBackEditNotesMethod(cell: NotesTableViewCell) {
        self.isEditedInSalesAuto = true
        
        let indexPath = tblView.indexPath(for: cell)
        
        var title = ""
        var message = ""
        if indexPath?.row == 0 {
            if isSendProposal {
                title = NotesForConfigure.proposal.rawValue
                message = strProposalNote
            }else {
                title = NotesForConfigure.customer.rawValue
                message = strCustomerNote
            }
        }else if indexPath?.row == 1 {
            title = NotesForConfigure.proposal.rawValue
            message = strProposalNote
        }else {
            title = NotesForConfigure.internalN.rawValue
            message = strInternalNote
        }
        
        
        //Add Observer
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.notesValue),
            name: Commons.kNotificationNotes,
            object: nil)
        
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "FormBuilder" : "FormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNoteViewController") as? SalesNoteViewController
        vc?.strTitle = title
        vc?.strMessage = message
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true)
        
    }
    
    
    func callBackPaymentMethod(cell: PaymentTableViewCell) {
        print("Payment")
        self.isEditedInSalesAuto = true
        //AddLeadProspect
//        let arrPayment = [["AddLeadProspect": "Cash"], ["AddLeadProspect": "Check"], ["AddLeadProspect": "Credit Card"], ["AddLeadProspect": "Auto Charge Customer"], ["AddLeadProspect": "Collect at time of Scheduling"], ["AddLeadProspect": "Invoice"]]
//        openTableViewPopUp(tag: 3335, ary: (arrPayment as NSArray).mutableCopy() as! NSMutableArray)
        
        openTableViewPopUp(tag: 3335, ary: getPaymentModeFromMaster())

    }
    
    func callBackExpDate(cell: PaymentTableViewCell) {
        self.isEditedInSalesAuto = true
        print("ExpDate")
        
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
       // let fromDate = formatter.date(from:((tf_Date.text?.count)! > 0 ? tf_Date.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: Date(),Picker: datePicker, view: self.view ,strTitle : "Select Service Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            self.strDate = formatter.string(from: datePicker.date)
            print(self.strDate)
            cell.btnExpDate.setTitle("Expiration Date: \(self.strDate)", for: .normal)

        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func callBackFronImage(cell: PaymentTableViewCell, sender: Any) {
        self.isEditedInSalesAuto = true
        print("FronImage")
        self.isFrontImage = true
        let alert = UIAlertController(title: "Make your selection", message: nil, preferredStyle: .actionSheet)
        
            alert.addAction(UIAlertAction(title: "Preview Image", style: .default, handler: { _ in
                self.openPreviewImage()
            }))
        
            alert.addAction(UIAlertAction(title: "Capture New", style: .default, handler: { _ in
                if self.arrOFImagesName.count > 0 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Check front image already exist.", viewcontrol: self)

                }else {
                    self.openCamera(sourceDevice: .front)
                }
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                if self.arrOFImagesName.count > 0 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Check front image already exist.", viewcontrol: self)

                }else {
                    self.openGallary()
                }
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as! UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    
    func callBackBackImage(cell: PaymentTableViewCell, sender: Any) {
        self.isEditedInSalesAuto = true
        print("BackImage")
        self.isFrontImage = false

        let alert = UIAlertController(title: "Make your selection", message: nil, preferredStyle: .actionSheet)
        
            alert.addAction(UIAlertAction(title: "Preview Image", style: .default, handler: { _ in
                self.openPreviewImage()
            }))
        
            alert.addAction(UIAlertAction(title: "Capture New", style: .default, handler: { _ in
                if self.arrOfCheckBackImage.count > 0 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Check back image already exist.", viewcontrol: self)

                }else {
                    self.openCamera(sourceDevice: .rear)
                }
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                if self.arrOfCheckBackImage.count > 0 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Check back image already exist.", viewcontrol: self)

                }else {
                    self.openGallary()
                }
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as! UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    func callBackEletronicAuth(cell: PaymentTableViewCell)
    {
        self.isEditedInSalesAuto = true
        
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMasters.value(forKey: "ElectronicAuthorizationFormMaster") is NSDictionary)
        {
            if DeviceType.IS_IPAD {
                
                let storyboard = UIStoryboard.init(name: "ElectronicAuthorization_iPad", bundle: nil)
                
                let testController = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPad") as! ElectronicAuthorization_iPad
 
                if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
                {
                    if(isInternetAvailable() == false)
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(testController, animated: false)
                    }
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
                
            }else {
                
                let storyboard = UIStoryboard.init(name: "ElectronicAuthorization_iPhone", bundle: nil)
                let testController = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPhone") as! ElectronicAuthorization_iPhone
            
                if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
                {
                    if(isInternetAvailable() == false)
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(testController, animated: false)
                    }
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
        }
        
    }


    
    func callBackEletronicAuthOld(cell: PaymentTableViewCell)
    {
        self.isEditedInSalesAuto = true
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMasters.value(forKey: "ElectronicAuthorizationFormMaster") is NSDictionary)
        {
            
//            let storyboardIpad = UIStoryboard.init(name: "ElectronicAuthorization_iPhone", bundle: nil)
//            let testController = storyboardIpad.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPhone") as! ElectronicAuthorization
            let storyboard = UIStoryboard(name: "ElectronicAuthorization", bundle: nil)
            guard let testController = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorizationViewController") as? ElectronicAuthorizationViewController else { return }
//            destination.delegate = self
//            destination.isEditable = isEditable
//            destination.formDetail = electronicAuthorizationForm
//            destination.propertyDetails = propertyDetails
//            destination.entityDetail = entityDetail
//            self.navigationController?.pushViewController(destination, animated: true)
            // testController.strLeadId = strLeadId
            
            if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
            {
                if(isInternetAvailable() == false)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
            }
            else
            {
                self.navigationController?.pushViewController(testController, animated: false)
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
        }
    }
    
    //.......................
    func callBackIAgree(cell: SignatureTableViewCell) {
        self.isEditedInSalesAuto = true
        cell.btnIAgree.isSelected = !cell.btnIAgree.isSelected
        
        self.isIAgreeTAndC = cell.btnIAgree.isSelected
    }
    
    func callBackCustomerNotPresent(cell: SignatureTableViewCell) {
        self.isEditedInSalesAuto = true
        let indexPath1 = tblView.indexPath(for: cell)
        cell.btnIAgree.isSelected = false
        self.isIAgreeTAndC = false
        
        cell.btnCustomerNot.isSelected = !cell.btnCustomerNot.isSelected
        self.isCustomerNotPresent = cell.btnCustomerNot.isSelected

        if cell.btnCustomerNot.isSelected {
            cell.custSigHeight.constant =  0
            cell.tandC_height.constant =  80

            cell.lblCustSigTitle.isHidden = true
            cell.btnAllowCustomer.isHidden = false
            cell.lblAllowCustomer.isHidden = false

        }else {
            cell.tandC_height.constant =  120
            cell.custSigHeight.constant =  250
            cell.lblCustSigTitle.isHidden = false
            cell.btnAllowCustomer.isHidden = true
            cell.lblAllowCustomer.isHidden = true
        }
        
        
        if isCustomerNotPresent {
            matchesGeneralInfo.setValue("true", forKey: "isCustomerNotPresent")
        }else {
            matchesGeneralInfo.setValue("false", forKey: "isCustomerNotPresent")
        }


        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        manageAllowCustomerToSelection()
        
       // let indexP = IndexPath(row: Int(indexPath1?.row ?? 0), section: Int(indexPath1?.section ?? 0))
        self.tblView.reloadData()//reloadRows(at: [indexP], with: .none)
    }
    
    func callBackAllowCustomer(cell: SignatureTableViewCell) {

        self.isEditedInSalesAuto = true
        //let indexPath = tblView.indexPath(for: cell)
        cell.btnAllowCustomer.isSelected = !cell.btnAllowCustomer.isSelected

        if cell.btnAllowCustomer.isSelected {
            self.strIsAllowCustomer = "true"
        }else {
            self.strIsAllowCustomer = "false"
        }
    }
    
    func callBackTermAndCondition(cell: SignatureTableViewCell) {
//        arrStandardService
//        arrCustomService
//        arrBundleService
        self.isEditedInSalesAuto = true
        let arrSoldStandardService = self.arrStandardService + self.arrBundleService
        print(arrSoldStandardService)
        var arrSoldServiceNameNew: [String] = []
        var arrSoldTerms: [String] = []

        if arrSoldStandardService.count > 0 {
            for obj in arrSoldStandardService {
                print(obj.value(forKey: "serviceSysName"))
                let sysName: String = obj.value(forKey: "serviceSysName") as! String
                let soldTerms: String = obj.value(forKey: "serviceTermsConditions") as! String
                arrSoldServiceNameNew.append(sysName)
                arrSoldTerms.append(soldTerms)
            }
        }
        print(arrSoldTerms)
        
        var arrNonStanServiceName: [String] = []
        var arrNonStandardTermsCondition: [String] = []

        if arrCustomService.count > 0 {
            for obj in arrCustomService {
                
                let sysName: String = obj.value(forKey: "serviceName") as! String
                let soldTerms: String = obj.value(forKey: "nonStdServiceTermsConditions") as! String
                arrNonStanServiceName.append(sysName)
                arrNonStandardTermsCondition.append(soldTerms)
            }
        }
        print(arrNonStandardTermsCondition)
        
        
        let vc = UIStoryboard.init(name: "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesTermsConditionVC") as? SalesTermsConditionVC
        vc?.strFrom = "Residential"
        vc?.arrGeneralTermsConditionName = NSMutableArray(array: self.arrTerms)
        vc?.arrStandardServiceTermsConditionName = NSMutableArray(array: arrSoldServiceNameNew)
        vc?.arrStandardServiceTermsConditionDesc = NSMutableArray(array: arrSoldTerms)
        vc?.arrNonStandardServiceTermsConditionName = NSMutableArray(array: arrNonStanServiceName)
        vc?.arrNonStandardServiceTermsConditionDesc = NSMutableArray(array: arrNonStandardTermsCondition)
        vc?.modalPresentationStyle = .fullScreen
        
        self.present(vc!, animated: true)
    }
    
    func callBackCustomerSignature(cell: SignatureTableViewCell) {
        self.isEditedInSalesAuto = true
        let defaults = UserDefaults.standard
        defaults.set("fromCustomer", forKey: "signatureType")
        defaults.synchronize()
        //SignViewControlleriPad
        self.isCustomer = true
        
        if DeviceType.IS_IPAD {
            let vc = UIStoryboard.init(name: "SalesMainiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewControlleriPad") as? SignViewControlleriPad
            vc?.strType = "customer"
            self.present(vc!, animated: true)
        }else {
            let vc = UIStoryboard.init(name: "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewController") as? SignViewController
            vc?.strType = "customer"
            self.present(vc!, animated: true)
        }
        
    }
    
    func callBackTechnationSignature(cell: SignatureTableViewCell) {
        self.isEditedInSalesAuto = true
        let defaults = UserDefaults.standard
        defaults.set("fromInspector", forKey: "signatureType")
        defaults.synchronize()
        
        self.isCustomer = true
        if DeviceType.IS_IPAD {
            let vc = UIStoryboard.init(name: "SalesMainiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewControlleriPad") as? SignViewControlleriPad
            vc?.strType = "inspector"
            self.present(vc!, animated: true)
        }else {
            let vc = UIStoryboard.init(name: "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewController") as? SignViewController
            vc?.strType = "inspector"
            self.present(vc!, animated: true)
        }
//        let vc = UIStoryboard.init(name: "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: DeviceType.IS_IPAD ? "SignViewControlleriPad" : "SignViewController") as? SignViewController
//        vc?.strType = "inspector"
//        self.present(vc!, animated: true)
    }
    
    func callBackTextSW(cell: PaymentTableViewCell, value: Bool) {
        if value {
            strText = "true"
        }else {
            strText = "false"
        }
    }
    
    func callBackPhoneSW(cell: PaymentTableViewCell, value: Bool) {
        if value {
            strPhone = "true"
        }else {
            strPhone = "false"
        }
    }
    
    func callBackEmailSW(cell: PaymentTableViewCell, value: Bool) {
        if value {
            strEmail = "true"
        }else {
            strEmail = "false"
        }
    }
    
    
    @objc private func signatureValue(notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: Commons.kNotificationSignature, object: nil)
        print(notification.object)
        
        if self.isCustomer {
            self.customerSig = (notification.object as! UIImage)
        }else {
            self.techSig = (notification.object as! UIImage)
        }
        
        tblView.reloadData()
    }
    
    func openPreviewImage()
    {
        if self.isFrontImage {
            if self.arrOFImagesName.count == 0 {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No Image Available.", viewcontrol: self)
            }else {
                self.goToImagePreviewCheckFrontImage()
            }
        }else {
            if self.arrOfCheckBackImage.count == 0 {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No Image Available.", viewcontrol: self)
            }else {
                self.goToImagePreviewCheckBackImage()
            }
        }
    }
    
    func openCamera(sourceDevice: UIImagePickerController.CameraDevice)
    {
        if getOpportunityStatus() {
        }
        else
        {
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                let imgPicker = UIImagePickerController()
                imgPicker.allowsEditing = true
                imgPicker.delegate = self
                imgPicker.sourceType = .camera
                imgPicker.cameraDevice = sourceDevice
                present(imgPicker, animated: true)
            }else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Camera is not available.", viewcontrol: self)
            }
        }
        
    }

    func openGallary()
    {
        if getOpportunityStatus() {
        }
        else
        {
            let imgPicker = UIImagePickerController()
            imgPicker.allowsEditing = true
            imgPicker.delegate = self
            imgPicker.sourceType = .photoLibrary
            present(imgPicker, animated: true)
        }
    }
    
    
    func goToImagePreviewCheckFrontImage() {
        let defs = UserDefaults.standard
        defs.set(true, forKey: "forCheckFrontImageDelete")
        defs.set(false, forKey: "forCheckBackImageDelete")
        defs.synchronize()

        let mainStoryboard = UIStoryboard(
            name: "SalesMain",
            bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ImagePreviewSalesAuto") as? ImagePreviewSalesAuto
        objByProductVC?.arrOfImages = ((self.arrOFImagesName as NSArray).mutableCopy() as! NSMutableArray)//(self.arrOFImagesName as! NSMutableArray)
        objByProductVC?.indexOfImage = "0"
        
        if strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame
        {
            objByProductVC?.statusOfWorkOrder = "Complete"
        }
        else
        {
            objByProductVC?.statusOfWorkOrder = "InComplete"
        }
        
        objByProductVC?.strForCheckImage = "CheckImages"
        if let objByProductVC = objByProductVC {
            navigationController?.present(objByProductVC, animated: true)
        }
    }
    
    func goToImagePreviewCheckBackImage() {
        let defs = UserDefaults.standard
        defs.set(true, forKey: "forCheckBackImageDelete")
        defs.set(false, forKey: "forCheckFrontImageDelete")
        defs.synchronize()

        let mainStoryboard = UIStoryboard(
            name: "SalesMain",
            bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ImagePreviewSalesAuto") as? ImagePreviewSalesAuto
        objByProductVC?.arrOfImages = ((self.arrOfCheckBackImage as NSArray).mutableCopy() as! NSMutableArray)//arrOfCheckBackImage
        objByProductVC?.indexOfImage = "0"
        if strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame
        {
            objByProductVC?.statusOfWorkOrder = "Complete"
        }
        else
        {
            objByProductVC?.statusOfWorkOrder = "InComplete"
        }
        objByProductVC?.strForCheckImage = "CheckImages"

        if let objByProductVC = objByProductVC {
            navigationController?.present(objByProductVC, animated: true)
        }
    }
}

//MARK:- Camera Delegate
//MARK:-
extension SalesReviewViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        
        let dateFormatterD = DateFormatter()
        dateFormatterD.dateFormat = "MMddyyyy"
        let strDate = dateFormatterD.string(from: Date())
        
        let dateFormatterT = DateFormatter()
        dateFormatterT.dateFormat = "HHmmss"
        let strTime = dateFormatterT.string(from: Date())

        let strImageNamess = "CheckImg\(strDate)\(strTime).jpg"
        
        
        if self.isFrontImage {
           self.checkFrontImg = self.resizeImage(image: image, imgName: strImageNamess)
            self.arrOFImagesName.append(strImageNamess)
        }else {
            self.checkBackImg = self.resizeImage(image: image, imgName: strImageNamess)
            self.arrOfCheckBackImage.append(strImageNamess)
        }
        // print out the image size as a test
        print(image.size)
    }
    
    func resizeImage(image: UIImage, imgName: String) -> UIImage {
        
        if let imageData = image.jpeg(.lowest) {
            let filename = getDocumentsDirectory().appendingPathComponent(imgName)
            do {
                try imageData.write(to: filename, options: [])
            } catch {
                // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
            }
            
            return UIImage.init(data: imageData)!
        }else {
            return UIImage()
        }
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }

    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

// MARK: - Selection CustomTableView
// MARK: -

extension SalesReviewViewController: CustomTableView {
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if dictData["AddLeadProspect"] != nil
        {
            self.selectecPaymentType = dictData.value(forKey: "AddLeadProspect") as! String
            print(self.selectecPaymentType)
            if self.selectecPaymentType == "Cash" {
                self.strPaymentMode = "Cash"
            }else if self.selectecPaymentType == "Check" {
                self.strPaymentMode = "Check"
            }else if self.selectecPaymentType == "Credit Card" {
                self.strPaymentMode = "CreditCard"
            }else if self.selectecPaymentType == "Auto Charge Customer" {
                self.strPaymentMode = "AutoChargeCustomer"
            }else if self.selectecPaymentType == "Collect at time of Scheduling" {
                self.strPaymentMode = "CollectattimeofScheduling"
            }else if self.selectecPaymentType == "Invoice" {
                self.strPaymentMode = "Invoice"
            }else {
                self.strPaymentMode = ""
            }
        }
        else
        {
            print("\(dictData)")
            dictSelectedCustomPaymentMode = dictData
            self.selectecPaymentType = dictData.value(forKey: "Title") as! String
            self.strPaymentMode = "\(dictData.value(forKey: "SysName") ?? "")"
        }
        /*self.selectecPaymentType = dictData.value(forKey: "AddLeadProspect") as! String
        print(self.selectecPaymentType)
        if self.selectecPaymentType == "Cash" {
            self.strPaymentMode = "Cash"
        }else if self.selectecPaymentType == "Check" {
            self.strPaymentMode = "Check"
        }else if self.selectecPaymentType == "Credit Card" {
            self.strPaymentMode = "CreditCard"
        }else if self.selectecPaymentType == "Auto Charge Customer" {
            self.strPaymentMode = "AutoChargeCustomer"
        }else if self.selectecPaymentType == "Collect at time of Scheduling" {
            self.strPaymentMode = "CollectattimeofScheduling"
        }else if self.selectecPaymentType == "Invoice" {
            self.strPaymentMode = "Invoice"
        }else {
            self.strPaymentMode = ""
        }*/
        //strPaymentMode
        
        self.objPaymentInfo.setValue(self.strPaymentMode, forKey: "paymentMode")
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.tblView.reloadData()
    }
    
    func didSelect(aryData: NSMutableArray, tag: Int) {
        print(aryData, tag)
    }
    
    func fetchRenewalServiceFromCoreData(soldServiceId: String) -> String {
        print(soldServiceId)
        var strRenewalDesc = ""
        
        let arrAllObjRenewal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && serviceId = %@", strLeadId, soldServiceId))
        
        if arrAllObjRenewal.count > 0 {
            let matchesRenewal: NSManagedObject = arrAllObjRenewal.object(at: 0) as! NSManagedObject
            print(matchesRenewal)
            var strDesc = "\(matchesRenewal.value(forKey: "renewalDescription") ?? "")"
            if strDesc != "" {
                strDesc = strDesc + ":"
            }
            //let strAmount = "\(matchesRenewal.value(forKey: "renewalAmount") ?? "")"
            
            let strAmount =  "\(convertDoubleToCurrency(amount: Double("\(matchesRenewal.value(forKey: "renewalAmount") ?? "")") ?? 0.0))"
            
            let dict = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesRenewal.value(forKey: "renewalFrequencySysName") ?? "")")
            let strFrequency = dict.value(forKey: "FrequencyName") ?? ""
            strRenewalDesc = "\(strDesc) \(strAmount) (\(strFrequency))"
        }
        
        return strRenewalDesc
    }
}
