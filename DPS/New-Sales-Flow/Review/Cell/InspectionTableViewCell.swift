//
//  InspectionTableViewCell.swift
//  DPS
//
//  Created by APPLE on 28/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol SalesReviewCellDelegate : class {
    //PaymentTableViewCell Method
    func callBackPaymentMethod(cell: PaymentTableViewCell)
    func callBackExpDate(cell: PaymentTableViewCell)
    func callBackFronImage(cell: PaymentTableViewCell, sender: Any)
    func callBackBackImage(cell: PaymentTableViewCell, sender: Any)
    func callBackEletronicAuth(cell: PaymentTableViewCell)
    func textFieldEditingChanged(cell: PaymentTableViewCell, value: String)
    func textFieldEditingChangedCheck(cell: PaymentTableViewCell, value: String)
    func textFieldEditingChangedLicence(cell: PaymentTableViewCell, value: String)
    
    func callBackTextSW(cell: PaymentTableViewCell, value: Bool)
    func callBackPhoneSW(cell: PaymentTableViewCell, value: Bool)
    func callBackEmailSW(cell: PaymentTableViewCell, value: Bool)
    func callBackAddCard(cell: PaymentTableViewCell)


    //ConfigureNotesTableViewCell Method
    func callBackEditNotesMethod(cell: NotesTableViewCell)
    
    //SignatureTableViewCell Method
    func callBackCustomerSignature(cell: SignatureTableViewCell)
    func callBackTechnationSignature(cell: SignatureTableViewCell)
    func callBackIAgree(cell: SignatureTableViewCell)
    func callBackCustomerNotPresent(cell: SignatureTableViewCell)
    func callBackAllowCustomer(cell: SignatureTableViewCell)
    func callBackTermAndCondition(cell: SignatureTableViewCell)
    func callBackRefreshInspecotrSignature(cell: SignatureTableViewCell)
}

// MARK: 0.----------SalesNewGeneralInfo Cell---------
// MARK:
class SalesReviewGeneralInfo : UITableViewCell {
    
    //------Customer Info-----
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lbl_Type: UILabel!
    
    @IBOutlet weak var btn_CustomerName: UIButton!
    @IBOutlet weak var btn_CompanyName: UIButton!
    @IBOutlet weak var btn_Email: UIButton!
    @IBOutlet weak var btn_Mobile: UIButton!
    @IBOutlet weak var btn_Address: UIButton!
    @IBOutlet weak var lbl_Address: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func cellConfigure(objLeadDetails : NSManagedObject) {
        print(objLeadDetails)//customerName
        
        //self.img_Profile.downloaded(from: "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")")
        if "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")" != ""
        {
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")")
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")")
            
            if isImageExists!  {
                
                self.img_Profile.image = image
                
            }else {
                
                if let nameurl = URL(string: "\(URL.baseUrl)/Documents/CustomerAddressImages/" + "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")")
                {
                    self.img_Profile.sd_setImage(with: nameurl, placeholderImage: UIImage(named: "no_image.jpg"), completed: nil)
                }
                else
                {
                    self.img_Profile.image = UIImage(named: "no_image.jpg")!
                }
            }
        }
        //self.btn_CustomerName.setTitle("\(objLeadDetails.value(forKey: "customerName") ?? "")", for: .normal)
        
        let strName = "\(Global().strFullName(fromCoreDB: objLeadDetails) ?? "")"
        self.btn_CustomerName.setTitle("\(strName)", for: .normal)
        self.btn_CompanyName.setTitle("\(objLeadDetails.value(forKey: "companyName") ?? "")", for: .normal)
        self.btn_Email.setTitle("\(objLeadDetails.value(forKey: "primaryEmail") ?? "")", for: .normal)
        let strPrimaryPhone =  "\(objLeadDetails.value(forKey: "primaryPhone") ?? "")"
        let strSecondaryPhone =  "\(objLeadDetails.value(forKey: "secondaryPhone") ?? "")"
        let strCellNo =  "\(objLeadDetails.value(forKey: "cellNo") ?? "")"

        if strCellNo != ""{
                 self.btn_Mobile.setTitle("\( formattedNumber(number: strCellNo))", for: .normal)
        }
        else{
            if strPrimaryPhone != ""{
                self.btn_Mobile.setTitle("\( formattedNumber(number: strPrimaryPhone))", for: .normal)
            }
//            else if strSecondaryPhone != ""{
//                self.btn_Mobile.setTitle("\( formattedNumber(number: strSecondaryPhone))", for: .normal)
//            }
            else{
                self.btn_Mobile.setTitle("\( formattedNumber(number: ""))", for: .normal)
            }
        }

        // self.btn_Address.setTitle("\(objLeadDetails.value(forKey: "servicesAddress1")!)", for: .normal)
        
        let strAddress = Global().strCombinedAddressService(for: objLeadDetails)
        self.lbl_Address.text = strAddress
    }
}


//MARK:- 1. InspectionTableViewCell
//MARK:-
class InspectionTableViewCell: UITableViewCell {
    
    @objc var matchesGeneralInfo = NSManagedObject()
    @IBOutlet weak var heightDynemicView: NSLayoutConstraint!
    @IBOutlet weak var viewDynemicView: UIView!
    var aryDynemicForm = NSMutableArray()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:-----------------------------
    //MARK: - -----------loadDynamicForm  OR SAVE
    func loadDynamicForm() {
        
        // Fetch From Core Data if Data Exists
        
        let arrayDynamicInspection = getDataFromLocal(strEntity: "SalesDynamicInspection", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , "\(Global().getUserName()!)"))
        
        if(arrayDynamicInspection.count > 0)
        {
            let objSalesInspectionData = arrayDynamicInspection[0] as! NSManagedObject
            if objSalesInspectionData.value(forKey: "arrFinalInspection") is NSArray {
                var aryDynemicFormTemp = NSMutableArray()
                aryDynemicFormTemp = (objSalesInspectionData.value(forKey: "arrFinalInspection") as! NSArray).mutableCopy()as! NSMutableArray
                
                var isSameFlowType = true
                
                for (index, element) in aryDynemicFormTemp.enumerated() {
                    
                    //  print("Item \(index): \(element)")
                    var dict = NSMutableDictionary()
                    dict = (((element as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                    dict.setValue(false, forKey: "viewExpand")
                    dict.setValue("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", forKey: "LeadId")
                    aryDynemicFormTemp.replaceObject(at: index, with: dict)
                    
                    let flowTypeMaster = "\(dict.value(forKey: "FlowType") ?? "")"
                    
                    let flowTypeOpportunity = "\(matchesGeneralInfo.value(forKey: "flowType") ?? "")"
                    
                    if (flowTypeMaster != flowTypeOpportunity) && !(flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame) {
                        
                        isSameFlowType = false
                        
                    }
                    
                }
                
                // Change For To check if commercial or residentail flow saved and which flow type is of lead if it is changed then need to delete saved dynamic form and re enter new dynamic form.
                
                if isSameFlowType {
                    var arrOfFilteredDataTemp = [NSDictionary] ()
                    arrOfFilteredDataTemp = aryDynemicFormTemp as! [NSDictionary]
                    let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
                        (($0 )["SequenceNo"] as? CGFloat)! > (($1 )["SequenceNo"] as? CGFloat)!
                    })
                    self.aryDynemicForm = NSMutableArray()
                    self.aryDynemicForm = (sortedArray as NSArray).mutableCopy()as! NSMutableArray
                    CreatDynemicView(aryData: aryDynemicForm)
                    
                } else {
                    
                    // Get Dynamic Form From Masters and create Dynamic View
                    
                    self.fetchDynamicFormFromMasterAndCreateDynamicView()
                    
                }
                
                
            }
        }else{
            
            //  If Data not exist then check Status and Stage and then fetch From Masters
            
            if "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")".caseInsensitiveCompare("Complete") == .orderedSame && "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")".caseInsensitiveCompare("Won") == .orderedSame{
                
                // Create blank No Dynamic Form Exist
                
            }else{
                
                // Get Dynamic Form From Masters and create Dynamic View
                
                //self.fetchDynamicFormFromMasterAndCreateDynamicView()
                
            }
        }
    }
    
    //MARK: --------------------------
    //MARK: - CreatDynemicView
    func CreatDynemicView(aryData : NSMutableArray) {
        
        
        
        for v in viewDynemicView.subviews{
            v.removeFromSuperview()
        }
        var yAxis = 0.0 , extraSpace = DeviceType.IS_IPAD ? 20.0 : 12.0
        let height = DeviceType.IS_IPAD ? 55.0 : 40.0
        let heighttextView = DeviceType.IS_IPAD ? 135.0 : 100.0
        
        if(aryData.count != 0){
            for (headerIndex, item) in aryData.enumerated() {
                print("headerIndex---------------\(headerIndex)")
                
                var dictMAIN = NSMutableDictionary()
                dictMAIN = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                var isHeader = false
                
                
                //MARK:------------Start Section View ---------
                //                if dictMAIN.value(forKey: "viewExpand") as! Bool {
                let arySECTION = dictMAIN.value(forKey: "Sections")as! NSArray
                for (sectionIndex, item) in arySECTION.enumerated() {
                    print("sectionIndex---------------\(sectionIndex)")
                    
                    
                    let dictSectionData = (item as AnyObject) as! NSDictionary
                    
                    var isSection = false
                    //MARK:-Section Control
                    let arySECTIONControls = dictSectionData.value(forKey: "Controls")as! NSArray
                    for (controlIndex, item) in arySECTIONControls.enumerated() {
                        let dictSectionControl = (item as AnyObject) as! NSDictionary
                        print("COntrolINDEX---------------\(controlIndex)")
                        
                        //Check Value exist or not
                        if "\(dictSectionControl.value(forKey: "Value") ?? "")" != "" {
                            print("VVVVVVVVVVAAAA: \(dictSectionControl.value(forKey: "Value") ?? "")")
                            
                            if isHeader == false {
                                isHeader = true
                                //MARK:-------------Start Header View ---------
                                let viewHeader = UIView.init(frame: CGRect(x: 10, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 20, height: Int(height)))
                                viewHeader.backgroundColor = SalesNewColorCode.darkBlack
                                let lblHeaderTitle = UILabel.init(frame: CGRect(x: 40, y: 4, width: Int(self.viewDynemicView.frame.width) - 80 , height: Int(height) - 8))
                                lblHeaderTitle.textAlignment = .center
                                lblHeaderTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                
                                var strTitle = "\(dictMAIN.value(forKey: "FormName") ?? "")"
                                
                                if strTitle.count == 0 || strTitle == ""
                                {
                                    strTitle = "\(dictMAIN.value(forKey: "DepartmentName") ?? "")"
                                }
                                
                                lblHeaderTitle.text = strTitle//"\(dictMAIN.value(forKey: "DepartmentName")!)"
                                lblHeaderTitle.numberOfLines = 2
                                lblHeaderTitle.textColor = UIColor.white
                                viewHeader.addSubview(lblHeaderTitle)
                                self.viewDynemicView.addSubview(viewHeader)
                                yAxis = yAxis + height + extraSpace
                                //MARK:------------End Header View ---------
                            }
                            
                            if isSection == false {
                                isSection = true
                                //MARK:-------------Start Section View ---------
                                let title =  "\(dictSectionData.value(forKey: "Name")!)"
                                let viewSection = UIView.init(frame: CGRect(x: 10, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 20, height: Int(height)))
                                viewSection.backgroundColor = .lightGray
                                let lblSectionTitle = UILabel()
                                lblSectionTitle.frame = CGRect(x: 40, y: 4, width: Int(self.viewDynemicView.frame.width) - 80 , height: Int(height) - 8)
                                
                                lblSectionTitle.textAlignment = .center
                                lblSectionTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionTitle.text = title
                                lblSectionTitle.numberOfLines = 1
                                lblSectionTitle.textColor = UIColor.white
                                viewSection.addSubview(lblSectionTitle)
                                self.viewDynemicView.addSubview(viewSection)
                                yAxis = yAxis + height + extraSpace
                                //MARK:------------End Section View ---------
                            }
                            
                            //MARK:--- -Checkbox ||
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.checkbox) {
                                
                            }
                            
                            //MARK:-------- Checkbox_combo
                           if ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.checkbox_combo){
                                
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                let values = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",")as NSArray
                                                                print(values)
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                for (subControlIndex, item) in aryOption.enumerated() {
                                    
                                    let optionDict = (item as AnyObject) as! NSDictionary
                                    let strLabel = "\(optionDict.value(forKey: "Label") ?? "")"
                                    let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                    
                                    if(values.contains("\(strValue)")){
                                        
                                        let lblTitle = UILabel()
                                        lblTitle.text = "\(strLabel)"
                                        lblTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                        lblTitle.numberOfLines = 0
                                        lblTitle.textColor = UIColor.darkGray
                                        
                                        var lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (self.viewDynemicView.frame.width), font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                        if(lbltitleHeight < 35.0){
                                            lbltitleHeight = (DeviceType.IS_IPAD ? 40.0 : 30.0)
                                        }
                                        lblTitle.frame = CGRect(x: Int(20 + 10), y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(lbltitleHeight))
                                        self.viewDynemicView.addSubview(lblTitle)
                                        yAxis = yAxis + lbltitleHeight + extraSpace
                                        
                                    }else{
                                        
                                    }
                                }
                            }
                            
                            //MARK:---------Radio || Radio_combo
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.radio) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.radio_combo){
                                
                                
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let values = "\(dictSectionControl.value(forKey: "Value")!)"
                                
                                for (subControlIndex, item) in aryOption.enumerated() {
                                    let optionDict = (item as AnyObject) as! NSDictionary
                                    let strLabel = "\(optionDict.value(forKey: "Label") ?? "")"
                                    let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                    
                                    //                                        let btnRadioBox = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0), height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
                                    if(values.lowercased() == strValue.lowercased()){
                              
                                        let lblTitle = UILabel()
                                        lblTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                        lblTitle.text = "\(strLabel)"
                                        lblTitle.numberOfLines = 0
                                        lblTitle.textColor = UIColor.darkGray
                                        
                                        var lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (viewDynemicView.frame.width - 40), font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                        if(lbltitleHeight < 35.0){
                                            lbltitleHeight = (DeviceType.IS_IPAD ? 40.0 : 30.0)
                                        }
                                        
                                        lblTitle.frame =  CGRect(x: Int(30), y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(lbltitleHeight))
                                        
                                        self.viewDynemicView.addSubview(lblTitle)
                                        yAxis = yAxis + lbltitleHeight + extraSpace
                                    }else{
                                    }
                                    
                                }
                                
                            }
                            
                            //MARK:---------TextView || Paragraph
                            //el-textarea
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.textarea) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.paragraph) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.parag)
                            {
                                //                                    if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = Value
                                let heightCalculatelblValue = Value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis - 10), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                lblValue.numberOfLines = 0
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                /* }else{
                                 let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                 let lblSectionDetailTitle = UILabel()
                                 lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 lblSectionDetailTitle.text = title
                                 let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                 lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                 
                                 lblSectionDetailTitle.numberOfLines = 0
                                 lblSectionDetailTitle.textColor = UIColor.darkGray
                                 self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                 yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                 
                                 let textarea = UITextView.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(heighttextView)))
                                 textarea.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 textarea.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                 textarea.textColor = UIColor.darkGray
                                 textarea.layer.cornerRadius = 8.0
                                 textarea.layer.borderWidth = 1.0
                                 textarea.layer.borderColor = UIColor.lightGray.cgColor
                                 //textarea.delegate = self
                                 
                                 textarea.tag = headerIndex*1000+sectionIndex
                                 textarea.placeholderLabel.tag = controlIndex*1000+0
                                 
                                 self.viewDynemicView.addSubview(textarea)
                                 yAxis = yAxis + heighttextView + extraSpace
                                 }*/
                            }
                            
                            //MARK:---------UITextField
                            //el-textbox,el-email,el-phone,el-currency,el-decimal,el-number,el-auto-num,el-precent,el-upload
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.textbox || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.email || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.phone || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.currency || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.decimal || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.number || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.autonum || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.precent ){
                                
                                //                                    if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = Value
                                let heightCalculatelblValue = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18: 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                //                                    }else{
                                //                                        let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                //                                        textarea?.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                //                                        textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                //                                        textarea!.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                //                                        textarea!.textColor = UIColor.darkGray
                                //                                        textarea!.lineColor = UIColor.lightGray
                                //                                        //textarea!.delegate = self
                                //
                                //                                        textarea!.tag = headerIndex*1000+sectionIndex
                                //                                        let lftview = UILabel()
                                //                                        lftview.tag = controlIndex*1000+0
                                //                                        textarea!.leftView = lftview
                                //
                                //                                        switch "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() {
                                //                                        case DynemicFormCheck.email:
                                //                                            textarea!.keyboardType = .emailAddress
                                //                                        case DynemicFormCheck.phone:
                                //                                            textarea!.text = formattedNumber(number: textarea!.text!)
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        case DynemicFormCheck.currency:
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        case DynemicFormCheck.decimal:
                                //                                            textarea!.keyboardType = .decimalPad
                                //                                        case DynemicFormCheck.number:
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        case DynemicFormCheck.autonum:
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        case DynemicFormCheck.precent:
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        default:
                                //                                            textarea!.keyboardType = .default
                                //                                        }
                                //                                        self.viewDynemicView.addSubview(textarea!)
                                //                                        yAxis = yAxis + height + extraSpace
                                //                                    }
                                
                                
                            }
                            
                            //MARK:---------Datetime
                            //el-date , el-date-time
                            
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date_time){
                                
                                //     if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = Value
                                let heightCalculatelblValue = Value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                
                                lblValue.numberOfLines = 0
                                lblValue.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                /* }else{
                                 let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 textarea!.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                 textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                 textarea!.textColor = UIColor.darkGray
                                 textarea!.lineColor = UIColor.lightGray
                                 textarea!.isUserInteractionEnabled = false
                                 self.viewDynemicView.addSubview(textarea!)
                                 let btnDate = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 self.viewDynemicView.addSubview(btnDate)
                                 btnDate.setImage(UIImage(named: "Calendar"), for: .normal)
                                 btnDate.contentHorizontalAlignment = .right
                                 btnDate.imageView?.contentMode = .scaleAspectFit
                                 
                                 btnDate.tag = headerIndex*1000+sectionIndex
                                 btnDate.titleLabel?.tag = controlIndex*1000+0
                                 
                                 if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date){
                                 //                                        btnDate.addTarget(self, action: #selector(didTap_btnDate(sender:)), for: .touchUpInside)
                                 }else{
                                 //                                        btnDate.addTarget(self, action: #selector(didTap_btnDateTime(sender:)), for: .touchUpInside)
                                 }
                                 yAxis = yAxis + height + extraSpace
                                 
                                 }*/
                            }
                            //MARK:---------URL
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.url){
                                
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                
                                let btnUrl = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                btnUrl.titleLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                //                                btnUrl.addTarget(self, action: #selector(didTap_btnURL(sender:)), for: .touchUpInside)
                                
                                btnUrl.tag = headerIndex*1000+sectionIndex
                                btnUrl.titleLabel?.tag = controlIndex*1000+0
                                
                                //                                let attributeString = NSMutableAttributedString(
                                //                                    string: "\(dictSectionControl.value(forKey: "Value")!)",
                                //                                    attributes: yourAttributes
                                //                                )
                                //                                btnUrl.setAttributedTitle(attributeString, for: .normal)
                                self.viewDynemicView.addSubview(btnUrl)
                                yAxis = yAxis + 25.0 + extraSpace
                                
                            }
                            //MARK:---------Dropdown
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.dropdown){
                                //                                    if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                let aryTempSlectedOption = NSMutableArray()
                                
                                for (_, item1) in aryValues.enumerated() {
                                    let strOptionvalue = "\(item1 as AnyObject)"
                                    for (_, item) in aryOption.enumerated() {
                                        let optionDict = (item as AnyObject) as! NSDictionary
                                        let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                        if(strOptionvalue.lowercased() == strValue.lowercased()){
                                            aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                        }
                                    }
                                }
                                
                                let value = aryTempSlectedOption.componentsJoined(by: ",").replacingOccurrences(of: ",", with: "\n")
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = value
                                let heightCalculatelblValue = value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                lblValue.numberOfLines = 0
                                lblValue.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                /*  }else{
                                 let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                 textarea!.textColor = UIColor.darkGray
                                 textarea!.lineColor = UIColor.lightGray
                                 textarea!.isUserInteractionEnabled = false
                                 let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                 let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                 let aryTempSlectedOption = NSMutableArray()
                                 
                                 for (_, item1) in aryValues.enumerated() {
                                 let strOptionvalue = "\(item1 as AnyObject)"
                                 for (_, item) in aryOption.enumerated() {
                                 let optionDict = (item as AnyObject) as! NSDictionary
                                 let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                 if(strOptionvalue.lowercased() == strValue.lowercased()){
                                 aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                 }
                                 }
                                 
                                 }
                                 
                                 textarea?.text = aryTempSlectedOption.componentsJoined(by: ",")
                                 self.viewDynemicView.addSubview(textarea!)
                                 let btn = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 btn.setImage(UIImage(named: "arrow_2"), for: .normal)
                                 btn.contentHorizontalAlignment = .right
                                 btn.imageView?.contentMode = .scaleAspectFit
                                 btn.tag = headerIndex*1000+sectionIndex
                                 btn.titleLabel?.tag = controlIndex*1000+0
                                 //                                btn.addTarget(self, action: #selector(didTap_btnDropDown(sender:)), for: .touchUpInside)
                                 self.viewDynemicView.addSubview(btn)
                                 yAxis = yAxis + height + extraSpace
                                 }*/
                            }
                            //MARK:---------Multi_select
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.multi_select){
                                
                                //                                    if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                let aryTempSlectedOption = NSMutableArray()
                                
                                for (_, item1) in aryValues.enumerated() {
                                    let strOptionvalue = "\(item1 as AnyObject)"
                                    for (_, item) in aryOption.enumerated() {
                                        let optionDict = (item as AnyObject) as! NSDictionary
                                        let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                        if(strOptionvalue.lowercased() == strValue.lowercased()){
                                            aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                        }
                                    }
                                }
                                
                                let value = aryTempSlectedOption.componentsJoined(by: ",").replacingOccurrences(of: ",", with: "\n")
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = value
                                let heightCalculatelblValue = value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                lblValue.numberOfLines = 0
                                lblValue.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                
                                /* }else{
                                 
                                 let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                 textarea!.textColor = UIColor.darkGray
                                 textarea!.lineColor = UIColor.lightGray
                                 textarea!.isUserInteractionEnabled = false
                                 
                                 let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                 let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                 let aryTempSlectedOption = NSMutableArray()
                                 
                                 for (_, item1) in aryValues.enumerated() {
                                 let strOptionvalue = "\(item1 as AnyObject)"
                                 for (_, item) in aryOption.enumerated() {
                                 let optionDict = (item as AnyObject) as! NSDictionary
                                 let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                 if(strOptionvalue.lowercased() == strValue.lowercased()){
                                 aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                 }
                                 }
                                 
                                 }
                                 
                                 textarea?.text = aryTempSlectedOption.componentsJoined(by: ",")
                                 
                                 self.viewDynemicView.addSubview(textarea!)
                                 let btn = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 btn.setImage(UIImage(named: "Next2"), for: .normal)
                                 btn.contentHorizontalAlignment = .right
                                 btn.imageView?.contentMode = .scaleAspectFit
                                 btn.tag = headerIndex*1000+sectionIndex
                                 btn.titleLabel?.tag = controlIndex*1000+0
                                 //                                btn.addTarget(self, action: #selector(didTap_btnDropDownMultiSelection(sender:)), for: .touchUpInside)
                                 
                                 self.viewDynemicView.addSubview(btn)
                                 
                                 yAxis = yAxis + height + extraSpace
                                 }*/
                            }
                        }
                    }
                }
                //                }
                
                //MARK:------------End Section View ---------
            }
        }
        heightDynemicView.constant = CGFloat(yAxis + 50)
    }
    
    func fetchDynamicFormFromMasterAndCreateDynamicView() {
        
        // Get Dynamic Form From Masters
        
        let arrTempInspection = NSMutableArray()
        
        if let arrayMaster = nsud.value(forKey: "MasterSalesAutomationDynamicForm") as? NSArray {
            
            if arrayMaster.isKind(of: NSArray.self) {
                
                for item in arrayMaster
                {
                    let dictDataL = item as! NSDictionary
                    
                    let branchSysNameMaster = "\(dictDataL.value(forKey: "BranchSysName") ?? "")"
                    let flowTypeMaster = "\(dictDataL.value(forKey: "FlowType") ?? "")"
                    
                    
                    let branchSysNameOpprtunity = "\(matchesGeneralInfo.value(forKey: "branchSysName") ?? "")"
                    let flowTypeOpportunity = "\(matchesGeneralInfo.value(forKey: "flowType") ?? "")"
                    
                    
                    if flowTypeOpportunity.caseInsensitiveCompare("Commercial") == .orderedSame {
                        
                        if (branchSysNameMaster ==  branchSysNameOpprtunity) && ( flowTypeMaster.caseInsensitiveCompare("Commercial") == .orderedSame ||  flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame ){
                            
                            // Add All Duynamic Forms Of Commmercail FLow.
                            arrTempInspection.add(dictDataL)
                            
                        }
                        
                    }else{
                        
                        if (branchSysNameMaster ==  branchSysNameOpprtunity) && ( flowTypeMaster.caseInsensitiveCompare("Residential") == .orderedSame ||  flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame ){
                            
                            // Add All Duynamic Forms Of Commmercail FLow.
                            arrTempInspection.add(dictDataL)
                            
                        }
                        
                    }
                    
                    
                }
            }
        }
        
        if arrTempInspection.count > 0 {
            
            // If found data in Master will create Dynamic View.
            
            let aryDynemicFormTemp = NSMutableArray()
            
            aryDynemicFormTemp.addObjects(from: arrTempInspection as! [Any])
            
            for (index, element) in aryDynemicFormTemp.enumerated() {
                //  print("Item \(index): \(element)")
                var dict = NSMutableDictionary()
                dict = (((element as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                dict.setValue(false, forKey: "viewExpand")
                dict.setValue("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", forKey: "LeadId")
                aryDynemicFormTemp.replaceObject(at: index, with: dict)
            }
            
            // call Func to crate dynamic Form.
            
            var arrOfFilteredDataTemp = [NSDictionary] ()
            arrOfFilteredDataTemp = aryDynemicFormTemp as! [NSDictionary]
            let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
                (($0 )["SequenceNo"] as? CGFloat)! > (($1 )["SequenceNo"] as? CGFloat)!
            })
            self.aryDynemicForm = NSMutableArray()
            self.aryDynemicForm = (sortedArray as NSArray).mutableCopy()as! NSMutableArray
            CreatDynemicView(aryData: aryDynemicForm)
            
            
        }else{
            
            // No Data Found in Master So blank Screen No Dynamic View We can show alert if required.
            
            
        }
    }
    
    func getStatusCompleteWon() -> Bool {
        let strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
        let strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won"){
            return true
        }else{
            return false
        }
    }
}

//MARK:- 2. ServiceTableViewCell
//MARK:-
class ServiceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerBg: UIView!
    @IBOutlet weak var headerConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTimePeriod: UILabel!
    @IBOutlet weak var lblBilled: UILabel!
    
    @IBOutlet weak var lblOneTimePriceValue: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblInitialTitle: UILabel!
    //    @IBOutlet weak var lblDiscountPercentValue: UILabel!
    //    @IBOutlet weak var lblFinalOneTimePriceValue: UILabel!
    @IBOutlet weak var lblMaintenancePriceValue: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var photoCollection: UICollectionView!
    
    @IBOutlet weak var maintPriceHConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblRenewalDecription: UILabel!
    
    let inset: CGFloat = 0
    let minimumLineSpacing: CGFloat = 0
    let minimumInteritemSpacing: CGFloat = 0
    
    var dictLoginData = NSDictionary()
    var chkFreqConfiguration = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setStandardServiceValues(objCustomServiceDetail : NSManagedObject)
    {
        
        var isUnitBased = false
        var isParameterBased = false
        
        let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "serviceSysName") ?? "")")
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")")

        
        if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
        {
            isUnitBased = true
        }
        else
        {
            isUnitBased = false
        }
        
        if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
        {
            isParameterBased = true
        }
        else
        {
            isParameterBased = false
        }
        
        
        self.lblTitle.text = "\(dictService.value(forKey: "Name") ?? "")"
        self.lblTimePeriod.text = "\(objCustomServiceDetail.value(forKey: "serviceFrequency") ?? "")"
        
        let billed: Double = Double("\(objCustomServiceDetail.value(forKey: "billingFrequencyPrice") ?? "0")") ?? 0.0
        self.lblBilled.text = String(format: "$%.2f", billed)
        
        //self.lblDescription.text = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        
        let strDesc = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        
        if strDesc.count == 0
        {
            self.lblDescription.text = ""
        }
        else
        {
            self.lblDescription.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)
        }
        
        
        
        
        let initial: Double = Double("\(objCustomServiceDetail.value(forKey: "initialPrice") ?? "0")") ?? 0.0
        var sumFinalInitialPrice = 0.0, sumFinalMaintPrice = 0.0

        //For Parameter based service
        if isParameterBased
        {
            let arrPara = objCustomServiceDetail.value(forKey: "additionalParameterPriceDcs") as! NSArray
            
            if arrPara.count > 0
            {
                for i in 0..<arrPara.count
                {
                    let dict = arrPara.object(at: i) as! NSDictionary
                    let arrKey = dict.allKeys as NSArray
                    if arrKey.contains("FinalInitialUnitPrice")
                    {
                        sumFinalInitialPrice = sumFinalInitialPrice + Double(("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).floatValue)
                    }
                    if arrKey.contains("FinalMaintUnitPrice")
                    {
                        sumFinalMaintPrice = sumFinalMaintPrice + Double(("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).floatValue)
                    }
                }
            }
        }
        
        var unit = 1.0
        if isUnitBased
        {
            unit = Double(("\(objCustomServiceDetail.value(forKey: "unit") ?? "")" as NSString).floatValue)
        }
        
        
        var finalPrice = ((initial * unit) + sumFinalInitialPrice)
        if finalPrice < 0
        {
            finalPrice = 0
        }
        self.lblOneTimePriceValue.text = String(format: "$%.2f", finalPrice)
        
        var discount: Double = Double(objCustomServiceDetail.value(forKey: "discount") as! String) ?? 0.0
        if discount < 0 {
            discount = 0
        }
        
        self.lblDiscountValue.text = String(format: "$%.2f", discount)
        
        //        let discountPercent: Double = Double(objCustomServiceDetail.value(forKey: "discountPercentage") as! String) ?? 0.0
        //        self.lblDiscountPercentValue.text = String(format: "%.2f", discountPercent)
        //
        //        let finalPrice = initial - discount
        //        self.lblFinalOneTimePriceValue.text = String(format: "$%.2f", finalPrice)
        
        var maint: Double = (Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0)//((Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0 * unit) + sumFinalMaintPrice)
        if maint < 0
        {
            maint = 0
        }
        self.lblMaintenancePriceValue.text = String(format: "$%.2f", maint)
        
        //        if objCustomServiceDetail.value(forKey: "isSold") as! String == "true" {
        //            btnIsAgreement.isSelected = true
        //        }else {
        //            btnIsAgreement.isSelected = false
        //        }
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = Double(finalPrice)
        totalMaintPrice = Double(maint)

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        maintPriceHConstraint.constant = 25
        if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            lblInitialTitle.text = "One Time Price"
            maintPriceHConstraint.constant = 0
            lblMaintenancePriceValue.isHidden = true
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBilled.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
    }
    
    func setCustomServiceValues(objCustomServiceDetail : NSManagedObject)
    {
        
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")")
        
        
        self.lblTitle.text = "\(objCustomServiceDetail.value(forKey: "serviceName") ?? "")"
        self.lblTimePeriod.text = "\(objCustomServiceDetail.value(forKey: "serviceFrequency") ?? "")"
        
        let billed: Double = Double(objCustomServiceDetail.value(forKey: "billingFrequencyPrice") as! String) ?? 0.0
        self.lblBilled.text = String(format: "$%.2f", billed)
        
        //self.lblDescription.text = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        
        let strDesc = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        
        if strDesc.count == 0
        {
            self.lblDescription.text = ""
        }
        else
        {
//            let data = Data("\(strDesc)".utf8)
//
//            if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//            {
//                self.lblDescription.attributedText = attributedString
//            }
            
            self.lblDescription.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)

        }
        
        let initial: Double = Double(objCustomServiceDetail.value(forKey: "initialPrice") as! String) ?? 0.0
        self.lblOneTimePriceValue.text = String(format: "$%.2f", initial)
        
        var discount: Double = Double(objCustomServiceDetail.value(forKey: "discount") as! String) ?? 0.0
        if discount < 0 {
            discount = 0
        }
        self.lblDiscountValue.text = String(format: "$%.2f", discount)
        
        let discountPercent: Double = Double(objCustomServiceDetail.value(forKey: "discountPercentage") as! String) ?? 0.0
        //        self.lblDiscountPercentValue.text = String(format: "%.2f", discountPercent)
        //
        //        let finalPrice = initial - discount
        //        self.lblFinalOneTimePriceValue.text = String(format: "$%.2f", finalPrice)
        
        let maint: Double = Double(objCustomServiceDetail.value(forKey: "maintenancePrice") as! String) ?? 0.0
        self.lblMaintenancePriceValue.text = String(format: "$%.2f", maint)
        
        //        if objCustomServiceDetail.value(forKey: "isSold") as! String == "true" {
        //            btnIsAgreement.isSelected = true
        //        }else {
        //            btnIsAgreement.isSelected = false
        //        }
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = ("\(objCustomServiceDetail.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue
        totalMaintPrice = ("\(objCustomServiceDetail.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        maintPriceHConstraint.constant = 25
        if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            lblInitialTitle.text = "One Time Price"
            maintPriceHConstraint.constant = 0
            lblMaintenancePriceValue.isHidden = true
            
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBilled.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
    }
    
    func setBundleServiceValues(objBundleServiceDetail : NSManagedObject)
    {
        
        var isUnitBased = false
        var isParameterBased = false
        
        let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objBundleServiceDetail.value(forKey: "serviceSysName") ?? "")")
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objBundleServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objBundleServiceDetail.value(forKey: "frequencySysName") ?? "")")

        
        if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
        {
            isUnitBased = true
        }
        else
        {
            isUnitBased = false
        }
        
        if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
        {
            isParameterBased = true
        }
        else
        {
            isParameterBased = false
        }
        
        
        self.lblTitle.text = "\(objBundleServiceDetail.value(forKey: "serviceSysName") ?? "")"
        self.lblTimePeriod.text = "\(objBundleServiceDetail.value(forKey: "serviceFrequency") ?? "")"
        
        let billed: Double = Double(objBundleServiceDetail.value(forKey: "billingFrequencyPrice") as! String) ?? 0.0
        self.lblBilled.text = String(format: "$%.2f", billed)
        
        self.lblDescription.text = "\(objBundleServiceDetail.value(forKey: "serviceDescription") ?? "")"
        //        if self.lblDescription.text == "" {
        //            self.topSpaceDesc.constant = 0
        //            self.bottomSpaceDesc.constant = 0
        //        }
        
        let initial: Double = Double(objBundleServiceDetail.value(forKey: "initialPrice") as! String) ?? 0.0
        var sumFinalInitialPrice = 0.0, sumFinalMaintPrice = 0.0

        //For Parameter based service
        if isParameterBased
        {
            let arrPara = objBundleServiceDetail.value(forKey: "additionalParameterPriceDcs") as! NSArray
            
            if arrPara.count > 0
            {
                for i in 0..<arrPara.count
                {
                    let dict = arrPara.object(at: i) as! NSDictionary
                    let arrKey = dict.allKeys as NSArray
                    if arrKey.contains("FinalInitialUnitPrice")
                    {
                        sumFinalInitialPrice = sumFinalInitialPrice + Double(("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).floatValue)
                    }
                    if arrKey.contains("FinalMaintUnitPrice")
                    {
                        sumFinalMaintPrice = sumFinalMaintPrice + Double(("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).floatValue)
                    }
                }
            }
        }
        
        var unit = 1.0
        if isUnitBased
        {
            unit = Double(("\(objBundleServiceDetail.value(forKey: "unit") ?? "")" as NSString).floatValue)
        }
        
        
        var finalPrice = ((initial * unit) + sumFinalInitialPrice)
        if finalPrice < 0
        {
            finalPrice = 0
        }
        self.lblOneTimePriceValue.text = String(format: "$%.2f", finalPrice)
        
        var discount: Double = Double(objBundleServiceDetail.value(forKey: "discount") as! String) ?? 0.0
        if discount < 0 {
            discount = 0
        }
        self.lblDiscountValue.text = String(format: "$%.2f", discount)
        
        //        let discountPercent: Double = Double(objCustomServiceDetail.value(forKey: "discountPercentage") as! String) ?? 0.0
        //        self.lblDiscountPercentValue.text = String(format: "%.2f", discountPercent)
        //
        //        let finalPrice = initial - discount
        //        self.lblFinalOneTimePriceValue.text = String(format: "$%.2f", finalPrice)
        
        var maint: Double = ((Double(objBundleServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0 * unit) + sumFinalMaintPrice)
        if maint < 0
        {
            maint = 0
        }
        self.lblMaintenancePriceValue.text = String(format: "$%.2f", maint)
        
        //        if objCustomServiceDetail.value(forKey: "isSold") as! String == "true" {
        //            btnIsAgreement.isSelected = true
        //        }else {
        //            btnIsAgreement.isSelected = false
        //        }
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = Double(finalPrice)
        totalMaintPrice = Double(maint)

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        
        if "\(objBundleServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            lblInitialTitle.text = "One Time Price"
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(objBundleServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBilled.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
    }
    
    
    func collectionSetup(row: Int) {
        self.photoCollection.dataSource = self
        self.photoCollection.delegate = self
        self.photoCollection.reloadData()
    }
}

extension ServiceTableViewCell : UICollectionViewDelegate , UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3//arrData.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! ServiceCollectionViewCell
        cell.imgImage.downloaded(from: "https://picsum.photos/300/200")
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("SSSS")
    }
}

extension ServiceTableViewCell: UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //let width = collectionViewOutlet.frame.width//(self.view.frame.size.width ) / 5
        
        return CGSize(width: 110, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset) //.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    //    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    //
    //        let visibleIndex = Int(targetContentOffset.pointee.x / collectionViewOutlet.frame.width)
    //        print(visibleIndex)
    //       // self.pageControl.currentPage = visibleIndex
    //    }
    
}



//MARK:- 3. PricingInfoTableViewCell
//MARK:-
class PricingInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblSubTotalInitial       : UILabel!
    @IBOutlet weak var lblCouponDiscount        : UILabel!
    @IBOutlet weak var lblCredit                : UILabel!
    @IBOutlet weak var lblOtherDiscount         : UILabel!
    @IBOutlet weak var lblTotalPrice            : UILabel!
    @IBOutlet weak var lblTaxAmount             : UILabel!
    @IBOutlet weak var lblBillingAmount         : UILabel!
    @IBOutlet weak var lblCreditMaint: UILabel!
    
    
    @IBOutlet weak var lblSubTotalMaintenance   : UILabel!
    @IBOutlet weak var lblTotalPriceMaintenance : UILabel!
    @IBOutlet weak var lblTaxAmountMaintenance  : UILabel!
    @IBOutlet weak var lblTotalDueAmount        : UILabel!
    
    @IBOutlet weak var tblMaintFrqHeight: NSLayoutConstraint!
    @IBOutlet weak var tblMaintFrq: UITableView!
    var arrMaintBillingPrice  : [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func tableMaintSetUp (arrMaintBillingPrice: [String]) {
        //Credit
        if arrMaintBillingPrice.count > 0{
            self.arrMaintBillingPrice.removeAll()
            
            self.tblMaintFrq.delegate = self
            self.tblMaintFrq.dataSource = self
            self.arrMaintBillingPrice = arrMaintBillingPrice
            self.tblMaintFrq.reloadData()
            
            self.tblMaintFrqHeight.constant = CGFloat(arrMaintBillingPrice.count * 25)
        }
    }
    
    
}

//MARK:- 4. DiscountTableViewCell
//MARK:-
class DiscountTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var tblCouponHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCoupon: UITableView!
    var arrAppliedCoupon : [NSManagedObject] = []
    
    @IBOutlet weak var tblCreditHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCredit: UITableView!
    var arrAppliedCredit : [NSManagedObject] = []
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func tableSetUp (arrAppliedCoupon : [NSManagedObject]) {
        //Coupon
        if arrAppliedCoupon.count > 0{
            self.arrAppliedCoupon.removeAll()
            
            self.tblCoupon.delegate = self
            self.tblCoupon.dataSource = self
            self.arrAppliedCoupon = arrAppliedCoupon
            self.tblCoupon.reloadData()
            
            self.tblCouponHeight.constant = CGFloat(arrAppliedCoupon.count * 50)
        }
    }
    
    func tableCreditSetUp (arrAppliedCredit : [NSManagedObject]) {
        //Credit
        if arrAppliedCredit.count > 0{
            self.arrAppliedCredit.removeAll()
            
            self.tblCredit.delegate = self
            self.tblCredit.dataSource = self
            self.arrAppliedCredit = arrAppliedCredit
            self.tblCredit.reloadData()
            
            self.tblCreditHeight.constant = CGFloat(arrAppliedCredit.count * 50)
        }
    }
}


//MARK:- 5. PaymentTableViewCell
//MARK:-
class PaymentTableViewCell: UITableViewCell {
    weak var delegate : SalesReviewCellDelegate?
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTaxAmount: UILabel!
    @IBOutlet weak var lblBillingAmount: UILabel!

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtCheckNo: UITextField!
    @IBOutlet weak var txtLicenceNo: UITextField!
    @IBOutlet weak var btnExpDate: UIButton!
    
    
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var licenceView: UIView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var amountViewHightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnEleTAuth: UIButton!
    @IBOutlet weak var lblEleAuthStatus: UILabel!
    @IBOutlet weak var btnBackI: UIButton!
    @IBOutlet weak var btnFrontI: UIButton!
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var swText: UISwitch!
    @IBOutlet weak var swPhone: UISwitch!
    @IBOutlet weak var swEmail: UISwitch!
    
    @IBOutlet weak var viewForCardType: UIView!
    @IBOutlet weak var cardTypeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCardType: UILabel!
    @IBOutlet weak var btnAddCard: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtAmount.delegate = self
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func paymentMethodAction(_ sender: Any) {
        self.delegate?.callBackPaymentMethod(cell: self)
    }
    @IBAction func selectExpDateAction(_ sender: Any) {
        self.delegate?.callBackExpDate(cell: self)
    }
    @IBAction func frontImageAction(_ sender: Any) {
        self.delegate?.callBackFronImage(cell: self, sender: sender)
    }
    @IBAction func backImageAction(_ sender: Any) {
        self.delegate?.callBackBackImage(cell: self, sender: sender)
    }
    
    @IBAction func eletronicAuthAction(_ sender: Any) {
        self.delegate?.callBackEletronicAuth(cell: self)
    }
    
    @IBAction func editingChanged(_ sender: UITextField) {
        self.delegate? .textFieldEditingChanged (cell: self, value: txtAmount.text!)
    }
    @IBAction func checkEditChanged(_ sender: UITextField) {
        self.delegate? .textFieldEditingChangedCheck(cell: self, value: txtCheckNo.text!)
    }
    @IBAction func licenceEditChange(_ sender: UITextField) {
        self.delegate? .textFieldEditingChangedLicence(cell: self, value: txtLicenceNo.text!)
    }
    
    @IBAction func textAction(_ sender: UISwitch) {
        self.delegate?.callBackTextSW(cell: self, value: sender.isOn)

    }
    
    @IBAction func phoneAction(_ sender: UISwitch) {
        self.delegate?.callBackPhoneSW(cell: self, value: sender.isOn)

    }
    
    @IBAction func emailSWAction(_ sender: UISwitch) {
        self.delegate?.callBackEmailSW(cell: self, value: sender.isOn)

    }
    
    @IBAction func addCardAction(_ sender: Any) {
        self.delegate?.callBackAddCard(cell: self)
    }
    
}

//MARK:- 6. NotesTableViewCell
//MARK:-
class NotesTableViewCell: UITableViewCell {
    weak var delegate : SalesReviewCellDelegate?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func editAction(_ sender: Any) {
        self.delegate?.callBackEditNotesMethod(cell: self)
    }
}

//MARK:- 7. MonthOfServiceTableViewCell
//MARK:-
class MonthOfServiceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblMonths: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

//MARK:- 8. AgreementTableViewCell
//MARK:-
class AgreementTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

//MARK:- 9. SignatureTableViewCell
//MARK:-
class SignatureTableViewCell: UITableViewCell {
    weak var delegate : SalesReviewCellDelegate?
    @IBOutlet weak var btnCustomerSig: UIButton!
    @IBOutlet weak var btnTechSig: UIButton!
    @IBOutlet weak var btnIAgree: UIButton!
    @IBOutlet weak var btnCustomerNot: UIButton!
    @IBOutlet weak var btnAllowCustomer: UIButton!
    @IBOutlet weak var lblAllowCustomer: UILabel!
    @IBOutlet weak var lblCustSigTitle: UILabel!
    
    @IBOutlet weak var custSigHeight: NSLayoutConstraint!
    @IBOutlet weak var tandC_height: NSLayoutConstraint!
    
    @IBOutlet weak var btnRefreshInspSign: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func termsAndConditionAction(_ sender: Any) {
        self.delegate?.callBackTermAndCondition(cell: self)
    }
    
    @IBAction func agreeAction(_ sender: UIButton) {
        self.delegate?.callBackIAgree(cell: self)
    }
    
    @IBAction func customernotPresentAction(_ sender: UIButton) {
        
        self.delegate?.callBackCustomerNotPresent(cell: self)
    }
    
    @IBAction func allowCustomerAction(_ sender: UIButton) {
        self.delegate?.callBackAllowCustomer(cell: self)
    }
    
    @IBAction func customerSignatureAction(_ sender: Any) {
        self.delegate?.callBackCustomerSignature(cell: self)
    }
    
    @IBAction func technationSignatureAction(_ sender: Any) {
        self.delegate?.callBackTechnationSignature(cell: self)
    }
    
    @IBAction func refreshInspectorSignature(_ sender: Any) {
        self.delegate?.callBackRefreshInspecotrSignature(cell: self)
    }
}


//MARK:- TablView Delegate for Pricing
extension PricingInfoTableViewCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMaintBillingPrice.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Service
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponCell", for: indexPath as IndexPath) as! ConfigCouponTableViewCell
        
        cell.lblMaintPrice.text = self.arrMaintBillingPrice[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 25
    }
    
}


//MARK:- TablView Delegate for Coupon and Credit
extension DiscountTableViewCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCoupon {
            return  self.arrAppliedCoupon.count
        }else {
            return  self.arrAppliedCredit.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Service
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponCell", for: indexPath as IndexPath) as! ConfigCouponTableViewCell
        
        if tableView == tblCoupon {
            var objCoupon = NSManagedObject()
            objCoupon = self.arrAppliedCoupon[indexPath.row]
            
            
            var strCouponName = ""
            
            strCouponName = "\(objCoupon.value(forKey: "name") ?? "")"
            
            if strCouponName == ""
            {
                strCouponName = "\(objCoupon.value(forKey: "discountCode") ?? "")"
            }
            
            cell.lblCouponCode.text = strCouponName//objCoupon.value(forKey: "discountCode") as? String
            cell.lblCouponAmount.text = String(format: "$%.2f", Double("\(objCoupon.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
            cell.lblCouponDescription.text = objCoupon.value(forKey: "discountDescription") as? String
            
        }else {
            var objCredit = NSManagedObject()
            objCredit = self.arrAppliedCredit[indexPath.row]
            
            let strInitialAmount = String(format: "$%.2f", Double("\(objCredit.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
            let strMaintAmount = String(format: "$%.2f", Double("\(objCredit.value(forKey: "appliedMaintDiscount") ?? "0")") ?? 0.0)
            var strForBoth = ""
            var isBoth = false
            if "\(objCredit.value(forKey: "applicableForInitial") ?? "0")" ==  "1" || "\(objCredit.value(forKey: "applicableForInitial") ?? "0")" == "true" {
                strForBoth = strInitialAmount + " / " + "$0.00"
                isBoth = true
            }else {
                isBoth = false
            }
            if "\(objCredit.value(forKey: "applicableForMaintenance") ?? "0")" ==  "1" || "\(objCredit.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                strForBoth = "$0.00" + " / " + strMaintAmount
                isBoth = true
            }else {
                isBoth = false
            }
            
            if isBoth {
                strForBoth = strInitialAmount + " / " + strMaintAmount
            }
            
            cell.lblCouponAmount.text = strForBoth
            var strCouponName = ""
            
            strCouponName = "\(objCredit.value(forKey: "name") ?? "")"
            
            if strCouponName == ""
            {
                strCouponName = "\(objCredit.value(forKey: "discountCode") ?? "")"
            }
            if strCouponName == ""{
                strCouponName = getCreditNameBySysName(strSysName: "\(objCredit.value(forKey: "discountSysName") ?? "")")
            }
            cell.lblCouponCode.text = strCouponName//"\(objCredit.value(forKey: "name") ?? "")"
            cell.lblCouponDescription.text = objCredit.value(forKey: "discountDescription") as? String
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tblCoupon{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if tableView.visibleCells.contains(cell) {
                    self.tblCouponHeight?.constant = self.tblCoupon.contentSize.height
                }
            }
        }
        if tableView == tblCredit{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if tableView.visibleCells.contains(cell) {
                    self.tblCreditHeight?.constant = self.tblCredit.contentSize.height
                }
            }
        }
    }
    
    func getCreditNameBySysName(strSysName : String) -> String{
        
        var strName = ""
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if  dictMasters.value(forKey: "DiscountSetupMasterCredit") is [[String: Any]]{
            
            let arrDiscountCredit = dictMasters.value(forKey: "DiscountSetupMasterCredit") as? [[String:Any]]
            if (arrDiscountCredit?.count ?? 0) > 0
            {
                for i in 0..<arrDiscountCredit!.count
                {
                    let strSysNameDiscount = arrDiscountCredit![i]["SysName"] as? String ?? ""
                    if strSysNameDiscount.lowercased() == strSysName.lowercased(){
                        strName = arrDiscountCredit?[i]["Name"] as? String ?? ""
                    }
                    
                }
            }
        }
        
        return strName
    }
    
}


// MARK: --------------------- Text Field Delegate --------------

extension PaymentTableViewCell : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        if ( textField == txtAmount)
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)

            return chk
        }
        else
        {
            
            return true
            
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
}
