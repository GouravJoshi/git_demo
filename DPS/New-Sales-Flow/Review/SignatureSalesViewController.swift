//
//  SignatureViewController.swift
//  DPS
//
//  Created by APPLE on 02/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

struct Line {
    let strokeWidth: Float
    let color: UIColor
    var points: [CGPoint]
}

class Canvas: UIView {

    // public function
    fileprivate var strokeColor = UIColor.black
    fileprivate var strokeWidth: Float = 1

    func setStrokeWidth(width: Float) {
        self.strokeWidth = width
    }

    func setStrokeColor(color: UIColor) {
        self.strokeColor = color
    }

    func undo() {
        _ = lines.popLast()
        setNeedsDisplay()
    }

    func clear() {
        lines.removeAll()
        setNeedsDisplay()
    }

    fileprivate var lines = [Line]()

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        guard let context = UIGraphicsGetCurrentContext() else { return }

        lines.forEach { (line) in
            context.setStrokeColor(line.color.cgColor)
            context.setLineWidth(CGFloat(line.strokeWidth))
            context.setLineCap(.round)
            for (i, p) in line.points.enumerated() {
                if i == 0 {
                    context.move(to: p)
                } else {
                    context.addLine(to: p)
                }
            }
            context.strokePath()
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        lines.append(Line.init(strokeWidth: strokeWidth, color: strokeColor, points: []))
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else { return }
        guard var lastLine = lines.popLast() else { return }
        lastLine.points.append(point)
        lines.append(lastLine)
        setNeedsDisplay()
    }
}


class SignatureSalesViewController: UIViewController {
    
    // Interface Links
    @IBOutlet weak var signatureView: Canvas!
    @IBOutlet weak var imgSig: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        signatureView.setStrokeColor(color: .black)
    }
    
    func setupViews(){
        
//        signatureView.layer.borderWidth = 0.5
//        signatureView.layer.borderColor = UIColor.black.cgColor
//        signatureView.layer.cornerRadius = 10
    }
    
    @IBAction func clearBtnTapped(_ sender: UIButton) {
        signatureView.clear()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        let renderer = UIGraphicsImageRenderer(size: self.signatureView.bounds.size)
        let image = renderer.image { ctx in

            self.signatureView.drawHierarchy(in: self.signatureView.bounds, afterScreenUpdates: true)
        }
       // self.imgSig.image = image
        NotificationCenter.default.post(name: Commons.kNotificationSignature, object: image, userInfo: nil)

        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
