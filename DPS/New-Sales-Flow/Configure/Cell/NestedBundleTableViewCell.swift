//
//  NestedBundleTableViewCell.swift
//  DPS
//
//  Created by APPLE on 15/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
protocol NestedBundleConfigureCellDelegate : class {
    //ConfigureServiceTableViewCell Method
    func callBackAddAgreementMethodBundle(isSelected: Bool, cell: NestedBundleTableViewCell)
}

class NestedBundleTableViewCell: UITableViewCell {
    weak var delegate : NestedBundleConfigureCellDelegate?

    @IBOutlet weak var headerBgView: UIView!
    @IBOutlet weak var descriptionBgView: UIView!
    @IBOutlet weak var detailBgView: UIView!
    @IBOutlet weak var lblInitialTitle: UILabel!
    @IBOutlet weak var lblFinalInitialTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTimePeriod: UILabel!
    @IBOutlet weak var lblBilled: UILabel!
    
    @IBOutlet weak var lblOneTimePriceValue: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblDiscountPercentValue: UILabel!
    @IBOutlet weak var lblFinalOneTimePriceValue: UILabel!
    @IBOutlet weak var lblMaintenancePriceValue: UILabel!
    
    
    @IBOutlet weak var btnIsAgreement: UIButton!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var topSpaceDesc: NSLayoutConstraint!
    @IBOutlet weak var bottomSpaceDesc: NSLayoutConstraint!
    var dictLoginData = NSDictionary()
    var chkFreqConfiguration = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setBundleServiceValues(objCustomServiceDetail : NSManagedObject)
    {
        var isUnitBased = false
        var isParameterBased = false
        
        let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "serviceSysName") ?? "")")
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")")

        
        if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
        {
            isUnitBased = true
        }
        else
        {
            isUnitBased = false
        }
        
        if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
        {
            isParameterBased = true
        }
        else
        {
            isParameterBased = false
        }
        
        
        self.lblTitle.text = "\(dictService.value(forKey: "Name") ?? "")"
        self.lblTimePeriod.text = "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")"
        
        let billed: Double = Double(objCustomServiceDetail.value(forKey: "billingFrequencyPrice") as! String) ?? 0.0
        
        self.lblDescription.text = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        if self.lblDescription.text == "" {
            self.topSpaceDesc.constant = 0
            self.bottomSpaceDesc.constant = 0
        }
        
        let initial: Double = Double(objCustomServiceDetail.value(forKey: "initialPrice") as! String) ?? 0.0
        
        var discount: Double = Double(objCustomServiceDetail.value(forKey: "discount") as! String) ?? 0.0
        if discount < 0 {
            discount = 0
        }
        self.lblDiscountValue.text = String(format: "$%.2f", discount)
        
        let discountPercent: Double = Double(objCustomServiceDetail.value(forKey: "discountPercentage") as! String) ?? 0.0
        self.lblDiscountPercentValue.text = String(format: "%.2f", discountPercent)

        
        var sumFinalInitialPrice = 0.0, sumFinalMaintPrice = 0.0

        //For Parameter based service
        if isParameterBased
        {
            let arrPara = objCustomServiceDetail.value(forKey: "additionalParameterPriceDcs") as! NSArray
            
            if arrPara.count > 0
            {
                for i in 0..<arrPara.count
                {
                    let dict = arrPara.object(at: i) as! NSDictionary
                    let arrKey = dict.allKeys as NSArray
                    if arrKey.contains("FinalInitialUnitPrice")
                    {
                        sumFinalInitialPrice = sumFinalInitialPrice + Double(("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).floatValue)
                    }
                    if arrKey.contains("FinalMaintUnitPrice")
                    {
                        sumFinalMaintPrice = sumFinalMaintPrice + Double(("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).floatValue)
                    }
                }
            }
        }
        
        var unit = 1.0
        if isUnitBased
        {
            unit = Double(("\(objCustomServiceDetail.value(forKey: "unit") ?? "")" as NSString).floatValue)
        }
        
        
        var finalPrice = ((initial * unit) + sumFinalInitialPrice)
        if finalPrice < 0
        {
            finalPrice = 0
        }
        
        self.lblOneTimePriceValue.text = String(format: "$%.2f", finalPrice)
        self.lblFinalOneTimePriceValue.text = String(format: "$%.2f", finalPrice - discount)
        
        var maint: Double = (Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0)//(((Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0) * unit) + sumFinalMaintPrice)
        if maint < 0
        {
            maint = 0
        }
        self.lblMaintenancePriceValue.text = String(format: "$%.2f", maint)
        
        if objCustomServiceDetail.value(forKey: "isSold") as! String == "true" {
            btnIsAgreement.isSelected = true
        }else {
            btnIsAgreement.isSelected = false
        }
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = Double(finalPrice)
        totalMaintPrice = Double(maint)

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        
        if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            lblInitialTitle.text = "One Time Price"
            lblFinalInitialTitle.text = "Final One Time Price"
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBilled.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
    }
    
    @IBAction func addToAgreementAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        delegate?.callBackAddAgreementMethodBundle(isSelected: sender.isSelected, cell: self)
    }
    
}
