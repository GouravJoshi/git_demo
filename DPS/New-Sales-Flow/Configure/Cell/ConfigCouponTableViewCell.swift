//
//  CouponTableViewCell.swift
//  DPS
//
//  Created by APPLE on 24/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class ConfigCouponTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var lblCouponAmount: UILabel!
    @IBOutlet weak var lblCouponDescription: UILabel!
    
    @IBOutlet weak var lblMaintPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
