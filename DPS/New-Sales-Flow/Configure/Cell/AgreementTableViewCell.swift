//
//  AgreementTableViewCell.swift
//  DPS
//
//  Created by APPLE on 28/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol AgreementCellDelegate : class {
    //ConfigureServiceTableViewCell Method
    func callBackCheckUncheckMethod(cell: AgreementChkTableViewCell)
}

class AgreementChkTableViewCell: UITableViewCell {
    weak var delegate : AgreementCellDelegate?

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func agreeAction(_ sender: Any) {
        delegate?.callBackCheckUncheckMethod(cell: self)
    }
}
