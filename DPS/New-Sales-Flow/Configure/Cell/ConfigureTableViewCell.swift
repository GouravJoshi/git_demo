//
//  ConfigureTableViewCell.swift
//  DPS
//
//  Created by APPLE on 09/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol ConfigureCellDelegate : class {
    //ConfigureServiceTableViewCell Method
    func callBackAddAgreementMethod(cell: ConfigureServiceTableViewCell)
    func callBackIsTaxableMethod(cell: ConfigureServiceTableViewCell)

    //ConfigureNotesTableViewCell Method
    func callBackEditNotesMethod(cell: ConfigureNotesTableViewCell)
    
    //ConfigureMonthTableViewCell Method
    func callBackMonthMethod(cell: ConfigureMonthTableViewCell, month: String, tag: Int)
    
    //ConfigureAgreementTableViewCell Method
    func callBackAgreementMethod(cell: ConfigureAgreementTableViewCell, objCheckAgreement: NSManagedObject, isCheck: Bool)
    
    //ConfigurePricingTableViewCell Method
    func callBackTaxCodeMethod(cell: ConfigurePricingTableViewCell, taxCode: String)
    func callBackApplyCouponMethod(cell: ConfigurePricingTableViewCell, couponCode: String)
    func callBackDeleteCoupon(cell: ConfigurePricingTableViewCell, objCouponDetails : NSManagedObject)
    func callBackSelectCredit(cell: ConfigurePricingTableViewCell)
    func callBackAddCreditMethod(cell: ConfigurePricingTableViewCell, creditCode: String)
    
    func callBackAddAgreementMethodBundle(isSelected: Bool, indexPath: IndexPath, row: Int)


}

//MARK:- 1. ConfigureServiceTableViewCell
//MARK:-
class ConfigureServiceTableViewCell: UITableViewCell {
    weak var delegate : ConfigureCellDelegate?
    
    @IBOutlet weak var lblInitialTitle      : UILabel!
    @IBOutlet weak var lblFinalInitialTitle : UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTimePeriod: UILabel!
    @IBOutlet weak var lblBilled: UILabel!
    
    @IBOutlet weak var lblOneTimePriceValue: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblDiscountPercentValue: UILabel!
    @IBOutlet weak var lblFinalOneTimePriceValue: UILabel!
    @IBOutlet weak var lblMaintenancePriceValue: UILabel!
    @IBOutlet weak var maintPriceHConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var btnIsAgreement: UIButton!
    @IBOutlet weak var btnIsTaxable: UIButton!
    @IBOutlet weak var lblTaxable: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var topSpaceDesc: NSLayoutConstraint!
    @IBOutlet weak var bottomSpaceDesc: NSLayoutConstraint!
    @IBOutlet weak var lblRenewalDecription: UILabel!
    @IBOutlet weak var lineViewR: UIView!
    
    var dictLoginData = NSDictionary()
    var chkFreqConfiguration = false

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setStandardServiceValues(objCustomServiceDetail : NSManagedObject)
    {
        var isUnitBased = false
        var isParameterBased = false
        self.btnIsTaxable.isHidden = true
        self.lblTaxable.isHidden = true
        
        let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "serviceSysName") ?? "")")
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")")

        
        if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
        {
            isUnitBased = true
        }
        else
        {
            isUnitBased = false
        }
        
        if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
        {
            isParameterBased = true
        }
        else
        {
            isParameterBased = false
        }
        
        
        self.lblTitle.text = "\(dictService.value(forKey: "Name") ?? "")"
        self.lblTimePeriod.text = "\(objCustomServiceDetail.value(forKey: "serviceFrequency") ?? "")"
        
        let billed: Double = Double(objCustomServiceDetail.value(forKey: "billingFrequencyPrice") as! String) ?? 0.0
        
        //self.lblDescription.text = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        
        let strDesc = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        
        if strDesc.count == 0
        {
            self.lblDescription.text = ""
        }
        else
        {
            self.lblDescription.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)
        }
        if self.lblDescription.text == "" {
            self.topSpaceDesc.constant = 0
            self.bottomSpaceDesc.constant = 0
        }
        
        let initial: Double = Double(objCustomServiceDetail.value(forKey: "initialPrice") as! String) ?? 0.0
        
        var discount: Double = Double(objCustomServiceDetail.value(forKey: "discount") as! String) ?? 0.0
        if discount < 0 {
            discount = 0
        }
        
        self.lblDiscountValue.text = String(format: "$%.2f", discount)
        
        let discountPercent: Double = Double(objCustomServiceDetail.value(forKey: "discountPercentage") as! String) ?? 0.0
        self.lblDiscountPercentValue.text = String(format: "%.2f", discountPercent)

        
        var sumFinalInitialPrice = 0.0, sumFinalMaintPrice = 0.0

        //For Parameter based service
        if isParameterBased
        {
            let arrPara = objCustomServiceDetail.value(forKey: "additionalParameterPriceDcs") as! NSArray
            
            if arrPara.count > 0
            {
                for i in 0..<arrPara.count
                {
                    let dict = arrPara.object(at: i) as! NSDictionary
                    let arrKey = dict.allKeys as NSArray
                    if arrKey.contains("FinalInitialUnitPrice")
                    {
                        sumFinalInitialPrice = sumFinalInitialPrice + Double(("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).floatValue)
                    }
                    if arrKey.contains("FinalMaintUnitPrice")
                    {
                        sumFinalMaintPrice = sumFinalMaintPrice + Double(("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).floatValue)
                    }
                }
            }
        }
        
        var unit = 1.0
        if isUnitBased
        {
            unit = Double(("\(objCustomServiceDetail.value(forKey: "unit") ?? "")" as NSString).floatValue)
        }
        
        
        var finalPrice = ((initial * unit) + sumFinalInitialPrice)
        if finalPrice < 0
        {
            finalPrice = 0
        }
        
        self.lblOneTimePriceValue.text = String(format: "$%.2f", finalPrice)
        self.lblFinalOneTimePriceValue.text = String(format: "$%.2f", finalPrice - discount)
        
        var maint: Double = (Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0)//(((Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0) * unit) + sumFinalMaintPrice)
        if maint < 0
        {
            maint = 0
        }
        self.lblMaintenancePriceValue.text = String(format: "$%.2f", maint)
        
        if objCustomServiceDetail.value(forKey: "isSold") as! String == "true" {
            btnIsAgreement.isSelected = true
        }else {
            btnIsAgreement.isSelected = false
        }
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = Double(finalPrice)
        totalMaintPrice = Double(maint)

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        maintPriceHConstraint.constant = 25
        lblMaintenancePriceValue.isHidden = false
        lblInitialTitle.text = "Initial Price"
        lblFinalInitialTitle.text = "Final Initial Price"
        if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            lblInitialTitle.text = "One Time Price"
            lblFinalInitialTitle.text = "Final One Time Price"
            maintPriceHConstraint.constant = 0
            lblMaintenancePriceValue.isHidden = true
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBilled.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
    }
    
    func setCustomServiceValues(objCustomServiceDetail : NSManagedObject)
    {
        self.btnIsTaxable.isHidden = false
        self.lblTaxable.isHidden = false
        
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")")
        
        
        self.lblTitle.text = "\(objCustomServiceDetail.value(forKey: "serviceName") ?? "")"
        self.lblTimePeriod.text = "\(objCustomServiceDetail.value(forKey: "serviceFrequency") ?? "")"
        
        let billed: Double = Double(objCustomServiceDetail.value(forKey: "billingFrequencyPrice") as! String) ?? 0.0
        self.lblBilled.text = String(format: "$%.2f", billed)
        
       // self.lblDescription.text = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        
        let strDesc = "\(objCustomServiceDetail.value(forKey: "serviceDescription") ?? "")"
        
        if strDesc.count == 0
        {
            self.lblDescription.text = ""
        }
        else
        {
//            let data = Data("\(strDesc)".utf8)
//
//            if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//            {
//                self.lblDescription.attributedText = attributedString
//            }
            
            self.lblDescription.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)
            
        }
        
        if self.lblDescription.text == "" {
            self.topSpaceDesc.constant = 0
            self.bottomSpaceDesc.constant = 0
        }

        let initial: Double = Double(objCustomServiceDetail.value(forKey: "initialPrice") as! String) ?? 0.0
        self.lblOneTimePriceValue.text = String(format: "$%.2f", initial)
        
        var discount: Double = Double(objCustomServiceDetail.value(forKey: "discount") as! String) ?? 0.0
        if discount < 0 {
            discount = 0
        }
        self.lblDiscountValue.text = String(format: "$%.2f", discount)
        
        let discountPercent: Double = Double(objCustomServiceDetail.value(forKey: "discountPercentage") as! String) ?? 0.0
        self.lblDiscountPercentValue.text = String(format: "%.2f", discountPercent)

        let finalPrice = initial - discount
        self.lblFinalOneTimePriceValue.text = String(format: "$%.2f", finalPrice)
        
        let maint: Double = Double(objCustomServiceDetail.value(forKey: "maintenancePrice") as! String) ?? 0.0
        self.lblMaintenancePriceValue.text = String(format: "$%.2f", maint)
        
        if objCustomServiceDetail.value(forKey: "isSold") as! String == "true" {
            btnIsAgreement.isSelected = true
        }else {
            btnIsAgreement.isSelected = false
        }
        
        if objCustomServiceDetail.value(forKey: "isTaxable") as! String == "true" {
            btnIsTaxable.isSelected = true
        }else {
            btnIsTaxable.isSelected = false
        }
        
      //  lblBilled.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
        
        //Billing Amount Calculation
        
        var totalInitialPrice = 0.0, totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        totalInitialPrice = ("\(objCustomServiceDetail.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue
        totalMaintPrice = ("\(objCustomServiceDetail.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue

        let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

        maintPriceHConstraint.constant = 25
        lblMaintenancePriceValue.isHidden = false
        lblInitialTitle.text = "Initial Price"
        lblFinalInitialTitle.text = "Final Initial Price"
        if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            lblInitialTitle.text = "One Time Price"
            lblFinalInitialTitle.text = "Final One Time Price"
            maintPriceHConstraint.constant = 0
            lblMaintenancePriceValue.isHidden = true
            
        }
        else
        {
            if chkFreqConfiguration
            {
                if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            else
            {
                totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
            }
        }
        
        if totalBillingAmount < 0
        {
            totalBillingAmount = 0.0
        }
        
        lblBilled.text = "Billed \(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"

    }
    
    
    @IBAction func addToAgreementAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        delegate?.callBackAddAgreementMethod(cell: self)

    }
    
    @IBAction func isTaxableAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        delegate?.callBackIsTaxableMethod(cell: self)
    }
}

//MARK:- 2. ConfigurePricingTableViewCell
//MARK:-
class ConfigurePricingTableViewCell: UITableViewCell {

    weak var delegate : ConfigureCellDelegate?

    
    @IBOutlet weak var lblSubTotalInitial: UILabel!
    @IBOutlet weak var lblCouponDiscount: UILabel!
    @IBOutlet weak var lblCredit: UILabel!
    @IBOutlet weak var lblOtherDiscount: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblTaxAmount: UILabel!
    @IBOutlet weak var lblBillingAmount: UILabel!
    
    
    @IBOutlet weak var lblSubTotalMaintenance: UILabel!
    @IBOutlet weak var lblTotalPriceMaintenance: UILabel!
    @IBOutlet weak var lblTaxAmountMaintenance: UILabel!
    @IBOutlet weak var lblTotalDueAmount: UILabel!
    
    
    @IBOutlet weak var txtTaxCode: UITextField!
    @IBOutlet weak var txtCouponCode: UITextField!
    @IBOutlet weak var txtCredit: UITextField!
    
    @IBOutlet weak var btnTax           : UIButton!
    @IBOutlet weak var btnApplyCoupon   : UIButton!
    @IBOutlet weak var btnCredit        : UIButton!
    @IBOutlet weak var btnAddCredit     : UIButton!
    
    @IBOutlet weak var tblCouponHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCoupon: UITableView!
    var arrAppliedCoupon : [NSManagedObject] = []
    
    @IBOutlet weak var tblCreditHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCredit: UITableView!
    var arrAppliedCredit : [NSManagedObject] = []
    
    @IBOutlet weak var tblMaintFrqHeight: NSLayoutConstraint!
    @IBOutlet weak var tblMaintFrq: UITableView!
    var arrMaintBillingPrice  : [String] = []

    @IBOutlet weak var lblCreditMaint: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func tableSetUp (arrAppliedCoupon : [NSManagedObject]) {
        //Coupon
        if arrAppliedCoupon.count > 0{
            self.arrAppliedCoupon.removeAll()
            
            self.tblCoupon.delegate = self
            self.tblCoupon.dataSource = self
            self.arrAppliedCoupon = arrAppliedCoupon
            self.tblCoupon.reloadData()
            
            self.tblCouponHeight.constant = CGFloat(arrAppliedCoupon.count * 50)
        }
    }
    
    func tableCreditSetUp (arrAppliedCredit : [NSManagedObject]) {
        //Credit
        if arrAppliedCredit.count > 0{
            self.arrAppliedCredit.removeAll()
            
            self.tblCredit.delegate = self
            self.tblCredit.dataSource = self
            self.arrAppliedCredit = arrAppliedCredit
            self.tblCredit.reloadData()
            
            self.tblCreditHeight.constant = CGFloat(arrAppliedCredit.count * 50)
        }
    }
    
    func tableMaintSetUp (arrMaintBillingPrice: [String]) {
        //Credit
        if arrMaintBillingPrice.count > 0{
            self.arrMaintBillingPrice.removeAll()
            
            self.tblMaintFrq.delegate = self
            self.tblMaintFrq.dataSource = self
            self.arrMaintBillingPrice = arrMaintBillingPrice
            self.tblMaintFrq.reloadData()
            
            self.tblMaintFrqHeight.constant = CGFloat(arrMaintBillingPrice.count * 35)
        }
    }
    
    @IBAction func selectTaxCodeAction(_ sender: Any) {
        self.delegate?.callBackTaxCodeMethod(cell: self, taxCode: txtTaxCode.text ?? "")
    }
    
    @IBAction func applyCouponAction(_ sender: Any) {
        self.delegate?.callBackApplyCouponMethod(cell: self, couponCode: txtCouponCode.text ?? "")
    }
    
    @IBAction func selectCreditAction(_ sender: Any) {
        self.delegate?.callBackSelectCredit(cell: self)
    }
    
    @IBAction func addCreditAction(_ sender: Any) {
        self.delegate?.callBackAddCreditMethod(cell: self, creditCode: txtCredit.text ?? "")

    }
}

//MARK:- 3. ConfigureNotesTableViewCell
//MARK:-
class ConfigureNotesTableViewCell: UITableViewCell {
    weak var delegate : ConfigureCellDelegate?

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func editAction(_ sender: Any) {
        self.delegate?.callBackEditNotesMethod(cell: self)
    }
}

//MARK:- 3. ConfigureMonthTableViewCell
//MARK:-
class ConfigureMonthTableViewCell: UITableViewCell {
    weak var delegate : ConfigureCellDelegate?
    
    
    @IBOutlet weak var btnJan: UIButton!
    @IBOutlet weak var btnFeb: UIButton!
    @IBOutlet weak var btnMar: UIButton!
    @IBOutlet weak var btnApr: UIButton!
    @IBOutlet weak var btnMay: UIButton!
    @IBOutlet weak var btnJun: UIButton!
    @IBOutlet weak var btnJul: UIButton!
    @IBOutlet weak var btnAug: UIButton!
    @IBOutlet weak var btnSpt: UIButton!
    @IBOutlet weak var btnOct: UIButton!
    @IBOutlet weak var btnNov: UIButton!
    @IBOutlet weak var btnDec: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setMonthValues(arrMonth: [String]) {
        
        for month in arrMonth {
            if month == "January" {
                btnJan.isSelected = true
            }
            
            if month == "February" {
                btnFeb.isSelected = true
            }
            
            if month == "March" {
                btnMar.isSelected = true
            }
            
            if month == "April" {
                btnApr.isSelected = true
            }
            
            if month == "May" {
                btnMay.isSelected = true
            }
            
            if month == "June" {
                btnJun.isSelected = true
            }
            
            if month == "July" {
                btnJul.isSelected = true
            }
            
            if month == "August" {
                btnAug.isSelected = true
            }
            if month == "September" {
                btnSpt.isSelected = true
            }
            if month == "October" {
                btnOct.isSelected = true
            }
            if month == "November" {
                btnNov.isSelected = true
            }
            if month == "December" {
                btnDec.isSelected = true
            }

        }
    }
    
    @IBAction func janAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "January", tag: 0)
    }
    
    @IBAction func febAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "February", tag: 1)
    }
    
    @IBAction func marAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "March", tag: 2)
    }
    
    @IBAction func aprAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "April", tag: 3)
    }
    
    @IBAction func mayAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "May", tag: 4)
    }
    
    @IBAction func juneAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "June", tag: 5)
    }
    
    @IBAction func julAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "July", tag: 6)
    }
    
    @IBAction func augAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "August", tag: 7)
    }
    
    @IBAction func septAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "September", tag: 8)
    }
    
    @IBAction func octAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "October", tag: 9)
    }
    
    @IBAction func novAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "November", tag: 10)
    }
    
    @IBAction func decAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "December", tag: 11)
    }
    
}

//MARK:- 3. ConfigureAgreementTableViewCell
//MARK:-
class ConfigureAgreementTableViewCell: UITableViewCell {
    weak var delegate : ConfigureCellDelegate?

    @IBOutlet weak var tblCheckList: UITableView!
    @IBOutlet weak var tblChkHeight: NSLayoutConstraint!
    var arrayAgreementCheckList: [Any] = []
    var isCheckEnable = true
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func tableSetUp (arrayAgreementCheckList : [Any], isCompleteWon: Bool) {
        
        self.arrayAgreementCheckList.removeAll()
        self.isCheckEnable = isCompleteWon
        self.tblCheckList.delegate = self
        self.tblCheckList.dataSource = self
        self.arrayAgreementCheckList = arrayAgreementCheckList
        self.tblCheckList.reloadData()
        
        self.tblChkHeight.constant = CGFloat(arrayAgreementCheckList.count * 40)
    }
   
}

//MARK:- TablView Delegate for Coupon and Credit
extension ConfigurePricingTableViewCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCoupon {
            return  self.arrAppliedCoupon.count
        }else if tableView == tblCredit {
            return  self.arrAppliedCredit.count
        }else {
            return self.arrMaintBillingPrice.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Service
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponCell", for: indexPath as IndexPath) as! ConfigCouponTableViewCell
      
        if tableView == tblCoupon {
            var objCoupon = NSManagedObject()
            objCoupon = self.arrAppliedCoupon[indexPath.row]

            var strCouponName = "\(objCoupon.value(forKey: "name") ?? "")"
            
            if strCouponName == ""
            {
                strCouponName = "\(objCoupon.value(forKey: "discountCode") ?? "")"
            }
            
            cell.lblCouponCode.text = strCouponName//objCoupon.value(forKey: "name") as? String //discountCode
            cell.lblCouponAmount.text = String(format: "$%.2f", Double("\(objCoupon.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
            cell.lblCouponDescription.text = objCoupon.value(forKey: "discountDescription") as? String
            
        }else if tableView == tblCredit {
            var objCredit = NSManagedObject()
            objCredit = self.arrAppliedCredit[indexPath.row]
            
            let strInitialAmount = String(format: "$%.2f", Double("\(objCredit.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
            let strMaintAmount = String(format: "$%.2f", Double("\(objCredit.value(forKey: "appliedMaintDiscount") ?? "0")") ?? 0.0)
            var strForBoth = ""
            var isBoth = false
            if "\(objCredit.value(forKey: "applicableForInitial") ?? "0")" ==  "1" || "\(objCredit.value(forKey: "applicableForInitial") ?? "0")" == "true" {
                strForBoth = strInitialAmount + " / " + "$0.00"
                isBoth = true
            }else {
                isBoth = false
            }
            if "\(objCredit.value(forKey: "applicableForMaintenance") ?? "0")" ==  "1" || "\(objCredit.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                strForBoth = "$0.00" + " / " + strMaintAmount
                isBoth = true
            }else {
                isBoth = false
            }
            
            if isBoth {
                strForBoth = strInitialAmount + " / " + strMaintAmount
            }
            
            cell.lblCouponAmount.text = strForBoth
            var strCouponName = ""
            
            strCouponName = "\(objCredit.value(forKey: "name") ?? "")"
            
            if strCouponName == ""
            {
                strCouponName = "\(objCredit.value(forKey: "discountCode") ?? "")"
            }
            if strCouponName == ""{
                strCouponName = getCreditNameBySysName(strSysName: "\(objCredit.value(forKey: "discountSysName") ?? "")")
            }
            cell.lblCouponCode.text = strCouponName//"\(objCredit.value(forKey: "name") ?? "")"
            cell.lblCouponDescription.text = objCredit.value(forKey: "discountDescription") as? String
        }else {
            //self.arrMaintBillingPrice
            cell.lblMaintPrice.text = self.arrMaintBillingPrice[indexPath.row]
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
        print("Deleted")
        if tableView == tblCoupon {
            //Coupon
            var objCoupon = NSManagedObject()
            objCoupon = self.arrAppliedCoupon[indexPath.row]
            delegate?.callBackDeleteCoupon(cell: self, objCouponDetails: objCoupon)

        }else if tableView == tblCredit {
            //Credit
            var objCredit = NSManagedObject()
            objCredit = self.arrAppliedCredit[indexPath.row]
            delegate?.callBackDeleteCoupon(cell: self, objCouponDetails: objCredit)

        }else {
            
        }
      }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tblCoupon{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if tableView.visibleCells.contains(cell) {
                    self.tblCouponHeight?.constant = self.tblCoupon.contentSize.height
                }
            }
        }
        if tableView == tblCredit{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if tableView.visibleCells.contains(cell) {
                    self.tblCreditHeight?.constant = self.tblCredit.contentSize.height
                }
            }
        }
    }
    
    func getCreditNameBySysName(strSysName : String) -> String{
        
        var strName = ""
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if  dictMasters.value(forKey: "DiscountSetupMasterCredit") is [[String: Any]]{
            
            let arrDiscountCredit = dictMasters.value(forKey: "DiscountSetupMasterCredit") as? [[String:Any]]
            if (arrDiscountCredit?.count ?? 0) > 0
            {
                for i in 0..<arrDiscountCredit!.count
                {
                    let strSysNameDiscount = arrDiscountCredit![i]["SysName"] as? String ?? ""
                    if strSysNameDiscount.lowercased() == strSysName.lowercased(){
                        strName = arrDiscountCredit?[i]["Name"] as? String ?? ""
                    }
                    
                }
            }
        }
        
        return strName
    }
}

//MARK:- TablView Delegate for Agreement Check List
extension ConfigureAgreementTableViewCell : UITableViewDelegate, UITableViewDataSource, AgreementCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayAgreementCheckList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Service
        let cell = tableView.dequeueReusableCell(withIdentifier: "agreementCell", for: indexPath as IndexPath) as! AgreementChkTableViewCell
        var objAgree = NSManagedObject()
        objAgree = self.arrayAgreementCheckList[indexPath.row] as! NSManagedObject
        print(objAgree)
        let dict = getAgreementCheckListObject(strCheckListId: "\(objAgree.value(forKey: "agreementChecklistId") ?? "")")
        print(dict)
        cell.lblTitle.text = "\(dict.value(forKey: "Title") ?? "")"

        if "\(objAgree.value(forKey: "isActive") ?? "0")" == "1" || "\(objAgree.value(forKey: "isActive") ?? "false")" == "true" {
            cell.btnCheck.isSelected = true
        }else {
            cell.btnCheck.isSelected = false
        }
        
        if isCheckEnable {
            cell.btnCheck.isUserInteractionEnabled = false
        }
        
        cell.delegate = self
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    func callBackCheckUncheckMethod(cell: AgreementChkTableViewCell) {
        let indexPath = tblCheckList.indexPath(for: cell)
        
        cell.btnCheck.isSelected = !cell.btnCheck.isSelected
        
        var objAgree = NSManagedObject()
        objAgree = self.arrayAgreementCheckList[indexPath!.row] as! NSManagedObject
        print(objAgree)
        
        delegate?.callBackAgreementMethod(cell: self, objCheckAgreement: objAgree, isCheck: cell.btnCheck.isSelected)
    }
}
