//
//  BundleServiceTableViewCell.swift
//  DPS
//
//  Created by APPLE on 15/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit


class ConfigBundleServiceTableViewCell: UITableViewCell {
    weak var delegate : ConfigureCellDelegate?

    
    var arrSectionTitle: [[String: Any]] = []
    var isCompleteWon = false
    var isFromReview = false
    var isFromSendProposal = false

    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblBundleService: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureBundleTebleView(arrBundleService : [NSManagedObject], isCompleteWon: Bool) {
        self.isCompleteWon = isCompleteWon
        
        self.arrSectionTitle.removeAll()
        
        var arrUniqBundleId: [String] = []
        for i in 0..<arrBundleService.count {
            let objManage: NSManagedObject = arrBundleService[i]
            print(objManage)
            let strId = "\(objManage.value(forKey: "bundleId") ?? "")"
            if !(arrUniqBundleId.contains(strId)) {
                arrUniqBundleId.append(strId)
            }
        }
        
        
        for i in 0..<arrUniqBundleId.count {
            var arrTempBundleService : [NSManagedObject] = []

            let dictBundle = Helper.getBundleObjectFromId(strId: arrUniqBundleId[i]) as NSDictionary
            let bundleId = "\(dictBundle.value(forKey: "BundleName") ?? "")"
            var dictTemp:[String: Any] = [:]
            dictTemp["title"] = bundleId
            for objWorkorderDetail in arrBundleService {
                //bundleId
                let strId = "\(objWorkorderDetail.value(forKey: "bundleId") ?? "")"

                if arrUniqBundleId[i] == strId {
                    arrTempBundleService.append(objWorkorderDetail)
                }
            }
            dictTemp["bundleArray"] = arrTempBundleService
            self.arrSectionTitle.append(dictTemp)
        }
        print(self.arrSectionTitle)
        
        
        self.tblBundleService.delegate = self
        self.tblBundleService.dataSource = self
        self.tblBundleService.reloadData()
       
        //Cell Height Calculation for tableveiw height
//        var heightH = 0.0
//        for obj in tblBundleService.visibleCells {
//            if let cell = obj as? NestedBundleTableViewCell {
//                heightH = heightH + Double(cell.bounds.height)
//            }
//        }
        self.tblHeightConstraint.constant = CGFloat((204 * arrBundleService.count) + ((50 + 30) * self.arrSectionTitle.count))
    }
}

extension ConfigBundleServiceTableViewCell : UITableViewDelegate, UITableViewDataSource, NestedBundleConfigureCellDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrSectionTitle.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrBundle: [NSManagedObject] = self.arrSectionTitle[section]["bundleArray"] as! [NSManagedObject]
        return  arrBundle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Service
        let cell = tableView.dequeueReusableCell(withIdentifier: "NestedBundleCell", for: indexPath as IndexPath) as! NestedBundleTableViewCell
      
      //  cell.delegate = self
        let arrBundle: [NSManagedObject] = self.arrSectionTitle[indexPath.section]["bundleArray"] as! [NSManagedObject]

        
        var objCustomServiceDetail = NSManagedObject()
        objCustomServiceDetail = arrBundle[indexPath.row]
        cell.setBundleServiceValues(objCustomServiceDetail: objCustomServiceDetail)
        
        if isFromSendProposal {
            cell.lblTitle.textColor = .white
            cell.lblBilled.textColor = .white
            cell.lblTimePeriod.textColor = .white
            cell.headerBgView.backgroundColor = UIColor.init(hex: "4F5A68")
            cell.descriptionBgView.backgroundColor = UIColor.init(hex: "E1E2E2")
            cell.detailBgView.backgroundColor = UIColor.init(hex: "E1E2E2")
        }
        

        return cell
        
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
//        if section == 0 || section == 1 || section == 2 {
            return 50
//        }else {
//            return 1
//        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: self.tblBundleService.frame.width,
                                              height: 30))
        headerView.backgroundColor = .white
        
        let label = UILabel()
        label.frame = CGRect(x: 60, y: 0, width: self.tblBundleService.frame.width - 40, height: 50)
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 20:18)

        let strTitle: String = self.arrSectionTitle[section]["title"] as! String
        label.text = "Bundle: \(strTitle)"
        label.textColor = .black
        
        let myChkButton = UIButton()
        myChkButton.frame = CGRect(x: 20, y: 0, width: 30, height: 50)
        myChkButton.tag = section
        
        let arrBundle: [NSManagedObject] = self.arrSectionTitle[section]["bundleArray"] as! [NSManagedObject]
        let objCustomServiceDetail: NSManagedObject = arrBundle[0]
        if objCustomServiceDetail.value(forKey: "isSold") as! String == "true" {
            myChkButton.isSelected = true
        }else {
            myChkButton.isSelected = false
        }
        myChkButton.setImage(UIImage.init(named: "check_box_2New.png"), for: .selected)
        myChkButton.setImage(UIImage.init(named: "check_box_1New.png"), for: .normal)
        myChkButton.addTarget(self, action: #selector(self.buttonTapped), for: .touchUpInside)

        if !isFromReview
        {

            headerView.addSubview(myChkButton)
        }
        else {
            label.frame = CGRect(x: 20, y: 0, width: self.tblBundleService.frame.width - 40, height: 50)
        }
        headerView.addSubview(label)

        if self.isCompleteWon {
            myChkButton.isUserInteractionEnabled = false
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: self.tblBundleService.frame.width,
                                              height: 30))
        footerView.backgroundColor = .lightGray

        if isFromReview && !isFromSendProposal{
            footerView.backgroundColor = .white

            let footerViewBG = UIView(frame: CGRect(x: 10,
                                                  y: 0,
                                                  width: self.tblBundleService.frame.width - 20,
                                                  height: 30))
            footerViewBG.backgroundColor = .lightGray
            footerView.addSubview(footerViewBG)
        }
        
        
        let labelInitial = UILabel(frame: CGRect(x: 10, y: 0, width: (self.tblBundleService.frame.width/2), height: 30))
        labelInitial.textAlignment = .left
        labelInitial.font = UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 15:12)
        
        let labelMaint = UILabel(frame: CGRect(x: (self.tblBundleService.frame.width/2), y: 0, width: (self.tblBundleService.frame.width/2) - 10, height: 30))
        labelMaint.textAlignment = .right
        labelMaint.font = UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 15:12)
        
        
    
        let arrBundle: [NSManagedObject] = self.arrSectionTitle[section]["bundleArray"] as! [NSManagedObject]
       
        var initial: Double = 0.0
        var maint: Double = 0.0
        for objServiceDetail in arrBundle {
            initial = initial + (Double(objServiceDetail.value(forKey: "initialPrice") as! String) ?? 0.0)
            maint = maint + (Double(objServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0)
        }
        print(initial, maint)
        if isFromReview {
            labelInitial.frame = CGRect(x: 20, y: 0, width: (self.tblBundleService.frame.width/2), height: 30)
            labelMaint.frame = CGRect(x: (self.tblBundleService.frame.width/2), y: 0, width: (self.tblBundleService.frame.width/2) - 20, height: 30)

        }
        
        labelInitial.text = String(format: "Total Initial Price: $%.2f", initial)
        labelInitial.textColor = .black
        
        labelMaint.text = String(format: "Total Maint Price: $%.2f", maint)
        labelMaint.textColor = .black
        
        footerView.addSubview(labelInitial)
        footerView.addSubview(labelMaint)

        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //Callback
    func callBackAddAgreementMethodBundle(isSelected: Bool, cell: NestedBundleTableViewCell) {
        //let indexPath = tblBundleService.indexPath(for: cell)

        //delegate?.callBackAddAgreementMethodBundle(isSelected: isSelected, cell: self, row: indexPath?.row ?? 0)
    }
    
    @objc func buttonTapped(sender : UIButton) {
        print(sender.tag)
        sender.isSelected = !sender.isSelected
        print(sender.isSelected)
        
        let arrBundle: [NSManagedObject] = self.arrSectionTitle[sender.tag]["bundleArray"] as! [NSManagedObject]
        var count = 0
        for i in 0..<self.arrSectionTitle.count {
            if i < sender.tag {
                let arrBundle: [NSManagedObject] = self.arrSectionTitle[i]["bundleArray"] as! [NSManagedObject]
                count = count + arrBundle.count
            }
        }
        
        for i in 0..<arrBundle.count {
            let indexPath = IndexPath(row: i, section: sender.tag)
            delegate?.callBackAddAgreementMethodBundle(isSelected: sender.isSelected, indexPath: indexPath, row: (i+count))
        }
    }
}
