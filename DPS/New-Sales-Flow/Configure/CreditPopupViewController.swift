//
//  CreditPopupViewController.swift
//  DPS
//
//  Created by APPLE on 26/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class CreditPopupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblCredit: UITableView!
    var arrDiscountCredit:[Any] = []
    var arrTaxCode:[Any] = []
    var strCodeType = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UITableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if strCodeType == "Tax" {
            return self.arrTaxCode.count
        }else {
            return self.arrDiscountCredit.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Service
        let cell = tableView.dequeueReusableCell(withIdentifier: "creditCell", for: indexPath as IndexPath) as! CreditTableViewCell
        
        if strCodeType == "Tax" {
            let dictAppliedTax: [String: Any] = self.arrTaxCode[indexPath.row] as! [String : Any]
            cell.lblCreditName.text = dictAppliedTax["Name"] as? String
        }else {
            let dictAppliedCredit: [String: Any] = self.arrDiscountCredit[indexPath.row] as! [String : Any]
            cell.lblCreditName.text = dictAppliedCredit["Name"] as? String
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if strCodeType == "Tax" {
            let dictAppliedTax: [String: Any] = self.arrTaxCode[indexPath.row] as! [String : Any]
            
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: Commons.kNotificationTaxCode, object: nil, userInfo: dictAppliedTax)
            })
        }else {
            let dictAppliedCredit: [String: Any] = self.arrDiscountCredit[indexPath.row] as! [String : Any]
            
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: Commons.kNotificationCredit, object: nil, userInfo: dictAppliedCredit)
            })
        }
        
    }
}

//MARK:- Credit TableView Cell Class
class CreditTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblCreditName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
