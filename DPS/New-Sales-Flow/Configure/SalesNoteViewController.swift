//
//  SalesNoteViewController.swift
//  DPS
//
//  Created by APPLE on 16/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNoteViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    var strTitle = ""
    var strMessage = ""
    var strPlaceHolder = "Please enter your notes."

    @IBOutlet weak var txtNotesView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(strMessage)
        // Do any additional setup after loading the view.
        self.lblTitle.text = strTitle
        if strMessage != "" {
            strPlaceHolder = strMessage
        }else {
            strPlaceHolder = "Please enter your notes."
        }
        
        if strPlaceHolder == "Please enter your notes." {
            txtNotesView.textColor = UIColor.lightGray
        }else {
            txtNotesView.textColor = UIColor.black
        }
        txtNotesView.text = strPlaceHolder
        txtNotesView.isEditable = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveNoteAction(_ sender: Any) {
        if self.txtNotesView.text == "" || self.txtNotesView.text == "Please enter your notes."//strPlaceHolder
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter \(strTitle.lowercased()) description.", viewcontrol: self)
        }else {
            NotificationCenter.default.post(name: Commons.kNotificationNotes, object: strTitle, userInfo: ["note": txtNotesView.text])

            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension SalesNoteViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            strPlaceHolder = "Please enter your notes."
            textView.text = strPlaceHolder
            textView.textColor = UIColor.lightGray
        }
    }
}
