//
//  SalesConfigureViewModel.swift
//  DPS
//
//  Created by APPLE on 25/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

protocol SalesConfigureDelegate: class {
    func sucessfullyGetCredits(_ output: [Any])
    func sucessfullyGetTaxFromTaxCode(_ output: Any)
    func sucessfullyGetTemplateKey(_ output: Any)
}
class SalesConfigurePresenter: NSObject {
    weak private var delegate: SalesConfigureDelegate?
    private lazy var service = {
        
        return SalesConfigureNetworkService()
    }()
    
    init(_ delegate: SalesConfigureDelegate) {
        self.delegate = delegate
    }
    
    
    /**
    * Method name: requestToGetAppliedCredit
    * Description:This method is for FormBuilder View controller and it calls the private method to call a FormBuilder API.
    * Parameters:
    */
    public func requestToGetAppliedCredit(apiUrl: String)
    {
        requestToGetAppliedCreditHttp(apiUrl: apiUrl);
    }
    
    //MARK: Private Functions
    private func requestToGetAppliedCreditHttp(apiUrl: String)
    {
        
        SalesConfigureNetworkService.sharedInstance.requestToGetAppliedCredit(apiUrl: apiUrl) { (result, error) in
            
            
            if(error != nil){
//                self.delegate?.requestFailedWithError(message: error?.localizedDescription ?? Alert.sharedInstance.MSG_UNKNOWN_ERROR)
            }else if (result != nil){

                print(result as Any)
                self.delegate?.sucessfullyGetCredits(result!)
            }
        }
    }
    
    /**
    * Method name: requestToGetTaxByTaxCode
    * Description:
    * Parameters:
    */
    public func requestToGetTaxByTaxCode(apiUrl: String)
    {
        requestToGetTaxByTaxCodeHttp(apiUrl: apiUrl);
    }
    
    //MARK: Private Functions
    private func requestToGetTaxByTaxCodeHttp(apiUrl: String)
    {
        
        SalesConfigureNetworkService.sharedInstance.requestToGetTaxByTaxCode(apiUrl: apiUrl) { (result, error) in
            
            
            if(error != nil){
//                self.delegate?.requestFailedWithError(message: error?.localizedDescription ?? Alert.sharedInstance.MSG_UNKNOWN_ERROR)
            }else if (result != nil){

                print(result as Any)
                self.delegate?.sucessfullyGetTaxFromTaxCode(result!)
            }
        }
    }
    
    /**
    * Method name: requestToGetTemplateKey
    * Description:
    * Parameters:
    */
    public func requestToGetTemplateKey(apiUrl: String)
    {
        requestToGetTemplateKeyHttp(apiUrl: apiUrl);
    }
    
    //MARK: Private Functions
    private func requestToGetTemplateKeyHttp(apiUrl: String)
    {
        
        SalesConfigureNetworkService.sharedInstance.requestToGetTemplateKey(apiUrl: apiUrl) { (result, error) in
            
            
            if(error != nil){
//                self.delegate?.requestFailedWithError(message: error?.localizedDescription ?? Alert.sharedInstance.MSG_UNKNOWN_ERROR)
            }else if (result != nil){

                print(result as Any)
                self.delegate?.sucessfullyGetTemplateKey(result!)
            }
        }
    }
    
    
    /**
    * Method name: requestToGetAppliedCredit
    * Description:This method is for FormBuilder View controller and it calls the private method to call a FormBuilder API.
    * Parameters:
    */
    public func requestToUploadImageGraph(apiUrl: String, fileName: String, img_grf: UIImage)
    {
        requestToUploadImageGraphHttp(apiUrl: apiUrl, fileName: fileName, img_grf: img_grf);
    }
    
    //MARK: Private Functions
    private func requestToUploadImageGraphHttp(apiUrl: String, fileName: String, img_grf: UIImage)
    {
        
        SalesConfigureNetworkService.sharedInstance.requestToUploadImageGraph(apiUrl: apiUrl, fileName: fileName, img_grf: img_grf) { (result, error) in
            
            
            if(error != nil){
//                self.delegate?.requestFailedWithError(message: error?.localizedDescription ?? Alert.sharedInstance.MSG_UNKNOWN_ERROR)
            }else if (result != nil){

                print(result as Any)
              //  self.delegate?.sucessfullyGetCredits(result!)
            }
        }
    }
}
