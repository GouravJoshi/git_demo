//
//  SalesConfigureNetworkService.swift
//  DPS
//
//  Created by APPLE on 25/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

class SalesConfigureNetworkService {
    
    
    
    //Shared Instance
    static let sharedInstance = SalesConfigureNetworkService()
    /**
    * Method name: requestToGetAppliedCredit
    * Description:
    * Parameters:
    * Return:
    */
    
    func requestToGetAppliedCredit(apiUrl: String, andCompletion completionBlock: @escaping (_ result: [Any]?, Error?) -> Void){
        
        print(apiUrl)

        
        ServiceManager.sharedInstance.getRequestWithDirectUrlSession(apiUrl) { (response) in
            
            switch(response){
            case .failure(let error):
                completionBlock(nil, error)
                break
            case .success(let response):
                completionBlock(response, nil)
                print(response)
                break;
            }
        }
    }
    
    func requestToGetTaxByTaxCode(apiUrl: String, andCompletion completionBlock: @escaping (_ result: Any?, Error?) -> Void){
        
        print(apiUrl)

        
        ServiceManager.sharedInstance.getRequestWithDirectUrlForTaxSession(apiUrl) { (response) in
            
            switch(response){
            case .failure(let error):
                completionBlock(nil, error)
                break
            case .success(let response):
                completionBlock(response, nil)
                print(response)
                break;
            }
        }
    }
    
    func requestToGetTemplateKey(apiUrl: String, andCompletion completionBlock: @escaping (_ result: Any?, Error?) -> Void){
        
        print(apiUrl)

        
        ServiceManager.sharedInstance.getRequestWithDirectUrlForTemplateKeySession(apiUrl) { (response) in
            
            switch(response){
            case .failure(let error):
                completionBlock(nil, error)
                break
            case .success(let response):
                completionBlock(response, nil)
                print(response)
                break;
            }
        }
    }
    
    func requestToUploadImageGraph(apiUrl: String, fileName: String, img_grf: UIImage, andCompletion completionBlock: @escaping (_ result: Any?, Error?) -> Void){
        
        print(apiUrl)

        
        ServiceManager.sharedInstance.uploadImageGraph(apiUrl, paramName: "", fileName: fileName, profile_image: img_grf) { (response) in
            
            switch(response){
            case .failure(let error):
                completionBlock(nil, error)
                break
            case .success(let response):
                completionBlock(response, nil)
                print(response)
                break;
            }
        }
    }
    
}

