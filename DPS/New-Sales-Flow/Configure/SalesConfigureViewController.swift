//
//  SalesConfigureViewController.swift
//  DPS
//
//  Created by APPLE on 04/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import CoreData

enum NotesForConfigure: String {
    case customer = "Customer Note"
    case proposal = "Proposal Note"
    case internalN = "Internal Note"
}
class SalesConfigureViewController: UIViewController {
    
    private lazy var presenter = {
        return SalesConfigurePresenter(self)
    }()
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnSendProposal: UIButton!
    @IBOutlet weak var btnMarkAsLost: UIButton!
    var secTitleCount = 0
    var arrSectionTitle = ["", "", "", ""]
    var arrNotes = ["Customer Notes",
                    "Proposal Notes",
                    "Internal Notes"]
    
    var selectecPaymentType = ""
    
    //Var for Lead Details
    var strPreferedMonth = ""
    var strCustomerNote  = ""
    var strProposalNote  = ""
    var strInternalNote  = ""
    var strAccountNoGlobal = ""
    var strLeadNumberLead = ""
    var strLeadStatusGlobal = ""
    var strStageSysName = ""
    var strTaxValue = ""
    var strTaxCode = ""
    var strServiceUrlMain = ""
    var strIsServiceAddrTaxExempt = ""
    var strServiceAddressSubType = ""

    var arrayOfMonth: [String] = []
    var arrayOfFreqPrice    : [String] = []
    var arrayOfFreqSysName  : [String] = []
    var arrMaintBillingPrice  : [String] = []
    
    var arrayAgreementCheckList: [Any] = []
    var arrayAgreementCheckListTemp: NSArray = []

    var dictGlobleCouponDetail: [String: Any]?
    var arrDiscountCredit:[Any] = []
    var arrAppliedCreditDetail:[Any] = []
    var dictSelectedCredit: [String: Any]?
    var dictSelectedTaxCode: [String: Any]?
    var arrTaxCode:[Any]?
    
    var arrStandardService : [NSManagedObject] = []
    var arrCustomService : [NSManagedObject] = []
    var arrBundleService : [NSManagedObject] = []
    
    var arrStandardServiceSold : [NSManagedObject] = []
    var arrCustomServiceSold : [NSManagedObject] = []
    var arrBundleServiceSold : [NSManagedObject] = []
    var arrStandardServiceNotSold : [NSManagedObject] = []
    var arrCustomServiceNotSold : [NSManagedObject] = []
    var arrBundleServiceNotSold : [NSManagedObject] = []
    
    var dictPricingInfo: [String: Any] = [:]
    
    var arrAppliedCoupon : [NSManagedObject] = []
    var arrAppliedCredit : [NSManagedObject] = []
    
    var objLeadDetails : NSManagedObject?
    var objPaymentInfo : NSManagedObject?
    
    var objAgreementDetails : NSManagedObject?
    
    //Pricing Information
    var subTotalInitialPrice    = 0.0
    var couponDiscountPrice     = 0.0
    var creditAmount            = 0.0
    var creditAmountMaint       = 0.0
    var totalDiscountPrice      = 0.0
    var totalPrice              = 0.0
    var taxAmount               = 0.0
    var taxMaintAmount          = 0.0
    var billingAmount           = 0.0
    var initialStanTax          = 0.0
    var discountStanTax         = 0.0
    var initialMaintTax         = 0.0
    var taxableInitialAmount    = 0.0
    var taxableMaintAmount      = 0.0

    var initialNonStanTax          = 0.0
    var discountNonStanTax         = 0.0
    var initialNonMaintTax         = 0.0
    
    var subTotalMaintenanceAmount    = 0.0
    
    var appliedDiscountMaint = 0.0
    var appliedDiscountInitial = 0.0
    
    
    var isCouponSaved = false
    var chkFreqConfiguration = false
    var arrImageDetails: [[String: Any]] = []
    var arrGraphDetails: [[String: Any]] = []
    
    var dictTaxStatus           : NSDictionary = NSDictionary()
    var dictCommercialStatus    : NSDictionary = NSDictionary()
    var dictResidentialStatus   : NSDictionary = NSDictionary()

    
    //Nilind
    var strGlobalStatusSysName = ""
    var strGlobalStageSysName = ""
    var dictLoginData = NSDictionary()
    var matchesGeneralInfo = NSManagedObject()
    var strLeadId = "", strEmpName = "", strUserName = "", strCompanyKey = "", strBranchSysName = ""
    var isProposalFoolowUp = false
    var isServiceFollowUp = false
    var serviceFollowDays = 0
    var proposalFollowDays = 0
    var isFromServiceFollowUpDataBase = false
    var isFromProposalFolloupDataBase = false
    var isSendProposal = false
    var chkForLost = false
    var arySalesProposalFollowUpList = NSArray() , arySalesServiceFollowUpList = NSArray()
    var arySalesProposalServices = NSArray()
    var loader = UIAlertController()
    var isPreferredMonths = false
    var countCRMImage = 0
    
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.arrayOfMonth.removeAll()
        self.strLeadId = "\(nsud.value(forKey: "LeadId") ?? "")"
        basicFunction()
        self.serviceName()
       
        self.serviceTaxableStatus()
        self.initialSetup()
        
        
        //----Header
        lblTitle.text = "\(nsud.value(forKey: "titleHeader1") ?? "")"
        lblSubTitle.text = "\(nsud.value(forKey: "titleHeader2") ?? "")"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        basicFunction()
        if getOpportunityStatus() {
            self.btnSendProposal.isHidden = true
            self.btnSendProposal.isUserInteractionEnabled = false
           

        }else {
            self.btnSendProposal.isHidden = false
            self.btnSendProposal.isUserInteractionEnabled = true
           

        }
        
        if getOpportunityStatus() || getLostStatus()
        {
            self.btnMarkAsLost.isUserInteractionEnabled = false
            self.btnMarkAsLost.isHidden = true
        }
        else
        {
            self.btnMarkAsLost.isUserInteractionEnabled = true
            self.btnMarkAsLost.isHidden = false
        }
    }
    
    func getOpportunityStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    func getLostStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Lost") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func initialSetup() {
        subTotalInitialPrice    = 0.0
        creditAmount            = 0.0
        creditAmountMaint            = 0.0
        totalDiscountPrice      = 0.0
        totalPrice              = 0.0
        taxAmount               = 0.0
        billingAmount           = 0.0
        subTotalMaintenanceAmount    = 0.0
        
        initialStanTax           = 0.0
        discountStanTax          = 0.0
        initialMaintTax          = 0.0
        
        initialNonStanTax          = 0.0
        discountNonStanTax         = 0.0
        initialNonMaintTax         = 0.0
        
        taxMaintAmount           = 0.0
        taxableInitialAmount    = 0.0
        taxableMaintAmount      = 0.0
        
        self.secTitleCount = 0
        self.arrSectionTitle.removeAll()
        
        self.arrayOfFreqSysName.removeAll()
        self.arrayOfFreqPrice.removeAll()
        
        self.getLeadDetails()
        self.fetchStandardServiceData()
        self.fetchCustomServiceData()
        self.fetchPaymentInfo()
        
        self.getAppliedCouponOrCredit()

        self.billingFreqancyCalculationMethod()
        self.billingFreqancyCalculationOnCredit()
        
        self.arrSectionTitle.append("")
        self.arrSectionTitle.append("")
        self.arrSectionTitle.append("")
        self.arrSectionTitle.append("")
        
        self.serviceTaxCalculation()
        
        if !isPreferredMonths {
            arrSectionTitle.remove(at: arrSectionTitle.count - 1)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: {
            self.tblView.reloadData()
        })
    }
    
    //---------------------------------
    func getAppliedCouponOrCredit() {
        self.couponDiscountPrice        = 0.0
        self.creditAmount               = 0.0
        self.creditAmountMaint            = 0.0
        
        self.arrAppliedCoupon.removeAll()
        self.arrAppliedCredit.removeAll()
        
        self.arrAppliedCoupon = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Coupon")
        self.arrAppliedCredit = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Credit")
        print(self.arrAppliedCoupon)
        print(self.arrAppliedCredit)
        
        if self.arrAppliedCoupon.count > 0 {
            for coupon in self.arrAppliedCoupon {
                self.couponDiscountPrice = self.couponDiscountPrice + (Double("\(coupon.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
            }
        }
        
        if self.arrAppliedCredit.count > 0 {
            for credit in self.arrAppliedCredit {
                
                if ("\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "1" || "\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "true") && ("\(credit.value(forKey: "applicableForInitial") ?? "0")" == "1" || "\(credit.value(forKey: "applicableForInitial") ?? "0")" == "true") {
                    
                    self.creditAmountMaint = self.creditAmountMaint + (Double("\(credit.value(forKey: "appliedMaintDiscount") ?? "0")") ?? 0.0)

                    self.creditAmount = self.creditAmount + (Double("\(credit.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
                }else if "\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "1" || "\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                    self.creditAmountMaint = self.creditAmountMaint + (Double("\(credit.value(forKey: "appliedMaintDiscount") ?? "0")") ?? 0.0)
                }else {
                    self.creditAmount = self.creditAmount + (Double("\(credit.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
                }
            }
        }
        self.tblView.reloadData()
    }
    
    func updatePricingInfoDict() {
//        self.dictPricingInfo["couponDiscount"]
        //****Sub Total****
        self.dictPricingInfo["subTotalInitial"] = String(format: "$%.2f", self.subTotalInitialPrice)
        /**********************/

        //****Coupon Discount****
        self.dictPricingInfo["couponDiscount"] = String(format: "$%.2f", self.couponDiscountPrice)
        /**********************/

        //****Credit****
        if self.creditAmount > self.subTotalInitialPrice {
            self.creditAmount = self.subTotalInitialPrice
        }
        self.dictPricingInfo["credit"] = String(format: "$%.2f", self.creditAmount)
        /**********************/

        //****Other Discount****
        self.dictPricingInfo["otherDiscount"] = String(format: "$%.2f", (self.totalDiscountPrice - self.couponDiscountPrice))
        /**********************/
        
        //****Total Price****
        self.totalPrice = (self.subTotalInitialPrice
                            - (self.creditAmount + self.totalDiscountPrice))
        if self.totalPrice < 0 {
            self.totalPrice = 0.0
        }
        self.dictPricingInfo["totalPrice"] = String(format: "$%.2f", self.totalPrice)
        /**********************/

        //****Tax Amount****
        self.dictPricingInfo["taxAmount"] = String(format: "$%.2f", self.taxAmount)
        /**********************/

        //****Billing Amount****
        self.billingAmount = self.totalPrice + self.taxAmount
        self.dictPricingInfo["billingAmount"] = String(format: "$%.2f", self.billingAmount)
        /**********************/
        
        //*Maintenance Service Price*
        //****SubTotal Amount****
        self.dictPricingInfo["subTotalMaintenance"] = String(format: "$%.2f", self.subTotalMaintenanceAmount)
        /**********************/
        
        //****Credit Maint****
        if self.creditAmountMaint > self.subTotalMaintenanceAmount {
            self.creditAmountMaint = self.subTotalMaintenanceAmount
        }
        self.dictPricingInfo["creditMaint"] = String(format: "$%.2f", self.creditAmountMaint)
        /**********************/
        
        //****Total Price****y
        var totalMaint = self.subTotalMaintenanceAmount - self.creditAmountMaint
        if totalMaint < 0 {
            totalMaint = 0.0
        }
        self.dictPricingInfo["totalPriceMaintenance"] = String(format: "$%.2f", totalMaint)
        /**********************/
        
        //****Tax Amount****
        self.dictPricingInfo["taxAmountMaintenance"] = String(format: "$%.2f", taxMaintAmount)
        /**********************/
        
        //****Billing Amount****
        let dueAmount = self.subTotalMaintenanceAmount - self.creditAmountMaint + taxMaintAmount
        self.dictPricingInfo["totalDueAmount"] = String(format: "$%.2f", dueAmount)
        /**********************/
        
        /*Maint billing info*/
        if self.arrMaintBillingPrice.count > 0 {
            self.dictPricingInfo["arrMaintBillingPrice"] = self.arrMaintBillingPrice
        }
        else{
            self.dictPricingInfo["arrMaintBillingPrice"] = []
        }
        /**********************/
        
        
        self.dictPricingInfo["taxableAmount"] = String(format: "$%.2f", self.taxableInitialAmount)
        self.dictPricingInfo["taxableMaintAmount"] = String(format: "$%.2f", self.taxableMaintAmount)
        print(self.dictPricingInfo)
    }
    
    func getLeadDetails() {
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        print(arrayLeadDetail)
        if(arrayLeadDetail.count > 0)
        {
            objLeadDetails = (arrayLeadDetail.firstObject as! NSManagedObject)
            
            //Get months from local DB
            self.strPreferedMonth = "\(objLeadDetails!.value(forKey: "strPreferredMonth")!)"
            if self.strPreferedMonth == "0" || self.strPreferedMonth == "" {
            }else{
                arrayOfMonth = strPreferedMonth.components(separatedBy: ", ")
            }
            
            self.strProposalNote = "\(objLeadDetails!.value(forKey: "proposalNotes")!)"
            self.strInternalNote = "\(objLeadDetails!.value(forKey: "notes")!)"
            self.strAccountNoGlobal = "\(objLeadDetails!.value(forKey: "accountNo")!)"
            self.strLeadNumberLead = "\(objLeadDetails!.value(forKey: "leadNumber")!)"
            self.strLeadStatusGlobal = "\(objLeadDetails!.value(forKey: "statusSysName")!)"
            self.strStageSysName = "\(objLeadDetails!.value(forKey: "stageSysName")!)"
            self.strTaxValue = "\(objLeadDetails!.value(forKey: "tax")!)"
            self.strTaxCode = "\(objLeadDetails!.value(forKey: "taxSysName")!)"
            self.strIsServiceAddrTaxExempt = "\(objLeadDetails!.value(forKey: "isServiceAddrTaxExempt")!)"
            self.strServiceAddressSubType = "\(objLeadDetails!.value(forKey: "serviceAddressSubType")!)"

            if self.strTaxValue == "" && self.strTaxCode != "" {
                self.getTaxCodeDetailWithApi(strTaxCode: self.strTaxCode)
            }
            
        }
    }
    
    func updateLeadDetails(taxValue: Double) {
        print(self.dictSelectedTaxCode)
        var strTaxCode = dictSelectedTaxCode?["SysName"] as? String ?? ""
        if strTaxCode == "" {
            strTaxCode = self.strTaxCode
        }
        
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        print(arrayLeadDetail)
        if(arrayLeadDetail.count > 0)
        {
            let objLeadInfo : NSManagedObject? = (arrayLeadDetail[0] as! NSManagedObject)
            objLeadInfo?.setValue("\(taxValue)", forKey: "tax")
            objLeadInfo?.setValue(strTaxCode, forKey: "taxSysName")
            
            let context = getContext()
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
        }
        
        self.initialSetup()
    }
    
    func fetchStandardServiceData() {
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
        
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, self.strLeadId))
        
        self.arrStandardService.removeAll()
        self.arrBundleService.removeAll()
        if arryOfData.count > 0 {
            for objWorkorderDetail in arryOfData {
                let objWorkorderDetailTemp: NSManagedObject = objWorkorderDetail as! NSManagedObject
                //bundleId
                let bundleId = "\(objWorkorderDetailTemp.value(forKey: "bundleId") ?? "")"
                
                if bundleId == "" || bundleId == "0"
                {
                    self.arrStandardService.append(objWorkorderDetailTemp)
                    
                }else {
                    self.arrBundleService.append(objWorkorderDetailTemp)
                }
            }
            print(self.arrStandardService.count)
            print(self.arrBundleService.count)
            self.subtotalStandard()
            self.subtotalBundle()

        }else{
            
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
        }
    }
    
    
    func fetchCustomServiceData() {
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
        
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, self.strLeadId))
        
        self.arrCustomService.removeAll()
        if arryOfData.count > 0 {
            for objWorkorderDetail in arryOfData {
                self.arrCustomService.append(objWorkorderDetail as! NSManagedObject)
            }
            print(self.arrCustomService)
            
            self.subtotalNonStandard()
       //     tblView.reloadData()
        }else{
            
            // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
        }
        if self.arrBundleService.count > 0 {
            self.arrSectionTitle.insert("Bundle Service", at: self.secTitleCount)
            self.secTitleCount = self.secTitleCount + 1
        }
        
        //Getting Credit from API Call
        let strDetailUrl = "/api/CoreToSaleAuto/GetDiscountsForAccountExcludingLeadNo?"
        //let strType = "Credit"
        let strEmpBranchSysName = "\(dictLoginData.value(forKeyPath: "EmployeeBranchSysName") ?? "")"
        self.strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        
        let strUrl = "\(strServiceUrlMain)\(strDetailUrl)companyKey=\(strCompanyKey)&branchSysName=\(strEmpBranchSysName)&AccountNo=\(strAccountNoGlobal)&&leadNoToExclude=\(self.strLeadNumberLead)"
        print(strUrl)
        self.presenter.requestToGetAppliedCredit(apiUrl: strUrl)
    }
    
    func fetchPaymentInfo() {
        
        let arrPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        //objPaymentInfo
        print(arrPaymentInfo)
        if(arrPaymentInfo.count > 0)
        {
            objPaymentInfo = (arrPaymentInfo.firstObject as! NSManagedObject)
            
            //
            self.strCustomerNote = "\(objPaymentInfo!.value(forKey: "specialInstructions")!)"
            
        }else {
            self.savePaymentInfo()
        }
    }
    
    func savePaymentInfo () {
        let context = getContext()
        // let entityPaymentInfo = PaymentInfo(context: context)
        //        let entityPaymentInfo = NSManagedObject(entity: PaymentInfo.Type, insertInto: context)
        let entityPaymentInfo = NSEntityDescription.insertNewObject(forEntityName: "PaymentInfo", into: context)
        
        entityPaymentInfo.setValue(self.strLeadId, forKey: "leadId")
        entityPaymentInfo.setValue("", forKey: "leadPaymentDetailId")
        entityPaymentInfo.setValue("", forKey: "paymentMode")
        entityPaymentInfo.setValue("", forKey: "amount")
        entityPaymentInfo.setValue("", forKey: "checkNo")
        entityPaymentInfo.setValue("", forKey: "licenseNo")
        entityPaymentInfo.setValue("", forKey: "expirationDate")
        entityPaymentInfo.setValue("", forKey: "specialInstructions")
        entityPaymentInfo.setValue("", forKey: "agreement")
        entityPaymentInfo.setValue("", forKey: "proposal")
        entityPaymentInfo.setValue("", forKey: "customerSignature")
        entityPaymentInfo.setValue("", forKey: "salesSignature")
        entityPaymentInfo.setValue("", forKey: "createdBy")
        entityPaymentInfo.setValue("", forKey: "createdDate")
        entityPaymentInfo.setValue("", forKey: "modifiedBy")
        entityPaymentInfo.setValue("", forKey: "modifiedDate")
        entityPaymentInfo.setValue(strUserName, forKey: "userName")
        entityPaymentInfo.setValue(strCompanyKey, forKey: "companyKey")
        entityPaymentInfo.setValue("", forKey: "checkBackImagePath")
        entityPaymentInfo.setValue("", forKey: "checkFrontImagePath")
        entityPaymentInfo.setValue("", forKey: "inspection")
        
        
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.fetchPaymentInfo()
        
    }
    
    func subtotalStandard() {
        print(self.arrStandardService)
        var totalInitialPrice = 0.0
        var totalDiscountPrice = 0.0
        var totalMaintPrice = 0.0
        self.arrStandardServiceSold.removeAll()
        self.arrStandardServiceNotSold.removeAll()
        
        if self.arrStandardService.count > 0 {
            arrSectionTitle.insert("Standard Service", at: self.secTitleCount)
            self.secTitleCount = self.secTitleCount + 1
        }
        
        for objServiceDetail in self.arrStandardService {
            
            if objServiceDetail.value(forKey: "isSold") as! String == "true" || objServiceDetail.value(forKey: "isSold") as! String == "1" {
                var isUnitBased = false
                var isParameterBased = false
                
                let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "serviceSysName") ?? "")")
                
                if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
                {
                    isUnitBased = true
                }
                else
                {
                    isUnitBased = false
                }
                
                if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
                {
                    isParameterBased = true
                }
                else
                {
                    isParameterBased = false
                }
                
                
                var sumFinalInitialPrice = 0.0, sumFinalMaintPrice = 0.0
                //For Parameter based service
                if isParameterBased
                {
                    let arrPara = objServiceDetail.value(forKey: "additionalParameterPriceDcs") as! NSArray
                    
                    if arrPara.count > 0
                    {
                        for i in 0..<arrPara.count
                        {
                            let dict = arrPara.object(at: i) as! NSDictionary
                            let arrKey = dict.allKeys as NSArray
                            if arrKey.contains("FinalInitialUnitPrice")
                            {
                                sumFinalInitialPrice = sumFinalInitialPrice + Double(("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).floatValue)
                            }
                            if arrKey.contains("FinalMaintUnitPrice")
                            {
                                sumFinalMaintPrice = sumFinalMaintPrice + Double(("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).floatValue)
                            }
                        }
                    }
                }
                
                var unit = 1.0
                if isUnitBased
                {
                    unit = Double(("\(objServiceDetail.value(forKey: "unit") ?? "")" as NSString).floatValue)
                }
                
                
                
                //Initial Price
                let initialPrice: String = objServiceDetail.value(forKey: "initialPrice") as! String
                totalInitialPrice = totalInitialPrice + ((Double(initialPrice) ?? 0.0) * unit)
                totalInitialPrice = (totalInitialPrice + sumFinalInitialPrice)
                if totalInitialPrice < 0
                {
                    totalInitialPrice = 0
                }
                
                
                //Discount Price
                let discountPrice: String = objServiceDetail.value(forKey: "discount") as! String
                totalDiscountPrice = totalDiscountPrice + (Double(discountPrice) ?? 0.0)
                
                //totalMaintPrice
                let maintPrice: String = objServiceDetail.value(forKey: "totalMaintPrice") as! String
                totalMaintPrice = totalMaintPrice + (Double(maintPrice) ?? 0.0)//totalMaintPrice + ((Double(maintPrice) ?? 0.0) * unit) + sumFinalMaintPrice
                if totalMaintPrice < 0
                {
                    totalMaintPrice = 0
                }
                
                
                //For Billing Frequancy
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
                let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")")
                
                var totalBillingAmount = 0.0
                let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
                let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"
                if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                {
                    totalBillingAmount = (((Double(initialPrice) ?? 0.0) * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    if chkFreqConfiguration
                    {
                        if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                        {
                            totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                        else
                        {
                            totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                    }
                    else
                    {
                        totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                
                if totalBillingAmount < 0
                {
                    totalBillingAmount = 0.0
                }
                //                let billingFrequencyPrice: String = objServiceDetail.value(forKey: "billingFrequencyPrice") as! String
                self.arrayOfFreqPrice.append("\(totalBillingAmount)")
                
                let billingFrequencySysName: String = objServiceDetail.value(forKey: "billingFrequencySysName") as! String
                self.arrayOfFreqSysName.append(billingFrequencySysName)
                
                self.arrStandardServiceSold.append(objServiceDetail)
                
                
                //Tax Calculations
                if self.strIsServiceAddrTaxExempt == "false" {
                    if strServiceAddressSubType == "Commercial" {
                        let strServiceSysName = "\(objServiceDetail.value(forKey: "serviceSysName") ?? "")"
                        let strIsValue = "\(self.dictCommercialStatus.value(forKey: strServiceSysName) ?? "")"
                        if strIsValue == "1" {
                            let strInitial = "\(objServiceDetail.value(forKey: "totalInitialPrice") ?? "0")"
                            let strDiscount = "\(objServiceDetail.value(forKey: "discount") ?? "0")"

                            self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                            
                            self.discountStanTax = self.discountStanTax + (Double(strDiscount) ?? 0.0)

                            self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                            print(self.initialStanTax, self.discountStanTax)
                        }
                    }else {
                        let strServiceSysName = "\(objServiceDetail.value(forKey: "serviceSysName") ?? "")"
                        let strIsValue = "\(self.dictResidentialStatus.value(forKey: strServiceSysName) ?? "")"
                        if strIsValue == "1" {
                            let strInitial = "\(objServiceDetail.value(forKey: "totalInitialPrice") ?? "0")"
                            let strDiscount = "\(objServiceDetail.value(forKey: "discount") ?? "0")"

                            self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                            
                            self.discountStanTax = self.discountStanTax + (Double(strDiscount) ?? 0.0)

                            self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                            print(self.initialStanTax, self.discountStanTax)
                        }
                    }
                }
                
                
            }else {
                self.arrStandardServiceNotSold.append(objServiceDetail)
            }
            
            
        }
        self.subTotalInitialPrice  = self.subTotalInitialPrice + totalInitialPrice
        self.totalDiscountPrice  = self.totalDiscountPrice + totalDiscountPrice
        self.subTotalMaintenanceAmount  = self.subTotalMaintenanceAmount + totalMaintPrice
        
        print(self.subTotalInitialPrice)
        print(self.totalDiscountPrice)
        print(self.subTotalMaintenanceAmount)
        
    }
    
    func subtotalBundle() {
        print(self.arrBundleService)
        var totalInitialPrice = 0.0
        var totalDiscountPrice = 0.0
        var totalMaintPrice = 0.0
        self.arrBundleServiceSold.removeAll()
        self.arrBundleServiceNotSold.removeAll()
        
        for objServiceDetail in self.arrBundleService {
            
            if "\(objServiceDetail.value(forKey: "isSold") ?? "")" == "true" || "\(objServiceDetail.value(forKey: "isSold") ?? "")" == "1" {
                var isUnitBased = false
                var isParameterBased = false
                
                let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "serviceSysName") ?? "")")
                
                if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
                {
                    isUnitBased = true
                }
                else
                {
                    isUnitBased = false
                }
                
                if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
                {
                    isParameterBased = true
                }
                else
                {
                    isParameterBased = false
                }
                
                
                var sumFinalInitialPrice = 0.0, sumFinalMaintPrice = 0.0
                //For Parameter based service
                if isParameterBased
                {
                    let arrPara = objServiceDetail.value(forKey: "additionalParameterPriceDcs") as! NSArray
                    
                    if arrPara.count > 0
                    {
                        for i in 0..<arrPara.count
                        {
                            let dict = arrPara.object(at: i) as! NSDictionary
                            let arrKey = dict.allKeys as NSArray
                            if arrKey.contains("FinalInitialUnitPrice")
                            {
                                sumFinalInitialPrice = sumFinalInitialPrice + Double(("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).floatValue)
                            }
                            if arrKey.contains("FinalMaintUnitPrice")
                            {
                                sumFinalMaintPrice = sumFinalMaintPrice + Double(("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).floatValue)
                            }
                        }
                    }
                }
                
                var unit = 1.0
                if isUnitBased
                {
                    unit = Double(("\(objServiceDetail.value(forKey: "unit") ?? "")" as NSString).floatValue)
                }
                
                //Initial Price
                let initialPrice: String = "\(objServiceDetail.value(forKey: "initialPrice") ?? "")"
                totalInitialPrice = totalInitialPrice + ((Double(initialPrice) ?? 0.0) * unit)
                totalInitialPrice = (totalInitialPrice + sumFinalInitialPrice)
                if totalInitialPrice < 0
                {
                    totalInitialPrice = 0
                }
                
                //Discount Price
                let discountPrice: String = "\(objServiceDetail.value(forKey: "discount") ?? "")"
                totalDiscountPrice = totalDiscountPrice + (Double(discountPrice) ?? 0.0)
                
                //totalMaintPrice
                let maintPrice: String = "\(objServiceDetail.value(forKey: "totalMaintPrice") ?? "")"
                totalMaintPrice = totalMaintPrice + ((Double(maintPrice) ?? 0.0) * unit) + sumFinalMaintPrice
                if totalMaintPrice < 0
                {
                    totalMaintPrice = 0
                }
                
                //For Billing Frequancy
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
                let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")")
                
                var totalBillingAmount = 0.0
                let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
                let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"
                if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                {
                    totalBillingAmount = (((Double(initialPrice) ?? 0.0) * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    if chkFreqConfiguration
                    {
                        if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                        {
                            totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                        else
                        {
                            totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                    }
                    else
                    {
                        totalBillingAmount = ( (Double(maintPrice) ?? 0.0 * unit) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                
                if totalBillingAmount < 0
                {
                    totalBillingAmount = 0.0
                }
                //                let billingFrequencyPrice: String = objServiceDetail.value(forKey: "billingFrequencyPrice") as! String
                self.arrayOfFreqPrice.append("\(totalBillingAmount)")
                
                let billingFrequencySysName: String = objServiceDetail.value(forKey: "billingFrequencySysName") as! String
                self.arrayOfFreqSysName.append(billingFrequencySysName)
                
                self.arrBundleServiceSold.append(objServiceDetail)
                
                
                //Tax Calculations
                if self.strIsServiceAddrTaxExempt == "false" {
                    if strServiceAddressSubType == "Commercial" {
                        let strServiceSysName = "\(objServiceDetail.value(forKey: "serviceSysName") ?? "")"
                        let strIsValue = "\(self.dictCommercialStatus.value(forKey: strServiceSysName) ?? "")"
                        if strIsValue == "1" {
                            let strInitial = "\(objServiceDetail.value(forKey: "totalInitialPrice") ?? "0")"
                            let strDiscount = "\(objServiceDetail.value(forKey: "discount") ?? "0")"

                            self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                            
                            self.discountStanTax = self.discountStanTax + (Double(strDiscount) ?? 0.0)

                            self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                            print(self.initialStanTax, self.discountStanTax)
                        }
                    }else {
                        let strServiceSysName = "\(objServiceDetail.value(forKey: "serviceSysName") ?? "")"
                        let strIsValue = "\(self.dictCommercialStatus.value(forKey: strServiceSysName) ?? "")"
                        if strIsValue == "1" {
                            let strInitial = "\(objServiceDetail.value(forKey: "totalInitialPrice") ?? "0")"
                            let strDiscount = "\(objServiceDetail.value(forKey: "discount") ?? "0")"
                            
                            self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                            
                            self.discountStanTax = self.discountStanTax + (Double(strDiscount) ?? 0.0)
                            
                            self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)
                            
                            print(self.initialStanTax, self.discountStanTax)
                        }
                        
                    }
                }
                
            }else {
                self.arrBundleServiceNotSold.append(objServiceDetail)
            }
        }
        
        self.subTotalInitialPrice  = self.subTotalInitialPrice + totalInitialPrice
        self.totalDiscountPrice  = self.totalDiscountPrice + totalDiscountPrice
        self.subTotalMaintenanceAmount  = self.subTotalMaintenanceAmount + totalMaintPrice
        
        print(self.subTotalInitialPrice)
        print(self.totalDiscountPrice)
        print(self.subTotalMaintenanceAmount)
        
    }
    
    func subtotalNonStandard() {
        print(self.arrCustomService)
        var totalInitialPrice = 0.0
        var totalDiscountPrice = 0.0
        var totalMaintPrice = 0.0
        self.arrCustomServiceSold.removeAll()
        self.arrCustomServiceNotSold.removeAll()
        
        if self.arrCustomService.count > 0 {
            arrSectionTitle.insert("Custom Service", at: self.secTitleCount)
            self.secTitleCount = self.secTitleCount + 1
        }
        
        
        for objServiceDetail in self.arrCustomService {
            if objServiceDetail.value(forKey: "isSold") as! String == "true" || objServiceDetail.value(forKey: "isSold") as! String == "1" {
                
                //Initial Price
                let initialPrice: String = objServiceDetail.value(forKey: "initialPrice") as! String
                totalInitialPrice = totalInitialPrice + (Double(initialPrice) ?? 0.0)
                
                //Discount Price
                let discountPrice: String = objServiceDetail.value(forKey: "discount") as! String
                totalDiscountPrice = totalDiscountPrice + (Double(discountPrice) ?? 0.0)
                
                //totalMaintPrice
                let maintPrice: String = objServiceDetail.value(forKey: "maintenancePrice") as! String
                totalMaintPrice = totalMaintPrice + (Double(maintPrice) ?? 0.0)
                
                
                //For Billing Frequancy
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
                let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")")
                
                var totalBillingAmount = 0.0
                let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
                let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"
                if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                {
                    totalBillingAmount = ((Double(initialPrice) ?? 0.0) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    if chkFreqConfiguration
                    {
                        if "\(objServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                        {
                            totalBillingAmount = ((Double(maintPrice) ?? 0.0) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                        else
                        {
                            totalBillingAmount = ( (Double(maintPrice) ?? 0.0) * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                    }
                    else
                    {
                        totalBillingAmount = ( (Double(maintPrice) ?? 0.0) * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                
                if totalBillingAmount < 0
                {
                    totalBillingAmount = 0.0
                }
                //                let billingFrequencyPrice: String = objServiceDetail.value(forKey: "billingFrequencyPrice") as! String
                self.arrayOfFreqPrice.append("\(totalBillingAmount)")
                
                let billingFrequencySysName: String = objServiceDetail.value(forKey: "billingFrequencySysName") as! String
                self.arrayOfFreqSysName.append(billingFrequencySysName)
                
                print(objServiceDetail.value(forKey: "isTaxable") as! String)
                if ("\(objServiceDetail.value(forKey: "isTaxable") ?? "false")" == "true" || "\(objServiceDetail.value(forKey: "isTaxable") ?? "0")" == "1") && (strIsServiceAddrTaxExempt.caseInsensitiveCompare("false") == .orderedSame) {
                    
                    self.initialStanTax = self.initialStanTax + (Double(initialPrice) ?? 0.0)

                    self.discountStanTax = self.discountStanTax + (Double(discountPrice) ?? 0.0)


                    self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                    print(self.initialStanTax, self.discountStanTax, self.initialMaintTax)
                    
                    self.initialNonStanTax = self.initialNonStanTax + (Double(initialPrice) ?? 0.0)
                    
                    self.discountNonStanTax = self.discountNonStanTax + (Double(discountPrice) ?? 0.0)
                    
                    
                    self.initialNonMaintTax  = self.initialNonMaintTax + (Double(maintPrice) ?? 0.0)

                    print(self.initialNonStanTax, self.discountNonStanTax, self.initialNonMaintTax)
                    
                }
                
                self.arrCustomServiceSold.append(objServiceDetail)
                
            }else {
                self.arrCustomServiceNotSold.append(objServiceDetail)
            }
        }
        
        self.subTotalInitialPrice  = self.subTotalInitialPrice + totalInitialPrice
        self.totalDiscountPrice  = self.totalDiscountPrice + totalDiscountPrice
        self.subTotalMaintenanceAmount  = self.subTotalMaintenanceAmount + totalMaintPrice
        
        print(self.subTotalInitialPrice)
        print(self.totalDiscountPrice)
        print(self.subTotalMaintenanceAmount)
        
    }
    
    func serviceName() {
        var arrDiscountCoupon = NSArray()
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            //Getting Credit and Coupon Detail
            if(dictMaster.value(forKey: "DiscountSetupMasterCoupon") is NSArray) {
                arrDiscountCoupon = dictMaster.value(forKey: "DiscountSetupMasterCoupon") as! NSArray

                if (arrDiscountCoupon.count  > 0)
                {
                    for couponDetailTemp in arrDiscountCoupon {
                        print(couponDetailTemp)
                        let couponDetail: [String: Any] = couponDetailTemp as! [String : Any]
                        if couponDetail["Name"] as! String == "" {
                            self.dictGlobleCouponDetail = couponDetail
                            break
                        }else {
                        }
                    }
                }
                
                //Getting Tax Code List
                self.arrTaxCode = dictMaster["TaxMaster"] as! [[String : Any]]
                
                //Getting Agreement Check List
                arrayAgreementCheckListTemp = dictMaster["AgreementChecklist"] as! NSArray
                var arrTempAgreementList: [[String : Any]] = []
                for agreementDetail in self.arrayAgreementCheckListTemp {
                    
                    let dictAgreementDetail: [String: Any] = agreementDetail as! [String : Any]
                    let strIsActive = "\(dictAgreementDetail["IsActive"] ?? "0")"
                    
                    if strIsActive == "1" || strIsActive == "true" {
                        arrTempAgreementList.append(agreementDetail as! [String : Any])
                    }
                }
                self.arrayAgreementCheckListTemp = arrTempAgreementList as NSArray
                print(self.arrayAgreementCheckListTemp)
                self.fetchAgreementCheckListSales()
                
                let indexPath = IndexPath(row: 0, section: (6 - (3 - self.secTitleCount)))
                self.tblView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    func fetchAgreementCheckListSales()
    {
        arrayAgreementCheckList.removeAll()
        let arrayAgreementDetail = getDataFromCoreDataBaseArray(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        print(arrayAgreementDetail)
        if(arrayAgreementDetail.count > 0)
        {
            print(self.arrayAgreementCheckListTemp.count)
            if self.arrayAgreementCheckListTemp.count > arrayAgreementDetail.count {
                self.arrayAgreementCheckList = arrayAgreementDetail as! [Any];
                self.saveAgreementCheckList()

            }else {
                //self.arrayAgreementCheckList = arrayAgreementDetail as! [Any];
                
                for match in arrayAgreementDetail
                {
                    for obj in self.arrayAgreementCheckListTemp{
                       
                        if("\((match as! NSManagedObject).value(forKey: "agreementChecklistId")!)" == "\((obj as! NSDictionary).value(forKey: "AgreementChecklistId")!)")
                        {
                            var isNew = true
                            for newMatch in arrayAgreementCheckList {
                                if("\((match as! NSManagedObject).value(forKey: "agreementChecklistId")!)" == "\((newMatch as! NSManagedObject).value(forKey: "agreementChecklistId")!)")
                                {
                                    isNew = false
                                }
                            }
                            
                            if isNew {
                                arrayAgreementCheckList.append(match)
                            }
                            break
                        }
                          
                    }
                }
                
                
                //if Checklist not saved in Database then save
                if arrayAgreementCheckList.count < self.arrayAgreementCheckListTemp.count {
                    var arrTemp: NSMutableArray = []
                    for objTemp in self.arrayAgreementCheckListTemp {
                        var isMetch = false
                        for newTemp in arrayAgreementCheckList {
                            if("\((objTemp as! NSDictionary).value(forKey: "AgreementChecklistId")!)" == "\((newTemp as! NSManagedObject).value(forKey: "agreementChecklistId")!)")
                            {
                                isMetch = true
                                break
                            }
                        }
                        if !isMetch {
                            arrTemp.add(objTemp)
                        }
                    }
                    self.saveUnselectedAgreementCheckList(arrUnselectedAgreementCheckList: arrTemp)
                }

            }
        }else {
            self.saveAgreementCheckList()
        }
    }
    
    func saveUnselectedAgreementCheckList(arrUnselectedAgreementCheckList: NSArray) {
        var noDuplicates = [[String: Any]]()
        var usedChecklistId = [String]()
        for dict in arrUnselectedAgreementCheckList {
            print(dict)
            let dict1: [String: Any] = dict as! [String : Any]
            if let checklistId = dict1["AgreementChecklistId"] {
                noDuplicates.append(dict as! [String : Any])
                usedChecklistId.append("\(checklistId)")
            }
        }
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        for objCheckList in arrUnselectedAgreementCheckList {
            let dict: [String: Any] = objCheckList as! [String : Any]
            
            var isSave = true
            for i in 0..<self.arrayAgreementCheckList.count {
                var objAgree = NSManagedObject()
                objAgree = self.arrayAgreementCheckList[i] as! NSManagedObject
                print(objAgree)
                let dict1 = getAgreementCheckListObject(strCheckListId: "\(objAgree.value(forKey: "agreementChecklistId") ?? "")")
                print(dict1)
                if "\(dict["AgreementChecklistId"] ?? "")" == "\(dict1["AgreementChecklistId"] ?? "")" {
                    isSave = false
                    break
                }
            }
            if isSave {
                let context = getContext()
                let entityLeadAgreementChecklistSetups = LeadAgreementChecklistSetups(context: context)
                
                entityLeadAgreementChecklistSetups.leadId = self.strLeadId;
                entityLeadAgreementChecklistSetups.leadAgreementChecklistSetupId = "0";
                entityLeadAgreementChecklistSetups.agreementChecklistId = "\(dict["AgreementChecklistId"] ?? "")"
                entityLeadAgreementChecklistSetups.agreementChecklistSysName = "\(dict["SysName"] ?? "")"
                
                if "\(dict["IsActive"] ?? "0")" == "1" || "\(dict["IsActive"] ?? "false")" == "true" {
                    entityLeadAgreementChecklistSetups.isActive = "false"
                }
                else {
                    entityLeadAgreementChecklistSetups.isActive = "false"
                }
                entityLeadAgreementChecklistSetups.userName = strUserName;
                entityLeadAgreementChecklistSetups.companyKey = strCompanyKey;
                
                //save the object
                do { try context.save()
                    print("Record Saved Updated.")
                } catch let error as NSError {
                    debugPrint("Could not save. \(error), \(error.userInfo)")
                }
            }
            
        }
        self.arrayAgreementCheckList.removeAll()
        self.arrayAgreementCheckList = noDuplicates
        print(self.arrayAgreementCheckList)
        
        if arrayAgreementCheckList.count > 0
        {
            self.fetchAgreementCheckListSales()
        }
    }
    func fetchAgreementCheckListSalesOld() {
        let arrayAgreementDetail = getDataFromCoreDataBaseArray(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        print(arrayAgreementDetail)
        if(arrayAgreementDetail.count > 0)
        {
            print(self.arrayAgreementCheckListTemp.count)
            if self.arrayAgreementCheckListTemp.count > arrayAgreementDetail.count {
                self.arrayAgreementCheckList = arrayAgreementDetail as! [Any];
                self.saveAgreementCheckList()

            }else {
                self.arrayAgreementCheckList = arrayAgreementDetail as! [Any];
            }
        }else {
            self.saveAgreementCheckList()
        }
    }
    
    func saveAgreementCheckList() {
        var noDuplicates = [[String: Any]]()
        var usedChecklistId = [String]()
        for dict in self.arrayAgreementCheckListTemp {
            print(dict)
            let dict1: [String: Any] = dict as! [String : Any]
            if let checklistId = dict1["AgreementChecklistId"] {
                noDuplicates.append(dict as! [String : Any])
                usedChecklistId.append("\(checklistId)")
            }
        }
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        for objCheckList in self.arrayAgreementCheckListTemp {
            let dict: [String: Any] = objCheckList as! [String : Any]
            
            var isSave = true
            for i in 0..<self.arrayAgreementCheckList.count {
                var objAgree = NSManagedObject()
                objAgree = self.arrayAgreementCheckList[i] as! NSManagedObject
                print(objAgree)
                let dict1 = getAgreementCheckListObject(strCheckListId: "\(objAgree.value(forKey: "agreementChecklistId") ?? "")")
                print(dict1)
                if "\(dict["AgreementChecklistId"] ?? "")" == "\(dict1["AgreementChecklistId"] ?? "")" {
                    isSave = false
                    break
                }
            }
            if isSave {
                let context = getContext()
                let entityLeadAgreementChecklistSetups = LeadAgreementChecklistSetups(context: context)
                
                entityLeadAgreementChecklistSetups.leadId = self.strLeadId;
                entityLeadAgreementChecklistSetups.leadAgreementChecklistSetupId = "0";
                entityLeadAgreementChecklistSetups.agreementChecklistId = "\(dict["AgreementChecklistId"] ?? "")"
                entityLeadAgreementChecklistSetups.agreementChecklistSysName = "\(dict["SysName"] ?? "")"
                
                if "\(dict["IsActive"] ?? "0")" == "1" || "\(dict["IsActive"] ?? "false")" == "true" {
                    entityLeadAgreementChecklistSetups.isActive = "false"
                }
                else {
                    entityLeadAgreementChecklistSetups.isActive = "false"
                }
                entityLeadAgreementChecklistSetups.userName = strUserName;
                entityLeadAgreementChecklistSetups.companyKey = strCompanyKey;
                
                //save the object
                do { try context.save()
                    print("Record Saved Updated.")
                } catch let error as NSError {
                    debugPrint("Could not save. \(error), \(error.userInfo)")
                }
            }
            
        }
        self.arrayAgreementCheckList.removeAll()
        self.arrayAgreementCheckList = noDuplicates
        print(self.arrayAgreementCheckList)
        
        if arrayAgreementCheckList.count > 0
        {
            self.fetchAgreementCheckListSales()
        }
    }
    
    func billingFreqancyCalculationMethod() {
        self.arrMaintBillingPrice.removeAll()
        
        let arrUniqFreqSysName = Set(self.arrayOfFreqSysName)
        for value in arrUniqFreqSysName {
            var price = 0.0
            for i in 0..<self.arrayOfFreqSysName.count
            {
                let strVal = self.arrayOfFreqSysName[i]
                let strValPrice = self.arrayOfFreqPrice[i]
                if value == strVal {
                    price = price + (Double(strValPrice) ?? 0.0)
                }
            }
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: value)
            let frqName = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
            print(frqName)
            let strMaintBillingPrice = "\(String(format: "$%.2f", price))/\(frqName)"
            self.arrMaintBillingPrice.append(strMaintBillingPrice)
        }
    }
    
    func billingFreqancyCalculationOnCredit() {
        if self.arrAppliedCredit.count > 0 {
            var maintApplied = 0.0
            for objApplied in self.arrAppliedCredit {
                var objApplied1 = NSManagedObject()
                objApplied1 = objApplied
                let strMaint: String = "\(objApplied1.value(forKey: "appliedMaintDiscount") ?? "0")"
                maintApplied = maintApplied + (Double(strMaint) ?? 0.0)
            }
            print(maintApplied)

            let serviceSold: [NSManagedObject] = (self.arrStandardServiceSold + self.arrBundleServiceSold + self.arrCustomServiceSold)
            print(serviceSold)
            var strBilled = ""
            
            if serviceSold.count > 0 {
                var objCustomServiceDetail = NSManagedObject()
                objCustomServiceDetail = serviceSold[0]
                

                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
                let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")")

                
                var maint: Double = (Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0) - maintApplied//(((Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0) * unit) + sumFinalMaintPrice)
                if maint < 0
                {
                    maint = 0
                }
                
                
                //Billing Amount Calculation
                
                var totalMaintPrice = 0.0
                var totalBillingAmount = 0.0
                
                totalMaintPrice = Double(maint)

                let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
                let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

                if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                {
                }
                else
                {
                    if chkFreqConfiguration
                    {
                        if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                        {
                            totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                        else
                        {
                            totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                    }
                    else
                    {
                        totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                
                if totalBillingAmount < 0
                {
                    totalBillingAmount = 0.0
                }
                
                strBilled = "\(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                
                print(strBilled)
                
                //BillingFrequencyPrice - Billing Amount TotalMaintPrice - Total Maintenance Price
            }
            
            //Replace Object
            let arrBilled = strBilled.components(separatedBy: "/")
            
            print(self.arrMaintBillingPrice)
            print(self.arrayOfFreqSysName)
            print(self.arrayOfFreqPrice)

            for i in 0..<self.arrayOfFreqSysName.count
            {
                if self.arrayOfFreqSysName[i] == arrBilled[1] {
                    self.arrayOfFreqPrice[i] = arrBilled[0].replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil)
                    break
                }
            }
            print(self.arrayOfFreqPrice)

            self.arrMaintBillingPrice.removeAll()
            let arrUniqFreqSysName = Set(self.arrayOfFreqSysName)
            for value in arrUniqFreqSysName {
                var price = 0.0
                for i in 0..<self.arrayOfFreqSysName.count
                {
                    let strVal = self.arrayOfFreqSysName[i]
                    let strValPrice = self.arrayOfFreqPrice[i]
                    if value == strVal {
                        price = price + (Double(strValPrice) ?? 0.0)
                    }
                }
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: value)
                let frqName = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                print(frqName)
                let strMaintBillingPrice = "\(String(format: "$%.2f", price))/\(frqName)"
                self.arrMaintBillingPrice.append(strMaintBillingPrice)
            }
            print(self.arrMaintBillingPrice)
        }
    }
    
    func serviceTaxableStatus() {
        let name: NSMutableArray = NSMutableArray()
        let sysName: NSMutableArray = NSMutableArray()
        let commercialVal: NSMutableArray = NSMutableArray()
        let residentialVal: NSMutableArray = NSMutableArray()
        
        if nsud.value(forKey: "MasterSalesAutomation") != nil {
            let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if dictMasters.value(forKey: "Categories") != nil {
                let arrCatergory = dictMasters.value(forKey: "Categories") as! [Any]
                for i in 0..<arrCatergory.count {
                    let dict: NSDictionary = arrCatergory[i] as! NSDictionary
                    
                    let arrServices = dict["Services"] as? [AnyHashable]
                    for j in 0..<(arrServices?.count ?? 0) {
                        let dict: NSDictionary = (arrServices?[j] as? NSDictionary)!
                        var str: String?
                        if let value = dict["IsTaxable"] {
                            str = "\(value)"
                        }
                        print("str>>>>\(str ?? "")")
                        name.add(str ?? "")
                        if let value = dict["SysName"] {
                            sysName.add("\(value)")
                        }
                        if let value = dict["IsCommercialTaxable"] {
                            commercialVal.add("\(value)")
                        }
                        if let value = dict["IsResidentialTaxable"] {
                            residentialVal.add("\(value)")
                        }
                    }
                }
            }
        }
        
        self.dictTaxStatus = NSDictionary(objects:name as! [Any], forKeys:sysName as! [NSCopying])
        self.dictCommercialStatus = NSDictionary(objects:commercialVal as! [Any], forKeys:sysName as! [NSCopying])
        self.dictResidentialStatus = NSDictionary(objects:residentialVal as! [Any], forKeys:sysName as! [NSCopying])
        print(self.dictTaxStatus, self.dictCommercialStatus, self.dictResidentialStatus)
    }
    
    func serviceTaxCalculation() {
        
        
        let stdInitialStanTax = self.initialStanTax - self.initialNonStanTax
        let stdMnitialMaintTax = self.initialMaintTax - self.initialNonMaintTax
        let stdDiscountStanTax = self.discountStanTax - self.couponDiscountPrice - self.discountNonStanTax
        self.taxableInitialAmount = (stdInitialStanTax - stdDiscountStanTax) + (initialNonStanTax - self.discountNonStanTax)
        self.taxableMaintAmount = (self.initialMaintTax - creditAmountMaint)
        
        print("Total: ", self.initialStanTax, self.discountStanTax, self.initialMaintTax)
        print("Discount Credit: ", creditAmount, creditAmountMaint);
        print("Discount Coupon: ", self.couponDiscountPrice);
        print("Total Std: ", stdInitialStanTax, stdDiscountStanTax, stdMnitialMaintTax)
        print("Total Non Std: ", self.initialNonStanTax, self.discountNonStanTax, self.initialNonMaintTax)
        
        //Calculation Start
        let taxStd = ((stdInitialStanTax - stdDiscountStanTax)*(Double(strTaxValue) ?? 0.0))/100
        let taxNonStd = ((initialNonStanTax - self.discountNonStanTax)*(Double(strTaxValue) ?? 0.0))/100
        var totalTax = taxStd + taxNonStd
        var totalMaintTax = ((self.initialMaintTax - creditAmountMaint) * (Double(strTaxValue) ?? 0.0)) / 100
        //Coupon Credit Discount Calculation
        if self.couponDiscountPrice > 0 {
            let calculateTaxDiscount = self.initialStanTax - self.discountStanTax
            self.taxableInitialAmount = calculateTaxDiscount
            totalTax = (calculateTaxDiscount*(Double(strTaxValue) ?? 0.0))/100
        }
        if creditAmount > 0 {
            //Initial Tax Calculation
            let calculateTaxDiscount = taxCalculation()
            
            let stdVal = calculateTaxDiscount
            let nonStdVal = (initialNonStanTax - self.discountNonStanTax)
            
            let totTaxableVal = (stdVal + nonStdVal)
            self.taxableInitialAmount = totTaxableVal
            
            totalTax = (totTaxableVal*(Double(strTaxValue) ?? 0.0))/100
            
            //Maint Tax Calculation
        }
        
        if creditAmountMaint > 0 {
            //Maint Tax Calculation
            let calculateMaintTaxDiscount = taxCalculationForMaint() + self.initialNonMaintTax
            self.taxableMaintAmount = calculateMaintTaxDiscount
            totalMaintTax = (calculateMaintTaxDiscount*(Double(strTaxValue) ?? 0.0))/100
        }
        
        self.taxAmount = Double(round(100*totalTax)/100)
        self.taxMaintAmount = Double(round(100*totalMaintTax)/100)
        
        print(self.taxAmount, self.taxMaintAmount)
        print(self.taxableInitialAmount, self.taxableMaintAmount)
        self.updatePricingInfoDict()
    }
    
    func taxCalculation()-> Double {
        
        var taxCalculationCoupon = 0.0
        var taxCalculationCredit = 0.0
        var taxCalculationTotal = 0.0
        
       // taxCalculationCoupon = fetchFromCoreDataStandardForTax(arrCreditCoupon: self.arrAppliedCoupon, type: "Coupon")
        taxCalculationCredit = fetchFromCoreDataStandardForTax(arrCreditCoupon: self.arrAppliedCredit, type: "Credit")
        if(taxCalculationCredit < 0)
        {
            taxCalculationCredit = 0;
        }
        
        taxCalculationTotal = taxCalculationCoupon + taxCalculationCredit;
        
        return taxCalculationTotal
    }
    
    func fetchFromCoreDataStandardForTax(arrCreditCoupon: [NSManagedObject], type: String) -> Double {
        
        var arrDiscountCreditInitialTemp: [NSManagedObject] = []
        var arrDiscountCouponInitialTemp: [NSManagedObject] = []

        var arrAllObjTax = self.arrStandardServiceSold + self.arrBundleServiceSold
        print(arrAllObjTax)
        
        var totalTaxApplicableAmountCoupon = 0.0
        
        if arrCreditCoupon.count > 0 {
            for credit in arrCreditCoupon {
                
                if ("\(credit.value(forKey: "applicableForInitial") ?? "0")" == "1" || "\(credit.value(forKey: "applicableForInitial") ?? "0")" == "true") {
                    arrDiscountCreditInitialTemp.append(credit)
                }
                
                if ("\(credit.value(forKey: "discountType") ?? "")" == "coupon" || "\(credit.value(forKey: "discountType") ?? "")" == "Coupon") {
                    arrDiscountCouponInitialTemp.append(credit)
                }
            }
        }
        
        for objStdSold in arrAllObjTax {
            let matchesAllObjTax: NSManagedObject = objStdSold
            print(matchesAllObjTax)
            
            //Tax Calculations
            if self.strIsServiceAddrTaxExempt == "false" {
                
                if "\(matchesAllObjTax.value(forKey: "serviceSysName") ?? "")".count>0
                {
                    if strServiceAddressSubType == "Commercial" {
                        if self.dictCommercialStatus.count > 0 {
                            if self.dictCommercialStatus.value(forKey: matchesAllObjTax.value(forKey: "serviceSysName") as! String) as! String == "1" {
                                var totalCouponDiscount = 0.0
                                var totalCreditDiscount = 0.0
                                
                                //Coupon
                                for coupon in arrDiscountCouponInitialTemp {
                                    let matchesTaxCoupon: NSManagedObject = coupon
                                    totalCouponDiscount = 0.0
                                    
                                    let strCouponSID: String = "\(matchesTaxCoupon.value(forKey: "soldServiceId") ?? "")"
                                    let strServiceSID: String = "\(matchesAllObjTax.value(forKey: "soldServiceStandardId") ?? "")"
                                    
                                    if strCouponSID == strServiceSID {
                                        let couponAmt = "\(matchesTaxCoupon.value(forKey: "appliedInitialDiscount") ?? "0.0")"
                                        totalCouponDiscount = totalCouponDiscount + (Double(couponAmt) ?? 0.0)
                                    }
                                }
                                
                                //Credit
                                for credit in arrDiscountCreditInitialTemp {
                                    totalCreditDiscount = 0.0
                                    let matchesTaxCoupon: NSManagedObject = credit
                                    
                                    let strCouponSID: String = "\(matchesTaxCoupon.value(forKey: "soldServiceId") ?? "")"
                                    let strServiceSID: String = "\(matchesAllObjTax.value(forKey: "soldServiceStandardId") ?? "")"
                                    
                                    if strCouponSID == strServiceSID {
                                        let creditAmt = "\(matchesTaxCoupon.value(forKey: "appliedInitialDiscount") ?? "0.0")"
                                        totalCreditDiscount = totalCreditDiscount + (Double(creditAmt) ?? 0.0)
                                    }
                                    
                                }
                                //End
                                
                                var otherDiscount = 0.0
                                if (Double("\(matchesAllObjTax.value(forKey: "discount") ?? "0")") ?? 0.0) > totalCouponDiscount {
                                    otherDiscount = (Double("\(matchesAllObjTax.value(forKey: "discount") ?? "0")") ?? 0.0) - totalCouponDiscount
                                }else {
                                    otherDiscount = (Double("\(matchesAllObjTax.value(forKey: "discount") ?? "0")") ?? 0.0)
                                    
                                }
                                print(otherDiscount)
                                
                                var unitValue = 0.0
                                unitValue = (Double("\(matchesAllObjTax.value(forKey: "unit") ?? "0")") ?? 0.0)
                                if unitValue == 0 {
                                    unitValue = 1
                                }
                                
                                totalTaxApplicableAmountCoupon = totalTaxApplicableAmountCoupon + (Double("\(matchesAllObjTax.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) - totalCouponDiscount - otherDiscount - totalCreditDiscount;
                            }
                        }

                    }
                }
                else if strServiceAddressSubType == "Residential" {
                    
                    if "\(matchesAllObjTax.value(forKey: "serviceSysName") ?? "")".count>0
                    {
                        if self.dictCommercialStatus.count > 0 {
                            if self.dictCommercialStatus.value(forKey: matchesAllObjTax.value(forKey: "serviceSysName") as! String) as! String == "1" {
                                var totalCouponDiscount = 0.0
                                var totalCreditDiscount = 0.0
                                
                                //Coupon
                                for coupon in arrDiscountCouponInitialTemp {
                                    let matchesTaxCoupon: NSManagedObject = coupon
                                    totalCouponDiscount = 0.0
                                    
                                    let strCouponSID: String = "\(matchesTaxCoupon.value(forKey: "soldServiceId") ?? "")"
                                    let strServiceSID: String = "\(matchesAllObjTax.value(forKey: "soldServiceStandardId") ?? "")"
                                    
                                    if strCouponSID == strServiceSID {
                                        let couponAmt = "\(matchesTaxCoupon.value(forKey: "appliedInitialDiscount") ?? "0.0")"
                                        totalCouponDiscount = totalCouponDiscount + (Double(couponAmt) ?? 0.0)
                                    }
                                }
                                
                                //Credit
                                for credit in arrDiscountCreditInitialTemp {
                                    totalCreditDiscount = 0.0
                                    let matchesTaxCoupon: NSManagedObject = credit
                                    
                                    let strCouponSID: String = "\(matchesTaxCoupon.value(forKey: "soldServiceId") ?? "")"
                                    let strServiceSID: String = "\(matchesAllObjTax.value(forKey: "soldServiceStandardId") ?? "")"
                                    
                                    if strCouponSID == strServiceSID {
                                        let creditAmt = "\(matchesTaxCoupon.value(forKey: "appliedInitialDiscount") ?? "0.0")"
                                        totalCreditDiscount = totalCreditDiscount + (Double(creditAmt) ?? 0.0)
                                    }
                                    
                                }
                                //End
                                
                                var otherDiscount = 0.0
                                if (Double("\(matchesAllObjTax.value(forKey: "discount") ?? "0")") ?? 0.0) > totalCouponDiscount {
                                    otherDiscount = (Double("\(matchesAllObjTax.value(forKey: "discount") ?? "0")") ?? 0.0) - totalCouponDiscount
                                }else {
                                    otherDiscount = (Double("\(matchesAllObjTax.value(forKey: "discount") ?? "0")") ?? 0.0)
                                    
                                }
                                print(otherDiscount)
                                
                                var unitValue = 0.0
                                unitValue = (Double("\(matchesAllObjTax.value(forKey: "unit") ?? "0")") ?? 0.0)
                                if unitValue == 0 {
                                    unitValue = 1
                                }
                                
                                totalTaxApplicableAmountCoupon = totalTaxApplicableAmountCoupon + (Double("\(matchesAllObjTax.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) - totalCouponDiscount - otherDiscount - totalCreditDiscount;
                            }
                        }
                    }
                }
            }
        }
        
        
        return totalTaxApplicableAmountCoupon
    }
    
    func taxCalculationForMaint() -> Double {
        var taxCalculationCreditMaint = 0.0
        
        taxCalculationCreditMaint = fetchFromCoreDataStandardForTaxForMaint()
        return taxCalculationCreditMaint
    }
    
    func fetchFromCoreDataStandardForTaxForMaint()-> Double {
        var arrDiscountCreditMaintTemp: [NSManagedObject] = []
        
        var arrAllObjTax = self.arrStandardServiceSold + self.arrBundleServiceSold
        print(arrAllObjTax)
        
        var totalTaxApplicableAmountCoupon = 0.0
        if self.arrAppliedCredit.count > 0 {
            for credit in self.arrAppliedCredit {
                
                if ("\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "1" || "\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "true") {
                    arrDiscountCreditMaintTemp.append(credit)
                }
            }
        }
        
        
        for objStdSold in arrAllObjTax {
            let matchesAllObjTax: NSManagedObject = objStdSold
            print(matchesAllObjTax)
            
            //Tax Calculations
            if self.strIsServiceAddrTaxExempt == "false" {
                if strServiceAddressSubType == "Commercial" {
                    
                    if "\(matchesAllObjTax.value(forKey: "serviceSysName") ?? "")".count>0
                    {
                        if self.dictCommercialStatus.value(forKey: matchesAllObjTax.value(forKey: "serviceSysName") as! String) as! String == "1" {
                            
                            var totalCreditDiscount = 0.0
                            
                            //Credit
                            for credit in arrDiscountCreditMaintTemp {
                                totalCreditDiscount = 0.0
                                let matchesTaxCoupon: NSManagedObject = credit
                                
                                let strCouponSID: String = "\(matchesTaxCoupon.value(forKey: "soldServiceId") ?? "")"
                                let strServiceSID: String = "\(matchesAllObjTax.value(forKey: "soldServiceStandardId") ?? "")"
                                
                                if strCouponSID == strServiceSID {
                                    let creditAmt = "\(matchesTaxCoupon.value(forKey: "appliedMaintDiscount") ?? "0.0")"
                                    totalCreditDiscount = totalCreditDiscount + (Double(creditAmt) ?? 0.0)
                                }
                                
                            }
                            //End
                            
                            
                            totalTaxApplicableAmountCoupon = totalTaxApplicableAmountCoupon + (Double("\(matchesAllObjTax.value(forKey: "totalMaintPrice") ?? "0")") ?? 0.0) - totalCreditDiscount;
                        }
                    }
                }else if strServiceAddressSubType == "Residential" {
                    
                    if "\(matchesAllObjTax.value(forKey: "serviceSysName") ?? "")".count>0
                    {
                        
                        if self.dictCommercialStatus.value(forKey: matchesAllObjTax.value(forKey: "serviceSysName") as! String) as! String == "1" {
                            
                            var totalCreditDiscount = 0.0
                            
                            //Credit
                            for credit in arrDiscountCreditMaintTemp {
                                totalCreditDiscount = 0.0
                                let matchesTaxCoupon: NSManagedObject = credit
                                
                                let strCouponSID: String = "\(matchesTaxCoupon.value(forKey: "soldServiceId") ?? "")"
                                let strServiceSID: String = "\(matchesAllObjTax.value(forKey: "soldServiceStandardId") ?? "")"
                                
                                if strCouponSID == strServiceSID {
                                    let creditAmt = "\(matchesTaxCoupon.value(forKey: "appliedInitialDiscount") ?? "0.0")"
                                    totalCreditDiscount = totalCreditDiscount + (Double(creditAmt) ?? 0.0)
                                }
                                
                            }
                            //End
                            
                            
                            totalTaxApplicableAmountCoupon = totalTaxApplicableAmountCoupon + (Double("\(matchesAllObjTax.value(forKey: "totalMaintPrice") ?? "0")") ?? 0.0) - totalCreditDiscount;
                        }
                    }
                }
            }
        }
        
        return totalTaxApplicableAmountCoupon
    }
    
    @IBAction func backcrtion(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func sendProposalAction(_ sender: Any) {
        let serviceCount = (self.arrStandardServiceNotSold.count + self.arrBundleServiceNotSold.count + self.arrCustomServiceNotSold.count)
        
        if serviceCount > 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "FormBuilder" : "FormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesReviewViewController") as? SalesReviewViewController
            vc?.modalPresentationStyle = .fullScreen
            
            vc!.isSendProposal = true
            vc!.matchesGeneralInfo = self.objLeadDetails!
            vc!.objPaymentInfo = self.objPaymentInfo
            vc!.arrayOfMonth = self.arrayOfMonth
            vc!.dictPricingInfo = self.dictPricingInfo
            vc!.arrStandardServiceProposal = self.arrStandardServiceNotSold
            vc!.arrCustomServiceProposal = self.arrCustomServiceNotSold
            vc!.arrBundleServiceProposal = self.arrBundleServiceNotSold
            fetchServiceProposalFollowUp()
            self.navigationController?.pushViewController(vc!, animated: false)
        }else {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To proceed, please add atleast one service to send proposal.", viewcontrol: self)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        Global().updateSalesModifydate(strLeadId as String)
        
        let serviceCount = (self.arrStandardServiceSold.count + self.arrBundleServiceSold.count + self.arrCustomServiceSold.count)
        
        if serviceCount > 0 {
            if (nsud.bool(forKey: "TaxCodeReq")) && self.strTaxCode.isEmpty && !getOpportunityStatus()
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Tax code is required, Please select tax code.", viewcontrol: self)
                
            }else {
                if serviceCount > 1 || self.arrBundleServiceSold.count > 0 {
                    
                    self.goToReview()
                    
                }else {
                    
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(self.loader, animated: false, completion: nil)
                    
                    //Getting TemplateKey For Form Builder
                    let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                    
                    let strNewTaxUrl = "/api/MobileToSaleAuto/GetSalesReportTemplateByServiceForMobile?companyKey="
                    self.strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
                    
                    let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
                    
                    var categorySysName = ""
                    var serviceSysName = ""
                    var nonStdDepartmentSysName = ""
                    
                    if self.arrStandardServiceSold.count > 0 {
                        let objeStandardService: NSManagedObject = self.arrStandardServiceSold[0]
                        print(objeStandardService)
                        serviceSysName = objeStandardService.value(forKey: "serviceSysName") as! String
                        let catObj = getCategoryObjectFromServiceSysName(strServiceSysName: serviceSysName)//objeStandardService.value(forKey: "categorySysName") as! String
                        if catObj.value(forKey: "SysName") != nil {
                            categorySysName = catObj.value(forKey: "SysName") as! String
                        }else {
                            categorySysName = ""
                        }
                        print("Standard")
                    }else {
                        let objeNonStandardService: NSManagedObject = self.arrCustomServiceSold[0]
                        print(objeNonStandardService)
                        nonStdDepartmentSysName = objeNonStandardService.value(forKey: "departmentSysname") as! String
                        
                        print("Non Standard")
                        
                    }
                    
                    let strUrl = "\(strServiceUrlMain)\(strNewTaxUrl)\(strCompanyKey)&serviceSysName=\(serviceSysName)&categorySysName=\(categorySysName)&nonStdDepartmentSysName=\(nonStdDepartmentSysName)&flowtype=Residential"
                    
                    if getOpportunityStatus()
                    {
                        self.loader.dismiss(animated: false) {
                            if "\(self.objLeadDetails?.value(forKey: "pdfPath") ?? "")".count > 0 && "\(self.objLeadDetails?.value(forKey: "templateKey") ?? "")".count > 0
                            {
                                self.goToFormBuilder(dict: [:])
                            }
                            else
                            {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                                    self.goToReview()
                                    
                                })
                            }
                        }
                        
                        //self.goToFormBuilder(dict: [:])
                    }
                    else
                    {
                        if ReachabilityTest.isConnectedToNetwork() {
                            print("Internet connection available")
                            //self.presenter.requestToGetTemplateKey(apiUrl: strUrl)
                           
                            
                            //loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                            //self.present(self.loader, animated: false, completion: nil)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                                
                                self.getTemplateKey(strURL: strUrl)
                                
                            }
                            
                        }
                        else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No internet connection available", viewcontrol: self)
                        }
                    }
                }
            }
            
        }else {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To proceed, please add atleast one service to the review.", viewcontrol: self)
        }
        
    }
    
    @IBAction func action_Home(_ sender: Any) {
        self.view.endEditing(true)
        self.goToAppointment()
    }
    
    //MARK: - ------------------ Footer Actions ---------------------
    
    
    func goToEmailHistory()  {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
              let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
              testController?.modalPresentationStyle = .fullScreen
              testController?.strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
              testController?.strCompanyKey = strCompanyKey
              testController?.strUserName = strUserName

              self.present(testController!, animated: false, completion: nil)
    }
    
    func goToServiceHistory()  {
        
        /*let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = "\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
        
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
        
        
    }
    func goToServiceNotesHistory()  {
        
        //ServiceNotesHistoryViewController
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController?.strTypeOfService = "sales";
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    
    
    
    @IBAction func actionOnSetting(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
            self.view.endEditing(true)
            
            self.goToEmailHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
        
            self.goToSetupHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
        
            self.goToServiceNotesHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
        
            self.goToServiceHistory()
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
           // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.up]
        }
        self.present(alert, animated: true)
       
      }
    
    @IBAction func actionOnImage(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Make Your Selection", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.AddImages, style: .default, handler: { action in
            self.GotoGlobleImageGraphView(strType: "Before")
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.AddDocuments, style: .default, handler: { [self] action in
            let strLeadID =  strLeadId

            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: DeviceType.IS_IPAD ? "UploadDocumentSalesiPad" : "UploadDocumentSales") as! UploadDocumentSales
            vc.strWoId = "\(strLeadID)" as NSString
            self.navigationController?.present(vc, animated: true, completion: nil)

        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
      //      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.down]
        }
        self.present(alert, animated: true)
    }
    
    @IBAction func actionOnGraph(_ sender: Any) {
        
        self.view.endEditing(true)
        nsud.setValue(true, forKey: "firstGraphImage")
        nsud.setValue(false, forKey: "servicegraph")
        GotoGlobleImageGraphView(strType: "Graph")
    }
    
    @IBAction func actionOnChemicalSensitivityList(_ sender: Any) {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoGlobleImageGraphView(strType : String)  {
        
        let strLeadID =  matchesGeneralInfo.value(forKey: "leadId") ?? ""
        var strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
        var strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
        if(nsud.value(forKey: "backFromInspectionNew") != nil){
            
            if(nsud.value(forKey: "backFromInspectionNew") as! Bool == true){
                strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
                strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
            }
        }
    
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPhone" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        vc.strHeaderTitle = strType as NSString
        vc.strWoId = "\(strLeadID)" as NSString
        vc.strWoLeadId = "\(strLeadID)" as NSString
        vc.strModuleType = "SalesFlow"
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won"){
            vc.strWoStatus = "Complete"
        }else{
            vc.strWoStatus = "InComplete"
        }
        vc.strWoStatus = "InComplete"
        self.navigationController?.present(vc, animated: true, completion: nil)

    }
    
    func goToReview()
    {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "FormBuilder" : "FormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesReviewViewController") as? SalesReviewViewController
        vc?.modalPresentationStyle = .fullScreen
        
        vc!.isSendProposal = false
        vc!.matchesGeneralInfo = self.objLeadDetails!
        vc!.objPaymentInfo = self.objPaymentInfo
        vc!.arrayAgreementCheckList = self.arrayAgreementCheckList
        vc!.arrayOfMonth = self.arrayOfMonth
        
        vc!.arrStandardServiceReview = self.arrStandardServiceSold
        vc!.arrCustomServiceReview = self.arrCustomServiceSold
        vc!.arrBundleServiceReview = self.arrBundleServiceSold
        vc!.arrStandardServiceProposal = self.arrStandardServiceNotSold
        vc!.arrCustomServiceProposal = self.arrCustomServiceNotSold
        vc!.arrBundleServiceProposal = self.arrBundleServiceNotSold
        
        vc!.dictPricingInfo = self.dictPricingInfo
        vc!.arrAppliedCoupon = self.arrAppliedCoupon
        vc!.arrAppliedCredit = self.arrAppliedCredit
        fetchServiceProposalFollowUp()
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func goToAppointment()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            

            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    @IBAction func markAsLostAction(_ sender: Any)
    {
        Global().updateSalesModifydate(strLeadId as String)
        
        let alert = UIAlertController(title: alertMessage, message: "Are you sure to mark Opportunity as lost?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "YES", style: .default, handler: { (nil) in
            self.chkForLost = true
            
            self.objLeadDetails?.setValue("Complete", forKey: "statusSysName")
            self.objLeadDetails?.setValue("Lost", forKey: "stageSysName")

            let context = getContext()
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
            self.goToAppointment()
        }))
        alert.addAction(UIAlertAction (title: "NO", style: .cancel, handler: { (nil) in
            self.chkForLost = false
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
       // vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    
    
    //Nilind
    
}



// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SalesConfigureViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        print("Section: \(self.arrSectionTitle.count)")
        return self.arrSectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.secTitleCount == 1 {
            if section == 0 {
                if arrSectionTitle[section] == "Standard Service" {
                    return  self.arrStandardService.count
                }else if arrSectionTitle[section] == "Custom Service" {
                    return  self.arrCustomService.count
                }else {
                    return  1
                }
            }
        }else if self.secTitleCount == 2 {
            if section == 0 {
                if arrSectionTitle[section] == "Standard Service" {
                    return  self.arrStandardService.count
                }else if arrSectionTitle[section] == "Custom Service" {
                    return  self.arrCustomService.count
                }else {
                    return  1
                }
            }else if section == 1 {
                if arrSectionTitle[section] == "Custom Service" {
                    return  self.arrCustomService.count
                }else {
                    return  1
                }
            }
            
        }else if self.secTitleCount == 3 {
            if section == 0 {
                if arrSectionTitle[section] == "Standard Service" {
                    return  self.arrStandardService.count
                }else if arrSectionTitle[section] == "Custom Service" {
                    return  self.arrCustomService.count
                }else {
                    return  1
                }
            }else if section == 1 {
                if arrSectionTitle[section] == "Custom Service" {
                    return  self.arrCustomService.count
                }else {
                    return  1
                }
            }else if section == 2 {
                return  1
            }
        }
        
        
        if(section == (3 - (3 - self.secTitleCount))){
            return  1
        }else if(section == (4 - (3 - self.secTitleCount))){
            return  arrNotes.count
        }else {
            return  1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        //print(arrSectionTitle[indexPath.section])
        print(self.secTitleCount)
        
        if self.secTitleCount == 1 {
            if indexPath.section == 0 {
                if arrSectionTitle[indexPath.section] == "Standard Service" {
                    //Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ConfigureServiceTableViewCell
                    cell.delegate = self

                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrStandardService[indexPath.row]
                    cell.setStandardServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    let strRenewal = self.fetchRenewalServiceFromCoreData(soldServiceId: "\(objCustomServiceDetail.value(forKey: "serviceId") ?? "")")
                    
                    cell.lblRenewalDecription.isHidden = true
                    cell.lineViewR.isHidden = true
                    if strRenewal != "" {
                        cell.lblRenewalDecription.isHidden = false
                        cell.lineViewR.isHidden = false
//                        let myString = "Renewal: "
//                        cell.lblRenewalDecription.text = myString + strRenewal
                        let myString = NSAttributedString(string: "Renewal: ", attributes: [
                            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)
                        ])

                        let mutableAttString = NSMutableAttributedString()

                        mutableAttString.append(myString)
                        mutableAttString.append(NSAttributedString(string: strRenewal))

                        cell.lblRenewalDecription.attributedText = mutableAttString

                    }
                    
                    if getOpportunityStatus() {
                        cell.btnIsAgreement.isUserInteractionEnabled = false
                    }
                    return cell
                }else if arrSectionTitle[indexPath.section] == "Custom Service" {
                    //Custom Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ConfigureServiceTableViewCell
                    cell.delegate = self

                    
                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrCustomService[indexPath.row]
                    cell.setCustomServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    cell.lblRenewalDecription.isHidden = true
                    cell.lineViewR.isHidden = true
                    
                    if getOpportunityStatus() {
                        cell.btnIsAgreement.isUserInteractionEnabled = false
                        cell.btnIsTaxable.isUserInteractionEnabled = false
                    }
                    return cell
                }else {
                    //Bundle Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceCell", for: indexPath as IndexPath) as! ConfigBundleServiceTableViewCell
                    cell.delegate = self
                    cell.configureBundleTebleView(arrBundleService: self.arrBundleService, isCompleteWon: getOpportunityStatus())

                    return cell
                }
                
                
            }
        }else if self.secTitleCount == 2 {
            if indexPath.section == 0 {
                if arrSectionTitle[indexPath.section] == "Standard Service" {
                    //Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ConfigureServiceTableViewCell
                    cell.delegate = self

                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrStandardService[indexPath.row]
                    cell.setStandardServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    
                    
                    let strRenewal = self.fetchRenewalServiceFromCoreData(soldServiceId: "\(objCustomServiceDetail.value(forKey: "serviceId") ?? "")")
                    cell.lblRenewalDecription.isHidden = true
                    cell.lineViewR.isHidden = true
                    if strRenewal != "" {
                        cell.lblRenewalDecription.isHidden = false
                        cell.lineViewR.isHidden = false
//                        let myString = "Renewal: "
//                        cell.lblRenewalDecription.text = myString + strRenewal
                        let myString = NSAttributedString(string: "Renewal: ", attributes: [
                            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)
                        ])

                        let mutableAttString = NSMutableAttributedString()

                        mutableAttString.append(myString)
                        mutableAttString.append(NSAttributedString(string: strRenewal))

                        cell.lblRenewalDecription.attributedText = mutableAttString

                    }
                    
                    if getOpportunityStatus() {
                        cell.btnIsAgreement.isUserInteractionEnabled = false
                    }
                    return cell
                }else if arrSectionTitle[indexPath.section] == "Custom Service" {
                    //Custom Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ConfigureServiceTableViewCell
                    cell.delegate = self

                    
                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrCustomService[indexPath.row]
                    cell.setCustomServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    cell.lblRenewalDecription.isHidden = true
                    cell.lineViewR.isHidden = true
                    
                    if getOpportunityStatus() {
                        cell.btnIsAgreement.isUserInteractionEnabled = false
                        cell.btnIsTaxable.isUserInteractionEnabled = false
                    }
                    return cell
                }else {
                    //Bundle Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceCell", for: indexPath as IndexPath) as! ConfigBundleServiceTableViewCell
                    cell.delegate = self
                    cell.configureBundleTebleView(arrBundleService: self.arrBundleService, isCompleteWon: getOpportunityStatus())

                    return cell
                }
                
                
            }else if(indexPath.section == 1) {
                if arrSectionTitle[indexPath.section] == "Custom Service" {
                    //Custom Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ConfigureServiceTableViewCell
                    cell.delegate = self

                    
                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrCustomService[indexPath.row]
                    cell.setCustomServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    cell.lblRenewalDecription.isHidden = true
                    cell.lineViewR.isHidden = true
                    
                    if getOpportunityStatus() {
                        cell.btnIsAgreement.isUserInteractionEnabled = false
                        cell.btnIsTaxable.isUserInteractionEnabled = false
                    }
                    return cell
                }else {
                    //Bundle Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceCell", for: indexPath as IndexPath) as! ConfigBundleServiceTableViewCell
                    cell.delegate = self
                    cell.configureBundleTebleView(arrBundleService: self.arrBundleService, isCompleteWon: getOpportunityStatus())

                    return cell
                }
                
            }
        }else if self.secTitleCount == 3 {
            if indexPath.section == 0 {
                if arrSectionTitle[indexPath.section] == "Standard Service" {
                    //Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ConfigureServiceTableViewCell
                    cell.delegate = self

                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrStandardService[indexPath.row]
                    cell.setStandardServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    
                    let strRenewal = self.fetchRenewalServiceFromCoreData(soldServiceId: "\(objCustomServiceDetail.value(forKey: "serviceId") ?? "")")
                    
                    cell.lblRenewalDecription.isHidden = true
                    cell.lineViewR.isHidden = true
                    if strRenewal != "" {
                        cell.lblRenewalDecription.isHidden = false
                        cell.lineViewR.isHidden = false
//                        let myString = "Renewal: "
//                        cell.lblRenewalDecription.text = myString + strRenewal
                        let myString = NSAttributedString(string: "Renewal: ", attributes: [
                            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)
                        ])

                        let mutableAttString = NSMutableAttributedString()

                        mutableAttString.append(myString)
                        mutableAttString.append(NSAttributedString(string: strRenewal))

                        cell.lblRenewalDecription.attributedText = mutableAttString

                    }

                    if getOpportunityStatus() {
                        cell.btnIsAgreement.isUserInteractionEnabled = false
                    }
                    return cell
                }else if arrSectionTitle[indexPath.section] == "Custom Service" {
                    //Custom Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ConfigureServiceTableViewCell
                    cell.delegate = self

                    
                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrCustomService[indexPath.row]
                    cell.setCustomServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    cell.lblRenewalDecription.isHidden = true
                    cell.lineViewR.isHidden = true
                    
                    if getOpportunityStatus() {
                        cell.btnIsAgreement.isUserInteractionEnabled = false
                        cell.btnIsTaxable.isUserInteractionEnabled = false
                    }
                    return cell
                }else {
                    //Bundle Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceCell", for: indexPath as IndexPath) as! ConfigBundleServiceTableViewCell
                    cell.delegate = self
                    cell.configureBundleTebleView(arrBundleService: self.arrBundleService, isCompleteWon: getOpportunityStatus())

                    return cell
                }
                
                
            }else if(indexPath.section == 1) {
                if arrSectionTitle[indexPath.section] == "Custom Service" {
                    //Custom Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ConfigureServiceTableViewCell
                    cell.delegate = self

                    
                    var objCustomServiceDetail = NSManagedObject()
                    objCustomServiceDetail = self.arrCustomService[indexPath.row]
                    cell.setCustomServiceValues(objCustomServiceDetail: objCustomServiceDetail)
                    cell.lblRenewalDecription.isHidden = true
                    cell.lineViewR.isHidden = true
                    
                    if getOpportunityStatus() {
                        cell.btnIsAgreement.isUserInteractionEnabled = false
                        cell.btnIsTaxable.isUserInteractionEnabled = false
                    }
                    return cell
                }else {
                    //Bundle Service
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceCell", for: indexPath as IndexPath) as! ConfigBundleServiceTableViewCell
                    cell.delegate = self
                    cell.configureBundleTebleView(arrBundleService: self.arrBundleService, isCompleteWon: getOpportunityStatus())

                    return cell
                }
                
            }else if(indexPath.section == 2) {
                //Bundle Service
                let cell = tableView.dequeueReusableCell(withIdentifier: "BundleServiceCell", for: indexPath as IndexPath) as! ConfigBundleServiceTableViewCell
                cell.delegate = self
                cell.configureBundleTebleView(arrBundleService: self.arrBundleService, isCompleteWon: getOpportunityStatus())

                return cell
                
            }
        }
        
        
         if(indexPath.section == (3 - (3 - self.secTitleCount))) {
            //Pricing Info
            let cell = tableView.dequeueReusableCell(withIdentifier: "PricingCell", for: indexPath as IndexPath) as! ConfigurePricingTableViewCell
 
            cell.delegate = self
            
            /*Coupon Details*/
            if self.arrAppliedCoupon.count > 0 {
                cell.tableSetUp(arrAppliedCoupon : self.arrAppliedCoupon)
            }else {
                cell.arrAppliedCoupon.removeAll()
                cell.tblCouponHeight.constant = 20
                cell.tblCoupon.reloadData()
            }
             
             if getOpportunityStatus()
             {
                 cell.tblCoupon.isUserInteractionEnabled = false
                 cell.tblCredit.isUserInteractionEnabled = false
             }
             else
             {
                 cell.tblCoupon.isUserInteractionEnabled = true
                 cell.tblCredit.isUserInteractionEnabled = true

             }
             
            /**********************/
            
            /*Credit Details*/
            if self.arrAppliedCredit.count > 0 {
               // cell.txtCredit.text = dictSelectedCredit?["Name"] as? String ?? ""
                cell.tableCreditSetUp(arrAppliedCredit : self.arrAppliedCredit)
            }else {
                cell.arrAppliedCredit.removeAll()
                cell.tblCreditHeight.constant = 20
                cell.tblCredit.reloadData()
            }
            /**********************/
            
            
            //****Sub Total****
            cell.lblSubTotalInitial.text = String(format: "$%.2f", self.subTotalInitialPrice)
            self.dictPricingInfo["subTotalInitial"] = cell.lblSubTotalInitial.text
            /**********************/

            //****Coupon Discount****
            cell.lblCouponDiscount.text = String(format: "$%.2f", self.couponDiscountPrice)
            self.dictPricingInfo["couponDiscount"] = cell.lblCouponDiscount.text
            /**********************/

            //****Credit****
            if self.creditAmount > self.subTotalInitialPrice {
                self.creditAmount = self.subTotalInitialPrice
            }
            
            cell.lblCredit.text = String(format: "$%.2f", self.creditAmount)
            self.dictPricingInfo["credit"] = cell.lblCredit.text
            /**********************/

            //****Other Discount****
            cell.lblOtherDiscount.text = String(format: "$%.2f", (self.totalDiscountPrice - self.couponDiscountPrice))
            self.dictPricingInfo["otherDiscount"] = cell.lblOtherDiscount.text
            /**********************/
            
            //****Total Price****
            self.totalPrice = (self.subTotalInitialPrice
                                - (self.creditAmount + self.totalDiscountPrice))
            if self.totalPrice < 0 {
                self.totalPrice = 0.0
            }
            cell.lblTotalPrice.text = String(format: "$%.2f", self.totalPrice)
            self.dictPricingInfo["totalPrice"] = cell.lblTotalPrice.text
            /**********************/

            //****Tax Amount****
            cell.lblTaxAmount.text = String(format: "$%.2f", self.taxAmount)
            self.dictPricingInfo["taxAmount"] = cell.lblTaxAmount.text
            /**********************/

            //****Billing Amount****
            self.billingAmount = self.totalPrice + self.taxAmount
            cell.lblBillingAmount.text = String(format: "$%.2f", self.billingAmount)
            self.dictPricingInfo["billingAmount"] = cell.lblBillingAmount.text
            /**********************/
            
            //*Maintenance Service Price*
            //****SubTotal Amount****
            cell.lblSubTotalMaintenance.text = String(format: "$%.2f", self.subTotalMaintenanceAmount)
            self.dictPricingInfo["subTotalMaintenance"] = cell.lblSubTotalMaintenance.text
            /**********************/
            
            //****Credit Maint****
            if self.creditAmountMaint > self.subTotalMaintenanceAmount {
                self.creditAmountMaint = self.subTotalMaintenanceAmount
            }
            cell.lblCreditMaint.text = String(format: "$%.2f", self.creditAmountMaint)
            self.dictPricingInfo["creditMaint"] = cell.lblCreditMaint.text
            /**********************/
            
            //****Total Price****y
            var totalMaint = self.subTotalMaintenanceAmount - self.creditAmountMaint
            if totalMaint < 0 {
                totalMaint = 0.0
            }
            cell.lblTotalPriceMaintenance.text = String(format: "$%.2f", totalMaint)
            self.dictPricingInfo["totalPriceMaintenance"] = cell.lblTotalPriceMaintenance.text
            /**********************/
            
            //****Tax Amount****
            cell.lblTaxAmountMaintenance.text = String(format: "$%.2f", taxMaintAmount)
            self.dictPricingInfo["taxAmountMaintenance"] = cell.lblTaxAmountMaintenance.text
            /**********************/
            
            //****Billing Amount****
            let dueAmount = self.subTotalMaintenanceAmount - self.creditAmountMaint + taxMaintAmount
            cell.lblTotalDueAmount.text = String(format: "$%.2f", dueAmount)
            self.dictPricingInfo["totalDueAmount"] = cell.lblTotalDueAmount.text
            /**********************/
            
            /*Maint billing info*/
            if self.arrMaintBillingPrice.count > 0 {
                cell.tableMaintSetUp(arrMaintBillingPrice : self.arrMaintBillingPrice)
                self.dictPricingInfo["arrMaintBillingPrice"] = self.arrMaintBillingPrice
            }else {
                self.dictPricingInfo["arrMaintBillingPrice"] = []
                cell.arrMaintBillingPrice.removeAll()
                cell.tblMaintFrqHeight.constant = 20
                cell.tblMaintFrq.reloadData()
            }
            /**********************/
             let aryTaxCodeSysName = "\(matchesGeneralInfo.value(forKey: "taxSysName") ?? "")".split(separator: ",")
             SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryTaxCodeSysName as NSArray, getmasterName: "MasterSalesAutomation", getkeyName: "TaxMaster", keyName: "Name", keySysName: "SysName") { [self] (result, name, sysname) in
                 if self.strTaxCode != "" {
                     cell.txtTaxCode.text = name
                 }
                 else
                 {
                     cell.txtTaxCode.text = ""
                 }
             }
             
            if self.strTaxCode != "" {
               // cell.txtTaxCode.text = self.strTaxCode
            }
            
             if arrAppliedCoupon.count > 0 || arrAppliedCredit.count > 0 {
                 DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                     tblView.beginUpdates()
                     tblView.endUpdates()
                 }
             }
             
            if getOpportunityStatus() {
                cell.txtCouponCode.isUserInteractionEnabled = false
                cell.txtCredit.isUserInteractionEnabled = false
                cell.txtTaxCode.isUserInteractionEnabled = false
                cell.btnTax.isUserInteractionEnabled = false
                cell.btnApplyCoupon.isUserInteractionEnabled = false
                cell.btnCredit.isUserInteractionEnabled = false
                cell.btnAddCredit.isUserInteractionEnabled = false
            }
            return cell
            
        }else if(indexPath.section == (4 - (3 - self.secTitleCount))) {
            //Notes
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell", for: indexPath as IndexPath) as! ConfigureNotesTableViewCell
            
            cell.delegate = self

            cell.lblTitle.text = arrNotes[indexPath.row]
            if indexPath.row == 0 {
                cell.lblSubTitle.text = self.strCustomerNote
                if getOpportunityStatus() {
                    cell.btnEdit.isUserInteractionEnabled = false
                }
                else
                {
                    cell.btnEdit.isUserInteractionEnabled = true
                }
                
            }else if indexPath.row == 1 {
                cell.lblSubTitle.text = self.strProposalNote
                
                if getOpportunityStatus() {
                    cell.btnEdit.isUserInteractionEnabled = false
                }
                else
                {
                    cell.btnEdit.isUserInteractionEnabled = true
                }
                
            }else {
                cell.lblSubTitle.text = self.strInternalNote
                
                cell.btnEdit.isUserInteractionEnabled = true
            }
            
            return cell
        }else if(indexPath.section == (5 - (3 - self.secTitleCount))) {
            if !isPreferredMonths {
                //Agreement
                let cell = tableView.dequeueReusableCell(withIdentifier: "AgreementCell", for: indexPath as IndexPath) as! ConfigureAgreementTableViewCell
                cell.delegate = self
                /*Credit Details*/
                print(self.arrayAgreementCheckList)
                if self.arrayAgreementCheckList.count > 0 {
                    var isCW = false
                    if getOpportunityStatus() {
                        isCW = true
                    }
                    cell.tableSetUp(arrayAgreementCheckList : self.arrayAgreementCheckList, isCompleteWon: isCW)
                    
                }
                return cell
            }else {
                //Months
                let cell = tableView.dequeueReusableCell(withIdentifier: "MonthCell", for: indexPath as IndexPath) as! ConfigureMonthTableViewCell
                cell.delegate = self
                
                cell.setMonthValues(arrMonth: arrayOfMonth)
                
                if getOpportunityStatus() {
                    cell.btnJan.isUserInteractionEnabled = false
                    cell.btnFeb.isUserInteractionEnabled = false
                    cell.btnMar.isUserInteractionEnabled = false
                    cell.btnApr.isUserInteractionEnabled = false
                    cell.btnMay.isUserInteractionEnabled = false
                    cell.btnJun.isUserInteractionEnabled = false
                    cell.btnJul.isUserInteractionEnabled = false
                    cell.btnAug.isUserInteractionEnabled = false
                    cell.btnSpt.isUserInteractionEnabled = false
                    cell.btnOct.isUserInteractionEnabled = false
                    cell.btnNov.isUserInteractionEnabled = false
                    cell.btnDec.isUserInteractionEnabled = false
                }
                return cell
            }
            
        }else  {
            //Agreement
            let cell = tableView.dequeueReusableCell(withIdentifier: "AgreementCell", for: indexPath as IndexPath) as! ConfigureAgreementTableViewCell
            cell.delegate = self
            /*Credit Details*/
            print(self.arrayAgreementCheckList)
            if self.arrayAgreementCheckList.count > 0 {
                var isCW = false
                if getOpportunityStatus() {
                    isCW = true
                }
                cell.tableSetUp(arrayAgreementCheckList : self.arrayAgreementCheckList, isCompleteWon: isCW)
                
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        if self.secTitleCount == 1 {
            if section == 0 {
                return  70
            }else {
                return 1
            }
        }else if self.secTitleCount == 2 {
            if section == 0 || section == 1 {
                return  70
            }else {
                return 1
            }
            
        }else if self.secTitleCount == 3 {
            if section == 0 || section == 1 || section == 2 {
                return  70
            }else {
                return 1
            }
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: self.tblView.frame.width,
                                              height: 30))
        headerView.backgroundColor = .white
        
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: self.tblView.frame.width - 40, height: 70))
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 21)
        label.text = self.arrSectionTitle[section]
        label.textColor = .black
        
        headerView.addSubview(label)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    // MARK: -----------Pricing Calculation---------
    
    func pricingCalculation() {
        
    }
    
}

// MARK: ----------SalesReviewCell Delegate Cell---------
// MARK:
extension SalesConfigureViewController: ConfigureCellDelegate {
    func callBackTaxCodeMethod(cell: ConfigurePricingTableViewCell, taxCode: String) {
        
        if (isInternetAvailable()){
            
            if self.arrTaxCode?.count ?? 0 > 0 {
                openTableViewPopUp(tag: 3333, ary: (self.arrTaxCode! as NSArray).mutableCopy() as! NSMutableArray)
            }else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No tax code available.", viewcontrol: self)
            }
            
            }
            
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Can't update taxcode offline", viewcontrol: self)
        }
        
    }
    
    func callBackSelectCredit(cell: ConfigurePricingTableViewCell) {
        
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            //Getting Credit and Coupon Detail
            if(dictMaster.value(forKey: "DiscountSetupMasterCredit") is NSArray) {
                
                self.arrDiscountCredit = dictMaster.value(forKey: "DiscountSetupMasterCredit") as! [Any]
                print(self.arrDiscountCredit)

                let arrTempCredit = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Credit")
                let arrOnlyAppliedCredit: NSMutableArray = NSMutableArray(array: self.arrDiscountCredit)
                
                
                for object in arrTempCredit {
                    
                    let strSysNameDiscount = object.value(forKey: "discountSysName")
                    for dict in arrDiscountCredit {
                        let dict1: [String: Any] = dict as! [String : Any]
                        if strSysNameDiscount as! String == "\(dict1["SysName"] ?? "")" {
                            arrOnlyAppliedCredit.remove(dict)
                        }
                        
                    }
                }
                self.arrDiscountCredit.removeAll()
                self.arrDiscountCredit = arrOnlyAppliedCredit as! [Any]
                
                
                
                if ReachabilityTest.isConnectedToNetwork() {
                    print("Internet connection available")
                    
                    let arrTemp: NSMutableArray = NSMutableArray()
                    let arrTempNew: NSMutableArray = NSMutableArray(array: self.arrDiscountCredit)
                    for dictCredit in arrDiscountCredit {
                        let dictCredit1: [String: Any] = dictCredit as! [String : Any]
                        
                        for dictAppliedCredit in self.arrAppliedCreditDetail {
                            let dictAppliedCredit1: [String: Any] = dictAppliedCredit as! [String : Any]
                            let strC1: String = dictCredit1["SysName"] as! String
                            let strC2: String = dictAppliedCredit1["SysName"] as! String
                            if strC1 == strC2 {
                                arrTemp.add(dictCredit1)
                                break
                            }
                        }
                    }
                    
                    arrTempNew.removeObjects(in: arrTemp as! [Any])
                    self.arrDiscountCredit.removeAll()
                    self.arrDiscountCredit = arrTempNew as! [Any]
                }
                else{
                   print("No internet connection available")
                }
                
                if self.arrDiscountCredit.count > 0 {
                    openTableViewPopUp(tag: 3334, ary: (self.arrDiscountCredit as NSArray).mutableCopy() as! NSMutableArray)

                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No credit available.", viewcontrol: self)
                }
            }
        }
    }
    
    func callBackAddCreditMethod(cell: ConfigurePricingTableViewCell, creditCode: String) {
        
        //Check Service Add to Agreement or not
        let serviceCount = (self.arrStandardServiceSold.count + self.arrBundleServiceSold.count + self.arrCustomServiceSold.count)
        
        if serviceCount == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please add atleast one service to apply credit.", viewcontrol: self)
            return
        }
        
        if creditCode == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select credit code.", viewcontrol: self)

        }else {
            
            let arrStanAllSoldService = self.arrStandardServiceSold + self.arrBundleServiceSold
            print(arrStanAllSoldService.count)
            
            if arrStanAllSoldService.count > 0 {
                
                guard let dictMaster: [String: Any] = UserDefaults.standard.object(forKey: "MasterSalesAutomation") as? [String : Any] else
                {
                    return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                }
                //can define Globle : arrDiscountCredit
                                
                guard let arrDiscountCredit: [[String: Any]] = dictMaster["DiscountSetupMasterCredit"] as? [[String : Any]] else
                {
                    return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                }
                
                print(arrDiscountCredit)
                
                
                let strDiscountCredit = "\(self.dictSelectedCredit?["SysName"] ?? "")"
                let isCreditApplied = self.checkForAppliedDiscount(strType: "Credit", strDiscountCode: strDiscountCredit, couponUsage: "OneTime")
                
                print(self.dictSelectedCredit)
                if isCreditApplied {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Credit Coupon already applied.", viewcontrol: self)
                }else {
                    
                    for dictObj in arrDiscountCredit {
                        var dict: [String: Any] = dictObj
                        
                        /*Check In Applied Credit Available*/
                        if strDiscountCredit == "\(dict["SysName"] ?? "")" {
                            
                            /************Initial and Maint value to credite calculation**********/
                            var valueInitial = 0.0
                            var valueMaint = 0.0
                            let strInitial = "\(arrStanAllSoldService[0].value(forKey: "totalInitialPrice") ?? "0")"
                            let strMaint = "\(arrStanAllSoldService[0].value(forKey: "totalMaintPrice") ?? "0")"
                            valueInitial = Double(strInitial) ?? 0.0
                            valueMaint = Double(strMaint) ?? 0.0
                            /***********************/
                            
                            if "\(dict["IsDiscountPercent"] ?? "")" == "1" || "\(dict["IsDiscountPercent"] ?? "")" == "true" {
                                
                                //***-Discount Percent Base-***
                                
                                appliedDiscountInitial = (valueInitial * Double("\(dict["DiscountPercent"] ?? "0")")!) / 100
                                appliedDiscountMaint = (valueMaint * Double("\(dict["DiscountPercent"] ?? "0")")!) / 100

                                var isToSave = false
                                //Maint
                                if "\(dict["ApplicableForMaintenance"] ?? "0")" == "1" || "\(dict["ApplicableForMaintenance"] ?? "0")" == "true" {
                                    
                                    let arrTempCreditAlreadyApplied = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Coupon") + self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Credit")

                                    
                                    //***-Calculation To Total Already Exist Discount Amount-***
                                    var totalDiscountAmoutAlreadyPresent = 0.0
                                    for objTemp in arrTempCreditAlreadyApplied {
                                        
                                        let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedMaintDiscount") ?? "0")"
                                        let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")

                                        
                                        let discountAmountPresentt: Double = Double(strDiscountAmountPresent)!

                                        if "\(objTemp.value(forKey: "applicableForMaintenance") ?? "0")" ==  "1" || "\(objTemp.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                    }
                                    //**************
                                    
                                    //Check Maint Amount to update on DB
                                    if !(valueMaint >= (totalDiscountAmoutAlreadyPresent + appliedDiscountMaint)) {
                                        
                                        if (valueMaint < (totalDiscountAmoutAlreadyPresent + appliedDiscountMaint)) {
                                            
                                            let amountDiscountToApplyCredit = valueMaint - totalDiscountAmoutAlreadyPresent
                                            
                                            let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                            dictTempToSave.setValue("\(amountDiscountToApplyCredit)", forKey: "DiscountAmount")
                                            
                                            dict = dictTempToSave as! [String : Any]
                                            isToSave = true
                                            if amountDiscountToApplyCredit > 0 {
                                                isToSave = true
                                                appliedDiscountMaint = amountDiscountToApplyCredit
                                            }else {
                                                isToSave = false
                                                appliedDiscountMaint = 0.0
                                            }
                                            
                                        }
                                        
                                    }else {
                                        if appliedDiscountMaint > Double(strMaint) ?? 0.0 {
                                            appliedDiscountMaint = Double(strMaint) ?? 0.0
                                        }
                                        
                                        let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                        dictTempToSave.setValue("\(appliedDiscountMaint)", forKey: "DiscountAmount")
                                        dict = dictTempToSave as! [String : Any]
                                        isToSave = true
                                    }
                                }else {
                                    appliedDiscountMaint = 0.0
                                }
                                
                                
                                //Initial
                                if "\(dict["ApplicableForInitial"] ?? "0")" == "1" || "\(dict["ApplicableForInitial"] ?? "0")" == "true" {
                                    
                                    let arrTempCreditAlreadyApplied = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Coupon") + self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Credit")

                                    var totalDiscountAmoutAlreadyPresent = 0.0
                                    
                                    //***-Calculation To Total Already Exist Discount Amount-***
                                    for objTemp in arrTempCreditAlreadyApplied {
                                        
                                        let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedInitialDiscount") ?? "0")"
                                        let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")

                                        
                                        let discountAmountPresentt: Double = Double(strDiscountAmountPresent)!

                                        if "\(objTemp.value(forKey: "applicableForInitial") ?? "0")" ==  "1" || "\(objTemp.value(forKey: "applicableForInitial") ?? "0")" == "true" {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                        
                                        if !("\(objTemp.value(forKey: "discountType") ?? "0")" ==  "Credit") {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                    }
                                    //*******************
                                    
                                    //Check Initial Amount to update on DB
                                    if !(valueInitial >= (totalDiscountAmoutAlreadyPresent + appliedDiscountInitial)) {
                                        
                                        if (valueInitial < (totalDiscountAmoutAlreadyPresent + appliedDiscountInitial)) {
                                            
                                            let amountDiscountToApplyCredit = valueInitial - totalDiscountAmoutAlreadyPresent
                                            
                                            let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                            dictTempToSave.setValue("\(amountDiscountToApplyCredit)", forKey: "DiscountAmount")
                                            
                                            dict = dictTempToSave as! [String : Any]
                                            isToSave = true
                                            if amountDiscountToApplyCredit > 0 {
                                                isToSave = true
                                                appliedDiscountInitial = amountDiscountToApplyCredit
                                            }else {
                                                isToSave = false
                                                appliedDiscountInitial = 0.0;
                                            }
                                            
                                        }
                                        
                                    }else {
                                        if appliedDiscountInitial > Double(strInitial) ?? 0.0 {
                                            appliedDiscountInitial = Double(strInitial) ?? 0.0
                                        }
                                        
                                        let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                        dictTempToSave.setValue("\(appliedDiscountInitial)", forKey: "DiscountAmount")
                                        
                                        dict = dictTempToSave as! [String : Any]
                                        isToSave = true
                                    }
                                }else {
                                    appliedDiscountInitial = 0.0
                                }
                                
                                
                                //Want to Save on DB
                                if isToSave {
                                    self.saveAppliedDiscountCouponAndCreditCoreData(dictCouponDetail: dict, strType: "Credit")

                                }else {
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Credit can not be more then billing amount.", viewcontrol: self)
                                }
                                
                            }else {
                                
                                //***-Discount: Amount Base-***
                                
                                appliedDiscountMaint = 0.0
                                appliedDiscountInitial = 0.0
                                var discount = 0.0

                                
                                let strDiscount = "\(dict["DiscountAmount"] ?? "0")"
                                discount = Double(strDiscount) ?? 0.0
                                appliedDiscountInitial = discount
                                appliedDiscountMaint = discount

                                var isToSave = false
                                //Maint
                                if "\(dict["ApplicableForMaintenance"] ?? "0")" == "1" || "\(dict["ApplicableForMaintenance"] ?? "0")" == "true" {
                                    
                                    let arrTempCreditAlreadyApplied = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Coupon") + self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Credit")

                                    var totalDiscountAmoutAlreadyPresent = 0.0
                                    
                                    //***-Calculation To Total Already Exist Discount Amount-***
                                    for objTemp in arrTempCreditAlreadyApplied {
                                        
                                        let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedMaintDiscount") ?? "0")"
                                        let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")

                                        
                                        let discountAmountPresentt: Double = Double(strDiscountAmountPresent)!

                                        if "\(objTemp.value(forKey: "applicableForMaintenance") ?? "0")" ==  "1" || "\(objTemp.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                    }
                                    //*******************
                                    
                                    //Check Maint Amount to update on DB
                                    if !(valueMaint >= (totalDiscountAmoutAlreadyPresent + discount)) {
                                        
                                        if (valueMaint < (totalDiscountAmoutAlreadyPresent + discount)) {
                                            
                                            let amountDiscountToApplyCredit = valueMaint - totalDiscountAmoutAlreadyPresent
                                            
                                            var dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                            dictTempToSave.setValue("\(amountDiscountToApplyCredit)", forKey: "DiscountAmount")
                                            
                                            dict = dictTempToSave as! [String : Any]
                                            isToSave = true
                                            if amountDiscountToApplyCredit > 0 {
                                                isToSave = true
                                                appliedDiscountMaint = amountDiscountToApplyCredit
                                            }else {
                                                isToSave = false
                                                appliedDiscountMaint = 0.0
                                            }
                                            
                                        }
                                        
                                    }else {
                                        let strMaint = "\(arrStanAllSoldService[0].value(forKey: "totalMaintPrice") ?? "0")"
                                        if discount > Double(strMaint) ?? 0.0 {
                                            appliedDiscountMaint = Double(strMaint) ?? 0.0
                                        }
                                        
                                        isToSave = true
                                    }
                                }else {
                                    appliedDiscountMaint = 0.0
                                }
                                
                                
                                //Initial
                                if "\(dict["ApplicableForInitial"] ?? "0")" == "1" || "\(dict["ApplicableForInitial"] ?? "0")" == "true" {
                                    
                                    let arrTempCreditAlreadyApplied = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Coupon") + self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Credit")

                                    //***-Calculation To Total Already Exist Discount Amount-***
                                    var totalDiscountAmoutAlreadyPresent = 0.0
                                    for objTemp in arrTempCreditAlreadyApplied {
                                        
                                        let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedInitialDiscount") ?? "0")"
                                        let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")

                                        let discountAmountPresentt: Double = Double(strDiscountAmountPresent)!

                                        if "\(objTemp.value(forKey: "applicableForInitial") ?? "0")" ==  "1" || "\(objTemp.value(forKey: "applicableForInitial") ?? "0")" == "true" {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                        
                                        if !("\(objTemp.value(forKey: "discountType") ?? "0")" ==  "Credit") {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                    }
                                    //*******************
                                    
                                    //Check Initial Amount to update on DB
                                    if !(valueInitial >= (totalDiscountAmoutAlreadyPresent + discount)) {
                                        
                                        if (valueInitial < (totalDiscountAmoutAlreadyPresent + discount)) {
                                            
                                            let amountDiscountToApplyCredit = valueInitial - totalDiscountAmoutAlreadyPresent
                                            
                                            let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                            dictTempToSave.setValue("\(amountDiscountToApplyCredit)", forKey: "DiscountAmount")
                                            
                                            dict = dictTempToSave as! [String : Any]
                                            isToSave = true
                                            if amountDiscountToApplyCredit > 0 {
                                                isToSave = true
                                                appliedDiscountInitial = amountDiscountToApplyCredit
                                            }else {
                                                isToSave = false
                                                appliedDiscountInitial = 0.0;
                                            }
                                            
                                        }
                                        
                                    }else {
                                        let strInitial = "\(arrStanAllSoldService[0].value(forKey: "totalInitialPrice") ?? "0")"

                                        if discount > Double(strInitial) ?? 0.0 {
                                            appliedDiscountInitial = Double(strInitial) ?? 0.0
                                        }
                                        isToSave = true
                                    }
                                }else {
                                    appliedDiscountInitial = 0.0
                                }
                                
                                
                                //Want to Save On DB
                                if isToSave {
                                    self.saveAppliedDiscountCouponAndCreditCoreData(dictCouponDetail: dict, strType: "Credit")

                                }else {
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Credit can not be more then billing amount.", viewcontrol: self)
                                }
                                
                            }
                            
                            break
                        }
                    }
                    
                }
                
                
            }else {
                //Credit applicable service not available
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Credit applicable service not available.", viewcontrol: self)
            }
        }
        cell.txtCredit.text = ""

    }
    
    
    
    
    func callBackDeleteCoupon(cell: ConfigurePricingTableViewCell, objCouponDetails: NSManagedObject) {
        print(objCouponDetails)
        
        let strServiceName = "\(objCouponDetails.value(forKey: "serviceSysName") ?? "")"
        let strDiscountAmount = "\(objCouponDetails.value(forKey: "appliedInitialDiscount") ?? "")"
        let strDiscountPer = "\(objCouponDetails.value(forKey: "discountPercent") ?? "")"
        let strChkDiscountPer = "\(objCouponDetails.value(forKey: "isDiscountPercent") ?? "")"
        
        let strDiscountType = "\(objCouponDetails.value(forKey: "discountType") ?? "")"

        
        let alert = UIAlertController(title: alertMessage, message: "Are you sure want to delete", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Cancel", style: .destructive, handler: { (nil) in
        }))
        
        alert.addAction(UIAlertAction (title: "OK", style: .cancel, handler: { (nil) in
            
            let context = getContext()
            context.delete(objCouponDetails)
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
            
            
            self.updateDiscountAfterDelete(strService: strServiceName, strDiscountAmount: strDiscountAmount, strDiscountPer: strDiscountPer, strChkDiscountPer: strChkDiscountPer, strType: strDiscountType)
            
            self.initialSetup()
            self.getAppliedCouponOrCredit()
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func callBackApplyCouponMethod(cell: ConfigurePricingTableViewCell, couponCode: String) {
        
        //Check Service Add to Agreement or not
        let serviceCount = (self.arrStandardServiceSold.count + self.arrBundleServiceSold.count + self.arrCustomServiceSold.count)
        
        if serviceCount == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please add atleast one service to apply coupon.", viewcontrol: self)
            return
        }
        
        if couponCode == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter coupon code.", viewcontrol: self)
        }else {
            
            print(couponCode)
            guard let dictMaster: [String: Any] = UserDefaults.standard.object(forKey: "MasterSalesAutomation") as? [String : Any] else
            {
                return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
            }
            
            guard let arrDiscountCoupon: [[String: Any]] = dictMaster["DiscountSetupMasterCoupon"] as? [[String : Any]] else
            {
                return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
            }
            
            //***Check Applied Coupon Available or Not***
            var isValidCoupon = false
            var dictCouponDetail: [String: Any]?
            for couponDetail in arrDiscountCoupon {
                if couponDetail["DiscountCode"] as! String == couponCode {
                    isValidCoupon = true
                    dictCouponDetail = couponDetail
                    break
                }else {
                    isValidCoupon = false
                }
            }
            
            if !isValidCoupon {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Invalid coupon code.", viewcontrol: self)
            }else {
                //Coupon Availe
                
                print(dictCouponDetail)
                let isCouponApplied = self.checkForAppliedDiscount(strType: "Coupon", strDiscountCode: couponCode, couponUsage: dictCouponDetail?["Usage"] as? String ?? "")
                if isCouponApplied {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Coupon already applied.", viewcontrol: self)
                }else {
                    
                    //Applied coupon Valid on Not
                    let isValidDiscount = self.checkCouponValidity(dictCouponDetail: dictCouponDetail!)
                    if isValidDiscount {
                        //Coupon Valid
                      
                        let arrTempCouponAlreadyApplied = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Coupon")
                        let arrTempCreditAlreadyApplied = self.fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: "Credit")
                        var totalDiscountAmoutAlreadyPresent = 0.0
                        
                        //Coupon discout already Applied
                        for objTemp in arrTempCouponAlreadyApplied {
                            print(objTemp)
                            let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedInitialDiscount") ?? "")"
                            let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")
                            
                            let discountAmountPresent: Double = Double(strDiscountAmountPresent) ?? 0.0
                            
                            let applicableForMaintenance = "\(objTemp.value(forKey: "applicableForMaintenance") ?? "")"
                            if applicableForMaintenance == "1" || applicableForMaintenance == "true" {
                                
                            }else {
                                totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresent;
                            }
                        }
                        //+ Credit discout already Applied
                        for objTemp in arrTempCreditAlreadyApplied {
                            print(objTemp)
                            let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedInitialDiscount") ?? "")"
                            let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")
                            
                            let discountAmountPresent: Double = Double(strDiscountAmountPresent) ?? 0.0
                            
                            let applicableForMaintenance = "\(objTemp.value(forKey: "applicableForMaintenance") ?? "")"
                            if applicableForMaintenance == "1" || applicableForMaintenance == "true" {
                                
                            }else {
                                totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresent;
                            }
                        }
                        
                        var isToSave = false
                        var strIsServiceFound = ""
                        let strDiscountAmountTemp = "\(dictCouponDetail?["DiscountAmount"] ?? "")"
                        
                        let discount: Double = Double(strDiscountAmountTemp) ?? 0.0
                        //20
                        //If Not Service Base
                        print(self.arrStandardService.count)
                        print(self.arrBundleService.count)
                        let arrStanAllSoldService = self.arrStandardServiceSold + self.arrBundleServiceSold
                        
                        
                        if "\(dictCouponDetail?["IsServiceBased"] ?? "0")" == "1" || "\(dictCouponDetail?["IsServiceBased"] ?? "false")".caseInsensitiveCompare("true") == .orderedSame
                        {
//                            "ServiceSysName": SignaturePestControl
                            // Service Base

                            var objMatchesDiscoutUpdate : NSManagedObject?
                            for i in 0..<arrStanAllSoldService.count {
                                objMatchesDiscoutUpdate = (arrStanAllSoldService[i] as! NSManagedObject)
                                
                                if objMatchesDiscoutUpdate?.value(forKey: "serviceSysName") as! String == "\(dictCouponDetail?["ServiceSysName"] ?? "")" {
                                    
                                    //
                                    /************Initial  value to Coupon calculation**********/
                                    var valueInitial = 0.0
                                    let strInitial = "\((objMatchesDiscoutUpdate as AnyObject).value(forKey: "totalInitialPrice") ?? "0")"
                                    valueInitial = Double(strInitial) ?? 0.0
                                    /***********************/
                                    if i == 0 {
                                        if (!(valueInitial >= (totalDiscountAmoutAlreadyPresent + discount))) {
                                            
                                            if (valueInitial < (totalDiscountAmoutAlreadyPresent + discount)) {
                                                
                                                let amountDiscountToApplyCredit = valueInitial - totalDiscountAmoutAlreadyPresent
                                                
                                                var dictTempToSave = dictCouponDetail
                                                dictTempToSave?["DiscountAmount"] = "\(amountDiscountToApplyCredit)"
                                                dictCouponDetail = dictTempToSave
                                                
                                                isToSave = true

                                                if (amountDiscountToApplyCredit > 0) {
                                                    
                                                    isToSave = true
                                                    appliedDiscountInitial = amountDiscountToApplyCredit
                                                    
                                                    
                                                }else{
                                                    
                                                    isToSave = false
                                                    appliedDiscountInitial = 0.0;
                                                    
                                                }
                                            }
                                            
                                        }else {
                                            appliedDiscountInitial = discount;
                                            isToSave = true
                                        }
                                    }else {
                                        if (!(valueInitial >= discount)) {
                                            
                                            if (valueInitial < discount) {
                                                
                                                let amountDiscountToApplyCredit = discount
                                                
                                                var dictTempToSave = dictCouponDetail
                                                dictTempToSave?["DiscountAmount"] = "\(amountDiscountToApplyCredit)"
                                                dictCouponDetail = dictTempToSave
                                                
                                                isToSave = true

                                                if (amountDiscountToApplyCredit > 0) {
                                                    
                                                    isToSave = true
                                                    appliedDiscountInitial = amountDiscountToApplyCredit
                                                    
                                                    
                                                }else{
                                                    
                                                    isToSave = false
                                                    appliedDiscountInitial = 0.0;
                                                    
                                                }
                                            }
                                            
                                        }else {
                                            appliedDiscountInitial = discount;
                                            isToSave = true
                                        }
                                    }
                                    strIsServiceFound = "true"
                                    break
                                }else {
                                    strIsServiceFound = "false"
                                }
                            }
                        }
                        else
                        {
                            //Non Service Base
                            strIsServiceFound = ""
                            /************Initial  value to Coupon calculation**********/
                            var valueInitial = 0.0
                            let strInitial = "\((arrStanAllSoldService[0] as AnyObject).value(forKey: "totalInitialPrice") ?? "0")"
                            valueInitial = Double(strInitial) ?? 0.0
                            /***********************/
                            
                            if (!(valueInitial >= (totalDiscountAmoutAlreadyPresent + discount))) {
                                
                                if (valueInitial < (totalDiscountAmoutAlreadyPresent + discount)) {
                                    
                                    let amountDiscountToApplyCredit = valueInitial - totalDiscountAmoutAlreadyPresent
                                    
                                    print(dictCouponDetail)
                                    var dictTempToSave = dictCouponDetail
                                    
                                    dictTempToSave?["DiscountAmount"] = "\(amountDiscountToApplyCredit)"
                                    
                                    dictCouponDetail = dictTempToSave
                                    print(dictCouponDetail)
                                    
                                    isToSave = true

                                    if (amountDiscountToApplyCredit > 0) {
                                        
                                        isToSave = true
                                        appliedDiscountInitial = amountDiscountToApplyCredit
                                        
                                        
                                    }else{
                                        
                                        isToSave = false
                                        appliedDiscountInitial = 0.0;
                                        
                                    }
                                }
                                
                            }else {
                                appliedDiscountInitial = discount;
                                isToSave = true
                            }
                        }
                        
                        
                        
                        //Save data to DB
                        if isToSave {
                            let strDiscountPerCheck: String =  "\(dictCouponDetail?["IsDiscountPercent"] ?? "0")"
                            
                            var isServiceBaseCoupon = false
                            
                            if strDiscountPerCheck == "1" || strDiscountPerCheck == "true" {
                                self.updateStandardServiceDataBase(strServiceNameDiscount: "\(dictCouponDetail?["ServiceSysName"] ?? "")",
                                                                   strDiscountAmount: "0",
                                                                   strDisountCheck: "1",
                                                                   strDiscountPercent: "\(dictCouponDetail?["DiscountPercent"] ?? "")",
                                                                   isServiceBaseCoupon: isServiceBaseCoupon)
                            }else {
                                self.updateStandardServiceDataBase(strServiceNameDiscount: "\(dictCouponDetail?["ServiceSysName"] ?? "")",
                                                                   strDiscountAmount: "\(dictCouponDetail?["DiscountAmount"] ?? "")",
                                                                   strDisountCheck: "0",
                                                                   strDiscountPercent: "0",
                                                                   isServiceBaseCoupon: isServiceBaseCoupon)
                            }

                            if isCouponSaved {
                                self.saveAppliedDiscountCouponAndCreditCoreData(dictCouponDetail: dictCouponDetail!, strType: "Coupon")
                            }
                            
                        }else {
                            
                            if strIsServiceFound == "false" {
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Coupon applicable service is not available.", viewcontrol: self)
                            }else {
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Coupon can not be more then billing amount.", viewcontrol: self)
                            }
                        }
                    }else {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Invalid coupon code.", viewcontrol: self)
                    }
                    
                }
            }
        }
        
//        appliedDiscountMaint = 0.0
//        appliedDiscountInitial = 0.0
        cell.txtCouponCode.text = ""
    }
    
    
    
    
    //Notes
    func callBackEditNotesMethod(cell: ConfigureNotesTableViewCell) {
        
        
//        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesClarkPestiPhone" : "SalesClarkPestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TextEditorNormal") as? TextEditorNormal
//        vc?.modalPresentationStyle = .fullScreen
//        self.present(vc!, animated: true)
        let indexPath = tblView.indexPath(for: cell)
        
        var title = ""
        var message = ""
        if indexPath?.row == 0 {
            if isSendProposal {
                title = NotesForConfigure.proposal.rawValue
                message = strProposalNote
            }else {
                title = NotesForConfigure.customer.rawValue
                message = strCustomerNote
            }
        }else if indexPath?.row == 1 {
            title = NotesForConfigure.proposal.rawValue
            message = strProposalNote
        }else {
            title = NotesForConfigure.internalN.rawValue
            message = strInternalNote
        }
        
        
        //Add Observer
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.notesValue),
            name: Commons.kNotificationNotes,
            object: nil)
        
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "FormBuilder" : "FormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNoteViewController") as? SalesNoteViewController
        vc?.strTitle = title
        vc?.strMessage = message
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true)
    }
    
    //Agreement Add
    func callBackAddAgreementMethodBundle(isSelected: Bool, indexPath: IndexPath, row: Int)//
    {
        //let indexPath = tblView.indexPath(for: cell)
        print(indexPath)
        
        if isSelected {
            self.updateAddToAgreementBundle(isAddAgreement: "true", indexPath: indexPath, bundleRow: row)
        }else {
            self.updateAddToAgreementBundle(isAddAgreement: "false", indexPath: indexPath, bundleRow: row)
        }
    }
    
    func callBackAddAgreementMethod(cell: ConfigureServiceTableViewCell)
    {
        guard let indexPath = tblView.indexPath(for: cell) else { return }

        var strTaxable = "false"
        if cell.btnIsTaxable.isSelected {
            strTaxable = "true"
        }
        
        if cell.btnIsAgreement.isSelected {
            self.updateAddToAgreement(isAddAgreement: "true", isTaxable: strTaxable, indexPath: indexPath, bundleRow: 0)
        }else {
            self.updateAddToAgreement(isAddAgreement: "false", isTaxable: strTaxable, indexPath: indexPath, bundleRow: 0)
        }
    }
    
    func callBackIsTaxableMethod(cell: ConfigureServiceTableViewCell)
    {
        let indexPath = tblView.indexPath(for: cell)
        var strIsAgreement = "false"
        if cell.btnIsAgreement.isSelected {
            strIsAgreement = "true"
        }
        
        if cell.btnIsTaxable.isSelected {
            self.updateAddToAgreement(isAddAgreement: strIsAgreement, isTaxable: "true", indexPath: indexPath!, bundleRow: 0)
        }else {
            self.updateAddToAgreement(isAddAgreement: strIsAgreement, isTaxable: "false", indexPath: indexPath!, bundleRow: 0)
        }
    }
    
    //Month
    func callBackMonthMethod(cell: ConfigureMonthTableViewCell, month: String, tag: Int) {
        
        if self.arrayOfMonth.contains(month) {
            if let index = arrayOfMonth.firstIndex(of: month) {
                arrayOfMonth.remove(at: index)
            }
        }else {
            self.arrayOfMonth.append(month)
        }
        
        print(self.arrayOfMonth)
        strPreferedMonth = arrayOfMonth.joined(separator: ", ")
        print(strPreferedMonth)
        self.updateLeadDetails()
    }
    
    func callBackAgreementMethod(cell: ConfigureAgreementTableViewCell, objCheckAgreement: NSManagedObject, isCheck: Bool)
    {
        let indexPath = tblView.indexPath(for: cell)
        print(objCheckAgreement)
        
        if isCheck {
            objCheckAgreement.setValue("true", forKey: "isActive")
        }else {
            objCheckAgreement.setValue("false", forKey: "isActive")
        }

        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    
    //Update methods DB
    func updateAddToAgreement(isAddAgreement: String, isTaxable: String, indexPath: IndexPath, bundleRow: Int) {
        
        var objServiceDetail = NSManagedObject()
        
        if self.secTitleCount == 1 {
            if indexPath.section == 0 {
                if arrSectionTitle[indexPath.section] == "Standard Service" {
                    objServiceDetail = self.arrStandardService[indexPath.row]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                }else if arrSectionTitle[indexPath.section] == "Custom Service" {
                    objServiceDetail = self.arrCustomService[indexPath.row]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                    objServiceDetail.setValue(isTaxable, forKey: "isTaxable")
                }else {
                    objServiceDetail = self.arrBundleService[bundleRow]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                }
            }
        }else if self.secTitleCount == 2 {
            if indexPath.section == 0 {
                if arrSectionTitle[indexPath.section] == "Standard Service" {
                    objServiceDetail = self.arrStandardService[indexPath.row]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                }else if arrSectionTitle[indexPath.section] == "Custom Service" {
                    objServiceDetail = self.arrCustomService[indexPath.row]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                    objServiceDetail.setValue(isTaxable, forKey: "isTaxable")
                }else {
                    objServiceDetail = self.arrBundleService[bundleRow]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                }
            }else if indexPath.section == 1 {
                if arrSectionTitle[indexPath.section] == "Custom Service" {
                    objServiceDetail = self.arrCustomService[indexPath.row]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                    objServiceDetail.setValue(isTaxable, forKey: "isTaxable")
                }else {
                    objServiceDetail = self.arrBundleService[bundleRow]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                }
            }
            
        }else if self.secTitleCount == 3 {
            if indexPath.section == 0 {
                if arrSectionTitle[indexPath.section] == "Standard Service" {
                    objServiceDetail = self.arrStandardService[indexPath.row]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                }else if arrSectionTitle[indexPath.section] == "Custom Service" {
                    objServiceDetail = self.arrCustomService[indexPath.row]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                    objServiceDetail.setValue(isTaxable, forKey: "isTaxable")
                }else {
                    objServiceDetail = self.arrBundleService[bundleRow]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                }
            }else if indexPath.section == 1 {
                if arrSectionTitle[indexPath.section] == "Custom Service" {
                    objServiceDetail = self.arrCustomService[indexPath.row]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                    objServiceDetail.setValue(isTaxable, forKey: "isTaxable")
                }else {
                    objServiceDetail = self.arrBundleService[bundleRow]
                    objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
                }
            }else {
                objServiceDetail = self.arrBundleService[bundleRow]
                objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
            }
        }
        
        if isAddAgreement == "false" {
            print(arrAppliedCoupon)
            print(arrAppliedCredit)
            let arrCreditAndCoupon = arrAppliedCoupon + arrAppliedCredit
            print(arrCreditAndCoupon)

            for couponOrCredit in arrCreditAndCoupon {
                var objCouponCredit = NSManagedObject()
                objCouponCredit = couponOrCredit
                let soldServiceId = "\((objCouponCredit as AnyObject).value(forKey: "soldServiceId") ?? "")"
                let discountType = "\((objCouponCredit as AnyObject).value(forKey: "discountType") ?? "")"
                //print(objServiceDetail.value(forKey: "soldServiceStandardId") as! String)
                
                if  objServiceDetail.entity.attributesByName.contains(where: { (key: String, value: NSAttributeDescription) in key == "soldServiceStandardId"}) == true
                {
                    if objServiceDetail.value(forKey: "soldServiceStandardId") as! String == soldServiceId {
                        
                        if discountType == "Coupon" {
                            //Delete Coupon
                            print("Delete Coupon")
                            self.deleteCreditOrCoupon(objCouponDetails: objCouponCredit)
                        }else {
                            //Delete Credit
                            print("Delete Credit")
                            self.deleteCreditOrCoupon(objCouponDetails: objCouponCredit)
                        }
                    }
                }
                
            }
        }
        
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
    }
    
    func deleteCreditOrCoupon(objCouponDetails: NSManagedObject) {
        print(objCouponDetails)
        
        let strServiceName = "\(objCouponDetails.value(forKey: "serviceSysName") ?? "")"
        let strDiscountAmount = "\(objCouponDetails.value(forKey: "appliedInitialDiscount") ?? "")"
        let strDiscountPer = "\(objCouponDetails.value(forKey: "discountPercent") ?? "")"
        let strChkDiscountPer = "\(objCouponDetails.value(forKey: "isDiscountPercent") ?? "")"
        
        let strDiscountType = "\(objCouponDetails.value(forKey: "discountType") ?? "")"

        
        let context = getContext()
        context.delete(objCouponDetails)
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
                
        self.updateDiscountAfterDelete(strService: strServiceName, strDiscountAmount: strDiscountAmount, strDiscountPer: strDiscountPer, strChkDiscountPer: strChkDiscountPer, strType: strDiscountType)
        
    }

    func updateAddToAgreementBundle(isAddAgreement: String, indexPath: IndexPath, bundleRow: Int) {
        
        var objCustomServiceDetail = NSManagedObject()
        
        objCustomServiceDetail = self.arrBundleService[bundleRow]
        objCustomServiceDetail.setValue(isAddAgreement, forKey: "isSold")
        
        
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
    }
    func updateLeadDetails() {
        
        objLeadDetails?.setValue(strPreferedMonth, forKey: "strPreferredMonth")
        objLeadDetails?.setValue(strInternalNote, forKey: "notes")
        objLeadDetails?.setValue(strProposalNote, forKey: "proposalNotes")

        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func updatePaymentInfo() {
        
        objPaymentInfo?.setValue(strCustomerNote, forKey: "specialInstructions")

        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func updateAgreementCheckList(strCheckList: String) {
        objAgreementDetails?.setValue(strCheckList, forKey: "agreementChecklistSysName")
        
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //Observer Method
    @objc private func notesValue(notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: Commons.kNotificationNotes, object: nil)

        
        let dict: [String: String] = (notification.userInfo as? [String: String])!
        if notification.object as! String == NotesForConfigure.customer.rawValue {
            self.strCustomerNote = dict["note"] ?? ""
            self.updatePaymentInfo()
        }else if notification.object as! String == NotesForConfigure.proposal.rawValue {
            self.strProposalNote = dict["note"] ?? ""
            self.updateLeadDetails()
        }else {
            self.strInternalNote = dict["note"] ?? ""
            self.updateLeadDetails()
        }
        
        tblView.reloadData()
    }
    
    //Observer Method
    @objc private func creditMethodValue(notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: Commons.kNotificationCredit, object: nil)

        self.dictSelectedCredit = (notification.userInfo as! [String : Any])
        let indexPath = IndexPath(row: 0, section: (3 - (3 - self.secTitleCount)))
        let cell: ConfigurePricingTableViewCell = self.tblView.cellForRow(at: indexPath)! as! ConfigurePricingTableViewCell
        cell.txtCredit.text = dictSelectedCredit?["Name"] as? String ?? ""
    }
    
    //MARK:- *CoreData Update For Credit & Coupon*
    /***************CoreData Update for Credit & Coupon********************/
    func updateStandardServiceDataBase(strServiceNameDiscount: String, strDiscountAmount: String, strDisountCheck: String, strDiscountPercent: String, isServiceBaseCoupon: Bool) {
        
        print(strServiceNameDiscount)
        print(strDiscountAmount)
        print(strDisountCheck)
        print(strDiscountPercent)
        print(isServiceBaseCoupon)

        let context = getContext()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && isSold == %@", self.strLeadId, "true"))

        if arryOfData.count > 0 {
            
            var chkDiscountApplied = false
            var chkForServiceBasedCoupon = false
            var chkForBundle = false
            self.isCouponSaved = false

            var objMatchesDiscoutUpdate : NSManagedObject?
            for objWorkorderDetail in arryOfData {
                //bundleId
//                let bundleId = "\((objWorkorderDetail as AnyObject).value(forKey: "bundleId") ?? "")"
                objMatchesDiscoutUpdate = (objWorkorderDetail as! NSManagedObject)

//                if bundleId == "" || bundleId == "0"
//                {
                    chkForServiceBasedCoupon = true
                    chkForBundle = false
                    if objMatchesDiscoutUpdate?.value(forKey: "serviceSysName") as! String == strServiceNameDiscount {
                        
                        if strDisountCheck == "1" {
                            var discountPerCoupon = 0.0
                            var discount = 0.0
                            discountPerCoupon = Double(strDiscountPercent)! + (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discountPercentage") ?? "0")") ?? 0.0)
                            
                            if discountPerCoupon >= 100 {
                                discountPerCoupon = 100
                            }
                            
                            //initialPrice
                            discount = ((Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) * discountPerCoupon)/100
                            print(discount)
                            
                            if discount > (Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) {
                                discount = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0)
                            }
                            
                            //Set value for update
                            objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discount), forKey: "discount")
                            objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discountPerCoupon), forKey: "discountPercentage")
                            
                            chkDiscountApplied = true
                            //Calulcate Applied Discount Amount For Initial And Maint
                            //ToDo
                            self.couponDiscountPrice = discount
                            print(self.couponDiscountPrice)
                            

                        }else {
                            
                            var discountPerCoupon = 0.0
                            var discount = 0.0
                            
                            //initialPrice
                            discount = Double(strDiscountAmount)! + (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discount") ?? "0")") ?? 0.0)
                            if discount > (Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) {
                                discount = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0)
                            }
                            
                            
                            discountPerCoupon = discountPerCoupon + (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discountPercentage") ?? "0")") ?? 0.0)
                            
                            discountPerCoupon=(discount*100)/(Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0);
                            
                            
                            //Set value for update
                            objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discount), forKey: "discount")
                            objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discountPerCoupon), forKey: "discountPercentage")
                            
                            chkDiscountApplied = true
                            
                            //Calulcate Applied Discount Amount For Initial And Maint
                            self.couponDiscountPrice = discount
                            print(self.couponDiscountPrice)
                            
                        }
                        self.isCouponSaved = true
                    }
                    
            }
            
            if !chkDiscountApplied {
                if(strServiceNameDiscount.count == 0)//Non ServiceBased
                {
                    objMatchesDiscoutUpdate = (arryOfData[0] as! NSManagedObject)

                        if strDisountCheck == "1" {
                            //***Discount: Persentage Base***
                            var discountPerCoupon = 0.0
                            var discount = 0.0
                            
                            discountPerCoupon = Double(strDiscountPercent)! + (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discountPercentage") ?? "0")") ?? 0.0)
                            
                            if discountPerCoupon >= 100 {
                                discountPerCoupon = 100
                            }
                            
                            //initialPrice
                            discount = ((Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) * discountPerCoupon)/100
                            print(discount)
                            
                            if discount > (Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) {
                                discount = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0)
                            }
                            
                            //Set value for update
                            objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discount), forKey: "discount")
                            objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discountPerCoupon), forKey: "discountPercentage")
                            
                            chkDiscountApplied = true
                            //Calulcate Applied Discount Amount For Initial And Maint
                            //ToDo
                            self.couponDiscountPrice = discount
                            print(self.couponDiscountPrice)
                            

                        }else {
                            //***Discount: Amount Base***
                            var discountPerCoupon = 0.0
                            var discount = 0.0
                            
                            //initialPrice
                            discount = Double(strDiscountAmount)! + (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discount") ?? "0")") ?? 0.0)
                            if discount > (Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) {
                                discount = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0)
                            }
                            
                            
                            discountPerCoupon = discountPerCoupon + (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discountPercentage") ?? "0")") ?? 0.0)
                            
                            discountPerCoupon=(discount*100)/(Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0);
                            
                            
                            //Set value for update
                            objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discount), forKey: "discount")
                            objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discountPerCoupon), forKey: "discountPercentage")
                            
                            chkDiscountApplied = true
                            
                            //Calulcate Applied Discount Amount For Initial And Maint
                            self.couponDiscountPrice = discount
                            print(self.couponDiscountPrice)
                            
                    }
                }
                self.isCouponSaved = true
            }

            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
            self.initialSetup()

        }else{
            
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
        }
    }
    
    func fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot(strType: String) -> [NSManagedObject] {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        print(arryOfData)
        
        var arrDiscountCoupon : [NSManagedObject] = []
        var arrDiscountCredit : [NSManagedObject] = []

        let arrStanAllSoldService = self.arrStandardServiceSold + self.arrBundleServiceSold
        print(arrStanAllSoldService.count)

        if arryOfData.count > 0 {
            for objMetchDiscount in arryOfData {
                let soldServiceId = "\((objMetchDiscount as AnyObject).value(forKey: "soldServiceId") ?? "")"
                let discountType = "\((objMetchDiscount as AnyObject).value(forKey: "discountType") ?? "")"

                for objStanAllSoldService in arrStanAllSoldService {
                    print(objStanAllSoldService.value(forKey: "soldServiceStandardId") as! String)
                    if objStanAllSoldService.value(forKey: "soldServiceStandardId") as! String == soldServiceId {

                        if discountType == "Coupon" {
                            arrDiscountCoupon.append(objMetchDiscount as! NSManagedObject)
                        }else {
                            arrDiscountCredit.append(objMetchDiscount as! NSManagedObject)
                        }
                    }
                }
            }
            print(self.arrCustomService)
            
        }
        

        if strType == "Coupon"
        {
            return arrDiscountCoupon
        }else {
            return arrDiscountCredit
        }
    }
    
    func checkCouponValidity(dictCouponDetail: [String: Any]) -> Bool {
        
        
        var strValidFrom =  changeStringDateToGivenFormat(strDate: "\(dictCouponDetail["ValidFrom"] ?? "")", strRequiredFormat: "MM/dd/yyyy")
        
        if strValidFrom.count == 0 || strValidFrom == ""
        {
            strValidFrom = Global().getCurrentDate()
        }
        
        var strValidTo = changeStringDateToGivenFormat(strDate: "\(dictCouponDetail["ValidTo"] ?? "")", strRequiredFormat: "MM/dd/yyyy")
        
        if strValidTo.count == 0 || strValidTo == ""
        {
            strValidTo = Global().getCurrentDate()
        }
        
        let strCurrentDate = changeStringDateToGivenFormat(strDate: Global().getCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        
        //01 July -  05 July   02 July
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let currentDate = dateFormatter.date(from:strCurrentDate)!
        let validFrom = dateFormatter.date(from:strValidFrom)!
        let validTo = dateFormatter.date(from:strValidTo)!

        if currentDate >= validFrom && currentDate <= validTo
        {
            return true
        }
        else
        {
            return false
        }
       
    }
    
    func checkForAppliedDiscount(strType: String, strDiscountCode: String, couponUsage: String) -> Bool {
        print(strType, strDiscountCode, couponUsage)
        
        
        var predicate: NSPredicate?
        if strType == "Credit" {
            if couponUsage == "OneTime" {
                predicate = NSPredicate(format: "accountNo == %@ && discountSysName == %@", self.strAccountNoGlobal, strDiscountCode)
            }else if couponUsage == "Multiple" {
                predicate = NSPredicate(format: "leadId == %@ && discountSysName == %@", self.strLeadId, strDiscountCode)
            }else {
                predicate = NSPredicate(format: "leadId == %@ && discountSysName == %@", self.strLeadId, strDiscountCode)
            }
        }else {
            if couponUsage == "OneTime" {
                predicate = NSPredicate(format: "accountNo == %@ && discountCode == %@", self.strAccountNoGlobal, strDiscountCode)
            }else if couponUsage == "Multiple" {
                predicate = NSPredicate(format: "leadId == %@ && discountCode == %@", self.strLeadId, strDiscountCode)
            }else {
                predicate = NSPredicate(format: "leadId == %@ && discountCode == %@", self.strLeadId, strDiscountCode)
            }
        }
        
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: predicate!)
        
        print(arryOfData)
        
        if couponUsage == "OneTime" {
            if arryOfData.count == 0 {
                return false
            }else {
                return true
            }
        }else if couponUsage == "Multiple" {
            if arryOfData.count == 0 {
                return false
            }else {
                return true
            }
        }else {
            return true
        }
    }
    
    func saveAppliedDiscountCouponAndCreditCoreData(dictCouponDetail: [String: Any], strType: String) {
        print(dictCouponDetail)
        let context = getContext()
        let strServiceName = "\(dictCouponDetail["ServiceSysName"] ?? "")"
        let strDiscountAmount = "\(dictCouponDetail["DiscountAmount"] ?? "")"
        var strDiscountPer = ""
        var strChkDiscountPer = ""

        if strType == "Coupon" {
            
            let appliedDiscountInfo = LeadAppliedDiscounts(context: context)
            
            appliedDiscountInfo.leadAppliedDiscountId = "\(dictCouponDetail["DiscountSetupId"] ?? "")"
            appliedDiscountInfo.serviceSysName = strServiceName
            appliedDiscountInfo.discountSysName = "\(dictCouponDetail["SysName"] ?? "")"
            appliedDiscountInfo.discountType = "\(dictCouponDetail["Type"] ?? "")"
            appliedDiscountInfo.discountCode = "\(dictCouponDetail["DiscountCode"] ?? "")"
            appliedDiscountInfo.discountAmount = strDiscountAmount
            appliedDiscountInfo.discountDescription = "\(dictCouponDetail["Description"] ?? "")"
//            appliedDiscountInfo.applicableForInitial = "\(dictCouponDetail["ApplicableForInitial"] ?? "")"
//            appliedDiscountInfo.applicableForMaintenance = "\(dictCouponDetail["ApplicableForMaintenance"] ?? "")"
            
            if "\(dictCouponDetail["ApplicableForInitial"] ?? "0")" == "1" || "\(dictCouponDetail["ApplicableForInitial"] ?? "false")" == "true" {
                appliedDiscountInfo.applicableForInitial = "true"
            }else {
                appliedDiscountInfo.applicableForInitial = "false"
            }
            
            if "\(dictCouponDetail["ApplicableForMaintenance"] ?? "0")" == "1" || "\(dictCouponDetail["ApplicableForMaintenance"] ?? "false")" == "true" {
                appliedDiscountInfo.applicableForMaintenance = "true"
            }else {
                appliedDiscountInfo.applicableForMaintenance = "false"
            }
            
            if "\(dictCouponDetail["IsActive"] ?? "0")" == "1" {
                appliedDiscountInfo.isActive = "true"
            }else {
                appliedDiscountInfo.isActive = "false"
            }
            appliedDiscountInfo.isApplied = "true"
            

            let arrStanAllSoldService = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && isSold == %@", self.strLeadId, "true"))
            //let arrStanAllSoldService = self.arrStandardService + self.arrBundleService
            if arrStanAllSoldService.count > 0 {
                let objStanAllSoldService: NSManagedObject = arrStanAllSoldService[0] as! NSManagedObject
                appliedDiscountInfo.soldServiceId = (objStanAllSoldService.value(forKey: "soldServiceStandardId") as! String)
                if strServiceName == "" {
                    appliedDiscountInfo.serviceSysName = (objStanAllSoldService.value(forKey: "serviceSysName") as! String)
                }
                
                if "\(dictCouponDetail["IsDiscountPercent"] ?? "0")" == "1" {
                    strChkDiscountPer = "true"
                    strDiscountPer = "\(dictCouponDetail["DiscountPercent"] ?? "0")"
                    
                    let initialPrice = Double((objStanAllSoldService.value(forKey: "totalInitialPrice") as! String)) ?? 0.0
                    let discountAmount = (initialPrice * (Double(strDiscountPer) ?? 0.0))/100
                    appliedDiscountInfo.discountAmount = "\(discountAmount)"
                    self.appliedDiscountInitial = discountAmount;

                }else {
                    strChkDiscountPer = "false"
                    strDiscountPer = "0"
                }
                appliedDiscountInfo.isDiscountPercent = strChkDiscountPer
                appliedDiscountInfo.discountPercent = strDiscountPer
            }else {
                appliedDiscountInfo.soldServiceId = "false"
            }
            
            /*let arrStanAllSoldService = self.arrStandardService + self.arrBundleService
            if arrStanAllSoldService.count > 0 {
                let objStanAllSoldService = arrStanAllSoldService[0]
                appliedDiscountInfo.soldServiceId = (objStanAllSoldService.value(forKey: "soldServiceStandardId") as! String)
                if strServiceName == "" {
                    appliedDiscountInfo.serviceSysName = (objStanAllSoldService.value(forKey: "serviceSysName") as! String)
                }
            }else {
                appliedDiscountInfo.soldServiceId = "false"
            }*/
            
            appliedDiscountInfo.name = "\(dictCouponDetail["Name"] ?? "")"
            appliedDiscountInfo.serviceType = "Standard"
            appliedDiscountInfo.leadId = self.strLeadId
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            appliedDiscountInfo.userName = strUserName
            appliedDiscountInfo.companyKey = strCompanyKey
            
            appliedDiscountInfo.appliedMaintDiscount = String(format: "%.02f", self.appliedDiscountMaint)
            appliedDiscountInfo.appliedInitialDiscount = String(format: "%.02f", self.appliedDiscountInitial);
            
            appliedDiscountInfo.accountNo = self.strAccountNoGlobal

            
        }else {
            //Credit
            let appliedDiscountInfo = LeadAppliedDiscounts(context: context)
            
            appliedDiscountInfo.leadAppliedDiscountId = "\(dictCouponDetail["DiscountSetupId"] ?? "")"
            appliedDiscountInfo.serviceSysName = strServiceName
            appliedDiscountInfo.discountSysName = "\(dictCouponDetail["SysName"] ?? "")"
            appliedDiscountInfo.discountType = "\(dictCouponDetail["Type"] ?? "")"
            appliedDiscountInfo.discountCode = "\(dictCouponDetail["DiscountCode"] ?? "")"
            appliedDiscountInfo.discountAmount = strDiscountAmount
            appliedDiscountInfo.discountDescription = "\(dictCouponDetail["Description"] ?? "")"
            //appliedDiscountInfo.applicableForInitial = "\(dictCouponDetail["ApplicableForInitial"] ?? "")"
            //appliedDiscountInfo.applicableForMaintenance = "\(dictCouponDetail["ApplicableForMaintenance"] ?? "")"
            if "\(dictCouponDetail["ApplicableForInitial"] ?? "0")" == "1" || "\(dictCouponDetail["ApplicableForInitial"] ?? "false")" == "true" {
                appliedDiscountInfo.applicableForInitial = "true"
            }else {
                appliedDiscountInfo.applicableForInitial = "false"
            }
            
            if "\(dictCouponDetail["ApplicableForMaintenance"] ?? "0")" == "1" || "\(dictCouponDetail["ApplicableForMaintenance"] ?? "false")" == "true" {
                appliedDiscountInfo.applicableForMaintenance = "true"
            }else {
                appliedDiscountInfo.applicableForMaintenance = "false"
            }
            if "\(dictCouponDetail["IsActive"] ?? "0")" == "1" {
                appliedDiscountInfo.isActive = "true"
            }else {
                appliedDiscountInfo.isActive = "false"
            }
            
            if "\(dictCouponDetail["IsDiscountPercent"] ?? "0")" == "1" {
                strChkDiscountPer = "true"
                strDiscountPer = "\(dictCouponDetail["DiscountPercent"] ?? "0")"
            }else {
                strChkDiscountPer = "false"
                strDiscountPer = "0"
            }
            appliedDiscountInfo.isDiscountPercent = strChkDiscountPer
            appliedDiscountInfo.discountPercent = strDiscountPer

            let arrStanAllSoldService = self.arrStandardServiceSold + self.arrBundleServiceSold
            if arrStanAllSoldService.count > 0 {
                let objStanAllSoldService = arrStanAllSoldService[0]
                appliedDiscountInfo.soldServiceId = (objStanAllSoldService.value(forKey: "soldServiceStandardId") as! String)
            }else {
                appliedDiscountInfo.soldServiceId = "false"
            }
            
            appliedDiscountInfo.name = "\(dictCouponDetail["Name"] ?? "")"
            appliedDiscountInfo.serviceType = "Standard"
            appliedDiscountInfo.leadId = self.strLeadId
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            appliedDiscountInfo.userName = strUserName
            appliedDiscountInfo.companyKey = strCompanyKey

            //self.creditAmountMaint
            //self.creditAmount
            appliedDiscountInfo.appliedMaintDiscount = String(format: "%.02f", self.appliedDiscountMaint)
            appliedDiscountInfo.appliedInitialDiscount = String(format: "%.02f", self.appliedDiscountInitial);
            
            appliedDiscountInfo.accountNo = self.strAccountNoGlobal

            
        }
        
        do { try context.save()
            print("Record Saved Successfully.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
       // self.getAppliedCouponOrCredit()
    }
    
    func updateDiscountAfterDelete(strService: String, strDiscountAmount: String, strDiscountPer: String, strChkDiscountPer: String, strType: String) {
        print(strService)
        print(strDiscountAmount)
        print(strDiscountPer)
        print(strChkDiscountPer)

        
        let context = getContext()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && isSold == %@", self.strLeadId, "true"))

        if arryOfData.count > 0 {
            
            var chkDiscountApplied = false
            var chkForServiceBasedCoupon = false
            var chkForBundle = false
            self.isCouponSaved = false

            var objMatchesDiscoutUpdate : NSManagedObject?
            
            for objWorkorderDetail in arryOfData {
                
                objMatchesDiscoutUpdate = (objWorkorderDetail as! NSManagedObject)
                chkForServiceBasedCoupon = true
                chkForBundle = false
                if objMatchesDiscoutUpdate?.value(forKey: "serviceSysName") as! String == strService {
                    
                    if strChkDiscountPer == "1" || strChkDiscountPer == "true" {
                        var discountPerCoupon = 0.0
                        var discount = 0.0
                        discountPerCoupon = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discountPercentage") ?? "0")") ?? 0.0) - Double(strDiscountPer)!
                        //initialPrice
                        discount = ((Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) * discountPerCoupon)/100
                        
                        
                        //Set value for update
                        objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discount), forKey: "discount")
                        objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discountPerCoupon), forKey: "discountPercentage")
                        
                        chkDiscountApplied = true
                        //Calulcate Applied Discount Amount For Initial And Maint
                        //ToDo
                        self.couponDiscountPrice = discount
                        print(self.couponDiscountPrice)
                        
                        
                    }else {
                        
                        var discountPerCoupon = 0.0
                        var discount = 0.0
                        
                        //initialPrice
                        if strType == "Coupon" {
                            discount = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discount") ?? "0")") ?? 0.0) - Double(strDiscountAmount)!
                        }else {
                            discount = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discount") ?? "0")") ?? 0.0)
                        }
                        
                        discountPerCoupon=(discount*100)/(Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0);
                        
                        
                        //Set value for update
                        objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discount), forKey: "discount")
                        objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discountPerCoupon), forKey: "discountPercentage")
                        
                        chkDiscountApplied = true
                        
                        //Calulcate Applied Discount Amount For Initial And Maint
                        self.couponDiscountPrice = discount
                        print(self.couponDiscountPrice)
                        
                    }
                }
            }
            
            if !chkDiscountApplied {
                if(strService.count == 0)//Non ServiceBased
                {
                    objMatchesDiscoutUpdate = (arryOfData[0] as! NSManagedObject)
                    
                    if strChkDiscountPer == "1" || strChkDiscountPer == "true" {
                        var discountPerCoupon = 0.0
                        var discount = 0.0
                        
                        //Set value for update
                        if strType == "Coupon" {
                            discountPerCoupon = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discountPercentage") ?? "0")") ?? 0.0) - Double(strDiscountPer)!
                        }else {
                            discountPerCoupon = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discountPercentage") ?? "0")") ?? 0.0)
                        }
                        
                        discount = ((Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0) * discountPerCoupon)/100
                        
                        discountPerCoupon=(discount*100)/(Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0);
                        
                        
                        //Set value for update
                        objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discount), forKey: "discount")
                        objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discountPerCoupon), forKey: "discountPercentage")
                        
                        chkDiscountApplied = true
                        
                    }else {
                        
                        var discountPerCoupon = 0.0
                        var discount = 0.0
                        
                        //initialPrice
                        if strType == "Coupon" {
                            discount = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discount") ?? "0")") ?? 0.0) - Double(strDiscountAmount)!
                        }else {
                            discount = (Double("\(objMatchesDiscoutUpdate?.value(forKey: "discount") ?? "0")") ?? 0.0)
                        }
                        
                        discountPerCoupon=(discount*100)/(Double("\(objMatchesDiscoutUpdate?.value(forKey: "totalInitialPrice") ?? "0")") ?? 0.0);
                        
                        
                        //Set value for update
                        objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discount), forKey: "discount")
                        objMatchesDiscoutUpdate!.setValue(String(format: "%.2f", discountPerCoupon), forKey: "discountPercentage")
                        
                    }
                }
            }

            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
            self.initialSetup()

        }else{
            
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
        }
    }
    /*******************************************************************/
    
    //MARK:-------------------------------------- Nilind --------------------------------------
    
    //MARK:------------------ Service & Proposal Function -------------

    func basicFunction()  {
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strLeadId = ("\(nsud.value(forKey: "LeadId") ?? "")" as NSString) as String
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        let strPreferdMonth: String = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPreferredMonth") ?? "")"
        if strPreferdMonth == "1" || strPreferdMonth == "true" {
            isPreferredMonths = true
        }else{
            isPreferredMonths = false
        }
        
        matchesGeneralInfo = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
            
        strGlobalStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        strGlobalStatusSysName = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strBranchSysName = ("\(matchesGeneralInfo.value(forKey: "branchSysName") ?? "")" as NSString) as String
        
    }
    
    func getServicePropsalBasicDetails() {
       
        if nsud.value(forKey: "companyDetail") is NSDictionary
        {
            let dictCompanyDetail = nsud.value(forKey: "companyDetail") as! NSDictionary
            
            isProposalFoolowUp = (dictCompanyDetail.value(forKey: "IsProposalFollowUp") ?? false) as! Bool
            isServiceFollowUp = (dictCompanyDetail.value(forKey: "IsServiceFollowUp") ?? false) as! Bool
            serviceFollowDays = "\(dictCompanyDetail.value(forKey: "ServiceFollowUpDays") ?? "")".count > 0 ? (dictCompanyDetail.value(forKey: "ServiceFollowUpDays") ?? 0) as! Int : 0
            proposalFollowDays = "\(dictCompanyDetail.value(forKey: "ProposalFollowUpDays") ?? "")".count > 0 ? (dictCompanyDetail.value(forKey: "ProposalFollowUpDays") ?? 0) as! Int : 0
        }
        
        
        arySalesServiceFollowUpList = getSoldServiceDepartments()
        
        let object = getUnSoldServiceDepartmentsAndServices()
        arySalesProposalFollowUpList = object.arrUnSoldDept
        arySalesProposalServices = object.arrProposalService
    }
    
    func getSoldServiceDepartments() -> NSArray {
        
        let arrSoldDept = NSMutableArray()
        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
    
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
        
        
        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            arrSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
        }
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            arrSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
        }

        let arrUniqueDept = arrSoldDept.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrUniqueDept)")
        
        
        return arrUniqueDept
    }
    
    func getUnSoldServiceDepartmentsAndServices() -> (arrUnSoldDept : NSArray, arrProposalService: NSArray) {
        
        let arrUnSoldDept = NSMutableArray()
        let arrUnSoldServiceName = NSMutableArray()

        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
    
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
        
        
        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            arrUnSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
            
            arrUnSoldServiceName.add(getServiceObjectFromIdAndSysName(strId: "", strSysName: strServiceSysName).value(forKey: "Name") ?? "")
        }
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            arrUnSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
            arrUnSoldServiceName.add("\(matches.value(forKey: "serviceName") ?? "")")
        }


        let arrUniqueDept = arrUnSoldDept.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrUniqueDept)")
        
        
        return (arrUniqueDept, arrUnSoldServiceName)
    }
    
    
    func getServiceDetailFromDept(strDepartmentSysName : String) -> String  {
        
        let arrServiceName = NSMutableArray()
        
        //Standard
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))

        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            
            if strDeptSysName == strDepartmentSysName
            {
                let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: strServiceSysName)
                arrServiceName.add("\(dictService.value(forKey: "Name") ?? "")")
            }
        }
        
        //Custom
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            
            if strDeptSysName == strDepartmentSysName
            {
                arrServiceName.add("\(matches.value(forKey: "serviceName") ?? "")")
            }
        }
        
    
        if arrServiceName.count > 0
        {
            return arrServiceName.componentsJoined(by: ", ")
        }
        else
        {
            return ""
        }
    }
    func saveServiceFollowUp()  {
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        let strToday = changeStringDateToGivenFormat(strDate: Global().strCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let modifiedDate = Calendar.current.date(byAdding: .day, value: serviceFollowDays, to: dateFormatter.date(from: strToday)!)!
        let strFollowUpDate = dateFormatter.string(from: modifiedDate)
        
        for item in arySalesServiceFollowUpList
        {
            let matches = item as! NSDictionary
            
            arrOfKeys = [
                "departmentId",
                "departmentName",
                "departmentSysName",
                "followupDate",
                "id",
                "leadId",
                "notes",
                "serviceId",
                "serviceName",
                "serviceSysName"
            ]
            
            arrOfValues = [
                "",
                "\(matches.value(forKey: "Name") ?? "")",
                "\(matches.value(forKey: "SysName") ?? "")",
                strFollowUpDate, //\(Global().strCurrentDate() ?? "")
                (Global().getReferenceNumberNew() ?? ""),
                strLeadId,
                "Follow up for: \(getServiceDetailFromDept(strDepartmentSysName: "\(matches.value(forKey: "SysName") ?? "")"))",
                "",
                "",
                ""
            ]
            
            saveDataInDB(strEntity: "ServiceFollowUpDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        

    }
    func fetchServiceProposalFollowUp() {
        
        getServicePropsalBasicDetails()
        
        if getOpportunityStatus()
        {
            
        }
        else
        {
            if isServiceFollowUp
            {
                deleteServiceFollowup()
                saveServiceFollowUp()
            }
            if isProposalFoolowUp
            {
                deleteProposalFollowup()
                saveProposalFollowUp()
            }
            
        }
    }
    
    func deleteServiceFollowup()  {
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "ServiceFollowUpDcs", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))

        for item in arrayData
        {
            let objTempSales = item as! NSManagedObject
            deleteDataFromDB(obj: objTempSales)

        }
    }
    func deleteProposalFollowup()  {
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "ProposalFollowUpDcs", predicate: NSPredicate(format: "proposalLeadId == %@", self.strLeadId))
        
        for item in arrayData
        {
            let objTempSales = item as! NSManagedObject
            deleteDataFromDB(obj: objTempSales)

        }
    }
    func saveProposalFollowUp()  {
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
    
        let strToday = changeStringDateToGivenFormat(strDate: Global().strCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let modifiedDate = Calendar.current.date(byAdding: .day, value: proposalFollowDays, to: dateFormatter.date(from: strToday)!)!
        let strFollowUpDate = dateFormatter.string(from: modifiedDate)
        
        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
    
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))

        
        
        for item in arraySoldServiceStandard //For Standard
        {
            let matches = item as! NSManagedObject
            
            let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "serviceSysName") ?? "")")
            
            
            arrOfKeys = [
                "proposalFollowupDate",
                "proposalId",
                "proposalLeadId",
                "proposalNotes",
                "proposalServiceId",
                "proposalServiceName",
                "proposalServiceSysName"
            ]
            
            
            arrOfValues = [
                strFollowUpDate, //\(Global().strCurrentDate() ?? "")
                "\(Global().getReferenceNumberNew() ?? "")",
                 strLeadId,
                "Follow up for: \(dictService.value(forKey: "Name") ?? "")",
                "\(matches.value(forKey: "serviceId") ?? "")",
                "\(dictService.value(forKey: "Name") ?? "")",
                "\(matches.value(forKey: "serviceSysName") ?? "")"
            ]
            
            saveDataInDB(strEntity: "ProposalFollowUpDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        for item in arraySoldServiceCustom //For Custom
        {
            let matches = item as! NSManagedObject
            
            arrOfKeys = [
                "proposalFollowupDate",
                "proposalId",
                "proposalLeadId",
                "proposalNotes",
                "proposalServiceId",
                "proposalServiceName",
                "proposalServiceSysName"
            ]
            
            arrOfValues = [
                strFollowUpDate, //\(Global().strCurrentDate() ?? "")
                "\(Global().getReferenceNumberNew() ?? "")",
                 strLeadId,
                "Follow up for: \(matches.value(forKey: "serviceName") ?? "")", //Follow up for: \(getServiceDetailFromDept(strDepartmentSysName: "\(matches.value(forKey: "departmentSysname") ?? "")"))
                "",
                "\(matches.value(forKey: "serviceName") ?? "")",
                "\(matches.value(forKey: "serviceName") ?? "")"
            ]
            
            saveDataInDB(strEntity: "ProposalFollowUpDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
    }
    
    //MARK:------------------ Other Documents Function -------------
    
    func fetchOtherDocumentFromMaster() -> NSArray  {
        
        //let arrDept = getBranchWiseDepartment(strBranchSysName: strBranchSysName) as! NSArray
        var arrOtherDocuments = NSArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "OtherDocumentsMasterExts") is NSArray){
                
                arrOtherDocuments = dictMaster.value(forKey: "OtherDocumentsMasterExts") as! NSArray
            }
        }
        
        
        let arrAllDocuments = NSMutableArray()
        
        //For Sold Service
        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, isSendProposal ? "false" : "true"))

        
        
        //Fetch Documets for Sold Serivce Department, Category and Service Wise
        
        for item in arraySoldServiceStandard
        {
            let match = item as! NSManagedObject
            let strServiceId = "\(match.value(forKey: "serviceId") ?? "")"
            let strServiceSysName = "\(match.value(forKey: "serviceSysName") ?? "")"
            let dictCategory = getCategoryObjectFromServiceSysName(strServiceSysName: strServiceSysName)
            let strCategorySysName = "\(dictCategory.value(forKey: "SysName") ?? "")"
            let strDeptSysName =  getDeptSysNameFromServiceSysName(strServiceSysName: "\(match.value(forKey: "serviceSysName") ?? "")")

            
            for itemDoc in arrOtherDocuments
            {
                let dictDoc = itemDoc as! NSDictionary
                
                if "\(dictDoc.value(forKey: "DepartmentSysName") ?? "")" == strDeptSysName || "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == strCategorySysName || "\(dictDoc.value(forKey: "ServiceId") ?? "")" == strServiceId
                {
                    arrAllDocuments.add(dictDoc)
                }
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == strServiceSysName && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == ""
                {
                    arrAllDocuments.add(dictDoc)
                }
                if ("\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == strServiceSysName || "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "0") && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == ""
                {
                    arrAllDocuments.add(dictDoc)
                }
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == strServiceSysName && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == strCategorySysName
                {
                    arrAllDocuments.add(dictDoc)
                }
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "" && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == ""
                {
                    arrAllDocuments.add(dictDoc)
                }
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "0" && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == ""
                {
                    arrAllDocuments.add(dictDoc)
                }
            }
        }
        
        //Fetch Document for Custom
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, isSendProposal ? "false" : "true"))
        
        for item in arraySoldServiceCustom
        {
            let match = item as! NSManagedObject
            let strDeptSysName = "\(match.value(forKey: "departmentSysname") ?? "")"
            
            for itemDoc in arrOtherDocuments
            {
                let dictDoc = itemDoc as! NSDictionary
                if "\(dictDoc.value(forKey: "DepartmentSysName") ?? "")" == strDeptSysName && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == "" && ("\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "" || "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "0")
                {
                    arrAllDocuments.add(dictDoc)
                }
            }
        }
        
        
        let arrUniqueDocuments = arrAllDocuments.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrUniqueDocuments)")
        
        return arrUniqueDocuments

    }
    
    func fetchOtherDocumentsFromDB()  {
        
        let arrAllDocuments = fetchOtherDocumentFromMaster()
        
        let arrSelectedDocument = NSMutableArray()
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        for item in arrayData
        {
            let matches = item as! NSManagedObject
            
            for itemOtherDoc in arrAllDocuments
            {
                let dictDoc = itemOtherDoc as! NSDictionary
                
                if "\(dictDoc.value(forKey: "OtherDocSysName") ?? "")" == "\(matches.value(forKey: "") ?? "otherDocSysName")"
                {
                    arrSelectedDocument.add(dictDoc)
                }
            }
        }
        for itemOtherDoc in arrAllDocuments
        {
            let dictDoc = itemOtherDoc as! NSDictionary
            
            if "\(dictDoc.value(forKey: "isDefault") ?? "")" == "true"
            {
                arrSelectedDocument.add(dictDoc)
            }
        }

    }
    func saveOtherDocuments()  {
        
    }
    func deleteOtherDocuments()  {
        
    }
    
}

extension SalesConfigureViewController: SalesConfigureDelegate {
    func sucessfullyGetTemplateKey(_ output: Any) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: {
            self.fetchServiceProposalFollowUp()
            self.loader.dismiss(animated: false) {
                
                if let dict: [String: Any] = output as? [String : Any] {
                    if let msg = dict["Message"] {
                        print(msg)
                        //Template key not found
                        self.goToReview()
                    }else {
                        
                        if let strTemplateKey = dict["TemplateKey"] as? String {
                            if strTemplateKey.count > 0 {
                                self.arrImageDetails.removeAll()
                                self.arrGraphDetails.removeAll()
                                
                                self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                self.present(self.loader, animated: false, completion: nil)
                                
                                self.fetchImageFromDB(strHeaderTitle: "Before") { success in
                                    if success {
                                        self.uploadCRMImages() { success in
                                            if success {
                                                self.fetchImageFromDB(strHeaderTitle: "Graph") { success in
                                                    self.loader.dismiss(animated: false) {
                                                        if success {
                                                            self.goToFormBuilder(dict: dict)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }else {
                                self.goToReview()
                            }
                        }
                        else {
                            print("Failure")
                            self.goToReview()
                        }
                    }
                }else {
                    print("Nulllllll")
                    self.goToReview()
                }
            }
        })
        
    }
    
    func sucessfullyGetTaxFromTaxCode(_ output: Any) {
        //Tax Value
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: {
            self.loader.dismiss(animated: false) {
                let taxValue: Double = output as! Double
                print(taxValue)
                self.updateLeadDetails(taxValue: taxValue)
            }
            
        })
    }
    
    func sucessfullyGetCredits(_ output: [Any]) {
        print(output)
        self.arrAppliedCreditDetail = output
    }
    
    
    func goToFormBuilder(dict: [String: Any]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = UIStoryboard.init(name: "FormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormBuilderViewController") as? FormBuilderViewController
            vc?.dictTemplateObj = dict
            vc!.matchesGeneralInfo = self.objLeadDetails!
            vc!.objPaymentInfo = self.objPaymentInfo
            vc?.arrImageDetails = self.arrImageDetails
            vc?.arrGraphDetails = self.arrGraphDetails
            vc!.dictPricingInfo = self.dictPricingInfo
            if self.arrStandardServiceSold.count > 0 {
                vc!.arrSoldService = self.arrStandardServiceSold
            }else {
                vc!.arrSoldService = self.arrCustomServiceSold
            }
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    func fetchImageFromDB(strHeaderTitle : String, completion: @escaping(_ success: Bool) -> Void) {
        // Do something
        //let arrayOfImageData = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isImageSyncforMobile == %@", self.strLeadId, strHeaderTitle, "false"))
        let arrayOfImageData = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isImageSyncforMobile == %@ && isInternalUsage == %@", self.strLeadId, strHeaderTitle, "false","false"))

        
        
        print(arrayOfImageData)
        if arrayOfImageData.count > 0 {
            
            var arrImgName: [String] = []
            var arrImgCaption: [String] = []
            var arrImgDescription: [String] = []
            
            for imgName in arrayOfImageData {
                let objTemp: NSManagedObject = imgName as! NSManagedObject
                print(objTemp)
                
                let strImagePath = objTemp.value(forKey: "leadImagePath")
                arrImgName.append(strImagePath as! String)
                
                let strCap = objTemp.value(forKey: "leadImageCaption")
                let strDec = objTemp.value(forKey: "descriptionImageDetail")
                arrImgCaption.append(strCap as! String)
                arrImgDescription.append(strDec as! String)
            }
            print(arrImgName)
            
            let arrOfImages = self.getUIImageFromString(arrOfImagesString : arrImgName)
            
            ServiceGalleryService().uploadImageForGeneralInfo(_strServiceUrlMainServiceAutomation: self.strServiceUrlMain, arrOfImages, arrImgName) { (object, error) in
                if let object = object {
                    print(object)
                    if object.count > 0 {
                        for i in 0..<object.count {
                            let imgPath: String = (object[i].relativePath ?? "") + (object[i].name ?? "")
                            
                            var strUrl = String()
                            strUrl = self.strServiceUrlMain
                                      
                            if imgPath.contains("Documents"){
                                strUrl = strUrl + imgPath
                            }else{
                                strUrl = strUrl + "//Documents/" + imgPath
                            }
                            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                            strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                            
                            // save back in DB after Syncing.
                            
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("leadImagePath")
                            arrOfKeys.add("isImageSyncforMobile")

                            arrOfValues.add(imgPath)
                            arrOfValues.add("true")
                          
                            let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strLeadId,"\(object[i].name ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                            
                            if isSuccess
                            {
                                // Image updated in DB.
                            }
                            
                            if i == object.count - 1 {
                                // loop completed go to fetch all images and redirect to form builder page
                                let isTrue = self.fetchAllImagesFromDBAfterSync(strHeaderTitle : strHeaderTitle)
                                completion(isTrue)
                            }
                        }
                    }else {
                        completion(true)
                    }

                } else {
                    completion(true)
                }
            }
        }else{
            let isTrue = self.fetchAllImagesFromDBAfterSync(strHeaderTitle : strHeaderTitle)

            completion(isTrue)
        }
    }
    
    func fetchAllImagesFromDBAfterSync(strHeaderTitle : String)-> Bool {
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strServiceUrlMainL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        let strServiceCRMUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"

        var arrayOfImageData = NSArray()
        if strHeaderTitle == "Crm" {
            arrayOfImageData = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isInternalUsage == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, "false"))
        }else  {
            arrayOfImageData = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isInternalUsage == %@", self.strLeadId, strHeaderTitle, "false"))
        }
        
        
        //let arrImageCRM = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))

              
        if arrayOfImageData.count > 0 {
            
            for i in 0..<arrayOfImageData.count {
                
                let objTemp: NSManagedObject = arrayOfImageData[i] as! NSManagedObject
                print(objTemp)
                if strHeaderTitle == "Crm" {
                    //CRM Image
                    let strImagePath = objTemp.value(forKey: "path")
                    
                    let strCap = objTemp.value(forKey: "cRMImageCaption")
                    let strDec = objTemp.value(forKey: "cRMImageDescription")
                    
                    let imgPath: String = strImagePath as! String//(object[i].relativePath ?? "") + (object[i].name ?? "")
                    
                    var strUrl = String()
                    strUrl = strServiceCRMUrlMain
//                    if imgPath.contains("Documents"){
//                        strUrl = strUrl + imgPath
//                    }else{
                        strUrl = strUrl + "//Documents/" + imgPath
//                    }
                    
                    strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                    strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                    
                    var order = 0
                    if self.arrImageDetails.count > 0 {
                        order = self.arrImageDetails.count
                    }else {
                        order = i
                    }
                    
                    var dict: [String: Any] = [:]
                    dict.removeAll()
                    dict["image"]       = strUrl
                    dict["order"]       = order + 1
                    dict["caption"]     = strCap as! String
                    dict["description"] = strDec as! String
                    
                    self.arrImageDetails.append(dict)
                    
                }else {
                    let strImagePath = objTemp.value(forKey: "leadImagePath")
                    
                    let strCap = objTemp.value(forKey: "leadImageCaption")
                    let strDec = objTemp.value(forKey: "descriptionImageDetail")
                    
                    let imgPath: String = strImagePath as! String//(object[i].relativePath ?? "") + (object[i].name ?? "")
                    
                    var strUrl = String()
                    strUrl = strServiceUrlMainL
                              
                    if imgPath.contains("Documents"){
                        strUrl = strUrl + imgPath
                    }else{
                        strUrl = strUrl + "//Documents/" + imgPath
                    }
                    
                    strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                    strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                    
                    var dict: [String: Any] = [:]
                    dict.removeAll()
                    dict["image"]       = strUrl
                    dict["order"]       = i + 1
                    dict["caption"]     = strCap as! String
                    dict["description"] = strDec as! String
                    
                    if "\(objTemp.value(forKey: "leadImageType") ?? "")" == "Before" {
                        self.arrImageDetails.append(dict)
                    }else {
                        self.arrGraphDetails.append(dict)
                    }
                }
                
            }
        }
        return true
//        self.goToFormBuilder(dict: dict, strPdfPath: "")
    }
    
    func getUIImageFromString(arrOfImagesString: [String]) -> [UIImage]{
        var arrOfUIImages = [UIImage]()
        
        for i in 0..<arrOfImagesString.count{
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: arrOfImagesString[i])
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: arrOfImagesString[i])
            
            if isImageExists == true{
                arrOfUIImages.append(image!)
            }
            
        }
        
        return arrOfUIImages
    }
    
    
    func getTemplateKey(strURL: String)  {
        
        
        self.fetchServiceProposalFollowUp()

        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        
        print("Sales Auto Get Leads API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "SalesGetLead_AppointmentVC") { (response, status) in
            
            self.loader.dismiss(animated: false) {
                
                if(status)
                {
                    
                    let diff1 = CFAbsoluteTimeGetCurrent() - start1
                    print("Took \(diff1) seconds to get Sales Auto Appointments API Data From SQL Server.")
                    
                    print("Sales Auto Get Appointments API Response")
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        if response.value(forKey: "data") is NSDictionary {
                            
                            let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                            
                            let dictSalesLeadResponseLocal = dictData.value(forKey: "data") as! NSDictionary
                            
                            print(dictSalesLeadResponseLocal)
                            
                            if "\(dictSalesLeadResponseLocal.value(forKey: "TemplateKey") ?? "")".count > 0
                            {
                                self.arrImageDetails.removeAll()
                                self.arrGraphDetails.removeAll()
                                
                                let post_paramsValue = dictSalesLeadResponseLocal as! Dictionary<String,Any>
                                
                                self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                self.present(self.loader, animated: false, completion: nil)
                                
                                //Nilnd
                                self.fetchImageFromDB(strHeaderTitle: "Before") { success in
                                    if success {
                                        
                                        self.uploadCRMImages() { success in
                                            if success {
                                                self.fetchImageFromDB(strHeaderTitle: "Graph") { success in
                                                    self.loader.dismiss(animated: false) {
                                                        if success {
                                                            self.goToFormBuilder(dict: post_paramsValue)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                self.goToReview()
                            }
                        }
                        else
                        {
                            self.goToReview()
                        }
                        
                    }
                    else
                    {
                        self.goToReview()
                    }
                }
                else
                {
                    self.goToReview()
                }
            }
            
        }
        
    }
    
    func uploadCRMImages(completion: @escaping(_ success: Bool) -> Void) {
        self.countCRMImage = 0
        
        let arrImageCRM = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isImageSyncforMobile == %@ && isInternalUsage == %@", "\(self.matchesGeneralInfo.value(forKey: "leadId") ?? "")" , self.strUserName, "false", "false"))
        
        let aryCRMImagesList = NSMutableArray()
        
        for item in arrImageCRM
        {
            let match = item as! NSManagedObject
            aryCRMImagesList.add("\(match.value(forKey: "path") ?? "")")

        }
        
        if aryCRMImagesList.count != 0 {
            
            let arrImageData = NSMutableArray()
            let arrImageName = NSMutableArray()

            for item in aryCRMImagesList {
                let strImage = "\(item)"
                arrImageData.add(getImage(imagePath: strImage))
                arrImageName.add(strImage)
            }
           
            for item in aryCRMImagesList
            {
                //self.dispatchGroupSales.enter()
                
                let strImage = "\(item)"
                ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Gallery.uploadCRMImagesSingle)") { (response, status) in
                    
                    //self.dispatchGroupSales.leave()
                    self.countCRMImage = self.countCRMImage + 1
                    print(response ?? "")
                    
                    if aryCRMImagesList.count == self.countCRMImage
                    {
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        var count = 0
                        for item in aryCRMImagesList
                        {
                            let imgPath = item as! String
        
                            arrOfKeys.add("path")
                            arrOfKeys.add("isImageSyncforMobile")

                            arrOfValues.add(imgPath)
                            arrOfValues.add("true")
                          
                            let isSuccess =  getDataFromDbToUpdate(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && path == %@", "\(self.matchesGeneralInfo.value(forKey: "leadId") ?? "")",imgPath), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                            
                            if isSuccess
                            {
                                // Image updated in DB.
                            }
                            
                            if count == aryCRMImagesList.count - 1 {
                                // loop completed go to fetch all images and redirect to form builder page
                                let isTrue = self.fetchAllImagesFromDBAfterSync(strHeaderTitle : "Crm")
                                completion(isTrue)
                            }
                            
                            count = count + 1
                        }
                    }else {
                        completion(true)
                    }
                }
            }
        }else {
            let isTrue = self.fetchAllImagesFromDBAfterSync(strHeaderTitle : "Crm")
            completion(isTrue)
        }

    }

    func getImage(imagePath : String)-> UIImage{
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: imagePath){
            return UIImage(contentsOfFile: imagePath)!
        }else{
            return UIImage()
        }
    }
     
}

// MARK: - Selection CustomTableView
// MARK: -

extension SalesConfigureViewController: CustomTableView {
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        print(dictData)
        
        if tag == 3333 {
            //Tax
            self.dictSelectedTaxCode = (dictData as! [String : Any])
            
            let indexPath = IndexPath(row: 0, section: (3 - (3 - self.secTitleCount)))
            let cell: ConfigurePricingTableViewCell = self.tblView.cellForRow(at: indexPath)! as! ConfigurePricingTableViewCell
            cell.txtTaxCode.text = dictSelectedTaxCode?["Name"] as? String ?? ""
            let strTaxCode = dictSelectedTaxCode?["SysName"] as? String ?? ""
            
            if cell.txtTaxCode.text != "" {
                self.getTaxCodeDetailWithApi(strTaxCode: strTaxCode)
            }else {
                self.strTaxCode = ""
                let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
                
                print(arrayLeadDetail)
                if(arrayLeadDetail.count > 0)
                {
                    let objLeadInfo : NSManagedObject? = (arrayLeadDetail[0] as! NSManagedObject)
                    objLeadInfo?.setValue("0", forKey: "tax")
                    objLeadInfo?.setValue("", forKey: "taxSysName")
                    
                    let context = getContext()
                    //save the object
                    do { try context.save()
                        print("Record Saved Updated.")
                    } catch let error as NSError {
                        debugPrint("Could not save. \(error), \(error.userInfo)")
                    }
                }
                self.initialSetup()
            }
        /*    //Getting Tax from API Call
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            let strNewTaxUrl = "/api/MobileToSaleAuto/GetTaxByTaxCode?companyKey="
            self.strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
            let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

            
            let strUrl = "\(strServiceUrlMain)\(strNewTaxUrl)\(strCompanyKey)&taxSysName=\(strTaxCode)"
            print(strUrl)
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            self.presenter.requestToGetTaxByTaxCode(apiUrl: strUrl)*/
        }else {
            //Credit 3334
            self.dictSelectedCredit = (dictData as! [String : Any])
            let indexPath = IndexPath(row: 0, section: (3 - (3 - self.secTitleCount)))
            let cell: ConfigurePricingTableViewCell = self.tblView.cellForRow(at: indexPath)! as! ConfigurePricingTableViewCell
            cell.txtCredit.text = dictSelectedCredit?["Name"] as? String ?? ""
        }
        
    }
    
    func getTaxCodeDetailWithApi(strTaxCode: String) {
        //Getting Tax from API Call
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strNewTaxUrl = "/api/MobileToSaleAuto/GetTaxByTaxCode?companyKey="
        self.strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

        
        let strUrl = "\(strServiceUrlMain)\(strNewTaxUrl)\(strCompanyKey)&taxSysName=\(strTaxCode)"
        print(strUrl)
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
        self.presenter.requestToGetTaxByTaxCode(apiUrl: strUrl)
    }
    
    func didSelect(aryData: NSMutableArray, tag: Int) {
        print(aryData, tag)
    }

    func fetchRenewalServiceFromCoreData(soldServiceId: String) -> String {
        print(soldServiceId)
        var strRenewalDesc = ""
        
        let arrAllObjRenewal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && serviceId = %@", strLeadId, soldServiceId))
        
        if arrAllObjRenewal.count > 0 {
            let matchesRenewal: NSManagedObject = arrAllObjRenewal.object(at: 0) as! NSManagedObject
            print(matchesRenewal)
            var strDesc = "\(matchesRenewal.value(forKey: "renewalDescription") ?? "")"
            if strDesc != "" {
                strDesc = strDesc + ":"
            }
            
            //let strAmount = "\(matchesRenewal.value(forKey: "renewalAmount") ?? "")"
            
            let strAmount =  "\(convertDoubleToCurrency(amount: Double("\(matchesRenewal.value(forKey: "renewalAmount") ?? "")") ?? 0.0))"
            
            let dict = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesRenewal.value(forKey: "renewalFrequencySysName") ?? "")")
            let strFrequency = dict.value(forKey: "FrequencyName") ?? ""
            strRenewalDesc = "\(strDesc) \(strAmount) (\(strFrequency))"
        }
        
        return strRenewalDesc
    }
}
