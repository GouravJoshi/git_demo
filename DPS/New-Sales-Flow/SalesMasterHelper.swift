//
//  SalesMasterHelper.swift
//  DPS
//
//  Created by Saavan Patidar on 14/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit


class SalesMasterHelper: UIViewController {

    
    //MARK: ---------------- Variable Declaration --------------------
    
    
    // MARK: - --------- Global Variables ---------
    
    @objc var strLeadId = NSString ()
    @objc var strBranchSysName = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var strEmpName = String()
    var dictLoginData = NSDictionary()
    
    // MARK: - ---------Dictionary ---------
    var dictSalesMaster = NSDictionary()

    // MARK: - ---------Array ---------
    
    var arrCategory = NSArray()
    var arrFrequency = NSArray()
    var arrBillingFrequency = NSArray()

    
    // MARK: - ---------String ---------

    
    override func viewDidLoad() {
        super.viewDidLoad()

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        setInitial()
    }
    func setInitial()  {
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil)
        {
            dictSalesMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        }
    
        arrCategory = global.getCategoryDeptWiseGlobal()! as NSArray
        let arrFreq = getFrequency()
        arrFrequency = arrFreq.arrServiceFreq
        arrBillingFrequency = arrFreq.arrBillingFreq

    }

    //MARK: ---------------- Get Category, Service and Frequency Object Methods --------------------
    
    
    func getFrequency() -> (arrServiceFreq: NSArray, arrBillingFreq: NSArray)
    {
        var arrTempServiceFreq = NSArray()
        var arrTempBillingFreq = NSArray()

        
            if(nsud.value(forKey: "MasterSalesAutomation") != nil){
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "Frequencies") is NSArray){
                    arrTempServiceFreq = dictMaster.value(forKey: "Frequencies") as! NSArray
                
                    //Frequency as per type
                    
                    let arrTemp = NSMutableArray()
                    let arrTemp2 = NSMutableArray()

                    if (arrTempServiceFreq.count  > 0)
                    {
                        for item in arrTempServiceFreq
                        {
                            let dict = item as! NSDictionary
                            let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                            if strType == "Service" || strType == "Both" || strType == ""
                            {
                                arrTemp.add(dict)
                            }
                            if strType == "Billing" || strType == "Both" || strType == ""
                            {
                                arrTemp2.add(dict)
                            }
                        }
                    }
                    arrTempServiceFreq = arrTemp as NSArray
                    arrTempBillingFreq = arrTemp2 as NSArray

                }
            }
        return (arrTempServiceFreq,arrTempBillingFreq)
    }
    
    func getCategoryObjectFromSysName(strSysName: String) -> NSDictionary
    {
        var dictObject = NSDictionary()

        for itemCategory in arrCategory
        {
            let dictCategory = itemCategory as! NSDictionary
            
            if strSysName == "\(dictCategory.value(forKey: "SysName") ?? "")"
            {
                dictObject = dictCategory
                break
            }
        }
        
        return dictObject
    }
    
    func getFrequencyObjectFromIdAndSysName(strId : String, strSysName: String) -> NSDictionary {
       
        var dictObject = NSDictionary()
        
        var strVal = ""
        
        if !strId.isEmpty
        {
            strVal = strId
        }
        if !strSysName.isEmpty
        {
            strVal = strSysName
        }
        
        for item in arrFrequency
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "FrequencyId") ?? "")" == strVal
            {
                dictObject = dict
                break
            }
        }
        return dictObject
    }
    
    func getServiceObjectFromIdAndSysName(strId : String, strSysName: String) -> NSDictionary {
       
        var dictObject = NSDictionary()
        
        var strVal = ""
        
        if !strId.isEmpty
        {
            strVal = strId
        }
        if !strSysName.isEmpty
        {
            strVal = strSysName
        }
        
        for item in arrCategory
        {
            let dict = item as! NSDictionary
            
            if dict.value(forKey: "Services") is NSArray
            {
                let arrService = dict.value(forKey: "Services") as! NSArray
                
                for itemService in arrService
                {
                    let dictService = itemService as! NSDictionary
                    
                    if strVal == "\(dictService.value(forKey: "ServiceMasterId") ?? "")"
                    {
                        dictObject = dictService
                        
                        break
                    }
                    
                }
            }
        }
        
        return dictObject
    }

}
