//
//  NewSales_SelectService.swift
//  DPS
//
//  Created by Saavan Patidar on 17/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol BackRefreshFormBuilderProtocol : class
{
    func refreshView(data : Any)
}

class CollectionTopViewCell: UICollectionViewCell
{
    @IBOutlet var btn: UIButton!
}

class NewSales_SelectService: UIViewController {
    
    
    weak var refreshOnBack: BackRefreshFormBuilderProtocol?
    
    //MARK: ---------------- Variable --------------------

    var isUnitBasedService = false
    var isPrameterBasedService = false
    var isEditInSalesAuto = false
    var isCouponApplied = false
    
    //MARK: ---------------- Outlet --------------------
    
    @IBOutlet var lblHeaderTitle: UILabel!
    @IBOutlet var lblHeaderSubTitle: UILabel!
    @IBOutlet var txtFldCoupon: ACFloatingTextField!
    
    @IBOutlet var btnApplyCoupon: ButtonWithShadow!
    @IBOutlet var scrollTopButton: UIScrollView!
    @IBOutlet var const_LblLine_L: NSLayoutConstraint!
    @IBOutlet var const_BtnStandard_L: NSLayoutConstraint!
    @IBOutlet var const_BtnCustom_L: NSLayoutConstraint!
    @IBOutlet var btnStandard: UIButton!
    @IBOutlet var btnCustom: UIButton!
    @IBOutlet var btnAddService: UIButton!
    
    @IBOutlet var tblData: UITableView!
        
    // MARK: - --------- Global Variables ---------
    
    @objc var strLeadId = NSString ()
    @objc var strBranchSysName = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var strAccountNo = ""
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var strEmpName = String()
    var dictLoginData = NSDictionary()
    var isClickOnStandard = true
    var chkFreqConfiguration = false
    var strGlobalStageSysName = ""
    var strGlobalStatusSysName = ""

    // MARK: - --------- Array ---------
    
    var arrStandardServices = NSArray()
    var arrCustomService = NSArray()
    var arrBundelServices = NSArray()
    var arrCouponDetail = NSArray()
    var arrCouponMaster = NSArray()
    var arrRenewalServices = NSArray()

    var objLeadDetail = NSManagedObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        basicFunction()
        setInitial()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTab(notification:)), name: Notification.Name("updateTab"), object: nil)
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        
           super.viewWillLayoutSubviews()
           if(ScreenSize.SCREEN_WIDTH < 435){
               scrollTopButton.contentSize = CGSize(width: 460, height: self.scrollTopButton.frame.size.height)
           }
        
        
       }

    override func viewWillAppear(_ animated: Bool) {
        
        basicFunction()
        refreshData()
    }
    
    @objc func updateTab(notification: Notification) {
        print(notification.object)
        if notification.object as! String == "Custom" {
            self.actionOnCustom(self.btnCustom)
        }else {
            self.actionOnStandard(self.btnStandard)
        }
    }
    //MARK: ---------------- Other Functions --------------------
    
    func refreshData()  {
        
        arrStandardServices = fetchStandardServiceFromCoreData()
        arrCustomService = fetchCustomServiceFromCoreData()
        arrRenewalServices = fetchRenewalServiceFromCoreData()
        arrCouponDetail = fetchCouponDetailFromCoreData()
        tblData.reloadData()
    }
    
    
    func basicFunction()  {
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strLeadId = "\(nsud.value(forKey: "LeadId") ?? "")" as NSString
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

        objLeadDetail = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
        strBranchSysName = "\(objLeadDetail.value(forKey: "branchSysName") ?? "")" as NSString
        strAccountNo = "\(objLeadDetail.value(forKey: "accountNo") ?? "")"
        strGlobalStageSysName = "\(objLeadDetail.value(forKey: "stageSysName") ?? "")"
        strGlobalStatusSysName = "\(objLeadDetail.value(forKey: "statusSysName") ?? "")"

        tblData.showsVerticalScrollIndicator = false
        
        if getOpportunityStatus()
        {
            btnAddService.isEnabled = false
            txtFldCoupon.isEnabled = false
            btnApplyCoupon.isEnabled = false
        }
    }
    
    func getOpportunityStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func setInitial() {
        
        lblHeaderTitle.text = "\(nsud.value(forKey: "titleHeader1") ?? "")"
        lblHeaderSubTitle.text = "\(nsud.value(forKey: "titleHeader2") ?? "")"

        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
        
       
        tblData.estimatedRowHeight = 300
        tblData.rowHeight = UITableView.automaticDimension
        tblData.tableFooterView = UIView(frame: .zero)

       
    }
    func goToAddService(type: NewServiceType) {
                    
        let vc = storyboardNewSalesSelectService.instantiateViewController(withIdentifier: "NewSales_AddServiceVC") as! NewSales_AddServiceVC
        //vc.modalPresentationStyle  =  .fullScreen
        vc.serviceType = type
        self.navigationController?.pushViewController(vc, animated: false)
        //present(vc, animated: false, completion: nil)
    }
    
    func getBundleCount() -> NSArray {
        var arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        
        let arr = arryOfData.filter { (matches) -> Bool in
            return "\((matches as! NSManagedObject).value(forKey: "bundleId") ?? "")" != "0"
        } as NSArray
        
        arryOfData = arr
        return arryOfData
    }
    
    func getBundleRowCount(strBundleId : String) -> NSArray {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && bundleId == %@", strUserName, strLeadId, strBundleId))
    
        return arryOfData
    }
    
    
    func goForEditService(matchesServiceEdit : NSManagedObject, type : NewServiceType)
    {
        let vc = storyboardNewSalesSelectService.instantiateViewController(withIdentifier: "NewSales_AddServiceVC") as! NewSales_AddServiceVC
        vc.serviceType = type
        vc.matchesServiceEdit = matchesServiceEdit
        vc.isEditService = true
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    func goToConfigure() {
        
        if !getOpportunityStatus()
        {
            finalUpdatePricingAndBillingAmount()
            finalUpdatePricingAndBillingAmountCustom()
        }
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "FormBuilder" : "FormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesConfigureViewController") as? SalesConfigureViewController
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    //MARK: ---------------- Local Function --------------------

    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
    func goToNoramlHTMLEditor(strText : String, matchesObject : NSManagedObject) {
        
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        vc.delegate = self
        vc.strfrom = "ForRenewal"
        vc.matchesObject = matchesObject
        vc.strHtml = strText
        self.present(vc, animated: false, completion: nil)

    }
    
    func goToNoramlTextEditor(strText : String, matchesObject: NSManagedObject)
    {
        
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "SalesClarkPestiPad" : "SalesClarkPestiPhone", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TextEditorNormal") as! TextEditorNormal
        vc.delegate = self
        vc.strfrom = "SalesFollowUp"
        vc.strHtml = strText
        vc.matchesObject = matchesObject
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)

    }
    
    
    func validationForCoupon() -> (check: Bool, message: String) {
        
        var check = true
        var message = ""
        
        let strAppliedCoupon = txtFldCoupon.text ?? ""
        
        arrCouponMaster = getCouponDiscountMaster() as NSArray
        
        let dictCoupon = getCouponObject(strDisocuntName: strAppliedCoupon) as NSDictionary
        
        let strDiscountUsage = "\(dictCoupon.value(forKey: "Usage") ?? "")"
        
        if fetchAllServiceFromCoreData().count == 0
        {
            message =  "\(alertServcie)"
            check = false
        }
        else if txtFldCoupon.text == "" || txtFldCoupon.text?.count == 0
        {
            message =  "\(alertCouopn)"
            check = false
        }
        else if dictCoupon.count == 0
        {
            message =  "\(alertInvalidCoupon)"
            check = false
        }
        else if checkForAppliedCoupon(strDisocuntCode: strAppliedCoupon, strDisocuntUsage: strDiscountUsage)
        {
            message =  "\(alertCouopnApplied)"
            check = false
        }
        else if !discountValidityCheck(dictDiscountObject: dictCoupon, strDisocuntCode: strAppliedCoupon)
        {
            message =  "\(alertInvalidCoupon)"
            check = false
        }
        else
        {
            check = true
            applyCoupon(dictDiscountObject: dictCoupon, strDisocuntCode: strAppliedCoupon)
            print("Valid Coupon")
        }
        
        return (check,message)
    }
    func validationForSave() -> (check: Bool, message: String) {
        
        var check = true
        var message = ""
        
        if fetchAllServiceFromCoreData().count == 0  && arrBundelServices.count == 0 && arrCustomService.count == 0
        {
            message =  "\(alertServcie)"
            check = false
        }
        else
        {
            check = true
        }
        
        return (check,message)
    }
    
    func applyCoupon(dictDiscountObject: NSDictionary, strDisocuntCode: String)  {
        
        // Check for  coupon is service based or not
        
        var isServiceBaseCoupon = false
        var isPercentageBasedCoupon = false
        var predicateServiceBase = NSPredicate()
        var predicateNormal = NSPredicate()

        
        if "\(dictDiscountObject.value(forKey: "IsServiceBased") ?? "")" == "1" || "\(dictDiscountObject.value(forKey: "IsServiceBased") ?? "")".caseInsensitiveCompare("true") == .orderedSame
        {
            isServiceBaseCoupon = true
            predicateServiceBase = NSPredicate(format: "userName == %@ && leadId == %@ && serviceSysName == %@", strUserName, strLeadId, "\(dictDiscountObject.value(forKey: "ServiceSysName") ?? "")")
        }
        else
        {
            isServiceBaseCoupon = false
            predicateNormal = NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId)
        }
        
        if "\(dictDiscountObject.value(forKey: "IsDiscountPercent") ?? "")" == "1" || "\(dictDiscountObject.value(forKey: "IsDiscountPercent") ?? "")".caseInsensitiveCompare("true") == .orderedSame
        {
            isPercentageBasedCoupon = true
        }
        else
        {
            isPercentageBasedCoupon = false
        }
        
        var arryOfServiceData = NSArray()
        
        if isServiceBaseCoupon
        {
            arryOfServiceData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: predicateServiceBase)
            
//            if arryOfServiceData.count == 0
//            {
//                predicateNormal = NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId)
//
//                arryOfServiceData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: predicateNormal)
//            }
        }
        else
        {
            predicateNormal = NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId)

            arryOfServiceData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: predicateNormal)
        }
    
        guard arryOfServiceData.count > 0 else {
            print("Enter in gurad with count \(arryOfServiceData.count)")
            self.view.endEditing(true)
            
            if isServiceBaseCoupon
            {
                return showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Coupon applicable service is not available", viewcontrol: self)
            }
            else
            {
                return
            }
            //return //showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Invalid Coupon", viewcontrol: self)
        }
        
        // Calculate Coupon Amount
        
        var appliedAmountCoupon = Float()
        
        appliedAmountCoupon = 0.0
        
        //let totalPrice = getTotalServicePriceForDiscount(matchesObject: arryOfServiceData.object(at: 0) as! NSManagedObject)
        
        let totalPrice = getTotalServicePriceAndDiscount(matchesObject: arryOfServiceData.object(at: 0) as! NSManagedObject).totalInitialPrice - getTotalServicePriceAndDiscount(matchesObject: arryOfServiceData.object(at: 0) as! NSManagedObject).discount


        if isPercentageBasedCoupon
        {
            let per = ("\(dictDiscountObject.value(forKey: "DiscountPercent") ?? "")" as NSString).floatValue
            appliedAmountCoupon = (totalPrice * per) / 100
        }
        else
        {
            appliedAmountCoupon = ("\(dictDiscountObject.value(forKey: "DiscountAmount") ?? "")" as NSString).floatValue
        }
        
        if appliedAmountCoupon > totalPrice
        {
            appliedAmountCoupon = totalPrice
        }
        
        
        saveDiscountCouponInCoreData(discountObject: dictDiscountObject, strAppliedAmount: stringToFloat(strValue: "\(appliedAmountCoupon)"), matchesObject: arryOfServiceData.object(at: 0) as! NSManagedObject)
        
        updateServiceDetailForDiscount(discountObject: dictDiscountObject, strAppliedAmount: stringToFloat(strValue: "\(appliedAmountCoupon)"), matchesObject: arryOfServiceData.object(at: 0) as! NSManagedObject, isAddCoupon: true)
        
    }
    
    func saveDiscountCouponInCoreData(discountObject : NSDictionary, strAppliedAmount: String , matchesObject : NSManagedObject)  {
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()

        arrOfKeys = ["accountNo",
                     "applicableForInitial",
                     "applicableForMaintenance",
                     "appliedInitialDiscount",
                     "appliedMaintDiscount",
                     "companyKey",
                     "discountAmount",
                     "discountCode",
                     "discountDescription",
                     "discountPercent",
                     "discountSysName",
                     "discountType",
                     "isActive",
                     "isApplied",
                     "isDiscountPercent",
                     "leadAppliedDiscountId",
                     "leadId",
                     "name",
                     "serviceSysName",
                     "serviceType",
                     "soldServiceId",
                     "userName"]
        
        arrOfValues = [strAccountNo,
                       "\(discountObject.value(forKey: "ApplicableForInitial") ?? "")" == "1" ? "true" : "false",
                       "\(discountObject.value(forKey: "ApplicableForMaintenance") ?? "")" == "1" ? "true" : "false",
                       strAppliedAmount,
                       stringToFloat(strValue: ""),
                       strCompanyKey,
                       stringToFloat(strValue: "\(discountObject.value(forKey: "DiscountAmount") ?? "")"),
                       "\(discountObject.value(forKey: "DiscountCode") ?? "")",
                       "\(discountObject.value(forKey: "Description") ?? "")",
                       stringToFloat(strValue: "\(discountObject.value(forKey: "DiscountPercent") ?? "")"),
                       "\(discountObject.value(forKey: "SysName") ?? "")",
                       "\(discountObject.value(forKey: "Type") ?? "")",
                       "\(discountObject.value(forKey: "isActive") ?? "")" == "1" ? "true" : "false",
                       "true",
                       "\(discountObject.value(forKey: "IsDiscountPercent") ?? "")" == "1" ? "true" : "false",
                       "\(discountObject.value(forKey: "DiscountSetupId") ?? "")",
                       strLeadId,
                       "\(discountObject.value(forKey: "Name") ?? "")",
                       "\(matchesObject.value(forKey: "serviceSysName") ?? "")",
                       "Standard",
                       "\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")",
                       strUserName]
                       
        saveDataInDB(strEntity: "LeadAppliedDiscounts", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func updateServiceDetailForDiscount(discountObject : NSDictionary, strAppliedAmount: String , matchesObject : NSManagedObject, isAddCoupon: Bool) {
        
        var discount = 0.0
        var discountPer = 0.0
        
        if isAddCoupon
        {
            discount = Double(("\(strAppliedAmount)" as NSString).floatValue) + Double(("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).floatValue)
        }
        else //On Delete Coupon
        {
            discount = Double(("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).floatValue) - (strAppliedAmount as NSString).doubleValue
        }
        
        if discount < 0
        {
            discount = 0
        }
        
        discountPer = (discount * 100) / Double(getTotalServicePriceAndDiscount(matchesObject: matchesObject).totalInitialPrice)

        if discountPer < 0
        {
            discountPer = 0
        }
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = [
            "discount",
            "discountPercentage"
        ]
        
        
        arrOfValues = [
            stringToFloat(strValue: "\(discount)"),
            stringToFloat(strValue: "\(discountPer)")
        ]
        
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,"\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        
        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
        
    }
    
    func getTotalServicePriceAndDiscount(matchesObject: NSManagedObject) -> (totalInitialPrice : Float, discount : Float)  {
        
        var isUnitBased = false
        var isParameterBased = false
        
        let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "serviceSysName") ?? "")")

        if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
        {
            isUnitBased = true
        }
        else
        {
            isUnitBased = false
        }
        
        if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
        {
            isParameterBased = true
        }
        else
        {
            isParameterBased = false
        }
        
        var unit = 1.0
        
        if isUnitBased
        {
            unit = Double(("\(matchesObject.value(forKey: "unit") ?? "")" as NSString).floatValue)
        }
        
        //For Parameter based service
        
        var sumParaInitialPrice = 0.0, sumParaMaintPrice = 0.0

        if isParameterBased
        {
            let arrPara = matchesObject.value(forKey: "additionalParameterPriceDcs") as! NSArray
            
            if arrPara.count > 0
            {
                for item in arrPara
                {
                    let dict = item as! NSDictionary
                    let priceInitial = "\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")"
                    let priceMaint = "\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")"

                    sumParaInitialPrice = sumParaInitialPrice + (priceInitial as NSString).doubleValue
                    sumParaMaintPrice = sumParaMaintPrice + (priceMaint as NSString).doubleValue

                }
            }
        }
        
        let finalInitialPrice = (("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).floatValue * Float(unit) ) + Float(sumParaInitialPrice)
        
        let discount = ("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).floatValue
        
        return (finalInitialPrice,discount)
        
    }

    func checkForAppliedCoupon(strDisocuntCode : String, strDisocuntUsage : String) -> Bool {
                
        var predicate = NSPredicate()
        
        if strDisocuntUsage.caseInsensitiveCompare("OneTime") == .orderedSame
        {
            predicate = NSPredicate(format: "accountNo = %@ && discountCode == %@", strAccountNo,strDisocuntCode)
        }
        else
        {
            predicate = NSPredicate(format: "userName == %@ && leadId == %@ && discountCode == %@", strUserName, strLeadId,strDisocuntCode)
        }
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: predicate)
        
        if arryOfData.count == 0
        {
            return false
        }
        else
        {
            return true
        }
       
    }
    func discountValidityCheck(dictDiscountObject: NSDictionary, strDisocuntCode: String) -> Bool {
        
        
        var strValidFrom =  changeStringDateToGivenFormat(strDate: "\(dictDiscountObject.value(forKey: "ValidFrom") ?? "")", strRequiredFormat: "MM/dd/yyyy")
        
        if strValidFrom.count == 0 || strValidFrom == ""
        {
            strValidFrom = Global().getCurrentDate()
        }
        
        var strValidTo = changeStringDateToGivenFormat(strDate: "\(dictDiscountObject.value(forKey: "ValidTo") ?? "")", strRequiredFormat: "MM/dd/yyyy")
        
        if strValidTo.count == 0 || strValidTo == ""
        {
            strValidTo = Global().getCurrentDate()
        }
        
        let strCurrentDate = changeStringDateToGivenFormat(strDate: Global().getCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        
        //01 July -  05 July   02 July
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let currentDate = dateFormatter.date(from:strCurrentDate)!
        let validFrom = dateFormatter.date(from:strValidFrom)!
        let validTo = dateFormatter.date(from:strValidTo)!

        if currentDate >= validFrom && currentDate <= validTo
        {
            return true
        }
        else
        {
            return false
        }
       
    }
    func discountValidityCheckOld(dictDiscountObject: NSDictionary, strDisocuntCode: String) -> Bool {
        
        
        var strValidFrom =  changeStringDateToGivenFormat(strDate: "\(dictDiscountObject.value(forKey: "ValidFrom") ?? "")", strRequiredFormat: "MM/dd/yyyy")
        
        if strValidFrom.count == 0 || strValidFrom == ""
        {
            strValidFrom = Global().getCurrentDate()
        }
        
        var strValidTo = changeStringDateToGivenFormat(strDate: "\(dictDiscountObject.value(forKey: "ValidTo") ?? "")", strRequiredFormat: "MM/dd/yyyy")
        
        if strValidTo.count == 0 || strValidTo == ""
        {
            strValidTo = Global().getCurrentDate()
        }
        
        let strCurrentDate = changeStringDateToGivenFormat(strDate: Global().getCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        
        //01 July -  05 July   02 July
        
        if strCurrentDate >= strValidFrom && strCurrentDate <= strValidTo
        {
            return true
        }
        else
        {
            return false
        }
       
    }
    
    func GotoGlobleImageGraphView(strType : String)  {
        let strLeadID =  objLeadDetail.value(forKey: "leadId") ?? ""
        var strStatusss =  objLeadDetail.value(forKey: "statusSysName") ?? ""
        var strStageSysName =  objLeadDetail.value(forKey: "stageSysName") ?? ""
        if(nsud.value(forKey: "backFromInspectionNew") != nil){
            
            if(nsud.value(forKey: "backFromInspectionNew") as! Bool == true){
                strStatusss =  objLeadDetail.value(forKey: "statusSysName") ?? ""
                strStageSysName =  objLeadDetail.value(forKey: "stageSysName") ?? ""
            }
        }
    
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPhone" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        vc.strHeaderTitle = strType as NSString
        vc.strWoId = "\(strLeadID)" as NSString
        vc.strWoLeadId = "\(strLeadID)" as NSString
        vc.strModuleType = "SalesFlow"
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won"){
            vc.strWoStatus = "Complete"
        }else{
            vc.strWoStatus = "InComplete"
        }
        vc.strWoStatus = "InComplete"
        self.navigationController?.present(vc, animated: true, completion: nil)

    }
    //MARK: ---------------- Core Data Function --------------------

    func fetchCouponAppliedOnService(soldServiceId : String)  {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && soldServiceId == %@", strUserName, strLeadId, soldServiceId))
        
        if arryOfData.count > 0
        {
            isCouponApplied = true
        }
        else
        {
            isCouponApplied = false
        }
        
    }
    
    
    func fetchStandardServiceFromCoreData() -> NSArray {
        
        var arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        
        let arr = arryOfData.filter { (matches) -> Bool in
            return "\((matches as! NSManagedObject).value(forKey: "bundleId") ?? "")" == "0" || "\((matches as! NSManagedObject).value(forKey: "bundleId") ?? "")" == ""
        } as NSArray
        
        arryOfData = arr
        
        return arryOfData
    }
    
    func fetchCustomServiceFromCoreData() -> NSArray {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        
        return arryOfData
    }
    
    func fetchAllServiceFromCoreData() -> NSArray {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        
        return arryOfData
    }
    
    func fetchRenewalServiceFromCoreData() -> NSArray {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        return arryOfData
    }
    func fetchCouponDetailFromCoreData() -> NSArray {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId == %@ && discountType=%@", strLeadId,"Coupon"))
        
        return arryOfData
    }
    
    func deleteStandardService(matches : NSManagedObject)  {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
            
            self.isEditInSalesAuto = true
            
            let strSoldServiceId = "\(matches.value(forKey: "soldServiceStandardId") ?? "")"
            deleteDataFromDB(obj: matches)
            self.arrStandardServices =  self.fetchStandardServiceFromCoreData()
            
            //For Corrosponding Renwal
            
            let arrayDataRenwal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ AND soldServiceStandardId = %@", self.strLeadId, strSoldServiceId))
            
            if arrayDataRenwal.count > 0 {
                
                for item in arrayDataRenwal
                {
                    let objTempSales = item as! NSManagedObject
                    deleteDataFromDB(obj: objTempSales)
                    self.arrRenewalServices = self.fetchRenewalServiceFromCoreData()
                }
                
            }
            
            //For Corrosponding Coupon
            
            let arrayDataCoupon = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId == %@ AND soldServiceId = %@", self.strLeadId, strSoldServiceId))
            
            if arrayDataCoupon.count > 0 {
                
                for item in arrayDataCoupon
                {
                    let objTempSales = item as! NSManagedObject
                    deleteDataFromDB(obj: objTempSales)
                    self.arrCouponDetail = self.fetchCouponDetailFromCoreData()
                }
             
            }
            
            self.tblData.reloadData()
            self.view.endEditing(true)
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
        }))

        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
    }
    
    func editStandardService(matches : NSManagedObject)  {
        
        self.isEditInSalesAuto = true
        self.goForEditService(matchesServiceEdit: matches, type: NewServiceType.Standard)
    }
    
    func deleteCustomService(matches : NSManagedObject) {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
            
            self.isEditInSalesAuto = true
            deleteDataFromDB(obj: matches)
            self.arrCustomService = self.fetchCustomServiceFromCoreData()
            self.tblData.reloadData()
            self.view.endEditing(true)
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
        }))

        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
    }
    func editCustomService(matches : NSManagedObject) {
        
        self.isEditInSalesAuto = true
        self.goForEditService(matchesServiceEdit: matches, type: NewServiceType.Custom)
    }
    
    func deleteRenewalService(matches : NSManagedObject) {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
            
            self.isEditInSalesAuto = true
            deleteDataFromDB(obj: matches)
            self.arrRenewalServices = self.fetchRenewalServiceFromCoreData()
            self.tblData.reloadData()
            self.view.endEditing(true)
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
        }))

        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        

    }
    
    func deleteCouponDetail(matches : NSManagedObject)  {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
            
            self.isEditInSalesAuto = true
                        
            let strSoldServiceId = "\(matches.value(forKey: "soldServiceId") ?? "")"
            let strDiscountAmount = "\(matches.value(forKey: "appliedInitialDiscount") ?? "")"

            deleteDataFromDB(obj: matches)
            self.arrCouponDetail = self.fetchCouponDetailFromCoreData()
            self.tblData.reloadData()
            
            //Get Service Object
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && soldServiceStandardId = %@", self.strUserName, self.strLeadId,strSoldServiceId))
            if arryOfData.count > 0
            {
                self.updateServiceDetailForDiscount(discountObject: NSDictionary(), strAppliedAmount: strDiscountAmount, matchesObject: arryOfData.object(at: 0) as! NSManagedObject, isAddCoupon: false)
            }
        
            self.view.endEditing(true)
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
        }))

        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
    }
    func getTotalCouponDiscountOnService(strSoldServiceId : String) -> Double {
        
        let arrayDataCoupon = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId == %@ AND soldServiceId = %@", self.strLeadId, strSoldServiceId))

        var totalDiscount = 0.0
        
        for item in arrayDataCoupon
        {
            let matches = item as! NSManagedObject
            totalDiscount = totalDiscount + Double(("\(matches.value(forKey: "appliedDiscount") ?? "")" as NSString).floatValue)
        }
        
        return totalDiscount
    }
    
    func goToEmailHistory()  {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = objLeadDetail
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
              let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
              testController?.modalPresentationStyle = .fullScreen
              testController?.strAccountNo = "\(objLeadDetail.value(forKey: "accountNo") ?? "")"
              testController?.strCompanyKey = strCompanyKey
              testController?.strUserName = strUserName

              self.present(testController!, animated: false, completion: nil)
    }
    
    func goToServiceHistory()  {
        
        /*let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = "\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
        
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow = "\(objLeadDetail.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
        
        
    }
    func goToServiceNotesHistory()  {
        
        //ServiceNotesHistoryViewController
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController?.strTypeOfService = "sales";
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(objLeadDetail.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(objLeadDetail.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAppointment()
    {
           
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            

            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    
    
    //MARK: ---------------- Actions --------------------
    
    @IBAction func actionOnBack(_ sender: Any) {
        
        if getOpportunityStatus()
        {
            refreshOnBack?.refreshView(data: "")
        }
        
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionOnStandard(_ sender: Any) {
        
        txtFldCoupon.isHidden = false
        btnApplyCoupon.isHidden = false
        
        
        isClickOnStandard = true
        const_LblLine_L.constant = 0
        btnStandard.setTitleColor(UIColor.appThemeColor, for: .normal)

        btnCustom.setTitleColor(UIColor.black, for: .normal)
        
        tblData.reloadData()
    }
    
    @IBAction func actionOnCustom(_ sender: Any) {
   
        txtFldCoupon.isHidden = true
        btnApplyCoupon.isHidden = true
        
        isClickOnStandard = false
        const_LblLine_L.constant = btnCustom.frame.origin.x
        
        btnCustom.setTitleColor(UIColor.appThemeColor, for: .normal)

        btnStandard.setTitleColor(UIColor.black, for: .normal)
        tblData.reloadData()
    }
    
    @IBAction func actionOnAddService(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Standard Services", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAddService(type: NewServiceType.Standard)
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Bundled Services", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAddService(type: NewServiceType.Bundle)

        }))
        
        alert.addAction(UIAlertAction(title: "Custom Services", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAddService(type: NewServiceType.Custom)

        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        if DeviceType.IS_IPAD
        {
            alert.popoverPresentationController?.sourceView = self.btnAddService
        }
        //alert.popoverPresentationController?.sourceView = self.btnAddImage
        
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    @IBAction func actionOnApplyCoupon(_ sender: Any) {
        
        let isProceed = validationForCoupon()
        
        if !isProceed.check
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: isProceed.message, viewcontrol: self)
        }
        else
        {
            refreshData()
            isEditInSalesAuto = true
            self.view.endEditing(true)
            txtFldCoupon.text = ""
            print("valid coupon")
        }
    }
    
    @IBAction func actionOnCancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func actionOnSetting(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
            self.view.endEditing(true)
            
            self.goToEmailHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
        
            self.goToSetupHistory()
            
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
        
            self.goToServiceNotesHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
        
            self.goToServiceHistory()
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
           // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.up]
        }
        self.present(alert, animated: true)
       
      }
    
    @IBAction func actionOnImage(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Make Your Selection", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.AddImages, style: .default, handler: { action in
            self.GotoGlobleImageGraphView(strType: "Before")
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.AddDocuments, style: .default, handler: { [self] action in
            let strLeadID =  strLeadId

            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: DeviceType.IS_IPAD ? "UploadDocumentSalesiPad" : "UploadDocumentSales") as! UploadDocumentSales
            vc.strWoId = "\(strLeadID)" as NSString
            self.navigationController?.present(vc, animated: true, completion: nil)

        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
      //      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.down]
        }
        self.present(alert, animated: true)
    }
    
    @IBAction func actionOnGraph(_ sender: Any) {
        
        self.view.endEditing(true)
        nsud.setValue(true, forKey: "firstGraphImage")
        nsud.setValue(false, forKey: "servicegraph")
        GotoGlobleImageGraphView(strType: "Graph")
    }
    
    @IBAction func actionOnChemicalSensitivityList(_ sender: Any) {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = objLeadDetail
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func action_Home(_ sender: Any) {
        self.view.endEditing(true)
        self.goToAppointment()
    }
    
    
    @IBAction func actionOnSaveContinue(_ sender: Any) {
        
        if getOpportunityStatus()
        {
            goToConfigure()
        }
        else
        {
            let isProceed = validationForSave()
            
            if !isProceed.check
            {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: isProceed.message, viewcontrol: self)
            }
            else
            {
                goToConfigure()
                
                if isEditInSalesAuto
                {
                    Global().updateSalesModifydate("\(objLeadDetail.value(forKey: "leadId") ?? "")" as String)
                }
            }
        }
    }
}

// MARK: ----------------------- Table Extension -------------------------


extension NewSales_SelectService: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == tblData
        {
            return 5
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
        viewheader.backgroundColor = UIColor.white//init(red: 246/255, green: 246/255, blue: 246/255, alpha: 1)
        
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: 300, height: (DeviceType.IS_IPAD ? 50 : 40)))
        
        if tableView == tblData
        {
            if(section == 0)
            {
                lbl.text = "Applied Coupon"
                lbl.textColor = UIColor.theme()
            }
            else if(section == 1)
            {
                lbl.text = "Maintenance Service"
                lbl.textColor = UIColor.theme()
            }
            else if(section == 2)
            {
                lbl.text = "Custom Service"
                lbl.textColor = UIColor.theme()
            }
            else if(section == 3)
            {
                lbl.text = "Bundled Service"
                lbl.textColor = UIColor.theme()
            }
            else if(section == 4)
            {
                lbl.text = "Renewal Service"
                lbl.textColor = UIColor.theme()
            }
            lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
            viewheader.addSubview(lbl)
            return viewheader
        }
        else
        {
            return viewheader
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
        if tableView == tblData
        {
            if section == 0  //Coupon Service
            {
                if arrCouponDetail.count > 0
                {
                    return (DeviceType.IS_IPAD ? 60 : 50)
                }
                else
                {
                    return 0
                }
            }
            else if section == 1  //Standard Service
            {
                if isClickOnStandard
                {
                    if arrStandardServices.count > 0
                    {
                        return (DeviceType.IS_IPAD ? 60 : 50)
                    }
                    else
                    {
                        return 0
                    }
                }
                else
                {
                    return 0
                }
               
            }
            else if section == 2  //Non Standard
            {
                if !isClickOnStandard
                {
                    if arrCustomService.count > 0
                    {
                        return (DeviceType.IS_IPAD ? 60 : 50)
                    }
                    else
                    {
                        return 0
                    }
                }
                else
                {
                    return 0
                }
                
            }
            else if section == 3  //Bundled Service
            {
                if isClickOnStandard {
                    
                    if getBundleCount().count == 0
                    {
                        return 0

                    }
                    else
                    {
                        return (DeviceType.IS_IPAD ? 60 : 50)
                    }
                }
                else
                {
                    return 0
                }
            }
            else if section == 4  //Renewal Service
            {
                if isClickOnStandard
                {
                    if arrRenewalServices.count > 0
                    {
                        return (DeviceType.IS_IPAD ? 60 : 50)
                    }
                    else
                    {
                        return 0
                    }
                }
                else
                {
                    return 0
                }
                
            }
            else
            {
                return (DeviceType.IS_IPAD ? 60 : 50)
            }
        }
        else
        {
            return (DeviceType.IS_IPAD ? 60 : 50)
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblData
        {
            if section == 0  //Coupon Service
            {
                return arrCouponDetail.count
            }
            else if section == 1  //Standard Service
            {
                if isClickOnStandard
                {
                    return arrStandardServices.count
                }
                else
                {
                    return 0

                }
            }
            else if section == 2  //Non Standard
            {
                if !isClickOnStandard
                {
                    return arrCustomService.count

                }
                else
                {
                    return 0

                }
            }
            else if section == 3  //Bundled Service Header Talbeview Cell
            {
                if isClickOnStandard
                {
                    return 1
                }
                else
                {
                    return 0
                }
            }
            else if section == 4  //Renewal Service
            {
                if isClickOnStandard
                {
                    return arrRenewalServices.count
                }
                else
                {
                    return 0
                }
                
            }
            else
            {
                return 1
            }
        }
        else
        {
            return 1
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblData
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CouponTableViewCell", for: indexPath) as! CouponTableViewCell
                
                let matchesObject = arrCouponDetail.object(at: indexPath.row) as! NSManagedObject
                cell.setData(matchesObject: matchesObject)
                cell.delegate = self
                
                if getOpportunityStatus()
                {
                    cell.isUserInteractionEnabled = false
                }
               
                return cell
            }
            else if indexPath.section == 1
            {

                let matchesObject = arrStandardServices.object(at: indexPath.row) as! NSManagedObject
            
                
                if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OneTimeServiceTableViewCell", for: indexPath) as! OneTimeServiceTableViewCell
                    cell.setData(matchesObject: matchesObject)
                    cell.delegate = self
                    if getOpportunityStatus()
                    {
                        cell.isUserInteractionEnabled = false
                    }
                    
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "StandardServiceTableViewCell", for: indexPath) as! StandardServiceTableViewCell
                    cell.setData(matchesObject: matchesObject)
                    cell.delegate = self
                    if getOpportunityStatus()
                    {
                        cell.isUserInteractionEnabled = false
                    }
                    
                    return cell
                }


            }
            else if indexPath.section == 2
            {
                let matchesObject = arrCustomService.object(at: indexPath.row) as! NSManagedObject

                if "\(matchesObject.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OneTimeServiceTableViewCell", for: indexPath) as! OneTimeServiceTableViewCell
                    
                    cell.delegateCustom = self
                    cell.setDataCustom(matchesObject: matchesObject)
                    if getOpportunityStatus()
                    {
                        cell.isUserInteractionEnabled = false
                    }
                    
                    return cell
                }
                
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CustomServiceTableViewCell", for: indexPath) as! CustomServiceTableViewCell
                    cell.setData(matchesObject: matchesObject)
                    cell.delegate = self
                    if getOpportunityStatus()
                    {
                        cell.isUserInteractionEnabled = false
                    }
                    return cell
                }
 
            }
            else if indexPath.section == 3
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell", for: indexPath) as! HeaderTableViewCell
                cell.delegate = self
                cell.configureTable()
                
                if getOpportunityStatus()
                {
                    cell.isUserInteractionEnabled = false
                }
                return cell
            }
            else if indexPath.section == 4
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RenewalServiceCell", for: indexPath) as! RenewalServiceCell
                let matchesObject = arrRenewalServices.object(at: indexPath.row) as! NSManagedObject
                cell.setData(matchesObject: matchesObject)
                cell.delegate = self

                if getOpportunityStatus()
                {
                    cell.isUserInteractionEnabled = false
                }
                
                return cell
            }
            else
            {
                return UITableViewCell()
            }
        }
        else
        {
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        if indexPath.section == 3 //Height Calculation Logic
        {
            if isClickOnStandard
            {
                var height = 0
                
                if DeviceType.IS_IPAD
                {
                    if getBundleCount().count == 1
                    {
                        height = getBundleCount().count * 325 + getBundleCount().count * (80 + 10)

                    }
                    else
                    {
                        height = getBundleCount().count * 275 + getBundleCount().count * 80
                    }
                }
                else
                {
                    height = getBundleCount().count * 210 + getBundleCount().count * 50
                }
                
                return CGFloat(height)
            }
            else
            {
                return 0
            }
        }
        else
        {
            return UITableView.automaticDimension

        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete
        {
            
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if indexPath.section == 3
        {
            return false
        }
        else
        {
            return false//true

        }
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
                
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            
            
            if indexPath.section == 1
            {
                
                let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
                    
                    self.isEditInSalesAuto = true
                    
                    let matches = self.arrStandardServices.object(at: indexPath.row) as! NSManagedObject
                    
                    let arrayData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ AND soldServiceStandardId = %@ AND serviceSysName = %@", self.strLeadId, "\(matches.value(forKey: "soldServiceStandardId")!)","\(matches.value(forKey: "serviceSysName")!)"))
                    
                    let strSoldServiceId = "\(matches.value(forKey: "soldServiceStandardId") ?? "")"
                    if arrayData.count > 0 {
                        
                        let objTempSales = arrayData[0] as! NSManagedObject
                        
                        deleteDataFromDB(obj: objTempSales)
                        self.arrStandardServices =  self.fetchStandardServiceFromCoreData()
                        
                        
                        //For Corrosponding Renwal
                        
                        let arrayDataRenwal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ AND soldServiceStandardId = %@", self.strLeadId, strSoldServiceId))
                        
                        if arrayDataRenwal.count > 0 {
                            
                            let objTempSales = arrayDataRenwal[0] as! NSManagedObject
                            deleteDataFromDB(obj: objTempSales)
                            self.arrRenewalServices = self.fetchRenewalServiceFromCoreData()
                        }
                        
                        self.tblData.reloadData()
                    }

                    self.view.endEditing(true)
                }))
                alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                    
                }))

                alert.popoverPresentationController?.sourceView = self.view
                self.present(alert, animated: true, completion: {
                })
                

            }
            else if indexPath.section == 2
            {
                
                let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
                    
                    self.isEditInSalesAuto = true
                    
                    let matches = self.arrCustomService.object(at: indexPath.row) as! NSManagedObject
                    
                    let arrayData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ AND soldServiceNonStandardId = %@ AND serviceName = %@", self.strLeadId, "\(matches.value(forKey: "soldServiceNonStandardId")!)","\(matches.value(forKey: "serviceName")!)"))
                    
                    if arrayData.count > 0 {
                        
                        let objTempSales = arrayData[0] as! NSManagedObject
                        
                        deleteDataFromDB(obj: objTempSales)
                        self.arrCustomService = self.fetchCustomServiceFromCoreData()
                        self.tblData.reloadData()
                    }
                    self.view.endEditing(true)
                }))
                alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                    
                }))

                alert.popoverPresentationController?.sourceView = self.view
                self.present(alert, animated: true, completion: {
                })
                

            }
            else if indexPath.section == 4
            {
                
                let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
                    
                    self.isEditInSalesAuto = true
                    
                    let matches = self.arrRenewalServices.object(at: indexPath.row) as! NSManagedObject
                    
                    let arrayData = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ AND soldServiceStandardId = %@", self.strLeadId, "\(matches.value(forKey: "soldServiceStandardId") ?? "")"))
                    
                    if arrayData.count > 0 {
                        
                        let objTempSales = arrayData[0] as! NSManagedObject
                        
                        deleteDataFromDB(obj: objTempSales)
                        self.arrRenewalServices = self.fetchRenewalServiceFromCoreData()
                        self.tblData.reloadData()
                    }
                    self.view.endEditing(true)
                }))
                alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                    
                }))

                alert.popoverPresentationController?.sourceView = self.view
                self.present(alert, animated: true, completion: {
                })
                

            }
            
        }

        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            if indexPath.section == 1
            {
                self.isEditInSalesAuto = true
                let matches = self.arrStandardServices.object(at: indexPath.row) as! NSManagedObject
                self.goForEditService(matchesServiceEdit: matches, type: NewServiceType.Standard)
            }
            else if indexPath.section == 2
            {
                self.isEditInSalesAuto = true
                let matches = self.arrCustomService.object(at: indexPath.row) as! NSManagedObject
                self.goForEditService(matchesServiceEdit: matches, type: NewServiceType.Custom)
            }
        }

        delete.backgroundColor = UIColor.init(red: 171/255, green: 52/255, blue: 77/255, alpha: 1)

        edit.backgroundColor = UIColor.init(red: 211/255, green: 161/255, blue: 58/255, alpha: 1)

        if indexPath.section == 4
        {
            return [delete]
        }
        else
        {
            return [delete, edit]
        }
        
    }
}
// MARK: ----------------------- Bundle Table Extension -------------------------

extension NewSales_SelectService : HeaderTitleTableCellProtocol
{
    func deleteBundleNew(strId: String) {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
            
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && bundleId=%@", self.strUserName, self.strLeadId, strId))
            
            for item in arryOfData
            {
                let objTempSales  = item as! NSManagedObject
                deleteDataFromDB(obj: objTempSales)
            }
            self.tblData.reloadData()
            
            
            self.view.endEditing(true)
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
        }))

        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
    }
}

// MARK: ----------------------- Standard & Para Service Table Extension -------------------------


extension NewSales_SelectService : StandardTableCellProtocol
{
    func btnDeleteServiceClicked(matchesObject: NSManagedObject) {
        
        deleteStandardService(matches: matchesObject)
    }
    
    func btnEditServiceClicked(matchesObject: NSManagedObject) {
        
        editStandardService(matches: matchesObject)
    }
    
    func addPlusService(arrPlusService: NSMutableArray, matchesParentObject: NSManagedObject, strServiceSysName: String, strBillingFreqSysName: String, strServiceFreqSysName: String) {
                self.isEditInSalesAuto = true
                let vc = storyboardNewSalesSelectService.instantiateViewController(withIdentifier: "NewSales_AddServiceVC") as! NewSales_AddServiceVC
                vc.serviceType = .Standard
                vc.isPlusService = true
                vc.objParentForPlusService = matchesParentObject
                vc.arrPlusService = arrPlusService as NSArray
                self.navigationController?.pushViewController(vc, animated: false)
    }
    func getTextFieldAction(txtTag: Int, value:String , matchesObject: NSManagedObject) {
        
        self.isEditInSalesAuto = true
        fetchCouponAppliedOnService(soldServiceId: "\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")")
        var modTxtField = 0 // Parameter service object
        var assignValue = value
        
        if txtTag >= 1000 && txtTag < 2000
        {
            modTxtField = txtTag - 1000
        }
        else if txtTag >= 2000 && txtTag < 3000
        {
            modTxtField = txtTag - 2000
        }
        else if txtTag >= 3000 && txtTag < 4000
        {
            modTxtField = txtTag - 3000
        }
        else if txtTag >= 4000 && txtTag < 5000
        {
            modTxtField = txtTag - 4000
        }
        else if txtTag >= 5000 && txtTag < 6000
        {
            modTxtField = txtTag - 5000
        }
        else if txtTag >= 6000 && txtTag < 7000
        {
            modTxtField = txtTag - 6000
        }
        
        let textFldNumber = (txtTag - modTxtField)/1000
        
        if txtTag >= 1000 && txtTag < 7000
        {
            let arrAdditionalParamterDcs = matchesObject.value(forKey: "additionalParameterPriceDcs") as! NSArray
            
            guard arrAdditionalParamterDcs.count > 0  else {
                
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Some thing went wrong, please sync master and try again later", viewcontrol: self)
                self.tblData.reloadData()
                return
            }
            
            var dict = NSDictionary()
            
            guard arrAdditionalParamterDcs.object(at: modTxtField) is NSDictionary else {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Some thing went wrong, please sync master and try again later", viewcontrol: self)
                self.tblData.reloadData()
                return
            }
            dict = arrAdditionalParamterDcs.object(at: modTxtField) as! NSDictionary
            
            var dictParaObject = NSMutableDictionary()
            dictParaObject = dict.mutableCopy() as! NSMutableDictionary

            if textFldNumber == 2
            {
                if assignValue == "" || (assignValue as NSString).floatValue == 0
                {
                    assignValue = "0"
                }
                dictParaObject.setValue(assignValue, forKey: "Unit")
                
                let initialPrice = Double("\(dictParaObject.value(forKey: "InitialUnitPrice") ?? "")") ?? 0.0
                let maintPrice = Double("\(dictParaObject.value(forKey: "MaintUnitPrice") ?? "")") ?? 0.0
                var calculateInitial = 0.0, calculateMaint = 0.0
                calculateInitial = initialPrice * Double((assignValue as NSString).floatValue)
                calculateMaint = maintPrice * Double((assignValue as NSString).floatValue)
                
                if calculateInitial < Double("\(dictParaObject.value(forKey: "MinUnitPrice") ?? "")") ?? 0.0
                {
                    calculateInitial = Double("\(dictParaObject.value(forKey: "MinUnitPrice") ?? "")") ?? 0.0
                }
                dictParaObject.setValue(stringToFloat(strValue: "\(calculateInitial)"), forKey: "FinalInitialUnitPrice")
                dictParaObject.setValue(stringToFloat(strValue: "\(calculateMaint)"), forKey: "FinalMaintUnitPrice")

                
                print("Para service unit entered \(dictParaObject)")
            }
            else if textFldNumber == 5
            {
                
                if ("\(dictParaObject.value(forKey: "Unit") ?? "")" as NSString).floatValue == 0 || assignValue == "" || (assignValue as NSString).floatValue == 0 {
                
                    assignValue = stringToFloat(strValue: "")

                }
                
                dictParaObject.setValue(assignValue, forKey: "FinalInitialUnitPrice")

                print("Para initial price entered \(dictParaObject)")
            }
            else if textFldNumber == 6
            {
                
                if ("\(dictParaObject.value(forKey: "Unit") ?? "")" as NSString).floatValue == 0 || assignValue == "" || (assignValue as NSString).floatValue == 0 {
                
                    assignValue = stringToFloat(strValue: "")

                }
               
                dictParaObject.setValue(assignValue, forKey: "FinalMaintUnitPrice")
                print("Para maint price entered \(dictParaObject)")
            }
            
            
            if isCouponApplied
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Change in price may impact coupon discount so please remove and add the coupon again", viewcontrol: self)
                tblData.reloadData()
            }
            else
            {
                var arrUpdate = NSMutableArray()
                arrUpdate = arrAdditionalParamterDcs as NSArray as! NSMutableArray
                arrUpdate.replaceObject(at: modTxtField, with: dictParaObject)
               
                var arrOfKeys = NSMutableArray()
                var arrOfValues = NSMutableArray()
                
                arrOfKeys = [
                 "additionalParameterPriceDcs"
                ]
                
                arrOfValues = [
                    arrUpdate]
                
                var isSuccess = Bool()
                
                isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,"\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if(isSuccess == true)
                {
                    print("updated successsfully")
                    tblData.reloadData()
                }
                else
                {
                    print("updates failed")
                }
            }
        }
        
        if isCouponApplied
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Change in price may impact coupon discount so please remove and add the coupon again", viewcontrol: self)
            tblData.reloadData()
        }
        else
        {
            //Final Update Total Price and Billing Amount
            
            finalUpdatePricingAndBillingAmount()
            
            // For Renewal
            
            let arrRenewal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId = %@", strLeadId, "\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"))
            
            if arrRenewal.count > 0
            {
                let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "serviceSysName") ?? "")")
                
                if dictService.count > 0
                {
                    if dictService.value(forKey: "ServiceMasterRenewalPrices") is NSArray
                    {
                        let arrRenewaNaster = dictService.value(forKey: "ServiceMasterRenewalPrices") as! NSArray
                        
                        let arr = arrRenewaNaster.filter { (dict) -> Bool in
                            
                            return "\((dict as! NSDictionary).value(forKey: "ServiceMasterId") ?? "")".caseInsensitiveCompare("\(dictService.value(forKey: "ServiceMasterId") ?? "")") == .orderedSame
                        } as NSArray

                        if arr.count > 0
                        {
                            let dictRenewalData = arr.object(at: 0) as! NSDictionary
                            
                            
                            let arryOfServiceData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && soldServiceStandardId=%@", strUserName, strLeadId, "\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"))
                            if arryOfServiceData.count > 0
                            {
                                let matches = arryOfServiceData.object(at: 0) as! NSManagedObject
                                
                                updateRenewalDetail(dictRenewalData: dictRenewalData, TotalInitialPrice: "\(matches.value(forKey: "totalInitialPrice") ?? "")", SoldServiceStandardId: "\(matches.value(forKey: "soldServiceStandardId") ?? "")", ServiceMasterId: "\(matches.value(forKey: "serviceId") ?? "")")
                            }

                        }
                    }
                }
            }
        }
        
    }
    
    
    func finalUpdatePricingAndBillingAmount() {
    

        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool

        let arryOfServiceData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        
        var totalInitialPrice = 0.0
        var totalMaintPrice = 0.0
        var finalUnitBaseInitialPrice = 0.0
        var finalUnitBasedMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        for item in arryOfServiceData
        {
            totalInitialPrice = 0.0
            totalMaintPrice = 0.0
            finalUnitBaseInitialPrice = 0.0
            finalUnitBasedMaintPrice = 0.0
            totalBillingAmount = 0.0
            
            let matches = item as! NSManagedObject
            
            //let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "serviceSysName") ?? "")")
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "billingFrequencySysName") ?? "")")
            let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "frequencySysName") ?? "")")
            
            
            let initialPrice = ("\(matches.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue
            let maintPrice = ("\(matches.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue
            let unit = ("\(matches.value(forKey: "unit") ?? "")" as NSString).doubleValue
            
            
            if unit > 0
            {
                totalInitialPrice = totalInitialPrice + (initialPrice * unit)
                totalMaintPrice = totalMaintPrice + (maintPrice * unit)
                finalUnitBaseInitialPrice = (initialPrice * unit)
                finalUnitBasedMaintPrice = (maintPrice * unit)
            }
            else
            {
                totalInitialPrice = totalInitialPrice + initialPrice
                totalMaintPrice = totalMaintPrice + maintPrice
                finalUnitBaseInitialPrice = initialPrice
                finalUnitBasedMaintPrice = maintPrice

            }
            
            
            if matches.value(forKey: "additionalParameterPriceDcs") is NSArray
            {
                let arrPara = matches.value(forKey: "additionalParameterPriceDcs") as! NSArray
                
                if arrPara.count > 0
                {
                    for item in arrPara
                    {
                        let dict = item as! NSDictionary
                        
                        totalInitialPrice = totalInitialPrice + ("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).doubleValue
                        totalMaintPrice = totalMaintPrice + ("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).doubleValue

                    }
                }

            }
            
            //Billing Price Calculation
            
            let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            
            if "\(matches.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                
            }
            else
            {
                if chkFreqConfiguration
                {
                    if "\(matches.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                    {
                        totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                    else
                    {
                        totalBillingAmount = ( totalMaintPrice *  (((strServiceFrqYearOccurence as NSString).doubleValue) - 1) ) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            
            if totalBillingAmount < 0
            {
                totalBillingAmount = 0.0
            }
            
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            arrOfKeys = [
                "billingFrequencyPrice",
                "finalUnitBasedInitialPrice",
                "finalUnitBasedMaintPrice",
                "totalInitialPrice",
                "totalMaintPrice",
            ]
            
            
            arrOfValues = [
                
                String(format: "%.5f", totalBillingAmount),
                String(format: "%.5f", finalUnitBaseInitialPrice),
                String(format: "%.5f", finalUnitBasedMaintPrice),
                String(format: "%.5f", totalInitialPrice),
                String(format: "%.5f", totalMaintPrice)
            ]
            
            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId, "\(matches.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            
            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
             
        }
        

    }
    func finalUpdatePricingAndBillingAmountCustom() {
    

        chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool

        let arryOfServiceData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        
        var totalInitialPrice = 0.0
        var totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        for item in arryOfServiceData
        {
            totalInitialPrice = 0.0
            totalMaintPrice = 0.0
            totalBillingAmount = 0.0
            
            let matches = item as! NSManagedObject
            
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "billingFrequencySysName") ?? "")")
            let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "frequencySysName") ?? "")")
            
            
            let initialPrice = ("\(matches.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue
            let maintPrice = ("\(matches.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue
//            let unit = ("\(matches.value(forKey: "unit") ?? "")" as NSString).doubleValue
            
            
           
            totalInitialPrice = initialPrice
            totalMaintPrice = maintPrice
             

            //Billing Price Calculation
            
            let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            
            if "\(matches.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                
            }
            else
            {
                if chkFreqConfiguration
                {
                    if "\(matches.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                    {
                        totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                    else
                    {
                        totalBillingAmount = ( totalMaintPrice *  (((strServiceFrqYearOccurence as NSString).doubleValue) - 1) ) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            
            if totalBillingAmount < 0
            {
                totalBillingAmount = 0.0
            }
            
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            arrOfKeys = [
                "billingFrequencyPrice"
            ]
            
            
            arrOfValues = [
                
                String(format: "%.5f", totalBillingAmount)
            ]
            
            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@",strLeadId, "\(matches.value(forKey: "soldServiceNonStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            
            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
             
        }
        

    }
    
    func updateRenewalDetail(dictRenewalData : NSDictionary , TotalInitialPrice strInitialPrice: String, SoldServiceStandardId strsoldServiceStandardId: String, ServiceMasterId strServiceMasterIdRenewal: String)
    {
        
        var chkForUpdate = false
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strRenewalAmount = String()
        var strRenewalPercentage = String()
                
        if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "Percentage"
        {
            strRenewalPercentage = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")"
            
            var price = Float()
            var per = Float()
            per = Float("\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")") ?? 0.0
            price = (Float(strInitialPrice) ?? 0.0) * per / 100
            
            if price < Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
            {
                price = Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
            }
            chkForUpdate = true
            strRenewalAmount = String(format: "%.2f", price)
        }
        else
        {
            strRenewalPercentage = "0.00"
        }
        
        if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "FlatPrice"
        {
            strRenewalAmount = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "0.00")"
        }
        
        arrOfKeys = [
                    "renewalAmount"
                     ]
        
        arrOfValues = [
                       strRenewalAmount
                       ]
        
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId, strsoldServiceStandardId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)


        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
        
    }
    
}

// MARK: ----------------------- Renewal Service Table Extension -------------------------
extension NewSales_SelectService : RenewalServiceCellProtocol
{
    func btnDeleteRenewalServiceClicked(matchesObject: NSManagedObject) {
        
        deleteRenewalService(matches: matchesObject)
    }
    
    func btnEditDescClicked(matchesObject: NSManagedObject) {
        self.isEditInSalesAuto = true
        //goToNoramlHTMLEditor(strText: "\(matchesObject.value(forKey: "renewalDescription") ?? "")", matchesObject: matchesObject)
        
        goToNoramlTextEditor(strText: "\(matchesObject.value(forKey: "renewalDescription") ?? "")", matchesObject: matchesObject)
    }

}

// MARK: ----------------------- Custom Service Table Extension -------------------------

extension NewSales_SelectService: CustomServiceTableCellProtocol
{
    func btnDeleteCustomServiceClicked(matchesObject: NSManagedObject) {
        
        deleteCustomService(matches: matchesObject)
    }
    
    func btnEditCustomServiceClicked(matchesObject: NSManagedObject) {
        
        editCustomService(matches: matchesObject)
    }
    
}

// MARK: ----------------------- Coupon Service Table Extension -------------------------

extension NewSales_SelectService : CouponTableCellProtocol
{
    
    func btnDeleteCouponServiceClicked(matchesObject: NSManagedObject) {
        
        deleteCouponDetail(matches: matchesObject)
    }
    
    
}

// MARK: - -------------- Normal Text Editor --------------


extension NewSales_SelectService: HTMLEditorDelegate
{
    func didReceiveHTMLEditorText(strText: String) {
    
        
    }
    
    func didReceiveRenewalHTMLEditorText(strText: String, managedObject: NSManagedObject) {
        
        self.isEditInSalesAuto = true
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = [
        
            "renewalDescription",
        ]

        arrOfValues = [
            strText,
            ]
        
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,"\(managedObject.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        
        if(isSuccess == true)
        {
            print("updated successsfully")
            tblData.reloadData()
        }
        else
        {
            print("updates failed")
        }
    }
    
    func getTextFieldActionRenewal(txtTag: Int, value:String , matchesObject: NSManagedObject) {
        
        self.isEditInSalesAuto = true
        
        var assignValue = value
        var strInitalPrice = "0"
        var strRenewalAmount = ""

        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && soldServiceStandardId == %@", strUserName, strLeadId, "\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"))

        if arryOfData.count > 0
        {
            let matchedService = arryOfData.object(at: 0) as! NSManagedObject
            
            var dictService = NSDictionary()
             dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matchedService.value(forKey: "serviceSysName") ?? "")")
             
             if dictService.value(forKey: "ServiceMasterRenewalPrices") is NSArray
             {
                 let arrRenewaNaster = dictService.value(forKey: "ServiceMasterRenewalPrices") as! NSArray
                 
                 let arr = arrRenewaNaster.filter { (dict) -> Bool in
                     
                     return "\((dict as! NSDictionary).value(forKey: "ServiceMasterId") ?? "")".caseInsensitiveCompare("\(dictService.value(forKey: "ServiceMasterId") ?? "")") == .orderedSame
                 } as NSArray

                 if arr.count > 0
                 {
//                     if arryOfData.count > 0 {
//                         if arryOfData[0] is NSManagedObject{
//                             let obj = arryOfData[0] as! NSManagedObject
//                             strInitalPrice = "\(obj.value(forKey: "initialPrice") ?? "")"
//                         }
//                     }
                     let dictRenewalData = arr.object(at: 0) as! NSDictionary
                     if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "Percentage"
                     {
//                         var price = Float()
//                         var per = Float()
//                         per = Float("\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")") ?? 0.0
//                         price = (Float(strInitalPrice) ?? 0.0 ) * per / 100
//
//                         if price < Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
//                         {
//                             price = Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
//                         }
                         
 //                        strRenewalAmount = String(format: "%.2f", price)
                         strRenewalAmount = String(format: "%.2f", (Float(assignValue) ?? 0.0 ))
                         
                     }
                     else if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "FlatPrice"
                     {
                         //strRenewalAmount = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "0.00")"
                         strRenewalAmount = String(format: "%.2f", (Float(assignValue) ?? 0.0 ))
                     }
                     
                 }
             }
             assignValue = strRenewalAmount
            
        }
        
        if assignValue == "" || (assignValue as NSString).floatValue == 0
        {
            assignValue = stringToFloat(strValue: assignValue)
        }
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = [
        
            "renewalAmount",
        ]

        arrOfValues = [
            stringToFloat(strValue: assignValue),
            ]
        
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,"\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        if(isSuccess == true)
        {
            print("updated successsfully")
            tblData.reloadData()
        }
        else
        {
            print("updates failed")
        }
    }
}

extension NewSales_SelectService: TextEditorDelegate
{
    func didReceiveEditorText(strText: String) {
        
        
    }
    func didReceiveEditorTextWithObject(strText : String, object: NSManagedObject)
    {
        self.isEditInSalesAuto = true
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = [
        
            "renewalDescription",
        ]

        arrOfValues = [
            strText,
            ]
        
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,"\(object.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        
        if(isSuccess == true)
        {
            print("updated successsfully")
            tblData.reloadData()
        }
        else
        {
            print("updates failed")
        }
    }
}
