//
//  NewSales_AddServiceVC.swift
//  DPS
//
//  Created by Saavan Patidar on 17/05/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

enum NewServiceType: String {
    
    case Standard, Bundle, Custom
}
enum EditInternalNotes: String {
    
    case Standard, Custom
}
enum EditHTMLDesc: String {
    
    case StandardServiceDesc, CustomServiceDesc,CustomTermsCondition
}
class NewSales_AddServiceVC: UIViewController {

    
    //MARK: ---------------- Variable --------------------

    var isUnitBasedService = false
    var isParameterBasedService = false
    var isPlusService = false
    var isParameterExist = false
    var strHTMLTermsConditionCustom = ""
    var strHTMLServiceDescriptionCustom = ""


    //MARK: ---------------- Outlets --------------------
    
    @IBOutlet var lblHeaderTitle: UILabel!
    @IBOutlet var const_LblLine_L: NSLayoutConstraint!
    @IBOutlet var const_BtnStandard_L: NSLayoutConstraint!
    @IBOutlet var const_BtnCustom_L: NSLayoutConstraint!
    @IBOutlet var viewTopButton: UIView!
    @IBOutlet var btnStandard: UIButton!
    @IBOutlet var btnCustom: UIButton!
    
    //MARK: ----- View Standard Service -----

    
    @IBOutlet var viewStandardService: UIView!
    
    @IBOutlet var txtFldCategoryStandard: ACFloatingTextField!
    
    @IBOutlet var btnCategoryStandard: UIButton!
    @IBOutlet var txtFldServiceStandard: ACFloatingTextField!
    
    @IBOutlet var btnServiceStandard: UIButton!
    
    
    @IBOutlet var viewServicePackageStandard: UIView!
    
    @IBOutlet var txtFldServicePackageStandard: ACFloatingTextField!

    @IBOutlet var btnServicePackageStandard: UIButton!
    
    @IBOutlet var txtFldServiceFreqStandard: ACFloatingTextField!
    
    @IBOutlet var btnServiceFreqStandard: UIButton!
    
    @IBOutlet var txtFldBillingFrqStandard: ACFloatingTextField!
    
    @IBOutlet var btnBillingFreqStandard: UIButton!
    
    @IBOutlet var txtFldInitialPriceStandard: ACFloatingTextField!
    @IBOutlet var txtFldMaintPriceStandard: ACFloatingTextField!
    @IBOutlet var txtFldDiscountStandard: ACFloatingTextField!
    @IBOutlet var txtFldDiscountPerStandard: ACFloatingTextField!
    
    //@IBOutlet var txtViewServiceDescStandard: UITextView!
    
    @IBOutlet var lblServiceDescStandard: UILabel!
    
    @IBOutlet var btnEditServiceDescStandard: UIButton!
    @IBOutlet var btnEditInternalNotesStandard: UIButton!

    //@IBOutlet var txtViewInternalNotesStandard: UITextView!
    @IBOutlet var lblInternalNotesStandard: UILabel!
    
    @IBOutlet var viewUnitStandard: UIView!
    @IBOutlet var txtFldUnitStandard: ACFloatingTextField!
    
    @IBOutlet var btnCancelStandard: UIButton!
    
    @IBOutlet var btnSaveStandard: UIButton!
    //MARK: ----- View Bundle Service -----
    
    @IBOutlet var viewBundleService: UIView!
    @IBOutlet var txtFldBundleName: ACFloatingTextField!
    
    @IBOutlet var btnBundleName: UIButton!
    @IBOutlet var txtFldBillingFreqBundle: ACFloatingTextField!
    
    @IBOutlet var btnBillingFreqBundle: UIButton!
    @IBOutlet var btnCancelBundle: UIButton!
    
    @IBOutlet var btnSaveBundle: UIButton!
    
    //MARK: ----- View Custom Service -----
    
    @IBOutlet var viewCustomService: UIView!
    
    @IBOutlet var txtFldDeptNameCustom: ACFloatingTextField!
    
    @IBOutlet var btnDeptNameCustom: UIButton!
    @IBOutlet var txtFldServiceNameCustom: ACFloatingTextField!
    
    @IBOutlet var txtFldServiceFreqCustom: ACFloatingTextField!
    
    @IBOutlet var btnBillingFreqCustom: UIButton!
    @IBOutlet var txtFldBillingFreqCustom: ACFloatingTextField!
    
    @IBOutlet var txtFldInitialPriceCustom: ACFloatingTextField!
    @IBOutlet var txtFldMaintPriceCustom: ACFloatingTextField!
    @IBOutlet var txtFldDiscountCustom: ACFloatingTextField!
    @IBOutlet var txtFldDiscountPerCustom: ACFloatingTextField!
    
    @IBOutlet var txtViewServiceDescCustom: UITextView!
    
    @IBOutlet var lblServiceDescCustom: UILabel!
    @IBOutlet var btnEditServiceDescCustom: UIButton!
    
    @IBOutlet var lblInternalNotesCustom: UILabel!
    @IBOutlet var txtViewInternalNotesCustom: UITextView!
    @IBOutlet var btnEditInternalNotesCustom: UIButton!

    @IBOutlet var lblTermsConditionCustom: UILabel!
    @IBOutlet var txtViewTermsConditionCustom: UITextView!
    @IBOutlet var btnEditTermsConditionCustom: UIButton!

    @IBOutlet var btnCancelCustom: UIButton!
    
    @IBOutlet var btnSaveCustom: UIButton!
    
    //MARK: ---------------- Variable Declaration --------------------
    
    var serviceType = NewServiceType.Standard
    
    // MARK: - --------- Global Variables ---------
    
    @objc var strLeadId = NSString ()
    @objc var strBranchSysName = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    @objc var objParentForPlusService = NSManagedObject()

    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var strEmpName = String()
    var dictLoginData = NSDictionary()
    
    // MARK: - ---------Dictionary ---------
    
    var dictCategory = NSDictionary()
    var dictService = NSDictionary()
    var dictServiceFrequency = NSDictionary()
    var dictServiceFrequencyCustom = NSDictionary()

    var dictBillingFrequency = NSDictionary()
    var dictBillingFrequencyCustom = NSDictionary()

    var dictPackage = NSDictionary()

    
    var dictDepartment = NSDictionary()
    var dictFrequencyNonStan = NSDictionary()
    var dictServiceNameFromSysname = NSDictionary()
    var dictServiceTermsFromSysname = NSDictionary()
    var dictBundle = NSDictionary()
    // MARK: - ---------Array ---------
    
    var arrCategory = NSArray()
    var arySelectedCategory = NSMutableArray()
    
    var arrService = NSArray()
    var arySelectedService = NSMutableArray()
    var arrPlusService = NSArray()

    
    var arrServicePackage = NSArray()
    var arySelectedServicePackage = NSMutableArray()

    var arrServiceFrequency = NSArray()
    var arySelectedServiceFrequency = NSMutableArray()
    var arySelectedServiceFrequencyCustom = NSMutableArray()


    var arrFrequency = NSArray()
    var arrFrequencyAll = NSArray()
    var arrServiceBillingFrequency = NSArray()
    var arrServiceBillingFrequencyCustom = NSArray()

    var arySelectedBillingFrequency = NSMutableArray()
    var arySelectedBillingFrequencyCustom = NSMutableArray()

    
    var arrDepartment = NSMutableArray()
    var arySelectedDepartment = NSMutableArray()

    var arrBundle = NSArray()
    var arySelectedBundle = NSMutableArray()
    
    var arrServiceAdditionalParaDCS = NSMutableArray()
    var arrServiceMasterRenewalPrices = NSArray()
    
    // MARK: - ---------String ---------
    
    var strCategorySysName = String()
    var strCategoryName = String()
    var strServiceName = String()
    var strServiceSysName = String()
    var strServiceId = String()
    var strPackageId = String()
    var strPackageName = String()
    var strMinimumInitialPrice = String()
    var strMinimumMaintPrice = String()
    var strFreqencySysName = String()
    var strFreqencyName = String()
    var strDeptSysName = String()
    var strFreqencySysNameNonStan = String()
    var strFreqencyNameNonStan = String()
    var strHtmlTermsConditionNonStan = String()
    var strHtmlDescriptionNonStan = String()
    var strSoldServiceStandardId = String()
    var strSoldServiceNonStandardId = String()
    var strArea = ""
    var strBedroom = ""
    var strBathroom = ""
    var strLinearSqFt = ""
    var strLotsize = ""
    var strNoStory = ""
    var strTurfArea = ""
    var strShrubArea = ""
    var strTotalInitialPriceSatandard = ""

    //MARK: - ---------Bool ---------
    
    var isChangeServiceDesc = Bool()
    var isEditDescNonStan = Bool()
    var isEditTermsNonStan = Bool()
    var isEditService = Bool()
    var isCouponApplied = Bool()


    var editNotesType = EditInternalNotes.Standard
    var editHtmlDescType = EditHTMLDesc.StandardServiceDesc
    var objLeadDetail = NSManagedObject()

    
    @objc var strForEdit = NSString ()
    @objc var matchesServiceEdit = NSManagedObject ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        basicFunction()
        setInitial()
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: ---------------- Other Function --------------------
    
    fileprivate func back()
    {
        self.navigationController?.popViewController(animated: false)
    }
    func basicFunction()  {
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strLeadId = "\(nsud.value(forKey: "LeadId") ?? "")" as NSString
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        objLeadDetail = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
            
        strBranchSysName = "\(objLeadDetail.value(forKey: "branchSysName") ?? "")" as NSString
        
        getPropertyInfo()
    }
    fileprivate func getPropertyInfo()  {
        
        strArea = getOnlyTrimmedString(strText: "\(objLeadDetail.value(forKey: "area") ?? "")")
        strBedroom = getOnlyTrimmedString(strText: "\(objLeadDetail.value(forKey: "noOfBedroom") ?? "")")
        strBathroom = getOnlyTrimmedString(strText: "\(objLeadDetail.value(forKey: "noOfBathroom") ?? "")")
        strLinearSqFt = getOnlyTrimmedString(strText: "\(objLeadDetail.value(forKey: "linearSqFt") ?? "")")
        strLotsize = getOnlyTrimmedString(strText: "\(objLeadDetail.value(forKey: "lotSizeSqFt") ?? "")")
        strNoStory = getOnlyTrimmedString(strText: "\(objLeadDetail.value(forKey: "noOfStory") ?? "")")
        strTurfArea = getOnlyTrimmedString(strText: "\(objLeadDetail.value(forKey: "turfArea") ?? "")")
        strShrubArea = getOnlyTrimmedString(strText: "\(objLeadDetail.value(forKey: "shrubArea") ?? "")")
    }
    fileprivate func setInitial() {
        //----Navin
        txtFldInitialPriceStandard.keyboardType = .decimalPad
        txtFldMaintPriceStandard.keyboardType = .decimalPad
        txtFldDiscountStandard.keyboardType = .decimalPad
        txtFldDiscountPerStandard.keyboardType = .decimalPad
        txtFldInitialPriceCustom.keyboardType = .decimalPad
        txtFldMaintPriceCustom.keyboardType = .decimalPad
        txtFldDiscountCustom.keyboardType = .decimalPad
        txtFldDiscountPerCustom.keyboardType = .decimalPad

        //----End

        clearDefault()
        strCategorySysName = ""
        strServiceSysName = ""
        strFreqencySysName = ""
        strServiceId = ""
        strDeptSysName = ""
        strFreqencyNameNonStan = ""
        strFreqencySysNameNonStan = ""
        isChangeServiceDesc = false
        isEditDescNonStan = false
        isEditTermsNonStan = false
        strHtmlDescriptionNonStan = ""
        strHtmlTermsConditionNonStan = ""
        lblServiceDescStandard.text = ""
        lblInternalNotesStandard.text = ""
        lblServiceDescCustom.text = ""
        lblInternalNotesCustom.text = ""
        lblTermsConditionCustom.text = ""



        arrCategory = global.getCategoryDeptWiseGlobal()! as NSArray
        let arrFreq = getFrequency()
        arrFrequency = arrFreq.arrServiceFreq
        arrServiceBillingFrequency = arrFreq.arrBillingFreq
        arrFrequencyAll = arrFreq.arrAllFreq
        arrServiceBillingFrequencyCustom = arrFreq.arrBillingFreq
        setTextZeroValue()
        setDefaultBillingFrequency()
        getDepartmentNonStan()
        getServiceBundle()
        setServiceView()
        if isPlusService
        {
            getPlusServiceDetail()
        }
        
        if isEditService
        {
            editServiceDetail()
        }
    }
   
    fileprivate func setServiceView()  {
        
        if serviceType == .Standard
        {
            viewStandardService.isHidden = false
            viewBundleService.isHidden = true
            viewCustomService.isHidden = true
            lblHeaderTitle.text = "Add Standard Service"
            
            const_LblLine_L.constant = 0
            btnStandard.setTitleColor(UIColor.appThemeColor, for: .normal)
            btnCustom.setTitleColor(UIColor.black, for: .normal)
            viewTopButton.isHidden = false

        }
        else if serviceType == .Bundle
        {
            viewStandardService.isHidden = true
            viewBundleService.isHidden = false
            viewCustomService.isHidden = true
            lblHeaderTitle.text = "Add Bundle Service"
            viewTopButton.isHidden = true


        }
        else if serviceType == .Custom
        {
            viewStandardService.isHidden = true
            viewBundleService.isHidden = true
            viewCustomService.isHidden = false
            lblHeaderTitle.text = "Add Custom Service"
            
            const_LblLine_L.constant = self.view.frame.size.width / 2 + 5//btnCustom.frame.origin.x
            
            btnCustom.setTitleColor(UIColor.appThemeColor, for: .normal)
            btnStandard.setTitleColor(UIColor.black, for: .normal)
            viewTopButton.isHidden = false


        }
    }
    
    func getPlusServiceDetail()  {
        
        if isPlusService
        {
            txtFldCategoryStandard.isEnabled = false
            dictCategory = getCategoryObjectFromServiceSysName(strServiceSysName: "\(objParentForPlusService.value(forKey: "serviceSysName") ?? "")")
            strCategorySysName = "\(dictCategory.value(forKey: "SysName") ?? "")"
            txtFldCategoryStandard.text = "\(dictCategory.value(forKey: "Name") ?? "")"
            btnCategoryStandard.isEnabled = false
        }
        else
        {
            txtFldCategoryStandard.isEnabled = true
            btnCategoryStandard.isEnabled = true

        }
    }

    func getCategories()  {
        
    }
   
    func getFrequency() -> (arrServiceFreq: NSArray, arrBillingFreq: NSArray, arrAllFreq: NSArray)
    {
        var arrTempServiceFreq = NSArray()
        var arrTempBillingFreq = NSArray()
        var arrAllTypeFreq = NSArray()


        
            if(nsud.value(forKey: "MasterSalesAutomation") != nil){
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "Frequencies") is NSArray){
                    arrTempServiceFreq = dictMaster.value(forKey: "Frequencies") as! NSArray
                
                    //Frequency as per type
                    
                    let arrTemp = NSMutableArray()
                    let arrTemp2 = NSMutableArray()
                    let arrTemp3 = NSMutableArray()


                    if (arrTempServiceFreq.count  > 0)
                    {
                        for item in arrTempServiceFreq
                        {
                            let dict = item as! NSDictionary
                            let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                            if strType == "Service" || strType == "Both" || strType == ""
                            {
                                arrTemp.add(dict)
                            }
                            if strType == "Billing" || strType == "Both" || strType == ""
                            {
                                arrTemp2.add(dict)
                            }
                            arrTemp3.add(dict)
                        }
                    }
                                        
                    
                    arrTempServiceFreq = arrTemp as NSArray
                    arrTempBillingFreq = arrTemp2 as NSArray
                    arrAllTypeFreq = arrTemp3 as NSArray

                }
            }
        return (arrTempServiceFreq,arrTempBillingFreq,arrAllTypeFreq)
    }
    func getFrequencyOld() -> NSArray
    {
        var arrTempFreq = NSArray()

            if(nsud.value(forKey: "MasterSalesAutomation") != nil){
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "Frequencies") is NSArray){
                    arrTempFreq = dictMaster.value(forKey: "Frequencies") as! NSArray
                
                    //Frequency as per type
                    
                    let arrTemp = NSMutableArray()
                    
                    if (arrTempFreq.count  > 0)
                    {
                        for item in arrTempFreq
                        {
                            let dict = item as! NSDictionary
                            let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                            if strType == "Service" || strType == "Both" || strType == ""
                            {
                                arrTemp.add(dict)
                            }
                        }
                    }
                    arrTempFreq = arrTemp as NSArray
                }
            }
        return arrTempFreq
    }
    func getBillingFrequency() -> NSArray
    {
        var arrTempFreq = NSArray()

            if(nsud.value(forKey: "MasterSalesAutomation") != nil){
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "Frequencies") is NSArray){
                    
                    arrTempFreq = dictMaster.value(forKey: "Frequencies") as! NSArray
                
                    //Frequency as per type
                    
                    let arrTemp = NSMutableArray()
                    
                    if (arrTempFreq.count  > 0)
                    {
                        for item in arrTempFreq
                        {
                            let dict = item as! NSDictionary
                            let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                            if strType == "Billing" || strType == "Both" || strType == ""
                            {
                                arrTemp.add(dict)
                            }
                        }
                    }
                    arrTempFreq = arrTemp as NSArray
                }
            }
        return arrTempFreq
    }
    func getDepartmentNonStan()
    {
        arrDepartment = NSMutableArray()
        
        if nsud.value(forKey: "LeadDetailMaster") != nil
        {
            let  dictSalesLeadMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictSalesLeadMaster.value(forKey: "BranchMasters") is NSArray
            {
                let arrDept = dictSalesLeadMaster.value(forKey: "BranchMasters") as! NSArray
                
                for itemBranch in arrDept
                {
                    let dictBranch = itemBranch as! NSDictionary
                    
                    let arrTemp = dictBranch.value(forKey: "Departments") as! NSArray
                    
                    for itemDept in arrTemp
                    {
                        let dictDept = itemDept as! NSDictionary
                        
                        if ("\(dictBranch.value(forKey: "SysName")!)") == strBranchSysName as String
                        {
                            arrDepartment.add(dictDept)
                        }
                    }
                    
                }
            }
           
        }
    }
    func getServiceBundle()
    {
        arrBundle = NSMutableArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "ServiceBundles") is NSArray){
                
                arrBundle = dictMaster.value(forKey: "ServiceBundles") as! NSArray
            }
            
            let arrData = NSMutableArray()
            for item in arrBundle
            {
                let dict = item as! NSDictionary
                if "\(dict.value(forKey: "IsActive") ?? "")" == "1" && "\(dict.value(forKey: "IsDelete") ?? "")" == "0"
                {
                    arrData.add(dict)
                }
            }
            arrBundle = arrData as NSArray
        }
    }
    func clearDefault()
    {
        nsud.set("", forKey: "htmlContents")
        nsud.synchronize()
    }
    func setColorBorderForView(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
   
    func endEditing()
    {
        self.view.endEditing(true)
    }
    func getTrimmedString(strText : String) -> String {
        var str = ""
        str = strText
        str =  str.trimmingCharacters(in: .whitespacesAndNewlines)
        //str = stringToFloat(strValue: str)
        return str
    }
    func getOnlyTrimmedString(strText : String) -> String {
        var str = ""
        str = strText
        str =  str.trimmingCharacters(in: .whitespacesAndNewlines)
        str = stringToFloat(strValue: str)
        return str
    }
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = storyboardNewSalesGeneralInfo.instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.isFromSalesSelection = true
            vc.delegate = self
            
            if titleHeader == "Service Category"
            {
                vc.isForCategory = true
                vc.arrDepartmentCategory = arrDepartment
            }
            else
            {
                vc.isForCategory = false
                vc.arrDepartmentCategory = NSMutableArray()
            }
            
            //vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            //self.present(vc, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func goToNoramlTextEditor(strText : String) {
        
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "SalesClarkPestiPad" : "SalesClarkPestiPhone", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TextEditorNormal") as! TextEditorNormal
        vc.delegate = self
        vc.strfrom = "SalesInternalNotesStandard"
        vc.strHtml = strText
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)

    }
    func goToNoramlHTMLEditor(strText : String) {
        
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        vc.delegate = self
        vc.strfrom = "SalesServiceDecStandard"
        vc.strHtml = strText
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)

    }

    func getServiceFrequencyOnClick(arrFrequency : NSArray) {
        
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrFrequency)
        
        if arrOfData.count > 0
        {
            if serviceType == .Standard || serviceType == .Bundle
            {
                goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedServiceFrequency, tag: 4, titleHeader: "Service Frequency", isMultiSelection: false, ShowNameKey: "FrequencyName")

            }
            else
            {
                goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedServiceFrequencyCustom, tag: 8, titleHeader: "Service Frequency", isMultiSelection: false, ShowNameKey: "FrequencyName")

            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No service frequency found", viewcontrol: self)
        }
    }
    
    func setDefaultBillingFrequency() {
        
        
        //For Standard Service
        
        arrServiceBillingFrequency = getBillingFrequency()
        
        for item in arrServiceBillingFrequency
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("Monthly") == .orderedSame {
                
                self.arySelectedBillingFrequency = NSMutableArray()
                self.arySelectedBillingFrequency.add(dict)
                dictBillingFrequency = dict
                txtFldBillingFrqStandard.text = "\(dict.value(forKey: "FrequencyName") ?? "")"
                txtFldBillingFreqBundle.text = "\(dict.value(forKey: "FrequencyName") ?? "")"

            }
        }
        
        if txtFldServiceFreqStandard.text == "OneTime" || txtFldServiceFreqStandard.text == "One Time"
        {
            forOneTimeBillingFrequency()
        }
        
        
        //For Custom Service
        
        arrServiceBillingFrequencyCustom = getBillingFrequency()
        
        for item in arrServiceBillingFrequencyCustom
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("Monthly") == .orderedSame {
                
                self.arySelectedBillingFrequencyCustom = NSMutableArray()
                self.arySelectedBillingFrequencyCustom.add(dict)
                txtFldBillingFreqCustom.text = "\(dict.value(forKey: "FrequencyName") ?? "")"
                dictBillingFrequencyCustom = dict
            }
        }
       
        if txtFldServiceFreqCustom.text == "OneTime" || txtFldServiceFreqCustom.text == "One Time"
        {
            forOneTimeBillingFrequency()
        }
        
        
        
       /* if serviceType == .Standard || serviceType == .Bundle
        {
            arrServiceBillingFrequency = getBillingFrequency()
            
            for item in arrServiceBillingFrequency
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("Monthly") == .orderedSame {
                    
                    self.arySelectedBillingFrequency = NSMutableArray()
                    self.arySelectedBillingFrequency.add(dict)
                    txtFldBillingFrqStandard.text = "\(dict.value(forKey: "FrequencyName") ?? "")"
                    txtFldBillingFreqBundle.text = "\(dict.value(forKey: "FrequencyName") ?? "")"

                }
            }
            
            if txtFldServiceFreqStandard.text == "OneTime" || txtFldServiceFreqStandard.text == "One Time"
            {
                forOneTimeBillingFrequency()
            }
        }
        else
        {
            arrServiceBillingFrequencyCustom = getBillingFrequency()
            
            for item in arrServiceBillingFrequencyCustom
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("Monthly") == .orderedSame {
                    
                    self.arySelectedBillingFrequencyCustom = NSMutableArray()
                    self.arySelectedBillingFrequencyCustom.add(dict)
                    txtFldBillingFreqCustom.text = "\(dict.value(forKey: "FrequencyName") ?? "")"
                
                }
            }
           
            if txtFldServiceFreqCustom.text == "OneTime" || txtFldServiceFreqCustom.text == "One Time"
            {
                forOneTimeBillingFrequency()
            }
        }*/
    
    }
    func setDefaultBillingFreqSetupAsPerType(strDefaultBillingFreqType: String, strDefaultBillinfFreqSysName: String)  {
        
        if strDefaultBillingFreqType == "SameAsService"
        {
            if "\(dictServiceFrequency.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                
            }
            else
            {
                for item in arrServiceBillingFrequency
                {
                    let dict = item as! NSDictionary
                    
                    if "\(dict.value(forKey: "SysName") ?? "")" == "\(dictServiceFrequency.value(forKey: "SysName") ?? "")"
                    {
                        var strName = ""
                        strName =  "\(dict.value(forKey: "FrequencyName") ?? "")"
                        self.arySelectedBillingFrequency = NSMutableArray()
                        self.arySelectedBillingFrequency.add(dict)
                        txtFldBillingFrqStandard.text = strName
                        dictBillingFrequency = dict

                        break
                    }
                }
                
            }
        }
        else if strDefaultBillingFreqType == "OtherFrequency"
        {
            if "\(dictServiceFrequency.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                
            }
            else
            {
                for item in arrServiceBillingFrequency
                {
                    let dict = item as! NSDictionary
                    
                    if "\(dict.value(forKey: "SysName") ?? "")" == strDefaultBillinfFreqSysName
                    {
                        var strName = ""
                        strName =  "\(dict.value(forKey: "FrequencyName") ?? "")"
                        self.arySelectedBillingFrequency = NSMutableArray()
                        self.arySelectedBillingFrequency.add(dict)
                        txtFldBillingFrqStandard.text = strName
                        dictBillingFrequency = dict

                        break
                    }
                }
                
            }
        }
        else if strDefaultBillingFreqType == ""
        {
            
        }
        
    }
    
    
    func forOneTimeBillingFrequency() {
        
        if serviceType == .Standard //|| serviceType == .Bundle
        {
            let arrFinal = NSMutableArray()
            
            for item in arrFrequency
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame {
                    
                    self.arySelectedBillingFrequency = NSMutableArray()
                    self.arySelectedBillingFrequency.add(dict)
                    arrFinal.add(dict)
                    txtFldBillingFrqStandard.text = "\(dict.value(forKey: "FrequencyName") ?? "")"
                    arrServiceBillingFrequency = arrFinal as NSArray
                    dictBillingFrequency = dict
                   
                    break
                }
            }
        }
        else if serviceType == .Custom
        {
            let arrFinal = NSMutableArray()
            
            for item in arrFrequency
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame {
                    
                        self.arySelectedBillingFrequencyCustom = NSMutableArray()
                        self.arySelectedBillingFrequencyCustom.add(dict)
                        arrFinal.add(dict)
                        arrServiceBillingFrequencyCustom = arrFinal as NSArray
                        txtFldBillingFreqCustom.text = "\(dict.value(forKey: "FrequencyName") ?? "")"
                        dictBillingFrequencyCustom = dict
                    break
                }
            }
        }
        
    }

    
    func getBillingFrequencyOnClick(arrFrequency : NSArray) {
        
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrFrequency)
        
        if arrOfData.count > 0
        {
            if serviceType == .Standard || serviceType == .Bundle
            {
                goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedBillingFrequency, tag: 5, titleHeader: "Billing Frequency", isMultiSelection: false, ShowNameKey: "FrequencyName")
            }
            else if serviceType == .Custom
            {
                goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedBillingFrequencyCustom, tag: 9, titleHeader: "Billing Frequency", isMultiSelection: false, ShowNameKey: "FrequencyName")
            }
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No billing frequency found", viewcontrol: self)
        }
    }
    
    
    //MARK: ---- Service From Category ------

    func getServiceNameFromCategory(strCategoryName: String) -> NSArray
    {
        var arrServiceData  = NSArray()
        
        for item in arrCategory
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "SysName")!)" == strCategorySysName
            {
                arrServiceData = dict.value(forKey: "Services") as! NSArray
                break
            }
        }
        
        let arrData = NSMutableArray()
        
        for item in arrServiceData
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1"
            {
                arrData.add(dict)
            }
        }
        
        arrServiceData = arrData as NSArray
     
        return arrServiceData
    }
    
    //MARK: ---- Package From Service ------

    func getPackageFromService(ServiceSysName: String) -> NSArray
    {
        var arrServiceData  = NSArray()

        for item in arrService
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "SysName")!)" == strServiceSysName
            {
                if dict.value(forKey: "ServicePackageDcs") is NSArray
                {
                    arrServiceData  = dict.value(forKey: "ServicePackageDcs") as! NSArray
                }
            }
        }
                        
        return arrServiceData
    }
    
    //MARK: ---- Frequency From Package ------

    func getFrequencyFromPackage() -> NSArray
    {
        var arrServiceData  = NSArray()
        var arrServicePackageData  = NSArray()
                
        for item in arrServicePackage
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "ServicePackageId")!)" == strPackageId
            {
                arrServicePackageData = dict.value(forKey: "ServicePackageDetails") as! NSArray
                break
            }
        }
    
        let arrFreqPackage = NSMutableArray()
        
        for item in arrServicePackageData
        {
            let dict = item as! NSDictionary
            
            for itemFreq in arrFrequency
            {
                let dictFreq = itemFreq as! NSDictionary
                
                if "\(dictFreq.value(forKey: "FrequencyId") ?? "")" ==  "\(dict.value(forKey: "FrequencyId") ?? "")"
                {
                    arrFreqPackage.add(dictFreq)
                }
            }
        }
        
        arrServiceData = arrFreqPackage.mutableCopy() as! NSArray
        
        return arrServiceData
    }
    
    func getInitialPriceAndMaintPrice(dictPackage: NSDictionary, strFreqId: String) -> (InitialPrice: String, MaintPrice: String) {
        
        var initialPrice = "0.0"
        var maintPrice = "0.0"
        
        if dictPackage.value(forKey: "ServicePackageDetails") is NSArray
        {
            let arrPackageDetail = dictPackage.value(forKey: "ServicePackageDetails") as! NSArray
            
            for item in arrPackageDetail
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "FrequencyId") ?? "")" == strFreqId
                {
                    initialPrice = stringToFloat(strValue: "\(dict.value(forKey: "PackageCost") ?? "")")
                    maintPrice = stringToFloat(strValue: "\(dict.value(forKey: "PackageMaintCost") ?? "")")
                    strMinimumInitialPrice = stringToFloat(strValue: "\(dict.value(forKey: "MinPackageCost") ?? "")")
                    strMinimumMaintPrice = stringToFloat(strValue: "\(dict.value(forKey: "MinPackageMaintCost") ?? "")")
                    break
                }
            }
        }
        
        return (initialPrice, maintPrice)
    }
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
    func setTextZeroValue()  {
        txtFldInitialPriceStandard.text = getTrimmedString(strText: "")
        txtFldMaintPriceStandard.text = getTrimmedString(strText: "")
        txtFldDiscountStandard.text = getTrimmedString(strText: "")
        txtFldDiscountPerStandard.text = getTrimmedString(strText: "")
    }
    //MARK: ---- Paramter Based Service Code ------
    func getParameterBasedFreq(strParaServiceSysName : String) -> NSArray {
        
        var arrParaFreq = NSArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "ServicePricingSetup") is NSArray){
                
                let arrTempServiceFreq = (dictMaster.value(forKey: "ServicePricingSetup") as! NSArray).filter({ (dict) -> Bool in
                    return "\((dict as! NSDictionary).value(forKey: "IsActive") ?? "")".caseInsensitiveCompare("1") == .orderedSame || "\((dict as! NSDictionary).value(forKey: "IsActive") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                }) as NSArray
                

                arrParaFreq = arrTempServiceFreq.filter({ (dict) -> Bool in
                    return "\((dict as! NSDictionary).value(forKey: "ServiceSysName") ?? "")" == strParaServiceSysName
                }) as NSArray
            }
        }
        
        let arrMutable = NSMutableArray()
        for item in arrParaFreq {
            let dictItem = item as! NSDictionary
            
            for itemFreq in arrFrequencyAll { //arrFrequency
                let dict = itemFreq as! NSDictionary
                if "\(dict.value(forKey: "SysName") ?? "")" == "\(dictItem.value(forKey: "FrequencySysName") ?? "")"
                {
                    arrMutable.add(dict)
                }
            }
            
        }
        arrParaFreq = arrMutable as NSArray
        
        return arrParaFreq
        
    }
    
    func getActiveArray(arrayInput : NSArray) -> NSArray {
        
        let activeArray = arrayInput.filter { (dict) -> Bool in
            return "\((dict as! NSDictionary).value(forKey: "IsActive") ?? "")" == "1" || "\((dict as! NSDictionary).value(forKey: "IsActive") ?? "")".caseInsensitiveCompare("true") == .orderedSame
        } as NSArray
        return activeArray
    }
    
    func getParamterBasesInitialPriceAndMaintPrice() -> (InitialPrice: String, MaintPrice: String) {
        
        var arrParaService = NSArray()
        var initialPrice = "0.0"
        var maintPrice = "0.0"
        
        for item in arrCategory
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "SysName")!)" == strCategorySysName
            {
                arrParaService = dict.value(forKey: "Services") as! NSArray
                arrParaService = getActiveArray(arrayInput: arrParaService)
                arrParaService = arrParaService.filter({ (dict) -> Bool in
                    return "\((dict as! NSDictionary).value(forKey: "SysName") ?? "")" == strServiceSysName
                }) as NSArray
                break
                
            }
        }
        
        arrServiceAdditionalParaDCS = NSMutableArray()
        let arrServiceAdditionalParaDCSDefaultTrue  = NSMutableArray()
        
        for item in arrParaService {
            
            let dict = item as! NSDictionary
            
            if dict.value(forKey: "ServiceParametersDcs") is NSArray
            {
                let arrServiceParametersDcs  = dict.value(forKey: "ServiceParametersDcs") as! NSArray
                
                for itemNew in arrServiceParametersDcs
                {
                    let dictNew = itemNew as! NSDictionary
                    
                    if "\(dictNew.value(forKey: "IsDefault") ?? "")" == "0"
                    {
                        arrServiceAdditionalParaDCS.add(dictNew)
                    }
                    else
                    {
                        arrServiceAdditionalParaDCSDefaultTrue .add(dictNew)
                    }
                }
            }
        }
        
        var tempZillow = false
        
        for item in getServicePricingSetupDetailDCSArray()  {
            
            let dictItemDetailDcs = item as! NSDictionary
            
            if dictItemDetailDcs.value(forKey: "ServicePricingParameterDcs") is NSArray
            {
                var arrServicePricingParameterDcs = dictItemDetailDcs.value(forKey: "ServicePricingParameterDcs") as! NSArray
                tempZillow = false
                arrServicePricingParameterDcs = getActiveArray(arrayInput: arrServicePricingParameterDcs)
                
                for item2 in getValidPropertyNameToBeCompare(arrAdditionalParaDCSDefaultTrue: arrServiceAdditionalParaDCSDefaultTrue) {
                    
                    let strServicePropertySysName = "\(item2 )"

                    //if strServicePropertySysName.caseInsensitiveCompare("area") == .orderedSame &&  (strArea as NSString).floatValue != 0
                    
                        for item3 in arrServicePricingParameterDcs
                        {
                            let dictItem3 = item3 as! NSDictionary
                            let strPricingPropertySysName = "\(dictItem3.value(forKey: "ParameterSysName") ?? "")"
                            let strRangeFrom = "\(dictItem3.value(forKey: "RangeFrom") ?? "")"
                            let strRangeTo = "\(dictItem3.value(forKey: "RangeTo") ?? "")"

                            
                            if strServicePropertySysName.caseInsensitiveCompare(strPricingPropertySysName) == .orderedSame
                            {
                                if strPricingPropertySysName.caseInsensitiveCompare("area") == .orderedSame && strArea.count > 0
                                {
                                    
                                    if (((strArea as NSString).floatValue > (strRangeFrom as NSString).floatValue)||(strArea as NSString).floatValue == (strRangeFrom as NSString).floatValue) && ((strArea as NSString).floatValue < (strRangeTo as NSString).floatValue)||(strArea as NSString).floatValue == (strRangeTo as NSString).floatValue
                                    {
                                        tempZillow = true
                                    }
                                    else
                                    {
                                        tempZillow = false
                                    }
                                    
                                    break
                                }
                                else if strPricingPropertySysName.caseInsensitiveCompare("noOfBedroom") == .orderedSame && strBedroom.count > 0
                                {
                                    
                                    if ((strBedroom as NSString).floatValue < (strRangeTo as NSString).floatValue)||((strBedroom as NSString).floatValue == (strRangeTo as NSString).floatValue)
                                    {
                                        tempZillow = true
                                    }
                                    else
                                    {
                                        tempZillow = false
                                        
                                    }
                                    
                                    break
                                }
                                else if strPricingPropertySysName.caseInsensitiveCompare("noOfBathroom") == .orderedSame && strBathroom.count > 0
                                {
                                    if ((strBathroom as NSString).floatValue < (strRangeTo as NSString).floatValue)||((strBathroom as NSString).floatValue == (strRangeTo as NSString).floatValue)
                                    {
                                        tempZillow = true
                                    }
                                    else
                                    {
                                        tempZillow = false

                                    }
                                    break
                                }
                                else if strPricingPropertySysName.caseInsensitiveCompare("LinearSqFt") == .orderedSame && strLinearSqFt.count > 0
                                {
                                    if (((strLinearSqFt as NSString).floatValue > (strRangeFrom as NSString).floatValue)||(strLinearSqFt as NSString).floatValue == (strRangeFrom as NSString).floatValue) && ((strLinearSqFt as NSString).floatValue < (strRangeTo as NSString).floatValue)||(strLinearSqFt as NSString).floatValue == (strRangeTo as NSString).floatValue
                                    {
                                        tempZillow = true
                                    }
                                    else
                                    {
                                        tempZillow = false

                                    }
                                    break
                                }
                                else if strPricingPropertySysName.caseInsensitiveCompare("LotSize") == .orderedSame && strLotsize.count > 0
                                {
                                    if (((strLotsize as NSString).floatValue > (strRangeFrom as NSString).floatValue)||(strLotsize as NSString).floatValue == (strRangeFrom as NSString).floatValue) && ((strLotsize as NSString).floatValue < (strRangeTo as NSString).floatValue)||(strLotsize as NSString).floatValue == (strRangeTo as NSString).floatValue
                                    {
                                        tempZillow = true
                                    }
                                    else
                                    {
                                        tempZillow = false

                                    }
                                    break
                                }
                                else if strPricingPropertySysName.caseInsensitiveCompare("NoOfStory") == .orderedSame && strNoStory.count > 0
                                {
                                    if ((strNoStory as NSString).floatValue < (strRangeTo as NSString).floatValue)||((strNoStory as NSString).floatValue == (strRangeTo as NSString).floatValue)
                                    {
                                        tempZillow = true
                                    }
                                    else
                                    {
                                        tempZillow = false

                                    }
                                    break
                                }
                                else if strPricingPropertySysName.caseInsensitiveCompare("turfArea") == .orderedSame && strTurfArea.count > 0
                                {
                                    if (((strTurfArea as NSString).floatValue > (strRangeFrom as NSString).floatValue)||(strTurfArea as NSString).floatValue == (strRangeFrom as NSString).floatValue) && ((strTurfArea as NSString).floatValue < (strRangeTo as NSString).floatValue)||(strTurfArea as NSString).floatValue == (strRangeTo as NSString).floatValue
                                    {
                                        tempZillow = true
                                    }
                                    else
                                    {
                                        tempZillow = false

                                    }
                                    break
                                }
                                else if strPricingPropertySysName.caseInsensitiveCompare("shrubArea") == .orderedSame && strShrubArea.count > 0
                                {
                                    if (((strShrubArea as NSString).floatValue > (strRangeFrom as NSString).floatValue)||(strShrubArea as NSString).floatValue == (strRangeFrom as NSString).floatValue) && ((strShrubArea as NSString).floatValue < (strRangeTo as NSString).floatValue)||(strShrubArea as NSString).floatValue == (strRangeTo as NSString).floatValue
                                    {
                                        tempZillow = true
                                    }
                                    else
                                    {
                                        tempZillow = false

                                    }
                                    break
                                }
                            }
                            
                            
                        }
                    
                    if tempZillow == false
                    {
                        break
                    }
                    
                
                }
                
                initialPrice = stringToFloat(strValue: "\(dictItemDetailDcs.value(forKey: "InitialPrice") ?? "")")
                maintPrice =  stringToFloat(strValue: "\(dictItemDetailDcs.value(forKey: "MaintenancePrice") ?? "")")
                
                if tempZillow == true
                {
                    if "\(dictItemDetailDcs.value(forKey: "IsTBD") ?? "")" == "1"
                    {
                        tempZillow = false
                        break
                    }
                    break
                }
                
            }
            
        }
        
        if tempZillow == true
        {
        }
        else
        {
            initialPrice = "TBD"
            maintPrice = "TBD"
        }
       
        return (initialPrice, maintPrice)
    }
    func getValidPropertyNameToBeCompare(arrAdditionalParaDCSDefaultTrue : NSMutableArray) -> NSArray
    {
        isParameterExist = false
        
        let arrZillowSysName = NSMutableArray()
        
        for item in arrAdditionalParaDCSDefaultTrue
        {
            let dict = item as! NSDictionary
            
            let strPropertyName = "\(dict.value(forKey:"SysName") ?? "")"
            
            if strPropertyName.caseInsensitiveCompare("area") == .orderedSame &&  strArea.count > 0//(strArea as NSString).floatValue != 0
            {
                arrZillowSysName.add(strPropertyName)
                isParameterExist = true
            }
            else if strPropertyName.caseInsensitiveCompare("noOfBedroom") == .orderedSame && strBedroom.count > 0//(strBedroom as NSString).floatValue != 0
            {
                arrZillowSysName.add(strPropertyName)
                isParameterExist = true

            }
            else if strPropertyName.caseInsensitiveCompare("noOfBathroom") == .orderedSame &&   strBathroom.count > 0//(strBathroom as NSString).floatValue != 0
            {
                arrZillowSysName.add(strPropertyName)
                isParameterExist = true

            }
            else if strPropertyName.caseInsensitiveCompare("LotSize") == .orderedSame &&  strLotsize.count > 0//(strLotsize as NSString).floatValue != 0
            {
                arrZillowSysName.add(strPropertyName)
                isParameterExist = true

            }
            else if strPropertyName.caseInsensitiveCompare("linearSqFt") == .orderedSame &&  strLinearSqFt.count > 0//(strLinearSqFt as NSString).floatValue != 0
            {
                arrZillowSysName.add(strPropertyName)
                isParameterExist = true

            }
            else if strPropertyName.caseInsensitiveCompare("noOfStory") == .orderedSame &&  strNoStory.count > 0//(strNoStory as NSString).floatValue != 0
            {
                arrZillowSysName.add(strPropertyName)
                isParameterExist = true

            }
            else if strPropertyName.caseInsensitiveCompare("turfArea") == .orderedSame &&  strTurfArea.count > 0//(strTurfArea as NSString).floatValue != 0
            {
                arrZillowSysName.add(strPropertyName)
                isParameterExist = true

            }
            else if strPropertyName.caseInsensitiveCompare("shrubArea") == .orderedSame &&  strShrubArea.count > 0//(strShrubArea as NSString).floatValue != 0
            {
                arrZillowSysName.add(strPropertyName)
                isParameterExist = true

            }
            else
            {
                isParameterExist = false
                break
            }
            
        }
        return arrZillowSysName as NSArray
    }
    func getServicePricingSetupDetailDCSArray() -> NSArray {
        
        var arrPricing = NSArray()

        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "ServicePricingSetup") is NSArray){
                
                var arrPricingTemp = NSArray()
                arrPricingTemp = getActiveArray(arrayInput: dictMaster.value(forKey: "ServicePricingSetup") as! NSArray)
                arrPricing = arrPricingTemp.filter({ (dict) -> Bool in
                    return ("\((dict as! NSDictionary).value(forKey: "ServiceSysName") ?? "")" == strServiceSysName) && "\((dict as! NSDictionary).value(forKey: "FrequencySysName") ?? "")" == strFreqencySysName
                }) as NSArray
            }
        }
        
        var dictObject = NSDictionary()
        
        if arrPricing.count > 0
        {
            dictObject = arrPricing.object(at: 0) as! NSDictionary
            arrPricing = dictObject.value(forKey: "ServicePricingDetailDcs") as! NSArray
            arrPricing = getActiveArray(arrayInput: arrPricing)
        }
        else
        {
            arrPricing = NSArray()
        }
        
        return arrPricing
    }
    
    
    
    //MARK: ---- Data Base Methods ------

    func saveStandardServiceToCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var arrAdditionalPara = NSMutableArray()
        
        var strIntialPrice = ""
        var strMaintPrice = ""
        var strDescription = ""
        var strInternalNotes = ""
        var isTBD = "false"

        var strFinalUnitIntialPrice = ""
        var strFinalUnitMaintPrice = ""
        var strTotalIntialPrice = ""
        var strTotalMaintPrice = ""

        var strUnit = ""
        var strDiscount = ""
        var strDiscountPer = ""

        
        strIntialPrice = getTrimmedString(strText: txtFldInitialPriceStandard.text ?? "")
        strMaintPrice = getTrimmedString(strText: txtFldMaintPriceStandard.text ?? "")
        strDiscount = getTrimmedString(strText: txtFldDiscountStandard.text ?? "")
        strDiscountPer = getTrimmedString(strText: txtFldDiscountPerStandard.text ?? "")
        strUnit = getTrimmedString(strText: txtFldUnitStandard.text ?? "")
        strSoldServiceStandardId = global.getReferenceNumberNew()
        
        if strIntialPrice == "TBD"
        {
           isTBD = "true"
        }
        else
        {
            isTBD = "false"
        }
        
        //Unit Base Price Calculation
        
        var finalUnitBaseInitialPrice = 0.0
        var finalUnitBaseMaintPrice = 0.0

        finalUnitBaseInitialPrice = ((strUnit as NSString).doubleValue) * ((strIntialPrice as NSString).doubleValue)
        finalUnitBaseMaintPrice = ((strUnit as NSString).doubleValue) * ((strMaintPrice as NSString).doubleValue)
        
        strFinalUnitIntialPrice = stringToFloat(strValue: "\(finalUnitBaseInitialPrice)")
        strFinalUnitMaintPrice = stringToFloat(strValue: "\(finalUnitBaseMaintPrice)")

        //For Total Price Calculation
        //Total Paramprice need to calculate
        arrAdditionalPara = NSMutableArray()
        if isParameterBasedService
        {
            for item in arrServiceAdditionalParaDCS
            {
                let dict = item as! NSDictionary
                var arrOfKeyPara = NSArray()
                var arrOfValuePara = NSArray()
                arrOfKeyPara = ["SoldServiceStandardId",
                             "AdditionalParameterName",
                             "UnitName",
                             "AddiParamSysname",
                             "Unit",
                             "MinUnitPrice",
                             "InitialUnitPrice",
                             "MaintUnitPrice",
                             "FinalInitialUnitPrice",
                             "FinalMaintUnitPrice",
                             "IsInitalPricechange",
                             "IsMaintPricechange"]
                
                
                arrOfValuePara = [strSoldServiceStandardId,
                                  "\(dict.value(forKey: "Name") ?? "")",
                                  "\(dict.value(forKey: "UnitName") ?? "")",
                                  "\(dict.value(forKey: "SysName") ?? "")",
                                  "0",
                                  stringToFloat(strValue: "\(dict.value(forKey: "MinimumPrice") ?? "")"),
                                  stringToFloat(strValue: "\(dict.value(forKey: "InitialUnitPrice") ?? "")"),
                                  stringToFloat(strValue: "\(dict.value(forKey: "MaintUnitPrice") ?? "")"),
                                  stringToFloat(strValue: ""),
                                  stringToFloat(strValue: ""),
                                  "0",
                                  "0"]
                
                let dict_Para = NSDictionary.init(objects: arrOfValuePara as! [Any], forKeys: arrOfKeyPara as! [NSCopying])
                arrAdditionalPara.add(dict_Para)
            }
        }
        if !isUnitBasedService
        {
            strUnit = "1"
        }

        strTotalIntialPrice = stringToFloat(strValue: "\(((strUnit as NSString).doubleValue) * ((strIntialPrice as NSString).doubleValue))")
        strTotalInitialPriceSatandard = strTotalIntialPrice
        strTotalMaintPrice = stringToFloat(strValue: "\(((strUnit as NSString).doubleValue) * ((strMaintPrice as NSString).doubleValue))")
        //
        
        strDescription = "\(dictService.value(forKey: "Description") ?? "")"//lblServiceDescStandard.text ?? ""
        strInternalNotes = "" //lblInternalNotesStandard.text ?? ""
        
        
        if "\(dictServiceFrequency.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            strMaintPrice = "0.00"
            strFinalUnitMaintPrice = "0.00"
            strTotalMaintPrice = "0.00"
        }
        
        
        
        isChangeServiceDesc = false
        
        if strDescription == "\(dictService.value(forKey: "Description") ?? "")"
        {
            isChangeServiceDesc = false
        }
        else
        {
            isChangeServiceDesc = true
        }
                
        arrOfKeys = ["additionalParameterPriceDcs",
                     "billingFrequencyPrice",
                     "billingFrequencySysName",
                     "bundleDetailId",
                     "bundleId",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "discount",
                     "discountPercentage",
                     "finalUnitBasedInitialPrice",
                     "finalUnitBasedMaintPrice",
                     "frequencySysName",
                     "initialPrice",
                     "isChangeServiceDesc",
                     "isCommercialTaxable",
                     "isResidentialTaxable",
                     "isSold",
                     "isTBD",
                     "leadId",
                     "maintenancePrice",
                     "modifiedBy",
                     "modifiedDate",
                     "modifiedInitialPrice",
                     "modifiedMaintenancePrice",
                     "packageId",
                     "serviceDescription",
                     "serviceFrequency",
                     "serviceId",
                     "servicePackageName",
                     "serviceSysName",
                     "serviceTermsConditions",
                     "soldServiceStandardId",
                     "totalInitialPrice",
                     "totalMaintPrice",
                     "unit",
                     "userName",
                     "internalNotes",
                     "categorySysName"]
        
        arrOfValues = [arrAdditionalPara,
                       "0",
                       "\(dictBillingFrequency.value(forKey: "SysName") ?? "")",
                       "0",
                       "0",
                       strCompanyKey,
                       strUserName,
                       global.strCurrentDate(),
                       strDiscount,
                       strDiscountPer,
                       strFinalUnitIntialPrice, //finalUnitBasedInitialPrice
                       strFinalUnitMaintPrice, //finalUnitBasedMaintPrice
                       "\(dictServiceFrequency.value(forKey: "SysName") ?? "")",
                       strIntialPrice,
                       isChangeServiceDesc ? "true" : "false",
                       "false",
                       "false",
                       "false",
                       isTBD,
                       strLeadId,
                       strMaintPrice,
                       strUserName,
                       global.modifyDate(),
                       "",
                       "",
                       "\(dictPackage.value(forKey: "ServicePackageId") ?? "")",
                       strDescription,
                       "\(dictServiceFrequency.value(forKey: "FrequencyName") ?? "")",
                       "\(dictService.value(forKey: "ServiceMasterId") ?? "")",
                       "\(dictPackage.value(forKey: "PackageName") ?? "")",
                       "\(dictService.value(forKey: "SysName") ?? "")",
                       "\(dictService.value(forKey: "TermsConditions") ?? "")", //serviceTermsConditions
                       strSoldServiceStandardId,
                       strTotalIntialPrice,
                       strTotalMaintPrice,
                       strUnit,
                       strUserName,
                       strInternalNotes,
                       strCategorySysName]
        
        saveDataInDB(strEntity: "SoldServiceStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    func saveBundleServiceToCoreData()
    {
        if dictBundle.value(forKey: "ServiceBundleDetails") is NSArray
        {
            let arrBundleDetail = dictBundle.value(forKey: "ServiceBundleDetails") as! NSArray
                        
//            for item in arrBundleDetail
//            {
            for i in 0..<arrBundleDetail.count
            {
                
                let dictBundleDetail = arrBundleDetail[i] as! NSDictionary
                //let dictBundleDetail = item as! NSDictionary
                
                var arrOfKeys = NSMutableArray()
                var arrOfValues = NSMutableArray()
                
                let arrAdditionalPara = NSMutableArray()
                
                var strIntialPrice = ""
                var strMaintPrice = ""
                var strDescription = ""
                var strInternalNotes = ""
                var isTBD = "false"
                
                var strDiscount = ""
                var strDiscountPer = ""
                
                strIntialPrice = getTrimmedString(strText: "\(dictBundleDetail.value(forKey: "BundleCost") ?? "")")
                strMaintPrice = getTrimmedString(strText: "\(dictBundleDetail.value(forKey: "BundleMaintCost") ?? "")")
                strDiscount = getTrimmedString(strText: "")
                strDiscountPer = getTrimmedString(strText: "")
                dictServiceFrequency = getFrequencyObjectFromIdAndSysNameOld(strId: "", strSysName: "\(dictBundleDetail.value(forKey: "FrequencySysName") ?? "")")
                
                
                
                if strIntialPrice == "TBD"
                {
                    isTBD = "true"
                }
                else
                {
                    isTBD = "false"
                }
                
                strDescription = lblServiceDescStandard.text ?? ""
                strInternalNotes = ""
                
                
                let dictBundleService = getServiceObjectFromIdAndSysName(strId: "\(dictBundleDetail.value(forKey: "ServiceId") ?? "")", strSysName: "") as NSDictionary
                
                arrOfKeys = ["additionalParameterPriceDcs",
                             "billingFrequencyPrice",
                             "billingFrequencySysName",
                             "bundleDetailId",
                             "bundleId",
                             "companyKey",
                             "createdBy",
                             "createdDate",
                             "discount",
                             "discountPercentage",
                             "finalUnitBasedInitialPrice",
                             "finalUnitBasedMaintPrice",
                             "frequencySysName",
                             "initialPrice",
                             "isChangeServiceDesc",
                             "isCommercialTaxable",
                             "isResidentialTaxable",
                             "isSold",
                             "isTBD",
                             "leadId",
                             "maintenancePrice",
                             "modifiedBy",
                             "modifiedDate",
                             "modifiedInitialPrice",
                             "modifiedMaintenancePrice",
                             "packageId",
                             "serviceDescription",
                             "serviceFrequency",
                             "serviceId",
                             "servicePackageName",
                             "serviceSysName",
                             "serviceTermsConditions",
                             "soldServiceStandardId",
                             "totalInitialPrice",
                             "totalMaintPrice",
                             "unit",
                             "userName",
                             "internalNotes",
                             "categorySysName"]
                
                arrOfValues = [arrAdditionalPara,
                               "0",
                               "\(dictBillingFrequency.value(forKey: "SysName") ?? "")",
                               "\(dictBundleDetail.value(forKey: "BundleDetailId") ?? "")",
                               "\(dictBundleDetail.value(forKey: "ServiceBundleId") ?? "")",
                               strCompanyKey,
                               strUserName,
                               global.strCurrentDate(),
                               strDiscount,
                               strDiscountPer,
                               strIntialPrice,
                               strMaintPrice,
                               "\(dictBundleDetail.value(forKey: "FrequencySysName") ?? "")",
                               strIntialPrice,
                               "false",
                               "false",
                               "false",
                               "false",
                               isTBD,
                               strLeadId,
                               strMaintPrice,
                               strUserName,
                               global.modifyDate(),
                               "",
                               "",
                               "",
                               strDescription,
                               "\(dictServiceFrequency.value(forKey: "FrequencyName") ?? "")",
                               "\(dictBundleDetail.value(forKey: "ServiceId") ?? "")",
                               "",
                               "\(dictBundleService.value(forKey: "SysName") ?? "")",
                               "\(dictBundleService.value(forKey: "TermsConditions") ?? "")",
                               global.getReferenceNumberNew() + "\(i)",
                               strIntialPrice,
                               strMaintPrice,
                               "1",
                               strUserName,
                               strInternalNotes,
                               "\(dictBundleDetail.value(forKey: "CategorySysName") ?? "")"]
                
                saveDataInDB(strEntity: "SoldServiceStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }

        }

    }
    
    
    func saveCustomServiceToCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strIntialPrice = ""
        var strMaintPrice = ""
        var strDiscount = ""
        var strDiscountPer = ""

        var strDescription = ""
        var strInternalNotes = ""
        var strTermsCondition = ""
        
        
        strIntialPrice = getTrimmedString(strText: txtFldInitialPriceCustom.text ?? "")
        strMaintPrice = getTrimmedString(strText: txtFldMaintPriceCustom.text ?? "")
        strDiscount = getTrimmedString(strText: txtFldDiscountCustom.text ?? "")
        strDiscountPer = getTrimmedString(strText: txtFldDiscountPerCustom.text ?? "")

        
        strDescription = strHTMLServiceDescriptionCustom//lblServiceDescCustom.text ?? ""
        strInternalNotes = ""//lblInternalNotesCustom.text ?? ""
        strTermsCondition = strHTMLTermsConditionCustom//lblTermsConditionCustom.text ?? ""
        
        
        if "\(dictServiceFrequencyCustom.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
        {
            strMaintPrice = "0.00"
        }
        
        
        arrOfKeys = ["billingFrequencyPrice",
                     "billingFrequencySysName",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "departmentSysname",
                     "discount",
                     "discountPercentage",
                     "frequencySysName",
                     "initialPrice",
                     "isSold",
                     "leadId",
                     "maintenancePrice",
                     "modifiedBy",
                     "modifiedDate",
                     "modifiedInitialPrice",
                     "modifiedMaintenancePrice",
                     "nonStdServiceTermsConditions",
                     "serviceDescription",
                     "serviceFrequency",
                     "serviceName",
                     "soldServiceNonStandardId",
                     "userName",
                     "internalNotes",
                     "isTaxable"]
        
        arrOfValues = ["",
                       "\(dictBillingFrequencyCustom.value(forKey: "SysName") ?? "")",
                       strCompanyKey,
                       strUserName,
                       global.strCurrentDate(),
                       "\(dictDepartment.value(forKey: "SysName") ?? "")",
                       strDiscount,
                       strDiscountPer,
                       "\(dictServiceFrequencyCustom.value(forKey: "SysName") ?? "")",
                       strIntialPrice,
                       "false",
                       strLeadId,
                       strMaintPrice,
                       strUserName,
                       global.modifyDate(),
                       strIntialPrice,
                       strMaintPrice,
                       strTermsCondition,
                       strDescription,
                       "\(dictServiceFrequencyCustom.value(forKey: "FrequencyName") ?? "")",
                       txtFldServiceNameCustom.text ?? "",
                       global.getReferenceNumberNew(),
                       strUserName,
                       strInternalNotes,
                       "false"]
        
        saveDataInDB(strEntity: "SoldServiceNonStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    
    func updateServiceDetail()
    {
        if serviceType == .Standard
        {
            var strIntialPrice = ""
            var strMaintPrice = ""
            var strFinalUnitIntialPrice = ""
            var strFinalUnitMaintPrice = ""
            var strTotalIntialPrice = ""
            var strTotalMaintPrice = ""

            var strUnit = ""
            var strDiscount = ""
            var strDiscountPer = ""

            var strServiceDescription = String()
            
            strIntialPrice = getTrimmedString(strText: txtFldInitialPriceStandard.text ?? "")
            strMaintPrice = getTrimmedString(strText: txtFldMaintPriceStandard.text ?? "")
            strDiscount = getTrimmedString(strText: txtFldDiscountStandard.text ?? "")
            strDiscountPer = getTrimmedString(strText: txtFldDiscountPerStandard.text ?? "")
            
            if isCouponApplied
            {
                strDiscount = ""
                strDiscountPer = ""
            }
            
            strUnit = getTrimmedString(strText: txtFldUnitStandard.text ?? "")

            strServiceDescription = "\(dictService.value(forKey: "Description") ?? "")" //lblServiceDescStandard.text ?? ""
            
            if strServiceDescription == "\(dictService.value(forKey: "Description") ?? "")"
            {
                isChangeServiceDesc = false
            }
            else
            {
                isChangeServiceDesc = true
            }
        
            
            if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1" || "\(dictService.value(forKey: "IsUnitBasedService") ?? "")".caseInsensitiveCompare("true") == .orderedSame
            {
                //Unit Base Price Calculation
                
                var finalUnitBaseInitialPrice = 0.0
                var finalUnitBaseMaintPrice = 0.0

                finalUnitBaseInitialPrice = ((strUnit as NSString).doubleValue) * ((strIntialPrice as NSString).doubleValue)
                finalUnitBaseMaintPrice = ((strUnit as NSString).doubleValue) * ((strMaintPrice as NSString).doubleValue)
                
                strFinalUnitIntialPrice = stringToFloat(strValue: "\(finalUnitBaseInitialPrice)")
                strFinalUnitMaintPrice = stringToFloat(strValue: "\(finalUnitBaseMaintPrice)")
            }
            else
            {
                strFinalUnitIntialPrice = getTrimmedString(strText: "")
                strFinalUnitMaintPrice = getTrimmedString(strText: "")
            }
            
            
            /*if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1" || "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")".caseInsensitiveCompare("true") == .orderedSame
            {
                //For Total Price Calculation
                
                strTotalIntialPrice = stringToFloat(strValue: "\(((strUnit as NSString).doubleValue) * ((strIntialPrice as NSString).doubleValue))")
                strTotalMaintPrice = stringToFloat(strValue: "\(strMaintPrice)")
            }
            else
            {
                //For Total Price Calculation
                
                strTotalIntialPrice = stringToFloat(strValue: "\(((strUnit as NSString).doubleValue) * ((strIntialPrice as NSString).doubleValue))")
                strTotalMaintPrice = stringToFloat(strValue: "\(((strUnit as NSString).doubleValue) * ((strMaintPrice as NSString).doubleValue))")
            }*/
            
            
            var totalInitialPrice = 0.0
            var totalMaintPrice = 0.0
            
            
            if ((strUnit as NSString).doubleValue) > 0
            {
                totalInitialPrice = ((strUnit as NSString).doubleValue) * ((strIntialPrice as NSString).doubleValue)
                totalMaintPrice = ((strUnit as NSString).doubleValue) * ((strMaintPrice as NSString).doubleValue)
            }
            else
            {
                totalInitialPrice = ((strIntialPrice as NSString).doubleValue)
                totalMaintPrice = ((strMaintPrice as NSString).doubleValue)
            }
            
            if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1" || "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")".caseInsensitiveCompare("true") == .orderedSame
            {
                if matchesServiceEdit.value(forKey: "additionalParameterPriceDcs") is NSArray
                {
                    let arrPara = matchesServiceEdit.value(forKey: "additionalParameterPriceDcs") as! NSArray
                    
                    if arrPara.count > 0
                    {
                        for item in arrPara
                        {
                            let dict = item as! NSDictionary
                            
                            totalInitialPrice = totalInitialPrice + ("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).doubleValue
                            totalMaintPrice = totalMaintPrice + ("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).doubleValue

                        }
                    }

                }
            }
            
            strTotalIntialPrice = String(format: "%.5f", totalInitialPrice)//String(format: "%.5f", totalInitialPrice)
            strTotalMaintPrice = String(format: "%.5f", totalMaintPrice)
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            arrOfKeys = [
                "billingFrequencyPrice",
                "billingFrequencySysName",
                "finalUnitBasedInitialPrice",
                "finalUnitBasedMaintPrice",
                "frequencySysName",
                "initialPrice",
                "maintenancePrice",
                "serviceFrequency",
                "totalInitialPrice",
                "totalMaintPrice",
                "internalNotes",
                "categorySysName",
                "isChangeServiceDesc",
                "discount",
                "discountPercentage",//
                "packageId",
                "servicePackageName",
                "unit",
            ]
            
            
            arrOfValues = [
                "0",
                "\(dictBillingFrequency.value(forKey: "SysName") ?? "")",
                strFinalUnitIntialPrice,
                strFinalUnitMaintPrice,
                strFreqencySysName,
                strIntialPrice,
                strMaintPrice,
                strFreqencyName,
                strTotalIntialPrice,
                strTotalMaintPrice,
                "", //\(lblInternalNotesStandard.text ?? "")
                strCategorySysName,
                "false",
                strDiscount,
                strDiscountPer,
                "\(dictPackage.value(forKey: "ServicePackageId") ?? "")",
                "\(dictPackage.value(forKey: "PackageName") ?? "")",
                strUnit]
            
            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,strSoldServiceStandardId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            
            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
            
            //Renewal Update Code
            
            updateRenewalServiceDetail(ServiceObject: dictService, TotalInitialPrice: strTotalIntialPrice, SoldServiceId: strSoldServiceStandardId)
            
        }
        else if serviceType == .Custom
        {
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            var strIntialPrice = ""
            var strMaintPrice = ""
            var strDiscount = ""
            var strDiscountPer = ""

            var strDescription = ""
            var strInternalNotes = ""
            var strTermsCondition = ""
            
            
            strIntialPrice = getTrimmedString(strText: txtFldInitialPriceCustom.text ?? "")
            strMaintPrice = getTrimmedString(strText: txtFldMaintPriceCustom.text ?? "")
            strDiscount = getTrimmedString(strText: txtFldDiscountCustom.text ?? "")
            strDiscountPer = getTrimmedString(strText: txtFldDiscountPerCustom.text ?? "")

            
            strDescription = strHTMLServiceDescriptionCustom//lblServiceDescCustom.text ?? ""
            strInternalNotes = ""//lblInternalNotesCustom.text ?? ""
            strTermsCondition = strHTMLTermsConditionCustom//lblTermsConditionCustom.text ?? ""
            
            
            arrOfKeys = ["billingFrequencyPrice",
                         "billingFrequencySysName",
                         "departmentSysname",
                         "discount",
                         "discountPercentage",
                         "frequencySysName",
                         "initialPrice",
                         "maintenancePrice",
                         "modifiedInitialPrice",
                         "modifiedMaintenancePrice",
                         "nonStdServiceTermsConditions",
                         "serviceDescription",
                         "serviceFrequency",
                         "serviceName",
                         "internalNotes",
                         "isTaxable"]
            
            arrOfValues = ["",
                           "\(dictBillingFrequencyCustom.value(forKey: "SysName") ?? "")",
                           "\(dictDepartment.value(forKey: "SysName") ?? "")",
                           strDiscount,
                           strDiscountPer,
                           "\(dictServiceFrequencyCustom.value(forKey: "SysName") ?? "")",
                           strIntialPrice,
                           strMaintPrice,
                           strIntialPrice,
                           strMaintPrice,
                           strTermsCondition,
                           strDescription,
                           "\(dictServiceFrequencyCustom.value(forKey: "FrequencyName") ?? "")",
                           txtFldServiceNameCustom.text ?? "",
                           strInternalNotes,
                           "false"]

            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@",strLeadId, strSoldServiceNonStandardId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)


            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
        }
    }
    
    func getRenewalObject(ServiceId strServiceId: String) -> NSDictionary {
        
        var dictObject = NSDictionary()
        var arrObject = NSArray()

        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "DiscountSetupMasterCoupon") is NSArray){
                
                let arr = dictMaster.value(forKey: "DiscountSetupMasterCoupon") as! NSArray
                arrObject = arr.filter { (dict) -> Bool in
                    
                    return "\((dict as! NSDictionary).value(forKey: "DiscountCode") ?? "")".caseInsensitiveCompare(strServiceId) == .orderedSame
                } as NSArray
            }
        }
        if arrObject.count > 0
        {
            dictObject = arrObject.object(at: 0) as! NSDictionary
        }
        
        return dictObject
    }
    
    func updateRenewalServiceDetail(ServiceObject dictService : NSDictionary , TotalInitialPrice strInitialPrice : String, SoldServiceId strSoldServiceStandardId: String)
    {
        let arrRenewal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId = %@", strLeadId, strSoldServiceStandardId))
                
        if arrRenewal.count > 0
        {
            var strRenewalAmount = ""

            if dictService.value(forKey: "ServiceMasterRenewalPrices") is NSArray
            {
                let arrRenewaNaster = dictService.value(forKey: "ServiceMasterRenewalPrices") as! NSArray
                
                let arr = arrRenewaNaster.filter { (dict) -> Bool in
                    
                    return "\((dict as! NSDictionary).value(forKey: "ServiceMasterId") ?? "")".caseInsensitiveCompare("\(dictService.value(forKey: "ServiceMasterId") ?? "")") == .orderedSame
                } as NSArray

                if arr.count > 0
                {
                    let dictRenewalData = arr.object(at: 0) as! NSDictionary
                    if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "Percentage"
                    {
                        var price = Float()
                        var per = Float()
                        per = Float("\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")") ?? 0.0
                        price = (Float(strInitialPrice) ?? 0.0 ) * per / 100
                        
                        if price < Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
                        {
                            price = Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
                        }
                        
                        strRenewalAmount = String(format: "%.2f", price)
                        
                    }
                    else if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "FlatPrice"
                    {
                        strRenewalAmount = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "0.00")"
                    }
                    
                }

            }
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            arrOfKeys = [
                
                "renewalAmount",
            ]
            
            arrOfValues = [
                strRenewalAmount,
            ]
            
            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId, strSoldServiceStandardId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            
            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
            

        }
    }
    
    
    func chkserviceExistOrNot() -> Bool
    {
        var chkExist = Bool()
        
        chkExist = false
        
        let arryOfWorkOrderData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ AND serviceSysName == %@", strLeadId,strServiceSysName))
        
        if arryOfWorkOrderData.count > 0
        {
            
            chkExist = true
            
        }
        else
        {
            chkExist = false
            
        }
        
        return chkExist
    }
    
    // MARK: ------ Manage Coupon on Edit Serivce ------------
    
    func fetchCouponAppliedOnService(soldServiceId : String)  {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && soldServiceId == %@", strUserName, strLeadId, soldServiceId))
        
        if arryOfData.count > 0
        {
            isCouponApplied = true
        }
        else
        {
            isCouponApplied = false
        }
        
    }
    
    func deleteCouponDetail(matches : NSManagedObject)  {
        
        
        let strSoldServiceId = "\(matches.value(forKey: "soldServiceStandardId") ?? "")"

        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && soldServiceId = %@", self.strUserName, self.strLeadId,strSoldServiceId))
        
        if arryOfData.count > 0
        {
            for item in arryOfData
            {
                let objMatch = item as! NSManagedObject
                deleteDataFromDB(obj: objMatch)
            }
        }
    
        self.view.endEditing(true)
        
    }
    func getTotalServicePriceAndDiscount(matchesObject: NSManagedObject) -> (totalInitialPrice : Float, discount : Float)  {
        
        var isUnitBased = false
        var isParameterBased = false
        
        let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matchesObject.value(forKey: "serviceSysName") ?? "")")

        if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1"
        {
            isUnitBased = true
        }
        else
        {
            isUnitBased = false
        }
        
        if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1"
        {
            isParameterBased = true
        }
        else
        {
            isParameterBased = false
        }
        
        var unit = 1.0
        
        if isUnitBased
        {
            unit = Double(("\(matchesObject.value(forKey: "unit") ?? "")" as NSString).floatValue)
        }
        
        //For Parameter based service
        
        var sumParaInitialPrice = 0.0, sumParaMaintPrice = 0.0

        if isParameterBased
        {
            let arrPara = matchesObject.value(forKey: "additionalParameterPriceDcs") as! NSArray
            
            if arrPara.count > 0
            {
                for item in arrPara
                {
                    let dict = item as! NSDictionary
                    let priceInitial = "\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")"
                    let priceMaint = "\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")"

                    sumParaInitialPrice = sumParaInitialPrice + (priceInitial as NSString).doubleValue
                    sumParaMaintPrice = sumParaMaintPrice + (priceMaint as NSString).doubleValue

                }
            }
        }
        
        let finalInitialPrice = (("\(matchesObject.value(forKey: "initialPrice") ?? "")" as NSString).floatValue * Float(unit) ) + Float(sumParaInitialPrice)
        
        let discount = ("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).floatValue
        
        return (finalInitialPrice,discount)
        
    }
    func updateServiceDetailForDiscount(discountObject : NSDictionary, strAppliedAmount: String , matchesObject : NSManagedObject, isAddCoupon: Bool) {
        
        var discount = 0.0
        var discountPer = 0.0
        
        discount = Double(("\(matchesObject.value(forKey: "discount") ?? "")" as NSString).floatValue) - Double(getTotalCouponDiscountOnService(strSoldServiceId: "\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"))

        discountPer = (discount * 100) / Double(getTotalServicePriceAndDiscount(matchesObject: matchesServiceEdit).totalInitialPrice)

        if discount < 0
        {
            discount = 0
            discountPer = 0
        }
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = [
            "discount",
            "discountPercentage"
        ]
        
        
        arrOfValues = [
            stringToFloat(strValue: "\(discount)"),
            stringToFloat(strValue: "\(discountPer)")
        ]
        
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,"\(matchesObject.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        
        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
        
    }
    func getTotalCouponDiscountOnService(strSoldServiceId : String) -> Double {
        
        let arrayDataCoupon = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId == %@ AND soldServiceId = %@", self.strLeadId, strSoldServiceId))

        var totalDiscount = 0.0
        
        for item in arrayDataCoupon
        {
            let matches = item as! NSManagedObject
            totalDiscount = totalDiscount + Double(("\(matches.value(forKey: "appliedDiscount") ?? "")" as NSString).floatValue)
        }
        
        return totalDiscount
    }
    
    
    //MARK: ---------------- Get Category, Service and Frequency Object Methods --------------------
    
    
    func getCategoryObjectFromSysName(strSysName: String) -> NSDictionary
    {
        var dictObject = NSDictionary()

        for itemCategory in arrCategory
        {
            let dictCategory = itemCategory as! NSDictionary
            
            if strSysName == "\(dictCategory.value(forKey: "SysName") ?? "")"
            {
                dictObject = dictCategory
                break
            }
        }
        
        return dictObject
    }
    
    func getFrequencyObjectFromIdAndSysNameOld(strId : String, strSysName: String) -> NSDictionary {
       
        var dictObject = NSDictionary()
        
        var strVal = ""
        
        if !strId.isEmpty
        {
            strVal = strId
        }
        if !strSysName.isEmpty
        {
            strVal = strSysName
        }
        
        for item in arrFrequency
        {
            let dict = item as! NSDictionary
            
            if strId.count > 0
            {
                if "\(dict.value(forKey: "FrequencyId") ?? "")" == strVal
                {
                    dictObject = dict
                    break
                }
            }
            else
            {
                if "\(dict.value(forKey: "SysName") ?? "")" == strVal
                {
                    dictObject = dict
                    break
                }
            }
            
            
            
        }
        return dictObject
    }
    
    func getServiceObjectFromIdAndSysName(strId : String, strSysName: String) -> NSDictionary {
       
        var dictObject = NSDictionary()
        
        var strVal = ""
        
        if !strId.isEmpty
        {
            strVal = strId
        }
        if !strSysName.isEmpty
        {
            strVal = strSysName
        }
        
        for item in arrCategory
        {
            let dict = item as! NSDictionary
            
            if dict.value(forKey: "Services") is NSArray
            {
                let arrService = dict.value(forKey: "Services") as! NSArray
                
                for itemService in arrService
                {
                    let dictService = itemService as! NSDictionary
                    
                    if strId.count > 0
                    {
                        if strVal == "\(dictService.value(forKey: "ServiceMasterId") ?? "")"
                        {
                            dictObject = dictService
                            
                            break
                        }
                    }
                    else
                    {
                        if strVal == "\(dictService.value(forKey: "SysName") ?? "")"
                        {
                            dictObject = dictService
                            
                            break
                        }
                    }
                    
                    
                }
            }
        }
        
        return dictObject
    }
    
    // MARK: ------------------- Edit Service Detail -------------------
    
    func editServiceDetail()
    {
        strSoldServiceStandardId = ""
        strSoldServiceNonStandardId = ""
        

        if serviceType == .Standard
        {
            btnSaveStandard.setTitle("Update", for: .normal)

            btnCustom.isEnabled = false
            btnCategoryStandard.isEnabled = false
            txtFldCategoryStandard.isEnabled = false
            btnServiceStandard.isEnabled = false
            txtFldServiceStandard.isEnabled = false

            
            print("For Standard \(matchesServiceEdit)")
            
            strSoldServiceStandardId = "\(matchesServiceEdit.value(forKey: "soldServiceStandardId") ?? "")"
            strCategorySysName = "\(matchesServiceEdit.value(forKey: "categorySysName") ?? "")"
            dictCategory = getCategoryObjectFromSysName(strSysName: strCategorySysName)
            
            if strCategorySysName == ""
            {
                dictCategory = getCategoryObjectFromServiceSysName(strServiceSysName: "\(matchesServiceEdit.value(forKey: "serviceSysName") ?? "")")
                strCategorySysName = "\(dictCategory.value(forKey: "SysName") ?? "")"
            }
        
            strCategoryName = "\(dictCategory.value(forKey: "Name") ?? "")"
            txtFldCategoryStandard.text = "\(dictCategory.value(forKey: "Name") ?? "")"
            arySelectedCategory = NSMutableArray()
            arySelectedCategory.add(dictCategory)
            
            
            strServiceSysName = "\(matchesServiceEdit.value(forKey: "serviceSysName") ?? "")"
            dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: strServiceSysName)
            strServiceId = "\(dictService.value(forKey: "ServiceMasterId") ?? "")"
            strServiceName = "\(dictService.value(forKey: "Name") ?? "")"
            txtFldServiceStandard.text = "\(dictService.value(forKey: "Name") ?? "")"
            arySelectedService = NSMutableArray()
            arySelectedService.add(dictService)
            arrService = getServiceNameFromCategory(strCategoryName: strCategorySysName)

            
            
            strPackageId = "\(matchesServiceEdit.value(forKey: "packageId") ?? "")"
            strPackageName = "\(matchesServiceEdit.value(forKey: "servicePackageName") ?? "")"
            txtFldServicePackageStandard.text = "\(matchesServiceEdit.value(forKey: "servicePackageName") ?? "")"
            dictPackage = getPackageObjectFromPackageId(strServiceSysName: strServiceSysName, strPackageId: "\(matchesServiceEdit.value(forKey: "packageId") ?? "")")
            arySelectedServicePackage = NSMutableArray()
            arySelectedServicePackage.add(dictPackage)
            arrServicePackage = getPackageFromService(ServiceSysName: strServiceSysName)

            
            
            
            strFreqencySysName = "\(matchesServiceEdit.value(forKey: "frequencySysName") ?? "")"
            strFreqencyName = "\(matchesServiceEdit.value(forKey: "serviceFrequency") ?? "")"
            txtFldServiceFreqStandard.text = "\(matchesServiceEdit.value(forKey: "serviceFrequency") ?? "")"
            dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesServiceEdit.value(forKey: "frequencySysName") ?? "")")
            arySelectedServiceFrequency = NSMutableArray()
            arySelectedServiceFrequency.add(dictServiceFrequency)

            dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesServiceEdit.value(forKey: "billingFrequencySysName") ?? "")")
            txtFldBillingFrqStandard.text = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
            arySelectedBillingFrequency = NSMutableArray()
            arySelectedBillingFrequency.add(dictBillingFrequency)
            
            
            txtFldInitialPriceStandard.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "initialPrice") ?? "")")
            txtFldMaintPriceStandard.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "maintenancePrice") ?? "")")
            
            let strDiscount = handleNegativeValue(strValue: "\(matchesServiceEdit.value(forKey: "discount") ?? "")")
            txtFldDiscountStandard.text = stringToFloat(strValue: strDiscount)
            
            let strDiscountPer = handleNegativeValue(strValue: "\(matchesServiceEdit.value(forKey: "discountPercentage") ?? "")")

            txtFldDiscountPerStandard.text = stringToFloat(strValue: strDiscountPer)

            
            let strDesc = "\(matchesServiceEdit.value(forKey: "serviceDescription") ?? "")"
            
            if strDesc.count == 0
            {
                lblServiceDescStandard.text = ""
            }
            else
            {
//                let data = Data("\(strDesc)".utf8)
//
//                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                {
//                    lblServiceDescStandard.attributedText = attributedString
//                }
                
                self.lblServiceDescStandard.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)

            }
            
            lblInternalNotesStandard.text = "\(matchesServiceEdit.value(forKey: "internalNotes") ?? "")"
            
            for item in arrCategory
            {
                let dict = item as! NSDictionary
                
                if dict.value(forKey: "Services") is NSArray
                {
                    let arrServiceTemp = dict.value(forKey: "Services") as! NSArray
                    
                    for itemService in arrServiceTemp
                    {
                        let dictServiceNew = itemService as! NSDictionary
                        
                        if "\(dictServiceNew.value(forKey: "SysName" ) ?? "")" == "\(matchesServiceEdit.value(forKey: "serviceSysName") ?? "")"
                        {
                            strCategoryName = "\(dict.value(forKey: "Name") ?? "")"
                            strCategorySysName = "\(dict.value(forKey: "SysName") ?? "")"

                            break
                        }
                    }
                }
            }
            txtFldCategoryStandard.text = strCategoryName
            
            
            if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1" || "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")".caseInsensitiveCompare("true") == .orderedSame
            {
                isParameterBasedService = true
                viewServicePackageStandard.isHidden = true
            }
            else
            {
                isParameterBasedService = false
                viewServicePackageStandard.isHidden = false
            }
            
            if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1" || "\(dictService.value(forKey: "IsUnitBasedService") ?? "")".caseInsensitiveCompare("true") == .orderedSame
            {
                isUnitBasedService = true
                viewUnitStandard.isHidden = false
                txtFldUnitStandard.text = "\(matchesServiceEdit.value(forKey: "unit") ?? "")"

            }
            else
            {
                isUnitBasedService = false
                viewUnitStandard.isHidden = true
                txtFldUnitStandard.text = "\(matchesServiceEdit.value(forKey: "unit") ?? "")"
            }
            
            if "\(dictServiceFrequency.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
               
                txtFldMaintPriceStandard.isHidden = true
                txtFldMaintPriceStandard.text = ""
            }
            else
            {
                txtFldMaintPriceStandard.isHidden = false
            }
            
            //For Coupon
            
            fetchCouponAppliedOnService(soldServiceId: strSoldServiceStandardId)
            
        }
        else if serviceType == .Custom
        {
            //Show Custom View
            const_LblLine_L.constant = btnCustom.frame.origin.x
            
            btnCustom.setTitleColor(UIColor.appThemeColor, for: .normal)

            btnStandard.setTitleColor(UIColor.black, for: .normal)
            
            serviceType = .Custom
            
            setServiceView()
            
            //End
            
            btnSaveCustom.setTitle("Update", for: .normal)
            
            btnStandard.isEnabled = false
            
            print("For Non Standard \(matchesServiceEdit)")
            
            strSoldServiceNonStandardId = "\(matchesServiceEdit.value(forKey: "soldServiceNonStandardId") ?? "")"

            strDeptSysName = "\(matchesServiceEdit.value(forKey: "departmentSysname") ?? "")"
            
            for item in arrDepartment
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SysName") ?? "")" == strDeptSysName
                {
                    //btnDepartmentNonStandard.setTitle("\(dict.value(forKey: "Name") ?? "")", for: .normal)
                    txtFldDeptNameCustom.text = "\(dict.value(forKey: "Name") ?? "")"
                    dictDepartment = dict
                    break;
                }
            }
            arySelectedDepartment = NSMutableArray()
            arySelectedDepartment.add(dictDepartment)
            
            
            strFreqencySysNameNonStan = "\(matchesServiceEdit.value(forKey: "frequencySysName") ?? "")"
            
            strFreqencyNameNonStan = "\(matchesServiceEdit.value(forKey: "serviceFrequency") ?? "")"
            
            txtFldServiceFreqCustom.text = "\(matchesServiceEdit.value(forKey: "serviceFrequency") ?? "")"
            
            
            txtFldServiceNameCustom.text = "\(matchesServiceEdit.value(forKey: "serviceName") ?? "")"
            
            
            txtFldInitialPriceCustom.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "initialPrice") ?? "")")
            
            txtFldMaintPriceCustom.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "maintenancePrice") ?? "")")
            
            txtFldDiscountCustom.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "discount") ?? "")")
            txtFldDiscountPerCustom.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "discountPercentage") ?? "")")
            
             
            strHtmlDescriptionNonStan = "\(matchesServiceEdit.value(forKey: "serviceDescription") ?? "")"
            
            strHTMLServiceDescriptionCustom = strHtmlDescriptionNonStan
            
            if strHtmlDescriptionNonStan.count == 0
            {
                lblServiceDescCustom.text = ""
            }
            else
            {
//                let data = Data("\(strHtmlDescriptionNonStan)".utf8)
//
//                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                {
//                    lblServiceDescCustom.attributedText = attributedString
//                }
                
                lblServiceDescCustom.attributedText = getAttributedHtmlStringUnicode(strText: strHtmlDescriptionNonStan)

            }
            
            
            lblInternalNotesCustom.text = "\(matchesServiceEdit.value(forKey: "internalNotes") ?? "")"

            strHtmlTermsConditionNonStan = "\(matchesServiceEdit.value(forKey: "nonStdServiceTermsConditions") ?? "")"
            strHTMLTermsConditionCustom = strHtmlTermsConditionNonStan

            
            if strHtmlTermsConditionNonStan.count == 0
            {
                lblTermsConditionCustom.text = ""
            }
            else
            {
//                let data = Data("\(strHtmlTermsConditionNonStan)".utf8)
//
//                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                {
//                    lblTermsConditionCustom.attributedText = attributedString
//                }
                
                lblTermsConditionCustom.attributedText = getAttributedHtmlStringUnicode(strText: strHtmlTermsConditionNonStan)


            }
            
            
            dictBillingFrequencyCustom = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesServiceEdit.value(forKey: "billingFrequencySysName") ?? "")")
            dictServiceFrequencyCustom = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesServiceEdit.value(forKey: "frequencySysName") ?? "")")
            
            arySelectedServiceFrequencyCustom = NSMutableArray()
            arySelectedServiceFrequencyCustom.add(dictServiceFrequencyCustom)
            arySelectedBillingFrequencyCustom = NSMutableArray()
            arySelectedBillingFrequency.add(dictBillingFrequencyCustom)

        }
    }
    
    // MARK: - ----------------Renewal Service Method --------------------
    
    func addRenewalService()
    {
        
        if !checkRenewalServiceExistence(strRenewalServiceId: strServiceId)
        {
            var chkRenewalStatus = Bool()
            chkRenewalStatus = chkRenewalService(strCategorySysName: strCategorySysName, strServiceSysName: strServiceSysName)
            
            if arrServiceMasterRenewalPrices.count > 0 && chkRenewalStatus == true
            {
                let dict = arrServiceMasterRenewalPrices.object(at: 0) as! NSDictionary
                        
                saveRenewalServiceInToCoreData(dictRenewalData: dict, TotalInitialPrice: strTotalInitialPriceSatandard, ServiceId: strSoldServiceStandardId, ServiceMasterId: strServiceId)
                
            }
            
        }

    }
    func checkRenewalServiceExistence(strRenewalServiceId : String) -> Bool
    {
        var chkRenewalServiceExist = false
        
        let arrayData = getDataFromLocal(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@", "serviceId == %@", strLeadId, strRenewalServiceId))
        
        if arrayData.count > 0
        {
            chkRenewalServiceExist = true
        }
        
        return chkRenewalServiceExist
    }

    func chkRenewalService(strCategorySysName : String, strServiceSysName: String) -> Bool
    {
        
        var isRenwal = Bool()
        isRenwal = false
        
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary

            if dictMaster.value(forKey: "Categories") is NSArray
            {
                let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
               
                for item in arrCategory
                {
                    let dict = item as! NSDictionary
                    
                    if strCategorySysName == "\(dict.value(forKey: "SysName") ?? "")"
                    {
                        if dict.value(forKey: "Services") is NSArray
                        {
                            let arrService = dict.value(forKey: "Services") as! NSArray
                            for itemService in arrService
                            {
                                let dictService = itemService as! NSDictionary
                                
                                if strServiceSysName == "\(dictService.value(forKey: "SysName") ?? "")"
                                {
                                    if "\(dictService.value(forKey: "IsRenewal") ?? "")" == "1" || "\(dictService.value(forKey: "IsRenewal") ?? "")" == "true" || "\(dictService.value(forKey: "IsRenewal") ?? "")" == "True"
                                    {
                                        arrServiceMasterRenewalPrices = dictService.value(forKey: "ServiceMasterRenewalPrices") as! NSArray
                                        
                                        isRenwal = true
                                        
                                        break
                                    }
                                }
                            }
                        }
                    }
                    
                    if isRenwal == true
                    {
                        break
                    }
                    
                }
                
            }
        }
        
        return isRenwal
        
    }
    
    func saveRenewalServiceInToCoreData(dictRenewalData : NSDictionary , TotalInitialPrice strInitialPrice: String, ServiceId strServiceId: String, ServiceMasterId strServiceMasterIdRenewal: String)
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strRenewalAmount = String()
        var strRenewalPercentage = String()
                
        if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "Percentage"
        {
            strRenewalPercentage = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")"
            
            var price = Float()
            var per = Float()
            per = Float("\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")") ?? 0.0
            price = (Float(strInitialPrice) ?? 0.0) * per / 100
            if price < Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
            {
                price = Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
            }
            
            strRenewalAmount = String(format: "%.2f", price)
        }
        else
        {
            strRenewalPercentage = "0.00"
        }
        
        if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "FlatPrice"
        {
            strRenewalAmount = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "0.00")"
        }
       
        arrOfKeys = ["companyKey",
                     "leadId",
                     "renewalAmount",
                     "renewalDescription",
                     "renewalFrequencySysName",
                     "renewalPercentage",
                     "renewalServiceId",
                     "serviceId",
                     "soldServiceStandardId",
                     "userName"]
        
        arrOfValues = [strCompanyKey,
                       strLeadId,
                       strRenewalAmount,
                       "\(dictRenewalData.value(forKey: "Description") ?? "")",
                       "\(dictRenewalData.value(forKey: "FrequencySysName") ?? "")",
                       strRenewalPercentage,
                       strServiceMasterIdRenewal,
                       strServiceMasterIdRenewal,
                       strServiceId,
                       strUserName]
        
        saveDataInDB(strEntity: "RenewalServiceExtDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }


    //MARK: ---------------- Actions --------------------
    
    @IBAction func actionOnBack(_ sender: Any) {
        
        back()
    }
    
    
    @IBAction func actionOnStandard(_ sender: Any) {
        
        const_LblLine_L.constant = 0
        btnStandard.setTitleColor(UIColor.appThemeColor, for: .normal)

        btnCustom.setTitleColor(UIColor.black, for: .normal)
        
        serviceType = .Standard
        
        //setDefaultBillingFrequency()
        setServiceView()
    }
    
    @IBAction func actionOnCustom(_ sender: Any) {
   
        const_LblLine_L.constant = btnCustom.frame.origin.x
        
        btnCustom.setTitleColor(UIColor.appThemeColor, for: .normal)

        btnStandard.setTitleColor(UIColor.black, for: .normal)
        
        serviceType = .Custom
        
        setServiceView()
    }
    
    //MARK: ----- Actions View Standard -----
    
    @IBAction func actionOnCategoryStandard(_ sender: Any) {
        
        endEditing()
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrCategory)
        
        if arrOfData.count > 0
        {

            goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedCategory, tag: 1, titleHeader: "Service Category", isMultiSelection: false, ShowNameKey: "Name")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "No category found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnServiceStandard(_ sender: Any) {
        
        endEditing()
        if strCategorySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertCategory, viewcontrol: self)
            
        }
        else
        {
            var arrOfData = NSMutableArray()
            
            if isPlusService
            {
                arrService = arrPlusService

            }
            else
            {
                arrService = getServiceNameFromCategory(strCategoryName: strCategorySysName)
            }
            arrOfData = NSMutableArray(array: arrService)
            
            if arrOfData.count > 0
            {
                goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedService, tag: 2, titleHeader: "Service", isMultiSelection: false, ShowNameKey: "Name")
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "No service found", viewcontrol: self)
            }
        }
    }
    
    @IBAction func actionOnServicePackageStandard(_ sender: Any) {
        
        endEditing()
        
        if strCategorySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertCategory, viewcontrol: self)
            
        }
        else if strServiceSysName == ""
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertServiceName, viewcontrol: self)
        }
        else
        {
            var arrOfData = NSMutableArray()
            
            arrServicePackage = getPackageFromService(ServiceSysName: strServiceSysName)
            
            arrOfData = NSMutableArray(array: arrServicePackage)

            if arrOfData.count > 0
            {
                goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedServicePackage, tag: 3, titleHeader: "Package", isMultiSelection: false, ShowNameKey: "PackageName")
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "No package found", viewcontrol: self)
            }
        }
    
    }
    
    
    @IBAction func actionOnServiceFreqStandard(_ sender: Any) {
        
        endEditing()

        if strCategorySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertCategory, viewcontrol: self)
            
        }
        else if strServiceSysName == ""
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertServiceName, viewcontrol: self)
        }
        else if txtFldServicePackageStandard.text == "" && !isParameterBasedService
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertPackage, viewcontrol: self)
        }
        else
        {
            if isParameterBasedService
            {
                getServiceFrequencyOnClick(arrFrequency: getParameterBasedFreq(strParaServiceSysName: strServiceSysName))
            }
            else
            {
                getServiceFrequencyOnClick(arrFrequency: getFrequencyFromPackage())

            }

        }
        
    }
    
    @IBAction func actionOnBillingFreqStandard(_ sender: Any) {
        
        endEditing()
        
        getBillingFrequencyOnClick(arrFrequency: arrServiceBillingFrequency)

        

    }
    
    @IBAction func actionOnEditServiceDescStandard(_ sender: Any) {
        
        editHtmlDescType = .StandardServiceDesc
        if let strText = lblServiceDescStandard.text {
            
            goToNoramlHTMLEditor(strText: strText)
        }
    }
    
    @IBAction func actionOnEditInternalNotesStandard(_ sender: Any) {
        
        editNotesType = .Standard

        if let strText = lblInternalNotesStandard.text {
            
            goToNoramlTextEditor(strText: strText)
        }
        
    }
    
    @IBAction func actionOnCancelStandard(_ sender: Any) {
        
        back()
    }
    
    @IBAction func actionOnSaveStandard(_ sender: Any) {
        
        let isProceed = validationForStandardService()
        
        if !isProceed.check
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: isProceed.message, viewcontrol: self)
        }
        else
        {
            if isEditService
            {
                
                if isCouponApplied
                {
                    let alert = UIAlertController(title: Alert, message: alertEditPrice, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in

                        updateServiceDetail()
                        self.deleteCouponDetail(matches: self.matchesServiceEdit)
                        back()
                    }))
                    alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                        
                    }))

                    alert.popoverPresentationController?.sourceView = self.view
                    self.present(alert, animated: true, completion: {
                    })
                }
                else
                {
                    updateServiceDetail()
                }
                
            }
            else
            {
                
                if isPlusService
                {
                    
                    let stParentServiceFreq = "\(objParentForPlusService.value(forKey: "frequencySysName") ?? "")"
                    let stParentBillingFreq = "\(objParentForPlusService.value(forKey: "billingFrequencySysName") ?? "")"
                    
                    
                    if "\(dictServiceFrequency.value(forKey: "SysName") ?? "")" == stParentServiceFreq && "\(dictBillingFrequency.value(forKey: "SysName") ?? "")" == stParentBillingFreq
                    {
                        
                        saveStandardServiceToCoreData()
                        addRenewalService()

                    }
                    else
                    {
                        let alert = UIAlertController(title: Alert, message: alertPlusService, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in
                            
                            saveStandardServiceToCoreData()
                            addRenewalService()
                            back()
                        }))
                        
                        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                            
                        }))

                        alert.popoverPresentationController?.sourceView = self.view
                        self.present(alert, animated: true, completion: {
                        })
                        
                    }

                }
                else
                {
                    saveStandardServiceToCoreData()
                    addRenewalService()
                }
                

            }
            Global().updateSalesModifydate(strLeadId as String)
            NotificationCenter.default.post(name: Notification.Name("updateTab"), object: "Standard")

            back()
        }
        
    }
    func validationForStandardService() -> (check: Bool, message: String) {
        
    
        var check = true
        var message = String()
        let strInitialPrice = txtFldInitialPriceStandard.text ?? ""
        let strMaintPrice = txtFldMaintPriceStandard.text ?? ""
        let strUnit = txtFldUnitStandard.text ?? ""
        let strDiscount = txtFldDiscountStandard.text ?? ""
        let strCategory = txtFldCategoryStandard.text ?? ""
        let strService = txtFldServiceStandard.text ?? ""
        let strPackage = txtFldServicePackageStandard.text ?? ""
        let strServiceFrequency = txtFldServiceFreqStandard.text ?? ""
        let strBillingFrequency = txtFldBillingFrqStandard.text ?? ""
        
        var totalInitialPrice = 0.0
        
        if (strUnit as NSString).doubleValue > 0
        {
            totalInitialPrice = ((strInitialPrice as NSString).doubleValue) * ((strUnit as NSString).doubleValue)
        }
        else
        {
            totalInitialPrice = (strInitialPrice as NSString).doubleValue
        }
        
        var totalMaintPrice = 0.0
        
        if (strUnit as NSString).doubleValue > 0
        {
            totalMaintPrice = ((strMaintPrice as NSString).doubleValue) * ((strUnit as NSString).doubleValue)
        }
        else
        {
            totalMaintPrice = (strMaintPrice as NSString).doubleValue
        }

        
        if strCategory == "" || strCategory.count == 0 {
            
            message = "\(alertCategory)"
            
            check = false
        }
        else if strService == "" || strService.count == 0 {
            
            message = "\(alertServiceName)"
            
            check = false
        }
        else if (strPackage == "" || strPackage.count == 0) &&  isParameterBasedService == false {
            
            message = "\(alertPackage)"
            
            check = false
        }
        else if strServiceFrequency == "" || strServiceFrequency.count == 0 {
            
            message = "\(alertServiceFrequency)"
            
            check = false
        }
        else if strBillingFrequency == "" || strBillingFrequency.count == 0 {
            
            message = "\(alertBillingFrequency)"
            
            check = false
        }
        else if strInitialPrice == "" || strInitialPrice.count == 0 {
            
            message = "\(alertInitialPrice)"
            
            check = false
        }
        
        else if (strDiscount as NSString).doubleValue > totalInitialPrice {
            
            message = "\(alertInitialPriceForDiscount)"
            
            check = false
        }
        
        else if totalInitialPrice < (strMinimumInitialPrice as NSString).doubleValue {
            
            message = "\(alertMinimumInitialPrice)\(strMinimumInitialPrice)"
            
            check = false
        }
        else if totalMaintPrice < (strMinimumMaintPrice as NSString).doubleValue && (strServiceFrequency != "OneTime")
        {
            
            message =  "\(alertMinimumMaintPrice)\(strMinimumMaintPrice)"
            check = false

        }
        else if (strUnit == "" || (strUnit as NSString).floatValue == 0) && isUnitBasedService == true
        {
            message =  "\(alertUnit)"
            check = false
            
        }
        else if chkserviceExistOrNot() && isEditService == false
        {
            message =  "\(alertServiceAlreadyExist)"
            check = false
        }
    
        return (check,message)
    }
    
    //MARK: ----- Actions View Bundle -----
    
    @IBAction func actionOnBundle(_ sender: Any) {
        //ServiceBundles
        
        serviceType = .Bundle
        endEditing()
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrBundle)
        
        if arrOfData.count > 0
        {

            goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedBundle, tag: 7, titleHeader: "Service Bundle", isMultiSelection: false, ShowNameKey: "BundleName")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No bundle found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnBillingFreqBundle(_ sender: Any) {
        
        endEditing()
        getBillingFrequencyOnClick(arrFrequency: arrFrequency)
        
    }
    
    @IBAction func actionOnCancelBundle(_ sender: Any) {
        
        back()
    }
    
    @IBAction func actionOnSaveBundle(_ sender: Any) {
        
        let isProceed = validationForBundle()
        
        if !isProceed.check
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: isProceed.message, viewcontrol: self)
        }
        else
        {
            saveBundleServiceToCoreData()
            Global().updateSalesModifydate(strLeadId as String)
            NotificationCenter.default.post(name: Notification.Name("updateTab"), object: "Standard")

            back()
        }
    }
    
  
    func validationForBundle() -> (check: Bool, message: String) {
        
        var check = true
        var message = String()
      

        let strBundle = txtFldBundleName.text ?? ""
        let strBillingFrequency = txtFldBillingFreqBundle.text ?? ""
        
        
        if strBundle == "" || strBundle.count == 0 {
            
            message = "\(alertBundle)"
            
            check = false
        }
        else if strBillingFrequency == "" || strBillingFrequency.count == 0 {
            
            message = "\(alertBillingFrequency)"
            
            check = false
        }
        
        
        return (check,message)
    }
        
        
        var check = true
    //MARK: ----- Actions View Custom -----

    @IBAction func actionOnDeptNameCustom(_ sender: Any) {
        
        endEditing()
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrDepartment)
        
        if arrOfData.count > 0
        {

            goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedDepartment, tag: 6, titleHeader: "Department", isMultiSelection: false, ShowNameKey: "Name")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No department found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnServiceFreqCustom(_ sender: Any) {
        
        endEditing()
        getServiceFrequencyOnClick(arrFrequency: arrFrequency)
    }
    
    @IBAction func actionOnBillingFreqCustom(_ sender: Any) {
        
        endEditing()
        getBillingFrequencyOnClick(arrFrequency: arrServiceBillingFrequencyCustom)
        
    }
    
    @IBAction func actionOnEditServiceDescCustom(_ sender: Any) {
        
        editHtmlDescType = .CustomServiceDesc
        
//        if let strText = lblServiceDescCustom.text {
//
//            goToNoramlHTMLEditor(strText: strText)
//        }
        
        goToNoramlHTMLEditor(strText: strHTMLServiceDescriptionCustom)

    }
    
    @IBAction func actionOnEditInternalNotesCustom(_ sender: Any) {
        
        editNotesType = .Custom

        if let strText = lblInternalNotesCustom.text {
            
            goToNoramlTextEditor(strText: strText)
        }
    }
    
    @IBAction func actionOnEditTermsConditionCustom(_ sender: Any) {
        
        editHtmlDescType = .CustomTermsCondition
        
        
//        if let strText = lblTermsConditionCustom.text {
//
//            goToNoramlHTMLEditor(strText: strText)
//        }
        
        goToNoramlHTMLEditor(strText: strHTMLTermsConditionCustom)

    }
    
    @IBAction func actionOnCancelCustom(_ sender: Any) {
        
        back()
    }
    
    @IBAction func actionOnSaveCustom(_ sender: Any) {
        
        let isProceed = validationForCustomService()
        
        if !isProceed.check
        {
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: isProceed.message, viewcontrol: self)
        }
        else
        {
            if isEditService
            {
                updateServiceDetail()
            }
            else
            {
                saveCustomServiceToCoreData()
            }
            Global().updateSalesModifydate(strLeadId as String)
            NotificationCenter.default.post(name: Notification.Name("updateTab"), object: "Custom")

            back()
        }
    }
    
    func validationForCustomService() -> (check: Bool, message: String) {
        
        
        var check = true
        var message = String()
        let strDepartmentName = txtFldDeptNameCustom.text ?? ""
        let strServiceFrequency = txtFldServiceFreqCustom.text ?? ""
        let strBillingFrequency = txtFldBillingFreqCustom.text ?? ""
        let strServiceName = txtFldServiceNameCustom.text ?? ""
        let strInitialPrice = txtFldInitialPriceCustom.text ?? ""
        let strDiscount = txtFldDiscountCustom.text ?? ""
        
        if strDepartmentName == "" || strDepartmentName.count == 0 {
            
            message = "\(alertDepartment)"
            
            check = false
        }
        else if strServiceFrequency == "" || strServiceFrequency.count == 0 {
            
            message = "\(alertServiceFrequency)"
            
            check = false
        }
        else if strBillingFrequency == "" || strBillingFrequency.count == 0 {
            
            message = "\(alertBillingFrequency)"
            
            check = false
        }
        else if strServiceName == "" || strServiceName.count == 0 {
            
            message = "\(alertServiceName)"
            
            check = false
        }
        else if strInitialPrice == "" || strInitialPrice.count == 0 {
            
            message = "\(alertInitialPrice)"
            
            check = false
        }
        
        else if (strDiscount as NSString).floatValue > (strInitialPrice as NSString).floatValue {
            
            message = "\(alertInitialPriceForDiscount)"
            
            check = false
        }
    
        return (check,message)
    }
}


// MARK: - ---------------------------- Extensions ----------------------------



// MARK: - -------------- SalesNew_SelectionProtocol --------------

extension NewSales_AddServiceVC: SalesNew_SelectionProtocol
{
  
    func didSelect(aryData: NSMutableArray, tag: Int) {
        //-----Tag------
        //-----Category------
        if(tag == 2){
            if(aryData.count == 0){
                arrCategory = NSMutableArray()
                txtFldCategoryStandard.text = ""
            }else{
                var strName = ""
                arrCategory = NSMutableArray()
                arrCategory = aryData.mutableCopy() as! NSMutableArray
                
                for item in aryData{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtFldCategoryStandard.text = strName
            }
        }
        
    }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        
        
        guard dictData.count > 0 else {
            return
        }
        
        // Category
        if tag == 1
        {
            if(dictData.count == 0){
                txtFldCategoryStandard.text = ""
            }else{
                var strName = ""
                dictService = dictData
                strName = "\(strName),\((dictData as AnyObject).value(forKey: "Name")!)"
                self.arySelectedCategory = NSMutableArray()
                self.arySelectedCategory.add(dictData)
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                dictCategory = dictData
                strCategorySysName = "\(dictData.value(forKey: "SysName") ?? "")"
                txtFldCategoryStandard.text = strName
            }
            txtFldServiceStandard.text = ""
            strServiceSysName = ""
            
            txtFldServicePackageStandard.text = ""
            strPackageId = ""
            
            txtFldServiceFreqStandard.text = ""
            strFreqencySysName = ""
            
            arySelectedService = NSMutableArray()
            arySelectedServicePackage = NSMutableArray()
            arySelectedServiceFrequency = NSMutableArray()
            setDefaultBillingFrequency()
            setTextZeroValue()

        }
        
        // Service
        else if tag == 2
        {
            if(dictData.count == 0){
                txtFldServiceStandard.text = ""
            }else{
                var strName = ""
                dictService = dictData
                strName = "\(strName),\((dictData as AnyObject).value(forKey: "Name")!)"
                self.arySelectedService = NSMutableArray()
                self.arySelectedService.add(dictData)
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                strServiceSysName = "\(dictService.value(forKey: "SysName") ?? "")"
                strServiceId = "\(dictService.value(forKey: "ServiceMasterId") ?? "")"
                txtFldServiceStandard.text = strName
                lblServiceDescStandard.text = "\(dictService.value(forKey: "Description") ?? "")"
                lblInternalNotesStandard.text = "\(dictService.value(forKey: "Description") ?? "")"
                
                if let strText = lblServiceDescStandard.text
                {
//                    let data = Data("\(strText)".utf8)
//                    if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                    {
//                        lblServiceDescStandard.attributedText = attributedString
//                    }
                    
                    self.lblServiceDescStandard.attributedText = getAttributedHtmlStringUnicode(strText: strText)

                    
                    
                }
                else
                {
                    lblServiceDescStandard.text = ""
                }
                
                if let strText = lblInternalNotesStandard.text
                {
//                    let data = Data("\(strText)".utf8)
//                    if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                    {
//                        lblInternalNotesStandard.attributedText = attributedString
//                    }
                    
                    self.lblInternalNotesStandard.attributedText = getAttributedHtmlStringUnicode(strText: strText)

                    
                    
                }
                else
                {
                    lblInternalNotesStandard.text = ""
                }

                if "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")" == "1" || "\(dictService.value(forKey: "IsParameterizedPriced") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                {
                    isParameterBasedService = true
                    viewServicePackageStandard.isHidden = true
                }
                else
                {
                    isParameterBasedService = false
                    viewServicePackageStandard.isHidden = false
                }
                
                if "\(dictService.value(forKey: "IsUnitBasedService") ?? "")" == "1" || "\(dictService.value(forKey: "IsUnitBasedService") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                {
                    isUnitBasedService = true
                    viewUnitStandard.isHidden = false
                }
                else
                {
                    isUnitBasedService = false
                    viewUnitStandard.isHidden = true
                }
                
            }
            txtFldServicePackageStandard.text = ""
            strPackageId = ""
            arySelectedServicePackage = NSMutableArray()
            
            txtFldServiceFreqStandard.text = ""
            strFreqencySysName = ""
            arySelectedServiceFrequency = NSMutableArray()
            
            setDefaultBillingFrequency()
            setTextZeroValue()
        }
        
        // Package
        else if tag == 3
        {
            if(dictData.count == 0){
                txtFldDeptNameCustom.text = ""
            }else{
                var strName = ""
                dictPackage = dictData
                strName = "\(strName),\((dictData as AnyObject).value(forKey: "PackageName")!)"
                self.arySelectedServicePackage = NSMutableArray()
                self.arySelectedServicePackage.add(dictData)
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                strPackageId = "\(dictPackage.value(forKey: "ServicePackageId") ?? "")"
                strPackageName = "\(dictPackage.value(forKey: "PackageName") ?? "")"

                txtFldServicePackageStandard.text = strName
            }
            txtFldServiceFreqStandard.text = ""
            strFreqencySysName = ""
            arySelectedServiceFrequency = NSMutableArray()
            setDefaultBillingFrequency()
            setTextZeroValue()
        }
        
        // Service Frequency Standard
        else if tag == 4
        {
            if(dictData.count == 0){
                if serviceType == .Standard
                {
                    txtFldServiceFreqStandard.text = ""

                }
                else
                {
                }
            }else{
                
                if serviceType == .Standard
                {
                    var strName = ""
                    dictServiceFrequency = dictData
                    strName = "\(strName),\((dictData as AnyObject).value(forKey: "FrequencyName")!)"
                    self.arySelectedServiceFrequency = NSMutableArray()
                    self.arySelectedServiceFrequency.add(dictData)
                    if strName.count != 0{
                        strName = String(strName.dropFirst())
                    }
                    
                    txtFldServiceFreqStandard.text = strName
                    strFreqencySysName = "\(dictServiceFrequency.value(forKey: "SysName") ?? "")"

                    if isParameterBasedService {
                        
                        
                        let price = getParamterBasesInitialPriceAndMaintPrice()
                        txtFldInitialPriceStandard.text = price.InitialPrice
                        txtFldMaintPriceStandard.text = price.MaintPrice
                        
                        if !isParameterExist
                        {
                            txtFldInitialPriceStandard.text = "TBD"
                            txtFldMaintPriceStandard.text = "TBD"
                        }
                    
                    }
                    else
                    {
                        let price = getInitialPriceAndMaintPrice(dictPackage: dictPackage, strFreqId: "\(dictServiceFrequency.value(forKey: "FrequencyId") ?? "")")
                        txtFldInitialPriceStandard.text = price.InitialPrice
                        txtFldMaintPriceStandard.text = price.MaintPrice
                    }
                    txtFldDiscountStandard.text = getTrimmedString(strText: "")
                    txtFldDiscountPerStandard.text = getTrimmedString(strText: "")
                    
                    
                    if "\(dictServiceFrequency.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                    {
                        forOneTimeBillingFrequency()
                        txtFldMaintPriceStandard.isHidden = true
                        txtFldMaintPriceStandard.text = ""
                    }
                    else
                    {
                        arrServiceBillingFrequency = getBillingFrequency()
                        setDefaultBillingFrequency()
                        txtFldMaintPriceStandard.isHidden = false

                    }
                    
                    //Set Billing Freq As Per Type
                    
                    let strDefaultBillingFreqType = "\(dictService.value(forKey: "DefaultBillingFrequency") ?? "")"
                    let strDefaultBillingFreqSysName = "\(dictService.value(forKey: "DefaultBillingFrequencySysName") ?? "")"
                    
                    setDefaultBillingFreqSetupAsPerType(strDefaultBillingFreqType: strDefaultBillingFreqType, strDefaultBillinfFreqSysName: strDefaultBillingFreqSysName)

                    
                }
                else if serviceType == .Custom
                {
                    
                }
            }
        }
        
        // Service Frequency Custom
        else if tag == 8
        {
            if(dictData.count == 0){
                
                txtFldServiceFreqCustom.text = ""
                
            }else{
                
                if serviceType == .Standard
                {
                    
                }
                else if serviceType == .Custom
                {
                    
                    var strName = ""
                    dictServiceFrequencyCustom = dictData
                    strName = "\(strName),\((dictData as AnyObject).value(forKey: "FrequencyName")!)"
                    self.arySelectedServiceFrequencyCustom = NSMutableArray()
                    self.arySelectedServiceFrequencyCustom.add(dictData)
                    
                    if strName.count != 0{
                        strName = String(strName.dropFirst())
                    }
                
                    txtFldServiceFreqCustom.text = strName
                    
                    if "\(dictServiceFrequencyCustom.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                    {
                        forOneTimeBillingFrequency()
                        txtFldMaintPriceCustom.isHidden = true
                    }
                    else
                    {
                        arrServiceBillingFrequencyCustom = getBillingFrequency()
                        setDefaultBillingFrequency()
                        txtFldMaintPriceCustom.isHidden = false
                    }
                   
                }
            }
        }
        // Billing Frequency Standard
        else if tag == 5
        {
            if(dictData.count == 0){
                
                if serviceType == .Standard
                {
                    txtFldBillingFrqStandard.text = ""
                }
                else if serviceType == .Bundle
                {
                    txtFldBillingFreqBundle.text = ""
                }
            }
            else{
               
                if serviceType == .Standard || serviceType == .Bundle
                {
                    var strName = ""
                    dictBillingFrequency = dictData
                    strName = "\(strName),\((dictData as AnyObject).value(forKey: "FrequencyName")!)"
                    self.arySelectedBillingFrequency = NSMutableArray()
                    self.arySelectedBillingFrequency.add(dictData)
                    if strName.count != 0{
                        strName = String(strName.dropFirst())
                    }
                    if serviceType == .Standard
                    {
                        txtFldBillingFrqStandard.text = strName
                    }
                    else if serviceType == .Bundle
                    {
                        txtFldBillingFreqBundle.text = strName

                    }
                }
                else if serviceType == .Custom
                {
                    
                }
                
            }
        }
        //Billing Freq Custom
        else if tag == 9
        {
            if(dictData.count == 0){
                
                if serviceType == .Custom
                {
                    txtFldBillingFreqCustom.text = ""
                }
                else
                {
                    
                }
            }
            else{
               
                if serviceType == .Standard || serviceType == .Bundle
                {
                }
                else if serviceType == .Custom
                {
                    var strName = ""
                    dictBillingFrequencyCustom = dictData
                    strName = "\(strName),\((dictData as AnyObject).value(forKey: "FrequencyName")!)"
                    self.arySelectedBillingFrequencyCustom = NSMutableArray()
                    self.arySelectedBillingFrequencyCustom.add(dictData)
                    if strName.count != 0{
                        strName = String(strName.dropFirst())
                    }
                    txtFldBillingFreqCustom.text = strName
                }
                
            }
        }
        
        // Department
        else if tag == 6
        {
            if(dictData.count == 0){
                txtFldDeptNameCustom.text = ""
            }else{
                var strName = ""
                dictDepartment = dictData
                strName = "\(strName),\((dictData as AnyObject).value(forKey: "Name")!)"
                self.arySelectedDepartment = NSMutableArray()
                self.arySelectedDepartment.add(dictData)
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtFldDeptNameCustom.text = strName
            }
        }
        
        // Bundle
        else if tag == 7
        {
            if(dictData.count == 0){
                txtFldBundleName.text = ""
            }else{
                var strName = ""
                dictBundle = dictData
                strName = "\(strName),\((dictData as AnyObject).value(forKey: "BundleName")!)"
                self.arySelectedBundle = NSMutableArray()
                self.arySelectedBundle.add(dictData)
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                txtFldBundleName.text = strName
            }
        }
        
    }
}

// MARK: - -------------- Normal Text Editor --------------


extension NewSales_AddServiceVC: TextEditorDelegate
{
    func didReceiveEditorText(strText: String) {
        
        if editNotesType == .Standard
        {
            lblInternalNotesStandard.text = strText
        }
        else if editNotesType == .Custom
        {
            lblInternalNotesCustom.text = strText
        }
    }
    func didReceiveEditorTextWithObject(strText : String, object: NSManagedObject)
    {
        
    }
}

// MARK: - -------------- HTML Text Editor --------------

extension NewSales_AddServiceVC: HTMLEditorDelegate
{
    func didReceiveRenewalHTMLEditorText(strText: String, managedObject: NSManagedObject) {
        
    }
    
    func didReceiveHTMLEditorText(strText: String) {
        
        if editHtmlDescType == .StandardServiceDesc
        {
            if strText.count > 0
            {
//                let data = Data("\(strText)".utf8)
//                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                {
//                    lblServiceDescStandard.attributedText = attributedString
//                }
                
                self.lblServiceDescStandard.attributedText = getAttributedHtmlStringUnicode(strText: strText)

                
            }
            else
            {
                lblServiceDescStandard.text = ""
            }
        }
        else if editHtmlDescType == .CustomServiceDesc
        {
            if strText.count > 0
            {
//                let data = Data("\(strText)".utf8)
//                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//                {
//                    lblServiceDescCustom.attributedText = attributedString
//                }
                
                
//                var attributedStringHTML: NSAttributedString? = nil
//                do {
//                    if let data = strText.data(using: .unicode) {
//                        attributedStringHTML = try NSAttributedString(
//                            data: data,
//                            options: [.documentType: NSAttributedString.DocumentType.html],
//                            documentAttributes: nil)
//                    }
//                } catch {
//                }
                lblServiceDescCustom.attributedText = getAttributedHtmlStringUnicode(strText: strText)
                
                
            }
            else
            {
                lblServiceDescCustom.text = ""
            }
            
            strHTMLServiceDescriptionCustom = strText
            
        }
        else if editHtmlDescType == .CustomTermsCondition
        {
            if strText.count > 0
            {
                /*let data = Data("\(strText)".utf8)
                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                {
                    lblTermsConditionCustom.attributedText = attributedString
                }*/
                
//                var attributedStringHTML: NSAttributedString? = nil
//                do {
//                    if let data = strText.data(using: .unicode) {
//                        attributedStringHTML = try NSAttributedString(
//                            data: data,
//                            options: [.documentType: NSAttributedString.DocumentType.html],
//                            documentAttributes: nil)
//                    }
//                } catch {
//                }
                
                lblTermsConditionCustom.attributedText = getAttributedHtmlStringUnicode(strText: strText)
            }
            else
            {
                lblTermsConditionCustom.text = ""
            }
            
            strHTMLTermsConditionCustom = strText

        }
    }
}

// MARK: --------------------- Text Field Delegate --------------

extension NewSales_AddServiceVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //yesEditedSomething = true
        
        if ( textField == txtFldInitialPriceStandard || textField == txtFldMaintPriceStandard  || textField == txtFldInitialPriceCustom  || textField == txtFldMaintPriceCustom || textField == txtFldUnitStandard || textField == txtFldDiscountStandard || textField == txtFldDiscountPerStandard || textField == txtFldDiscountCustom || textField == txtFldDiscountPerCustom )
        {
                        
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            if textField == txtFldInitialPriceStandard
            {
                if string == ""
                {
                    var initial = 0.0, discountPer = 0.0
                    var strIntialPrice = ""
                    var strDiscount = ""

                    strIntialPrice = ((txtFldInitialPriceStandard.text ?? "") as NSString).replacingCharacters(in: range, with: "")
                    
                    strDiscount = txtFldDiscountStandard.text ?? ""
                    
                    let strUnit = "\(txtFldUnitStandard.text ?? "")"
                    
                    if (strUnit as NSString).doubleValue > 0
                    {
                        initial = (strIntialPrice as NSString).doubleValue * (strUnit as NSString).doubleValue
                    }
                    else
                    {
                        initial = (strIntialPrice as NSString).doubleValue
                    }
                    
                    discountPer = ((strDiscount as NSString).doubleValue * 100)/initial
                    
                    if discountPer < 0 || discountPer < 0
                    {
                        discountPer = 0
                    }
                    txtFldDiscountPerStandard.text = stringToFloat(strValue: "\(discountPer)")
                }
                else
                {
                    var initial = 0.0, discountPer = 0.0
                    var strIntialPrice = ""
                    var strDiscount = ""

                    strIntialPrice = "\(txtFldInitialPriceStandard.text ?? "")" + string
                    
                    strDiscount = txtFldDiscountStandard.text ?? ""
                    
                    let strUnit = "\(txtFldUnitStandard.text ?? "")"
                    
                    if (strUnit as NSString).doubleValue > 0
                    {
                        initial = (strIntialPrice as NSString).doubleValue * (strUnit as NSString).doubleValue
                    }
                    else
                    {
                        initial = (strIntialPrice as NSString).doubleValue
                    }
                    
                    discountPer = ((strDiscount as NSString).doubleValue * 100)/initial
                    
                    if discountPer < 0 || discountPer < 0
                    {
                        discountPer = 0
                    }
                    txtFldDiscountPerStandard.text = stringToFloat(strValue: "\(discountPer)")
                }
                
            }
            else if textField == txtFldDiscountStandard
            {
                if string == ""
                {
                    var initial = 0.0, discountPer = 0.0
                    var strDiscount = ""
                    let strIntialPrice = getTrimmedString(strText: txtFldInitialPriceStandard.text ?? "")
                    
                    let strUnit = "\(txtFldUnitStandard.text ?? "")"
                    
                    if (strUnit as NSString).doubleValue > 0
                    {
                        initial = (strIntialPrice as NSString).doubleValue * (strUnit as NSString).doubleValue
                    }
                    else
                    {
                        initial = (strIntialPrice as NSString).doubleValue
                    }
                    
                    var strText = ""
                    strText = getTrimmedString(strText: txtFldDiscountStandard.text ?? "")
                    strDiscount = (strText as NSString).replacingCharacters(in: range, with: "")
                    discountPer = (((strDiscount as NSString).doubleValue) * 100) / initial
                    txtFldDiscountPerStandard.text = stringToFloat(strValue: "\(discountPer)")
                }
                else
                {
                    var initial = 0.0, discountPer = 0.0
                    var strDiscount = ""
                    let strIntialPrice = getTrimmedString(strText: txtFldInitialPriceStandard.text ?? "")
                    
                    let strUnit = "\(txtFldUnitStandard.text ?? "")"
                    
                    if (strUnit as NSString).doubleValue > 0
                    {
                        initial = (strIntialPrice as NSString).doubleValue * (strUnit as NSString).doubleValue
                    }
                    else
                    {
                        initial = (strIntialPrice as NSString).doubleValue
                    }
                    
                    var strText = ""
                    strText = "\(txtFldDiscountStandard.text ?? "")" + string
                    strDiscount = strText
                    discountPer = (((strDiscount as NSString).doubleValue) * 100) / initial
                    txtFldDiscountPerStandard.text = stringToFloat(strValue: "\(discountPer)")
                }
            }
            else if textField == txtFldDiscountPerStandard
            {
                if string == ""
                {
                    var initial = 0.0, discount = 0.0
                    var strDiscountPer = ""
                    let strIntialPrice = getTrimmedString(strText: txtFldInitialPriceStandard.text ?? "")
                    
                    let strUnit = "\(txtFldUnitStandard.text ?? "")"
                    
                    if (strUnit as NSString).doubleValue > 0
                    {
                        initial = (strIntialPrice as NSString).doubleValue * (strUnit as NSString).doubleValue
                    }
                    else
                    {
                        initial = (strIntialPrice as NSString).doubleValue
                    }
                    
                    var strText = ""
                    strText = getTrimmedString(strText: txtFldDiscountPerStandard.text ?? "")
                    strDiscountPer = (strText as NSString).replacingCharacters(in: range, with: string)
                    discount = initial * ((strDiscountPer as NSString).doubleValue)/100
                    
                    txtFldDiscountStandard.text = stringToFloat(strValue: "\(discount)")
                    
                    
                }
                else
                {
                    var initial = 0.0, discount = 0.0
                    let strIntialPrice = getTrimmedString(strText: txtFldInitialPriceStandard.text ?? "")
                    
                    let strUnit = "\(txtFldUnitStandard.text ?? "")"
                    
                    if (strUnit as NSString).doubleValue > 0
                    {
                        initial = (strIntialPrice as NSString).doubleValue * (strUnit as NSString).doubleValue
                    }
                    else
                    {
                        initial = (strIntialPrice as NSString).doubleValue
                    }
 
                    var strDiscountPer = ""
                    strDiscountPer = "\(txtFldDiscountPerStandard.text ?? "")" + string
                    discount = initial * ((strDiscountPer as NSString).doubleValue)/100
                    txtFldDiscountStandard.text = stringToFloat(strValue: "\(discount)")
                    
                }
            }
            else if textField == txtFldInitialPriceCustom
            {
                if string == ""
                {
                    var initial = 0.0, discountPer = 0.0
                    var strIntialPrice = ""
                    var strDiscount = ""

                    strIntialPrice = ((txtFldInitialPriceCustom.text ?? "") as NSString).replacingCharacters(in: range, with: "")
                    
                    strDiscount = txtFldDiscountCustom.text ?? ""
                    
                    initial = (strIntialPrice as NSString).doubleValue
                    discountPer = ((strDiscount as NSString).doubleValue * 100)/initial
                    
                    if discountPer < 0 || discountPer < 0
                    {
                        discountPer = 0
                    }
                    txtFldDiscountPerCustom.text = stringToFloat(strValue: "\(discountPer)")
                }
                else
                {
                    var initial = 0.0, discountPer = 0.0
                    var strIntialPrice = ""
                    var strDiscount = ""

                    strIntialPrice = "\( txtFldInitialPriceCustom.text ?? "")" + string
                    
                    strDiscount = txtFldDiscountCustom.text ?? ""
                    
                    initial = (strIntialPrice as NSString).doubleValue
                    
                    discountPer = ((strDiscount as NSString).doubleValue * 100)/initial
                    
                    if discountPer < 0 || discountPer < 0
                    {
                        discountPer = 0
                    }
                    txtFldDiscountPerCustom.text = stringToFloat(strValue: "\(discountPer)")
                }
                
            }
            else if textField == txtFldDiscountCustom
            {
                if string == ""
                {
                    var initial = 0.0, discountPer = 0.0
                    var strDiscount = ""
                    let strIntialPrice = getTrimmedString(strText: txtFldInitialPriceCustom.text ?? "")
                    initial = (strIntialPrice as NSString).doubleValue
                    var strText = ""
                    strText = getTrimmedString(strText: txtFldDiscountCustom.text ?? "")
                    strDiscount = (strText as NSString).replacingCharacters(in: range, with: "")
                    discountPer = (((strDiscount as NSString).doubleValue) * 100) / initial
                    txtFldDiscountPerCustom.text = stringToFloat(strValue: "\(discountPer)")
                }
                else
                {
                    var initial = 0.0, discountPer = 0.0
                    var strDiscount = ""
                    let strIntialPrice = getTrimmedString(strText: txtFldInitialPriceCustom.text ?? "")
                    initial = (strIntialPrice as NSString).doubleValue
                    var strText = ""
                    strText = "\(txtFldDiscountCustom.text ?? "")" + string
                    strDiscount = strText
                    discountPer = (((strDiscount as NSString).doubleValue) * 100) / initial
                    txtFldDiscountPerCustom.text = stringToFloat(strValue: "\(discountPer)")
                }
            }
            else if textField == txtFldDiscountPerCustom
            {
                if string == ""
                {
                    var initial = 0.0, discount = 0.0
                    var strDiscountPer = ""
                    let strIntialPrice = getTrimmedString(strText: txtFldInitialPriceCustom.text ?? "")
                    initial = (strIntialPrice as NSString).doubleValue
                    
                    var strText = ""
                    strText = getTrimmedString(strText: txtFldDiscountPerCustom.text ?? "")
                    strDiscountPer = (strText as NSString).replacingCharacters(in: range, with: "")
                    discount = initial * ((strDiscountPer as NSString).doubleValue)/100
                    
                    txtFldDiscountCustom.text = stringToFloat(strValue: "\(discount)")
                }
                else
                {
                    var initial = 0.0, discount = 0.0
                    let strIntialPrice = getTrimmedString(strText: txtFldInitialPriceCustom.text ?? "")
                    initial = (strIntialPrice as NSString).doubleValue
                    var strDiscountPer = ""
                    strDiscountPer = "\(txtFldDiscountPerCustom.text ?? "")" + string
                    discount = initial * ((strDiscountPer as NSString).doubleValue)/100
                    txtFldDiscountCustom.text = stringToFloat(strValue: "\(discount)")

                }
            }
            else if textField == txtFldUnitStandard
            {
                if string == ""
                {
                    var initial = 0.0, discountPer = 0.0
                    var strIntialPrice = ""
                    var strDiscount = ""
                    let strUnit = ("\(txtFldUnitStandard.text ?? "")" as NSString).replacingCharacters(in: range, with: "")

                    strIntialPrice = txtFldInitialPriceStandard.text ?? ""
                    
                    strDiscount = txtFldDiscountStandard.text ?? ""
                    
                    if (strUnit as NSString).doubleValue > 0
                    {
                        initial = (strIntialPrice as NSString).doubleValue * (strUnit as NSString).doubleValue
                    }
                    else
                    {
                        initial = (strIntialPrice as NSString).doubleValue
                    }
                    
                    discountPer = ((strDiscount as NSString).doubleValue * 100)/initial
                    
                    if discountPer < 0 || discountPer < 0
                    {
                        discountPer = 0
                    }
                    txtFldDiscountPerStandard.text = stringToFloat(strValue: "\(discountPer)")
                }
                else
                {
                    var initial = 0.0, discountPer = 0.0
                    var strIntialPrice = ""
                    var strDiscount = ""
                    let strUnit = "\(txtFldUnitStandard.text ?? "")" + string

                    strIntialPrice = "\(txtFldInitialPriceStandard.text ?? "")"
                    
                    strDiscount = txtFldDiscountStandard.text ?? ""
                    
                    if (strUnit as NSString).doubleValue > 0
                    {
                        initial = (strIntialPrice as NSString).doubleValue * (strUnit as NSString).doubleValue
                    }
                    else
                    {
                        initial = (strIntialPrice as NSString).doubleValue
                    }
                    
                    discountPer = ((strDiscount as NSString).doubleValue * 100)/initial
                    
                    if discountPer < 0 || discountPer < 0
                    {
                        discountPer = 0
                    }
                    txtFldDiscountPerStandard.text = stringToFloat(strValue: "\(discountPer)")
                }
                
            }
            
            
            return chk
            
        }
        else
        {
            
            return true
            
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.text == "TBD"
        {
            textField.text = ""
        }
        
        return true
        
    }
    
}
