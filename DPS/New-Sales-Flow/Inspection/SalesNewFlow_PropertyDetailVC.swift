//
//  SalesNewFlow_PropertyDetailVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 03/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNewFlow_PropertyDetailVC: UIViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var txtArea: ACFloatingTextField!
    @IBOutlet weak var txtLinerFt: ACFloatingTextField!
    @IBOutlet weak var txtBedRooms: ACFloatingTextField!
    @IBOutlet weak var txtBathroom: ACFloatingTextField!
    @IBOutlet weak var txtNumberOfStorys: ACFloatingTextField!
    @IBOutlet weak var txtYearBuilt: ACFloatingTextField!
    @IBOutlet weak var txtTrufSq: ACFloatingTextField!
    @IBOutlet weak var txtShrub: ACFloatingTextField!
    @IBOutlet weak var txtSpouseName: ACFloatingTextField!
    @IBOutlet weak var txtLotSize: ACFloatingTextField!
    @IBOutlet weak var txtUSDAHardiness: ACFloatingTextField!
    
    
    @IBOutlet weak var switchVegetableGarden: UISwitch!
    @IBOutlet weak var switchChecmicalAllergies: UISwitch!
    @IBOutlet weak var switchAnimalInTheBackyard: UISwitch!
    var loader = UIAlertController()
    
    //MARK: - Variable
    @objc var matchesGeneralInfo = NSManagedObject()
    var strLeadId = ""
    var isEditInSalesInfo = false
    //MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //-----Navin
        txtArea.keyboardType = .decimalPad
        txtLinerFt.keyboardType = .decimalPad
        txtBedRooms.keyboardType = .numberPad
        txtBathroom.keyboardType = .numberPad
        txtNumberOfStorys.keyboardType = .numberPad
        txtYearBuilt.keyboardType = .numberPad
        txtTrufSq.keyboardType = .decimalPad
        txtShrub.keyboardType = .decimalPad
        txtLotSize.keyboardType = .decimalPad

        //-----End
        
        assignValues()
        
        strLeadId = "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")"
        
        //let strLinearFt = calculateLinerSquareFt(Area: txtArea.text == "" ? "0" : txtArea.text ?? "", NoOfStroy: txtNumberOfStorys.text == "" ? "0" : txtNumberOfStorys.text ?? "")//calculateLinerSquareFt(strArea: (txtArea.text == "" ? "0" : txtArea.text)!, strNoOfStory: (txtNumberOfStorys.text == "" ? "0" : txtNumberOfStorys.text ?? ""))
        
        //txtLinerFt.text = strLinearFt
        
        txtLinerFt.text = "\(matchesGeneralInfo.value(forKey: "linearSqFt") ?? "")"//stringToFloat(strValue: "\(matchesGeneralInfo.value(forKey: "linearSqFt") ?? "")")
        
        
        //let strTurf = calculateTurfArea(LotSize: txtLotSize.text ?? "", Area: txtArea.text ?? "", Story: txtNumberOfStorys.text ?? "", ShrubArea: txtShrub.text ?? "")
        
        //txtTrufSq.text = strTurf
        
        txtTrufSq.text = "\(matchesGeneralInfo.value(forKey: "turfArea") ?? "")"//stringToFloat(strValue: "\(matchesGeneralInfo.value(forKey: "turfArea") ?? "")")

        

    }
    
    //MARK: - IBActions
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnGetPropertyDetailsAction(_ sender: Any) {
        
        if isInternetAvailable()
        {
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            isEditInSalesInfo = true
            GetPropertyDetail()
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertInternet, viewcontrol: self)
        }
      
        
    }
    func validation() -> Bool{
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: txtNumberOfStorys.text!)) else {
                return false
            }
            return true
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if validation() == false{
            self.view.makeToast("Please enter numeric value in # of Stories")
        }
        else{
            if isEditInSalesInfo
            {
                Global().updateSalesModifydate(strLeadId as String)
            }
            SaveInToDb()
        }
       
    }
    //MARK: - Custom Funtions
    
    func assignValues(){
        
        txtArea.text = "\(matchesGeneralInfo.value(forKey: "area") ?? "")"
        txtLotSize.text = "\(matchesGeneralInfo.value(forKey: "lotSizeSqFt") ?? "")"
        txtYearBuilt.text = "\(matchesGeneralInfo.value(forKey: "yearBuilt") ?? "")"
        txtBathroom.text = "\(matchesGeneralInfo.value(forKey: "noOfBathroom") ?? "")"
        txtBedRooms.text = "\(matchesGeneralInfo.value(forKey: "noOfBedroom") ?? "")"
        txtNumberOfStorys.text = "\(matchesGeneralInfo.value(forKey: "noOfStory") ?? "")"
        txtLinerFt.text = "\(matchesGeneralInfo.value(forKey: "linearSqFt") ?? "")"
        txtShrub.text = "\(matchesGeneralInfo.value(forKey: "shrubArea") ?? "")"
        txtTrufSq.text = "\(matchesGeneralInfo.value(forKey: "turfArea") ?? "")"
        
    }
    
    func SaveInToDb(){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        
        arrOfKeys.add("area")
        arrOfKeys.add("lotSizeSqFt")
        arrOfKeys.add("yearBuilt")
        arrOfKeys.add("noOfBathroom")
        arrOfKeys.add("noOfBedroom")
        arrOfKeys.add("NoOfStory")
        arrOfKeys.add("linearSqFt")
        arrOfKeys.add("shrubArea")
        arrOfKeys.add("turfArea")
        
        
        arrOfValues.add(txtArea.text ?? "")
        arrOfValues.add(txtLotSize.text ?? "")
        arrOfValues.add(txtYearBuilt.text ?? "")
        arrOfValues.add(txtBathroom.text ?? "")
        arrOfValues.add(txtBedRooms.text ?? "")
        arrOfValues.add(txtNumberOfStorys.text ?? "")
        arrOfValues.add(txtLinerFt.text ?? "")
        arrOfValues.add(txtShrub.text ?? "")
        arrOfValues.add(txtTrufSq.text ?? "")
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            print("Success")
            self.navigationController?.popViewController(animated: false)
        }
        else {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
    }
    
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
    
    
    func calculateTurfArea(LotSize strLotSize: String, Area strArea: String, Story strNoOfStory: String, ShrubArea strShrubArea: String) -> String {
        
        if strLotSize == "" || strLotSize.count == 0
        {
            return ""
        }
        else
        {
            var turfArea = 0.0
            
            var strStoryMultiplier = "0"
            if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
            {
                let dictSalesLeadMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if dictSalesLeadMaster.value(forKey: "TurfAreaStoryMultiplier") is NSArray
                {
                    let arrTurfArea = dictSalesLeadMaster.value(forKey: "TurfAreaStoryMultiplier") as! NSArray
                    
                    for item in arrTurfArea
                    {
                        let dictTurf = item as! NSDictionary
                        
                        if "\( dictTurf.value(forKey: "NoOfStory") ?? "")" == strNoOfStory
                        {
                            strStoryMultiplier = "\( dictTurf.value(forKey: "Multiplier") ?? "")"
                            break
                        }
                        
                    }

                    turfArea = ((strLotSize as NSString).doubleValue) - (((strArea as NSString).doubleValue * (strStoryMultiplier as NSString).doubleValue) - (strShrubArea as NSString).doubleValue)
                    //        turfArea=[strLotSize floatValue]-(([strArea floatValue]*[strStoryMultiplier floatValue])-[strShrubArea floatValue]);

                    if turfArea < 0
                    {
                        turfArea = 0
                    }
                }
                
            }
            return String(format: "%.2f", turfArea)
        }
        
    }
    
    
    func calculateLinerSquareFt(Area strArea: String, NoOfStroy strNoOfStory: String) -> String {
        
        
        var lineraSqFtValue = 0.0
        
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictSalesLeadMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary

            if dictSalesLeadMaster.value(forKey: "LinearSqFtMasterExtServiceDc") is NSArray
            {
                let arrLinearSqFtMasterExtServiceDc = dictSalesLeadMaster.value(forKey: "LinearSqFtMasterExtServiceDc") as! NSArray
                
                for item in arrLinearSqFtMasterExtServiceDc
                {
                    let dict = item as! NSDictionary
                    
                    let areaFrom = ("\(dict.value(forKey: "AreaFrom") ?? "")" as NSString).floatValue
                    let areaTo =  ("\(dict.value(forKey: "AreaTo") ?? "")" as NSString).floatValue
                    
                    if (areaFrom <= (strArea as NSString).floatValue && (strArea as NSString).floatValue <= areaTo) && (strNoOfStory == "\(dict.value(forKey: "NoOfStory") ?? "")")
                    {
                        lineraSqFtValue = Double(("\(dict.value(forKey: "LinearSqFt") ?? "")" as NSString).floatValue)
                        break
                    }
                    

                }
            }
        }
        
        if lineraSqFtValue > 0
        {
            
        }
        else
        {
            lineraSqFtValue = ("\(txtLinerFt.text ?? "")" as NSString).doubleValue
        }
        
        return String(format: "%.2f", lineraSqFtValue)
        
    }

    //MARK: - API calling
    
    func GetPropertyDetail()
    {
        
        let strKey = "X1-ZWz19zkb2lkumj_7zykg" //@"X1-ZWz1fn2iqwnc3v_5jgqp";
        var strUrl = ""
        var strAddress = ""
        strAddress = "\(matchesGeneralInfo.value(forKey: "servicesAddress1") ?? "")"
        
        //http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=X1-ZWz19zkb2lkumj_7zykg&address=18715%20Rogers%20Lk%20&citystatezip=SanAntonio,TX,78258
        
        var strState = "\(matchesGeneralInfo.value(forKey: "serviceState") ?? "")"
        
        
        if strState.lowercased() == "California".lowercased(){
            strState = "CA"
        }
        
        if strState.lowercased() == "Texas".lowercased(){
            strState = "TX"
        }
        
        
        let strCity = "\(matchesGeneralInfo.value(forKey: "serviceCity") ?? "")"
        let strZipcode = "\(matchesGeneralInfo.value(forKey: "serviceZipcode") ?? "")"
        
        
        let dictRecord = NSMutableDictionary()
        
        dictRecord.setValue(strAddress, forKey: "AddressLine1")
        dictRecord.setValue(strCity, forKey: "City")
        dictRecord.setValue(strState, forKey: "State")
        dictRecord.setValue(strZipcode, forKey: "PostalCode")
        
//        dictRecord.setValue("18715 Rogers Lk", forKey: "AddressLine1")
//        dictRecord.setValue("San Antonio", forKey: "City")
//        dictRecord.setValue("TX", forKey: "State")
//        dictRecord.setValue("70258", forKey: "PostalCode")
        
        let aryRecord = NSMutableArray()
        aryRecord.add(dictRecord)
        let dictData = NSMutableDictionary()
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let apiKeyMellissa = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.MelissaKey")!)"
        
        dictData.setValue(apiKeyMellissa.count == 0 ? "X1-ZWz19zkb2lkumj_7zykg" : apiKeyMellissa, forKey: "CustomerId")
        dictData.setValue("Property V4 - LookupProperty", forKey: "TransmissionReference")
        dictData.setValue("1", forKey: "TotalRecords")
        dictData.setValue("GrpAll", forKey: "Columns")
        dictData.setValue(aryRecord, forKey: "Records")
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictData) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictData, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        print(dictData)
        Global().getServerResponseForSendLead(toServer: "https://property.melissadata.net/v4/WEB/LookupProperty/", requestData, (NSDictionary() as! [AnyHashable : Any]), "", ""){ [self] (success, response, error) in
            
            self.loader.dismiss(animated: false)
            
            if(success)
            {
                if((response as NSDictionary?)?.value(forKey: "Records") is NSArray){
                    
                    var aryRecord = NSMutableArray()
                    aryRecord = ((response as NSDictionary?)?.value(forKey: "Records") as! NSArray).mutableCopy()as! NSMutableArray
                    
                    if(aryRecord.count != 0){
                        
                        if((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") is NSDictionary){
                            var dictRecord = NSMutableDictionary()
                            dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            let dictIntRoomInfo =  removeNullFromDict(dict: dictRecord)
                            self.txtBathroom.text = "\(dictIntRoomInfo.value(forKey: "BathCount")!)"
                            self.txtBedRooms.text = "\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)"
                            self.txtNumberOfStorys.text = "\(dictIntRoomInfo.value(forKey: "StoriesCount")!)"
                            
                        }else{
                            self.txtBathroom.text = ""
                            self.txtBedRooms.text = ""
                            self.txtNumberOfStorys.text = ""
                            
                        }
                        
                        if((aryRecord[0] as AnyObject).value(forKey: "PropertyUseInfo") is NSDictionary){
                            var dictRecord = NSMutableDictionary()
                            dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PropertyUseInfo") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            let dictIntRoomInfo =  removeNullFromDict(dict: dictRecord)
                            self.txtYearBuilt.text = "\(dictIntRoomInfo.value(forKey: "YearBuilt")!)"
                            
                        }
                        else{
                            self.txtBathroom.text = ""
                            self.txtBedRooms.text = ""
                            self.txtNumberOfStorys.text = ""
                            
                        }
                        
                        if((aryRecord[0] as AnyObject).value(forKey: "PropertySize") is NSDictionary){
                            var dictRecord = NSMutableDictionary()
                            dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PropertySize") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            let dictPropertySize =  removeNullFromDict(dict: dictRecord)
                            self.txtArea.text = "\(dictPropertySize.value(forKey: "AreaBuilding")!)"
                            self.txtLotSize.text = "\(dictPropertySize.value(forKey: "AreaLotSF")!)"
                        }else{
                            self.txtArea.text = ""
                            self.txtLotSize.text = ""
                        }
                       //----Navin -- For calculate Truf Area
                        
                        txtLinerFt.text = calculateLinerSquareFt(Area: txtArea.text ?? "", NoOfStroy: txtNumberOfStorys.text ?? "")
                        
                        txtTrufSq.text = calculateTurfArea(LotSize: txtLotSize.text ?? "", Area: txtArea.text ?? "", Story: txtNumberOfStorys.text ?? "", ShrubArea: "\(txtShrub.text ?? "")")
                        //---- END-----
                    }
                }
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    
}

// MARK: --------------------- Text Field Delegate --------------

extension SalesNewFlow_PropertyDetailVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        isEditInSalesInfo = true
            
        if textField == txtArea
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            if string == ""
            {
                var strValue = ""
                strValue = ("\(txtArea.text ?? "")" as NSString).replacingCharacters(in: range, with: "")
                txtTrufSq.text = calculateTurfArea(LotSize: txtLotSize.text ?? "", Area: strValue, Story: txtNumberOfStorys.text ?? "", ShrubArea: txtShrub.text ?? "")
                txtLinerFt.text = calculateLinerSquareFt(Area: strValue, NoOfStroy: txtNumberOfStorys.text ?? "")
            }
            else
            {
                var strValue = ""
                strValue = "\( txtArea.text ?? "")" + string
                txtTrufSq.text = calculateTurfArea(LotSize: txtLotSize.text ?? "", Area: strValue, Story: txtNumberOfStorys.text ?? "", ShrubArea: txtShrub.text ?? "")
                txtLinerFt.text = calculateLinerSquareFt(Area: strValue, NoOfStroy: txtNumberOfStorys.text ?? "")
            }
            
            return chk
        }
        if textField == txtBedRooms
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            
            return chk
        }
        if textField == txtBathroom
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            
            return chk
        }
        if textField == txtLotSize
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            if string == ""
            {
                var strValue = ""
                strValue = ("\(txtLotSize.text ?? "")" as NSString).replacingCharacters(in: range, with: "")
                txtTrufSq.text = calculateTurfArea(LotSize: strValue, Area: txtArea.text ?? "", Story: txtNumberOfStorys.text ?? "", ShrubArea: txtShrub.text ?? "")
            }
            else
            {
                var strValue = ""
                strValue = "\( txtLotSize.text ?? "")" + string
                txtTrufSq.text = calculateTurfArea(LotSize: strValue, Area: txtArea.text ?? "", Story: txtNumberOfStorys.text ?? "", ShrubArea: txtShrub.text ?? "")
            }
            
            
            return chk
        }
        if textField == txtLinerFt
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            
            return chk
        }
        if textField == txtYearBuilt
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            
            return chk
        }
        if textField == txtNumberOfStorys
        {
            //let chk = decimalValidation(textField: textField, range: range, string: string)
            
            if string == ""
            {
                var strValue = ""
                strValue = ("\(txtNumberOfStorys.text ?? "")" as NSString).replacingCharacters(in: range, with: "")
                txtTrufSq.text = calculateTurfArea(LotSize: txtLotSize.text ?? "", Area: txtArea.text ?? "", Story: strValue, ShrubArea: txtShrub.text ?? "")
                txtLinerFt.text = calculateLinerSquareFt(Area: txtArea.text ?? "", NoOfStroy: strValue)
            }
            else
            {
                var strValue = ""
                strValue = "\( txtNumberOfStorys.text ?? "")" + string
                txtTrufSq.text = calculateTurfArea(LotSize: txtLotSize.text ?? "", Area: txtArea.text ?? "", Story: strValue, ShrubArea: txtShrub.text ?? "")
                txtLinerFt.text = calculateLinerSquareFt(Area: txtArea.text ?? "", NoOfStroy: strValue)
            }
            
            return true
        }
        if textField == txtShrub
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            if string == ""
            {
                var strValue = ""
                strValue = ("\(txtShrub.text ?? "")" as NSString).replacingCharacters(in: range, with: "")
                txtTrufSq.text = calculateTurfArea(LotSize: txtLotSize.text ?? "", Area: txtArea.text ?? "", Story: txtNumberOfStorys.text ?? "", ShrubArea: strValue)
            }
            else
            {
                var strValue = ""
                strValue = "\( txtShrub.text ?? "")" + string
                txtTrufSq.text = calculateTurfArea(LotSize: txtLotSize.text ?? "", Area: txtArea.text ?? "", Story: txtNumberOfStorys.text ?? "", ShrubArea: strValue)
            }
            
            return chk
        }
        if textField == txtTrufSq
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            
            return chk
        }
        
           
        return true
    
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}
