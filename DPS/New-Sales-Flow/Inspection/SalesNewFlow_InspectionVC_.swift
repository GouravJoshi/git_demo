//
//  SalesNewFlow_Inspection.swift
//  DPS
//
//  Created by Akshay Hastekar on 03/06/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import SafariServices
import CoreData


class SalesNewFlow_InspectionVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet var lblHeaderTitle: UILabel!
    @IBOutlet var lblHeaderSubTitle: UILabel!
    @IBOutlet weak var lblBedRoom: UILabel!
    @IBOutlet weak var lblLotSize: UILabel!
    @IBOutlet weak var lblTurfSize: UILabel!
    @IBOutlet weak var lblShrubSize: UILabel!
    @IBOutlet weak var lblLinearSize: UILabel!
    @IBOutlet weak var viewLotSize: UIView!
    @IBOutlet weak var viewTurfSize: UIView!
    @IBOutlet weak var viewShrubSize: UIView!
    @IBOutlet weak var viewLinearSize: UIView!
    @IBOutlet weak var lblSpouseName: UILabel!
    @IBOutlet weak var lblUSDAHaediness: UILabel!
    @IBOutlet weak var lblBackyardDogName: UILabel!
    @IBOutlet weak var lblNoBodyInstructions: UILabel!
    @IBOutlet weak var lblPoolSize: UILabel!
    @IBOutlet weak var lblGrassType: UILabel!
    @IBOutlet weak var switchVegetableGarden: UISwitch!
    @IBOutlet weak var switchChemicalAllergies: UISwitch!
    @IBOutlet weak var switchAnimalsInBackyard: UISwitch!
    @IBOutlet weak var viewDynemicView: UIView!
    @IBOutlet weak var heightDynemicView: NSLayoutConstraint!
    @IBOutlet weak var btnEdit: UIButton!


    //MARK: - Variable
    @objc var matchesGeneralInfo = NSManagedObject()
    var aryDynemicForm = NSMutableArray() , aryFormBuilder = NSMutableArray()
    var strUserName = ""
    var strCompanyKey = ""
    
   let yourAttributes: [NSAttributedString.Key: Any] = [
       .font: UIFont.systemFont(ofSize: 14),
       .foregroundColor: UIColor.blue,
       .underlineStyle: NSUnderlineStyle.single.rawValue
   ]
    var tagForDropDownHeaderSection = 0
    var tagForDropDownControlSubcontrol = 0
    var loader = UIAlertController()
    
    //SalesInspectionFormBuilder - Saavan April 2022
    
    var objLeadDetails : NSManagedObject?
    var arrImageDetails: [[String: Any]] = []
    var arrCRMImageDetails: [[String: Any]] = []
    var arrGraphDetails: [[String: Any]] = []
    var countCRMImage = 0
    var dictSelectedGlobal = [String:Any]()
    //
    //MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        dict_Login_Data =  (UserDefaults.standard.value(forKey: "LoginDetails") as! NSDictionary).mutableCopy()as!NSMutableDictionary

        strUserName = "\(dict_Login_Data.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dict_Login_Data.value(forKeyPath: "Company.CompanyKey") ?? "")"
        lblHeaderTitle.text = "\(nsud.value(forKey: "titleHeader1") ?? "")"
        lblHeaderSubTitle.text = "\(nsud.value(forKey: "titleHeader2") ?? "")"
        
        //Loader
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            loadDynamicForm()
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        assignValues()
        
        if(getStatusCompleteWon())
        {
            self.btnEdit.isHidden = true
        }else{
            self.btnEdit.isHidden = false
        }
    }
    
    
     //MARK:
     //MARK:-------Header--IBAction-----------

 
     @IBAction func action_History(_ sender: UIButton) {
         let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
         alert.view.tintColor = UIColor.black
         alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
             self.view.endEditing(true)
             
            self.goToEmailHistory()
         }))
         alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
         
            self.goToSetupHistory()
         }))
         alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
         
            self.goToServiceNotesHistory()

         }))
         alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
         
            self.goToServiceHistory()
         }))
         alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
             
         }))
         
         if let popoverController = alert.popoverPresentationController {
             popoverController.sourceView = sender
            // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
             popoverController.permittedArrowDirections = [.up]
         }
         self.present(alert, animated: true)
        
       }
    
   
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnEditPropertyDetailAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesNewFlowInspection : SalesNewStoryBoardName.SalesNewFlowInspection, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNewFlow_PropertyDetailVC") as! SalesNewFlow_PropertyDetailVC
        vc.matchesGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func btnSwitchVegetableGardenAction(_ sender: Any) {
        
    }
    @IBAction func btnSwitchChecmicalAllergiesAction(_ sender: Any) {
        
    }
    @IBAction func btnSwitchAnimalsINtheBckyard(_ sender: Any) {
        
    }
    //MARK:
    //MARK:-------Footer--IBAction-----------
    @IBAction func action_Image(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Make Your Selection", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.AddImages, style: .default, handler: { action in
            self.GotoGlobleImageGraphView(strType: "Before")
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.AddDocuments, style: .default, handler: { [self] action in
            let strLeadID =  matchesGeneralInfo.value(forKey: "leadId") ?? ""

            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: DeviceType.IS_IPAD ? "UploadDocumentSalesiPad" : "UploadDocumentSales") as! UploadDocumentSales
            vc.strWoId = "\(strLeadID)" as NSString
            self.navigationController?.present(vc, animated: true, completion: nil)

        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
      //      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.down]
        }
        self.present(alert, animated: true)
    }
    @IBAction func action_Graph(_ sender: Any) {
        self.view.endEditing(true)
        nsud.setValue(true, forKey: "firstGraphImage")
        nsud.setValue(false, forKey: "servicegraph")
        GotoGlobleImageGraphView(strType: "Graph")
        //self.tempLoad()
        
    }
    @IBAction func action_ChemicalSensitive(_ sender: Any) {
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func action_Home(_ sender: Any) {
        self.view.endEditing(true)
        self.goToAppointment()
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        if(getStatusCompleteWon()){
            /*let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewSendEmail : SalesNewStoryBoardName.SalesFlowNewSendEmail, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
            vc.matchesGeneralInfo = matchesGeneralInfo
            vc.strSendMailStatusFrom = Enum_SalesNewSendMailFrom.Text
            self.navigationController?.pushViewController(vc, animated: false)*/
            
            goToNewSalesFlowSelectService()
            
        }else{
            if validation() {
                self.saveDynamicFormToDB()
                
                let strLeadID =  matchesGeneralInfo.value(forKey: "leadId") ?? ""
                Global().updateSalesModifydate(strLeadID as? String)
                
                /*let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewSendEmail : SalesNewStoryBoardName.SalesFlowNewSendEmail, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
                vc.matchesGeneralInfo = matchesGeneralInfo
                vc.strSendMailStatusFrom = Enum_SalesNewSendMailFrom.Text
                self.navigationController?.pushViewController(vc, animated: false)*/
                goToNewSalesFlowSelectService()
            }
        }
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func GotoGlobleImageGraphView(strType : String)  {
        let strLeadID =  matchesGeneralInfo.value(forKey: "leadId") ?? ""
        var strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
        var strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
        if(nsud.value(forKey: "backFromInspectionNew") != nil){
            
            if(nsud.value(forKey: "backFromInspectionNew") as! Bool == true){
                strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
                strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
            }
        }
    
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPhone" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        vc.strHeaderTitle = strType as NSString
        vc.strWoId = "\(strLeadID)" as NSString
        vc.strWoLeadId = "\(strLeadID)" as NSString
        vc.strModuleType = "SalesFlow"
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won"){
            vc.strWoStatus = "Complete"
        }else{
            vc.strWoStatus = "InComplete"
        }
        vc.strWoStatus = "InComplete"
        self.navigationController?.present(vc, animated: true, completion: nil)

    }
    //MARK:-----------------------------
    //MARK: - -----------Custom Funtions
    
    func goToNewSalesFlowSelectService() {
        
        if "\(matchesGeneralInfo.value(forKey: "flowType") ?? "")".caseInsensitiveCompare("Commercial") == .orderedSame
        {
            goToNewCommercialFlowSelectService()
        }
        else
        {
            let controller = storyboardNewSalesSelectService.instantiateViewController(withIdentifier: "NewSales_SelectService") as! NewSales_SelectService
            controller.refreshOnBack = self
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
    
    func goToNewCommercialFlowSelectService() {

         let controller =  UIStoryboard.init(name:"NewCommercialServices", bundle: nil).instantiateViewController(withIdentifier: "NewCommerialSelectServiceViewController") as! NewCommerialSelectServiceViewController
         controller.strLeadId = "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" as NSString
         self.navigationController?.pushViewController(controller, animated: false)
     }
    
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.delegate = self
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func getStatusCompleteWon() -> Bool {
        let strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
        let strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
        
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won") {
            return true
        }else{
            return false
        }
    }
    
    func goToEmailHistory()  {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
              let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
              testController?.modalPresentationStyle = .fullScreen
              testController?.strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
              testController?.strCompanyKey = strCompanyKey
              testController?.strUserName = strUserName

              self.present(testController!, animated: false, completion: nil)
    }
    
    func goToServiceHistory()  {
        
        /*let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = "\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
        
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
        
        
    }
    func goToServiceNotesHistory()  {
        
        //ServiceNotesHistoryViewController
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController?.strTypeOfService = "sales";
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAppointment()
    {
           
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            

            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    
    
    func assignValues(){
        
        var strPropertyDetail = ""
        
        if("\(matchesGeneralInfo.value(forKey: "noOfBedroom") ?? "")" != ""){
            strPropertyDetail = strPropertyDetail + "\(matchesGeneralInfo.value(forKey: "noOfBedroom") ?? "") bd   |   "
        }
        if("\(matchesGeneralInfo.value(forKey: "noOfBathroom") ?? "")" != ""){
            strPropertyDetail = strPropertyDetail + "\(matchesGeneralInfo.value(forKey: "noOfBathroom") ?? "") ba   |   "
        }
        if("\(matchesGeneralInfo.value(forKey: "area") ?? "")" != ""){
            strPropertyDetail = strPropertyDetail + "\(matchesGeneralInfo.value(forKey: "area") ?? "") sqft  |  "
        }
        if("\(matchesGeneralInfo.value(forKey: "noOfStory") ?? "")" != ""){
            strPropertyDetail = strPropertyDetail + "\(matchesGeneralInfo.value(forKey: "noOfStory") ?? "") story   |   "
        }
        if("\(matchesGeneralInfo.value(forKey: "yearBuilt") ?? "")" != ""){
            strPropertyDetail = strPropertyDetail + "\(matchesGeneralInfo.value(forKey: "yearBuilt") ?? "")|   "
        }
        if(strPropertyDetail.count != 0){
            strPropertyDetail = String(strPropertyDetail.dropLast(4))
        }
        
        lblBedRoom.text = strPropertyDetail
        lblLotSize.text =  "\(matchesGeneralInfo.value(forKey: "lotSizeSqFt") ?? "")" + " " + "sqft"
        (lblLotSize.text == "sqft" ? (viewLotSize.isHidden = true) : (viewLotSize.isHidden = false))
        lblLinearSize.text = "\(matchesGeneralInfo.value(forKey: "linearSqFt") ?? "")" + " " + " ft"
        (lblLinearSize.text == "ft" ?( viewLinearSize.isHidden = true) : (viewLinearSize.isHidden = false))
        lblTurfSize.text = "\(matchesGeneralInfo.value(forKey: "turfArea") ?? "")" +  " " + " sqft"
        (lblTurfSize.text == "sqft" ?( viewTurfSize.isHidden = true) : (viewTurfSize.isHidden = false))
        lblShrubSize.text = "\(matchesGeneralInfo.value(forKey: "shrubArea") ?? "")" + " " + " sqft"
        (lblShrubSize.text == "sqft" ?( viewShrubSize.isHidden = true) : (viewShrubSize.isHidden = false))
    }
    
    func validation() -> Bool {
        if(self.aryDynemicForm.count != 0){
            for (headerIndex, item) in self.aryDynemicForm.enumerated() {
                print("headerIndex---------------\(headerIndex)")

                var dictMAIN = NSMutableDictionary()
                dictMAIN = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let HeaderName = "\(dictMAIN.value(forKey: "DepartmentName")!)"


                if dictMAIN.value(forKey: "viewExpand") as! Bool {
                    let arySECTION = dictMAIN.value(forKey: "Sections")as! NSArray

                    for (sectionIndex, item) in arySECTION.enumerated() {
                        print("sectionIndex---------------\(sectionIndex)")

                        
                        let dictSectionData = (item as AnyObject) as! NSDictionary
                        let sectionTitle =  "\(dictSectionData.value(forKey: "Name")!)"

                        let arySECTIONControls = dictSectionData.value(forKey: "Controls")as! NSArray
                        for (controlIndex, item) in arySECTIONControls.enumerated() {
                            let dictSectionControl = (item as AnyObject) as! NSDictionary
                           print("COntrolINDEX---------------\(controlIndex)")
                            //MARK:---------email

                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.email){
                                let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                if Value.count != 0{
                                    let isValid = Global().checkIfCommaSepartedEmailsAreValid(Value)
                                    if !isValid {
                                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid email in DepartmentName - \(HeaderName) and Section - \(sectionTitle)", viewcontrol: self)
                                        break
                                    }
                                }
                            }
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.phone){
                                let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                if Value.count != 0{
                                    if Value.count < 12 {
                                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid phone in DepartmentName - \(HeaderName) and Section - \(sectionTitle)", viewcontrol: self)
                                        break
                                    }
                                }
                            }
                           
                        }
                    }
                }
                
            }
            
        }
        return true
    }
    //MARK:-----------------------------
    //MARK: - -----------loadDynamicForm  OR SAVE
    func loadDynamicForm() {
        
        // Fetch From Core Data if Data Exists
        
        let arrayDynamicInspection = getDataFromLocal(strEntity: "SalesDynamicInspection", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , "\(Global().getUserName()!)"))
        
        if(arrayDynamicInspection.count > 0)
        {
            let objSalesInspectionData = arrayDynamicInspection[0] as! NSManagedObject
            if objSalesInspectionData.value(forKey: "arrFinalInspection") is NSArray {
               var aryDynemicFormTemp = NSMutableArray()
                aryDynemicFormTemp = CompareMsterAndLocalDataAndCreatNewJson(aryMaster: getDynemicFormFromMaster(), aryLocal: (objSalesInspectionData.value(forKey: "arrFinalInspection") as! NSArray).mutableCopy()as! NSMutableArray)
                
                var isSameFlowType = true
                for (index, element) in aryDynemicFormTemp.enumerated() {
                    var dict = NSMutableDictionary()
                    dict = (((element as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                    dict.setValue(false, forKey: "viewExpand")
                    dict.setValue("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", forKey: "LeadId")
                    aryDynemicFormTemp.replaceObject(at: index, with: dict)
                    let flowTypeMaster = "\(dict.value(forKey: "FlowType") ?? "")"
                    let flowTypeOpportunity = "\(matchesGeneralInfo.value(forKey: "flowType") ?? "")"
                    if (flowTypeMaster != flowTypeOpportunity) && !(flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame) {
                        isSameFlowType = false
                    }
                }
                
                // Change For To check if commercial or residentail flow saved and which flow type is of lead if it is changed then need to delete saved dynamic form and re enter new dynamic form.

                if isSameFlowType {
                    
                    
                    
                    
                    var arrOfFilteredDataTemp = [NSDictionary] ()
                    arrOfFilteredDataTemp = aryDynemicFormTemp as! [NSDictionary]
                    let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
                        (($0 )["SequenceNo"] as? CGFloat)! < (($1 )["SequenceNo"] as? CGFloat)!
                    })
                    self.aryDynemicForm = NSMutableArray()
                    self.aryDynemicForm = (sortedArray as NSArray).mutableCopy()as! NSMutableArray
                    CreatDynemicView(aryData: aryDynemicForm)
                    
                } else {
                    
                    // Get Dynamic Form From Masters and create Dynamic View

                    self.fetchDynamicFormFromMasterAndCreateDynamicView()

                }
            }
        }else{
            
            //  If Data not exist then check Status and Stage and then fetch From Masters
            
            if "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")".caseInsensitiveCompare("Complete") == .orderedSame && "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")".caseInsensitiveCompare("Won") == .orderedSame{
                
                // Create blank No Dynamic Form Exist
                
            }else{
                
                // Get Dynamic Form From Masters and create Dynamic View
                
                self.fetchDynamicFormFromMasterAndCreateDynamicView()
                                
            }
        }
        self.loader.dismiss(animated: false) {}
    }
    
    func tempLoad() {
        
        let arrayInspectionFormBuilder = getDataFromLocal(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && companyKey == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , "\(Global().getUserName()!)" , Global().getCompanyKey()))
      //Convert NSManagObject to Dict
        let arytemp = NSMutableArray()
        for item in arrayInspectionFormBuilder {
            arytemp.add((item as! NSManagedObject).toDict())
        }
        //Get Unique Template Key
        var arytemplateKey = Set<String>()
        arytemp.filter { data in
            if arytemplateKey.contains((data as! NSDictionary).value(forKey: "templateKey") as! String) {
                return false
            } else {
                arytemplateKey.insert((data as! NSDictionary).value(forKey: "templateKey") as! String)
                return true
            }
        }
        //Get  Template Key related all Data
        let aryFinal = NSMutableArray()
        for item in arytemplateKey {
            let templateKey = "\(item)"
            let aryTemp1 = arytemp.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "templateKey")!)" ==  templateKey
            }
            
            //Sort Data by date
            var arrOfFilteredDataTemp = [NSDictionary] ()
            arrOfFilteredDataTemp = aryTemp1 as! [NSDictionary]
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            //dateFormatter.dateFormat = "MM/dd/yyyy"
            let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
                (dateFormatter.date(from:(($0 )["createdDate"] as? String)!))! > (dateFormatter.date(from:(($1 )["createdDate"] as? String)!))!
            })
            print(sortedArray)
            if(sortedArray.count != 0){
                aryFinal.add(sortedArray[0])
            }
        }
        print(aryFinal)
        //self.aryFormBuilder = NSMutableArray()
        //self.aryFormBuilder = (aryFinal as NSArray).mutableCopy() as! NSMutableArray
    }
    
    func loadInspectionFormBuilderData(){
        
        if getStatusCompleteWon() {
            
            let arrayInspectionFormBuilder = getDataFromLocal(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && companyKey == %@ && docType == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , "\(Global().getUserName()!)" , Global().getCompanyKey(), "Inspection"))
          //Convert NSManagObject to Dict
            let arytemp = NSMutableArray()
            for item in arrayInspectionFormBuilder {
                
                let match = item as! NSManagedObject
                
                if "\(match.value(forKey: "templateKey") ?? "")".count > 0
                {
                    arytemp.add((item as! NSManagedObject).toDict())
                }
            }
            
            var arrKey: [String] = []
            for item in arytemp
            {
                let match = item as! NSDictionary
                //arytemp.add("\(match.value(forKey: "templateKey"))")
                arrKey.append("\(match.value(forKey: "templateKey") ?? "")")
            }
            let arrUnique = arrKey.unique()
            
            
            //Get Unique Template Key
//            var arytemplateKey = Set<String>()
//            arytemp.filter { data in
//                if arytemplateKey.contains((data as! NSDictionary).value(forKey: "templateKey") as! String) {
//                    return false
//                } else {
//                    arytemplateKey.insert((data as! NSDictionary).value(forKey: "templateKey") as! String)
//                    return true
//                }
//            }
            
            let arytemplateKey = arrUnique
            //Get  Template Key related all Data
            let aryFinal = NSMutableArray()
            for item in arytemplateKey {
                let templateKey = "\(item)"
                let aryTemp1 = arytemp.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "templateKey")!)" ==  templateKey
                }
                
                //Sort Data by date
                var arrOfFilteredDataTemp = [NSDictionary] ()
                arrOfFilteredDataTemp = aryTemp1 as! [NSDictionary]
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                //dateFormatter.dateFormat = "MM/dd/yyyy"
                let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
                    (dateFormatter.date(from:(($0 )["createdDate"] as? String)!))! > (dateFormatter.date(from:(($1 )["createdDate"] as? String)!))!
                })
                print(sortedArray)
                if(sortedArray.count != 0){
                    aryFinal.add(sortedArray[0])
                }
            }
            print(aryFinal)
            self.aryFormBuilder = NSMutableArray()
            self.aryFormBuilder = (aryFinal as NSArray).mutableCopy() as! NSMutableArray
        }else{
            let arrayInspectionFormBuilder = getDataFromLocal(strEntity: "SalesInspectionFormBuilder", predicate: NSPredicate(format: "flowType == %@ && userName == %@ && companyKey == %@", "\(matchesGeneralInfo.value(forKey: "flowType") ?? "")" , "\(Global().getUserName()!)" , Global().getCompanyKey()))
            self.aryFormBuilder = NSMutableArray()
            self.aryFormBuilder = arrayInspectionFormBuilder.mutableCopy() as! NSMutableArray
        }
        print(self.aryFormBuilder.count)
    }
    
    func fetchDynamicFormFromMasterAndCreateDynamicView() {
        
        // Get Dynamic Form From Masters
        
        let arrTempInspection = getDynemicFormFromMaster()

        if arrTempInspection.count > 0 {
            
            // If found data in Master will create Dynamic View.
            
            let aryDynemicFormTemp = NSMutableArray()
            
            aryDynemicFormTemp.addObjects(from: arrTempInspection as! [Any])
            
            for (index, element) in aryDynemicFormTemp.enumerated() {
              //  print("Item \(index): \(element)")
                var dict = NSMutableDictionary()
                dict = (((element as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                dict.setValue(false, forKey: "viewExpand")
                dict.setValue("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", forKey: "LeadId")
                aryDynemicFormTemp.replaceObject(at: index, with: dict)
            }
            
            // call Func to crate dynamic Form.

            var arrOfFilteredDataTemp = [NSDictionary] ()
            arrOfFilteredDataTemp = aryDynemicFormTemp as! [NSDictionary]
            let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
                (($0 )["SequenceNo"] as? CGFloat)! < (($1 )["SequenceNo"] as? CGFloat)!
            })
            self.aryDynemicForm = NSMutableArray()
            self.aryDynemicForm = (sortedArray as NSArray).mutableCopy()as! NSMutableArray
            CreatDynemicView(aryData: aryDynemicForm)
            
            
        }else{
            
            // No Data Found in Master So blank Screen No Dynamic View We can show alert if required.
            CreatDynemicView(aryData: [])
        }
                
    }
    
    
    
    func getDynemicFormFromMaster() -> NSMutableArray {
        let arrTempInspection = NSMutableArray()
        
        if let arrayMaster = nsud.value(forKey: "MasterSalesAutomationDynamicForm") as? NSArray {
            
            if arrayMaster.isKind(of: NSArray.self) {
                 
                for item in arrayMaster
                {
                    let dictDataL = item as! NSDictionary
                    
                    let branchSysNameMaster = "\(dictDataL.value(forKey: "BranchSysName") ?? "")"
                    let flowTypeMaster = "\(dictDataL.value(forKey: "FlowType") ?? "")"
                    
                    
                    let branchSysNameOpprtunity = "\(matchesGeneralInfo.value(forKey: "branchSysName") ?? "")"
                    let flowTypeOpportunity = "\(matchesGeneralInfo.value(forKey: "flowType") ?? "")"
                    
                    
                    if flowTypeOpportunity.caseInsensitiveCompare("Commercial") == .orderedSame {
                        
                        if (branchSysNameMaster ==  branchSysNameOpprtunity) && ( flowTypeMaster.caseInsensitiveCompare("Commercial") == .orderedSame ||  flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame ){
                            
                            // Add All Duynamic Forms Of Commmercail FLow.
                            arrTempInspection.add(dictDataL)
                            
                        }
                                                        
                    }else{
                        
                        if (branchSysNameMaster ==  branchSysNameOpprtunity) && ( flowTypeMaster.caseInsensitiveCompare("Residential") == .orderedSame ||  flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame ){
                            
                            // Add All Duynamic Forms Of Commmercail FLow.
                            arrTempInspection.add(dictDataL)

                        }
                        
                    }
                    

                }
            }
        }
        return arrTempInspection
    }
    
    
    
    func CompareMsterAndLocalDataAndCreatNewJson(aryMaster : NSMutableArray , aryLocal : NSMutableArray) -> NSMutableArray  {
        
        let arrayfinal = NSMutableArray()
        //arrayfinal = aryMaster
        
        for item in aryMaster {
            let dictInspectionData = (item as AnyObject) as! NSDictionary
            let strDepartmentSysName = "\(dictInspectionData.value(forKey: "DepartmentSysName")!)"
            let aryTemp = aryLocal.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "DepartmentSysName")!)" == strDepartmentSysName }
            if(aryTemp.count != 0){
                arrayfinal.add(aryTemp[0])
            }else{
                arrayfinal.add(dictInspectionData)
            }
        }
        return arrayfinal
        
        
    }
    
    func saveDynamicFormToDB() {
        
        // delete first from DB and then Insert new record
        deleteAllRecordsFromDB(strEntity: "SalesDynamicInspection", predicate: NSPredicate(format: "leadId = %@ AND userName = %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")",Global().getUserName()))

        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("arrFinalInspection")
        arrOfKeys.add("leadId")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("empId")
        arrOfKeys.add("isSentToServer")

        
        arrOfValues.add(aryDynemicForm)
        arrOfValues.add("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add("Yes")
        
        saveDataInDB(strEntity: "SalesDynamicInspection", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        
    }
    
    //MARK: --------------------------
    //MARK: - CreatDynemicView
    
    func CreatDynemicView(aryData : NSMutableArray) {
        
 
 
        for v in viewDynemicView.subviews{
            v.removeFromSuperview()
        }
        var yAxis = 0.0 , extraSpace = DeviceType.IS_IPAD ? 20.0 : 12.0
        let height = DeviceType.IS_IPAD ? 55.0 : 40.0
        let heighttextView = DeviceType.IS_IPAD ? 135.0 : 100.0

        if(aryData.count != 0){
            for (headerIndex, item) in aryData.enumerated() {
                print("headerIndex---------------\(headerIndex)")

                var dictMAIN = NSMutableDictionary()
                dictMAIN = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                //MARK:-------------Start Header View ---------
                let viewHeader = UIView.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width), height: Int(height)))
                viewHeader.backgroundColor = SalesNewColorCode.darkBlack
                let lblHeaderTitle = UILabel.init(frame: CGRect(x: 40, y: 4, width: Int(self.viewDynemicView.frame.width) - 80 , height: Int(height) - 8))
                lblHeaderTitle.textAlignment = .center
                lblHeaderTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                
                var strTitle = "\(dictMAIN.value(forKey: "FormName") ?? "")"
                
                if strTitle.count == 0 || strTitle == ""
                {
                    strTitle = "\(dictMAIN.value(forKey: "DepartmentName") ?? "")"
                }
                
                lblHeaderTitle.text = strTitle//"\(dictMAIN.value(forKey: "DepartmentName")!)"
                lblHeaderTitle.numberOfLines = 2
                lblHeaderTitle.textColor = UIColor.white
                viewHeader.addSubview(lblHeaderTitle)
                let btnHeaderDetail = UIButton.init(frame: CGRect(x: 15, y: 0, width: Int(self.viewDynemicView.frame.width), height: Int(height)))
                btnHeaderDetail.setTitle(dictMAIN.value(forKey: "viewExpand") as! Bool ? "-" : "+", for: .normal)
                btnHeaderDetail.titleLabel?.font = UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 28.0 : 22.0)
                btnHeaderDetail.tag = headerIndex
                btnHeaderDetail.addTarget(self, action: #selector(didTap_btnHeaderDetail(sender:)), for: .touchUpInside)
                btnHeaderDetail.contentHorizontalAlignment = .left
                btnHeaderDetail.tag = headerIndex
                viewHeader.addSubview(btnHeaderDetail)
                self.viewDynemicView.addSubview(viewHeader)
                yAxis = yAxis + height + extraSpace
           
                //MARK:------------End Header View ---------
             
                //MARK:------------Start Section View ---------
                if dictMAIN.value(forKey: "viewExpand") as! Bool {
                    let arySECTION = dictMAIN.value(forKey: "Sections")as! NSArray
                    for (sectionIndex, item) in arySECTION.enumerated() {
                        print("sectionIndex---------------\(sectionIndex)")

                        
                        let dictSectionData = (item as AnyObject) as! NSDictionary
                        let title =  "\(dictSectionData.value(forKey: "Name")!)"
                        let viewSection = UIView.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width), height: Int(height)))
                        viewSection.backgroundColor = SalesNewColorCode.lightgray
                        let lblSectionTitle = UILabel()
                        lblSectionTitle.frame = CGRect(x: 40, y: 4, width: Int(self.viewDynemicView.frame.width) - 80 , height: Int(height) - 8)
                        
                        lblSectionTitle.textAlignment = .center
                        lblSectionTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                        lblSectionTitle.text = title
                        lblSectionTitle.numberOfLines = 1
                        lblSectionTitle.textColor = UIColor.white
                        viewSection.addSubview(lblSectionTitle)
                        self.viewDynemicView.addSubview(viewSection)
                        yAxis = yAxis + height + extraSpace
                        //MARK:-Section Control
                        let arySECTIONControls = dictSectionData.value(forKey: "Controls")as! NSArray
                        for (controlIndex, item) in arySECTIONControls.enumerated() {
                            let dictSectionControl = (item as AnyObject) as! NSDictionary
                           print("COntrolINDEX---------------\(controlIndex)")
                            //MARK:---------Checkbox

//                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.checkbox) {
//
//                               let title =  "\(dictSectionControl.value(forKey: "Label")!)"
//                                let lblSectionDetailTitle = UILabel()
//                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
//                                lblSectionDetailTitle.text = title
//                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
//                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
//
//                                lblSectionDetailTitle.numberOfLines = 0
//                                lblSectionDetailTitle.textColor = UIColor.darkGray
//                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
//                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
//                                let values = "\(dictSectionControl.value(forKey: "Value")!)"
//
//                                let btnCheckBox = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0) , height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
//                                if(values.lowercased() == "true"){
//                                    btnCheckBox.setImage(UIImage(named: "NPMA_Check"), for: .normal)
//                                }else{
//                                    btnCheckBox.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
//                                }
//
//                                btnCheckBox.titleLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
//                                btnCheckBox.contentHorizontalAlignment = .left
//                                btnCheckBox.tag = headerIndex*10000+sectionIndex
//                                btnCheckBox.titleLabel?.tag = controlIndex*10000+0
//                                btnCheckBox.addTarget(self, action: #selector(didTap_btnCheckbox(sender:)), for: .touchUpInside)
//
//                                if getStatusCompleteWon() {
//                                    btnCheckBox.isUserInteractionEnabled = false
//                                }else{
//                                    btnCheckBox.isUserInteractionEnabled = true
//                                }
//                                self.viewDynemicView.addSubview(btnCheckBox)
//                                let strLabel = "\(dictSectionControl.value(forKey: "Label")!)"
//                                let lblTitle = UILabel()
//                                lblTitle.text = "\(strLabel)"
//                                lblTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
//                                lblTitle.numberOfLines = 0
//                                lblTitle.textColor = UIColor.darkGray
//
//                                var lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (viewDynemicView.frame.width - btnCheckBox.frame.maxX + 10.0), font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
//                                if(lbltitleHeight < 35.0){
//                                    lbltitleHeight = (DeviceType.IS_IPAD ? 40.0 : 30.0)
//                                }
//                                lblTitle.frame =  CGRect(x: Int(btnCheckBox.frame.maxX + 10), y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - (Int(btnCheckBox.frame.maxX) + 10) , height: Int(lbltitleHeight)
//                                )
//                                btnCheckBox.center.y = lblTitle.center.y
//
//                                self.viewDynemicView.addSubview(lblTitle)
//                                yAxis = yAxis + lbltitleHeight + extraSpace
//
//                            }
                            //MARK:------ Checkbox_combo

           
                            if ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.checkbox_combo){
                                
                               let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                             
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                let values = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",")as NSArray

                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                for (subControlIndex, item) in aryOption.enumerated() {
                                    
                                    let optionDict = (item as AnyObject) as! NSDictionary
                                    let strLabel = "\(optionDict.value(forKey: "Label") ?? "")"
                                    let strValue = "\(optionDict.value(forKey: "Value") ?? "")"

                                    let btnCheckBox = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0) , height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
                                    if(values.contains("\(strValue)")){
                                        btnCheckBox.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                                    }else{
                                        btnCheckBox.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                                    }
                                   
                                    btnCheckBox.titleLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                    btnCheckBox.contentHorizontalAlignment = .left
                                    btnCheckBox.tag = headerIndex*10000+sectionIndex
                                    btnCheckBox.titleLabel?.tag = controlIndex*10000+subControlIndex
                                 
                                        btnCheckBox.addTarget(self, action: #selector(didTap_btnCheckboxCombo(sender:)), for: .touchUpInside)
                                  
                                    if getStatusCompleteWon() {
                                        btnCheckBox.isUserInteractionEnabled = false
                                    }else{
                                        btnCheckBox.isUserInteractionEnabled = true

                                    }
                                    self.viewDynemicView.addSubview(btnCheckBox)

                                    
                                    let lblTitle = UILabel()
                                    lblTitle.text = "\(strLabel)"
                                    lblTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                    lblTitle.numberOfLines = 0
                                    lblTitle.textColor = UIColor.darkGray
                                    
                                    
                                    var lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (viewDynemicView.frame.width - btnCheckBox.frame.maxX + 10.0), font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                    if(lbltitleHeight < 35.0){
                                        lbltitleHeight = (DeviceType.IS_IPAD ? 40.0 : 30.0)
                                    }
                                    lblTitle.frame =  CGRect(x: Int(btnCheckBox.frame.maxX + 10), y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - (Int(btnCheckBox.frame.maxX) + 10) , height: Int(lbltitleHeight)
                                    )
                                    btnCheckBox.center.y = lblTitle.center.y

                                    self.viewDynemicView.addSubview(lblTitle)
                                    yAxis = yAxis + lbltitleHeight + extraSpace
                                }
                            }
                       
                            //MARK:---------Radio || Radio_combo
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.radio) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.radio_combo){
                                
                                
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                 let lblSectionDetailTitle = UILabel()
                                 lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 lblSectionDetailTitle.text = title
                                 let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                 lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                           
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace

                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let values = "\(dictSectionControl.value(forKey: "Value")!)"

                                for (subControlIndex, item) in aryOption.enumerated() {
                                    let optionDict = (item as AnyObject) as! NSDictionary
                                   let strLabel = "\(optionDict.value(forKey: "Label") ?? "")"
                                    let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                    
                                    let btnRadioBox = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0), height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
                                    if(values.lowercased() == strValue.lowercased() && values != "") {
                                        btnRadioBox.setImage(UIImage(named: "redio_2"), for: .normal)
                                    }else{
                                        btnRadioBox.setImage(UIImage(named: "redio_1"), for: .normal)
                                    }
                                    btnRadioBox.titleLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                    btnRadioBox.contentHorizontalAlignment = .left
                                   
                                    btnRadioBox.tag = headerIndex*10000+sectionIndex
                                    btnRadioBox.titleLabel?.tag = controlIndex*10000+subControlIndex

                                    if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.radio){
                                        btnRadioBox.addTarget(self, action: #selector(didTap_btnRadio(sender:)), for: .touchUpInside)
                                    }else{
                                        btnRadioBox.addTarget(self, action: #selector(didTap_btnRadioCombo(sender:)), for: .touchUpInside)
                                    }
                                    if getStatusCompleteWon() {
                                        btnRadioBox.isUserInteractionEnabled = false
                                    }else{
                                        btnRadioBox.isUserInteractionEnabled = true

                                    }
                                    self.viewDynemicView.addSubview(btnRadioBox)
                                    
                                    let lblTitle = UILabel()
                                    lblTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                    lblTitle.text = "\(strLabel)"
                                    lblTitle.numberOfLines = 0
                                    lblTitle.textColor = UIColor.darkGray
                                    
                                    
                                    var lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (viewDynemicView.frame.width - btnRadioBox.frame.maxX + 10.0), font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                    if(lbltitleHeight < 35.0){
                                        lbltitleHeight = (DeviceType.IS_IPAD ? 40.0 : 30.0)
                                    }
                                    lblTitle.frame =  CGRect(x: Int(btnRadioBox.frame.maxX + 10), y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - (Int(btnRadioBox.frame.maxX) + 10) , height: Int(lbltitleHeight)
                                    )
                                    btnRadioBox.center.y = lblTitle.center.y

                                  
                                    self.viewDynemicView.addSubview(lblTitle)
                                    yAxis = yAxis + lbltitleHeight + extraSpace
                                }

                            }
                           
                            //MARK:---------TextView || Paragraph
                            //el-textarea
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.textarea) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.paragraph)
                            {
                                if getStatusCompleteWon() {
                                    let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                     let lblSectionDetailTitle = UILabel()
                                     lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                     lblSectionDetailTitle.text = title
                                     let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                     lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                               
                                    lblSectionDetailTitle.numberOfLines = 0
                                    lblSectionDetailTitle.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                    yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                    
                                    let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                    let lblValue = UILabel()
                                    lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                    lblValue.text = Value
                                    let heightCalculatelblValue = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                    lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                    
                                    lblValue.numberOfLines = 0
                                    lblValue.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblValue)
                                    yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                    
                                    
                                }else{
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                 let lblSectionDetailTitle = UILabel()
                                 lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 lblSectionDetailTitle.text = title
                                 let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                 lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                           
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let textarea = UITextView.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(heighttextView)))
                                textarea.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                textarea.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                textarea.textColor = UIColor.darkGray
                                textarea.layer.cornerRadius = 8.0
                                textarea.layer.borderWidth = 1.0
                                textarea.layer.borderColor = UIColor.lightGray.cgColor
                                textarea.delegate = self
                                
                                textarea.tag = headerIndex*10000+sectionIndex
                                textarea.placeholderLabel.tag = controlIndex*10000+0
                                
                                self.viewDynemicView.addSubview(textarea)
                                yAxis = yAxis + heighttextView + extraSpace
                                }
                            }
                          
                            //MARK:---------UITextField
                            //el-textbox,el-email,el-phone,el-currency,el-decimal,el-number,el-auto-num,el-precent,el-upload
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.textbox || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.email || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.phone || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.currency || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.decimal || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.number || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.autonum || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.precent ){
                       
                                if getStatusCompleteWon() {
                                    let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                     let lblSectionDetailTitle = UILabel()
                                     lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                     lblSectionDetailTitle.text = title
                                     let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                     lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                               
                                    lblSectionDetailTitle.numberOfLines = 0
                                    lblSectionDetailTitle.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                    yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                    
                                    let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                    let lblValue = UILabel()
                                    lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                    lblValue.text = Value
                                    let heightCalculatelblValue = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18: 14))
                                    lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                    
                                    lblValue.numberOfLines = 0
                                    lblValue.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblValue)
                                    yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                    
                                    
                                }else{
                                    let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                    textarea?.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                    textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                    textarea!.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                    textarea!.textColor = UIColor.darkGray
                                    textarea!.lineColor = UIColor.lightGray
                                    textarea!.delegate = self
                                  
                                    textarea!.tag = headerIndex*10000+sectionIndex
                                    let lftview = UILabel()
                                    lftview.tag = controlIndex*10000+0
                                    textarea!.leftView = lftview
                                    
                                    switch "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() {
                                    case DynemicFormCheck.email:
                                        textarea!.keyboardType = .emailAddress
                                    case DynemicFormCheck.phone:
                                        textarea!.text = formattedNumber(number: textarea!.text!)
                                        textarea!.keyboardType = .phonePad
                                    case DynemicFormCheck.currency:
                                        textarea!.keyboardType = .phonePad
                                    case DynemicFormCheck.decimal:
                                        textarea!.keyboardType = .decimalPad
                                    case DynemicFormCheck.number:
                                        textarea!.keyboardType = .phonePad
                                    case DynemicFormCheck.autonum:
                                        textarea!.keyboardType = .phonePad
                                    case DynemicFormCheck.precent:
                                        textarea!.keyboardType = .phonePad
                                    default:
                                        textarea!.keyboardType = .default
                                    }
                                    self.viewDynemicView.addSubview(textarea!)
                                    yAxis = yAxis + height + extraSpace
                                }
                                
                          
                            }
            
                            //MARK:---------Datetime
                            //el-date , el-date-time
                            
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date_time){
                                
                                if getStatusCompleteWon() {
                                    let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                    let lblSectionDetailTitle = UILabel()
                                    lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                    lblSectionDetailTitle.text = title
                                    let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                    lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                    
                                    lblSectionDetailTitle.numberOfLines = 0
                                    lblSectionDetailTitle.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                    yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                    
                                    let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                    let lblValue = UILabel()
                                    lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                    lblValue.text = Value
                                    let heightCalculatelblValue = Value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                    lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                    
                                    lblValue.numberOfLines = 0
                                    lblValue.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblValue)
                                    yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                    
                                    
                                }else{
                                    let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                    textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                    textarea!.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                    textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                    textarea!.textColor = UIColor.darkGray
                                    textarea!.lineColor = UIColor.lightGray
                                    textarea!.isUserInteractionEnabled = false
                                    self.viewDynemicView.addSubview(textarea!)
                                    let btnDate = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                    self.viewDynemicView.addSubview(btnDate)
                                    btnDate.setImage(UIImage(named: "Calendar"), for: .normal)
                                    btnDate.contentHorizontalAlignment = .right
                                    btnDate.imageView?.contentMode = .scaleAspectFit
                                    
                                    btnDate.tag = headerIndex*10000+sectionIndex
                                    btnDate.titleLabel?.tag = controlIndex*10000+0
                                    
                                    if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date){
                                        btnDate.addTarget(self, action: #selector(didTap_btnDate(sender:)), for: .touchUpInside)
                                    }else{
                                        btnDate.addTarget(self, action: #selector(didTap_btnDateTime(sender:)), for: .touchUpInside)
                                    }
                                    yAxis = yAxis + height + extraSpace
                                    
                                }
                            }
                            //MARK:---------URL
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.url){
                                
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                 let lblSectionDetailTitle = UILabel()
                                 lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 lblSectionDetailTitle.text = title
                                 let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                 lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                           
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                
                                let btnUrl = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                btnUrl.titleLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                btnUrl.addTarget(self, action: #selector(didTap_btnURL(sender:)), for: .touchUpInside)
                                
                                btnUrl.tag = headerIndex*10000+sectionIndex
                                btnUrl.titleLabel?.tag = controlIndex*10000+0

                                let attributeString = NSMutableAttributedString(
                                    string: "\(dictSectionControl.value(forKey: "Value")!)",
                                    attributes: yourAttributes
                                )
                                btnUrl.setAttributedTitle(attributeString, for: .normal)
                                self.viewDynemicView.addSubview(btnUrl)
                                yAxis = yAxis + 25.0 + extraSpace

                            }
                            //MARK:---------Dropdown
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.dropdown){
                                if getStatusCompleteWon() {
                                    let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                     let lblSectionDetailTitle = UILabel()
                                     lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                     lblSectionDetailTitle.text = title
                                     let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                     lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                               
                                    lblSectionDetailTitle.numberOfLines = 0
                                    lblSectionDetailTitle.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                    yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                    
                                    let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                    let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                    let aryTempSlectedOption = NSMutableArray()
                                 
                                        for (_, item1) in aryValues.enumerated() {
                                            let strOptionvalue = "\(item1 as AnyObject)"
                                            for (_, item) in aryOption.enumerated() {
                                                let optionDict = (item as AnyObject) as! NSDictionary
                                                let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                                if(strOptionvalue.lowercased() == strValue.lowercased()){
                                                    aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                                }
                                            }
                                        }
                                 
                                    let value = aryTempSlectedOption.componentsJoined(by: ",").replacingOccurrences(of: ",", with: "\n")
                                    let lblValue = UILabel()
                                    lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                    lblValue.text = value
                                    let heightCalculatelblValue = value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                    lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                    lblValue.numberOfLines = 0
                                    lblValue.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblValue)
                                    yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                    
                                    
                                }else{
                                let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                textarea!.textColor = UIColor.darkGray
                                textarea!.lineColor = UIColor.lightGray
                                textarea!.isUserInteractionEnabled = false
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                let aryTempSlectedOption = NSMutableArray()
                             
                                    for (_, item1) in aryValues.enumerated() {
                                        let strOptionvalue = "\(item1 as AnyObject)"
                                        for (_, item) in aryOption.enumerated() {
                                            let optionDict = (item as AnyObject) as! NSDictionary
                                            let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                            if(strOptionvalue.lowercased() == strValue.lowercased()){
                                                aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                            }
                                        }
                                       
                                    }
                             
                                textarea?.text = aryTempSlectedOption.componentsJoined(by: ",")
                                self.viewDynemicView.addSubview(textarea!)
                                let btn = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                btn.setImage(UIImage(named: "arrow_2"), for: .normal)
                                btn.contentHorizontalAlignment = .right
                                btn.imageView?.contentMode = .scaleAspectFit
                                btn.tag = headerIndex*10000+sectionIndex
                                btn.titleLabel?.tag = controlIndex*10000+0
                                btn.addTarget(self, action: #selector(didTap_btnDropDown(sender:)), for: .touchUpInside)
                                self.viewDynemicView.addSubview(btn)
                                yAxis = yAxis + height + extraSpace
                                }
                            }
                            //MARK:---------Multi_select
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.multi_select){
                                
                                if getStatusCompleteWon() {
                                    let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                     let lblSectionDetailTitle = UILabel()
                                     lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                     lblSectionDetailTitle.text = title
                                     let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                     lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                               
                                    lblSectionDetailTitle.numberOfLines = 0
                                    lblSectionDetailTitle.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                    yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                    
                                    let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                    let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                    let aryTempSlectedOption = NSMutableArray()
                                 
                                        for (_, item1) in aryValues.enumerated() {
                                            let strOptionvalue = "\(item1 as AnyObject)"
                                            for (_, item) in aryOption.enumerated() {
                                                let optionDict = (item as AnyObject) as! NSDictionary
                                                let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                                if(strOptionvalue.lowercased() == strValue.lowercased()){
                                                    aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                                }
                                            }
                                        }
                                 
                                    let value = aryTempSlectedOption.componentsJoined(by: ",").replacingOccurrences(of: ",", with: "\n")
                                    let lblValue = UILabel()
                                    lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                    lblValue.text = value
                                    let heightCalculatelblValue = value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                    lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                    lblValue.numberOfLines = 0
                                    lblValue.textColor = UIColor.darkGray
                                    self.viewDynemicView.addSubview(lblValue)
                                    yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                    
                                    
                                }else{
                                
                                let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                textarea!.textColor = UIColor.darkGray
                                textarea!.lineColor = UIColor.lightGray
                                textarea!.isUserInteractionEnabled = false
                            
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                let aryTempSlectedOption = NSMutableArray()
                             
                                    for (_, item1) in aryValues.enumerated() {
                                        let strOptionvalue = "\(item1 as AnyObject)"
                                        for (_, item) in aryOption.enumerated() {
                                            let optionDict = (item as AnyObject) as! NSDictionary
                                            let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                            if(strOptionvalue.lowercased() == strValue.lowercased()){
                                                aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                            }
                                        }
                                    }
                                textarea?.text = aryTempSlectedOption.componentsJoined(by: ",")
                                self.viewDynemicView.addSubview(textarea!)
                                let btn = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                btn.setImage(UIImage(named: "Next2"), for: .normal)
                                btn.contentHorizontalAlignment = .right
                                btn.imageView?.contentMode = .scaleAspectFit
                                btn.tag = headerIndex*10000+sectionIndex
                                btn.titleLabel?.tag = controlIndex*10000+0
                                btn.addTarget(self, action: #selector(didTap_btnDropDownMultiSelection(sender:)), for: .touchUpInside)
                                
                                self.viewDynemicView.addSubview(btn)

                                yAxis = yAxis + height + extraSpace
                                }
                            }
                        }
                    }
                }
                
                //MARK:------------End Section View ---------
            }
        }
        loadInspectionFormBuilderData()
        if aryFormBuilder.count != 0 {
            for (headerIndex, item) in aryFormBuilder.enumerated() {
                print("headerIndex---------------\(headerIndex)")
                //MARK:-------------Start Header View ---------
                let viewHeader = UIView.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width), height: Int(height)))
                viewHeader.backgroundColor = SalesNewColorCode.darkBlack
                let lblHeaderTitle = UILabel.init(frame: CGRect(x: 40, y: 4, width: Int(self.viewDynemicView.frame.width) - 80 , height: Int(height) - 8))
                lblHeaderTitle.textAlignment = .center
                lblHeaderTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                
                var strTitle = ""
                if getStatusCompleteWon() {
                    strTitle = "\((item as AnyObject).value(forKey: "title") ?? "")"
                }else{
                    strTitle = "\((item as AnyObject).value(forKey: "templateName") ?? "")"
                }
                
                lblHeaderTitle.text = strTitle//"\(dictMAIN.value(forKey: "DepartmentName")!)"
                lblHeaderTitle.numberOfLines = 2
                lblHeaderTitle.textColor = UIColor.white
                viewHeader.addSubview(lblHeaderTitle)
                let btnHeaderDetail = UIButton.init(frame: CGRect(x: 15, y: 0, width: Int(self.viewDynemicView.frame.width), height: Int(height)))
               // btnHeaderDetail.setTitle(dictMAIN.value(forKey: "viewExpand") as! Bool ? "-" : "+", for: .normal)
                btnHeaderDetail.setTitle("+", for: .normal)
                btnHeaderDetail.titleLabel?.font = UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 28.0 : 22.0)
                btnHeaderDetail.tag = headerIndex
                btnHeaderDetail.addTarget(self, action: #selector(didTap_btnHeaderFormBuilderDetail(sender:)), for: .touchUpInside)
                btnHeaderDetail.contentHorizontalAlignment = .left
                btnHeaderDetail.tag = headerIndex
                viewHeader.addSubview(btnHeaderDetail)
                self.viewDynemicView.addSubview(viewHeader)
                yAxis = yAxis + height + extraSpace
            }
        }
        
        heightDynemicView.constant = CGFloat(yAxis + 50)
    }
    
    
    //MARK: -------------------------------------
    //MARK:------------FormBuilderAction ---------

    //MARK: ---Header Action
    @objc func didTap_btnHeaderFormBuilderDetail(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag
      
        if getStatusCompleteWon() {
            let objselect = (aryFormBuilder.object(at: headerTag)as! NSDictionary)
            self.goToFormBuilder(dict: objselect as! [String : Any], strPdfPath: "\(objselect["fileName"] ?? "")")
        }else{
            let objselect = (aryFormBuilder.object(at: headerTag)as! NSManagedObject)
            let dictSelected = objselect.toDict()
            dictSelectedGlobal = dictSelected
            if isInternetAvailable() {
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(self.loader, animated: false, completion: nil)
                
                //Nilnd
                let arrImageCRM = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isImageSyncforMobile == %@ && isInternalUsage == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false", "false"))
                if arrImageCRM.count>0
                {
                    uploadCRMImages(arrImageCRM: arrImageCRM)
                }
                else
                {
                    self.fetchImageFromDB(dict: dictSelected)
                }
                
                //self.fetchImageFromDB(dict: dictSelected)
                
            }else{
                
                Global().alertMethod(Alert, ErrorInternetMsg)
            }
        }
        //let post_paramsValue = dictSelected
    }
   
    
    //MARK: -------------------------------------
    //MARK:------------DynemicFormAction ---------

    //MARK: ---Header Action
    @objc func didTap_btnHeaderDetail(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag
        let dictselect = (aryDynemicForm.object(at: headerTag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        (dictselect.value(forKey: "viewExpand")as! Bool) ? dictselect.setValue(false, forKey: "viewExpand") : dictselect.setValue(true, forKey: "viewExpand")
        aryDynemicForm.replaceObject(at: sender.tag, with: dictselect)
        CreatDynemicView(aryData: aryDynemicForm)
    }
   
    //MARK: -----Checkbox Action
    @objc func didTap_btnCheckbox(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag/10000
        let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
        let dictControl = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
        var strValue = ""
        if("\(dictControl.value(forKey: "Value")!)".lowercased() == "true"){
            strValue = "false"
        }else{
            strValue = "true"
        }
        
        
         updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: "\(strValue)")


    }
    //MARK: -----CheckboxCombo Action
    @objc func didTap_btnCheckboxCombo(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag/10000
         let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
   
        let dictControl = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
        let aryValueControl = ("\(dictControl.value(forKey: "Value")!)".split(separator: ",")as NSArray).mutableCopy()as! NSMutableArray

        let dictOption = getValueFromSub_Control(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
        let strValue = "\(dictOption.value(forKey: "Value")!)"
       
        if(sender.currentImage == UIImage(named: "NPMA_UnCheck")){
            if !aryValueControl.contains(strValue) {
                aryValueControl.add(strValue)
            }
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }else{
            aryValueControl.remove(strValue)
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }
        
        let strSelectedOption = aryValueControl.componentsJoined(by: ",")
        updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: strSelectedOption)

    }
    //MARK: -----Radio Action
    @objc func didTap_btnRadio(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag/10000
        let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
        let dictOption = getValueFromSub_Control(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
        var strValue = ""

        if(sender.currentImage == UIImage(named: "redio_2")){
            sender.setImage(UIImage(named: "redio_1"), for: .normal)
            strValue = ""
        }else{
            sender.setImage(UIImage(named: "redio_2"), for: .normal)
            strValue = "\(dictOption.value(forKey: "Value")!)"

        }
        updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: strValue)


    }
    //MARK: -----RadioCombo Action
    @objc func didTap_btnRadioCombo(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag/10000
        let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
        let dictOption = getValueFromSub_Control(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
        var strValue = ""

        if(sender.currentImage == UIImage(named: "redio_2")){
            sender.setImage(UIImage(named: "redio_1"), for: .normal)
            strValue = ""
        }else{
            sender.setImage(UIImage(named: "redio_2"), for: .normal)
            strValue = "\(dictOption.value(forKey: "Value")!)"

        }
        updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: strValue)

    }
    //MARK: -----Date Action
    @objc func didTap_btnDate(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag/10000
        let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let dictOption = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
        let strValue = "\(dictOption.value(forKey: "Value")!)"
        let fromDate = formatter.date(from:((strValue.count) > 0 ? strValue : result))!
        
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select  Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: formatter.string(from: datePicker.date))
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    //MARK: -----DateTime Action
    @objc func didTap_btnDateTime(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag/10000
        let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
       
        let dictOption = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
        let strValue = "\(dictOption.value(forKey: "Value")!)"
        
        let fromDate = formatter.date(from:((strValue.count) > 0 ? strValue : result))!

        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Date Time")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            
            updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: formatter.string(from: datePicker.date))

        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    //MARK: -----URL Action
    @objc func didTap_btnURL(sender : UIButton) {
        self.view.endEditing(true)

        let headerTag = sender.tag/10000
        let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
        
        let dictOption = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)

        let strValue = "\(dictOption.value(forKey: "Value")!)"
        if(strValue.count > 0){
            var safariVC: SFSafariViewController?
            safariVC = SFSafariViewController(url: URL(string: strValue)!)
            safariVC!.delegate = self
            self.present(safariVC!, animated: true, completion: nil)
        }
        
    }
    //MARK: -----DropDown Action
    @objc func didTap_btnDropDown(sender : UIButton) {
        self.view.endEditing(true)

        tagForDropDownHeaderSection = sender.tag
        tagForDropDownControlSubcontrol = sender.titleLabel!.tag
        
        let headerTag = sender.tag/10000
        let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
        
        let dictOption = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
        let aryOption = (dictOption.value(forKey: "Options")as! NSArray).mutableCopy()as!NSMutableArray
        let arySelectedType = NSMutableArray()
        let aryValue = "\(dictOption.value(forKey: "Value")!)".split(separator: ",") as NSArray
        for item in aryValue {
            let strvalue = "\(item)"
            for item in aryOption {
                let strvalueOption =  "\(((item as AnyObject) as! NSDictionary).value(forKey: "Value")!)"
                if strvalueOption ==  strvalue{
                    arySelectedType.add(item)
                }
            }
        }
        if(aryOption.count != 0){
            openTableViewPopUp(tag: 59, ary: (aryOption as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: arySelectedType)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
    }
    //MARK: -----DropDown MultiSelection Action
    @objc func didTap_btnDropDownMultiSelection(sender : UIButton) {
        self.view.endEditing(true)

        tagForDropDownHeaderSection = sender.tag
        tagForDropDownControlSubcontrol = sender.titleLabel!.tag
        
        let headerTag = sender.tag/10000
        let sectionTag = sender.tag%10000
        let controlTag = sender.titleLabel!.tag/10000
        let subControlTag = sender.titleLabel!.tag%10000
        
        let dictOption = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)
       
        let arySelectedType = NSMutableArray()
        let aryOption = (dictOption.value(forKey: "Options")as! NSArray).mutableCopy()as!NSMutableArray
        let aryValue = "\(dictOption.value(forKey: "Value")!)".split(separator: ",") as NSArray
        for item in aryValue {
            let strvalue = "\(item)"
            for item in aryOption {
                let strvalueOption =  "\(((item as AnyObject) as! NSDictionary).value(forKey: "Value")!)"
                if strvalueOption ==  strvalue{
                    arySelectedType.add(item)
                }
            }
        }
        
        if(aryOption.count != 0){
            goToNewSalesSelectionVC(arrItem: aryOption, arrSelectedItem: arySelectedType, tag: 1, titleHeader: "Select", isMultiSelection: true, ShowNameKey: "Name")
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
        
    }
    //MARK: ------------------------------------
    //MARK: -----Update And GetValue

    
    func updateValue(headertag : Int , sectionTag : Int , controlTag : Int , subCOntrolTag : Int , value : String)  {
        
        let dictHeader = aryDynemicForm.object(at: headertag)as! NSDictionary
        let arySections = (dictHeader.value(forKey: "Sections")as! NSArray).mutableCopy() as! NSMutableArray
        if(arySections.count != 0){
            let dictControl = (arySections.object(at: sectionTag)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            let aryControls = (dictControl.value(forKey: "Controls")as! NSArray).mutableCopy()as!NSMutableArray
            if(aryControls.count != 0){
                let dictOption = (aryControls.object(at: controlTag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                dictOption.setValue(value, forKey: "Value")
                aryControls.replaceObject(at: controlTag, with: dictOption)
                dictControl.setValue(aryControls, forKey: "Controls")
                arySections.replaceObject(at: sectionTag, with: dictControl)
                dictHeader.setValue(arySections, forKey: "Sections")
                aryDynemicForm.replaceObject(at: headertag, with: dictHeader)
               
                switch "\(dictOption.value(forKey: "Element") ?? "")".lowercased() {
                case DynemicFormCheck.textbox:
                    break
                case DynemicFormCheck.email:
                    break
                case DynemicFormCheck.phone:
                    break
                case DynemicFormCheck.currency:
                    break
                case DynemicFormCheck.decimal:
                    break
                case DynemicFormCheck.number:
                    break
                case DynemicFormCheck.autonum:
                    break
                case DynemicFormCheck.precent:
                    break
                case DynemicFormCheck.upload:
                   break
                case DynemicFormCheck.textarea:
                   break
                case DynemicFormCheck.paragraph:
                   break
                default:
                    CreatDynemicView(aryData: aryDynemicForm)
                }

            }
        }
        
        
    }
  
    func getValueFromControl(headertag : Int , sectionTag : Int , controlTag : Int , subCOntrolTag : Int) -> NSMutableDictionary {
        let dictHeader = aryDynemicForm.object(at: headertag)as! NSDictionary
        let arySections = (dictHeader.value(forKey: "Sections")as! NSArray).mutableCopy() as! NSMutableArray
        if(arySections.count != 0){
            let dictControl = (arySections.object(at: sectionTag)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            let aryControls = (dictControl.value(forKey: "Controls")as! NSArray).mutableCopy()as!NSMutableArray
            if(aryControls.count != 0){
                let dictOption = (aryControls.object(at: controlTag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
               return dictOption
            }
        }
      return NSMutableDictionary()
    }
    func getValueFromSub_Control(headertag : Int , sectionTag : Int , controlTag : Int , subCOntrolTag : Int) -> NSMutableDictionary {
        let dictHeader = aryDynemicForm.object(at: headertag)as! NSDictionary
        let arySections = (dictHeader.value(forKey: "Sections")as! NSArray).mutableCopy() as! NSMutableArray
        if(arySections.count != 0){
            let dictControl = (arySections.object(at: sectionTag)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            let aryControls = (dictControl.value(forKey: "Controls")as! NSArray).mutableCopy()as!NSMutableArray
            if(aryControls.count != 0){
                let dictOption = (aryControls.object(at: controlTag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let aryOption = (dictOption.value(forKey: "Options")as! NSArray).mutableCopy()as!NSMutableArray
                if(aryOption.count != 0){
                    let dictOption = (aryOption.object(at: subCOntrolTag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    return dictOption

                }
            }
        }
      return NSMutableDictionary()
    }
    
    //
    //MARK: - Form Builder Changes

    //SalesInspectionFormBuilder - Saavan April 2022
    //let post_paramsValue = dictSalesLeadResponseLocal as! Dictionary<String,Any>
    
    func fetchAllImagesFromDBAfterSync(dict: [String: Any]) {
        
        self.arrImageDetails.removeAll()
        self.arrGraphDetails.removeAll()
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strLeadIDL =  "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")"
        let strServiceUrlMainL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        let strServiceCRMUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"

        let arrayOfImageDataBefore = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isInternalUsage == %@", strLeadIDL, "Before", "false"))
        
        let arrayOfImageDataGraph = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isInternalUsage == %@", strLeadIDL, "Graph", "false"))
        
        let arrImageCRM = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isInternalUsage == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, "false"))


        let arrImageBefore_CRM = arrayOfImageDataBefore + arrImageCRM
        
        let arrayOfImageData = arrayOfImageDataBefore + arrayOfImageDataGraph
        
        if arrayOfImageData.count > 0 {
            
            for i in 0..<arrayOfImageData.count {
                
                let objTemp: NSManagedObject = arrayOfImageData[i] as! NSManagedObject
                print(objTemp)
                
                let strImagePath = objTemp.value(forKey: "leadImagePath")
                
                let strCap = objTemp.value(forKey: "leadImageCaption")
                let strDec = objTemp.value(forKey: "descriptionImageDetail")
                
                let imgPath: String = strImagePath as! String//(object[i].relativePath ?? "") + (object[i].name ?? "")
                let imageUrl = "\(strServiceUrlMainL)\(imgPath)"
                
                var strUrl = String()
                strUrl = strServiceUrlMainL
                          
                if imgPath.contains("Documents"){
                    strUrl = strUrl + imgPath
                }else{
                    strUrl = strUrl + "//Documents/" + imgPath
                }
                
                strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                
                var dict: [String: Any] = [:]
                dict.removeAll()
                dict["image"]       = strUrl
                dict["order"]       = i + 1
                dict["caption"]     = strCap as! String
                dict["description"] = strDec as! String
                
                
                if "\(objTemp.value(forKey: "leadImageType") ?? "")" == "Before" {
                    //self.arrImageDetails.append(dict)
                }else {
                    //self.arrGraphDetails.append(dict)
                }
            }
        }
        
        
        //Nilind Image
        
        if arrImageBefore_CRM.count > 0 {  //arrayOfImageDataBefore
            
            for i in 0..<arrImageBefore_CRM.count {
                
                let objTemp: NSManagedObject = arrImageBefore_CRM[i] as! NSManagedObject //arrayOfImageDataBefore
                print(objTemp)
                
                if objTemp.isKind(of: ImageDetail.self)
                {
                    let strImagePath = objTemp.value(forKey: "leadImagePath")
                    
                    let strCap = objTemp.value(forKey: "leadImageCaption")
                    let strDec = objTemp.value(forKey: "descriptionImageDetail")
                    
                    let imgPath: String = strImagePath as! String//(object[i].relativePath ?? "") + (object[i].name ?? "")
                    let imageUrl = "\(strServiceUrlMainL)\(imgPath)"
                    
                    var strUrl = String()
                    strUrl = strServiceUrlMainL
                              
                    if imgPath.contains("Documents"){
                        strUrl = strUrl + imgPath
                    }else{
                        strUrl = strUrl + "//Documents/" + imgPath
                    }
                    
                    strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                    strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                    
                    var dict: [String: Any] = [:]
                    dict.removeAll()
                    dict["image"]       = strUrl
                    dict["order"]       = i + 1
                    dict["caption"]     = strCap as! String
                    dict["description"] = strDec as! String
                    
                    
                    if "\(objTemp.value(forKey: "leadImageType") ?? "")" == "Before" {
                        self.arrImageDetails.append(dict)
                    }else {
                        //self.arrGraphDetails.append(dict)
                    }
                }
                else //CRM Image Detail
                {
                    let strImagePath = objTemp.value(forKey: "path")
                    
                    let strCap = objTemp.value(forKey: "cRMImageCaption")
                    let strDec = objTemp.value(forKey: "cRMImageDescription")
                    
                    let imgPath: String = strImagePath as! String//(object[i].relativePath ?? "") + (object[i].name ?? "")
                    let imageUrl = "\(strServiceCRMUrlMain)\(imgPath)"
                    
                    var strUrl = String()
                    strUrl = strServiceCRMUrlMain
                              
//                    if imgPath.contains("Documents"){
//                        strUrl = strUrl + imgPath
//                    }else{
//                        strUrl = strUrl + "//Documents/" + imgPath
//                    }
                    strUrl = strUrl + "//Documents/" + imgPath
                    strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                    strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                    
                    var dict: [String: Any] = [:]
                    dict.removeAll()
                    dict["image"]       = strUrl
                    dict["order"]       = i + 1
                    dict["caption"]     = strCap as! String
                    dict["description"] = strDec as! String
                    
                    
                    self.arrImageDetails.append(dict)
                    
//                    if "\(objTemp.value(forKey: "leadImageType") ?? "")" == "Before" {
//                        //self.arrImageDetails.append(dict)
//                    }else {
//                        //self.arrGraphDetails.append(dict)
//                    }
                }
                

            }
        }
        
        //Nilind Graph
        
        if arrayOfImageDataGraph.count > 0 {
            
            for i in 0..<arrayOfImageDataGraph.count {
                
                let objTemp: NSManagedObject = arrayOfImageDataGraph[i] as! NSManagedObject
                print(objTemp)
                
                let strImagePath = objTemp.value(forKey: "leadImagePath")
                
                let strCap = objTemp.value(forKey: "leadImageCaption")
                let strDec = objTemp.value(forKey: "descriptionImageDetail")
                
                let imgPath: String = strImagePath as! String//(object[i].relativePath ?? "") + (object[i].name ?? "")
                let imageUrl = "\(strServiceUrlMainL)\(imgPath)"
                
                var strUrl = String()
                strUrl = strServiceUrlMainL
                          
                if imgPath.contains("Documents"){
                    strUrl = strUrl + imgPath
                }else{
                    strUrl = strUrl + "//Documents/" + imgPath
                }
                
                strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                
                var dict: [String: Any] = [:]
                dict.removeAll()
                dict["image"]       = strUrl
                dict["order"]       = i + 1
                dict["caption"]     = strCap as! String
                dict["description"] = strDec as! String
                
                
                if "\(objTemp.value(forKey: "leadImageType") ?? "")" == "Before" {
                    //self.arrImageDetails.append(dict)
                }else {
                    self.arrGraphDetails.append(dict)
                }
            }
        }
        
        
        
        self.goToFormBuilder(dict: dict, strPdfPath: "")
    }
    
    func fetchImageFromDB(dict: [String: Any]) {

        self.arrImageDetails.removeAll()
        self.arrGraphDetails.removeAll()
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strLeadIDL =  "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")"
        let strServiceUrlMainL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"

        let arrayOfImageDataBefore = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isImageSyncforMobile == %@ && isInternalUsage == %@" , strLeadIDL, "Before", "false", "false"))
        
        let arrayOfImageDataGraph = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isImageSyncforMobile == %@ && isInternalUsage == %@", strLeadIDL, "Graph", "false", "false"))

        let arrayOfImageData = arrayOfImageDataBefore + arrayOfImageDataGraph
        
        print(arrayOfImageData)
        if arrayOfImageData.count > 0
        {
            
            var arrImgName: [String] = []
            var arrImgCaption: [String] = []
            var arrImgDescription: [String] = []
            
            for imgName in arrayOfImageData {
                let objTemp: NSManagedObject = imgName as! NSManagedObject
                print(objTemp)
                
                let strImagePath = objTemp.value(forKey: "leadImagePath")
                arrImgName.append(strImagePath as! String)
                
                let strCap = objTemp.value(forKey: "leadImageCaption")
                let strDec = objTemp.value(forKey: "descriptionImageDetail")
                arrImgCaption.append(strCap as! String)
                arrImgDescription.append(strDec as! String)
            }
            print(arrImgName)
            
            let arrOfImages = self.getUIImageFromString(arrOfImagesString : arrImgName)
            
            ServiceGalleryService().uploadImageForGeneralInfo(_strServiceUrlMainServiceAutomation: strServiceUrlMainL, arrOfImages, arrImgName) { (object, error) in
                if let object = object {
                    print(object)
                    
                    for i in 0..<object.count {
                        
                        let imgPath: String = (object[i].relativePath ?? "") + (object[i].name ?? "")
                        let imageUrl = "\(strServiceUrlMainL)\(imgPath)"
                        
                        var strUrl = String()
                        strUrl = strServiceUrlMainL
                                  
                        if imgPath.contains("Documents"){
                            strUrl = strUrl + imgPath
                        }else{
                            strUrl = strUrl + "//Documents/" + imgPath
                        }
                        strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                        strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                        
                        // save back in DB after Syncing.
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("leadImagePath")
                        arrOfKeys.add("isImageSyncforMobile")

                        arrOfValues.add(imgPath)
                        arrOfValues.add("true")
                      
                        let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", strLeadIDL,"\(object[i].name ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess
                        {
                            // Image updated in DB.
                        }
                        
                        if i == object.count - 1 {
                            // loop completed go to fetch all images and redirect to form builder page
                            self.fetchAllImagesFromDBAfterSync(dict: dict)
                        }

                    }
                } else {
                    print(error ?? "")
                    self.loader.dismiss(animated: false) {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }
            }
        }
        else
        {
            self.fetchAllImagesFromDBAfterSync(dict: dict)
        }
    }
    
    func getUIImageFromString(arrOfImagesString: [String]) -> [UIImage]{
        var arrOfUIImages = [UIImage]()
        
        for i in 0..<arrOfImagesString.count{
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: arrOfImagesString[i])
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: arrOfImagesString[i])
            
            if isImageExists == true{
                arrOfUIImages.append(image!)
            }
            
        }
        
        return arrOfUIImages
    }
    
    func goToFormBuilder(dict: [String: Any], strPdfPath:String) {
        
        self.loader.dismiss(animated: false) {}

        self.getLeadDetails()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = UIStoryboard.init(name: "SalesFormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesFormBuilderVC") as? SalesFormBuilderVC
            vc?.dictTemplateObj = dict
            vc!.matchesGeneralInfo = self.objLeadDetails!
            vc?.arrImageDetails = self.arrImageDetails
            vc?.arrGraphDetails = self.arrGraphDetails
            print("Before Images sent to form builder ==== %@",self.arrImageDetails)
            print("Graph Images sent to form builder ==== %@",self.arrGraphDetails)
            vc?.strSavedPdfUrl = strPdfPath
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    func getLeadDetails() {
        let strLeadIDL =  "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")"
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strLeadIDL))
        if(arrayLeadDetail.count > 0)
        {
            objLeadDetails = (arrayLeadDetail.firstObject as! NSManagedObject)
        }
    }
    
    //MARK: -------------------- CRM Image Upload ----------------

    
    func uploadCRMImages(arrImageCRM: NSArray)  {
        self.countCRMImage = 0
        
        let aryCRMImagesList = NSMutableArray()
        
        for item in arrImageCRM
        {
            let match = item as! NSManagedObject
            aryCRMImagesList.add("\(match.value(forKey: "path") ?? "")")

        }
        
        if aryCRMImagesList.count != 0 {
            
            let arrImageData = NSMutableArray()
            let arrImageName = NSMutableArray()

            for item in aryCRMImagesList {
                let strImage = "\(item)"
                arrImageData.add(getImage(imagePath: strImage))
                arrImageName.add(strImage)
            }
           
            for item in aryCRMImagesList
            {
                //self.dispatchGroupSales.enter()
                
                let strImage = "\(item)"
                ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Gallery.uploadCRMImagesSingle)") { (response, status) in
                    
                    //self.dispatchGroupSales.leave()
                    self.countCRMImage = self.countCRMImage + 1
                    print(response ?? "")
                    
                    if aryCRMImagesList.count == self.countCRMImage
                    {
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        for item in aryCRMImagesList
                        {
                            let imgPath = item as! String
        
                            arrOfKeys.add("path")
                            arrOfKeys.add("isImageSyncforMobile")

                            arrOfValues.add(imgPath)
                            arrOfValues.add("true")
                          
                            let isSuccess =  getDataFromDbToUpdate(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && path == %@", "\(self.matchesGeneralInfo.value(forKey: "leadId") ?? "")",imgPath), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                            
                            if isSuccess
                            {
                                // Image updated in DB.
                            }
                        }

                        self.fetchImageFromDB(dict: self.dictSelectedGlobal)
                    }
                }
            }
        }

    }
    
    func getImage(imagePath : String)-> UIImage{
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: imagePath){
            return UIImage(contentsOfFile: imagePath)!
        }else{
            return UIImage()
        }
    }
}


//MARK: -------------------------------------
//MARK: DynemicFormCheck


enum DynemicFormCheck {
    static let checkbox = "el-checkbox"
    static let checkbox_combo = "el-checkbox-combo"
    static let radio = "el-radio"
    static let radio_combo = "el-radio-combo"
    static let textarea = "el-textarea"
    static let paragraph = "el-paragraph"

    static let textbox = "el-textbox"
    static let email = "el-email"
    static let phone = "el-phone"
    static let currency = "el-currency"
    static let decimal = "el-decimal"
    static let number = "el-number"
    static let autonum = "el-auto-num"
    static let precent = "el-precent"
    static let upload = "el-upload"
    static let date = "el-date"
    static let date_time = "el-date-time"
    static let url = "el-url"
    static let dropdown = "el-dropdown" //19
    static let multi_select = "el-multi-select"
    static let parag = "el-para"


}
//MARK: -------------------------------------
//MARK: -----SFSafariViewControllerDelegate

extension SalesNewFlow_InspectionVC : SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true) { () -> Void in
                print("You just dismissed the login view.")
            }
        }

    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
            print("didLoadSuccessfully: \(didLoadSuccessfully)")

        }
}
//MARK: -------------------------------------
//MARK: -----UITextViewDelegate

extension SalesNewFlow_InspectionVC : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        let headerTag = textView.tag/10000
        let sectionTag = textView.tag%10000
        let controlTag = textView.placeholderLabel!.tag/10000
        let subControlTag = textView.placeholderLabel!.tag%10000
        updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: "\(textView.text!)")
    }
}

//MARK: -------------------------------------
//MARK: -----UITextFieldDelegate

extension SalesNewFlow_InspectionVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let headerTag = textField.tag/10000
        let sectionTag = textField.tag%10000
        let controlTag = textField.leftView!.tag/10000
        let subControlTag = textField.leftView!.tag%10000
        let dictControl = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)

        
        switch "\(dictControl.value(forKey: "Element") ?? "")".lowercased() {
       
        case DynemicFormCheck.email:
            
            if textField.text?.count != 0{
                let isValid = Global().checkIfCommaSepartedEmailsAreValid(textField.text!)

                if !isValid {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid email!", viewcontrol: self)
                }
            }
            print("DynemicFormCheck.phone")
           
        case DynemicFormCheck.phone:
            
            if textField.text?.count != 0{
                if textField.text!.count < 12 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid phone!", viewcontrol: self)
                }
            }
            print("DynemicFormCheck.phone")
            
        default:
            break
        }
        
        updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: "\(textField.text!)")

    }
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let headerTag = textField.tag/10000
        let sectionTag = textField.tag%10000
        let controlTag = textField.leftView!.tag/10000
        let subControlTag = textField.leftView!.tag%10000
        let dictControl = getValueFromControl(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag)

        if (range.location == 0) && (string == " ")
        {
            return false
        }
        switch "\(dictControl.value(forKey: "Element") ?? "")".lowercased() {
        case DynemicFormCheck.textbox:
            print("DynemicFormCheck.textbox")
        case DynemicFormCheck.email:
            print("DynemicFormCheck.email")
        case DynemicFormCheck.phone:
            print("DynemicFormCheck.phone")
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = textField.text!
                strTextt = String(strTextt.dropLast())
                textField.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        case DynemicFormCheck.currency:
            print("DynemicFormCheck.currency")
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 35)
          
            return isValidd

        case DynemicFormCheck.decimal:
            print("DynemicFormCheck.decimal")
            return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 35)

        case DynemicFormCheck.number:
            print("DynemicFormCheck.number")
           return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 35)
        case DynemicFormCheck.autonum:
            print("DynemicFormCheck.autonum")
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 35)

        case DynemicFormCheck.precent:
            print("DynemicFormCheck.precent")
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 35)

        case DynemicFormCheck.upload:
            print("DynemicFormCheck.upload")
        default:
            break
        }

  

        return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 85)
    }
    
}
// MARK: --------------------------
// MARK: - Selection CustomTableView


extension SalesNewFlow_InspectionVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 59)
        {
            if(dictData.count != 0){
                let headerTag = tagForDropDownHeaderSection/10000
                let sectionTag = tagForDropDownHeaderSection%10000
                let controlTag = tagForDropDownControlSubcontrol/10000
                let subControlTag = tagForDropDownControlSubcontrol%10000
                let value = "\(dictData.value(forKey: "Value")!)"
                updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: "\(value)")
            }else{
                let headerTag = tagForDropDownHeaderSection/10000
                let sectionTag = tagForDropDownHeaderSection%10000
                let controlTag = tagForDropDownControlSubcontrol/10000
                let subControlTag = tagForDropDownControlSubcontrol%10000
                updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: "")
            }
        }
    }
}
// MARK: ------------------------------
// MARK: - SalesNew_SelectionProtocol


extension SalesNewFlow_InspectionVC: SalesNew_SelectionProtocol
{
  
    func didSelect(aryData: NSMutableArray, tag: Int) {
        //-----Tag------
        if(tag == 1){
            if aryData.count != 0 {
                let headerTag = tagForDropDownHeaderSection/10000
                let sectionTag = tagForDropDownHeaderSection%10000
                let controlTag = tagForDropDownControlSubcontrol/10000
                let subControlTag = tagForDropDownControlSubcontrol%10000
                let aryValue = NSMutableArray()
                for item in aryData {
                    let value = "\(((item as AnyObject)as! NSDictionary).value(forKey: "Value")!)"
                    aryValue.add(value)
                }
                let strValue = aryValue.componentsJoined(by: ",")
                updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: "\(strValue)")
            }else{
                let headerTag = tagForDropDownHeaderSection/10000
                let sectionTag = tagForDropDownHeaderSection%10000
                let controlTag = tagForDropDownControlSubcontrol/10000
                let subControlTag = tagForDropDownControlSubcontrol%10000
      
                updateValue(headertag: headerTag, sectionTag: sectionTag, controlTag: controlTag, subCOntrolTag: subControlTag, value: "")
            }
        }
    }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        
    }
}


extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        if boundingBox.height < 30.0{
            return 30.0
        }
        return boundingBox.height + 10
    }
}
extension SalesNewFlow_InspectionVC: BackRefreshFormBuilderProtocol
{
    
    func refreshView(data: Any) {
        self.loadDynamicForm()
    }
    
}
extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: Set<Iterator.Element> = []
        return filter { seen.insert($0).inserted }
    }
}
