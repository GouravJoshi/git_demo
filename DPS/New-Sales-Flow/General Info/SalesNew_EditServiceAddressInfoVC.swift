//
//  SalesNew_EditServiceAddressInfoVC.swift
//  DPS
//
//  Created by NavinPatidar on 5/14/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_EditServiceAddressInfoVC: UIViewController {

    //MARK:---------IBOutlet-----------
    //MARK:
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var tf_PropertyType: ACFloatingTextField!
    @IBOutlet weak var tf_ServiceAddressSubType: ACFloatingTextField!

    @IBOutlet weak var tf_ServiceAddress: ACFloatingTextField!
    @IBOutlet weak var tf_County: ACFloatingTextField!
    @IBOutlet weak var tf_ServiceMapCode: ACFloatingTextField!
    @IBOutlet weak var tf_ServiceGateCode: ACFloatingTextField!
    @IBOutlet weak var tf_SchoolDistrict: ACFloatingTextField!
    @IBOutlet weak var tf_TaxCode: ACFloatingTextField!
    @IBOutlet weak var txt_AddressNote: UITextView!
    @IBOutlet weak var txt_Direction: UITextView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewUserdefinefiled: UIView!
    @IBOutlet weak var height_viewUserdefinefiled: NSLayoutConstraint!

    //MARK:---------Variable-----------
    //MARK:
    var strTitle = ""
    var aryPropertySelected = NSMutableArray(), aryTaxCodeSelected = NSMutableArray(),arySelectedType = NSMutableArray()
    var dataGeneralInfo = NSManagedObject()
    
    var strTax = ""

    // MARK: - ----------- Google Address Code ----------------
    
    @IBOutlet weak var scrollView: UIScrollView!
    var tableView =  UITableView()

    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var radius: Double = 0.0
    private var strictBounds: Bool = false
    var txtAddressMaxY = CGFloat()

    var strAddressLine1 = ""
    var strAddressLine2 = ""
    var strStateId = ""
    var strCityName = ""
    var strZipCode = ""
    var strAddressId = ""
    
    private var places = [Place]() {
        didSet { tableView.reloadData() }
    }
    //MARK:---------LifeCycle-----------
    //MARK:
    override func viewDidLoad() {
        super.viewDidLoad()
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.manageScrollHeight(notification:)), name: Notification.Name("UDF"), object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        setInitialUI()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
    }
    //MARK:-------Extra--Function-----------
    //MARK:
   
    func setInitialUI() {
        apiKey = "\((nsud.value(forKey: "LoginDetails") as! NSDictionary).value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"

        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = UIColor.groupTableViewBackground
        tableView.layer.cornerRadius = 8.0
        lblTitle.text = strTitle
      
        txt_AddressNote.layer.cornerRadius = 8.0
        txt_AddressNote.layer.borderWidth = 1.0
        txt_AddressNote.layer.borderColor = UIColor.lightGray.cgColor
        
        txt_Direction.layer.cornerRadius = 8.0
        txt_Direction.layer.borderWidth = 1.0
        txt_Direction.layer.borderColor = UIColor.lightGray.cgColor
        
        tf_PropertyType.delegate = self
        tf_ServiceAddress.delegate = self
        tf_County.delegate = self
        tf_ServiceMapCode.delegate = self
        tf_ServiceGateCode.delegate = self
        tf_SchoolDistrict.delegate = self
        tf_TaxCode.delegate = self
        txt_AddressNote.delegate = self
        txt_Direction.delegate = self
        
        
        //Set Service address Detail Info
        let aryAddressPropertyTypeSysName = "\(dataGeneralInfo.value(forKey: "serviceAddressPropertyTypeSysName") ?? "")".split(separator: ",")
        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryAddressPropertyTypeSysName as NSArray, getmasterName: "MasterSalesAutomation", getkeyName: "AddressPropertyTypeMaster", keyName: "Name", keySysName: "SysName") { [self] (result, name, sysname) in
            tf_PropertyType.text = name
            self.aryPropertySelected = result.mutableCopy()as! NSMutableArray
        }
        
        tf_ServiceAddress.text = Global().strCombinedAddressService(for: dataGeneralInfo) ?? ""
        tf_County.text = "\(dataGeneralInfo.value(forKey: "serviceCounty") ?? "")"
        tf_ServiceMapCode.text = "\(dataGeneralInfo.value(forKey: "serviceMapCode") ?? "")"
        tf_ServiceGateCode.text = "\(dataGeneralInfo.value(forKey: "serviceGateCode") ?? "")"
        tf_SchoolDistrict.text = "\(dataGeneralInfo.value(forKey: "serviceSchoolDistrict") ?? "")"
       
        let aryTaxCodeSysName = "\(dataGeneralInfo.value(forKey: "taxSysName") ?? "")".split(separator: ",")
        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryTaxCodeSysName as NSArray, getmasterName: "MasterSalesAutomation", getkeyName: "TaxMaster", keyName: "Name", keySysName: "SysName") { [self] (result, name, sysname) in
            tf_TaxCode.text = name
            aryTaxCodeSelected = result.mutableCopy()as! NSMutableArray
        }
        txt_Direction.text = "\(dataGeneralInfo.value(forKey: "serviceDirection") ?? "")"
        txt_AddressNote.text =  "\(dataGeneralInfo.value(forKey: "serviceNotes") ?? "")"
       
        
        if("\(dataGeneralInfo.value(forKey: "serviceAddressSubType") ?? "")" == ""){
            self.arySelectedType = NSMutableArray()
            self.arySelectedType.add(["Name":"Residential", "id" : "1"])
            tf_ServiceAddressSubType.text = "Residential"
        }else{
            tf_ServiceAddressSubType.text = "\(dataGeneralInfo.value(forKey: "serviceAddressSubType") ?? "")"
            if "\(dataGeneralInfo.value(forKey: "serviceAddressSubType") ?? "")".lowercased() ==  "Residential".lowercased(){
                self.arySelectedType = NSMutableArray()
                self.arySelectedType.add(["Name":"Residential", "id" : "1"])
            }else{
                self.arySelectedType = NSMutableArray()
                self.arySelectedType.add(["Name":"Commercial","id":"2"])
            }
            
        }
        creatUDFView()
    }
//    @objc func manageScrollHeight(notification: Notification) {
//        var strHeight =  "\(notification.object ?? "0")"
//        if strHeight == ""{
//            strHeight = "\(self.height_viewUserdefinefiled.constant)"
//        }else{
//            self.height_viewUserdefinefiled.constant = 0.0
//        }
//            self.height_viewUserdefinefiled.constant = CGFloat(Int(strHeight) ?? 0) + CGFloat(UserDefineFiledVC().GetSaveOtherData().count * 50)
//
//    }
    func creatUDFView()  {
        self.height_viewUserdefinefiled.constant = 0.0
        let strID = "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"
        var aryUDFSaveData_Service = UserDefineFiledVC().getUserdefineSaveData(type: Userdefinefiled_DynemicForm_Type.ServiceAddress, id: strID)
        let aryMasterData = UserDefineFiledVC().getUserdefineMasterData(type: Userdefinefiled_DynemicForm_Type.ServiceAddress)
        //----------When data is blank then use master data -------
        let arytempSave = NSMutableArray()
        //if aryUDFSaveData.count == 0 {
        for item in aryMasterData {
            let strType = "\((item as AnyObject).value(forKey: "type") ?? "")"
            let name = "\((item as AnyObject).value(forKey: "name") ?? "")"
            
            
            let temp = aryUDFSaveData_Service.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(name)" } as NSArray
            
            
            
            if (temp.count == 0 && (strType == Userdefinefiled_DynemicForm.radio_group  || strType == Userdefinefiled_DynemicForm.select || strType == Userdefinefiled_DynemicForm.checkbox_group) ) {
                if(((item as AnyObject).value(forKey: "values") is NSArray)){
                    let ary = ((item as AnyObject).value(forKey: "values") as! NSArray)
                    
                    let temp = ary.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "selected")!)".lowercased() == "true" ||  "\((task as! NSDictionary).value(forKey: "selected")!)".lowercased() == "1" } as NSArray
                    
                    if(temp.count != 0){
                        let aryValue = NSMutableArray()
                        for item in temp {
                            aryValue.add("\((item as AnyObject).value(forKey: "value") ?? "")")
                        }
                        let dict = NSMutableDictionary()
                        dict.setValue(aryValue, forKey: "userData")
                        dict.setValue(name, forKey: "name")
                        arytempSave.add(dict)
                    }
                }
            }
            if (temp.count == 0 && (strType == Userdefinefiled_DynemicForm.number ||  strType == Userdefinefiled_DynemicForm.text || strType == Userdefinefiled_DynemicForm.textarea || strType == Userdefinefiled_DynemicForm.date)){
                let dict = NSMutableDictionary()
                let aryValue = NSMutableArray()
                if((item as AnyObject).value(forKey: "value") != nil){
                    aryValue.add("\((item as AnyObject).value(forKey: "value") ?? "")")
                    dict.setValue(aryValue, forKey: "userData")
                    dict.setValue(name, forKey: "name")
                    arytempSave.add(dict)
                }
                
            }
        }
        aryUDFSaveData_Service += arytempSave.mutableCopy() as! NSMutableArray
        
  
  
    // remove blank value
    let aryTem = NSMutableArray()
    for item in aryUDFSaveData_Service {
        if(item is NSDictionary){
            let dictSaved = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
            if(dictSaved.value(forKey: "userData") is NSArray){
                let ary = (dictSaved.value(forKey: "userData") as! NSArray)
                let array = ary.filter({  "\($0)" != ""})
                dictSaved.setValue(array, forKey: "userData")
                aryTem.add(dictSaved)
            }
         
        }
        
    }
        aryUDFSaveData_Service = NSMutableArray()
        aryUDFSaveData_Service  = aryTem
        aryOtherUDF = NSMutableArray()
        /// }
    //----------End-------
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            
            UserDefineFiledVC().CreatFormUserdefine(strid: strID, aryJson_Master: aryMasterData, aryResponceObj:  aryUDFSaveData_Service, viewUserdefinefiled:  self.viewUserdefinefiled, controllor: self) { aryUserdefine, aryUserdefineResponce, height in
                self.height_viewUserdefinefiled.constant = CGFloat(height)
            }
        }
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func Validation() -> String {
        self.view.endEditing(true)
        if tf_ServiceAddress.text?.count == 0 {
            return "Please enter address."
        }
        if (tf_ServiceAddress.text!.count > 0)
        {
            
            let dictOfEnteredAddress = formatEnteredAddress(value: tf_ServiceAddress.text!)
            
            if dictOfEnteredAddress.count == 0
            {
                return "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode)."
            }
            else
            {
                
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"
                
                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"
                
                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    strStateId = ""
                }
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                    return "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode)."
                }
                if strCityName.count == 0 {
                    return "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode)."
                }
                if strStateId.count == 0 {
                    return "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode)."
                }
                if strZipCode.count == 0 {
                    return "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode)."
                }
                if strZipCode.count != 5 {
                    return "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode)."
                }
                
            }
        }
         if tf_ServiceAddressSubType.text?.count == 0 {
            return "Address subtype required!"
        }
        
        if(nsud.bool(forKey: "TaxCodeReq"))
        {
            if tf_TaxCode.text == "" {
                return "Please select Taxcode under Service Address."
            }
        }
        return ""
    }
    func   updateSalesWODetail(strWdoID : String) {
        let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@", strWdoID))
        if(arrayLeadDetail.count > 0){
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()

            
      
            arrOfKeys.add("serviceCounty")
            arrOfValues.add(tf_County.text?.trimmed ?? "") //1
            arrOfKeys.add("serviceMapCode")
            arrOfValues.add(tf_ServiceMapCode.text?.trimmed ?? "")//2
            arrOfKeys.add("serviceGateCode")
            arrOfValues.add(tf_ServiceGateCode.text?.trimmed ?? "")//3
            arrOfKeys.add("serviceSchoolDistrict")
            arrOfValues.add(tf_SchoolDistrict.text?.trimmed ?? "")//4
            arrOfKeys.add("serviceDirection")
            arrOfValues.add(txt_Direction.text?.trimmed ?? "")//5
            arrOfKeys.add("serviceNotes")
            arrOfValues.add(txt_AddressNote.text?.trimmed ?? "")//6
         
            let idTaxSysName = NSMutableArray()
            for employee in aryTaxCodeSelected {
                idTaxSysName.add((employee as AnyObject).value(forKey: "SysName") ?? "")
            }
            arrOfKeys.add("taxSysName")
            arrOfValues.add(idTaxSysName.componentsJoined(by: ","))//7
            
            arrOfKeys.add("tax")
            arrOfValues.add(strTax)//8
          
            let idPropertyTypeSysName = NSMutableArray()
            for employee in aryPropertySelected {
                idPropertyTypeSysName.add((employee as AnyObject).value(forKey: "SysName") ?? "")
            }
            arrOfKeys.add("serviceAddressPropertyTypeSysName")
            arrOfValues.add(idPropertyTypeSysName.componentsJoined(by: ","))//9
            
            //Address
            let dictOfEnteredAddress = formatEnteredAddress(value: tf_ServiceAddress.text!)
            arrOfKeys.add("servicesAddress1")
            arrOfValues.add("\(dictOfEnteredAddress.value(forKey: "AddresLine1") ?? "")" ) //10
            arrOfKeys.add("serviceAddress2")
            arrOfValues.add("\(dictOfEnteredAddress.value(forKey: "AddresLine2") ?? "")" ) //11
            arrOfKeys.add("serviceCity")
            arrOfValues.add("\(dictOfEnteredAddress.value(forKey: "CityName") ?? "")" )//12
            arrOfKeys.add("serviceState")
            arrOfValues.add("\(dictOfEnteredAddress.value(forKey: "StateName") ?? "")" )//13
            arrOfKeys.add("serviceCountry")
            arrOfValues.add("UnitedStates" )//14
            arrOfKeys.add("serviceZipcode")
            arrOfValues.add("\(dictOfEnteredAddress.value(forKey: "ZipCode") ?? "")" )//15

            arrOfKeys.add("serviceAddressSubType")
            arrOfValues.add("\(tf_ServiceAddressSubType.text!)" )//16
            
            
            arrOfKeys.add("modifiedBy")
            arrOfValues.add(Global().getEmployeeId())
            arrOfKeys.add("modifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS'Z", ""))
            arrOfKeys.add("userName")
            arrOfValues.add(Global().getUserName())
        
            if(getDataFromDbToUpdate(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@",strWdoID), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)){
                saveUserdefineDataToDB(id: strWdoID)
            }
         
        }
    }
    func saveUserdefineDataToDB(id : String ) {
        

        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        let dictSave = NSMutableDictionary()
        dictSave.setValue(Userdefinefiled_DynemicForm_Type.ServiceAddress, forKey: "Type")
        dictSave.setValue(aryUDFSaveData, forKey: "userDefinedFieldObj")

        arrOfKeys = ["leadId",
                     "userName","companyKey",
                     "userdefinedata"]
        arrOfValues = [id,
                       Global().getUserName(),
                       Global().getCompanyKey(),
                       dictSave]

        UserDefineFiledVC().saveUDFInDataBase(type: Userdefinefiled_DynemicForm_Type.ServiceAddress, aryKeys: arrOfKeys, aryValues: arrOfValues, strleadId: id)

        self.navigationController?.popViewController(animated: false)
        
    }
    
    //MARK:-------Header--IBAction-----------
    //MARK:
    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func action_Close(_ sender: Any) {
        tf_ServiceAddress.text = ""
        self.view.endEditing(true)
        self.tableView.removeFromSuperview()
    }
    @IBAction func action_PropertyType(_ sender: Any) {
    
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary {
            let dictLeadDetailMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if dictLeadDetailMaster.value(forKey: "AddressPropertyTypeMaster") is NSArray {
                var arrOfData = (dictLeadDetailMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).mutableCopy() as! NSMutableArray
                arrOfData = returnFilteredArray(array: arrOfData)
                if(arrOfData.count != 0){
                    openTableViewPopUp(tag: 65, ary: (arrOfData as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: aryPropertySelected)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
    }
    @IBAction func action_Type(_ sender: Any) {
        self.view.endEditing(true)
        let aryTypelist = [["Name":"Residential","id":"1"],
                           ["Name":"Commercial","id":"2"]]
        openTableViewPopUp(tag: 59, ary: (aryTypelist as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: arySelectedType)
    }
    @IBAction func action_TaxCode(_ sender: Any) {
        if (isInternetAvailable()){
            if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary {
                let dictLeadDetailMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                if dictLeadDetailMaster.value(forKey: "TaxMaster") is NSArray {
                    var arrOfData = (dictLeadDetailMaster.value(forKey: "TaxMaster") as! NSArray).mutableCopy() as! NSMutableArray
                    arrOfData = returnFilteredArray(array: arrOfData)
                    if(arrOfData.count != 0){
                        openTableViewPopUp(tag: 250, ary: (arrOfData as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: aryTaxCodeSelected)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Can't update taxcode offline", viewcontrol: self)
        }
     

    }
    @IBAction func action_Save(_ sender: Any) {
        let aryMasterData = UserDefineFiledVC().GetMasterData()
        let aryresponceobj = UserDefineFiledVC().GetSaveData()
       
        if(Validation() != ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Validation(), viewcontrol: self)
        }
        
        else if(UserDefineFiledVC().validation_UserdefineFiled(aryMaster: aryMasterData, aryresponceobj: aryresponceobj) != ""){

            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: UserDefineFiledVC().validation_UserdefineFiled(aryMaster: aryMasterData, aryresponceobj: aryresponceobj), viewcontrol: self)
        }
        else{
            Global().updateSalesModifydate("\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String)

            updateSalesWODetail(strWdoID: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        }
    }
    
    @IBAction func action_AddAddress(_ sender: Any) {
        let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
        let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
        vc.tag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: false, completion: {})
    }

    
}
//MARK:------UITextFieldDelegate-----------
//MARK:
extension SalesNew_EditServiceAddressInfoVC : UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == tf_ServiceAddress){
            let txtAfterUpdate:NSString = tf_ServiceAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            self.getPlaces(with: parameters) {
                self.places = $0
            }
            return true
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == tf_ServiceAddress){
            self.scrollView.isScrollEnabled = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == tf_ServiceAddress){
            self.scrollView.isScrollEnabled = true
            self.tableView.removeFromSuperview()
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if(textField == tf_ServiceAddress){
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.tableView.removeFromSuperview()
                    return true
                    
                }
            }
            var txtAfterUpdate:NSString = tf_ServiceAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
}
//MARK:------UITextViewDelegate-----------
//MARK:
extension SalesNew_EditServiceAddressInfoVC : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        return true
    }
   
}

// MARK: -
// MARK: - ----------------------Google Search API

extension SalesNew_EditServiceAddressInfoVC {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.tableView.removeFromSuperview()
                }
                else
                {
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    if DeviceType.IS_IPHONE_6_OR_LESS {
                        self.tableView.frame = CGRect(x: self.viewAddress.frame.origin.x, y: self.viewAddress.frame.maxY, width: self.viewAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)
                    } else {
                        self.tableView.frame = CGRect(x: self.viewAddress.frame.origin.x, y: self.viewAddress.frame.maxY, width: self.viewAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 180)
                    }
                    self.scrollView.addSubview(self.tableView)
                }
            }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -
// MARK: - -----------------------Add Address Delgates

extension SalesNew_EditServiceAddressInfoVC : AddNewAddressiPhoneDelegate{
    
    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {
        print(dictData)
        self.tf_ServiceAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
    }
    
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension SalesNew_EditServiceAddressInfoVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.contentView.backgroundColor = UIColor.clear
        cell?.backgroundColor = UIColor.clear
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        }
        let place = places[indexPath.row]
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 12)
        cell!.textLabel?.numberOfLines = 0
        cell!.textLabel?.text = place.mainAddress + "\n" + place.secondaryAddress
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            //self.txtFldAddress.text = value.formattedAddress
            self.tf_ServiceAddress.text = addressFormattedByGoogle(value: value)
            
            self.places = [];
        }
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
// MARK: - Selection CustomTableView
// MARK: -

extension SalesNew_EditServiceAddressInfoVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 59)
        {
            if(dictData.count == 0){
                tf_ServiceAddressSubType.text = ""
                self.arySelectedType = NSMutableArray()
            }else{
                tf_ServiceAddressSubType.text = "\(dictData.value(forKey: "Name")!)"
                self.arySelectedType = NSMutableArray()
                self.arySelectedType.add(dictData)
            }
        }
        
        if(tag == 65)
       {
           if(dictData.count == 0){
               tf_PropertyType.text = ""
               aryPropertySelected = NSMutableArray()
           }else{
               aryPropertySelected = NSMutableArray()
               aryPropertySelected.add(dictData)
            tf_PropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
           }
       }
        if(tag == 250)
        {
            if(dictData.count == 0){
                strTax = ""
                tf_TaxCode.text = ""
                aryTaxCodeSelected = NSMutableArray()
            }else{
              
                self.CallTax_API(dict: dictData)
                
            }
        }
        
    }
}



// MARK: - API Calling --------
// MARK: -

extension SalesNew_EditServiceAddressInfoVC
{
    func CallTax_API(dict : NSDictionary)  {
        var loader = UIAlertController()

        
        let strTaxSysName = "\(dict.value(forKey: "SysName") ?? "")"
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strUrlMain = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") as! String
        let strUrl = strUrlMain + "/api/MobileToSaleAuto/GetTaxByTaxCode?companyKey=\(Global().getCompanyKey()!)&taxSysName=\(strTaxSysName)"

        let semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: strUrl)!,timeoutInterval: Double.infinity)
        request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
        request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
        request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
        request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
        request.httpMethod = "GET"

        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        
        self.present(loader, animated: false, completion: nil)
        let task = URLSession.shared.dataTask(with: request) { [self] data, response, error in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                loader.dismiss(animated: false, completion: nil)
            }
          guard let data = data else {
            print(String(describing: error))
            
            semaphore.signal()
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Can't update taxcode Please try again.", viewcontrol: self)

            return
          }
            
          print(String(data: data, encoding: .utf8)!)
          semaphore.signal()
            if (String(data: data, encoding: .utf8)!).count > 0
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                    aryTaxCodeSelected = NSMutableArray()
                    aryTaxCodeSelected.add(dict)
                    strTax = (String(data: data, encoding: .utf8)!)
                    tf_TaxCode.text = "\(dict.value(forKey: "Name") ?? "")"
                    
                }
              
            }
        }

        task.resume()
        semaphore.wait()

        
        
    }
}
