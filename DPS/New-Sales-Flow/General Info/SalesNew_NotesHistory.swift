//
//  SalesNew_NotesHistory.swift
//  DPS
//
//  Created by INFOCRATS on 29/11/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_NotesHistory: UIViewController {

    //MARK: - IBOutlet
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!

    
   //MARK: - Variable
    @objc var strAccccountId = ""
    @objc var strIsFromSales = Bool()
    var strTitle = ""
    @objc var strLeadNo = ""
    var arrOfNoteHistory = [[String: Any]]()
    var heightConstaintForlblTitle = CGFloat()
    var loader = UIAlertController()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        tblView.estimatedRowHeight = 100.0
        // Do any additional setup after loading the view.
        
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        
        self.present(loader, animated: false, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            
            //self.syncTimeRange(dictToSend: getTimeRangeData(), strLeadIdToSend: strLeadId)

            self.getServiceNotesList()

        }
    }
    //MARK: - IBAction
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnAddNoteAction(_ sender: Any) {
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_AddNotesHistroy") as? SalesNew_AddNotesHistroy
        if strIsFromSales == true{
            testController!.tempstrRefId = strLeadNo as NSString
            testController!.tempstrRefType = "lead"

        }else{
        
            testController!.tempstrRefId = strLeadNo as NSString
        testController!.tempstrRefType = "WorkOrder"
        }
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    //MARK: - Custom Funcs

    //MARK: - Call WebService
    func getServiceNotesList(){
        
//        let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
//        self.present(loader, animated: false, completion: nil)
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strURL = String(format:  "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" +  UrlServiceNotesHistoryNewSales , strAccccountId, Global().getCompanyKey())
        
        print("GET Service History  URL \(strURL)")
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
            loader.dismiss(animated: false) {
                
            }
            if(status)
            {
                loader.dismiss(animated: false) {
                    
                    self.arrOfNoteHistory.removeAll()
                    self.arrOfNoteHistory = response.value(forKey: "data") as! [[String:Any]]
                    tblView.reloadData()
                }
            }
            else{
                loader.dismiss(animated: false) {
                print("failed")
                }
            }
        }
    }
    

}

extension SalesNew_NotesHistory : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arrOfNoteHistory.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "salesNew_NotesHistoryCell") as! salesNew_NotesHistoryCell
        cell.selectionStyle = .none
        
        let dict = arrOfNoteHistory[indexPath.row]
        heightConstaintForlblTitle = CGFloat()
        let noteDes =  dict["Note"] as? String ?? ""
        if noteDes != ""
        {
           // cell.lblNoteDetails.text = "Note Description: " + noteDes
            var fontsize = CGFloat()
            if DeviceType.IS_IPAD{
                fontsize = 18
            }
            else{
                fontsize = 15
            }
            let normalText = noteDes
            let boldText  = "Note Description: "
            let attributedString = NSMutableAttributedString(string:normalText)

            let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: fontsize)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)

            boldString.append(attributedString)
        
            cell.lblNoteDetails.attributedText = boldString
            
            }
        else{
            cell.lblNoteDetails.text =  "Note Description: -"
        }
        
        if dict["EmployeeName"] as? String ?? "" != ""
        {
            cell.lblEmployee.text = dict["EmployeeName"] as? String ?? ""
        }
        else{
            cell.lblEmployee.text =  ""
        }
        
       
        if dict["ClientCreatedDate"] as? String ?? "" != ""
        {
            cell.lblDate.text = changeStringDateToGivenFormat(strDate: dict["ClientCreatedDate"] as? String ?? "", strRequiredFormat: "MM/dd/yyyy hh:mm a")
        }
        else{
            cell.lblDate.text =  ""
        }
        
        if dict["ExpiryDate"] as? String ?? "" != ""
        {
            cell.lblExpiaryDate.text = changeStringDateToGivenFormat(strDate: dict["ExpiryDate"] as? String ?? "", strRequiredFormat: "MM/dd/yyyy")
        }
        else{
            cell.lblExpiaryDate.text =  ""
        }
        
        if dict["NoteCodeTitle"] as? String ?? "" != ""
        {
            cell.lblNoteCode.text = dict["NoteCodeTitle"] as? String ?? ""
        }
        else{
            cell.lblNoteCode.text =  ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}

class salesNew_NotesHistoryCell: UITableViewCell {
    
    //MARK: -IBOutlets
    @IBOutlet weak var lblNoteCode: UILabel!
    @IBOutlet weak var lblEmployee: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblExpiaryDate: UILabel!
    @IBOutlet weak var lblNoteDetails: UILabel!
    @IBOutlet weak var heightConstraintForTitle: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
