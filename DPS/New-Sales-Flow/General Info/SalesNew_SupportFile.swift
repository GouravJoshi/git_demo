//
//  SalesNew_SupportFile.swift
//  DPS
//
//  Created by NavinPatidar on 5/25/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import UIKit
import MessageUI

enum SalesNewListName {
    static let EmailHistory = "Email History"
    static let ChemicalSensitivity = "Chemical Sensitive"
    static let SetupHistory = "Setup History"
    static let NotesHistory = "Notes History"
    static let ServiceHistory = "Service History"
    static let AddImages = "Add Images"
    static let AddDocuments = "Add Documents"
}

enum SalesNewStoryBoardName {
    static let SalesFlowNewGeneralInfo = "NewSales_GeneralInfo"//"SalesFlowNewGeneralInfo"
    static let SalesNewFlowInspection = "NewSales_Inspection"//"SalesNewFlowInspection"
    static let SalesFlowNewSendEmail = "NewSales_SendEmail"//"SalesFlowNewSendEmail"

}

enum SalesNewColorCode {
    static let darkBlack = hexStringToUIColor(hex: "37404E")
    static let lightgray = UIColor.lightGray

}





class SalesNew_SupportFile: NSObject {
    func GetNameBySysNameCommon(arysysNameList:NSArray,getmasterName:String,getkeyName:String,keyName:String,keySysName:String,OnResultBlock: @escaping (_ aryData: NSArray,_ strTypeName:String , _ strSysName:String) -> Void) {
        let aryData = NSMutableArray()
        var strName = ""
        var strSysName = ""
        if let dict = nsud.value(forKey: getmasterName) {
            if(dict is NSDictionary){
                if let aryMaster = (dict as! NSDictionary).value(forKey: getkeyName){
                    if(aryMaster is NSArray){
                        for item  in aryMaster as! NSArray{
                            let sysName = (item as! NSDictionary).value(forKey: keySysName) as! String
                            for item1  in arysysNameList {
                                let sysName1 = (item1 as! String)
                                if(sysName == sysName1){
                                    aryData.add(item)
                                }
                            }
                        }
                    }
                }
            }
        }
        for item in aryData {
            let Name = (item as! NSDictionary).value(forKey: keyName) as! String
            strName = strName + "\(Name),"
            let SysName = (item as! NSDictionary).value(forKey: keySysName) as! String
            strSysName = strSysName + "\(SysName),"
            
        }
        if strName.count != 0 {
            strName = String(strName.dropLast())
        }
        if strSysName.count != 0 {
            strSysName = String(strSysName.dropLast())
        }
        OnResultBlock((aryData) ,strName ,strSysName)
    }
    
    func getSalesWODataFromCoreData(strleadId : String) -> NSManagedObject {
        let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@ && userName == %@", strleadId , "\(Global().getUserName()!)"))
        

        if(arrayLeadDetail.count > 0)
        {
            return arrayLeadDetail.firstObject as! NSManagedObject
        }
        return NSManagedObject()
    }
    
    func InitializePickerInActionSheet(slectedDate : Date ,Picker: UIDatePicker ,view : UIView ,strTitle : String) -> UIAlertController {
        let alertController = UIAlertController(title: strTitle, message: nil, preferredStyle: .actionSheet)
        var customView1 = UIDatePicker()
        customView1 = Picker
        customView1.date = slectedDate
        customView1.backgroundColor = UIColor.blue
        if #available(iOS 13.4, *) {
            customView1.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        alertController.view.addSubview(customView1)
        customView1.translatesAutoresizingMaskIntoConstraints = false
        customView1.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 30).isActive = true
        customView1.rightAnchor.constraint(equalTo: alertController.view.rightAnchor, constant: -10).isActive = true
        customView1.leftAnchor.constraint(equalTo: alertController.view.leftAnchor, constant: 10).isActive = true
        customView1.heightAnchor.constraint(equalToConstant: 215).isActive = true
        
        alertController.view.tintColor = UIColor.black
        alertController.view.translatesAutoresizingMaskIntoConstraints = false
        alertController.view.heightAnchor.constraint(equalToConstant: 350).isActive = true
        alertController.view.tintColor = UIColor.black
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = CGRect(x: view.bounds.midX, y: view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        return alertController
    }
    
    func InitializePickerInActionSheetWithClearButton(slectedDate : Date ,Picker: UIDatePicker ,view : UIView ,strTitle : String) -> UIAlertController {
        let alertController = UIAlertController(title: strTitle, message: nil, preferredStyle: .actionSheet)
        var customView1 = UIDatePicker()
        customView1 = Picker
        customView1.date = slectedDate
        customView1.backgroundColor = UIColor.blue
        if #available(iOS 13.4, *) {
            customView1.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        alertController.view.addSubview(customView1)
        customView1.translatesAutoresizingMaskIntoConstraints = false
        customView1.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 30).isActive = true
        customView1.rightAnchor.constraint(equalTo: alertController.view.rightAnchor, constant: -10).isActive = true
        customView1.leftAnchor.constraint(equalTo: alertController.view.leftAnchor, constant: 10).isActive = true
        customView1.heightAnchor.constraint(equalToConstant: 215).isActive = true
        
        alertController.view.tintColor = UIColor.black
        alertController.view.translatesAutoresizingMaskIntoConstraints = false
        alertController.view.heightAnchor.constraint(equalToConstant: 400).isActive = true
        alertController.view.tintColor = UIColor.black
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = CGRect(x: view.bounds.midX, y: view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        return alertController
    }
}


