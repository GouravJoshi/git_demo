//
//  SalesNew_EditServiceRequiredVC.swift
//  DPS
//
//  Created by NavinPatidar on 6/1/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_EditServiceRequiredVC: UIViewController {
    
    
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:
    //MARK:---------Variable-----------
    
    var strTitle = "" , strKey = ""
    var dataGeneralInfo = NSManagedObject()
    var dictLoginData = NSDictionary()
    var strEmpName = ""
    var strUserName = ""
    var strCompanyKey = ""
    //MARK:
    //MARK:---------LifeCycle-----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        setInitialUI()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
    }
    //MARK:
    //MARK:---------IBAction-----------
    
    @IBAction func action_Back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        switch strTitle {
        case "Problem Description":
            updateSalesWODetail(strWdoID: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")

            
        case "Description":
            updateSalesWODetail(strWdoID: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")

        case "Internal Notes":
            updateSalesWODetail(strWdoID: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")

        case "Customer Notes":
            
            updatePaymentInfo(strLead: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
            self.navigationController?.popViewController(animated: true)
        default:
            break
        }
        
        
        
        //updateSalesWODetail(strWdoID: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        
    }
    //MARK:
    //MARK:-------Extra--Function-----------
    
    func setInitialUI() {
        txtView.delegate = self
        txtView.placeholder = "Enter here..."
        lblTitle.text = strTitle
        
        
        switch strTitle {
        case "Problem Description":
            strKey = "problemDescription"
            let strTxt =  "\(dataGeneralInfo.value(forKey: strKey) ?? "")"
            self.txtView.text = strTxt
            
        case "Description":
            strKey = "descriptionLeadDetail"
            let strTxt =  "\(dataGeneralInfo.value(forKey: strKey) ?? "")"
            self.txtView.text = strTxt
        case "Internal Notes":
            strKey = "notes"
            let strTxt =  "\(dataGeneralInfo.value(forKey: strKey) ?? "")"
            self.txtView.text = strTxt
        case "Customer Notes":
            strKey = "specialInstructions"
            
            let arrayPaymentDetail = getDataFromLocal(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"))
            if arrayPaymentDetail.count > 0
            {
                let matches = arrayPaymentDetail.object(at: 0) as! NSManagedObject
                let strTxt =  "\(matches.value(forKey: strKey) ?? "")"
                self.txtView.text = strTxt
            }
            
        default:
            break
        }
        
    }
    
    func updatePaymentInfo(strLead : String)  {
        
        let arrayPaymentDetail = getDataFromLocal(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLead))
        
        if arrayPaymentDetail.count == 0
        {
            savePaymentInfo(strLead: strLead)
        }
        else
        {
            var isSuccess = Bool()
            let arrOfKeys = NSMutableArray()
            arrOfKeys.add("specialInstructions")
            let arrOfValues = NSMutableArray()
            arrOfValues.add(txtView.text)

            isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@ && userName == %@",strLead,strUserName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
    }
    func savePaymentInfo(strLead : String) {
        
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        arrOfKeys = ["agreement",
                     "amount",
                     "checkBackImagePath",
                     "checkFrontImagePath",
                     "checkNo",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "customerSignature",
                     "expirationDate",
                     "inspection",
                     "leadId",
                     "leadPaymentDetailId",
                     "licenseNo",
                     "modifiedBy",
                     "modifiedDate",
                     "paymentMode",
                     "proposal",
                     "salesSignature",
                     "specialInstructions",
                     "userName"]
        
        arrOfValues = ["",
                     "",
                     "",
                     "",
                     "",
                     strCompanyKey,
                     "",
                     "",
                     "",
                     "",
                     "",
                     strLead,
                     "",
                     "",
                     "",
                     "",
                     "",
                     "",
                     "",
                     txtView.text,
                     strUserName]
        
        saveDataInDB(strEntity: "PaymentInfo", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }

func updateSalesWODetail(strWdoID : String) {
    let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@", strWdoID))
    if(arrayLeadDetail.count > 0){
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
      
        arrOfKeys.add(strKey)
        arrOfValues.add("\(txtView.text!)")
  
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("modifyDate")
        arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS'Z", ""))
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
      
        if(getDataFromDbToUpdate(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@",strWdoID), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)){
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    
}

}
//MARK:
//MARK:------UITextViewDelegate---------

extension SalesNew_EditServiceRequiredVC : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
}

