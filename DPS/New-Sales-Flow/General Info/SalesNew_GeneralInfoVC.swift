//
//  SalesNew_GeneralInfoVC.swift
//  DPS
//
//  Created by NavinPatidar on 5/13/21.Fgloble
//  Copyright © 2021 Saavan. All rights reserved. changes
// Changes

import UIKit

class SalesNew_GeneralInfoVC: UIViewController {
    //MARK:
    //MARK:---------IBOutlet-----------
  
    @IBOutlet weak var scrollTopButton: UIScrollView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btncancle: UIButton!
    @IBOutlet weak var btnSave: UIButton!

    var dataGeneralInfo = NSManagedObject()
    let indexPath1 = IndexPath(row: 0, section: 0)
    let indexPath2 = IndexPath(row: 0, section: 1)
    var serviceAddressImagePathTagLocal = 0
    var serviceAddressImagePath = String()
    var isEditSchedule = false
    var strUserName = ""
    var strCompanyKey = ""
    var isEditInSalesAuto = false
    var chkTaxCodeRequired = false
    //MARK:
    //MARK:---------LifeCycle-----------
  
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            setInitialUI()

            if !isInternetAvailable()
            {
                showAccountAndNotesAlert(isOffline: true)
            }
            else
            {
                getNotesAlert()
            }
            downloadAllImages()
            downloadAllCRMImages()
        }
        
    }
    
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !Check_Login_Session_expired() {
            dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
            print(dataGeneralInfo)
            downloadGraphXML()
            self.tblView.reloadData()
        }
      
    }
    //MARK:
    //MARK:-------Extra--Function-----------
    func setInitialUI() {
        dict_Login_Data =  (UserDefaults.standard.value(forKey: "LoginDetails") as! NSDictionary).mutableCopy()as!NSMutableDictionary
        strUserName = "\(dict_Login_Data.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dict_Login_Data.value(forKeyPath: "Company.CompanyKey") ?? "")"
        isEditSchedule = (dict_Login_Data.value(forKeyPath: "Company.CompanyConfig.IsRescheduleSalesAppointment") ?? false) as! Bool
        serviceAddressImagePath = "\(dataGeneralInfo.value(forKey: "serviceAddressImagePath") ?? "")"
        self.tblView.tableFooterView = UIView()
    }
     func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(.camera)){
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.allowsEditing = true
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    
     func openGallery() {
        DispatchQueue.main.async {
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
    }
    func updateWorkOrder(strWdoID : String)  {
        let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@", strWdoID))
        if(arrayLeadDetail.count > 0){
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()

            arrOfKeys.add("serviceAddressImagePath")
            arrOfValues.add(self.serviceAddressImagePath) //1
   
            arrOfKeys.add("modifiedBy")
            arrOfValues.add(Global().getEmployeeId())
            arrOfKeys.add("modifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS'Z", ""))
            arrOfKeys.add("userName")
            arrOfValues.add(Global().getUserName())
          
            if(getDataFromDbToUpdate(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@",strWdoID), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.tblView.reloadSections([0], with: .none)
                }
            }
           
        }
    }
    
    func goToEmailHistory()  {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
              let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
              testController?.modalPresentationStyle = .fullScreen
              testController?.strAccountNo = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
              testController?.strCompanyKey = strCompanyKey
              testController?.strUserName = strUserName

              self.present(testController!, animated: false, completion: nil)
    }
    
    func goToServiceHistory()  {
        
        /*let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = "\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
        
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
        
        
    }
    
    func goToAppointment()
    {
           
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    
    func goToServiceNotesHistory()  {
        
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController?.strTypeOfService = "sales";
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    //MARK:
    //MARK:-------Header--IBAction-----------

    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        
    }
    @IBAction func action_History(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
            self.view.endEditing(true)
            
            self.goToEmailHistory()
            
        }))
        
        alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
        
            self.goToSetupHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
        
            self.goToServiceNotesHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
        
            self.goToServiceHistory()
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
           // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.up]
        }
        self.present(alert, animated: true)
       
      }
    //MARK:
    //MARK:-------Footor--IBAction-----------
    
    @IBAction func action_Image(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Make Your Selection", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.AddImages, style: .default, handler: { action in
            self.GotoGlobleImageGraphView(strType: "Before")
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.AddDocuments, style: .default, handler: { [self] action in
            let strLeadID =  dataGeneralInfo.value(forKey: "leadId") ?? ""

            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: DeviceType.IS_IPAD ? "UploadDocumentSalesiPad" : "UploadDocumentSales") as! UploadDocumentSales
            vc.strWoId = "\(strLeadID)" as NSString
            self.navigationController?.present(vc, animated: true, completion: nil)

        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
      //      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.down]
        }
        self.present(alert, animated: true)
    }
    @IBAction func action_Graph(_ sender: Any) {
        self.view.endEditing(true)
        nsud.setValue(true, forKey: "firstGraphImage")
        nsud.setValue(false, forKey: "servicegraph")
        GotoGlobleImageGraphView(strType: "Graph")
        
    }
    @IBAction func action_ChemicalSensitive(_ sender: Any) {
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func action_Home(_ sender: Any) {
        self.view.endEditing(true)
        self.goToAppointment()
    }
    
    
    @IBAction func action_Cancel(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    
    func updateLeadIdDetail()  {
        
        let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"))
        if(arrayLeadDetail.count > 0){
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()

            arrOfKeys.add("versionDate")
            arrOfValues.add(VersionDate)
            
            arrOfKeys.add("versionNumber")
            arrOfValues.add(VersionNumber)
            
            arrOfKeys.add("deviceVersion")
            arrOfValues.add(UIDevice.current.systemVersion)
            
            arrOfKeys.add("deviceName")
            arrOfValues.add(UIDevice.current.name)
   
            arrOfKeys.add("modifiedBy")
            arrOfValues.add(Global().getEmployeeId())
            
            arrOfKeys.add("modifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS'Z", ""))
            
            arrOfKeys.add("userName")
            arrOfValues.add(Global().getUserName())
            
            arrOfKeys.add("customerName")
            arrOfValues.add("\(Global().strFullName(fromCoreDB: dataGeneralInfo) ?? "")") //1
            
            
            if "\(dataGeneralInfo.value(forKey: "flowType") ?? "")" == "Commercial"
            {
                arrOfKeys.add("isCommTaxUpdated")
                arrOfValues.add("true")
            }
            
            if(getDataFromDbToUpdate(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)){
            
                print("Data updated")
            }
           
        }
    }
    
    @IBAction func action_Continue(_ sender: Any) {
        
        self.view.endEditing(true)
        
        updateLeadIdDetail()
        
        if getStatusCompleteWon()
        {
            let proceed = validationForSaveForCompleteWon()
            
            if !proceed.check
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: proceed.message, viewcontrol: self)
            }
            else
            {
                if isEditInSalesAuto
                {
                    Global().updateSalesModifydate("\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String)
                }
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesNewFlowInspection : SalesNewStoryBoardName.SalesNewFlowInspection, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNewFlow_InspectionVC") as! SalesNewFlow_InspectionVC
                vc.matchesGeneralInfo = dataGeneralInfo
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        else
        {
 
            let proceed = validationForSave()
            
            if !proceed.check
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: proceed.message, viewcontrol: self)
            }
            else
            {
                if isEditInSalesAuto
                {
                    Global().updateSalesModifydate("\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String)
                }
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesNewFlowInspection : SalesNewStoryBoardName.SalesNewFlowInspection, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNewFlow_InspectionVC") as! SalesNewFlow_InspectionVC
                vc.matchesGeneralInfo = dataGeneralInfo
                self.navigationController?.pushViewController(vc, animated: false)
            }

        }
        
       
    }
    
    func validationForSaveForCompleteWon() -> (check: Bool, message: String) {
        
        var check = false
        
        var message = ""
        
        if "\(dataGeneralInfo.value(forKey: "leadName") ?? "")".isEmpty
        {
            message = "Opportunity name is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "firstName") ?? "")".isEmpty
        {
            message = "First name is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "lastName") ?? "")".isEmpty
        {
            message = "Last name is missing"
        }
        else
        {
            check = true
        }
        
        return (check,message)
    }
    
    func validationForSave() -> (check: Bool, message: String) {
        
        var check = false
        var message = ""
        
        if "\(dataGeneralInfo.value(forKey: "leadName") ?? "")".isEmpty
        {
            message = "Opportunity name is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "firstName") ?? "")".isEmpty
        {
            message = "First name is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "lastName") ?? "")".isEmpty
        {
            message = "Last name is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "servicesAddress1") ?? "")".isEmpty
        {
            message = "Service address is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "serviceCity") ?? "")".isEmpty
        {
            message = "Service address city is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "serviceState") ?? "")".isEmpty
        {
            message = "Service address state is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "serviceCountry") ?? "")".isEmpty
        {
            message = "Service address country is missing"
        }
        else if "\(dataGeneralInfo.value(forKey: "serviceZipcode") ?? "")".isEmpty
        {
            message = "Service address zipcode is missing"
        }
        else if (nsud.bool(forKey: "TaxCodeReq")) && "\(dataGeneralInfo.value(forKey: "taxSysName") ?? "")".isEmpty
        {
            message = "Tax code is missing"
        }
        else if !self.checkTaxCode()
        {
            message = "Tax code is missing"
        }
        else
        {
            check = true
        }
        
        return (check,message)
    }
    
    func checkTaxCode() -> Bool {
        
        var chkTaxExist = false

        if "\(dataGeneralInfo.value(forKey: "taxSysName") ?? "")".count > 0
        {
            let aryTaxCodeSysName = "\(dataGeneralInfo.value(forKey: "taxSysName") ?? "")".split(separator: ",")
            SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryTaxCodeSysName as NSArray, getmasterName: "MasterSalesAutomation", getkeyName: "TaxMaster", keyName: "Name", keySysName: "SysName") { [self] (result, name, sysname) in
                //tf_TaxCode.text = name
                //aryTaxCodeSelected = result.mutableCopy()as! NSMutableArray
                if result.count > 0
                {
                    chkTaxExist = true
                }
                else
                {
                    chkTaxExist = false
                    
                    if !nsud.bool(forKey: "TaxCodeReq")
                    {
                        chkTaxExist = true
                    }

                }
            }
        }
        else
        {
            chkTaxExist = true
        }
        
        return chkTaxExist
    }
    
    func downloadGraphXML() {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")", "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")", "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")", "Graph", "1"))
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlFileName = "\(dictData.value(forKey: "leadXmlPath") ?? "")"
            
            var strURL = String()
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") {
                
                strURL = "\(value)"
                
            }
            
            strURL = strURL + "\(xmlFileName)"
            
            strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
            
            DispatchQueue.global(qos: .background).async {
                
                let url = URL(string:strURL)
                let data = try? Data(contentsOf: url!)
                
                if data != nil && (data?.count)! > 0 {
                    
                    DispatchQueue.main.async {
                        
                        savepdfDocumentDirectory(strFileName: xmlFileName, dataa: data!)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func GotoGlobleImageGraphView(strType : String)  {
        let strLeadID =  dataGeneralInfo.value(forKey: "leadId") ?? ""
        var strStatusss =  dataGeneralInfo.value(forKey: "statusSysName") ?? ""
        var strStageSysName =  dataGeneralInfo.value(forKey: "stageSysName") ?? ""
        if(nsud.value(forKey: "backFromInspectionNew") != nil){
            
            if(nsud.value(forKey: "backFromInspectionNew") as! Bool == true){
                strStatusss =  dataGeneralInfo.value(forKey: "statusSysName") ?? ""
                strStageSysName =  dataGeneralInfo.value(forKey: "stageSysName") ?? ""
            }
        }
    
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPhone" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        vc.strHeaderTitle = strType as NSString
        vc.strWoId = "\(strLeadID)" as NSString
        vc.strWoLeadId = "\(strLeadID)" as NSString
        vc.strModuleType = "SalesFlow"
        
        
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won"){
            vc.strWoStatus = "Complete"
        }else{
            vc.strWoStatus = "InComplete"
        }
        vc.strWoStatus = "InComplete"
        self.navigationController?.present(vc, animated: true, completion: nil)

    }
    
    //MARK: - Alert notes API work
    
    func clearDBBeforeAddingAPIData(){
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "AlertNotes", predicate: NSPredicate(format: "workorderId == %@", "\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")"))
        
        if arryOfData.count > 0
        {
            for i in 0..<arryOfData.count{
                let objData = arryOfData[i] as! NSManagedObject
                deleteDataFromDB(obj: objData)
            }
        }
    }
    
    func getDataFromDB() -> NSMutableArray{
        let objOfNotes = NSMutableArray()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "AlertNotes", predicate: NSPredicate(format: "workorderId == %@", "\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")"))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objOfNotes.add(dict)
            }
        }
        return objOfNotes
    }
    
    func getNotesAlert(){
        
        let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        
        self.present(loader, animated: false, completion: nil)
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let refType = "leadno"
        
        let strURL = String(format:  "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")" +  URL.ServiceNewPestFlow.getNotesAlert, refType, "\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")" , Global().getCompanyKey())
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
            
            loader.dismiss(animated: true)
            if(status)
            {
                self.clearDBBeforeAddingAPIData()
                let arrOfNotesTemp = response.value(forKey: "data") as! [[String:Any]]
                for i in 0..<arrOfNotesTemp.count{
                    setDataIntoCoreDate(arrOfCurrentIndex: arrOfNotesTemp[i])
                }
                    showAccountAndNotesAlert(isOffline: false)
            }
        }
    }
    
    func showAccountAndNotesAlert(isOffline: Bool){
        if isOffline == true{
            let strAlertMessage = dataGeneralInfo.value(forKey: "accountDescription") as! String
            
            if strAlertMessage.count > 0 {
                
                let alert = UIAlertController(title: "Alert", message: strAlertMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        else{
            let arrOfNotes = getDataFromDB()
            let strAlertMessage = dataGeneralInfo.value(forKey: "accountDescription") as! String
            var arrOfListOfMessages = [String]()
            if strAlertMessage != ""{
                arrOfListOfMessages.append(strAlertMessage)
            }
            for i in 0..<arrOfNotes.count{
                
                let obj = arrOfNotes[i] as! NSManagedObject
                arrOfListOfMessages.append("\(obj.value(forKey: "noteText")!)")
            }
            
            if arrOfNotes.count > 0 || strAlertMessage != ""{
                let alert = UIAlertController(title: "Alert", message: arrOfListOfMessages.toBulletList(), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func setDataIntoCoreDate(arrOfCurrentIndex: [String: Any]){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("noteText")
        arrOfKeys.add("noteId")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("username")
        
        
        arrOfValues.add(arrOfCurrentIndex["Note"] as? String ?? "")
        arrOfValues.add(arrOfCurrentIndex["LeadNoteId"] as? String ?? "")
        arrOfValues.add("\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")")
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        
        saveDataInDB(strEntity: "AlertNotes", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    
    
    // MARK: - ----------------Extra FUnction
    // MARK: -
    func getStatusCompleteWon() -> Bool {
        let strStatusss =  dataGeneralInfo.value(forKey: "statusSysName") ?? ""
        let strStageSysName =  dataGeneralInfo.value(forKey: "stageSysName") ?? ""
        
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won") {
            return true
        }else{
            return false
        }
    }
    func checkForMarkAsLost() -> Bool {
        let strStatusss =  dataGeneralInfo.value(forKey: "statusSysName") ?? ""
        let strStageSysName =  dataGeneralInfo.value(forKey: "stageSysName") ?? ""
        
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "lost"){
            return true
        }else{
            return false
        }
    }

}

// MARK: -
// MARK: - ----------------UITableViewDelegate

extension SalesNew_GeneralInfoVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  section == 0 ? 1 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        //Customer Info
        if(indexPath.section == 0){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesNewGeneralInfo" : "SalesNewGeneralInfo", for: indexPath as IndexPath) as! SalesNewGeneralInfo
            
            cellCustomerInfoConfigure(cell: cell)
            cell.btn_Edit_CustomerInfo.addTarget(self, action: #selector(actionCustomerInfoEdit), for: .touchUpInside)
            cell.btn_Edit_Schedule.addTarget(self, action: #selector(actionViewBtnSchedule), for: .touchUpInside)
            cell.segment.addTarget(self, action: #selector(actionViewSegment), for: .valueChanged)
           
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(gesture:)))
            cell.img_Profile.isUserInteractionEnabled = true
            
            if self.getStatusCompleteWon()
            {
                cell.img_Profile.isUserInteractionEnabled = false
            }
            
            cell.img_Profile.addGestureRecognizer(tap)
     
                // Show Server image
            if(serviceAddressImagePath != ""){
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: self.serviceAddressImagePath)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: self.serviceAddressImagePath)
                
                if isImageExists!  {
                    
                    cell.img_Profile.image = image
                    
                }else {
                    
                    if let nameurl = URL(string: "\(URL.baseUrl)/Documents/CustomerAddressImages/" + self.serviceAddressImagePath)
                    {
                        cell.img_Profile.sd_setImage(with: nameurl, placeholderImage: UIImage(named: "no_image.jpg"), completed: nil)
                    }
                    else
                    {
                        cell.img_Profile.image = UIImage(named: "no_image.jpg")!
                    }
                }
                
                /*if GetImageFromDocumentDirectory(strFileName: self.serviceAddressImagePath).imageAsset != nil{
                    cell.img_Profile.image = GetImageFromDocumentDirectory(strFileName: self.serviceAddressImagePath)
                    
                }
                else{
                    
                    if let nameurl = URL(string: "\(URL.baseUrl)/Documents/CustomerAddressImages/" + self.serviceAddressImagePath)
                    {
                        cell.img_Profile.sd_setImage(with: nameurl, placeholderImage: UIImage(named: "no_image.jpg"), completed: nil)
                    }
                    else
                    {
                        cell.img_Profile.image = UIImage(named: "no_image.jpg")!
                    }
                }*/
                
            }
            
            else{
                //jab kuch n ho to
                cell.img_Profile.image = UIImage(named: "no_image.jpg")!
            }
            
            if(getStatusCompleteWon())
            {
                cell.btn_Edit_CustomerInfo.isHidden = true
                cell.btn_Edit_Schedule.isHidden = true
                cell.btn_Edit_CustomerInfo.isHidden = false
            }
            else
            {
                cell.btn_Edit_CustomerInfo.isHidden = false
                
                if !isEditSchedule
                {
                    cell.btn_Edit_Schedule.isHidden = true
                }
                else
                {
                    cell.btn_Edit_Schedule.isHidden = false
                }
            }
            
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesNewGeneralInfo_Serv_Req" : "SalesNewGeneralInfo_Serv_Req", for: indexPath as IndexPath) as! SalesNewGeneralInfo
            cellServiceRequiredConfigure(cell: cell)
            return cell
        }
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableView.automaticDimension
     
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func fetchTagDetail() -> String  {
        
        var strTagDetail = ""
        
        
        let arrTagMasterId = NSMutableArray()
        let arySelectedTags = NSMutableArray()
        
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadTagExtDcs", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"))
                
        if arryOfData.count > 0
        {
            for item in arryOfData
            {
                let objMatch = item as! NSManagedObject
                arrTagMasterId.add("\(objMatch.value(forKey: "tagMasterId") ?? "")")
            }
            
            for item in arrTagMasterId
            {
                let strId = item as! String
                
                for itemNew in SalesNew_EditMainInformationVC().getAllTag()
                {
                    let dict = itemNew as! NSDictionary
                    
                    if "\(dict.value(forKey: "TagMasterId") ?? "")" == strId
                    {
                        arySelectedTags.add(dict)
                    }
                }
            }
            
            var strName = ""
            for item in arySelectedTags
            {
                strName = "\(strName), \((item as AnyObject).value(forKey: "Tag") ?? "")"
            }
            if strName.count != 0{
                strName = String(strName.dropFirst())
            }
            strTagDetail = strName
        }
        else
        {
            strTagDetail = ""
            
        }
        
        return strTagDetail
    }
    
    func cellCustomerInfoConfigure(cell : SalesNewGeneralInfo) {
        if(cell.segment.selectedSegmentIndex == 0){
            cell.stkviewBillingInfo.isHidden = true
            cell.stkviewServiceInfo.isHidden = false
        }else{
            cell.stkviewServiceInfo.isHidden = true
            cell.stkviewBillingInfo.isHidden = false
        }
        print(dataGeneralInfo)
        let strleadNumber =  dataGeneralInfo.value(forKey: "leadNumber") ?? " "
        let strleadName =  dataGeneralInfo.value(forKey: "leadName") ?? " "
        let straccountNo =  dataGeneralInfo.value(forKey: "accountNo") ?? " "
        let strFirstName =  dataGeneralInfo.value(forKey: "firstName") ?? " "
        let strMiddleName =  dataGeneralInfo.value(forKey: "middleName") ?? " "
        let strLastName =  dataGeneralInfo.value(forKey: "lastName") ?? " "
        let strCompanyName =  dataGeneralInfo.value(forKey: "companyName") ?? " "
        let strPrimaryEmail =  dataGeneralInfo.value(forKey: "primaryEmail") ?? ""
        let strPrimaryPhone =  "\(dataGeneralInfo.value(forKey: "primaryPhone") ?? "")"
        let strSecondaryEmail =  dataGeneralInfo.value(forKey: "secondaryEmail") ?? ""
        let strSecondaryPhone =  dataGeneralInfo.value(forKey: "secondaryPhone") ?? ""
        let strCellNo =  "\(dataGeneralInfo.value(forKey: "cellNo") ?? "")"
        let strServiceAddress =  Global().strCombinedAddressService(for: dataGeneralInfo) ?? " "
        let strBillingAddress =  Global().strCombinedAddressBilling(for: dataGeneralInfo)

        let strFlowType =  dataGeneralInfo.value(forKey: "flowType") ?? ""
        let strDriveTime =  dataGeneralInfo.value(forKey: "driveTime") ?? ""
        let strSalesType =  dataGeneralInfo.value(forKey: "salesType") ?? ""
        let strPrioritySysName =  "\(dataGeneralInfo.value(forKey: "prioritySysName") ?? "")"
        let aryUrgencySysName = strPrioritySysName.split(separator: ",")
        let strSourceSysName =  "\(dataGeneralInfo.value(forKey: "sourceSysName") ?? "")"
        let arySourceSysName = strSourceSysName.split(separator: ",")
        let strBranchSysName =  "\(dataGeneralInfo.value(forKey: "branchSysName") ?? "")"
        let aryBranchSysName = strBranchSysName.split(separator: ",")
        let isBillingAddressSame =  "\(dataGeneralInfo.value(forKey: "isBillingAddressSame") ?? "")"

        //Service Info
        let serviceFirstName =  "\(dataGeneralInfo.value(forKey: "serviceFirstName") ?? "")"
        let serviceMiddleName =  "\(dataGeneralInfo.value(forKey: "serviceMiddleName") ?? "")"
        let serviceLastName =  "\(dataGeneralInfo.value(forKey: "serviceLastName") ?? "")"
        let aryPOC = [serviceFirstName,serviceMiddleName,serviceLastName].filter({ $0 != ""})
        let servicePOC =  aryPOC.joined(separator: " ") //", "

        let serviceSchoolDistrict =  "\(dataGeneralInfo.value(forKey: "serviceSchoolDistrict") ?? "")"
        let serviceMapCode =  "\(dataGeneralInfo.value(forKey: "serviceMapCode") ?? "")"
        let serviceGateCode =  "\(dataGeneralInfo.value(forKey: "serviceGateCode") ?? "")"
        let serviceCounty =  "\(dataGeneralInfo.value(forKey: "serviceCounty") ?? "")"

        let serviceCellNo =  "\(dataGeneralInfo.value(forKey: "serviceCellNo") ?? "")"
        let servicePrimaryPhone =  "\(dataGeneralInfo.value(forKey: "servicePrimaryPhone") ?? "")"
        let serviceSecondaryPhone =  "\(dataGeneralInfo.value(forKey: "serviceSecondaryPhone") ?? "")"
        let aryServiceCell = [formattedNumber(number: serviceCellNo),formattedNumber(number: servicePrimaryPhone),formattedNumber(number: serviceSecondaryPhone)].filter({ $0 != ""})
        let serviceCall =  aryServiceCell.joined(separator: ", ")
        
        let servicePrimaryEmail =  "\(dataGeneralInfo.value(forKey: "servicePrimaryEmail") ?? "")"
        let serviceSecondaryEmail =  "\(dataGeneralInfo.value(forKey: "serviceSecondaryEmail") ?? "")"
        let aryServiceEmail = [servicePrimaryEmail,serviceSecondaryEmail].filter({ $0 != ""})
        let serviceEmail =  aryServiceEmail.joined(separator: ", ")
      
        
        //Billing Info
        let billingFirstName =  "\(dataGeneralInfo.value(forKey: "billingFirstName") ?? "")"
        let billingMiddleName =  "\(dataGeneralInfo.value(forKey: "billingMiddleName") ?? "")"
        let billingLastName =  "\(dataGeneralInfo.value(forKey: "billingLastName") ?? "")"
        let arybillingPOC = [billingFirstName,billingMiddleName,billingLastName].filter({ $0 != ""})
        let billingPOC =  arybillingPOC.joined(separator: " ") //", "
        
        let billingSchoolDistrict =  "\(dataGeneralInfo.value(forKey: "billingSchoolDistrict") ?? "")"
        let billingPrimaryEmail =  "\(dataGeneralInfo.value(forKey: "billingPrimaryEmail") ?? "")"
        let billingSecondaryEmail =  "\(dataGeneralInfo.value(forKey: "billingSecondaryEmail") ?? "")"
        let aryBillingEmail = [billingPrimaryEmail,billingSecondaryEmail].filter({ $0 != ""})
        let billingEmail =  aryBillingEmail.joined(separator: ", ")
        
        let billingMapcode =  "\(dataGeneralInfo.value(forKey: "billingMapcode") ?? "")"
        let billingCounty =  "\(dataGeneralInfo.value(forKey: "billingCounty") ?? "")"
       
        let billingCellNo =  "\(dataGeneralInfo.value(forKey: "billingCellNo") ?? "")"
        let billingPrimaryPhone =  "\(dataGeneralInfo.value(forKey: "billingPrimaryPhone") ?? "")"
        let billingSecondaryPhone =  "\(dataGeneralInfo.value(forKey: "billingSecondaryPhone") ?? "")"
        let aryBillingCell = [formattedNumber(number: billingCellNo),formattedNumber(number: billingPrimaryPhone),formattedNumber(number: billingSecondaryPhone)].filter({ $0 != ""})
        let billingCall =  aryBillingCell.joined(separator: ", ")
   
        
        //AlertAMount Tag
        let accountDescription =  "\(dataGeneralInfo.value(forKey: "accountDescription") ?? "")"
      //  let LeadTagExtDcs =  dataGeneralInfo.value(forKey: "LeadTagExtDcs") as! NSArray

        //Scheduled
        let scheduleStartDate =  "\(dataGeneralInfo.value(forKey: "scheduleStartDate") ?? "")"
        let earliestStartTime =  "\(dataGeneralInfo.value(forKey: "earliestStartTime") ?? "")"
        let latestStartTime =  "\(dataGeneralInfo.value(forKey: "latestStartTime") ?? "")"

    
        
        
        //----Header
        lblTitle.text =  Global().strFullName(fromCoreDB: dataGeneralInfo)
        lblSubTitle.text = "Opp# \(strleadNumber)" + " " + "Acc# \(straccountNo)"
        nsud.setValue(lblTitle.text, forKey: "titleHeader1")
        nsud.setValue(lblSubTitle.text, forKey: "titleHeader2")
        nsud.synchronize()
        
        //----1 Block
        cell.lbl_CustomerName.text = ReplaceBlankToSpace(str: Global().strFullName(fromCoreDB: dataGeneralInfo))
        cell.lbl_CompanyName.text = ReplaceBlankToSpace(str: "\(strCompanyName)")
        cell.lbl_Email.text = ReplaceBlankToSpace(str: "\(strPrimaryEmail)")
        cell.lbl_Email.isUserInteractionEnabled = true
        cell.lbl_Email.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnEmail(sender:))))
        cell.lbl_Mobile.text = formattedNumber(number: ReplaceBlankToSpace(str: "\(strCellNo == "" ? strPrimaryPhone : strCellNo)"))
        cell.lbl_Mobile.text =  cell.lbl_Mobile.text == "" ? " " : cell.lbl_Mobile.text
        cell.lbl_Mobile.isUserInteractionEnabled = true
        cell.lbl_Mobile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnCall(sender:))))
        cell.lbl_Address.attributedText = NSAttributedString(string: ReplaceBlankToSpace(str: "\(strServiceAddress)"), attributes:[.underlineStyle: NSUnderlineStyle.single.rawValue ,NSAttributedString.Key.font: cell.lbl_Address.font])
        cell.lbl_Address.isUserInteractionEnabled = true
        cell.lbl_Address.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnAddress(sender:))))
        //----2 Block
        cell.lbl_Type.text = "\(strFlowType)"
        cell.lbl_TypeNew.text = "\(strFlowType)"

        cell.lbl_Drive.text = "\(strDriveTime)"
        if "\(strDriveTime)" == "" {
            cell.viewDrive.isHidden = true
        }else{
            cell.viewDrive.isHidden = false
        }
        
        cell.lbl_Lead.text = "\(strSalesType)"
        
        if cell.lbl_Lead.text?.caseInsensitiveCompare("inside") == .orderedSame
        {
            cell.lbl_Lead.text = "Inside"
        }
        else if cell.lbl_Lead.text?.caseInsensitiveCompare("field") == .orderedSame
        {
            cell.lbl_Lead.text = "Field"
        }
        else
        {
            cell.lbl_Lead.text = "Inside"
        }
        
                   
        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: arySourceSysName as NSArray, getmasterName: "LeadDetailMaster", getkeyName: "SourceMasters", keyName: "Name", keySysName: "SourceSysName") { (result, name, sysName) in
            cell.lbl_Source.text = name
        }
        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryBranchSysName as NSArray, getmasterName: "LeadDetailMaster", getkeyName: "BranchMastersAll", keyName: "Name", keySysName: "SysName") { (result, name, sysName) in
            cell.lbl_Branch.text = name
        }
        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryUrgencySysName as NSArray, getmasterName: "LeadDetailMaster", getkeyName: "UrgencyMasters", keyName: "Name", keySysName: "SysName") { (result, name, sysName) in
            cell.lbl_Urgency.text = name
        }
                
        //------3. For Billing Address
        if isBillingAddressSame.lowercased() == "true" || isBillingAddressSame == "1"{
            cell.imgbtnSameAsService.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            cell.stkviewBillingAddress.isHidden = true
            cell.lbl_BillingAddress.text = ""
        }else{
            cell.imgbtnSameAsService.setImage(UIImage(named: "check_box_1.png"), for: .normal) // for check
            cell.stkviewBillingAddress.isHidden = false
            cell.lbl_BillingAddress.text = strBillingAddress
            
        }
        
        //-------4. ServicePOC
        cell.lblServicePOC.text = servicePOC
        cell.lblServiceGetCode.text = serviceGateCode
        cell.lblServiceMapCode.text = serviceMapCode
        cell.lblServiceSchoolDis.text = serviceSchoolDistrict
        cell.lblServiceCountyCode.text = serviceCounty
        cell.lblServiceEmail.text = serviceEmail
        cell.lblServiceEmail.isUserInteractionEnabled = true
        cell.lblServiceEmail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnServiceEmail(sender:))))
        cell.lblServicePhone.text = serviceCall
        cell.lblServicePhone.isUserInteractionEnabled = true
        cell.lblServicePhone.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnServiceCall(sender:))))
        //-------4. BiilingPOC
        cell.lblBillingPOC.text = billingPOC
        cell.lblBillingMapCode.text = billingMapcode
        cell.lblBillingCountyCode.text = billingCounty
        cell.lblBillingSchoolDis.text = billingSchoolDistrict
        cell.lblServiceCountyCode.text = serviceCounty
        cell.lblBillingEmail.text = billingEmail
        cell.lblBillingEmail.isUserInteractionEnabled = true
        cell.lblBillingEmail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnBillingEmail(sender:))))
        cell.lblBillingPhone.text = billingCall
        cell.lblBillingPhone.isUserInteractionEnabled = true
        cell.lblBillingPhone.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnBillingCall(sender:))))
        //---------5.Alert Or AMount OR Tag
        cell.lblAlert.numberOfLines = 0
        cell.lblAlert.text = accountDescription == "" ? "" : "Account Alert : \(accountDescription)"
        
//     var TagMasterId = ""
//        for item in LeadTagExtDcs {
//            let Name = (item as! NSDictionary).value(forKey: "TagMasterId") as! String
//            TagMasterId = TagMasterId + "\(Name),"
//        }
//        if TagMasterId.count != 0 {
//            TagMasterId = String(TagMasterId.dropLast())
//        }
//        let aryTagMasterId = TagMasterId.split(separator: ",")
//
//        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryTagMasterId as NSArray, getmasterName: "LeadDetailMaster", getkeyName: "TagMasters", keyName: "Tag", keySysName: "TagMasterId") { (result, name, sysName) in
//            cell.lblTags.text = name
//        }
//
        cell.lblTags.text = fetchTagDetail()
        
        
        
        //-------6. Schedule
        
        cell.lblScheduleDate.text = changeStringDateToGivenFormat(strDate: scheduleStartDate, strRequiredFormat: "MM/dd/yyyy hh:mm a")
        //Scheduled TimeRangeId
        var aryTimeRange  = NSArray()
        if("\(dataGeneralInfo.value(forKey: "timeRangeId") ?? "")") != "" && "\(dataGeneralInfo.value(forKey: "timeRangeId") ?? "")" != "0"{
            if(nsud.value(forKey: "MasterSalesTimeRange") != nil){
                if(nsud.value(forKey: "MasterSalesTimeRange") is NSArray){
                    
                    aryTimeRange =  (nsud.value(forKey: "MasterSalesTimeRange") as! NSArray).filter { (task) -> Bool in
                        return ("\((task as! NSDictionary).value(forKey: "RangeofTimeId")!)".lowercased().contains("\(dataGeneralInfo.value(forKey: "timeRangeId") ?? "")"))
                    } as NSArray
                }
            }
            
            
            if(aryTimeRange.count != 0){
                let dictData = aryTimeRange.object(at: 0) as! NSDictionary
                let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
                let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
                let strrenge = StartInterval + " - " + EndInterval
                cell.lblScheduleDateRange.text = "Range \(strrenge)"
                
            }
        }else{
            cell.lblScheduleDateRange.text = ""
        }

    }
    func cellServiceRequiredConfigure(cell : SalesNewGeneralInfo) {
        
        if(getStatusCompleteWon())
        {
            cell.btn_EditCustomerNotes.isHidden = true
            cell.btn_EditInternalNotes.isHidden = false
            cell.btn_EditProblemIdentification.isHidden = false
            cell.btn_EditDescription.isHidden = false
        }
        else
        {
            cell.btn_EditProblemIdentification.isHidden = false
            cell.btn_EditInternalNotes.isHidden = false
            cell.btn_EditCustomerNotes.isHidden = false
            cell.btn_EditDescription.isHidden = false

        }
        
        cell.btn_EditProblemIdentification.addTarget(self, action: #selector(actionProblemDescription), for: .touchUpInside)
        cell.btn_EditDescription.addTarget(self, action: #selector(actionEditDescription), for: .touchUpInside)
        cell.btn_EditInternalNotes.addTarget(self, action: #selector(actionEditInternalNotes), for: .touchUpInside)
        cell.btn_EditCustomerNotes.addTarget(self, action: #selector(actionEditCustomerNotes), for: .touchUpInside)
  
        let problemIdentification =  "\(dataGeneralInfo.value(forKey: "problemDescription") ?? "")"
        let description =  "\(dataGeneralInfo.value(forKey: "descriptionLeadDetail") ?? "")"
        let internalNotes =  "\(dataGeneralInfo.value(forKey: "notes") ?? "")"
        let customerNotes =  "\(dataGeneralInfo.value(forKey: "notes") ?? "")"

        cell.lbl_ProblemIdentification.text = problemIdentification
        cell.lbl_Description.text = description
        cell.lbl_InternalNotes.text = internalNotes
        cell.lbl_CustomerNote.text = customerNotes
        
        let arrayPaymentDetail = getDataFromLocal(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"))
        if arrayPaymentDetail.count > 0
        {
            let matches = arrayPaymentDetail.object(at: 0) as! NSManagedObject
            let strTxt =  "\(matches.value(forKey: "specialInstructions") ?? "")"
            cell.lbl_CustomerNote.text = strTxt
        }
        else
        {
            cell.lbl_CustomerNote.text = ""

        }

        
    }
    
    func ReplaceBlankToSpace(str: String) -> String {
        return str == "" ? " " : str
    }
        
    func downloadAllImages()  {
        
        let arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"))
        
        for item in arrayOfImages
        {
            let objTemp = item as! NSManagedObject

            let strImagePath = objTemp.value(forKey: "leadImagePath") as! String
                        
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                                
            }else {
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                //if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")
                {
                    
                    strURL = "\(value)"
                    
                }
                
                if strImagePath.contains("Documents")
                {
                    strURL = strURL + "\(strImagePath)"
                }
                else
                {
                     strURL = strURL + "/Documents/" + "\(strImagePath)"
                }
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)

                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)

                        }}
                    
                }
            }
        }

    }
    func downloadAllCRMImages()  {
        
        let arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@", "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"))
        
        for item in arrayOfImages
        {
            let objTemp = item as! NSManagedObject

            let strImagePath = objTemp.value(forKey: "path") as! String
                        
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                                
            }else {
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                //if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")
                {
                    
                    strURL = "\(value)"
                    
                }
                
//                if strImagePath.contains("Documents")
//                {
//                    strURL = strURL + "\(strImagePath)"
//                }
//                else
//                {
//                     strURL = strURL + "/Documents/" + "\(strImagePath)"
//                }
                strURL = strURL + "/Documents/" + "\(strImagePath)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)

                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)

                        }}
                    
                }
            }
        }

    }
    // MARK:
    // MARK: -----------CustomerInfo Action---------
    @objc func imageTapped(gesture: UIGestureRecognizer) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        alert.view.addSubview(UIView())
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        alert.view.tintColor = UIColor.darkGray
        self.present(alert, animated: true, completion: nil)
    }
    @objc func actionCustomerInfoEdit(sender : UIButton) {
        self.view.endEditing(true)
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditGeneralInfo") as! SalesNew_EditGeneralInfo
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @objc func actionViewSegment(sender : UISegmentedControl) {
        self.view.endEditing(true)
        self.tblView.reloadData()

    }
    @objc func actionViewBtnAddress(sender : UIButton) {
        self.view.endEditing(true)
        if isInternetAvailable() {
                        
            let strAddress = Global().strCombinedAddressService(for: dataGeneralInfo)
            
            if strAddress!.count > 0
            {
                
                let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
                // Create the actions
                
                let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    Global().redirect(onAppleMap: self, strAddress)
                    
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    @objc func actionViewBtnEmail(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        let cell = tblView.cellForRow(at: indexPath1)as! SalesNewGeneralInfo
        if(cell.lbl_Email.text?.trimmed.count != 0 ){
            guard MFMessageComposeViewController.canSendText() else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMailComposer, viewcontrol: self)

                      return
                  }
                  let messageVC = MFMessageComposeViewController()
                  messageVC.body = "";
                  messageVC.recipients = ["\(cell.lbl_Email.text!)"]
                  messageVC.messageComposeDelegate = self;
                  self.present(messageVC, animated: false, completion: nil)
        }
     
    }
    @objc func actionViewBtnCall(sender : UIButton) {
        self.view.endEditing(true)
        let cell = tblView.cellForRow(at: indexPath1)as! SalesNewGeneralInfo
        if(cell.lbl_Mobile.text?.trimmed.count != 0 ){
            if !(callingFunction(number: cell.lbl_Mobile.text!.trimmed as NSString)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)

        }
        }
    }
    @objc func actionViewBtnSchedule(sender : UIButton) {
        
        if getStatusCompleteWon()
        {
            
        }
        else
        {
            self.view.endEditing(true)
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditScheduleVC") as! SalesNew_EditScheduleVC
            vc.dataGeneralInfo = dataGeneralInfo

            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    
    
    // MARK:
    // MARK: -----------Service Info Action---------
    @objc func actionViewBtnServiceEmail(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        let cell = tblView.cellForRow(at: indexPath1)as! SalesNewGeneralInfo
        if(cell.lblServiceEmail.text?.trimmed.count != 0 ){
            guard MFMessageComposeViewController.canSendText() else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMailComposer, viewcontrol: self)

                      return
                  }
                  let messageVC = MFMessageComposeViewController()
                  messageVC.body = "";
                  messageVC.recipients = ["\(cell.lblServiceEmail.text!)"]
                  messageVC.messageComposeDelegate = self;
                  self.present(messageVC, animated: false, completion: nil)
        }
     
    }
    @objc func actionViewBtnServiceCall(sender : UIButton) {
        self.view.endEditing(true)
        let cell = tblView.cellForRow(at: indexPath1)as! SalesNewGeneralInfo
        let aryNumber = cell.lblServicePhone.text?.split(separator: ",")
        if(aryNumber!.count > 1){
            let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.alert)
            for item in aryNumber! {
                alert.addAction(UIAlertAction (title: "\(item)", style: .default, handler: { (nil) in
                    
                    if(("\(item)") != ""){
                        if !(callingFunction(number: "\(item)".trimmed as NSString)){
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
                    }
                    }
             
                }))
            }
            alert.addAction(UIAlertAction (title: "Dismiss", style: .default, handler: { (nil) in
            }))
            alert.view.tintColor = UIColor.black
            self.present(alert, animated: true, completion: nil)
        }else{
            if((cell.lblServicePhone.text!.trimmed as NSString) != ""){
                if !(callingFunction(number: cell.lblServicePhone.text!.trimmed as NSString)){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
            }
            }
            
          
        }
       
    }
    // MARK:
    // MARK: -----------Billing Info Action---------
    @objc func actionViewBtnBillingEmail(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        let cell = tblView.cellForRow(at: indexPath1)as! SalesNewGeneralInfo
        if(cell.lblBillingEmail.text?.trimmed.count != 0 ){
            guard MFMessageComposeViewController.canSendText() else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMailComposer, viewcontrol: self)

                      return
                  }
                  let messageVC = MFMessageComposeViewController()
                  messageVC.body = "";
                  messageVC.recipients = ["\(cell.lblBillingEmail.text!)"]
                  messageVC.messageComposeDelegate = self;
                  self.present(messageVC, animated: false, completion: nil)
        }
     
    }
    @objc func actionViewBtnBillingCall(sender : UIButton) {
        self.view.endEditing(true)
        let cell = tblView.cellForRow(at: indexPath1)as! SalesNewGeneralInfo
        let aryNumber = cell.lblBillingPhone.text?.split(separator: ",")
        if(aryNumber!.count > 1){
            let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.alert)
            for item in aryNumber! {
                alert.addAction(UIAlertAction (title: "\(item)", style: .default, handler: { (nil) in
                    if(("\(item)".trimmed as NSString) != ""){
                        if !(callingFunction(number: "\(item)".trimmed as NSString)){
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
                    }
                    }
            
                }))
            }
            alert.addAction(UIAlertAction (title: "Dismiss", style: .default, handler: { (nil) in
            }))
            alert.view.tintColor = UIColor.black
            self.present(alert, animated: true, completion: nil)
        }else{
            
            if((cell.lblBillingPhone.text!.trimmed as NSString) != ""){
                if !(callingFunction(number: cell.lblBillingPhone.text!.trimmed as NSString)){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
            }
            }
       
        }
       
    }
    // MARK:
    // MARK: ----------ServiceRequired Action---------
    
    @objc func actionProblemDescription(sender : UIButton) {
        self.view.endEditing(true)
        isEditInSalesAuto = true
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditServiceRequiredVC") as! SalesNew_EditServiceRequiredVC
        vc.strTitle = "Problem Description"
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @objc func actionEditDescription(sender : UIButton) {
        isEditInSalesAuto = true
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditServiceRequiredVC") as! SalesNew_EditServiceRequiredVC
        vc.strTitle = "Description"
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @objc func actionEditInternalNotes(sender : UIButton) {
        
        isEditInSalesAuto = true
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditServiceRequiredVC") as! SalesNew_EditServiceRequiredVC
        vc.strTitle = "Internal Notes"
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @objc func actionEditCustomerNotes(sender : UIButton) {
        
        isEditInSalesAuto = true
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditServiceRequiredVC") as! SalesNew_EditServiceRequiredVC
        vc.strTitle = "Customer Notes"
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    
    
}
// MARK:
// MARK: ----------SalesNewGeneralInfo Cell---------


class SalesNewGeneralInfo : UITableViewCell {
    
    //------Customer Info-----
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lbl_Type: UILabel!
    @IBOutlet weak var lbl_TypeNew: UILabel!


    @IBOutlet weak var lbl_CustomerName: UILabel!
    @IBOutlet weak var lbl_CompanyName: UILabel!
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_Mobile: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
  
    @IBOutlet weak var lbl_Drive: UILabel!
    @IBOutlet weak var viewDrive: UIView!

    @IBOutlet weak var lbl_Lead: UILabel!
    @IBOutlet weak var lbl_Source: UILabel!
    @IBOutlet weak var lbl_Branch: UILabel!
    @IBOutlet weak var lbl_Urgency: UILabel!
    @IBOutlet weak var lbl_BillingAddress: UILabel!
    @IBOutlet weak var btnSameAsService: UIButton!
    @IBOutlet weak var imgbtnSameAsService: UIButton!
    @IBOutlet weak var stkviewBillingAddress: UIStackView!

    @IBOutlet weak var segment: UISegmentedControl! {
        didSet {
//            height_Segment.constant = DeviceType.IS_IPAD ? 40.0 :  30.0
//            let font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 :  15)
//            UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            
            height_Segment.constant = DeviceType.IS_IPAD ? 40.0 :  30.0
            let font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 :  15)
            UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            segment.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            segment.setTitleTextAttributes([.foregroundColor: UIColor.appThemeColor], for: .normal)
        }
    }
    @IBOutlet weak var height_Segment: NSLayoutConstraint!

    @IBOutlet weak var stkviewServiceInfo: UIStackView!
    @IBOutlet weak var lblServicePOC: UILabel!
    @IBOutlet weak var lblServiceGetCode: UILabel!
    @IBOutlet weak var lblServiceMapCode: UILabel!
    @IBOutlet weak var lblServiceCountyCode: UILabel!
    @IBOutlet weak var lblServiceSchoolDis: UILabel!
    @IBOutlet weak var lblServiceEmail: UILabel!
    @IBOutlet weak var lblServicePhone: UILabel!

    
    @IBOutlet weak var stkviewBillingInfo: UIStackView!
    @IBOutlet weak var lblBillingPOC: UILabel!
    @IBOutlet weak var lblBillingMapCode: UILabel!
    @IBOutlet weak var lblBillingCountyCode: UILabel!
    @IBOutlet weak var lblBillingSchoolDis: UILabel!
    @IBOutlet weak var lblBillingEmail: UILabel!
    @IBOutlet weak var lblBillingPhone: UILabel!
    
    
    @IBOutlet weak var lblAccountBalance: UILabel!
    @IBOutlet weak var lblTags: UILabel!
    @IBOutlet weak var lblAlert: UILabel!

    @IBOutlet weak var btn_Edit_CustomerInfo: UIButton!
    @IBOutlet weak var btn_Edit_Schedule: UIButton!

    @IBOutlet weak var lblScheduleDate: UILabel!
    @IBOutlet weak var lblScheduleDateRange: UILabel!
    //------Service Required-----
    
    @IBOutlet weak var btn_EditProblemIdentification: UIButton!
    @IBOutlet weak var btn_EditDescription: UIButton!
    @IBOutlet weak var btn_EditInternalNotes: UIButton!
    @IBOutlet weak var btn_EditCustomerNotes: UIButton!

    @IBOutlet weak var lbl_ProblemIdentification: UILabel!
    @IBOutlet weak var lbl_Description: UILabel!
    @IBOutlet weak var lbl_InternalNotes: UILabel!
    @IBOutlet weak var lbl_CustomerNote: UILabel!

    
}


// MARK: - ----------------MFMessageComposeViewControllerDelegate
// MARK: -

extension SalesNew_GeneralInfoVC: MFMessageComposeViewControllerDelegate{
func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
       switch (result.rawValue) {
           case MessageComposeResult.cancelled.rawValue:
           print("Message was cancelled")
           self.dismiss(animated: true, completion: nil)
       case MessageComposeResult.failed.rawValue:
           print("Message failed")
           self.dismiss(animated: true, completion: nil)
       case MessageComposeResult.sent.rawValue:
           print("Message was sent")
           self.dismiss(animated: true, completion: nil)
       default:
           break;
       }
   }
}
// MARK:- Image Picker Controller Delegate
extension SalesNew_GeneralInfoVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            
            self.isEditInSalesAuto = true
            guard let image = info[.editedImage] as? UIImage else { return }
            self.serviceAddressImagePathTagLocal = 1
            self.serviceAddressImagePath = "Img" + "\(self.dataGeneralInfo.value(forKey: "leadId") ?? "")" + "\(getUniqueValueForId())" + ".jpg"
            let imageResized = Global().resizeImageGloballl(image)
            saveImageDocumentDirectory(strFileName: self.serviceAddressImagePath, image: imageResized!)
            self.updateWorkOrder(strWdoID: "\(self.dataGeneralInfo.value(forKey: "leadId") ?? "")")
        

            //I

        }
    }
}
