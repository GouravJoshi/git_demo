//
//  SalesNew_SelectionVC.swift
//  DPS
//
//  Created by NavinPatidar on 5/21/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_SelectionVC: UIViewController {
    // MARK: -
    // MARK: -variable
    weak var delegate: SalesNew_SelectionProtocol?
    @objc var strTitle = String() , strShowNameKey = String()
    @objc var aryItems = NSMutableArray() , aryMainItems = NSMutableArray()
    @objc var arySelectedItems = NSMutableArray()
    @objc var arrDepartmentCategory = NSMutableArray()
    @objc var isForCategory = false

    @objc var strTag = Int()
    @objc var isMultiSelection = Bool()
    @objc var isFromSalesSelection = Bool()
    // MARK: -
    // MARK: -IBOutlet
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblTitle: UILabel!

    // MARK: -
    // MARK: -LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        tvForList.tableFooterView = UIView()
        tvForList.dataSource = self
        tvForList.delegate = self
        //self.view.backgroundColor = UIColor.clear
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 14)
        } else {
            // Fallback on earlier versions
        }
        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
        }
        searchBar.layer.borderWidth = 0
        searchBar.layer.opacity = 1.0
        
        aryMainItems = aryItems
        lblTitle.text = strTitle
    }
    // MARK: -
    // MARK: -IBAction
    @IBAction func actionOnBack(_ sender: UIButton)
    {
        self.view.endEditing(true)
        back()
    }
    func back()  {
        
        if isFromSalesSelection
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.dismiss(animated: false, completion: nil)
            
        }
    }
    
    @IBAction func actionOnClickVoiceRecognizationButton(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.aryMainItems.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }
    @IBAction func actionOnDone(_ sender: UIButton)
    {
        if(arySelectedItems.count != 0){
            if(isMultiSelection){
                delegate?.didSelect(aryData: arySelectedItems, tag: self.strTag)
            }else{
                let dict = arySelectedItems.object(at: 0)as! NSDictionary
                delegate?.didSelect(dictData: dict, tag: self.strTag)
            }
            back()
            self.view.endEditing(true)
        }
        else
        {
                    
            if(isMultiSelection){
                delegate?.didSelect(aryData: arySelectedItems, tag: self.strTag)
            }else{
                let dict = NSDictionary()
                delegate?.didSelect(dictData: dict, tag: self.strTag)
            }
            
            back()
            self.view.endEditing(true)
        }
    }
}

// MARK: -
// MARK: -UITableViewDelegate

extension SalesNew_SelectionVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.contentView.backgroundColor = UIColor.clear
        cell?.backgroundColor = UIColor.clear
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        }
        let dict = aryItems.object(at: indexPath.row)as! NSDictionary
        cell?.textLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 15 : 12)
        cell!.textLabel?.numberOfLines = 0
        

        var strDeptName = ""
        
        if isForCategory
        {
            let arr = arrDepartmentCategory.filter { (dictDept) -> Bool in
                
                return "\((dictDept as! NSDictionary).value(forKey: "SysName") ?? "")" == "\(dict.value(forKey: "DepartmentSysName") ?? "")"
            } as NSArray
            
            if arr.count > 0
            {
                let dictObject = arr.object(at: 0) as! NSDictionary
                
                strDeptName = "\(dictObject.value(forKey: "Name") ?? "")"
            }
        }
        
        
        
        let keyName = strShowNameKey.split(separator: ",")
        if keyName.count == 1 {
            cell!.textLabel?.text = "\(dict.value(forKey: "\(strShowNameKey)")!)"
        }else{
            cell!.textLabel?.text =  "\(dict.value(forKey: String(keyName[0]))!)" + " - " + "\(dict.value(forKey: String(keyName[1]))!)"
        }
        
        if isForCategory
        {
            if let strTitle = cell!.textLabel?.text
            {
                cell!.textLabel?.text = "\(strTitle) - (\(strDeptName))"
            }
            
        }

        if arySelectedItems.contains(dict) {
            cell?.accessoryType = .checkmark
        }else{
            cell?.accessoryType = .none
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isMultiSelection {
            let dict = aryItems.object(at: indexPath.row)as! NSDictionary

            if arySelectedItems.contains(dict) {
                arySelectedItems.remove(dict)
            }else{
                arySelectedItems.add(dict)
            }
            self.tvForList.reloadData()
        }else{
            let dict = aryItems.object(at: indexPath.row)as! NSDictionary
            delegate?.didSelect(dictData: dict, tag: self.strTag)
            back()
        }
    }
    
}
// MARK: ----
// MARK: -UISearchBar delegate
extension SalesNew_SelectionVC : UISearchBarDelegate{

func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    searchAutocomplete(searchText: searchText)
}

func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    self.view.endEditing(true)
    if(searchBar.text?.count == 0)
    {
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter text to be searched.", viewcontrol: self)
    }
}

func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.text = ""
    self.view.endEditing(true)
    self.aryItems = NSMutableArray()
    self.aryItems = aryMainItems
    self.tvForList.dataSource = self
    self.tvForList.delegate = self
    self.tvForList.reloadData()
}
    func searchAutocomplete(searchText: String) -> Void{
        if searchText.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.resignFirstResponder()
            }
            self.aryItems = NSMutableArray()
            self.aryItems = aryMainItems
            self.tvForList.dataSource = self
            self.tvForList.delegate = self
            self.tvForList.reloadData()
        }
        else{
            var aryTemp = NSArray()
            let keyName = strShowNameKey.split(separator: ",")
            if keyName.count == 1 {
           
                aryTemp = aryMainItems.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: strShowNameKey)!)".lowercased().contains(searchText.lowercased()) } as NSArray
            }
            else{
             
                aryTemp = aryMainItems.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: String(keyName[0]))!)".lowercased().contains(searchText.lowercased()) || "\((task as! NSDictionary).value(forKey: String(keyName[1]))!)".lowercased().contains(searchText.lowercased()) } as NSArray
            }
            
            
            self.aryItems = NSMutableArray()
            self.aryItems = (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            self.tvForList.dataSource = self
            self.tvForList.delegate = self
            self.tvForList.reloadData()
        }
    }
}

// MARK: ----
// MARK: -SalesNew_SelectionProtocol
extension SalesNew_SelectionVC : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if #available(iOS 13.0, *) {
            self.searchBar.searchTextField.text = str
        } else {
            self.searchBar.text = str
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.searchAutocomplete(searchText: str)
        }
    }
}

// MARK: -
// MARK: -SalesNew_SelectionProtocol
protocol SalesNew_SelectionProtocol : class
{
    func didSelect(dictData : NSDictionary ,tag : Int)
    func didSelect(aryData : NSMutableArray ,tag : Int)
}
