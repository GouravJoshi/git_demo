//
//  SalesNew_EditGeneralInfo.swift
//  DPS
//
//  Created by NavinPatidar on 5/14/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_EditGeneralInfo: UIViewController {
    //MARK:---------IBOutlet-----------
    //MARK:
    @IBOutlet weak var tblView: UITableView!
    var dataGeneralInfo = NSManagedObject()

    //MARK:---------Variable-----------
    //MARK:
    var arylist = NSMutableArray()
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 16.0 : 14.0),
        NSAttributedString.Key.foregroundColor : UIColor.darkGray,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    //MARK:---------LifeCycle-----------
    //MARK:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialUI()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        
        if getOpportunityStatus()
        {
            
        }

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
    }
    //MARK:-------Extra--Function-----------
    //MARK:
    func getOpportunityStatus() -> Bool {
        
        let strStatusss =  dataGeneralInfo.value(forKey: "statusSysName") ?? ""
        let strStageSysName =  dataGeneralInfo.value(forKey: "stageSysName") ?? ""
        
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won")
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func setInitialUI() {
        self.tblView.tableFooterView = UIView()
        arylist = [["title":"Edit Main Information","image":"Customer1"],
                   ["title":"Edit Service Address Information","image":"Map"],
                   ["title":"Edit Service Contact Details","image":"Customer1"],
                   ["title":"Edit Billing Address Information","image":"accounting.png"],
                   ["title":"Edit Billing Contact Details","image":"accounting.png"]]
        self.tblView.reloadData()
    }
    
    //MARK:-------Header--IBAction-----------
    //MARK:
    @IBAction func action_Back(_ sender: Any) {
        self.view.endEditing(true)

        self.navigationController?.popViewController(animated: false)
    }


}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SalesNew_EditGeneralInfo: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arylist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesNew_EditGeneralInfoCell" : "SalesNew_EditGeneralInfoCell", for: indexPath as IndexPath) as! SalesNew_EditGeneralInfoCell
        let dict = arylist[indexPath.row]as! NSDictionary
        cell.img.image = UIImage(named: "\(dict.value(forKey: "image")!)")
        cell.lbltitle.text = "\(dict.value(forKey: "title")!)"
        
        if getOpportunityStatus()
        {
            if indexPath.row == 1
            {
                cell.isUserInteractionEnabled = false
            }
        }
        
            return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DeviceType.IS_IPAD ? 60 : 50
     
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arylist[indexPath.row]as! NSDictionary
        
        switch indexPath.row {
        case 0:
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditMainInformationVC") as! SalesNew_EditMainInformationVC
            vc.dataGeneralInfo = dataGeneralInfo

            vc.strTitle = "\(dict.value(forKey: "title")!)"
            self.navigationController?.pushViewController(vc, animated: false)
        case 1:
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditServiceAddressInfoVC") as! SalesNew_EditServiceAddressInfoVC
            vc.strTitle = "\(dict.value(forKey: "title")!)"
            vc.dataGeneralInfo = dataGeneralInfo

            self.navigationController?.pushViewController(vc, animated: false)
        case 2:
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditServiceContactDetailVC") as! SalesNew_EditServiceContactDetailVC
            vc.strTitle = "\(dict.value(forKey: "title")!)"
            
            vc.dataGeneralInfo = dataGeneralInfo

            self.navigationController?.pushViewController(vc, animated: false)
        case 3:
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditBillingAddressInfoVC") as! SalesNew_EditBillingAddressInfoVC
            vc.strTitle = "\(dict.value(forKey: "title")!)"
            vc.dataGeneralInfo = dataGeneralInfo
            self.navigationController?.pushViewController(vc, animated: false)
        case 4:
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EditBillingContactDetailVC") as! SalesNew_EditBillingContactDetailVC
            vc.strTitle = "\(dict.value(forKey: "title")!)"
            vc.dataGeneralInfo = dataGeneralInfo
            self.navigationController?.pushViewController(vc, animated: false)
        default:
            break
        }
    }
    
}
// MARK: ----------SalesNewGeneralInfo Cell---------
// MARK:

class SalesNew_EditGeneralInfoCell : UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    
}
