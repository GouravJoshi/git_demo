//
//  SalesNew_EditBillingContactDetailVC.swift
//  DPS
//
//  Created by NavinPatidar on 5/14/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_EditBillingContactDetailVC: UIViewController {
    
    //MARK:---------IBOutlet-----------
    //MARK:
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
   
    @IBOutlet weak var tf_BillingFirstName: ACFloatingTextField!
    @IBOutlet weak var tf_BillingMiddleName: ACFloatingTextField!
    @IBOutlet weak var tf_BillingLastName: ACFloatingTextField!
    @IBOutlet weak var tf_PrimaryEmail: ACFloatingTextField!
    @IBOutlet weak var tf_SecondaryEmail: ACFloatingTextField!
    @IBOutlet weak var tf_PrimaryPhone: ACFloatingTextField!
    @IBOutlet weak var tf_SecondaryPhone: ACFloatingTextField!
    @IBOutlet weak var tf_CellPhone: ACFloatingTextField!
    //MARK:---------Variable-----------
    //MARK:
    var strTitle = ""
    var dataGeneralInfo = NSManagedObject()

    //MARK:---------LifeCycle-----------
    //MARK:
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        setInitialUI()

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
    }
    //MARK:-------Extra--Function-----------
    //MARK:
    func setInitialUI() {
        lblTitle.text = strTitle
        tf_BillingFirstName.delegate = self
        tf_BillingMiddleName.delegate = self
        tf_BillingLastName.delegate = self
        tf_PrimaryEmail.delegate = self
        tf_SecondaryEmail.delegate = self
        tf_PrimaryPhone.delegate = self
        tf_SecondaryPhone.delegate = self
        tf_CellPhone.delegate = self
        
        //Set Billing Contact Detail Info

        tf_BillingFirstName.text =  "\(dataGeneralInfo.value(forKey: "billingFirstName") ?? "")"
        tf_BillingMiddleName.text =  "\(dataGeneralInfo.value(forKey: "billingMiddleName") ?? "")"
        tf_BillingLastName.text =  "\(dataGeneralInfo.value(forKey: "billingLastName") ?? "")"
        tf_PrimaryEmail.text =  "\(dataGeneralInfo.value(forKey: "billingPrimaryEmail") ?? "")"
        tf_SecondaryEmail.text =  "\(dataGeneralInfo.value(forKey: "billingSecondaryEmail") ?? "")"
       
        
        tf_PrimaryPhone.text =  formattedNumber(number: "\(dataGeneralInfo.value(forKey: "billingPrimaryPhone") ?? "")")
        tf_SecondaryPhone.text = formattedNumber(number: "\(dataGeneralInfo.value(forKey: "billingSecondaryPhone") ?? "")")
        tf_CellPhone.text =  formattedNumber(number: "\(dataGeneralInfo.value(forKey: "billingCellNo") ?? "")")

      
  
    }
    func updateSalesWODetail(strWdoID : String) {
        let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@", strWdoID))
        if(arrayLeadDetail.count > 0){
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("billingFirstName")
            arrOfValues.add(tf_BillingFirstName.text?.trimmed ?? "") //1
            arrOfKeys.add("billingMiddleName")
            arrOfValues.add(tf_BillingMiddleName.text?.trimmed ?? "")//2
            arrOfKeys.add("billingLastName")
            arrOfValues.add(tf_BillingLastName.text?.trimmed ?? "")//3
            arrOfKeys.add("billingPrimaryEmail")
            arrOfValues.add(tf_PrimaryEmail.text?.trimmed ?? "")//4
            arrOfKeys.add("billingSecondaryEmail")
            arrOfValues.add(tf_SecondaryEmail.text?.trimmed ?? "")//5
            arrOfKeys.add("billingPrimaryPhone")
            arrOfValues.add(tf_PrimaryPhone.text?.trimmed ?? "")//6
            arrOfKeys.add("billingSecondaryPhone")
            arrOfValues.add(tf_SecondaryPhone.text?.trimmed ?? "")//7
            arrOfKeys.add("billingCellNo")
            arrOfValues.add(tf_CellPhone.text?.trimmed ?? "")//8
           
            arrOfKeys.add("modifiedBy")
            arrOfValues.add(Global().getEmployeeId())
            arrOfKeys.add("modifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS'Z", ""))
            arrOfKeys.add("userName")
            arrOfValues.add(Global().getUserName())
          
            if(getDataFromDbToUpdate(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@",strWdoID), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)){
                self.navigationController?.popViewController(animated: false)
            }
           
        }
    }
    
    
    func Validation() -> String {
        self.view.endEditing(true)
        if tf_BillingFirstName.text?.count == 0 {
            return "First name required!"
        }else if tf_BillingLastName.text?.count == 0 {
            return "Last name required!"
        }
        
        if tf_PrimaryEmail.text?.count != 0{
            
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(tf_PrimaryEmail.text!)
            if !(isValid) {
                return "Please enter valid primary email!"
            }
      
        }
        if tf_SecondaryEmail.text?.count != 0{
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(tf_SecondaryEmail.text!)
            if !(isValid) {
                return "Please enter valid secondary email!"
            }
           
        }
        if tf_PrimaryPhone.text?.count != 0{
            if tf_PrimaryPhone.text!.count < 12 {
                return "Please enter valid primary phone!"
            }
        }
        if tf_SecondaryPhone.text?.count != 0{
            if tf_SecondaryPhone.text!.count < 12 {
                return "Please enter valid secondary phone!"
            }
        }
        if tf_CellPhone.text?.count != 0{
            if tf_CellPhone.text!.count < 12 {
                return "Please enter valid cell phone!"
            }
        }
        return ""
    }
    //MARK:--------IBAction-----------
    //MARK:
    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }

    @IBAction func action_Save(_ sender: Any) {
        
        if(Validation() != ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Validation(), viewcontrol: self)
        }else{
            Global().updateSalesModifydate("\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String)
            
            updateSalesWODetail(strWdoID: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        }
        
    }

}

//MARK:------UITextFieldDelegate-----------
//MARK:
extension SalesNew_EditBillingContactDetailVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if textField == tf_CellPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = tf_CellPhone.text!
                strTextt = String(strTextt.dropLast())
                tf_CellPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        if textField == tf_PrimaryPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = tf_PrimaryPhone.text!
                strTextt = String(strTextt.dropLast())
                tf_PrimaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        if textField == tf_SecondaryPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = tf_SecondaryPhone.text!
                strTextt = String(strTextt.dropLast())
                tf_SecondaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 85)
    }
}
