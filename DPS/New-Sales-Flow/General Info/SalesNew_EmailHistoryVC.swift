//
//  SalesNew_EmailHistoryVC.swift
//  DPS
//
//  Created by NavinPatidar on 6/2/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit



class SalesNew_EmailHistoryVC: UIViewController {
    // MARK: -
    // MARK: -variable
    
    var aryEmailHistory = NSMutableArray() , aryChemicalSensitivity = NSMutableArray() , aryServiceHistory = NSMutableArray()
    var aryTblData = NSMutableArray()

    var strTitle = ""
    var dataGeneralInfo = NSManagedObject()
    var loader = UIAlertController()
    // MARK: -
    // MARK: -IBOutlet
   
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!

    // MARK: -
    // MARK: -LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        tvForList.tableFooterView = UIView()
      
        lblTitle.text = strTitle
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        whichApiCall()
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 14)
            searchBar.delegate = self
        } else {
            // Fallback on earlier versions
        }
        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
        }
        searchBar.layer.borderWidth = 0
        searchBar.layer.opacity = 1.0
    }
    
    // MARK: -
    // MARK: -IBAction
    @IBAction func actionOnBack(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionOnClickVoiceRecognizationButton(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.aryTblData.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }
}
// MARK: -
// MARK: -APICalling

extension SalesNew_EmailHistoryVC {
    func whichApiCall()  {
        switch strTitle {
        case SalesNewListName.EmailHistory:
            CallEmailHistoryAPI()
        case SalesNewListName.ChemicalSensitivity:
            CallChemicalSensitivityAPI()
        case SalesNewListName.ServiceHistory:
            CallServiceHistoryAPI()
        default:
            break
        }
    }
    
    func CallEmailHistoryAPI() {
        if (isInternetAvailable()){
            
            let strLeadNumber = "\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")"
            let strURL = "\(dict_Login_Data.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "//api/LeadNowAppToSalesProcess/GetEmailHistoryForMobile?CompanyKey=" + "\(Global().getCompanyKey()!)" + "&Reftype=Lead" + "&RefNo=" + strLeadNumber
            
            let strUrlwithString = strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strUrlwithString!, responseStringComing: "") { [self] (response, status) in
                print(response)
                loader.dismiss(animated: false) {
                    if(response.value(forKey: "data") is NSArray){
                        self.aryEmailHistory = NSMutableArray()
                        self.aryEmailHistory = (response.value(forKey: "data") as! NSArray).mutableCopy()as! NSMutableArray
                    }
                    tblViewReload()
                }
                
            }
            
        }else{
            tblViewReload()

        }
    }
    func CallChemicalSensitivityAPI() {
        if (isInternetAvailable()){
            let strURL = "\(dict_Login_Data.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetChemicalSensitiveContactListByAddress
            let strAddress = Global().strCombinedAddressService(for: dataGeneralInfo) ?? ""
            let dictOfEnteredAddress = formatEnteredAddress(value: strAddress)
            let servicesAddress1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1") ?? "")"
            let serviceAddress2 = "\(dictOfEnteredAddress.value(forKey: "AddresLine2") ?? "")"
            let serviceCity = "\(dictOfEnteredAddress.value(forKey: "CityName") ?? "")"
            let serviceCountry = "1"
            let serviceZipcode = "\(dictOfEnteredAddress.value(forKey: "ZipCode") ?? "")"
            var strStateId = ""
            let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
        if dictStateDataTemp.count > 0 {
            strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
        }else{
            strStateId = ""
        }
            let strLat = "";
            let strLong = "";
            
            let strFinalURL = strURL + "companykey=\(Global().getCompanyKey()!)&address1=\(servicesAddress1)&address2=\(serviceAddress2)&cityName=\(serviceCity)&countryId=\(serviceCountry)&stateId=\(strStateId)&zipCode=\(serviceZipcode)&Latitude=\(strLat)&Longitude=\(strLong)"
            let strUrlwithString = strFinalURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strUrlwithString!, responseStringComing: "") { [self] (response, status) in
                print(response)
                loader.dismiss(animated: false) {
                    if(response.value(forKey: "data") is NSArray){
                        self.aryChemicalSensitivity = NSMutableArray()
                        self.aryChemicalSensitivity = (response.value(forKey: "data") as! NSArray).mutableCopy()as! NSMutableArray
                    }
                    tblViewReload()
                }
                
            }
            
        }else{
            tblViewReload()

        }
    }
    func CallServiceHistoryAPI() {
        
    }
    
    
    func tblViewReload() {
      
        switch strTitle {
        case SalesNewListName.EmailHistory:
            self.aryTblData = NSMutableArray()
            self.aryTblData = aryEmailHistory
        case SalesNewListName.ChemicalSensitivity:
            self.aryTblData = NSMutableArray()
            self.aryTblData = aryChemicalSensitivity
        case SalesNewListName.ServiceHistory:
            self.aryTblData = NSMutableArray()
            self.aryTblData = aryServiceHistory
        default:
            break
        }
        tvForList.dataSource = self
        tvForList.delegate = self
        tvForList.reloadData()
    }
    
}

// MARK: -
// MARK: -UITableViewDelegate

extension SalesNew_EmailHistoryVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTblData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        switch strTitle {
        case SalesNewListName.EmailHistory:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesEmailHistoryCell" : "SalesEmailHistoryCell", for: indexPath as IndexPath) as! SalesEmailHistoryCell
            
            let dict = removeNullFromDict(dict: (aryTblData.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            let EmailSubject = dict.value(forKey: "EmailSubject") ?? ""
            let CreatedDate = dict.value(forKey: "CreatedDate") ?? ""
            let EmailTo = dict.value(forKey: "EmailTo") ?? ""
            let EmailFrom = dict.value(forKey: "EmailFrom") ?? ""
            let CreatedByName = dict.value(forKey: "CreatedByName") ?? ""
            let CreatedByUserImage = dict.value(forKey: "CreatedByUserImage") ?? ""
            let imgUrlNew = replaceBackSlasheFromUrl(strUrl: CreatedByUserImage as! String)

            cell.emailHis_lblName.text = "\(CreatedByName)"
            cell.emailHis_lblFrom.text = "From: \(EmailFrom)"
            cell.emailHis_lblTo.text = "To: \(EmailTo)"
            cell.emailHis_lblSubject.text = "Subject: \(EmailSubject)"
            cell.emailHis_imgView.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
            cell.emailHis_imgView.layer.cornerRadius = DeviceType.IS_IPAD ? 32.0 : 25.0
            cell.emailHis_imgView.layer.borderWidth = 1.0
            cell.emailHis_imgView.layer.borderColor = UIColor.lightGray.cgColor
            if "\(CreatedDate)".count > 0 {
                cell.emailHis_lblDate.text = "\(Global().convertDate("\(CreatedDate)")!) \(Global().convertTime("\(CreatedDate)")!)"
            }else{
                cell.emailHis_lblDate.text  = ""
            }
            
            
            let aryMedia = (dict.value(forKey: "LeadMedias") as! NSArray).mutableCopy()as! NSMutableArray
            
            for v in cell.scroll.subviews{
                if v is UILabel {
                
                    v.removeFromSuperview()
                }
                
            }
            
            
            if aryMedia.count != 0 {
                var xPoint = 0
                for (index, element) in aryMedia.enumerated() {
                    let dict = element as AnyObject
                    let dict1 = removeNullFromDict(dict: (dict as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                    print("\(index): \(element)")
                    let button = UILabel()
                    button.font = cell.emailHis_lblSubject.font
                    button.textColor = cell.emailHis_lblViewDetail.textColor
                    let size = "\(dict1.value(forKey: "Title") ?? ""), ".size(withAttributes:[.font: UIFont.systemFont(ofSize:cell.emailHis_lblSubject.font.pointSize)])
                    button.numberOfLines = 2
                    button.frame = CGRect(x: xPoint, y: 0, width: Int(size.width) , height: Int(cell.scroll.frame.height))
                    if(index == aryMedia.count - 1){
                        button.text = "\(dict1.value(forKey: "Title") ?? "")"
                    }else{
                        button.text = "\(dict1.value(forKey: "Title") ?? ""), "
                    }
                   
                   
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
                    button.tag = index*1000+indexPath.row
                    
                    button.isUserInteractionEnabled = true
                    button.addGestureRecognizer(tap)
                    cell.scroll.addSubview(button)
                   
                    cell.scroll.isUserInteractionEnabled = true
                    xPoint = xPoint + Int(button.frame.width)
                 //   button.addTarget(self, action: #selector(goToTopMenu(sender:)), for: .touchUpInside)
                    cell.heightscroll_AGREEMENT.constant = DeviceType.IS_IPAD ? 20.0 : 20.0
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                        cell.scroll.contentSize = CGSize(width: xPoint + 20, height: Int(cell.scroll.frame.height))
                    }
                    
                }
                cell.emailHis_lblAGREEMENT.text = "Attachments:"
            }else{
                cell.emailHis_lblAGREEMENT.text = ""
                cell.heightscroll_AGREEMENT.constant = 0.0
            }
            
            
//            let text = "View Details, View Message"
//            cell.emailHis_lblViewDetail.text = text
//            cell.emailHis_lblViewDetail.tag = indexPath.row
//            cell.emailHis_lblViewDetail.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
//            cell.emailHis_lblViewDetail.isUserInteractionEnabled = true
            
            cell.emailHis_lblViewDetail.text = "View Details, "
            cell.emailHis_lblViewDetail.isUserInteractionEnabled = true
            cell.emailHis_lblViewDetail.lineBreakMode = .byWordWrapping
//            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabeldetail(_:)))
//            tapGesture.numberOfTouchesRequired = 1
//            tapGesture.view?.tag = indexPath.row
//            cell.emailHis_lblViewDetail.addGestureRecognizer(tapGesture)
            
            cell.btnViewDetail.tag = indexPath.row
            cell.btnViewDetail.addTarget(self, action: #selector(tappedOnLabeldetail), for: .touchUpInside)

            
            
            
            cell.emailHis_lblViewMessage.text = "View Message"
            cell.emailHis_lblViewMessage.isUserInteractionEnabled = true
            cell.emailHis_lblViewMessage.lineBreakMode = .byWordWrapping
//            let tapGesture1 = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabelMessage(_:)))
//            tapGesture1.numberOfTouchesRequired = 1
//            tapGesture1.view?.tag = indexPath.row
//            cell.emailHis_lblViewMessage.addGestureRecognizer(tapGesture1)
            
            cell.btnViewMessage.tag = indexPath.row
            cell.btnViewMessage.addTarget(self, action: #selector(tappedOnLabelMessage), for: .touchUpInside)
            
            
            
            
            
//
//            cell.emailHis_btnViewDetail.addTarget(self, action: #selector(ViewDetail), for: .touchUpInside)
//            cell.emailHis_btnViewMessage.addTarget(self, action: #selector(ViewMessage), for: .touchUpInside)
//
//            //  cell.emailHis_btnViewAgreeMent.addTarget(self, action: #selector(ViewAgreeMent), for: .touchUpInside)
//
//            cell.emailHis_btnViewDetail.tag = indexPath.row
//            cell.emailHis_btnViewMessage.tag = indexPath.row
          
          //  cell.emailHis_btnViewAgreeMent.tag = indexPath.row
            
//            cell.emailHis_btnViewDetail.layer.cornerRadius = 6.0
//            cell.emailHis_btnViewDetail.layer.borderWidth = 1.0
//            cell.emailHis_btnViewDetail.layer.borderColor = UIColor.lightGray.cgColor
//
//            cell.emailHis_btnViewMessage.layer.cornerRadius = 6.0
//            cell.emailHis_btnViewMessage.layer.borderWidth = 1.0
//            cell.emailHis_btnViewMessage.layer.borderColor = UIColor.lightGray.cgColor
            
//            cell.emailHis_btnViewAgreeMent.layer.cornerRadius = 6.0
//            cell.emailHis_btnViewAgreeMent.layer.borderWidth = 1.0
//            cell.emailHis_btnViewAgreeMent.layer.borderColor = UIColor.lightGray.cgColor
//
            
            return cell
            
        case SalesNewListName.ChemicalSensitivity:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesChemicalSensitivityCell" : "SalesChemicalSensitivityCell", for: indexPath as IndexPath) as! SalesChemicalSensitivityCell
            let dict = removeNullFromDict(dict: (aryTblData.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            var strName = "\(dict.value(forKey:"FirstName") ?? "")"
            
            if "\(dict.value(forKey: "MiddleName") ?? "")".count > 0
            {
                strName = strName + " " + "\(dict.value(forKey: "MiddleName") ?? "")"
            }
            if "\(dict.value(forKey: "LastName") ?? "")".count > 0
            {
                strName = strName + " " + "\(dict.value(forKey: "LastName") ?? "")"
            }
            
            let FirstName = strName//dict.value(forKey: "FirstName") ?? ""
            let FullAddress = dict.value(forKey: "FullAddress") ?? ""
            let CellPhone1 = dict.value(forKey: "CellPhone1") ?? ""
            let PrimaryPhone = dict.value(forKey: "PrimaryPhone") ?? ""
            let SecondaryPhone = dict.value(forKey: "SecondaryPhone") ?? ""
            let PrimaryEmail = dict.value(forKey: "PrimaryEmail") ?? ""
            let SecondaryEmail = dict.value(forKey: "SecondaryEmail") ?? ""
            
            cell.lblName.text = "\(FirstName)"
            "\(FirstName)" == "" ? (cell.stk_NameStatic.isHidden = true) : (cell.stk_NameStatic.isHidden = false)
            
            cell.lblAddress.text = "\(FullAddress)"
            cell.lblAddress.isUserInteractionEnabled = true
            cell.lblAddress.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnAddress(sender:))))
            "\(FullAddress)" == "" ? (cell.stk_AddressStatic.isHidden = true) : (cell.stk_AddressStatic.isHidden = false)
            
            cell.lblPrimaryEmail.text = "\(PrimaryEmail)"
            cell.lblPrimaryEmail.isUserInteractionEnabled = true
            cell.lblPrimaryEmail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnPrimaryEmail(sender:))))
            "\(PrimaryEmail)" == "" ? (cell.stk_PrimaryEmailStatic.isHidden = true) : (cell.stk_PrimaryEmailStatic.isHidden = false)
            
            cell.lblSecondaryEmail.text = "\(SecondaryEmail)"
            cell.lblSecondaryEmail.isUserInteractionEnabled = true
            cell.lblSecondaryEmail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnSecondaryEmail(sender:))))
            "\(SecondaryEmail)" == "" ? (cell.stk_SecondaryEmailStatic.isHidden = true) : (cell.stk_SecondaryEmailStatic.isHidden = false)
            
            cell.lblPrimaryPhone.text = formattedNumber(number: "\(PrimaryPhone)")
            cell.lblPrimaryPhone.isUserInteractionEnabled = true
            cell.lblPrimaryPhone.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnPrimaryCall(sender:))))
            "\(PrimaryPhone)" == "" ? (cell.stk_PrimaryPhoneStatic.isHidden = true) : (cell.stk_PrimaryPhoneStatic.isHidden = false)
            
            cell.lblSecondaryPhone.text = formattedNumber(number: "\(SecondaryPhone)")
            cell.lblSecondaryPhone.isUserInteractionEnabled = true
            cell.lblSecondaryPhone.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnSecondaryCall(sender:))))
            "\(SecondaryPhone)" == "" ? (cell.stk_SecondaryPhoneStatic.isHidden = true) : (cell.stk_SecondaryPhoneStatic.isHidden = false)
            
            
            cell.lblCell.text = formattedNumber(number: "\(CellPhone1)")
            cell.lblCell.isUserInteractionEnabled = true
            cell.lblCell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionViewBtnCellCall(sender:))))
            "\(CellPhone1)" == "" ? (cell.stk_CellStatic.isHidden = true) : (cell.stk_CellStatic.isHidden = false)
            return cell
            
        case SalesNewListName.ServiceHistory:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesServiceHistoryCell" : "SalesServiceHistoryCell", for: indexPath as IndexPath) as! SalesServiceHistoryCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesServiceHistoryCell" : "SalesServiceHistoryCell", for: indexPath as IndexPath) as! SalesServiceHistoryCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
  
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 5, y: 5, width: tableView.frame.width , height: 120))
        returnedView.backgroundColor = .white
        let label = UILabel(frame: CGRect(x: 5, y: 5, width: tableView.frame.width - 10, height: 110))
        label.textAlignment = .center
        label.textColor = UIColor.lightGray
        label.text = !isInternetAvailable() ? alertInternet : ((aryTblData.count == 0) ? alertDataNotFound : "")
        returnedView.addSubview(label)
        return returnedView
    }
   
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryTblData.count == 0 ? 120 : 0
    }
    //MARK:
    //MARK: ChemicalSensitivity Action
    
    @objc func actionViewBtnPrimaryEmail(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        guard let getTag = sender.view?.tag else { return }
          print("getTag == \(getTag)")
        let dict = removeNullFromDict(dict: (aryTblData.object(at: Int(getTag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let PrimaryEmail = dict.value(forKey: "PrimaryEmail") ?? ""
        if("\(PrimaryEmail)".trimmed.count != 0 ){
            guard MFMessageComposeViewController.canSendText() else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMailComposer, viewcontrol: self)

                      return
                  }
                  let messageVC = MFMessageComposeViewController()
                  messageVC.body = "";
                  messageVC.recipients = ["\(PrimaryEmail)"]
                  messageVC.messageComposeDelegate = self;
                  self.present(messageVC, animated: false, completion: nil)
        }
     
    }
    @objc func actionViewBtnSecondaryEmail(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        guard let getTag = sender.view?.tag else { return }
          print("getTag == \(getTag)")
        let dict = removeNullFromDict(dict: (aryTblData.object(at: Int(getTag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let SecondaryEmail = dict.value(forKey: "SecondaryEmail") ?? ""
        if("\(SecondaryEmail)".trimmed.count != 0 ){
            guard MFMessageComposeViewController.canSendText() else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMailComposer, viewcontrol: self)

                      return
                  }
                  let messageVC = MFMessageComposeViewController()
                  messageVC.body = "";
                  messageVC.recipients = ["\(SecondaryEmail)"]
                  messageVC.messageComposeDelegate = self;
                  self.present(messageVC, animated: false, completion: nil)
        }
     
    }
    @objc func actionViewBtnPrimaryCall(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        guard let getTag = sender.view?.tag else { return }
          print("getTag == \(getTag)")
        let dict = removeNullFromDict(dict: (aryTblData.object(at: Int(getTag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let PrimaryPhone = dict.value(forKey: "PrimaryPhone") ?? ""
        if("\(PrimaryPhone)".trimmed.count != 0 ){
            if !(callingFunction(number: "\(PrimaryPhone)".trimmed as NSString)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
    }
}
    
    @objc func actionViewBtnSecondaryCall(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        guard let getTag = sender.view?.tag else { return }
          print("getTag == \(getTag)")
        let dict = removeNullFromDict(dict: (aryTblData.object(at: Int(getTag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let SecondaryPhone = dict.value(forKey: "SecondaryPhone") ?? ""
        if("\(SecondaryPhone)".trimmed.count != 0 ){
            if !(callingFunction(number: "\(SecondaryPhone)".trimmed as NSString)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
    }
}
    @objc func actionViewBtnCellCall(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        guard let getTag = sender.view?.tag else { return }
          print("getTag == \(getTag)")
        let dict = removeNullFromDict(dict: (aryTblData.object(at: Int(getTag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let CellPhone1 = dict.value(forKey: "CellPhone1") ?? ""
        if("\(CellPhone1)".trimmed.count != 0 ){
            if !(callingFunction(number: "\(CellPhone1)".trimmed as NSString)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
    }
}
    @objc func actionViewBtnAddress(sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
        guard let getTag = sender.view?.tag else { return }
          print("getTag == \(getTag)")
        let dict = removeNullFromDict(dict: (aryTblData.object(at: Int(getTag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let FullAddress = dict.value(forKey: "FullAddress") ?? ""
        if("\(FullAddress)".trimmed.count != 0 ){
                let alertController = UIAlertController(title: "Alert", message: "\(FullAddress)", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    Global().redirect(onAppleMap: self, "\(FullAddress)")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                {
                    UIAlertAction in
                }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
        }
        
    }
    //MARK:
    //MARK: HistoryEmail Action
   
    @objc
       func tapFunction(sender:UITapGestureRecognizer) {
           print("tap working")
           let row = sender.view!.tag/1000
           let section = sender.view!.tag%1000
      
           let dict1 = aryTblData.object(at: section)as! NSDictionary
           let aryMedia = (dict1.value(forKey: "LeadMedias") as! NSArray).mutableCopy()as! NSMutableArray
           let dict = aryMedia.object(at: row)as! NSDictionary


           
           
           if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Image") == .orderedSame
           {
              
               if let imgURL = dict.value(forKey: "Path")
               {
                   if("\(imgURL)" == "<null>" || "\(imgURL)".count == 0 || "\(imgURL)" == " ")
                   {
                      showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Image not found.", viewcontrol: self)
                   }
                   else
                   {
                      // cell.imgView.isHidden = false
                       var imgUrlNew = imgURL as! String
                       imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\", with: "/")
                       imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\", with: "/")
                       imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\", with: "/")
                       imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\", with: "/")
                       imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\", with: "/")
                       imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                       imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                       imgUrlNew = imgUrlNew.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                       let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
                       let vc = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as! PreviewImageVC
                      // vc.img = image!
                       vc.strimgUrl = imgUrlNew
                       self.present(vc, animated: false, completion: nil)
               
                   }
               }
               //
               
           }
           else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Document") == .orderedSame
           {
             
               let  strPdfNameNew = "\(dict.value(forKey: "Path") ?? "")".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                     guard let url = URL(string: "\(strPdfNameNew)") else { return }
                     UIApplication.shared.open(url)
//                let safariVC = SFSafariViewController(url: NSURL(string: encodedURL!)! as URL)
//                self.present(safariVC, animated: true, completion: nil)
//                  safariVC.delegate = self
               
           }
           else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Audio") == .orderedSame
           {
               
               let audioURL = "\("\( dict.value(forKey: "Path") ?? "")")"
               let player = AVPlayer(url: URL(string: audioURL)!)
               let playerViewController = AVPlayerViewController()
               playerViewController.player = player
               self.present(playerViewController, animated: true) {
                   playerViewController.player!.play()
               }
           }
           else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Video") == .orderedSame
           {
               let audioURL = "\("\( dict.value(forKey: "Path") ?? "")")"
               let player = AVPlayer(url: URL(string: audioURL)!)
               let playerViewController = AVPlayerViewController()
               playerViewController.player = player
               self.present(playerViewController, animated: true) {
                   playerViewController.player!.play()
               }
           }
           
           
           
       }
    
    
//    @objc func ViewAgreeMent(sender : UIButton) {
//        self.view.endEditing(true)
//              let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailListDetail_iPhoneVC") as? EmailListDetail_iPhoneVC
//              vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//              vc!.modalTransitionStyle = .coverVertical
//              let dict = aryTblData.object(at: sender.tag)as! NSDictionary
//             vc!.aryAttechment = (dict.value(forKey: "LeadMedias") as! NSArray).mutableCopy()as! NSMutableArray
//              vc!.strComeFrom = "HistoryMessageAgreement"
//              self.present(vc!, animated: false, completion: nil)
//    }

    
//    @objc func tappedOnLabeldetail(_ gesture: UITapGestureRecognizer) {
//        self.view.endEditing(true)
//        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailListDetail_iPhoneVC") as? EmailListDetail_iPhoneVC
//        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        vc!.modalTransitionStyle = .coverVertical
//        let dict = aryTblData.object(at: gesture.view!.tag)as! NSDictionary
//        vc!.strComeFrom = "HistoryDetail"
//        vc!.EmailHistoryId = "\(dict.value(forKey: "EmailHistoryId")!)"
//        self.present(vc!, animated: false, completion: nil)
//    }
    
    @objc func tappedOnLabeldetail(sender : UIButton) {
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailListDetail_iPhoneVC") as? EmailListDetail_iPhoneVC
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc!.modalTransitionStyle = .coverVertical
        let dict = aryTblData.object(at:sender.tag)as! NSDictionary
        vc!.strComeFrom = "HistoryDetail"
        vc!.EmailHistoryId = "\(dict.value(forKey: "EmailHistoryId")!)"
        self.present(vc!, animated: false, completion: nil)
    }
    
    
    
    
    
//    @objc func tappedOnLabelMessage(_ gesture: UITapGestureRecognizer) {
//        self.view.endEditing(true)
//        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailListDetail_iPhoneVC") as? EmailListDetail_iPhoneVC
//        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        vc!.modalTransitionStyle = .coverVertical
//        let dict = aryTblData.object(at: gesture.view!.tag)as! NSDictionary
//        vc!.strComeFrom = "HistoryMessage"
//        vc!.EmailHistoryId = "\(dict.value(forKey: "EmailHistoryId")!)"
//        self.present(vc!, animated: false, completion: nil)
//    }
    @objc func tappedOnLabelMessage(sender : UIButton) {
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailListDetail_iPhoneVC") as? EmailListDetail_iPhoneVC
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc!.modalTransitionStyle = .coverVertical
        let dict = aryTblData.object(at: sender.tag)as! NSDictionary
        vc!.strComeFrom = "HistoryMessage"
        vc!.EmailHistoryId = "\(dict.value(forKey: "EmailHistoryId")!)"
        self.present(vc!, animated: false, completion: nil)
    }
    
    
    
}

extension UITapGestureRecognizer {
   
   func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
       guard let attributedText = label.attributedText else { return false }

       let mutableStr = NSMutableAttributedString.init(attributedString: attributedText)
       mutableStr.addAttributes([NSAttributedString.Key.font : label.font!], range: NSRange.init(location: 0, length: attributedText.length))
       
       // If the label have text alignment. Delete this code if label have a default (left) aligment. Possible to add the attribute in previous adding.
       let paragraphStyle = NSMutableParagraphStyle()
       paragraphStyle.alignment = .center
       mutableStr.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: attributedText.length))

       // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
       let layoutManager = NSLayoutManager()
       let textContainer = NSTextContainer(size: CGSize.zero)
       let textStorage = NSTextStorage(attributedString: mutableStr)
       
       // Configure layoutManager and textStorage
       layoutManager.addTextContainer(textContainer)
       textStorage.addLayoutManager(layoutManager)
       
       // Configure textContainer
       textContainer.lineFragmentPadding = 0.0
       textContainer.lineBreakMode = label.lineBreakMode
       textContainer.maximumNumberOfLines = label.numberOfLines
       let labelSize = label.bounds.size
       textContainer.size = labelSize
       
       // Find the tapped character location and compare it to the specified range
       let locationOfTouchInLabel = self.location(in: label)
       let textBoundingBox = layoutManager.usedRect(for: textContainer)
       let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                         y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
       let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                    y: locationOfTouchInLabel.y - textContainerOffset.y);
       let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
       return NSLocationInRange(indexOfCharacter, targetRange)
   }
   
}
// MARK: ----
// MARK: -UISearchBar delegate
extension SalesNew_EmailHistoryVC : UISearchBarDelegate{

func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    searchAutocomplete(searchText: searchText)
}

func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    self.view.endEditing(true)
    if(searchBar.text?.count == 0)
    {
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter text to be searched.", viewcontrol: self)
    }
}

func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.text = ""
    self.view.endEditing(true)
    ClearSearchData()
}
    func searchAutocomplete(searchText: String) -> Void{
        if searchText.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.resignFirstResponder()
            }
            ClearSearchData()
        }
        else{
            var aryTemp = NSArray()
            switch strTitle {
            case SalesNewListName.EmailHistory:
                aryTemp = aryEmailHistory.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "CreatedByName")!)".lowercased().contains(searchText.lowercased()) || "\((task as! NSDictionary).value(forKey: "EmailFrom")!)".lowercased().contains(searchText.lowercased()) ||  "\((task as! NSDictionary).value(forKey: "EmailTo")!)".lowercased().contains(searchText.lowercased()) } as NSArray
            case SalesNewListName.ChemicalSensitivity:
                aryTemp = aryChemicalSensitivity.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "FirstName")!)".lowercased().contains(searchText.lowercased()) } as NSArray
            case SalesNewListName.ServiceHistory:
                aryTemp = aryServiceHistory.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "")!)".lowercased().contains(searchText.lowercased()) } as NSArray
            default:
                break
            }
 
            self.aryTblData = NSMutableArray()
            self.aryTblData = (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            self.tvForList.dataSource = self
            self.tvForList.delegate = self
            self.tvForList.reloadData()
        }
    }
    func ClearSearchData() {
        switch strTitle {
        case SalesNewListName.EmailHistory:
            self.aryTblData = NSMutableArray()
            self.aryTblData =  self.aryEmailHistory
        case SalesNewListName.ChemicalSensitivity:
            self.aryTblData = NSMutableArray()
            self.aryTblData =  self.aryChemicalSensitivity
        case SalesNewListName.ChemicalSensitivity:
            self.aryTblData = NSMutableArray()
            self.aryTblData =  self.aryServiceHistory
        default:
          break
        }
        self.tvForList.dataSource = self
        self.tvForList.delegate = self
        self.tvForList.reloadData()
    }
}

// MARK: -
// MARK: ----------SalesEmailHistoryCell---------
class SalesEmailHistoryCell: UITableViewCell {
    
    @IBOutlet weak var emailHis_lblName: UILabel!
    @IBOutlet weak var emailHis_lblDate: UILabel!
    @IBOutlet weak var emailHis_lblSubject: UILabel!
    @IBOutlet weak var emailHis_lblFrom: UILabel!
    @IBOutlet weak var emailHis_imgView: UIImageView!
    @IBOutlet weak var emailHis_lblTo: UILabel!
  
    @IBOutlet weak var scroll: UIScrollView!
  
    @IBOutlet weak var heightscroll_AGREEMENT: NSLayoutConstraint!
    @IBOutlet weak var emailHis_lblAGREEMENT: UILabel!
    @IBOutlet weak var emailHis_lblViewDetail: UILabel!
    @IBOutlet weak var emailHis_lblViewMessage: UILabel!
    
    @IBOutlet weak var btnViewDetail: UIButton!
    @IBOutlet weak var btnViewMessage: UIButton!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
}
// MARK: -
// MARK: ----------SalesChemicalSensitivityCell---------
class SalesChemicalSensitivityCell: UITableViewCell {
  
    @IBOutlet weak var stk_NameStatic: UIStackView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var stk_AddressStatic: UIStackView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var stk_CellStatic: UIStackView!
    @IBOutlet weak var lblCell: UILabel!
    @IBOutlet weak var stk_PrimaryPhoneStatic: UIStackView!
    @IBOutlet weak var lblPrimaryPhone: UILabel!
    @IBOutlet weak var stk_SecondaryPhoneStatic: UIStackView!
    @IBOutlet weak var lblSecondaryPhone: UILabel!
    @IBOutlet weak var stk_PrimaryEmailStatic: UIStackView!
    @IBOutlet weak var lblPrimaryEmail: UILabel!
    @IBOutlet weak var stk_SecondaryEmailStatic: UIStackView!
    @IBOutlet weak var lblSecondaryEmail: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: ----------SalesServiceHistoryCell---------
class SalesServiceHistoryCell: UITableViewCell {
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: ----
// MARK: -SalesNew_SelectionProtocol
extension SalesNew_EmailHistoryVC : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if #available(iOS 13.0, *) {
            self.searchBar.searchTextField.text = str
        } else {
            self.searchBar.text = str
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.searchAutocomplete(searchText: str)
        }
    }
}
// MARK: -
// MARK: -MFMessageComposeViewControllerDelegate

extension SalesNew_EmailHistoryVC: MFMessageComposeViewControllerDelegate{
func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
       switch (result.rawValue) {
           case MessageComposeResult.cancelled.rawValue:
           print("Message was cancelled")
           self.dismiss(animated: true, completion: nil)
       case MessageComposeResult.failed.rawValue:
           print("Message failed")
           self.dismiss(animated: true, completion: nil)
       case MessageComposeResult.sent.rawValue:
           print("Message was sent")
           self.dismiss(animated: true, completion: nil)
       default:
           break;
       }
   }
}
