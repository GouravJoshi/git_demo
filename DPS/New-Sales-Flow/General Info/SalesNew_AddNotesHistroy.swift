//
//  SalesNew_AddNotesHistroy.swift
//  DPS
//
//  Created by Ruchika Verma on 15/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_AddNotesHistroy: UIViewController {
    
    // MARK: - ----------------------------------- Outlets -----------------------------------
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var btnAddNotes: UIButton!
    @IBOutlet weak var btnOpenExpirayDate: UIButton!
    @IBOutlet weak var btnOpenNoteCodeDate: UIButton!
    @IBOutlet weak var txtExpiraryDate: ACFloatingTextField!
    @IBOutlet weak var txtNoteCode: ACFloatingTextField!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var txtViewNotes: UITextView!
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    var dictLoginData = NSDictionary()
    var loader = UIAlertController()
    var arrofNotesCodeMaster = [String]()
    var strExpiryDate = ""
    var strSelectedNoteCode = ""
    
    // MARK: -  --- String ---
    
    var strLeadType = String()
    var strRefType = String()
    var strRefId = String()
    
    @objc var tempstrRefType = NSString ()
    @objc var tempstrRefId = NSString ()
    
    
    // MARK: -  --- NSManagedObject ---
    
    var dictLeadData = NSManagedObject()
    
    var dictActivityData = NSDictionary()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if tempstrRefId != "" && tempstrRefType != ""{
            self.strRefType = self.tempstrRefType as String
            self.strRefId = self.tempstrRefId as String
        }
        txtViewNotes.text = ""
        
        setBorderColor(item: txtViewNotes)
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
        print("\(dictLeadData)")
        // Do any additional setup after loading the view.
        
        if strLeadType == "Activity"
        {
            lblHeader.text = "Comments"
            btnAddNotes.setTitle("Save", for: .normal   )
            txtViewNotes.placeholder = "Enter comments..."
        }
        else
        {
            lblHeader.text = "Notes"
            btnAddNotes.setTitle("Add", for: .normal   )
            txtViewNotes.placeholder = "Enter notes..."
            txtViewNotes.layer.borderColor = UIColor.white.cgColor
            txtViewNotes.layer.borderWidth = 1
        }
        fetchNotesCodeFromMaster()
        
    }
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        if tempstrRefType != "" && tempstrRefId != ""{
            self.dismiss(animated: false, completion: nil)
        }
        else{
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    
    @IBAction func btnSelectExpiryDate(_ sender: UIButton){
        self.view.endEditing(true)
        //        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        //        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        //        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //        vc.modalTransitionStyle = .coverVertical
        //        vc.strType = "Date"
        //        vc.handleDateSelectionForDatePickerProtocol = self
        //        vc.strTag = 1
        //        vc.chkForMinDate = true
        //        self.present(vc, animated: true, completion: {})
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((txtExpiraryDate.text?.count)! > 0 ? txtExpiraryDate.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.date = formatter.date(from: result)!//dateFormatter.string(from: fromDate)
        datePicker.datePickerMode = UIDatePicker.Mode.date
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheetWithClearButton(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
            
            txtExpiraryDate.text = formatter.string(from: datePicker.date)
            strExpiryDate = txtExpiraryDate.text ?? ""
        }))
        alertController.addAction(UIAlertAction(title: "Clear", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
            
            txtExpiraryDate.text = ""
            strExpiryDate = txtExpiraryDate.text ?? ""
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSelectNoteCode(_ sender: UIButton){
        self.view.endEditing(true)
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "-----Select-----"
        vc.strTag = 77777
        if arrofNotesCodeMaster.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelPaymentTable = self
            vc.aryOfNoteCodes = arrofNotesCodeMaster
            self.present(vc, animated: false, completion: {})
        }
        else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    
    @IBAction func actionOnAddNotes(_ sender: Any)
    {
        
        self.view.endEditing(true)
        
        if strLeadType == "Activity"
        {
            if((txtViewNotes.text.trimmingCharacters(in: NSCharacterSet.whitespaces)).count == 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter comment", viewcontrol: self)
            }
            else
            {
                addComment()
            }
            
        }
        else
        {
            if(Validation() != ""){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Validation(), viewcontrol: self)
            }else{
                
                if strRefType.lowercased() == "lead".lowercased(){
                    AddLeadNote()
                }
                else if strRefType.lowercased() == "WorkOrder".lowercased(){
                    AddLeadNote()
                }
                else{
                    addNotes()
                }
            }
        }
    }
    
    
    // MARK: -  ------------------------------ Custom function  ------------------------------
    
    func Validation() -> String {
        let noteDescription = txtViewNotes.text.trimmingCharacters(in: .whitespaces)
//        if txtNoteCode.text?.count == 0 {
//            return "Please select note code"
//        }
        if noteDescription.count == 0 {
            return "Please enter note"
        }
        return ""
    }
    
    func fetchNotesCodeFromMaster() {
        arrofNotesCodeMaster.removeAll()
        let nsUd = UserDefaults.standard
        let dictOfMaster = nsUd.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictOfMaster.value(forKey: "NoteCodeMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        for k in 0 ..< arrOfData.count {
            if arrOfData[k] is NSDictionary {
                let dictOfData = arrOfData[k] as! NSDictionary
                if "\(dictOfData.value(forKey: "IsActive")!)" == "1"{
                    arrofNotesCodeMaster.append("\(dictOfData.value(forKey: "Title")!)")
                }
            }
        }
    }
    
    func fetchNoteCodeMasterId() -> String{
        var strId = ""
        let nsUd = UserDefaults.standard
        let dictOfMaster = nsUd.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictOfMaster.value(forKey: "NoteCodeMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        for k in 0 ..< arrOfData.count {
            if arrOfData[k] is NSDictionary {
                let dictOfData = arrOfData[k] as! NSDictionary
                if "\(dictOfData.value(forKey: "IsActive")!)" == "1"{
                    if "\(dictOfData.value(forKey: "Title")!)" == strSelectedNoteCode{
                        strId = "\(dictOfData.value(forKey: "NoteCodeMasterId")!)"
                        break
                    }
                }
            }
        }
        
        return strId
    }
    // MARK: -  ------------------------------ API Calling  ------------------------------
    
    /*  later dded by prateek sir in sales (ios chnges by ruchika)
     https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/AddUpdateNoteRefTypeRefNo
     {
     "Note":"Test note added ruchika 5:04",
     "EmployeeId":"143035",
     "CompanyKey":"Production",
     "ExpiryDate":"",
     "RefType":"lead",
     "LeadNoteId":"",
     "RefId":"",
     "RefNo":"23420",
     "NoteCodeMasterId":""
     }*/
    func AddLeadNote()
    {
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            var strUrl =  "\(strServiceUrlMain)\(UrlAddNotesSalesNew)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            let strNoteCodeMasterId = fetchNoteCodeMasterId()
            var key = NSArray()
            var value = NSArray()
            
            let noteDescription = txtViewNotes.text.trimmingCharacters(in: .whitespaces)

            key = ["Note",
                   "EmployeeId",
                   "CompanyKey",
                   "ExpiryDate",
                   "RefType",
                   "LeadNoteId",
                   "RefId",
                   "RefNo",
                   "NoteCodeMasterId"]
            
            value = [ noteDescription,
                      "\(strEmpID)",
                      strCompanyKey,
                      strExpiryDate,
                      strRefType,
                      "",
                      "",
                      strRefId,
                      strNoteCodeMasterId]
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            
                 let str = getJson(from: dict_ToSend)
            print("Json \(str ?? "")" as Any)
            
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeadersResponseString(dictJson: dict_ToSend as NSDictionary, url: strUrl , responseStringComing: "Timeline") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {
                    
                    if(Status){
                        
                        let strResponse = Response["data"] as! String
                        
                        if strResponse.count > 0
                        {
                            
                            UserDefaults.standard.set(true, forKey: "isNotesAdded")
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedNotes_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TimeLine_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            
                            if self.tempstrRefType != ""{
                                self.dismiss(animated: false, completion: nil)
                            }
                            self.navigationController?.popViewController(animated: false)
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                }
                
            }
            
            
        }
        
    }
    
    func addNotes()
    {
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            var strUrl =  "\(strServiceUrlMain)\(UrlAddNotesNew)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            let noteDescription = txtViewNotes.text.trimmingCharacters(in: .whitespaces)

            var key = NSArray()
            var value = NSArray()
            
            key = ["LeadNoteId",
                   "RefId",
                   "RefType",
                   "Note",
                   "NoteCodeMasterId",
                   "ExpiryDate",
                   "EmployeeId",
                   "CompanyKey"
            ]
            
            value = ["",
                     strRefId,
                     strRefType,
                     noteDescription,
                     "",
                     "",
                     "\(strEmpID)",
                     strCompanyKey
            ]
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            
            let str = getJson(from: dict_ToSend)
            print("Json \(str ?? "")" as Any)
            
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeadersResponseString(dictJson: dict_ToSend as NSDictionary, url: strUrl , responseStringComing: "Timeline") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {
                    
                    if(Status){
                        
                        let strResponse = Response["data"] as! String
                        
                        if strResponse.count > 0
                        {
                            
                            UserDefaults.standard.set(true, forKey: "isNotesAdded")
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedNotes_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TimeLine_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            
                            if self.tempstrRefType != ""{
                                self.dismiss(animated: false, completion: nil)
                            }
                            self.navigationController?.popViewController(animated: false)
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                }
                
            }
            
            
        }
        
    }
    
    func addComment()
    {
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            var strUrl =  "\(strServiceUrlMain)\(UrlPostComment)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            var dictLocal = NSMutableDictionary()
            
            key = ["ActivityId",
                   "Comment",
                   "CreatedBy",
                   "CreatedDate",
                   "EmployeeId"
            ]
            
            value = [strRefId,
                     txtViewNotes.text,
                     strEmpID,
                     Global().strCurrentDate(),
                     strEmpID
            ]
            
            
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            
            dictLocal = (dict_ToSend as NSDictionary).mutableCopy() as! NSMutableDictionary
            
            let str = getJson(from: dict_ToSend)
            print("Json \(str ?? "")" as Any)
            
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeadersResponseString(dictJson: dict_ToSend as NSDictionary, url: strUrl , responseStringComing: "Timeline") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {
                    
                    if(Status){
                        
                        let strResponse = Response["data"] as! String
                        
                        if strResponse.count > 0
                        {
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CommentAdded_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            self.navigationController?.popViewController(animated: false)
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                }
            }
        }
    }
    
    // MARK: -  ------------------------------ Core Data Functions  ------------------------------
    
    func saveWebLeadToCoreData (strLeadType: String)
    {
        if strLeadType == "WebLead"
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("createdByName")
            arrOfKeys.add("leadNoteId")
            arrOfKeys.add("leadNumber")
            arrOfKeys.add("note")
            arrOfKeys.add("opportunityNumber")
            arrOfKeys.add("userName")
            
            arrOfValues.add("\(Global().getCurrentDate() ?? "")")
            arrOfValues.add("\(strCompanyKey)")
            arrOfValues.add("\(strUserName)")
            arrOfValues.add("")
            
            arrOfValues.add("\(dictLeadData.value(forKey: "LeadNumber") ?? "")")
            
            arrOfValues.add(txtViewNotes.text ?? "")
            arrOfValues.add("")
            arrOfValues.add("\(strUserName)")
            
            saveDataInDB(strEntity: "TotalNotesCRM", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        else
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("createdByName")
            arrOfKeys.add("leadNoteId")
            arrOfKeys.add("leadNumber")
            arrOfKeys.add("note")
            arrOfKeys.add("opportunityNumber")
            arrOfKeys.add("userName")
            
            arrOfValues.add("\(Global().getCurrentDate() ?? "")")
            arrOfValues.add("\(strCompanyKey)")
            arrOfValues.add("\(strUserName)")
            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add(txtViewNotes.text ?? "")
            arrOfValues.add("\(dictLeadData.value(forKey: "OpportunityNumber") ?? "")")
            arrOfValues.add("\(strUserName)")
            
            saveDataInDB(strEntity: "TotalNotesCRM", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        
        
    }
    
}
extension SalesNew_AddNotesHistroy: UITextViewDelegate
{
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if(text == "\n") {
                textView.resignFirstResponder()
                return false
            }
            else{
                
                let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            
                    if newText.count > 5000{
                        print("jyada hai")
                    }
                    else{
                        lblCount.text =  "\(newText.count)/5000"
                    }
                    return newText.count < 5000
            }
        }
}


extension SalesNew_AddNotesHistroy: DatePickerProtocol{
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int) {
        strExpiryDate = strDate
        txtExpiraryDate.text = strExpiryDate
    }
}

extension SalesNew_AddNotesHistroy: PaymentInfoDetails
{
    func getPaymentInfo(dictData: String, index: Int) {
        
        if(dictData.count == 0){
            print(dictData)
            strSelectedNoteCode = dictData
            txtNoteCode.text = strSelectedNoteCode
            txtExpiraryDate.text = ""
        }else{
            print(dictData)
            strSelectedNoteCode = dictData
            txtNoteCode.text = strSelectedNoteCode
        }
        
    }
}
