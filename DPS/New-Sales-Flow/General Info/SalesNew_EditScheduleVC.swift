//
//  SalesNew_EditScheduleVC.swift
//  DPS
//
//  Created by NavinPatidar on 6/1/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_EditScheduleVC: UIViewController {
    //MARK:
    //MARK:---------Variable-----------
    
    var dataGeneralInfo = NSManagedObject()
    var datePicker:UIDatePicker!
    var aryTimeRange = NSArray() ,arySelectedRange = NSMutableArray()
    //MARK:
    //MARK:---------IBOutlet-----------
    @IBOutlet weak var tf_Date: ACFloatingTextField!
    @IBOutlet weak var tf_Time: ACFloatingTextField!
    @IBOutlet weak var tf_Range: ACFloatingTextField!
    @IBOutlet weak var height_tf_Range: NSLayoutConstraint!
    @IBOutlet weak var btn_Specific: UIButton!
    @IBOutlet weak var btn_TimeRange: UIButton!

    //MARK:
    //MARK:---------LifeCycle-----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(nsud.value(forKey: "MasterSalesTimeRange") != nil){
            if(nsud.value(forKey: "MasterSalesTimeRange") is NSArray){
                self.aryTimeRange = nsud.value(forKey: "MasterSalesTimeRange") as! NSArray
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        setInitialUI()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
    }
    //MARK:
    //MARK:---------IBAction-----------
    
    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func action_OnDate(_ sender: UIButton) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Date.text?.count)! > 0 ? tf_Date.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.date = fromDate
        datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Service Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Date.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
      
    }
    @IBAction func action_OnTime(_ sender: UIButton) {
        self.view.endEditing(true)
   
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Time.text?.count)! > 0 ? tf_Time.text! : result))!
        
        let datePicker = UIDatePicker()
       // datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.time
        datePicker.date = fromDate
      //  datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)

        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate, Picker: datePicker, view: self.view ,strTitle : "Select Service Time")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Time.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func action_OnSelectRange(_ sender: Any) {
        self.view.endEditing(true)
        if(aryTimeRange.count != 0){
            goToNewSalesSelectionVC(arrItem: aryTimeRange.mutableCopy()as! NSMutableArray, arrSelectedItem: self.arySelectedRange, tag: 1, titleHeader: "Time Range", isMultiSelection: false, ShowNameKey: "StartInterval,EndInterval")
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
    }

    @IBAction func action_OnSpecific(_ sender: Any) {
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
        arySelectedRange = NSMutableArray()
        tf_Range.text = ""
    }
    @IBAction func action_OnTimeRange(_ sender: Any) {
        height_tf_Range.constant = DeviceType.IS_IPAD ? 50.0 : 44.0
        btn_Specific.setImage(UIImage(named: "redio_1"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_2"), for: .normal)

    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        if btn_TimeRange.currentImage == UIImage(named: "redio_2")  && arySelectedRange.count == 0{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select time range.", viewcontrol: self)
        }else{
            updateSalesWODetail(strWdoID: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
            Global().updateSalesModifydate("\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String)

        }
        
    }
    //MARK:
    //MARK:-------Extra--Function-----------
    
    func setInitialUI() {
        
        let scheduleStartDate =  "\(dataGeneralInfo.value(forKey: "scheduleStartDate") ?? "")"
        //Scheduled TimeRangeId
        if("\(dataGeneralInfo.value(forKey: "timeRangeId") ?? "")") != "" && "\(dataGeneralInfo.value(forKey: "timeRangeId") ?? "")" != "0"{
            height_tf_Range.constant = DeviceType.IS_IPAD ? 50.0 : 44.0
            btn_Specific.setImage(UIImage(named: "redio_1"), for: .normal)
            btn_TimeRange.setImage(UIImage(named: "redio_2"), for: .normal)
            
            if "\(dataGeneralInfo.value(forKey: "scheduleDate") ?? "")".count > 0
            {
                tf_Date.text = changeStringDateToGivenFormat(strDate: "\(dataGeneralInfo.value(forKey: "scheduleDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            }
            else
            {
                tf_Date.text = changeStringDateToGivenFormat(strDate: scheduleStartDate, strRequiredFormat: "MM/dd/yyyy")
            }
            
            
            if "\(dataGeneralInfo.value(forKey: "scheduleTime") ?? "")".count > 0
            {
                tf_Time.text = changeStringDateToGivenFormat(strDate: "\(dataGeneralInfo.value(forKey: "scheduleTime") ?? "")", strRequiredFormat: "hh:mm a")

            }
            else
            {
                let startTime = changeStringDateToGivenFormat(strDate: scheduleStartDate, strRequiredFormat: "hh:mm a")
                tf_Time.text = startTime
            }
            
            let aryTimeRange = self.aryTimeRange.filter { (task) -> Bool in
                return ("\((task as! NSDictionary).value(forKey: "RangeofTimeId")!)".lowercased().contains("\(dataGeneralInfo.value(forKey: "timeRangeId") ?? "")"))
            } as NSArray
            
            if(aryTimeRange.count != 0){
                let dictData = aryTimeRange.object(at: 0) as! NSDictionary
                let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
                let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
                let strrenge = StartInterval + " - " + EndInterval
                tf_Range.text = strrenge
                
                if tf_Time.text == "" 
                {
                    tf_Time.text = StartInterval
                }

                self.arySelectedRange = NSMutableArray()
                self.arySelectedRange.add(dictData)
                tf_Range.tag = Int("\(dataGeneralInfo.value(forKey: "timeRangeId") ?? "0")")!

            }
 
        }else{
            
            height_tf_Range.constant = 0.0
            btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
            btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
            
            if "\(dataGeneralInfo.value(forKey: "scheduleDate") ?? "")".count > 0
            {
                tf_Date.text = changeStringDateToGivenFormat(strDate: "\(dataGeneralInfo.value(forKey: "scheduleDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            }
            else
            {
                tf_Date.text = changeStringDateToGivenFormat(strDate: scheduleStartDate, strRequiredFormat: "MM/dd/yyyy")
            }
            
            
            if "\(dataGeneralInfo.value(forKey: "scheduleTime") ?? "")".count > 0
            {
                tf_Time.text = changeStringDateToGivenFormat(strDate: "\(dataGeneralInfo.value(forKey: "scheduleTime") ?? "")", strRequiredFormat: "hh:mm a") //"\(dataGeneralInfo.value(forKey: "scheduleTime") ?? "")"//

            }
            else
            {
                let startTime = changeStringDateToGivenFormat(strDate: scheduleStartDate, strRequiredFormat: "hh:mm a")
                tf_Time.text = startTime
            }
            
        }

    }
    
    @objc func datePickerChanged(picker: UIDatePicker) {
        print(picker.date)
    }
 
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.delegate = self
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func updateSalesWODetail(strWdoID : String) {
        let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@", strWdoID))
        if(arrayLeadDetail.count > 0){
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("modifiedBy")
            arrOfValues.add(Global().getEmployeeId())
            arrOfKeys.add("modifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS'Z", ""))
            arrOfKeys.add("userName")
            arrOfValues.add(Global().getUserName())
            
            let datetime = "\(tf_Date.text!) \(tf_Time.text!)"
            let strDate = changeStringDateToGivenFormat(strDate: datetime, strRequiredFormat: "yyyy-MM-dd'T'HH:mm:ss")
            arrOfKeys.add("scheduleStartDate")
            arrOfValues.add(strDate)
            
            // for time range selected
            if btn_TimeRange.currentImage == UIImage(named: "redio_2")  && arySelectedRange.count != 0{
                
                let dictSelected = arySelectedRange.object(at: 0) as! NSDictionary
       
                    
                arrOfKeys.add("timeRangeId")
                arrOfValues.add("\(tf_Range.tag)")
                
                arrOfKeys.add("earliestStartTime")
                arrOfValues.add("\(dictSelected.value(forKey: "StartInterval")!)")
                
                arrOfKeys.add("latestStartTime")
                arrOfValues.add("\(dictSelected.value(forKey: "EndInterval")!)")
                
                arrOfKeys.add("scheduleTimeType")
                arrOfValues.add("Range")
                
                
                
            }else{
                arrOfKeys.add("timeRangeId")
                arrOfValues.add("")
                
                arrOfKeys.add("earliestStartTime")
                arrOfValues.add("")
                
                arrOfKeys.add("latestStartTime")
                arrOfValues.add("")
                
                arrOfKeys.add("scheduleTimeType")
                arrOfValues.add("Specific")
            }
            
            arrOfKeys.add("scheduleDate")
            arrOfValues.add(tf_Date.text ?? "")
            
            arrOfKeys.add("scheduleTime")
            arrOfValues.add(tf_Time.text ?? "")
            
            
            
            if(getDataFromDbToUpdate(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@",strWdoID), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)){
                self.navigationController?.popViewController(animated: false)
            }
            
        }
    }
    
}

// MARK: -
// MARK: - SalesNew_SelectionProtocol


extension SalesNew_EditScheduleVC: SalesNew_SelectionProtocol
{
    func didSelect(aryData: NSMutableArray, tag: Int) {
        
     }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        print(dictData)
        if(dictData.count != 0){
            self.arySelectedRange = NSMutableArray()
            self.arySelectedRange.add(dictData)
            let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
            let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
            let strrenge = StartInterval + " - " + EndInterval
            tf_Range.text = strrenge
            tf_Time.text = StartInterval
            
            tf_Range.tag = Int("\(dictData.value(forKey: "RangeofTimeId")!)")!
            
        }else{
            tf_Range.text = ""
        }
    }
}
