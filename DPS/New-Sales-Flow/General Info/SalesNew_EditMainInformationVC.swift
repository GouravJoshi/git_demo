//
//  SalesNew_EditMainInformationVC.swift
//  DPS
//
//  Created by NavinPatidar on 5/14/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class SalesNew_EditMainInformationVC: UIViewController {
    
    //MARK:---------Variable-----------
    //MARK:
    var strTitle = ""
    var arySelectedSource = NSMutableArray(),arySelectedUrgency = NSMutableArray(),arySelectedTags = NSMutableArray(),arySelectedSaleType = NSMutableArray(),arySelectedType = NSMutableArray()
    var dataGeneralInfo = NSManagedObject()
    var strLeadId = ""
    var strUserName = ""
    var strCompanyKey = ""
    var dictLoginData = NSDictionary()

    //MARK:---------IBOutlet-----------
    //MARK:
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
   
    @IBOutlet weak var tf_OpportunityName: ACFloatingTextField!
    @IBOutlet weak var tf_Company: ACFloatingTextField!
    
    @IBOutlet weak var tf_FirstName: ACFloatingTextField!
    @IBOutlet weak var tf_MiddleName: ACFloatingTextField!
    @IBOutlet weak var tf_LastName: ACFloatingTextField!
    @IBOutlet weak var tf_PrimaryEmail: ACFloatingTextField!
    @IBOutlet weak var tf_SecondaryEmail: ACFloatingTextField!
    @IBOutlet weak var tf_PrimaryPhone: ACFloatingTextField!
    @IBOutlet weak var tf_SecondaryPhone: ACFloatingTextField!
    @IBOutlet weak var tf_CellPhone: ACFloatingTextField!
    @IBOutlet weak var txt_AccountAlert: UITextView!

    @IBOutlet weak var tf_Type: ACFloatingTextField!
    @IBOutlet weak var tf_Source: ACFloatingTextField!
    @IBOutlet weak var tf_SaleType: ACFloatingTextField!
    @IBOutlet weak var tf_Urgency: ACFloatingTextField!
    @IBOutlet weak var tf_Tags: ACFloatingTextField!

    //MARK:---------LifeCycle-----------
    //MARK:

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strLeadId = "\(dataGeneralInfo.value(forKey: "leadId") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
        setInitialUI()
        fetchTagDetail()


    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
    }
    //MARK:-------Extra--Function-----------
    //MARK:
    func setInitialUI() {
        lblTitle.text = strTitle
        txt_AccountAlert.layer.cornerRadius = 8.0
        txt_AccountAlert.layer.borderWidth = 1.0
        txt_AccountAlert.layer.borderColor = UIColor.lightGray.cgColor
        
        tf_OpportunityName.delegate = self
        tf_FirstName.delegate = self
        tf_MiddleName.delegate = self
        tf_LastName.delegate = self
        tf_PrimaryEmail.delegate = self
        tf_SecondaryEmail.delegate = self
        tf_PrimaryPhone.delegate = self
        tf_SecondaryPhone.delegate = self
        tf_CellPhone.delegate = self
   
        tf_FirstName.text =  "\(dataGeneralInfo.value(forKey: "firstName") ?? "")"
        tf_MiddleName.text =  "\(dataGeneralInfo.value(forKey: "middleName") ?? "")"
        tf_LastName.text =  "\(dataGeneralInfo.value(forKey: "lastName") ?? "")"

        tf_PrimaryEmail.text =  "\(dataGeneralInfo.value(forKey: "primaryEmail") ?? "")"
        tf_SecondaryEmail.text =  "\(dataGeneralInfo.value(forKey: "secondaryEmail") ?? "")"
     
        tf_OpportunityName.text =  "\(dataGeneralInfo.value(forKey: "leadName") ?? "")"
        tf_Company.text =  "\(dataGeneralInfo.value(forKey: "companyName") ?? "")"

        tf_PrimaryPhone.text =  formattedNumber(number: "\(dataGeneralInfo.value(forKey: "primaryPhone") ?? "")")
        tf_SecondaryPhone.text = formattedNumber(number: "\(dataGeneralInfo.value(forKey: "secondaryPhone") ?? "")")
        tf_CellPhone.text =  formattedNumber(number: "\(dataGeneralInfo.value(forKey: "cellNo") ?? "")")

        txt_AccountAlert.text =  "\(dataGeneralInfo.value(forKey: "accountDescription") ?? "")"
        
        let strFlowType =  "\(dataGeneralInfo.value(forKey: "flowType") ?? "")"
        let dict = NSMutableDictionary()
        dict.setValue(strFlowType, forKey: "Name")
        dict.setValue(strFlowType == "Residential" ? "1" : "2", forKey: "id")
        arySelectedType.add(dict)
        tf_Type.text = strFlowType
        
        let strSalesType =  "\(dataGeneralInfo.value(forKey: "salesType") ?? "")"
        let dictSalesType = NSMutableDictionary()
        dictSalesType.setValue(strSalesType, forKey: "Name")
        dictSalesType.setValue(strSalesType.caseInsensitiveCompare("inside") == .orderedSame ? "1" : "2", forKey: "id")
        arySelectedSaleType.add(dictSalesType)
        
        if strSalesType.caseInsensitiveCompare("inside") == .orderedSame
        {
            tf_SaleType.text = "Inside"
        }
        else if strSalesType.caseInsensitiveCompare("field") == .orderedSame
        {
            tf_SaleType.text = "Field"
        }
        // tf_SaleType.text = strSalesType
        
        let strPrioritySysName =  "\(dataGeneralInfo.value(forKey: "prioritySysName") ?? "")"
        let aryUrgencySysName = strPrioritySysName.split(separator: ",")
        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryUrgencySysName as NSArray, getmasterName: "LeadDetailMaster", getkeyName: "UrgencyMasters", keyName: "Name", keySysName: "SysName") { [self] (result, name, sysName) in
            tf_Urgency.text = name
            self.arySelectedUrgency = result.mutableCopy()as! NSMutableArray
            
        }
        let strSourceSysName =  "\(dataGeneralInfo.value(forKey: "sourceSysName") ?? "")"
        let arySourceSysName = strSourceSysName.split(separator: ",")
        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: arySourceSysName as NSArray, getmasterName: "LeadDetailMaster", getkeyName: "SourceMasters", keyName: "Name", keySysName: "SourceSysName") {[self] (result, name, sysName) in
            tf_Source.text = name
            self.arySelectedSource = result.mutableCopy()as! NSMutableArray
        }

        txt_AccountAlert.text =  "\(dataGeneralInfo.value(forKey: "accountDescription") ?? "")"
        
//        let LeadTagExtDcs =  dataGeneralInfo.value(forKey: "LeadTagExtDcs") as! NSArray
//        var TagMasterId = ""
//           for item in LeadTagExtDcs {
//            let Name = (item as! NSDictionary).value(forKey: "TagMasterId") as! String
//            TagMasterId = TagMasterId + "\(Name),"
//           }
//        if TagMasterId.count != 0 {
//            TagMasterId = String(TagMasterId.dropLast())
//        }
//        let aryTagMasterId = TagMasterId.split(separator: ",")
//        SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryTagMasterId as NSArray , getmasterName: "LeadDetailMaster", getkeyName: "TagMasters", keyName: "Tag", keySysName: "TagMasterId") { [self] (result, name, sysName) in
//            tf_Tags.text = name
//            self.arySelectedTags = result.mutableCopy()as! NSMutableArray
//        }
        
    }
    
    func getOpportunityStatus() -> Bool {
        
        let strStatusss =  dataGeneralInfo.value(forKey: "statusSysName") ?? ""
        let strStageSysName =  dataGeneralInfo.value(forKey: "stageSysName") ?? ""
        
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won")
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func updateSalesWODetail(strWdoID : String) {
        let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@", strWdoID))
        if(arrayLeadDetail.count > 0){
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()

            arrOfKeys.add("firstName")
            arrOfValues.add(tf_FirstName.text?.trimmed ?? "") //1
            arrOfKeys.add("middleName")
            arrOfValues.add(tf_MiddleName.text?.trimmed ?? "")//2
            arrOfKeys.add("lastName")
            arrOfValues.add(tf_LastName.text?.trimmed ?? "")//3
            arrOfKeys.add("primaryEmail")
            arrOfValues.add(tf_PrimaryEmail.text?.trimmed ?? "")//4
            arrOfKeys.add("secondaryEmail")
            arrOfValues.add(tf_SecondaryEmail.text?.trimmed ?? "")//5
            arrOfKeys.add("primaryPhone")
            arrOfValues.add(tf_PrimaryPhone.text?.trimmed ?? "")//6
            arrOfKeys.add("secondaryPhone")
            arrOfValues.add(tf_SecondaryPhone.text?.trimmed ?? "")//7
            arrOfKeys.add("cellNo")
            arrOfValues.add(tf_CellPhone.text?.trimmed ?? "")//8
            arrOfKeys.add("flowType")
            arrOfValues.add(tf_Type.text?.trimmed ?? "")//9
            arrOfKeys.add("salesType")
            if tf_SaleType.text == "inside" || tf_SaleType.text == "Inside"
            {
                arrOfValues.add("inside")
            }
            else if tf_SaleType.text == "field" || tf_SaleType.text == "Field"
            {
                arrOfValues.add("field")
            }
            else
            {
                arrOfValues.add("inside")
            }
            //arrOfValues.add(tf_SaleType.text?.trimmed ?? "")//10
            
            
            
            arrOfKeys.add("accountDescription")
            arrOfValues.add(txt_AccountAlert.text?.trimmed ?? "")//11
            
            
            let idsourceSysName = NSMutableArray()
            for employee in arySelectedSource {
                idsourceSysName.add((employee as AnyObject).value(forKey: "SourceSysName") ?? "")
            }
            arrOfKeys.add("sourceSysName")
            arrOfValues.add(idsourceSysName.componentsJoined(by: ","))//12
            
            let idprioritySysName = NSMutableArray()
            for employee in arySelectedUrgency {
                idprioritySysName.add((employee as AnyObject).value(forKey: "SysName") ?? "")
            }
            arrOfKeys.add("prioritySysName")
            arrOfValues.add(idprioritySysName.componentsJoined(by: ","))//13
        
            // YAd se Bhejna hai isko
            
            
//            let aryTagSend = NSMutableArray()
//            for item in arySelectedTags {
//                let dict = NSMutableDictionary()
//                dict.setValue("\((item as AnyObject).value(forKey: "LeadTagId") ?? "0")" , forKey: "LeadTagId")
//                dict.setValue("\((item as AnyObject).value(forKey: "LeadId") ?? "0")" , forKey: "LeadId")
//                dict.setValue("\((item as AnyObject).value(forKey: "TagMasterId") ?? "0")", forKey: "TagMasterId")
//                aryTagSend.add(dict)
//            }
//            arrOfKeys.add("LeadTagExtDcs")
//            arrOfValues.add(aryTagSend)
            
            
            
            arrOfKeys.add("modifiedBy")
            arrOfValues.add(Global().getEmployeeId())
            arrOfKeys.add("modifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("yyyy-MM-dd'T'HH:mm:ss.SSS'Z", ""))
            arrOfKeys.add("userName")
            arrOfValues.add(Global().getUserName())
          
            arrOfKeys.add("leadName")
            arrOfValues.add(tf_OpportunityName.text?.trimmed ?? "") //1
            arrOfKeys.add("companyName")
            arrOfValues.add(tf_Company.text?.trimmed ?? "")//2
            
            if(getDataFromDbToUpdate(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@",strWdoID), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)){
                self.navigationController?.popViewController(animated: true)
            }
           
        }
    }
   
    func Validation() -> String {
        
        
        if tf_OpportunityName.text?.count == 0{
            //tf_OpportunityName.text = ""
            //tf_OpportunityName.becomeFirstResponder()
            return "Opportunity name required!"
        }
        
        if tf_FirstName.text?.trimmed.count == 0  {
            return "First name required!"
        }else if tf_LastName.text?.count == 0 {
            return "Last name required!"
        }
        
        if tf_PrimaryEmail.text?.count != 0{
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(tf_PrimaryEmail.text!)

            if !(isValid) {
                return "Please enter valid primary email!"
            }
        }
        if tf_SecondaryEmail.text?.count != 0{
             
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(tf_SecondaryEmail.text!)

            if !(isValid) {
                return "Please enter valid secondary email!"
            }
        }
        if tf_PrimaryPhone.text?.count != 0{
            if tf_PrimaryPhone.text!.count < 12 {
                return "Please enter valid primary phone!"
            }
        }
        if tf_SecondaryPhone.text?.count != 0{
            if tf_SecondaryPhone.text!.count < 12 {
                return "Please enter valid secondary phone!"
            }
        }
        if tf_CellPhone.text?.count != 0{
            if tf_CellPhone.text!.count < 12 {
                return "Please enter valid cell phone!"
            }
        }
        
        if tf_Type.text?.count == 0{
            return "Flow type required!"
        }
        return ""
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.delegate = self
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func fetchTagDetail()
    {
        let arrTagMasterId = NSMutableArray()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadTagExtDcs", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        
        if arryOfData.count > 0
        {
            for item in arryOfData
            {
                let objMatch = item as! NSManagedObject
                arrTagMasterId.add("\(objMatch.value(forKey: "tagMasterId") ?? "")")
            }
            
            for item in arrTagMasterId
            {
                let strId = item as! String
                
                for itemNew in getAllTag()
                {
                    let dict = itemNew as! NSDictionary
                    
                    if "\(dict.value(forKey: "TagMasterId") ?? "")" == strId
                    {
                        arySelectedTags.add(dict)
                    }
                }
            }
            
            var strName = ""
            for item in arySelectedTags
            {
                strName = "\(strName),\((item as AnyObject).value(forKey: "Tag")!)"
            }
            if strName.count != 0{
                strName = String(strName.dropFirst())
            }
            tf_Tags.text = strName
        }
        else
        {
            tf_Tags.text = ""

        }
        
        
    }
    func getTageObjectFromId(strId : String) -> NSDictionary  {
        
        var dict = NSDictionary()
        let dictLeadDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        if dictLeadDetailMaster.value(forKey: "TagMasters") is NSArray {
            
            let arr = dictLeadDetailMaster.value(forKey: "TagMasters") as! NSArray
            
            let arrFilter =  arr.filter { (dict) -> Bool in
                
                return "\((dict as! NSDictionary).value(forKey: "TagMasterId") ?? "")" == strId
            } as NSArray
            
            if arrFilter.count > 0
            {
                dict = arrFilter.object(at: 0) as! NSDictionary

            }
        }
        
        return dict
    }
    func getAllTag() -> NSArray {
        
        var arrTag = NSArray()
        let dictLeadDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        if dictLeadDetailMaster.value(forKey: "TagMasters") is NSArray {
            
            let arr = dictLeadDetailMaster.value(forKey: "TagMasters") as! NSArray
            arrTag = arr
        }
        return arrTag
    }
    
    func saveTag() {
                
        let arryOfTag =  getDataFromCoreDataBaseArray(strEntity: "LeadTagExtDcs", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
        for item in arryOfTag
        {
            let objTempSales  = item as! NSManagedObject
            deleteDataFromDB(obj: objTempSales)
        }
        
        for item in arySelectedTags
        {
            let dict = item as! NSDictionary
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()

            arrOfKeys = ["leadId",
                         "userName",
                         "companyKey",
                         "leadTagId",
                         "tagMasterId"]
            arrOfValues = [strLeadId,
                         strUserName,
                         strCompanyKey,
                         "0",
                         "\(dict.value(forKey: "TagMasterId") ?? "")"]
                    
            saveDataInDB(strEntity: "LeadTagExtDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }

        
    }

    //MARK:------IBAction-----------
    //MARK:
    @IBAction func action_Back(_ sender: Any) {
        self.view.endEditing(true)

        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func action_Type(_ sender: Any) {
        
        //Temp commented untill New Commercial Flow Started 19 Oct  2021
        
        if !getOpportunityStatus()
        {
            self.view.endEditing(true)
            let aryTypelist = [["Name":"Residential","id":"1"],
                               ["Name":"Commercial","id":"2"]]
            openTableViewPopUp(tag: 59, ary: (aryTypelist as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: arySelectedType)
        }
        
    }
    @IBAction func action_Source(_ sender: Any) {
        self.view.endEditing(true)
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary {
            let dictLeadDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                var arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                arrOfData = returnFilteredArray(array: arrOfData)
                if(arrOfData.count != 0){
                    goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedSource, tag: 2, titleHeader: "Source", isMultiSelection: true, ShowNameKey: "Name")
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
                
            }
        }
    }
    @IBAction func action_SaleType(_ sender: Any) {
        
       
        self.view.endEditing(true)
        let aryTypelist = [["Name":"Inside","id":"1"],
                           ["Name":"Field","id":"2"]]
        openTableViewPopUp(tag: 250, ary: (aryTypelist as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: arySelectedSaleType)
       
    }
      
    @IBAction func action_Urgency(_ sender: Any) {
        self.view.endEditing(true)
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary {
            let dictLeadDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if dictLeadDetailMaster.value(forKey: "UrgencyMasters") is NSArray {
                var arrOfData = (dictLeadDetailMaster.value(forKey: "UrgencyMasters") as! NSArray).mutableCopy() as! NSMutableArray
                arrOfData = returnFilteredArray(array: arrOfData)
                if(arrOfData.count != 0){
                    openTableViewPopUp(tag: 62, ary: (arrOfData as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: arySelectedUrgency)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
    }
    
    @IBAction func action_Tags(_ sender: Any) {
        //Tag , TagMasterId
        self.view.endEditing(true)
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary {
            let dictLeadDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if dictLeadDetailMaster.value(forKey: "TagMasters") is NSArray {
                var arrOfData = (dictLeadDetailMaster.value(forKey: "TagMasters") as! NSArray).mutableCopy() as! NSMutableArray
                arrOfData = returnFilteredArray(array: arrOfData)
                if(arrOfData.count != 0){
                    goToNewSalesSelectionVC(arrItem: arrOfData, arrSelectedItem: self.arySelectedTags, tag: 1, titleHeader: "Tag", isMultiSelection: true, ShowNameKey: "Tag")
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
        }
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if(Validation() != ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Validation(), viewcontrol: self)
        }else{
            saveTag()
            updateSalesWODetail(strWdoID: "\(dataGeneralInfo.value(forKey: "leadId") ?? "")")
            Global().updateSalesModifydate("\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String)
        }
    }

}




//MARK:------UITextFieldDelegate-----------
//MARK:
extension SalesNew_EditMainInformationVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
         //textField.text = textField.text?.trimmed
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if textField == tf_CellPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = tf_CellPhone.text!
                strTextt = String(strTextt.dropLast())
                tf_CellPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        if textField == tf_PrimaryPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = tf_PrimaryPhone.text!
                strTextt = String(strTextt.dropLast())
                tf_PrimaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        if textField == tf_SecondaryPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = tf_SecondaryPhone.text!
                strTextt = String(strTextt.dropLast())
                tf_SecondaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 85)
    }
    
}
// MARK: - Selection CustomTableView
// MARK: -

extension SalesNew_EditMainInformationVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 59)
        {
            if(dictData.count == 0){
                tf_Type.text = ""
                self.arySelectedType = NSMutableArray()
            }else{
                tf_Type.text = "\(dictData.value(forKey: "Name")!)"
                self.arySelectedType = NSMutableArray()
                self.arySelectedType.add(dictData)
            }
        }
        else if(tag == 250)
        {
            if(dictData.count == 0){
                tf_SaleType.text = ""
                self.arySelectedSaleType = NSMutableArray()
            }else{
                tf_SaleType.text = "\(dictData.value(forKey: "Name")!)"
                self.arySelectedSaleType = NSMutableArray()
                self.arySelectedSaleType.add(dictData)
            }
         
        }
        else if(tag == 62)
        {
            if(dictData.count == 0){
                tf_Urgency.text = ""
                tf_Urgency.tag = 0
                self.arySelectedUrgency = NSMutableArray()

            }else{
                
                tf_Urgency.text = "\(dictData.value(forKey: "Name")!)"
                tf_Urgency.tag = ("\(dictData.value(forKeyPath: "UrgencyId")!)" == "" ? 0 : Int("\(dictData.value(forKeyPath: "UrgencyId")!)"))!
                self.arySelectedUrgency = NSMutableArray()
                self.arySelectedUrgency.add(dictData)
            }
        }
    }
}

// MARK: - SalesNew_SelectionProtocol
// MARK: -

extension SalesNew_EditMainInformationVC: SalesNew_SelectionProtocol
{
  
    func didSelect(aryData: NSMutableArray, tag: Int) {
        //-----Tag------
        if(tag == 1){
            if(aryData.count != 0){
                var strName = ""
                arySelectedTags = NSMutableArray()
                arySelectedTags = aryData.mutableCopy() as! NSMutableArray
                for item in arySelectedTags{
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Tag")!)"
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                tf_Tags.text = strName
            }else{
                tf_Tags.text = ""
            }
        }
        //-----Source------
        else if(tag == 2){
            if(aryData.count == 0){
                arySelectedSource = NSMutableArray()
                tf_Source.text = ""
            }else{
                var strName = ""
                arySelectedSource = NSMutableArray()
                arySelectedSource = aryData.mutableCopy() as! NSMutableArray
                
                for item in aryData{
                
                    strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                }
                if strName.count != 0{
                    strName = String(strName.dropFirst())
                }
                tf_Source.text = strName
            }
        }
        
    }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        
    }
}

