//
//  FormBuilderViewModel.swift
//  DPS
//
//  Created by APPLE on 05/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
protocol FormBuilderDelegate: CommonView {
    func sucessfullyGetHTML(_ output: String)
    func sucessfullyGetPDF(_ output: String)
}

class FormBuilderPresenter: NSObject {
    weak private var delegate: FormBuilderDelegate?
    private lazy var service = {
        
        return FormBuilderService()
    }()
    
    init(_ delegate: FormBuilderDelegate) {
        self.delegate = delegate
    }
    
    
    /**
    * Method name: apiCallForFormBuilder
    * Description:This method is for FormBuilder View controller and it calls the private method to call a FormBuilder API.
    * Parameters:
    */
    public func apiCallForFormBuilder(parameters: [String: Any])
    {
        apiCallForFormBuilderHttp(parameters: parameters);
    }
    
    //MARK: Private Functions
    private func apiCallForFormBuilderHttp(parameters: [String: Any])
    {
        
        delegate?.showLoader()
        FormBuilderService.sharedInstance.requestSetupFormBuilder(parameters: parameters) { (result, error) in
            
            
            if(error != nil){
//                self.delegate?.requestFailedWithError(message: error?.localizedDescription ?? Alert.sharedInstance.MSG_UNKNOWN_ERROR)
            }else if (result != nil){

                print(result as Any)
                self.delegate?.sucessfullyGetHTML(result ?? "")
            }
        }
    }
    
    
    /**
    * Method name: requestToGetPdf
    * Description:
    * Parameters:
    */
    public func requestToGetPdf(strbase64: String, contractorId: String, accountNo: String, isRelativePath: Bool)
    {
        requestToGetPdfHttp(strbase64: strbase64, contractorId: contractorId, accountNo: accountNo, isRelativePath: isRelativePath);
    }
    
    //MARK: Private Functions
    private func requestToGetPdfHttp(strbase64: String, contractorId: String, accountNo: String, isRelativePath: Bool)
    {
        
        delegate?.showLoader()
        FormBuilderService.sharedInstance.requestToGetPdf(strbase64: strbase64, contractorId: contractorId, accountNo: accountNo, isRelativePath: isRelativePath) { (result, error) in
            
            
            if(error != nil){
//                self.delegate?.requestFailedWithError(message: error?.localizedDescription ?? Alert.sharedInstance.MSG_UNKNOWN_ERROR)
            }else if (result != nil){

                print(result as Any)
                self.delegate?.sucessfullyGetPDF(result ?? "")
            }
        }
    }
}
