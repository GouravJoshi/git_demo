//
//  PopupPickerViewController.swift
//  Discovered
//
//  Created by Mac on 23/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

enum PopUpTitle: String {
    case payment = "Payment Method"
    
}

struct Payment {
    var payment = [String]()
}



class PopupPickerViewController: UIViewController {

    @IBOutlet weak var picker: UIPickerView!
    
    var strTitle: String = ""
    var paymentMethod = Payment()
    

    @IBOutlet weak var navBar: UINavigationItem!
   
    var selectedPaymentMethod: String?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.selectedPaymentMethod = "----Select----"
        
        
        self.navBar.title = strTitle
        if strTitle == PopUpTitle.payment.rawValue {
            paymentMethod.payment = ["----Select----", "Cash", "Check", "Credit Card", "Auto Charge Customer", "Collect at time of Scheduling", "Invoice"]
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if self.strTitle == PopUpTitle.payment.rawValue {
                NotificationCenter.default.post(name: Commons.kNotificationPaymentMethod, object: self.selectedPaymentMethod, userInfo: nil)
            }
        })
    }
}

extension PopupPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return paymentMethod.payment.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.paymentMethod.payment[row]
    }
    
    // Capture the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        self.selectedPaymentMethod = paymentMethod.payment[row]
    }
}
