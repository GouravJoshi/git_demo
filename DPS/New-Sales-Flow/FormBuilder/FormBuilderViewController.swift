//
//  FormBuilderViewController.swift
//  DPS
//
//  Created by APPLE on 04/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import WebKit


class FormBuilderViewController: UIViewController, WKNavigationDelegate {
    var webView: WKWebView!
    
    private lazy var presenter = {
        return FormBuilderPresenter(self)
    }()
    
    @IBOutlet weak var loadSpinner: UIActivityIndicatorView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var viewForWeb: UIView!
    @IBOutlet weak var tblViewForPayment: UITableView!
    @IBOutlet weak var viewForSwitch: UIView!
    @IBOutlet weak var collectionViewFB: UICollectionView!
    @IBOutlet weak var switchCustomerPresent: UISwitch!
    @IBOutlet weak var switchViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTaxAmount: UILabel!
    @IBOutlet weak var lblBillingAmount: UILabel!
    @IBOutlet weak var lblSelectedPaymentMethod: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtCheckNo: UITextField!
    @IBOutlet weak var txtDrivingLicenceNo: UITextField!
    @IBOutlet weak var amountViewHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var licenceView: UIView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var cardView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnExpDate: UIButton!
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var loadCount: Int = 0
    var dictTemplateObj: [String: Any]?
    var matchesGeneralInfo : NSManagedObject!
    var objPaymentInfo : NSManagedObject!
    var dictPricingInfo: [String: Any] = [:]
    var arrSoldService: [NSManagedObject] = []
    
    let inset: CGFloat = 5
    let minimumLineSpacing: CGFloat = 0
    let minimumInteritemSpacing: CGFloat = 0
    var arrItem = ["Salesperson", "Customer", "Payment", "Review"]
    var arrImages = ["Salesperson-Customer", "Salesperson-Customer", "Payment", "Review"]
    var selectedIndex = 0
    var role = 1
    var strOverViewPDF = ""
    var strLeadStatusGlobal         = ""
    var strStageSysName         = ""
    var arrImageDetails: [[String: Any]] = []
    var arrGraphDetails: [[String: Any]] = []
    
    var selectecPaymentType = ""
    var strPaymentMode = ""
    var strAmount = ""
    var strChequeNo     = ""
    var strLicenseNo    = ""
    var strDate         = ""
    var arrTerms: [String] = []
    var strTemplateKey  = ""
    let global = Global()
    var strCompanyKey = ""
    var strUserName = ""
    var isElementIntegration = false

    var isFrontImage = false
    var checkFrontImg: UIImage?
    var checkBackImg: UIImage?
    var arrOFImagesName: [String] = []
    var arrOfCheckBackImage: [String] = []
    var strLeadId = ""
    var strBaseUrl = ""
    var strAccountNo = ""


    var strText = "false"
    var strPhone = "false"
    var strEmail = "false"
    
    
    @IBOutlet weak var btn_Text: UISwitch!
    @IBOutlet weak var btn_Phone: UISwitch!
    @IBOutlet weak var btn_Email: UISwitch!
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var btnBackI: UIButton!
    @IBOutlet weak var btnFrontI: UIButton!
    @IBOutlet weak var btnEleTAuth: UIButton!
    @IBOutlet weak var lblEleAuth: UILabel!
    
    var shouldShowElectronicFormLink = false
    var isAuthFormFilled = false
    var dictSelectedCustomPaymentMode = NSDictionary()
    
    @IBOutlet weak var viewForCardType: UIView!
    @IBOutlet weak var cardTypeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCardType: UILabel!
    @IBOutlet weak var btnAddCard: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.lblSelectedPaymentMethod.text = "----Select----"
        switchViewHeightConstraint.constant = 0
        viewForSwitch.isHidden = true
        switchCustomerPresent.isEnabled = false
        amountViewHightConstraint.constant = 0
        amountView.isHidden = true
        checkView.isHidden = true
        licenceView.isHidden = true
        buttonsView.isHidden = true
        cardView.isHidden = true
        cardView_HeightConstraint.constant = 0
        self.loadSpinner.isHidden = true
//        print(dictTemplateObj)
//        print(matchesGeneralInfo)
//        print(objPaymentInfo)
//        print(self.arrImageDetails)
//        print(self.arrGraphDetails)
//
//        print(self.dictPricingInfo)
        self.strLeadId = "\(nsud.value(forKey: "LeadId") ?? "")"
        self.setLeadDetails()
        self.paymentInitialSetUp()
        if getOpportunityStatus() == false {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.localStorageWebView()//4.
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loginDetails()
        self.setLeadDetails()
        
        if getOpportunityStatus() {
            
            print(self.strOverViewPDF)
            self.btn_Text.isUserInteractionEnabled = false
            self.btn_Phone.isUserInteractionEnabled = false
            self.btn_Email.isUserInteractionEnabled = false
            
            self.btnExpDate.isUserInteractionEnabled = false
            self.btnPayment.isUserInteractionEnabled = false
            self.btnFrontI.isUserInteractionEnabled = false
            self.btnBackI.isUserInteractionEnabled = false
            
            self.txtCheckNo.isUserInteractionEnabled = false
            self.txtDrivingLicenceNo.isUserInteractionEnabled = false

            self.goTOOverView()
        }else {
            print(selectedIndex)
            if selectedIndex == 3 {
                self.goTOOverView()
            }else{
                //                self.localStorageWebView()//4.
            }
        }
        
        //----Header
        lblTitle.text = "\(nsud.value(forKey: "titleHeader1") ?? "")"
        lblSubTitle.text = "\(nsud.value(forKey: "titleHeader2") ?? "")"
        self.electronicAuthCondition()
        handleCheckImage()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loadCount += 1
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

        loadCount -= 1

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            if self!.loadCount == 0 {
                self!.stopLoading()
            }
        }

    }
    
    func startLoading() {
        
        self.loadSpinner.isHidden = false
        self.loadSpinner.startAnimating()
        self.loadPdfOverView(strPDF: self.strOverViewPDF)
        self.webView.navigationDelegate = self

        
    }
    
    func stopLoading() {
        self.loadSpinner.stopAnimating()
        self.loadSpinner.isHidden = true
    }
    
    func handleCheckImage() {
        
        if nsud.bool(forKey: "frontImageDeleted")
        {
            arrOFImagesName = []
            nsud.setValue(false, forKey: "frontImageDeleted")
            nsud.synchronize()
            self.objPaymentInfo?.setValue("", forKey: "checkFrontImagePath")
        }
        if nsud.bool(forKey: "backImageDeleted")
        {
            arrOfCheckBackImage = []
            nsud.setValue(false, forKey: "backImageDeleted")
            nsud.synchronize()
            self.objPaymentInfo?.setValue("", forKey: "checkBackImagePath")
        }
        
        if nsud.bool(forKey: "yesEditImage")
        {
            nsud.setValue(false, forKey: "yesEditImage")
            nsud.synchronize()
            
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Service_iPhone" : "Service_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditImageViewController") as! EditImageViewController
            
            self.navigationController?.present(vc, animated: true, completion: nil)
        }


        if nsud.bool(forKey: "frontImageDeleted") || nsud.bool(forKey: "backImageDeleted") {
            let context = getContext()
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    func getOpportunityStatus() -> Bool
    {
        if strLeadStatusGlobal.caseInsensitiveCompare("Complete") == .orderedSame && strStageSysName.caseInsensitiveCompare("Won") == .orderedSame
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func loginDetails() {
        let dictLoginData: [String : Any] = nsud.value(forKey: "LoginDetails") as! [String : Any]
        print(dictLoginData)
        
        self.strCompanyKey = (dictLoginData["Company"]
                                as? [String: Any])?["CompanyKey"]
            as? String ?? ""
        self.strUserName = (dictLoginData["Company"]
                                as? [String: Any])?["Username"]
            as? String ?? ""
        
        self.isElementIntegration = ((dictLoginData["Company"]
                                        as? [String: Any])?["CompanyConfig"]
                                        as? [String: Any])?["IsElementIntegration"] as? Bool ?? false
        
        self.strBaseUrl = (((dictLoginData["Company"]
            as? [String: Any])?["CompanyConfig"] as? [String: Any])?["SalesAutoModule"] as? [String: Any])?["ServiceUrl"] as? String ?? ""//"\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        print(self.strBaseUrl)
        
        shouldShowElectronicFormLink =   ((dictLoginData["Company"]
                                    as? [String: Any])?["CompanyConfig"]
                                    as? [String: Any])?["IsElectronicAuthorizationForm"] as? Bool ?? false
        
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func setLeadDetails() {
       // self.matchesGeneralInfo = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
        print(self.matchesGeneralInfo)

        
//        self.strTaxCodeSysName = "\(matchesGeneralInfo.value(forKey: "taxSysName")!)"
        strLeadStatusGlobal = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        
        if self.strOverViewPDF == "" {
            self.strOverViewPDF = "\(matchesGeneralInfo.value(forKey: "pdfPath") ?? "")"
        }
        
        let strIsAuthFormFilled = "\(matchesGeneralInfo.value(forKey: "accountElectronicAuthorizationFormSigned") ?? "")"
        if strIsAuthFormFilled == "true" || strIsAuthFormFilled == "1" {
            isAuthFormFilled = true
        }else {
            isAuthFormFilled = false
        }
        
        self.strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"

        self.strText = "\(matchesGeneralInfo.value(forKey: "smsReminders") ?? "false")"
        self.strPhone = "\(matchesGeneralInfo.value(forKey: "phoneReminders") ?? "false")"
        self.strEmail = "\(matchesGeneralInfo.value(forKey: "emailReminders") ?? "false")"

        if self.strText == "" || self.strText == "false" || self.strText == "0" {
            self.strText = "false"
        }else {
            self.strText = "true"
        }
        if self.strPhone == "" || self.strPhone == "false" || self.strPhone == "0" {
            self.strPhone = "false"
        }else {
            self.strPhone = "true"
        }
        if self.strEmail == "" || self.strEmail == "false" || self.strEmail == "0" {
            self.strEmail = "false"
        }else {
            self.strEmail = "true"
        }
        
        if self.strText == "true" || self.strText == "1" {
            self.btn_Text.isOn = true
        }else{
            self.btn_Text.isOn = false
        }
        if self.strPhone == "true" || self.strPhone == "1" {
            self.btn_Phone.isOn = true
        }else{
            self.btn_Phone.isOn = false
        }
        if self.strEmail == "true" || self.strEmail == "1" {
            self.btn_Email.isOn = true
        }else{
            self.btn_Email.isOn = false
        }
        
        arrTerms.append(matchesGeneralInfo.value(forKey: "leadGeneralTermsConditions") as? String ?? "")
        if arrTerms[0] == "" {
            arrTerms.removeAll()
            let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            let arrTermsTempNew: NSArray = dictMasters.value(forKey: "MultipleGeneralTermsConditions") as! NSArray
            var tempMatch = false
            
            if arrTermsTempNew.count > 0 {
                var dictGneralTerms: [String: Any]?
                for dict in arrTermsTempNew {
                    print(dict)
                    dictGneralTerms = dict as? [String : Any]
                    if "\(dictGneralTerms!["IsDefault"] ?? "0")" == "1"  && "\(dictGneralTerms!["IsActive"] ?? "0")" == "1" {
                        tempMatch = true
                        break
                    }
                }
                
                if tempMatch {
                    arrTerms.append(dictGneralTerms!["TermsConditions"] as! String)
                }else {
                    arrTerms.append("")
                }
                
            }else {
                arrTerms.append("")
            }
        }
        print(arrTerms)
        
        
//        let audioPath = "\(matchesGeneralInfo.value(forKey: "audioFilePath") ?? "")"
//        if (audioPath.count>0) {
//            print(audioPath)
//            strGlobalAudio = audioPath;
//            self.downloadingAudio(strPath: audioPath)
//        }else {
//            strGlobalAudio = ""
//        }
        
    }
    
    func electronicAuthCondition() {
        if shouldShowElectronicFormLink {
            if isAuthFormFilled {
                //Available
                self.btnEleTAuth.isHidden = false
                self.lblEleAuth.textColor = UIColor.init(_colorLiteralRed: 0.0/155.0, green: 51.0/155.0, blue: 25.0/155.0, alpha: 1.0)
                self.lblEleAuth.text = "(Signed Form Available On Account)"
            }else {
                //Not Available
                self.btnEleTAuth.isHidden = false
                self.lblEleAuth.textColor = .red
                self.lblEleAuth.text = "(Signed Form Not Available On Account)"
            }
        }else {
            //Button Hide
            self.btnEleTAuth.isHidden = true
            self.lblEleAuth.isHidden = true
        }
    }
    
    func goToCreditCardView() {
        
        /*
         objCreditCard.strGlobalLeadId=strLeadId;
         objCreditCard.strAmount=_txtPaidAmountPriceInforamtion.text;
         objCreditCard.strTypeOfService=@"Lead";
         objCreditCard.strDeviceType=@"iPhone";
         */
        
        let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
        let objCreditCardView = storyboardIpad.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
        
        var strValue = String()
        
        if !switchCustomerPresent.isOn
        {
            strValue = "no"
        }
        else
        {
            strValue = "yes"
        }
        objCreditCardView?.isCustomerPresent = strValue as NSString
        objCreditCardView?.strAmount = txtAmount.text!
        objCreditCardView?.strGlobalLeadId = self.strLeadId
        objCreditCardView?.strTypeOfService = "Lead"
        //objCreditCardView?.workOrderDetailNew = objWorkorderDetail
        objCreditCardView?.strFromWhichView = "FormBuilder"
        objCreditCardView?.strDeviceType = "iPad"
        //  self.navigationController?.pushViewController(objCreditCardView!, animated: false)
        self.present(objCreditCardView!, animated:true, completion:nil)
        
    }
    
    func goTOOverView() {
//        self.selectedIndex = self.selectedIndex + 1
        self.selectedIndex = 3
        
        self.switchViewHeightConstraint.constant = 0
        self.viewForSwitch.isHidden = true
        self.collectionViewFB.reloadData()
        self.startLoading()
        self.loadPdfOverView(strPDF: self.strOverViewPDF)
        
        self.view.bringSubviewToFront(self.viewForWeb)
        cardView.isHidden = false
        cardView_HeightConstraint.constant = DeviceType.IS_IPAD ? 105 : 85
        self.view.bringSubviewToFront(cardView)
        self.view.bringSubviewToFront(self.loadSpinner)

    }
    
    //4.
    func localStorageWebView() {
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strFirstName = "\(matchesGeneralInfo.value(forKey: "firstName") ?? "")"
        let strLastName = "\(matchesGeneralInfo.value(forKey: "lastName") ?? "")"
        let strMiddleName = "\(matchesGeneralInfo.value(forKey: "middleName") ?? "")"
        let strSAddress1 = "\(matchesGeneralInfo.value(forKey: "servicesAddress1") ?? "")"
        let strSAddress2 = "\(matchesGeneralInfo.value(forKey: "serviceAddress2") ?? "")"
        let strSCity = "\(matchesGeneralInfo.value(forKey: "serviceCity") ?? "")"
        let strSState = "\(matchesGeneralInfo.value(forKey: "serviceState") ?? "")"
        let strSZip = "\(matchesGeneralInfo.value(forKey: "serviceZipcode") ?? "")"
        let strPhone = formattedNumber(number: ReplaceBlankToSpace(str: "\(matchesGeneralInfo.value(forKey: "primaryPhone") ?? "")"))
        let strMobile = formattedNumber(number: ReplaceBlankToSpace(str: "\(matchesGeneralInfo.value(forKey: "cellNo") ?? "")"))
        let strEmail = "\(matchesGeneralInfo.value(forKey: "primaryEmail") ?? "")"
        let strSecEmail = "\(matchesGeneralInfo.value(forKey: "secondaryEmail") ?? "")"
        self.strTemplateKey = "\(self.dictTemplateObj?["TemplateKey"] ?? "")"
        /**************Basic****************/
        let strCustomerCompanyName = "\(matchesGeneralInfo.value(forKey: "companyName") ?? "")"
        let strAccountName = Global().strFullName(fromCoreDB: matchesGeneralInfo) ?? ""
        let strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        //let strEmpFullName = "\(matchesGeneralInfo.value(forKey: "firstName") ?? "") \(matchesGeneralInfo.value(forKey: "lastName") ?? "")"
        let strEmpFullName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        let strEmpLicenseNo = "\(dictLoginData.value(forKey: "LicenceNo") ?? "")"//"\(objPaymentInfo.value(forKey: "licenseNo") ?? "")"
        /******************************/
        
        /**************Payment****************/
        var objSoldServiceDetail = NSManagedObject()
        objSoldServiceDetail = self.arrSoldService[0]
        print(objSoldServiceDetail)
        let dictObj = objSoldServiceDetail.toDict()
        var sysName = ""
        if dictObj["serviceSysName"] != nil {
            sysName =  "\(dictObj["serviceSysName"] ?? "")"
        }else {
            sysName = "\(dictObj["serviceName"] ?? "")"
        }
        
        var pkjId = ""
        if dictObj["packageId"] != nil {
            pkjId = "\(dictObj["packageId"] ?? "")"
        }
        
        let dictPackage = getPackageObjectFromPackageId(strServiceSysName: sysName, strPackageId: pkjId)
       
        let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objSoldServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
        
        let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objSoldServiceDetail.value(forKey: "frequencySysName") ?? "")")
        print(dictBillingFrequency)
        print(dictServiceFrequency)

        let strPackageName = "\(dictPackage["PackageName"] ?? "")"
        let strInitialAmountWithDiscount = (self.dictPricingInfo["totalPrice"] as! String).replacingOccurrences(of: "$", with: "")
        let strInitialAmountWithoutDiscount = (self.dictPricingInfo["subTotalInitial"] as! String).replacingOccurrences(of: "$", with: "")
        let strMaintenanceAmount = (self.dictPricingInfo["totalPriceMaintenance"] as! String).replacingOccurrences(of: "$", with: "")
        let strServiceFrequency = "\(dictServiceFrequency["FrequencyName"] ?? "")"
        let strBillingFrequency = "\(dictBillingFrequency["FrequencyName"] ?? "")"
        let strBillingAmount = (self.dictPricingInfo["billingAmount"] as! String).replacingOccurrences(of: "$", with: "")
        /******************************/
        let strBillingAddressLine1  = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? "")"
        let strBillingAddressLine2  = "\(matchesGeneralInfo.value(forKey: "billingAddress2") ?? "")"
        let strBillingCity          = "\(matchesGeneralInfo.value(forKey: "billingCity") ?? "")"
        let strBillingState         = "\(matchesGeneralInfo.value(forKey: "billingState") ?? "")"
        let strBillingZip           = "\(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
        let strBillingAddressLine1Line2 = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? "")" + " " + "\(matchesGeneralInfo.value(forKey: "billingAddress2") ?? "")"
        let strBillingFullAddress   = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? ""), \(matchesGeneralInfo.value(forKey: "billingAddress2") ?? ""), \(matchesGeneralInfo.value(forKey: "billingCity") ?? ""), \(matchesGeneralInfo.value(forKey: "billingState") ?? ""), \(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
        let strBillingCityStateZip  = "\(matchesGeneralInfo.value(forKey: "billingCity") ?? ""), \(matchesGeneralInfo.value(forKey: "billingState") ?? ""), \(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
        let strDateTime = global.strCurrentDate()
        let arrTemp = strDateTime?.split(separator: " ")
        let time = (arrTemp?[1] ?? "") + " " + (arrTemp?[2] ?? "")
        let paramObject: [String: Any] = ["templateKey": self.strTemplateKey,
                                          "date": "",
                                          "role": "\(role)",
                                          "type": "IOS",
                                          "contractorId": self.strLeadId,
                                          "companyKey": self.strCompanyKey,
                                          "showBtnReview": false,
                                          "json": ["[LastFirstName]": "\(strLastName) \(strFirstName)",
                                                   "[FullName]": "\(strFirstName) \(strLastName)",
                                                   "[FirstName]": strFirstName,
                                                   "[LastName]": strLastName,
                                                   "[MiddleName]": strMiddleName,
                                                   "[ServiceAddressLine1]": strSAddress1,
                                                   "[ServiceAddressLine2]": strSAddress2,
                                                   "[ServiceCity]": strSCity,
                                                   "[City]": strSCity,
                                                   "[Block]": "",
                                                   "[District]": "",
                                                   "[ServiceState]": strSState,
                                                   "[ServiceZip]": strSZip,
                                                   "[ServiceAddressLine1Line2]": "\(strSAddress1) \(strSAddress2)",
                                                   "[ServiceFullAddress]": "\(strSAddress1) \(strSAddress2), \(strSCity), \(strSState), \(strSZip)",
                                                   "[ServiceCityStateZip]": "\(strSCity), \(strSState), \(strSZip)",
                                                   "[PhoneNumber]": strPhone,
                                                   "[MobileNumber]": strMobile,
                                                   "[Email]": strEmail,
                                                   "[AlternateEmail]": strSecEmail,
                                                   "[customImage]": self.arrImageDetails,
                                                   "[customGraph]": self.arrGraphDetails,
                                                  
                                                   "[CustomerCompanyName]": strCustomerCompanyName,
                                                   "[AccountName]": strAccountName,
                                                   "[Account#]": strAccountNo,
                                                   "[EmpFullName]": strEmpFullName,
                                                   "[EmpLicenseNo]": strEmpLicenseNo,
                                                  
                                                   "[PackageName]": strPackageName,
                                                   "[InitialAmountWithDiscount]": strInitialAmountWithDiscount,
                                                   "[InitialAmountWithoutDiscount]": strInitialAmountWithoutDiscount,
                                                   "[MaintenanceAmount]": strMaintenanceAmount,
                                                   "[ServiceFrequency]": strServiceFrequency,
                                                   "[BillingFrequency]": strBillingFrequency,
                                                   "[BillingAmount]": strBillingAmount,
                                                  
                                                   "[BillingAddressLine1]": strBillingAddressLine1,
                                                   "[BillingAddressLine2]": strBillingAddressLine2,
                                                   "[BillingCity]": strBillingCity,
                                                   "[BillingState]": strBillingState,
                                                   "[BillingZip]": strBillingZip,
                                                   "[BillingAddressLine1Line2]": strBillingAddressLine1Line2,
                                                   "[BillingFullAddress]": strBillingFullAddress,
                                                   "[BillingCityStateZip]": strBillingCityStateZip,
                                                  
                                                   "[CurrentDate]": global.getCurrentDate(),
                                                   "[CurrentDateTime]": global.strCurrentDate(),
                                                   "[CurrentTime]": time]]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
        if let jsonString = String(data: jsonData!, encoding: .utf8) {
            // print(jsonString)
            //messageHandlers
            let messageHandlers = "buttonClicked"
            let configuration = WKWebViewConfiguration()
            let contentController = WKUserContentController()
            let js = "localStorage.setItem('templateData', JSON.stringify(\(jsonString)));"
            let userScript = WKUserScript(source: js, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: false)
            contentController.addUserScript(userScript)
            configuration.userContentController = contentController
            configuration.userContentController.add(self, name: messageHandlers)
            
            webView = WKWebView(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height), configuration: configuration)
            
            var strFormbuilderUrl = ""
            if MainUrl == "https://pcrs.pestream.com/" {
                strFormbuilderUrl = "https://formbuilder.pestream.com:8443/#/template-preview"
            }else {
                strFormbuilderUrl = "https://formbuilderstaging.pestream.com:8443/#/template-preview"
            }
            webView.load(URLRequest(url: URL(string: strFormbuilderUrl)!))
            self.viewForWeb.addSubview(webView)
            
            webView.topAnchor.constraint(equalTo: self.viewForWeb.topAnchor).isActive = true
            webView.leftAnchor.constraint(equalTo: self.viewForWeb.leftAnchor).isActive = true
            webView.rightAnchor.constraint(equalTo: self.viewForWeb.rightAnchor).isActive = true
            webView.bottomAnchor.constraint(equalTo: self.viewForWeb.bottomAnchor).isActive = true
            webView.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    func ReplaceBlankToSpace(str: String) -> String {
        return str == "" ? " " : str
    }
    
    func loadPdfOverView(strPDF: String) {
        let urlStr = strPDF
        
        let replaced = urlStr.replacingOccurrences(of: "\\", with: "")
        print(replaced)
        let strPdfRelative = replaced.replacingOccurrences(of: "\"", with: "")
        
        let pdfLink = self.strBaseUrl + "/Documents" + strPdfRelative
        print(pdfLink)

        let url = URL(string: pdfLink)
        
        if self.webView != nil {
            if url != nil {
                self.webView.load(URLRequest(url: url!))
            }
        }else {
            webView = WKWebView(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
            
            if url != nil {
                self.webView.load(URLRequest(url: url!))
                self.viewForWeb.addSubview(webView)
                
                webView.topAnchor.constraint(equalTo: self.viewForWeb.topAnchor).isActive = true
                webView.leftAnchor.constraint(equalTo: self.viewForWeb.leftAnchor).isActive = true
                webView.rightAnchor.constraint(equalTo: self.viewForWeb.rightAnchor).isActive = true
                webView.bottomAnchor.constraint(equalTo: self.viewForWeb.bottomAnchor).isActive = true
                webView.translatesAutoresizingMaskIntoConstraints = false
            }
        }
    }
    
    func paymentInitialSetUp() {
        //Payment
        self.lblSubTotal.text        = "\(self.dictPricingInfo["subTotalInitial"] ?? "")"
        self.lblTotal.text          = "\(self.dictPricingInfo["totalPrice"] ?? "")"
        self.lblTaxAmount.text      = "\(self.dictPricingInfo["taxAmount"] ?? "")"
        self.lblBillingAmount.text  = "\(self.dictPricingInfo["billingAmount"] ?? "")"
        self.txtAmount.text         = "\(self.dictPricingInfo["billingAmount"] ?? "")"
        self.strAmount = self.txtAmount.text ?? ""
        

        print(objPaymentInfo)
        self.strPaymentMode = "\(self.objPaymentInfo.value(forKey: "paymentMode") ?? "")"
        
        print(self.selectecPaymentType)
        if self.strPaymentMode == "Cash" {
            self.selectecPaymentType = "Cash"
        }else if self.strPaymentMode == "Check" {
            self.selectecPaymentType = "Check"
        }else if self.strPaymentMode == "CreditCard" {
            self.selectecPaymentType = "Credit Card"
        }else if self.strPaymentMode == "AutoChargeCustomer" {
            self.selectecPaymentType = "Auto Charge Customer"
        }else if self.strPaymentMode == "CollectattimeofScheduling" {
            self.selectecPaymentType = "Collect at time of Scheduling"
        }else if self.strPaymentMode == "Invoice" {
            self.selectecPaymentType = "Invoice"
        }else {
            //self.selectecPaymentType = ""
            //Fetch Payment Mode From Master
            var paymentModeFound = false
            
            let arrPaymentMode = getPaymentModeFromMaster()
            
            for item in arrPaymentMode
            {
                let dict = item as! NSDictionary
                
                if dict["AddLeadProspect"] == nil
                {
                    if "\(dict.value(forKey: "SysName") ?? "")" == strPaymentMode
                    {
                        self.selectecPaymentType = "\(dict.value(forKey: "Title") ?? "")"
                        paymentModeFound = true
                        dictSelectedCustomPaymentMode = dict
                        break
                    }
                }
                    
            }
            if !paymentModeFound
            {
                self.selectecPaymentType = ""
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.refreshPaymentMode(strMode: self.selectecPaymentType)
        })
        
        self.arrOFImagesName.removeAll()
        self.arrOfCheckBackImage.removeAll()

        let checkFrontImagePath = "\(self.objPaymentInfo.value(forKey: "checkFrontImagePath") ?? "")"//self.objPaymentInfo.value(forKey: "checkFrontImagePath") as! String//[matches valueForKey:@"CheckFrontImagePath"];
        if (checkFrontImagePath.count > 0)
        {
            arrOFImagesName.append(checkFrontImagePath)
        }
                
        let checkBackImagePath = "\(self.objPaymentInfo.value(forKey: "checkBackImagePath") ?? "")"//self.objPaymentInfo.value(forKey: "checkBackImagePath") as! String//[matches valueForKey:@"CheckFrontImagePath"];
        if (checkBackImagePath.count > 0)
        {
            arrOfCheckBackImage.append(checkBackImagePath)
        }
        
        self.strChequeNo = "\(self.objPaymentInfo.value(forKey: "checkNo") ?? "")"
        self.strLicenseNo = "\(self.objPaymentInfo.value(forKey: "licenseNo") ?? "")"
       // self.strDate = "\(self.objPaymentInfo.value(forKey: "expirationDate") ?? "")"
        
        if "\(objPaymentInfo.value(forKey: "expirationDate") ?? "")".count > 0
        {
            let str = changeStringDateToGivenFormat(strDate: "\(objPaymentInfo.value(forKey: "expirationDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            self.strDate = str
        }
        
        //Payment mode info
        self.txtCheckNo.text = self.strChequeNo
        self.txtDrivingLicenceNo.text = self.strLicenseNo
        self.btnExpDate.setTitle("Expiration Date: \(self.strDate)", for: .normal)
    }
    
    
    
    @IBAction func historyAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
            self.view.endEditing(true)
            
            self.goToEmailHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
        
            self.goToSetupHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
        
            self.goToServiceNotesHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
        
            self.goToServiceHistory()
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
           // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.up]
        }
        self.present(alert, animated: true)
       
      }
    
    @IBAction func backToScheduleAction(_ sender: Any) {
        

        
        var isAppointmentFound = false
        
        var controllerVC = UIViewController()
        
        do
        {
            sleep(1)
        }
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is AppointmentVC {
                
                isAppointmentFound = true
                controllerVC = aViewController
                //break
                //self.navigationController!.popToViewController(aViewController, animated: false)
            }
        }
        
        if isAppointmentFound
        {
            nsud.setValue(true, forKey: "RefreshAppointmentList")
            nsud.synchronize()
            
            self.navigationController?.popToViewController(controllerVC, animated: false)
            print("Pop to appointment")
            
        }
        else
        {
            
            print("Push to appointment")
            
            let defsAppointemnts = UserDefaults.standard
            let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
            String
            if strAppointmentFlow == "New"
            {
                if DeviceType.IS_IPAD
                {
                    let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                    let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                    "AppointmentVC") as? AppointmentVC
                    if let objAppointmentView = objAppointmentView {
                        navigationController?.pushViewController(objAppointmentView, animated:
                                                                    
                                                                    false)
                    }
                }
                else
                {
                    let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                    let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                    "AppointmentVC") as? AppointmentVC
                    if let objAppointmentView = objAppointmentView {
                        navigationController?.pushViewController(objAppointmentView, animated:
                                                                    
                                                                    false)
                    }
                }
            }
            else
            {
                if DeviceType.IS_IPAD
                {
                    let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                    "AppointmentViewiPad") as? AppointmentViewiPad
                    if let objByProductVC = objByProductVC {
                        navigationController?.pushViewController(objByProductVC, animated: false)
                    }
                }
                else
                {
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                    "AppointmentView") as? AppointmentView
                    if let objByProductVC = objByProductVC {
                        navigationController?.pushViewController(objByProductVC, animated: false)
                    }
                }
            }
        }
        
    }
    
    //MARK: - ------------------ Footer Actions ---------------------
    
    
    func goToEmailHistory()  {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
              let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
              testController?.modalPresentationStyle = .fullScreen
              testController?.strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
              testController?.strCompanyKey = strCompanyKey
              testController?.strUserName = strUserName

              self.present(testController!, animated: false, completion: nil)
    }
    
    func goToServiceHistory()  {
        
        /*let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = "\(dataGeneralInfo.value(forKey: "leadId") ?? "")" as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
        
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
        
        
    }
    func goToServiceNotesHistory()  {
        
        //ServiceNotesHistoryViewController
//        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
//        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
//        testController?.strTypeOfService = "sales";
//        testController?.modalPresentationStyle = .fullScreen
//        self.present(testController!, animated: false, completion: nil)
        
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    @IBAction func action_RecordAudio(_ sender: Any) {
        //yesEditedSomething = true
        
        let isAudioPermission = Global().isAudioPermissionAvailable()
        
        if isAudioPermission
        {
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)

            if !getOpportunityStatus()
            {
                alert.addAction(UIAlertAction(title: "Record", style: .default , handler:{ (UIAlertAction)in
                    let storyboardIpad = UIStoryboard.init(name: "MainiPad", bundle: nil)
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioViewiPad") as? RecordAudioViewiPad
                    testController?.strFromWhere = "SalesInvoice"//flowTypeWdoSalesService
                    testController?.modalPresentationStyle = .fullScreen
                    self.present(testController!, animated: false, completion: nil)
                }))
            }
                
            alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
                let audioName = "\(nsud.value(forKey: "AudioNameService") ?? "")"
                //audioName = self.strAudioName
                
                if audioName.count > 0
                {
                    let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
                    testController!.strAudioName = audioName as! String
                    testController?.modalPresentationStyle = .fullScreen
                    self.present(testController!, animated: false, completion: nil)
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No audio file found", viewcontrol: self)
                }
                
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in }))
            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: { })
        }
        else
        {
            let alertController = UIAlertController (title: Alert, message: alertAudioVideoPermission, preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: "Go-To Settings", style: .default) { (_) -> Void in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            }
            alertController.addAction(settingsAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
        }

    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func customerPresentSwitch(_ sender: UISwitch) {
        print(sender.isOn)
        
        if self.strOverViewPDF != "" {
            if sender.isOn {
                selectedIndex = 1
                
                self.localStorageWebView()//4.
                self.view.bringSubviewToFront(self.viewForWeb)
                cardView.isHidden = true
                cardView_HeightConstraint.constant = 0
            }else {
                selectedIndex = 3
                
                self.startLoading()
                self.loadPdfOverView(strPDF: self.strOverViewPDF)
                cardView.isHidden = false
                cardView_HeightConstraint.constant = DeviceType.IS_IPAD ? 105 : 85
                self.view.bringSubviewToFront(cardView)
            }
            self.collectionViewFB.reloadData()
        }
    }
    
    @IBAction func cancelPaymentAction(_ sender: UIButton) {
        if getOpportunityStatus() {
            self.navigationController?.popViewController(animated: false)
        }else {
            if self.selectedIndex == 2 {
                //Payment Cancel button
                self.selectedIndex = 1
                self.collectionViewFB.reloadData()
                self.cardView.isHidden = true
                self.switchViewHeightConstraint.constant = 40
                self.viewForSwitch.isHidden = false
                self.view.bringSubviewToFront(self.viewForWeb)
                self.localStorageWebView()
                
                
            }else {
                //OverView Cancel button
                if !switchCustomerPresent.isOn {
                    self.switchCustomerPresent.isOn = true
                    self.customerPresentSwitch(self.switchCustomerPresent)
                }else {
                    //cancel from Payment -> Overview
                    self.view.bringSubviewToFront(self.tblViewForPayment)
                    cardView.isHidden = false
                    cardView_HeightConstraint.constant = DeviceType.IS_IPAD ? 105 : 85
                    self.view.bringSubviewToFront(cardView)
                    self.selectedIndex = 2
                }
                self.collectionViewFB.reloadData()
            }
        }
    }
    
    @IBAction func savePaymentContinueAction(_ sender: UIButton) {
        self.strChequeNo  = txtCheckNo.text ?? ""
        self.strLicenseNo = txtDrivingLicenceNo.text ?? ""
        if self.selectedIndex == 2 {
            
            //Payment Continue button
            if self.lblSelectedPaymentMethod.text == "----Select----" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select a payment method.", viewcontrol: self)
            }else {
                if self.strPaymentMode == "CreditCard" || self.strPaymentMode == "Credit Card" {
                    if isElementIntegration == true
                    {
                        self.selectedIndex = 3
                        self.goToCreditCardView()
                    }else {
                        self.paymentConditionMethod()
                    }
                }else {
                    self.paymentConditionMethod()
                }
            }
        }else {
            //self.selectedIndex = 3 := OverView Continue button
            //MARK:- Code Should be here for Continue Button on OverView Tab
            print("OverView")
            self.updateLeadIdDetail()
            
            /*Complition Alert*/
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)
            
            self.goToSendMailViewController()
        }
    }
    
    @IBAction func actionOnImage(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Make Your Selection", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.AddImages, style: .default, handler: { action in
            self.GotoGlobleImageGraphView(strType: "Before")
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.AddDocuments, style: .default, handler: { [self] action in
            let strLeadID =  strLeadId

            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: DeviceType.IS_IPAD ? "UploadDocumentSalesiPad" : "UploadDocumentSales") as! UploadDocumentSales
            vc.strWoId = "\(strLeadID)" as NSString
            self.navigationController?.present(vc, animated: true, completion: nil)

        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
      //      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.down]
        }
        self.present(alert, animated: true)
    }
    
    @IBAction func actionOnGraph(_ sender: Any) {
        
        self.view.endEditing(true)
        nsud.setValue(true, forKey: "firstGraphImage")
        nsud.setValue(false, forKey: "servicegraph")
        GotoGlobleImageGraphView(strType: "Graph")
    }
    
    @IBAction func action_Home(_ sender: Any) {
        self.view.endEditing(true)
        self.goToAppointment()
    }
    func goToAppointment()
    {
           
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            
 
            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    
    @IBAction func actionOnChemicalSensitivityList(_ sender: Any) {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = matchesGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoGlobleImageGraphView(strType : String)  {
        
        let strLeadID =  matchesGeneralInfo.value(forKey: "leadId") ?? ""
        var strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
        var strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
        if(nsud.value(forKey: "backFromInspectionNew") != nil){
            
            if(nsud.value(forKey: "backFromInspectionNew") as! Bool == true){
                strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
                strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
            }
        }
    
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPhone" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        vc.strHeaderTitle = strType as NSString
        vc.strWoId = "\(strLeadID)" as NSString
        vc.strWoLeadId = "\(strLeadID)" as NSString
        vc.strModuleType = "SalesFlow"
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "lost"){
            vc.strWoStatus = "Complete"
        }else{
            vc.strWoStatus = "InComplete"
        }
        vc.strWoStatus = "InComplete"
        self.navigationController?.present(vc, animated: true, completion: nil)

    }
    
    func amountMethod() {
        
        if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
        }else {
            //chkChequeClick = NO;
            self.strLicenseNo = "";
            self.strChequeNo = "";
            self.strDate = "";
            
            /*Final Update*/
            self.updatePaymentInfoCoreData()
        }
    }
    func noAmountMethod() {
        
        self.strLicenseNo = "";
        self.strChequeNo = "";
        self.strDate = "";
        self.strAmount = "0";
        
        /*Final Update*/
        self.updatePaymentInfoCoreData()
    }
    
    func paymentConditionMethod() {
        
        if self.strPaymentMode == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select Payment Type", viewcontrol: self)
        }else if self.strPaymentMode == "Cash"  || self.strPaymentMode == "CollectattimeofScheduling" {
            
            if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
            }else {
                //chkChequeClick = NO;
                self.strLicenseNo = "";
                self.strChequeNo = "";
                self.strDate = "";
                
                /*Final Update*/
                self.updatePaymentInfoCoreData()
            }
            
        }else if self.strPaymentMode == "Check" {
            
            if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
            }else if self.strChequeNo == "" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Check #.", viewcontrol: self)
            }else {
                print(self.strChequeNo)
                print(self.strLicenseNo)
                print(self.strAmount)
                print(self.strDate)
                /*Final Update*/
                self.updatePaymentInfoCoreData()
            }
            
        }else if self.strPaymentMode == "CreditCard" || self.strPaymentMode == "Credit Card" {
            
            if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
            }else {
                //chkChequeClick = NO;
                self.strLicenseNo = "";
                self.strChequeNo = "";
                self.strDate = "";
                
                /*Final Update*/
                self.updatePaymentInfoCoreData()
            }
            
        }else {
            
            if dictSelectedCustomPaymentMode.count > 0 && "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1"
            {
                amountMethod()
            }
            else
            {
                noAmountMethod()
            }
            
//            self.strLicenseNo = "";
//            self.strChequeNo = "";
//            self.strDate = "";
//            self.strAmount = "0";
//            self.updatePaymentInfoCoreData()
        }
        
    }
    //Core Data
    func updatePaymentInfoCoreData() {
        
        print(objPaymentInfo)
        self.objPaymentInfo!.setValue(self.strLeadId, forKey: "leadId")
        self.objPaymentInfo.setValue(self.strPaymentMode, forKey: "paymentMode")
        self.objPaymentInfo.setValue(self.strAmount, forKey: "amount")
        self.objPaymentInfo.setValue(self.strChequeNo, forKey: "checkNo")
        self.objPaymentInfo.setValue(self.strLicenseNo, forKey: "licenseNo")
        self.objPaymentInfo.setValue(self.strDate, forKey: "expirationDate")
        self.objPaymentInfo.setValue("", forKey: "customerSignature")
        self.objPaymentInfo.setValue("", forKey: "salesSignature")
    /*    if self.isCustomerNotPresent {
            self.objPaymentInfo.setValue("", forKey: "customerSignature")
        }else {
            self.objPaymentInfo.setValue(self.strCustomerSignature, forKey: "customerSignature")
        }
        self.objPaymentInfo.setValue(self.strSalesSignature, forKey: "salesSignature")
*/
        let defaults = UserDefaults.standard
        var strSignUrl: String = defaults.value(forKey: "ServiceTechSignPath") as? String ?? ""
        let isPreSetSign: Bool = defaults.bool(forKey: "isPreSetSignSales")
        if isPreSetSign && strSignUrl != "" {
            strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
            
            var result = ""
            let equalRange = NSString(string: strSignUrl).range(of: "Documents", options: .backwards)

            if equalRange.location != NSNotFound {
                print(equalRange.location)
                print(equalRange.length)
                let fromR = equalRange.location + equalRange.length
                result = strSignUrl.substring(from: fromR)
            }else {
                result = strSignUrl
            }
            print(result)
            self.objPaymentInfo.setValue(result, forKey: "salesSignature")

        }
  
        self.objPaymentInfo.setValue(global.modifyDate(), forKey: "modifiedDate")
      //  self.objPaymentInfo.setValue(self.strCustomerNote, forKey: "specialInstructions")
        self.objPaymentInfo.setValue(self.strCompanyKey, forKey: "companyKey")
        self.objPaymentInfo.setValue(self.strUserName, forKey: "userName")

        let defs = UserDefaults.standard
        let arrImageToSend: NSMutableArray = NSMutableArray()
        var arrTemp: NSMutableArray = NSMutableArray()
        
        if let val = defs.value(forKey: "arrImageToSend") {
            arrTemp = NSMutableArray(object: val)
        }
        for i in 0..<arrTemp.count {
            arrImageToSend.add(arrTemp[i])
        }
        

        print(arrImageToSend)
        
        if self.strPaymentMode == "Check" {
            
            if arrOFImagesName.count > 0 {
                self.objPaymentInfo.setValue(self.arrOFImagesName[0], forKey: "checkFrontImagePath")
            }else {
                self.objPaymentInfo.setValue("", forKey: "checkFrontImagePath")
            }
            
            if arrOfCheckBackImage.count > 0 {
                self.objPaymentInfo.setValue(self.arrOfCheckBackImage[0], forKey: "checkBackImagePath")
            }else {
                self.objPaymentInfo.setValue("", forKey: "checkBackImagePath")
            }
            
        }
        defs.setValue(arrImageToSend, forKey: "arrImageToSend")
        defs.synchronize()
        
//        self.objPaymentInfo.setValue("", forKey: "leadPaymentDetailId")
//        self.objPaymentInfo.setValue("", forKey: "agreement")
//        self.objPaymentInfo.setValue("", forKey: "proposal")
//        self.objPaymentInfo.setValue("", forKey: "createdBy")
//        self.objPaymentInfo.setValue("", forKey: "createdDate")
//        self.objPaymentInfo.setValue("", forKey: "modifiedBy")
//        self.objPaymentInfo.setValue("", forKey: "inspection")
        
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        
        if self.strOverViewPDF != "" {
            self.selectedIndex = 3
            self.goTOOverView()
        }
        self.fetchPaymentInfo()
    }
    
    func getPaymentModeFromMaster() -> NSMutableArray {
        
        var arrPaymentMaster = NSArray()
        
        var  dictLoginData = NSDictionary()
        
        dictLoginData =  nsud.value(forKey: "LoginDetails") as! NSDictionary

        let isSalesPaymentTypeDynamic = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsSalesPaymentTypeDynamic") as! Bool
        //isSalesPaymentTypeDynamic = true
        if isSalesPaymentTypeDynamic
        {
            if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
            {
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "PaymentTypeObj") is NSArray) {
                     
                    arrPaymentMaster = dictMaster.value(forKey: "PaymentTypeObj") as! NSArray
                }
            }
        }

        let arrPaymentNew = NSMutableArray()
        arrPaymentNew.add(["AddLeadProspect": "Cash"])
        arrPaymentNew.add(["AddLeadProspect": "Check"])
        arrPaymentNew.add(["AddLeadProspect": "Credit Card"])
        arrPaymentNew.add(["AddLeadProspect": "Auto Charge Customer"])
        arrPaymentNew.add(["AddLeadProspect": "Collect at time of Scheduling"])
        arrPaymentNew.add(["AddLeadProspect": "Invoice"])
        
        for item in arrPaymentMaster
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1" || ("\(dict.value(forKey: "IsActive") ?? "")" == "0" && "\(dict.value(forKey: "SysName")!)" == strPaymentMode)
            {
                arrPaymentNew.add(dict)
            }
        }
        print(arrPaymentNew)
        
        return arrPaymentNew
    }
    
    @IBAction func paymentMethodDropDownAction(_ sender: Any) {
        self.view.endEditing(true)
//        let arrPayment = [["AddLeadProspect": "Cash"], ["AddLeadProspect": "Check"], ["AddLeadProspect": "Credit Card"], ["AddLeadProspect": "Auto Charge Customer"], ["AddLeadProspect": "Collect at time of Scheduling"], ["AddLeadProspect": "Invoice"]]
//        openTableViewPopUp(tag: 3335, ary: (arrPayment as NSArray).mutableCopy() as! NSMutableArray)
        
        openTableViewPopUp(tag: 3335, ary: getPaymentModeFromMaster())
    }
    
    @IBAction func electronicAuthorisationFormACtion(_ sender: Any) {
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMasters.value(forKey: "ElectronicAuthorizationFormMaster") is NSDictionary)
        {
            
            if DeviceType.IS_IPAD {
                let storyboard = UIStoryboard.init(name: "ElectronicAuthorization_iPad", bundle: nil)
                let testController = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPad") as! ElectronicAuthorization_iPad
                
                if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
                {
                    if(isInternetAvailable() == false)
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(testController, animated: false)
                    }
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
            }else {
                //iPhone
                let storyboard = UIStoryboard.init(name: "ElectronicAuthorization_iPhone", bundle: nil)
                let testController = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPhone") as! ElectronicAuthorization_iPhone

                if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
                {
                    if(isInternetAvailable() == false)
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(testController, animated: false)
                    }
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
            }
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
        }
    }
    
    @IBAction func selectExpirationDateAction(_ sender: Any) {
        print("ExpDate")
        
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
       // let fromDate = formatter.date(from:((tf_Date.text?.count)! > 0 ? tf_Date.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: Date(),Picker: datePicker, view: self.view ,strTitle : "Select Service Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            self.strDate = formatter.string(from: datePicker.date)
            print(self.strDate)
            self.btnExpDate.setTitle("Expiration Date: \(self.strDate)", for: .normal)

        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        if let date = sender?.date {
            self.strDate = dateFormatter.string(from: date)
            print("Picked the date \(self.strDate)")
        }
        btnExpDate.setTitle("Expiration Date: \(self.strDate)", for: .normal)
    }
    
    func fetchPaymentInfo() {
                
        let arrPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))

        if(arrPaymentInfo.count > 0)
        {
            objPaymentInfo = (arrPaymentInfo.firstObject as! NSManagedObject)
            print(objPaymentInfo)
            let strCustomerNote = "\(objPaymentInfo!.value(forKey: "specialInstructions")!)"
            print(strCustomerNote)
        }else {
        }
    }
    
    @IBAction func backImageAction(_ sender: Any) {
        self.isFrontImage = false

        let alert = UIAlertController(title: "Make your selection", message: nil, preferredStyle: .actionSheet)
        
            alert.addAction(UIAlertAction(title: "Preview Image", style: .default, handler: { _ in
                self.openPreviewImage()
            }))
        
            alert.addAction(UIAlertAction(title: "Capture New", style: .default, handler: { _ in
                if self.arrOfCheckBackImage.count > 0 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Check back image already exist.", viewcontrol: self)

                }else {
                    self.openCamera(sourceDevice: .rear)
                }
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                if self.arrOfCheckBackImage.count > 0 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Check back image already exist.", viewcontrol: self)

                }else {
                    self.openGallary()
                }
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as! UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func fronImageAction(_ sender: Any) {
        self.isFrontImage = true
        
        let alert = UIAlertController(title: "Make your selection", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Preview Image", style: .default, handler: { _ in
                self.openPreviewImage()
            }))
    
            alert.addAction(UIAlertAction(title: "Capture New", style: .default, handler: { _ in
                if self.arrOFImagesName.count > 0 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Check front image already exist.", viewcontrol: self)

                }else {
                    self.openCamera(sourceDevice: .front)
                }
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                if self.arrOFImagesName.count > 0 {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Check front image already exist.", viewcontrol: self)

                }else {
                    self.openGallary()
                }
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as! UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        //MARK: - Add Card Action
        print("Add Card")
        self.goToPestPac_PaymentIntegration()
    }
    
    func openPreviewImage()
    {
        if self.isFrontImage {
            if self.arrOFImagesName.count == 0 {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No Image Available.", viewcontrol: self)
            }else {
                self.goToImagePreviewCheckFrontImage()
            }
        }else {
            if self.arrOfCheckBackImage.count == 0 {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No Image Available.", viewcontrol: self)
            }else {
                self.goToImagePreviewCheckBackImage()
            }
        }
    }
    
    func openCamera(sourceDevice: UIImagePickerController.CameraDevice)
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imgPicker = UIImagePickerController()
            imgPicker.allowsEditing = true
            imgPicker.delegate = self
            imgPicker.sourceType = .camera
            imgPicker.cameraDevice = sourceDevice
            present(imgPicker, animated: true)
        }else {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Camera is not available.", viewcontrol: self)
        }
    }

    func openGallary()
    {
        let imgPicker = UIImagePickerController()
        imgPicker.allowsEditing = true
        imgPicker.delegate = self
        imgPicker.sourceType = .photoLibrary
        present(imgPicker, animated: true)
    }
    
    func goToImagePreviewCheckFrontImage() {
        let defs = UserDefaults.standard
        defs.set(true, forKey: "forCheckFrontImageDelete")
        defs.set(false, forKey: "forCheckBackImageDelete")
        defs.synchronize()

        let mainStoryboard = UIStoryboard(
            name: "SalesMain",
            bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ImagePreviewSalesAuto") as? ImagePreviewSalesAuto
        objByProductVC?.arrOfImages = ((self.arrOFImagesName as NSArray).mutableCopy() as! NSMutableArray)//(self.arrOFImagesName as! NSMutableArray)
        objByProductVC?.indexOfImage = "0"
        
        if strLeadStatusGlobal.caseInsensitiveCompare("Complete") == .orderedSame && strStageSysName.caseInsensitiveCompare("Won") == .orderedSame
        {
            objByProductVC?.statusOfWorkOrder = "Complete"
        }
        else
        {
            objByProductVC?.statusOfWorkOrder = "InComplete"
        }
        
        objByProductVC?.strForCheckImage = "CheckImages"
        if let objByProductVC = objByProductVC {
            navigationController?.present(objByProductVC, animated: true)
        }
    }
    
    func goToImagePreviewCheckBackImage() {
        let defs = UserDefaults.standard
        defs.set(true, forKey: "forCheckBackImageDelete")
        defs.set(false, forKey: "forCheckFrontImageDelete")
        defs.synchronize()

        let mainStoryboard = UIStoryboard(
            name: "SalesMain",
            bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ImagePreviewSalesAuto") as? ImagePreviewSalesAuto
        objByProductVC?.arrOfImages = ((self.arrOfCheckBackImage as NSArray).mutableCopy() as! NSMutableArray)//arrOfCheckBackImage
        objByProductVC?.indexOfImage = "0"
        if strLeadStatusGlobal.caseInsensitiveCompare("Complete") == .orderedSame && strStageSysName.caseInsensitiveCompare("Won") == .orderedSame
        {
            objByProductVC?.statusOfWorkOrder = "Complete"
        }
        else
        {
            objByProductVC?.statusOfWorkOrder = "InComplete"
        }
        objByProductVC?.strForCheckImage = "CheckImages"

        if let objByProductVC = objByProductVC {
            navigationController?.present(objByProductVC, animated: true)
        }
    }
    
    @IBAction func textSwitch(_ sender: UISwitch) {
        if sender.isOn {
            strText = "true"
        }else {
            strText = "false"
        }
    }
    
    @IBAction func phoneSwitch(_ sender: UISwitch) {
        if sender.isOn {
            strPhone = "true"
        }else {
            strPhone = "false"
        }
    }
    
    @IBAction func emailSwitch(_ sender: UISwitch) {
        if sender.isOn {
            strEmail = "true"
        }else {
            strEmail = "false"
        }
    }
    
    
    //MARK: - Go To SendMailViewController
    func goToSendMailViewController() {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
//
//            let vc = storyboardNewSalesSendMail.instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
//
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
        
        let alert = UIAlertController(title: Alert, message: "Appointment completed", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in

            let vc = storyboardNewSalesSendMail.instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
            self.navigationController?.pushViewController(vc, animated: false)

        }))

        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
        
    }
    
    
    func updateLeadIdDetail() {
        let context = getContext()
        
        if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card" {
            if !self.isElementIntegration {
                self.matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            }
        }else {
            self.matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
        }
        
//        if self.strGlobalAudio.count>0 {
//            matchesGeneralInfo.setValue(self.strGlobalAudio, forKey: "audioFilePath")
//        }else {
//            matchesGeneralInfo.setValue("", forKey: "audioFilePath")
//        }
        
        matchesGeneralInfo.setValue("true", forKey: "iAgreeTerms")

        let str = self.matchesGeneralInfo.value(forKey: "isCustomerNotPresent")
        
        if !switchCustomerPresent.isOn {
            self.matchesGeneralInfo.setValue("true", forKey: "isCustomerNotPresent")
        }else {
            self.matchesGeneralInfo.setValue("false", forKey: "isCustomerNotPresent")
        }
        
//        if self.customerSig == nil {
//            matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
//            matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
//        }else {
        matchesGeneralInfo.setValue("true", forKey: "isAgreementSigned")
        matchesGeneralInfo.setValue("true", forKey: "isAgreementGenerated")
//        }
        
//        if self.techSig == nil {
//            matchesGeneralInfo.setValue("false", forKey: "isEmployeePresetSignature")
//        }else {
//            matchesGeneralInfo.setValue("true", forKey: "isEmployeePresetSignature")
//        }
        
        self.matchesGeneralInfo.setValue("yes", forKey: "zSync")

        print(self.dictPricingInfo)
        
        let strFinalBilledAmount = (self.dictPricingInfo["subTotalInitial"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        var strFinalCollectedAmount = strAmount
        let strFinalDiscount = (self.dictPricingInfo["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strFinalSubTotal = (self.dictPricingInfo["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

        if strPaymentMode.caseInsensitiveCompare("Cash") == .orderedSame || strPaymentMode.caseInsensitiveCompare("Check") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CreditCard") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CollectattimeofScheduling") == .orderedSame {
            strFinalCollectedAmount = strAmount
        }else {
            
            if dictSelectedCustomPaymentMode.count > 0 && "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1"
            {
                strFinalCollectedAmount = strAmount
            }
            else
            {
                strFinalCollectedAmount = "0"
            }
        }
        
        self.matchesGeneralInfo.setValue(strFinalBilledAmount, forKey: "billedAmount")
        self.matchesGeneralInfo.setValue(strFinalDiscount, forKey: "couponDiscount")
        self.matchesGeneralInfo.setValue(strFinalCollectedAmount, forKey: "collectedAmount")
        self.matchesGeneralInfo.setValue(strFinalSubTotal, forKey: "subTotalAmount")
     
        print(self.strTemplateKey)
        print(self.strOverViewPDF)
        self.matchesGeneralInfo.setValue(self.strTemplateKey, forKey: "templateKey")
        self.matchesGeneralInfo.setValue(self.strOverViewPDF, forKey: "pdfPath")

//        let strTaxAmt = (self.dictPricingInfo?["taxAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
//        let strTaxMaintAmt = (self.dictPricingInfo?["taxAmountMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
//        let strDueMaintAmt = (self.dictPricingInfo?["totalDueAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

//        matchesGeneralInfo.setValue(strTaxAmt, forKey: "taxAmount")
        self.matchesGeneralInfo.setValue(strFinalBilledAmount, forKey: "totalPrice")
//        matchesGeneralInfo.setValue(strTaxMaintAmt, forKey: "taxableMaintAmount")
//        matchesGeneralInfo.setValue(strDueMaintAmt, forKey: "taxMaintAmount")
        
        self.matchesGeneralInfo.setValue("Won", forKey: "stageSysName")

//        if isSendProposal {
//            matchesGeneralInfo.setValue("Open", forKey: "statusSysName")
//            matchesGeneralInfo.setValue("Proposed", forKey: "stageSysName")
//            matchesGeneralInfo.setValue("true", forKey: "isProposalFromMobile")
//
//        }

        if !switchCustomerPresent.isOn {
            self.matchesGeneralInfo.setValue("CompletePending", forKey: "stageSysName")
            if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card"{
                if !self.isElementIntegration {
                    self.matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                }
            }
        }else {
            self.matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
        }
//        matchesGeneralInfo.setValue(self.strMonthOfService, forKey: "strPreferredMonth")
//        matchesGeneralInfo.setValue(self.strProposalNote, forKey: "proposalNotes")

        if arrTerms.count>0{
            self.matchesGeneralInfo.setValue(arrTerms[0], forKey: "leadGeneralTermsConditions")
        }
        
//        matchesGeneralInfo.setValue(self.strInternalNote, forKey: "notes")
        if !switchCustomerPresent.isOn {
            self.matchesGeneralInfo.setValue("true", forKey: "isSelectionAllowedForCustomer")
        }else {
            self.matchesGeneralInfo.setValue("false", forKey: "isSelectionAllowedForCustomer")
        }
        self.matchesGeneralInfo.setValue("true", forKey: "isMailSend")
        
        self.matchesGeneralInfo.setValue(strText, forKey: "smsReminders")
        self.matchesGeneralInfo.setValue(strPhone, forKey: "phoneReminders")
        self.matchesGeneralInfo.setValue(strEmail, forKey: "emailReminders")
        
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        let defaults = UserDefaults.standard
        defaults.setValue(matchesGeneralInfo.value(forKey: "statusSysName"), forKey: "leadStatusSales")
        defaults.setValue(matchesGeneralInfo.value(forKey: "stageSysName"), forKey: "stageSysNameSales")
        defaults.synchronize()
        self.getLeadDetails()
    }
    
    func getLeadDetails() {
//        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
//
//        print(arrayLeadDetail)
        self.matchesGeneralInfo = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
        print(self.matchesGeneralInfo)
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
       // vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }

    // MARK: ------------- Pestpac Integration -------------------
    
    

    func showCreditCardDetail() -> String {
        // if billToLocationId.count > 0 && billToLocationId != 0 && paymentMode = "CreditCard"
        // show add card button else hide
        
        let billToId = "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")"//"1693164"
        var strTypeNo = "\(matchesGeneralInfo.value(forKey: "creditCardType") ?? "")"//"Discover" //Nedd to add by back end - creditCardType
        let creditCardNumber = "\(matchesGeneralInfo.value(forKey: "cardAccountNumber") ?? "")"//"*************7890" //Nedd to add by back end - creditCardNo
        
        
        
        if creditCardNumber.count >= 4  {
            let last4 = creditCardNumber.suffix(4)
            strTypeNo = strTypeNo + "  \(last4)"
        }
        // For Local Check.
        if(nsud.value(forKey: billToId) != nil){
            
            if nsud.value(forKey: billToId) is NSDictionary {
                
                let dictCreditCardInfo = nsud.value(forKey: billToId) as! NSDictionary
                let accountNumber = String("\(dictCreditCardInfo.value(forKey: "accountNumber") ?? "")".suffix(4))
                strTypeNo = "\(dictCreditCardInfo.value(forKey: "creditCardType") ?? "") " + accountNumber
            }
        }
        
        print(strTypeNo) // Show this value with Add Cart button If strtypeno.count > 0 than only show card value lable
        return strTypeNo
    }
    
    func goToPestPac_PaymentIntegration() {
        
        self.view.endEditing(true)
        
        if isInternetAvailable() {
            
            let firstNameL = "\(matchesGeneralInfo.value(forKey: "billingFirstName") ?? "")"
            let lastNameL = "\(matchesGeneralInfo.value(forKey: "billingLastName") ?? "")"
            let billingAddress1L = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? "")"
            let billingZipcodeL = "\(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
            let billToLocationIdL = "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")"
            let webLeadIdL = strLeadId
            let contactNameToCheck = "\(firstNameL)" + "\(lastNameL)"

            let contactName = "\(firstNameL)" + " " + "\(lastNameL)"
            
            let ammountL = Double(strAmount) ?? 0.0
                        
            if contactNameToCheck.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing contact name.", viewcontrol: self)
                
            }
            else if contactName.count > 30 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Billing contact name must be less than 30 characters", viewcontrol: self)
                
            }
            else if billingAddress1L.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing address.", viewcontrol: self)
                
            }else if billingZipcodeL.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing zipcode.", viewcontrol: self)
                
            }else if billToLocationIdL.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billto location id.", viewcontrol: self)
                
            }else if webLeadIdL.count == 0 || webLeadIdL == "0" {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Lead does not exist.", viewcontrol: self)
                
            }else if ammountL <= 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide amount to charge.", viewcontrol: self)
                
            }else{
                
                let dictPaymentDetails = NSMutableDictionary()

                dictPaymentDetails.setValue(webLeadIdL, forKey: "leadId")
                dictPaymentDetails.setValue(billToLocationIdL, forKey: "billToId")
                dictPaymentDetails.setValue("\(ammountL)", forKey: "amount")
                dictPaymentDetails.setValue(contactName, forKey: "nameOnCard")
                dictPaymentDetails.setValue(billingAddress1L, forKey: "billingAddress")
                dictPaymentDetails.setValue(billingZipcodeL, forKey: "billingPostalCode")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestPacPayment" : "PestPacPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "PestPacPaymentVC") as? PestPacPaymentVC
                vc?.dictPaymentDetails = dictPaymentDetails
                self.navigationController?.present(vc!, animated: false, completion: nil)

            }
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }
        
        /*if isInternetAvailable() {
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterDashboardNew_iPhoneVC") as? FilterDashboardNew_iPhoneVC
            vc!.delegate = self
            vc!.strViewComeFrom = "Dashboard"
            self.navigationController?.present(vc!, animated: true)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }*/
        
    }
}

//MARK:- Camera Delegate
//MARK:-
extension FormBuilderViewController:UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        
        let dateFormatterD = DateFormatter()
        dateFormatterD.dateFormat = "MMddyyyy"
        let strDate = dateFormatterD.string(from: Date())
        
        let dateFormatterT = DateFormatter()
        dateFormatterT.dateFormat = "HHmmss"
        let strTime = dateFormatterT.string(from: Date())

        let strImageNamess = "CheckImg\(strDate)\(strTime).jpg"
        
        if self.isFrontImage {
           self.checkFrontImg = self.resizeImage(image: image, imgName: strImageNamess)
            self.arrOFImagesName.append(strImageNamess)
        }else {
            self.checkBackImg = self.resizeImage(image: image, imgName: strImageNamess)
            self.arrOfCheckBackImage.append(strImageNamess)
        }
        // print out the image size as a test
        print(image.size)
    }
    
    func resizeImage(image: UIImage, imgName: String) -> UIImage {
        
        if let imageData = image.jpeg(.lowest) {
            let filename = getDocumentsDirectory().appendingPathComponent(imgName)
            do {
                try imageData.write(to: filename, options: [])
            } catch {
                // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
            }
            
            return UIImage.init(data: imageData)!
        }else {
            return UIImage()
        }
    }
}

//MARK:- WK WebView Delegate
//MARK:-
extension FormBuilderViewController:WKScriptMessageHandler, WKUIDelegate {
            
    func userContentController(
        _ userContentController:
            WKUserContentController,
        didReceive message: WKScriptMessage) {
        
        if message.name == "buttonClicked",
           let dict = message.body as? NSDictionary {
            
            showLoader()
            //Get PDF from Api Calling
            self.presenter.requestToGetPdf(strbase64: dict["resPdfBase64"] as! String, contractorId: "\(self.role)", accountNo: self.strAccountNo, isRelativePath: true)
            
            
            self.selectedIndex = self.selectedIndex + 1
            self.role = 2
          //  self.saveBase64StringToPDF(base64: dict["resPdfBase64"] as! String)
            if self.selectedIndex == 1 {
                self.switchViewHeightConstraint.constant = 40
                self.viewForSwitch.isHidden = false
                self.collectionViewFB.reloadData()

                cardView.isHidden = true
                cardView_HeightConstraint.constant = 0
                self.localStorageWebView()//4.

            }else {
                self.switchViewHeightConstraint.constant = 0
                self.viewForSwitch.isHidden = true
                self.collectionViewFB.reloadData()
                self.webView.load(URLRequest(url: URL(string:"about:blank")!))

                self.view.bringSubviewToFront(self.tblViewForPayment)
                cardView.isHidden = false
                cardView_HeightConstraint.constant = DeviceType.IS_IPAD ? 105 : 85
                self.view.bringSubviewToFront(cardView)
            }
        }
    }
}

//MARK:- UICollectionView For Header buttons
//MARK:-
extension FormBuilderViewController : UICollectionViewDelegate , UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! ButtonCollectionCell

        cell.lblTitle.text = arrItem[indexPath.row]
        cell.imgIcon.image = UIImage.init(named: arrImages[indexPath.row])
        
        if selectedIndex == indexPath.row {
            cell.viewBg.backgroundColor = .black
            cell.lblTitle.textColor = .white
            cell.imgIcon.tintColor = .white
        }else if selectedIndex > indexPath.row {
            cell.viewBg.backgroundColor = .appThemeColor
            cell.lblTitle.textColor = .white
            cell.imgIcon.tintColor = .white
        }else{
            cell.viewBg.backgroundColor = .white
            cell.lblTitle.textColor = .black
            cell.imgIcon.tintColor = .black

        }

        if !switchCustomerPresent.isOn {
            if indexPath.row == 1 || indexPath.row == 2 {
                cell.viewBg.backgroundColor = .white
                cell.lblTitle.textColor = .lightGray
                cell.imgIcon.tintColor = .lightGray
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension FormBuilderViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //let width = collectionViewOutlet.frame.width//(self.view.frame.size.width ) / 5
        return CGSize(width: collectionViewFB.frame.width/4 - 2, height: collectionViewFB.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset) //.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
}

//MARK: Button Collection Cell
//MARK:-
class ButtonCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    //MARK: Custom Methods
    func addSpacesBetweenCharacters(lable: UILabel) {
        //Logo Name
        lable.attributedText = NSAttributedString(string: lable.text ?? "", attributes:[ NSAttributedString.Key.kern: 5])

    }
}


//MARK:- API Response
//MARK:-
extension FormBuilderViewController: FormBuilderDelegate {
    func showLoader() {
        
    }
    
    func hideLoader() {
        
    }
    
    func showAlert(message: String?) {
        
    }
    
    func sucessfullyGetHTML(_ output: String) {
        print(output)
        
//        if output != "" {
//            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: { [self] in
//                //webView.loadHTMLString(output, baseURL: nil)
//            })
//        }
    }
    
    func sucessfullyGetPDF(_ output: String) {
        print(output)
     //   let strUrl = output.replacingOccurrences(of: "\\", with: "")

        let replaced = output.replacingOccurrences(of: "\\", with: "")
        print(replaced)
        let strPdfRelative = replaced.replacingOccurrences(of: "\"", with: "")
        print(strPdfRelative)

        
        self.strOverViewPDF = strPdfRelative//strUrl
        if output != "" {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: { [self] in
                self.switchCustomerPresent.isEnabled = true
            })
        }
    }
}

// MARK: - Selection CustomTableView
// MARK: -

extension FormBuilderViewController: CustomTableView {
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        print(dictData)
        
        if tag == 3335 {
            //self.selectecPaymentType = dictData.value(forKey: "AddLeadProspect") as! String
            //print(self.selectecPaymentType)
            
            if dictData["AddLeadProspect"] != nil
            {
                self.selectecPaymentType = dictData.value(forKey: "AddLeadProspect") as! String
                print(self.selectecPaymentType)
                if self.selectecPaymentType == "Cash" {
                    self.strPaymentMode = "Cash"
                }else if self.selectecPaymentType == "Check" {
                    self.strPaymentMode = "Check"
                }else if self.selectecPaymentType == "Credit Card" {
                    self.strPaymentMode = "CreditCard"
                }else if self.selectecPaymentType == "Auto Charge Customer" {
                    self.strPaymentMode = "AutoChargeCustomer"
                }else if self.selectecPaymentType == "Collect at time of Scheduling" {
                    self.strPaymentMode = "CollectattimeofScheduling"
                }else if self.selectecPaymentType == "Invoice" {
                    self.strPaymentMode = "Invoice"
                }else {
                    self.strPaymentMode = ""
                }
            }
            else
            {
                print("\(dictData)")
                dictSelectedCustomPaymentMode = dictData
                self.selectecPaymentType = dictData.value(forKey: "Title") as! String
                self.strPaymentMode = "\(dictData.value(forKey: "SysName") ?? "")"
            }
            self.refreshPaymentMode(strMode: self.selectecPaymentType)
        }else {
            
        }
        
    }
    
    func didSelect(aryData: NSMutableArray, tag: Int) {
        print(aryData, tag)
    }
    
    func refreshPaymentMode(strMode: String) {
        self.lblSelectedPaymentMethod.text = strMode
        amountViewHightConstraint.constant = 0
        amountView.isHidden = true
        checkView.isHidden = true
        licenceView.isHidden = true
        buttonsView.isHidden = true
        self.viewForCardType.isHidden = true
        self.cardTypeHeightConstraint.constant = 0
        
        print(self.selectecPaymentType)
        if strMode == "Cash" {
            self.strPaymentMode = "Cash"
            amountView.isHidden = false
            amountViewHightConstraint.constant = 65
        }else if strMode == "Check" {
            self.strPaymentMode = "Check"
            amountViewHightConstraint.constant = 350
            
            amountView.isHidden = false
            checkView.isHidden = false
            licenceView.isHidden = false
            buttonsView.isHidden = false
            
        }else if strMode == "Credit Card" {
            self.strPaymentMode = "CreditCard"
            amountView.isHidden = false
            amountViewHightConstraint.constant = 65
            
//            if "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")".count > 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != nil && isPestPacIntegartion()
//            {
//                self.viewForCardType.isHidden = false
//                self.cardTypeHeightConstraint.constant = 50
//                self.lblCardType.text = self.showCreditCardDetail()
//            }
//            else
//            {
//                self.viewForCardType.isHidden = true
//                self.cardTypeHeightConstraint.constant = 0
//            }
            
        }else if strMode == "Auto Charge Customer" {
            self.strPaymentMode = "AutoChargeCustomer"
            amountViewHightConstraint.constant = 0
        }else if strMode == "Collect at time of Scheduling" {
            self.strPaymentMode = "CollectattimeofScheduling"
            amountView.isHidden = false
            amountViewHightConstraint.constant = 65
        }else if strMode == "Invoice" {
            self.strPaymentMode = "Invoice"
            amountViewHightConstraint.constant = 0
        }else {
            self.strPaymentMode = ""
            amountViewHightConstraint.constant = 0
            
            if dictSelectedCustomPaymentMode.count > 0
            {
                if "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1" || "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                {
                    amountViewHightConstraint.constant = 65
                    amountView.isHidden = false
                    self.selectecPaymentType = dictSelectedCustomPaymentMode.value(forKey: "Title") as! String
                    self.strPaymentMode = "\(dictSelectedCustomPaymentMode.value(forKey: "SysName") ?? "")"
                }
                if "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "0" || "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")".caseInsensitiveCompare("false") == .orderedSame
                {
                    amountViewHightConstraint.constant = 0
                    self.selectecPaymentType = dictSelectedCustomPaymentMode.value(forKey: "Title") as! String
                    self.strPaymentMode = "\(dictSelectedCustomPaymentMode.value(forKey: "SysName") ?? "")"
                }
            }
            

        }
        
        if strPaymentMode != ""
        {
            if "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")".count > 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != nil && isPestPacIntegartion()
            {
                self.viewForCardType.isHidden = false
                self.cardTypeHeightConstraint.constant = 50
                self.lblCardType.text = self.showCreditCardDetail()
            }
            else
            {
                self.viewForCardType.isHidden = true
                self.cardTypeHeightConstraint.constant = 0
            }
        }
        else
        {
            self.viewForCardType.isHidden = true
            self.cardTypeHeightConstraint.constant = 0
        }
    }
}


extension NSManagedObject {
    func toDict() -> [String:Any] {
        let keys = Array(entity.attributesByName.keys)
        return dictionaryWithValues(forKeys:keys)
    }
}
