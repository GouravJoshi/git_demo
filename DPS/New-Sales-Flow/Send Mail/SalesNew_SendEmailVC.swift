//
//  SalesNew_SendEmailVC.swift
//  DPS
//
//  Created by NavinPatidar on 6/28/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import MessageUI
import CoreData


enum Enum_SalesNewSendMailFrom {
        case Email
        case Text
        case Service
        case Proposal
}

class SalesNew_SendEmailVC: UIViewController {
    
    //MARK:-------------------
    
    //MARK: - Variable
    @objc var matchesGeneralInfo = NSManagedObject()
    
    var arySendMailList = NSMutableArray() , arySendTextList = NSMutableArray(), arySendDocumentList = NSMutableArray()
    var arySalesProposalFollowUpList = NSArray() , arySalesServiceFollowUpList = NSArray()
    var arySalesProposalServices = NSArray()
    
    var strSendMailStatusFrom = Enum_SalesNewSendMailFrom.Email
    
    var isCustomerPresent = false //strForSendProposal = "" ,
    
    @objc var strForSendProposal = ""
    
    
    var dictFinal = NSMutableDictionary()
    var dictForFollowUp = NSMutableDictionary()

    var aryAudioList = NSMutableArray(), aryImagesList = NSMutableArray(), aryXMLList = NSMutableArray(), aryTargetImagesList = NSMutableArray(), aryCRMImagesList = NSMutableArray()
    //Nilind
    
    var dictLoginData = NSDictionary()
    
    var arrSelectedDocuments = NSMutableArray()
    var arrAllOtherDocuments = NSArray()

    
    var strLeadId = "", strEmpName = "", strUserName = "", strCompanyKey = "", strBranchSysName = "", strCompanyId = "", strFlowType = "", strGlobalStatusSysName = "" , strGlobalStageSysName = "", strSurveyStatus = "", strClickHereUrl = "", strCompanyName = "", strEmpEmail = "", strEmpPhone = "", strStatusOfInitialSetup = "", strEmpId = "", strFrom = ""
    
    var isProposalFoolowUp = false
    var isServiceFollowUp = false
    var isSendProposal = false
    var serviceFollowDays = 0
    var proposalFollowDays = 0
    var isFromServiceFollowUpDataBase = false
    var isFromProposalFolloupDataBase = false
    var isEditServiceFollowUp = false
    var isEditProposalFollowUp = false
    var isResend = false
    var isCustomerNotPresent = false
    var isGGQIntegration = false
    var isSurveyCompleted = false
    var isShowTextButton = false
    var isSyncApiCalledForText = false

    var loader = UIAlertController()
    var dispatchGroupSales = DispatchGroup()
    var matchesServiceFollowUp = NSManagedObject()
    var matchesProposalFollowUp = NSManagedObject()
    var isEditSchedule = false
    
    //Preview
    var strStatusForPreview = ""
    var strStageForPreview = ""
    var isPreview = false
    
    //MARK:-------------------
    //MARK: - IBOutlets
    @IBOutlet weak var btnRadioText: UIButton!
    @IBOutlet weak var btnRadioMail: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnInitialSetUp: UIButton!
    @IBOutlet weak var btnSurveyy: UIButton!
    
    @IBOutlet weak var stkEmail: UIStackView!
    @IBOutlet weak var txtEmail: ACFloatingTextField!
    @IBOutlet weak var txtSubject: ACFloatingTextField!

    @IBOutlet weak var stkPhone: UIStackView!
    @IBOutlet weak var txtPhone: ACFloatingTextField!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var tvList: UITableView!

    @IBOutlet var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var viewForPreview: UIView!
    @IBOutlet weak var webViewForPreview: WKWebView!
    
    @IBOutlet weak var btnContinue_Preview: UIButton!
    //MARK:-------------------
    //MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            tvList.tableFooterView = UIView()
            //    chkSendTextShow=[[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ShowTextOnMobile"]]boolValue];

            nsud.set(false, forKey: "isServiceSurvey")
            nsud.set(false, forKey: "isMechanicalSurvey")
            nsud.set(true, forKey: "isNewSalesSurvey")

            nsud.synchronize()
            
            basicFunction()
            UIInitialization()
            showPreviewData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    //MARK:-------------------
    //MARK: -CustomeFunction
    
    
    func showPreviewData()  {
        
        webViewForPreview.navigationDelegate = self
        
        if isInternetAvailable()
        {
            if "\(matchesGeneralInfo.value(forKey: "isPreview") ?? "")".caseInsensitiveCompare("true") == .orderedSame //|| !getOpportunityStatus()
            {
                lblHeaderTitle.text = "Agreement Preview"
                viewForPreview.isHidden = false
                isPreview = true
                startLoader()
                syncAPICalling()
            }
            else
            {
                viewForPreview.isHidden = true
                isPreview = false
            }
        }
        else
        {
            viewForPreview.isHidden = true
            isPreview = false
        }
    }
    
    
    func basicFunction()  {
        
        
//        NSUserDefaults *defsIsService=[NSUserDefaults standardUserDefaults];
//        [defsIsService setBool:YES forKey:@"isServiceSurvey"];
//        [defsIsService setBool:NO forKey:@"isMechanicalSurvey"];
//        [defsIsService synchronize];
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        isEditSchedule = (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsRescheduleSalesAppointment") ?? false) as! Bool

        strLeadId = ("\(nsud.value(forKey: "LeadId") ?? "")" as NSString) as String
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.SalesAutoCompanyId") ?? "")"
        
        
        strCompanyName = "\(dictLoginData.value(forKeyPath: "Company.Name") ?? "")"
        strEmpEmail = "\(dictLoginData.value(forKey: "EmployeeEmail") ?? "")"
        strEmpPhone = "\(dictLoginData.value(forKey: "EmployeePrimaryPhone") ?? "")"
        strEmpId = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"

        
        
        isGGQIntegration = (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsGGQIntegration") ?? false) as! Bool
        isShowTextButton = (dictLoginData.value(forKeyPath: "Company.CompanyConfig.ShowTextOnMobile") ?? false) as! Bool
        
        matchesGeneralInfo = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
            
        strBranchSysName = ("\(matchesGeneralInfo.value(forKey: "branchSysName") ?? "")" as NSString) as String
        strFlowType = ("\(matchesGeneralInfo.value(forKey: "flowType") ?? "")" as NSString) as String

        strGlobalStatusSysName = ("\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")" as NSString) as String
        strGlobalStageSysName = ("\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")" as NSString) as String
        
        strSurveyStatus = ("\(matchesGeneralInfo.value(forKey: "surveyStatus") ?? "")" as NSString) as String

        if strSurveyStatus.caseInsensitiveCompare("Completed") == .orderedSame || strSurveyStatus.caseInsensitiveCompare("Complete") == .orderedSame
        {
            isSurveyCompleted = true
        }
        else
        {
            isSurveyCompleted = false
        }
        
        strStatusOfInitialSetup = "\(matchesGeneralInfo.value(forKey: "isInitialSetupCreated") ?? "")"
        /*if(matchesGeneralInfo.value(forKey: "isProposalFromMobile") != nil)
        {
            let isProposalFromMobile = "\(matchesGeneralInfo.value(forKey: "isProposalFromMobile") ?? "")"
            if isProposalFromMobile.lowercased() == "true" {
               isSendProposal = true
            }else{
                isSendProposal = false
            }
        }
        else
        {
            isSendProposal = false
        }*/
        
        if "\(matchesGeneralInfo.value(forKey: "isCustomerNotPresent") ?? "")" == "true"
        {
            isCustomerPresent = false
        }
        else
        {
            isCustomerPresent = true
            
            if isSendProposal
            {
                isCustomerPresent = false
            }
        }
        
        if isSendProposal
        {
            lblHeaderTitle.text = "Send Proposal"
        }
        else
        {
            lblHeaderTitle.text = "Send Agreement"
        }
        
        
        if "\(matchesGeneralInfo.value(forKey: "isResendAgreementProposalMail") ?? "")" == "true"
        {
            isResend = true
            btnSend.setTitle("Resend", for: .normal)
        }
        else
        {
            isResend = false
            btnSend.setTitle("Send", for: .normal)
        }
        
        isResend = false
        btnSend.setTitle("Send", for: .normal)
        
        if showSurveyButton()
        {
            btnSurveyy.isHidden = false
            btnInitialSetUp.isHidden = true
        }
        else
        {
            btnSurveyy.isHidden = true
            btnInitialSetUp.isHidden = true

        }
        setEmailSubject()
        fetchOtherDocumentsFromDB()
        getServicePropsalBasicDetails()
    }
    
    func showDefaultEmailSubject(strType : String) -> String  {
        
        return ""
    }
    
    func getOpportunityStatus() -> Bool
    {
        if strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func UIInitialization()  {
        
        btnAdd.layer.cornerRadius = DeviceType.IS_IPAD ? 25 : 22
        btnCancel.layer.cornerRadius = DeviceType.IS_IPAD ? 25 : 22
        btnSend.layer.cornerRadius = DeviceType.IS_IPAD ? 25 : 22
        btnInitialSetUp.layer.cornerRadius = DeviceType.IS_IPAD ? 25 : 22
        btnSurveyy.layer.cornerRadius = DeviceType.IS_IPAD ? 25 : 22
        
        matchesGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
        
        btnRadioMail.setImage(UIImage(named: "redio_2"), for: .normal)
        btnRadioText.setImage(UIImage(named: "redio_1"), for: .normal)
                
        txtPhone.delegate = self
        txtEmail.delegate = self
        txtSubject.delegate = self
       
        switch strSendMailStatusFrom {
        case Enum_SalesNewSendMailFrom.Email:
            stkEmail.isHidden = false
            stkPhone.isHidden = true
            btnRadioMail.setImage(UIImage(named: "redio_2"), for: .normal)
            btnRadioText.setImage(UIImage(named: "redio_1"), for: .normal)
            CallforEmailOnly()
            break
        case Enum_SalesNewSendMailFrom.Text:
            stkEmail.isHidden = true
            stkPhone.isHidden = false
            btnSend.setTitle("Send Text", for: .normal)
            btnRadioMail.setImage(UIImage(named: "redio_1"), for: .normal)
            btnRadioText.setImage(UIImage(named: "redio_2"), for: .normal)
            CallforTextAndReloadOnly()
            break
        default:
            break
        }
        tvList.dataSource = self
        tvList.delegate = self
        
        if isShowTextButton
        {
            btnRadioText.isHidden = false
        }
        else
        {
            btnRadioText.isHidden = true
        }
       
    }
    func CallforEmailOnly()  {
        //------Show From Name-------
         
         let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
         if("\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoReportType")!)" == "CompanyEmail"){
             lblFrom.text = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoReportEmail") ?? "")"
         }else{
             lblFrom.text = "\(dictLoginData.value(forKey: "EmployeeEmail") ?? "")"
         }
         lblFrom.text = "From : " + ((lblFrom.text == "" ? "\(dictLoginData.value(forKeyPath: "Company.Email") ?? "")" :  lblFrom.text)!)
        
         //---------Resend and Send Button text
//         if CheckResendAgreementProposalMail() {
//                 btnSend.setTitle("Resend Mail", for: .normal)
//             }else{
//                 btnSend.setTitle("Send Mail", for: .normal)
//         }
         //--------Save Lead Email in DataBase-----
         
         let primaryEmail  =  "\(matchesGeneralInfo.value(forKey: "primaryEmail") ?? "")"
         let secondaryEmail  =   "\(matchesGeneralInfo.value(forKey: "secondaryEmail") ?? "")"
         let strEmailLead = "\(primaryEmail),\(secondaryEmail)"
         let aryTempEmail = strEmailLead.split(separator: ",")
         for item in aryTempEmail {
             if("\(item)" != ""){
                 if !CheckEmailAlreadyExist(strEmail: "\(item)") {
                     self.SaveLeadEmailInDataBase(strEmail: "\(item)", strSubject: "", strLeadID: "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
                 }
             }
         }
         //--------Save Default Email in DataBase-----
         
         //--Delete First
         let arrayEmailDetail = getDataFromLocal(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isDefaultEmail == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName , "true"))
         for item in arrayEmailDetail {
             let obj = (item as AnyObject) as! NSManagedObject
             self.deleteDefaultEmailFromDB(object: obj)
         }

         let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
         let strBranchSysName = "\(matchesGeneralInfo.value(forKey: "branchSysName") ?? "")"
        
        if dictMasters.value(forKey: "DefaultEmails") is NSArray
        {
            let arrOfDefaultEmailsDB = (dictMasters.value(forKey: "DefaultEmails")as! NSArray).mutableCopy() as! NSMutableArray
            
            let aryTemp = arrOfDefaultEmailsDB.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "BranchSysName")!)".contains(strBranchSysName) }
            if(aryTemp.count != 0){
                for item in aryTemp {
                    let EmailId = "\((item as AnyObject).value(forKey: "EmailId")!)"
                    if("\(EmailId)" != ""){
                        if("\(EmailId)" == "##EmployeeEmail##"){
                            let stremail = "\(dictLoginData.value(forKey: "EmployeeEmail")!)"
                            if !CheckEmailAlreadyExist(strEmail: "\(stremail)") {
                                self.SaveDefaultMasterEmailInDataBase(strEmail: "\(stremail)", strSubject: "", strLeadID: "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
                            }
                        }else{
                            if !CheckEmailAlreadyExist(strEmail: "\(EmailId)") {
                                self.SaveDefaultMasterEmailInDataBase(strEmail: "\(EmailId)", strSubject: "", strLeadID: "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
                            }
                        }
                    }
                }
            }
        }
        ReloadEmailList()
    }
    
    
    func CallforTextAndReloadOnly()  {
        let arrayLeadContactDetailExtSerDc = getDataFromLocal(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        if arrayLeadContactDetailExtSerDc.count != 0 {
             arySendTextList = NSMutableArray()
            arySendTextList = arrayLeadContactDetailExtSerDc.mutableCopy() as! NSMutableArray
        }
        self.tvList.reloadData()
    }
    func showSurveyButton() -> Bool {
        
        var show = false
        
        if isGGQIntegration && "\(nsud.value(forKey: "SurveyID") ?? "")".count > 0 && isSendProposal == false && isSurveyCompleted == false && isCustomerPresent == true
        {
            show = true
        }
        else
        {
            show = false
        }
        
        return show
    }
    func showInitialSetupButton() -> Bool {
        
        var show = false
        
        if isGGQIntegration && "\(nsud.value(forKey: "SurveyID") ?? "")".count > 0 && isSendProposal == false && isSurveyCompleted == false && isCustomerPresent == true
        {
            show = false
        }
        else
        {
            show = false
        }
        
        return show
    }
    
    func updateLeadDetail()  {
        
        if !isPreview
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()

            arrOfKeys.add("isMailSend")
            arrOfValues.add("true")

            let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@",strUserName, strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                
            }
        }
        
    }
    func updateLeadDetailAfterPreview()  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("isMailSend")
        arrOfValues.add("true")
        
        arrOfKeys.add("isPreview")
        arrOfValues.add("false")
        
        arrOfKeys.add("statusSysName")
        arrOfValues.add(strStatusForPreview)
        
        arrOfKeys.add("stageSysName")
        arrOfValues.add(strStageForPreview)

        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@",strUserName, strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            
        }
        
    }
    
    func updatePaymentInfoAfterResponse(dictResponse : NSDictionary)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        strClickHereUrl = "\(dictResponse.value(forKey: "clickHereUrl") ?? "")"//[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"clickHereUrl"]];

        arrOfKeys.add("agreement")
        arrOfKeys.add("proposal")
        arrOfKeys.add("inspection")
        
        arrOfValues.add("\(dictResponse.value(forKey: "Agreement") ?? "")")
        arrOfValues.add("\(dictResponse.value(forKey: "Proposal") ?? "")")
        arrOfValues.add("\(dictResponse.value(forKey: "Inspection") ?? "")")

        
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "PaymentInfo", predicate: NSPredicate(format: "userName == %@ && leadId == %@",strUserName, strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            
        }
        
    }
    
    func getJsonArray(arrData : NSArray) -> NSMutableArray {
        
        let arrMutable = NSMutableArray()
        
        for item in arrData {
            let match = item as! NSManagedObject
            let dictData = getMutableDictionaryFromNSManagedObject(obj: match ) as NSDictionary
            arrMutable.add(dictData)
        }

        return arrMutable
    }
    
    func getJsonObject(match : NSManagedObject) -> NSDictionary {
        
        let dictData = getMutableDictionaryFromNSManagedObject(obj: match ) as NSDictionary
        
        return dictData
    }
    
    func fetchAllServices() -> String {
        
        let arrAllServiceName = NSMutableArray()
        
        
        let arraySoldServiceStandardDetail = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        for item in arraySoldServiceStandardDetail
        {
            let obj = item as! NSManagedObject
            arrAllServiceName.add("\(getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(obj.value(forKey: "serviceSysName") ?? "")").value(forKey: "Name") ?? "")")
        }
        
        let arraySoldServiceNonStandardDetail = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        for item in arraySoldServiceNonStandardDetail
        {
            let obj = item as! NSManagedObject
            arrAllServiceName.add("\(obj.value(forKey: "serviceName") ?? "")")
        }
        
        if arrAllServiceName.count > 0
        {
            return arrAllServiceName.componentsJoined(by: ", ")

        }
        else
        {
            return " "

        }
    }
    func fetchAllSoldServices() -> String {
        
        let arrAllServiceName = NSMutableArray()
        
        
        let arraySoldServiceStandardDetail = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
        
        for item in arraySoldServiceStandardDetail
        {
            let obj = item as! NSManagedObject
            arrAllServiceName.add("\(getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(obj.value(forKey: "serviceSysName") ?? "")").value(forKey: "Name") ?? "")")
        }
        
        let arraySoldServiceNonStandardDetail = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
        
        for item in arraySoldServiceNonStandardDetail
        {
            let obj = item as! NSManagedObject
            arrAllServiceName.add("\(obj.value(forKey: "serviceName") ?? "")")
        }
        
        if arrAllServiceName.count > 0
        {
            return arrAllServiceName.componentsJoined(by: ", ")

        }
        else
        {
            return " "

        }
    }
    func fetchAllProposedServices() -> String {
        
        let arrAllServiceName = NSMutableArray()
        
        
        let arraySoldServiceStandardDetail = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
        
        for item in arraySoldServiceStandardDetail
        {
            let obj = item as! NSManagedObject
            arrAllServiceName.add("\(getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(obj.value(forKey: "serviceSysName") ?? "")").value(forKey: "Name") ?? "")")
        }
        
        let arraySoldServiceNonStandardDetail = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
        
        for item in arraySoldServiceNonStandardDetail
        {
            let obj = item as! NSManagedObject
            arrAllServiceName.add("\(obj.value(forKey: "serviceName") ?? "")")
        }
        
        if arrAllServiceName.count > 0
        {
            return arrAllServiceName.componentsJoined(by: ", ")

        }
        else
        {
            return " "

        }
    }
    
    func handleTag(strFinalMessage: String) -> String {
        
        var strFinalMessage = strFinalMessage
        
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##AccountNo##", with: "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")")
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##CustomerCompanyName##", with: "\(matchesGeneralInfo.value(forKey: "companyName") ?? "")")
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##CustomerName##", with: "\(matchesGeneralInfo.value(forKey: "customerName") ?? "")")
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##EmployeeName##", with: strEmpName)
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##EmployeEmail##", with: strEmpEmail)
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##EmployeePhone##", with: strEmpPhone)
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##CompanyName##", with: strCompanyName)
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##OpportunityContactName##", with: strUserName)
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##OpportunityFirstName##", with: "\(matchesGeneralInfo.value(forKey: "firstName") ?? "")")
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##OpportunityMiddleName##", with: "\(matchesGeneralInfo.value(forKey: "middleName") ?? "")")
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##OpportunityLastName##", with: "\(matchesGeneralInfo.value(forKey: "lastName") ?? "")")
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##ProposedServices##", with: fetchAllProposedServices())
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##SoldServices##", with: fetchAllSoldServices())
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##Services##", with: fetchAllServices())
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##CurrentDate##", with: "\(Global().getCurrentDate() ?? "")")
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##ScheduleDate##", with: changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "scheduleStartDate") ?? "")", strRequiredFormat: "MM/dd/yyyy"))
        
        return strFinalMessage
    }
    
    func setEmailSubject()  {
        
        if "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")" == "Complete" && "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")" == "Won"
        {
            
            txtSubject.text = setDefaultEmailSubject(strAgreementType: "SalesAgreement")
            
        }
        else if "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")" == "Complete" && "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")" == "CompletePending"
        {
            txtSubject.text = setDefaultEmailSubject(strAgreementType: "CustomerSignatureLink")
        }
        else if "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")" == "Open" && "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")" == "Proposed"
        {
            txtSubject.text = setDefaultEmailSubject(strAgreementType: "SalesProposal")
        }
        else
        {
            txtSubject.text = setDefaultEmailSubject(strAgreementType: "SalesAgreement")
        }
        
    }
    
    func setDefaultEmailSubject(strAgreementType : String) -> String {
        
        var strFinalSubject = ""
        var arrFinal = NSArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "EmailTemplates") is NSArray){
                
                let arrObject = dictMaster.value(forKey: "EmailTemplates") as! NSArray
               
                arrFinal = arrObject.filter { (dict) -> Bool in
                    
                    return "\((dict as! NSDictionary).value(forKey: "SysName") ?? "")" == strAgreementType
                } as NSArray
            }
        }
        
        if arrFinal.count > 0
        {
            let dict = arrFinal.object(at: 0) as! NSDictionary
            strFinalSubject = handleTag(strFinalMessage: "\(dict.value(forKey: "EmailSubject") ?? "")")
        }
    
        return strFinalSubject
    }
    
    
    
    //MARK:------------------- Navigaiton Function -------------------
    
    func goToAppointmentNew() throws {
           
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            

            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    func goToAppointment()  {
                
        do {
            try goToAppointmentNew()
        } catch  {
            print(error.localizedDescription)
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
        }
        
    }
    
    func goToAppointmentOld()  {
                        

        
        var isAppointmentFound = false
        
        var controllerVC = UIViewController()

        do
        {
            sleep(1)
        }
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is AppointmentVC {
                
                isAppointmentFound = true
                controllerVC = aViewController
                //break
                //self.navigationController!.popToViewController(aViewController, animated: false)
            }
        }
        
        if isAppointmentFound
        {
            nsud.setValue(true, forKey: "RefreshAppointmentList")
            nsud.synchronize()
            
            self.navigationController?.popToViewController(controllerVC, animated: false)
            print("Pop to appointment")

        }
        else
        {
            
            print("Push to appointment")

            let defsAppointemnts = UserDefaults.standard
            let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                String
            if strAppointmentFlow == "New"
            {
                if DeviceType.IS_IPAD
                {
                    let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                    let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                    "AppointmentVC") as? AppointmentVC
                    if let objAppointmentView = objAppointmentView {
                        navigationController?.pushViewController(objAppointmentView, animated:
                                                                    
                                                                    false)
                    }
                }
                else
                {
                    let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                    let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                    "AppointmentVC") as? AppointmentVC
                    if let objAppointmentView = objAppointmentView {
                        navigationController?.pushViewController(objAppointmentView, animated:
                                                                    
                                                                    false)
                    }
                }
            }
            else
            {
                if DeviceType.IS_IPAD
                {
                    let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                    "AppointmentViewiPad") as? AppointmentViewiPad
                    if let objByProductVC = objByProductVC {
                        navigationController?.pushViewController(objByProductVC, animated: false)
                    }
                }
                else
                {
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                    "AppointmentView") as? AppointmentView
                    if let objByProductVC = objByProductVC {
                        navigationController?.pushViewController(objByProductVC, animated: false)
                    }
                }
            }
        }
        
    }
    
    
    func goToReview()  {
        
        
        if strFrom == "Appointment"
        {
            self.goToAppointment()
        }
        else
        {
            
            self.goToAppointment()
            
//            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//
//            for aViewController in viewControllers {
//
//                if aViewController is SalesReviewViewController {
//
//                    self.navigationController?.popToViewController(aViewController, animated: false)
//
//                }
//                else
//                {
//
//                    if aViewController is FormBuilderViewController {
//
//                        self.navigationController?.popToViewController(aViewController, animated: false)
//
//                    }
//
//                }
//
//            }
        }

    }
    func goToSurvey()  {
        
    }
    func goToInitialSetup()  {
        
    }
    func goToGenerateWorkOrder()  {
        
    }
    func goToCommercialAgreement()  {
        
        if strFrom == "Appointment"
        {
            self.goToAppointment()
        }
        else
        {
            self.goToAppointment()
            
//            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//
//            for aViewController in viewControllers {
//
//                if aViewController is NewCommericialReviewViewController {
//
//                    self.navigationController?.popToViewController(aViewController, animated: false)
//                    break
//                }
//
//            }
        }

    }
    func goToBeginAudit()  {
        
        if DeviceType.IS_IPAD
        {
            let bav = BeginAuditViewiPad(nibName: "BeginAuditView_iPad", bundle: nil)
            //bav.isFromSendEmail = "true"
            bav.statusOfInitialSetup = strStatusOfInitialSetup
            navigationController?.pushViewController(bav, animated: false)
        }
        else
        {
            if UIScreen.main.bounds.size.height == 480.0 {
                //move to your iphone5 xib
                let bav = BeginAuditView(nibName: "BeginAuditView", bundle: nil)
                bav.isFromSendEmail = strStatusOfInitialSetup
                navigationController?.pushViewController(bav, animated: false)
                
            } else {
                //move to your iphone4s xib
                let bav = BeginAuditView(nibName: "BeginAuditView_iphone5", bundle: nil)
                bav.isFromSendEmail = strStatusOfInitialSetup
                navigationController?.pushViewController(bav, animated: false)
            }
        }
        
        
        
    }
    
    //MARK:-------------------
    //MARK: -IBAction
    @IBAction func btnBackAction(_ sender: Any) {
        
        if isPreview
        {
            self.navigationController?.popViewController(animated: false)
        }
        else
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isMailSend")
            arrOfValues.add("false")
            
            let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@",strUserName, strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
            }
            saveDocumentToBeSend()
            goToAppointment()
        }

    }
    
    @IBAction func actionBtnRadioEmail(_ sender: Any) {
        
        strSendMailStatusFrom = .Email
        UIInitialization()

    }
    @IBAction func actionBtnRadioPhone(_ sender: Any) {
        
        strSendMailStatusFrom = .Text
        UIInitialization()
      
    }
    
    @IBAction func btnAddAction(_ sender: Any) {
        self.view.endEditing(true)
     //-----For Send Email-------
        if(strSendMailStatusFrom == Enum_SalesNewSendMailFrom.Email){
            if txtEmail.text == "" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email is required!", viewcontrol: self)
            }else if !(validateEmail(email: txtEmail.text!)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EmailAddress, viewcontrol: self)
            }else{
                //--CheckAlready Exist or not
                if CheckEmailAlreadyExist(strEmail: txtEmail.text!) {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email is already exist ", viewcontrol: self)

                }else{
                    SaveLeadEmailInDataBase(strEmail: txtEmail.text!, strSubject: txtSubject.text!, strLeadID: "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")

                }
            }
        }
        //-----For Send Text-------

        else if(strSendMailStatusFrom == Enum_SalesNewSendMailFrom.Text){
            if txtPhone.text == "" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Phone number is required!", viewcontrol: self)
                
            }
            else if(txtPhone.text!.count < 12){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Phone number invalid!", viewcontrol: self)
                
               
            }else{
                
                //--CheckAlready Exist or not
                if(CheckTextAlreadyExist(strText: txtPhone.text!)){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Phone number is already exist ", viewcontrol: self)
                }
               else{
                SaveLeadTextInDataBase(strText: txtPhone.text!, strSubject: "", strLeadID: "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
                }
                
              
            }
        }
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
             
        Global().updateSalesModifydate(strLeadId as String)
        
        if strSendMailStatusFrom == .Email
        {
            let isProceed = validationForSendEmail()
            
            if !isProceed.check
            {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: isProceed.message, viewcontrol: self)
            }
            else
            {
                updateEmailSubject()
                
                if(isInternetAvailable()){
                    
                    if isResend
                    {
                        fetchAllDataFromDB()
                        reSendSalesLeadDataToServer(dictToSend: getResendData(), strLeadIdToSend: strLeadId)
                    }
                    else
                    {
                        startLoader()
                        
                        if isEditSchedule && "\(matchesGeneralInfo.value(forKey: "scheduleTimeType") ?? "")".count > 0
                        {
                            self.syncTimeRange(dictToSend: getTimeRangeData(), strLeadIdToSend: strLeadId)
                        }
                        else
                        {
                            syncAPICalling()
                        }
                        //syncAPICalling()
                    }
                }
                else
                {
//                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Data saved offline", viewcontrol: self)
//
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
//
//                        self.goToAppointment()
//
//                    }
                    
                    let alert = UIAlertController(title: Alert, message: "Data saved offline", preferredStyle: .alert)

                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in

                        self.goToAppointment()
                    }))

                    alert.popoverPresentationController?.sourceView = self.view
                    self.present(alert, animated: true, completion: {
                    })
                   
                }
                

                Global().updateSalesModifydate(strLeadId)
            }
        }
        else if strSendMailStatusFrom == .Text
        {
            let isProceed = validationForSendText()
            
            if !isProceed.check
            {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: isProceed.message, viewcontrol: self)
            }
            else
            {
                if isSyncApiCalledForText
                {
                    openMessageComposer(strMessage: getSendFinalMessage())
                }
                else
                {
                    if(isInternetAvailable()){
                        
                        //syncAPICalling()
                        
                        startLoader()
                        
                        if isEditSchedule && "\(matchesGeneralInfo.value(forKey: "scheduleTimeType") ?? "")".count > 0
                        {
                            self.syncTimeRange(dictToSend: getTimeRangeData(), strLeadIdToSend: strLeadId)
                        }
                        else
                        {
                            syncAPICalling()
                        }

                    }
                    else
                    {
                        //showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Data saved offline", viewcontrol: self)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in

                            self.goToAppointment()

                        }

                    }
                }
                Global().updateSalesModifydate(strLeadId)
            }
        }
        
        
    }
    @IBAction func actionOnContinue_Preview(_ sender: Any) {
        
        
        updateLeadDetailAfterPreview()
        viewForPreview.isHidden = true
        
    }
    
    func validationForSendText() -> (check: Bool, message: String) {
       
        var check = true
        var message = String()
        
        let arrContactDetail = getDataFromLocal(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        let arrContactDetailSelected = getDataFromLocal(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isMessageSent == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, "true"))


        if arrContactDetail.count == 0
        {
            message = "\("Please add contact number")"
            
            check = false
        }
        else if arrContactDetailSelected.count == 0
        {
            message = "\("Please select contact number")"
            
            check = false
        }
        else
        {
            check = true
        }
        
        return (check,message)
    }
    func validationForSendEmail() -> (check: Bool, message: String) {
       
        var check = true
        var message = String()
        
        let arrEmail = getDataFromLocal(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))

        if txtSubject.text?.count == 0
        {
            message = "\("Add subject")"
            
            check = false
        }
        else if arrEmail.count == 0
        {
            message = "\("Add email")"
            
            check = false
        }
        else if getDataFromLocal(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isMailSent == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, "true")).count == 0
        {
            message = "\("Please select email to send")"
            
            check = false
        }
        else
        {
            check = true
        }
        
        return (check,message)
    }
    
    
    @IBAction func btnCancelAction(_ sender: Any) {
         
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("isMailSend")
        arrOfValues.add("false")

        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@",strUserName, strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            
        }
        saveDocumentToBeSend()
        goToAppointment()

    }
    @IBAction func btnInitialSetupAction(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMain" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "InitialSetUp") as! InitialSetUp
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnSurveyAction(_ sender: Any) {
       
        goToBeginAudit()
    }
    
    //MARK:------------------- API Calling -------------------
        
    func fetchAllDataFromDB() {
        
        updateLeadDetail()
        saveDocumentToBeSend()
        fetchLeadDetail()
        fetchSoldServiceStandardDetail()
        fetchSoldServiceNonStandardDetail()
        fetchImageDetail()
        fetchEmailDetail()
        fetchPaymentInfo()
        fetchLeadPreference()
        fetchDocumentsDetail()
        fetchCurrentServices()
        fetchForAppliedDiscountFromCoreData()
        fetchRenewalServiceData()
        fetchElectronicAuthorizedForm()
        fetchAgreementCheckListSales()
        fetchServiceFollowUp()
        fetchProposalFollowUp()
        fetchTagDetail()
        fetchUserDefinedData()
        fetchCRMImageDetail()
        //ClarkPest
        
        fetchMultiTermsFromCoreDataClarkPest()
        fetchTargetFromCoreData()
        fetchScopeFromCoreData()
        fetchLeadCommercialInitialInfoFromCoreData()
        fetchLeadCommercialMaintInfoFromCoreData()
        fetchLeadCommercialDetailExtDcFromCoreData()
        fetchForAppliedDiscountFromCoreDataClarkPest()
        fetchSalesMarketingContentFromCoreDataClarkPest()
    }
    
    func getResendData() -> NSDictionary {
        

        let arrKeys = ["LeadId",
                        "LeadNumber",
                        "CompanyId",
                        "IsProposalFromMobile",
                        "DocumentsDetail",
                        "EmailDetail"]
        
        let arrValues = [strLeadId,
                         "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")",
                         strCompanyId,
                         "\(matchesGeneralInfo.value(forKey: "isProposalFromMobile") ?? "")" == "true" ? "YES" : "NO",
                         dictFinal.value(forKey: "DocumentsDetail"),
                         dictFinal.value(forKey: "EmailDetail")]
        
        let dictObject = NSDictionary.init(objects: arrValues as [Any], forKeys: arrKeys as [NSCopying])
        
        return dictObject
    }
    func getLeadContactData() -> NSDictionary {
        
        let dictContactTemp = NSMutableDictionary()
        
        let arrayLeadMarketingContentExtDc = getDataFromLocal(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")",strUserName))
        if (arrayLeadMarketingContentExtDc.count==0)
        {
            dictContactTemp.setValue(NSArray(), forKey: "LeadContactDetailExtSerDc")
        }else
        {
            dictContactTemp.setValue(getJsonArray(arrData: arrayLeadMarketingContentExtDc), forKey: "LeadContactDetailExtSerDc")
        }
        
        
        

        let arrKeys = ["LeadId",
                        "LeadNumber",
                        "CompanyId",
                        "IsProposalFromMobile",
                        "DocumentsDetail",
                        "LeadContactDetailExtSerDc"]
        
        let arrValues = [strLeadId,
                         "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")",
                         strCompanyId,
                         "\(matchesGeneralInfo.value(forKey: "isProposalFromMobile") ?? "")" == "true" ? "YES" : "NO",
                         dictFinal.value(forKey: "DocumentsDetail"),
                         dictContactTemp.value(forKey: "LeadContactDetailExtSerDc")]
        
        let dictObject = NSDictionary.init(objects: arrValues as [Any], forKeys: arrKeys as [NSCopying])
        
        return dictObject
    }
    
    func syncAPICalling()  {
       
        fetchAllDataFromDB()
        fetchSalesDynamicForm()
    }
    func sendingDataToServer()  {
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            
            //self.syncTimeRange(dictToSend: getTimeRangeData(), strLeadIdToSend: strLeadId)

            self.uploadAllTypeData()

        }
        
        self.dispatchGroupSales.notify(queue: DispatchQueue.main, execute: {
            DispatchQueue.main.async {
                // Reload Table
                
                let dictFinalNew = self.dictFinal as NSDictionary
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
                    
                    self.sendingSalesLeadDataToServer(dictToSend: dictFinalNew, strLeadIdToSend: self.strLeadId)

                }

                
            }
        })
    }
    func uploadAllTypeData()  {
        
        
        //Upload Service Address Image
        
        let strServiceAddressImage = "\(matchesGeneralInfo.value(forKey: "serviceAddressImagePath") ?? "")"
        
        if strServiceAddressImage.count > 0
        {
            self.dispatchGroupSales.enter()
            
            ServiceGalleryService().uploadImageSalesOld(strImageName: strServiceAddressImage, strUrll: "\(URL.Gallery.uploadServiceAddressImageSales)") { (response, status) in
                
                self.dispatchGroupSales.leave()
                print(response ?? "")
            }
        }
        
        //Gallery Image
        
        let arrImage = getDataFromLocal(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        aryImagesList = NSMutableArray()
        
        for item in arrImage
        {
            let match = item as! NSManagedObject
            aryImagesList.add("\(match.value(forKey: "leadImagePath") ?? "")")

        }
        
        if aryImagesList.count != 0 {
            
            uploadGalleryImages()
        }
        
        //CRM Image
        
        let arrImageCRM = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isImageSyncforMobile == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
        aryCRMImagesList = NSMutableArray()
        
        for item in arrImageCRM
        {
            let match = item as! NSManagedObject
            aryCRMImagesList.add("\(match.value(forKey: "path") ?? "")")

        }
        
        if aryCRMImagesList.count != 0 {
            
            uploadCRMImages()
        }
        
        
        //Singature Image
        
        let arrSignatureImage = NSMutableArray()
        let arrCheckImage = NSMutableArray()

        let arrDataPayment = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId=%@",strLeadId))
        
        for item in arrDataPayment
        {
            let match = item as! NSManagedObject
            
            if "\(match.value(forKey: "customerSignature") ?? "")".count > 0
            {
                arrSignatureImage.add("\(match.value(forKey: "customerSignature") ?? "")")
            }
            if "\(match.value(forKey: "salesSignature") ?? "")".count > 0
            {
                arrSignatureImage.add("\(match.value(forKey: "salesSignature") ?? "")")
            }
            if "\(match.value(forKey: "checkFrontImagePath") ?? "")".count > 0
            {
                arrCheckImage.add("\(match.value(forKey: "checkFrontImagePath") ?? "")")
            }
            if "\(match.value(forKey: "checkBackImagePath") ?? "")".count > 0
            {
                arrCheckImage.add("\(match.value(forKey: "checkBackImagePath") ?? "")")
            }
    
        }
        
        if arrSignatureImage.count != 0
        {
            uploadSingnatureImage(arrSignatureImage: arrSignatureImage)
        }
        
        //Check Image
        
        if arrCheckImage.count != 0
        {
            uploadChequeImage(arrCheckImage: arrCheckImage)
        }

    
        //Target Image
                
        let arrTargetImage = getDataFromLocal(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        aryTargetImagesList = NSMutableArray()
        
        for item in arrTargetImage
        {
            let match = item as! NSManagedObject
            aryTargetImagesList.add("\(match.value(forKey: "leadImagePath") ?? "")")

        }
        
        if aryTargetImagesList.count != 0 {
            
            uploadTargetImages()
        }
        
        
        
        // Upload EAF Sign
        
        let arrDataEAF = getDataFromCoreDataBaseArray(strEntity: "ElectronicAuthorizedForm", predicate: NSPredicate(format: "leadId=%@",strLeadId))

        let arrSignEAF = NSMutableArray()
        for item in arrDataEAF
        {
            let match = item as! NSManagedObject
            arrSignEAF.add("\(match.value(forKey: "signaturePath") ?? "")")
        }
                
        if arrSignEAF.count != 0 {
        
            uploadEAFSignatureImage(arrImageName: arrSignEAF)
        }
        
        
        
        // Upload Documents
        
        let arrayOfLeadsDocuments = getDataFromCoreDataBaseArray(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId=%@ && docType=%@ && isToUpload=%@",strLeadId, "Other","true"))
        
        if arrayOfLeadsDocuments.count > 0 {
            
            for k1 in 0 ..< arrayOfLeadsDocuments.count {
                
                let objTemp = arrayOfLeadsDocuments[k1] as! NSManagedObject
                var strfileName = "\(objTemp.value(forKey: "fileName") ?? "")"
                
                strfileName = Global().strDocName(fromPath: strfileName)
                
                let strleadDocumentId = "\(objTemp.value(forKey: "leadDocumentId") ?? "")"
                
                let url = "\(URL.Document.uploadDocument)"
                let strURL = url
                self.uploadDocumentsSales(strLeadIdToSend: strLeadId, strDocName: strfileName, strDocId: strleadDocumentId, strUrll: strURL)
                
            }
            
        }
        
        // Upload Audio
        
        if aryAudioList.count != 0 {

            for item in aryAudioList
            {
                let stAudioSales = item as! String
                
                if stAudioSales.count > 0 {
                    
                    let url = "\(URL.Document.uploadAudio)"

                    let strURL = url
                    
                    let strName = SalesCoreDataVC().strAudioName(stAudioSales)
                    
                    self.uploadAudioSales(strAudioName: strName, strUrll: strURL)
                    
                }
            }
        }
        
        // Upload Graph XML

        if aryXMLList.count != 0 {

            for item in aryXMLList
            {
                let strXmlName = item as! String
                
                if strXmlName.count > 0 {
                    
                    let url = "\(URL.Document.uploadXML)"

                    let strURL = url
                    
                    let strName = SalesCoreDataVC().strAudioName(strXmlName)
                    
                    self.uploadAudioSales(strAudioName: "\\Documents\\GraphXML\\\(strName)", strUrll: strURL)

                    //self.uploadAudioSales(strAudioName: strName, strUrll: strURL)
                    
                }
            }
        }
        
    }
    
    func uploadSingnatureImage(arrSignatureImage : NSMutableArray)  {
        
        //Singnature Image
        
        let arrImageData = NSMutableArray()
        let arrImageName = NSMutableArray()

        for item in arrSignatureImage {
            let strImage = "\(item)"
            arrImageData.add(getImage(imagePath: strImage))
            arrImageName.add(strImage)
        }
        
        
        for item in arrImageName
        {
            self.dispatchGroupSales.enter()
            
            let strImage = "\(item)"
            ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Signature.upload)") { (response, status) in
                
                self.dispatchGroupSales.leave()
                print(response)
            }
        }
        

        /*self.dispatchGroupSales.enter()
        
        ServiceGalleryService().uploadImageSales(strUrl: URL.Signature.upload, arrImageData as! [UIImage], arrImageName as! [String]) { (response, status) in
            
            self.dispatchGroupSales.leave()
            
            if let object = response?.first{
                print(object)
                
            } else {
                
            }
        }*/

    }
    func uploadChequeImage(arrCheckImage : NSMutableArray)
    {
        let arrImageData = NSMutableArray()
        let arrImageName = NSMutableArray()

        for item in arrCheckImage {
            let strImage = "\(item)"
            arrImageData.add(getImage(imagePath: strImage))
            arrImageName.add(strImage)
        }
        
        
        for item in arrImageName
        {
            self.dispatchGroupSales.enter()
            
            let strImage = "\(item)"
            ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Signature.uploadCheque)") { (response, status) in
                
                self.dispatchGroupSales.leave()
                print(response)
            }
        }
        
        
          
       /* self.dispatchGroupSales.enter()
        
        ServiceGalleryService().uploadImageSales(strUrl: URL.Signature.uploadCheque, arrImageData as! [UIImage], arrImageName as! [String]) { (response, status) in
            
            self.dispatchGroupSales.leave()
            
            if let object = response?.first{
                print(object)
                
            } else {
                
            }
        }*/
    }

    func uploadTargetImages()  {
        
        let arrImageData = NSMutableArray()
        let arrImageName = NSMutableArray()

        for item in aryTargetImagesList {
            let strImage = "\(item)"
            arrImageData.add(getImage(imagePath: strImage))
            arrImageName.add(strImage)
        }
       
       
        
        for item in arrImageName
        {
            self.dispatchGroupSales.enter()
            
            let strImage = "\(item)"
            ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Gallery.uploadTargetImage)") { (response, status) in
                
                self.dispatchGroupSales.leave()
                print(response ?? "")
            }
        }
        
        
        /*ServiceGalleryService().uploadImageSales(strUrl: "\(URL.Gallery.uploadImages)", arrImageData as! [UIImage], arrImageName as! [String]) { (response, status) in
            
            self.dispatchGroupSales.leave()
            
            if let object = response?.first{
                print(object)
                
            } else {
                
            }
        }*/
    }
    
    
    func uploadCRMImages()  {
        
        let arrImageData = NSMutableArray()
        let arrImageName = NSMutableArray()

        for item in aryCRMImagesList {
            let strImage = "\(item)"
            arrImageData.add(getImage(imagePath: strImage))
            arrImageName.add(strImage)
        }
       
       
        
        for item in aryCRMImagesList
        {
            self.dispatchGroupSales.enter()
            
            let strImage = "\(item)"
            ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Gallery.uploadCRMImagesSingle)") { (response, status) in
                
                self.dispatchGroupSales.leave()
                print(response ?? "")
            }
        }

    }
    
    func uploadGalleryImages()  {
        
        let arrImageData = NSMutableArray()
        let arrImageName = NSMutableArray()

        for item in aryImagesList {
            let strImage = "\(item)"
            arrImageData.add(getImage(imagePath: strImage))
            arrImageName.add(strImage)
        }
       
       
        
        for item in aryImagesList
        {
            self.dispatchGroupSales.enter()
            
            let strImage = "\(item)"
            ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Gallery.uploadImagesSingle)") { (response, status) in
                
                self.dispatchGroupSales.leave()
                print(response ?? "")
            }
        }
        
        
        /*ServiceGalleryService().uploadImageSales(strUrl: "\(URL.Gallery.uploadImages)", arrImageData as! [UIImage], arrImageName as! [String]) { (response, status) in
            
            self.dispatchGroupSales.leave()
            
            if let object = response?.first{
                print(object)
                
            } else {
                
            }
        }*/
    }
    func uploadEAFSignatureImage(arrImageName: NSMutableArray)  {

        
        for item in arrImageName
        {
            self.dispatchGroupSales.enter()
            
            let strImage = "\(item)"
            ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Signature.uploadEAF)") { (response, status) in
                
                self.dispatchGroupSales.leave()
                print(response)
            }
        }
        
        
        /*let image = getImage(imagePath: strImageName)
      
        self.dispatchGroupSales.enter()
        
        SeviceReportService().uploadImage(image: image, fileName: strImageName, type: UploadType.eafSign) { (object, error) in
            
            self.dispatchGroupSales.leave()
            
            if let object = object?.first{
                print(object)
            } else {
                //self.delegate?.hideLoader()
                //self.delegate?.showAlert(message: error)
            }
        }*/
    }
    
    
    func getImage(imagePath : String)-> UIImage{
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: imagePath){
            return UIImage(contentsOfFile: imagePath)!
        }else{
            return UIImage()
        }
    }
    
    func uploadAudioSales(strAudioName : String , strUrll : String) {
        
        
        if fileAvailableAtPath(strname: strAudioName) {
            
            self.dispatchGroupSales.enter()

            let dataToSend = GetDataFromDocumentDirectory(strFileName: strAudioName)
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strAudioName, url: strUrll) { (responce, status) in
                
                  self.dispatchGroupSales.leave()
                
                if(status)
                {
                   // debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        
                    }
                }
            }
        }
    }
    
    func uploadDocumentsSales(strLeadIdToSend : String , strDocName : String ,strDocId : String , strUrll : String) {
        
        if fileAvailableAtPath(strname: strDocName) {
            
            self.dispatchGroupSales.enter()
            
            let dataToSend = GetDataFromDocumentDirectory(strFileName: strDocName)
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strDocName, url: strUrll) { (responce, status) in
                
                self.dispatchGroupSales.leave()
                
                if(status)
                {
                    debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("isToUpload")
                        arrOfValues.add("false")
                        
                        let isSuccess =  getDataFromDbToUpdate(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId=%@ && docType=%@ && leadDocumentId = %@", strLeadIdToSend , "Other" , strDocId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess {
                            
                            // Saved in DB
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    func someThingWentWrong()  {
        
        let alert = UIAlertController(title: Alert, message: alertSomeError, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
            
            self.navigationController?.popViewController(animated: false)
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
    }
    
    func sendingSalesLeadDataToServer(dictToSend : NSDictionary , strLeadIdToSend : String) {
        
        //self.dispatchGroup.enter(); print("Dispatch Group Main Entered");
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print("Final Json : \(jsonString)")
            
        }
        
        let strUrl = "\(URL.salesAutoBaseUrl)" + UrlSalesFinalUpdate
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "dict"){ (responce, status) in
            
            // Sales Data synced
        
           // self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            self.loader.dismiss(animated: false)
            
            if(status)
            {

                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    let value = responce.value(forKey: "data")
                    var strBillToId = "", strCreditCardType = "", strCardAccountNo = ""
                    
                    debugPrint(responce)
                    
                    if self.isPreview
                    {
                        print("Apply Preview Functionality")
                        if value is NSDictionary
                        {
                            let dictTemp = value as! NSDictionary
                            
                            var strPreviewUrl = "\(dictTemp.value(forKey: "PrintPreviewUrl") ?? "")"
                            
                            strPreviewUrl = strPreviewUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                           
                            guard let strUrl = URL(string: strPreviewUrl) else
                            {
                                return self.someThingWentWrong()
                            }
                            
                            self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                            self.present(self.loader, animated: false, completion: nil)
                            self.webViewForPreview.load(URLRequest(url: strUrl))
                        }
                    }
                    else
                    {
                        // Saving Target In DB Also
                        
                        if value is NSDictionary {
                            
                            let dictTemp = value as! NSDictionary
                            
                            if dictTemp.value(forKey: "leadCommercialTargetExtDcs") is NSArray {
                                
                                let leadCommercialTargetExtDcs = dictTemp.value(forKey: "leadCommercialTargetExtDcs") as! NSArray
                                
                                WebService().savingTargets(arrTarget: leadCommercialTargetExtDcs, strLeadId: strLeadIdToSend)
                                
                            }
                            
                            strBillToId = "\(dictTemp.value(forKey: "billToLocationId") ?? "")"
                            strCreditCardType = "\(dictTemp.value(forKey: "CreditCardType") ?? "")"
                            strCardAccountNo = "\(dictTemp.value(forKey: "CardAccountNumber") ?? "")"
                            
                        }
                        
                        // Updating in DB that dyanmic form is synced
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("zSync")
                        arrOfValues.add("no")
                        
                        arrOfKeys.add("isMailSend")
                        arrOfValues.add("false")
                        
                        arrOfKeys.add("billToLocationId")
                        arrOfValues.add(strBillToId)
                        
                        if strBillToId.count > 0 {
                            // remove from nsud billtoid info which was set locally
                            nsud.setValue("", forKey: strBillToId)
                        }
        
                        arrOfKeys.add("creditCardType")
                        arrOfValues.add(strCreditCardType)
                        
                        arrOfKeys.add("cardAccountNumber")
                        arrOfValues.add(strCardAccountNo)
                        
                        
                        
                        if self.isCustomerPresent
                        {
                            arrOfKeys.add("isResendAgreementProposalMail")
                            arrOfValues.add("false")//true
                        }
                        
                        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId=%@",self.strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess {
                            
                            
                        }
                        if self.strSendMailStatusFrom == .Text
                        {
                            self.isSyncApiCalledForText = true
                            self.updatePaymentInfoAfterResponse(dictResponse: value as! NSDictionary)
                            self.openMessageComposer(strMessage: self.getSendFinalMessage())
                            
    //                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
    //                            self.sendingContactDetailDataToServer(dictToSend: self.getLeadContactData(), strLeadIdToSend: self.strLeadId)
    //
    //                        }
                        }
                        else
                        {
                            if self.isSendProposal
                            {
                                //self.goToAppointment()
                                
                                
                                let alert = UIAlertController(title: Alert, message: alertSuccessfullProposal, preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                    
                                    self.goToAppointment()
                                    
                                }))
                                
                                alert.popoverPresentationController?.sourceView = self.view
                                self.present(alert, animated: true, completion: {
                                })
                                
                                
                            }
                            else
                            {
                                if self.strFlowType.caseInsensitiveCompare("Residential") == .orderedSame
                                {
                                    //self.goToReview()
                                    
                                    let alert = UIAlertController(title: Alert, message: alertSuccessfullAgreement, preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                        
                                        self.goToReview()
                                        
                                    }))
                                    
                                    alert.popoverPresentationController?.sourceView = self.view
                                    self.present(alert, animated: true, completion: {
                                    })
                                    
                                    
                                }
                                else if self.strFlowType.caseInsensitiveCompare("Commercial") == .orderedSame
                                {
                                    //self.goToCommercialAgreement()
                                    
                                    let alert = UIAlertController(title: Alert, message: alertSuccessfullAgreement, preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                        
                                        self.goToCommercialAgreement()
                                        
                                    }))
                                    
                                    alert.popoverPresentationController?.sourceView = self.view
                                    self.present(alert, animated: true, completion: {
                                    })
                                    
                                    
                                }
                                else
                                {
                                    self.goToAppointment()
                                                                    
                                }
                            }
                        }
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
            
        }
        
    }
    func reSendSalesLeadDataToServer(dictToSend : NSDictionary , strLeadIdToSend : String) {
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = "\(URL.salesAutoBaseUrl)" + UrlSalesResendEmail
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "bool"){ (responce, status) in
            
            self.loader.dismiss(animated: false)
            
            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    //self.goToAppointment()
                    
                    let alert = UIAlertController(title: Alert, message: alertSuccessfullAgreement, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                        
                        self.goToAppointment()
                        
                    }))
                    
                    alert.popoverPresentationController?.sourceView = self.view
                    self.present(alert, animated: true, completion: {
                    })
                    
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

            }
        }
        
    }
    func sendingSalesDynamicDataToServer(arrData : NSArray , strLeadIdToSend : String) {
        
        
        self.dispatchGroupSales.enter()
        
        var keys = NSArray()
        var values = NSArray()
        keys = ["InspectionFormsData"]
        
        if arrData.count > 0
        {
            let dictData = arrData[0] as! NSDictionary
            values = dictData.value(forKey: "arrFinalInspection") as! NSArray
        }
        else
        {
            values = []
        }
        //values = [arrData]
        
        let dictToSend = NSMutableDictionary()//NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        dictToSend.setValue(values, forKey: "InspectionFormsData")
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = "\(URL.salesAutoBaseUrl)" + UrlSalesInspectionDynamicFormSubmission
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "string"){ (responce, status) in
            
            self.dispatchGroupSales.leave()
            
            //self.sendingDataToServer()

            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    // Handle Success Response Here
                    
                    // Updating in DB that dyanmic form is synced
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("isSentToServer")
                    arrOfValues.add("Yes")
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "SalesDynamicInspection", predicate: NSPredicate(format: "leadId = %@ AND userName = %@",strLeadIdToSend, self.strUserName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        
                        
                    }
                    
                }
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                
                self.sendingDataToServer()
                
            }
            
        }
        
    }
    func sendingContactDetailDataToServer(dictToSend : NSDictionary , strLeadIdToSend : String) {
        
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = "\(URL.SendText.updateTextDetail)"
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "bool"){ (responce, status) in
            
            self.loader.dismiss(animated: false)
            
            if(status)
            {
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    //self.goToAppointment()
                    
                    let alert = UIAlertController(title: Alert, message: alertSuccessfullSync, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                        
                        self.goToAppointment()
                        
                    }))
                    
                    alert.popoverPresentationController?.sourceView = self.view
                    self.present(alert, animated: true, completion: {
                    })
                    
                    
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

            }
        }
        
    }
    
    
    func getTimeRangeData() -> NSDictionary {
        
        /*
         "LeadId": "465543",
         "LeadNumber": "22937",
         "CompanyId": "3",
         "CompanyKey": "Production",
         "AccountNo": "P7878802324",
         "EmployeeId": "32171",
         "Result": "",
         "RangeofTimeId": "952",
         "ServiceDate": "09\/13\/2021",
         "ServiceTime": "02:00:00",
         "EarliestTimeRange": "02:00:00",
         "LatestTimeRange": "03:00:00",
         "ScheduleTimeType": "Range",
         "ScheduleStartDate": "2021-09-13T02:00:00",
         "ScheduleEndDate": "2021-09-13T12:46:00",
         "TotalEstimationTime": ""
         */
        
        let strEarliestStartTime = changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "earliestStartTime") ?? "")", strRequiredFormat: "HH:mm:ss")
        let strLatestStartTime = changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "latestStartTime") ?? "")", strRequiredFormat: "HH:mm:ss")
        let strServiceTime = changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "scheduleTime") ?? "")", strRequiredFormat: "HH:mm:ss")


        
        
        let arrKeys = ["LeadId",
                       "LeadNumber",
                       "CompanyId",
                       "CompanyKey",
                       "AccountNo",
                       "EmployeeId",
                       "Result",
                       "RangeofTimeId",
                       "ServiceDate",
                       "ServiceTime",
                       "EarliestTimeRange",
                       "LatestTimeRange",
                       "ScheduleTimeType",
                       "ScheduleStartDate",
                       "ScheduleEndDate",
                       "TotalEstimationTime"]
        
        let arrValues = [strLeadId,
                         "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")",
                         strCompanyId,
                         strCompanyKey,
                         "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")",
                         strEmpId,
                         "",
                         "\(matchesGeneralInfo.value(forKey: "timeRangeId") ?? "")",
                         "\(matchesGeneralInfo.value(forKey: "scheduleDate") ?? "")",
                         strServiceTime,
                         strEarliestStartTime,
                         strLatestStartTime,
                         "\(matchesGeneralInfo.value(forKey: "scheduleTimeType") ?? "")",
                         "\(matchesGeneralInfo.value(forKey: "scheduleStartDate") ?? "")",
                         "\(matchesGeneralInfo.value(forKey: "scheduleEndDate") ?? "")",
                         ""]
        
//        let arrValues = [strLeadId,
//                         "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")",
//                         strCompanyId,
//                         strCompanyKey,
//                         "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")",
//                         strEmpId,
//                         "",
//                         "\(matchesGeneralInfo.value(forKey: "timeRangeId") ?? "")",
//                         "\(matchesGeneralInfo.value(forKey: "scheduleDate") ?? "")",
//                         "\(matchesGeneralInfo.value(forKey: "scheduleTime") ?? "")",
//                         "\(matchesGeneralInfo.value(forKey: "earliestStartTime") ?? "")",
//                         "\(matchesGeneralInfo.value(forKey: "latestStartTime") ?? "")",
//                         "\(matchesGeneralInfo.value(forKey: "scheduleTimeType") ?? "")",
//                         "\(matchesGeneralInfo.value(forKey: "scheduleStartDate") ?? "")",
//                         "\(matchesGeneralInfo.value(forKey: "scheduleEndDate") ?? "")",
//                         ""]
   
        
        let dictObject = NSDictionary.init(objects: arrValues as [Any], forKeys: arrKeys as [NSCopying])
        
        return dictObject
    }
    
    func syncTimeRange(dictToSend : NSDictionary , strLeadIdToSend : String)  {
        
        //dispatchGroupSales.enter()
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = "\(URL.TimeRange.updateTimeRange)"
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "bool"){ (responce, status) in
            
            //self.dispatchGroupSales.leave()
            
            
            if(status)
            {
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                }
                else
                {
                   // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
                
            }
            else
            {
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

            }
            
            self.syncAPICalling()

            
        }
        
    }
    
    //MARK:------------------- FETCHING ALL RECORDS CORE DATA -------------------

    func startLoader() {
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
    }
    
    func fetchSalesDynamicForm (){
        
        
        self.dispatchGroupSales = DispatchGroup()
        
        let arraySalesDynamicInspection = getDataFromLocal(strEntity: "SalesDynamicInspection", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")",strUserName))
        
        if (arraySalesDynamicInspection.count==0)
        {
            //loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            //self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                
                self.sendingDataToServer()
                
            }
            
        }
        else
        {
            var arrResponseInspection = NSArray()
            let matches = arraySalesDynamicInspection.object(at: 0) as! NSManagedObject
            //arrResponseInspection = arraySalesDynamicInspection.object(at: 0) as! NSArray
            arrResponseInspection = [getJsonObject(match: matches)]//[matches]
            
            //loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            //self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                
                self.sendingSalesDynamicDataToServer(arrData: arrResponseInspection, strLeadIdToSend: strLeadId)
            }
            
        }
    }
   
    func fetchLeadDetail() {
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        if (arrayLeadDetail.count==0)
        {
            dictFinal.setValue("", forKey: "LeadDetail")
        }else
        {
            let obj = arrayLeadDetail.object(at: 0)as! NSManagedObject
            
            let dictLead = getJsonObject(match: obj) as NSDictionary
            var dictMutable = NSMutableDictionary()
            
            let strzdateScheduledStart = changeStringDateToGivenFormat(strDate: "\(dictLead.value(forKey: "zdateScheduledStart") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            let strdateModified = changeStringDateToGivenFormat(strDate: "\(dictLead.value(forKey: "dateModified") ?? "")", strRequiredFormat: "MM/dd/yyyy")

            
            dictMutable = dictLead as! NSMutableDictionary
            dictMutable.removeObject(forKey: "zdateScheduledStart")
            dictMutable.removeObject(forKey: "dateModified")
            dictMutable.setObject(strzdateScheduledStart, forKey: "zdateScheduledStart" as NSCopying)
            dictMutable.setObject(strdateModified, forKey: "dateModified" as NSCopying)
            
            let strDescription = "\(dictMutable.value(forKey: "descriptionLeadDetail") ?? "")"
            dictMutable.setValue(strDescription, forKey: "description")
            

            dictFinal.setValue(dictMutable, forKey: "LeadDetail")
            aryAudioList.add("\(obj.value(forKey: "audioFilePath") ?? "")")
            
        }
        
    }
    
    func fetchPaymentInfo() {
        let arrayPaymentInfoDetail = getDataFromLocal(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        if (arrayPaymentInfoDetail.count==0)
        {
            dictFinal.setValue("", forKey: "PaymentInfo")
        }else
        {
            for item in arrayPaymentInfoDetail {
                let obj = (item as AnyObject)as! NSManagedObject
                let strCustomerImage = "\(obj.value(forKey: "customerSignature") ?? "")"
                let strSalesPersonImage = "\(obj.value(forKey: "salesSignature") ?? "")"
                let strCheckFrontImage = "\(obj.value(forKey: "checkFrontImagePath") ?? "")"
                let strCheckBackImage = "\(obj.value(forKey: "checkBackImagePath") ?? "")"
                if strCustomerImage != "" {
                    aryImagesList.add(strCustomerImage)
                }
                if strSalesPersonImage != "" {
                    aryImagesList.add(strSalesPersonImage)
                }
                if strCheckFrontImage != "" {
                    aryImagesList.add(strCheckFrontImage)
                }
                if strCheckBackImage != "" {
                    aryImagesList.add(strCheckBackImage)
                }
                dictFinal.setValue(getJsonObject(match: obj), forKey: "PaymentInfo")

            }
          
        }
    }
    
    func fetchImageDetail() {
        let arrayImageDetail = getDataFromLocal(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        if (arrayImageDetail.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "ImagesDetail")
        }else
        {
            for item in arrayImageDetail {
                let obj = (item as AnyObject)as! NSManagedObject
                if ("\(obj.value(forKey: "isNewGraph") ?? "")".lowercased() == "true" || "\(obj.value(forKey: "isNewGraph") ?? "")" == "1")
                {
                    let strXMLName = "\(obj.value(forKey: "leadXmlPath") ?? "")";
                    if (strXMLName.count != 0)
                    {
                        aryXMLList.add(strXMLName)
                    }

                }
            }
          
            dictFinal.setValue(getJsonArray(arrData: arrayImageDetail), forKey: "ImagesDetail")

        }
        
    }
    
    func fetchCRMImageDetail() {
        
        
        let arrayImageDetail = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        if (arrayImageDetail.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "CRMImagesDetail")
        }
        else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayImageDetail), forKey: "CRMImagesDetail")

        }
        
    }
    
    func fetchEmailDetail()  {
            let arrayEmailDetail = getDataFromLocal(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
            if (arrayEmailDetail.count==0)
            {
                dictFinal.setValue(NSArray(), forKey: "EmailDetail")
            }else
            {
                dictFinal.setValue(getJsonArray(arrData: arrayEmailDetail), forKey: "EmailDetail")
            }
            
        
    }
    func fetchLeadContactDetail (){
        let arrayLeadMarketingContentExtDc = getDataFromLocal(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")",strUserName))
        if (arrayLeadMarketingContentExtDc.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadContactDetailExtSerDc")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadMarketingContentExtDc), forKey: "LeadContactDetailExtSerDc")
        }
    }
    
    func fetchDocumentsDetail() {
        let arrayDocumentsDetail = getDataFromLocal(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        if (arrayDocumentsDetail.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "DocumentsDetail")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayDocumentsDetail), forKey: "DocumentsDetail")
        }
    }
    func fetchLeadPreference(){
        let arrayLeadPreference = getDataFromLocal(strEntity: "LeadPreference", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        if (arrayLeadPreference.count==0)
        {
            dictFinal.setValue("", forKey: "LeadPreference")
        }else
        {
            let obj = arrayLeadPreference.object(at: 0)as! NSManagedObject
            dictFinal.setValue(getJsonObject(match: obj), forKey: "LeadPreference")
        }
        
    }
    
    func fetchSoldServiceStandardDetail(){
       // let arraySoldServiceStandardDetail = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, isSendProposal ? "false" : "true"))
        
        
        let arraySoldServiceStandardDetail = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))

        
        if (arraySoldServiceStandardDetail.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "SoldServiceStandardDetail")
        }else
        {
            //dictFinal.setValue(arraySoldServiceStandardDetail, forKey: "SoldServiceStandardDetail")
            dictFinal.setValue(getJsonArray(arrData: arraySoldServiceStandardDetail), forKey: "SoldServiceStandardDetail")

        }
        
        
    }
    func fetchSoldServiceNonStandardDetail()
    {
        //let arraySoldServiceNonStandardDetail = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, isSendProposal ? "false" : "true"))
        
        let arraySoldServiceNonStandardDetail = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))

        if (arraySoldServiceNonStandardDetail.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "SoldServiceNonStandardDetail")
        }else
        {
            //dictFinal.setValue(arraySoldServiceNonStandardDetail, forKey: "SoldServiceNonStandardDetail")
            dictFinal.setValue(getJsonArray(arrData: arraySoldServiceNonStandardDetail), forKey: "SoldServiceNonStandardDetail")

        }
        
        
    }
    func fetchCurrentServices(){
        let arrayCurrentServices = getDataFromLocal(strEntity: "CurrentService", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        if (arrayCurrentServices.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "CurrentService")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayCurrentServices), forKey: "CurrentService")
        }
        
    }
    
    func fetchAgreementCheckListSales(){//LeadAgreementChecklistSetups
        let arrayLeadAgreementChecklistSetups = getDataFromLocal(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId == %@ && isActive == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , "true" ,strUserName))
        if (arrayLeadAgreementChecklistSetups.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadAgreementChecklistSetups")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadAgreementChecklistSetups), forKey: "LeadAgreementChecklistSetups")
        }
        
        
    }
    
    
    func fetchElectronicAuthorizedForm(){
        let arrayElectronicAuthorizedForm = getDataFromLocal(strEntity: "ElectronicAuthorizedForm", predicate: NSPredicate(format: "leadId == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" ))
        if (arrayElectronicAuthorizedForm.count==0)
        {
            dictFinal.setValue("", forKey: "ElectronicAuthorizationForm")
        }else
        {
            let obj = arrayElectronicAuthorizedForm.object(at: 0)as! NSManagedObject
            dictFinal.setValue(getJsonObject(match: obj), forKey: "ElectronicAuthorizationForm")
        }
    }
        
    func fetchForAppliedDiscountFromCoreData(){
        let arrayLeadAppliedDiscounts = getDataFromLocal(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", strUserName))
        if (arrayLeadAppliedDiscounts.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadAppliedDiscounts")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadAppliedDiscounts), forKey: "LeadAppliedDiscounts")
        }
    }
    func fetchRenewalServiceData(){
        let arrayRenewalServiceExtDcs = getDataFromLocal(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", strUserName))
        if (arrayRenewalServiceExtDcs.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "RenewalServiceExtDcs")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayRenewalServiceExtDcs), forKey: "RenewalServiceExtDcs")
        }
    }
    func fetchServiceFollowUp(){
        
        let arrayServiceFollowUp = getDataFromLocal(strEntity: "ServiceFollowUpDcs", predicate: NSPredicate(format: "leadId == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")"))
        
        
        if (arrayServiceFollowUp.count==0)
        {
            
            dictForFollowUp.setValue(NSArray(), forKey: "serviceFollowUpDcs")
            dictFinal.setValue(dictForFollowUp, forKey: "LeadFollowup")
          
        }
        else
        {
            if isServiceFollowUp
            {
                dictForFollowUp.setValue(getJsonArray(arrData: arrayServiceFollowUp), forKey: "serviceFollowUpDcs")
                dictFinal.setValue(dictForFollowUp, forKey: "LeadFollowup")
            }
            else
            {
                dictForFollowUp.setValue(NSArray(), forKey: "serviceFollowUpDcs")
                dictFinal.setValue(dictForFollowUp, forKey: "LeadFollowup")
            }
            
        }
        
       
        
    }
    func fetchProposalFollowUp(){
        
        //Proposal Followup
        
        let arrayProposalFollowUp = getDataFromLocal(strEntity: "ProposalFollowUpDcs", predicate: NSPredicate(format: "proposalLeadId == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")"))
        
        if (arrayProposalFollowUp.count==0)
        {
            dictForFollowUp.setValue(NSArray(), forKey: "proposalFollowUpDcs")
            dictFinal.setValue(dictForFollowUp, forKey: "LeadFollowup")
        }
        else
        {
            if isProposalFoolowUp
            {
                dictForFollowUp.setValue(getJsonArray(arrData: arrayProposalFollowUp), forKey: "proposalFollowUpDcs")
                dictFinal.setValue(dictForFollowUp, forKey: "LeadFollowup")
            }
            else
            {
                dictForFollowUp.setValue(NSArray(), forKey: "proposalFollowUpDcs")
                dictFinal.setValue(dictForFollowUp, forKey: "LeadFollowup")
            }
            
        }
        
    }
    func fetchTagDetail(){
        
        
        let arrayLeadTagExtDcs = getDataFromLocal(strEntity: "LeadTagExtDcs", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", strUserName))
        
        if (arrayLeadTagExtDcs.count==0)
        {
            dictFinal.setValue( NSArray(), forKey: "LeadTagExtDcs")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadTagExtDcs), forKey: "LeadTagExtDcs")
        }
        
    }
    
    
    func fetchUserDefinedData() {
        let arrayDocumentsDetail = getDataFromLocal(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        if (arrayDocumentsDetail.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "UDFSavedData")
        }else
        {
            let arrData = NSMutableArray()
            
            for item in getJsonArray(arrData: arrayDocumentsDetail)
            {
                let dict = item as? NSDictionary
                
                if let dictData = dict
                {
                    arrData.add(dictData.value(forKey: "userdefinedata"))
                }
            }
            //dictFinal.setValue(getJsonArray(arrData: arrayDocumentsDetail), forKey: "UDFSavedData")
            dictFinal.setValue(arrData, forKey: "UDFSavedData")
        }
    }
    
    //For  ClarkPest
    
    func fetchAllClarkPestData() {
        
        fetchTargetFromCoreData()
        fetchScopeFromCoreData()
        fetchLeadCommercialInitialInfoFromCoreData()
        fetchLeadCommercialMaintInfoFromCoreData()
        fetchLeadCommercialMaintInfoFromCoreData()
        fetchMultiTermsFromCoreDataClarkPest()
        fetchForAppliedDiscountFromCoreDataClarkPest()
        fetchSalesMarketingContentFromCoreDataClarkPest()
        
    }
    
    
    func fetchScopeFromCoreData(){
        let arrayLeadCommercialScopeExtDc = getDataFromLocal(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        if (arrayLeadCommercialScopeExtDc.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadCommercialScopeExtDc")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadCommercialScopeExtDc), forKey: "LeadCommercialScopeExtDc")
        }
    }
    func fetchTargetFromCoreData ()
    {
        var arrayLeadCommercialTargetExtDc = getDataFromLocal(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        let arrTarget = NSMutableArray()
        //arrTarget = arrayLeadCommercialTargetExtDc.mutableCopy() as! NSMutableArray
        
        if (arrayLeadCommercialTargetExtDc.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadCommercialTargetExtDc")
        }
        else
        {
            for i in 0 ..< arrayLeadCommercialTargetExtDc.count
            {
                let match = arrayLeadCommercialTargetExtDc[i] as! NSManagedObject

                let strLeadCommercialTargetId = "\(match.value(forKey: "leadCommercialTargetId") ?? "")"
                let strLeadCommercialMobileTargetId = "\(match.value(forKey: "mobileTargetId") ?? "")"
                
                let arrTargetImage = fetchTargetImageDetailFromCoreDate(strLeadCommercialTargetId: strLeadCommercialTargetId, strLeadCommercialMobileTargetId: strLeadCommercialMobileTargetId)
                let dict = NSMutableDictionary()
                dict.setObject(arrTargetImage, forKey: "targetImageDetailExtDc" as NSCopying)
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("isChangedTargetDesc")
                arrOfKeys.add("leadCommercialTargetId")
                arrOfKeys.add("leadId")
                arrOfKeys.add("mobileTargetId")
                arrOfKeys.add("name")
                arrOfKeys.add("targetDescription")
                arrOfKeys.add("targetSysName")
                arrOfKeys.add("userName")
                arrOfKeys.add("wdoProblemIdentificationId")
                arrOfKeys.add("targetImageDetailExtDc")

                
                arrOfValues.add("\(match.value(forKey: "companyKey") ?? "")")
                arrOfValues.add("\(match.value(forKey: "isChangedTargetDesc") ?? "")")
                arrOfValues.add("\(match.value(forKey: "leadCommercialTargetId") ?? "")")
                arrOfValues.add("\(match.value(forKey: "leadId") ?? "")")
                arrOfValues.add("\(match.value(forKey: "mobileTargetId") ?? "")")
                arrOfValues.add("\(match.value(forKey: "name") ?? "")")
                arrOfValues.add("\(match.value(forKey: "targetDescription") ?? "")")
                arrOfValues.add("\(match.value(forKey: "targetSysName") ?? "")")
                arrOfValues.add("\(match.value(forKey: "userName") ?? "")")
                arrOfValues.add("\(match.value(forKey: "wdoProblemIdentificationId") ?? "")")
                arrOfValues.add(getJsonArray(arrData: arrTargetImage))

                
                let dict_Para = NSDictionary.init(objects: arrOfValues as! [Any], forKeys: arrOfKeys as! [NSCopying])
                arrTarget.add(dict_Para)
                
                //arrTarget.add(dict)
            }
            
            arrayLeadCommercialTargetExtDc = NSArray()
            arrayLeadCommercialTargetExtDc = arrTarget
            
            dictFinal.setValue(arrayLeadCommercialTargetExtDc, forKey: "LeadCommercialTargetExtDc")

            
            //dictFinal.setValue(getJsonArray(arrData: arrayLeadCommercialTargetExtDc), forKey: "LeadCommercialTargetExtDc")
        }
    }
    
    func fetchTargetImageDetailFromCoreDate(strLeadCommercialTargetId: String, strLeadCommercialMobileTargetId: String) -> NSArray {
        
        var arrayTargetImage = NSArray()
        
        if strLeadCommercialTargetId.count > 0
        {
            arrayTargetImage = getDataFromLocal(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strLeadCommercialTargetId))
        }
        else
        {
            arrayTargetImage = getDataFromLocal(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strLeadCommercialMobileTargetId))
        }
        
        return arrayTargetImage
    }
    
    
    
    func fetchLeadCommercialInitialInfoFromCoreData (){
        let arrayLeadCommercialInitialInfoExtDc = getDataFromLocal(strEntity: "LeadCommercialInitialInfoExtDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        if (arrayLeadCommercialInitialInfoExtDc.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadCommercialInitialInfoExtDc")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadCommercialInitialInfoExtDc), forKey: "LeadCommercialInitialInfoExtDc")
        }
    }
    func fetchLeadCommercialMaintInfoFromCoreData (){
        let arrayLeadCommercialMaintInfoExtDc = getDataFromLocal(strEntity: "LeadCommercialMaintInfoExtDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" ,strUserName))
        if (arrayLeadCommercialMaintInfoExtDc.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadCommercialMaintInfoExtDc")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadCommercialMaintInfoExtDc), forKey: "LeadCommercialMaintInfoExtDc")
        }
    }
    func fetchLeadCommercialDetailExtDcFromCoreData (){
        let arrayLeadCommercialDetailExtDc = getDataFromLocal(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")",strUserName))
        if (arrayLeadCommercialDetailExtDc.count==0)
        {
            dictFinal.setValue("", forKey: "LeadCommercialDetailExtDc")
        }else
        {
            let obj = arrayLeadCommercialDetailExtDc.object(at: 0)as! NSManagedObject
            dictFinal.setValue(getJsonObject(match: obj), forKey: "LeadCommercialDetailExtDc")
        }
    }
    
    func fetchMultiTermsFromCoreDataClarkPest (){
        let arrayLeadCommercialTermsExtDc = getDataFromLocal(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")",strUserName))
        if (arrayLeadCommercialTermsExtDc.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadCommercialTermsExtDc")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadCommercialTermsExtDc), forKey: "LeadCommercialTermsExtDc")
        }
    }
    
    func fetchForAppliedDiscountFromCoreDataClarkPest (){
        let arrayLeadCommercialDiscountExtDc = getDataFromLocal(strEntity: "LeadCommercialDiscountExtDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")",strUserName))
        if (arrayLeadCommercialDiscountExtDc.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadCommercialDiscountExtDc")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadCommercialDiscountExtDc), forKey: "LeadCommercialDiscountExtDc")
        }
    }
    
    func fetchSalesMarketingContentFromCoreDataClarkPest (){
        let arrayLeadMarketingContentExtDc = getDataFromLocal(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")",strUserName))
        if (arrayLeadMarketingContentExtDc.count==0)
        {
            dictFinal.setValue(NSArray(), forKey: "LeadMarketingContentExtDc")
        }else
        {
            dictFinal.setValue(getJsonArray(arrData: arrayLeadMarketingContentExtDc), forKey: "LeadMarketingContentExtDc")
        }
    }
    
    
        
        
    //MARK:-------------------
    //MARK: - CoreData Related Function
    
    //MARK:------FOR Email-------

    func SaveLeadEmailInDataBase(strEmail : String , strSubject : String , strLeadID : String)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
        
        arrOfKeys.add("emailId")
        arrOfValues.add(strEmail)
        
        arrOfKeys.add("isCustomerEmail")
        arrOfValues.add("true")

        arrOfKeys.add("isMailSent")
        arrOfValues.add("true")

        arrOfKeys.add("leadMailId")
        arrOfValues.add("")

        arrOfKeys.add("isDefaultEmail")
        arrOfValues.add("false")

        arrOfKeys.add("subject")
        arrOfValues.add(strSubject)

        arrOfKeys.add("leadId")
        arrOfValues.add(strLeadID)
        
        saveDataInDB(strEntity: "EmailDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        txtEmail.text = ""
        //txtSubject.text = ""
        ReloadEmailList()
    }
    func SaveDefaultMasterEmailInDataBase(strEmail : String , strSubject : String , strLeadID : String)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
        
        arrOfKeys.add("emailId")
        arrOfValues.add(strEmail)
        
        arrOfKeys.add("isCustomerEmail")
        arrOfValues.add("true")

        arrOfKeys.add("isMailSent")
        arrOfValues.add("false")

        arrOfKeys.add("leadMailId")
        arrOfValues.add("")

        arrOfKeys.add("isDefaultEmail")
        arrOfValues.add("true")

        arrOfKeys.add("subject")
        arrOfValues.add(strSubject)

        arrOfKeys.add("leadId")
        arrOfValues.add(strLeadID)
        
        saveDataInDB(strEntity: "EmailDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        ReloadEmailList()
    }
    
    func deleteDefaultEmailFromDB(object : NSManagedObject) {
        deleteDataFromDB(obj: object)
    }
    
    func UpdateSendMailStatusInDataBase(index : Int)
    {
        let dict = (arySendMailList.object(at: index)as! NSManagedObject)
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("isMailSent")
        
        let  isMailSent = "\(dict.value(forKey: "isMailSent")!)".lowercased()
        if(isMailSent == "true" || isMailSent == "1"){
            arrOfValues.add("false")
        }else{
            arrOfValues.add("true")
        }
    
        let isSuccess = getDataFromDbToUpdate(strEntity: "EmailDetail", predicate: NSPredicate(format: "userName == %@ && emailId == %@ && leadId == %@",Global().getUserName(), "\(dict.value(forKey: "emailId")!)", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        if isSuccess {
            //Update Modify Date In Work Order DB
            Global().updateSalesModifydate("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
            ReloadEmailList()
        }
    }
    func updateEmailSubject()
    {
        
        let arrayEmailDetail = getDataFromLocal(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        
        for item in arrayEmailDetail
        {
            let matches = item as! NSManagedObject
            let strEmailId = "\(matches.value(forKey: "emailId") ?? "")"
            
            if strEmailId != "" && strEmailId.count > 0
            {
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("subject")
                arrOfValues.add(txtSubject.text ?? "")
              

                let isSuccess = getDataFromDbToUpdate(strEntity: "EmailDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && emailId == %@",strUserName, strLeadId, strEmailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    Global().updateSalesModifydate("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
                    
                }
            }
           
        }
        ReloadEmailList()
    }
    
    func ReloadEmailList()  {
        let arrayEmailDetail = getDataFromLocal(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        self.arySendMailList = NSMutableArray()
        self.arySendMailList = arrayEmailDetail.mutableCopy() as! NSMutableArray
        self.tvList.reloadData()
    }
    
    func CheckEmailAlreadyExist(strEmail : String) -> Bool {
        let arrayEmailDetail = getDataFromLocal(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        let aryTemp = arrayEmailDetail.filter { (task) -> Bool in
            return "\((task as! NSManagedObject).value(forKey: "emailId")!)".contains(strEmail) }
        if(aryTemp.count != 0){
            return true
        }
        return false
    }
    
    
    func CheckResendAgreementProposalMail() -> Bool {
        
        let arrayisResendAgreementProposalMail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")"))
        if arrayisResendAgreementProposalMail.count != 0 {
            let item = arrayisResendAgreementProposalMail.object(at: 0) as! NSManagedObject
            if "\(item.value(forKey: "isResendAgreementProposalMail")!)".lowercased() == "true" || "\(item.value(forKey: "isResendAgreementProposalMail")!)" == "1" {
                return true
            }else{
                return false
                
            }
        }
        return false
    }
    
    
    //MARK: -------FOR Text-------
    func CheckTextAlreadyExist(strText : String) -> Bool {
        let arrayTextDetail = getDataFromLocal(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        let aryTemp = arrayTextDetail.filter { (task) -> Bool in
            return "\((task as! NSManagedObject).value(forKey: "contactNumber")!)".contains(strText.replacingOccurrences(of: "-", with: "")) }
        if(aryTemp.count != 0){
            return true
        }
        return false
    }
 
    func SaveLeadTextInDataBase(strText : String , strSubject : String , strLeadID : String)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
      
        arrOfKeys.add("contactNumber")
        arrOfValues.add(strText.replacingOccurrences(of: "-", with: ""))
        arrOfKeys.add("isCustomerNumber")
        arrOfValues.add("true")
        arrOfKeys.add("isMessageSent")
        arrOfValues.add("true")
        arrOfKeys.add("leadContactId")
        arrOfValues.add("")
        arrOfKeys.add("isDefaultNumber")
        arrOfValues.add("true")
        arrOfKeys.add("subject")
        arrOfValues.add(strSubject)
        arrOfKeys.add("leadId")
        arrOfValues.add(strLeadID)
        
        saveDataInDB(strEntity: "LeadContactDetailExtSerDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        txtPhone.text = ""
        CallforTextAndReloadOnly()

    }

    func UpdateSendTextStatusInDataBase(index : Int)
    {
        let dict = (arySendTextList.object(at: index)as! NSManagedObject)
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("isMessageSent")
        
        let  isMailSent = "\(dict.value(forKey: "isMessageSent")!)".lowercased()
        if(isMailSent == "true" || isMailSent == "1"){
            arrOfValues.add("false")
        }else{
            arrOfValues.add("true")
        }
    
        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "userName == %@ && contactNumber == %@",Global().getUserName(), "\(dict.value(forKey: "contactNumber")!)".replacingOccurrences(of: "-", with: "")), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        if isSuccess {
            //Update Modify Date In Work Order DB
            Global().updateSalesModifydate("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")")
            CallforTextAndReloadOnly()
        }
    }
    func getSendFinalMessage() -> String {
  
        //strClickHereUrl ka data nikalna hai  Yad se
        var strAgreementUrl = "" , strProposalUrl = "" , strInspectionUrl = "" , strSalesUrl = "" , strCustomerNotPresentUrl = "", strFinalMessage = "" //, strClickHereUrl = ""
        let dictLogin = nsud.value(forKey: "LoginDetails")as! NSDictionary
        strSalesUrl = "\(dictLogin.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        let dictPaymentData = dictFinal.value(forKey: "DocumentsDetail") is NSDictionary ?  (dictFinal.value(forKey: "DocumentsDetail")as! NSDictionary) : NSDictionary()
        if dictPaymentData.count != 0 {
            strAgreementUrl = "\n\(strSalesUrl)//Documents/\n\(dictPaymentData.value(forKey: "agreement") ?? "")"
            strProposalUrl = "\n\(strSalesUrl)//Documents/\n\(dictPaymentData.value(forKey: "proposal") ?? "")"
            strInspectionUrl = "\n\(strSalesUrl)//Documents/\n\(dictPaymentData.value(forKey: "inspection") ?? "")"
        }
        strCustomerNotPresentUrl = "\n\(MainUrlWeb)/\(strClickHereUrl)"

        if "\(matchesGeneralInfo.value(forKey: "isCustomerNotPresent") ?? "")" == "true"
        {
              strFinalMessage = self.getMessageFromMasterSalesAutomation(strType: "Customer Signature Link")
        }
        else
        {
            var strProposal = ""
            let leadDetail = matchesGeneralInfo as NSManagedObject//self.dictFinal.value(forKey: "LeadDetail") is NSManagedObject ? self.dictFinal.value(forKey: "LeadDetail") as! NSManagedObject : NSManagedObject()
            if(leadDetail.value(forKey: "isProposalFromMobile") != nil){
                let isProposalFromMobile = "\(leadDetail.value(forKey: "isProposalFromMobile") ?? "")"
                if isProposalFromMobile.lowercased() == "true" {
                    strProposal = "Proposal"
                }else{
                    strProposal = "Signed Agreements"
                }
            }
            strFinalMessage = self.getMessageFromMasterSalesAutomation(strType: strProposal)
        }
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##CustomerSignatureLink##", with: strCustomerNotPresentUrl)
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##AgreementPDFLink##", with: strAgreementUrl)
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##ProposalPDFLink##", with: strProposalUrl)
        strFinalMessage = strFinalMessage.replacingOccurrences(of: "##InspectionPDFLink##", with: strInspectionUrl)
        return strFinalMessage
        
    }
    
    func getMessageFromMasterSalesAutomation(strType : String) -> String{
       var message = ""
        var arrTextTemplatesMaster = NSMutableArray()
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") is NSDictionary ? nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary : NSDictionary()
        if dictMasters.count != 0 {
            if dictMasters.value(forKey: "TextTemplatesSelectListDcs") is NSArray{
                arrTextTemplatesMaster = (dictMasters.value(forKey: "TextTemplatesSelectListDcs")as! NSArray).mutableCopy() as! NSMutableArray
            }
        }
        let aryTemp = arrTextTemplatesMaster.filter { (task) -> Bool in
            return "\((task as! NSDictionary).value(forKey: "TemplateName")!)".contains(strType) } as NSArray
        if aryTemp.count != 0 {
            let dict = aryTemp.object(at: 0)as! NSDictionary
            message = "\(dict.value(forKey: "TemplateContent") ?? "")"
        }
        
        
        let dictLogin = nsud.value(forKey: "LoginDetails") as! NSDictionary
     
        
        
        //let leadDetail = self.dictFinal.value(forKey: "LeadDetail") is NSManagedObject ? self.dictFinal.value(forKey: "LeadDetail") as! NSManagedObject : NSManagedObject()
        
        let leadDetail = matchesGeneralInfo as NSManagedObject
        
        var strCustomerName = "" , strCustomerCompanyName = "" , strAccountNo = ""
        
        if(leadDetail.value(forKey: "isProposalFromMobile") != nil){
            strCustomerName = "\(leadDetail.value(forKey: "customerName") ?? "")"
            strCustomerCompanyName = "\(leadDetail.value(forKey: "companyName") ?? "")"
            strAccountNo  = "\(leadDetail.value(forKey: "accountNo") ?? "")"
        }
        
        message = message.replacingOccurrences(of: "##EmployeeName##", with: "\(dictLogin.value(forKey: "EmployeeName") ?? "")")
        message = message.replacingOccurrences(of: "##EmployeEmail##",with: "\(dictLogin.value(forKey: "EmployeeEmail") ?? "")")
        message = message.replacingOccurrences(of: "##EmployeePhone##",with: "\(dictLogin.value(forKey: "EmployeePrimaryPhone") ?? "")")
        message = message.replacingOccurrences(of: "##CompanyName##", with: "\(dictLogin.value(forKeyPath: "Company.Name") ?? "")")
        message = message.replacingOccurrences(of: "##AccountNo##", with: strAccountNo)
        message = message.replacingOccurrences(of: "##CustomerCompanyName##", with: strCustomerCompanyName)
        message = message.replacingOccurrences(of: "##CustomerName##", with: strCustomerName)
        
        return message
    }
    
    func openMessageComposer(strMessage : String)  {
        guard MFMessageComposeViewController.canSendText() else {
            return
        }
        let messageVC = MFMessageComposeViewController()
        let arrayTextDetail = getDataFromLocal(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))
        let aryTemp = arrayTextDetail.filter { (task) -> Bool in
            return "\((task as! NSManagedObject).value(forKey: "isMessageSent")!)".lowercased().contains("true") }
        let aryNumber = NSMutableArray()
        for item in aryTemp {
            aryNumber.add("\((item as AnyObject).value(forKey: "contactNumber")!)")
        }
        
        messageVC.body = strMessage
        messageVC.recipients = aryNumber as? [String]
        messageVC.messageComposeDelegate = self;
        
        self.present(messageVC, animated: false, completion: nil)
    }
    //MARK: -------FOR Document-------

    func fatchDocumentList() -> NSMutableArray {
        self.arySendDocumentList = NSMutableArray()
        let arySendDocumentList_Temp = NSMutableArray()
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary

        let aryDocumentTemp = dictMasters.value(forKey: "OtherDocumentsMasterExts") as! NSArray
        let arrCatergory = dictMasters.value(forKey: "Categories") as! NSArray
        for item in self.fetchSoldServiceToGetDocument() {
            let ServiceId = "\(((item as AnyObject) as! NSManagedObject).value(forKey: "ServiceId")!)"
            let aryTemp = arrCatergory.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "ServiceId")!)".contains(ServiceId)
            }
            
            if aryTemp.count != 0 {
                let dict = (aryTemp as NSArray).object(at: 0) as! NSDictionary
                let strDepartmentSysName = "\(dict.value(forKey: "DepartmentSysName")!)"
                let strCategorySysName = "\(dict.value(forKey: "SysName")!)"
                //var aryService = dict.value(forKey: "Services") as! NSArray
                
                
                let strSysName = "\(dict.value(forKey: "SysName")!)"

                //----DepartmentSysName--

                let aryTempDepartmentSysName = aryDocumentTemp.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "DepartmentSysName")!)".contains(strDepartmentSysName)
                }
                if aryTempDepartmentSysName.count != 0 {
                    arySendDocumentList_Temp.add((aryTempDepartmentSysName as NSArray).object(at: 0))
                }
                
                //----CategorySysName--
                let aryTempCategorySysName = aryDocumentTemp.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "CategorySysName")!)".contains(strCategorySysName)
                }
                if aryTempCategorySysName.count != 0 {
                    arySendDocumentList_Temp.add((aryTempCategorySysName as NSArray).object(at: 0))
                }
                
                //----ServiceId--
                let aryTempServiceId = aryDocumentTemp.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "ServiceId")!)".contains(ServiceId)
                }
                if aryTempServiceId.count != 0 {
                    arySendDocumentList_Temp.add((aryTempServiceId as NSArray).object(at: 0))
                }
                
                
                let uniqueUnordered = Array(Set(arrayLiteral: arySendDocumentList_Temp))
                self.arySendDocumentList = (uniqueUnordered as NSArray).mutableCopy() as! NSMutableArray
                
                //---------CategorySysName ServiceSysName--------

                let aryTempCategoryAndServiceSysName = self.arySendDocumentList.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "CategorySysName")!)".contains(strCategorySysName) &&  ("\((task as! NSManagedObject).value(forKey: "ServiceSysName")!)".contains("") || "\((task as! NSManagedObject).value(forKey: "ServiceSysName")!)".contains("0"))
                }
                if aryTempCategoryAndServiceSysName.count != 0 {
                    arySendDocumentList_Temp.add((aryTempCategoryAndServiceSysName as NSArray).object(at: 0))
                }
                let aryTempCategoryAndServiceSysName1 = self.arySendDocumentList.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "ServiceSysName")!)".contains(strSysName) &&  "\((task as! NSManagedObject).value(forKey: "CategorySysName")!)".contains(strCategorySysName)
                }
                if aryTempCategoryAndServiceSysName1.count != 0 {
                    arySendDocumentList_Temp.add((aryTempCategoryAndServiceSysName1 as NSArray).object(at: 0))
                }
                
                let aryTempCategoryAndServiceSysName2 = self.arySendDocumentList.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "ServiceSysName")!)".contains("") &&  "\((task as! NSManagedObject).value(forKey: "CategorySysName")!)".contains("")
                }
                if aryTempCategoryAndServiceSysName2.count != 0 {
                    arySendDocumentList_Temp.add((aryTempCategoryAndServiceSysName2 as NSArray).object(at: 0))
                }
            }
        }
        return self.arySendDocumentList
    }

    func fetchSoldServiceToGetDocument() -> NSArray{
        let array_SoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , (strForSendProposal == "forSendProposal") ? "false" : "true"))
       return array_SoldServiceStandard
    }
    func fetchSoldServiceToGetDocumentFor_NonStanDard() -> NSArray{
        let array_SoldServiceNonStandardDetail = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , (strForSendProposal == "forSendProposal") ? "false" : "true"))
       return array_SoldServiceNonStandardDetail
    }
        
    //MARK:------------------ Other Documents Function -------------
    
    func fetchOtherDocumentFromMaster() -> NSArray  {
        
        //let arrDept = getBranchWiseDepartment(strBranchSysName: strBranchSysName) as! NSArray
        var arrOtherDocuments = NSArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "OtherDocumentsMasterExts") is NSArray){
                
                arrOtherDocuments = dictMaster.value(forKey: "OtherDocumentsMasterExts") as! NSArray
            }
        }
        
        let arrAllDocumentsDeptCatgServiceWise = NSMutableArray()

        let arrAllDocuments = NSMutableArray()
        
        //For Sold Service
        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, isSendProposal ? "false" : "true"))

        
        
        //Fetch Documets for Sold Serivce Department, Category and Service Wise
        
        for item in arraySoldServiceStandard
        {
            let match = item as! NSManagedObject
            let strServiceId = "\(match.value(forKey: "serviceId") ?? "")"
            let strServiceSysName = "\(match.value(forKey: "serviceSysName") ?? "")"
            let dictCategory = getCategoryObjectFromServiceSysName(strServiceSysName: strServiceSysName)
            let strCategorySysName = "\(dictCategory.value(forKey: "SysName") ?? "")"
            let strDeptSysName =  getDeptSysNameFromServiceSysName(strServiceSysName: "\(match.value(forKey: "serviceSysName") ?? "")")

            
            for itemDoc in arrOtherDocuments
            {
                let dictDoc = itemDoc as! NSDictionary
                
                if "\(dictDoc.value(forKey: "DepartmentSysName") ?? "")" == strDeptSysName || "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == strCategorySysName || "\(dictDoc.value(forKey: "ServiceId") ?? "")" == strServiceId
                {
                    arrAllDocumentsDeptCatgServiceWise.add(dictDoc)
                }
            }
        }
        
        for item in arraySoldServiceStandard
        {
            let match = item as! NSManagedObject
            //let strServiceId = "\(match.value(forKey: "serviceId") ?? "")"
            let strServiceSysName = "\(match.value(forKey: "serviceSysName") ?? "")"
            let dictCategory = getCategoryObjectFromServiceSysName(strServiceSysName: strServiceSysName)
            let strCategorySysName = "\(dictCategory.value(forKey: "SysName") ?? "")"
            //let strDeptSysName =  getDeptSysNameFromServiceSysName(strServiceSysName: "\(match.value(forKey: "serviceSysName") ?? "")")

            for itemDoc in arrAllDocumentsDeptCatgServiceWise
            {
                let dictDoc = itemDoc as! NSDictionary
                
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "" && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == strCategorySysName
                {
                    arrAllDocuments.add(dictDoc)
                }
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == strServiceSysName && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == ""
                {
                    arrAllDocuments.add(dictDoc)
                }
                if ("\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == strServiceSysName || "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "0") && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == ""
                {
                    arrAllDocuments.add(dictDoc)
                }
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == strServiceSysName && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == strCategorySysName
                {
                    arrAllDocuments.add(dictDoc)
                }
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "" && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == ""
                {
                    arrAllDocuments.add(dictDoc)
                }
                if "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "0" && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == ""
                {
                    arrAllDocuments.add(dictDoc)
                }
            }
        }
        
        //Fetch Document for Custom
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, isSendProposal ? "false" : "true"))
        
        for item in arraySoldServiceCustom
        {
            let match = item as! NSManagedObject
            let strDeptSysName = "\(match.value(forKey: "departmentSysname") ?? "")"
            
            for itemDoc in arrOtherDocuments
            {
                let dictDoc = itemDoc as! NSDictionary
                if "\(dictDoc.value(forKey: "DepartmentSysName") ?? "")" == strDeptSysName && "\(dictDoc.value(forKey: "CategorySysName") ?? "")" == "" && ("\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "" || "\(dictDoc.value(forKey: "ServiceSysName") ?? "")" == "0")
                {
                    arrAllDocuments.add(dictDoc)
                }
            }
        }
        
        
        let arr = arrAllDocuments.filter {  (dict) -> Bool in
            
            return "\((dict as! NSDictionary).value(forKey: "BranchSysName") ?? "")" == strBranchSysName
        }
        
        if arr.count > 0
        {
            let arrUniqueDocuments = arr.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
            print("Unique array \(arrUniqueDocuments)")
            
            let arrTitle = NSMutableArray()
            for item in arrUniqueDocuments
            {
                let objDoc = item as! NSDictionary
                arrTitle.add("\(objDoc.value(forKey: "Title") ?? "")")
            }

            let arrUniqueTitle = arrTitle.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
            print("Unique array \(arrUniqueTitle)")
            
            let arrFinalUnique = NSMutableArray()
            
            for item in arrTitle
            {
                let strTitle = item as! String
                
                for itemDoc in arrUniqueDocuments
                {
                    let objDoc = itemDoc as! NSDictionary
                    
                    if "\(objDoc.value(forKey: "Title") ?? "")" == strTitle
                    {
                        arrFinalUnique.add(objDoc)
                        break
                    }
                }
            }
            
            if arrFinalUnique.count > 0
            {
                let arrUniqueDocumentsNew = arrFinalUnique.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
                print("Unique array \(arrUniqueDocuments)")
                
                return arrUniqueDocumentsNew
            }
            else
            {
                return NSArray()
            }
            
        }
        else
        {
            return NSArray()
        }
        
        //let arrUniqueDocuments = arrAllDocuments.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        //print("Unique array \(arrUniqueDocuments)")
        
        //return arrUniqueDocuments

    }
    
    func fetchOtherDocumentsFromDB()  {
        
        let arrAllDocuments = fetchOtherDocumentFromMaster()
        
        arrAllOtherDocuments = arrAllDocuments
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        for item in arrayData
        {
            let matches = item as! NSManagedObject
            
            for itemOtherDoc in arrAllDocuments
            {
                let dictDoc = itemOtherDoc as! NSDictionary
                
                if "\(dictDoc.value(forKey: "OtherDocSysName") ?? "")" == "\(matches.value(forKey: "otherDocSysName") ?? "")"
                {
                    arrSelectedDocuments.add(dictDoc)
                }
            }
        }
        for itemOtherDoc in arrAllDocuments
        {
            let dictDoc = itemOtherDoc as! NSDictionary
            
            if "\(dictDoc.value(forKey: "IsDefault") ?? "")" == "1"
            {
                arrSelectedDocuments.add(dictDoc)
            }
        }
        
        let arrUniqueDocuments = arrSelectedDocuments.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrUniqueDocuments)")
        
        arrSelectedDocuments = NSMutableArray()
        
        arrSelectedDocuments = arrUniqueDocuments.mutableCopy() as! NSMutableArray

    }
    
    func deleteOtherDefaultDocument()  {
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId == %@ && docType == %@", self.strLeadId, "DefaultDocuments"))

        for item in arrayData
        {
            let objTempSales = item as! NSManagedObject
            deleteDataFromDB(obj: objTempSales)

        }
    }
    
    
    func saveDocumentToBeSend()  {
        
        deleteOtherDefaultDocument()
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        for item in arrSelectedDocuments
        {
            let dictDoc = item as! NSDictionary
            
            arrOfKeys = [
                "accountNo",
                "companyKey",
                "createdBy",
                "createdDate",
                "descriptionDocument",
                "docFormatType",
                "docType",
                "fileName",
                "isAddendum",
                "isSync",
                "isToUpload",
                "leadDocumentId",
                "leadId",
                "modifiedBy",
                "modifiedDate",
                "otherDocSysName",
                "scheduleStartDate",
                "title",
                "userName"
            ]
            
            arrOfValues = [
                
                "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")",
                strCompanyKey,
                strUserName,
                "\(Global().getModifiedDate() ?? "")",
                "",
                "docFormatType",
                "DefaultDocuments",
                "\(dictDoc.value(forKey: "OtherDocName") ?? "")",
                "",
                "",
                "",
                "\(dictDoc.value(forKey: "OtherDocumentId") ?? "")",
                strLeadId,
                strUserName,
                "\(Global().getModifiedDate() ?? "")",
                "\(dictDoc.value(forKey: "OtherDocSysName") ?? "")",
                "\(Global().getModifiedDate() ?? "")",
                "\(dictDoc.value(forKey: "Title") ?? "")",
                strUserName
            ]
            
            saveDataInDB(strEntity: "DocumentsDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
    }
    
    //MARK:------------------- Service & Proposal Function -------------
    
    func updateServiceFollowUp(strDate : String, strNotes : String, matchesObject: NSManagedObject)  {
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        if strDate.count > 0
        {
            arrOfKeys = [
                "followupDate"
            ]
            arrOfValues = [
                strDate
            ]
        }
        if strNotes.count > 0
        {
            arrOfKeys = [
                "notes"
            ]
            arrOfValues = [
                strNotes
            ]
        }
        
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceFollowUpDcs", predicate: NSPredicate(format: "leadId == %@ && id == %@",strLeadId, "\(matchesObject.value(forKey: "id") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
    }
    func updateProposalFollowUp(strDate : String, strNotes : String, matchesObject : NSManagedObject)  {
        
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        if strDate.count > 0
        {
            arrOfKeys = [
                "proposalFollowupDate"
            ]
            arrOfValues = [
                strDate
            ]
        }
        if strNotes.count > 0
        {
            arrOfKeys = [
                "proposalNotes"
            ]
            arrOfValues = [
                strNotes
            ]
        }
        
       
        var isSuccess = Bool()
        
        isSuccess =  getDataFromDbToUpdate(strEntity: "ProposalFollowUpDcs", predicate: NSPredicate(format: "proposalLeadId == %@ && proposalId == %@",strLeadId,"\(matchesObject.value(forKey: "proposalId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
    }
    
    func getServicePropsalBasicDetails() {
       
       /* if nsud.value(forKey: "companyDetail") is NSDictionary
        {
        let dictCompanyDetail = nsud.value(forKey: "companyDetail") as! NSDictionary
        
        isProposalFoolowUp = (dictCompanyDetail.value(forKey: "IsProposalFollowUp") ?? false) as! Bool
        isServiceFollowUp = (dictCompanyDetail.value(forKey: "IsServiceFollowUp") ?? false) as! Bool
        serviceFollowDays = "\(dictCompanyDetail.value(forKey: "ServiceFollowUpDays") ?? "")".count > 0 ? (dictCompanyDetail.value(forKey: "ServiceFollowUpDays") ?? 0) as! Int : 0
        proposalFollowDays = "\(dictCompanyDetail.value(forKey: "ProposalFollowUpDays") ?? "")".count > 0 ? (dictCompanyDetail.value(forKey: "ProposalFollowUpDays") ?? 0) as! Int : 0
        }
        
        
        arySalesServiceFollowUpList = getSoldServiceDepartments()
        
        let object = getUnSoldServiceDepartmentsAndServices()
        arySalesProposalFollowUpList = object.arrUnSoldDept
        arySalesProposalServices = object.arrProposalService*/
        
        
        if nsud.value(forKey: "companyDetail") is NSDictionary
        {
            let dictCompanyDetail = nsud.value(forKey: "companyDetail") as! NSDictionary
            
            isProposalFoolowUp = (dictCompanyDetail.value(forKey: "IsProposalFollowUp") ?? false) as! Bool
            isServiceFollowUp = (dictCompanyDetail.value(forKey: "IsServiceFollowUp") ?? false) as! Bool
            serviceFollowDays = "\(dictCompanyDetail.value(forKey: "ServiceFollowUpDays") ?? "")".count > 0 ? (dictCompanyDetail.value(forKey: "ServiceFollowUpDays") ?? 0) as! Int : 0
            proposalFollowDays = "\(dictCompanyDetail.value(forKey: "ProposalFollowUpDays") ?? "")".count > 0 ? (dictCompanyDetail.value(forKey: "ProposalFollowUpDays") ?? 0) as! Int : 0
        }
        
        arySalesServiceFollowUpList = fetchServiceFollowUpFromDB()
        arySalesProposalFollowUpList = fetchProposalFollowUpFromDB()
    }
    
    func fetchServiceFollowUpFromDB() -> NSArray {
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "ServiceFollowUpDcs", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        return arrayData
    }
    func fetchProposalFollowUpFromDB() -> NSArray {
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "ProposalFollowUpDcs", predicate: NSPredicate(format: "proposalLeadId == %@", self.strLeadId))
        
        return arrayData
    }
    
    func getSoldServiceDepartments() -> NSArray {
        
        let arrSoldDept = NSMutableArray()
        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
    
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
        
        
        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            arrSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
        }
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            arrSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
        }


        let arrUniqueDept = arrSoldDept.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrUniqueDept)")
        
        
        return arrUniqueDept
    }
    
    func getUnSoldServiceDepartmentsAndServices() -> (arrUnSoldDept : NSArray, arrProposalService: NSArray) {
        
        let arrUnSoldDept = NSMutableArray()
        let arrUnSoldServiceName = NSMutableArray()

        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
    
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
        
        
        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            arrUnSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
            
            arrUnSoldServiceName.add(getServiceObjectFromIdAndSysName(strId: "", strSysName: strServiceSysName).value(forKey: "Name") ?? "")
        }
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            arrUnSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
            arrUnSoldServiceName.add("\(matches.value(forKey: "serviceName") ?? "")")
        }


        let arrUniqueDept = arrUnSoldDept.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrUniqueDept)")
        
        
        return (arrUniqueDept, arrUnSoldServiceName)
    }
    
    
    func getServiceDetailFromDept(strDepartmentSysName : String) -> String  {
        
        let arrServiceName = NSMutableArray()
        
        //Standard
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))

        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            
            if strDeptSysName == strDepartmentSysName
            {
                let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: strServiceSysName)
                arrServiceName.add("\(dictService.value(forKey: "Name") ?? "")")
            }
        }
        
        //Custom
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            
            if strDeptSysName == strDepartmentSysName
            {
                arrServiceName.add("\(matches.value(forKey: "serviceName") ?? "")")
            }
        }
        
    
        if arrServiceName.count > 0
        {
            return arrServiceName.componentsJoined(by: ", ")
        }
        else
        {
            return ""
        }
    }
    
}

//MARK:-------------------
//MARK: - UITableViewDelegate && UITableViewDelegate
extension SalesNew_SendEmailVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            switch strSendMailStatusFrom {
            case .Email:
                return arySendMailList.count
            case .Text:
                return arySendTextList.count
            default:
                return 0
            }
        case 1:
            
            switch strSendMailStatusFrom {
            
            case .Email:
                return arrAllOtherDocuments.count
            case .Text:
                return 0
            default:
                return 0
            }
        case 2:
            
            switch strSendMailStatusFrom {
            
            case .Email:
                return arySalesServiceFollowUpList.count
            case .Text:
                return 0
            default:
                return 0
            }
            
        case 3:
            
            switch strSendMailStatusFrom {
            
            case .Email:
                return arySalesProposalFollowUpList.count
            case .Text:
                return 0
            default:
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesSendMailCell" : "SalesSendMailCell", for: indexPath as IndexPath) as! SalesSendMailCell
            
            if strSendMailStatusFrom ==  Enum_SalesNewSendMailFrom.Email{
                
                let dict = (arySendMailList.object(at: indexPath.row)as! NSManagedObject)
                cell.btnCheckBox.tag = indexPath.row
                cell.btnCheckBox.addTarget(self, action: #selector(actionCheckBoxEmail), for: .touchUpInside)
                cell.lbl_MailTitle.text = "\(dict.value(forKey: "emailId")!)"
                let  isMailSent = "\(dict.value(forKey: "isMailSent")!)".lowercased()
                if(isMailSent == "true" || isMailSent == "1"){
                    cell.btnCheckBox.setImage(UIImage(named: "checked.png"), for: .normal)
                }else{
                    cell.btnCheckBox.setImage(UIImage(named: ""), for: .normal)

                }
                cell.btnCheckBox.isUserInteractionEnabled = false
                return cell
            }else{
              
                let dict = (arySendTextList.object(at: indexPath.row)as! NSManagedObject)
                cell.lbl_MailTitle.text = formattedNumber(number: "\(dict.value(forKey: "contactNumber")!)")
                
                cell.btnCheckBox.tag = indexPath.row
                cell.btnCheckBox.addTarget(self, action: #selector(actionCheckBoxEmail), for: .touchUpInside)
                let  isMailSent = "\(dict.value(forKey: "isMessageSent")!)".lowercased()
                if(isMailSent == "true" || isMailSent == "1"){
                    cell.btnCheckBox.setImage(UIImage(named: "checked.png"), for: .normal)
                }else{
                    cell.btnCheckBox.setImage(UIImage(named: ""), for: .normal)

                }
                cell.btnCheckBox.isUserInteractionEnabled = false

                return cell
            }
            
            
           
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesDocumentCell" : "SalesDocumentCell", for: indexPath as IndexPath) as! SalesDocumentCell
            
            let dictDoc = arrAllOtherDocuments.object(at: indexPath.row) as! NSDictionary
            cell.lbl_DocumentTitle.text = "\(dictDoc.value(forKey: "Title") ?? "")"
            cell.btnCheckBox.tag = indexPath.row
            cell.btnCheckBox.addTarget(self, action: #selector(actionCheckBoxText), for: .touchUpInside)
            
            var chkSelect = false
            for item in arrSelectedDocuments
            {
                let dict = item as! NSDictionary
                if "\(dict.value(forKey: "OtherDocSysName") ?? "")" == "\(dictDoc.value(forKey: "OtherDocSysName") ?? "")"
                {
                    chkSelect = true
                    break
                }
            }
            if chkSelect
            {
                cell.btnCheckBox.setImage(UIImage(named: "checked.png"), for: .normal)
            }
            else
            {
                cell.btnCheckBox.setImage(UIImage(named: ""), for: .normal)
            }
            cell.btnCheckBox.isUserInteractionEnabled = false
            //let dict = removeNullFromDict(dict: (aryTblData.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
           // cell.lbl_Title.text = "\(dict.value(forKey: "")!)"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesServiceFollowUpCell" : "SalesServiceFollowUpCell", for: indexPath as IndexPath) as! SalesServiceFollowUpCell
            
            let matches = arySalesServiceFollowUpList.object(at: indexPath.row) as! NSManagedObject
            
            cell.lblDeptName.text = "\(matches.value(forKey: "departmentName") ?? "")"
            cell.lblNotes.text = "\(matches.value(forKey: "notes") ?? "")"
            cell.btnFollowUp.tag = indexPath.row
            cell.btnEditNotes.tag = indexPath.row
            cell.btnFollowUp.addTarget(self, action: #selector(btnActionDateForServiceFollowUp), for: .touchUpInside)
            cell.btnEditNotes.addTarget(self, action: #selector(btnActionEditNotesForServiceFollowUp), for: .touchUpInside)
            
            cell.btnFollowUp.setTitle("\(matches.value(forKey: "followupDate") ?? "")", for: .normal)

            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "SalesProposalFollowUpCell" : "SalesProposalFollowUpCell", for: indexPath as IndexPath) as! SalesProposalFollowUpCell

            let matches = arySalesProposalFollowUpList.object(at: indexPath.row) as! NSManagedObject
            
            cell.lblServiceName.text = "\(matches.value(forKey: "proposalServiceName") ?? "")"
            cell.lblNotes.text = "\(matches.value(forKey: "proposalNotes") ?? "")"
            cell.btnFollowUp.tag = indexPath.row
            cell.btnEditNotes.tag = indexPath.row
            cell.btnFollowUp.addTarget(self, action: #selector(btnActionDateForProposalFollowUp), for: .touchUpInside)
            cell.btnEditNotes.addTarget(self, action: #selector(btnActionEditNotesForProposalFollowUp), for: .touchUpInside)
            
            cell.btnFollowUp.setTitle("\(matches.value(forKey: "proposalFollowupDate") ?? "")", for: .normal)

            
            return cell
        }
        
        
       
    }
    @objc func btnActionEditNotesForServiceFollowUp(sender: UIButton)
    {
        isEditServiceFollowUp = true
        isEditProposalFollowUp = false
        let matches = arySalesServiceFollowUpList.object(at: sender.tag) as! NSManagedObject
        goToNoramlTextEditor(strText: "\(matches.value(forKey: "notes") ?? "")", matchesObject: matches)
    }
    @objc func btnActionEditNotesForProposalFollowUp(sender: UIButton)
    {
        isEditServiceFollowUp = false
        isEditProposalFollowUp = true
        let matches = arySalesProposalFollowUpList.object(at: sender.tag) as! NSManagedObject
        goToNoramlTextEditor(strText: "\(matches.value(forKey: "proposalNotes") ?? "")",matchesObject: matches)
    }
    @objc func btnActionDateForServiceFollowUp(sender: UIButton)
    {
        isEditServiceFollowUp = true
        isEditProposalFollowUp = false
        
        let matches = arySalesServiceFollowUpList.object(at: sender.tag) as! NSManagedObject
        matchesServiceFollowUp = matches
        let strToday = changeStringDateToGivenFormat(strDate: "\(matches.value(forKey: "followupDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let modifiedDate = dateFormatter.date(from: strToday)
        gotoDatePickerView(sender: sender, strType: "Date" , dateToSet: modifiedDate ?? Date())
        
    }
    @objc func btnActionDateForProposalFollowUp(sender: UIButton)
    {
        isEditServiceFollowUp = false
        isEditProposalFollowUp = true
        let matches = arySalesProposalFollowUpList.object(at: sender.tag) as! NSManagedObject
        matchesProposalFollowUp = matches
        let strToday = changeStringDateToGivenFormat(strDate: "\(matches.value(forKey: "proposalFollowupDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let modifiedDate = dateFormatter.date(from: strToday)
        gotoDatePickerView(sender: sender, strType: "Date" , dateToSet: modifiedDate ?? Date())

    }
    
    func gotoDatePickerView(sender: UIButton, strType:String , dateToSet:Date)  {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = false
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func goToNoramlTextEditor(strText : String, matchesObject: NSManagedObject) {
        
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "SalesClarkPestiPad" : "SalesClarkPestiPhone", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TextEditorNormal") as! TextEditorNormal
        vc.delegate = self
        vc.strfrom = "SalesFollowUp"
        vc.strHtml = strText
        vc.matchesObject = matchesObject
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch strSendMailStatusFrom {
            case Enum_SalesNewSendMailFrom.Text:
                UpdateSendTextStatusInDataBase(index: indexPath.row)
                break
            case Enum_SalesNewSendMailFrom.Email:
                UpdateSendMailStatusInDataBase(index: indexPath.row)
            default:
                break
            }
            tvList.reloadData()
        case 1:
            
            let dictDoc = arrAllOtherDocuments.object(at: indexPath.row) as! NSDictionary
            var chkSelect = false
            for item in arrSelectedDocuments
            {
                let dict = item as! NSDictionary
                if "\(dict.value(forKey: "OtherDocSysName") ?? "")" == "\(dictDoc.value(forKey: "OtherDocSysName") ?? "")"
                {
                    chkSelect = true
                    break
                }
            }
            if chkSelect
            {
                arrSelectedDocuments.remove(dictDoc)
            }
            else
            {
                arrSelectedDocuments.add(dictDoc)
            }
            tvList.reloadData()
            break
        case 2:
            break
        default:
            break
        }
        
    }
  
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            switch strSendMailStatusFrom {
            case Enum_SalesNewSendMailFrom.Text:
                return "Send Text to:"
            case Enum_SalesNewSendMailFrom.Email:
                return "Send Mail to:"
            default:
                return ""
            }
        case 1:
            return "Select Document:"
        case 2:
            return "Service Follow Up:"
        default:
            return "Proposal Follow Up:"
        }
    }
    

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        switch section {
        case 0:
            switch strSendMailStatusFrom {
            
            case Enum_SalesNewSendMailFrom.Email:
                return arySendMailList.count != 0 ?  (DeviceType.IS_IPAD ? 60 : 40) : 0
            case Enum_SalesNewSendMailFrom.Text:
                return arySendTextList.count != 0 ?  (DeviceType.IS_IPAD ? 60 : 40) : 0
            default:
                return 0
            }
            
        case 1:
            if strSendMailStatusFrom == .Email
            {
                return arrAllOtherDocuments.count != 0 ?  (DeviceType.IS_IPAD ? 60 : 40) : 0

            }
            else
            {
                return 0
            }
        case 2:
            
            if strSendMailStatusFrom == .Email
            {
                return arySalesServiceFollowUpList.count != 0 ?  (DeviceType.IS_IPAD ? 60 : 40) : 0
            }
            else
            {
                return 0
            }
            
        default:
            
            if strSendMailStatusFrom == .Email
            {
                return arySalesProposalFollowUpList.count != 0 ?  (DeviceType.IS_IPAD ? 60 : 40) : 0
            }
            else
            {
                return 0
            }
        }
    }
    
    @objc func actionCheckBoxEmail(sender : UIButton) {
        self.view.endEditing(true)
        
    }
    @objc func actionCheckBoxText(sender : UIButton) {
        self.view.endEditing(true)
       
        
    }
}
// MARK: -
// MARK: ----------SalesSendMailCell---------
class SalesSendMailCell: UITableViewCell {
    
    @IBOutlet weak var lbl_MailTitle: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
}
// MARK: -
// MARK: ----------SalesDocumentCell---------
class SalesDocumentCell: UITableViewCell {
    
    @IBOutlet weak var lbl_DocumentTitle: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
}
// MARK: -
// MARK: ----------SalesServiceFollowUpCell---------
class SalesServiceFollowUpCell: UITableViewCell {
        
    @IBOutlet var lblDeptName: UILabel!
    @IBOutlet weak var btnFollowUp: UIButton!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var btnEditNotes: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
}
// MARK: -
// MARK: ----------SalesProposalFollowUpCell---------
class SalesProposalFollowUpCell: UITableViewCell {
    
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet weak var btnFollowUp: UIButton!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var btnEditNotes: UIButton!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
    }
}
//MARK: -------------------------------------
//MARK: -----UITextFieldDelegate

extension SalesNew_SendEmailVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = textField.text!
                strTextt = String(strTextt.dropLast())
                textField.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
      return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 249)
    }
    
}



//MARK: -------------------------------------
//MARK: -----MFMessageComposeViewControllerDelegate
extension SalesNew_SendEmailVC : MFMessageComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
          switch (result.rawValue) {
              case MessageComposeResult.cancelled.rawValue:
              print("Message was cancelled")
              self.dismiss(animated: true, completion: nil)
          case MessageComposeResult.failed.rawValue:
              print("Message failed")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.sendingContactDetailDataToServer(dictToSend: self.getLeadContactData(), strLeadIdToSend: self.strLeadId)

            }
              self.dismiss(animated: true, completion: nil)
          case MessageComposeResult.sent.rawValue:
              print("Message was sent")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.sendingContactDetailDataToServer(dictToSend: self.getLeadContactData(), strLeadIdToSend: self.strLeadId)

            }
            
              self.dismiss(animated: true, completion: nil)
          default:
              break;
          }
      }
}
// MARK: - -------------- Normal Text Editor --------------


extension SalesNew_SendEmailVC: TextEditorDelegate
{
    func didReceiveEditorText(strText: String) {
        
        
    }
    func didReceiveEditorTextWithObject(strText : String, object: NSManagedObject)
    {
        if isEditServiceFollowUp
        {
            self.updateServiceFollowUp(strDate: "", strNotes: strText, matchesObject: object)
        }
        if isEditProposalFollowUp
        {
            self.updateProposalFollowUp(strDate: "", strNotes: strText, matchesObject: object)
        }
        getServicePropsalBasicDetails()
        tvList.reloadData()
    }
}

extension SalesNew_SendEmailVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        print(strDate)
        var strDateFinal = strDate
        
        let strToday = changeStringDateToGivenFormat(strDate: Global().strCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let dateToday =  dateFormatter.date(from: strToday)
        let dateFromPicker = dateFormatter.date(from: strDate)
        
        if dateFromPicker!.compare(dateToday!) == .orderedAscending
        {
            strDateFinal = strToday
        }
        
        if isEditServiceFollowUp
        {
            self.updateServiceFollowUp(strDate: strDateFinal, strNotes: "", matchesObject: matchesServiceFollowUp)
        }
        if isEditProposalFollowUp
        {
            self.updateProposalFollowUp(strDate: strDateFinal, strNotes: "", matchesObject: matchesProposalFollowUp)
        }
        getServicePropsalBasicDetails()
        tvList.reloadData()
       
    }
}
extension SalesNew_SendEmailVC: WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        self.loader.dismiss(animated: false)
        print("Loading finished")
    }
}
