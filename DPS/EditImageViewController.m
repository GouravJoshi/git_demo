//
//  EditImageViewController.m
//  DPS dfgd
//
//  Created by Saavan Patidar on 19/04/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  Saavan Patidar 2021

#import "EditImageViewController.h"
#import "Global.h"
#import "Header.h"

@interface EditImageViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    BOOL isSignImage;
    UIView *viewForSign;
    NSString *strSignImageName,*strImagePathToEdit;
    NSData *signImageData;
    BOOL image;
    Global *global;
    UITableView *tblData;
    NSMutableArray *arrDataTblView,*arrOfColor;
    UIView *viewForDate;
    UIView *viewBackGround;
}

@end

@implementation EditImageViewController

//Nilind For Signature 8 Sep
@synthesize mySignatureImage;
@synthesize lastContactPoint1, lastContactPoint2, currentPoint;
@synthesize imageFrame;
@synthesize fingerMoved;
//..........................
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================

    NSUserDefaults *defsImageName=[NSUserDefaults standardUserDefaults];
    strImagePathToEdit=[defsImageName valueForKey:@"editImagePath"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImagePathToEdit]];
    UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];
    _imageToEdit=imageEdit;
    
    global=[[Global alloc]init];
    
    viewForSign=[[UIView alloc]init];
    viewForSign.frame=CGRectMake(2, _btnCancel.frame.origin.y+_btnCancel.frame.size.height+50 , [UIScreen mainScreen].bounds.size.width-4, [UIScreen mainScreen].bounds.size.width-4);
    //viewForSign.frame=CGRectMake(2, 200 , [UIScreen mainScreen].bounds.size.width-4, [UIScreen mainScreen].bounds.size.width-4);

    
    //....................
#pragma mark- TEMP nilind 8 septs
    
    isSignImage=false;
    mySignatureImage.image = nil;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    CGFloat screenWidth = screenRect.size.width;
    
    mySignatureImage.image = _imageToEdit;
    imageFrame = CGRectMake(0,0,viewForSign.bounds.size.width,viewForSign.bounds.size.height);
    [viewForSign.layer setCornerRadius:5.0f];
    [viewForSign.layer setBorderColor:[UIColor blackColor].CGColor];
    [viewForSign.layer setBorderWidth:0.8f];
    [viewForSign.layer setShadowColor:[UIColor blackColor].CGColor];
    [viewForSign.layer setShadowOpacity:0.3];
    [viewForSign.layer setShadowRadius:3.0];
    [viewForSign.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    //allocate an image view and add to the main view
    mySignatureImage = [[UIImageView alloc] initWithImage:nil];
    mySignatureImage.frame = imageFrame;
    mySignatureImage.backgroundColor = [UIColor whiteColor];
    [viewForSign addSubview:mySignatureImage];
    [self.view addSubview:viewForSign];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    mySignatureImage.image = nil;
    mySignatureImage.image = _imageToEdit;
   // mySignatureImage.contentMode=UIViewContentModeScaleAspectFit;
    //......................................
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- SIGNATURE CODE 8 SEPT
//when one or more fingers touch down in a view or window
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //did our finger moved yet?
    fingerMoved = NO;
    UITouch *touch = [touches anyObject];
    
    //    //just clear the image if the user tapped twice on the screen
    //    if ([touch tapCount] == 2) {
    //        mySignatureImage.image = nil;
    //        return;
    //    }
    
    //we need 3 points of contact to make our signature smooth using quadratic bezier curve
    currentPoint = [touch locationInView:mySignatureImage];
    lastContactPoint1 = [touch previousLocationInView:mySignatureImage];
    lastContactPoint2 = [touch previousLocationInView:mySignatureImage];
    
}


//when one or more fingers associated with an event move within a view or window
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //well its obvious that our finger moved on the screen
    fingerMoved = YES;
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass: [_viewSignature class]]) {
        NSLog(@"finger moved on imageview");
        isSignImage=true;
        //your touch was in a uipickerview ... do whatever you have to do
    }
    //save previous contact locations
    lastContactPoint2 = lastContactPoint1;
    lastContactPoint1 = [touch previousLocationInView:mySignatureImage];
    //save current location
    currentPoint = [touch locationInView:mySignatureImage];
    
    //find mid points to be used for quadratic bezier curve
    CGPoint midPoint1 = [self midPoint:lastContactPoint1 withPoint:lastContactPoint2];
    CGPoint midPoint2 = [self midPoint:currentPoint withPoint:lastContactPoint1];
    
    //create a bitmap-based graphics context and makes it the current context
    UIGraphicsBeginImageContext(imageFrame.size);
    
    //draw the entire image in the specified rectangle frame
    [mySignatureImage.image drawInRect:CGRectMake(0, 0, imageFrame.size.width, imageFrame.size.height)];
    
    //set line cap, width, stroke color and begin path
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    float widthValue=[[defs valueForKey:@"widthForLine"] floatValue];
    NSString *strColor=[defs valueForKey:@"colorForLine"];

    if (!widthValue) {
        widthValue=3.0f;
    }
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), widthValue);

    if ([strColor isEqualToString:@"Red"]) {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 0.0, 0.0, 1.0);
    }else if ([strColor isEqualToString:@"Green"]) {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 1.0, 0.0, 1.0);
    }else if ([strColor isEqualToString:@"Blue"]) {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 1.0, 1.0);
    }else if ([strColor isEqualToString:@"White"]) {//White
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 1.0, 1.0, 1.0);
    }else if ([strColor isEqualToString:@"Black"]) {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
    }else {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 1.0, 1.0);
    }
    
    
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    
    //begin a new new subpath at this point
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), midPoint1.x, midPoint1.y);
    //create quadratic Bézier curve from the current point using a control point and an end point
    CGContextAddQuadCurveToPoint(UIGraphicsGetCurrentContext(),
                                 lastContactPoint1.x, lastContactPoint1.y, midPoint2.x, midPoint2.y);
    
    //set the miter limit for the joins of connected lines in a graphics context
    CGContextSetMiterLimit(UIGraphicsGetCurrentContext(), 2.0);
    
    //paint a line along the current path
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    //set the image based on the contents of the current bitmap-based graphics context
    mySignatureImage.image = UIGraphicsGetImageFromCurrentImageContext();
    
    //remove the current bitmap-based graphics context from the top of the stack
    UIGraphicsEndImageContext();
    
    //lastContactPoint = currentPoint;
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // UITouch *touch = [touches anyObject];
    
    //if the finger never moved draw a point
    if(!fingerMoved) {
        // mySignatureImage.image=nil;
        NSLog(@"finger not moved...!!!");
        
    }
}

//calculate midpoint between two points
- (CGPoint) midPoint:(CGPoint )p0 withPoint: (CGPoint) p1 {
    return (CGPoint) {
        (p0.x + p1.x) / 2.0,
        (p0.y + p1.y) / 2.0
    };
}
- (IBAction)actionOnSaveSign:(id)sender
{
    if (mySignatureImage.image==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please add Sign to proceed further." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImagePathToEdit]];
        signImageData = UIImagePNGRepresentation(mySignatureImage.image);
        [signImageData writeToFile:path atomically:YES];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Sign Saved" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
//        [alert show];
//        
//        [global autoDismissAlerView:alert];
        
        
    }
    
}
- (IBAction)actionOnClearSign:(id)sender
{
    mySignatureImage.image = nil;
    
    NSUserDefaults *defsImageName=[NSUserDefaults standardUserDefaults];
    strImagePathToEdit=[defsImageName valueForKey:@"editImagePath"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImagePathToEdit]];
    UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];
    _imageToEdit=imageEdit;

    mySignatureImage.image = _imageToEdit;
    strSignImageName=nil;
    signImageData=nil;
    isSignImage=false;
    image=YES;
    
}
- (IBAction)actionOnExitSign:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)action_SelectColor:(id)sender {
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    arrDataTblView=[[NSMutableArray alloc]init];
    arrOfColor=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObject:@"Red"];
    [arrDataTblView addObject:@"Green"];
    [arrDataTblView addObject:@"Blue"];
    [arrDataTblView addObject:@"Black"];
    [arrDataTblView addObject:@"White"];
    
    UIColor *colorRed=[UIColor redColor];
    UIColor *colorGreen=[UIColor greenColor];
    UIColor *colorBlue=[UIColor blueColor];
    UIColor *colorBlack=[UIColor blackColor];
    UIColor *colorWhite=[UIColor whiteColor];
    
    [arrOfColor addObject:colorRed];
    [arrOfColor addObject:colorGreen];
    [arrOfColor addObject:colorBlue];
    [arrOfColor addObject:colorBlack];
    [arrOfColor addObject:colorWhite];
    
    
    tblData.tag=101;
    [self tableLoad:tblData.tag];
    
}

- (IBAction)action_SelectWidth:(id)sender {
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    arrDataTblView=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObject:@"1.0f"];
    [arrDataTblView addObject:@"2.0f"];
    [arrDataTblView addObject:@"3.0f"];
    [arrDataTblView addObject:@"4.0f"];
    [arrDataTblView addObject:@"5.0f"];
    [arrDataTblView addObject:@"6.0f"];
    [arrDataTblView addObject:@"7.0f"];

    
    tblData.tag=102;
    [self tableLoad:tblData.tag];
}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}

//============================================================================
//============================================================================
#pragma mark- ------------METHODS--------------
//============================================================================
//============================================================================
-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    tblData.frame=CGRectMake(2, _btnCancel.frame.origin.y+_btnCancel.frame.size.height+50, [UIScreen mainScreen].bounds.size.width-4, [UIScreen mainScreen].bounds.size.width-4);
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width, 42)];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(doneMethod) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGround addSubview:btnDone];

    
}

-(void)doneMethod{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];

    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrDataTblView count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (!(arrDataTblView.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 101:
            {
                cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                UIColor *color=[arrOfColor objectAtIndex:indexPath.row];
                cell.textLabel.textColor=color;
                if ([[arrDataTblView objectAtIndex:indexPath.row] isEqualToString:@"White"]) {
                    
                    cell.textLabel.textColor=[UIColor blackColor];
                  //  cell.textLabel.backgroundColor=[UIColor blackColor];
                    
                }else{
                    
                    UIColor *color=[arrOfColor objectAtIndex:indexPath.row];
                    cell.textLabel.textColor=color;
                    
                }
                break;
            }
            case 102:
            {
                cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                cell.textLabel.textColor=[UIColor blackColor];
                break;
            }
            default:
                break;
        }
        
    }
    if ([UIScreen mainScreen].bounds.size.height==667) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }
    else
        cell.textLabel.font=[UIFont systemFontOfSize:15];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!(arrDataTblView.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 101:
            {
                
                NSString *strString=[arrDataTblView objectAtIndex:indexPath.row];
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setValue:strString forKey:@"colorForLine"];
                [defs synchronize];
                
                break;
            }
            case 102:
            {
                
                NSString *strString=[arrDataTblView objectAtIndex:indexPath.row];
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setValue:strString forKey:@"widthForLine"];
                [defs synchronize];

                break;
            }
                
            default:
                break;
        }
        
    }
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}

@end
