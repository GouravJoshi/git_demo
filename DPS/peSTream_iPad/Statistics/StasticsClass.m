//
//  StasticsClass.m
//  DPS
//
//  Created by Rakesh Jain on 22/01/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import "StasticsClass.h"

@implementation StasticsClass
{
    NSMutableDictionary *dictForStartEndDate;
    
    NSString *strFirstQuarterStartDate,*strFirstQuarterEndDate;
    NSString *strSecondQuarterStartDate,*strSecondQuarterEndDate;
    NSString *strThirdQuarterStartDate,*strThirdQuarterEndDate;
    
    BOOL chkForLastQuarter;
}
-(void)getStart_EndDate:(NSString *)dayType
{
    [self firstQuarter];
    [self secondQuarter];
    [self thirdQuarter];
    
    dictForStartEndDate=[[NSMutableDictionary alloc]init];
    
    if ([dayType caseInsensitiveCompare:@"Today"]==NSOrderedSame)
    {
        [dictForStartEndDate setObject:[self getCurrentDate] forKey:@"StartDate"];
        [dictForStartEndDate setObject:[self getCurrentDate] forKey:@"EndDate"];
    }
    else if ([dayType caseInsensitiveCompare:@"Tomorrow"]==NSOrderedSame)
    {
        [dictForStartEndDate setObject:[self getNextDate:[self getCurrentDate]] forKey:@"StartDate"];
        [dictForStartEndDate setObject:[self getNextDate:[self getCurrentDate]] forKey:@"EndDate"];
    }
    else if ([dayType caseInsensitiveCompare:@"Yesterday"]==NSOrderedSame)
    {
        [dictForStartEndDate setObject:[self getPreviousDate:[self getCurrentDate]] forKey:@"StartDate"];
        [dictForStartEndDate setObject:[self getPreviousDate:[self getCurrentDate]] forKey:@"EndDate"];
    }
    else if ([dayType caseInsensitiveCompare:@"This Week"]==NSOrderedSame)
    {
        [self forCurrentWeek];
    }
    else if ([dayType caseInsensitiveCompare:@"Next Week"]==NSOrderedSame)
    {
        [self forNextWeek];
    }
    else if ([dayType caseInsensitiveCompare:@"Last Week"]==NSOrderedSame)
    {
        [self forLastWeek];
    }
    else if ([dayType caseInsensitiveCompare:@"This Month"]==NSOrderedSame)
    {
        [self forCurrentMonth];
    }
    else if ([dayType caseInsensitiveCompare:@"Last Month"]==NSOrderedSame)
    {
        [self forLastMonth];
    }
    else if ([dayType caseInsensitiveCompare:@"This Quarter"]==NSOrderedSame)
    {
        //[self forThisQuarter];
        [self checkForQuarter];
    }
    else if ([dayType caseInsensitiveCompare:@"Last Quarter"]==NSOrderedSame)
    {
        chkForLastQuarter=YES;
        //[self forLastQuarter];
        [self checkForQuarter];
    }
    else if ([dayType caseInsensitiveCompare:@"This Year"]==NSOrderedSame)
    {
        [self forThisYear];
    }
    else if ([dayType caseInsensitiveCompare:@"Last Year"]==NSOrderedSame)
    {
        [self forLastYear:1];
    }
    else if ([dayType caseInsensitiveCompare:@"Last 2 Year"]==NSOrderedSame)
    {
        [self forLastYear:2];
    }
    else if ([dayType caseInsensitiveCompare:@"Last 3 Year"]==NSOrderedSame)
    {
        [self forLastYear:3];
    }
    else if ([dayType caseInsensitiveCompare:@"Last 4 Year"]==NSOrderedSame)
    {
        [self forLastYear:4];
    }
    else if ([dayType caseInsensitiveCompare:@"Last 5 Year"]==NSOrderedSame)
    {
        [self forLastYear:5];
    }
    
    NSLog(@"Date Record for %@ = %@",dayType,dictForStartEndDate);
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setObject:dayType forKey:@"dayType"];
    [defs synchronize];
    
    [_delegate getDelegateData:dictForStartEndDate];
}
-(NSString *)getCurrentDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;
}
-(NSString*)getPreviousDate: (NSString*)strCurrentDate
{
    NSTimeInterval secondsInHours = -(23*60*60 + 59*60 + 59);
    NSString* newUpdatedTime=[self updateHours:[self getCurrentDate] :secondsInHours];
    return newUpdatedTime;
}
-(NSString*)getNextDate: (NSString*)strCurrentDate
{
    NSTimeInterval secondsInHours = (23*60*60 + 59*60 + 59)*2;
    NSString* newUpdatedTime=[self updateHours:[self getCurrentDate] :secondsInHours];
    return newUpdatedTime;
}
-(NSString*)updateHours: (NSString*)strMaxDate : (NSTimeInterval)secondsInHours
{
    NSDateFormatter *dateForMateNew=[[NSDateFormatter alloc]init];
    [dateForMateNew setDateFormat:@"MM/dd/yyyy"];
    
    NSDate *dateEightHoursAhead = [[dateForMateNew dateFromString:strMaxDate] dateByAddingTimeInterval:secondsInHours];
    
    NSString* newTime = [dateForMateNew stringFromDate:dateEightHoursAhead];
    
    NSDate *dateNew=[dateForMateNew dateFromString:newTime];
    
    NSString* strPayPeriodDate=[dateForMateNew stringFromDate:dateNew];
    
    return strPayPeriodDate;
}
-(void)forCurrentWeek
{
    
    dictForStartEndDate=[[NSMutableDictionary alloc]init];
    
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSLog(@"Today date is %@",today);
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];// you can use your format.
    
    //Week Start Date
    
    NSCalendar *gregorian = [[NSCalendar alloc]        initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday |  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay  fromDate:today];
    
    NSInteger dayofweek = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:today] weekday];// this will give you current day of week
    
    [components setDay:([components day] - ((dayofweek) - 2))];// for beginning of the week.
    
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateFormat_first = [[NSDateFormatter alloc] init];
    [dateFormat_first setDateFormat:@"MM/dd/yyyy"];
    
    NSString* dateString2Prev = [dateFormat stringFromDate:beginningOfWeek];
    
    NSDate* weekstartPrev = [dateFormat_first dateFromString:dateString2Prev];
    
    NSLog(@"%@",weekstartPrev);
    
    [dictForStartEndDate setObject:dateString2Prev forKey:@"StartDate"];
    
    //Week End Date
    //NSCalendarIdentifierGregorian
    //  NSCalendar *gregorianEnd = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendar *gregorianEnd = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    // NSDateComponents *componentsEnd = [gregorianEnd components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:today];
    NSDateComponents *componentsEnd = [gregorianEnd components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:today];
    
    NSInteger Enddayofweek = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:today] weekday];// this will give you current day of week
    
    [componentsEnd setDay:([componentsEnd day]+(7-Enddayofweek)+1)];// for end day of the week
    
    NSDate *EndOfWeek = [gregorianEnd dateFromComponents:componentsEnd];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    NSString* dateEndPrev = [dateFormat stringFromDate:EndOfWeek];
    
    NSDate* weekEndPrev = [dateFormat_End dateFromString:dateEndPrev] ;
    NSLog(@"%@",weekEndPrev);
    
    [dictForStartEndDate setObject:dateEndPrev forKey:@"EndDate"];
    
}
-(void)forLastWeek
{
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSDate*dateNew=[today dateByAddingTimeInterval:-(86400*7)];
    NSLog(@"Today date is %@",[dateFormatNew stringFromDate:dateNew]);
    
    //Week Start Date
    
    NSCalendar *gregorian = [[NSCalendar alloc]        initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday |  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay  fromDate:dateNew];
    
    NSInteger dayofweek = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:dateNew] weekday];// this will give you current day of week
    
    [components setDay:([components day] - ((dayofweek) - 2))];// for beginning of the week.
    
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateFormat_first = [[NSDateFormatter alloc] init];
    [dateFormat_first setDateFormat:@"MM/dd/yyyy"];
    
    NSString* dateString2Prev = [dateFormatNew stringFromDate:beginningOfWeek];
    
    NSDate* weekstartPrev = [dateFormat_first dateFromString:dateString2Prev];
    
    NSLog(@"%@",weekstartPrev);
    
    [dictForStartEndDate setObject:dateString2Prev forKey:@"StartDate"];
    
    
    
    NSCalendar *gregorianEnd = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    // NSDateComponents *componentsEnd = [gregorianEnd components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:today];
    NSDateComponents *componentsEnd = [gregorianEnd components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:dateNew];
    
    NSInteger Enddayofweek = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:dateNew] weekday];// this will give you current day of week
    
    [componentsEnd setDay:([componentsEnd day]+(7-Enddayofweek)+1)];// for end day of the week
    
    NSDate *EndOfWeek = [gregorianEnd dateFromComponents:componentsEnd];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    NSString* dateEndPrev = [dateFormatNew stringFromDate:EndOfWeek];
    
    NSDate* weekEndPrev = [dateFormat_End dateFromString:dateEndPrev] ;
    NSLog(@"%@",weekEndPrev);
    
    [dictForStartEndDate setObject:dateEndPrev forKey:@"EndDate"];
    
}

-(void)forCurrentMonth
{
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSLog(@"Today date is %@",today);
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];// you can use your format.
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    
    NSDate *startDateMonth = [calendar dateFromComponents:components];
    NSDateFormatter *dateFormat_Start = [[NSDateFormatter alloc] init];
    [dateFormat_Start setDateFormat:@"MM/dd/yyyy"];
    NSString* dateStartMonth = [dateFormat stringFromDate:startDateMonth];
    [dictForStartEndDate setObject:dateStartMonth forKey:@"StartDate"];
    
    
    //End Date Month
    NSCalendar* calendarEndDay = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* componentsEndDay = [calendarEndDay components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    
    NSRange dayRange = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:today];
    
    [componentsEndDay setDay:dayRange.length];
    [componentsEndDay setHour:23];
    [componentsEndDay setMinute:59];
    [componentsEndDay setSecond:59];
    
    NSDate *endDateMonth = [calendarEndDay dateFromComponents:componentsEndDay];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    NSString* dateEndMonth = [dateFormat stringFromDate:endDateMonth];
    [dictForStartEndDate setObject:dateEndMonth forKey:@"EndDate"];
    
    
}
-(void)forLastMonth
{
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString: strCurrentDate];
    NSLog(@"Today date is %@",today);
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* componentsLastMonth = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:today];
    
    NSInteger dayInMonth = [componentsLastMonth day];
    
    // Update the components, initially setting the day in month to 0
    NSInteger newMonth = ([componentsLastMonth month] - 1);
    [componentsLastMonth setDay:1];
    [componentsLastMonth setMonth:newMonth];
    
    // Determine the valid day range for that month
    NSDate* workingDate = [calendar dateFromComponents:componentsLastMonth];
    NSRange dayRange = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:workingDate];
    
    // Set the day clamping to the maximum number of days in that month
    [componentsLastMonth setDay:MIN(1, dayRange.length)];
    
    
    //Start Date For Last Month
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *startDateMonth = [calendar dateFromComponents:componentsLastMonth];
    NSDateFormatter *dateFormat_Start = [[NSDateFormatter alloc] init];
    [dateFormat_Start setDateFormat:@"MM/dd/yyyy"];
    NSString* dateStartMonth = [dateFormat stringFromDate:startDateMonth];
    [dictForStartEndDate setObject:dateStartMonth forKey:@"StartDate"];
    
    
    //End Date Month
    
    
    [componentsLastMonth setDay:dayRange.length];
    [componentsLastMonth setHour:23];
    [componentsLastMonth setMinute:59];
    [componentsLastMonth setSecond:59];
    
    NSDate *endDateMonth = [calendar dateFromComponents:componentsLastMonth];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    NSString* dateEndMonth = [dateFormat stringFromDate:endDateMonth];
    [dictForStartEndDate setObject:dateEndMonth forKey:@"EndDate"];
    
}
-(void)forThisQuarter
{
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSLog(@"Today date is %@",today);
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* componentsLastMonth = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:today];
    
    NSInteger dayInMonth = [componentsLastMonth day];
    
    // Update the components, initially setting the day in month to 0
    NSInteger newMonth = ([componentsLastMonth month] + 4);
    [componentsLastMonth setDay:1];
    [componentsLastMonth setMonth:newMonth];
    
    // Determine the valid day range for that month
    NSDate* workingDate = [calendar dateFromComponents:componentsLastMonth];
    NSRange dayRange = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:workingDate];
    
    // Set the day clamping to the maximum number of days in that month
    [componentsLastMonth setDay:MIN(1, dayRange.length)];
    
    
    //Start Date For Last Month
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *startDateMonth = [calendar dateFromComponents:componentsLastMonth];
    NSDateFormatter *dateFormat_Start = [[NSDateFormatter alloc] init];
    [dateFormat_Start setDateFormat:@"MM/dd/yyyy"];
    NSString* dateStartMonth = [dateFormat stringFromDate:startDateMonth];
    [dictForStartEndDate setObject:dateStartMonth forKey:@"StartDate"];
    
    //End Date Month
    
    
    [componentsLastMonth setDay:dayRange.length];
    [componentsLastMonth setHour:23];
    [componentsLastMonth setMinute:59];
    [componentsLastMonth setSecond:59];
    
    NSDate *endDateMonth = [calendar dateFromComponents:componentsLastMonth];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    NSString* dateEndMonth = [dateFormat stringFromDate:endDateMonth];
    [dictForStartEndDate setObject:dateEndMonth forKey:@"EndDate"];
    
}
-(void)forLastQuarter
{
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSLog(@"Today date is %@",today);
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* componentsLastMonth = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:today];
    
    NSInteger dayInMonth = [componentsLastMonth day];
    
    // Update the components, initially setting the day in month to 0
    NSInteger newMonth = ([componentsLastMonth month] - 4);
    [componentsLastMonth setDay:1];
    [componentsLastMonth setMonth:newMonth];
    
    // Determine the valid day range for that month
    NSDate* workingDate = [calendar dateFromComponents:componentsLastMonth];
    NSRange dayRange = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:workingDate];
    
    // Set the day clamping to the maximum number of days in that month
    [componentsLastMonth setDay:MIN(1, dayRange.length)];
    
    
    //Start Date For Last Month
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *startDateMonth = [calendar dateFromComponents:componentsLastMonth];
    NSDateFormatter *dateFormat_Start = [[NSDateFormatter alloc] init];
    [dateFormat_Start setDateFormat:@"MM/dd/yyyy"];
    NSString* dateStartMonth = [dateFormat stringFromDate:startDateMonth];
    [dictForStartEndDate setObject:dateStartMonth forKey:@"StartDate"];
    
    //End Date Month
    
    
    [componentsLastMonth setDay:dayRange.length];
    [componentsLastMonth setHour:23];
    [componentsLastMonth setMinute:59];
    [componentsLastMonth setSecond:59];
    
    NSDate *endDateMonth = [calendar dateFromComponents:componentsLastMonth];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    NSString* dateEndMonth = [dateFormat stringFromDate:endDateMonth];
    [dictForStartEndDate setObject:dateEndMonth forKey:@"EndDate"];
    
}
-(void)forThisYear
{
    NSDate *currentYear=[[NSDate alloc]init];
    currentYear=[NSDate date];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    
    //Get first date of current year
    NSString *firstDateString=[NSString stringWithFormat:@"01-01-%@",currentYearString];
    
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"MM/dd/yyyy"];
    NSDate *firstDate = [[NSDate alloc]init];
    firstDate = [formatter2 dateFromString:firstDateString];
    
    NSString* dateStartMonth = [formatter2 stringFromDate:firstDate];
    [dictForStartEndDate setObject:dateStartMonth forKey:@"StartDate"];
    
    
    NSString *lastDateString=[NSString stringWithFormat:@"12-31-%@",currentYearString];
    formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"MM/dd/yyyy"];
    NSDate *lastDate = [[NSDate alloc]init];
    lastDate = [formatter2 dateFromString:lastDateString];
    NSString* dateLastMonth = [formatter2 stringFromDate:lastDate];
    [dictForStartEndDate setObject:dateLastMonth forKey:@"EndDate"];
    
}
-(void)forLastYear:(int)yearValue
{
    NSDate *currentYear=[[NSDate alloc]init];
    currentYear=[NSDate date];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    
    //Get first date of current year
    
    int lastYear=[currentYearString intValue]-yearValue;
    currentYearString=[NSString stringWithFormat:@"%d",lastYear];
    NSString *firstDateString=[NSString stringWithFormat:@"01/01/%@",currentYearString];
    
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"MM/dd/yyyy"];
    NSDate *firstDate = [[NSDate alloc]init];
    firstDate = [formatter2 dateFromString:firstDateString];
    
    NSString* dateStartMonth = [formatter2 stringFromDate:firstDate];
    [dictForStartEndDate setObject:dateStartMonth forKey:@"StartDate"];
    
    
    NSString *lastDateString=[NSString stringWithFormat:@"12/31/%@",currentYearString];
    formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"MM/dd/yyyy"];
    NSDate *lastDate = [[NSDate alloc]init];
    lastDate = [formatter2 dateFromString:lastDateString];
    NSString* dateLastMonth = [formatter2 stringFromDate:lastDate];
    [dictForStartEndDate setObject:dateLastMonth forKey:@"EndDate"];
    
}
-(void)lastQuarter
{
    dictForStartEndDate=[[NSMutableDictionary alloc]init];
    
    NSString *strStartDate=@"01/01/2019";
    NSString *strEndDate=@"04/30/2019";
    
    NSDate *currentYear=[[NSDate alloc]init];
    currentYear=[NSDate date];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    
    int lastYear=[currentYearString intValue]-1;
    currentYearString=[NSString stringWithFormat:@"%d",lastYear];
    //Get first date of current year
    
    strStartDate=[NSString stringWithFormat:@"09/01/%@",currentYearString];
    strEndDate=[NSString stringWithFormat:@"12/31/%@",currentYearString];
    
    //strFirstQuarterStartDate=strStartDate;
    //strFirstQuarterEndDate=strEndDate;
    
    [dictForStartEndDate setObject:strStartDate forKey:@"StartDate"];
    [dictForStartEndDate setObject:strEndDate forKey:@"EndDate"];
    
    
}
-(void)firstQuarter
{
    dictForStartEndDate=[[NSMutableDictionary alloc]init];
    
    NSString *strStartDate=@"01/01/2019";
    NSString *strEndDate=@"04/30/2019";
    
    NSDate *currentYear=[[NSDate alloc]init];
    currentYear=[NSDate date];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    
    //Get first date of current year
    
    strStartDate=[NSString stringWithFormat:@"01/01/%@",currentYearString];
    strEndDate=[NSString stringWithFormat:@"04/30/%@",currentYearString];
    
    strFirstQuarterStartDate=strStartDate;
    strFirstQuarterEndDate=strEndDate;
    
    [dictForStartEndDate setObject:strStartDate forKey:@"StartDate"];
    [dictForStartEndDate setObject:strEndDate forKey:@"EndDate"];
    
    
}
-(void)secondQuarter
{
    dictForStartEndDate=[[NSMutableDictionary alloc]init];
    
    NSString *strStartDate=@"05/01/2019";
    NSString *strEndDate=@"08/31/2019";
    
    NSDate *currentYear=[[NSDate alloc]init];
    currentYear=[NSDate date];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    
    //Get first date of current year
    
    strStartDate=[NSString stringWithFormat:@"05/01/%@",currentYearString];
    strEndDate=[NSString stringWithFormat:@"08/31/%@",currentYearString];
    
    strSecondQuarterStartDate=strStartDate;
    strSecondQuarterEndDate=strEndDate;
    
    [dictForStartEndDate setObject:strStartDate forKey:@"StartDate"];
    [dictForStartEndDate setObject:strEndDate forKey:@"EndDate"];
    
}
-(void)thirdQuarter
{
    dictForStartEndDate=[[NSMutableDictionary alloc]init];
    
    NSString *strStartDate=@"09/01/2019";
    NSString *strEndDate=@"12/31/2019";
    
    NSDate *currentYear=[[NSDate alloc]init];
    currentYear=[NSDate date];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    
    //Get first date of current year
    
    strStartDate=[NSString stringWithFormat:@"09/01/%@",currentYearString];
    strEndDate=[NSString stringWithFormat:@"12/31/%@",currentYearString];
    
    
    strThirdQuarterStartDate=strStartDate;
    strThirdQuarterEndDate=strEndDate;
    
    [dictForStartEndDate setObject:strStartDate forKey:@"StartDate"];
    [dictForStartEndDate setObject:strEndDate forKey:@"EndDate"];
    
}
-(void)checkForQuarter
{
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSLog(@"Today date is %@",today);
    
    
    if(([[dateFormatNew dateFromString:strFirstQuarterStartDate] compare:today]==NSOrderedAscending || [[dateFormatNew dateFromString:strFirstQuarterStartDate] compare:today]==NSOrderedSame) && ([today compare:[dateFormatNew dateFromString:strFirstQuarterEndDate]]==NSOrderedAscending) )
        
    {
        NSLog(@"First Quarter");
        [self firstQuarter];
        
        if (chkForLastQuarter==YES)
        {
            [self lastQuarter];
            chkForLastQuarter=NO;
        }
        
    }
    else if(([[dateFormatNew dateFromString:strSecondQuarterStartDate] compare:today]==NSOrderedAscending || [[dateFormatNew dateFromString:strSecondQuarterStartDate] compare:today]==NSOrderedSame) && ([today compare:[dateFormatNew dateFromString:strSecondQuarterEndDate]]==NSOrderedAscending) )
        
    {
        
        NSLog(@"Second Quarter");
        [self secondQuarter];
        
        if (chkForLastQuarter==YES)
        {
            [self firstQuarter];
            chkForLastQuarter=NO;
        }
        
    }
    else if(([[dateFormatNew dateFromString:strThirdQuarterStartDate] compare:today]==NSOrderedAscending || [[dateFormatNew dateFromString:strThirdQuarterStartDate] compare:today]==NSOrderedSame) && ([today compare:[dateFormatNew dateFromString:strThirdQuarterEndDate]]==NSOrderedAscending) )
        
    {
        
        NSLog(@"Third Quarter");
        [self thirdQuarter];
        
        if (chkForLastQuarter==YES)
        {
            [self secondQuarter];
            chkForLastQuarter=NO;
        }
        
    }
    
    
}
-(void)forNextWeek
{
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSDate*dateNew=[today dateByAddingTimeInterval:+(86400*7)];
    NSLog(@"Today date is %@",[dateFormatNew stringFromDate:dateNew]);
    
    //Week Start Date
    
    NSCalendar *gregorian = [[NSCalendar alloc]        initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday |  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay  fromDate:dateNew];
    
    NSInteger dayofweek = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:dateNew] weekday];// this will give you current day of week
    
    [components setDay:([components day] - ((dayofweek) - 2))];// for beginning of the week.
    
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateFormat_first = [[NSDateFormatter alloc] init];
    [dateFormat_first setDateFormat:@"MM/dd/yyyy"];
    
    NSString* dateString2Prev = [dateFormatNew stringFromDate:beginningOfWeek];
    
    NSDate* weekstartPrev = [dateFormat_first dateFromString:dateString2Prev];
    
    NSLog(@"%@",weekstartPrev);
    
    [dictForStartEndDate setObject:dateString2Prev forKey:@"StartDate"];
    
    
    
    NSCalendar *gregorianEnd = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    // NSDateComponents *componentsEnd = [gregorianEnd components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:today];
    NSDateComponents *componentsEnd = [gregorianEnd components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:dateNew];
    
    NSInteger Enddayofweek = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:dateNew] weekday];// this will give you current day of week
    
    [componentsEnd setDay:([componentsEnd day]+(7-Enddayofweek)+1)];// for end day of the week
    
    NSDate *EndOfWeek = [gregorianEnd dateFromComponents:componentsEnd];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    NSString* dateEndPrev = [dateFormatNew stringFromDate:EndOfWeek];
    
    NSDate* weekEndPrev = [dateFormat_End dateFromString:dateEndPrev] ;
    NSLog(@"%@",weekEndPrev);
    
    [dictForStartEndDate setObject:dateEndPrev forKey:@"EndDate"];
    
}
@end
