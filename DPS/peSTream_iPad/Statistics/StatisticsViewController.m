//
//  StatisticsViewController.m
//  DPS
//
//  Created by Saavan Patidar on 15/01/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import "StatisticsViewController.h"
#import "AllImportsViewController.h"

@interface StatisticsViewController ()
{
    
    NSMutableArray *arrOfStatsData,*arrOfStatsData2;
    Global *global;
    NSString *strDayType;
    NSString *strStartDate,*strEndDate;
    NSMutableArray *arrTitle;
}

@end

@implementation StatisticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    global=[[Global alloc]init];
    arrOfStatsData=[NSMutableArray new];
    arrOfStatsData2=[NSMutableArray new];

    arrTitle=[[NSMutableArray alloc]init];
   /*[arrOfStatsData addObject:@"# of Leads : 123"];
    [arrOfStatsData addObject:@"# of Opportunity : 512"];
    [arrOfStatsData addObject:@"Proposed values : $10000"];
    [arrOfStatsData addObject:@"Sold pending : $234587"];
    [arrOfStatsData addObject:@"Sold won : $324430"];
    [arrOfStatsData addObject:@"# of Leads : 123"];
    [arrOfStatsData addObject:@"# of Opportunity : 512"];
    [arrOfStatsData addObject:@"Proposed values : $10000"];
    [arrOfStatsData addObject:@"Sold pending : $234587"];
    [arrOfStatsData addObject:@"Sold won : $324430"];
    [arrOfStatsData addObject:@"# of Leads : 123"];
    [arrOfStatsData addObject:@"# of Opportunity : 512"];
    [arrOfStatsData addObject:@"Proposed values : $10000"];
    [arrOfStatsData addObject:@"Sold pending : $234587"];
    [arrOfStatsData addObject:@"Sold won : $324430"];
    [arrOfStatsData addObject:@"# of Leads : 123"];
    [arrOfStatsData addObject:@"# of Opportunity : 512"];
    [arrOfStatsData addObject:@"Proposed values : $10000"];
    [arrOfStatsData addObject:@"Sold pending : $234587"];
    [arrOfStatsData addObject:@"Sold won : $324430"];*/
    // Do any additional setup after loading the view.
    
    [self fetchStasticsData];
   
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    if ([defs boolForKey:@"fromStatisticsSetting"]==YES)
    {
        [self fetchStasticsData];
        [defs setBool:NO forKey:@"fromStatisticsSetting"];
        [defs synchronize];
    }
}
-(void)fetchStasticsData
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strDayType=[defs valueForKey:@"dayType"];
    
    if ([strDayType isEqual:nil]||[strDayType isEqual:@""]|| strDayType.length==0)
    {
        strDayType=@"Today";
    }
    
    StasticsClass *objDelegate=[[StasticsClass alloc]init];
    objDelegate.delegate=self;
    [objDelegate getStart_EndDate:strDayType];
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        
    }
    else
    {
        [self getData];
    }
}
-(void)getDelegateData:(NSDictionary *)dict
{
    NSLog(@"%@",dict);
    strStartDate=[dict valueForKey:@"StartDate"];
    strEndDate=[dict valueForKey:@"EndDate"];
   
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark-  ------------- Button Action Methods -------------

- (IBAction)action_Back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark-  ------------- Tableview Delegates methods -------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return tableView.rowHeight;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrOfStatsData.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    tblCell *cell = (tblCell *)[tableView dequeueReusableCellWithIdentifier:@"StatisticsCell" forIndexPath:indexPath];
    NSDictionary *dict=[arrOfStatsData objectAtIndex:indexPath.row];
    cell.lblStats.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
    cell.imgViewStats.image=[UIImage imageNamed:@"analytics.png"];
    [tableView setSeparatorColor:[UIColor clearColor]];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = tableView.frame;
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-20, 50)];
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    NSString *sectionName;
    
    sectionName = @"Statistics";
    
    title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-20, 50)];
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    title.text = sectionName;
    title.backgroundColor=[UIColor clearColor];
    title.textColor=[UIColor blackColor];
    title.font=[UIFont boldSystemFontOfSize:20];
    
    headerView.backgroundColor=[UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:1.0f];
    [headerView addSubview:title];
    
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 50;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 5;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -600;


    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;

    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);


    //3. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];

}

- (IBAction)action_Settings:(id)sender {
    
    if([UIScreen mainScreen].bounds.size.height<1000)
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        StatisticsSettingsViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"StatisticsSettingsViewControlleriPhone"];
        [self presentViewController:objByProductVC animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        StatisticsSettingsViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"StatisticsSettingsViewController"];
        [self presentViewController:objByProductVC animated:YES completion:nil];
    }
    
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrOfStatsData.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"StatsCell";
    
    ServiceGraphCollectionViewCell *cell = (ServiceGraphCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSDictionary *dict=[arrOfStatsData objectAtIndex:indexPath.row];

    cell.lblStats.text=[NSString stringWithFormat:@"%@ : %@",[dict valueForKey:@"Title"],[dict valueForKey:@"Value"]];
    
    /*cell.contentView.layer.cornerRadius =  20;
    cell.contentView.layer.borderWidth = 2.0;
    cell.contentView.layer.borderColor = [[UIColor clearColor]CGColor];
    cell.contentView.layer.masksToBounds = YES;
    
    cell.contentView.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.contentView.layer.shadowOffset = CGSizeMake(0, 2);
    cell.contentView.layer.shadowRadius = 2.0;
    cell.layer.shadowOpacity = 0.5;
    cell.layer.masksToBounds = NO;
    cell.backgroundColor = [UIColor whiteColor];
    cell.lblStats.backgroundColor = [UIColor whiteColor];*/
   // cell.layer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:cell.contentView.layer.bounds cornerRadius:cell.contentView.layer.cornerRadius]CGPath];
    
   /* cell.contentView.layer.cornerRadius = 20
    cell.contentView.layer.borderWidth = 1.0
    cell.contentView.layer.borderColor = UIColor.clear.cgColor
    cell.contentView.layer.masksToBounds = true
    cell.layer.shadowColor = UIColor.black.cgColor
    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
    cell.layer.shadowRadius = 2.0
    cell.layer.shadowOpacity = 0.5
    cell.layer.masksToBounds = false
    cell.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath*/

    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat widthh = [UIScreen mainScreen].bounds.size.width/3;
    if ([UIScreen mainScreen].bounds.size.height<600)
    {
        widthh = [UIScreen mainScreen].bounds.size.width/2;
    }
    return CGSizeMake(widthh-10, widthh-10);
    
}

- (void)collectionView:(UICollectionView *)collectionView
       willDisplayCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.alpha = 0.4;
    cell.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void)
     {
         cell.alpha = 1;
         cell.transform = CGAffineTransformMakeScale(1, 1);
     } completion:^(BOOL finished){ }];
    
}


/*
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    if(section==0)
    {
    return arrOfStatsData2.count;
    }
    else
         return arrOfStatsData.count;
        
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"StatsCell";
   
    ServiceGraphCollectionViewCell *cell = (ServiceGraphCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (indexPath.section==0)
    {
        NSDictionary *dict=[arrOfStatsData2 objectAtIndex:indexPath.row];
        
        
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"Amount"]]doubleValue]==0)
        {
            
            cell.lblStats.text=[NSString stringWithFormat:@"%@ : %@",[dict valueForKey:@"Title"],[dict valueForKey:@"Value"]];
            
        }
        else
        {
            cell.lblStats.text=[NSString stringWithFormat:@"%@ : $%@",[dict valueForKey:@"Title"],[dict valueForKey:@"Amount"]];
        }
    }
    else
    {
        NSDictionary *dict=[arrOfStatsData objectAtIndex:indexPath.row];
        
        
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"Amount"]]doubleValue]==0)
        {
            
            cell.lblStats.text=[NSString stringWithFormat:@"%@ : %@",[dict valueForKey:@"Title"],[dict valueForKey:@"Value"]];
            
        }
        else
        {
            cell.lblStats.text=[NSString stringWithFormat:@"%@ : $%@",[dict valueForKey:@"Title"],[dict valueForKey:@"Amount"]];
        }
    }
    if([UIScreen mainScreen].bounds.size.height<1000)
    {
        cell.lblStats.font=[UIFont boldSystemFontOfSize:12];
    }
   // [ cell.btnShow addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat widthh = [UIScreen mainScreen].bounds.size.width/3;
    
    return CGSizeMake(widthh-10, widthh-10);
    
}

- (void)collectionView:(UICollectionView *)collectionView
       willDisplayCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.alpha = 0.4;
    cell.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void)
     {
         cell.alpha = 1;
         cell.transform = CGAffineTransformMakeScale(1, 1);
     } completion:^(BOOL finished){ }];
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return arrTitle.count;
//    if (UICollectionElementKindSectionHeader)
//    {
//        return arrTitle.count;
//
//    }
//    else
//    {
//        return 0;
//    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    
    if(section==0)
    {
        
        if (arrOfStatsData2.count==0)
        {
            return CGSizeMake([UIScreen mainScreen].bounds.size.width, 0);

        }
        else
        {
            return CGSizeMake([UIScreen mainScreen].bounds.size.width, 50);
        }
    }
    else
    {
        if (arrOfStatsData.count==0)
        {
            return CGSizeMake([UIScreen mainScreen].bounds.size.width, 0);
            
        }
        else
        {
            return CGSizeMake([UIScreen mainScreen].bounds.size.width, 50);
            
        }
        
    }
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ServiceGraphCollectionViewCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        //NSString *title = [[NSString alloc]initWithFormat:@"Recipe Group #%i", indexPath.section + 1];
        headerView.lblHeader.font=[UIFont boldSystemFontOfSize:22];
        if([UIScreen mainScreen].bounds.size.height<1000)
        {
            headerView.lblHeader.font=[UIFont boldSystemFontOfSize:14];
        }
        headerView.lblHeader.textColor=[UIColor whiteColor];
        headerView.lblHeader.text =[NSString stringWithFormat:@"   %@", [arrTitle objectAtIndex:indexPath.section]];
        headerView.btnShow.tag=indexPath.section;
       // [headerView.btnShow setTitle:@"Show" forState:UIControlStateNormal];
      //  [ headerView.btnShow addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];

        //@"Header Titile";
        //UIImage *headerImage = [UIImage imageNamed:@"header_banner.png"];
       // headerView.backgroundImage.image = headerImage;
        
        reusableview = headerView;
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        
        reusableview = footerview;
    }
    
    return reusableview;
}
- (void) functionName:(UIButton *) sender {
    
    NSLog(@"Tag : %ld", (long)sender.tag);
    
    if (sender.tag==0)
    {
        arrOfStatsData2=[[NSMutableArray alloc]init];
        [global AlertMethod:@"Alert!" :@"For first section"];
    }
    else if (sender.tag==1)
    {
        arrOfStatsData=[[NSMutableArray alloc]init];

        [global AlertMethod:@"Alert!" :@"For second section"];
    }
    
}
 */



#pragma mark- -------- API CALL --------------

-(void)getData
{
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
   NSString*  strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
   // strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
   NSString* strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
   NSString * strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
      
    }
    else
    {
        
        NSString *strUrl;
        
        strUrl = [NSString stringWithFormat:@"%@%@%@&StartDate=%@&EndDate=%@&employeeid=%@",strServiceUrlMain,URLGetStastics,strCompanyKey,strStartDate,strEndDate,strEmpID],//@"terminix"];
        
        NSLog(@"Statstics Url ----%@",strUrl);
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Statistics..."];
        
        //============================================================================
        //============================================================================
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getSynchronousServerResponseForUrl:strUrl :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (success)
                     {

                         if (response.count==0)
                         {
                             NSString *strTitle = Alert;
                             NSString *strMsg = @"No record found";
                             [global AlertMethod:strTitle :strMsg];
                         }
                         else
                         {
                             //NSArray *arrLeadDashboard=[response valueForKey:@"LeadDeshboards"];
                             NSArray *arrOpportunityDeshboards=[response valueForKey:@"OpportunityReturnDeshboards"];
                             arrOfStatsData=[[NSMutableArray alloc]init];
                             
                             //arrTitle=[[NSMutableArray alloc]init];
                             [arrOfStatsData addObjectsFromArray:arrOpportunityDeshboards];
                            
//                             [arrTitle addObject:@"Lead Details"];
//                             [arrTitle addObject:@"Opportunity Details"];
//
//                             arrOfStatsData2=[[NSMutableArray alloc]init];
//                             [arrOfStatsData2 addObjectsFromArray:arrLeadDashboard];

                         }
                         [_collectionViewStats reloadData];
                         [DejalBezelActivityView removeView];
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                         [DejalBezelActivityView removeView];
                     }
                 });
             }];
        });
    }
}
@end
