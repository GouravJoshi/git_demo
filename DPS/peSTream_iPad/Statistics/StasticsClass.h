//
//  StasticsClass.h
//  DPS
//
//  Created by Rakesh Jain on 22/01/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol StasticsClassDelegate

-(void)getDelegateData:(NSDictionary *)dict;
@end


@interface StasticsClass : NSObject
@property(nonatomic,assign) id<StasticsClassDelegate> delegate;
-(void)getStart_EndDate:(NSString *)strDayType;

@end
