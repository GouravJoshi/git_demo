//
//  StatisticsSettingsViewController.m
//  DPS
//
//  Created by Saavan Patidar on 15/01/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import "StatisticsSettingsViewController.h"
#import "AllImportsViewController.h"



@interface StatisticsSettingsViewController ()
{
    
    NSMutableArray *arrOfStatsData;
    int selectedRow;
    NSMutableDictionary *dictForStartEndDate;
    
    NSString *strFirstQuarterStartDate,*strFirstQuarterEndDate;
    NSString *strSecondQuarterStartDate,*strSecondQuarterEndDate;
    NSString *strThirdQuarterStartDate,*strThirdQuarterEndDate;
    
    BOOL chkForLastQuarter;
    NSString *strStartDate,*strEndDate;
   
    NSString* strDayType;
}
@end

@implementation StatisticsSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedRow = 0;
    chkForLastQuarter=NO;
    arrOfStatsData=[NSMutableArray new];
    [arrOfStatsData addObject:@"Today"];
    [arrOfStatsData addObject:@"Yesterday"];
    [arrOfStatsData addObject:@"This Week"];
    [arrOfStatsData addObject:@"Last Week"];
    [arrOfStatsData addObject:@"This Month"];
    [arrOfStatsData addObject:@"Last Month"];
    [arrOfStatsData addObject:@"This Quarter"];
    [arrOfStatsData addObject:@"Last Quarter"];
    [arrOfStatsData addObject:@"This Year"];
    [arrOfStatsData addObject:@"Last Year"];
    /*[arrOfStatsData addObject:@"Last 2 Year"];
    [arrOfStatsData addObject:@"Last 3 Year"];
    [arrOfStatsData addObject:@"Last 4 Year"];
    [arrOfStatsData addObject:@"Last 5 Year"];
    [arrOfStatsData addObject:@"Till Now"];*/

    // Do any additional setup after loading the view.
    dictForStartEndDate=[[NSMutableDictionary alloc]init];

   
   /* [self firstQuarter];
    [self secondQuarter];
    [self thirdQuarter];*/
    
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     strDayType=[defs valueForKey:@"dayType"];
    if ([strDayType isEqual:nil]||[strDayType isEqual:@""]|| strDayType.length==0)
    {
        strDayType=@"Today";
    }
    
    StasticsClass *objDelegate=[[StasticsClass alloc]init];
    objDelegate.delegate=self;
    [objDelegate getStart_EndDate:strDayType];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-  ------------- Button Action Methods -------------

- (IBAction)action_Back:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"fromStatisticsSetting"];
    [defs synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark-  ------------- Tableview Delegates methods -------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return tableView.rowHeight;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrOfStatsData.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    tblCell *cell = (tblCell *)[tableView dequeueReusableCellWithIdentifier:@"StatisticsCell" forIndexPath:indexPath];
    cell.lblStats.text=arrOfStatsData[indexPath.row];
    cell.imgViewStats.image=[UIImage imageNamed:@"repairing-service.png"];
    [tableView setSeparatorColor:[UIColor clearColor]];
    
    if (selectedRow==indexPath.row) {
        
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        
    } else {
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    }
    if ([arrOfStatsData[indexPath.row] isEqualToString:strDayType])
    {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    if([UIScreen mainScreen].bounds.size.height<1000)
    {
        cell.lblStats.font=[UIFont boldSystemFontOfSize:12];
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = tableView.frame;
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-20, 50)];
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    NSString *sectionName;
    
    sectionName = @"Time";
    
    title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-20, 50)];
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    title.text = sectionName;
    title.backgroundColor=[UIColor clearColor];
    title.textColor=[UIColor blackColor];
    
    title.font=[UIFont boldSystemFontOfSize:22];
    
    if([UIScreen mainScreen].bounds.size.height<1000)
    {
        title.font=[UIFont boldSystemFontOfSize:14];
    }
    
    headerView.backgroundColor=[UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:1.0f];
    [headerView addSubview:title];
    
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 50;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 5;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedRow = indexPath.row;
    
    [_tblViewStatisticsSettings reloadData];
    
    NSIndexPath* ipath = [NSIndexPath indexPathForRow: selectedRow inSection: 0];
    
    [_tblViewStatisticsSettings scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: NO];
    
   // [self getStart_EndDate:[arrOfStatsData objectAtIndex:indexPath.row]];
    
    //Delegate Via Nilind
    
    StasticsClass *objDelegate=[[StasticsClass alloc]init];
    objDelegate.delegate=self;
    [objDelegate getStart_EndDate:[arrOfStatsData objectAtIndex:indexPath.row]];
}

/*
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.alpha = 0.4;
    cell.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void)
     {
         cell.alpha = 1;
         cell.transform = CGAffineTransformMakeScale(1, 1);
     } completion:^(BOOL finished){ }];
    
}
*/

#pragma mark- ------- DELEGATE RESPONSE ------
-(void)getDelegateData:(NSDictionary *)dict
{
   // NSLog(@"%@",dict);
    strStartDate=[dict valueForKey:@"StartDate"];
    strEndDate=[dict valueForKey:@"EndDate"];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strDayType=[defs valueForKey:@"dayType"];
    [_tblViewStatisticsSettings reloadData];
    
}
- (IBAction)actionOnDone:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"fromStatisticsSetting"];
    [defs synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
