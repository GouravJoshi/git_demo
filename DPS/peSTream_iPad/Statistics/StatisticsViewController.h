//
//  StatisticsViewController.h
//  DPS
//
//  Created by Saavan Patidar on 15/01/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StasticsClass.h"
@interface StatisticsViewController : UIViewController<StasticsClassDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewStatistics;
- (IBAction)action_Settings:(id)sender;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewStats;

@end
