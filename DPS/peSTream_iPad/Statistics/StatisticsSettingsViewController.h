//
//  StatisticsSettingsViewController.h
//  DPS
//
//  Created by Saavan Patidar on 15/01/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StasticsClass.h"
@interface StatisticsSettingsViewController : UIViewController<StasticsClassDelegate>

- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewStatisticsSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
- (IBAction)actionOnDone:(id)sender;

@end
